/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Is Prefix Notation QType1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.IsPrefixNotationQType1#getIsPrefixNotation <em>Is Prefix Notation</em>}</li>
 *   <li>{@link Ada.IsPrefixNotationQType1#getNotAnElement <em>Not An Element</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getIsPrefixNotationQType1()
 * @model extendedMetaData="name='is_prefix_notation_q_._1_._type' kind='elementOnly'"
 * @generated
 */
public interface IsPrefixNotationQType1 extends EObject {
	/**
	 * Returns the value of the '<em><b>Is Prefix Notation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Prefix Notation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Prefix Notation</em>' containment reference.
	 * @see #setIsPrefixNotation(IsPrefixNotation)
	 * @see Ada.AdaPackage#getIsPrefixNotationQType1_IsPrefixNotation()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='is_prefix_notation' namespace='##targetNamespace'"
	 * @generated
	 */
	IsPrefixNotation getIsPrefixNotation();

	/**
	 * Sets the value of the '{@link Ada.IsPrefixNotationQType1#getIsPrefixNotation <em>Is Prefix Notation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Prefix Notation</em>' containment reference.
	 * @see #getIsPrefixNotation()
	 * @generated
	 */
	void setIsPrefixNotation(IsPrefixNotation value);

	/**
	 * Returns the value of the '<em><b>Not An Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not An Element</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not An Element</em>' containment reference.
	 * @see #setNotAnElement(NotAnElement)
	 * @see Ada.AdaPackage#getIsPrefixNotationQType1_NotAnElement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not_an_element' namespace='##targetNamespace'"
	 * @generated
	 */
	NotAnElement getNotAnElement();

	/**
	 * Sets the value of the '{@link Ada.IsPrefixNotationQType1#getNotAnElement <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not An Element</em>' containment reference.
	 * @see #getNotAnElement()
	 * @generated
	 */
	void setNotAnElement(NotAnElement value);

} // IsPrefixNotationQType1
