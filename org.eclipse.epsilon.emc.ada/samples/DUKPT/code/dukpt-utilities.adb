--------------------------------------------------------------------
-- Copyright Altran UK
-- HITEA2
--
-- File:
--    $Id: dukpt-utilities.adb 2379 2017-02-28 10:02:48Z aturner@EUROPE $
--
-- Description:
--    Not all of these subprograms are used in the implementation but are
--    useful for debugging and so are retained:
--       To_Binary, To_Hex, Pop_Count
--------------------------------------------------------------------
--------------------------------------------------------------------
-- Implementation Notes:
--
--------------------------------------------------------------------
with Ada.Text_IO; use Ada.Text_IO;

package body DUKPT.Utilities
is
   --------------------------------------------------------------------
   -- Implementation Notes:
   --
   --------------------------------------------------------------------
   procedure Generate_LRC_Byte(FKR_Block : in out B_Block136)
   is
      LRC : Crypto.Types.Byte;
   begin
      LRC := FKR_Block(0);
      for A in 1 .. 15 loop
         LRC := FKR_Block(A) xor LRC;
      end loop;
      FKR_Block(16) := LRC;
   end Generate_LRC_Byte;

   --------------------------------------------------------------------
   -- Implementation Notes:
   --
   --------------------------------------------------------------------
   procedure Validate_LRC_Byte(FKR_Block : in     B_Block136;
                               LRC_Valid :    out Boolean)
   is
      LRC : Crypto.Types.Byte;
   begin
      LRC := FKR_Block(0);
      for A in 1 .. 15 loop
         LRC := FKR_Block(A) xor LRC;
      end loop;
      if FKR_Block(16) = LRC then
         LRC_Valid := True;
      else
         LRC_Valid := False;
      end if;
   end Validate_LRC_Byte;

   --------------------------------------------------------------------
   -- Implementation Notes:
   --    To invalidate the LRC byte, ANS X9.24-1:2009 suggests
   --    incrementing the LRC byte but this could overflow so
   --    instead we do something almost as simple.
   --------------------------------------------------------------------
   procedure Invalidate_LRC_Byte(FKR_Block : in out B_Block136)
   is
   begin
      if FKR_Block(16) < 2 then
         FKR_Block(16) := FKR_Block(16) + 1;
      else
         FKR_Block(16) := FKR_Block(16) - 1;
      end if;
   end Invalidate_LRC_Byte;

   --------------------------------------------------------------------
   -- Implementation Notes:
   --
   --------------------------------------------------------------------
   procedure Increment_EC(EC        : in out Interfaces.Unsigned_32;
                          Increment : in     Interfaces.Unsigned_32)
   is
      Overflow_Threshold : Unsigned_32 := 2#111111111111111111111#;
   begin
      if EC < Overflow_Threshold and Increment < Overflow_Threshold then
         EC := EC + Increment;
         pragma Assert_And_Cut(EC <= Overflow_Threshold, "EC OVERFLOW - DEVICE IS DEAD");
      else
         null;
         -- TODO what do we do when bricking?
      end if;
   end Increment_EC;

   --------------------------------------------------------------------
   -- Implementation Notes:
   --    Need to chunk the EC and then read overwrite the last 4 bytes
   --    in the KSNR.
   --------------------------------------------------------------------
   procedure Update_KSNR(EC   : in     Interfaces.Unsigned_32;
                         KSNR : in out Crypto.Types.B_Block128)
   is
      EC_Chunks : Crypto.Types.B_Block32;
   begin
      EC_Chunks := Chunk_32(EC);
      KSNR(12) := EC_Chunks(0);
      KSNR(13) := EC_Chunks(1);
      KSNR(14) := EC_Chunks(2);
      KSNR(15) := EC_Chunks(3);
   end Update_KSNR;

   --------------------------------------------------------------------
   -- Implementation Notes:
   --    This is for debug and dev tool only - not used in DUKPT
   --    implementation.
   --------------------------------------------------------------------
   package IIO is new Ada.Text_IO.Integer_IO(Integer);

   function To_Binary(N: Natural) return String is
      --pragma SPARK_Mode (Off);
      S: String(1 .. 64); -- more than plenty!
      Left:  Positive := S'First;
      Right: Positive := S'Last;
   begin
      IIO.Put(To => S, Item => N, Base => 2); -- This is the conversion!
      -- Now S is a String with many spaces and some "2#...#" somewhere.
      -- We only need the "..." part without spaces or base markers.
      while S(Left) /= '#' loop
         Left := Left + 1;
      end loop;
      while S(Right) /= '#' loop
         Right := Right - 1;
      end loop;
      return S(Left+1 .. Right-1);
   end To_Binary;

   --------------------------------------------------------------------
   -- Implementation Notes:
   --    This is for debug and dev tool only - not used in DUKPT
   --    implementation.
   --------------------------------------------------------------------
   function To_Hex(N: Natural) return String is
      --pragma SPARK_Mode (Off);
      S: String(1 .. 64); -- more than plenty!
      Left:  Positive := S'First;
      Right: Positive := S'Last;
   begin
      IIO.Put(To => S, Item => N, Base => 16); -- This is the conversion!
      -- Now S is a String with many spaces and some "2#...#" somewhere.
      -- We only need the "..." part without spaces or base markers.
      while S(Left) /= '#' loop
         Left := Left + 1;
      end loop;
      while S(Right) /= '#' loop
         Right := Right - 1;
      end loop;
      return S(Left+1 .. Right-1);
   end To_Hex;

   --------------------------------------------------------------------
   -- Implementation Notes:
   --    Get Hamming weight.
   --
   --------------------------------------------------------------------
   function Pop_Count(N : Interfaces.Unsigned_32) return Natural is
      K5555 : constant Interfaces.Unsigned_32 := 16#55555555#;
      K3333 : constant Interfaces.Unsigned_32 := 16#33333333#;
      K0f0f : constant Interfaces.Unsigned_32 := 16#0f0f0f0f#;
      K0101 : constant Interfaces.Unsigned_32 := 16#01010101#;
      X     :          Interfaces.Unsigned_32 := N;
   begin
      X :=  X            - (Shift_Right(X, 1)  and k5555);
      X := (X and k3333) + (Shift_Right(X, 2)  and k3333);
      X := (X            + (Shift_Right(X, 4)) and K0f0f);
      X := Shift_Right((X * k0101), 24);
      return Natural(X);
   end Pop_Count;

   --------------------------------------------------------------------
   -- Implementation Notes:
   --    Chunk a 32 bit unsigned into 4 bytes.
   --    Return bytes in an array - can use Crypto types.
   --
   --------------------------------------------------------------------
   function Chunk_32(Block : Interfaces.Unsigned_32) return Crypto.Types.B_Block32 is
      tmp : Interfaces.Unsigned_32;
      -- Most significant to least significant 8 bits respectively:
      Byte_0, Byte_1, Byte_2, Byte_3: Crypto.Types.Byte;
      Return_Block : Crypto.Types.B_Block32;
   begin
      tmp := Interfaces.Shift_Left(Block, 24);
      Byte_3 := Crypto.Types.Byte(Interfaces.Shift_Right(tmp, 24));

      tmp := Interfaces.Shift_Left(Block, 16);
      Byte_2 := Crypto.Types.Byte(Interfaces.Shift_Right(tmp, 24));

      tmp := Interfaces.Shift_Left(Block, 8);
      Byte_1 := Crypto.Types.Byte(Interfaces.Shift_Right(tmp, 24));

      Byte_0 := Crypto.Types.Byte(Interfaces.Shift_Right(Block, 24));

      Return_Block := (0 => Byte_0, 1 => Byte_1,
                       2 => Byte_2, 3 => Byte_3);
      return Return_Block;
   end Chunk_32;

   --------------------------------------------------------------------
   -- Implementation Notes:
   --    The return index is numbered from left to right #0 to #20 for
   --    the 21 possible positions of the SR.
   --------------------------------------------------------------------
   function Shift_Register_Position(SR : Interfaces.Unsigned_32) return Natural_0_20 is
      Return_Position : Natural_0_20 := 0;
      SR_Test : Interfaces.Unsigned_32 := 2#100000000000000000000#;
   begin
      for Bit_Index in 0 .. 20 loop
         if SR = SR_Test then
            Return_Position := Natural_0_20(Bit_Index);
            exit;
         end if;
         SR_Test := Interfaces.Shift_Right(SR_Test, 1);
      end loop;
      return Return_Position;
   end Shift_Register_Position;

   --------------------------------------------------------------------
   -- Implementation Notes:
   --    Gets the Nth most significant bit of the Subject and sets the
   --    equivilent bit in Sub_N_Most_Sig.
   --
   --    N is 0 indexed but what matters if the N'First means
   --    return Subject as the Target. N'First + 1 means return the most
   --    significant (Population - 1) bits set in Target.
   --
   --    Also return a shift Register with the set bit corresponding to
   --    the least significant bit left in Target.
   --------------------------------------------------------------------
   procedure Get_N_Most_Sig_Bits(Subject        : in     Interfaces.Unsigned_32;
                                 N              : in     Interfaces.Unsigned_32;
                                 Population     : in     Interfaces.Unsigned_32;
                                 Sub_N_Most_Sig :    out Interfaces.Unsigned_32;
                                 Shift_Register :    out Interfaces.Unsigned_32)
   is
      Sub_Temp      : Interfaces.Unsigned_32 := Subject;
      Count         : Interfaces.Unsigned_32 := 0;
      Set_Bit_Index : Natural                := 0;
   begin
      -- Loop until we find the right number of set bits
      for Ind in 0 .. 20 loop
         if Sub_Temp mod 2 /= 0 then
            if N = 1 and Sub_Temp = 1 then
               Set_Bit_Index := Ind;
               exit;
            elsif Count = Population - N then
               Set_Bit_Index := Ind;
               exit;
            else
               Count := Count + 1;
            end if;
         end if;
         Sub_Temp := Interfaces.Shift_Right(Sub_Temp, 1);
      end loop;

      Sub_N_Most_Sig := Interfaces.Shift_Right(Subject, Set_Bit_Index);
      Sub_N_Most_Sig := Interfaces.Shift_Left(Sub_N_Most_Sig, Set_Bit_Index);

      Shift_Register := Interfaces.Shift_Left(2#1#, Integer(Set_Bit_Index));

   end Get_N_Most_Sig_Bits;

end DUKPT.Utilities;
