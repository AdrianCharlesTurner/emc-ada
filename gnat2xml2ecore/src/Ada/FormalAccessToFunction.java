/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Formal Access To Function</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.FormalAccessToFunction#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.FormalAccessToFunction#getHasNullExclusionQ <em>Has Null Exclusion Q</em>}</li>
 *   <li>{@link Ada.FormalAccessToFunction#getAccessToSubprogramParameterProfileQl <em>Access To Subprogram Parameter Profile Ql</em>}</li>
 *   <li>{@link Ada.FormalAccessToFunction#getIsNotNullReturnQ <em>Is Not Null Return Q</em>}</li>
 *   <li>{@link Ada.FormalAccessToFunction#getAccessToFunctionResultProfileQ <em>Access To Function Result Profile Q</em>}</li>
 *   <li>{@link Ada.FormalAccessToFunction#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getFormalAccessToFunction()
 * @model extendedMetaData="name='Formal_Access_To_Function' kind='elementOnly'"
 * @generated
 */
public interface FormalAccessToFunction extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getFormalAccessToFunction_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.FormalAccessToFunction#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Has Null Exclusion Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Null Exclusion Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Null Exclusion Q</em>' containment reference.
	 * @see #setHasNullExclusionQ(HasNullExclusionQType9)
	 * @see Ada.AdaPackage#getFormalAccessToFunction_HasNullExclusionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='has_null_exclusion_q' namespace='##targetNamespace'"
	 * @generated
	 */
	HasNullExclusionQType9 getHasNullExclusionQ();

	/**
	 * Sets the value of the '{@link Ada.FormalAccessToFunction#getHasNullExclusionQ <em>Has Null Exclusion Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Null Exclusion Q</em>' containment reference.
	 * @see #getHasNullExclusionQ()
	 * @generated
	 */
	void setHasNullExclusionQ(HasNullExclusionQType9 value);

	/**
	 * Returns the value of the '<em><b>Access To Subprogram Parameter Profile Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Subprogram Parameter Profile Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Subprogram Parameter Profile Ql</em>' containment reference.
	 * @see #setAccessToSubprogramParameterProfileQl(ParameterSpecificationList)
	 * @see Ada.AdaPackage#getFormalAccessToFunction_AccessToSubprogramParameterProfileQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='access_to_subprogram_parameter_profile_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ParameterSpecificationList getAccessToSubprogramParameterProfileQl();

	/**
	 * Sets the value of the '{@link Ada.FormalAccessToFunction#getAccessToSubprogramParameterProfileQl <em>Access To Subprogram Parameter Profile Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access To Subprogram Parameter Profile Ql</em>' containment reference.
	 * @see #getAccessToSubprogramParameterProfileQl()
	 * @generated
	 */
	void setAccessToSubprogramParameterProfileQl(ParameterSpecificationList value);

	/**
	 * Returns the value of the '<em><b>Is Not Null Return Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Not Null Return Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Not Null Return Q</em>' containment reference.
	 * @see #setIsNotNullReturnQ(IsNotNullReturnQType8)
	 * @see Ada.AdaPackage#getFormalAccessToFunction_IsNotNullReturnQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='is_not_null_return_q' namespace='##targetNamespace'"
	 * @generated
	 */
	IsNotNullReturnQType8 getIsNotNullReturnQ();

	/**
	 * Sets the value of the '{@link Ada.FormalAccessToFunction#getIsNotNullReturnQ <em>Is Not Null Return Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Not Null Return Q</em>' containment reference.
	 * @see #getIsNotNullReturnQ()
	 * @generated
	 */
	void setIsNotNullReturnQ(IsNotNullReturnQType8 value);

	/**
	 * Returns the value of the '<em><b>Access To Function Result Profile Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Function Result Profile Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Function Result Profile Q</em>' containment reference.
	 * @see #setAccessToFunctionResultProfileQ(ElementClass)
	 * @see Ada.AdaPackage#getFormalAccessToFunction_AccessToFunctionResultProfileQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='access_to_function_result_profile_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementClass getAccessToFunctionResultProfileQ();

	/**
	 * Sets the value of the '{@link Ada.FormalAccessToFunction#getAccessToFunctionResultProfileQ <em>Access To Function Result Profile Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access To Function Result Profile Q</em>' containment reference.
	 * @see #getAccessToFunctionResultProfileQ()
	 * @generated
	 */
	void setAccessToFunctionResultProfileQ(ElementClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getFormalAccessToFunction_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.FormalAccessToFunction#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // FormalAccessToFunction
