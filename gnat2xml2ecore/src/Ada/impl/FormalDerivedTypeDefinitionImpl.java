/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.ExpressionClass;
import Ada.ExpressionList;
import Ada.FormalDerivedTypeDefinition;
import Ada.HasAbstractQType7;
import Ada.HasLimitedQType9;
import Ada.HasPrivateQType;
import Ada.HasSynchronizedQType;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Formal Derived Type Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.FormalDerivedTypeDefinitionImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.FormalDerivedTypeDefinitionImpl#getHasAbstractQ <em>Has Abstract Q</em>}</li>
 *   <li>{@link Ada.impl.FormalDerivedTypeDefinitionImpl#getHasLimitedQ <em>Has Limited Q</em>}</li>
 *   <li>{@link Ada.impl.FormalDerivedTypeDefinitionImpl#getHasSynchronizedQ <em>Has Synchronized Q</em>}</li>
 *   <li>{@link Ada.impl.FormalDerivedTypeDefinitionImpl#getSubtypeMarkQ <em>Subtype Mark Q</em>}</li>
 *   <li>{@link Ada.impl.FormalDerivedTypeDefinitionImpl#getDefinitionInterfaceListQl <em>Definition Interface List Ql</em>}</li>
 *   <li>{@link Ada.impl.FormalDerivedTypeDefinitionImpl#getHasPrivateQ <em>Has Private Q</em>}</li>
 *   <li>{@link Ada.impl.FormalDerivedTypeDefinitionImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FormalDerivedTypeDefinitionImpl extends MinimalEObjectImpl.Container implements FormalDerivedTypeDefinition {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getHasAbstractQ() <em>Has Abstract Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasAbstractQ()
	 * @generated
	 * @ordered
	 */
	protected HasAbstractQType7 hasAbstractQ;

	/**
	 * The cached value of the '{@link #getHasLimitedQ() <em>Has Limited Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasLimitedQ()
	 * @generated
	 * @ordered
	 */
	protected HasLimitedQType9 hasLimitedQ;

	/**
	 * The cached value of the '{@link #getHasSynchronizedQ() <em>Has Synchronized Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasSynchronizedQ()
	 * @generated
	 * @ordered
	 */
	protected HasSynchronizedQType hasSynchronizedQ;

	/**
	 * The cached value of the '{@link #getSubtypeMarkQ() <em>Subtype Mark Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubtypeMarkQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass subtypeMarkQ;

	/**
	 * The cached value of the '{@link #getDefinitionInterfaceListQl() <em>Definition Interface List Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefinitionInterfaceListQl()
	 * @generated
	 * @ordered
	 */
	protected ExpressionList definitionInterfaceListQl;

	/**
	 * The cached value of the '{@link #getHasPrivateQ() <em>Has Private Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasPrivateQ()
	 * @generated
	 * @ordered
	 */
	protected HasPrivateQType hasPrivateQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FormalDerivedTypeDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getFormalDerivedTypeDefinition();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAbstractQType7 getHasAbstractQ() {
		return hasAbstractQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasAbstractQ(HasAbstractQType7 newHasAbstractQ, NotificationChain msgs) {
		HasAbstractQType7 oldHasAbstractQ = hasAbstractQ;
		hasAbstractQ = newHasAbstractQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_ABSTRACT_Q, oldHasAbstractQ, newHasAbstractQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasAbstractQ(HasAbstractQType7 newHasAbstractQ) {
		if (newHasAbstractQ != hasAbstractQ) {
			NotificationChain msgs = null;
			if (hasAbstractQ != null)
				msgs = ((InternalEObject)hasAbstractQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_ABSTRACT_Q, null, msgs);
			if (newHasAbstractQ != null)
				msgs = ((InternalEObject)newHasAbstractQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_ABSTRACT_Q, null, msgs);
			msgs = basicSetHasAbstractQ(newHasAbstractQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_ABSTRACT_Q, newHasAbstractQ, newHasAbstractQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasLimitedQType9 getHasLimitedQ() {
		return hasLimitedQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasLimitedQ(HasLimitedQType9 newHasLimitedQ, NotificationChain msgs) {
		HasLimitedQType9 oldHasLimitedQ = hasLimitedQ;
		hasLimitedQ = newHasLimitedQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_LIMITED_Q, oldHasLimitedQ, newHasLimitedQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasLimitedQ(HasLimitedQType9 newHasLimitedQ) {
		if (newHasLimitedQ != hasLimitedQ) {
			NotificationChain msgs = null;
			if (hasLimitedQ != null)
				msgs = ((InternalEObject)hasLimitedQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_LIMITED_Q, null, msgs);
			if (newHasLimitedQ != null)
				msgs = ((InternalEObject)newHasLimitedQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_LIMITED_Q, null, msgs);
			msgs = basicSetHasLimitedQ(newHasLimitedQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_LIMITED_Q, newHasLimitedQ, newHasLimitedQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasSynchronizedQType getHasSynchronizedQ() {
		return hasSynchronizedQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasSynchronizedQ(HasSynchronizedQType newHasSynchronizedQ, NotificationChain msgs) {
		HasSynchronizedQType oldHasSynchronizedQ = hasSynchronizedQ;
		hasSynchronizedQ = newHasSynchronizedQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_SYNCHRONIZED_Q, oldHasSynchronizedQ, newHasSynchronizedQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasSynchronizedQ(HasSynchronizedQType newHasSynchronizedQ) {
		if (newHasSynchronizedQ != hasSynchronizedQ) {
			NotificationChain msgs = null;
			if (hasSynchronizedQ != null)
				msgs = ((InternalEObject)hasSynchronizedQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_SYNCHRONIZED_Q, null, msgs);
			if (newHasSynchronizedQ != null)
				msgs = ((InternalEObject)newHasSynchronizedQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_SYNCHRONIZED_Q, null, msgs);
			msgs = basicSetHasSynchronizedQ(newHasSynchronizedQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_SYNCHRONIZED_Q, newHasSynchronizedQ, newHasSynchronizedQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getSubtypeMarkQ() {
		return subtypeMarkQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSubtypeMarkQ(ExpressionClass newSubtypeMarkQ, NotificationChain msgs) {
		ExpressionClass oldSubtypeMarkQ = subtypeMarkQ;
		subtypeMarkQ = newSubtypeMarkQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__SUBTYPE_MARK_Q, oldSubtypeMarkQ, newSubtypeMarkQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubtypeMarkQ(ExpressionClass newSubtypeMarkQ) {
		if (newSubtypeMarkQ != subtypeMarkQ) {
			NotificationChain msgs = null;
			if (subtypeMarkQ != null)
				msgs = ((InternalEObject)subtypeMarkQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__SUBTYPE_MARK_Q, null, msgs);
			if (newSubtypeMarkQ != null)
				msgs = ((InternalEObject)newSubtypeMarkQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__SUBTYPE_MARK_Q, null, msgs);
			msgs = basicSetSubtypeMarkQ(newSubtypeMarkQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__SUBTYPE_MARK_Q, newSubtypeMarkQ, newSubtypeMarkQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionList getDefinitionInterfaceListQl() {
		return definitionInterfaceListQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefinitionInterfaceListQl(ExpressionList newDefinitionInterfaceListQl, NotificationChain msgs) {
		ExpressionList oldDefinitionInterfaceListQl = definitionInterfaceListQl;
		definitionInterfaceListQl = newDefinitionInterfaceListQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__DEFINITION_INTERFACE_LIST_QL, oldDefinitionInterfaceListQl, newDefinitionInterfaceListQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefinitionInterfaceListQl(ExpressionList newDefinitionInterfaceListQl) {
		if (newDefinitionInterfaceListQl != definitionInterfaceListQl) {
			NotificationChain msgs = null;
			if (definitionInterfaceListQl != null)
				msgs = ((InternalEObject)definitionInterfaceListQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__DEFINITION_INTERFACE_LIST_QL, null, msgs);
			if (newDefinitionInterfaceListQl != null)
				msgs = ((InternalEObject)newDefinitionInterfaceListQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__DEFINITION_INTERFACE_LIST_QL, null, msgs);
			msgs = basicSetDefinitionInterfaceListQl(newDefinitionInterfaceListQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__DEFINITION_INTERFACE_LIST_QL, newDefinitionInterfaceListQl, newDefinitionInterfaceListQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasPrivateQType getHasPrivateQ() {
		return hasPrivateQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasPrivateQ(HasPrivateQType newHasPrivateQ, NotificationChain msgs) {
		HasPrivateQType oldHasPrivateQ = hasPrivateQ;
		hasPrivateQ = newHasPrivateQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_PRIVATE_Q, oldHasPrivateQ, newHasPrivateQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasPrivateQ(HasPrivateQType newHasPrivateQ) {
		if (newHasPrivateQ != hasPrivateQ) {
			NotificationChain msgs = null;
			if (hasPrivateQ != null)
				msgs = ((InternalEObject)hasPrivateQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_PRIVATE_Q, null, msgs);
			if (newHasPrivateQ != null)
				msgs = ((InternalEObject)newHasPrivateQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_PRIVATE_Q, null, msgs);
			msgs = basicSetHasPrivateQ(newHasPrivateQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_PRIVATE_Q, newHasPrivateQ, newHasPrivateQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_ABSTRACT_Q:
				return basicSetHasAbstractQ(null, msgs);
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_LIMITED_Q:
				return basicSetHasLimitedQ(null, msgs);
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_SYNCHRONIZED_Q:
				return basicSetHasSynchronizedQ(null, msgs);
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__SUBTYPE_MARK_Q:
				return basicSetSubtypeMarkQ(null, msgs);
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__DEFINITION_INTERFACE_LIST_QL:
				return basicSetDefinitionInterfaceListQl(null, msgs);
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_PRIVATE_Q:
				return basicSetHasPrivateQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__SLOC:
				return getSloc();
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_ABSTRACT_Q:
				return getHasAbstractQ();
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_LIMITED_Q:
				return getHasLimitedQ();
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_SYNCHRONIZED_Q:
				return getHasSynchronizedQ();
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__SUBTYPE_MARK_Q:
				return getSubtypeMarkQ();
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__DEFINITION_INTERFACE_LIST_QL:
				return getDefinitionInterfaceListQl();
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_PRIVATE_Q:
				return getHasPrivateQ();
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_ABSTRACT_Q:
				setHasAbstractQ((HasAbstractQType7)newValue);
				return;
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_LIMITED_Q:
				setHasLimitedQ((HasLimitedQType9)newValue);
				return;
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_SYNCHRONIZED_Q:
				setHasSynchronizedQ((HasSynchronizedQType)newValue);
				return;
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__SUBTYPE_MARK_Q:
				setSubtypeMarkQ((ExpressionClass)newValue);
				return;
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__DEFINITION_INTERFACE_LIST_QL:
				setDefinitionInterfaceListQl((ExpressionList)newValue);
				return;
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_PRIVATE_Q:
				setHasPrivateQ((HasPrivateQType)newValue);
				return;
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_ABSTRACT_Q:
				setHasAbstractQ((HasAbstractQType7)null);
				return;
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_LIMITED_Q:
				setHasLimitedQ((HasLimitedQType9)null);
				return;
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_SYNCHRONIZED_Q:
				setHasSynchronizedQ((HasSynchronizedQType)null);
				return;
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__SUBTYPE_MARK_Q:
				setSubtypeMarkQ((ExpressionClass)null);
				return;
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__DEFINITION_INTERFACE_LIST_QL:
				setDefinitionInterfaceListQl((ExpressionList)null);
				return;
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_PRIVATE_Q:
				setHasPrivateQ((HasPrivateQType)null);
				return;
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__SLOC:
				return sloc != null;
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_ABSTRACT_Q:
				return hasAbstractQ != null;
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_LIMITED_Q:
				return hasLimitedQ != null;
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_SYNCHRONIZED_Q:
				return hasSynchronizedQ != null;
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__SUBTYPE_MARK_Q:
				return subtypeMarkQ != null;
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__DEFINITION_INTERFACE_LIST_QL:
				return definitionInterfaceListQl != null;
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__HAS_PRIVATE_Q:
				return hasPrivateQ != null;
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //FormalDerivedTypeDefinitionImpl
