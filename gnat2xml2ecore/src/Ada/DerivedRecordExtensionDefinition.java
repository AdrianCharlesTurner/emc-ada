/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Derived Record Extension Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.DerivedRecordExtensionDefinition#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.DerivedRecordExtensionDefinition#getHasAbstractQ <em>Has Abstract Q</em>}</li>
 *   <li>{@link Ada.DerivedRecordExtensionDefinition#getHasLimitedQ <em>Has Limited Q</em>}</li>
 *   <li>{@link Ada.DerivedRecordExtensionDefinition#getParentSubtypeIndicationQ <em>Parent Subtype Indication Q</em>}</li>
 *   <li>{@link Ada.DerivedRecordExtensionDefinition#getDefinitionInterfaceListQl <em>Definition Interface List Ql</em>}</li>
 *   <li>{@link Ada.DerivedRecordExtensionDefinition#getRecordDefinitionQ <em>Record Definition Q</em>}</li>
 *   <li>{@link Ada.DerivedRecordExtensionDefinition#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getDerivedRecordExtensionDefinition()
 * @model extendedMetaData="name='Derived_Record_Extension_Definition' kind='elementOnly'"
 * @generated
 */
public interface DerivedRecordExtensionDefinition extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getDerivedRecordExtensionDefinition_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.DerivedRecordExtensionDefinition#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Has Abstract Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Abstract Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Abstract Q</em>' containment reference.
	 * @see #setHasAbstractQ(HasAbstractQType12)
	 * @see Ada.AdaPackage#getDerivedRecordExtensionDefinition_HasAbstractQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='has_abstract_q' namespace='##targetNamespace'"
	 * @generated
	 */
	HasAbstractQType12 getHasAbstractQ();

	/**
	 * Sets the value of the '{@link Ada.DerivedRecordExtensionDefinition#getHasAbstractQ <em>Has Abstract Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Abstract Q</em>' containment reference.
	 * @see #getHasAbstractQ()
	 * @generated
	 */
	void setHasAbstractQ(HasAbstractQType12 value);

	/**
	 * Returns the value of the '<em><b>Has Limited Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Limited Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Limited Q</em>' containment reference.
	 * @see #setHasLimitedQ(HasLimitedQType6)
	 * @see Ada.AdaPackage#getDerivedRecordExtensionDefinition_HasLimitedQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='has_limited_q' namespace='##targetNamespace'"
	 * @generated
	 */
	HasLimitedQType6 getHasLimitedQ();

	/**
	 * Sets the value of the '{@link Ada.DerivedRecordExtensionDefinition#getHasLimitedQ <em>Has Limited Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Limited Q</em>' containment reference.
	 * @see #getHasLimitedQ()
	 * @generated
	 */
	void setHasLimitedQ(HasLimitedQType6 value);

	/**
	 * Returns the value of the '<em><b>Parent Subtype Indication Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Subtype Indication Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Subtype Indication Q</em>' containment reference.
	 * @see #setParentSubtypeIndicationQ(ElementClass)
	 * @see Ada.AdaPackage#getDerivedRecordExtensionDefinition_ParentSubtypeIndicationQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='parent_subtype_indication_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementClass getParentSubtypeIndicationQ();

	/**
	 * Sets the value of the '{@link Ada.DerivedRecordExtensionDefinition#getParentSubtypeIndicationQ <em>Parent Subtype Indication Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Subtype Indication Q</em>' containment reference.
	 * @see #getParentSubtypeIndicationQ()
	 * @generated
	 */
	void setParentSubtypeIndicationQ(ElementClass value);

	/**
	 * Returns the value of the '<em><b>Definition Interface List Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Definition Interface List Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definition Interface List Ql</em>' containment reference.
	 * @see #setDefinitionInterfaceListQl(ExpressionList)
	 * @see Ada.AdaPackage#getDerivedRecordExtensionDefinition_DefinitionInterfaceListQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='definition_interface_list_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionList getDefinitionInterfaceListQl();

	/**
	 * Sets the value of the '{@link Ada.DerivedRecordExtensionDefinition#getDefinitionInterfaceListQl <em>Definition Interface List Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Definition Interface List Ql</em>' containment reference.
	 * @see #getDefinitionInterfaceListQl()
	 * @generated
	 */
	void setDefinitionInterfaceListQl(ExpressionList value);

	/**
	 * Returns the value of the '<em><b>Record Definition Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Definition Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Definition Q</em>' containment reference.
	 * @see #setRecordDefinitionQ(DefinitionClass)
	 * @see Ada.AdaPackage#getDerivedRecordExtensionDefinition_RecordDefinitionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='record_definition_q' namespace='##targetNamespace'"
	 * @generated
	 */
	DefinitionClass getRecordDefinitionQ();

	/**
	 * Sets the value of the '{@link Ada.DerivedRecordExtensionDefinition#getRecordDefinitionQ <em>Record Definition Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Record Definition Q</em>' containment reference.
	 * @see #getRecordDefinitionQ()
	 * @generated
	 */
	void setRecordDefinitionQ(DefinitionClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getDerivedRecordExtensionDefinition_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.DerivedRecordExtensionDefinition#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // DerivedRecordExtensionDefinition
