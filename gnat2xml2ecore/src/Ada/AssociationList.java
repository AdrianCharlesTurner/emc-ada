/**
 */
package Ada;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Association List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.AssociationList#getGroup <em>Group</em>}</li>
 *   <li>{@link Ada.AssociationList#getNotAnElement <em>Not An Element</em>}</li>
 *   <li>{@link Ada.AssociationList#getPragmaArgumentAssociation <em>Pragma Argument Association</em>}</li>
 *   <li>{@link Ada.AssociationList#getDiscriminantAssociation <em>Discriminant Association</em>}</li>
 *   <li>{@link Ada.AssociationList#getRecordComponentAssociation <em>Record Component Association</em>}</li>
 *   <li>{@link Ada.AssociationList#getArrayComponentAssociation <em>Array Component Association</em>}</li>
 *   <li>{@link Ada.AssociationList#getParameterAssociation <em>Parameter Association</em>}</li>
 *   <li>{@link Ada.AssociationList#getGenericAssociation <em>Generic Association</em>}</li>
 *   <li>{@link Ada.AssociationList#getComment <em>Comment</em>}</li>
 *   <li>{@link Ada.AssociationList#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getAsynchronousPragma <em>Asynchronous Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getAtomicPragma <em>Atomic Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getAttachHandlerPragma <em>Attach Handler Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getControlledPragma <em>Controlled Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getConventionPragma <em>Convention Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getDiscardNamesPragma <em>Discard Names Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getElaboratePragma <em>Elaborate Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getElaborateAllPragma <em>Elaborate All Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getExportPragma <em>Export Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getImportPragma <em>Import Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getInlinePragma <em>Inline Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getInspectionPointPragma <em>Inspection Point Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getLinkerOptionsPragma <em>Linker Options Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getListPragma <em>List Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getLockingPolicyPragma <em>Locking Policy Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getOptimizePragma <em>Optimize Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getPackPragma <em>Pack Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getPagePragma <em>Page Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getPreelaboratePragma <em>Preelaborate Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getPriorityPragma <em>Priority Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getPurePragma <em>Pure Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getRemoteTypesPragma <em>Remote Types Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getRestrictionsPragma <em>Restrictions Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getReviewablePragma <em>Reviewable Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getSharedPassivePragma <em>Shared Passive Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getStorageSizePragma <em>Storage Size Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getSuppressPragma <em>Suppress Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getVolatilePragma <em>Volatile Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getAssertPragma <em>Assert Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getNoReturnPragma <em>No Return Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getProfilePragma <em>Profile Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getUnsuppressPragma <em>Unsuppress Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getCpuPragma <em>Cpu Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getIndependentPragma <em>Independent Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getIndependentComponentsPragma <em>Independent Components Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}</li>
 *   <li>{@link Ada.AssociationList#getUnknownPragma <em>Unknown Pragma</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getAssociationList()
 * @model extendedMetaData="name='Association_List' kind='elementOnly'"
 * @generated
 */
public interface AssociationList extends EObject {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' attribute list.
	 * @see Ada.AdaPackage#getAssociationList_Group()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:0'"
	 * @generated
	 */
	FeatureMap getGroup();

	/**
	 * Returns the value of the '<em><b>Not An Element</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NotAnElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not An Element</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not An Element</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_NotAnElement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='not_an_element' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NotAnElement> getNotAnElement();

	/**
	 * Returns the value of the '<em><b>Pragma Argument Association</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PragmaArgumentAssociation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pragma Argument Association</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pragma Argument Association</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_PragmaArgumentAssociation()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='pragma_argument_association' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PragmaArgumentAssociation> getPragmaArgumentAssociation();

	/**
	 * Returns the value of the '<em><b>Discriminant Association</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscriminantAssociation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discriminant Association</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discriminant Association</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_DiscriminantAssociation()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discriminant_association' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscriminantAssociation> getDiscriminantAssociation();

	/**
	 * Returns the value of the '<em><b>Record Component Association</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RecordComponentAssociation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Component Association</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Component Association</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_RecordComponentAssociation()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='record_component_association' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RecordComponentAssociation> getRecordComponentAssociation();

	/**
	 * Returns the value of the '<em><b>Array Component Association</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ArrayComponentAssociation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Array Component Association</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Array Component Association</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_ArrayComponentAssociation()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='array_component_association' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ArrayComponentAssociation> getArrayComponentAssociation();

	/**
	 * Returns the value of the '<em><b>Parameter Association</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ParameterAssociation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Association</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Association</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_ParameterAssociation()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='parameter_association' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ParameterAssociation> getParameterAssociation();

	/**
	 * Returns the value of the '<em><b>Generic Association</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.GenericAssociation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Association</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Association</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_GenericAssociation()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='generic_association' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<GenericAssociation> getGenericAssociation();

	/**
	 * Returns the value of the '<em><b>Comment</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.Comment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comment</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comment</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_Comment()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='comment' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<Comment> getComment();

	/**
	 * Returns the value of the '<em><b>All Calls Remote Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AllCallsRemotePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Calls Remote Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Calls Remote Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_AllCallsRemotePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='all_calls_remote_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AllCallsRemotePragma> getAllCallsRemotePragma();

	/**
	 * Returns the value of the '<em><b>Asynchronous Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AsynchronousPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asynchronous Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asynchronous Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_AsynchronousPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='asynchronous_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AsynchronousPragma> getAsynchronousPragma();

	/**
	 * Returns the value of the '<em><b>Atomic Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AtomicPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_AtomicPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='atomic_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AtomicPragma> getAtomicPragma();

	/**
	 * Returns the value of the '<em><b>Atomic Components Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AtomicComponentsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Components Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Components Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_AtomicComponentsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='atomic_components_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AtomicComponentsPragma> getAtomicComponentsPragma();

	/**
	 * Returns the value of the '<em><b>Attach Handler Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AttachHandlerPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attach Handler Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attach Handler Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_AttachHandlerPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='attach_handler_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AttachHandlerPragma> getAttachHandlerPragma();

	/**
	 * Returns the value of the '<em><b>Controlled Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ControlledPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controlled Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controlled Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_ControlledPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='controlled_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ControlledPragma> getControlledPragma();

	/**
	 * Returns the value of the '<em><b>Convention Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ConventionPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Convention Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Convention Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_ConventionPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='convention_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ConventionPragma> getConventionPragma();

	/**
	 * Returns the value of the '<em><b>Discard Names Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscardNamesPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discard Names Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discard Names Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_DiscardNamesPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discard_names_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscardNamesPragma> getDiscardNamesPragma();

	/**
	 * Returns the value of the '<em><b>Elaborate Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElaboratePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_ElaboratePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='elaborate_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElaboratePragma> getElaboratePragma();

	/**
	 * Returns the value of the '<em><b>Elaborate All Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElaborateAllPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate All Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate All Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_ElaborateAllPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='elaborate_all_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElaborateAllPragma> getElaborateAllPragma();

	/**
	 * Returns the value of the '<em><b>Elaborate Body Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElaborateBodyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Body Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Body Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_ElaborateBodyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='elaborate_body_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElaborateBodyPragma> getElaborateBodyPragma();

	/**
	 * Returns the value of the '<em><b>Export Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExportPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Export Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Export Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_ExportPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='export_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExportPragma> getExportPragma();

	/**
	 * Returns the value of the '<em><b>Import Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ImportPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Import Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Import Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_ImportPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='import_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ImportPragma> getImportPragma();

	/**
	 * Returns the value of the '<em><b>Inline Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InlinePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inline Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inline Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_InlinePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='inline_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InlinePragma> getInlinePragma();

	/**
	 * Returns the value of the '<em><b>Inspection Point Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InspectionPointPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inspection Point Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inspection Point Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_InspectionPointPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='inspection_point_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InspectionPointPragma> getInspectionPointPragma();

	/**
	 * Returns the value of the '<em><b>Interrupt Handler Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InterruptHandlerPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Handler Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Handler Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_InterruptHandlerPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='interrupt_handler_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InterruptHandlerPragma> getInterruptHandlerPragma();

	/**
	 * Returns the value of the '<em><b>Interrupt Priority Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InterruptPriorityPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Priority Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Priority Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_InterruptPriorityPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='interrupt_priority_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InterruptPriorityPragma> getInterruptPriorityPragma();

	/**
	 * Returns the value of the '<em><b>Linker Options Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LinkerOptionsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Linker Options Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Linker Options Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_LinkerOptionsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='linker_options_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LinkerOptionsPragma> getLinkerOptionsPragma();

	/**
	 * Returns the value of the '<em><b>List Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ListPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_ListPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='list_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ListPragma> getListPragma();

	/**
	 * Returns the value of the '<em><b>Locking Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LockingPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Locking Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Locking Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_LockingPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='locking_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LockingPolicyPragma> getLockingPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Normalize Scalars Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NormalizeScalarsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Normalize Scalars Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Normalize Scalars Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_NormalizeScalarsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='normalize_scalars_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NormalizeScalarsPragma> getNormalizeScalarsPragma();

	/**
	 * Returns the value of the '<em><b>Optimize Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OptimizePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Optimize Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Optimize Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_OptimizePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='optimize_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OptimizePragma> getOptimizePragma();

	/**
	 * Returns the value of the '<em><b>Pack Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PackPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pack Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pack Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_PackPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='pack_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PackPragma> getPackPragma();

	/**
	 * Returns the value of the '<em><b>Page Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PagePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Page Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Page Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_PagePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='page_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PagePragma> getPagePragma();

	/**
	 * Returns the value of the '<em><b>Preelaborate Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PreelaboratePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborate Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborate Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_PreelaboratePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='preelaborate_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PreelaboratePragma> getPreelaboratePragma();

	/**
	 * Returns the value of the '<em><b>Priority Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PriorityPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_PriorityPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='priority_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PriorityPragma> getPriorityPragma();

	/**
	 * Returns the value of the '<em><b>Pure Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PurePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pure Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pure Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_PurePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='pure_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PurePragma> getPurePragma();

	/**
	 * Returns the value of the '<em><b>Queuing Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.QueuingPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Queuing Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Queuing Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_QueuingPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='queuing_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<QueuingPolicyPragma> getQueuingPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Remote Call Interface Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RemoteCallInterfacePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Call Interface Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Call Interface Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_RemoteCallInterfacePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='remote_call_interface_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RemoteCallInterfacePragma> getRemoteCallInterfacePragma();

	/**
	 * Returns the value of the '<em><b>Remote Types Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RemoteTypesPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Types Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Types Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_RemoteTypesPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='remote_types_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RemoteTypesPragma> getRemoteTypesPragma();

	/**
	 * Returns the value of the '<em><b>Restrictions Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RestrictionsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Restrictions Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Restrictions Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_RestrictionsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='restrictions_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RestrictionsPragma> getRestrictionsPragma();

	/**
	 * Returns the value of the '<em><b>Reviewable Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ReviewablePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reviewable Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reviewable Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_ReviewablePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='reviewable_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ReviewablePragma> getReviewablePragma();

	/**
	 * Returns the value of the '<em><b>Shared Passive Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SharedPassivePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shared Passive Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shared Passive Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_SharedPassivePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='shared_passive_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SharedPassivePragma> getSharedPassivePragma();

	/**
	 * Returns the value of the '<em><b>Storage Size Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.StorageSizePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Size Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Size Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_StorageSizePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='storage_size_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<StorageSizePragma> getStorageSizePragma();

	/**
	 * Returns the value of the '<em><b>Suppress Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SuppressPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Suppress Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Suppress Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_SuppressPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='suppress_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SuppressPragma> getSuppressPragma();

	/**
	 * Returns the value of the '<em><b>Task Dispatching Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TaskDispatchingPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Dispatching Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Dispatching Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_TaskDispatchingPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='task_dispatching_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TaskDispatchingPolicyPragma> getTaskDispatchingPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Volatile Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.VolatilePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_VolatilePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='volatile_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<VolatilePragma> getVolatilePragma();

	/**
	 * Returns the value of the '<em><b>Volatile Components Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.VolatileComponentsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Components Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Components Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_VolatileComponentsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='volatile_components_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<VolatileComponentsPragma> getVolatileComponentsPragma();

	/**
	 * Returns the value of the '<em><b>Assert Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AssertPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assert Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assert Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_AssertPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='assert_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AssertPragma> getAssertPragma();

	/**
	 * Returns the value of the '<em><b>Assertion Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AssertionPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assertion Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assertion Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_AssertionPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='assertion_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AssertionPolicyPragma> getAssertionPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Detect Blocking Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DetectBlockingPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Detect Blocking Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Detect Blocking Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_DetectBlockingPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='detect_blocking_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DetectBlockingPragma> getDetectBlockingPragma();

	/**
	 * Returns the value of the '<em><b>No Return Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NoReturnPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>No Return Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>No Return Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_NoReturnPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='no_return_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NoReturnPragma> getNoReturnPragma();

	/**
	 * Returns the value of the '<em><b>Partition Elaboration Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PartitionElaborationPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partition Elaboration Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partition Elaboration Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_PartitionElaborationPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='partition_elaboration_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PartitionElaborationPolicyPragma> getPartitionElaborationPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Preelaborable Initialization Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PreelaborableInitializationPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborable Initialization Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborable Initialization Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_PreelaborableInitializationPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='preelaborable_initialization_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PreelaborableInitializationPragma> getPreelaborableInitializationPragma();

	/**
	 * Returns the value of the '<em><b>Priority Specific Dispatching Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PrioritySpecificDispatchingPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Specific Dispatching Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Specific Dispatching Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_PrioritySpecificDispatchingPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='priority_specific_dispatching_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PrioritySpecificDispatchingPragma> getPrioritySpecificDispatchingPragma();

	/**
	 * Returns the value of the '<em><b>Profile Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProfilePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Profile Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Profile Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_ProfilePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='profile_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProfilePragma> getProfilePragma();

	/**
	 * Returns the value of the '<em><b>Relative Deadline Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RelativeDeadlinePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relative Deadline Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relative Deadline Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_RelativeDeadlinePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='relative_deadline_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RelativeDeadlinePragma> getRelativeDeadlinePragma();

	/**
	 * Returns the value of the '<em><b>Unchecked Union Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UncheckedUnionPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unchecked Union Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unchecked Union Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_UncheckedUnionPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unchecked_union_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UncheckedUnionPragma> getUncheckedUnionPragma();

	/**
	 * Returns the value of the '<em><b>Unsuppress Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnsuppressPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unsuppress Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unsuppress Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_UnsuppressPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unsuppress_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnsuppressPragma> getUnsuppressPragma();

	/**
	 * Returns the value of the '<em><b>Default Storage Pool Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefaultStoragePoolPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Storage Pool Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Storage Pool Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_DefaultStoragePoolPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='default_storage_pool_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefaultStoragePoolPragma> getDefaultStoragePoolPragma();

	/**
	 * Returns the value of the '<em><b>Dispatching Domain Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DispatchingDomainPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dispatching Domain Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dispatching Domain Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_DispatchingDomainPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='dispatching_domain_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DispatchingDomainPragma> getDispatchingDomainPragma();

	/**
	 * Returns the value of the '<em><b>Cpu Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CpuPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cpu Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cpu Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_CpuPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='cpu_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CpuPragma> getCpuPragma();

	/**
	 * Returns the value of the '<em><b>Independent Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IndependentPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_IndependentPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='independent_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IndependentPragma> getIndependentPragma();

	/**
	 * Returns the value of the '<em><b>Independent Components Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IndependentComponentsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Components Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Components Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_IndependentComponentsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='independent_components_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IndependentComponentsPragma> getIndependentComponentsPragma();

	/**
	 * Returns the value of the '<em><b>Implementation Defined Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ImplementationDefinedPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implementation Defined Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implementation Defined Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_ImplementationDefinedPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='implementation_defined_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ImplementationDefinedPragma> getImplementationDefinedPragma();

	/**
	 * Returns the value of the '<em><b>Unknown Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnknownPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unknown Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unknown Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getAssociationList_UnknownPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unknown_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnknownPragma> getUnknownPragma();

} // AssociationList
