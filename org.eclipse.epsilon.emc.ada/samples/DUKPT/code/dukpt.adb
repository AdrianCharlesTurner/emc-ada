--------------------------------------------------------------------
-- Copyright Altran UK
-- HITEA2
--
-- File:
--    $Id: dukpt.adb 2379 2017-02-28 10:02:48Z aturner@EUROPE $
--
-- Description:
--    There are a number of deviations from the DUKPT 'spec' ANS X9.24-1:2009
--    These are in part due to errors in the spec and part due to
--    planned changes liek the move to AES128 instead of 3DES.
--
--    There is also an issue with the described 'short-cut' to recovering
--    the keys on the Server side. It is not explained at all in the
--    'spec' and so not implemented.
--------------------------------------------------------------------
--------------------------------------------------------------------
-- Implementation Notes:
--
--------------------------------------------------------------------
with Crypto.Symmetric.Blockcipher_AES128; use Crypto.Symmetric.Blockcipher_AES128;
with DUKPT.Utilities;                     use DUKPT.Utilities;
with Interfaces;                          use Interfaces;

package body DUKPT is
   --------------------------------------------------------------------
   -- Implementation Notes:
   --    See ANS X9.24-1:2009 Section A.6
   --    This subprogram initialises no state so that it can be re-used
   --    from the server side.
   --------------------------------------------------------------------
   procedure Derive_IPEK(BDK  : in     Crypto.Types.B_Block128;
                         IKSN : in     Crypto.Types.B_Block128;
                         IPEK :    out Crypto.Types.B_Block128)
   is
      EC_Init    : Interfaces.Unsigned_32;
      KSN        : Crypto.Types.B_Block128 := IKSN;
   begin
      -- Clear EC (21 right-most bits of KSNR)
      EC_Init := 2#0#;
      DUKPT.Utilities.Update_KSNR(EC_Init, KSN);
      -- Encrypt with AES128
      Crypto.Symmetric.Blockcipher_AES128.Prepare_Key(BDK);
      Crypto.Symmetric.Blockcipher_AES128.Encrypt(KSN, IPEK);
   end Derive_IPEK;

   --------------------------------------------------------------------
   -- Implementation Notes:
   --    See ANS X9.24-1:2009 Section A.6
   --    This directly uses the Ada Crypto Lib AES 128 encryption.
   --    It owul be better to have an interface package to allow modularity.
   --------------------------------------------------------------------
   procedure Non_Reversible_Key_Generation_Process(Key     : in     Crypto.Types.B_Block128;
                                                   Subject : in out Crypto.Types.B_Block128)
   is
      CR_Temp : Crypto.Types.B_Block128;
   begin
      Subject := Subject xor Key;
      Crypto.Symmetric.Blockcipher_AES128.Prepare_Key(Key);
      Crypto.Symmetric.Blockcipher_AES128.Encrypt(Subject, CR_Temp);
      Subject := CR_Temp xor Key;
   end Non_Reversible_Key_Generation_Process;

   --------------------------------------------------------------------
   -- Implementation Notes:
   --    See ANS X9.24-1:2009 Section A.6
   --    Also emcompasses New_Key_1, New_Key_4 and New_Key_2 by extending
   --    to DUKPT.Utilities.Increment_EC.
   --
   --    This procedure fills the FKR using initially the IPEK which is
   --    immediately overwritten with another key derived from the IPEK.
   --    All subsequent keys stored in the FKR are derived from this
   --    key derived from the IPEK. This chain continues in operation.
   --
   --    This procedure initalises KR
   --------------------------------------------------------------------
   procedure New_Key_3(CKP_Temp  : in     Natural_0_20;
                       EC_Temp   : in out Interfaces.Unsigned_32;
                       KSNR_Temp : in out Crypto.Types.B_Block128;
                       SR_Temp   : in     Interfaces.Unsigned_32;
                       FKR_Temp  : in out Future_Key_Register_Type)
   is
      Padded_SR     : Crypto.Types.B_Block128;
      SR_Block_32   : Crypto.Types.B_Block32;
      SR_Position   : Natural_0_20;
      CR_1          : Crypto.Types.B_Block128; -- Crypto Register 1
      KR_Temp       : Crypto.Types.B_Block128;
      SR_Temp_Local : Interfaces.Unsigned_32 := SR_Temp;
   begin
      while SR_Temp_Local /= 0 loop
         -- SR right-justified and padded up to 128 bits on the left with 0's OR'd
         -- with the the KSNR, copied to Crypto_Register_1
         -- Need to chunk SR into 3 bytes here right-justfied.
         SR_Block_32  := DUKPT.Utilities.Chunk_32(SR_Temp_Local);
         -- Index 13, 14 and 15 are the only bytes in the 128 bit block that will
         -- have any set bits in from the right-justified 21 bit SR.
         Padded_SR := (13 => SR_Block_32(1), 14 => SR_Block_32(2),
                       15 => SR_Block_32(3), others => 0);
         -- Update the KSNR before OR'ing
         DUKPT.Utilities.Update_KSNR(EC_Temp, KSNR_Temp);
         for A in 0 .. 15 loop
            CR_1(A) := Padded_SR(A) or KSNR_Temp(A);
         end loop;
         -- Copy FKR(CKP) into Key Register (KR)
         for B in 0 .. 15 loop
            KR_Temp(B) := FKR_Temp(Integer(CKP_Temp))(B);
         end loop;

         Non_Reversible_Key_Generation_Process(KR_Temp, CR_1);

         -- Store Crypto_Register_1 into the FKR inidicated by the
         -- position of the 1 in the SR
         SR_Position := Shift_Register_Position(SR_Temp_Local);
         for C in 0 .. 15 loop
            FKR_Temp(Integer(SR_Position))(C) := CR_1(C);
         end loop;
         -- Generate and store the LRC_Byte on this FKR
         Generate_LRC_Byte(FKR_Temp(Integer(SR_Position)));
         -- Shift the SR 1 to the right - loop will terminate when the 1 bit is
         -- shifted off the end and SR = 0
         SR_Temp_Local := Interfaces.Shift_Right(SR_Temp_Local, 1);
      end loop;
      -- Erase FKR(CKP) if not the first time through...
      -- This an error in ANSI spec that deletes the first key without using
      -- it causing an error in the Encryption Counter.
      if EC_Temp /= 0 then
         for B in 0 .. 15 loop
            FKR_Temp(Integer(CKP_Temp))(B) := 0;
            -- Invalidate the the LRC_byte for FKR(CKP) - already is if the key is erased?!
            DUKPT.Utilities.Invalidate_LRC_Byte(FKR_Temp(Integer(CKP_Temp)));
         end loop;
      end if;
      -- Safely Increment the EC until end of service
      DUKPT.Utilities.Increment_EC(EC_Temp, 1);

   end New_Key_3;

   --------------------------------------------------------------------
   -- Implementation Notes:
   --    Instead of using 59 bits for the Initial KSNR we use 107 bits
   --    so that when conbined with the 21 bit Encryption Counter there
   --    is a standard size 128 bit block. This also aids the interaction
   --    with the crypto registers in New_Key_3().
   --
   --    Why not separate the EC and the KSNR? Having to clear the
   --    right-most 21 bits of the array of 16 bytes seems dumb.
   --
   --    If we use Interfaces.Unsigned_32 for the EC and SR then we can
   --    add them together in an Interfaces.Unsigned_64 to check for 32-bit
   --    overflow without actual overflow. ANS X9.24-1:2009 Sec A.2 "New Key 2"
   --
   --------------------------------------------------------------------
   procedure Load_Initial_Keys_Stateless(IPEK      : in     Crypto.Types.B_Block128;
                                         IKSN      : in     Crypto.Types.B_Block128;
                                         EC_Temp   :    out Interfaces.Unsigned_32;
                                         KSNR_Temp :    out Crypto.Types.B_Block128;
                                         FKR_Temp  :    out Future_Key_Register_Type)
   is
      Padded_IPEK : B_Block136;
      FKR_Init    : B_Block136 := (others => 0);
      CKP_Temp : Natural_0_20;
      SR_Temp : Interfaces.Unsigned_32;
   begin
      -- Initialise EC and KSNR
      for B in 0 .. 15 loop
         KSNR_Temp(B) := IKSN(B);
      end loop;
      EC_Temp := 2#0#;
      DUKPT.Utilities.Update_KSNR(EC_Temp, KSNR_Temp);
      -- Pad IPEK with the LRC Byte
      Padded_IPEK := (0 => IPEK(0), 1 => IPEK(1), 2 => IPEK(2),
                      3 => IPEK(3), 4 => IPEK(4), 5 => IPEK(5),
                      6 => IPEK(6), 7 => IPEK(7), 8 => IPEK(8),
                      9 => IPEK(9), 10 => IPEK(10), 11 => IPEK(11),
                      12 => IPEK(12), 13 => IPEK(13), 14 => IPEK(14),
                      15 => IPEK(15), 16 => 0);
      -- Generate and store LRC in Padded_IPEK
      DUKPT.Utilities.Generate_LRC_Byte(Padded_IPEK);
      -- 'IPEK is loaded onto device' - copied to Future Key Register
      -- (FKR) #21 (one-based)
      for A in 0 .. 20 loop
         if A < 20 then
            FKR_Temp(A) := FKR_Init;
         else
            FKR_Temp(A) := Padded_IPEK;
         end if;
      end loop;
      -- Write address of FKR#21 into Current Key Pointer (CKP)
      CKP_Temp := 20;
      -- Set bit #1 (left-most) os Shift Register (SR) to 1, others => 0
      SR_Temp := 2#100000000000000000000#;
      -- Call New_Key_3
      New_Key_3(CKP_Temp, EC_Temp, KSNR_Temp, SR_Temp, FKR_Temp);
   end Load_Initial_Keys_Stateless;

   --------------------------------------------------------------------
   -- Implementation Notes:
   --    See ANS X9.24-1:2009 Section A.6
   --    This accessor function is used for the terminal side that requires
   --    state.
   --------------------------------------------------------------------
   procedure Load_Initial_Keys(IPEK : in     Crypto.Types.B_Block128;
                               IKSN : in     Crypto.Types.B_Block128)
   is
   begin
      Load_Initial_Keys_Stateless(IPEK, IKSN, EC, KSNR, FKR);
   end Load_Initial_Keys;

   --------------------------------------------------------------------
   -- Implementation Notes:
   --    See ANS X9.24-1:2009 Section A.6
   --    The 'spec' for this suggests skipping keys corresponing to EC
   --    values with population count 10 or more. This is supposed to
   --    be so that the Server can make shortcuts to get the key from
   --    the EC but this shortcut method is not given so is not
   --    implemented - and thus nor is the population count check.
   --------------------------------------------------------------------
   procedure New_Key(CKP_Temp  : in     Natural_0_20;
                     EC_Temp   : in out Interfaces.Unsigned_32;
                     KSNR_Temp : in out Crypto.Types.B_Block128;
                     SR_Temp   : in     Interfaces.Unsigned_32;
                     FKR_Temp  : in out Future_Key_Register_Type)
   is
      SR_Temp_2 : Interfaces.Unsigned_32 := SR_Temp;
   begin
      -- New_Key_1 requires a right shift in the SR before looping (done
      -- in New_Key_3 for convenience)
      SR_Temp_2 := Interfaces.Shift_Right(SR_Temp_2, 1);
      New_Key_3(CKP_Temp, EC_Temp, KSNR_Temp, SR_Temp_2, FKR_Temp);

      -- See Implementation Notes
--        -- Count number of set bits in the Encryption Counter
--        Set_Bit_Count := DUKPT.Utilities.Pop_Count(EC_Temp);
--        if Set_Bit_Count < 10 then
--           -- New_Key_1 requires a right shift in the SR before looping (done
--           -- in New_Key_3 for convenience)
--           SR_Temp_2 := Interfaces.Shift_Right(SR_Temp_2, 1);
--           New_Key_3(CKP_Temp, EC_Temp, KSNR_Temp, SR_Temp_2, FKR_Temp);
--        else
--           -- Erase FKR(CKP)
--           for B in 0 .. 15 loop
--              FKR_Temp(Integer(CKP_Temp))(B) := 0;
--           end loop;
--           -- Invalidate the the LRC_byte for FKR(CKP) - already is if the key is erased?!
--           DUKPT.Utilities.Invalidate_LRC_Byte(FKR_Temp(Integer(CKP_Temp)));
--           -- Safely Increment the EC until end of service
--           DUKPT.Utilities.Increment_EC(EC_Temp, SR_Temp_2);
--        end if;
   end New_Key;

   --------------------------------------------------------------------
   -- Implementation Notes:
   --    The encrypt and MAC key variants are derived as in
   --    ANS X9.24-1:2009 Section A.6 "Request PIN Entry 2".
   --------------------------------------------------------------------
   procedure Derive_Keys_From_Current_Key(CKP_Temp    : in     Natural_0_20;
                                          EC_Temp     : in out Interfaces.Unsigned_32;
                                          KSNR_Temp   : in out Crypto.Types.B_Block128;
                                          SR_Temp     : in     Interfaces.Unsigned_32;
                                          FKR_Temp    : in out Future_Key_Register_Type;
                                          Encrypt_Key :    out Crypto.Types.B_Block128;
                                          MAC_Key     :    out Crypto.Types.B_Block128)
   is
      KR_Temp : Crypto.Types.B_Block128;
      EK_tmp : Crypto.Types.B_Block128;
   begin
      -- Copy FKR(CKP) into Key Register
      for A in 0 .. 15 loop
         KR_Temp(A) := FKR_Temp(Integer(CKP_Temp))(A);
      end loop;
      -- MAC Key Register = Key Register xor 0000 0000 0000 FF00 0000 0000 0000 FF00
      MAC_Key := KR_Temp xor MAC_XOR;
      EK_tmp := KR_Temp xor Encrypt_XOR;
      Crypto.Symmetric.Blockcipher_AES128.Prepare_Key(EK_tmp);
      Crypto.Symmetric.Blockcipher_AES128.Encrypt(EK_tmp, Encrypt_Key);

      -- I think that DUKPT should only give the user the key.
      -- For example if the user wanted to send data with Cipher Block Chaining
      -- with AES128 encryption then they would have to provide their own IV.
      -- Regenerate Keys
      New_Key(CKP_Temp, EC_Temp, KSNR_Temp, SR_Temp, FKR_Temp);
   end Derive_Keys_From_Current_Key;

   --------------------------------------------------------------------
   -- Implementation Notes:
   --    See ANS X9.24-1:2009 Section A.6
   --    When there are no set bit sin the Encryption Counter then the
   --    device is dead - should this check be here for defense against
   --    random bit decay?
   --------------------------------------------------------------------
   procedure Set_Bit(EC_Temp       : in     Interfaces.Unsigned_32;
                     SR_Temp       :    out Interfaces.Unsigned_32;
                     Set_Bit_Index :    out Natural_0_20)
   is
      EC_Shifter : Interfaces.Unsigned_32 := EC_Temp;
   begin
      Set_Bit_Index := 0;
      -- Doesn't need to gaurd against EC = zero as this is done whenever
      -- EC is incremented.
      for Ind in 0 .. 20 loop
         if EC_Shifter rem 2 /= 0 then
            Set_Bit_Index := Natural_0_20(Ind);
            exit;
         end if ;
         EC_Shifter := Interfaces.Shift_Right(EC_Shifter, 1);
      end loop;
      SR_Temp := Interfaces.Shift_Left(2#1#, Integer(Set_Bit_Index));
      -- Make sure the Set_Bit_Index is the right way round for out
      Set_Bit_Index := 20 - Set_Bit_Index;
   end Set_Bit;

   --------------------------------------------------------------------
   -- Implementation Notes:
   --    This derives the keys for encryption and MAC from the key in
   --    the FKR pointed to by the EC. The EC is embedded in the returned
   --    KSN and then it is incremented internally ready for the next
   --    key derivation.
   --------------------------------------------------------------------
   procedure Get_Sender_Transaction_Keys_Stateless(EC_Temp     : in out Interfaces.Unsigned_32;
                                                   KSNR_Temp   : in out Crypto.Types.B_Block128;
                                                   FKR_Temp    : in out Future_Key_Register_Type;
                                                   Encrypt_Key :    out Crypto.Types.B_Block128;
                                                   MAC_Key     :    out Crypto.Types.B_Block128)
   is
      Set_Bit_Index : Natural_0_20;
      LRC_Valid     : Boolean;
      CKP_Temp      : Natural_0_20;
      SR_Temp       : Interfaces.Unsigned_32;
   begin
      -- Ensure Encrypt_Key and MAC_Key are initialised.
      Encrypt_Key := (others => 0);
      MAC_Key := (others => 0);

      -- Update the KSNR and write to output KSN here before we increment
      -- the EC anymore
      DUKPT.Utilities.Update_KSNR(EC_Temp, KSNR_Temp);

      -- After this number of iterations EC will overflow
      -- 21 bits if no keys have valid LRC checks.
      for A in 0 .. 20 loop
         -- Call "Set Bit"
         Set_Bit(EC_Temp, SR_Temp, Set_Bit_Index);
         -- CKP is FKR index corresponding to SR "1" bit
         CKP_Temp := Set_Bit_Index;
         -- Longitudinal Redundancy Check on FKR(CKP)
         Validate_LRC_Byte(FKR_Temp(Integer(CKP_Temp)), LRC_Valid);
         -- if byte is correct goto "Request PIN Entry 2"
         -- else add SR to EC
         if LRC_Valid = True then
            Derive_Keys_From_Current_Key(CKP_Temp, EC_Temp, KSNR_Temp, SR_Temp,
                                         FKR_Temp, Encrypt_Key, MAC_Key);
            exit;
         else
            -- Increment EC by SR
            DUKPT.Utilities.Increment_EC(EC_Temp, SR_Temp);
         end if;
      end loop;
   end Get_Sender_Transaction_Keys_Stateless;

   --------------------------------------------------------------------
   -- Implementation Notes:
   --    This accessor function is used for the terminal side that requires
   --    state.
   --------------------------------------------------------------------
   procedure Get_Sender_Transaction_Keys(KSN         :    out Crypto.Types.B_Block128;
                                         Encrypt_Key :    out Crypto.Types.B_Block128;
                                         MAC_Key     :    out Crypto.Types.B_Block128)
   is
      EC_Temp   : Interfaces.Unsigned_32   := EC;
      KSNR_Temp : Crypto.Types.B_Block128  := KSNR;
      SR_Temp   : Interfaces.Unsigned_32   := SR;
      FKR_Temp  : Future_Key_Register_Type := FKR;
   begin
      Get_Sender_Transaction_Keys_Stateless(EC_Temp, KSNR_Temp,
                                            FKR_Temp, Encrypt_Key, MAC_Key);
      for A in 0 .. 15 loop
         KSN(A) := KSNR_Temp(A);
         KSNR(A) := KSNR_Temp(A);
      end loop;
      -- Update the globals
      EC := EC_Temp;
      SR := SR_Temp;
      FKR := FKR_Temp;
   end Get_Sender_Transaction_Keys;

   --------------------------------------------------------------------
   -- Implementation Notes:
   --
   --------------------------------------------------------------------
   procedure Retrieve_Transaction_Keys(IPEK        : in     Crypto.Types.B_Block128;
                                       KSN         : in     Crypto.Types.B_Block128;
                                       Encrypt_Key :    out Crypto.Types.B_Block128;
                                       MAC_Key     :    out Crypto.Types.B_Block128)
   is
      EC_Hamming_Weight : Interfaces.Unsigned_32;
      KR_Temp           : Crypto.Types.B_Block128 := IPEK;
      KSN_Historic      : Crypto.Types.B_Block128;
      EC                : Interfaces.Unsigned_32;
      EC_Historic       : Interfaces.Unsigned_32;
      EC_Historic_Block : Crypto.Types.B_Block32;
      SR_Historic       : Interfaces.Unsigned_32;
      SR_Historic_Block : Crypto.Types.B_Block32;
      SR_Padded         : Crypto.Types.B_Block128;
      CR_1              : Crypto.Types.B_Block128 := (others => 0);
      EK_Temp           : Crypto.Types.B_Block128;
   begin
      EC := Interfaces.Shift_Left(Interfaces.Unsigned_32(KSN(13)), 16) +
            Interfaces.Shift_Left(Interfaces.Unsigned_32(KSN(14)), 8 ) +
            Interfaces.Unsigned_32(KSN(15));
      -- WHAT IF EC = 0 - this would break
      EC_Hamming_Weight := Interfaces.Unsigned_32(Pop_Count(EC));
      -- Initialise the KSN with zeroed EC
      KSN_Historic := KSN;

      for Chain_Index in 1 .. EC_Hamming_Weight loop
         -- Get the initial parameters to create the first key in the chain
         -- to the current key in use.
         DUKPT.Utilities.Get_N_Most_Sig_Bits(EC,
                                             Chain_Index,
                                             EC_Hamming_Weight,
                                             EC_Historic,
                                             SR_Historic);
         EC_Historic_Block := Chunk_32(EC_Historic);
         KSN_Historic(13) := EC_Historic_Block(1);
         KSN_Historic(14) := EC_Historic_Block(2);
         KSN_Historic(15) := EC_Historic_Block(3);
         SR_Historic_Block := Chunk_32(SR_Historic);
         SR_Padded := (13 => SR_Historic_Block(1), 14 => SR_Historic_Block(2),
                       15 => SR_Historic_Block(3), others => 0);
         -- Update the historic KSN before OR'ing
         DUKPT.Utilities.Update_KSNR(EC_Historic, KSN_Historic);
         for A in 0 .. 15 loop
            CR_1(A) := SR_Padded(A) or KSN_Historic(A);
         end loop;
         -- Generate the PIN Encryption Key
         Non_Reversible_Key_Generation_Process(KR_Temp, CR_1);
         -- The historic key is used as the key for the next non reversible
         -- key generation.
         KR_Temp := CR_1;
      end loop;

      -- Do the MAC and Encryption key derivation.
      MAC_Key := CR_1 xor MAC_XOR;
      EK_Temp := CR_1 xor ENCRYPT_XOR;
      Crypto.Symmetric.Blockcipher_AES128.Prepare_Key(EK_Temp);
      Crypto.Symmetric.Blockcipher_AES128.Encrypt(EK_Temp, Encrypt_Key);

   end Retrieve_Transaction_Keys;

end DUKPT;
