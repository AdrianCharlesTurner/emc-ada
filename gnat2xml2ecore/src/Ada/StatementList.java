/**
 */
package Ada;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Statement List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.StatementList#getGroup <em>Group</em>}</li>
 *   <li>{@link Ada.StatementList#getNotAnElement <em>Not An Element</em>}</li>
 *   <li>{@link Ada.StatementList#getNullStatement <em>Null Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getAssignmentStatement <em>Assignment Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getIfStatement <em>If Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getCaseStatement <em>Case Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getLoopStatement <em>Loop Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getWhileLoopStatement <em>While Loop Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getForLoopStatement <em>For Loop Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getBlockStatement <em>Block Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getExitStatement <em>Exit Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getGotoStatement <em>Goto Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getProcedureCallStatement <em>Procedure Call Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getReturnStatement <em>Return Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getExtendedReturnStatement <em>Extended Return Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getAcceptStatement <em>Accept Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getEntryCallStatement <em>Entry Call Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getRequeueStatement <em>Requeue Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getRequeueStatementWithAbort <em>Requeue Statement With Abort</em>}</li>
 *   <li>{@link Ada.StatementList#getDelayUntilStatement <em>Delay Until Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getDelayRelativeStatement <em>Delay Relative Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getTerminateAlternativeStatement <em>Terminate Alternative Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getSelectiveAcceptStatement <em>Selective Accept Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getTimedEntryCallStatement <em>Timed Entry Call Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getConditionalEntryCallStatement <em>Conditional Entry Call Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getAsynchronousSelectStatement <em>Asynchronous Select Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getAbortStatement <em>Abort Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getRaiseStatement <em>Raise Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getCodeStatement <em>Code Statement</em>}</li>
 *   <li>{@link Ada.StatementList#getComment <em>Comment</em>}</li>
 *   <li>{@link Ada.StatementList#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getAsynchronousPragma <em>Asynchronous Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getAtomicPragma <em>Atomic Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getAttachHandlerPragma <em>Attach Handler Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getControlledPragma <em>Controlled Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getConventionPragma <em>Convention Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getDiscardNamesPragma <em>Discard Names Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getElaboratePragma <em>Elaborate Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getElaborateAllPragma <em>Elaborate All Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getExportPragma <em>Export Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getImportPragma <em>Import Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getInlinePragma <em>Inline Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getInspectionPointPragma <em>Inspection Point Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getLinkerOptionsPragma <em>Linker Options Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getListPragma <em>List Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getLockingPolicyPragma <em>Locking Policy Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getOptimizePragma <em>Optimize Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getPackPragma <em>Pack Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getPagePragma <em>Page Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getPreelaboratePragma <em>Preelaborate Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getPriorityPragma <em>Priority Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getPurePragma <em>Pure Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getRemoteTypesPragma <em>Remote Types Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getRestrictionsPragma <em>Restrictions Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getReviewablePragma <em>Reviewable Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getSharedPassivePragma <em>Shared Passive Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getStorageSizePragma <em>Storage Size Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getSuppressPragma <em>Suppress Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getVolatilePragma <em>Volatile Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getAssertPragma <em>Assert Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getNoReturnPragma <em>No Return Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getProfilePragma <em>Profile Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getUnsuppressPragma <em>Unsuppress Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getCpuPragma <em>Cpu Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getIndependentPragma <em>Independent Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getIndependentComponentsPragma <em>Independent Components Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}</li>
 *   <li>{@link Ada.StatementList#getUnknownPragma <em>Unknown Pragma</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getStatementList()
 * @model extendedMetaData="name='Statement_List' kind='elementOnly'"
 * @generated
 */
public interface StatementList extends EObject {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' attribute list.
	 * @see Ada.AdaPackage#getStatementList_Group()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:0'"
	 * @generated
	 */
	FeatureMap getGroup();

	/**
	 * Returns the value of the '<em><b>Not An Element</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NotAnElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not An Element</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not An Element</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_NotAnElement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='not_an_element' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NotAnElement> getNotAnElement();

	/**
	 * Returns the value of the '<em><b>Null Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NullStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Null Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Null Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_NullStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='null_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NullStatement> getNullStatement();

	/**
	 * Returns the value of the '<em><b>Assignment Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AssignmentStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assignment Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assignment Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_AssignmentStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='assignment_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AssignmentStatement> getAssignmentStatement();

	/**
	 * Returns the value of the '<em><b>If Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IfStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>If Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>If Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_IfStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='if_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IfStatement> getIfStatement();

	/**
	 * Returns the value of the '<em><b>Case Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CaseStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Case Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Case Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_CaseStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='case_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CaseStatement> getCaseStatement();

	/**
	 * Returns the value of the '<em><b>Loop Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LoopStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Loop Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Loop Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_LoopStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='loop_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LoopStatement> getLoopStatement();

	/**
	 * Returns the value of the '<em><b>While Loop Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WhileLoopStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>While Loop Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>While Loop Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_WhileLoopStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='while_loop_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WhileLoopStatement> getWhileLoopStatement();

	/**
	 * Returns the value of the '<em><b>For Loop Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ForLoopStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>For Loop Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>For Loop Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_ForLoopStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='for_loop_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ForLoopStatement> getForLoopStatement();

	/**
	 * Returns the value of the '<em><b>Block Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.BlockStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Block Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Block Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_BlockStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='block_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<BlockStatement> getBlockStatement();

	/**
	 * Returns the value of the '<em><b>Exit Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExitStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exit Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exit Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_ExitStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='exit_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExitStatement> getExitStatement();

	/**
	 * Returns the value of the '<em><b>Goto Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.GotoStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Goto Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Goto Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_GotoStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='goto_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<GotoStatement> getGotoStatement();

	/**
	 * Returns the value of the '<em><b>Procedure Call Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProcedureCallStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure Call Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure Call Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_ProcedureCallStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='procedure_call_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProcedureCallStatement> getProcedureCallStatement();

	/**
	 * Returns the value of the '<em><b>Return Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ReturnStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Return Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Return Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_ReturnStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='return_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ReturnStatement> getReturnStatement();

	/**
	 * Returns the value of the '<em><b>Extended Return Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExtendedReturnStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extended Return Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extended Return Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_ExtendedReturnStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='extended_return_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExtendedReturnStatement> getExtendedReturnStatement();

	/**
	 * Returns the value of the '<em><b>Accept Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AcceptStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Accept Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Accept Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_AcceptStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='accept_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AcceptStatement> getAcceptStatement();

	/**
	 * Returns the value of the '<em><b>Entry Call Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.EntryCallStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry Call Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry Call Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_EntryCallStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='entry_call_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<EntryCallStatement> getEntryCallStatement();

	/**
	 * Returns the value of the '<em><b>Requeue Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RequeueStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requeue Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requeue Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_RequeueStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='requeue_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RequeueStatement> getRequeueStatement();

	/**
	 * Returns the value of the '<em><b>Requeue Statement With Abort</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RequeueStatementWithAbort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requeue Statement With Abort</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requeue Statement With Abort</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_RequeueStatementWithAbort()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='requeue_statement_with_abort' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RequeueStatementWithAbort> getRequeueStatementWithAbort();

	/**
	 * Returns the value of the '<em><b>Delay Until Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DelayUntilStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delay Until Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delay Until Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_DelayUntilStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='delay_until_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DelayUntilStatement> getDelayUntilStatement();

	/**
	 * Returns the value of the '<em><b>Delay Relative Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DelayRelativeStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delay Relative Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delay Relative Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_DelayRelativeStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='delay_relative_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DelayRelativeStatement> getDelayRelativeStatement();

	/**
	 * Returns the value of the '<em><b>Terminate Alternative Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TerminateAlternativeStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Terminate Alternative Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Terminate Alternative Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_TerminateAlternativeStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='terminate_alternative_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TerminateAlternativeStatement> getTerminateAlternativeStatement();

	/**
	 * Returns the value of the '<em><b>Selective Accept Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SelectiveAcceptStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selective Accept Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selective Accept Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_SelectiveAcceptStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='selective_accept_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SelectiveAcceptStatement> getSelectiveAcceptStatement();

	/**
	 * Returns the value of the '<em><b>Timed Entry Call Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TimedEntryCallStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timed Entry Call Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timed Entry Call Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_TimedEntryCallStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='timed_entry_call_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TimedEntryCallStatement> getTimedEntryCallStatement();

	/**
	 * Returns the value of the '<em><b>Conditional Entry Call Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ConditionalEntryCallStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conditional Entry Call Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conditional Entry Call Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_ConditionalEntryCallStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='conditional_entry_call_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ConditionalEntryCallStatement> getConditionalEntryCallStatement();

	/**
	 * Returns the value of the '<em><b>Asynchronous Select Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AsynchronousSelectStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asynchronous Select Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asynchronous Select Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_AsynchronousSelectStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='asynchronous_select_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AsynchronousSelectStatement> getAsynchronousSelectStatement();

	/**
	 * Returns the value of the '<em><b>Abort Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AbortStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abort Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abort Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_AbortStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='abort_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AbortStatement> getAbortStatement();

	/**
	 * Returns the value of the '<em><b>Raise Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RaiseStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Raise Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Raise Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_RaiseStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='raise_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RaiseStatement> getRaiseStatement();

	/**
	 * Returns the value of the '<em><b>Code Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CodeStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Code Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_CodeStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='code_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CodeStatement> getCodeStatement();

	/**
	 * Returns the value of the '<em><b>Comment</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.Comment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comment</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comment</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_Comment()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='comment' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<Comment> getComment();

	/**
	 * Returns the value of the '<em><b>All Calls Remote Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AllCallsRemotePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Calls Remote Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Calls Remote Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_AllCallsRemotePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='all_calls_remote_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AllCallsRemotePragma> getAllCallsRemotePragma();

	/**
	 * Returns the value of the '<em><b>Asynchronous Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AsynchronousPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asynchronous Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asynchronous Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_AsynchronousPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='asynchronous_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AsynchronousPragma> getAsynchronousPragma();

	/**
	 * Returns the value of the '<em><b>Atomic Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AtomicPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_AtomicPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='atomic_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AtomicPragma> getAtomicPragma();

	/**
	 * Returns the value of the '<em><b>Atomic Components Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AtomicComponentsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Components Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Components Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_AtomicComponentsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='atomic_components_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AtomicComponentsPragma> getAtomicComponentsPragma();

	/**
	 * Returns the value of the '<em><b>Attach Handler Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AttachHandlerPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attach Handler Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attach Handler Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_AttachHandlerPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='attach_handler_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AttachHandlerPragma> getAttachHandlerPragma();

	/**
	 * Returns the value of the '<em><b>Controlled Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ControlledPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controlled Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controlled Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_ControlledPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='controlled_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ControlledPragma> getControlledPragma();

	/**
	 * Returns the value of the '<em><b>Convention Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ConventionPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Convention Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Convention Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_ConventionPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='convention_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ConventionPragma> getConventionPragma();

	/**
	 * Returns the value of the '<em><b>Discard Names Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscardNamesPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discard Names Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discard Names Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_DiscardNamesPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discard_names_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscardNamesPragma> getDiscardNamesPragma();

	/**
	 * Returns the value of the '<em><b>Elaborate Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElaboratePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_ElaboratePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='elaborate_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElaboratePragma> getElaboratePragma();

	/**
	 * Returns the value of the '<em><b>Elaborate All Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElaborateAllPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate All Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate All Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_ElaborateAllPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='elaborate_all_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElaborateAllPragma> getElaborateAllPragma();

	/**
	 * Returns the value of the '<em><b>Elaborate Body Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElaborateBodyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Body Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Body Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_ElaborateBodyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='elaborate_body_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElaborateBodyPragma> getElaborateBodyPragma();

	/**
	 * Returns the value of the '<em><b>Export Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExportPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Export Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Export Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_ExportPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='export_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExportPragma> getExportPragma();

	/**
	 * Returns the value of the '<em><b>Import Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ImportPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Import Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Import Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_ImportPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='import_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ImportPragma> getImportPragma();

	/**
	 * Returns the value of the '<em><b>Inline Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InlinePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inline Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inline Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_InlinePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='inline_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InlinePragma> getInlinePragma();

	/**
	 * Returns the value of the '<em><b>Inspection Point Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InspectionPointPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inspection Point Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inspection Point Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_InspectionPointPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='inspection_point_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InspectionPointPragma> getInspectionPointPragma();

	/**
	 * Returns the value of the '<em><b>Interrupt Handler Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InterruptHandlerPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Handler Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Handler Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_InterruptHandlerPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='interrupt_handler_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InterruptHandlerPragma> getInterruptHandlerPragma();

	/**
	 * Returns the value of the '<em><b>Interrupt Priority Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InterruptPriorityPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Priority Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Priority Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_InterruptPriorityPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='interrupt_priority_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InterruptPriorityPragma> getInterruptPriorityPragma();

	/**
	 * Returns the value of the '<em><b>Linker Options Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LinkerOptionsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Linker Options Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Linker Options Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_LinkerOptionsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='linker_options_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LinkerOptionsPragma> getLinkerOptionsPragma();

	/**
	 * Returns the value of the '<em><b>List Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ListPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_ListPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='list_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ListPragma> getListPragma();

	/**
	 * Returns the value of the '<em><b>Locking Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LockingPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Locking Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Locking Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_LockingPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='locking_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LockingPolicyPragma> getLockingPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Normalize Scalars Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NormalizeScalarsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Normalize Scalars Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Normalize Scalars Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_NormalizeScalarsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='normalize_scalars_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NormalizeScalarsPragma> getNormalizeScalarsPragma();

	/**
	 * Returns the value of the '<em><b>Optimize Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OptimizePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Optimize Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Optimize Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_OptimizePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='optimize_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OptimizePragma> getOptimizePragma();

	/**
	 * Returns the value of the '<em><b>Pack Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PackPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pack Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pack Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_PackPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='pack_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PackPragma> getPackPragma();

	/**
	 * Returns the value of the '<em><b>Page Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PagePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Page Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Page Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_PagePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='page_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PagePragma> getPagePragma();

	/**
	 * Returns the value of the '<em><b>Preelaborate Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PreelaboratePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborate Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborate Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_PreelaboratePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='preelaborate_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PreelaboratePragma> getPreelaboratePragma();

	/**
	 * Returns the value of the '<em><b>Priority Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PriorityPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_PriorityPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='priority_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PriorityPragma> getPriorityPragma();

	/**
	 * Returns the value of the '<em><b>Pure Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PurePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pure Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pure Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_PurePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='pure_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PurePragma> getPurePragma();

	/**
	 * Returns the value of the '<em><b>Queuing Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.QueuingPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Queuing Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Queuing Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_QueuingPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='queuing_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<QueuingPolicyPragma> getQueuingPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Remote Call Interface Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RemoteCallInterfacePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Call Interface Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Call Interface Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_RemoteCallInterfacePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='remote_call_interface_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RemoteCallInterfacePragma> getRemoteCallInterfacePragma();

	/**
	 * Returns the value of the '<em><b>Remote Types Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RemoteTypesPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Types Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Types Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_RemoteTypesPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='remote_types_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RemoteTypesPragma> getRemoteTypesPragma();

	/**
	 * Returns the value of the '<em><b>Restrictions Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RestrictionsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Restrictions Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Restrictions Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_RestrictionsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='restrictions_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RestrictionsPragma> getRestrictionsPragma();

	/**
	 * Returns the value of the '<em><b>Reviewable Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ReviewablePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reviewable Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reviewable Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_ReviewablePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='reviewable_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ReviewablePragma> getReviewablePragma();

	/**
	 * Returns the value of the '<em><b>Shared Passive Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SharedPassivePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shared Passive Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shared Passive Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_SharedPassivePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='shared_passive_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SharedPassivePragma> getSharedPassivePragma();

	/**
	 * Returns the value of the '<em><b>Storage Size Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.StorageSizePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Size Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Size Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_StorageSizePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='storage_size_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<StorageSizePragma> getStorageSizePragma();

	/**
	 * Returns the value of the '<em><b>Suppress Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SuppressPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Suppress Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Suppress Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_SuppressPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='suppress_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SuppressPragma> getSuppressPragma();

	/**
	 * Returns the value of the '<em><b>Task Dispatching Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TaskDispatchingPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Dispatching Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Dispatching Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_TaskDispatchingPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='task_dispatching_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TaskDispatchingPolicyPragma> getTaskDispatchingPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Volatile Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.VolatilePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_VolatilePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='volatile_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<VolatilePragma> getVolatilePragma();

	/**
	 * Returns the value of the '<em><b>Volatile Components Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.VolatileComponentsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Components Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Components Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_VolatileComponentsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='volatile_components_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<VolatileComponentsPragma> getVolatileComponentsPragma();

	/**
	 * Returns the value of the '<em><b>Assert Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AssertPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assert Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assert Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_AssertPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='assert_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AssertPragma> getAssertPragma();

	/**
	 * Returns the value of the '<em><b>Assertion Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AssertionPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assertion Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assertion Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_AssertionPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='assertion_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AssertionPolicyPragma> getAssertionPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Detect Blocking Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DetectBlockingPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Detect Blocking Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Detect Blocking Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_DetectBlockingPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='detect_blocking_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DetectBlockingPragma> getDetectBlockingPragma();

	/**
	 * Returns the value of the '<em><b>No Return Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NoReturnPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>No Return Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>No Return Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_NoReturnPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='no_return_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NoReturnPragma> getNoReturnPragma();

	/**
	 * Returns the value of the '<em><b>Partition Elaboration Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PartitionElaborationPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partition Elaboration Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partition Elaboration Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_PartitionElaborationPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='partition_elaboration_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PartitionElaborationPolicyPragma> getPartitionElaborationPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Preelaborable Initialization Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PreelaborableInitializationPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborable Initialization Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborable Initialization Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_PreelaborableInitializationPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='preelaborable_initialization_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PreelaborableInitializationPragma> getPreelaborableInitializationPragma();

	/**
	 * Returns the value of the '<em><b>Priority Specific Dispatching Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PrioritySpecificDispatchingPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Specific Dispatching Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Specific Dispatching Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_PrioritySpecificDispatchingPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='priority_specific_dispatching_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PrioritySpecificDispatchingPragma> getPrioritySpecificDispatchingPragma();

	/**
	 * Returns the value of the '<em><b>Profile Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProfilePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Profile Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Profile Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_ProfilePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='profile_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProfilePragma> getProfilePragma();

	/**
	 * Returns the value of the '<em><b>Relative Deadline Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RelativeDeadlinePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relative Deadline Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relative Deadline Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_RelativeDeadlinePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='relative_deadline_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RelativeDeadlinePragma> getRelativeDeadlinePragma();

	/**
	 * Returns the value of the '<em><b>Unchecked Union Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UncheckedUnionPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unchecked Union Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unchecked Union Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_UncheckedUnionPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unchecked_union_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UncheckedUnionPragma> getUncheckedUnionPragma();

	/**
	 * Returns the value of the '<em><b>Unsuppress Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnsuppressPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unsuppress Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unsuppress Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_UnsuppressPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unsuppress_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnsuppressPragma> getUnsuppressPragma();

	/**
	 * Returns the value of the '<em><b>Default Storage Pool Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefaultStoragePoolPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Storage Pool Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Storage Pool Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_DefaultStoragePoolPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='default_storage_pool_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefaultStoragePoolPragma> getDefaultStoragePoolPragma();

	/**
	 * Returns the value of the '<em><b>Dispatching Domain Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DispatchingDomainPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dispatching Domain Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dispatching Domain Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_DispatchingDomainPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='dispatching_domain_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DispatchingDomainPragma> getDispatchingDomainPragma();

	/**
	 * Returns the value of the '<em><b>Cpu Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CpuPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cpu Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cpu Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_CpuPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='cpu_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CpuPragma> getCpuPragma();

	/**
	 * Returns the value of the '<em><b>Independent Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IndependentPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_IndependentPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='independent_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IndependentPragma> getIndependentPragma();

	/**
	 * Returns the value of the '<em><b>Independent Components Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IndependentComponentsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Components Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Components Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_IndependentComponentsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='independent_components_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IndependentComponentsPragma> getIndependentComponentsPragma();

	/**
	 * Returns the value of the '<em><b>Implementation Defined Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ImplementationDefinedPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implementation Defined Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implementation Defined Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_ImplementationDefinedPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='implementation_defined_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ImplementationDefinedPragma> getImplementationDefinedPragma();

	/**
	 * Returns the value of the '<em><b>Unknown Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnknownPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unknown Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unknown Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getStatementList_UnknownPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unknown_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnknownPragma> getUnknownPragma();

} // StatementList
