package org.eclipse.epsilon.emc.ada;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.eclipse.epsilon.emc.emf.EmfModel;
import org.eclipse.epsilon.eol.exceptions.models.EolModelLoadingException;

public class AdaModel extends EmfModel {
	
	protected AdaResourceFactory resourceFactory = new AdaResourceFactory();
	
	@Override
	protected ResourceSet createResourceSet() {
		return new CachedResourceSet() {
			public Resource createNewResource(URI uri, String contentType) {
				return resourceFactory.createResource(uri);
			}
		};
	}
	
	@Override
	protected void loadModel() throws EolModelLoadingException {
		setMetamodelUri("platform:/resource/org.eclipse.epsilon.emc.ada/model/ada.xsd");
		super.loadModel();
	}
}
