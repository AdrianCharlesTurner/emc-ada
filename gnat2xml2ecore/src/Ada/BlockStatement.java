/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Block Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.BlockStatement#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.BlockStatement#getLabelNamesQl <em>Label Names Ql</em>}</li>
 *   <li>{@link Ada.BlockStatement#getStatementIdentifierQ <em>Statement Identifier Q</em>}</li>
 *   <li>{@link Ada.BlockStatement#getBlockDeclarativeItemsQl <em>Block Declarative Items Ql</em>}</li>
 *   <li>{@link Ada.BlockStatement#getBlockStatementsQl <em>Block Statements Ql</em>}</li>
 *   <li>{@link Ada.BlockStatement#getBlockExceptionHandlersQl <em>Block Exception Handlers Ql</em>}</li>
 *   <li>{@link Ada.BlockStatement#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getBlockStatement()
 * @model extendedMetaData="name='Block_Statement' kind='elementOnly'"
 * @generated
 */
public interface BlockStatement extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getBlockStatement_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.BlockStatement#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Label Names Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label Names Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label Names Ql</em>' containment reference.
	 * @see #setLabelNamesQl(DefiningNameList)
	 * @see Ada.AdaPackage#getBlockStatement_LabelNamesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='label_names_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameList getLabelNamesQl();

	/**
	 * Sets the value of the '{@link Ada.BlockStatement#getLabelNamesQl <em>Label Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label Names Ql</em>' containment reference.
	 * @see #getLabelNamesQl()
	 * @generated
	 */
	void setLabelNamesQl(DefiningNameList value);

	/**
	 * Returns the value of the '<em><b>Statement Identifier Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Statement Identifier Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Statement Identifier Q</em>' containment reference.
	 * @see #setStatementIdentifierQ(DefiningNameClass)
	 * @see Ada.AdaPackage#getBlockStatement_StatementIdentifierQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='statement_identifier_q' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameClass getStatementIdentifierQ();

	/**
	 * Sets the value of the '{@link Ada.BlockStatement#getStatementIdentifierQ <em>Statement Identifier Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Statement Identifier Q</em>' containment reference.
	 * @see #getStatementIdentifierQ()
	 * @generated
	 */
	void setStatementIdentifierQ(DefiningNameClass value);

	/**
	 * Returns the value of the '<em><b>Block Declarative Items Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Block Declarative Items Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Block Declarative Items Ql</em>' containment reference.
	 * @see #setBlockDeclarativeItemsQl(DeclarativeItemList)
	 * @see Ada.AdaPackage#getBlockStatement_BlockDeclarativeItemsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='block_declarative_items_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DeclarativeItemList getBlockDeclarativeItemsQl();

	/**
	 * Sets the value of the '{@link Ada.BlockStatement#getBlockDeclarativeItemsQl <em>Block Declarative Items Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Block Declarative Items Ql</em>' containment reference.
	 * @see #getBlockDeclarativeItemsQl()
	 * @generated
	 */
	void setBlockDeclarativeItemsQl(DeclarativeItemList value);

	/**
	 * Returns the value of the '<em><b>Block Statements Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Block Statements Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Block Statements Ql</em>' containment reference.
	 * @see #setBlockStatementsQl(StatementList)
	 * @see Ada.AdaPackage#getBlockStatement_BlockStatementsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='block_statements_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	StatementList getBlockStatementsQl();

	/**
	 * Sets the value of the '{@link Ada.BlockStatement#getBlockStatementsQl <em>Block Statements Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Block Statements Ql</em>' containment reference.
	 * @see #getBlockStatementsQl()
	 * @generated
	 */
	void setBlockStatementsQl(StatementList value);

	/**
	 * Returns the value of the '<em><b>Block Exception Handlers Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Block Exception Handlers Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Block Exception Handlers Ql</em>' containment reference.
	 * @see #setBlockExceptionHandlersQl(ExceptionHandlerList)
	 * @see Ada.AdaPackage#getBlockStatement_BlockExceptionHandlersQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='block_exception_handlers_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ExceptionHandlerList getBlockExceptionHandlersQl();

	/**
	 * Sets the value of the '{@link Ada.BlockStatement#getBlockExceptionHandlersQl <em>Block Exception Handlers Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Block Exception Handlers Ql</em>' containment reference.
	 * @see #getBlockExceptionHandlersQl()
	 * @generated
	 */
	void setBlockExceptionHandlersQl(ExceptionHandlerList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getBlockStatement_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.BlockStatement#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // BlockStatement
