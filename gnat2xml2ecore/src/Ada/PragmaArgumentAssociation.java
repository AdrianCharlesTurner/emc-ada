/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pragma Argument Association</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.PragmaArgumentAssociation#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.PragmaArgumentAssociation#getFormalParameterQ <em>Formal Parameter Q</em>}</li>
 *   <li>{@link Ada.PragmaArgumentAssociation#getActualParameterQ <em>Actual Parameter Q</em>}</li>
 *   <li>{@link Ada.PragmaArgumentAssociation#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getPragmaArgumentAssociation()
 * @model extendedMetaData="name='Pragma_Argument_Association' kind='elementOnly'"
 * @generated
 */
public interface PragmaArgumentAssociation extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getPragmaArgumentAssociation_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.PragmaArgumentAssociation#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Formal Parameter Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Parameter Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Parameter Q</em>' containment reference.
	 * @see #setFormalParameterQ(ElementClass)
	 * @see Ada.AdaPackage#getPragmaArgumentAssociation_FormalParameterQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='formal_parameter_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementClass getFormalParameterQ();

	/**
	 * Sets the value of the '{@link Ada.PragmaArgumentAssociation#getFormalParameterQ <em>Formal Parameter Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Parameter Q</em>' containment reference.
	 * @see #getFormalParameterQ()
	 * @generated
	 */
	void setFormalParameterQ(ElementClass value);

	/**
	 * Returns the value of the '<em><b>Actual Parameter Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actual Parameter Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actual Parameter Q</em>' containment reference.
	 * @see #setActualParameterQ(ExpressionClass)
	 * @see Ada.AdaPackage#getPragmaArgumentAssociation_ActualParameterQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='actual_parameter_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getActualParameterQ();

	/**
	 * Sets the value of the '{@link Ada.PragmaArgumentAssociation#getActualParameterQ <em>Actual Parameter Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Actual Parameter Q</em>' containment reference.
	 * @see #getActualParameterQ()
	 * @generated
	 */
	void setActualParameterQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getPragmaArgumentAssociation_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.PragmaArgumentAssociation#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // PragmaArgumentAssociation
