/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Entry Call Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.EntryCallStatement#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.EntryCallStatement#getLabelNamesQl <em>Label Names Ql</em>}</li>
 *   <li>{@link Ada.EntryCallStatement#getCalledNameQ <em>Called Name Q</em>}</li>
 *   <li>{@link Ada.EntryCallStatement#getCallStatementParametersQl <em>Call Statement Parameters Ql</em>}</li>
 *   <li>{@link Ada.EntryCallStatement#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getEntryCallStatement()
 * @model extendedMetaData="name='Entry_Call_Statement' kind='elementOnly'"
 * @generated
 */
public interface EntryCallStatement extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getEntryCallStatement_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.EntryCallStatement#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Label Names Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label Names Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label Names Ql</em>' containment reference.
	 * @see #setLabelNamesQl(DefiningNameList)
	 * @see Ada.AdaPackage#getEntryCallStatement_LabelNamesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='label_names_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameList getLabelNamesQl();

	/**
	 * Sets the value of the '{@link Ada.EntryCallStatement#getLabelNamesQl <em>Label Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label Names Ql</em>' containment reference.
	 * @see #getLabelNamesQl()
	 * @generated
	 */
	void setLabelNamesQl(DefiningNameList value);

	/**
	 * Returns the value of the '<em><b>Called Name Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Called Name Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Called Name Q</em>' containment reference.
	 * @see #setCalledNameQ(ExpressionClass)
	 * @see Ada.AdaPackage#getEntryCallStatement_CalledNameQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='called_name_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getCalledNameQ();

	/**
	 * Sets the value of the '{@link Ada.EntryCallStatement#getCalledNameQ <em>Called Name Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Called Name Q</em>' containment reference.
	 * @see #getCalledNameQ()
	 * @generated
	 */
	void setCalledNameQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Call Statement Parameters Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Call Statement Parameters Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Call Statement Parameters Ql</em>' containment reference.
	 * @see #setCallStatementParametersQl(AssociationList)
	 * @see Ada.AdaPackage#getEntryCallStatement_CallStatementParametersQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='call_statement_parameters_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	AssociationList getCallStatementParametersQl();

	/**
	 * Sets the value of the '{@link Ada.EntryCallStatement#getCallStatementParametersQl <em>Call Statement Parameters Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Call Statement Parameters Ql</em>' containment reference.
	 * @see #getCallStatementParametersQl()
	 * @generated
	 */
	void setCallStatementParametersQl(AssociationList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getEntryCallStatement_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.EntryCallStatement#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // EntryCallStatement
