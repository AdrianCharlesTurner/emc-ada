/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.ComponentClause;
import Ada.DiscreteRangeClass;
import Ada.ExpressionClass;
import Ada.NameClass;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component Clause</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.ComponentClauseImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.ComponentClauseImpl#getRepresentationClauseNameQ <em>Representation Clause Name Q</em>}</li>
 *   <li>{@link Ada.impl.ComponentClauseImpl#getComponentClausePositionQ <em>Component Clause Position Q</em>}</li>
 *   <li>{@link Ada.impl.ComponentClauseImpl#getComponentClauseRangeQ <em>Component Clause Range Q</em>}</li>
 *   <li>{@link Ada.impl.ComponentClauseImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComponentClauseImpl extends MinimalEObjectImpl.Container implements ComponentClause {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getRepresentationClauseNameQ() <em>Representation Clause Name Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepresentationClauseNameQ()
	 * @generated
	 * @ordered
	 */
	protected NameClass representationClauseNameQ;

	/**
	 * The cached value of the '{@link #getComponentClausePositionQ() <em>Component Clause Position Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentClausePositionQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass componentClausePositionQ;

	/**
	 * The cached value of the '{@link #getComponentClauseRangeQ() <em>Component Clause Range Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentClauseRangeQ()
	 * @generated
	 * @ordered
	 */
	protected DiscreteRangeClass componentClauseRangeQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentClauseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getComponentClause();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.COMPONENT_CLAUSE__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.COMPONENT_CLAUSE__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.COMPONENT_CLAUSE__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.COMPONENT_CLAUSE__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NameClass getRepresentationClauseNameQ() {
		return representationClauseNameQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRepresentationClauseNameQ(NameClass newRepresentationClauseNameQ, NotificationChain msgs) {
		NameClass oldRepresentationClauseNameQ = representationClauseNameQ;
		representationClauseNameQ = newRepresentationClauseNameQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.COMPONENT_CLAUSE__REPRESENTATION_CLAUSE_NAME_Q, oldRepresentationClauseNameQ, newRepresentationClauseNameQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRepresentationClauseNameQ(NameClass newRepresentationClauseNameQ) {
		if (newRepresentationClauseNameQ != representationClauseNameQ) {
			NotificationChain msgs = null;
			if (representationClauseNameQ != null)
				msgs = ((InternalEObject)representationClauseNameQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.COMPONENT_CLAUSE__REPRESENTATION_CLAUSE_NAME_Q, null, msgs);
			if (newRepresentationClauseNameQ != null)
				msgs = ((InternalEObject)newRepresentationClauseNameQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.COMPONENT_CLAUSE__REPRESENTATION_CLAUSE_NAME_Q, null, msgs);
			msgs = basicSetRepresentationClauseNameQ(newRepresentationClauseNameQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.COMPONENT_CLAUSE__REPRESENTATION_CLAUSE_NAME_Q, newRepresentationClauseNameQ, newRepresentationClauseNameQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getComponentClausePositionQ() {
		return componentClausePositionQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComponentClausePositionQ(ExpressionClass newComponentClausePositionQ, NotificationChain msgs) {
		ExpressionClass oldComponentClausePositionQ = componentClausePositionQ;
		componentClausePositionQ = newComponentClausePositionQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.COMPONENT_CLAUSE__COMPONENT_CLAUSE_POSITION_Q, oldComponentClausePositionQ, newComponentClausePositionQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponentClausePositionQ(ExpressionClass newComponentClausePositionQ) {
		if (newComponentClausePositionQ != componentClausePositionQ) {
			NotificationChain msgs = null;
			if (componentClausePositionQ != null)
				msgs = ((InternalEObject)componentClausePositionQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.COMPONENT_CLAUSE__COMPONENT_CLAUSE_POSITION_Q, null, msgs);
			if (newComponentClausePositionQ != null)
				msgs = ((InternalEObject)newComponentClausePositionQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.COMPONENT_CLAUSE__COMPONENT_CLAUSE_POSITION_Q, null, msgs);
			msgs = basicSetComponentClausePositionQ(newComponentClausePositionQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.COMPONENT_CLAUSE__COMPONENT_CLAUSE_POSITION_Q, newComponentClausePositionQ, newComponentClausePositionQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscreteRangeClass getComponentClauseRangeQ() {
		return componentClauseRangeQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComponentClauseRangeQ(DiscreteRangeClass newComponentClauseRangeQ, NotificationChain msgs) {
		DiscreteRangeClass oldComponentClauseRangeQ = componentClauseRangeQ;
		componentClauseRangeQ = newComponentClauseRangeQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.COMPONENT_CLAUSE__COMPONENT_CLAUSE_RANGE_Q, oldComponentClauseRangeQ, newComponentClauseRangeQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponentClauseRangeQ(DiscreteRangeClass newComponentClauseRangeQ) {
		if (newComponentClauseRangeQ != componentClauseRangeQ) {
			NotificationChain msgs = null;
			if (componentClauseRangeQ != null)
				msgs = ((InternalEObject)componentClauseRangeQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.COMPONENT_CLAUSE__COMPONENT_CLAUSE_RANGE_Q, null, msgs);
			if (newComponentClauseRangeQ != null)
				msgs = ((InternalEObject)newComponentClauseRangeQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.COMPONENT_CLAUSE__COMPONENT_CLAUSE_RANGE_Q, null, msgs);
			msgs = basicSetComponentClauseRangeQ(newComponentClauseRangeQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.COMPONENT_CLAUSE__COMPONENT_CLAUSE_RANGE_Q, newComponentClauseRangeQ, newComponentClauseRangeQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.COMPONENT_CLAUSE__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.COMPONENT_CLAUSE__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.COMPONENT_CLAUSE__REPRESENTATION_CLAUSE_NAME_Q:
				return basicSetRepresentationClauseNameQ(null, msgs);
			case AdaPackage.COMPONENT_CLAUSE__COMPONENT_CLAUSE_POSITION_Q:
				return basicSetComponentClausePositionQ(null, msgs);
			case AdaPackage.COMPONENT_CLAUSE__COMPONENT_CLAUSE_RANGE_Q:
				return basicSetComponentClauseRangeQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.COMPONENT_CLAUSE__SLOC:
				return getSloc();
			case AdaPackage.COMPONENT_CLAUSE__REPRESENTATION_CLAUSE_NAME_Q:
				return getRepresentationClauseNameQ();
			case AdaPackage.COMPONENT_CLAUSE__COMPONENT_CLAUSE_POSITION_Q:
				return getComponentClausePositionQ();
			case AdaPackage.COMPONENT_CLAUSE__COMPONENT_CLAUSE_RANGE_Q:
				return getComponentClauseRangeQ();
			case AdaPackage.COMPONENT_CLAUSE__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.COMPONENT_CLAUSE__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.COMPONENT_CLAUSE__REPRESENTATION_CLAUSE_NAME_Q:
				setRepresentationClauseNameQ((NameClass)newValue);
				return;
			case AdaPackage.COMPONENT_CLAUSE__COMPONENT_CLAUSE_POSITION_Q:
				setComponentClausePositionQ((ExpressionClass)newValue);
				return;
			case AdaPackage.COMPONENT_CLAUSE__COMPONENT_CLAUSE_RANGE_Q:
				setComponentClauseRangeQ((DiscreteRangeClass)newValue);
				return;
			case AdaPackage.COMPONENT_CLAUSE__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.COMPONENT_CLAUSE__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.COMPONENT_CLAUSE__REPRESENTATION_CLAUSE_NAME_Q:
				setRepresentationClauseNameQ((NameClass)null);
				return;
			case AdaPackage.COMPONENT_CLAUSE__COMPONENT_CLAUSE_POSITION_Q:
				setComponentClausePositionQ((ExpressionClass)null);
				return;
			case AdaPackage.COMPONENT_CLAUSE__COMPONENT_CLAUSE_RANGE_Q:
				setComponentClauseRangeQ((DiscreteRangeClass)null);
				return;
			case AdaPackage.COMPONENT_CLAUSE__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.COMPONENT_CLAUSE__SLOC:
				return sloc != null;
			case AdaPackage.COMPONENT_CLAUSE__REPRESENTATION_CLAUSE_NAME_Q:
				return representationClauseNameQ != null;
			case AdaPackage.COMPONENT_CLAUSE__COMPONENT_CLAUSE_POSITION_Q:
				return componentClausePositionQ != null;
			case AdaPackage.COMPONENT_CLAUSE__COMPONENT_CLAUSE_RANGE_Q:
				return componentClauseRangeQ != null;
			case AdaPackage.COMPONENT_CLAUSE__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //ComponentClauseImpl
