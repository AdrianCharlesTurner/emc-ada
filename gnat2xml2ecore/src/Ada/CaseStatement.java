/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Case Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.CaseStatement#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.CaseStatement#getLabelNamesQl <em>Label Names Ql</em>}</li>
 *   <li>{@link Ada.CaseStatement#getCaseExpressionQ <em>Case Expression Q</em>}</li>
 *   <li>{@link Ada.CaseStatement#getStatementPathsQl <em>Statement Paths Ql</em>}</li>
 *   <li>{@link Ada.CaseStatement#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getCaseStatement()
 * @model extendedMetaData="name='Case_Statement' kind='elementOnly'"
 * @generated
 */
public interface CaseStatement extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getCaseStatement_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.CaseStatement#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Label Names Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label Names Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label Names Ql</em>' containment reference.
	 * @see #setLabelNamesQl(DefiningNameList)
	 * @see Ada.AdaPackage#getCaseStatement_LabelNamesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='label_names_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameList getLabelNamesQl();

	/**
	 * Sets the value of the '{@link Ada.CaseStatement#getLabelNamesQl <em>Label Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label Names Ql</em>' containment reference.
	 * @see #getLabelNamesQl()
	 * @generated
	 */
	void setLabelNamesQl(DefiningNameList value);

	/**
	 * Returns the value of the '<em><b>Case Expression Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Case Expression Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Case Expression Q</em>' containment reference.
	 * @see #setCaseExpressionQ(ExpressionClass)
	 * @see Ada.AdaPackage#getCaseStatement_CaseExpressionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='case_expression_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getCaseExpressionQ();

	/**
	 * Sets the value of the '{@link Ada.CaseStatement#getCaseExpressionQ <em>Case Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Case Expression Q</em>' containment reference.
	 * @see #getCaseExpressionQ()
	 * @generated
	 */
	void setCaseExpressionQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Statement Paths Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Statement Paths Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Statement Paths Ql</em>' containment reference.
	 * @see #setStatementPathsQl(PathList)
	 * @see Ada.AdaPackage#getCaseStatement_StatementPathsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='statement_paths_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	PathList getStatementPathsQl();

	/**
	 * Sets the value of the '{@link Ada.CaseStatement#getStatementPathsQl <em>Statement Paths Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Statement Paths Ql</em>' containment reference.
	 * @see #getStatementPathsQl()
	 * @generated
	 */
	void setStatementPathsQl(PathList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getCaseStatement_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.CaseStatement#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // CaseStatement
