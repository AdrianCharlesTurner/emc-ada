/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Record Component Association</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.RecordComponentAssociation#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.RecordComponentAssociation#getRecordComponentChoicesQl <em>Record Component Choices Ql</em>}</li>
 *   <li>{@link Ada.RecordComponentAssociation#getComponentExpressionQ <em>Component Expression Q</em>}</li>
 *   <li>{@link Ada.RecordComponentAssociation#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getRecordComponentAssociation()
 * @model extendedMetaData="name='Record_Component_Association' kind='elementOnly'"
 * @generated
 */
public interface RecordComponentAssociation extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getRecordComponentAssociation_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.RecordComponentAssociation#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Record Component Choices Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Component Choices Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Component Choices Ql</em>' containment reference.
	 * @see #setRecordComponentChoicesQl(ExpressionList)
	 * @see Ada.AdaPackage#getRecordComponentAssociation_RecordComponentChoicesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='record_component_choices_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionList getRecordComponentChoicesQl();

	/**
	 * Sets the value of the '{@link Ada.RecordComponentAssociation#getRecordComponentChoicesQl <em>Record Component Choices Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Record Component Choices Ql</em>' containment reference.
	 * @see #getRecordComponentChoicesQl()
	 * @generated
	 */
	void setRecordComponentChoicesQl(ExpressionList value);

	/**
	 * Returns the value of the '<em><b>Component Expression Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Expression Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Expression Q</em>' containment reference.
	 * @see #setComponentExpressionQ(ExpressionClass)
	 * @see Ada.AdaPackage#getRecordComponentAssociation_ComponentExpressionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='component_expression_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getComponentExpressionQ();

	/**
	 * Sets the value of the '{@link Ada.RecordComponentAssociation#getComponentExpressionQ <em>Component Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component Expression Q</em>' containment reference.
	 * @see #getComponentExpressionQ()
	 * @generated
	 */
	void setComponentExpressionQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getRecordComponentAssociation_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.RecordComponentAssociation#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // RecordComponentAssociation
