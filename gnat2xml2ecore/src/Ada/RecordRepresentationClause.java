/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Record Representation Clause</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.RecordRepresentationClause#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.RecordRepresentationClause#getRepresentationClauseNameQ <em>Representation Clause Name Q</em>}</li>
 *   <li>{@link Ada.RecordRepresentationClause#getModClauseExpressionQ <em>Mod Clause Expression Q</em>}</li>
 *   <li>{@link Ada.RecordRepresentationClause#getComponentClausesQl <em>Component Clauses Ql</em>}</li>
 *   <li>{@link Ada.RecordRepresentationClause#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getRecordRepresentationClause()
 * @model extendedMetaData="name='Record_Representation_Clause' kind='elementOnly'"
 * @generated
 */
public interface RecordRepresentationClause extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getRecordRepresentationClause_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.RecordRepresentationClause#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Representation Clause Name Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Representation Clause Name Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Representation Clause Name Q</em>' containment reference.
	 * @see #setRepresentationClauseNameQ(NameClass)
	 * @see Ada.AdaPackage#getRecordRepresentationClause_RepresentationClauseNameQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='representation_clause_name_q' namespace='##targetNamespace'"
	 * @generated
	 */
	NameClass getRepresentationClauseNameQ();

	/**
	 * Sets the value of the '{@link Ada.RecordRepresentationClause#getRepresentationClauseNameQ <em>Representation Clause Name Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Representation Clause Name Q</em>' containment reference.
	 * @see #getRepresentationClauseNameQ()
	 * @generated
	 */
	void setRepresentationClauseNameQ(NameClass value);

	/**
	 * Returns the value of the '<em><b>Mod Clause Expression Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mod Clause Expression Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mod Clause Expression Q</em>' containment reference.
	 * @see #setModClauseExpressionQ(ExpressionClass)
	 * @see Ada.AdaPackage#getRecordRepresentationClause_ModClauseExpressionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='mod_clause_expression_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getModClauseExpressionQ();

	/**
	 * Sets the value of the '{@link Ada.RecordRepresentationClause#getModClauseExpressionQ <em>Mod Clause Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mod Clause Expression Q</em>' containment reference.
	 * @see #getModClauseExpressionQ()
	 * @generated
	 */
	void setModClauseExpressionQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Component Clauses Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Clauses Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Clauses Ql</em>' containment reference.
	 * @see #setComponentClausesQl(ComponentClauseList)
	 * @see Ada.AdaPackage#getRecordRepresentationClause_ComponentClausesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='component_clauses_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ComponentClauseList getComponentClausesQl();

	/**
	 * Sets the value of the '{@link Ada.RecordRepresentationClause#getComponentClausesQl <em>Component Clauses Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component Clauses Ql</em>' containment reference.
	 * @see #getComponentClausesQl()
	 * @generated
	 */
	void setComponentClausesQl(ComponentClauseList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getRecordRepresentationClause_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.RecordRepresentationClause#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // RecordRepresentationClause
