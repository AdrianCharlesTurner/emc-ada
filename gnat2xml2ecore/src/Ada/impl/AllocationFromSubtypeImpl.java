/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.AllocationFromSubtype;
import Ada.ElementClass;
import Ada.ExpressionClass;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Allocation From Subtype</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.AllocationFromSubtypeImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.AllocationFromSubtypeImpl#getSubpoolNameQ <em>Subpool Name Q</em>}</li>
 *   <li>{@link Ada.impl.AllocationFromSubtypeImpl#getAllocatorSubtypeIndicationQ <em>Allocator Subtype Indication Q</em>}</li>
 *   <li>{@link Ada.impl.AllocationFromSubtypeImpl#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.impl.AllocationFromSubtypeImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AllocationFromSubtypeImpl extends MinimalEObjectImpl.Container implements AllocationFromSubtype {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getSubpoolNameQ() <em>Subpool Name Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubpoolNameQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass subpoolNameQ;

	/**
	 * The cached value of the '{@link #getAllocatorSubtypeIndicationQ() <em>Allocator Subtype Indication Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllocatorSubtypeIndicationQ()
	 * @generated
	 * @ordered
	 */
	protected ElementClass allocatorSubtypeIndicationQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AllocationFromSubtypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getAllocationFromSubtype();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ALLOCATION_FROM_SUBTYPE__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ALLOCATION_FROM_SUBTYPE__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ALLOCATION_FROM_SUBTYPE__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ALLOCATION_FROM_SUBTYPE__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getSubpoolNameQ() {
		return subpoolNameQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSubpoolNameQ(ExpressionClass newSubpoolNameQ, NotificationChain msgs) {
		ExpressionClass oldSubpoolNameQ = subpoolNameQ;
		subpoolNameQ = newSubpoolNameQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ALLOCATION_FROM_SUBTYPE__SUBPOOL_NAME_Q, oldSubpoolNameQ, newSubpoolNameQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubpoolNameQ(ExpressionClass newSubpoolNameQ) {
		if (newSubpoolNameQ != subpoolNameQ) {
			NotificationChain msgs = null;
			if (subpoolNameQ != null)
				msgs = ((InternalEObject)subpoolNameQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ALLOCATION_FROM_SUBTYPE__SUBPOOL_NAME_Q, null, msgs);
			if (newSubpoolNameQ != null)
				msgs = ((InternalEObject)newSubpoolNameQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ALLOCATION_FROM_SUBTYPE__SUBPOOL_NAME_Q, null, msgs);
			msgs = basicSetSubpoolNameQ(newSubpoolNameQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ALLOCATION_FROM_SUBTYPE__SUBPOOL_NAME_Q, newSubpoolNameQ, newSubpoolNameQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementClass getAllocatorSubtypeIndicationQ() {
		return allocatorSubtypeIndicationQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAllocatorSubtypeIndicationQ(ElementClass newAllocatorSubtypeIndicationQ, NotificationChain msgs) {
		ElementClass oldAllocatorSubtypeIndicationQ = allocatorSubtypeIndicationQ;
		allocatorSubtypeIndicationQ = newAllocatorSubtypeIndicationQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ALLOCATION_FROM_SUBTYPE__ALLOCATOR_SUBTYPE_INDICATION_Q, oldAllocatorSubtypeIndicationQ, newAllocatorSubtypeIndicationQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAllocatorSubtypeIndicationQ(ElementClass newAllocatorSubtypeIndicationQ) {
		if (newAllocatorSubtypeIndicationQ != allocatorSubtypeIndicationQ) {
			NotificationChain msgs = null;
			if (allocatorSubtypeIndicationQ != null)
				msgs = ((InternalEObject)allocatorSubtypeIndicationQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ALLOCATION_FROM_SUBTYPE__ALLOCATOR_SUBTYPE_INDICATION_Q, null, msgs);
			if (newAllocatorSubtypeIndicationQ != null)
				msgs = ((InternalEObject)newAllocatorSubtypeIndicationQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ALLOCATION_FROM_SUBTYPE__ALLOCATOR_SUBTYPE_INDICATION_Q, null, msgs);
			msgs = basicSetAllocatorSubtypeIndicationQ(newAllocatorSubtypeIndicationQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ALLOCATION_FROM_SUBTYPE__ALLOCATOR_SUBTYPE_INDICATION_Q, newAllocatorSubtypeIndicationQ, newAllocatorSubtypeIndicationQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ALLOCATION_FROM_SUBTYPE__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ALLOCATION_FROM_SUBTYPE__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.ALLOCATION_FROM_SUBTYPE__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.ALLOCATION_FROM_SUBTYPE__SUBPOOL_NAME_Q:
				return basicSetSubpoolNameQ(null, msgs);
			case AdaPackage.ALLOCATION_FROM_SUBTYPE__ALLOCATOR_SUBTYPE_INDICATION_Q:
				return basicSetAllocatorSubtypeIndicationQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.ALLOCATION_FROM_SUBTYPE__SLOC:
				return getSloc();
			case AdaPackage.ALLOCATION_FROM_SUBTYPE__SUBPOOL_NAME_Q:
				return getSubpoolNameQ();
			case AdaPackage.ALLOCATION_FROM_SUBTYPE__ALLOCATOR_SUBTYPE_INDICATION_Q:
				return getAllocatorSubtypeIndicationQ();
			case AdaPackage.ALLOCATION_FROM_SUBTYPE__CHECKS:
				return getChecks();
			case AdaPackage.ALLOCATION_FROM_SUBTYPE__TYPE:
				return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.ALLOCATION_FROM_SUBTYPE__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.ALLOCATION_FROM_SUBTYPE__SUBPOOL_NAME_Q:
				setSubpoolNameQ((ExpressionClass)newValue);
				return;
			case AdaPackage.ALLOCATION_FROM_SUBTYPE__ALLOCATOR_SUBTYPE_INDICATION_Q:
				setAllocatorSubtypeIndicationQ((ElementClass)newValue);
				return;
			case AdaPackage.ALLOCATION_FROM_SUBTYPE__CHECKS:
				setChecks((String)newValue);
				return;
			case AdaPackage.ALLOCATION_FROM_SUBTYPE__TYPE:
				setType((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.ALLOCATION_FROM_SUBTYPE__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.ALLOCATION_FROM_SUBTYPE__SUBPOOL_NAME_Q:
				setSubpoolNameQ((ExpressionClass)null);
				return;
			case AdaPackage.ALLOCATION_FROM_SUBTYPE__ALLOCATOR_SUBTYPE_INDICATION_Q:
				setAllocatorSubtypeIndicationQ((ElementClass)null);
				return;
			case AdaPackage.ALLOCATION_FROM_SUBTYPE__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
			case AdaPackage.ALLOCATION_FROM_SUBTYPE__TYPE:
				setType(TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.ALLOCATION_FROM_SUBTYPE__SLOC:
				return sloc != null;
			case AdaPackage.ALLOCATION_FROM_SUBTYPE__SUBPOOL_NAME_Q:
				return subpoolNameQ != null;
			case AdaPackage.ALLOCATION_FROM_SUBTYPE__ALLOCATOR_SUBTYPE_INDICATION_Q:
				return allocatorSubtypeIndicationQ != null;
			case AdaPackage.ALLOCATION_FROM_SUBTYPE__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
			case AdaPackage.ALLOCATION_FROM_SUBTYPE__TYPE:
				return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(", type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //AllocationFromSubtypeImpl
