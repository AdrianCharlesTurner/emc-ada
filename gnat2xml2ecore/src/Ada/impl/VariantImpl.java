/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.ElementList;
import Ada.RecordComponentList;
import Ada.SourceLocation;
import Ada.Variant;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Variant</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.VariantImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.VariantImpl#getVariantChoicesQl <em>Variant Choices Ql</em>}</li>
 *   <li>{@link Ada.impl.VariantImpl#getRecordComponentsQl <em>Record Components Ql</em>}</li>
 *   <li>{@link Ada.impl.VariantImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VariantImpl extends MinimalEObjectImpl.Container implements Variant {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getVariantChoicesQl() <em>Variant Choices Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariantChoicesQl()
	 * @generated
	 * @ordered
	 */
	protected ElementList variantChoicesQl;

	/**
	 * The cached value of the '{@link #getRecordComponentsQl() <em>Record Components Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRecordComponentsQl()
	 * @generated
	 * @ordered
	 */
	protected RecordComponentList recordComponentsQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VariantImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getVariant();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.VARIANT__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.VARIANT__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.VARIANT__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.VARIANT__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementList getVariantChoicesQl() {
		return variantChoicesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVariantChoicesQl(ElementList newVariantChoicesQl, NotificationChain msgs) {
		ElementList oldVariantChoicesQl = variantChoicesQl;
		variantChoicesQl = newVariantChoicesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.VARIANT__VARIANT_CHOICES_QL, oldVariantChoicesQl, newVariantChoicesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVariantChoicesQl(ElementList newVariantChoicesQl) {
		if (newVariantChoicesQl != variantChoicesQl) {
			NotificationChain msgs = null;
			if (variantChoicesQl != null)
				msgs = ((InternalEObject)variantChoicesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.VARIANT__VARIANT_CHOICES_QL, null, msgs);
			if (newVariantChoicesQl != null)
				msgs = ((InternalEObject)newVariantChoicesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.VARIANT__VARIANT_CHOICES_QL, null, msgs);
			msgs = basicSetVariantChoicesQl(newVariantChoicesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.VARIANT__VARIANT_CHOICES_QL, newVariantChoicesQl, newVariantChoicesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordComponentList getRecordComponentsQl() {
		return recordComponentsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRecordComponentsQl(RecordComponentList newRecordComponentsQl, NotificationChain msgs) {
		RecordComponentList oldRecordComponentsQl = recordComponentsQl;
		recordComponentsQl = newRecordComponentsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.VARIANT__RECORD_COMPONENTS_QL, oldRecordComponentsQl, newRecordComponentsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRecordComponentsQl(RecordComponentList newRecordComponentsQl) {
		if (newRecordComponentsQl != recordComponentsQl) {
			NotificationChain msgs = null;
			if (recordComponentsQl != null)
				msgs = ((InternalEObject)recordComponentsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.VARIANT__RECORD_COMPONENTS_QL, null, msgs);
			if (newRecordComponentsQl != null)
				msgs = ((InternalEObject)newRecordComponentsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.VARIANT__RECORD_COMPONENTS_QL, null, msgs);
			msgs = basicSetRecordComponentsQl(newRecordComponentsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.VARIANT__RECORD_COMPONENTS_QL, newRecordComponentsQl, newRecordComponentsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.VARIANT__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.VARIANT__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.VARIANT__VARIANT_CHOICES_QL:
				return basicSetVariantChoicesQl(null, msgs);
			case AdaPackage.VARIANT__RECORD_COMPONENTS_QL:
				return basicSetRecordComponentsQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.VARIANT__SLOC:
				return getSloc();
			case AdaPackage.VARIANT__VARIANT_CHOICES_QL:
				return getVariantChoicesQl();
			case AdaPackage.VARIANT__RECORD_COMPONENTS_QL:
				return getRecordComponentsQl();
			case AdaPackage.VARIANT__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.VARIANT__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.VARIANT__VARIANT_CHOICES_QL:
				setVariantChoicesQl((ElementList)newValue);
				return;
			case AdaPackage.VARIANT__RECORD_COMPONENTS_QL:
				setRecordComponentsQl((RecordComponentList)newValue);
				return;
			case AdaPackage.VARIANT__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.VARIANT__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.VARIANT__VARIANT_CHOICES_QL:
				setVariantChoicesQl((ElementList)null);
				return;
			case AdaPackage.VARIANT__RECORD_COMPONENTS_QL:
				setRecordComponentsQl((RecordComponentList)null);
				return;
			case AdaPackage.VARIANT__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.VARIANT__SLOC:
				return sloc != null;
			case AdaPackage.VARIANT__VARIANT_CHOICES_QL:
				return variantChoicesQl != null;
			case AdaPackage.VARIANT__RECORD_COMPONENTS_QL:
				return recordComponentsQl != null;
			case AdaPackage.VARIANT__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //VariantImpl
