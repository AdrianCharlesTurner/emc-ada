/**
 */
package Ada.impl;

import Ada.AbortStatement;
import Ada.AbsOperator;
import Ada.AcceptStatement;
import Ada.AccessAttribute;
import Ada.AccessToConstant;
import Ada.AccessToFunction;
import Ada.AccessToProcedure;
import Ada.AccessToProtectedFunction;
import Ada.AccessToProtectedProcedure;
import Ada.AccessToVariable;
import Ada.AdaPackage;
import Ada.AddressAttribute;
import Ada.AdjacentAttribute;
import Ada.AftAttribute;
import Ada.AlignmentAttribute;
import Ada.AllCallsRemotePragma;
import Ada.AllocationFromQualifiedExpression;
import Ada.AllocationFromSubtype;
import Ada.AndOperator;
import Ada.AndThenShortCircuit;
import Ada.AnonymousAccessToConstant;
import Ada.AnonymousAccessToFunction;
import Ada.AnonymousAccessToProcedure;
import Ada.AnonymousAccessToProtectedFunction;
import Ada.AnonymousAccessToProtectedProcedure;
import Ada.AnonymousAccessToVariable;
import Ada.ArrayComponentAssociation;
import Ada.AspectSpecification;
import Ada.AssertPragma;
import Ada.AssertionPolicyPragma;
import Ada.AssignmentStatement;
import Ada.AsynchronousPragma;
import Ada.AsynchronousSelectStatement;
import Ada.AtClause;
import Ada.AtomicComponentsPragma;
import Ada.AtomicPragma;
import Ada.AttachHandlerPragma;
import Ada.AttributeDefinitionClause;
import Ada.BaseAttribute;
import Ada.BitOrderAttribute;
import Ada.BlockStatement;
import Ada.BodyVersionAttribute;
import Ada.BoxExpression;
import Ada.CallableAttribute;
import Ada.CallerAttribute;
import Ada.CaseExpression;
import Ada.CaseExpressionPath;
import Ada.CasePath;
import Ada.CaseStatement;
import Ada.CeilingAttribute;
import Ada.CharacterLiteral;
import Ada.ChoiceParameterSpecification;
import Ada.ClassAttribute;
import Ada.CodeStatement;
import Ada.Comment;
import Ada.ComponentClause;
import Ada.ComponentDeclaration;
import Ada.ComponentDefinition;
import Ada.ComponentSizeAttribute;
import Ada.ComposeAttribute;
import Ada.ConcatenateOperator;
import Ada.ConditionalEntryCallStatement;
import Ada.ConstantDeclaration;
import Ada.ConstrainedArrayDefinition;
import Ada.ConstrainedAttribute;
import Ada.ControlledPragma;
import Ada.ConventionPragma;
import Ada.CopySignAttribute;
import Ada.CountAttribute;
import Ada.CpuPragma;
import Ada.DecimalFixedPointDefinition;
import Ada.DefaultStoragePoolPragma;
import Ada.DeferredConstantDeclaration;
import Ada.DefiningAbsOperator;
import Ada.DefiningAndOperator;
import Ada.DefiningCharacterLiteral;
import Ada.DefiningConcatenateOperator;
import Ada.DefiningDivideOperator;
import Ada.DefiningEnumerationLiteral;
import Ada.DefiningEqualOperator;
import Ada.DefiningExpandedName;
import Ada.DefiningExponentiateOperator;
import Ada.DefiningGreaterThanOperator;
import Ada.DefiningGreaterThanOrEqualOperator;
import Ada.DefiningIdentifier;
import Ada.DefiningLessThanOperator;
import Ada.DefiningLessThanOrEqualOperator;
import Ada.DefiningMinusOperator;
import Ada.DefiningModOperator;
import Ada.DefiningMultiplyOperator;
import Ada.DefiningNotEqualOperator;
import Ada.DefiningNotOperator;
import Ada.DefiningOrOperator;
import Ada.DefiningPlusOperator;
import Ada.DefiningRemOperator;
import Ada.DefiningUnaryMinusOperator;
import Ada.DefiningUnaryPlusOperator;
import Ada.DefiningXorOperator;
import Ada.DefiniteAttribute;
import Ada.DelayRelativeStatement;
import Ada.DelayUntilStatement;
import Ada.DeltaAttribute;
import Ada.DeltaConstraint;
import Ada.DenormAttribute;
import Ada.DerivedRecordExtensionDefinition;
import Ada.DerivedTypeDefinition;
import Ada.DetectBlockingPragma;
import Ada.DigitsAttribute;
import Ada.DigitsConstraint;
import Ada.DiscardNamesPragma;
import Ada.DiscreteRangeAttributeReference;
import Ada.DiscreteRangeAttributeReferenceAsSubtypeDefinition;
import Ada.DiscreteSimpleExpressionRange;
import Ada.DiscreteSimpleExpressionRangeAsSubtypeDefinition;
import Ada.DiscreteSubtypeIndication;
import Ada.DiscreteSubtypeIndicationAsSubtypeDefinition;
import Ada.DiscriminantAssociation;
import Ada.DiscriminantConstraint;
import Ada.DiscriminantSpecification;
import Ada.DispatchingDomainPragma;
import Ada.DivideOperator;
import Ada.ElaborateAllPragma;
import Ada.ElaborateBodyPragma;
import Ada.ElaboratePragma;
import Ada.ElementIteratorSpecification;
import Ada.ElementList;
import Ada.ElseExpressionPath;
import Ada.ElsePath;
import Ada.ElsifExpressionPath;
import Ada.ElsifPath;
import Ada.EntryBodyDeclaration;
import Ada.EntryCallStatement;
import Ada.EntryDeclaration;
import Ada.EntryIndexSpecification;
import Ada.EnumerationLiteral;
import Ada.EnumerationLiteralSpecification;
import Ada.EnumerationRepresentationClause;
import Ada.EnumerationTypeDefinition;
import Ada.EqualOperator;
import Ada.ExceptionDeclaration;
import Ada.ExceptionHandler;
import Ada.ExceptionRenamingDeclaration;
import Ada.ExitStatement;
import Ada.ExplicitDereference;
import Ada.ExponentAttribute;
import Ada.ExponentiateOperator;
import Ada.ExportPragma;
import Ada.ExpressionFunctionDeclaration;
import Ada.ExtendedReturnStatement;
import Ada.ExtensionAggregate;
import Ada.ExternalTagAttribute;
import Ada.FirstAttribute;
import Ada.FirstBitAttribute;
import Ada.FloatingPointDefinition;
import Ada.FloorAttribute;
import Ada.ForAllQuantifiedExpression;
import Ada.ForLoopStatement;
import Ada.ForSomeQuantifiedExpression;
import Ada.ForeAttribute;
import Ada.FormalAccessToConstant;
import Ada.FormalAccessToFunction;
import Ada.FormalAccessToProcedure;
import Ada.FormalAccessToProtectedFunction;
import Ada.FormalAccessToProtectedProcedure;
import Ada.FormalAccessToVariable;
import Ada.FormalConstrainedArrayDefinition;
import Ada.FormalDecimalFixedPointDefinition;
import Ada.FormalDerivedTypeDefinition;
import Ada.FormalDiscreteTypeDefinition;
import Ada.FormalFloatingPointDefinition;
import Ada.FormalFunctionDeclaration;
import Ada.FormalIncompleteTypeDeclaration;
import Ada.FormalLimitedInterface;
import Ada.FormalModularTypeDefinition;
import Ada.FormalObjectDeclaration;
import Ada.FormalOrdinaryFixedPointDefinition;
import Ada.FormalOrdinaryInterface;
import Ada.FormalPackageDeclaration;
import Ada.FormalPackageDeclarationWithBox;
import Ada.FormalPoolSpecificAccessToVariable;
import Ada.FormalPrivateTypeDefinition;
import Ada.FormalProcedureDeclaration;
import Ada.FormalProtectedInterface;
import Ada.FormalSignedIntegerTypeDefinition;
import Ada.FormalSynchronizedInterface;
import Ada.FormalTaggedPrivateTypeDefinition;
import Ada.FormalTaskInterface;
import Ada.FormalTypeDeclaration;
import Ada.FormalUnconstrainedArrayDefinition;
import Ada.FractionAttribute;
import Ada.FunctionBodyDeclaration;
import Ada.FunctionBodyStub;
import Ada.FunctionCall;
import Ada.FunctionDeclaration;
import Ada.FunctionInstantiation;
import Ada.FunctionRenamingDeclaration;
import Ada.GeneralizedIteratorSpecification;
import Ada.GenericAssociation;
import Ada.GenericFunctionDeclaration;
import Ada.GenericFunctionRenamingDeclaration;
import Ada.GenericPackageDeclaration;
import Ada.GenericPackageRenamingDeclaration;
import Ada.GenericProcedureDeclaration;
import Ada.GenericProcedureRenamingDeclaration;
import Ada.GotoStatement;
import Ada.GreaterThanOperator;
import Ada.GreaterThanOrEqualOperator;
import Ada.Identifier;
import Ada.IdentityAttribute;
import Ada.IfExpression;
import Ada.IfExpressionPath;
import Ada.IfPath;
import Ada.IfStatement;
import Ada.ImageAttribute;
import Ada.ImplementationDefinedAttribute;
import Ada.ImplementationDefinedPragma;
import Ada.ImportPragma;
import Ada.InMembershipTest;
import Ada.IncompleteTypeDeclaration;
import Ada.IndependentComponentsPragma;
import Ada.IndependentPragma;
import Ada.IndexConstraint;
import Ada.IndexedComponent;
import Ada.InlinePragma;
import Ada.InputAttribute;
import Ada.InspectionPointPragma;
import Ada.IntegerLiteral;
import Ada.IntegerNumberDeclaration;
import Ada.InterruptHandlerPragma;
import Ada.InterruptPriorityPragma;
import Ada.KnownDiscriminantPart;
import Ada.LastAttribute;
import Ada.LastBitAttribute;
import Ada.LeadingPartAttribute;
import Ada.LengthAttribute;
import Ada.LessThanOperator;
import Ada.LessThanOrEqualOperator;
import Ada.LimitedInterface;
import Ada.LinkerOptionsPragma;
import Ada.ListPragma;
import Ada.LockingPolicyPragma;
import Ada.LoopParameterSpecification;
import Ada.LoopStatement;
import Ada.MachineAttribute;
import Ada.MachineEmaxAttribute;
import Ada.MachineEminAttribute;
import Ada.MachineMantissaAttribute;
import Ada.MachineOverflowsAttribute;
import Ada.MachineRadixAttribute;
import Ada.MachineRoundingAttribute;
import Ada.MachineRoundsAttribute;
import Ada.MaxAlignmentForAllocationAttribute;
import Ada.MaxAttribute;
import Ada.MaxSizeInStorageElementsAttribute;
import Ada.MinAttribute;
import Ada.MinusOperator;
import Ada.ModAttribute;
import Ada.ModOperator;
import Ada.ModelAttribute;
import Ada.ModelEminAttribute;
import Ada.ModelEpsilonAttribute;
import Ada.ModelMantissaAttribute;
import Ada.ModelSmallAttribute;
import Ada.ModularTypeDefinition;
import Ada.ModulusAttribute;
import Ada.MultiplyOperator;
import Ada.NamedArrayAggregate;
import Ada.NoReturnPragma;
import Ada.NormalizeScalarsPragma;
import Ada.NotAnElement;
import Ada.NotEqualOperator;
import Ada.NotInMembershipTest;
import Ada.NotOperator;
import Ada.NullComponent;
import Ada.NullLiteral;
import Ada.NullProcedureDeclaration;
import Ada.NullRecordDefinition;
import Ada.NullStatement;
import Ada.ObjectRenamingDeclaration;
import Ada.OptimizePragma;
import Ada.OrElseShortCircuit;
import Ada.OrOperator;
import Ada.OrPath;
import Ada.OrdinaryFixedPointDefinition;
import Ada.OrdinaryInterface;
import Ada.OrdinaryTypeDeclaration;
import Ada.OthersChoice;
import Ada.OutputAttribute;
import Ada.OverlapsStorageAttribute;
import Ada.PackPragma;
import Ada.PackageBodyDeclaration;
import Ada.PackageBodyStub;
import Ada.PackageDeclaration;
import Ada.PackageInstantiation;
import Ada.PackageRenamingDeclaration;
import Ada.PagePragma;
import Ada.ParameterAssociation;
import Ada.ParameterSpecification;
import Ada.ParenthesizedExpression;
import Ada.PartitionElaborationPolicyPragma;
import Ada.PartitionIdAttribute;
import Ada.PlusOperator;
import Ada.PoolSpecificAccessToVariable;
import Ada.PosAttribute;
import Ada.PositionAttribute;
import Ada.PositionalArrayAggregate;
import Ada.PragmaArgumentAssociation;
import Ada.PredAttribute;
import Ada.PreelaborableInitializationPragma;
import Ada.PreelaboratePragma;
import Ada.PriorityAttribute;
import Ada.PriorityPragma;
import Ada.PrioritySpecificDispatchingPragma;
import Ada.PrivateExtensionDeclaration;
import Ada.PrivateExtensionDefinition;
import Ada.PrivateTypeDeclaration;
import Ada.PrivateTypeDefinition;
import Ada.ProcedureBodyDeclaration;
import Ada.ProcedureBodyStub;
import Ada.ProcedureCallStatement;
import Ada.ProcedureDeclaration;
import Ada.ProcedureInstantiation;
import Ada.ProcedureRenamingDeclaration;
import Ada.ProfilePragma;
import Ada.ProtectedBodyDeclaration;
import Ada.ProtectedBodyStub;
import Ada.ProtectedDefinition;
import Ada.ProtectedInterface;
import Ada.ProtectedTypeDeclaration;
import Ada.PurePragma;
import Ada.QualifiedExpression;
import Ada.QueuingPolicyPragma;
import Ada.RaiseExpression;
import Ada.RaiseStatement;
import Ada.RangeAttribute;
import Ada.RangeAttributeReference;
import Ada.ReadAttribute;
import Ada.RealLiteral;
import Ada.RealNumberDeclaration;
import Ada.RecordAggregate;
import Ada.RecordComponentAssociation;
import Ada.RecordDefinition;
import Ada.RecordRepresentationClause;
import Ada.RecordTypeDefinition;
import Ada.RelativeDeadlinePragma;
import Ada.RemOperator;
import Ada.RemainderAttribute;
import Ada.RemoteCallInterfacePragma;
import Ada.RemoteTypesPragma;
import Ada.RequeueStatement;
import Ada.RequeueStatementWithAbort;
import Ada.RestrictionsPragma;
import Ada.ReturnConstantSpecification;
import Ada.ReturnStatement;
import Ada.ReturnVariableSpecification;
import Ada.ReviewablePragma;
import Ada.RootIntegerDefinition;
import Ada.RootRealDefinition;
import Ada.RoundAttribute;
import Ada.RoundingAttribute;
import Ada.SafeFirstAttribute;
import Ada.SafeLastAttribute;
import Ada.ScaleAttribute;
import Ada.ScalingAttribute;
import Ada.SelectPath;
import Ada.SelectedComponent;
import Ada.SelectiveAcceptStatement;
import Ada.SharedPassivePragma;
import Ada.SignedIntegerTypeDefinition;
import Ada.SignedZerosAttribute;
import Ada.SimpleExpressionRange;
import Ada.SingleProtectedDeclaration;
import Ada.SingleTaskDeclaration;
import Ada.SizeAttribute;
import Ada.Slice;
import Ada.SmallAttribute;
import Ada.StoragePoolAttribute;
import Ada.StorageSizeAttribute;
import Ada.StorageSizePragma;
import Ada.StreamSizeAttribute;
import Ada.StringLiteral;
import Ada.SubtypeDeclaration;
import Ada.SubtypeIndication;
import Ada.SuccAttribute;
import Ada.SuppressPragma;
import Ada.SynchronizedInterface;
import Ada.TagAttribute;
import Ada.TaggedIncompleteTypeDeclaration;
import Ada.TaggedPrivateTypeDefinition;
import Ada.TaggedRecordTypeDefinition;
import Ada.TaskBodyDeclaration;
import Ada.TaskBodyStub;
import Ada.TaskDefinition;
import Ada.TaskDispatchingPolicyPragma;
import Ada.TaskInterface;
import Ada.TaskTypeDeclaration;
import Ada.TerminateAlternativeStatement;
import Ada.TerminatedAttribute;
import Ada.ThenAbortPath;
import Ada.TimedEntryCallStatement;
import Ada.TruncationAttribute;
import Ada.TypeConversion;
import Ada.UnaryMinusOperator;
import Ada.UnaryPlusOperator;
import Ada.UnbiasedRoundingAttribute;
import Ada.UncheckedAccessAttribute;
import Ada.UncheckedUnionPragma;
import Ada.UnconstrainedArrayDefinition;
import Ada.UniversalFixedDefinition;
import Ada.UniversalIntegerDefinition;
import Ada.UniversalRealDefinition;
import Ada.UnknownAttribute;
import Ada.UnknownDiscriminantPart;
import Ada.UnknownPragma;
import Ada.UnsuppressPragma;
import Ada.UseAllTypeClause;
import Ada.UsePackageClause;
import Ada.UseTypeClause;
import Ada.ValAttribute;
import Ada.ValidAttribute;
import Ada.ValueAttribute;
import Ada.VariableDeclaration;
import Ada.Variant;
import Ada.VariantPart;
import Ada.VersionAttribute;
import Ada.VolatileComponentsPragma;
import Ada.VolatilePragma;
import Ada.WhileLoopStatement;
import Ada.WideImageAttribute;
import Ada.WideValueAttribute;
import Ada.WideWideImageAttribute;
import Ada.WideWideValueAttribute;
import Ada.WideWideWidthAttribute;
import Ada.WideWidthAttribute;
import Ada.WidthAttribute;
import Ada.WithClause;
import Ada.WriteAttribute;
import Ada.XorOperator;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Element List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.ElementListImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getNotAnElement <em>Not An Element</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiningIdentifier <em>Defining Identifier</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiningCharacterLiteral <em>Defining Character Literal</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiningEnumerationLiteral <em>Defining Enumeration Literal</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiningAndOperator <em>Defining And Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiningOrOperator <em>Defining Or Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiningXorOperator <em>Defining Xor Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiningEqualOperator <em>Defining Equal Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiningNotEqualOperator <em>Defining Not Equal Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiningLessThanOperator <em>Defining Less Than Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiningLessThanOrEqualOperator <em>Defining Less Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiningGreaterThanOperator <em>Defining Greater Than Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiningGreaterThanOrEqualOperator <em>Defining Greater Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiningPlusOperator <em>Defining Plus Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiningMinusOperator <em>Defining Minus Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiningConcatenateOperator <em>Defining Concatenate Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiningUnaryPlusOperator <em>Defining Unary Plus Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiningUnaryMinusOperator <em>Defining Unary Minus Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiningMultiplyOperator <em>Defining Multiply Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiningDivideOperator <em>Defining Divide Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiningModOperator <em>Defining Mod Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiningRemOperator <em>Defining Rem Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiningExponentiateOperator <em>Defining Exponentiate Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiningAbsOperator <em>Defining Abs Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiningNotOperator <em>Defining Not Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiningExpandedName <em>Defining Expanded Name</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getOrdinaryTypeDeclaration <em>Ordinary Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getTaskTypeDeclaration <em>Task Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getProtectedTypeDeclaration <em>Protected Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getIncompleteTypeDeclaration <em>Incomplete Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getTaggedIncompleteTypeDeclaration <em>Tagged Incomplete Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPrivateTypeDeclaration <em>Private Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPrivateExtensionDeclaration <em>Private Extension Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getSubtypeDeclaration <em>Subtype Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getVariableDeclaration <em>Variable Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getConstantDeclaration <em>Constant Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDeferredConstantDeclaration <em>Deferred Constant Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getSingleTaskDeclaration <em>Single Task Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getSingleProtectedDeclaration <em>Single Protected Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getIntegerNumberDeclaration <em>Integer Number Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getRealNumberDeclaration <em>Real Number Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getEnumerationLiteralSpecification <em>Enumeration Literal Specification</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDiscriminantSpecification <em>Discriminant Specification</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getComponentDeclaration <em>Component Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getLoopParameterSpecification <em>Loop Parameter Specification</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getGeneralizedIteratorSpecification <em>Generalized Iterator Specification</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getElementIteratorSpecification <em>Element Iterator Specification</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getProcedureDeclaration <em>Procedure Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFunctionDeclaration <em>Function Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getParameterSpecification <em>Parameter Specification</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getProcedureBodyDeclaration <em>Procedure Body Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFunctionBodyDeclaration <em>Function Body Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getReturnVariableSpecification <em>Return Variable Specification</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getReturnConstantSpecification <em>Return Constant Specification</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getNullProcedureDeclaration <em>Null Procedure Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getExpressionFunctionDeclaration <em>Expression Function Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPackageDeclaration <em>Package Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPackageBodyDeclaration <em>Package Body Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getObjectRenamingDeclaration <em>Object Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getExceptionRenamingDeclaration <em>Exception Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPackageRenamingDeclaration <em>Package Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getProcedureRenamingDeclaration <em>Procedure Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFunctionRenamingDeclaration <em>Function Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getGenericPackageRenamingDeclaration <em>Generic Package Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getGenericProcedureRenamingDeclaration <em>Generic Procedure Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getGenericFunctionRenamingDeclaration <em>Generic Function Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getTaskBodyDeclaration <em>Task Body Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getProtectedBodyDeclaration <em>Protected Body Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getEntryDeclaration <em>Entry Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getEntryBodyDeclaration <em>Entry Body Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getEntryIndexSpecification <em>Entry Index Specification</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getProcedureBodyStub <em>Procedure Body Stub</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFunctionBodyStub <em>Function Body Stub</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPackageBodyStub <em>Package Body Stub</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getTaskBodyStub <em>Task Body Stub</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getProtectedBodyStub <em>Protected Body Stub</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getExceptionDeclaration <em>Exception Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getChoiceParameterSpecification <em>Choice Parameter Specification</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getGenericProcedureDeclaration <em>Generic Procedure Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getGenericFunctionDeclaration <em>Generic Function Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getGenericPackageDeclaration <em>Generic Package Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPackageInstantiation <em>Package Instantiation</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getProcedureInstantiation <em>Procedure Instantiation</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFunctionInstantiation <em>Function Instantiation</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalObjectDeclaration <em>Formal Object Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalTypeDeclaration <em>Formal Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalIncompleteTypeDeclaration <em>Formal Incomplete Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalProcedureDeclaration <em>Formal Procedure Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalFunctionDeclaration <em>Formal Function Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalPackageDeclaration <em>Formal Package Declaration</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalPackageDeclarationWithBox <em>Formal Package Declaration With Box</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDerivedTypeDefinition <em>Derived Type Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDerivedRecordExtensionDefinition <em>Derived Record Extension Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getEnumerationTypeDefinition <em>Enumeration Type Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getSignedIntegerTypeDefinition <em>Signed Integer Type Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getModularTypeDefinition <em>Modular Type Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getRootIntegerDefinition <em>Root Integer Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getRootRealDefinition <em>Root Real Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getUniversalIntegerDefinition <em>Universal Integer Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getUniversalRealDefinition <em>Universal Real Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getUniversalFixedDefinition <em>Universal Fixed Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFloatingPointDefinition <em>Floating Point Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getOrdinaryFixedPointDefinition <em>Ordinary Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDecimalFixedPointDefinition <em>Decimal Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getUnconstrainedArrayDefinition <em>Unconstrained Array Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getConstrainedArrayDefinition <em>Constrained Array Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getRecordTypeDefinition <em>Record Type Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getTaggedRecordTypeDefinition <em>Tagged Record Type Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getOrdinaryInterface <em>Ordinary Interface</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getLimitedInterface <em>Limited Interface</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getTaskInterface <em>Task Interface</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getProtectedInterface <em>Protected Interface</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getSynchronizedInterface <em>Synchronized Interface</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPoolSpecificAccessToVariable <em>Pool Specific Access To Variable</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAccessToVariable <em>Access To Variable</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAccessToConstant <em>Access To Constant</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAccessToProcedure <em>Access To Procedure</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAccessToProtectedProcedure <em>Access To Protected Procedure</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAccessToFunction <em>Access To Function</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAccessToProtectedFunction <em>Access To Protected Function</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getSubtypeIndication <em>Subtype Indication</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getRangeAttributeReference <em>Range Attribute Reference</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getSimpleExpressionRange <em>Simple Expression Range</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDigitsConstraint <em>Digits Constraint</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDeltaConstraint <em>Delta Constraint</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getIndexConstraint <em>Index Constraint</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDiscriminantConstraint <em>Discriminant Constraint</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getComponentDefinition <em>Component Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDiscreteSubtypeIndicationAsSubtypeDefinition <em>Discrete Subtype Indication As Subtype Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDiscreteRangeAttributeReferenceAsSubtypeDefinition <em>Discrete Range Attribute Reference As Subtype Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDiscreteSimpleExpressionRangeAsSubtypeDefinition <em>Discrete Simple Expression Range As Subtype Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDiscreteSubtypeIndication <em>Discrete Subtype Indication</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDiscreteRangeAttributeReference <em>Discrete Range Attribute Reference</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDiscreteSimpleExpressionRange <em>Discrete Simple Expression Range</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getUnknownDiscriminantPart <em>Unknown Discriminant Part</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getKnownDiscriminantPart <em>Known Discriminant Part</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getRecordDefinition <em>Record Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getNullRecordDefinition <em>Null Record Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getNullComponent <em>Null Component</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getVariantPart <em>Variant Part</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getVariant <em>Variant</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getOthersChoice <em>Others Choice</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAnonymousAccessToVariable <em>Anonymous Access To Variable</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAnonymousAccessToConstant <em>Anonymous Access To Constant</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAnonymousAccessToProcedure <em>Anonymous Access To Procedure</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAnonymousAccessToProtectedProcedure <em>Anonymous Access To Protected Procedure</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAnonymousAccessToFunction <em>Anonymous Access To Function</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAnonymousAccessToProtectedFunction <em>Anonymous Access To Protected Function</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPrivateTypeDefinition <em>Private Type Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getTaggedPrivateTypeDefinition <em>Tagged Private Type Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPrivateExtensionDefinition <em>Private Extension Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getTaskDefinition <em>Task Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getProtectedDefinition <em>Protected Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalPrivateTypeDefinition <em>Formal Private Type Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalTaggedPrivateTypeDefinition <em>Formal Tagged Private Type Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalDerivedTypeDefinition <em>Formal Derived Type Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalDiscreteTypeDefinition <em>Formal Discrete Type Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalSignedIntegerTypeDefinition <em>Formal Signed Integer Type Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalModularTypeDefinition <em>Formal Modular Type Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalFloatingPointDefinition <em>Formal Floating Point Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalOrdinaryFixedPointDefinition <em>Formal Ordinary Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalDecimalFixedPointDefinition <em>Formal Decimal Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalOrdinaryInterface <em>Formal Ordinary Interface</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalLimitedInterface <em>Formal Limited Interface</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalTaskInterface <em>Formal Task Interface</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalProtectedInterface <em>Formal Protected Interface</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalSynchronizedInterface <em>Formal Synchronized Interface</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalUnconstrainedArrayDefinition <em>Formal Unconstrained Array Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalConstrainedArrayDefinition <em>Formal Constrained Array Definition</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalPoolSpecificAccessToVariable <em>Formal Pool Specific Access To Variable</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalAccessToVariable <em>Formal Access To Variable</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalAccessToConstant <em>Formal Access To Constant</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalAccessToProcedure <em>Formal Access To Procedure</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalAccessToProtectedProcedure <em>Formal Access To Protected Procedure</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalAccessToFunction <em>Formal Access To Function</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFormalAccessToProtectedFunction <em>Formal Access To Protected Function</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAspectSpecification <em>Aspect Specification</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getBoxExpression <em>Box Expression</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getIntegerLiteral <em>Integer Literal</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getRealLiteral <em>Real Literal</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getStringLiteral <em>String Literal</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAndOperator <em>And Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getOrOperator <em>Or Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getXorOperator <em>Xor Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getEqualOperator <em>Equal Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getNotEqualOperator <em>Not Equal Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getLessThanOperator <em>Less Than Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getLessThanOrEqualOperator <em>Less Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getGreaterThanOperator <em>Greater Than Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getGreaterThanOrEqualOperator <em>Greater Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPlusOperator <em>Plus Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getMinusOperator <em>Minus Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getConcatenateOperator <em>Concatenate Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getUnaryPlusOperator <em>Unary Plus Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getUnaryMinusOperator <em>Unary Minus Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getMultiplyOperator <em>Multiply Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDivideOperator <em>Divide Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getModOperator <em>Mod Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getRemOperator <em>Rem Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getExponentiateOperator <em>Exponentiate Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAbsOperator <em>Abs Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getNotOperator <em>Not Operator</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getCharacterLiteral <em>Character Literal</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getEnumerationLiteral <em>Enumeration Literal</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getExplicitDereference <em>Explicit Dereference</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFunctionCall <em>Function Call</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getIndexedComponent <em>Indexed Component</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getSlice <em>Slice</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getSelectedComponent <em>Selected Component</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAccessAttribute <em>Access Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAddressAttribute <em>Address Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAdjacentAttribute <em>Adjacent Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAftAttribute <em>Aft Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAlignmentAttribute <em>Alignment Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getBaseAttribute <em>Base Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getBitOrderAttribute <em>Bit Order Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getBodyVersionAttribute <em>Body Version Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getCallableAttribute <em>Callable Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getCallerAttribute <em>Caller Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getCeilingAttribute <em>Ceiling Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getClassAttribute <em>Class Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getComponentSizeAttribute <em>Component Size Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getComposeAttribute <em>Compose Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getConstrainedAttribute <em>Constrained Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getCopySignAttribute <em>Copy Sign Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getCountAttribute <em>Count Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefiniteAttribute <em>Definite Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDeltaAttribute <em>Delta Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDenormAttribute <em>Denorm Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDigitsAttribute <em>Digits Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getExponentAttribute <em>Exponent Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getExternalTagAttribute <em>External Tag Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFirstAttribute <em>First Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFirstBitAttribute <em>First Bit Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFloorAttribute <em>Floor Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getForeAttribute <em>Fore Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getFractionAttribute <em>Fraction Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getIdentityAttribute <em>Identity Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getImageAttribute <em>Image Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getInputAttribute <em>Input Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getLastAttribute <em>Last Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getLastBitAttribute <em>Last Bit Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getLeadingPartAttribute <em>Leading Part Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getLengthAttribute <em>Length Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getMachineAttribute <em>Machine Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getMachineEmaxAttribute <em>Machine Emax Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getMachineEminAttribute <em>Machine Emin Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getMachineMantissaAttribute <em>Machine Mantissa Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getMachineOverflowsAttribute <em>Machine Overflows Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getMachineRadixAttribute <em>Machine Radix Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getMachineRoundsAttribute <em>Machine Rounds Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getMaxAttribute <em>Max Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getMaxSizeInStorageElementsAttribute <em>Max Size In Storage Elements Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getMinAttribute <em>Min Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getModelAttribute <em>Model Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getModelEminAttribute <em>Model Emin Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getModelEpsilonAttribute <em>Model Epsilon Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getModelMantissaAttribute <em>Model Mantissa Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getModelSmallAttribute <em>Model Small Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getModulusAttribute <em>Modulus Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getOutputAttribute <em>Output Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPartitionIdAttribute <em>Partition Id Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPosAttribute <em>Pos Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPositionAttribute <em>Position Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPredAttribute <em>Pred Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getRangeAttribute <em>Range Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getReadAttribute <em>Read Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getRemainderAttribute <em>Remainder Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getRoundAttribute <em>Round Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getRoundingAttribute <em>Rounding Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getSafeFirstAttribute <em>Safe First Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getSafeLastAttribute <em>Safe Last Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getScaleAttribute <em>Scale Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getScalingAttribute <em>Scaling Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getSignedZerosAttribute <em>Signed Zeros Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getSizeAttribute <em>Size Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getSmallAttribute <em>Small Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getStoragePoolAttribute <em>Storage Pool Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getStorageSizeAttribute <em>Storage Size Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getSuccAttribute <em>Succ Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getTagAttribute <em>Tag Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getTerminatedAttribute <em>Terminated Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getTruncationAttribute <em>Truncation Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getUnbiasedRoundingAttribute <em>Unbiased Rounding Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getUncheckedAccessAttribute <em>Unchecked Access Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getValAttribute <em>Val Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getValidAttribute <em>Valid Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getValueAttribute <em>Value Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getVersionAttribute <em>Version Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getWideImageAttribute <em>Wide Image Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getWideValueAttribute <em>Wide Value Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getWideWidthAttribute <em>Wide Width Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getWidthAttribute <em>Width Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getWriteAttribute <em>Write Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getMachineRoundingAttribute <em>Machine Rounding Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getModAttribute <em>Mod Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPriorityAttribute <em>Priority Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getStreamSizeAttribute <em>Stream Size Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getWideWideImageAttribute <em>Wide Wide Image Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getWideWideValueAttribute <em>Wide Wide Value Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getWideWideWidthAttribute <em>Wide Wide Width Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getMaxAlignmentForAllocationAttribute <em>Max Alignment For Allocation Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getOverlapsStorageAttribute <em>Overlaps Storage Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getImplementationDefinedAttribute <em>Implementation Defined Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getUnknownAttribute <em>Unknown Attribute</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getRecordAggregate <em>Record Aggregate</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getExtensionAggregate <em>Extension Aggregate</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPositionalArrayAggregate <em>Positional Array Aggregate</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getNamedArrayAggregate <em>Named Array Aggregate</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAndThenShortCircuit <em>And Then Short Circuit</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getOrElseShortCircuit <em>Or Else Short Circuit</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getInMembershipTest <em>In Membership Test</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getNotInMembershipTest <em>Not In Membership Test</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getNullLiteral <em>Null Literal</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getParenthesizedExpression <em>Parenthesized Expression</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getRaiseExpression <em>Raise Expression</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getTypeConversion <em>Type Conversion</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getQualifiedExpression <em>Qualified Expression</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAllocationFromSubtype <em>Allocation From Subtype</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAllocationFromQualifiedExpression <em>Allocation From Qualified Expression</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getCaseExpression <em>Case Expression</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getIfExpression <em>If Expression</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getForAllQuantifiedExpression <em>For All Quantified Expression</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getForSomeQuantifiedExpression <em>For Some Quantified Expression</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPragmaArgumentAssociation <em>Pragma Argument Association</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDiscriminantAssociation <em>Discriminant Association</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getRecordComponentAssociation <em>Record Component Association</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getArrayComponentAssociation <em>Array Component Association</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getParameterAssociation <em>Parameter Association</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getGenericAssociation <em>Generic Association</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getNullStatement <em>Null Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAssignmentStatement <em>Assignment Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getIfStatement <em>If Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getCaseStatement <em>Case Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getLoopStatement <em>Loop Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getWhileLoopStatement <em>While Loop Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getForLoopStatement <em>For Loop Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getBlockStatement <em>Block Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getExitStatement <em>Exit Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getGotoStatement <em>Goto Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getProcedureCallStatement <em>Procedure Call Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getReturnStatement <em>Return Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getExtendedReturnStatement <em>Extended Return Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAcceptStatement <em>Accept Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getEntryCallStatement <em>Entry Call Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getRequeueStatement <em>Requeue Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getRequeueStatementWithAbort <em>Requeue Statement With Abort</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDelayUntilStatement <em>Delay Until Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDelayRelativeStatement <em>Delay Relative Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getTerminateAlternativeStatement <em>Terminate Alternative Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getSelectiveAcceptStatement <em>Selective Accept Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getTimedEntryCallStatement <em>Timed Entry Call Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getConditionalEntryCallStatement <em>Conditional Entry Call Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAsynchronousSelectStatement <em>Asynchronous Select Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAbortStatement <em>Abort Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getRaiseStatement <em>Raise Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getCodeStatement <em>Code Statement</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getIfPath <em>If Path</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getElsifPath <em>Elsif Path</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getElsePath <em>Else Path</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getCasePath <em>Case Path</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getSelectPath <em>Select Path</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getOrPath <em>Or Path</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getThenAbortPath <em>Then Abort Path</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getCaseExpressionPath <em>Case Expression Path</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getIfExpressionPath <em>If Expression Path</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getElsifExpressionPath <em>Elsif Expression Path</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getElseExpressionPath <em>Else Expression Path</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getUsePackageClause <em>Use Package Clause</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getUseTypeClause <em>Use Type Clause</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getUseAllTypeClause <em>Use All Type Clause</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getWithClause <em>With Clause</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAttributeDefinitionClause <em>Attribute Definition Clause</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getEnumerationRepresentationClause <em>Enumeration Representation Clause</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getRecordRepresentationClause <em>Record Representation Clause</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAtClause <em>At Clause</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getComponentClause <em>Component Clause</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getExceptionHandler <em>Exception Handler</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAsynchronousPragma <em>Asynchronous Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAtomicPragma <em>Atomic Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAttachHandlerPragma <em>Attach Handler Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getControlledPragma <em>Controlled Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getConventionPragma <em>Convention Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDiscardNamesPragma <em>Discard Names Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getElaboratePragma <em>Elaborate Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getElaborateAllPragma <em>Elaborate All Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getExportPragma <em>Export Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getImportPragma <em>Import Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getInlinePragma <em>Inline Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getInspectionPointPragma <em>Inspection Point Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getLinkerOptionsPragma <em>Linker Options Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getListPragma <em>List Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getLockingPolicyPragma <em>Locking Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getOptimizePragma <em>Optimize Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPackPragma <em>Pack Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPagePragma <em>Page Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPreelaboratePragma <em>Preelaborate Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPriorityPragma <em>Priority Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPurePragma <em>Pure Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getRemoteTypesPragma <em>Remote Types Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getRestrictionsPragma <em>Restrictions Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getReviewablePragma <em>Reviewable Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getSharedPassivePragma <em>Shared Passive Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getStorageSizePragma <em>Storage Size Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getSuppressPragma <em>Suppress Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getVolatilePragma <em>Volatile Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAssertPragma <em>Assert Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getNoReturnPragma <em>No Return Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getProfilePragma <em>Profile Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getUnsuppressPragma <em>Unsuppress Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getCpuPragma <em>Cpu Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getIndependentPragma <em>Independent Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getIndependentComponentsPragma <em>Independent Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}</li>
 *   <li>{@link Ada.impl.ElementListImpl#getUnknownPragma <em>Unknown Pragma</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ElementListImpl extends MinimalEObjectImpl.Container implements ElementList {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ElementListImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getElementList();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, AdaPackage.ELEMENT_LIST__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NotAnElement> getNotAnElement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_NotAnElement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningIdentifier> getDefiningIdentifier() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiningIdentifier());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningCharacterLiteral> getDefiningCharacterLiteral() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiningCharacterLiteral());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningEnumerationLiteral> getDefiningEnumerationLiteral() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiningEnumerationLiteral());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningAndOperator> getDefiningAndOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiningAndOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningOrOperator> getDefiningOrOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiningOrOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningXorOperator> getDefiningXorOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiningXorOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningEqualOperator> getDefiningEqualOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiningEqualOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningNotEqualOperator> getDefiningNotEqualOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiningNotEqualOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningLessThanOperator> getDefiningLessThanOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiningLessThanOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningLessThanOrEqualOperator> getDefiningLessThanOrEqualOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiningLessThanOrEqualOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningGreaterThanOperator> getDefiningGreaterThanOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiningGreaterThanOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningGreaterThanOrEqualOperator> getDefiningGreaterThanOrEqualOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiningGreaterThanOrEqualOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningPlusOperator> getDefiningPlusOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiningPlusOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningMinusOperator> getDefiningMinusOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiningMinusOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningConcatenateOperator> getDefiningConcatenateOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiningConcatenateOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningUnaryPlusOperator> getDefiningUnaryPlusOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiningUnaryPlusOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningUnaryMinusOperator> getDefiningUnaryMinusOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiningUnaryMinusOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningMultiplyOperator> getDefiningMultiplyOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiningMultiplyOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningDivideOperator> getDefiningDivideOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiningDivideOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningModOperator> getDefiningModOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiningModOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningRemOperator> getDefiningRemOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiningRemOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningExponentiateOperator> getDefiningExponentiateOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiningExponentiateOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningAbsOperator> getDefiningAbsOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiningAbsOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningNotOperator> getDefiningNotOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiningNotOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningExpandedName> getDefiningExpandedName() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiningExpandedName());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OrdinaryTypeDeclaration> getOrdinaryTypeDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_OrdinaryTypeDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskTypeDeclaration> getTaskTypeDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_TaskTypeDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProtectedTypeDeclaration> getProtectedTypeDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ProtectedTypeDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IncompleteTypeDeclaration> getIncompleteTypeDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_IncompleteTypeDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaggedIncompleteTypeDeclaration> getTaggedIncompleteTypeDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_TaggedIncompleteTypeDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PrivateTypeDeclaration> getPrivateTypeDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PrivateTypeDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PrivateExtensionDeclaration> getPrivateExtensionDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PrivateExtensionDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SubtypeDeclaration> getSubtypeDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_SubtypeDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VariableDeclaration> getVariableDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_VariableDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConstantDeclaration> getConstantDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ConstantDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DeferredConstantDeclaration> getDeferredConstantDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DeferredConstantDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SingleTaskDeclaration> getSingleTaskDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_SingleTaskDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SingleProtectedDeclaration> getSingleProtectedDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_SingleProtectedDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IntegerNumberDeclaration> getIntegerNumberDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_IntegerNumberDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RealNumberDeclaration> getRealNumberDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_RealNumberDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EnumerationLiteralSpecification> getEnumerationLiteralSpecification() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_EnumerationLiteralSpecification());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscriminantSpecification> getDiscriminantSpecification() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DiscriminantSpecification());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComponentDeclaration> getComponentDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ComponentDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LoopParameterSpecification> getLoopParameterSpecification() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_LoopParameterSpecification());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GeneralizedIteratorSpecification> getGeneralizedIteratorSpecification() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_GeneralizedIteratorSpecification());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElementIteratorSpecification> getElementIteratorSpecification() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ElementIteratorSpecification());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcedureDeclaration> getProcedureDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ProcedureDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FunctionDeclaration> getFunctionDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FunctionDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ParameterSpecification> getParameterSpecification() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ParameterSpecification());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcedureBodyDeclaration> getProcedureBodyDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ProcedureBodyDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FunctionBodyDeclaration> getFunctionBodyDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FunctionBodyDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReturnVariableSpecification> getReturnVariableSpecification() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ReturnVariableSpecification());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReturnConstantSpecification> getReturnConstantSpecification() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ReturnConstantSpecification());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NullProcedureDeclaration> getNullProcedureDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_NullProcedureDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExpressionFunctionDeclaration> getExpressionFunctionDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ExpressionFunctionDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackageDeclaration> getPackageDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PackageDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackageBodyDeclaration> getPackageBodyDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PackageBodyDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObjectRenamingDeclaration> getObjectRenamingDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ObjectRenamingDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExceptionRenamingDeclaration> getExceptionRenamingDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ExceptionRenamingDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackageRenamingDeclaration> getPackageRenamingDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PackageRenamingDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcedureRenamingDeclaration> getProcedureRenamingDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ProcedureRenamingDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FunctionRenamingDeclaration> getFunctionRenamingDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FunctionRenamingDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GenericPackageRenamingDeclaration> getGenericPackageRenamingDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_GenericPackageRenamingDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GenericProcedureRenamingDeclaration> getGenericProcedureRenamingDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_GenericProcedureRenamingDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GenericFunctionRenamingDeclaration> getGenericFunctionRenamingDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_GenericFunctionRenamingDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskBodyDeclaration> getTaskBodyDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_TaskBodyDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProtectedBodyDeclaration> getProtectedBodyDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ProtectedBodyDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EntryDeclaration> getEntryDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_EntryDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EntryBodyDeclaration> getEntryBodyDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_EntryBodyDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EntryIndexSpecification> getEntryIndexSpecification() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_EntryIndexSpecification());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcedureBodyStub> getProcedureBodyStub() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ProcedureBodyStub());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FunctionBodyStub> getFunctionBodyStub() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FunctionBodyStub());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackageBodyStub> getPackageBodyStub() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PackageBodyStub());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskBodyStub> getTaskBodyStub() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_TaskBodyStub());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProtectedBodyStub> getProtectedBodyStub() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ProtectedBodyStub());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExceptionDeclaration> getExceptionDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ExceptionDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ChoiceParameterSpecification> getChoiceParameterSpecification() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ChoiceParameterSpecification());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GenericProcedureDeclaration> getGenericProcedureDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_GenericProcedureDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GenericFunctionDeclaration> getGenericFunctionDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_GenericFunctionDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GenericPackageDeclaration> getGenericPackageDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_GenericPackageDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackageInstantiation> getPackageInstantiation() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PackageInstantiation());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcedureInstantiation> getProcedureInstantiation() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ProcedureInstantiation());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FunctionInstantiation> getFunctionInstantiation() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FunctionInstantiation());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalObjectDeclaration> getFormalObjectDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalObjectDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalTypeDeclaration> getFormalTypeDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalTypeDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalIncompleteTypeDeclaration> getFormalIncompleteTypeDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalIncompleteTypeDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalProcedureDeclaration> getFormalProcedureDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalProcedureDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalFunctionDeclaration> getFormalFunctionDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalFunctionDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalPackageDeclaration> getFormalPackageDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalPackageDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalPackageDeclarationWithBox> getFormalPackageDeclarationWithBox() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalPackageDeclarationWithBox());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DerivedTypeDefinition> getDerivedTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DerivedTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DerivedRecordExtensionDefinition> getDerivedRecordExtensionDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DerivedRecordExtensionDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EnumerationTypeDefinition> getEnumerationTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_EnumerationTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SignedIntegerTypeDefinition> getSignedIntegerTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_SignedIntegerTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModularTypeDefinition> getModularTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ModularTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RootIntegerDefinition> getRootIntegerDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_RootIntegerDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RootRealDefinition> getRootRealDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_RootRealDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UniversalIntegerDefinition> getUniversalIntegerDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_UniversalIntegerDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UniversalRealDefinition> getUniversalRealDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_UniversalRealDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UniversalFixedDefinition> getUniversalFixedDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_UniversalFixedDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FloatingPointDefinition> getFloatingPointDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FloatingPointDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OrdinaryFixedPointDefinition> getOrdinaryFixedPointDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_OrdinaryFixedPointDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DecimalFixedPointDefinition> getDecimalFixedPointDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DecimalFixedPointDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnconstrainedArrayDefinition> getUnconstrainedArrayDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_UnconstrainedArrayDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConstrainedArrayDefinition> getConstrainedArrayDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ConstrainedArrayDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RecordTypeDefinition> getRecordTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_RecordTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaggedRecordTypeDefinition> getTaggedRecordTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_TaggedRecordTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OrdinaryInterface> getOrdinaryInterface() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_OrdinaryInterface());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LimitedInterface> getLimitedInterface() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_LimitedInterface());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskInterface> getTaskInterface() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_TaskInterface());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProtectedInterface> getProtectedInterface() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ProtectedInterface());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SynchronizedInterface> getSynchronizedInterface() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_SynchronizedInterface());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PoolSpecificAccessToVariable> getPoolSpecificAccessToVariable() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PoolSpecificAccessToVariable());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AccessToVariable> getAccessToVariable() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AccessToVariable());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AccessToConstant> getAccessToConstant() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AccessToConstant());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AccessToProcedure> getAccessToProcedure() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AccessToProcedure());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AccessToProtectedProcedure> getAccessToProtectedProcedure() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AccessToProtectedProcedure());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AccessToFunction> getAccessToFunction() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AccessToFunction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AccessToProtectedFunction> getAccessToProtectedFunction() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AccessToProtectedFunction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SubtypeIndication> getSubtypeIndication() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_SubtypeIndication());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RangeAttributeReference> getRangeAttributeReference() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_RangeAttributeReference());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SimpleExpressionRange> getSimpleExpressionRange() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_SimpleExpressionRange());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DigitsConstraint> getDigitsConstraint() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DigitsConstraint());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DeltaConstraint> getDeltaConstraint() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DeltaConstraint());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IndexConstraint> getIndexConstraint() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_IndexConstraint());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscriminantConstraint> getDiscriminantConstraint() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DiscriminantConstraint());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComponentDefinition> getComponentDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ComponentDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscreteSubtypeIndicationAsSubtypeDefinition> getDiscreteSubtypeIndicationAsSubtypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DiscreteSubtypeIndicationAsSubtypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscreteRangeAttributeReferenceAsSubtypeDefinition> getDiscreteRangeAttributeReferenceAsSubtypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DiscreteRangeAttributeReferenceAsSubtypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscreteSimpleExpressionRangeAsSubtypeDefinition> getDiscreteSimpleExpressionRangeAsSubtypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DiscreteSimpleExpressionRangeAsSubtypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscreteSubtypeIndication> getDiscreteSubtypeIndication() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DiscreteSubtypeIndication());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscreteRangeAttributeReference> getDiscreteRangeAttributeReference() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DiscreteRangeAttributeReference());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscreteSimpleExpressionRange> getDiscreteSimpleExpressionRange() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DiscreteSimpleExpressionRange());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnknownDiscriminantPart> getUnknownDiscriminantPart() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_UnknownDiscriminantPart());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<KnownDiscriminantPart> getKnownDiscriminantPart() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_KnownDiscriminantPart());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RecordDefinition> getRecordDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_RecordDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NullRecordDefinition> getNullRecordDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_NullRecordDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NullComponent> getNullComponent() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_NullComponent());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VariantPart> getVariantPart() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_VariantPart());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Variant> getVariant() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_Variant());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OthersChoice> getOthersChoice() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_OthersChoice());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AnonymousAccessToVariable> getAnonymousAccessToVariable() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AnonymousAccessToVariable());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AnonymousAccessToConstant> getAnonymousAccessToConstant() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AnonymousAccessToConstant());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AnonymousAccessToProcedure> getAnonymousAccessToProcedure() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AnonymousAccessToProcedure());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AnonymousAccessToProtectedProcedure> getAnonymousAccessToProtectedProcedure() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AnonymousAccessToProtectedProcedure());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AnonymousAccessToFunction> getAnonymousAccessToFunction() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AnonymousAccessToFunction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AnonymousAccessToProtectedFunction> getAnonymousAccessToProtectedFunction() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AnonymousAccessToProtectedFunction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PrivateTypeDefinition> getPrivateTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PrivateTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaggedPrivateTypeDefinition> getTaggedPrivateTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_TaggedPrivateTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PrivateExtensionDefinition> getPrivateExtensionDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PrivateExtensionDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskDefinition> getTaskDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_TaskDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProtectedDefinition> getProtectedDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ProtectedDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalPrivateTypeDefinition> getFormalPrivateTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalPrivateTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalTaggedPrivateTypeDefinition> getFormalTaggedPrivateTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalTaggedPrivateTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalDerivedTypeDefinition> getFormalDerivedTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalDerivedTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalDiscreteTypeDefinition> getFormalDiscreteTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalDiscreteTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalSignedIntegerTypeDefinition> getFormalSignedIntegerTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalSignedIntegerTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalModularTypeDefinition> getFormalModularTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalModularTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalFloatingPointDefinition> getFormalFloatingPointDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalFloatingPointDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalOrdinaryFixedPointDefinition> getFormalOrdinaryFixedPointDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalOrdinaryFixedPointDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalDecimalFixedPointDefinition> getFormalDecimalFixedPointDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalDecimalFixedPointDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalOrdinaryInterface> getFormalOrdinaryInterface() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalOrdinaryInterface());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalLimitedInterface> getFormalLimitedInterface() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalLimitedInterface());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalTaskInterface> getFormalTaskInterface() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalTaskInterface());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalProtectedInterface> getFormalProtectedInterface() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalProtectedInterface());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalSynchronizedInterface> getFormalSynchronizedInterface() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalSynchronizedInterface());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalUnconstrainedArrayDefinition> getFormalUnconstrainedArrayDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalUnconstrainedArrayDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalConstrainedArrayDefinition> getFormalConstrainedArrayDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalConstrainedArrayDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalPoolSpecificAccessToVariable> getFormalPoolSpecificAccessToVariable() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalPoolSpecificAccessToVariable());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalAccessToVariable> getFormalAccessToVariable() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalAccessToVariable());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalAccessToConstant> getFormalAccessToConstant() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalAccessToConstant());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalAccessToProcedure> getFormalAccessToProcedure() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalAccessToProcedure());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalAccessToProtectedProcedure> getFormalAccessToProtectedProcedure() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalAccessToProtectedProcedure());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalAccessToFunction> getFormalAccessToFunction() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalAccessToFunction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalAccessToProtectedFunction> getFormalAccessToProtectedFunction() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FormalAccessToProtectedFunction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AspectSpecification> getAspectSpecification() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AspectSpecification());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BoxExpression> getBoxExpression() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_BoxExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IntegerLiteral> getIntegerLiteral() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_IntegerLiteral());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RealLiteral> getRealLiteral() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_RealLiteral());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StringLiteral> getStringLiteral() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_StringLiteral());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Identifier> getIdentifier() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_Identifier());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AndOperator> getAndOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AndOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OrOperator> getOrOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_OrOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<XorOperator> getXorOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_XorOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EqualOperator> getEqualOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_EqualOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NotEqualOperator> getNotEqualOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_NotEqualOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LessThanOperator> getLessThanOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_LessThanOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LessThanOrEqualOperator> getLessThanOrEqualOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_LessThanOrEqualOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GreaterThanOperator> getGreaterThanOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_GreaterThanOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GreaterThanOrEqualOperator> getGreaterThanOrEqualOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_GreaterThanOrEqualOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PlusOperator> getPlusOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PlusOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MinusOperator> getMinusOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_MinusOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConcatenateOperator> getConcatenateOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ConcatenateOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnaryPlusOperator> getUnaryPlusOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_UnaryPlusOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnaryMinusOperator> getUnaryMinusOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_UnaryMinusOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MultiplyOperator> getMultiplyOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_MultiplyOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DivideOperator> getDivideOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DivideOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModOperator> getModOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ModOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RemOperator> getRemOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_RemOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExponentiateOperator> getExponentiateOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ExponentiateOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AbsOperator> getAbsOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AbsOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NotOperator> getNotOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_NotOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CharacterLiteral> getCharacterLiteral() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_CharacterLiteral());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EnumerationLiteral> getEnumerationLiteral() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_EnumerationLiteral());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExplicitDereference> getExplicitDereference() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ExplicitDereference());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FunctionCall> getFunctionCall() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FunctionCall());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IndexedComponent> getIndexedComponent() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_IndexedComponent());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Slice> getSlice() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_Slice());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SelectedComponent> getSelectedComponent() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_SelectedComponent());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AccessAttribute> getAccessAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AccessAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AddressAttribute> getAddressAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AddressAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AdjacentAttribute> getAdjacentAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AdjacentAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AftAttribute> getAftAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AftAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AlignmentAttribute> getAlignmentAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AlignmentAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BaseAttribute> getBaseAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_BaseAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BitOrderAttribute> getBitOrderAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_BitOrderAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BodyVersionAttribute> getBodyVersionAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_BodyVersionAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CallableAttribute> getCallableAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_CallableAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CallerAttribute> getCallerAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_CallerAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CeilingAttribute> getCeilingAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_CeilingAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ClassAttribute> getClassAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ClassAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComponentSizeAttribute> getComponentSizeAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ComponentSizeAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComposeAttribute> getComposeAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ComposeAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConstrainedAttribute> getConstrainedAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ConstrainedAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CopySignAttribute> getCopySignAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_CopySignAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CountAttribute> getCountAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_CountAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiniteAttribute> getDefiniteAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefiniteAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DeltaAttribute> getDeltaAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DeltaAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DenormAttribute> getDenormAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DenormAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DigitsAttribute> getDigitsAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DigitsAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExponentAttribute> getExponentAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ExponentAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExternalTagAttribute> getExternalTagAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ExternalTagAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FirstAttribute> getFirstAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FirstAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FirstBitAttribute> getFirstBitAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FirstBitAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FloorAttribute> getFloorAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FloorAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ForeAttribute> getForeAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ForeAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FractionAttribute> getFractionAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_FractionAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IdentityAttribute> getIdentityAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_IdentityAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ImageAttribute> getImageAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ImageAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InputAttribute> getInputAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_InputAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LastAttribute> getLastAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_LastAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LastBitAttribute> getLastBitAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_LastBitAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LeadingPartAttribute> getLeadingPartAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_LeadingPartAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LengthAttribute> getLengthAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_LengthAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MachineAttribute> getMachineAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_MachineAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MachineEmaxAttribute> getMachineEmaxAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_MachineEmaxAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MachineEminAttribute> getMachineEminAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_MachineEminAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MachineMantissaAttribute> getMachineMantissaAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_MachineMantissaAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MachineOverflowsAttribute> getMachineOverflowsAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_MachineOverflowsAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MachineRadixAttribute> getMachineRadixAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_MachineRadixAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MachineRoundsAttribute> getMachineRoundsAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_MachineRoundsAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaxAttribute> getMaxAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_MaxAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaxSizeInStorageElementsAttribute> getMaxSizeInStorageElementsAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_MaxSizeInStorageElementsAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MinAttribute> getMinAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_MinAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModelAttribute> getModelAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ModelAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModelEminAttribute> getModelEminAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ModelEminAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModelEpsilonAttribute> getModelEpsilonAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ModelEpsilonAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModelMantissaAttribute> getModelMantissaAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ModelMantissaAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModelSmallAttribute> getModelSmallAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ModelSmallAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModulusAttribute> getModulusAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ModulusAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OutputAttribute> getOutputAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_OutputAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PartitionIdAttribute> getPartitionIdAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PartitionIdAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PosAttribute> getPosAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PosAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PositionAttribute> getPositionAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PositionAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PredAttribute> getPredAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PredAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RangeAttribute> getRangeAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_RangeAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReadAttribute> getReadAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ReadAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RemainderAttribute> getRemainderAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_RemainderAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RoundAttribute> getRoundAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_RoundAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RoundingAttribute> getRoundingAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_RoundingAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SafeFirstAttribute> getSafeFirstAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_SafeFirstAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SafeLastAttribute> getSafeLastAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_SafeLastAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScaleAttribute> getScaleAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ScaleAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScalingAttribute> getScalingAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ScalingAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SignedZerosAttribute> getSignedZerosAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_SignedZerosAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SizeAttribute> getSizeAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_SizeAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SmallAttribute> getSmallAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_SmallAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StoragePoolAttribute> getStoragePoolAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_StoragePoolAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StorageSizeAttribute> getStorageSizeAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_StorageSizeAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SuccAttribute> getSuccAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_SuccAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TagAttribute> getTagAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_TagAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TerminatedAttribute> getTerminatedAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_TerminatedAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TruncationAttribute> getTruncationAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_TruncationAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnbiasedRoundingAttribute> getUnbiasedRoundingAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_UnbiasedRoundingAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UncheckedAccessAttribute> getUncheckedAccessAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_UncheckedAccessAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ValAttribute> getValAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ValAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ValidAttribute> getValidAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ValidAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ValueAttribute> getValueAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ValueAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VersionAttribute> getVersionAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_VersionAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<WideImageAttribute> getWideImageAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_WideImageAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<WideValueAttribute> getWideValueAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_WideValueAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<WideWidthAttribute> getWideWidthAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_WideWidthAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<WidthAttribute> getWidthAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_WidthAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<WriteAttribute> getWriteAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_WriteAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MachineRoundingAttribute> getMachineRoundingAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_MachineRoundingAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModAttribute> getModAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ModAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PriorityAttribute> getPriorityAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PriorityAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StreamSizeAttribute> getStreamSizeAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_StreamSizeAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<WideWideImageAttribute> getWideWideImageAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_WideWideImageAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<WideWideValueAttribute> getWideWideValueAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_WideWideValueAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<WideWideWidthAttribute> getWideWideWidthAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_WideWideWidthAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaxAlignmentForAllocationAttribute> getMaxAlignmentForAllocationAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_MaxAlignmentForAllocationAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OverlapsStorageAttribute> getOverlapsStorageAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_OverlapsStorageAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ImplementationDefinedAttribute> getImplementationDefinedAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ImplementationDefinedAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnknownAttribute> getUnknownAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_UnknownAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RecordAggregate> getRecordAggregate() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_RecordAggregate());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExtensionAggregate> getExtensionAggregate() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ExtensionAggregate());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PositionalArrayAggregate> getPositionalArrayAggregate() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PositionalArrayAggregate());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NamedArrayAggregate> getNamedArrayAggregate() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_NamedArrayAggregate());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AndThenShortCircuit> getAndThenShortCircuit() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AndThenShortCircuit());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OrElseShortCircuit> getOrElseShortCircuit() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_OrElseShortCircuit());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InMembershipTest> getInMembershipTest() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_InMembershipTest());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NotInMembershipTest> getNotInMembershipTest() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_NotInMembershipTest());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NullLiteral> getNullLiteral() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_NullLiteral());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ParenthesizedExpression> getParenthesizedExpression() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ParenthesizedExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RaiseExpression> getRaiseExpression() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_RaiseExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TypeConversion> getTypeConversion() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_TypeConversion());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QualifiedExpression> getQualifiedExpression() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_QualifiedExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AllocationFromSubtype> getAllocationFromSubtype() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AllocationFromSubtype());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AllocationFromQualifiedExpression> getAllocationFromQualifiedExpression() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AllocationFromQualifiedExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CaseExpression> getCaseExpression() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_CaseExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IfExpression> getIfExpression() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_IfExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ForAllQuantifiedExpression> getForAllQuantifiedExpression() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ForAllQuantifiedExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ForSomeQuantifiedExpression> getForSomeQuantifiedExpression() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ForSomeQuantifiedExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PragmaArgumentAssociation> getPragmaArgumentAssociation() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PragmaArgumentAssociation());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscriminantAssociation> getDiscriminantAssociation() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DiscriminantAssociation());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RecordComponentAssociation> getRecordComponentAssociation() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_RecordComponentAssociation());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ArrayComponentAssociation> getArrayComponentAssociation() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ArrayComponentAssociation());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ParameterAssociation> getParameterAssociation() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ParameterAssociation());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GenericAssociation> getGenericAssociation() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_GenericAssociation());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NullStatement> getNullStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_NullStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssignmentStatement> getAssignmentStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AssignmentStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IfStatement> getIfStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_IfStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CaseStatement> getCaseStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_CaseStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LoopStatement> getLoopStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_LoopStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<WhileLoopStatement> getWhileLoopStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_WhileLoopStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ForLoopStatement> getForLoopStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ForLoopStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BlockStatement> getBlockStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_BlockStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExitStatement> getExitStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ExitStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GotoStatement> getGotoStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_GotoStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcedureCallStatement> getProcedureCallStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ProcedureCallStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReturnStatement> getReturnStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ReturnStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExtendedReturnStatement> getExtendedReturnStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ExtendedReturnStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AcceptStatement> getAcceptStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AcceptStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EntryCallStatement> getEntryCallStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_EntryCallStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RequeueStatement> getRequeueStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_RequeueStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RequeueStatementWithAbort> getRequeueStatementWithAbort() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_RequeueStatementWithAbort());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DelayUntilStatement> getDelayUntilStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DelayUntilStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DelayRelativeStatement> getDelayRelativeStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DelayRelativeStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TerminateAlternativeStatement> getTerminateAlternativeStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_TerminateAlternativeStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SelectiveAcceptStatement> getSelectiveAcceptStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_SelectiveAcceptStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TimedEntryCallStatement> getTimedEntryCallStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_TimedEntryCallStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConditionalEntryCallStatement> getConditionalEntryCallStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ConditionalEntryCallStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AsynchronousSelectStatement> getAsynchronousSelectStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AsynchronousSelectStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AbortStatement> getAbortStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AbortStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RaiseStatement> getRaiseStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_RaiseStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CodeStatement> getCodeStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_CodeStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IfPath> getIfPath() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_IfPath());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElsifPath> getElsifPath() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ElsifPath());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElsePath> getElsePath() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ElsePath());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CasePath> getCasePath() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_CasePath());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SelectPath> getSelectPath() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_SelectPath());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OrPath> getOrPath() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_OrPath());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ThenAbortPath> getThenAbortPath() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ThenAbortPath());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CaseExpressionPath> getCaseExpressionPath() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_CaseExpressionPath());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IfExpressionPath> getIfExpressionPath() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_IfExpressionPath());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElsifExpressionPath> getElsifExpressionPath() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ElsifExpressionPath());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElseExpressionPath> getElseExpressionPath() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ElseExpressionPath());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UsePackageClause> getUsePackageClause() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_UsePackageClause());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UseTypeClause> getUseTypeClause() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_UseTypeClause());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UseAllTypeClause> getUseAllTypeClause() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_UseAllTypeClause());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<WithClause> getWithClause() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_WithClause());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AttributeDefinitionClause> getAttributeDefinitionClause() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AttributeDefinitionClause());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EnumerationRepresentationClause> getEnumerationRepresentationClause() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_EnumerationRepresentationClause());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RecordRepresentationClause> getRecordRepresentationClause() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_RecordRepresentationClause());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AtClause> getAtClause() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AtClause());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComponentClause> getComponentClause() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ComponentClause());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExceptionHandler> getExceptionHandler() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ExceptionHandler());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Comment> getComment() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_Comment());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AllCallsRemotePragma> getAllCallsRemotePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AllCallsRemotePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AsynchronousPragma> getAsynchronousPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AsynchronousPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AtomicPragma> getAtomicPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AtomicPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AtomicComponentsPragma> getAtomicComponentsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AtomicComponentsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AttachHandlerPragma> getAttachHandlerPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AttachHandlerPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlledPragma> getControlledPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ControlledPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConventionPragma> getConventionPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ConventionPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscardNamesPragma> getDiscardNamesPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DiscardNamesPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElaboratePragma> getElaboratePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ElaboratePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElaborateAllPragma> getElaborateAllPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ElaborateAllPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElaborateBodyPragma> getElaborateBodyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ElaborateBodyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExportPragma> getExportPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ExportPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ImportPragma> getImportPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ImportPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InlinePragma> getInlinePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_InlinePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InspectionPointPragma> getInspectionPointPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_InspectionPointPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InterruptHandlerPragma> getInterruptHandlerPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_InterruptHandlerPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InterruptPriorityPragma> getInterruptPriorityPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_InterruptPriorityPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LinkerOptionsPragma> getLinkerOptionsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_LinkerOptionsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ListPragma> getListPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ListPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LockingPolicyPragma> getLockingPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_LockingPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NormalizeScalarsPragma> getNormalizeScalarsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_NormalizeScalarsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OptimizePragma> getOptimizePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_OptimizePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackPragma> getPackPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PackPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PagePragma> getPagePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PagePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PreelaboratePragma> getPreelaboratePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PreelaboratePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PriorityPragma> getPriorityPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PriorityPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PurePragma> getPurePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PurePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QueuingPolicyPragma> getQueuingPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_QueuingPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RemoteCallInterfacePragma> getRemoteCallInterfacePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_RemoteCallInterfacePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RemoteTypesPragma> getRemoteTypesPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_RemoteTypesPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RestrictionsPragma> getRestrictionsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_RestrictionsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReviewablePragma> getReviewablePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ReviewablePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SharedPassivePragma> getSharedPassivePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_SharedPassivePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StorageSizePragma> getStorageSizePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_StorageSizePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SuppressPragma> getSuppressPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_SuppressPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskDispatchingPolicyPragma> getTaskDispatchingPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_TaskDispatchingPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VolatilePragma> getVolatilePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_VolatilePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VolatileComponentsPragma> getVolatileComponentsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_VolatileComponentsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssertPragma> getAssertPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AssertPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssertionPolicyPragma> getAssertionPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_AssertionPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DetectBlockingPragma> getDetectBlockingPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DetectBlockingPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NoReturnPragma> getNoReturnPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_NoReturnPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PartitionElaborationPolicyPragma> getPartitionElaborationPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PartitionElaborationPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PreelaborableInitializationPragma> getPreelaborableInitializationPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PreelaborableInitializationPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PrioritySpecificDispatchingPragma> getPrioritySpecificDispatchingPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_PrioritySpecificDispatchingPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProfilePragma> getProfilePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ProfilePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RelativeDeadlinePragma> getRelativeDeadlinePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_RelativeDeadlinePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UncheckedUnionPragma> getUncheckedUnionPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_UncheckedUnionPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnsuppressPragma> getUnsuppressPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_UnsuppressPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefaultStoragePoolPragma> getDefaultStoragePoolPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DefaultStoragePoolPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DispatchingDomainPragma> getDispatchingDomainPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_DispatchingDomainPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CpuPragma> getCpuPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_CpuPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IndependentPragma> getIndependentPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_IndependentPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IndependentComponentsPragma> getIndependentComponentsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_IndependentComponentsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ImplementationDefinedPragma> getImplementationDefinedPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_ImplementationDefinedPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnknownPragma> getUnknownPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getElementList_UnknownPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.ELEMENT_LIST__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__NOT_AN_ELEMENT:
				return ((InternalEList<?>)getNotAnElement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINING_IDENTIFIER:
				return ((InternalEList<?>)getDefiningIdentifier()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINING_CHARACTER_LITERAL:
				return ((InternalEList<?>)getDefiningCharacterLiteral()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINING_ENUMERATION_LITERAL:
				return ((InternalEList<?>)getDefiningEnumerationLiteral()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINING_AND_OPERATOR:
				return ((InternalEList<?>)getDefiningAndOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINING_OR_OPERATOR:
				return ((InternalEList<?>)getDefiningOrOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINING_XOR_OPERATOR:
				return ((InternalEList<?>)getDefiningXorOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINING_EQUAL_OPERATOR:
				return ((InternalEList<?>)getDefiningEqualOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINING_NOT_EQUAL_OPERATOR:
				return ((InternalEList<?>)getDefiningNotEqualOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINING_LESS_THAN_OPERATOR:
				return ((InternalEList<?>)getDefiningLessThanOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINING_LESS_THAN_OR_EQUAL_OPERATOR:
				return ((InternalEList<?>)getDefiningLessThanOrEqualOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINING_GREATER_THAN_OPERATOR:
				return ((InternalEList<?>)getDefiningGreaterThanOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINING_GREATER_THAN_OR_EQUAL_OPERATOR:
				return ((InternalEList<?>)getDefiningGreaterThanOrEqualOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINING_PLUS_OPERATOR:
				return ((InternalEList<?>)getDefiningPlusOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINING_MINUS_OPERATOR:
				return ((InternalEList<?>)getDefiningMinusOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINING_CONCATENATE_OPERATOR:
				return ((InternalEList<?>)getDefiningConcatenateOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINING_UNARY_PLUS_OPERATOR:
				return ((InternalEList<?>)getDefiningUnaryPlusOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINING_UNARY_MINUS_OPERATOR:
				return ((InternalEList<?>)getDefiningUnaryMinusOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINING_MULTIPLY_OPERATOR:
				return ((InternalEList<?>)getDefiningMultiplyOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINING_DIVIDE_OPERATOR:
				return ((InternalEList<?>)getDefiningDivideOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINING_MOD_OPERATOR:
				return ((InternalEList<?>)getDefiningModOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINING_REM_OPERATOR:
				return ((InternalEList<?>)getDefiningRemOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINING_EXPONENTIATE_OPERATOR:
				return ((InternalEList<?>)getDefiningExponentiateOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINING_ABS_OPERATOR:
				return ((InternalEList<?>)getDefiningAbsOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINING_NOT_OPERATOR:
				return ((InternalEList<?>)getDefiningNotOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINING_EXPANDED_NAME:
				return ((InternalEList<?>)getDefiningExpandedName()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ORDINARY_TYPE_DECLARATION:
				return ((InternalEList<?>)getOrdinaryTypeDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__TASK_TYPE_DECLARATION:
				return ((InternalEList<?>)getTaskTypeDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PROTECTED_TYPE_DECLARATION:
				return ((InternalEList<?>)getProtectedTypeDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__INCOMPLETE_TYPE_DECLARATION:
				return ((InternalEList<?>)getIncompleteTypeDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__TAGGED_INCOMPLETE_TYPE_DECLARATION:
				return ((InternalEList<?>)getTaggedIncompleteTypeDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PRIVATE_TYPE_DECLARATION:
				return ((InternalEList<?>)getPrivateTypeDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PRIVATE_EXTENSION_DECLARATION:
				return ((InternalEList<?>)getPrivateExtensionDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__SUBTYPE_DECLARATION:
				return ((InternalEList<?>)getSubtypeDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__VARIABLE_DECLARATION:
				return ((InternalEList<?>)getVariableDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__CONSTANT_DECLARATION:
				return ((InternalEList<?>)getConstantDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFERRED_CONSTANT_DECLARATION:
				return ((InternalEList<?>)getDeferredConstantDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__SINGLE_TASK_DECLARATION:
				return ((InternalEList<?>)getSingleTaskDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__SINGLE_PROTECTED_DECLARATION:
				return ((InternalEList<?>)getSingleProtectedDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__INTEGER_NUMBER_DECLARATION:
				return ((InternalEList<?>)getIntegerNumberDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__REAL_NUMBER_DECLARATION:
				return ((InternalEList<?>)getRealNumberDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ENUMERATION_LITERAL_SPECIFICATION:
				return ((InternalEList<?>)getEnumerationLiteralSpecification()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DISCRIMINANT_SPECIFICATION:
				return ((InternalEList<?>)getDiscriminantSpecification()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__COMPONENT_DECLARATION:
				return ((InternalEList<?>)getComponentDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__LOOP_PARAMETER_SPECIFICATION:
				return ((InternalEList<?>)getLoopParameterSpecification()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__GENERALIZED_ITERATOR_SPECIFICATION:
				return ((InternalEList<?>)getGeneralizedIteratorSpecification()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ELEMENT_ITERATOR_SPECIFICATION:
				return ((InternalEList<?>)getElementIteratorSpecification()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PROCEDURE_DECLARATION:
				return ((InternalEList<?>)getProcedureDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FUNCTION_DECLARATION:
				return ((InternalEList<?>)getFunctionDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PARAMETER_SPECIFICATION:
				return ((InternalEList<?>)getParameterSpecification()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PROCEDURE_BODY_DECLARATION:
				return ((InternalEList<?>)getProcedureBodyDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FUNCTION_BODY_DECLARATION:
				return ((InternalEList<?>)getFunctionBodyDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__RETURN_VARIABLE_SPECIFICATION:
				return ((InternalEList<?>)getReturnVariableSpecification()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__RETURN_CONSTANT_SPECIFICATION:
				return ((InternalEList<?>)getReturnConstantSpecification()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__NULL_PROCEDURE_DECLARATION:
				return ((InternalEList<?>)getNullProcedureDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__EXPRESSION_FUNCTION_DECLARATION:
				return ((InternalEList<?>)getExpressionFunctionDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PACKAGE_DECLARATION:
				return ((InternalEList<?>)getPackageDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PACKAGE_BODY_DECLARATION:
				return ((InternalEList<?>)getPackageBodyDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__OBJECT_RENAMING_DECLARATION:
				return ((InternalEList<?>)getObjectRenamingDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__EXCEPTION_RENAMING_DECLARATION:
				return ((InternalEList<?>)getExceptionRenamingDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PACKAGE_RENAMING_DECLARATION:
				return ((InternalEList<?>)getPackageRenamingDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PROCEDURE_RENAMING_DECLARATION:
				return ((InternalEList<?>)getProcedureRenamingDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FUNCTION_RENAMING_DECLARATION:
				return ((InternalEList<?>)getFunctionRenamingDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__GENERIC_PACKAGE_RENAMING_DECLARATION:
				return ((InternalEList<?>)getGenericPackageRenamingDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__GENERIC_PROCEDURE_RENAMING_DECLARATION:
				return ((InternalEList<?>)getGenericProcedureRenamingDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__GENERIC_FUNCTION_RENAMING_DECLARATION:
				return ((InternalEList<?>)getGenericFunctionRenamingDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__TASK_BODY_DECLARATION:
				return ((InternalEList<?>)getTaskBodyDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PROTECTED_BODY_DECLARATION:
				return ((InternalEList<?>)getProtectedBodyDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ENTRY_DECLARATION:
				return ((InternalEList<?>)getEntryDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ENTRY_BODY_DECLARATION:
				return ((InternalEList<?>)getEntryBodyDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ENTRY_INDEX_SPECIFICATION:
				return ((InternalEList<?>)getEntryIndexSpecification()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PROCEDURE_BODY_STUB:
				return ((InternalEList<?>)getProcedureBodyStub()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FUNCTION_BODY_STUB:
				return ((InternalEList<?>)getFunctionBodyStub()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PACKAGE_BODY_STUB:
				return ((InternalEList<?>)getPackageBodyStub()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__TASK_BODY_STUB:
				return ((InternalEList<?>)getTaskBodyStub()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PROTECTED_BODY_STUB:
				return ((InternalEList<?>)getProtectedBodyStub()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__EXCEPTION_DECLARATION:
				return ((InternalEList<?>)getExceptionDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__CHOICE_PARAMETER_SPECIFICATION:
				return ((InternalEList<?>)getChoiceParameterSpecification()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__GENERIC_PROCEDURE_DECLARATION:
				return ((InternalEList<?>)getGenericProcedureDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__GENERIC_FUNCTION_DECLARATION:
				return ((InternalEList<?>)getGenericFunctionDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__GENERIC_PACKAGE_DECLARATION:
				return ((InternalEList<?>)getGenericPackageDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PACKAGE_INSTANTIATION:
				return ((InternalEList<?>)getPackageInstantiation()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PROCEDURE_INSTANTIATION:
				return ((InternalEList<?>)getProcedureInstantiation()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FUNCTION_INSTANTIATION:
				return ((InternalEList<?>)getFunctionInstantiation()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_OBJECT_DECLARATION:
				return ((InternalEList<?>)getFormalObjectDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_TYPE_DECLARATION:
				return ((InternalEList<?>)getFormalTypeDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_INCOMPLETE_TYPE_DECLARATION:
				return ((InternalEList<?>)getFormalIncompleteTypeDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_PROCEDURE_DECLARATION:
				return ((InternalEList<?>)getFormalProcedureDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_FUNCTION_DECLARATION:
				return ((InternalEList<?>)getFormalFunctionDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_PACKAGE_DECLARATION:
				return ((InternalEList<?>)getFormalPackageDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_PACKAGE_DECLARATION_WITH_BOX:
				return ((InternalEList<?>)getFormalPackageDeclarationWithBox()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DERIVED_TYPE_DEFINITION:
				return ((InternalEList<?>)getDerivedTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DERIVED_RECORD_EXTENSION_DEFINITION:
				return ((InternalEList<?>)getDerivedRecordExtensionDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ENUMERATION_TYPE_DEFINITION:
				return ((InternalEList<?>)getEnumerationTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__SIGNED_INTEGER_TYPE_DEFINITION:
				return ((InternalEList<?>)getSignedIntegerTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__MODULAR_TYPE_DEFINITION:
				return ((InternalEList<?>)getModularTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ROOT_INTEGER_DEFINITION:
				return ((InternalEList<?>)getRootIntegerDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ROOT_REAL_DEFINITION:
				return ((InternalEList<?>)getRootRealDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__UNIVERSAL_INTEGER_DEFINITION:
				return ((InternalEList<?>)getUniversalIntegerDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__UNIVERSAL_REAL_DEFINITION:
				return ((InternalEList<?>)getUniversalRealDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__UNIVERSAL_FIXED_DEFINITION:
				return ((InternalEList<?>)getUniversalFixedDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FLOATING_POINT_DEFINITION:
				return ((InternalEList<?>)getFloatingPointDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ORDINARY_FIXED_POINT_DEFINITION:
				return ((InternalEList<?>)getOrdinaryFixedPointDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DECIMAL_FIXED_POINT_DEFINITION:
				return ((InternalEList<?>)getDecimalFixedPointDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__UNCONSTRAINED_ARRAY_DEFINITION:
				return ((InternalEList<?>)getUnconstrainedArrayDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__CONSTRAINED_ARRAY_DEFINITION:
				return ((InternalEList<?>)getConstrainedArrayDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__RECORD_TYPE_DEFINITION:
				return ((InternalEList<?>)getRecordTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__TAGGED_RECORD_TYPE_DEFINITION:
				return ((InternalEList<?>)getTaggedRecordTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ORDINARY_INTERFACE:
				return ((InternalEList<?>)getOrdinaryInterface()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__LIMITED_INTERFACE:
				return ((InternalEList<?>)getLimitedInterface()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__TASK_INTERFACE:
				return ((InternalEList<?>)getTaskInterface()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PROTECTED_INTERFACE:
				return ((InternalEList<?>)getProtectedInterface()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__SYNCHRONIZED_INTERFACE:
				return ((InternalEList<?>)getSynchronizedInterface()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				return ((InternalEList<?>)getPoolSpecificAccessToVariable()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_VARIABLE:
				return ((InternalEList<?>)getAccessToVariable()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_CONSTANT:
				return ((InternalEList<?>)getAccessToConstant()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_PROCEDURE:
				return ((InternalEList<?>)getAccessToProcedure()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_PROTECTED_PROCEDURE:
				return ((InternalEList<?>)getAccessToProtectedProcedure()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_FUNCTION:
				return ((InternalEList<?>)getAccessToFunction()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_PROTECTED_FUNCTION:
				return ((InternalEList<?>)getAccessToProtectedFunction()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__SUBTYPE_INDICATION:
				return ((InternalEList<?>)getSubtypeIndication()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__RANGE_ATTRIBUTE_REFERENCE:
				return ((InternalEList<?>)getRangeAttributeReference()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__SIMPLE_EXPRESSION_RANGE:
				return ((InternalEList<?>)getSimpleExpressionRange()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DIGITS_CONSTRAINT:
				return ((InternalEList<?>)getDigitsConstraint()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DELTA_CONSTRAINT:
				return ((InternalEList<?>)getDeltaConstraint()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__INDEX_CONSTRAINT:
				return ((InternalEList<?>)getIndexConstraint()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DISCRIMINANT_CONSTRAINT:
				return ((InternalEList<?>)getDiscriminantConstraint()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__COMPONENT_DEFINITION:
				return ((InternalEList<?>)getComponentDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION:
				return ((InternalEList<?>)getDiscreteSubtypeIndicationAsSubtypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION:
				return ((InternalEList<?>)getDiscreteRangeAttributeReferenceAsSubtypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION:
				return ((InternalEList<?>)getDiscreteSimpleExpressionRangeAsSubtypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DISCRETE_SUBTYPE_INDICATION:
				return ((InternalEList<?>)getDiscreteSubtypeIndication()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DISCRETE_RANGE_ATTRIBUTE_REFERENCE:
				return ((InternalEList<?>)getDiscreteRangeAttributeReference()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DISCRETE_SIMPLE_EXPRESSION_RANGE:
				return ((InternalEList<?>)getDiscreteSimpleExpressionRange()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__UNKNOWN_DISCRIMINANT_PART:
				return ((InternalEList<?>)getUnknownDiscriminantPart()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__KNOWN_DISCRIMINANT_PART:
				return ((InternalEList<?>)getKnownDiscriminantPart()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__RECORD_DEFINITION:
				return ((InternalEList<?>)getRecordDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__NULL_RECORD_DEFINITION:
				return ((InternalEList<?>)getNullRecordDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__NULL_COMPONENT:
				return ((InternalEList<?>)getNullComponent()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__VARIANT_PART:
				return ((InternalEList<?>)getVariantPart()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__VARIANT:
				return ((InternalEList<?>)getVariant()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__OTHERS_CHOICE:
				return ((InternalEList<?>)getOthersChoice()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_VARIABLE:
				return ((InternalEList<?>)getAnonymousAccessToVariable()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_CONSTANT:
				return ((InternalEList<?>)getAnonymousAccessToConstant()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_PROCEDURE:
				return ((InternalEList<?>)getAnonymousAccessToProcedure()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE:
				return ((InternalEList<?>)getAnonymousAccessToProtectedProcedure()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_FUNCTION:
				return ((InternalEList<?>)getAnonymousAccessToFunction()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION:
				return ((InternalEList<?>)getAnonymousAccessToProtectedFunction()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PRIVATE_TYPE_DEFINITION:
				return ((InternalEList<?>)getPrivateTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__TAGGED_PRIVATE_TYPE_DEFINITION:
				return ((InternalEList<?>)getTaggedPrivateTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PRIVATE_EXTENSION_DEFINITION:
				return ((InternalEList<?>)getPrivateExtensionDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__TASK_DEFINITION:
				return ((InternalEList<?>)getTaskDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PROTECTED_DEFINITION:
				return ((InternalEList<?>)getProtectedDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_PRIVATE_TYPE_DEFINITION:
				return ((InternalEList<?>)getFormalPrivateTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION:
				return ((InternalEList<?>)getFormalTaggedPrivateTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_DERIVED_TYPE_DEFINITION:
				return ((InternalEList<?>)getFormalDerivedTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_DISCRETE_TYPE_DEFINITION:
				return ((InternalEList<?>)getFormalDiscreteTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_SIGNED_INTEGER_TYPE_DEFINITION:
				return ((InternalEList<?>)getFormalSignedIntegerTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_MODULAR_TYPE_DEFINITION:
				return ((InternalEList<?>)getFormalModularTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_FLOATING_POINT_DEFINITION:
				return ((InternalEList<?>)getFormalFloatingPointDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_ORDINARY_FIXED_POINT_DEFINITION:
				return ((InternalEList<?>)getFormalOrdinaryFixedPointDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_DECIMAL_FIXED_POINT_DEFINITION:
				return ((InternalEList<?>)getFormalDecimalFixedPointDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_ORDINARY_INTERFACE:
				return ((InternalEList<?>)getFormalOrdinaryInterface()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_LIMITED_INTERFACE:
				return ((InternalEList<?>)getFormalLimitedInterface()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_TASK_INTERFACE:
				return ((InternalEList<?>)getFormalTaskInterface()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_PROTECTED_INTERFACE:
				return ((InternalEList<?>)getFormalProtectedInterface()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_SYNCHRONIZED_INTERFACE:
				return ((InternalEList<?>)getFormalSynchronizedInterface()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_UNCONSTRAINED_ARRAY_DEFINITION:
				return ((InternalEList<?>)getFormalUnconstrainedArrayDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_CONSTRAINED_ARRAY_DEFINITION:
				return ((InternalEList<?>)getFormalConstrainedArrayDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				return ((InternalEList<?>)getFormalPoolSpecificAccessToVariable()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_VARIABLE:
				return ((InternalEList<?>)getFormalAccessToVariable()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_CONSTANT:
				return ((InternalEList<?>)getFormalAccessToConstant()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_PROCEDURE:
				return ((InternalEList<?>)getFormalAccessToProcedure()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_PROTECTED_PROCEDURE:
				return ((InternalEList<?>)getFormalAccessToProtectedProcedure()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_FUNCTION:
				return ((InternalEList<?>)getFormalAccessToFunction()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_PROTECTED_FUNCTION:
				return ((InternalEList<?>)getFormalAccessToProtectedFunction()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ASPECT_SPECIFICATION:
				return ((InternalEList<?>)getAspectSpecification()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__BOX_EXPRESSION:
				return ((InternalEList<?>)getBoxExpression()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__INTEGER_LITERAL:
				return ((InternalEList<?>)getIntegerLiteral()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__REAL_LITERAL:
				return ((InternalEList<?>)getRealLiteral()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__STRING_LITERAL:
				return ((InternalEList<?>)getStringLiteral()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__IDENTIFIER:
				return ((InternalEList<?>)getIdentifier()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__AND_OPERATOR:
				return ((InternalEList<?>)getAndOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__OR_OPERATOR:
				return ((InternalEList<?>)getOrOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__XOR_OPERATOR:
				return ((InternalEList<?>)getXorOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__EQUAL_OPERATOR:
				return ((InternalEList<?>)getEqualOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__NOT_EQUAL_OPERATOR:
				return ((InternalEList<?>)getNotEqualOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__LESS_THAN_OPERATOR:
				return ((InternalEList<?>)getLessThanOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__LESS_THAN_OR_EQUAL_OPERATOR:
				return ((InternalEList<?>)getLessThanOrEqualOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__GREATER_THAN_OPERATOR:
				return ((InternalEList<?>)getGreaterThanOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__GREATER_THAN_OR_EQUAL_OPERATOR:
				return ((InternalEList<?>)getGreaterThanOrEqualOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PLUS_OPERATOR:
				return ((InternalEList<?>)getPlusOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__MINUS_OPERATOR:
				return ((InternalEList<?>)getMinusOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__CONCATENATE_OPERATOR:
				return ((InternalEList<?>)getConcatenateOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__UNARY_PLUS_OPERATOR:
				return ((InternalEList<?>)getUnaryPlusOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__UNARY_MINUS_OPERATOR:
				return ((InternalEList<?>)getUnaryMinusOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__MULTIPLY_OPERATOR:
				return ((InternalEList<?>)getMultiplyOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DIVIDE_OPERATOR:
				return ((InternalEList<?>)getDivideOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__MOD_OPERATOR:
				return ((InternalEList<?>)getModOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__REM_OPERATOR:
				return ((InternalEList<?>)getRemOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__EXPONENTIATE_OPERATOR:
				return ((InternalEList<?>)getExponentiateOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ABS_OPERATOR:
				return ((InternalEList<?>)getAbsOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__NOT_OPERATOR:
				return ((InternalEList<?>)getNotOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__CHARACTER_LITERAL:
				return ((InternalEList<?>)getCharacterLiteral()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ENUMERATION_LITERAL:
				return ((InternalEList<?>)getEnumerationLiteral()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__EXPLICIT_DEREFERENCE:
				return ((InternalEList<?>)getExplicitDereference()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FUNCTION_CALL:
				return ((InternalEList<?>)getFunctionCall()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__INDEXED_COMPONENT:
				return ((InternalEList<?>)getIndexedComponent()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__SLICE:
				return ((InternalEList<?>)getSlice()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__SELECTED_COMPONENT:
				return ((InternalEList<?>)getSelectedComponent()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ACCESS_ATTRIBUTE:
				return ((InternalEList<?>)getAccessAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ADDRESS_ATTRIBUTE:
				return ((InternalEList<?>)getAddressAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ADJACENT_ATTRIBUTE:
				return ((InternalEList<?>)getAdjacentAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__AFT_ATTRIBUTE:
				return ((InternalEList<?>)getAftAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ALIGNMENT_ATTRIBUTE:
				return ((InternalEList<?>)getAlignmentAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__BASE_ATTRIBUTE:
				return ((InternalEList<?>)getBaseAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__BIT_ORDER_ATTRIBUTE:
				return ((InternalEList<?>)getBitOrderAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__BODY_VERSION_ATTRIBUTE:
				return ((InternalEList<?>)getBodyVersionAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__CALLABLE_ATTRIBUTE:
				return ((InternalEList<?>)getCallableAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__CALLER_ATTRIBUTE:
				return ((InternalEList<?>)getCallerAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__CEILING_ATTRIBUTE:
				return ((InternalEList<?>)getCeilingAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__CLASS_ATTRIBUTE:
				return ((InternalEList<?>)getClassAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__COMPONENT_SIZE_ATTRIBUTE:
				return ((InternalEList<?>)getComponentSizeAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__COMPOSE_ATTRIBUTE:
				return ((InternalEList<?>)getComposeAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__CONSTRAINED_ATTRIBUTE:
				return ((InternalEList<?>)getConstrainedAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__COPY_SIGN_ATTRIBUTE:
				return ((InternalEList<?>)getCopySignAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__COUNT_ATTRIBUTE:
				return ((InternalEList<?>)getCountAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFINITE_ATTRIBUTE:
				return ((InternalEList<?>)getDefiniteAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DELTA_ATTRIBUTE:
				return ((InternalEList<?>)getDeltaAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DENORM_ATTRIBUTE:
				return ((InternalEList<?>)getDenormAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DIGITS_ATTRIBUTE:
				return ((InternalEList<?>)getDigitsAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__EXPONENT_ATTRIBUTE:
				return ((InternalEList<?>)getExponentAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__EXTERNAL_TAG_ATTRIBUTE:
				return ((InternalEList<?>)getExternalTagAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FIRST_ATTRIBUTE:
				return ((InternalEList<?>)getFirstAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FIRST_BIT_ATTRIBUTE:
				return ((InternalEList<?>)getFirstBitAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FLOOR_ATTRIBUTE:
				return ((InternalEList<?>)getFloorAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FORE_ATTRIBUTE:
				return ((InternalEList<?>)getForeAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FRACTION_ATTRIBUTE:
				return ((InternalEList<?>)getFractionAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__IDENTITY_ATTRIBUTE:
				return ((InternalEList<?>)getIdentityAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__IMAGE_ATTRIBUTE:
				return ((InternalEList<?>)getImageAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__INPUT_ATTRIBUTE:
				return ((InternalEList<?>)getInputAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__LAST_ATTRIBUTE:
				return ((InternalEList<?>)getLastAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__LAST_BIT_ATTRIBUTE:
				return ((InternalEList<?>)getLastBitAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__LEADING_PART_ATTRIBUTE:
				return ((InternalEList<?>)getLeadingPartAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__LENGTH_ATTRIBUTE:
				return ((InternalEList<?>)getLengthAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__MACHINE_ATTRIBUTE:
				return ((InternalEList<?>)getMachineAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__MACHINE_EMAX_ATTRIBUTE:
				return ((InternalEList<?>)getMachineEmaxAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__MACHINE_EMIN_ATTRIBUTE:
				return ((InternalEList<?>)getMachineEminAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__MACHINE_MANTISSA_ATTRIBUTE:
				return ((InternalEList<?>)getMachineMantissaAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__MACHINE_OVERFLOWS_ATTRIBUTE:
				return ((InternalEList<?>)getMachineOverflowsAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__MACHINE_RADIX_ATTRIBUTE:
				return ((InternalEList<?>)getMachineRadixAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__MACHINE_ROUNDS_ATTRIBUTE:
				return ((InternalEList<?>)getMachineRoundsAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__MAX_ATTRIBUTE:
				return ((InternalEList<?>)getMaxAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE:
				return ((InternalEList<?>)getMaxSizeInStorageElementsAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__MIN_ATTRIBUTE:
				return ((InternalEList<?>)getMinAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__MODEL_ATTRIBUTE:
				return ((InternalEList<?>)getModelAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__MODEL_EMIN_ATTRIBUTE:
				return ((InternalEList<?>)getModelEminAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__MODEL_EPSILON_ATTRIBUTE:
				return ((InternalEList<?>)getModelEpsilonAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__MODEL_MANTISSA_ATTRIBUTE:
				return ((InternalEList<?>)getModelMantissaAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__MODEL_SMALL_ATTRIBUTE:
				return ((InternalEList<?>)getModelSmallAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__MODULUS_ATTRIBUTE:
				return ((InternalEList<?>)getModulusAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__OUTPUT_ATTRIBUTE:
				return ((InternalEList<?>)getOutputAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PARTITION_ID_ATTRIBUTE:
				return ((InternalEList<?>)getPartitionIdAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__POS_ATTRIBUTE:
				return ((InternalEList<?>)getPosAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__POSITION_ATTRIBUTE:
				return ((InternalEList<?>)getPositionAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PRED_ATTRIBUTE:
				return ((InternalEList<?>)getPredAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__RANGE_ATTRIBUTE:
				return ((InternalEList<?>)getRangeAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__READ_ATTRIBUTE:
				return ((InternalEList<?>)getReadAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__REMAINDER_ATTRIBUTE:
				return ((InternalEList<?>)getRemainderAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ROUND_ATTRIBUTE:
				return ((InternalEList<?>)getRoundAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ROUNDING_ATTRIBUTE:
				return ((InternalEList<?>)getRoundingAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__SAFE_FIRST_ATTRIBUTE:
				return ((InternalEList<?>)getSafeFirstAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__SAFE_LAST_ATTRIBUTE:
				return ((InternalEList<?>)getSafeLastAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__SCALE_ATTRIBUTE:
				return ((InternalEList<?>)getScaleAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__SCALING_ATTRIBUTE:
				return ((InternalEList<?>)getScalingAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__SIGNED_ZEROS_ATTRIBUTE:
				return ((InternalEList<?>)getSignedZerosAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__SIZE_ATTRIBUTE:
				return ((InternalEList<?>)getSizeAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__SMALL_ATTRIBUTE:
				return ((InternalEList<?>)getSmallAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__STORAGE_POOL_ATTRIBUTE:
				return ((InternalEList<?>)getStoragePoolAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__STORAGE_SIZE_ATTRIBUTE:
				return ((InternalEList<?>)getStorageSizeAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__SUCC_ATTRIBUTE:
				return ((InternalEList<?>)getSuccAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__TAG_ATTRIBUTE:
				return ((InternalEList<?>)getTagAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__TERMINATED_ATTRIBUTE:
				return ((InternalEList<?>)getTerminatedAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__TRUNCATION_ATTRIBUTE:
				return ((InternalEList<?>)getTruncationAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__UNBIASED_ROUNDING_ATTRIBUTE:
				return ((InternalEList<?>)getUnbiasedRoundingAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__UNCHECKED_ACCESS_ATTRIBUTE:
				return ((InternalEList<?>)getUncheckedAccessAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__VAL_ATTRIBUTE:
				return ((InternalEList<?>)getValAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__VALID_ATTRIBUTE:
				return ((InternalEList<?>)getValidAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__VALUE_ATTRIBUTE:
				return ((InternalEList<?>)getValueAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__VERSION_ATTRIBUTE:
				return ((InternalEList<?>)getVersionAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__WIDE_IMAGE_ATTRIBUTE:
				return ((InternalEList<?>)getWideImageAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__WIDE_VALUE_ATTRIBUTE:
				return ((InternalEList<?>)getWideValueAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__WIDE_WIDTH_ATTRIBUTE:
				return ((InternalEList<?>)getWideWidthAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__WIDTH_ATTRIBUTE:
				return ((InternalEList<?>)getWidthAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__WRITE_ATTRIBUTE:
				return ((InternalEList<?>)getWriteAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__MACHINE_ROUNDING_ATTRIBUTE:
				return ((InternalEList<?>)getMachineRoundingAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__MOD_ATTRIBUTE:
				return ((InternalEList<?>)getModAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PRIORITY_ATTRIBUTE:
				return ((InternalEList<?>)getPriorityAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__STREAM_SIZE_ATTRIBUTE:
				return ((InternalEList<?>)getStreamSizeAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__WIDE_WIDE_IMAGE_ATTRIBUTE:
				return ((InternalEList<?>)getWideWideImageAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__WIDE_WIDE_VALUE_ATTRIBUTE:
				return ((InternalEList<?>)getWideWideValueAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__WIDE_WIDE_WIDTH_ATTRIBUTE:
				return ((InternalEList<?>)getWideWideWidthAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE:
				return ((InternalEList<?>)getMaxAlignmentForAllocationAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__OVERLAPS_STORAGE_ATTRIBUTE:
				return ((InternalEList<?>)getOverlapsStorageAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__IMPLEMENTATION_DEFINED_ATTRIBUTE:
				return ((InternalEList<?>)getImplementationDefinedAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__UNKNOWN_ATTRIBUTE:
				return ((InternalEList<?>)getUnknownAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__RECORD_AGGREGATE:
				return ((InternalEList<?>)getRecordAggregate()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__EXTENSION_AGGREGATE:
				return ((InternalEList<?>)getExtensionAggregate()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__POSITIONAL_ARRAY_AGGREGATE:
				return ((InternalEList<?>)getPositionalArrayAggregate()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__NAMED_ARRAY_AGGREGATE:
				return ((InternalEList<?>)getNamedArrayAggregate()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__AND_THEN_SHORT_CIRCUIT:
				return ((InternalEList<?>)getAndThenShortCircuit()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__OR_ELSE_SHORT_CIRCUIT:
				return ((InternalEList<?>)getOrElseShortCircuit()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__IN_MEMBERSHIP_TEST:
				return ((InternalEList<?>)getInMembershipTest()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__NOT_IN_MEMBERSHIP_TEST:
				return ((InternalEList<?>)getNotInMembershipTest()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__NULL_LITERAL:
				return ((InternalEList<?>)getNullLiteral()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PARENTHESIZED_EXPRESSION:
				return ((InternalEList<?>)getParenthesizedExpression()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__RAISE_EXPRESSION:
				return ((InternalEList<?>)getRaiseExpression()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__TYPE_CONVERSION:
				return ((InternalEList<?>)getTypeConversion()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__QUALIFIED_EXPRESSION:
				return ((InternalEList<?>)getQualifiedExpression()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ALLOCATION_FROM_SUBTYPE:
				return ((InternalEList<?>)getAllocationFromSubtype()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ALLOCATION_FROM_QUALIFIED_EXPRESSION:
				return ((InternalEList<?>)getAllocationFromQualifiedExpression()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__CASE_EXPRESSION:
				return ((InternalEList<?>)getCaseExpression()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__IF_EXPRESSION:
				return ((InternalEList<?>)getIfExpression()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FOR_ALL_QUANTIFIED_EXPRESSION:
				return ((InternalEList<?>)getForAllQuantifiedExpression()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FOR_SOME_QUANTIFIED_EXPRESSION:
				return ((InternalEList<?>)getForSomeQuantifiedExpression()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PRAGMA_ARGUMENT_ASSOCIATION:
				return ((InternalEList<?>)getPragmaArgumentAssociation()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DISCRIMINANT_ASSOCIATION:
				return ((InternalEList<?>)getDiscriminantAssociation()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__RECORD_COMPONENT_ASSOCIATION:
				return ((InternalEList<?>)getRecordComponentAssociation()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ARRAY_COMPONENT_ASSOCIATION:
				return ((InternalEList<?>)getArrayComponentAssociation()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PARAMETER_ASSOCIATION:
				return ((InternalEList<?>)getParameterAssociation()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__GENERIC_ASSOCIATION:
				return ((InternalEList<?>)getGenericAssociation()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__NULL_STATEMENT:
				return ((InternalEList<?>)getNullStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ASSIGNMENT_STATEMENT:
				return ((InternalEList<?>)getAssignmentStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__IF_STATEMENT:
				return ((InternalEList<?>)getIfStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__CASE_STATEMENT:
				return ((InternalEList<?>)getCaseStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__LOOP_STATEMENT:
				return ((InternalEList<?>)getLoopStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__WHILE_LOOP_STATEMENT:
				return ((InternalEList<?>)getWhileLoopStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__FOR_LOOP_STATEMENT:
				return ((InternalEList<?>)getForLoopStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__BLOCK_STATEMENT:
				return ((InternalEList<?>)getBlockStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__EXIT_STATEMENT:
				return ((InternalEList<?>)getExitStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__GOTO_STATEMENT:
				return ((InternalEList<?>)getGotoStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PROCEDURE_CALL_STATEMENT:
				return ((InternalEList<?>)getProcedureCallStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__RETURN_STATEMENT:
				return ((InternalEList<?>)getReturnStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__EXTENDED_RETURN_STATEMENT:
				return ((InternalEList<?>)getExtendedReturnStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ACCEPT_STATEMENT:
				return ((InternalEList<?>)getAcceptStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ENTRY_CALL_STATEMENT:
				return ((InternalEList<?>)getEntryCallStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__REQUEUE_STATEMENT:
				return ((InternalEList<?>)getRequeueStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__REQUEUE_STATEMENT_WITH_ABORT:
				return ((InternalEList<?>)getRequeueStatementWithAbort()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DELAY_UNTIL_STATEMENT:
				return ((InternalEList<?>)getDelayUntilStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DELAY_RELATIVE_STATEMENT:
				return ((InternalEList<?>)getDelayRelativeStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__TERMINATE_ALTERNATIVE_STATEMENT:
				return ((InternalEList<?>)getTerminateAlternativeStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__SELECTIVE_ACCEPT_STATEMENT:
				return ((InternalEList<?>)getSelectiveAcceptStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__TIMED_ENTRY_CALL_STATEMENT:
				return ((InternalEList<?>)getTimedEntryCallStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__CONDITIONAL_ENTRY_CALL_STATEMENT:
				return ((InternalEList<?>)getConditionalEntryCallStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ASYNCHRONOUS_SELECT_STATEMENT:
				return ((InternalEList<?>)getAsynchronousSelectStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ABORT_STATEMENT:
				return ((InternalEList<?>)getAbortStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__RAISE_STATEMENT:
				return ((InternalEList<?>)getRaiseStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__CODE_STATEMENT:
				return ((InternalEList<?>)getCodeStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__IF_PATH:
				return ((InternalEList<?>)getIfPath()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ELSIF_PATH:
				return ((InternalEList<?>)getElsifPath()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ELSE_PATH:
				return ((InternalEList<?>)getElsePath()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__CASE_PATH:
				return ((InternalEList<?>)getCasePath()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__SELECT_PATH:
				return ((InternalEList<?>)getSelectPath()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__OR_PATH:
				return ((InternalEList<?>)getOrPath()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__THEN_ABORT_PATH:
				return ((InternalEList<?>)getThenAbortPath()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__CASE_EXPRESSION_PATH:
				return ((InternalEList<?>)getCaseExpressionPath()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__IF_EXPRESSION_PATH:
				return ((InternalEList<?>)getIfExpressionPath()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ELSIF_EXPRESSION_PATH:
				return ((InternalEList<?>)getElsifExpressionPath()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ELSE_EXPRESSION_PATH:
				return ((InternalEList<?>)getElseExpressionPath()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__USE_PACKAGE_CLAUSE:
				return ((InternalEList<?>)getUsePackageClause()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__USE_TYPE_CLAUSE:
				return ((InternalEList<?>)getUseTypeClause()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__USE_ALL_TYPE_CLAUSE:
				return ((InternalEList<?>)getUseAllTypeClause()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__WITH_CLAUSE:
				return ((InternalEList<?>)getWithClause()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ATTRIBUTE_DEFINITION_CLAUSE:
				return ((InternalEList<?>)getAttributeDefinitionClause()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ENUMERATION_REPRESENTATION_CLAUSE:
				return ((InternalEList<?>)getEnumerationRepresentationClause()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__RECORD_REPRESENTATION_CLAUSE:
				return ((InternalEList<?>)getRecordRepresentationClause()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__AT_CLAUSE:
				return ((InternalEList<?>)getAtClause()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__COMPONENT_CLAUSE:
				return ((InternalEList<?>)getComponentClause()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__EXCEPTION_HANDLER:
				return ((InternalEList<?>)getExceptionHandler()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__COMMENT:
				return ((InternalEList<?>)getComment()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ALL_CALLS_REMOTE_PRAGMA:
				return ((InternalEList<?>)getAllCallsRemotePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ASYNCHRONOUS_PRAGMA:
				return ((InternalEList<?>)getAsynchronousPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ATOMIC_PRAGMA:
				return ((InternalEList<?>)getAtomicPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ATOMIC_COMPONENTS_PRAGMA:
				return ((InternalEList<?>)getAtomicComponentsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ATTACH_HANDLER_PRAGMA:
				return ((InternalEList<?>)getAttachHandlerPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__CONTROLLED_PRAGMA:
				return ((InternalEList<?>)getControlledPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__CONVENTION_PRAGMA:
				return ((InternalEList<?>)getConventionPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DISCARD_NAMES_PRAGMA:
				return ((InternalEList<?>)getDiscardNamesPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ELABORATE_PRAGMA:
				return ((InternalEList<?>)getElaboratePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ELABORATE_ALL_PRAGMA:
				return ((InternalEList<?>)getElaborateAllPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ELABORATE_BODY_PRAGMA:
				return ((InternalEList<?>)getElaborateBodyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__EXPORT_PRAGMA:
				return ((InternalEList<?>)getExportPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__IMPORT_PRAGMA:
				return ((InternalEList<?>)getImportPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__INLINE_PRAGMA:
				return ((InternalEList<?>)getInlinePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__INSPECTION_POINT_PRAGMA:
				return ((InternalEList<?>)getInspectionPointPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__INTERRUPT_HANDLER_PRAGMA:
				return ((InternalEList<?>)getInterruptHandlerPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__INTERRUPT_PRIORITY_PRAGMA:
				return ((InternalEList<?>)getInterruptPriorityPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__LINKER_OPTIONS_PRAGMA:
				return ((InternalEList<?>)getLinkerOptionsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__LIST_PRAGMA:
				return ((InternalEList<?>)getListPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__LOCKING_POLICY_PRAGMA:
				return ((InternalEList<?>)getLockingPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__NORMALIZE_SCALARS_PRAGMA:
				return ((InternalEList<?>)getNormalizeScalarsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__OPTIMIZE_PRAGMA:
				return ((InternalEList<?>)getOptimizePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PACK_PRAGMA:
				return ((InternalEList<?>)getPackPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PAGE_PRAGMA:
				return ((InternalEList<?>)getPagePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PREELABORATE_PRAGMA:
				return ((InternalEList<?>)getPreelaboratePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PRIORITY_PRAGMA:
				return ((InternalEList<?>)getPriorityPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PURE_PRAGMA:
				return ((InternalEList<?>)getPurePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__QUEUING_POLICY_PRAGMA:
				return ((InternalEList<?>)getQueuingPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				return ((InternalEList<?>)getRemoteCallInterfacePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__REMOTE_TYPES_PRAGMA:
				return ((InternalEList<?>)getRemoteTypesPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__RESTRICTIONS_PRAGMA:
				return ((InternalEList<?>)getRestrictionsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__REVIEWABLE_PRAGMA:
				return ((InternalEList<?>)getReviewablePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__SHARED_PASSIVE_PRAGMA:
				return ((InternalEList<?>)getSharedPassivePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__STORAGE_SIZE_PRAGMA:
				return ((InternalEList<?>)getStorageSizePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__SUPPRESS_PRAGMA:
				return ((InternalEList<?>)getSuppressPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				return ((InternalEList<?>)getTaskDispatchingPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__VOLATILE_PRAGMA:
				return ((InternalEList<?>)getVolatilePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__VOLATILE_COMPONENTS_PRAGMA:
				return ((InternalEList<?>)getVolatileComponentsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ASSERT_PRAGMA:
				return ((InternalEList<?>)getAssertPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__ASSERTION_POLICY_PRAGMA:
				return ((InternalEList<?>)getAssertionPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DETECT_BLOCKING_PRAGMA:
				return ((InternalEList<?>)getDetectBlockingPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__NO_RETURN_PRAGMA:
				return ((InternalEList<?>)getNoReturnPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				return ((InternalEList<?>)getPartitionElaborationPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				return ((InternalEList<?>)getPreelaborableInitializationPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return ((InternalEList<?>)getPrioritySpecificDispatchingPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__PROFILE_PRAGMA:
				return ((InternalEList<?>)getProfilePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__RELATIVE_DEADLINE_PRAGMA:
				return ((InternalEList<?>)getRelativeDeadlinePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__UNCHECKED_UNION_PRAGMA:
				return ((InternalEList<?>)getUncheckedUnionPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__UNSUPPRESS_PRAGMA:
				return ((InternalEList<?>)getUnsuppressPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				return ((InternalEList<?>)getDefaultStoragePoolPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__DISPATCHING_DOMAIN_PRAGMA:
				return ((InternalEList<?>)getDispatchingDomainPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__CPU_PRAGMA:
				return ((InternalEList<?>)getCpuPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__INDEPENDENT_PRAGMA:
				return ((InternalEList<?>)getIndependentPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				return ((InternalEList<?>)getIndependentComponentsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				return ((InternalEList<?>)getImplementationDefinedPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.ELEMENT_LIST__UNKNOWN_PRAGMA:
				return ((InternalEList<?>)getUnknownPragma()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.ELEMENT_LIST__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case AdaPackage.ELEMENT_LIST__NOT_AN_ELEMENT:
				return getNotAnElement();
			case AdaPackage.ELEMENT_LIST__DEFINING_IDENTIFIER:
				return getDefiningIdentifier();
			case AdaPackage.ELEMENT_LIST__DEFINING_CHARACTER_LITERAL:
				return getDefiningCharacterLiteral();
			case AdaPackage.ELEMENT_LIST__DEFINING_ENUMERATION_LITERAL:
				return getDefiningEnumerationLiteral();
			case AdaPackage.ELEMENT_LIST__DEFINING_AND_OPERATOR:
				return getDefiningAndOperator();
			case AdaPackage.ELEMENT_LIST__DEFINING_OR_OPERATOR:
				return getDefiningOrOperator();
			case AdaPackage.ELEMENT_LIST__DEFINING_XOR_OPERATOR:
				return getDefiningXorOperator();
			case AdaPackage.ELEMENT_LIST__DEFINING_EQUAL_OPERATOR:
				return getDefiningEqualOperator();
			case AdaPackage.ELEMENT_LIST__DEFINING_NOT_EQUAL_OPERATOR:
				return getDefiningNotEqualOperator();
			case AdaPackage.ELEMENT_LIST__DEFINING_LESS_THAN_OPERATOR:
				return getDefiningLessThanOperator();
			case AdaPackage.ELEMENT_LIST__DEFINING_LESS_THAN_OR_EQUAL_OPERATOR:
				return getDefiningLessThanOrEqualOperator();
			case AdaPackage.ELEMENT_LIST__DEFINING_GREATER_THAN_OPERATOR:
				return getDefiningGreaterThanOperator();
			case AdaPackage.ELEMENT_LIST__DEFINING_GREATER_THAN_OR_EQUAL_OPERATOR:
				return getDefiningGreaterThanOrEqualOperator();
			case AdaPackage.ELEMENT_LIST__DEFINING_PLUS_OPERATOR:
				return getDefiningPlusOperator();
			case AdaPackage.ELEMENT_LIST__DEFINING_MINUS_OPERATOR:
				return getDefiningMinusOperator();
			case AdaPackage.ELEMENT_LIST__DEFINING_CONCATENATE_OPERATOR:
				return getDefiningConcatenateOperator();
			case AdaPackage.ELEMENT_LIST__DEFINING_UNARY_PLUS_OPERATOR:
				return getDefiningUnaryPlusOperator();
			case AdaPackage.ELEMENT_LIST__DEFINING_UNARY_MINUS_OPERATOR:
				return getDefiningUnaryMinusOperator();
			case AdaPackage.ELEMENT_LIST__DEFINING_MULTIPLY_OPERATOR:
				return getDefiningMultiplyOperator();
			case AdaPackage.ELEMENT_LIST__DEFINING_DIVIDE_OPERATOR:
				return getDefiningDivideOperator();
			case AdaPackage.ELEMENT_LIST__DEFINING_MOD_OPERATOR:
				return getDefiningModOperator();
			case AdaPackage.ELEMENT_LIST__DEFINING_REM_OPERATOR:
				return getDefiningRemOperator();
			case AdaPackage.ELEMENT_LIST__DEFINING_EXPONENTIATE_OPERATOR:
				return getDefiningExponentiateOperator();
			case AdaPackage.ELEMENT_LIST__DEFINING_ABS_OPERATOR:
				return getDefiningAbsOperator();
			case AdaPackage.ELEMENT_LIST__DEFINING_NOT_OPERATOR:
				return getDefiningNotOperator();
			case AdaPackage.ELEMENT_LIST__DEFINING_EXPANDED_NAME:
				return getDefiningExpandedName();
			case AdaPackage.ELEMENT_LIST__ORDINARY_TYPE_DECLARATION:
				return getOrdinaryTypeDeclaration();
			case AdaPackage.ELEMENT_LIST__TASK_TYPE_DECLARATION:
				return getTaskTypeDeclaration();
			case AdaPackage.ELEMENT_LIST__PROTECTED_TYPE_DECLARATION:
				return getProtectedTypeDeclaration();
			case AdaPackage.ELEMENT_LIST__INCOMPLETE_TYPE_DECLARATION:
				return getIncompleteTypeDeclaration();
			case AdaPackage.ELEMENT_LIST__TAGGED_INCOMPLETE_TYPE_DECLARATION:
				return getTaggedIncompleteTypeDeclaration();
			case AdaPackage.ELEMENT_LIST__PRIVATE_TYPE_DECLARATION:
				return getPrivateTypeDeclaration();
			case AdaPackage.ELEMENT_LIST__PRIVATE_EXTENSION_DECLARATION:
				return getPrivateExtensionDeclaration();
			case AdaPackage.ELEMENT_LIST__SUBTYPE_DECLARATION:
				return getSubtypeDeclaration();
			case AdaPackage.ELEMENT_LIST__VARIABLE_DECLARATION:
				return getVariableDeclaration();
			case AdaPackage.ELEMENT_LIST__CONSTANT_DECLARATION:
				return getConstantDeclaration();
			case AdaPackage.ELEMENT_LIST__DEFERRED_CONSTANT_DECLARATION:
				return getDeferredConstantDeclaration();
			case AdaPackage.ELEMENT_LIST__SINGLE_TASK_DECLARATION:
				return getSingleTaskDeclaration();
			case AdaPackage.ELEMENT_LIST__SINGLE_PROTECTED_DECLARATION:
				return getSingleProtectedDeclaration();
			case AdaPackage.ELEMENT_LIST__INTEGER_NUMBER_DECLARATION:
				return getIntegerNumberDeclaration();
			case AdaPackage.ELEMENT_LIST__REAL_NUMBER_DECLARATION:
				return getRealNumberDeclaration();
			case AdaPackage.ELEMENT_LIST__ENUMERATION_LITERAL_SPECIFICATION:
				return getEnumerationLiteralSpecification();
			case AdaPackage.ELEMENT_LIST__DISCRIMINANT_SPECIFICATION:
				return getDiscriminantSpecification();
			case AdaPackage.ELEMENT_LIST__COMPONENT_DECLARATION:
				return getComponentDeclaration();
			case AdaPackage.ELEMENT_LIST__LOOP_PARAMETER_SPECIFICATION:
				return getLoopParameterSpecification();
			case AdaPackage.ELEMENT_LIST__GENERALIZED_ITERATOR_SPECIFICATION:
				return getGeneralizedIteratorSpecification();
			case AdaPackage.ELEMENT_LIST__ELEMENT_ITERATOR_SPECIFICATION:
				return getElementIteratorSpecification();
			case AdaPackage.ELEMENT_LIST__PROCEDURE_DECLARATION:
				return getProcedureDeclaration();
			case AdaPackage.ELEMENT_LIST__FUNCTION_DECLARATION:
				return getFunctionDeclaration();
			case AdaPackage.ELEMENT_LIST__PARAMETER_SPECIFICATION:
				return getParameterSpecification();
			case AdaPackage.ELEMENT_LIST__PROCEDURE_BODY_DECLARATION:
				return getProcedureBodyDeclaration();
			case AdaPackage.ELEMENT_LIST__FUNCTION_BODY_DECLARATION:
				return getFunctionBodyDeclaration();
			case AdaPackage.ELEMENT_LIST__RETURN_VARIABLE_SPECIFICATION:
				return getReturnVariableSpecification();
			case AdaPackage.ELEMENT_LIST__RETURN_CONSTANT_SPECIFICATION:
				return getReturnConstantSpecification();
			case AdaPackage.ELEMENT_LIST__NULL_PROCEDURE_DECLARATION:
				return getNullProcedureDeclaration();
			case AdaPackage.ELEMENT_LIST__EXPRESSION_FUNCTION_DECLARATION:
				return getExpressionFunctionDeclaration();
			case AdaPackage.ELEMENT_LIST__PACKAGE_DECLARATION:
				return getPackageDeclaration();
			case AdaPackage.ELEMENT_LIST__PACKAGE_BODY_DECLARATION:
				return getPackageBodyDeclaration();
			case AdaPackage.ELEMENT_LIST__OBJECT_RENAMING_DECLARATION:
				return getObjectRenamingDeclaration();
			case AdaPackage.ELEMENT_LIST__EXCEPTION_RENAMING_DECLARATION:
				return getExceptionRenamingDeclaration();
			case AdaPackage.ELEMENT_LIST__PACKAGE_RENAMING_DECLARATION:
				return getPackageRenamingDeclaration();
			case AdaPackage.ELEMENT_LIST__PROCEDURE_RENAMING_DECLARATION:
				return getProcedureRenamingDeclaration();
			case AdaPackage.ELEMENT_LIST__FUNCTION_RENAMING_DECLARATION:
				return getFunctionRenamingDeclaration();
			case AdaPackage.ELEMENT_LIST__GENERIC_PACKAGE_RENAMING_DECLARATION:
				return getGenericPackageRenamingDeclaration();
			case AdaPackage.ELEMENT_LIST__GENERIC_PROCEDURE_RENAMING_DECLARATION:
				return getGenericProcedureRenamingDeclaration();
			case AdaPackage.ELEMENT_LIST__GENERIC_FUNCTION_RENAMING_DECLARATION:
				return getGenericFunctionRenamingDeclaration();
			case AdaPackage.ELEMENT_LIST__TASK_BODY_DECLARATION:
				return getTaskBodyDeclaration();
			case AdaPackage.ELEMENT_LIST__PROTECTED_BODY_DECLARATION:
				return getProtectedBodyDeclaration();
			case AdaPackage.ELEMENT_LIST__ENTRY_DECLARATION:
				return getEntryDeclaration();
			case AdaPackage.ELEMENT_LIST__ENTRY_BODY_DECLARATION:
				return getEntryBodyDeclaration();
			case AdaPackage.ELEMENT_LIST__ENTRY_INDEX_SPECIFICATION:
				return getEntryIndexSpecification();
			case AdaPackage.ELEMENT_LIST__PROCEDURE_BODY_STUB:
				return getProcedureBodyStub();
			case AdaPackage.ELEMENT_LIST__FUNCTION_BODY_STUB:
				return getFunctionBodyStub();
			case AdaPackage.ELEMENT_LIST__PACKAGE_BODY_STUB:
				return getPackageBodyStub();
			case AdaPackage.ELEMENT_LIST__TASK_BODY_STUB:
				return getTaskBodyStub();
			case AdaPackage.ELEMENT_LIST__PROTECTED_BODY_STUB:
				return getProtectedBodyStub();
			case AdaPackage.ELEMENT_LIST__EXCEPTION_DECLARATION:
				return getExceptionDeclaration();
			case AdaPackage.ELEMENT_LIST__CHOICE_PARAMETER_SPECIFICATION:
				return getChoiceParameterSpecification();
			case AdaPackage.ELEMENT_LIST__GENERIC_PROCEDURE_DECLARATION:
				return getGenericProcedureDeclaration();
			case AdaPackage.ELEMENT_LIST__GENERIC_FUNCTION_DECLARATION:
				return getGenericFunctionDeclaration();
			case AdaPackage.ELEMENT_LIST__GENERIC_PACKAGE_DECLARATION:
				return getGenericPackageDeclaration();
			case AdaPackage.ELEMENT_LIST__PACKAGE_INSTANTIATION:
				return getPackageInstantiation();
			case AdaPackage.ELEMENT_LIST__PROCEDURE_INSTANTIATION:
				return getProcedureInstantiation();
			case AdaPackage.ELEMENT_LIST__FUNCTION_INSTANTIATION:
				return getFunctionInstantiation();
			case AdaPackage.ELEMENT_LIST__FORMAL_OBJECT_DECLARATION:
				return getFormalObjectDeclaration();
			case AdaPackage.ELEMENT_LIST__FORMAL_TYPE_DECLARATION:
				return getFormalTypeDeclaration();
			case AdaPackage.ELEMENT_LIST__FORMAL_INCOMPLETE_TYPE_DECLARATION:
				return getFormalIncompleteTypeDeclaration();
			case AdaPackage.ELEMENT_LIST__FORMAL_PROCEDURE_DECLARATION:
				return getFormalProcedureDeclaration();
			case AdaPackage.ELEMENT_LIST__FORMAL_FUNCTION_DECLARATION:
				return getFormalFunctionDeclaration();
			case AdaPackage.ELEMENT_LIST__FORMAL_PACKAGE_DECLARATION:
				return getFormalPackageDeclaration();
			case AdaPackage.ELEMENT_LIST__FORMAL_PACKAGE_DECLARATION_WITH_BOX:
				return getFormalPackageDeclarationWithBox();
			case AdaPackage.ELEMENT_LIST__DERIVED_TYPE_DEFINITION:
				return getDerivedTypeDefinition();
			case AdaPackage.ELEMENT_LIST__DERIVED_RECORD_EXTENSION_DEFINITION:
				return getDerivedRecordExtensionDefinition();
			case AdaPackage.ELEMENT_LIST__ENUMERATION_TYPE_DEFINITION:
				return getEnumerationTypeDefinition();
			case AdaPackage.ELEMENT_LIST__SIGNED_INTEGER_TYPE_DEFINITION:
				return getSignedIntegerTypeDefinition();
			case AdaPackage.ELEMENT_LIST__MODULAR_TYPE_DEFINITION:
				return getModularTypeDefinition();
			case AdaPackage.ELEMENT_LIST__ROOT_INTEGER_DEFINITION:
				return getRootIntegerDefinition();
			case AdaPackage.ELEMENT_LIST__ROOT_REAL_DEFINITION:
				return getRootRealDefinition();
			case AdaPackage.ELEMENT_LIST__UNIVERSAL_INTEGER_DEFINITION:
				return getUniversalIntegerDefinition();
			case AdaPackage.ELEMENT_LIST__UNIVERSAL_REAL_DEFINITION:
				return getUniversalRealDefinition();
			case AdaPackage.ELEMENT_LIST__UNIVERSAL_FIXED_DEFINITION:
				return getUniversalFixedDefinition();
			case AdaPackage.ELEMENT_LIST__FLOATING_POINT_DEFINITION:
				return getFloatingPointDefinition();
			case AdaPackage.ELEMENT_LIST__ORDINARY_FIXED_POINT_DEFINITION:
				return getOrdinaryFixedPointDefinition();
			case AdaPackage.ELEMENT_LIST__DECIMAL_FIXED_POINT_DEFINITION:
				return getDecimalFixedPointDefinition();
			case AdaPackage.ELEMENT_LIST__UNCONSTRAINED_ARRAY_DEFINITION:
				return getUnconstrainedArrayDefinition();
			case AdaPackage.ELEMENT_LIST__CONSTRAINED_ARRAY_DEFINITION:
				return getConstrainedArrayDefinition();
			case AdaPackage.ELEMENT_LIST__RECORD_TYPE_DEFINITION:
				return getRecordTypeDefinition();
			case AdaPackage.ELEMENT_LIST__TAGGED_RECORD_TYPE_DEFINITION:
				return getTaggedRecordTypeDefinition();
			case AdaPackage.ELEMENT_LIST__ORDINARY_INTERFACE:
				return getOrdinaryInterface();
			case AdaPackage.ELEMENT_LIST__LIMITED_INTERFACE:
				return getLimitedInterface();
			case AdaPackage.ELEMENT_LIST__TASK_INTERFACE:
				return getTaskInterface();
			case AdaPackage.ELEMENT_LIST__PROTECTED_INTERFACE:
				return getProtectedInterface();
			case AdaPackage.ELEMENT_LIST__SYNCHRONIZED_INTERFACE:
				return getSynchronizedInterface();
			case AdaPackage.ELEMENT_LIST__POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				return getPoolSpecificAccessToVariable();
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_VARIABLE:
				return getAccessToVariable();
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_CONSTANT:
				return getAccessToConstant();
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_PROCEDURE:
				return getAccessToProcedure();
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_PROTECTED_PROCEDURE:
				return getAccessToProtectedProcedure();
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_FUNCTION:
				return getAccessToFunction();
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_PROTECTED_FUNCTION:
				return getAccessToProtectedFunction();
			case AdaPackage.ELEMENT_LIST__SUBTYPE_INDICATION:
				return getSubtypeIndication();
			case AdaPackage.ELEMENT_LIST__RANGE_ATTRIBUTE_REFERENCE:
				return getRangeAttributeReference();
			case AdaPackage.ELEMENT_LIST__SIMPLE_EXPRESSION_RANGE:
				return getSimpleExpressionRange();
			case AdaPackage.ELEMENT_LIST__DIGITS_CONSTRAINT:
				return getDigitsConstraint();
			case AdaPackage.ELEMENT_LIST__DELTA_CONSTRAINT:
				return getDeltaConstraint();
			case AdaPackage.ELEMENT_LIST__INDEX_CONSTRAINT:
				return getIndexConstraint();
			case AdaPackage.ELEMENT_LIST__DISCRIMINANT_CONSTRAINT:
				return getDiscriminantConstraint();
			case AdaPackage.ELEMENT_LIST__COMPONENT_DEFINITION:
				return getComponentDefinition();
			case AdaPackage.ELEMENT_LIST__DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION:
				return getDiscreteSubtypeIndicationAsSubtypeDefinition();
			case AdaPackage.ELEMENT_LIST__DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION:
				return getDiscreteRangeAttributeReferenceAsSubtypeDefinition();
			case AdaPackage.ELEMENT_LIST__DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION:
				return getDiscreteSimpleExpressionRangeAsSubtypeDefinition();
			case AdaPackage.ELEMENT_LIST__DISCRETE_SUBTYPE_INDICATION:
				return getDiscreteSubtypeIndication();
			case AdaPackage.ELEMENT_LIST__DISCRETE_RANGE_ATTRIBUTE_REFERENCE:
				return getDiscreteRangeAttributeReference();
			case AdaPackage.ELEMENT_LIST__DISCRETE_SIMPLE_EXPRESSION_RANGE:
				return getDiscreteSimpleExpressionRange();
			case AdaPackage.ELEMENT_LIST__UNKNOWN_DISCRIMINANT_PART:
				return getUnknownDiscriminantPart();
			case AdaPackage.ELEMENT_LIST__KNOWN_DISCRIMINANT_PART:
				return getKnownDiscriminantPart();
			case AdaPackage.ELEMENT_LIST__RECORD_DEFINITION:
				return getRecordDefinition();
			case AdaPackage.ELEMENT_LIST__NULL_RECORD_DEFINITION:
				return getNullRecordDefinition();
			case AdaPackage.ELEMENT_LIST__NULL_COMPONENT:
				return getNullComponent();
			case AdaPackage.ELEMENT_LIST__VARIANT_PART:
				return getVariantPart();
			case AdaPackage.ELEMENT_LIST__VARIANT:
				return getVariant();
			case AdaPackage.ELEMENT_LIST__OTHERS_CHOICE:
				return getOthersChoice();
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_VARIABLE:
				return getAnonymousAccessToVariable();
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_CONSTANT:
				return getAnonymousAccessToConstant();
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_PROCEDURE:
				return getAnonymousAccessToProcedure();
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE:
				return getAnonymousAccessToProtectedProcedure();
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_FUNCTION:
				return getAnonymousAccessToFunction();
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION:
				return getAnonymousAccessToProtectedFunction();
			case AdaPackage.ELEMENT_LIST__PRIVATE_TYPE_DEFINITION:
				return getPrivateTypeDefinition();
			case AdaPackage.ELEMENT_LIST__TAGGED_PRIVATE_TYPE_DEFINITION:
				return getTaggedPrivateTypeDefinition();
			case AdaPackage.ELEMENT_LIST__PRIVATE_EXTENSION_DEFINITION:
				return getPrivateExtensionDefinition();
			case AdaPackage.ELEMENT_LIST__TASK_DEFINITION:
				return getTaskDefinition();
			case AdaPackage.ELEMENT_LIST__PROTECTED_DEFINITION:
				return getProtectedDefinition();
			case AdaPackage.ELEMENT_LIST__FORMAL_PRIVATE_TYPE_DEFINITION:
				return getFormalPrivateTypeDefinition();
			case AdaPackage.ELEMENT_LIST__FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION:
				return getFormalTaggedPrivateTypeDefinition();
			case AdaPackage.ELEMENT_LIST__FORMAL_DERIVED_TYPE_DEFINITION:
				return getFormalDerivedTypeDefinition();
			case AdaPackage.ELEMENT_LIST__FORMAL_DISCRETE_TYPE_DEFINITION:
				return getFormalDiscreteTypeDefinition();
			case AdaPackage.ELEMENT_LIST__FORMAL_SIGNED_INTEGER_TYPE_DEFINITION:
				return getFormalSignedIntegerTypeDefinition();
			case AdaPackage.ELEMENT_LIST__FORMAL_MODULAR_TYPE_DEFINITION:
				return getFormalModularTypeDefinition();
			case AdaPackage.ELEMENT_LIST__FORMAL_FLOATING_POINT_DEFINITION:
				return getFormalFloatingPointDefinition();
			case AdaPackage.ELEMENT_LIST__FORMAL_ORDINARY_FIXED_POINT_DEFINITION:
				return getFormalOrdinaryFixedPointDefinition();
			case AdaPackage.ELEMENT_LIST__FORMAL_DECIMAL_FIXED_POINT_DEFINITION:
				return getFormalDecimalFixedPointDefinition();
			case AdaPackage.ELEMENT_LIST__FORMAL_ORDINARY_INTERFACE:
				return getFormalOrdinaryInterface();
			case AdaPackage.ELEMENT_LIST__FORMAL_LIMITED_INTERFACE:
				return getFormalLimitedInterface();
			case AdaPackage.ELEMENT_LIST__FORMAL_TASK_INTERFACE:
				return getFormalTaskInterface();
			case AdaPackage.ELEMENT_LIST__FORMAL_PROTECTED_INTERFACE:
				return getFormalProtectedInterface();
			case AdaPackage.ELEMENT_LIST__FORMAL_SYNCHRONIZED_INTERFACE:
				return getFormalSynchronizedInterface();
			case AdaPackage.ELEMENT_LIST__FORMAL_UNCONSTRAINED_ARRAY_DEFINITION:
				return getFormalUnconstrainedArrayDefinition();
			case AdaPackage.ELEMENT_LIST__FORMAL_CONSTRAINED_ARRAY_DEFINITION:
				return getFormalConstrainedArrayDefinition();
			case AdaPackage.ELEMENT_LIST__FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				return getFormalPoolSpecificAccessToVariable();
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_VARIABLE:
				return getFormalAccessToVariable();
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_CONSTANT:
				return getFormalAccessToConstant();
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_PROCEDURE:
				return getFormalAccessToProcedure();
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_PROTECTED_PROCEDURE:
				return getFormalAccessToProtectedProcedure();
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_FUNCTION:
				return getFormalAccessToFunction();
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_PROTECTED_FUNCTION:
				return getFormalAccessToProtectedFunction();
			case AdaPackage.ELEMENT_LIST__ASPECT_SPECIFICATION:
				return getAspectSpecification();
			case AdaPackage.ELEMENT_LIST__BOX_EXPRESSION:
				return getBoxExpression();
			case AdaPackage.ELEMENT_LIST__INTEGER_LITERAL:
				return getIntegerLiteral();
			case AdaPackage.ELEMENT_LIST__REAL_LITERAL:
				return getRealLiteral();
			case AdaPackage.ELEMENT_LIST__STRING_LITERAL:
				return getStringLiteral();
			case AdaPackage.ELEMENT_LIST__IDENTIFIER:
				return getIdentifier();
			case AdaPackage.ELEMENT_LIST__AND_OPERATOR:
				return getAndOperator();
			case AdaPackage.ELEMENT_LIST__OR_OPERATOR:
				return getOrOperator();
			case AdaPackage.ELEMENT_LIST__XOR_OPERATOR:
				return getXorOperator();
			case AdaPackage.ELEMENT_LIST__EQUAL_OPERATOR:
				return getEqualOperator();
			case AdaPackage.ELEMENT_LIST__NOT_EQUAL_OPERATOR:
				return getNotEqualOperator();
			case AdaPackage.ELEMENT_LIST__LESS_THAN_OPERATOR:
				return getLessThanOperator();
			case AdaPackage.ELEMENT_LIST__LESS_THAN_OR_EQUAL_OPERATOR:
				return getLessThanOrEqualOperator();
			case AdaPackage.ELEMENT_LIST__GREATER_THAN_OPERATOR:
				return getGreaterThanOperator();
			case AdaPackage.ELEMENT_LIST__GREATER_THAN_OR_EQUAL_OPERATOR:
				return getGreaterThanOrEqualOperator();
			case AdaPackage.ELEMENT_LIST__PLUS_OPERATOR:
				return getPlusOperator();
			case AdaPackage.ELEMENT_LIST__MINUS_OPERATOR:
				return getMinusOperator();
			case AdaPackage.ELEMENT_LIST__CONCATENATE_OPERATOR:
				return getConcatenateOperator();
			case AdaPackage.ELEMENT_LIST__UNARY_PLUS_OPERATOR:
				return getUnaryPlusOperator();
			case AdaPackage.ELEMENT_LIST__UNARY_MINUS_OPERATOR:
				return getUnaryMinusOperator();
			case AdaPackage.ELEMENT_LIST__MULTIPLY_OPERATOR:
				return getMultiplyOperator();
			case AdaPackage.ELEMENT_LIST__DIVIDE_OPERATOR:
				return getDivideOperator();
			case AdaPackage.ELEMENT_LIST__MOD_OPERATOR:
				return getModOperator();
			case AdaPackage.ELEMENT_LIST__REM_OPERATOR:
				return getRemOperator();
			case AdaPackage.ELEMENT_LIST__EXPONENTIATE_OPERATOR:
				return getExponentiateOperator();
			case AdaPackage.ELEMENT_LIST__ABS_OPERATOR:
				return getAbsOperator();
			case AdaPackage.ELEMENT_LIST__NOT_OPERATOR:
				return getNotOperator();
			case AdaPackage.ELEMENT_LIST__CHARACTER_LITERAL:
				return getCharacterLiteral();
			case AdaPackage.ELEMENT_LIST__ENUMERATION_LITERAL:
				return getEnumerationLiteral();
			case AdaPackage.ELEMENT_LIST__EXPLICIT_DEREFERENCE:
				return getExplicitDereference();
			case AdaPackage.ELEMENT_LIST__FUNCTION_CALL:
				return getFunctionCall();
			case AdaPackage.ELEMENT_LIST__INDEXED_COMPONENT:
				return getIndexedComponent();
			case AdaPackage.ELEMENT_LIST__SLICE:
				return getSlice();
			case AdaPackage.ELEMENT_LIST__SELECTED_COMPONENT:
				return getSelectedComponent();
			case AdaPackage.ELEMENT_LIST__ACCESS_ATTRIBUTE:
				return getAccessAttribute();
			case AdaPackage.ELEMENT_LIST__ADDRESS_ATTRIBUTE:
				return getAddressAttribute();
			case AdaPackage.ELEMENT_LIST__ADJACENT_ATTRIBUTE:
				return getAdjacentAttribute();
			case AdaPackage.ELEMENT_LIST__AFT_ATTRIBUTE:
				return getAftAttribute();
			case AdaPackage.ELEMENT_LIST__ALIGNMENT_ATTRIBUTE:
				return getAlignmentAttribute();
			case AdaPackage.ELEMENT_LIST__BASE_ATTRIBUTE:
				return getBaseAttribute();
			case AdaPackage.ELEMENT_LIST__BIT_ORDER_ATTRIBUTE:
				return getBitOrderAttribute();
			case AdaPackage.ELEMENT_LIST__BODY_VERSION_ATTRIBUTE:
				return getBodyVersionAttribute();
			case AdaPackage.ELEMENT_LIST__CALLABLE_ATTRIBUTE:
				return getCallableAttribute();
			case AdaPackage.ELEMENT_LIST__CALLER_ATTRIBUTE:
				return getCallerAttribute();
			case AdaPackage.ELEMENT_LIST__CEILING_ATTRIBUTE:
				return getCeilingAttribute();
			case AdaPackage.ELEMENT_LIST__CLASS_ATTRIBUTE:
				return getClassAttribute();
			case AdaPackage.ELEMENT_LIST__COMPONENT_SIZE_ATTRIBUTE:
				return getComponentSizeAttribute();
			case AdaPackage.ELEMENT_LIST__COMPOSE_ATTRIBUTE:
				return getComposeAttribute();
			case AdaPackage.ELEMENT_LIST__CONSTRAINED_ATTRIBUTE:
				return getConstrainedAttribute();
			case AdaPackage.ELEMENT_LIST__COPY_SIGN_ATTRIBUTE:
				return getCopySignAttribute();
			case AdaPackage.ELEMENT_LIST__COUNT_ATTRIBUTE:
				return getCountAttribute();
			case AdaPackage.ELEMENT_LIST__DEFINITE_ATTRIBUTE:
				return getDefiniteAttribute();
			case AdaPackage.ELEMENT_LIST__DELTA_ATTRIBUTE:
				return getDeltaAttribute();
			case AdaPackage.ELEMENT_LIST__DENORM_ATTRIBUTE:
				return getDenormAttribute();
			case AdaPackage.ELEMENT_LIST__DIGITS_ATTRIBUTE:
				return getDigitsAttribute();
			case AdaPackage.ELEMENT_LIST__EXPONENT_ATTRIBUTE:
				return getExponentAttribute();
			case AdaPackage.ELEMENT_LIST__EXTERNAL_TAG_ATTRIBUTE:
				return getExternalTagAttribute();
			case AdaPackage.ELEMENT_LIST__FIRST_ATTRIBUTE:
				return getFirstAttribute();
			case AdaPackage.ELEMENT_LIST__FIRST_BIT_ATTRIBUTE:
				return getFirstBitAttribute();
			case AdaPackage.ELEMENT_LIST__FLOOR_ATTRIBUTE:
				return getFloorAttribute();
			case AdaPackage.ELEMENT_LIST__FORE_ATTRIBUTE:
				return getForeAttribute();
			case AdaPackage.ELEMENT_LIST__FRACTION_ATTRIBUTE:
				return getFractionAttribute();
			case AdaPackage.ELEMENT_LIST__IDENTITY_ATTRIBUTE:
				return getIdentityAttribute();
			case AdaPackage.ELEMENT_LIST__IMAGE_ATTRIBUTE:
				return getImageAttribute();
			case AdaPackage.ELEMENT_LIST__INPUT_ATTRIBUTE:
				return getInputAttribute();
			case AdaPackage.ELEMENT_LIST__LAST_ATTRIBUTE:
				return getLastAttribute();
			case AdaPackage.ELEMENT_LIST__LAST_BIT_ATTRIBUTE:
				return getLastBitAttribute();
			case AdaPackage.ELEMENT_LIST__LEADING_PART_ATTRIBUTE:
				return getLeadingPartAttribute();
			case AdaPackage.ELEMENT_LIST__LENGTH_ATTRIBUTE:
				return getLengthAttribute();
			case AdaPackage.ELEMENT_LIST__MACHINE_ATTRIBUTE:
				return getMachineAttribute();
			case AdaPackage.ELEMENT_LIST__MACHINE_EMAX_ATTRIBUTE:
				return getMachineEmaxAttribute();
			case AdaPackage.ELEMENT_LIST__MACHINE_EMIN_ATTRIBUTE:
				return getMachineEminAttribute();
			case AdaPackage.ELEMENT_LIST__MACHINE_MANTISSA_ATTRIBUTE:
				return getMachineMantissaAttribute();
			case AdaPackage.ELEMENT_LIST__MACHINE_OVERFLOWS_ATTRIBUTE:
				return getMachineOverflowsAttribute();
			case AdaPackage.ELEMENT_LIST__MACHINE_RADIX_ATTRIBUTE:
				return getMachineRadixAttribute();
			case AdaPackage.ELEMENT_LIST__MACHINE_ROUNDS_ATTRIBUTE:
				return getMachineRoundsAttribute();
			case AdaPackage.ELEMENT_LIST__MAX_ATTRIBUTE:
				return getMaxAttribute();
			case AdaPackage.ELEMENT_LIST__MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE:
				return getMaxSizeInStorageElementsAttribute();
			case AdaPackage.ELEMENT_LIST__MIN_ATTRIBUTE:
				return getMinAttribute();
			case AdaPackage.ELEMENT_LIST__MODEL_ATTRIBUTE:
				return getModelAttribute();
			case AdaPackage.ELEMENT_LIST__MODEL_EMIN_ATTRIBUTE:
				return getModelEminAttribute();
			case AdaPackage.ELEMENT_LIST__MODEL_EPSILON_ATTRIBUTE:
				return getModelEpsilonAttribute();
			case AdaPackage.ELEMENT_LIST__MODEL_MANTISSA_ATTRIBUTE:
				return getModelMantissaAttribute();
			case AdaPackage.ELEMENT_LIST__MODEL_SMALL_ATTRIBUTE:
				return getModelSmallAttribute();
			case AdaPackage.ELEMENT_LIST__MODULUS_ATTRIBUTE:
				return getModulusAttribute();
			case AdaPackage.ELEMENT_LIST__OUTPUT_ATTRIBUTE:
				return getOutputAttribute();
			case AdaPackage.ELEMENT_LIST__PARTITION_ID_ATTRIBUTE:
				return getPartitionIdAttribute();
			case AdaPackage.ELEMENT_LIST__POS_ATTRIBUTE:
				return getPosAttribute();
			case AdaPackage.ELEMENT_LIST__POSITION_ATTRIBUTE:
				return getPositionAttribute();
			case AdaPackage.ELEMENT_LIST__PRED_ATTRIBUTE:
				return getPredAttribute();
			case AdaPackage.ELEMENT_LIST__RANGE_ATTRIBUTE:
				return getRangeAttribute();
			case AdaPackage.ELEMENT_LIST__READ_ATTRIBUTE:
				return getReadAttribute();
			case AdaPackage.ELEMENT_LIST__REMAINDER_ATTRIBUTE:
				return getRemainderAttribute();
			case AdaPackage.ELEMENT_LIST__ROUND_ATTRIBUTE:
				return getRoundAttribute();
			case AdaPackage.ELEMENT_LIST__ROUNDING_ATTRIBUTE:
				return getRoundingAttribute();
			case AdaPackage.ELEMENT_LIST__SAFE_FIRST_ATTRIBUTE:
				return getSafeFirstAttribute();
			case AdaPackage.ELEMENT_LIST__SAFE_LAST_ATTRIBUTE:
				return getSafeLastAttribute();
			case AdaPackage.ELEMENT_LIST__SCALE_ATTRIBUTE:
				return getScaleAttribute();
			case AdaPackage.ELEMENT_LIST__SCALING_ATTRIBUTE:
				return getScalingAttribute();
			case AdaPackage.ELEMENT_LIST__SIGNED_ZEROS_ATTRIBUTE:
				return getSignedZerosAttribute();
			case AdaPackage.ELEMENT_LIST__SIZE_ATTRIBUTE:
				return getSizeAttribute();
			case AdaPackage.ELEMENT_LIST__SMALL_ATTRIBUTE:
				return getSmallAttribute();
			case AdaPackage.ELEMENT_LIST__STORAGE_POOL_ATTRIBUTE:
				return getStoragePoolAttribute();
			case AdaPackage.ELEMENT_LIST__STORAGE_SIZE_ATTRIBUTE:
				return getStorageSizeAttribute();
			case AdaPackage.ELEMENT_LIST__SUCC_ATTRIBUTE:
				return getSuccAttribute();
			case AdaPackage.ELEMENT_LIST__TAG_ATTRIBUTE:
				return getTagAttribute();
			case AdaPackage.ELEMENT_LIST__TERMINATED_ATTRIBUTE:
				return getTerminatedAttribute();
			case AdaPackage.ELEMENT_LIST__TRUNCATION_ATTRIBUTE:
				return getTruncationAttribute();
			case AdaPackage.ELEMENT_LIST__UNBIASED_ROUNDING_ATTRIBUTE:
				return getUnbiasedRoundingAttribute();
			case AdaPackage.ELEMENT_LIST__UNCHECKED_ACCESS_ATTRIBUTE:
				return getUncheckedAccessAttribute();
			case AdaPackage.ELEMENT_LIST__VAL_ATTRIBUTE:
				return getValAttribute();
			case AdaPackage.ELEMENT_LIST__VALID_ATTRIBUTE:
				return getValidAttribute();
			case AdaPackage.ELEMENT_LIST__VALUE_ATTRIBUTE:
				return getValueAttribute();
			case AdaPackage.ELEMENT_LIST__VERSION_ATTRIBUTE:
				return getVersionAttribute();
			case AdaPackage.ELEMENT_LIST__WIDE_IMAGE_ATTRIBUTE:
				return getWideImageAttribute();
			case AdaPackage.ELEMENT_LIST__WIDE_VALUE_ATTRIBUTE:
				return getWideValueAttribute();
			case AdaPackage.ELEMENT_LIST__WIDE_WIDTH_ATTRIBUTE:
				return getWideWidthAttribute();
			case AdaPackage.ELEMENT_LIST__WIDTH_ATTRIBUTE:
				return getWidthAttribute();
			case AdaPackage.ELEMENT_LIST__WRITE_ATTRIBUTE:
				return getWriteAttribute();
			case AdaPackage.ELEMENT_LIST__MACHINE_ROUNDING_ATTRIBUTE:
				return getMachineRoundingAttribute();
			case AdaPackage.ELEMENT_LIST__MOD_ATTRIBUTE:
				return getModAttribute();
			case AdaPackage.ELEMENT_LIST__PRIORITY_ATTRIBUTE:
				return getPriorityAttribute();
			case AdaPackage.ELEMENT_LIST__STREAM_SIZE_ATTRIBUTE:
				return getStreamSizeAttribute();
			case AdaPackage.ELEMENT_LIST__WIDE_WIDE_IMAGE_ATTRIBUTE:
				return getWideWideImageAttribute();
			case AdaPackage.ELEMENT_LIST__WIDE_WIDE_VALUE_ATTRIBUTE:
				return getWideWideValueAttribute();
			case AdaPackage.ELEMENT_LIST__WIDE_WIDE_WIDTH_ATTRIBUTE:
				return getWideWideWidthAttribute();
			case AdaPackage.ELEMENT_LIST__MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE:
				return getMaxAlignmentForAllocationAttribute();
			case AdaPackage.ELEMENT_LIST__OVERLAPS_STORAGE_ATTRIBUTE:
				return getOverlapsStorageAttribute();
			case AdaPackage.ELEMENT_LIST__IMPLEMENTATION_DEFINED_ATTRIBUTE:
				return getImplementationDefinedAttribute();
			case AdaPackage.ELEMENT_LIST__UNKNOWN_ATTRIBUTE:
				return getUnknownAttribute();
			case AdaPackage.ELEMENT_LIST__RECORD_AGGREGATE:
				return getRecordAggregate();
			case AdaPackage.ELEMENT_LIST__EXTENSION_AGGREGATE:
				return getExtensionAggregate();
			case AdaPackage.ELEMENT_LIST__POSITIONAL_ARRAY_AGGREGATE:
				return getPositionalArrayAggregate();
			case AdaPackage.ELEMENT_LIST__NAMED_ARRAY_AGGREGATE:
				return getNamedArrayAggregate();
			case AdaPackage.ELEMENT_LIST__AND_THEN_SHORT_CIRCUIT:
				return getAndThenShortCircuit();
			case AdaPackage.ELEMENT_LIST__OR_ELSE_SHORT_CIRCUIT:
				return getOrElseShortCircuit();
			case AdaPackage.ELEMENT_LIST__IN_MEMBERSHIP_TEST:
				return getInMembershipTest();
			case AdaPackage.ELEMENT_LIST__NOT_IN_MEMBERSHIP_TEST:
				return getNotInMembershipTest();
			case AdaPackage.ELEMENT_LIST__NULL_LITERAL:
				return getNullLiteral();
			case AdaPackage.ELEMENT_LIST__PARENTHESIZED_EXPRESSION:
				return getParenthesizedExpression();
			case AdaPackage.ELEMENT_LIST__RAISE_EXPRESSION:
				return getRaiseExpression();
			case AdaPackage.ELEMENT_LIST__TYPE_CONVERSION:
				return getTypeConversion();
			case AdaPackage.ELEMENT_LIST__QUALIFIED_EXPRESSION:
				return getQualifiedExpression();
			case AdaPackage.ELEMENT_LIST__ALLOCATION_FROM_SUBTYPE:
				return getAllocationFromSubtype();
			case AdaPackage.ELEMENT_LIST__ALLOCATION_FROM_QUALIFIED_EXPRESSION:
				return getAllocationFromQualifiedExpression();
			case AdaPackage.ELEMENT_LIST__CASE_EXPRESSION:
				return getCaseExpression();
			case AdaPackage.ELEMENT_LIST__IF_EXPRESSION:
				return getIfExpression();
			case AdaPackage.ELEMENT_LIST__FOR_ALL_QUANTIFIED_EXPRESSION:
				return getForAllQuantifiedExpression();
			case AdaPackage.ELEMENT_LIST__FOR_SOME_QUANTIFIED_EXPRESSION:
				return getForSomeQuantifiedExpression();
			case AdaPackage.ELEMENT_LIST__PRAGMA_ARGUMENT_ASSOCIATION:
				return getPragmaArgumentAssociation();
			case AdaPackage.ELEMENT_LIST__DISCRIMINANT_ASSOCIATION:
				return getDiscriminantAssociation();
			case AdaPackage.ELEMENT_LIST__RECORD_COMPONENT_ASSOCIATION:
				return getRecordComponentAssociation();
			case AdaPackage.ELEMENT_LIST__ARRAY_COMPONENT_ASSOCIATION:
				return getArrayComponentAssociation();
			case AdaPackage.ELEMENT_LIST__PARAMETER_ASSOCIATION:
				return getParameterAssociation();
			case AdaPackage.ELEMENT_LIST__GENERIC_ASSOCIATION:
				return getGenericAssociation();
			case AdaPackage.ELEMENT_LIST__NULL_STATEMENT:
				return getNullStatement();
			case AdaPackage.ELEMENT_LIST__ASSIGNMENT_STATEMENT:
				return getAssignmentStatement();
			case AdaPackage.ELEMENT_LIST__IF_STATEMENT:
				return getIfStatement();
			case AdaPackage.ELEMENT_LIST__CASE_STATEMENT:
				return getCaseStatement();
			case AdaPackage.ELEMENT_LIST__LOOP_STATEMENT:
				return getLoopStatement();
			case AdaPackage.ELEMENT_LIST__WHILE_LOOP_STATEMENT:
				return getWhileLoopStatement();
			case AdaPackage.ELEMENT_LIST__FOR_LOOP_STATEMENT:
				return getForLoopStatement();
			case AdaPackage.ELEMENT_LIST__BLOCK_STATEMENT:
				return getBlockStatement();
			case AdaPackage.ELEMENT_LIST__EXIT_STATEMENT:
				return getExitStatement();
			case AdaPackage.ELEMENT_LIST__GOTO_STATEMENT:
				return getGotoStatement();
			case AdaPackage.ELEMENT_LIST__PROCEDURE_CALL_STATEMENT:
				return getProcedureCallStatement();
			case AdaPackage.ELEMENT_LIST__RETURN_STATEMENT:
				return getReturnStatement();
			case AdaPackage.ELEMENT_LIST__EXTENDED_RETURN_STATEMENT:
				return getExtendedReturnStatement();
			case AdaPackage.ELEMENT_LIST__ACCEPT_STATEMENT:
				return getAcceptStatement();
			case AdaPackage.ELEMENT_LIST__ENTRY_CALL_STATEMENT:
				return getEntryCallStatement();
			case AdaPackage.ELEMENT_LIST__REQUEUE_STATEMENT:
				return getRequeueStatement();
			case AdaPackage.ELEMENT_LIST__REQUEUE_STATEMENT_WITH_ABORT:
				return getRequeueStatementWithAbort();
			case AdaPackage.ELEMENT_LIST__DELAY_UNTIL_STATEMENT:
				return getDelayUntilStatement();
			case AdaPackage.ELEMENT_LIST__DELAY_RELATIVE_STATEMENT:
				return getDelayRelativeStatement();
			case AdaPackage.ELEMENT_LIST__TERMINATE_ALTERNATIVE_STATEMENT:
				return getTerminateAlternativeStatement();
			case AdaPackage.ELEMENT_LIST__SELECTIVE_ACCEPT_STATEMENT:
				return getSelectiveAcceptStatement();
			case AdaPackage.ELEMENT_LIST__TIMED_ENTRY_CALL_STATEMENT:
				return getTimedEntryCallStatement();
			case AdaPackage.ELEMENT_LIST__CONDITIONAL_ENTRY_CALL_STATEMENT:
				return getConditionalEntryCallStatement();
			case AdaPackage.ELEMENT_LIST__ASYNCHRONOUS_SELECT_STATEMENT:
				return getAsynchronousSelectStatement();
			case AdaPackage.ELEMENT_LIST__ABORT_STATEMENT:
				return getAbortStatement();
			case AdaPackage.ELEMENT_LIST__RAISE_STATEMENT:
				return getRaiseStatement();
			case AdaPackage.ELEMENT_LIST__CODE_STATEMENT:
				return getCodeStatement();
			case AdaPackage.ELEMENT_LIST__IF_PATH:
				return getIfPath();
			case AdaPackage.ELEMENT_LIST__ELSIF_PATH:
				return getElsifPath();
			case AdaPackage.ELEMENT_LIST__ELSE_PATH:
				return getElsePath();
			case AdaPackage.ELEMENT_LIST__CASE_PATH:
				return getCasePath();
			case AdaPackage.ELEMENT_LIST__SELECT_PATH:
				return getSelectPath();
			case AdaPackage.ELEMENT_LIST__OR_PATH:
				return getOrPath();
			case AdaPackage.ELEMENT_LIST__THEN_ABORT_PATH:
				return getThenAbortPath();
			case AdaPackage.ELEMENT_LIST__CASE_EXPRESSION_PATH:
				return getCaseExpressionPath();
			case AdaPackage.ELEMENT_LIST__IF_EXPRESSION_PATH:
				return getIfExpressionPath();
			case AdaPackage.ELEMENT_LIST__ELSIF_EXPRESSION_PATH:
				return getElsifExpressionPath();
			case AdaPackage.ELEMENT_LIST__ELSE_EXPRESSION_PATH:
				return getElseExpressionPath();
			case AdaPackage.ELEMENT_LIST__USE_PACKAGE_CLAUSE:
				return getUsePackageClause();
			case AdaPackage.ELEMENT_LIST__USE_TYPE_CLAUSE:
				return getUseTypeClause();
			case AdaPackage.ELEMENT_LIST__USE_ALL_TYPE_CLAUSE:
				return getUseAllTypeClause();
			case AdaPackage.ELEMENT_LIST__WITH_CLAUSE:
				return getWithClause();
			case AdaPackage.ELEMENT_LIST__ATTRIBUTE_DEFINITION_CLAUSE:
				return getAttributeDefinitionClause();
			case AdaPackage.ELEMENT_LIST__ENUMERATION_REPRESENTATION_CLAUSE:
				return getEnumerationRepresentationClause();
			case AdaPackage.ELEMENT_LIST__RECORD_REPRESENTATION_CLAUSE:
				return getRecordRepresentationClause();
			case AdaPackage.ELEMENT_LIST__AT_CLAUSE:
				return getAtClause();
			case AdaPackage.ELEMENT_LIST__COMPONENT_CLAUSE:
				return getComponentClause();
			case AdaPackage.ELEMENT_LIST__EXCEPTION_HANDLER:
				return getExceptionHandler();
			case AdaPackage.ELEMENT_LIST__COMMENT:
				return getComment();
			case AdaPackage.ELEMENT_LIST__ALL_CALLS_REMOTE_PRAGMA:
				return getAllCallsRemotePragma();
			case AdaPackage.ELEMENT_LIST__ASYNCHRONOUS_PRAGMA:
				return getAsynchronousPragma();
			case AdaPackage.ELEMENT_LIST__ATOMIC_PRAGMA:
				return getAtomicPragma();
			case AdaPackage.ELEMENT_LIST__ATOMIC_COMPONENTS_PRAGMA:
				return getAtomicComponentsPragma();
			case AdaPackage.ELEMENT_LIST__ATTACH_HANDLER_PRAGMA:
				return getAttachHandlerPragma();
			case AdaPackage.ELEMENT_LIST__CONTROLLED_PRAGMA:
				return getControlledPragma();
			case AdaPackage.ELEMENT_LIST__CONVENTION_PRAGMA:
				return getConventionPragma();
			case AdaPackage.ELEMENT_LIST__DISCARD_NAMES_PRAGMA:
				return getDiscardNamesPragma();
			case AdaPackage.ELEMENT_LIST__ELABORATE_PRAGMA:
				return getElaboratePragma();
			case AdaPackage.ELEMENT_LIST__ELABORATE_ALL_PRAGMA:
				return getElaborateAllPragma();
			case AdaPackage.ELEMENT_LIST__ELABORATE_BODY_PRAGMA:
				return getElaborateBodyPragma();
			case AdaPackage.ELEMENT_LIST__EXPORT_PRAGMA:
				return getExportPragma();
			case AdaPackage.ELEMENT_LIST__IMPORT_PRAGMA:
				return getImportPragma();
			case AdaPackage.ELEMENT_LIST__INLINE_PRAGMA:
				return getInlinePragma();
			case AdaPackage.ELEMENT_LIST__INSPECTION_POINT_PRAGMA:
				return getInspectionPointPragma();
			case AdaPackage.ELEMENT_LIST__INTERRUPT_HANDLER_PRAGMA:
				return getInterruptHandlerPragma();
			case AdaPackage.ELEMENT_LIST__INTERRUPT_PRIORITY_PRAGMA:
				return getInterruptPriorityPragma();
			case AdaPackage.ELEMENT_LIST__LINKER_OPTIONS_PRAGMA:
				return getLinkerOptionsPragma();
			case AdaPackage.ELEMENT_LIST__LIST_PRAGMA:
				return getListPragma();
			case AdaPackage.ELEMENT_LIST__LOCKING_POLICY_PRAGMA:
				return getLockingPolicyPragma();
			case AdaPackage.ELEMENT_LIST__NORMALIZE_SCALARS_PRAGMA:
				return getNormalizeScalarsPragma();
			case AdaPackage.ELEMENT_LIST__OPTIMIZE_PRAGMA:
				return getOptimizePragma();
			case AdaPackage.ELEMENT_LIST__PACK_PRAGMA:
				return getPackPragma();
			case AdaPackage.ELEMENT_LIST__PAGE_PRAGMA:
				return getPagePragma();
			case AdaPackage.ELEMENT_LIST__PREELABORATE_PRAGMA:
				return getPreelaboratePragma();
			case AdaPackage.ELEMENT_LIST__PRIORITY_PRAGMA:
				return getPriorityPragma();
			case AdaPackage.ELEMENT_LIST__PURE_PRAGMA:
				return getPurePragma();
			case AdaPackage.ELEMENT_LIST__QUEUING_POLICY_PRAGMA:
				return getQueuingPolicyPragma();
			case AdaPackage.ELEMENT_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				return getRemoteCallInterfacePragma();
			case AdaPackage.ELEMENT_LIST__REMOTE_TYPES_PRAGMA:
				return getRemoteTypesPragma();
			case AdaPackage.ELEMENT_LIST__RESTRICTIONS_PRAGMA:
				return getRestrictionsPragma();
			case AdaPackage.ELEMENT_LIST__REVIEWABLE_PRAGMA:
				return getReviewablePragma();
			case AdaPackage.ELEMENT_LIST__SHARED_PASSIVE_PRAGMA:
				return getSharedPassivePragma();
			case AdaPackage.ELEMENT_LIST__STORAGE_SIZE_PRAGMA:
				return getStorageSizePragma();
			case AdaPackage.ELEMENT_LIST__SUPPRESS_PRAGMA:
				return getSuppressPragma();
			case AdaPackage.ELEMENT_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				return getTaskDispatchingPolicyPragma();
			case AdaPackage.ELEMENT_LIST__VOLATILE_PRAGMA:
				return getVolatilePragma();
			case AdaPackage.ELEMENT_LIST__VOLATILE_COMPONENTS_PRAGMA:
				return getVolatileComponentsPragma();
			case AdaPackage.ELEMENT_LIST__ASSERT_PRAGMA:
				return getAssertPragma();
			case AdaPackage.ELEMENT_LIST__ASSERTION_POLICY_PRAGMA:
				return getAssertionPolicyPragma();
			case AdaPackage.ELEMENT_LIST__DETECT_BLOCKING_PRAGMA:
				return getDetectBlockingPragma();
			case AdaPackage.ELEMENT_LIST__NO_RETURN_PRAGMA:
				return getNoReturnPragma();
			case AdaPackage.ELEMENT_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				return getPartitionElaborationPolicyPragma();
			case AdaPackage.ELEMENT_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				return getPreelaborableInitializationPragma();
			case AdaPackage.ELEMENT_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return getPrioritySpecificDispatchingPragma();
			case AdaPackage.ELEMENT_LIST__PROFILE_PRAGMA:
				return getProfilePragma();
			case AdaPackage.ELEMENT_LIST__RELATIVE_DEADLINE_PRAGMA:
				return getRelativeDeadlinePragma();
			case AdaPackage.ELEMENT_LIST__UNCHECKED_UNION_PRAGMA:
				return getUncheckedUnionPragma();
			case AdaPackage.ELEMENT_LIST__UNSUPPRESS_PRAGMA:
				return getUnsuppressPragma();
			case AdaPackage.ELEMENT_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				return getDefaultStoragePoolPragma();
			case AdaPackage.ELEMENT_LIST__DISPATCHING_DOMAIN_PRAGMA:
				return getDispatchingDomainPragma();
			case AdaPackage.ELEMENT_LIST__CPU_PRAGMA:
				return getCpuPragma();
			case AdaPackage.ELEMENT_LIST__INDEPENDENT_PRAGMA:
				return getIndependentPragma();
			case AdaPackage.ELEMENT_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				return getIndependentComponentsPragma();
			case AdaPackage.ELEMENT_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				return getImplementationDefinedPragma();
			case AdaPackage.ELEMENT_LIST__UNKNOWN_PRAGMA:
				return getUnknownPragma();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.ELEMENT_LIST__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case AdaPackage.ELEMENT_LIST__NOT_AN_ELEMENT:
				getNotAnElement().clear();
				getNotAnElement().addAll((Collection<? extends NotAnElement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_IDENTIFIER:
				getDefiningIdentifier().clear();
				getDefiningIdentifier().addAll((Collection<? extends DefiningIdentifier>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_CHARACTER_LITERAL:
				getDefiningCharacterLiteral().clear();
				getDefiningCharacterLiteral().addAll((Collection<? extends DefiningCharacterLiteral>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_ENUMERATION_LITERAL:
				getDefiningEnumerationLiteral().clear();
				getDefiningEnumerationLiteral().addAll((Collection<? extends DefiningEnumerationLiteral>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_AND_OPERATOR:
				getDefiningAndOperator().clear();
				getDefiningAndOperator().addAll((Collection<? extends DefiningAndOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_OR_OPERATOR:
				getDefiningOrOperator().clear();
				getDefiningOrOperator().addAll((Collection<? extends DefiningOrOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_XOR_OPERATOR:
				getDefiningXorOperator().clear();
				getDefiningXorOperator().addAll((Collection<? extends DefiningXorOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_EQUAL_OPERATOR:
				getDefiningEqualOperator().clear();
				getDefiningEqualOperator().addAll((Collection<? extends DefiningEqualOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_NOT_EQUAL_OPERATOR:
				getDefiningNotEqualOperator().clear();
				getDefiningNotEqualOperator().addAll((Collection<? extends DefiningNotEqualOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_LESS_THAN_OPERATOR:
				getDefiningLessThanOperator().clear();
				getDefiningLessThanOperator().addAll((Collection<? extends DefiningLessThanOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_LESS_THAN_OR_EQUAL_OPERATOR:
				getDefiningLessThanOrEqualOperator().clear();
				getDefiningLessThanOrEqualOperator().addAll((Collection<? extends DefiningLessThanOrEqualOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_GREATER_THAN_OPERATOR:
				getDefiningGreaterThanOperator().clear();
				getDefiningGreaterThanOperator().addAll((Collection<? extends DefiningGreaterThanOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_GREATER_THAN_OR_EQUAL_OPERATOR:
				getDefiningGreaterThanOrEqualOperator().clear();
				getDefiningGreaterThanOrEqualOperator().addAll((Collection<? extends DefiningGreaterThanOrEqualOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_PLUS_OPERATOR:
				getDefiningPlusOperator().clear();
				getDefiningPlusOperator().addAll((Collection<? extends DefiningPlusOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_MINUS_OPERATOR:
				getDefiningMinusOperator().clear();
				getDefiningMinusOperator().addAll((Collection<? extends DefiningMinusOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_CONCATENATE_OPERATOR:
				getDefiningConcatenateOperator().clear();
				getDefiningConcatenateOperator().addAll((Collection<? extends DefiningConcatenateOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_UNARY_PLUS_OPERATOR:
				getDefiningUnaryPlusOperator().clear();
				getDefiningUnaryPlusOperator().addAll((Collection<? extends DefiningUnaryPlusOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_UNARY_MINUS_OPERATOR:
				getDefiningUnaryMinusOperator().clear();
				getDefiningUnaryMinusOperator().addAll((Collection<? extends DefiningUnaryMinusOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_MULTIPLY_OPERATOR:
				getDefiningMultiplyOperator().clear();
				getDefiningMultiplyOperator().addAll((Collection<? extends DefiningMultiplyOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_DIVIDE_OPERATOR:
				getDefiningDivideOperator().clear();
				getDefiningDivideOperator().addAll((Collection<? extends DefiningDivideOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_MOD_OPERATOR:
				getDefiningModOperator().clear();
				getDefiningModOperator().addAll((Collection<? extends DefiningModOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_REM_OPERATOR:
				getDefiningRemOperator().clear();
				getDefiningRemOperator().addAll((Collection<? extends DefiningRemOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_EXPONENTIATE_OPERATOR:
				getDefiningExponentiateOperator().clear();
				getDefiningExponentiateOperator().addAll((Collection<? extends DefiningExponentiateOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_ABS_OPERATOR:
				getDefiningAbsOperator().clear();
				getDefiningAbsOperator().addAll((Collection<? extends DefiningAbsOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_NOT_OPERATOR:
				getDefiningNotOperator().clear();
				getDefiningNotOperator().addAll((Collection<? extends DefiningNotOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_EXPANDED_NAME:
				getDefiningExpandedName().clear();
				getDefiningExpandedName().addAll((Collection<? extends DefiningExpandedName>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ORDINARY_TYPE_DECLARATION:
				getOrdinaryTypeDeclaration().clear();
				getOrdinaryTypeDeclaration().addAll((Collection<? extends OrdinaryTypeDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__TASK_TYPE_DECLARATION:
				getTaskTypeDeclaration().clear();
				getTaskTypeDeclaration().addAll((Collection<? extends TaskTypeDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PROTECTED_TYPE_DECLARATION:
				getProtectedTypeDeclaration().clear();
				getProtectedTypeDeclaration().addAll((Collection<? extends ProtectedTypeDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__INCOMPLETE_TYPE_DECLARATION:
				getIncompleteTypeDeclaration().clear();
				getIncompleteTypeDeclaration().addAll((Collection<? extends IncompleteTypeDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__TAGGED_INCOMPLETE_TYPE_DECLARATION:
				getTaggedIncompleteTypeDeclaration().clear();
				getTaggedIncompleteTypeDeclaration().addAll((Collection<? extends TaggedIncompleteTypeDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PRIVATE_TYPE_DECLARATION:
				getPrivateTypeDeclaration().clear();
				getPrivateTypeDeclaration().addAll((Collection<? extends PrivateTypeDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PRIVATE_EXTENSION_DECLARATION:
				getPrivateExtensionDeclaration().clear();
				getPrivateExtensionDeclaration().addAll((Collection<? extends PrivateExtensionDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__SUBTYPE_DECLARATION:
				getSubtypeDeclaration().clear();
				getSubtypeDeclaration().addAll((Collection<? extends SubtypeDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__VARIABLE_DECLARATION:
				getVariableDeclaration().clear();
				getVariableDeclaration().addAll((Collection<? extends VariableDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__CONSTANT_DECLARATION:
				getConstantDeclaration().clear();
				getConstantDeclaration().addAll((Collection<? extends ConstantDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFERRED_CONSTANT_DECLARATION:
				getDeferredConstantDeclaration().clear();
				getDeferredConstantDeclaration().addAll((Collection<? extends DeferredConstantDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__SINGLE_TASK_DECLARATION:
				getSingleTaskDeclaration().clear();
				getSingleTaskDeclaration().addAll((Collection<? extends SingleTaskDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__SINGLE_PROTECTED_DECLARATION:
				getSingleProtectedDeclaration().clear();
				getSingleProtectedDeclaration().addAll((Collection<? extends SingleProtectedDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__INTEGER_NUMBER_DECLARATION:
				getIntegerNumberDeclaration().clear();
				getIntegerNumberDeclaration().addAll((Collection<? extends IntegerNumberDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__REAL_NUMBER_DECLARATION:
				getRealNumberDeclaration().clear();
				getRealNumberDeclaration().addAll((Collection<? extends RealNumberDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ENUMERATION_LITERAL_SPECIFICATION:
				getEnumerationLiteralSpecification().clear();
				getEnumerationLiteralSpecification().addAll((Collection<? extends EnumerationLiteralSpecification>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DISCRIMINANT_SPECIFICATION:
				getDiscriminantSpecification().clear();
				getDiscriminantSpecification().addAll((Collection<? extends DiscriminantSpecification>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__COMPONENT_DECLARATION:
				getComponentDeclaration().clear();
				getComponentDeclaration().addAll((Collection<? extends ComponentDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__LOOP_PARAMETER_SPECIFICATION:
				getLoopParameterSpecification().clear();
				getLoopParameterSpecification().addAll((Collection<? extends LoopParameterSpecification>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__GENERALIZED_ITERATOR_SPECIFICATION:
				getGeneralizedIteratorSpecification().clear();
				getGeneralizedIteratorSpecification().addAll((Collection<? extends GeneralizedIteratorSpecification>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ELEMENT_ITERATOR_SPECIFICATION:
				getElementIteratorSpecification().clear();
				getElementIteratorSpecification().addAll((Collection<? extends ElementIteratorSpecification>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PROCEDURE_DECLARATION:
				getProcedureDeclaration().clear();
				getProcedureDeclaration().addAll((Collection<? extends ProcedureDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FUNCTION_DECLARATION:
				getFunctionDeclaration().clear();
				getFunctionDeclaration().addAll((Collection<? extends FunctionDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PARAMETER_SPECIFICATION:
				getParameterSpecification().clear();
				getParameterSpecification().addAll((Collection<? extends ParameterSpecification>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PROCEDURE_BODY_DECLARATION:
				getProcedureBodyDeclaration().clear();
				getProcedureBodyDeclaration().addAll((Collection<? extends ProcedureBodyDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FUNCTION_BODY_DECLARATION:
				getFunctionBodyDeclaration().clear();
				getFunctionBodyDeclaration().addAll((Collection<? extends FunctionBodyDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__RETURN_VARIABLE_SPECIFICATION:
				getReturnVariableSpecification().clear();
				getReturnVariableSpecification().addAll((Collection<? extends ReturnVariableSpecification>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__RETURN_CONSTANT_SPECIFICATION:
				getReturnConstantSpecification().clear();
				getReturnConstantSpecification().addAll((Collection<? extends ReturnConstantSpecification>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__NULL_PROCEDURE_DECLARATION:
				getNullProcedureDeclaration().clear();
				getNullProcedureDeclaration().addAll((Collection<? extends NullProcedureDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__EXPRESSION_FUNCTION_DECLARATION:
				getExpressionFunctionDeclaration().clear();
				getExpressionFunctionDeclaration().addAll((Collection<? extends ExpressionFunctionDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PACKAGE_DECLARATION:
				getPackageDeclaration().clear();
				getPackageDeclaration().addAll((Collection<? extends PackageDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PACKAGE_BODY_DECLARATION:
				getPackageBodyDeclaration().clear();
				getPackageBodyDeclaration().addAll((Collection<? extends PackageBodyDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__OBJECT_RENAMING_DECLARATION:
				getObjectRenamingDeclaration().clear();
				getObjectRenamingDeclaration().addAll((Collection<? extends ObjectRenamingDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__EXCEPTION_RENAMING_DECLARATION:
				getExceptionRenamingDeclaration().clear();
				getExceptionRenamingDeclaration().addAll((Collection<? extends ExceptionRenamingDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PACKAGE_RENAMING_DECLARATION:
				getPackageRenamingDeclaration().clear();
				getPackageRenamingDeclaration().addAll((Collection<? extends PackageRenamingDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PROCEDURE_RENAMING_DECLARATION:
				getProcedureRenamingDeclaration().clear();
				getProcedureRenamingDeclaration().addAll((Collection<? extends ProcedureRenamingDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FUNCTION_RENAMING_DECLARATION:
				getFunctionRenamingDeclaration().clear();
				getFunctionRenamingDeclaration().addAll((Collection<? extends FunctionRenamingDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__GENERIC_PACKAGE_RENAMING_DECLARATION:
				getGenericPackageRenamingDeclaration().clear();
				getGenericPackageRenamingDeclaration().addAll((Collection<? extends GenericPackageRenamingDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__GENERIC_PROCEDURE_RENAMING_DECLARATION:
				getGenericProcedureRenamingDeclaration().clear();
				getGenericProcedureRenamingDeclaration().addAll((Collection<? extends GenericProcedureRenamingDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__GENERIC_FUNCTION_RENAMING_DECLARATION:
				getGenericFunctionRenamingDeclaration().clear();
				getGenericFunctionRenamingDeclaration().addAll((Collection<? extends GenericFunctionRenamingDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__TASK_BODY_DECLARATION:
				getTaskBodyDeclaration().clear();
				getTaskBodyDeclaration().addAll((Collection<? extends TaskBodyDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PROTECTED_BODY_DECLARATION:
				getProtectedBodyDeclaration().clear();
				getProtectedBodyDeclaration().addAll((Collection<? extends ProtectedBodyDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ENTRY_DECLARATION:
				getEntryDeclaration().clear();
				getEntryDeclaration().addAll((Collection<? extends EntryDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ENTRY_BODY_DECLARATION:
				getEntryBodyDeclaration().clear();
				getEntryBodyDeclaration().addAll((Collection<? extends EntryBodyDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ENTRY_INDEX_SPECIFICATION:
				getEntryIndexSpecification().clear();
				getEntryIndexSpecification().addAll((Collection<? extends EntryIndexSpecification>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PROCEDURE_BODY_STUB:
				getProcedureBodyStub().clear();
				getProcedureBodyStub().addAll((Collection<? extends ProcedureBodyStub>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FUNCTION_BODY_STUB:
				getFunctionBodyStub().clear();
				getFunctionBodyStub().addAll((Collection<? extends FunctionBodyStub>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PACKAGE_BODY_STUB:
				getPackageBodyStub().clear();
				getPackageBodyStub().addAll((Collection<? extends PackageBodyStub>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__TASK_BODY_STUB:
				getTaskBodyStub().clear();
				getTaskBodyStub().addAll((Collection<? extends TaskBodyStub>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PROTECTED_BODY_STUB:
				getProtectedBodyStub().clear();
				getProtectedBodyStub().addAll((Collection<? extends ProtectedBodyStub>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__EXCEPTION_DECLARATION:
				getExceptionDeclaration().clear();
				getExceptionDeclaration().addAll((Collection<? extends ExceptionDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__CHOICE_PARAMETER_SPECIFICATION:
				getChoiceParameterSpecification().clear();
				getChoiceParameterSpecification().addAll((Collection<? extends ChoiceParameterSpecification>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__GENERIC_PROCEDURE_DECLARATION:
				getGenericProcedureDeclaration().clear();
				getGenericProcedureDeclaration().addAll((Collection<? extends GenericProcedureDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__GENERIC_FUNCTION_DECLARATION:
				getGenericFunctionDeclaration().clear();
				getGenericFunctionDeclaration().addAll((Collection<? extends GenericFunctionDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__GENERIC_PACKAGE_DECLARATION:
				getGenericPackageDeclaration().clear();
				getGenericPackageDeclaration().addAll((Collection<? extends GenericPackageDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PACKAGE_INSTANTIATION:
				getPackageInstantiation().clear();
				getPackageInstantiation().addAll((Collection<? extends PackageInstantiation>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PROCEDURE_INSTANTIATION:
				getProcedureInstantiation().clear();
				getProcedureInstantiation().addAll((Collection<? extends ProcedureInstantiation>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FUNCTION_INSTANTIATION:
				getFunctionInstantiation().clear();
				getFunctionInstantiation().addAll((Collection<? extends FunctionInstantiation>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_OBJECT_DECLARATION:
				getFormalObjectDeclaration().clear();
				getFormalObjectDeclaration().addAll((Collection<? extends FormalObjectDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_TYPE_DECLARATION:
				getFormalTypeDeclaration().clear();
				getFormalTypeDeclaration().addAll((Collection<? extends FormalTypeDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_INCOMPLETE_TYPE_DECLARATION:
				getFormalIncompleteTypeDeclaration().clear();
				getFormalIncompleteTypeDeclaration().addAll((Collection<? extends FormalIncompleteTypeDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_PROCEDURE_DECLARATION:
				getFormalProcedureDeclaration().clear();
				getFormalProcedureDeclaration().addAll((Collection<? extends FormalProcedureDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_FUNCTION_DECLARATION:
				getFormalFunctionDeclaration().clear();
				getFormalFunctionDeclaration().addAll((Collection<? extends FormalFunctionDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_PACKAGE_DECLARATION:
				getFormalPackageDeclaration().clear();
				getFormalPackageDeclaration().addAll((Collection<? extends FormalPackageDeclaration>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_PACKAGE_DECLARATION_WITH_BOX:
				getFormalPackageDeclarationWithBox().clear();
				getFormalPackageDeclarationWithBox().addAll((Collection<? extends FormalPackageDeclarationWithBox>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DERIVED_TYPE_DEFINITION:
				getDerivedTypeDefinition().clear();
				getDerivedTypeDefinition().addAll((Collection<? extends DerivedTypeDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DERIVED_RECORD_EXTENSION_DEFINITION:
				getDerivedRecordExtensionDefinition().clear();
				getDerivedRecordExtensionDefinition().addAll((Collection<? extends DerivedRecordExtensionDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ENUMERATION_TYPE_DEFINITION:
				getEnumerationTypeDefinition().clear();
				getEnumerationTypeDefinition().addAll((Collection<? extends EnumerationTypeDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__SIGNED_INTEGER_TYPE_DEFINITION:
				getSignedIntegerTypeDefinition().clear();
				getSignedIntegerTypeDefinition().addAll((Collection<? extends SignedIntegerTypeDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__MODULAR_TYPE_DEFINITION:
				getModularTypeDefinition().clear();
				getModularTypeDefinition().addAll((Collection<? extends ModularTypeDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ROOT_INTEGER_DEFINITION:
				getRootIntegerDefinition().clear();
				getRootIntegerDefinition().addAll((Collection<? extends RootIntegerDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ROOT_REAL_DEFINITION:
				getRootRealDefinition().clear();
				getRootRealDefinition().addAll((Collection<? extends RootRealDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__UNIVERSAL_INTEGER_DEFINITION:
				getUniversalIntegerDefinition().clear();
				getUniversalIntegerDefinition().addAll((Collection<? extends UniversalIntegerDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__UNIVERSAL_REAL_DEFINITION:
				getUniversalRealDefinition().clear();
				getUniversalRealDefinition().addAll((Collection<? extends UniversalRealDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__UNIVERSAL_FIXED_DEFINITION:
				getUniversalFixedDefinition().clear();
				getUniversalFixedDefinition().addAll((Collection<? extends UniversalFixedDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FLOATING_POINT_DEFINITION:
				getFloatingPointDefinition().clear();
				getFloatingPointDefinition().addAll((Collection<? extends FloatingPointDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ORDINARY_FIXED_POINT_DEFINITION:
				getOrdinaryFixedPointDefinition().clear();
				getOrdinaryFixedPointDefinition().addAll((Collection<? extends OrdinaryFixedPointDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DECIMAL_FIXED_POINT_DEFINITION:
				getDecimalFixedPointDefinition().clear();
				getDecimalFixedPointDefinition().addAll((Collection<? extends DecimalFixedPointDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__UNCONSTRAINED_ARRAY_DEFINITION:
				getUnconstrainedArrayDefinition().clear();
				getUnconstrainedArrayDefinition().addAll((Collection<? extends UnconstrainedArrayDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__CONSTRAINED_ARRAY_DEFINITION:
				getConstrainedArrayDefinition().clear();
				getConstrainedArrayDefinition().addAll((Collection<? extends ConstrainedArrayDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__RECORD_TYPE_DEFINITION:
				getRecordTypeDefinition().clear();
				getRecordTypeDefinition().addAll((Collection<? extends RecordTypeDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__TAGGED_RECORD_TYPE_DEFINITION:
				getTaggedRecordTypeDefinition().clear();
				getTaggedRecordTypeDefinition().addAll((Collection<? extends TaggedRecordTypeDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ORDINARY_INTERFACE:
				getOrdinaryInterface().clear();
				getOrdinaryInterface().addAll((Collection<? extends OrdinaryInterface>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__LIMITED_INTERFACE:
				getLimitedInterface().clear();
				getLimitedInterface().addAll((Collection<? extends LimitedInterface>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__TASK_INTERFACE:
				getTaskInterface().clear();
				getTaskInterface().addAll((Collection<? extends TaskInterface>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PROTECTED_INTERFACE:
				getProtectedInterface().clear();
				getProtectedInterface().addAll((Collection<? extends ProtectedInterface>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__SYNCHRONIZED_INTERFACE:
				getSynchronizedInterface().clear();
				getSynchronizedInterface().addAll((Collection<? extends SynchronizedInterface>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				getPoolSpecificAccessToVariable().clear();
				getPoolSpecificAccessToVariable().addAll((Collection<? extends PoolSpecificAccessToVariable>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_VARIABLE:
				getAccessToVariable().clear();
				getAccessToVariable().addAll((Collection<? extends AccessToVariable>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_CONSTANT:
				getAccessToConstant().clear();
				getAccessToConstant().addAll((Collection<? extends AccessToConstant>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_PROCEDURE:
				getAccessToProcedure().clear();
				getAccessToProcedure().addAll((Collection<? extends AccessToProcedure>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_PROTECTED_PROCEDURE:
				getAccessToProtectedProcedure().clear();
				getAccessToProtectedProcedure().addAll((Collection<? extends AccessToProtectedProcedure>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_FUNCTION:
				getAccessToFunction().clear();
				getAccessToFunction().addAll((Collection<? extends AccessToFunction>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_PROTECTED_FUNCTION:
				getAccessToProtectedFunction().clear();
				getAccessToProtectedFunction().addAll((Collection<? extends AccessToProtectedFunction>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__SUBTYPE_INDICATION:
				getSubtypeIndication().clear();
				getSubtypeIndication().addAll((Collection<? extends SubtypeIndication>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__RANGE_ATTRIBUTE_REFERENCE:
				getRangeAttributeReference().clear();
				getRangeAttributeReference().addAll((Collection<? extends RangeAttributeReference>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__SIMPLE_EXPRESSION_RANGE:
				getSimpleExpressionRange().clear();
				getSimpleExpressionRange().addAll((Collection<? extends SimpleExpressionRange>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DIGITS_CONSTRAINT:
				getDigitsConstraint().clear();
				getDigitsConstraint().addAll((Collection<? extends DigitsConstraint>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DELTA_CONSTRAINT:
				getDeltaConstraint().clear();
				getDeltaConstraint().addAll((Collection<? extends DeltaConstraint>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__INDEX_CONSTRAINT:
				getIndexConstraint().clear();
				getIndexConstraint().addAll((Collection<? extends IndexConstraint>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DISCRIMINANT_CONSTRAINT:
				getDiscriminantConstraint().clear();
				getDiscriminantConstraint().addAll((Collection<? extends DiscriminantConstraint>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__COMPONENT_DEFINITION:
				getComponentDefinition().clear();
				getComponentDefinition().addAll((Collection<? extends ComponentDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION:
				getDiscreteSubtypeIndicationAsSubtypeDefinition().clear();
				getDiscreteSubtypeIndicationAsSubtypeDefinition().addAll((Collection<? extends DiscreteSubtypeIndicationAsSubtypeDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION:
				getDiscreteRangeAttributeReferenceAsSubtypeDefinition().clear();
				getDiscreteRangeAttributeReferenceAsSubtypeDefinition().addAll((Collection<? extends DiscreteRangeAttributeReferenceAsSubtypeDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION:
				getDiscreteSimpleExpressionRangeAsSubtypeDefinition().clear();
				getDiscreteSimpleExpressionRangeAsSubtypeDefinition().addAll((Collection<? extends DiscreteSimpleExpressionRangeAsSubtypeDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DISCRETE_SUBTYPE_INDICATION:
				getDiscreteSubtypeIndication().clear();
				getDiscreteSubtypeIndication().addAll((Collection<? extends DiscreteSubtypeIndication>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DISCRETE_RANGE_ATTRIBUTE_REFERENCE:
				getDiscreteRangeAttributeReference().clear();
				getDiscreteRangeAttributeReference().addAll((Collection<? extends DiscreteRangeAttributeReference>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DISCRETE_SIMPLE_EXPRESSION_RANGE:
				getDiscreteSimpleExpressionRange().clear();
				getDiscreteSimpleExpressionRange().addAll((Collection<? extends DiscreteSimpleExpressionRange>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__UNKNOWN_DISCRIMINANT_PART:
				getUnknownDiscriminantPart().clear();
				getUnknownDiscriminantPart().addAll((Collection<? extends UnknownDiscriminantPart>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__KNOWN_DISCRIMINANT_PART:
				getKnownDiscriminantPart().clear();
				getKnownDiscriminantPart().addAll((Collection<? extends KnownDiscriminantPart>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__RECORD_DEFINITION:
				getRecordDefinition().clear();
				getRecordDefinition().addAll((Collection<? extends RecordDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__NULL_RECORD_DEFINITION:
				getNullRecordDefinition().clear();
				getNullRecordDefinition().addAll((Collection<? extends NullRecordDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__NULL_COMPONENT:
				getNullComponent().clear();
				getNullComponent().addAll((Collection<? extends NullComponent>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__VARIANT_PART:
				getVariantPart().clear();
				getVariantPart().addAll((Collection<? extends VariantPart>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__VARIANT:
				getVariant().clear();
				getVariant().addAll((Collection<? extends Variant>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__OTHERS_CHOICE:
				getOthersChoice().clear();
				getOthersChoice().addAll((Collection<? extends OthersChoice>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_VARIABLE:
				getAnonymousAccessToVariable().clear();
				getAnonymousAccessToVariable().addAll((Collection<? extends AnonymousAccessToVariable>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_CONSTANT:
				getAnonymousAccessToConstant().clear();
				getAnonymousAccessToConstant().addAll((Collection<? extends AnonymousAccessToConstant>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_PROCEDURE:
				getAnonymousAccessToProcedure().clear();
				getAnonymousAccessToProcedure().addAll((Collection<? extends AnonymousAccessToProcedure>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE:
				getAnonymousAccessToProtectedProcedure().clear();
				getAnonymousAccessToProtectedProcedure().addAll((Collection<? extends AnonymousAccessToProtectedProcedure>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_FUNCTION:
				getAnonymousAccessToFunction().clear();
				getAnonymousAccessToFunction().addAll((Collection<? extends AnonymousAccessToFunction>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION:
				getAnonymousAccessToProtectedFunction().clear();
				getAnonymousAccessToProtectedFunction().addAll((Collection<? extends AnonymousAccessToProtectedFunction>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PRIVATE_TYPE_DEFINITION:
				getPrivateTypeDefinition().clear();
				getPrivateTypeDefinition().addAll((Collection<? extends PrivateTypeDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__TAGGED_PRIVATE_TYPE_DEFINITION:
				getTaggedPrivateTypeDefinition().clear();
				getTaggedPrivateTypeDefinition().addAll((Collection<? extends TaggedPrivateTypeDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PRIVATE_EXTENSION_DEFINITION:
				getPrivateExtensionDefinition().clear();
				getPrivateExtensionDefinition().addAll((Collection<? extends PrivateExtensionDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__TASK_DEFINITION:
				getTaskDefinition().clear();
				getTaskDefinition().addAll((Collection<? extends TaskDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PROTECTED_DEFINITION:
				getProtectedDefinition().clear();
				getProtectedDefinition().addAll((Collection<? extends ProtectedDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_PRIVATE_TYPE_DEFINITION:
				getFormalPrivateTypeDefinition().clear();
				getFormalPrivateTypeDefinition().addAll((Collection<? extends FormalPrivateTypeDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION:
				getFormalTaggedPrivateTypeDefinition().clear();
				getFormalTaggedPrivateTypeDefinition().addAll((Collection<? extends FormalTaggedPrivateTypeDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_DERIVED_TYPE_DEFINITION:
				getFormalDerivedTypeDefinition().clear();
				getFormalDerivedTypeDefinition().addAll((Collection<? extends FormalDerivedTypeDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_DISCRETE_TYPE_DEFINITION:
				getFormalDiscreteTypeDefinition().clear();
				getFormalDiscreteTypeDefinition().addAll((Collection<? extends FormalDiscreteTypeDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_SIGNED_INTEGER_TYPE_DEFINITION:
				getFormalSignedIntegerTypeDefinition().clear();
				getFormalSignedIntegerTypeDefinition().addAll((Collection<? extends FormalSignedIntegerTypeDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_MODULAR_TYPE_DEFINITION:
				getFormalModularTypeDefinition().clear();
				getFormalModularTypeDefinition().addAll((Collection<? extends FormalModularTypeDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_FLOATING_POINT_DEFINITION:
				getFormalFloatingPointDefinition().clear();
				getFormalFloatingPointDefinition().addAll((Collection<? extends FormalFloatingPointDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_ORDINARY_FIXED_POINT_DEFINITION:
				getFormalOrdinaryFixedPointDefinition().clear();
				getFormalOrdinaryFixedPointDefinition().addAll((Collection<? extends FormalOrdinaryFixedPointDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_DECIMAL_FIXED_POINT_DEFINITION:
				getFormalDecimalFixedPointDefinition().clear();
				getFormalDecimalFixedPointDefinition().addAll((Collection<? extends FormalDecimalFixedPointDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_ORDINARY_INTERFACE:
				getFormalOrdinaryInterface().clear();
				getFormalOrdinaryInterface().addAll((Collection<? extends FormalOrdinaryInterface>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_LIMITED_INTERFACE:
				getFormalLimitedInterface().clear();
				getFormalLimitedInterface().addAll((Collection<? extends FormalLimitedInterface>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_TASK_INTERFACE:
				getFormalTaskInterface().clear();
				getFormalTaskInterface().addAll((Collection<? extends FormalTaskInterface>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_PROTECTED_INTERFACE:
				getFormalProtectedInterface().clear();
				getFormalProtectedInterface().addAll((Collection<? extends FormalProtectedInterface>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_SYNCHRONIZED_INTERFACE:
				getFormalSynchronizedInterface().clear();
				getFormalSynchronizedInterface().addAll((Collection<? extends FormalSynchronizedInterface>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_UNCONSTRAINED_ARRAY_DEFINITION:
				getFormalUnconstrainedArrayDefinition().clear();
				getFormalUnconstrainedArrayDefinition().addAll((Collection<? extends FormalUnconstrainedArrayDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_CONSTRAINED_ARRAY_DEFINITION:
				getFormalConstrainedArrayDefinition().clear();
				getFormalConstrainedArrayDefinition().addAll((Collection<? extends FormalConstrainedArrayDefinition>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				getFormalPoolSpecificAccessToVariable().clear();
				getFormalPoolSpecificAccessToVariable().addAll((Collection<? extends FormalPoolSpecificAccessToVariable>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_VARIABLE:
				getFormalAccessToVariable().clear();
				getFormalAccessToVariable().addAll((Collection<? extends FormalAccessToVariable>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_CONSTANT:
				getFormalAccessToConstant().clear();
				getFormalAccessToConstant().addAll((Collection<? extends FormalAccessToConstant>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_PROCEDURE:
				getFormalAccessToProcedure().clear();
				getFormalAccessToProcedure().addAll((Collection<? extends FormalAccessToProcedure>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_PROTECTED_PROCEDURE:
				getFormalAccessToProtectedProcedure().clear();
				getFormalAccessToProtectedProcedure().addAll((Collection<? extends FormalAccessToProtectedProcedure>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_FUNCTION:
				getFormalAccessToFunction().clear();
				getFormalAccessToFunction().addAll((Collection<? extends FormalAccessToFunction>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_PROTECTED_FUNCTION:
				getFormalAccessToProtectedFunction().clear();
				getFormalAccessToProtectedFunction().addAll((Collection<? extends FormalAccessToProtectedFunction>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ASPECT_SPECIFICATION:
				getAspectSpecification().clear();
				getAspectSpecification().addAll((Collection<? extends AspectSpecification>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__BOX_EXPRESSION:
				getBoxExpression().clear();
				getBoxExpression().addAll((Collection<? extends BoxExpression>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__INTEGER_LITERAL:
				getIntegerLiteral().clear();
				getIntegerLiteral().addAll((Collection<? extends IntegerLiteral>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__REAL_LITERAL:
				getRealLiteral().clear();
				getRealLiteral().addAll((Collection<? extends RealLiteral>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__STRING_LITERAL:
				getStringLiteral().clear();
				getStringLiteral().addAll((Collection<? extends StringLiteral>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__IDENTIFIER:
				getIdentifier().clear();
				getIdentifier().addAll((Collection<? extends Identifier>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__AND_OPERATOR:
				getAndOperator().clear();
				getAndOperator().addAll((Collection<? extends AndOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__OR_OPERATOR:
				getOrOperator().clear();
				getOrOperator().addAll((Collection<? extends OrOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__XOR_OPERATOR:
				getXorOperator().clear();
				getXorOperator().addAll((Collection<? extends XorOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__EQUAL_OPERATOR:
				getEqualOperator().clear();
				getEqualOperator().addAll((Collection<? extends EqualOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__NOT_EQUAL_OPERATOR:
				getNotEqualOperator().clear();
				getNotEqualOperator().addAll((Collection<? extends NotEqualOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__LESS_THAN_OPERATOR:
				getLessThanOperator().clear();
				getLessThanOperator().addAll((Collection<? extends LessThanOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__LESS_THAN_OR_EQUAL_OPERATOR:
				getLessThanOrEqualOperator().clear();
				getLessThanOrEqualOperator().addAll((Collection<? extends LessThanOrEqualOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__GREATER_THAN_OPERATOR:
				getGreaterThanOperator().clear();
				getGreaterThanOperator().addAll((Collection<? extends GreaterThanOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__GREATER_THAN_OR_EQUAL_OPERATOR:
				getGreaterThanOrEqualOperator().clear();
				getGreaterThanOrEqualOperator().addAll((Collection<? extends GreaterThanOrEqualOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PLUS_OPERATOR:
				getPlusOperator().clear();
				getPlusOperator().addAll((Collection<? extends PlusOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__MINUS_OPERATOR:
				getMinusOperator().clear();
				getMinusOperator().addAll((Collection<? extends MinusOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__CONCATENATE_OPERATOR:
				getConcatenateOperator().clear();
				getConcatenateOperator().addAll((Collection<? extends ConcatenateOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__UNARY_PLUS_OPERATOR:
				getUnaryPlusOperator().clear();
				getUnaryPlusOperator().addAll((Collection<? extends UnaryPlusOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__UNARY_MINUS_OPERATOR:
				getUnaryMinusOperator().clear();
				getUnaryMinusOperator().addAll((Collection<? extends UnaryMinusOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__MULTIPLY_OPERATOR:
				getMultiplyOperator().clear();
				getMultiplyOperator().addAll((Collection<? extends MultiplyOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DIVIDE_OPERATOR:
				getDivideOperator().clear();
				getDivideOperator().addAll((Collection<? extends DivideOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__MOD_OPERATOR:
				getModOperator().clear();
				getModOperator().addAll((Collection<? extends ModOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__REM_OPERATOR:
				getRemOperator().clear();
				getRemOperator().addAll((Collection<? extends RemOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__EXPONENTIATE_OPERATOR:
				getExponentiateOperator().clear();
				getExponentiateOperator().addAll((Collection<? extends ExponentiateOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ABS_OPERATOR:
				getAbsOperator().clear();
				getAbsOperator().addAll((Collection<? extends AbsOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__NOT_OPERATOR:
				getNotOperator().clear();
				getNotOperator().addAll((Collection<? extends NotOperator>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__CHARACTER_LITERAL:
				getCharacterLiteral().clear();
				getCharacterLiteral().addAll((Collection<? extends CharacterLiteral>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ENUMERATION_LITERAL:
				getEnumerationLiteral().clear();
				getEnumerationLiteral().addAll((Collection<? extends EnumerationLiteral>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__EXPLICIT_DEREFERENCE:
				getExplicitDereference().clear();
				getExplicitDereference().addAll((Collection<? extends ExplicitDereference>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FUNCTION_CALL:
				getFunctionCall().clear();
				getFunctionCall().addAll((Collection<? extends FunctionCall>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__INDEXED_COMPONENT:
				getIndexedComponent().clear();
				getIndexedComponent().addAll((Collection<? extends IndexedComponent>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__SLICE:
				getSlice().clear();
				getSlice().addAll((Collection<? extends Slice>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__SELECTED_COMPONENT:
				getSelectedComponent().clear();
				getSelectedComponent().addAll((Collection<? extends SelectedComponent>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ACCESS_ATTRIBUTE:
				getAccessAttribute().clear();
				getAccessAttribute().addAll((Collection<? extends AccessAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ADDRESS_ATTRIBUTE:
				getAddressAttribute().clear();
				getAddressAttribute().addAll((Collection<? extends AddressAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ADJACENT_ATTRIBUTE:
				getAdjacentAttribute().clear();
				getAdjacentAttribute().addAll((Collection<? extends AdjacentAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__AFT_ATTRIBUTE:
				getAftAttribute().clear();
				getAftAttribute().addAll((Collection<? extends AftAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ALIGNMENT_ATTRIBUTE:
				getAlignmentAttribute().clear();
				getAlignmentAttribute().addAll((Collection<? extends AlignmentAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__BASE_ATTRIBUTE:
				getBaseAttribute().clear();
				getBaseAttribute().addAll((Collection<? extends BaseAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__BIT_ORDER_ATTRIBUTE:
				getBitOrderAttribute().clear();
				getBitOrderAttribute().addAll((Collection<? extends BitOrderAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__BODY_VERSION_ATTRIBUTE:
				getBodyVersionAttribute().clear();
				getBodyVersionAttribute().addAll((Collection<? extends BodyVersionAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__CALLABLE_ATTRIBUTE:
				getCallableAttribute().clear();
				getCallableAttribute().addAll((Collection<? extends CallableAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__CALLER_ATTRIBUTE:
				getCallerAttribute().clear();
				getCallerAttribute().addAll((Collection<? extends CallerAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__CEILING_ATTRIBUTE:
				getCeilingAttribute().clear();
				getCeilingAttribute().addAll((Collection<? extends CeilingAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__CLASS_ATTRIBUTE:
				getClassAttribute().clear();
				getClassAttribute().addAll((Collection<? extends ClassAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__COMPONENT_SIZE_ATTRIBUTE:
				getComponentSizeAttribute().clear();
				getComponentSizeAttribute().addAll((Collection<? extends ComponentSizeAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__COMPOSE_ATTRIBUTE:
				getComposeAttribute().clear();
				getComposeAttribute().addAll((Collection<? extends ComposeAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__CONSTRAINED_ATTRIBUTE:
				getConstrainedAttribute().clear();
				getConstrainedAttribute().addAll((Collection<? extends ConstrainedAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__COPY_SIGN_ATTRIBUTE:
				getCopySignAttribute().clear();
				getCopySignAttribute().addAll((Collection<? extends CopySignAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__COUNT_ATTRIBUTE:
				getCountAttribute().clear();
				getCountAttribute().addAll((Collection<? extends CountAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFINITE_ATTRIBUTE:
				getDefiniteAttribute().clear();
				getDefiniteAttribute().addAll((Collection<? extends DefiniteAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DELTA_ATTRIBUTE:
				getDeltaAttribute().clear();
				getDeltaAttribute().addAll((Collection<? extends DeltaAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DENORM_ATTRIBUTE:
				getDenormAttribute().clear();
				getDenormAttribute().addAll((Collection<? extends DenormAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DIGITS_ATTRIBUTE:
				getDigitsAttribute().clear();
				getDigitsAttribute().addAll((Collection<? extends DigitsAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__EXPONENT_ATTRIBUTE:
				getExponentAttribute().clear();
				getExponentAttribute().addAll((Collection<? extends ExponentAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__EXTERNAL_TAG_ATTRIBUTE:
				getExternalTagAttribute().clear();
				getExternalTagAttribute().addAll((Collection<? extends ExternalTagAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FIRST_ATTRIBUTE:
				getFirstAttribute().clear();
				getFirstAttribute().addAll((Collection<? extends FirstAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FIRST_BIT_ATTRIBUTE:
				getFirstBitAttribute().clear();
				getFirstBitAttribute().addAll((Collection<? extends FirstBitAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FLOOR_ATTRIBUTE:
				getFloorAttribute().clear();
				getFloorAttribute().addAll((Collection<? extends FloorAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FORE_ATTRIBUTE:
				getForeAttribute().clear();
				getForeAttribute().addAll((Collection<? extends ForeAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FRACTION_ATTRIBUTE:
				getFractionAttribute().clear();
				getFractionAttribute().addAll((Collection<? extends FractionAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__IDENTITY_ATTRIBUTE:
				getIdentityAttribute().clear();
				getIdentityAttribute().addAll((Collection<? extends IdentityAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__IMAGE_ATTRIBUTE:
				getImageAttribute().clear();
				getImageAttribute().addAll((Collection<? extends ImageAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__INPUT_ATTRIBUTE:
				getInputAttribute().clear();
				getInputAttribute().addAll((Collection<? extends InputAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__LAST_ATTRIBUTE:
				getLastAttribute().clear();
				getLastAttribute().addAll((Collection<? extends LastAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__LAST_BIT_ATTRIBUTE:
				getLastBitAttribute().clear();
				getLastBitAttribute().addAll((Collection<? extends LastBitAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__LEADING_PART_ATTRIBUTE:
				getLeadingPartAttribute().clear();
				getLeadingPartAttribute().addAll((Collection<? extends LeadingPartAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__LENGTH_ATTRIBUTE:
				getLengthAttribute().clear();
				getLengthAttribute().addAll((Collection<? extends LengthAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__MACHINE_ATTRIBUTE:
				getMachineAttribute().clear();
				getMachineAttribute().addAll((Collection<? extends MachineAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__MACHINE_EMAX_ATTRIBUTE:
				getMachineEmaxAttribute().clear();
				getMachineEmaxAttribute().addAll((Collection<? extends MachineEmaxAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__MACHINE_EMIN_ATTRIBUTE:
				getMachineEminAttribute().clear();
				getMachineEminAttribute().addAll((Collection<? extends MachineEminAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__MACHINE_MANTISSA_ATTRIBUTE:
				getMachineMantissaAttribute().clear();
				getMachineMantissaAttribute().addAll((Collection<? extends MachineMantissaAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__MACHINE_OVERFLOWS_ATTRIBUTE:
				getMachineOverflowsAttribute().clear();
				getMachineOverflowsAttribute().addAll((Collection<? extends MachineOverflowsAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__MACHINE_RADIX_ATTRIBUTE:
				getMachineRadixAttribute().clear();
				getMachineRadixAttribute().addAll((Collection<? extends MachineRadixAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__MACHINE_ROUNDS_ATTRIBUTE:
				getMachineRoundsAttribute().clear();
				getMachineRoundsAttribute().addAll((Collection<? extends MachineRoundsAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__MAX_ATTRIBUTE:
				getMaxAttribute().clear();
				getMaxAttribute().addAll((Collection<? extends MaxAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE:
				getMaxSizeInStorageElementsAttribute().clear();
				getMaxSizeInStorageElementsAttribute().addAll((Collection<? extends MaxSizeInStorageElementsAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__MIN_ATTRIBUTE:
				getMinAttribute().clear();
				getMinAttribute().addAll((Collection<? extends MinAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__MODEL_ATTRIBUTE:
				getModelAttribute().clear();
				getModelAttribute().addAll((Collection<? extends ModelAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__MODEL_EMIN_ATTRIBUTE:
				getModelEminAttribute().clear();
				getModelEminAttribute().addAll((Collection<? extends ModelEminAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__MODEL_EPSILON_ATTRIBUTE:
				getModelEpsilonAttribute().clear();
				getModelEpsilonAttribute().addAll((Collection<? extends ModelEpsilonAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__MODEL_MANTISSA_ATTRIBUTE:
				getModelMantissaAttribute().clear();
				getModelMantissaAttribute().addAll((Collection<? extends ModelMantissaAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__MODEL_SMALL_ATTRIBUTE:
				getModelSmallAttribute().clear();
				getModelSmallAttribute().addAll((Collection<? extends ModelSmallAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__MODULUS_ATTRIBUTE:
				getModulusAttribute().clear();
				getModulusAttribute().addAll((Collection<? extends ModulusAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__OUTPUT_ATTRIBUTE:
				getOutputAttribute().clear();
				getOutputAttribute().addAll((Collection<? extends OutputAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PARTITION_ID_ATTRIBUTE:
				getPartitionIdAttribute().clear();
				getPartitionIdAttribute().addAll((Collection<? extends PartitionIdAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__POS_ATTRIBUTE:
				getPosAttribute().clear();
				getPosAttribute().addAll((Collection<? extends PosAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__POSITION_ATTRIBUTE:
				getPositionAttribute().clear();
				getPositionAttribute().addAll((Collection<? extends PositionAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PRED_ATTRIBUTE:
				getPredAttribute().clear();
				getPredAttribute().addAll((Collection<? extends PredAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__RANGE_ATTRIBUTE:
				getRangeAttribute().clear();
				getRangeAttribute().addAll((Collection<? extends RangeAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__READ_ATTRIBUTE:
				getReadAttribute().clear();
				getReadAttribute().addAll((Collection<? extends ReadAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__REMAINDER_ATTRIBUTE:
				getRemainderAttribute().clear();
				getRemainderAttribute().addAll((Collection<? extends RemainderAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ROUND_ATTRIBUTE:
				getRoundAttribute().clear();
				getRoundAttribute().addAll((Collection<? extends RoundAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ROUNDING_ATTRIBUTE:
				getRoundingAttribute().clear();
				getRoundingAttribute().addAll((Collection<? extends RoundingAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__SAFE_FIRST_ATTRIBUTE:
				getSafeFirstAttribute().clear();
				getSafeFirstAttribute().addAll((Collection<? extends SafeFirstAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__SAFE_LAST_ATTRIBUTE:
				getSafeLastAttribute().clear();
				getSafeLastAttribute().addAll((Collection<? extends SafeLastAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__SCALE_ATTRIBUTE:
				getScaleAttribute().clear();
				getScaleAttribute().addAll((Collection<? extends ScaleAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__SCALING_ATTRIBUTE:
				getScalingAttribute().clear();
				getScalingAttribute().addAll((Collection<? extends ScalingAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__SIGNED_ZEROS_ATTRIBUTE:
				getSignedZerosAttribute().clear();
				getSignedZerosAttribute().addAll((Collection<? extends SignedZerosAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__SIZE_ATTRIBUTE:
				getSizeAttribute().clear();
				getSizeAttribute().addAll((Collection<? extends SizeAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__SMALL_ATTRIBUTE:
				getSmallAttribute().clear();
				getSmallAttribute().addAll((Collection<? extends SmallAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__STORAGE_POOL_ATTRIBUTE:
				getStoragePoolAttribute().clear();
				getStoragePoolAttribute().addAll((Collection<? extends StoragePoolAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__STORAGE_SIZE_ATTRIBUTE:
				getStorageSizeAttribute().clear();
				getStorageSizeAttribute().addAll((Collection<? extends StorageSizeAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__SUCC_ATTRIBUTE:
				getSuccAttribute().clear();
				getSuccAttribute().addAll((Collection<? extends SuccAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__TAG_ATTRIBUTE:
				getTagAttribute().clear();
				getTagAttribute().addAll((Collection<? extends TagAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__TERMINATED_ATTRIBUTE:
				getTerminatedAttribute().clear();
				getTerminatedAttribute().addAll((Collection<? extends TerminatedAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__TRUNCATION_ATTRIBUTE:
				getTruncationAttribute().clear();
				getTruncationAttribute().addAll((Collection<? extends TruncationAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__UNBIASED_ROUNDING_ATTRIBUTE:
				getUnbiasedRoundingAttribute().clear();
				getUnbiasedRoundingAttribute().addAll((Collection<? extends UnbiasedRoundingAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__UNCHECKED_ACCESS_ATTRIBUTE:
				getUncheckedAccessAttribute().clear();
				getUncheckedAccessAttribute().addAll((Collection<? extends UncheckedAccessAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__VAL_ATTRIBUTE:
				getValAttribute().clear();
				getValAttribute().addAll((Collection<? extends ValAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__VALID_ATTRIBUTE:
				getValidAttribute().clear();
				getValidAttribute().addAll((Collection<? extends ValidAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__VALUE_ATTRIBUTE:
				getValueAttribute().clear();
				getValueAttribute().addAll((Collection<? extends ValueAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__VERSION_ATTRIBUTE:
				getVersionAttribute().clear();
				getVersionAttribute().addAll((Collection<? extends VersionAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__WIDE_IMAGE_ATTRIBUTE:
				getWideImageAttribute().clear();
				getWideImageAttribute().addAll((Collection<? extends WideImageAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__WIDE_VALUE_ATTRIBUTE:
				getWideValueAttribute().clear();
				getWideValueAttribute().addAll((Collection<? extends WideValueAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__WIDE_WIDTH_ATTRIBUTE:
				getWideWidthAttribute().clear();
				getWideWidthAttribute().addAll((Collection<? extends WideWidthAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__WIDTH_ATTRIBUTE:
				getWidthAttribute().clear();
				getWidthAttribute().addAll((Collection<? extends WidthAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__WRITE_ATTRIBUTE:
				getWriteAttribute().clear();
				getWriteAttribute().addAll((Collection<? extends WriteAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__MACHINE_ROUNDING_ATTRIBUTE:
				getMachineRoundingAttribute().clear();
				getMachineRoundingAttribute().addAll((Collection<? extends MachineRoundingAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__MOD_ATTRIBUTE:
				getModAttribute().clear();
				getModAttribute().addAll((Collection<? extends ModAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PRIORITY_ATTRIBUTE:
				getPriorityAttribute().clear();
				getPriorityAttribute().addAll((Collection<? extends PriorityAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__STREAM_SIZE_ATTRIBUTE:
				getStreamSizeAttribute().clear();
				getStreamSizeAttribute().addAll((Collection<? extends StreamSizeAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__WIDE_WIDE_IMAGE_ATTRIBUTE:
				getWideWideImageAttribute().clear();
				getWideWideImageAttribute().addAll((Collection<? extends WideWideImageAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__WIDE_WIDE_VALUE_ATTRIBUTE:
				getWideWideValueAttribute().clear();
				getWideWideValueAttribute().addAll((Collection<? extends WideWideValueAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__WIDE_WIDE_WIDTH_ATTRIBUTE:
				getWideWideWidthAttribute().clear();
				getWideWideWidthAttribute().addAll((Collection<? extends WideWideWidthAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE:
				getMaxAlignmentForAllocationAttribute().clear();
				getMaxAlignmentForAllocationAttribute().addAll((Collection<? extends MaxAlignmentForAllocationAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__OVERLAPS_STORAGE_ATTRIBUTE:
				getOverlapsStorageAttribute().clear();
				getOverlapsStorageAttribute().addAll((Collection<? extends OverlapsStorageAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__IMPLEMENTATION_DEFINED_ATTRIBUTE:
				getImplementationDefinedAttribute().clear();
				getImplementationDefinedAttribute().addAll((Collection<? extends ImplementationDefinedAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__UNKNOWN_ATTRIBUTE:
				getUnknownAttribute().clear();
				getUnknownAttribute().addAll((Collection<? extends UnknownAttribute>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__RECORD_AGGREGATE:
				getRecordAggregate().clear();
				getRecordAggregate().addAll((Collection<? extends RecordAggregate>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__EXTENSION_AGGREGATE:
				getExtensionAggregate().clear();
				getExtensionAggregate().addAll((Collection<? extends ExtensionAggregate>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__POSITIONAL_ARRAY_AGGREGATE:
				getPositionalArrayAggregate().clear();
				getPositionalArrayAggregate().addAll((Collection<? extends PositionalArrayAggregate>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__NAMED_ARRAY_AGGREGATE:
				getNamedArrayAggregate().clear();
				getNamedArrayAggregate().addAll((Collection<? extends NamedArrayAggregate>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__AND_THEN_SHORT_CIRCUIT:
				getAndThenShortCircuit().clear();
				getAndThenShortCircuit().addAll((Collection<? extends AndThenShortCircuit>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__OR_ELSE_SHORT_CIRCUIT:
				getOrElseShortCircuit().clear();
				getOrElseShortCircuit().addAll((Collection<? extends OrElseShortCircuit>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__IN_MEMBERSHIP_TEST:
				getInMembershipTest().clear();
				getInMembershipTest().addAll((Collection<? extends InMembershipTest>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__NOT_IN_MEMBERSHIP_TEST:
				getNotInMembershipTest().clear();
				getNotInMembershipTest().addAll((Collection<? extends NotInMembershipTest>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__NULL_LITERAL:
				getNullLiteral().clear();
				getNullLiteral().addAll((Collection<? extends NullLiteral>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PARENTHESIZED_EXPRESSION:
				getParenthesizedExpression().clear();
				getParenthesizedExpression().addAll((Collection<? extends ParenthesizedExpression>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__RAISE_EXPRESSION:
				getRaiseExpression().clear();
				getRaiseExpression().addAll((Collection<? extends RaiseExpression>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__TYPE_CONVERSION:
				getTypeConversion().clear();
				getTypeConversion().addAll((Collection<? extends TypeConversion>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__QUALIFIED_EXPRESSION:
				getQualifiedExpression().clear();
				getQualifiedExpression().addAll((Collection<? extends QualifiedExpression>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ALLOCATION_FROM_SUBTYPE:
				getAllocationFromSubtype().clear();
				getAllocationFromSubtype().addAll((Collection<? extends AllocationFromSubtype>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ALLOCATION_FROM_QUALIFIED_EXPRESSION:
				getAllocationFromQualifiedExpression().clear();
				getAllocationFromQualifiedExpression().addAll((Collection<? extends AllocationFromQualifiedExpression>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__CASE_EXPRESSION:
				getCaseExpression().clear();
				getCaseExpression().addAll((Collection<? extends CaseExpression>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__IF_EXPRESSION:
				getIfExpression().clear();
				getIfExpression().addAll((Collection<? extends IfExpression>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FOR_ALL_QUANTIFIED_EXPRESSION:
				getForAllQuantifiedExpression().clear();
				getForAllQuantifiedExpression().addAll((Collection<? extends ForAllQuantifiedExpression>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FOR_SOME_QUANTIFIED_EXPRESSION:
				getForSomeQuantifiedExpression().clear();
				getForSomeQuantifiedExpression().addAll((Collection<? extends ForSomeQuantifiedExpression>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PRAGMA_ARGUMENT_ASSOCIATION:
				getPragmaArgumentAssociation().clear();
				getPragmaArgumentAssociation().addAll((Collection<? extends PragmaArgumentAssociation>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DISCRIMINANT_ASSOCIATION:
				getDiscriminantAssociation().clear();
				getDiscriminantAssociation().addAll((Collection<? extends DiscriminantAssociation>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__RECORD_COMPONENT_ASSOCIATION:
				getRecordComponentAssociation().clear();
				getRecordComponentAssociation().addAll((Collection<? extends RecordComponentAssociation>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ARRAY_COMPONENT_ASSOCIATION:
				getArrayComponentAssociation().clear();
				getArrayComponentAssociation().addAll((Collection<? extends ArrayComponentAssociation>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PARAMETER_ASSOCIATION:
				getParameterAssociation().clear();
				getParameterAssociation().addAll((Collection<? extends ParameterAssociation>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__GENERIC_ASSOCIATION:
				getGenericAssociation().clear();
				getGenericAssociation().addAll((Collection<? extends GenericAssociation>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__NULL_STATEMENT:
				getNullStatement().clear();
				getNullStatement().addAll((Collection<? extends NullStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ASSIGNMENT_STATEMENT:
				getAssignmentStatement().clear();
				getAssignmentStatement().addAll((Collection<? extends AssignmentStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__IF_STATEMENT:
				getIfStatement().clear();
				getIfStatement().addAll((Collection<? extends IfStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__CASE_STATEMENT:
				getCaseStatement().clear();
				getCaseStatement().addAll((Collection<? extends CaseStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__LOOP_STATEMENT:
				getLoopStatement().clear();
				getLoopStatement().addAll((Collection<? extends LoopStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__WHILE_LOOP_STATEMENT:
				getWhileLoopStatement().clear();
				getWhileLoopStatement().addAll((Collection<? extends WhileLoopStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__FOR_LOOP_STATEMENT:
				getForLoopStatement().clear();
				getForLoopStatement().addAll((Collection<? extends ForLoopStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__BLOCK_STATEMENT:
				getBlockStatement().clear();
				getBlockStatement().addAll((Collection<? extends BlockStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__EXIT_STATEMENT:
				getExitStatement().clear();
				getExitStatement().addAll((Collection<? extends ExitStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__GOTO_STATEMENT:
				getGotoStatement().clear();
				getGotoStatement().addAll((Collection<? extends GotoStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PROCEDURE_CALL_STATEMENT:
				getProcedureCallStatement().clear();
				getProcedureCallStatement().addAll((Collection<? extends ProcedureCallStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__RETURN_STATEMENT:
				getReturnStatement().clear();
				getReturnStatement().addAll((Collection<? extends ReturnStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__EXTENDED_RETURN_STATEMENT:
				getExtendedReturnStatement().clear();
				getExtendedReturnStatement().addAll((Collection<? extends ExtendedReturnStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ACCEPT_STATEMENT:
				getAcceptStatement().clear();
				getAcceptStatement().addAll((Collection<? extends AcceptStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ENTRY_CALL_STATEMENT:
				getEntryCallStatement().clear();
				getEntryCallStatement().addAll((Collection<? extends EntryCallStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__REQUEUE_STATEMENT:
				getRequeueStatement().clear();
				getRequeueStatement().addAll((Collection<? extends RequeueStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__REQUEUE_STATEMENT_WITH_ABORT:
				getRequeueStatementWithAbort().clear();
				getRequeueStatementWithAbort().addAll((Collection<? extends RequeueStatementWithAbort>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DELAY_UNTIL_STATEMENT:
				getDelayUntilStatement().clear();
				getDelayUntilStatement().addAll((Collection<? extends DelayUntilStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DELAY_RELATIVE_STATEMENT:
				getDelayRelativeStatement().clear();
				getDelayRelativeStatement().addAll((Collection<? extends DelayRelativeStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__TERMINATE_ALTERNATIVE_STATEMENT:
				getTerminateAlternativeStatement().clear();
				getTerminateAlternativeStatement().addAll((Collection<? extends TerminateAlternativeStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__SELECTIVE_ACCEPT_STATEMENT:
				getSelectiveAcceptStatement().clear();
				getSelectiveAcceptStatement().addAll((Collection<? extends SelectiveAcceptStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__TIMED_ENTRY_CALL_STATEMENT:
				getTimedEntryCallStatement().clear();
				getTimedEntryCallStatement().addAll((Collection<? extends TimedEntryCallStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__CONDITIONAL_ENTRY_CALL_STATEMENT:
				getConditionalEntryCallStatement().clear();
				getConditionalEntryCallStatement().addAll((Collection<? extends ConditionalEntryCallStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ASYNCHRONOUS_SELECT_STATEMENT:
				getAsynchronousSelectStatement().clear();
				getAsynchronousSelectStatement().addAll((Collection<? extends AsynchronousSelectStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ABORT_STATEMENT:
				getAbortStatement().clear();
				getAbortStatement().addAll((Collection<? extends AbortStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__RAISE_STATEMENT:
				getRaiseStatement().clear();
				getRaiseStatement().addAll((Collection<? extends RaiseStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__CODE_STATEMENT:
				getCodeStatement().clear();
				getCodeStatement().addAll((Collection<? extends CodeStatement>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__IF_PATH:
				getIfPath().clear();
				getIfPath().addAll((Collection<? extends IfPath>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ELSIF_PATH:
				getElsifPath().clear();
				getElsifPath().addAll((Collection<? extends ElsifPath>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ELSE_PATH:
				getElsePath().clear();
				getElsePath().addAll((Collection<? extends ElsePath>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__CASE_PATH:
				getCasePath().clear();
				getCasePath().addAll((Collection<? extends CasePath>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__SELECT_PATH:
				getSelectPath().clear();
				getSelectPath().addAll((Collection<? extends SelectPath>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__OR_PATH:
				getOrPath().clear();
				getOrPath().addAll((Collection<? extends OrPath>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__THEN_ABORT_PATH:
				getThenAbortPath().clear();
				getThenAbortPath().addAll((Collection<? extends ThenAbortPath>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__CASE_EXPRESSION_PATH:
				getCaseExpressionPath().clear();
				getCaseExpressionPath().addAll((Collection<? extends CaseExpressionPath>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__IF_EXPRESSION_PATH:
				getIfExpressionPath().clear();
				getIfExpressionPath().addAll((Collection<? extends IfExpressionPath>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ELSIF_EXPRESSION_PATH:
				getElsifExpressionPath().clear();
				getElsifExpressionPath().addAll((Collection<? extends ElsifExpressionPath>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ELSE_EXPRESSION_PATH:
				getElseExpressionPath().clear();
				getElseExpressionPath().addAll((Collection<? extends ElseExpressionPath>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__USE_PACKAGE_CLAUSE:
				getUsePackageClause().clear();
				getUsePackageClause().addAll((Collection<? extends UsePackageClause>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__USE_TYPE_CLAUSE:
				getUseTypeClause().clear();
				getUseTypeClause().addAll((Collection<? extends UseTypeClause>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__USE_ALL_TYPE_CLAUSE:
				getUseAllTypeClause().clear();
				getUseAllTypeClause().addAll((Collection<? extends UseAllTypeClause>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__WITH_CLAUSE:
				getWithClause().clear();
				getWithClause().addAll((Collection<? extends WithClause>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ATTRIBUTE_DEFINITION_CLAUSE:
				getAttributeDefinitionClause().clear();
				getAttributeDefinitionClause().addAll((Collection<? extends AttributeDefinitionClause>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ENUMERATION_REPRESENTATION_CLAUSE:
				getEnumerationRepresentationClause().clear();
				getEnumerationRepresentationClause().addAll((Collection<? extends EnumerationRepresentationClause>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__RECORD_REPRESENTATION_CLAUSE:
				getRecordRepresentationClause().clear();
				getRecordRepresentationClause().addAll((Collection<? extends RecordRepresentationClause>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__AT_CLAUSE:
				getAtClause().clear();
				getAtClause().addAll((Collection<? extends AtClause>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__COMPONENT_CLAUSE:
				getComponentClause().clear();
				getComponentClause().addAll((Collection<? extends ComponentClause>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__EXCEPTION_HANDLER:
				getExceptionHandler().clear();
				getExceptionHandler().addAll((Collection<? extends ExceptionHandler>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__COMMENT:
				getComment().clear();
				getComment().addAll((Collection<? extends Comment>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ALL_CALLS_REMOTE_PRAGMA:
				getAllCallsRemotePragma().clear();
				getAllCallsRemotePragma().addAll((Collection<? extends AllCallsRemotePragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ASYNCHRONOUS_PRAGMA:
				getAsynchronousPragma().clear();
				getAsynchronousPragma().addAll((Collection<? extends AsynchronousPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ATOMIC_PRAGMA:
				getAtomicPragma().clear();
				getAtomicPragma().addAll((Collection<? extends AtomicPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ATOMIC_COMPONENTS_PRAGMA:
				getAtomicComponentsPragma().clear();
				getAtomicComponentsPragma().addAll((Collection<? extends AtomicComponentsPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ATTACH_HANDLER_PRAGMA:
				getAttachHandlerPragma().clear();
				getAttachHandlerPragma().addAll((Collection<? extends AttachHandlerPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__CONTROLLED_PRAGMA:
				getControlledPragma().clear();
				getControlledPragma().addAll((Collection<? extends ControlledPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__CONVENTION_PRAGMA:
				getConventionPragma().clear();
				getConventionPragma().addAll((Collection<? extends ConventionPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DISCARD_NAMES_PRAGMA:
				getDiscardNamesPragma().clear();
				getDiscardNamesPragma().addAll((Collection<? extends DiscardNamesPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ELABORATE_PRAGMA:
				getElaboratePragma().clear();
				getElaboratePragma().addAll((Collection<? extends ElaboratePragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ELABORATE_ALL_PRAGMA:
				getElaborateAllPragma().clear();
				getElaborateAllPragma().addAll((Collection<? extends ElaborateAllPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ELABORATE_BODY_PRAGMA:
				getElaborateBodyPragma().clear();
				getElaborateBodyPragma().addAll((Collection<? extends ElaborateBodyPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__EXPORT_PRAGMA:
				getExportPragma().clear();
				getExportPragma().addAll((Collection<? extends ExportPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__IMPORT_PRAGMA:
				getImportPragma().clear();
				getImportPragma().addAll((Collection<? extends ImportPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__INLINE_PRAGMA:
				getInlinePragma().clear();
				getInlinePragma().addAll((Collection<? extends InlinePragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__INSPECTION_POINT_PRAGMA:
				getInspectionPointPragma().clear();
				getInspectionPointPragma().addAll((Collection<? extends InspectionPointPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__INTERRUPT_HANDLER_PRAGMA:
				getInterruptHandlerPragma().clear();
				getInterruptHandlerPragma().addAll((Collection<? extends InterruptHandlerPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__INTERRUPT_PRIORITY_PRAGMA:
				getInterruptPriorityPragma().clear();
				getInterruptPriorityPragma().addAll((Collection<? extends InterruptPriorityPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__LINKER_OPTIONS_PRAGMA:
				getLinkerOptionsPragma().clear();
				getLinkerOptionsPragma().addAll((Collection<? extends LinkerOptionsPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__LIST_PRAGMA:
				getListPragma().clear();
				getListPragma().addAll((Collection<? extends ListPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__LOCKING_POLICY_PRAGMA:
				getLockingPolicyPragma().clear();
				getLockingPolicyPragma().addAll((Collection<? extends LockingPolicyPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__NORMALIZE_SCALARS_PRAGMA:
				getNormalizeScalarsPragma().clear();
				getNormalizeScalarsPragma().addAll((Collection<? extends NormalizeScalarsPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__OPTIMIZE_PRAGMA:
				getOptimizePragma().clear();
				getOptimizePragma().addAll((Collection<? extends OptimizePragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PACK_PRAGMA:
				getPackPragma().clear();
				getPackPragma().addAll((Collection<? extends PackPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PAGE_PRAGMA:
				getPagePragma().clear();
				getPagePragma().addAll((Collection<? extends PagePragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PREELABORATE_PRAGMA:
				getPreelaboratePragma().clear();
				getPreelaboratePragma().addAll((Collection<? extends PreelaboratePragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PRIORITY_PRAGMA:
				getPriorityPragma().clear();
				getPriorityPragma().addAll((Collection<? extends PriorityPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PURE_PRAGMA:
				getPurePragma().clear();
				getPurePragma().addAll((Collection<? extends PurePragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__QUEUING_POLICY_PRAGMA:
				getQueuingPolicyPragma().clear();
				getQueuingPolicyPragma().addAll((Collection<? extends QueuingPolicyPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				getRemoteCallInterfacePragma().clear();
				getRemoteCallInterfacePragma().addAll((Collection<? extends RemoteCallInterfacePragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__REMOTE_TYPES_PRAGMA:
				getRemoteTypesPragma().clear();
				getRemoteTypesPragma().addAll((Collection<? extends RemoteTypesPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__RESTRICTIONS_PRAGMA:
				getRestrictionsPragma().clear();
				getRestrictionsPragma().addAll((Collection<? extends RestrictionsPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__REVIEWABLE_PRAGMA:
				getReviewablePragma().clear();
				getReviewablePragma().addAll((Collection<? extends ReviewablePragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__SHARED_PASSIVE_PRAGMA:
				getSharedPassivePragma().clear();
				getSharedPassivePragma().addAll((Collection<? extends SharedPassivePragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__STORAGE_SIZE_PRAGMA:
				getStorageSizePragma().clear();
				getStorageSizePragma().addAll((Collection<? extends StorageSizePragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__SUPPRESS_PRAGMA:
				getSuppressPragma().clear();
				getSuppressPragma().addAll((Collection<? extends SuppressPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				getTaskDispatchingPolicyPragma().clear();
				getTaskDispatchingPolicyPragma().addAll((Collection<? extends TaskDispatchingPolicyPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__VOLATILE_PRAGMA:
				getVolatilePragma().clear();
				getVolatilePragma().addAll((Collection<? extends VolatilePragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__VOLATILE_COMPONENTS_PRAGMA:
				getVolatileComponentsPragma().clear();
				getVolatileComponentsPragma().addAll((Collection<? extends VolatileComponentsPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ASSERT_PRAGMA:
				getAssertPragma().clear();
				getAssertPragma().addAll((Collection<? extends AssertPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__ASSERTION_POLICY_PRAGMA:
				getAssertionPolicyPragma().clear();
				getAssertionPolicyPragma().addAll((Collection<? extends AssertionPolicyPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DETECT_BLOCKING_PRAGMA:
				getDetectBlockingPragma().clear();
				getDetectBlockingPragma().addAll((Collection<? extends DetectBlockingPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__NO_RETURN_PRAGMA:
				getNoReturnPragma().clear();
				getNoReturnPragma().addAll((Collection<? extends NoReturnPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				getPartitionElaborationPolicyPragma().clear();
				getPartitionElaborationPolicyPragma().addAll((Collection<? extends PartitionElaborationPolicyPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				getPreelaborableInitializationPragma().clear();
				getPreelaborableInitializationPragma().addAll((Collection<? extends PreelaborableInitializationPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				getPrioritySpecificDispatchingPragma().clear();
				getPrioritySpecificDispatchingPragma().addAll((Collection<? extends PrioritySpecificDispatchingPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__PROFILE_PRAGMA:
				getProfilePragma().clear();
				getProfilePragma().addAll((Collection<? extends ProfilePragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__RELATIVE_DEADLINE_PRAGMA:
				getRelativeDeadlinePragma().clear();
				getRelativeDeadlinePragma().addAll((Collection<? extends RelativeDeadlinePragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__UNCHECKED_UNION_PRAGMA:
				getUncheckedUnionPragma().clear();
				getUncheckedUnionPragma().addAll((Collection<? extends UncheckedUnionPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__UNSUPPRESS_PRAGMA:
				getUnsuppressPragma().clear();
				getUnsuppressPragma().addAll((Collection<? extends UnsuppressPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				getDefaultStoragePoolPragma().clear();
				getDefaultStoragePoolPragma().addAll((Collection<? extends DefaultStoragePoolPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__DISPATCHING_DOMAIN_PRAGMA:
				getDispatchingDomainPragma().clear();
				getDispatchingDomainPragma().addAll((Collection<? extends DispatchingDomainPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__CPU_PRAGMA:
				getCpuPragma().clear();
				getCpuPragma().addAll((Collection<? extends CpuPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__INDEPENDENT_PRAGMA:
				getIndependentPragma().clear();
				getIndependentPragma().addAll((Collection<? extends IndependentPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				getIndependentComponentsPragma().clear();
				getIndependentComponentsPragma().addAll((Collection<? extends IndependentComponentsPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				getImplementationDefinedPragma().clear();
				getImplementationDefinedPragma().addAll((Collection<? extends ImplementationDefinedPragma>)newValue);
				return;
			case AdaPackage.ELEMENT_LIST__UNKNOWN_PRAGMA:
				getUnknownPragma().clear();
				getUnknownPragma().addAll((Collection<? extends UnknownPragma>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.ELEMENT_LIST__GROUP:
				getGroup().clear();
				return;
			case AdaPackage.ELEMENT_LIST__NOT_AN_ELEMENT:
				getNotAnElement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_IDENTIFIER:
				getDefiningIdentifier().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_CHARACTER_LITERAL:
				getDefiningCharacterLiteral().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_ENUMERATION_LITERAL:
				getDefiningEnumerationLiteral().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_AND_OPERATOR:
				getDefiningAndOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_OR_OPERATOR:
				getDefiningOrOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_XOR_OPERATOR:
				getDefiningXorOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_EQUAL_OPERATOR:
				getDefiningEqualOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_NOT_EQUAL_OPERATOR:
				getDefiningNotEqualOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_LESS_THAN_OPERATOR:
				getDefiningLessThanOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_LESS_THAN_OR_EQUAL_OPERATOR:
				getDefiningLessThanOrEqualOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_GREATER_THAN_OPERATOR:
				getDefiningGreaterThanOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_GREATER_THAN_OR_EQUAL_OPERATOR:
				getDefiningGreaterThanOrEqualOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_PLUS_OPERATOR:
				getDefiningPlusOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_MINUS_OPERATOR:
				getDefiningMinusOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_CONCATENATE_OPERATOR:
				getDefiningConcatenateOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_UNARY_PLUS_OPERATOR:
				getDefiningUnaryPlusOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_UNARY_MINUS_OPERATOR:
				getDefiningUnaryMinusOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_MULTIPLY_OPERATOR:
				getDefiningMultiplyOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_DIVIDE_OPERATOR:
				getDefiningDivideOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_MOD_OPERATOR:
				getDefiningModOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_REM_OPERATOR:
				getDefiningRemOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_EXPONENTIATE_OPERATOR:
				getDefiningExponentiateOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_ABS_OPERATOR:
				getDefiningAbsOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_NOT_OPERATOR:
				getDefiningNotOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINING_EXPANDED_NAME:
				getDefiningExpandedName().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ORDINARY_TYPE_DECLARATION:
				getOrdinaryTypeDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__TASK_TYPE_DECLARATION:
				getTaskTypeDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PROTECTED_TYPE_DECLARATION:
				getProtectedTypeDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__INCOMPLETE_TYPE_DECLARATION:
				getIncompleteTypeDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__TAGGED_INCOMPLETE_TYPE_DECLARATION:
				getTaggedIncompleteTypeDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PRIVATE_TYPE_DECLARATION:
				getPrivateTypeDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PRIVATE_EXTENSION_DECLARATION:
				getPrivateExtensionDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__SUBTYPE_DECLARATION:
				getSubtypeDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__VARIABLE_DECLARATION:
				getVariableDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__CONSTANT_DECLARATION:
				getConstantDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFERRED_CONSTANT_DECLARATION:
				getDeferredConstantDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__SINGLE_TASK_DECLARATION:
				getSingleTaskDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__SINGLE_PROTECTED_DECLARATION:
				getSingleProtectedDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__INTEGER_NUMBER_DECLARATION:
				getIntegerNumberDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__REAL_NUMBER_DECLARATION:
				getRealNumberDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ENUMERATION_LITERAL_SPECIFICATION:
				getEnumerationLiteralSpecification().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DISCRIMINANT_SPECIFICATION:
				getDiscriminantSpecification().clear();
				return;
			case AdaPackage.ELEMENT_LIST__COMPONENT_DECLARATION:
				getComponentDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__LOOP_PARAMETER_SPECIFICATION:
				getLoopParameterSpecification().clear();
				return;
			case AdaPackage.ELEMENT_LIST__GENERALIZED_ITERATOR_SPECIFICATION:
				getGeneralizedIteratorSpecification().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ELEMENT_ITERATOR_SPECIFICATION:
				getElementIteratorSpecification().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PROCEDURE_DECLARATION:
				getProcedureDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FUNCTION_DECLARATION:
				getFunctionDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PARAMETER_SPECIFICATION:
				getParameterSpecification().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PROCEDURE_BODY_DECLARATION:
				getProcedureBodyDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FUNCTION_BODY_DECLARATION:
				getFunctionBodyDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__RETURN_VARIABLE_SPECIFICATION:
				getReturnVariableSpecification().clear();
				return;
			case AdaPackage.ELEMENT_LIST__RETURN_CONSTANT_SPECIFICATION:
				getReturnConstantSpecification().clear();
				return;
			case AdaPackage.ELEMENT_LIST__NULL_PROCEDURE_DECLARATION:
				getNullProcedureDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__EXPRESSION_FUNCTION_DECLARATION:
				getExpressionFunctionDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PACKAGE_DECLARATION:
				getPackageDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PACKAGE_BODY_DECLARATION:
				getPackageBodyDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__OBJECT_RENAMING_DECLARATION:
				getObjectRenamingDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__EXCEPTION_RENAMING_DECLARATION:
				getExceptionRenamingDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PACKAGE_RENAMING_DECLARATION:
				getPackageRenamingDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PROCEDURE_RENAMING_DECLARATION:
				getProcedureRenamingDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FUNCTION_RENAMING_DECLARATION:
				getFunctionRenamingDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__GENERIC_PACKAGE_RENAMING_DECLARATION:
				getGenericPackageRenamingDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__GENERIC_PROCEDURE_RENAMING_DECLARATION:
				getGenericProcedureRenamingDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__GENERIC_FUNCTION_RENAMING_DECLARATION:
				getGenericFunctionRenamingDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__TASK_BODY_DECLARATION:
				getTaskBodyDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PROTECTED_BODY_DECLARATION:
				getProtectedBodyDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ENTRY_DECLARATION:
				getEntryDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ENTRY_BODY_DECLARATION:
				getEntryBodyDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ENTRY_INDEX_SPECIFICATION:
				getEntryIndexSpecification().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PROCEDURE_BODY_STUB:
				getProcedureBodyStub().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FUNCTION_BODY_STUB:
				getFunctionBodyStub().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PACKAGE_BODY_STUB:
				getPackageBodyStub().clear();
				return;
			case AdaPackage.ELEMENT_LIST__TASK_BODY_STUB:
				getTaskBodyStub().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PROTECTED_BODY_STUB:
				getProtectedBodyStub().clear();
				return;
			case AdaPackage.ELEMENT_LIST__EXCEPTION_DECLARATION:
				getExceptionDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__CHOICE_PARAMETER_SPECIFICATION:
				getChoiceParameterSpecification().clear();
				return;
			case AdaPackage.ELEMENT_LIST__GENERIC_PROCEDURE_DECLARATION:
				getGenericProcedureDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__GENERIC_FUNCTION_DECLARATION:
				getGenericFunctionDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__GENERIC_PACKAGE_DECLARATION:
				getGenericPackageDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PACKAGE_INSTANTIATION:
				getPackageInstantiation().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PROCEDURE_INSTANTIATION:
				getProcedureInstantiation().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FUNCTION_INSTANTIATION:
				getFunctionInstantiation().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_OBJECT_DECLARATION:
				getFormalObjectDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_TYPE_DECLARATION:
				getFormalTypeDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_INCOMPLETE_TYPE_DECLARATION:
				getFormalIncompleteTypeDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_PROCEDURE_DECLARATION:
				getFormalProcedureDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_FUNCTION_DECLARATION:
				getFormalFunctionDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_PACKAGE_DECLARATION:
				getFormalPackageDeclaration().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_PACKAGE_DECLARATION_WITH_BOX:
				getFormalPackageDeclarationWithBox().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DERIVED_TYPE_DEFINITION:
				getDerivedTypeDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DERIVED_RECORD_EXTENSION_DEFINITION:
				getDerivedRecordExtensionDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ENUMERATION_TYPE_DEFINITION:
				getEnumerationTypeDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__SIGNED_INTEGER_TYPE_DEFINITION:
				getSignedIntegerTypeDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__MODULAR_TYPE_DEFINITION:
				getModularTypeDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ROOT_INTEGER_DEFINITION:
				getRootIntegerDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ROOT_REAL_DEFINITION:
				getRootRealDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__UNIVERSAL_INTEGER_DEFINITION:
				getUniversalIntegerDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__UNIVERSAL_REAL_DEFINITION:
				getUniversalRealDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__UNIVERSAL_FIXED_DEFINITION:
				getUniversalFixedDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FLOATING_POINT_DEFINITION:
				getFloatingPointDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ORDINARY_FIXED_POINT_DEFINITION:
				getOrdinaryFixedPointDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DECIMAL_FIXED_POINT_DEFINITION:
				getDecimalFixedPointDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__UNCONSTRAINED_ARRAY_DEFINITION:
				getUnconstrainedArrayDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__CONSTRAINED_ARRAY_DEFINITION:
				getConstrainedArrayDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__RECORD_TYPE_DEFINITION:
				getRecordTypeDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__TAGGED_RECORD_TYPE_DEFINITION:
				getTaggedRecordTypeDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ORDINARY_INTERFACE:
				getOrdinaryInterface().clear();
				return;
			case AdaPackage.ELEMENT_LIST__LIMITED_INTERFACE:
				getLimitedInterface().clear();
				return;
			case AdaPackage.ELEMENT_LIST__TASK_INTERFACE:
				getTaskInterface().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PROTECTED_INTERFACE:
				getProtectedInterface().clear();
				return;
			case AdaPackage.ELEMENT_LIST__SYNCHRONIZED_INTERFACE:
				getSynchronizedInterface().clear();
				return;
			case AdaPackage.ELEMENT_LIST__POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				getPoolSpecificAccessToVariable().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_VARIABLE:
				getAccessToVariable().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_CONSTANT:
				getAccessToConstant().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_PROCEDURE:
				getAccessToProcedure().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_PROTECTED_PROCEDURE:
				getAccessToProtectedProcedure().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_FUNCTION:
				getAccessToFunction().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_PROTECTED_FUNCTION:
				getAccessToProtectedFunction().clear();
				return;
			case AdaPackage.ELEMENT_LIST__SUBTYPE_INDICATION:
				getSubtypeIndication().clear();
				return;
			case AdaPackage.ELEMENT_LIST__RANGE_ATTRIBUTE_REFERENCE:
				getRangeAttributeReference().clear();
				return;
			case AdaPackage.ELEMENT_LIST__SIMPLE_EXPRESSION_RANGE:
				getSimpleExpressionRange().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DIGITS_CONSTRAINT:
				getDigitsConstraint().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DELTA_CONSTRAINT:
				getDeltaConstraint().clear();
				return;
			case AdaPackage.ELEMENT_LIST__INDEX_CONSTRAINT:
				getIndexConstraint().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DISCRIMINANT_CONSTRAINT:
				getDiscriminantConstraint().clear();
				return;
			case AdaPackage.ELEMENT_LIST__COMPONENT_DEFINITION:
				getComponentDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION:
				getDiscreteSubtypeIndicationAsSubtypeDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION:
				getDiscreteRangeAttributeReferenceAsSubtypeDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION:
				getDiscreteSimpleExpressionRangeAsSubtypeDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DISCRETE_SUBTYPE_INDICATION:
				getDiscreteSubtypeIndication().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DISCRETE_RANGE_ATTRIBUTE_REFERENCE:
				getDiscreteRangeAttributeReference().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DISCRETE_SIMPLE_EXPRESSION_RANGE:
				getDiscreteSimpleExpressionRange().clear();
				return;
			case AdaPackage.ELEMENT_LIST__UNKNOWN_DISCRIMINANT_PART:
				getUnknownDiscriminantPart().clear();
				return;
			case AdaPackage.ELEMENT_LIST__KNOWN_DISCRIMINANT_PART:
				getKnownDiscriminantPart().clear();
				return;
			case AdaPackage.ELEMENT_LIST__RECORD_DEFINITION:
				getRecordDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__NULL_RECORD_DEFINITION:
				getNullRecordDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__NULL_COMPONENT:
				getNullComponent().clear();
				return;
			case AdaPackage.ELEMENT_LIST__VARIANT_PART:
				getVariantPart().clear();
				return;
			case AdaPackage.ELEMENT_LIST__VARIANT:
				getVariant().clear();
				return;
			case AdaPackage.ELEMENT_LIST__OTHERS_CHOICE:
				getOthersChoice().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_VARIABLE:
				getAnonymousAccessToVariable().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_CONSTANT:
				getAnonymousAccessToConstant().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_PROCEDURE:
				getAnonymousAccessToProcedure().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE:
				getAnonymousAccessToProtectedProcedure().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_FUNCTION:
				getAnonymousAccessToFunction().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION:
				getAnonymousAccessToProtectedFunction().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PRIVATE_TYPE_DEFINITION:
				getPrivateTypeDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__TAGGED_PRIVATE_TYPE_DEFINITION:
				getTaggedPrivateTypeDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PRIVATE_EXTENSION_DEFINITION:
				getPrivateExtensionDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__TASK_DEFINITION:
				getTaskDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PROTECTED_DEFINITION:
				getProtectedDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_PRIVATE_TYPE_DEFINITION:
				getFormalPrivateTypeDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION:
				getFormalTaggedPrivateTypeDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_DERIVED_TYPE_DEFINITION:
				getFormalDerivedTypeDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_DISCRETE_TYPE_DEFINITION:
				getFormalDiscreteTypeDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_SIGNED_INTEGER_TYPE_DEFINITION:
				getFormalSignedIntegerTypeDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_MODULAR_TYPE_DEFINITION:
				getFormalModularTypeDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_FLOATING_POINT_DEFINITION:
				getFormalFloatingPointDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_ORDINARY_FIXED_POINT_DEFINITION:
				getFormalOrdinaryFixedPointDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_DECIMAL_FIXED_POINT_DEFINITION:
				getFormalDecimalFixedPointDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_ORDINARY_INTERFACE:
				getFormalOrdinaryInterface().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_LIMITED_INTERFACE:
				getFormalLimitedInterface().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_TASK_INTERFACE:
				getFormalTaskInterface().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_PROTECTED_INTERFACE:
				getFormalProtectedInterface().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_SYNCHRONIZED_INTERFACE:
				getFormalSynchronizedInterface().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_UNCONSTRAINED_ARRAY_DEFINITION:
				getFormalUnconstrainedArrayDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_CONSTRAINED_ARRAY_DEFINITION:
				getFormalConstrainedArrayDefinition().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				getFormalPoolSpecificAccessToVariable().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_VARIABLE:
				getFormalAccessToVariable().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_CONSTANT:
				getFormalAccessToConstant().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_PROCEDURE:
				getFormalAccessToProcedure().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_PROTECTED_PROCEDURE:
				getFormalAccessToProtectedProcedure().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_FUNCTION:
				getFormalAccessToFunction().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_PROTECTED_FUNCTION:
				getFormalAccessToProtectedFunction().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ASPECT_SPECIFICATION:
				getAspectSpecification().clear();
				return;
			case AdaPackage.ELEMENT_LIST__BOX_EXPRESSION:
				getBoxExpression().clear();
				return;
			case AdaPackage.ELEMENT_LIST__INTEGER_LITERAL:
				getIntegerLiteral().clear();
				return;
			case AdaPackage.ELEMENT_LIST__REAL_LITERAL:
				getRealLiteral().clear();
				return;
			case AdaPackage.ELEMENT_LIST__STRING_LITERAL:
				getStringLiteral().clear();
				return;
			case AdaPackage.ELEMENT_LIST__IDENTIFIER:
				getIdentifier().clear();
				return;
			case AdaPackage.ELEMENT_LIST__AND_OPERATOR:
				getAndOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__OR_OPERATOR:
				getOrOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__XOR_OPERATOR:
				getXorOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__EQUAL_OPERATOR:
				getEqualOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__NOT_EQUAL_OPERATOR:
				getNotEqualOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__LESS_THAN_OPERATOR:
				getLessThanOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__LESS_THAN_OR_EQUAL_OPERATOR:
				getLessThanOrEqualOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__GREATER_THAN_OPERATOR:
				getGreaterThanOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__GREATER_THAN_OR_EQUAL_OPERATOR:
				getGreaterThanOrEqualOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PLUS_OPERATOR:
				getPlusOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__MINUS_OPERATOR:
				getMinusOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__CONCATENATE_OPERATOR:
				getConcatenateOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__UNARY_PLUS_OPERATOR:
				getUnaryPlusOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__UNARY_MINUS_OPERATOR:
				getUnaryMinusOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__MULTIPLY_OPERATOR:
				getMultiplyOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DIVIDE_OPERATOR:
				getDivideOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__MOD_OPERATOR:
				getModOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__REM_OPERATOR:
				getRemOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__EXPONENTIATE_OPERATOR:
				getExponentiateOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ABS_OPERATOR:
				getAbsOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__NOT_OPERATOR:
				getNotOperator().clear();
				return;
			case AdaPackage.ELEMENT_LIST__CHARACTER_LITERAL:
				getCharacterLiteral().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ENUMERATION_LITERAL:
				getEnumerationLiteral().clear();
				return;
			case AdaPackage.ELEMENT_LIST__EXPLICIT_DEREFERENCE:
				getExplicitDereference().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FUNCTION_CALL:
				getFunctionCall().clear();
				return;
			case AdaPackage.ELEMENT_LIST__INDEXED_COMPONENT:
				getIndexedComponent().clear();
				return;
			case AdaPackage.ELEMENT_LIST__SLICE:
				getSlice().clear();
				return;
			case AdaPackage.ELEMENT_LIST__SELECTED_COMPONENT:
				getSelectedComponent().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ACCESS_ATTRIBUTE:
				getAccessAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ADDRESS_ATTRIBUTE:
				getAddressAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ADJACENT_ATTRIBUTE:
				getAdjacentAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__AFT_ATTRIBUTE:
				getAftAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ALIGNMENT_ATTRIBUTE:
				getAlignmentAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__BASE_ATTRIBUTE:
				getBaseAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__BIT_ORDER_ATTRIBUTE:
				getBitOrderAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__BODY_VERSION_ATTRIBUTE:
				getBodyVersionAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__CALLABLE_ATTRIBUTE:
				getCallableAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__CALLER_ATTRIBUTE:
				getCallerAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__CEILING_ATTRIBUTE:
				getCeilingAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__CLASS_ATTRIBUTE:
				getClassAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__COMPONENT_SIZE_ATTRIBUTE:
				getComponentSizeAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__COMPOSE_ATTRIBUTE:
				getComposeAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__CONSTRAINED_ATTRIBUTE:
				getConstrainedAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__COPY_SIGN_ATTRIBUTE:
				getCopySignAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__COUNT_ATTRIBUTE:
				getCountAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFINITE_ATTRIBUTE:
				getDefiniteAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DELTA_ATTRIBUTE:
				getDeltaAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DENORM_ATTRIBUTE:
				getDenormAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DIGITS_ATTRIBUTE:
				getDigitsAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__EXPONENT_ATTRIBUTE:
				getExponentAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__EXTERNAL_TAG_ATTRIBUTE:
				getExternalTagAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FIRST_ATTRIBUTE:
				getFirstAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FIRST_BIT_ATTRIBUTE:
				getFirstBitAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FLOOR_ATTRIBUTE:
				getFloorAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FORE_ATTRIBUTE:
				getForeAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FRACTION_ATTRIBUTE:
				getFractionAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__IDENTITY_ATTRIBUTE:
				getIdentityAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__IMAGE_ATTRIBUTE:
				getImageAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__INPUT_ATTRIBUTE:
				getInputAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__LAST_ATTRIBUTE:
				getLastAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__LAST_BIT_ATTRIBUTE:
				getLastBitAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__LEADING_PART_ATTRIBUTE:
				getLeadingPartAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__LENGTH_ATTRIBUTE:
				getLengthAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__MACHINE_ATTRIBUTE:
				getMachineAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__MACHINE_EMAX_ATTRIBUTE:
				getMachineEmaxAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__MACHINE_EMIN_ATTRIBUTE:
				getMachineEminAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__MACHINE_MANTISSA_ATTRIBUTE:
				getMachineMantissaAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__MACHINE_OVERFLOWS_ATTRIBUTE:
				getMachineOverflowsAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__MACHINE_RADIX_ATTRIBUTE:
				getMachineRadixAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__MACHINE_ROUNDS_ATTRIBUTE:
				getMachineRoundsAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__MAX_ATTRIBUTE:
				getMaxAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE:
				getMaxSizeInStorageElementsAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__MIN_ATTRIBUTE:
				getMinAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__MODEL_ATTRIBUTE:
				getModelAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__MODEL_EMIN_ATTRIBUTE:
				getModelEminAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__MODEL_EPSILON_ATTRIBUTE:
				getModelEpsilonAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__MODEL_MANTISSA_ATTRIBUTE:
				getModelMantissaAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__MODEL_SMALL_ATTRIBUTE:
				getModelSmallAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__MODULUS_ATTRIBUTE:
				getModulusAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__OUTPUT_ATTRIBUTE:
				getOutputAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PARTITION_ID_ATTRIBUTE:
				getPartitionIdAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__POS_ATTRIBUTE:
				getPosAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__POSITION_ATTRIBUTE:
				getPositionAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PRED_ATTRIBUTE:
				getPredAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__RANGE_ATTRIBUTE:
				getRangeAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__READ_ATTRIBUTE:
				getReadAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__REMAINDER_ATTRIBUTE:
				getRemainderAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ROUND_ATTRIBUTE:
				getRoundAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ROUNDING_ATTRIBUTE:
				getRoundingAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__SAFE_FIRST_ATTRIBUTE:
				getSafeFirstAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__SAFE_LAST_ATTRIBUTE:
				getSafeLastAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__SCALE_ATTRIBUTE:
				getScaleAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__SCALING_ATTRIBUTE:
				getScalingAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__SIGNED_ZEROS_ATTRIBUTE:
				getSignedZerosAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__SIZE_ATTRIBUTE:
				getSizeAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__SMALL_ATTRIBUTE:
				getSmallAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__STORAGE_POOL_ATTRIBUTE:
				getStoragePoolAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__STORAGE_SIZE_ATTRIBUTE:
				getStorageSizeAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__SUCC_ATTRIBUTE:
				getSuccAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__TAG_ATTRIBUTE:
				getTagAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__TERMINATED_ATTRIBUTE:
				getTerminatedAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__TRUNCATION_ATTRIBUTE:
				getTruncationAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__UNBIASED_ROUNDING_ATTRIBUTE:
				getUnbiasedRoundingAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__UNCHECKED_ACCESS_ATTRIBUTE:
				getUncheckedAccessAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__VAL_ATTRIBUTE:
				getValAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__VALID_ATTRIBUTE:
				getValidAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__VALUE_ATTRIBUTE:
				getValueAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__VERSION_ATTRIBUTE:
				getVersionAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__WIDE_IMAGE_ATTRIBUTE:
				getWideImageAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__WIDE_VALUE_ATTRIBUTE:
				getWideValueAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__WIDE_WIDTH_ATTRIBUTE:
				getWideWidthAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__WIDTH_ATTRIBUTE:
				getWidthAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__WRITE_ATTRIBUTE:
				getWriteAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__MACHINE_ROUNDING_ATTRIBUTE:
				getMachineRoundingAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__MOD_ATTRIBUTE:
				getModAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PRIORITY_ATTRIBUTE:
				getPriorityAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__STREAM_SIZE_ATTRIBUTE:
				getStreamSizeAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__WIDE_WIDE_IMAGE_ATTRIBUTE:
				getWideWideImageAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__WIDE_WIDE_VALUE_ATTRIBUTE:
				getWideWideValueAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__WIDE_WIDE_WIDTH_ATTRIBUTE:
				getWideWideWidthAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE:
				getMaxAlignmentForAllocationAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__OVERLAPS_STORAGE_ATTRIBUTE:
				getOverlapsStorageAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__IMPLEMENTATION_DEFINED_ATTRIBUTE:
				getImplementationDefinedAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__UNKNOWN_ATTRIBUTE:
				getUnknownAttribute().clear();
				return;
			case AdaPackage.ELEMENT_LIST__RECORD_AGGREGATE:
				getRecordAggregate().clear();
				return;
			case AdaPackage.ELEMENT_LIST__EXTENSION_AGGREGATE:
				getExtensionAggregate().clear();
				return;
			case AdaPackage.ELEMENT_LIST__POSITIONAL_ARRAY_AGGREGATE:
				getPositionalArrayAggregate().clear();
				return;
			case AdaPackage.ELEMENT_LIST__NAMED_ARRAY_AGGREGATE:
				getNamedArrayAggregate().clear();
				return;
			case AdaPackage.ELEMENT_LIST__AND_THEN_SHORT_CIRCUIT:
				getAndThenShortCircuit().clear();
				return;
			case AdaPackage.ELEMENT_LIST__OR_ELSE_SHORT_CIRCUIT:
				getOrElseShortCircuit().clear();
				return;
			case AdaPackage.ELEMENT_LIST__IN_MEMBERSHIP_TEST:
				getInMembershipTest().clear();
				return;
			case AdaPackage.ELEMENT_LIST__NOT_IN_MEMBERSHIP_TEST:
				getNotInMembershipTest().clear();
				return;
			case AdaPackage.ELEMENT_LIST__NULL_LITERAL:
				getNullLiteral().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PARENTHESIZED_EXPRESSION:
				getParenthesizedExpression().clear();
				return;
			case AdaPackage.ELEMENT_LIST__RAISE_EXPRESSION:
				getRaiseExpression().clear();
				return;
			case AdaPackage.ELEMENT_LIST__TYPE_CONVERSION:
				getTypeConversion().clear();
				return;
			case AdaPackage.ELEMENT_LIST__QUALIFIED_EXPRESSION:
				getQualifiedExpression().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ALLOCATION_FROM_SUBTYPE:
				getAllocationFromSubtype().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ALLOCATION_FROM_QUALIFIED_EXPRESSION:
				getAllocationFromQualifiedExpression().clear();
				return;
			case AdaPackage.ELEMENT_LIST__CASE_EXPRESSION:
				getCaseExpression().clear();
				return;
			case AdaPackage.ELEMENT_LIST__IF_EXPRESSION:
				getIfExpression().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FOR_ALL_QUANTIFIED_EXPRESSION:
				getForAllQuantifiedExpression().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FOR_SOME_QUANTIFIED_EXPRESSION:
				getForSomeQuantifiedExpression().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PRAGMA_ARGUMENT_ASSOCIATION:
				getPragmaArgumentAssociation().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DISCRIMINANT_ASSOCIATION:
				getDiscriminantAssociation().clear();
				return;
			case AdaPackage.ELEMENT_LIST__RECORD_COMPONENT_ASSOCIATION:
				getRecordComponentAssociation().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ARRAY_COMPONENT_ASSOCIATION:
				getArrayComponentAssociation().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PARAMETER_ASSOCIATION:
				getParameterAssociation().clear();
				return;
			case AdaPackage.ELEMENT_LIST__GENERIC_ASSOCIATION:
				getGenericAssociation().clear();
				return;
			case AdaPackage.ELEMENT_LIST__NULL_STATEMENT:
				getNullStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ASSIGNMENT_STATEMENT:
				getAssignmentStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__IF_STATEMENT:
				getIfStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__CASE_STATEMENT:
				getCaseStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__LOOP_STATEMENT:
				getLoopStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__WHILE_LOOP_STATEMENT:
				getWhileLoopStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__FOR_LOOP_STATEMENT:
				getForLoopStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__BLOCK_STATEMENT:
				getBlockStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__EXIT_STATEMENT:
				getExitStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__GOTO_STATEMENT:
				getGotoStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PROCEDURE_CALL_STATEMENT:
				getProcedureCallStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__RETURN_STATEMENT:
				getReturnStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__EXTENDED_RETURN_STATEMENT:
				getExtendedReturnStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ACCEPT_STATEMENT:
				getAcceptStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ENTRY_CALL_STATEMENT:
				getEntryCallStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__REQUEUE_STATEMENT:
				getRequeueStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__REQUEUE_STATEMENT_WITH_ABORT:
				getRequeueStatementWithAbort().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DELAY_UNTIL_STATEMENT:
				getDelayUntilStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DELAY_RELATIVE_STATEMENT:
				getDelayRelativeStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__TERMINATE_ALTERNATIVE_STATEMENT:
				getTerminateAlternativeStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__SELECTIVE_ACCEPT_STATEMENT:
				getSelectiveAcceptStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__TIMED_ENTRY_CALL_STATEMENT:
				getTimedEntryCallStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__CONDITIONAL_ENTRY_CALL_STATEMENT:
				getConditionalEntryCallStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ASYNCHRONOUS_SELECT_STATEMENT:
				getAsynchronousSelectStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ABORT_STATEMENT:
				getAbortStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__RAISE_STATEMENT:
				getRaiseStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__CODE_STATEMENT:
				getCodeStatement().clear();
				return;
			case AdaPackage.ELEMENT_LIST__IF_PATH:
				getIfPath().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ELSIF_PATH:
				getElsifPath().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ELSE_PATH:
				getElsePath().clear();
				return;
			case AdaPackage.ELEMENT_LIST__CASE_PATH:
				getCasePath().clear();
				return;
			case AdaPackage.ELEMENT_LIST__SELECT_PATH:
				getSelectPath().clear();
				return;
			case AdaPackage.ELEMENT_LIST__OR_PATH:
				getOrPath().clear();
				return;
			case AdaPackage.ELEMENT_LIST__THEN_ABORT_PATH:
				getThenAbortPath().clear();
				return;
			case AdaPackage.ELEMENT_LIST__CASE_EXPRESSION_PATH:
				getCaseExpressionPath().clear();
				return;
			case AdaPackage.ELEMENT_LIST__IF_EXPRESSION_PATH:
				getIfExpressionPath().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ELSIF_EXPRESSION_PATH:
				getElsifExpressionPath().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ELSE_EXPRESSION_PATH:
				getElseExpressionPath().clear();
				return;
			case AdaPackage.ELEMENT_LIST__USE_PACKAGE_CLAUSE:
				getUsePackageClause().clear();
				return;
			case AdaPackage.ELEMENT_LIST__USE_TYPE_CLAUSE:
				getUseTypeClause().clear();
				return;
			case AdaPackage.ELEMENT_LIST__USE_ALL_TYPE_CLAUSE:
				getUseAllTypeClause().clear();
				return;
			case AdaPackage.ELEMENT_LIST__WITH_CLAUSE:
				getWithClause().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ATTRIBUTE_DEFINITION_CLAUSE:
				getAttributeDefinitionClause().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ENUMERATION_REPRESENTATION_CLAUSE:
				getEnumerationRepresentationClause().clear();
				return;
			case AdaPackage.ELEMENT_LIST__RECORD_REPRESENTATION_CLAUSE:
				getRecordRepresentationClause().clear();
				return;
			case AdaPackage.ELEMENT_LIST__AT_CLAUSE:
				getAtClause().clear();
				return;
			case AdaPackage.ELEMENT_LIST__COMPONENT_CLAUSE:
				getComponentClause().clear();
				return;
			case AdaPackage.ELEMENT_LIST__EXCEPTION_HANDLER:
				getExceptionHandler().clear();
				return;
			case AdaPackage.ELEMENT_LIST__COMMENT:
				getComment().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ALL_CALLS_REMOTE_PRAGMA:
				getAllCallsRemotePragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ASYNCHRONOUS_PRAGMA:
				getAsynchronousPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ATOMIC_PRAGMA:
				getAtomicPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ATOMIC_COMPONENTS_PRAGMA:
				getAtomicComponentsPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ATTACH_HANDLER_PRAGMA:
				getAttachHandlerPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__CONTROLLED_PRAGMA:
				getControlledPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__CONVENTION_PRAGMA:
				getConventionPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DISCARD_NAMES_PRAGMA:
				getDiscardNamesPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ELABORATE_PRAGMA:
				getElaboratePragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ELABORATE_ALL_PRAGMA:
				getElaborateAllPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ELABORATE_BODY_PRAGMA:
				getElaborateBodyPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__EXPORT_PRAGMA:
				getExportPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__IMPORT_PRAGMA:
				getImportPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__INLINE_PRAGMA:
				getInlinePragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__INSPECTION_POINT_PRAGMA:
				getInspectionPointPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__INTERRUPT_HANDLER_PRAGMA:
				getInterruptHandlerPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__INTERRUPT_PRIORITY_PRAGMA:
				getInterruptPriorityPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__LINKER_OPTIONS_PRAGMA:
				getLinkerOptionsPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__LIST_PRAGMA:
				getListPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__LOCKING_POLICY_PRAGMA:
				getLockingPolicyPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__NORMALIZE_SCALARS_PRAGMA:
				getNormalizeScalarsPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__OPTIMIZE_PRAGMA:
				getOptimizePragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PACK_PRAGMA:
				getPackPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PAGE_PRAGMA:
				getPagePragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PREELABORATE_PRAGMA:
				getPreelaboratePragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PRIORITY_PRAGMA:
				getPriorityPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PURE_PRAGMA:
				getPurePragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__QUEUING_POLICY_PRAGMA:
				getQueuingPolicyPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				getRemoteCallInterfacePragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__REMOTE_TYPES_PRAGMA:
				getRemoteTypesPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__RESTRICTIONS_PRAGMA:
				getRestrictionsPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__REVIEWABLE_PRAGMA:
				getReviewablePragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__SHARED_PASSIVE_PRAGMA:
				getSharedPassivePragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__STORAGE_SIZE_PRAGMA:
				getStorageSizePragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__SUPPRESS_PRAGMA:
				getSuppressPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				getTaskDispatchingPolicyPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__VOLATILE_PRAGMA:
				getVolatilePragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__VOLATILE_COMPONENTS_PRAGMA:
				getVolatileComponentsPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ASSERT_PRAGMA:
				getAssertPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__ASSERTION_POLICY_PRAGMA:
				getAssertionPolicyPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DETECT_BLOCKING_PRAGMA:
				getDetectBlockingPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__NO_RETURN_PRAGMA:
				getNoReturnPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				getPartitionElaborationPolicyPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				getPreelaborableInitializationPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				getPrioritySpecificDispatchingPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__PROFILE_PRAGMA:
				getProfilePragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__RELATIVE_DEADLINE_PRAGMA:
				getRelativeDeadlinePragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__UNCHECKED_UNION_PRAGMA:
				getUncheckedUnionPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__UNSUPPRESS_PRAGMA:
				getUnsuppressPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				getDefaultStoragePoolPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__DISPATCHING_DOMAIN_PRAGMA:
				getDispatchingDomainPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__CPU_PRAGMA:
				getCpuPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__INDEPENDENT_PRAGMA:
				getIndependentPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				getIndependentComponentsPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				getImplementationDefinedPragma().clear();
				return;
			case AdaPackage.ELEMENT_LIST__UNKNOWN_PRAGMA:
				getUnknownPragma().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.ELEMENT_LIST__GROUP:
				return group != null && !group.isEmpty();
			case AdaPackage.ELEMENT_LIST__NOT_AN_ELEMENT:
				return !getNotAnElement().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINING_IDENTIFIER:
				return !getDefiningIdentifier().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINING_CHARACTER_LITERAL:
				return !getDefiningCharacterLiteral().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINING_ENUMERATION_LITERAL:
				return !getDefiningEnumerationLiteral().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINING_AND_OPERATOR:
				return !getDefiningAndOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINING_OR_OPERATOR:
				return !getDefiningOrOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINING_XOR_OPERATOR:
				return !getDefiningXorOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINING_EQUAL_OPERATOR:
				return !getDefiningEqualOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINING_NOT_EQUAL_OPERATOR:
				return !getDefiningNotEqualOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINING_LESS_THAN_OPERATOR:
				return !getDefiningLessThanOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINING_LESS_THAN_OR_EQUAL_OPERATOR:
				return !getDefiningLessThanOrEqualOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINING_GREATER_THAN_OPERATOR:
				return !getDefiningGreaterThanOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINING_GREATER_THAN_OR_EQUAL_OPERATOR:
				return !getDefiningGreaterThanOrEqualOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINING_PLUS_OPERATOR:
				return !getDefiningPlusOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINING_MINUS_OPERATOR:
				return !getDefiningMinusOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINING_CONCATENATE_OPERATOR:
				return !getDefiningConcatenateOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINING_UNARY_PLUS_OPERATOR:
				return !getDefiningUnaryPlusOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINING_UNARY_MINUS_OPERATOR:
				return !getDefiningUnaryMinusOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINING_MULTIPLY_OPERATOR:
				return !getDefiningMultiplyOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINING_DIVIDE_OPERATOR:
				return !getDefiningDivideOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINING_MOD_OPERATOR:
				return !getDefiningModOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINING_REM_OPERATOR:
				return !getDefiningRemOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINING_EXPONENTIATE_OPERATOR:
				return !getDefiningExponentiateOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINING_ABS_OPERATOR:
				return !getDefiningAbsOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINING_NOT_OPERATOR:
				return !getDefiningNotOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINING_EXPANDED_NAME:
				return !getDefiningExpandedName().isEmpty();
			case AdaPackage.ELEMENT_LIST__ORDINARY_TYPE_DECLARATION:
				return !getOrdinaryTypeDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__TASK_TYPE_DECLARATION:
				return !getTaskTypeDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__PROTECTED_TYPE_DECLARATION:
				return !getProtectedTypeDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__INCOMPLETE_TYPE_DECLARATION:
				return !getIncompleteTypeDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__TAGGED_INCOMPLETE_TYPE_DECLARATION:
				return !getTaggedIncompleteTypeDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__PRIVATE_TYPE_DECLARATION:
				return !getPrivateTypeDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__PRIVATE_EXTENSION_DECLARATION:
				return !getPrivateExtensionDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__SUBTYPE_DECLARATION:
				return !getSubtypeDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__VARIABLE_DECLARATION:
				return !getVariableDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__CONSTANT_DECLARATION:
				return !getConstantDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFERRED_CONSTANT_DECLARATION:
				return !getDeferredConstantDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__SINGLE_TASK_DECLARATION:
				return !getSingleTaskDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__SINGLE_PROTECTED_DECLARATION:
				return !getSingleProtectedDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__INTEGER_NUMBER_DECLARATION:
				return !getIntegerNumberDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__REAL_NUMBER_DECLARATION:
				return !getRealNumberDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__ENUMERATION_LITERAL_SPECIFICATION:
				return !getEnumerationLiteralSpecification().isEmpty();
			case AdaPackage.ELEMENT_LIST__DISCRIMINANT_SPECIFICATION:
				return !getDiscriminantSpecification().isEmpty();
			case AdaPackage.ELEMENT_LIST__COMPONENT_DECLARATION:
				return !getComponentDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__LOOP_PARAMETER_SPECIFICATION:
				return !getLoopParameterSpecification().isEmpty();
			case AdaPackage.ELEMENT_LIST__GENERALIZED_ITERATOR_SPECIFICATION:
				return !getGeneralizedIteratorSpecification().isEmpty();
			case AdaPackage.ELEMENT_LIST__ELEMENT_ITERATOR_SPECIFICATION:
				return !getElementIteratorSpecification().isEmpty();
			case AdaPackage.ELEMENT_LIST__PROCEDURE_DECLARATION:
				return !getProcedureDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__FUNCTION_DECLARATION:
				return !getFunctionDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__PARAMETER_SPECIFICATION:
				return !getParameterSpecification().isEmpty();
			case AdaPackage.ELEMENT_LIST__PROCEDURE_BODY_DECLARATION:
				return !getProcedureBodyDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__FUNCTION_BODY_DECLARATION:
				return !getFunctionBodyDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__RETURN_VARIABLE_SPECIFICATION:
				return !getReturnVariableSpecification().isEmpty();
			case AdaPackage.ELEMENT_LIST__RETURN_CONSTANT_SPECIFICATION:
				return !getReturnConstantSpecification().isEmpty();
			case AdaPackage.ELEMENT_LIST__NULL_PROCEDURE_DECLARATION:
				return !getNullProcedureDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__EXPRESSION_FUNCTION_DECLARATION:
				return !getExpressionFunctionDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__PACKAGE_DECLARATION:
				return !getPackageDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__PACKAGE_BODY_DECLARATION:
				return !getPackageBodyDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__OBJECT_RENAMING_DECLARATION:
				return !getObjectRenamingDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__EXCEPTION_RENAMING_DECLARATION:
				return !getExceptionRenamingDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__PACKAGE_RENAMING_DECLARATION:
				return !getPackageRenamingDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__PROCEDURE_RENAMING_DECLARATION:
				return !getProcedureRenamingDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__FUNCTION_RENAMING_DECLARATION:
				return !getFunctionRenamingDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__GENERIC_PACKAGE_RENAMING_DECLARATION:
				return !getGenericPackageRenamingDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__GENERIC_PROCEDURE_RENAMING_DECLARATION:
				return !getGenericProcedureRenamingDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__GENERIC_FUNCTION_RENAMING_DECLARATION:
				return !getGenericFunctionRenamingDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__TASK_BODY_DECLARATION:
				return !getTaskBodyDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__PROTECTED_BODY_DECLARATION:
				return !getProtectedBodyDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__ENTRY_DECLARATION:
				return !getEntryDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__ENTRY_BODY_DECLARATION:
				return !getEntryBodyDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__ENTRY_INDEX_SPECIFICATION:
				return !getEntryIndexSpecification().isEmpty();
			case AdaPackage.ELEMENT_LIST__PROCEDURE_BODY_STUB:
				return !getProcedureBodyStub().isEmpty();
			case AdaPackage.ELEMENT_LIST__FUNCTION_BODY_STUB:
				return !getFunctionBodyStub().isEmpty();
			case AdaPackage.ELEMENT_LIST__PACKAGE_BODY_STUB:
				return !getPackageBodyStub().isEmpty();
			case AdaPackage.ELEMENT_LIST__TASK_BODY_STUB:
				return !getTaskBodyStub().isEmpty();
			case AdaPackage.ELEMENT_LIST__PROTECTED_BODY_STUB:
				return !getProtectedBodyStub().isEmpty();
			case AdaPackage.ELEMENT_LIST__EXCEPTION_DECLARATION:
				return !getExceptionDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__CHOICE_PARAMETER_SPECIFICATION:
				return !getChoiceParameterSpecification().isEmpty();
			case AdaPackage.ELEMENT_LIST__GENERIC_PROCEDURE_DECLARATION:
				return !getGenericProcedureDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__GENERIC_FUNCTION_DECLARATION:
				return !getGenericFunctionDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__GENERIC_PACKAGE_DECLARATION:
				return !getGenericPackageDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__PACKAGE_INSTANTIATION:
				return !getPackageInstantiation().isEmpty();
			case AdaPackage.ELEMENT_LIST__PROCEDURE_INSTANTIATION:
				return !getProcedureInstantiation().isEmpty();
			case AdaPackage.ELEMENT_LIST__FUNCTION_INSTANTIATION:
				return !getFunctionInstantiation().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_OBJECT_DECLARATION:
				return !getFormalObjectDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_TYPE_DECLARATION:
				return !getFormalTypeDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_INCOMPLETE_TYPE_DECLARATION:
				return !getFormalIncompleteTypeDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_PROCEDURE_DECLARATION:
				return !getFormalProcedureDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_FUNCTION_DECLARATION:
				return !getFormalFunctionDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_PACKAGE_DECLARATION:
				return !getFormalPackageDeclaration().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_PACKAGE_DECLARATION_WITH_BOX:
				return !getFormalPackageDeclarationWithBox().isEmpty();
			case AdaPackage.ELEMENT_LIST__DERIVED_TYPE_DEFINITION:
				return !getDerivedTypeDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__DERIVED_RECORD_EXTENSION_DEFINITION:
				return !getDerivedRecordExtensionDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__ENUMERATION_TYPE_DEFINITION:
				return !getEnumerationTypeDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__SIGNED_INTEGER_TYPE_DEFINITION:
				return !getSignedIntegerTypeDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__MODULAR_TYPE_DEFINITION:
				return !getModularTypeDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__ROOT_INTEGER_DEFINITION:
				return !getRootIntegerDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__ROOT_REAL_DEFINITION:
				return !getRootRealDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__UNIVERSAL_INTEGER_DEFINITION:
				return !getUniversalIntegerDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__UNIVERSAL_REAL_DEFINITION:
				return !getUniversalRealDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__UNIVERSAL_FIXED_DEFINITION:
				return !getUniversalFixedDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__FLOATING_POINT_DEFINITION:
				return !getFloatingPointDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__ORDINARY_FIXED_POINT_DEFINITION:
				return !getOrdinaryFixedPointDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__DECIMAL_FIXED_POINT_DEFINITION:
				return !getDecimalFixedPointDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__UNCONSTRAINED_ARRAY_DEFINITION:
				return !getUnconstrainedArrayDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__CONSTRAINED_ARRAY_DEFINITION:
				return !getConstrainedArrayDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__RECORD_TYPE_DEFINITION:
				return !getRecordTypeDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__TAGGED_RECORD_TYPE_DEFINITION:
				return !getTaggedRecordTypeDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__ORDINARY_INTERFACE:
				return !getOrdinaryInterface().isEmpty();
			case AdaPackage.ELEMENT_LIST__LIMITED_INTERFACE:
				return !getLimitedInterface().isEmpty();
			case AdaPackage.ELEMENT_LIST__TASK_INTERFACE:
				return !getTaskInterface().isEmpty();
			case AdaPackage.ELEMENT_LIST__PROTECTED_INTERFACE:
				return !getProtectedInterface().isEmpty();
			case AdaPackage.ELEMENT_LIST__SYNCHRONIZED_INTERFACE:
				return !getSynchronizedInterface().isEmpty();
			case AdaPackage.ELEMENT_LIST__POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				return !getPoolSpecificAccessToVariable().isEmpty();
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_VARIABLE:
				return !getAccessToVariable().isEmpty();
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_CONSTANT:
				return !getAccessToConstant().isEmpty();
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_PROCEDURE:
				return !getAccessToProcedure().isEmpty();
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_PROTECTED_PROCEDURE:
				return !getAccessToProtectedProcedure().isEmpty();
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_FUNCTION:
				return !getAccessToFunction().isEmpty();
			case AdaPackage.ELEMENT_LIST__ACCESS_TO_PROTECTED_FUNCTION:
				return !getAccessToProtectedFunction().isEmpty();
			case AdaPackage.ELEMENT_LIST__SUBTYPE_INDICATION:
				return !getSubtypeIndication().isEmpty();
			case AdaPackage.ELEMENT_LIST__RANGE_ATTRIBUTE_REFERENCE:
				return !getRangeAttributeReference().isEmpty();
			case AdaPackage.ELEMENT_LIST__SIMPLE_EXPRESSION_RANGE:
				return !getSimpleExpressionRange().isEmpty();
			case AdaPackage.ELEMENT_LIST__DIGITS_CONSTRAINT:
				return !getDigitsConstraint().isEmpty();
			case AdaPackage.ELEMENT_LIST__DELTA_CONSTRAINT:
				return !getDeltaConstraint().isEmpty();
			case AdaPackage.ELEMENT_LIST__INDEX_CONSTRAINT:
				return !getIndexConstraint().isEmpty();
			case AdaPackage.ELEMENT_LIST__DISCRIMINANT_CONSTRAINT:
				return !getDiscriminantConstraint().isEmpty();
			case AdaPackage.ELEMENT_LIST__COMPONENT_DEFINITION:
				return !getComponentDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION:
				return !getDiscreteSubtypeIndicationAsSubtypeDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION:
				return !getDiscreteRangeAttributeReferenceAsSubtypeDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION:
				return !getDiscreteSimpleExpressionRangeAsSubtypeDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__DISCRETE_SUBTYPE_INDICATION:
				return !getDiscreteSubtypeIndication().isEmpty();
			case AdaPackage.ELEMENT_LIST__DISCRETE_RANGE_ATTRIBUTE_REFERENCE:
				return !getDiscreteRangeAttributeReference().isEmpty();
			case AdaPackage.ELEMENT_LIST__DISCRETE_SIMPLE_EXPRESSION_RANGE:
				return !getDiscreteSimpleExpressionRange().isEmpty();
			case AdaPackage.ELEMENT_LIST__UNKNOWN_DISCRIMINANT_PART:
				return !getUnknownDiscriminantPart().isEmpty();
			case AdaPackage.ELEMENT_LIST__KNOWN_DISCRIMINANT_PART:
				return !getKnownDiscriminantPart().isEmpty();
			case AdaPackage.ELEMENT_LIST__RECORD_DEFINITION:
				return !getRecordDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__NULL_RECORD_DEFINITION:
				return !getNullRecordDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__NULL_COMPONENT:
				return !getNullComponent().isEmpty();
			case AdaPackage.ELEMENT_LIST__VARIANT_PART:
				return !getVariantPart().isEmpty();
			case AdaPackage.ELEMENT_LIST__VARIANT:
				return !getVariant().isEmpty();
			case AdaPackage.ELEMENT_LIST__OTHERS_CHOICE:
				return !getOthersChoice().isEmpty();
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_VARIABLE:
				return !getAnonymousAccessToVariable().isEmpty();
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_CONSTANT:
				return !getAnonymousAccessToConstant().isEmpty();
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_PROCEDURE:
				return !getAnonymousAccessToProcedure().isEmpty();
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE:
				return !getAnonymousAccessToProtectedProcedure().isEmpty();
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_FUNCTION:
				return !getAnonymousAccessToFunction().isEmpty();
			case AdaPackage.ELEMENT_LIST__ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION:
				return !getAnonymousAccessToProtectedFunction().isEmpty();
			case AdaPackage.ELEMENT_LIST__PRIVATE_TYPE_DEFINITION:
				return !getPrivateTypeDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__TAGGED_PRIVATE_TYPE_DEFINITION:
				return !getTaggedPrivateTypeDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__PRIVATE_EXTENSION_DEFINITION:
				return !getPrivateExtensionDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__TASK_DEFINITION:
				return !getTaskDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__PROTECTED_DEFINITION:
				return !getProtectedDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_PRIVATE_TYPE_DEFINITION:
				return !getFormalPrivateTypeDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION:
				return !getFormalTaggedPrivateTypeDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_DERIVED_TYPE_DEFINITION:
				return !getFormalDerivedTypeDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_DISCRETE_TYPE_DEFINITION:
				return !getFormalDiscreteTypeDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_SIGNED_INTEGER_TYPE_DEFINITION:
				return !getFormalSignedIntegerTypeDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_MODULAR_TYPE_DEFINITION:
				return !getFormalModularTypeDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_FLOATING_POINT_DEFINITION:
				return !getFormalFloatingPointDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_ORDINARY_FIXED_POINT_DEFINITION:
				return !getFormalOrdinaryFixedPointDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_DECIMAL_FIXED_POINT_DEFINITION:
				return !getFormalDecimalFixedPointDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_ORDINARY_INTERFACE:
				return !getFormalOrdinaryInterface().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_LIMITED_INTERFACE:
				return !getFormalLimitedInterface().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_TASK_INTERFACE:
				return !getFormalTaskInterface().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_PROTECTED_INTERFACE:
				return !getFormalProtectedInterface().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_SYNCHRONIZED_INTERFACE:
				return !getFormalSynchronizedInterface().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_UNCONSTRAINED_ARRAY_DEFINITION:
				return !getFormalUnconstrainedArrayDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_CONSTRAINED_ARRAY_DEFINITION:
				return !getFormalConstrainedArrayDefinition().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				return !getFormalPoolSpecificAccessToVariable().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_VARIABLE:
				return !getFormalAccessToVariable().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_CONSTANT:
				return !getFormalAccessToConstant().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_PROCEDURE:
				return !getFormalAccessToProcedure().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_PROTECTED_PROCEDURE:
				return !getFormalAccessToProtectedProcedure().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_FUNCTION:
				return !getFormalAccessToFunction().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORMAL_ACCESS_TO_PROTECTED_FUNCTION:
				return !getFormalAccessToProtectedFunction().isEmpty();
			case AdaPackage.ELEMENT_LIST__ASPECT_SPECIFICATION:
				return !getAspectSpecification().isEmpty();
			case AdaPackage.ELEMENT_LIST__BOX_EXPRESSION:
				return !getBoxExpression().isEmpty();
			case AdaPackage.ELEMENT_LIST__INTEGER_LITERAL:
				return !getIntegerLiteral().isEmpty();
			case AdaPackage.ELEMENT_LIST__REAL_LITERAL:
				return !getRealLiteral().isEmpty();
			case AdaPackage.ELEMENT_LIST__STRING_LITERAL:
				return !getStringLiteral().isEmpty();
			case AdaPackage.ELEMENT_LIST__IDENTIFIER:
				return !getIdentifier().isEmpty();
			case AdaPackage.ELEMENT_LIST__AND_OPERATOR:
				return !getAndOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__OR_OPERATOR:
				return !getOrOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__XOR_OPERATOR:
				return !getXorOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__EQUAL_OPERATOR:
				return !getEqualOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__NOT_EQUAL_OPERATOR:
				return !getNotEqualOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__LESS_THAN_OPERATOR:
				return !getLessThanOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__LESS_THAN_OR_EQUAL_OPERATOR:
				return !getLessThanOrEqualOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__GREATER_THAN_OPERATOR:
				return !getGreaterThanOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__GREATER_THAN_OR_EQUAL_OPERATOR:
				return !getGreaterThanOrEqualOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__PLUS_OPERATOR:
				return !getPlusOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__MINUS_OPERATOR:
				return !getMinusOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__CONCATENATE_OPERATOR:
				return !getConcatenateOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__UNARY_PLUS_OPERATOR:
				return !getUnaryPlusOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__UNARY_MINUS_OPERATOR:
				return !getUnaryMinusOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__MULTIPLY_OPERATOR:
				return !getMultiplyOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__DIVIDE_OPERATOR:
				return !getDivideOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__MOD_OPERATOR:
				return !getModOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__REM_OPERATOR:
				return !getRemOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__EXPONENTIATE_OPERATOR:
				return !getExponentiateOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__ABS_OPERATOR:
				return !getAbsOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__NOT_OPERATOR:
				return !getNotOperator().isEmpty();
			case AdaPackage.ELEMENT_LIST__CHARACTER_LITERAL:
				return !getCharacterLiteral().isEmpty();
			case AdaPackage.ELEMENT_LIST__ENUMERATION_LITERAL:
				return !getEnumerationLiteral().isEmpty();
			case AdaPackage.ELEMENT_LIST__EXPLICIT_DEREFERENCE:
				return !getExplicitDereference().isEmpty();
			case AdaPackage.ELEMENT_LIST__FUNCTION_CALL:
				return !getFunctionCall().isEmpty();
			case AdaPackage.ELEMENT_LIST__INDEXED_COMPONENT:
				return !getIndexedComponent().isEmpty();
			case AdaPackage.ELEMENT_LIST__SLICE:
				return !getSlice().isEmpty();
			case AdaPackage.ELEMENT_LIST__SELECTED_COMPONENT:
				return !getSelectedComponent().isEmpty();
			case AdaPackage.ELEMENT_LIST__ACCESS_ATTRIBUTE:
				return !getAccessAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__ADDRESS_ATTRIBUTE:
				return !getAddressAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__ADJACENT_ATTRIBUTE:
				return !getAdjacentAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__AFT_ATTRIBUTE:
				return !getAftAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__ALIGNMENT_ATTRIBUTE:
				return !getAlignmentAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__BASE_ATTRIBUTE:
				return !getBaseAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__BIT_ORDER_ATTRIBUTE:
				return !getBitOrderAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__BODY_VERSION_ATTRIBUTE:
				return !getBodyVersionAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__CALLABLE_ATTRIBUTE:
				return !getCallableAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__CALLER_ATTRIBUTE:
				return !getCallerAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__CEILING_ATTRIBUTE:
				return !getCeilingAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__CLASS_ATTRIBUTE:
				return !getClassAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__COMPONENT_SIZE_ATTRIBUTE:
				return !getComponentSizeAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__COMPOSE_ATTRIBUTE:
				return !getComposeAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__CONSTRAINED_ATTRIBUTE:
				return !getConstrainedAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__COPY_SIGN_ATTRIBUTE:
				return !getCopySignAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__COUNT_ATTRIBUTE:
				return !getCountAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFINITE_ATTRIBUTE:
				return !getDefiniteAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__DELTA_ATTRIBUTE:
				return !getDeltaAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__DENORM_ATTRIBUTE:
				return !getDenormAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__DIGITS_ATTRIBUTE:
				return !getDigitsAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__EXPONENT_ATTRIBUTE:
				return !getExponentAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__EXTERNAL_TAG_ATTRIBUTE:
				return !getExternalTagAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__FIRST_ATTRIBUTE:
				return !getFirstAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__FIRST_BIT_ATTRIBUTE:
				return !getFirstBitAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__FLOOR_ATTRIBUTE:
				return !getFloorAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__FORE_ATTRIBUTE:
				return !getForeAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__FRACTION_ATTRIBUTE:
				return !getFractionAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__IDENTITY_ATTRIBUTE:
				return !getIdentityAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__IMAGE_ATTRIBUTE:
				return !getImageAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__INPUT_ATTRIBUTE:
				return !getInputAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__LAST_ATTRIBUTE:
				return !getLastAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__LAST_BIT_ATTRIBUTE:
				return !getLastBitAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__LEADING_PART_ATTRIBUTE:
				return !getLeadingPartAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__LENGTH_ATTRIBUTE:
				return !getLengthAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__MACHINE_ATTRIBUTE:
				return !getMachineAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__MACHINE_EMAX_ATTRIBUTE:
				return !getMachineEmaxAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__MACHINE_EMIN_ATTRIBUTE:
				return !getMachineEminAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__MACHINE_MANTISSA_ATTRIBUTE:
				return !getMachineMantissaAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__MACHINE_OVERFLOWS_ATTRIBUTE:
				return !getMachineOverflowsAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__MACHINE_RADIX_ATTRIBUTE:
				return !getMachineRadixAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__MACHINE_ROUNDS_ATTRIBUTE:
				return !getMachineRoundsAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__MAX_ATTRIBUTE:
				return !getMaxAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE:
				return !getMaxSizeInStorageElementsAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__MIN_ATTRIBUTE:
				return !getMinAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__MODEL_ATTRIBUTE:
				return !getModelAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__MODEL_EMIN_ATTRIBUTE:
				return !getModelEminAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__MODEL_EPSILON_ATTRIBUTE:
				return !getModelEpsilonAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__MODEL_MANTISSA_ATTRIBUTE:
				return !getModelMantissaAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__MODEL_SMALL_ATTRIBUTE:
				return !getModelSmallAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__MODULUS_ATTRIBUTE:
				return !getModulusAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__OUTPUT_ATTRIBUTE:
				return !getOutputAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__PARTITION_ID_ATTRIBUTE:
				return !getPartitionIdAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__POS_ATTRIBUTE:
				return !getPosAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__POSITION_ATTRIBUTE:
				return !getPositionAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__PRED_ATTRIBUTE:
				return !getPredAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__RANGE_ATTRIBUTE:
				return !getRangeAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__READ_ATTRIBUTE:
				return !getReadAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__REMAINDER_ATTRIBUTE:
				return !getRemainderAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__ROUND_ATTRIBUTE:
				return !getRoundAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__ROUNDING_ATTRIBUTE:
				return !getRoundingAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__SAFE_FIRST_ATTRIBUTE:
				return !getSafeFirstAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__SAFE_LAST_ATTRIBUTE:
				return !getSafeLastAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__SCALE_ATTRIBUTE:
				return !getScaleAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__SCALING_ATTRIBUTE:
				return !getScalingAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__SIGNED_ZEROS_ATTRIBUTE:
				return !getSignedZerosAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__SIZE_ATTRIBUTE:
				return !getSizeAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__SMALL_ATTRIBUTE:
				return !getSmallAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__STORAGE_POOL_ATTRIBUTE:
				return !getStoragePoolAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__STORAGE_SIZE_ATTRIBUTE:
				return !getStorageSizeAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__SUCC_ATTRIBUTE:
				return !getSuccAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__TAG_ATTRIBUTE:
				return !getTagAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__TERMINATED_ATTRIBUTE:
				return !getTerminatedAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__TRUNCATION_ATTRIBUTE:
				return !getTruncationAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__UNBIASED_ROUNDING_ATTRIBUTE:
				return !getUnbiasedRoundingAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__UNCHECKED_ACCESS_ATTRIBUTE:
				return !getUncheckedAccessAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__VAL_ATTRIBUTE:
				return !getValAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__VALID_ATTRIBUTE:
				return !getValidAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__VALUE_ATTRIBUTE:
				return !getValueAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__VERSION_ATTRIBUTE:
				return !getVersionAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__WIDE_IMAGE_ATTRIBUTE:
				return !getWideImageAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__WIDE_VALUE_ATTRIBUTE:
				return !getWideValueAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__WIDE_WIDTH_ATTRIBUTE:
				return !getWideWidthAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__WIDTH_ATTRIBUTE:
				return !getWidthAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__WRITE_ATTRIBUTE:
				return !getWriteAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__MACHINE_ROUNDING_ATTRIBUTE:
				return !getMachineRoundingAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__MOD_ATTRIBUTE:
				return !getModAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__PRIORITY_ATTRIBUTE:
				return !getPriorityAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__STREAM_SIZE_ATTRIBUTE:
				return !getStreamSizeAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__WIDE_WIDE_IMAGE_ATTRIBUTE:
				return !getWideWideImageAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__WIDE_WIDE_VALUE_ATTRIBUTE:
				return !getWideWideValueAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__WIDE_WIDE_WIDTH_ATTRIBUTE:
				return !getWideWideWidthAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE:
				return !getMaxAlignmentForAllocationAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__OVERLAPS_STORAGE_ATTRIBUTE:
				return !getOverlapsStorageAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__IMPLEMENTATION_DEFINED_ATTRIBUTE:
				return !getImplementationDefinedAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__UNKNOWN_ATTRIBUTE:
				return !getUnknownAttribute().isEmpty();
			case AdaPackage.ELEMENT_LIST__RECORD_AGGREGATE:
				return !getRecordAggregate().isEmpty();
			case AdaPackage.ELEMENT_LIST__EXTENSION_AGGREGATE:
				return !getExtensionAggregate().isEmpty();
			case AdaPackage.ELEMENT_LIST__POSITIONAL_ARRAY_AGGREGATE:
				return !getPositionalArrayAggregate().isEmpty();
			case AdaPackage.ELEMENT_LIST__NAMED_ARRAY_AGGREGATE:
				return !getNamedArrayAggregate().isEmpty();
			case AdaPackage.ELEMENT_LIST__AND_THEN_SHORT_CIRCUIT:
				return !getAndThenShortCircuit().isEmpty();
			case AdaPackage.ELEMENT_LIST__OR_ELSE_SHORT_CIRCUIT:
				return !getOrElseShortCircuit().isEmpty();
			case AdaPackage.ELEMENT_LIST__IN_MEMBERSHIP_TEST:
				return !getInMembershipTest().isEmpty();
			case AdaPackage.ELEMENT_LIST__NOT_IN_MEMBERSHIP_TEST:
				return !getNotInMembershipTest().isEmpty();
			case AdaPackage.ELEMENT_LIST__NULL_LITERAL:
				return !getNullLiteral().isEmpty();
			case AdaPackage.ELEMENT_LIST__PARENTHESIZED_EXPRESSION:
				return !getParenthesizedExpression().isEmpty();
			case AdaPackage.ELEMENT_LIST__RAISE_EXPRESSION:
				return !getRaiseExpression().isEmpty();
			case AdaPackage.ELEMENT_LIST__TYPE_CONVERSION:
				return !getTypeConversion().isEmpty();
			case AdaPackage.ELEMENT_LIST__QUALIFIED_EXPRESSION:
				return !getQualifiedExpression().isEmpty();
			case AdaPackage.ELEMENT_LIST__ALLOCATION_FROM_SUBTYPE:
				return !getAllocationFromSubtype().isEmpty();
			case AdaPackage.ELEMENT_LIST__ALLOCATION_FROM_QUALIFIED_EXPRESSION:
				return !getAllocationFromQualifiedExpression().isEmpty();
			case AdaPackage.ELEMENT_LIST__CASE_EXPRESSION:
				return !getCaseExpression().isEmpty();
			case AdaPackage.ELEMENT_LIST__IF_EXPRESSION:
				return !getIfExpression().isEmpty();
			case AdaPackage.ELEMENT_LIST__FOR_ALL_QUANTIFIED_EXPRESSION:
				return !getForAllQuantifiedExpression().isEmpty();
			case AdaPackage.ELEMENT_LIST__FOR_SOME_QUANTIFIED_EXPRESSION:
				return !getForSomeQuantifiedExpression().isEmpty();
			case AdaPackage.ELEMENT_LIST__PRAGMA_ARGUMENT_ASSOCIATION:
				return !getPragmaArgumentAssociation().isEmpty();
			case AdaPackage.ELEMENT_LIST__DISCRIMINANT_ASSOCIATION:
				return !getDiscriminantAssociation().isEmpty();
			case AdaPackage.ELEMENT_LIST__RECORD_COMPONENT_ASSOCIATION:
				return !getRecordComponentAssociation().isEmpty();
			case AdaPackage.ELEMENT_LIST__ARRAY_COMPONENT_ASSOCIATION:
				return !getArrayComponentAssociation().isEmpty();
			case AdaPackage.ELEMENT_LIST__PARAMETER_ASSOCIATION:
				return !getParameterAssociation().isEmpty();
			case AdaPackage.ELEMENT_LIST__GENERIC_ASSOCIATION:
				return !getGenericAssociation().isEmpty();
			case AdaPackage.ELEMENT_LIST__NULL_STATEMENT:
				return !getNullStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__ASSIGNMENT_STATEMENT:
				return !getAssignmentStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__IF_STATEMENT:
				return !getIfStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__CASE_STATEMENT:
				return !getCaseStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__LOOP_STATEMENT:
				return !getLoopStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__WHILE_LOOP_STATEMENT:
				return !getWhileLoopStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__FOR_LOOP_STATEMENT:
				return !getForLoopStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__BLOCK_STATEMENT:
				return !getBlockStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__EXIT_STATEMENT:
				return !getExitStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__GOTO_STATEMENT:
				return !getGotoStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__PROCEDURE_CALL_STATEMENT:
				return !getProcedureCallStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__RETURN_STATEMENT:
				return !getReturnStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__EXTENDED_RETURN_STATEMENT:
				return !getExtendedReturnStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__ACCEPT_STATEMENT:
				return !getAcceptStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__ENTRY_CALL_STATEMENT:
				return !getEntryCallStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__REQUEUE_STATEMENT:
				return !getRequeueStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__REQUEUE_STATEMENT_WITH_ABORT:
				return !getRequeueStatementWithAbort().isEmpty();
			case AdaPackage.ELEMENT_LIST__DELAY_UNTIL_STATEMENT:
				return !getDelayUntilStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__DELAY_RELATIVE_STATEMENT:
				return !getDelayRelativeStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__TERMINATE_ALTERNATIVE_STATEMENT:
				return !getTerminateAlternativeStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__SELECTIVE_ACCEPT_STATEMENT:
				return !getSelectiveAcceptStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__TIMED_ENTRY_CALL_STATEMENT:
				return !getTimedEntryCallStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__CONDITIONAL_ENTRY_CALL_STATEMENT:
				return !getConditionalEntryCallStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__ASYNCHRONOUS_SELECT_STATEMENT:
				return !getAsynchronousSelectStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__ABORT_STATEMENT:
				return !getAbortStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__RAISE_STATEMENT:
				return !getRaiseStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__CODE_STATEMENT:
				return !getCodeStatement().isEmpty();
			case AdaPackage.ELEMENT_LIST__IF_PATH:
				return !getIfPath().isEmpty();
			case AdaPackage.ELEMENT_LIST__ELSIF_PATH:
				return !getElsifPath().isEmpty();
			case AdaPackage.ELEMENT_LIST__ELSE_PATH:
				return !getElsePath().isEmpty();
			case AdaPackage.ELEMENT_LIST__CASE_PATH:
				return !getCasePath().isEmpty();
			case AdaPackage.ELEMENT_LIST__SELECT_PATH:
				return !getSelectPath().isEmpty();
			case AdaPackage.ELEMENT_LIST__OR_PATH:
				return !getOrPath().isEmpty();
			case AdaPackage.ELEMENT_LIST__THEN_ABORT_PATH:
				return !getThenAbortPath().isEmpty();
			case AdaPackage.ELEMENT_LIST__CASE_EXPRESSION_PATH:
				return !getCaseExpressionPath().isEmpty();
			case AdaPackage.ELEMENT_LIST__IF_EXPRESSION_PATH:
				return !getIfExpressionPath().isEmpty();
			case AdaPackage.ELEMENT_LIST__ELSIF_EXPRESSION_PATH:
				return !getElsifExpressionPath().isEmpty();
			case AdaPackage.ELEMENT_LIST__ELSE_EXPRESSION_PATH:
				return !getElseExpressionPath().isEmpty();
			case AdaPackage.ELEMENT_LIST__USE_PACKAGE_CLAUSE:
				return !getUsePackageClause().isEmpty();
			case AdaPackage.ELEMENT_LIST__USE_TYPE_CLAUSE:
				return !getUseTypeClause().isEmpty();
			case AdaPackage.ELEMENT_LIST__USE_ALL_TYPE_CLAUSE:
				return !getUseAllTypeClause().isEmpty();
			case AdaPackage.ELEMENT_LIST__WITH_CLAUSE:
				return !getWithClause().isEmpty();
			case AdaPackage.ELEMENT_LIST__ATTRIBUTE_DEFINITION_CLAUSE:
				return !getAttributeDefinitionClause().isEmpty();
			case AdaPackage.ELEMENT_LIST__ENUMERATION_REPRESENTATION_CLAUSE:
				return !getEnumerationRepresentationClause().isEmpty();
			case AdaPackage.ELEMENT_LIST__RECORD_REPRESENTATION_CLAUSE:
				return !getRecordRepresentationClause().isEmpty();
			case AdaPackage.ELEMENT_LIST__AT_CLAUSE:
				return !getAtClause().isEmpty();
			case AdaPackage.ELEMENT_LIST__COMPONENT_CLAUSE:
				return !getComponentClause().isEmpty();
			case AdaPackage.ELEMENT_LIST__EXCEPTION_HANDLER:
				return !getExceptionHandler().isEmpty();
			case AdaPackage.ELEMENT_LIST__COMMENT:
				return !getComment().isEmpty();
			case AdaPackage.ELEMENT_LIST__ALL_CALLS_REMOTE_PRAGMA:
				return !getAllCallsRemotePragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__ASYNCHRONOUS_PRAGMA:
				return !getAsynchronousPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__ATOMIC_PRAGMA:
				return !getAtomicPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__ATOMIC_COMPONENTS_PRAGMA:
				return !getAtomicComponentsPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__ATTACH_HANDLER_PRAGMA:
				return !getAttachHandlerPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__CONTROLLED_PRAGMA:
				return !getControlledPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__CONVENTION_PRAGMA:
				return !getConventionPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__DISCARD_NAMES_PRAGMA:
				return !getDiscardNamesPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__ELABORATE_PRAGMA:
				return !getElaboratePragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__ELABORATE_ALL_PRAGMA:
				return !getElaborateAllPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__ELABORATE_BODY_PRAGMA:
				return !getElaborateBodyPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__EXPORT_PRAGMA:
				return !getExportPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__IMPORT_PRAGMA:
				return !getImportPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__INLINE_PRAGMA:
				return !getInlinePragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__INSPECTION_POINT_PRAGMA:
				return !getInspectionPointPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__INTERRUPT_HANDLER_PRAGMA:
				return !getInterruptHandlerPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__INTERRUPT_PRIORITY_PRAGMA:
				return !getInterruptPriorityPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__LINKER_OPTIONS_PRAGMA:
				return !getLinkerOptionsPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__LIST_PRAGMA:
				return !getListPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__LOCKING_POLICY_PRAGMA:
				return !getLockingPolicyPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__NORMALIZE_SCALARS_PRAGMA:
				return !getNormalizeScalarsPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__OPTIMIZE_PRAGMA:
				return !getOptimizePragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__PACK_PRAGMA:
				return !getPackPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__PAGE_PRAGMA:
				return !getPagePragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__PREELABORATE_PRAGMA:
				return !getPreelaboratePragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__PRIORITY_PRAGMA:
				return !getPriorityPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__PURE_PRAGMA:
				return !getPurePragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__QUEUING_POLICY_PRAGMA:
				return !getQueuingPolicyPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				return !getRemoteCallInterfacePragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__REMOTE_TYPES_PRAGMA:
				return !getRemoteTypesPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__RESTRICTIONS_PRAGMA:
				return !getRestrictionsPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__REVIEWABLE_PRAGMA:
				return !getReviewablePragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__SHARED_PASSIVE_PRAGMA:
				return !getSharedPassivePragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__STORAGE_SIZE_PRAGMA:
				return !getStorageSizePragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__SUPPRESS_PRAGMA:
				return !getSuppressPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				return !getTaskDispatchingPolicyPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__VOLATILE_PRAGMA:
				return !getVolatilePragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__VOLATILE_COMPONENTS_PRAGMA:
				return !getVolatileComponentsPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__ASSERT_PRAGMA:
				return !getAssertPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__ASSERTION_POLICY_PRAGMA:
				return !getAssertionPolicyPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__DETECT_BLOCKING_PRAGMA:
				return !getDetectBlockingPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__NO_RETURN_PRAGMA:
				return !getNoReturnPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				return !getPartitionElaborationPolicyPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				return !getPreelaborableInitializationPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return !getPrioritySpecificDispatchingPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__PROFILE_PRAGMA:
				return !getProfilePragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__RELATIVE_DEADLINE_PRAGMA:
				return !getRelativeDeadlinePragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__UNCHECKED_UNION_PRAGMA:
				return !getUncheckedUnionPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__UNSUPPRESS_PRAGMA:
				return !getUnsuppressPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				return !getDefaultStoragePoolPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__DISPATCHING_DOMAIN_PRAGMA:
				return !getDispatchingDomainPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__CPU_PRAGMA:
				return !getCpuPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__INDEPENDENT_PRAGMA:
				return !getIndependentPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				return !getIndependentComponentsPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				return !getImplementationDefinedPragma().isEmpty();
			case AdaPackage.ELEMENT_LIST__UNKNOWN_PRAGMA:
				return !getUnknownPragma().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(')');
		return result.toString();
	}

} //ElementListImpl
