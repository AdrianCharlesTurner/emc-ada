/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.DefinitionClass;
import Ada.DerivedRecordExtensionDefinition;
import Ada.ElementClass;
import Ada.ExpressionList;
import Ada.HasAbstractQType12;
import Ada.HasLimitedQType6;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Derived Record Extension Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.DerivedRecordExtensionDefinitionImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.DerivedRecordExtensionDefinitionImpl#getHasAbstractQ <em>Has Abstract Q</em>}</li>
 *   <li>{@link Ada.impl.DerivedRecordExtensionDefinitionImpl#getHasLimitedQ <em>Has Limited Q</em>}</li>
 *   <li>{@link Ada.impl.DerivedRecordExtensionDefinitionImpl#getParentSubtypeIndicationQ <em>Parent Subtype Indication Q</em>}</li>
 *   <li>{@link Ada.impl.DerivedRecordExtensionDefinitionImpl#getDefinitionInterfaceListQl <em>Definition Interface List Ql</em>}</li>
 *   <li>{@link Ada.impl.DerivedRecordExtensionDefinitionImpl#getRecordDefinitionQ <em>Record Definition Q</em>}</li>
 *   <li>{@link Ada.impl.DerivedRecordExtensionDefinitionImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DerivedRecordExtensionDefinitionImpl extends MinimalEObjectImpl.Container implements DerivedRecordExtensionDefinition {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getHasAbstractQ() <em>Has Abstract Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasAbstractQ()
	 * @generated
	 * @ordered
	 */
	protected HasAbstractQType12 hasAbstractQ;

	/**
	 * The cached value of the '{@link #getHasLimitedQ() <em>Has Limited Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasLimitedQ()
	 * @generated
	 * @ordered
	 */
	protected HasLimitedQType6 hasLimitedQ;

	/**
	 * The cached value of the '{@link #getParentSubtypeIndicationQ() <em>Parent Subtype Indication Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParentSubtypeIndicationQ()
	 * @generated
	 * @ordered
	 */
	protected ElementClass parentSubtypeIndicationQ;

	/**
	 * The cached value of the '{@link #getDefinitionInterfaceListQl() <em>Definition Interface List Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefinitionInterfaceListQl()
	 * @generated
	 * @ordered
	 */
	protected ExpressionList definitionInterfaceListQl;

	/**
	 * The cached value of the '{@link #getRecordDefinitionQ() <em>Record Definition Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRecordDefinitionQ()
	 * @generated
	 * @ordered
	 */
	protected DefinitionClass recordDefinitionQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DerivedRecordExtensionDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getDerivedRecordExtensionDefinition();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAbstractQType12 getHasAbstractQ() {
		return hasAbstractQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasAbstractQ(HasAbstractQType12 newHasAbstractQ, NotificationChain msgs) {
		HasAbstractQType12 oldHasAbstractQ = hasAbstractQ;
		hasAbstractQ = newHasAbstractQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__HAS_ABSTRACT_Q, oldHasAbstractQ, newHasAbstractQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasAbstractQ(HasAbstractQType12 newHasAbstractQ) {
		if (newHasAbstractQ != hasAbstractQ) {
			NotificationChain msgs = null;
			if (hasAbstractQ != null)
				msgs = ((InternalEObject)hasAbstractQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__HAS_ABSTRACT_Q, null, msgs);
			if (newHasAbstractQ != null)
				msgs = ((InternalEObject)newHasAbstractQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__HAS_ABSTRACT_Q, null, msgs);
			msgs = basicSetHasAbstractQ(newHasAbstractQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__HAS_ABSTRACT_Q, newHasAbstractQ, newHasAbstractQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasLimitedQType6 getHasLimitedQ() {
		return hasLimitedQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasLimitedQ(HasLimitedQType6 newHasLimitedQ, NotificationChain msgs) {
		HasLimitedQType6 oldHasLimitedQ = hasLimitedQ;
		hasLimitedQ = newHasLimitedQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__HAS_LIMITED_Q, oldHasLimitedQ, newHasLimitedQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasLimitedQ(HasLimitedQType6 newHasLimitedQ) {
		if (newHasLimitedQ != hasLimitedQ) {
			NotificationChain msgs = null;
			if (hasLimitedQ != null)
				msgs = ((InternalEObject)hasLimitedQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__HAS_LIMITED_Q, null, msgs);
			if (newHasLimitedQ != null)
				msgs = ((InternalEObject)newHasLimitedQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__HAS_LIMITED_Q, null, msgs);
			msgs = basicSetHasLimitedQ(newHasLimitedQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__HAS_LIMITED_Q, newHasLimitedQ, newHasLimitedQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementClass getParentSubtypeIndicationQ() {
		return parentSubtypeIndicationQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParentSubtypeIndicationQ(ElementClass newParentSubtypeIndicationQ, NotificationChain msgs) {
		ElementClass oldParentSubtypeIndicationQ = parentSubtypeIndicationQ;
		parentSubtypeIndicationQ = newParentSubtypeIndicationQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__PARENT_SUBTYPE_INDICATION_Q, oldParentSubtypeIndicationQ, newParentSubtypeIndicationQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentSubtypeIndicationQ(ElementClass newParentSubtypeIndicationQ) {
		if (newParentSubtypeIndicationQ != parentSubtypeIndicationQ) {
			NotificationChain msgs = null;
			if (parentSubtypeIndicationQ != null)
				msgs = ((InternalEObject)parentSubtypeIndicationQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__PARENT_SUBTYPE_INDICATION_Q, null, msgs);
			if (newParentSubtypeIndicationQ != null)
				msgs = ((InternalEObject)newParentSubtypeIndicationQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__PARENT_SUBTYPE_INDICATION_Q, null, msgs);
			msgs = basicSetParentSubtypeIndicationQ(newParentSubtypeIndicationQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__PARENT_SUBTYPE_INDICATION_Q, newParentSubtypeIndicationQ, newParentSubtypeIndicationQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionList getDefinitionInterfaceListQl() {
		return definitionInterfaceListQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefinitionInterfaceListQl(ExpressionList newDefinitionInterfaceListQl, NotificationChain msgs) {
		ExpressionList oldDefinitionInterfaceListQl = definitionInterfaceListQl;
		definitionInterfaceListQl = newDefinitionInterfaceListQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__DEFINITION_INTERFACE_LIST_QL, oldDefinitionInterfaceListQl, newDefinitionInterfaceListQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefinitionInterfaceListQl(ExpressionList newDefinitionInterfaceListQl) {
		if (newDefinitionInterfaceListQl != definitionInterfaceListQl) {
			NotificationChain msgs = null;
			if (definitionInterfaceListQl != null)
				msgs = ((InternalEObject)definitionInterfaceListQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__DEFINITION_INTERFACE_LIST_QL, null, msgs);
			if (newDefinitionInterfaceListQl != null)
				msgs = ((InternalEObject)newDefinitionInterfaceListQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__DEFINITION_INTERFACE_LIST_QL, null, msgs);
			msgs = basicSetDefinitionInterfaceListQl(newDefinitionInterfaceListQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__DEFINITION_INTERFACE_LIST_QL, newDefinitionInterfaceListQl, newDefinitionInterfaceListQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefinitionClass getRecordDefinitionQ() {
		return recordDefinitionQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRecordDefinitionQ(DefinitionClass newRecordDefinitionQ, NotificationChain msgs) {
		DefinitionClass oldRecordDefinitionQ = recordDefinitionQ;
		recordDefinitionQ = newRecordDefinitionQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__RECORD_DEFINITION_Q, oldRecordDefinitionQ, newRecordDefinitionQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRecordDefinitionQ(DefinitionClass newRecordDefinitionQ) {
		if (newRecordDefinitionQ != recordDefinitionQ) {
			NotificationChain msgs = null;
			if (recordDefinitionQ != null)
				msgs = ((InternalEObject)recordDefinitionQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__RECORD_DEFINITION_Q, null, msgs);
			if (newRecordDefinitionQ != null)
				msgs = ((InternalEObject)newRecordDefinitionQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__RECORD_DEFINITION_Q, null, msgs);
			msgs = basicSetRecordDefinitionQ(newRecordDefinitionQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__RECORD_DEFINITION_Q, newRecordDefinitionQ, newRecordDefinitionQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__HAS_ABSTRACT_Q:
				return basicSetHasAbstractQ(null, msgs);
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__HAS_LIMITED_Q:
				return basicSetHasLimitedQ(null, msgs);
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__PARENT_SUBTYPE_INDICATION_Q:
				return basicSetParentSubtypeIndicationQ(null, msgs);
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__DEFINITION_INTERFACE_LIST_QL:
				return basicSetDefinitionInterfaceListQl(null, msgs);
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__RECORD_DEFINITION_Q:
				return basicSetRecordDefinitionQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__SLOC:
				return getSloc();
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__HAS_ABSTRACT_Q:
				return getHasAbstractQ();
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__HAS_LIMITED_Q:
				return getHasLimitedQ();
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__PARENT_SUBTYPE_INDICATION_Q:
				return getParentSubtypeIndicationQ();
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__DEFINITION_INTERFACE_LIST_QL:
				return getDefinitionInterfaceListQl();
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__RECORD_DEFINITION_Q:
				return getRecordDefinitionQ();
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__HAS_ABSTRACT_Q:
				setHasAbstractQ((HasAbstractQType12)newValue);
				return;
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__HAS_LIMITED_Q:
				setHasLimitedQ((HasLimitedQType6)newValue);
				return;
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__PARENT_SUBTYPE_INDICATION_Q:
				setParentSubtypeIndicationQ((ElementClass)newValue);
				return;
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__DEFINITION_INTERFACE_LIST_QL:
				setDefinitionInterfaceListQl((ExpressionList)newValue);
				return;
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__RECORD_DEFINITION_Q:
				setRecordDefinitionQ((DefinitionClass)newValue);
				return;
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__HAS_ABSTRACT_Q:
				setHasAbstractQ((HasAbstractQType12)null);
				return;
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__HAS_LIMITED_Q:
				setHasLimitedQ((HasLimitedQType6)null);
				return;
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__PARENT_SUBTYPE_INDICATION_Q:
				setParentSubtypeIndicationQ((ElementClass)null);
				return;
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__DEFINITION_INTERFACE_LIST_QL:
				setDefinitionInterfaceListQl((ExpressionList)null);
				return;
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__RECORD_DEFINITION_Q:
				setRecordDefinitionQ((DefinitionClass)null);
				return;
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__SLOC:
				return sloc != null;
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__HAS_ABSTRACT_Q:
				return hasAbstractQ != null;
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__HAS_LIMITED_Q:
				return hasLimitedQ != null;
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__PARENT_SUBTYPE_INDICATION_Q:
				return parentSubtypeIndicationQ != null;
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__DEFINITION_INTERFACE_LIST_QL:
				return definitionInterfaceListQl != null;
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__RECORD_DEFINITION_Q:
				return recordDefinitionQ != null;
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //DerivedRecordExtensionDefinitionImpl
