/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Entry Body Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.EntryBodyDeclaration#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.EntryBodyDeclaration#getNamesQl <em>Names Ql</em>}</li>
 *   <li>{@link Ada.EntryBodyDeclaration#getEntryIndexSpecificationQ <em>Entry Index Specification Q</em>}</li>
 *   <li>{@link Ada.EntryBodyDeclaration#getParameterProfileQl <em>Parameter Profile Ql</em>}</li>
 *   <li>{@link Ada.EntryBodyDeclaration#getEntryBarrierQ <em>Entry Barrier Q</em>}</li>
 *   <li>{@link Ada.EntryBodyDeclaration#getBodyDeclarativeItemsQl <em>Body Declarative Items Ql</em>}</li>
 *   <li>{@link Ada.EntryBodyDeclaration#getBodyStatementsQl <em>Body Statements Ql</em>}</li>
 *   <li>{@link Ada.EntryBodyDeclaration#getBodyExceptionHandlersQl <em>Body Exception Handlers Ql</em>}</li>
 *   <li>{@link Ada.EntryBodyDeclaration#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getEntryBodyDeclaration()
 * @model extendedMetaData="name='Entry_Body_Declaration' kind='elementOnly'"
 * @generated
 */
public interface EntryBodyDeclaration extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getEntryBodyDeclaration_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.EntryBodyDeclaration#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Names Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Names Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Names Ql</em>' containment reference.
	 * @see #setNamesQl(DefiningNameList)
	 * @see Ada.AdaPackage#getEntryBodyDeclaration_NamesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='names_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameList getNamesQl();

	/**
	 * Sets the value of the '{@link Ada.EntryBodyDeclaration#getNamesQl <em>Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Names Ql</em>' containment reference.
	 * @see #getNamesQl()
	 * @generated
	 */
	void setNamesQl(DefiningNameList value);

	/**
	 * Returns the value of the '<em><b>Entry Index Specification Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry Index Specification Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry Index Specification Q</em>' containment reference.
	 * @see #setEntryIndexSpecificationQ(DeclarationClass)
	 * @see Ada.AdaPackage#getEntryBodyDeclaration_EntryIndexSpecificationQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='entry_index_specification_q' namespace='##targetNamespace'"
	 * @generated
	 */
	DeclarationClass getEntryIndexSpecificationQ();

	/**
	 * Sets the value of the '{@link Ada.EntryBodyDeclaration#getEntryIndexSpecificationQ <em>Entry Index Specification Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entry Index Specification Q</em>' containment reference.
	 * @see #getEntryIndexSpecificationQ()
	 * @generated
	 */
	void setEntryIndexSpecificationQ(DeclarationClass value);

	/**
	 * Returns the value of the '<em><b>Parameter Profile Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Profile Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Profile Ql</em>' containment reference.
	 * @see #setParameterProfileQl(ParameterSpecificationList)
	 * @see Ada.AdaPackage#getEntryBodyDeclaration_ParameterProfileQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='parameter_profile_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ParameterSpecificationList getParameterProfileQl();

	/**
	 * Sets the value of the '{@link Ada.EntryBodyDeclaration#getParameterProfileQl <em>Parameter Profile Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Profile Ql</em>' containment reference.
	 * @see #getParameterProfileQl()
	 * @generated
	 */
	void setParameterProfileQl(ParameterSpecificationList value);

	/**
	 * Returns the value of the '<em><b>Entry Barrier Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry Barrier Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry Barrier Q</em>' containment reference.
	 * @see #setEntryBarrierQ(ExpressionClass)
	 * @see Ada.AdaPackage#getEntryBodyDeclaration_EntryBarrierQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='entry_barrier_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getEntryBarrierQ();

	/**
	 * Sets the value of the '{@link Ada.EntryBodyDeclaration#getEntryBarrierQ <em>Entry Barrier Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entry Barrier Q</em>' containment reference.
	 * @see #getEntryBarrierQ()
	 * @generated
	 */
	void setEntryBarrierQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Body Declarative Items Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body Declarative Items Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body Declarative Items Ql</em>' containment reference.
	 * @see #setBodyDeclarativeItemsQl(ElementList)
	 * @see Ada.AdaPackage#getEntryBodyDeclaration_BodyDeclarativeItemsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='body_declarative_items_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementList getBodyDeclarativeItemsQl();

	/**
	 * Sets the value of the '{@link Ada.EntryBodyDeclaration#getBodyDeclarativeItemsQl <em>Body Declarative Items Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body Declarative Items Ql</em>' containment reference.
	 * @see #getBodyDeclarativeItemsQl()
	 * @generated
	 */
	void setBodyDeclarativeItemsQl(ElementList value);

	/**
	 * Returns the value of the '<em><b>Body Statements Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body Statements Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body Statements Ql</em>' containment reference.
	 * @see #setBodyStatementsQl(StatementList)
	 * @see Ada.AdaPackage#getEntryBodyDeclaration_BodyStatementsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='body_statements_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	StatementList getBodyStatementsQl();

	/**
	 * Sets the value of the '{@link Ada.EntryBodyDeclaration#getBodyStatementsQl <em>Body Statements Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body Statements Ql</em>' containment reference.
	 * @see #getBodyStatementsQl()
	 * @generated
	 */
	void setBodyStatementsQl(StatementList value);

	/**
	 * Returns the value of the '<em><b>Body Exception Handlers Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body Exception Handlers Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body Exception Handlers Ql</em>' containment reference.
	 * @see #setBodyExceptionHandlersQl(ExceptionHandlerList)
	 * @see Ada.AdaPackage#getEntryBodyDeclaration_BodyExceptionHandlersQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='body_exception_handlers_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ExceptionHandlerList getBodyExceptionHandlersQl();

	/**
	 * Sets the value of the '{@link Ada.EntryBodyDeclaration#getBodyExceptionHandlersQl <em>Body Exception Handlers Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body Exception Handlers Ql</em>' containment reference.
	 * @see #getBodyExceptionHandlersQl()
	 * @generated
	 */
	void setBodyExceptionHandlersQl(ExceptionHandlerList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getEntryBodyDeclaration_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.EntryBodyDeclaration#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // EntryBodyDeclaration
