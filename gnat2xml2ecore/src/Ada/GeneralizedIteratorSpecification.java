/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generalized Iterator Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.GeneralizedIteratorSpecification#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.GeneralizedIteratorSpecification#getNamesQl <em>Names Ql</em>}</li>
 *   <li>{@link Ada.GeneralizedIteratorSpecification#getHasReverseQ <em>Has Reverse Q</em>}</li>
 *   <li>{@link Ada.GeneralizedIteratorSpecification#getIterationSchemeNameQ <em>Iteration Scheme Name Q</em>}</li>
 *   <li>{@link Ada.GeneralizedIteratorSpecification#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getGeneralizedIteratorSpecification()
 * @model extendedMetaData="name='Generalized_Iterator_Specification' kind='elementOnly'"
 * @generated
 */
public interface GeneralizedIteratorSpecification extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getGeneralizedIteratorSpecification_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.GeneralizedIteratorSpecification#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Names Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Names Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Names Ql</em>' containment reference.
	 * @see #setNamesQl(DefiningNameList)
	 * @see Ada.AdaPackage#getGeneralizedIteratorSpecification_NamesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='names_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameList getNamesQl();

	/**
	 * Sets the value of the '{@link Ada.GeneralizedIteratorSpecification#getNamesQl <em>Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Names Ql</em>' containment reference.
	 * @see #getNamesQl()
	 * @generated
	 */
	void setNamesQl(DefiningNameList value);

	/**
	 * Returns the value of the '<em><b>Has Reverse Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Reverse Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Reverse Q</em>' containment reference.
	 * @see #setHasReverseQ(HasReverseQType1)
	 * @see Ada.AdaPackage#getGeneralizedIteratorSpecification_HasReverseQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='has_reverse_q' namespace='##targetNamespace'"
	 * @generated
	 */
	HasReverseQType1 getHasReverseQ();

	/**
	 * Sets the value of the '{@link Ada.GeneralizedIteratorSpecification#getHasReverseQ <em>Has Reverse Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Reverse Q</em>' containment reference.
	 * @see #getHasReverseQ()
	 * @generated
	 */
	void setHasReverseQ(HasReverseQType1 value);

	/**
	 * Returns the value of the '<em><b>Iteration Scheme Name Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iteration Scheme Name Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iteration Scheme Name Q</em>' containment reference.
	 * @see #setIterationSchemeNameQ(ElementClass)
	 * @see Ada.AdaPackage#getGeneralizedIteratorSpecification_IterationSchemeNameQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='iteration_scheme_name_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementClass getIterationSchemeNameQ();

	/**
	 * Sets the value of the '{@link Ada.GeneralizedIteratorSpecification#getIterationSchemeNameQ <em>Iteration Scheme Name Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iteration Scheme Name Q</em>' containment reference.
	 * @see #getIterationSchemeNameQ()
	 * @generated
	 */
	void setIterationSchemeNameQ(ElementClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getGeneralizedIteratorSpecification_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.GeneralizedIteratorSpecification#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // GeneralizedIteratorSpecification
