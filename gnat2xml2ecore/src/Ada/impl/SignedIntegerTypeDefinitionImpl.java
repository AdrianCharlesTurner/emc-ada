/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.RangeConstraintClass;
import Ada.SignedIntegerTypeDefinition;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Signed Integer Type Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.SignedIntegerTypeDefinitionImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.SignedIntegerTypeDefinitionImpl#getIntegerConstraintQ <em>Integer Constraint Q</em>}</li>
 *   <li>{@link Ada.impl.SignedIntegerTypeDefinitionImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SignedIntegerTypeDefinitionImpl extends MinimalEObjectImpl.Container implements SignedIntegerTypeDefinition {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getIntegerConstraintQ() <em>Integer Constraint Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegerConstraintQ()
	 * @generated
	 * @ordered
	 */
	protected RangeConstraintClass integerConstraintQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SignedIntegerTypeDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getSignedIntegerTypeDefinition();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.SIGNED_INTEGER_TYPE_DEFINITION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.SIGNED_INTEGER_TYPE_DEFINITION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.SIGNED_INTEGER_TYPE_DEFINITION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.SIGNED_INTEGER_TYPE_DEFINITION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RangeConstraintClass getIntegerConstraintQ() {
		return integerConstraintQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIntegerConstraintQ(RangeConstraintClass newIntegerConstraintQ, NotificationChain msgs) {
		RangeConstraintClass oldIntegerConstraintQ = integerConstraintQ;
		integerConstraintQ = newIntegerConstraintQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.SIGNED_INTEGER_TYPE_DEFINITION__INTEGER_CONSTRAINT_Q, oldIntegerConstraintQ, newIntegerConstraintQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntegerConstraintQ(RangeConstraintClass newIntegerConstraintQ) {
		if (newIntegerConstraintQ != integerConstraintQ) {
			NotificationChain msgs = null;
			if (integerConstraintQ != null)
				msgs = ((InternalEObject)integerConstraintQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.SIGNED_INTEGER_TYPE_DEFINITION__INTEGER_CONSTRAINT_Q, null, msgs);
			if (newIntegerConstraintQ != null)
				msgs = ((InternalEObject)newIntegerConstraintQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.SIGNED_INTEGER_TYPE_DEFINITION__INTEGER_CONSTRAINT_Q, null, msgs);
			msgs = basicSetIntegerConstraintQ(newIntegerConstraintQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.SIGNED_INTEGER_TYPE_DEFINITION__INTEGER_CONSTRAINT_Q, newIntegerConstraintQ, newIntegerConstraintQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.SIGNED_INTEGER_TYPE_DEFINITION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.SIGNED_INTEGER_TYPE_DEFINITION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.SIGNED_INTEGER_TYPE_DEFINITION__INTEGER_CONSTRAINT_Q:
				return basicSetIntegerConstraintQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.SIGNED_INTEGER_TYPE_DEFINITION__SLOC:
				return getSloc();
			case AdaPackage.SIGNED_INTEGER_TYPE_DEFINITION__INTEGER_CONSTRAINT_Q:
				return getIntegerConstraintQ();
			case AdaPackage.SIGNED_INTEGER_TYPE_DEFINITION__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.SIGNED_INTEGER_TYPE_DEFINITION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.SIGNED_INTEGER_TYPE_DEFINITION__INTEGER_CONSTRAINT_Q:
				setIntegerConstraintQ((RangeConstraintClass)newValue);
				return;
			case AdaPackage.SIGNED_INTEGER_TYPE_DEFINITION__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.SIGNED_INTEGER_TYPE_DEFINITION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.SIGNED_INTEGER_TYPE_DEFINITION__INTEGER_CONSTRAINT_Q:
				setIntegerConstraintQ((RangeConstraintClass)null);
				return;
			case AdaPackage.SIGNED_INTEGER_TYPE_DEFINITION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.SIGNED_INTEGER_TYPE_DEFINITION__SLOC:
				return sloc != null;
			case AdaPackage.SIGNED_INTEGER_TYPE_DEFINITION__INTEGER_CONSTRAINT_Q:
				return integerConstraintQ != null;
			case AdaPackage.SIGNED_INTEGER_TYPE_DEFINITION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //SignedIntegerTypeDefinitionImpl
