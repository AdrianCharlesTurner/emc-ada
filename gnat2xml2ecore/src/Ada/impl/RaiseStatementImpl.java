/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.DefiningNameList;
import Ada.ExpressionClass;
import Ada.RaiseStatement;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Raise Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.RaiseStatementImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.RaiseStatementImpl#getLabelNamesQl <em>Label Names Ql</em>}</li>
 *   <li>{@link Ada.impl.RaiseStatementImpl#getRaisedExceptionQ <em>Raised Exception Q</em>}</li>
 *   <li>{@link Ada.impl.RaiseStatementImpl#getAssociatedMessageQ <em>Associated Message Q</em>}</li>
 *   <li>{@link Ada.impl.RaiseStatementImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RaiseStatementImpl extends MinimalEObjectImpl.Container implements RaiseStatement {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getLabelNamesQl() <em>Label Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabelNamesQl()
	 * @generated
	 * @ordered
	 */
	protected DefiningNameList labelNamesQl;

	/**
	 * The cached value of the '{@link #getRaisedExceptionQ() <em>Raised Exception Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRaisedExceptionQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass raisedExceptionQ;

	/**
	 * The cached value of the '{@link #getAssociatedMessageQ() <em>Associated Message Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociatedMessageQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass associatedMessageQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RaiseStatementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getRaiseStatement();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.RAISE_STATEMENT__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.RAISE_STATEMENT__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.RAISE_STATEMENT__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.RAISE_STATEMENT__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNameList getLabelNamesQl() {
		return labelNamesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLabelNamesQl(DefiningNameList newLabelNamesQl, NotificationChain msgs) {
		DefiningNameList oldLabelNamesQl = labelNamesQl;
		labelNamesQl = newLabelNamesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.RAISE_STATEMENT__LABEL_NAMES_QL, oldLabelNamesQl, newLabelNamesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabelNamesQl(DefiningNameList newLabelNamesQl) {
		if (newLabelNamesQl != labelNamesQl) {
			NotificationChain msgs = null;
			if (labelNamesQl != null)
				msgs = ((InternalEObject)labelNamesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.RAISE_STATEMENT__LABEL_NAMES_QL, null, msgs);
			if (newLabelNamesQl != null)
				msgs = ((InternalEObject)newLabelNamesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.RAISE_STATEMENT__LABEL_NAMES_QL, null, msgs);
			msgs = basicSetLabelNamesQl(newLabelNamesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.RAISE_STATEMENT__LABEL_NAMES_QL, newLabelNamesQl, newLabelNamesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getRaisedExceptionQ() {
		return raisedExceptionQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRaisedExceptionQ(ExpressionClass newRaisedExceptionQ, NotificationChain msgs) {
		ExpressionClass oldRaisedExceptionQ = raisedExceptionQ;
		raisedExceptionQ = newRaisedExceptionQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.RAISE_STATEMENT__RAISED_EXCEPTION_Q, oldRaisedExceptionQ, newRaisedExceptionQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRaisedExceptionQ(ExpressionClass newRaisedExceptionQ) {
		if (newRaisedExceptionQ != raisedExceptionQ) {
			NotificationChain msgs = null;
			if (raisedExceptionQ != null)
				msgs = ((InternalEObject)raisedExceptionQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.RAISE_STATEMENT__RAISED_EXCEPTION_Q, null, msgs);
			if (newRaisedExceptionQ != null)
				msgs = ((InternalEObject)newRaisedExceptionQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.RAISE_STATEMENT__RAISED_EXCEPTION_Q, null, msgs);
			msgs = basicSetRaisedExceptionQ(newRaisedExceptionQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.RAISE_STATEMENT__RAISED_EXCEPTION_Q, newRaisedExceptionQ, newRaisedExceptionQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getAssociatedMessageQ() {
		return associatedMessageQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssociatedMessageQ(ExpressionClass newAssociatedMessageQ, NotificationChain msgs) {
		ExpressionClass oldAssociatedMessageQ = associatedMessageQ;
		associatedMessageQ = newAssociatedMessageQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.RAISE_STATEMENT__ASSOCIATED_MESSAGE_Q, oldAssociatedMessageQ, newAssociatedMessageQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssociatedMessageQ(ExpressionClass newAssociatedMessageQ) {
		if (newAssociatedMessageQ != associatedMessageQ) {
			NotificationChain msgs = null;
			if (associatedMessageQ != null)
				msgs = ((InternalEObject)associatedMessageQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.RAISE_STATEMENT__ASSOCIATED_MESSAGE_Q, null, msgs);
			if (newAssociatedMessageQ != null)
				msgs = ((InternalEObject)newAssociatedMessageQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.RAISE_STATEMENT__ASSOCIATED_MESSAGE_Q, null, msgs);
			msgs = basicSetAssociatedMessageQ(newAssociatedMessageQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.RAISE_STATEMENT__ASSOCIATED_MESSAGE_Q, newAssociatedMessageQ, newAssociatedMessageQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.RAISE_STATEMENT__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.RAISE_STATEMENT__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.RAISE_STATEMENT__LABEL_NAMES_QL:
				return basicSetLabelNamesQl(null, msgs);
			case AdaPackage.RAISE_STATEMENT__RAISED_EXCEPTION_Q:
				return basicSetRaisedExceptionQ(null, msgs);
			case AdaPackage.RAISE_STATEMENT__ASSOCIATED_MESSAGE_Q:
				return basicSetAssociatedMessageQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.RAISE_STATEMENT__SLOC:
				return getSloc();
			case AdaPackage.RAISE_STATEMENT__LABEL_NAMES_QL:
				return getLabelNamesQl();
			case AdaPackage.RAISE_STATEMENT__RAISED_EXCEPTION_Q:
				return getRaisedExceptionQ();
			case AdaPackage.RAISE_STATEMENT__ASSOCIATED_MESSAGE_Q:
				return getAssociatedMessageQ();
			case AdaPackage.RAISE_STATEMENT__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.RAISE_STATEMENT__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.RAISE_STATEMENT__LABEL_NAMES_QL:
				setLabelNamesQl((DefiningNameList)newValue);
				return;
			case AdaPackage.RAISE_STATEMENT__RAISED_EXCEPTION_Q:
				setRaisedExceptionQ((ExpressionClass)newValue);
				return;
			case AdaPackage.RAISE_STATEMENT__ASSOCIATED_MESSAGE_Q:
				setAssociatedMessageQ((ExpressionClass)newValue);
				return;
			case AdaPackage.RAISE_STATEMENT__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.RAISE_STATEMENT__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.RAISE_STATEMENT__LABEL_NAMES_QL:
				setLabelNamesQl((DefiningNameList)null);
				return;
			case AdaPackage.RAISE_STATEMENT__RAISED_EXCEPTION_Q:
				setRaisedExceptionQ((ExpressionClass)null);
				return;
			case AdaPackage.RAISE_STATEMENT__ASSOCIATED_MESSAGE_Q:
				setAssociatedMessageQ((ExpressionClass)null);
				return;
			case AdaPackage.RAISE_STATEMENT__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.RAISE_STATEMENT__SLOC:
				return sloc != null;
			case AdaPackage.RAISE_STATEMENT__LABEL_NAMES_QL:
				return labelNamesQl != null;
			case AdaPackage.RAISE_STATEMENT__RAISED_EXCEPTION_Q:
				return raisedExceptionQ != null;
			case AdaPackage.RAISE_STATEMENT__ASSOCIATED_MESSAGE_Q:
				return associatedMessageQ != null;
			case AdaPackage.RAISE_STATEMENT__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //RaiseStatementImpl
