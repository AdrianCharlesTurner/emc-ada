/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variant</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.Variant#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.Variant#getVariantChoicesQl <em>Variant Choices Ql</em>}</li>
 *   <li>{@link Ada.Variant#getRecordComponentsQl <em>Record Components Ql</em>}</li>
 *   <li>{@link Ada.Variant#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getVariant()
 * @model extendedMetaData="name='Variant' kind='elementOnly'"
 * @generated
 */
public interface Variant extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getVariant_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.Variant#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Variant Choices Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variant Choices Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variant Choices Ql</em>' containment reference.
	 * @see #setVariantChoicesQl(ElementList)
	 * @see Ada.AdaPackage#getVariant_VariantChoicesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='variant_choices_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementList getVariantChoicesQl();

	/**
	 * Sets the value of the '{@link Ada.Variant#getVariantChoicesQl <em>Variant Choices Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variant Choices Ql</em>' containment reference.
	 * @see #getVariantChoicesQl()
	 * @generated
	 */
	void setVariantChoicesQl(ElementList value);

	/**
	 * Returns the value of the '<em><b>Record Components Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Components Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Components Ql</em>' containment reference.
	 * @see #setRecordComponentsQl(RecordComponentList)
	 * @see Ada.AdaPackage#getVariant_RecordComponentsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='record_components_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	RecordComponentList getRecordComponentsQl();

	/**
	 * Sets the value of the '{@link Ada.Variant#getRecordComponentsQl <em>Record Components Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Record Components Ql</em>' containment reference.
	 * @see #getRecordComponentsQl()
	 * @generated
	 */
	void setRecordComponentsQl(RecordComponentList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getVariant_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.Variant#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // Variant
