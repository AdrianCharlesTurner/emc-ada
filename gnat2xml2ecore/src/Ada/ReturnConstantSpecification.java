/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Return Constant Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.ReturnConstantSpecification#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.ReturnConstantSpecification#getNamesQl <em>Names Ql</em>}</li>
 *   <li>{@link Ada.ReturnConstantSpecification#getHasAliasedQ <em>Has Aliased Q</em>}</li>
 *   <li>{@link Ada.ReturnConstantSpecification#getObjectDeclarationViewQ <em>Object Declaration View Q</em>}</li>
 *   <li>{@link Ada.ReturnConstantSpecification#getInitializationExpressionQ <em>Initialization Expression Q</em>}</li>
 *   <li>{@link Ada.ReturnConstantSpecification#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getReturnConstantSpecification()
 * @model extendedMetaData="name='Return_Constant_Specification' kind='elementOnly'"
 * @generated
 */
public interface ReturnConstantSpecification extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getReturnConstantSpecification_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.ReturnConstantSpecification#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Names Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Names Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Names Ql</em>' containment reference.
	 * @see #setNamesQl(DefiningNameList)
	 * @see Ada.AdaPackage#getReturnConstantSpecification_NamesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='names_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameList getNamesQl();

	/**
	 * Sets the value of the '{@link Ada.ReturnConstantSpecification#getNamesQl <em>Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Names Ql</em>' containment reference.
	 * @see #getNamesQl()
	 * @generated
	 */
	void setNamesQl(DefiningNameList value);

	/**
	 * Returns the value of the '<em><b>Has Aliased Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Aliased Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Aliased Q</em>' containment reference.
	 * @see #setHasAliasedQ(HasAliasedQType1)
	 * @see Ada.AdaPackage#getReturnConstantSpecification_HasAliasedQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='has_aliased_q' namespace='##targetNamespace'"
	 * @generated
	 */
	HasAliasedQType1 getHasAliasedQ();

	/**
	 * Sets the value of the '{@link Ada.ReturnConstantSpecification#getHasAliasedQ <em>Has Aliased Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Aliased Q</em>' containment reference.
	 * @see #getHasAliasedQ()
	 * @generated
	 */
	void setHasAliasedQ(HasAliasedQType1 value);

	/**
	 * Returns the value of the '<em><b>Object Declaration View Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Declaration View Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Declaration View Q</em>' containment reference.
	 * @see #setObjectDeclarationViewQ(DefinitionClass)
	 * @see Ada.AdaPackage#getReturnConstantSpecification_ObjectDeclarationViewQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='object_declaration_view_q' namespace='##targetNamespace'"
	 * @generated
	 */
	DefinitionClass getObjectDeclarationViewQ();

	/**
	 * Sets the value of the '{@link Ada.ReturnConstantSpecification#getObjectDeclarationViewQ <em>Object Declaration View Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Declaration View Q</em>' containment reference.
	 * @see #getObjectDeclarationViewQ()
	 * @generated
	 */
	void setObjectDeclarationViewQ(DefinitionClass value);

	/**
	 * Returns the value of the '<em><b>Initialization Expression Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initialization Expression Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initialization Expression Q</em>' containment reference.
	 * @see #setInitializationExpressionQ(ExpressionClass)
	 * @see Ada.AdaPackage#getReturnConstantSpecification_InitializationExpressionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='initialization_expression_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getInitializationExpressionQ();

	/**
	 * Sets the value of the '{@link Ada.ReturnConstantSpecification#getInitializationExpressionQ <em>Initialization Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initialization Expression Q</em>' containment reference.
	 * @see #getInitializationExpressionQ()
	 * @generated
	 */
	void setInitializationExpressionQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getReturnConstantSpecification_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.ReturnConstantSpecification#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // ReturnConstantSpecification
