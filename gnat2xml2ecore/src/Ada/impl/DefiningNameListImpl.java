/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.AllCallsRemotePragma;
import Ada.AssertPragma;
import Ada.AssertionPolicyPragma;
import Ada.AsynchronousPragma;
import Ada.AtomicComponentsPragma;
import Ada.AtomicPragma;
import Ada.AttachHandlerPragma;
import Ada.Comment;
import Ada.ControlledPragma;
import Ada.ConventionPragma;
import Ada.CpuPragma;
import Ada.DefaultStoragePoolPragma;
import Ada.DefiningAbsOperator;
import Ada.DefiningAndOperator;
import Ada.DefiningCharacterLiteral;
import Ada.DefiningConcatenateOperator;
import Ada.DefiningDivideOperator;
import Ada.DefiningEnumerationLiteral;
import Ada.DefiningEqualOperator;
import Ada.DefiningExpandedName;
import Ada.DefiningExponentiateOperator;
import Ada.DefiningGreaterThanOperator;
import Ada.DefiningGreaterThanOrEqualOperator;
import Ada.DefiningIdentifier;
import Ada.DefiningLessThanOperator;
import Ada.DefiningLessThanOrEqualOperator;
import Ada.DefiningMinusOperator;
import Ada.DefiningModOperator;
import Ada.DefiningMultiplyOperator;
import Ada.DefiningNameList;
import Ada.DefiningNotEqualOperator;
import Ada.DefiningNotOperator;
import Ada.DefiningOrOperator;
import Ada.DefiningPlusOperator;
import Ada.DefiningRemOperator;
import Ada.DefiningUnaryMinusOperator;
import Ada.DefiningUnaryPlusOperator;
import Ada.DefiningXorOperator;
import Ada.DetectBlockingPragma;
import Ada.DiscardNamesPragma;
import Ada.DispatchingDomainPragma;
import Ada.ElaborateAllPragma;
import Ada.ElaborateBodyPragma;
import Ada.ElaboratePragma;
import Ada.ExportPragma;
import Ada.ImplementationDefinedPragma;
import Ada.ImportPragma;
import Ada.IndependentComponentsPragma;
import Ada.IndependentPragma;
import Ada.InlinePragma;
import Ada.InspectionPointPragma;
import Ada.InterruptHandlerPragma;
import Ada.InterruptPriorityPragma;
import Ada.LinkerOptionsPragma;
import Ada.ListPragma;
import Ada.LockingPolicyPragma;
import Ada.NoReturnPragma;
import Ada.NormalizeScalarsPragma;
import Ada.NotAnElement;
import Ada.OptimizePragma;
import Ada.PackPragma;
import Ada.PagePragma;
import Ada.PartitionElaborationPolicyPragma;
import Ada.PreelaborableInitializationPragma;
import Ada.PreelaboratePragma;
import Ada.PriorityPragma;
import Ada.PrioritySpecificDispatchingPragma;
import Ada.ProfilePragma;
import Ada.PurePragma;
import Ada.QueuingPolicyPragma;
import Ada.RelativeDeadlinePragma;
import Ada.RemoteCallInterfacePragma;
import Ada.RemoteTypesPragma;
import Ada.RestrictionsPragma;
import Ada.ReviewablePragma;
import Ada.SharedPassivePragma;
import Ada.StorageSizePragma;
import Ada.SuppressPragma;
import Ada.TaskDispatchingPolicyPragma;
import Ada.UncheckedUnionPragma;
import Ada.UnknownPragma;
import Ada.UnsuppressPragma;
import Ada.VolatileComponentsPragma;
import Ada.VolatilePragma;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Defining Name List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getNotAnElement <em>Not An Element</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefiningIdentifier <em>Defining Identifier</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefiningCharacterLiteral <em>Defining Character Literal</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefiningEnumerationLiteral <em>Defining Enumeration Literal</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefiningAndOperator <em>Defining And Operator</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefiningOrOperator <em>Defining Or Operator</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefiningXorOperator <em>Defining Xor Operator</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefiningEqualOperator <em>Defining Equal Operator</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefiningNotEqualOperator <em>Defining Not Equal Operator</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefiningLessThanOperator <em>Defining Less Than Operator</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefiningLessThanOrEqualOperator <em>Defining Less Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefiningGreaterThanOperator <em>Defining Greater Than Operator</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefiningGreaterThanOrEqualOperator <em>Defining Greater Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefiningPlusOperator <em>Defining Plus Operator</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefiningMinusOperator <em>Defining Minus Operator</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefiningConcatenateOperator <em>Defining Concatenate Operator</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefiningUnaryPlusOperator <em>Defining Unary Plus Operator</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefiningUnaryMinusOperator <em>Defining Unary Minus Operator</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefiningMultiplyOperator <em>Defining Multiply Operator</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefiningDivideOperator <em>Defining Divide Operator</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefiningModOperator <em>Defining Mod Operator</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefiningRemOperator <em>Defining Rem Operator</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefiningExponentiateOperator <em>Defining Exponentiate Operator</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefiningAbsOperator <em>Defining Abs Operator</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefiningNotOperator <em>Defining Not Operator</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefiningExpandedName <em>Defining Expanded Name</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getAsynchronousPragma <em>Asynchronous Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getAtomicPragma <em>Atomic Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getAttachHandlerPragma <em>Attach Handler Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getControlledPragma <em>Controlled Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getConventionPragma <em>Convention Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDiscardNamesPragma <em>Discard Names Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getElaboratePragma <em>Elaborate Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getElaborateAllPragma <em>Elaborate All Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getExportPragma <em>Export Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getImportPragma <em>Import Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getInlinePragma <em>Inline Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getInspectionPointPragma <em>Inspection Point Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getLinkerOptionsPragma <em>Linker Options Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getListPragma <em>List Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getLockingPolicyPragma <em>Locking Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getOptimizePragma <em>Optimize Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getPackPragma <em>Pack Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getPagePragma <em>Page Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getPreelaboratePragma <em>Preelaborate Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getPriorityPragma <em>Priority Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getPurePragma <em>Pure Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getRemoteTypesPragma <em>Remote Types Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getRestrictionsPragma <em>Restrictions Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getReviewablePragma <em>Reviewable Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getSharedPassivePragma <em>Shared Passive Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getStorageSizePragma <em>Storage Size Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getSuppressPragma <em>Suppress Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getVolatilePragma <em>Volatile Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getAssertPragma <em>Assert Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getNoReturnPragma <em>No Return Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getProfilePragma <em>Profile Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getUnsuppressPragma <em>Unsuppress Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getCpuPragma <em>Cpu Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getIndependentPragma <em>Independent Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getIndependentComponentsPragma <em>Independent Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefiningNameListImpl#getUnknownPragma <em>Unknown Pragma</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DefiningNameListImpl extends MinimalEObjectImpl.Container implements DefiningNameList {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DefiningNameListImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getDefiningNameList();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, AdaPackage.DEFINING_NAME_LIST__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NotAnElement> getNotAnElement() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_NotAnElement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningIdentifier> getDefiningIdentifier() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefiningIdentifier());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningCharacterLiteral> getDefiningCharacterLiteral() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefiningCharacterLiteral());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningEnumerationLiteral> getDefiningEnumerationLiteral() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefiningEnumerationLiteral());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningAndOperator> getDefiningAndOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefiningAndOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningOrOperator> getDefiningOrOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefiningOrOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningXorOperator> getDefiningXorOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefiningXorOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningEqualOperator> getDefiningEqualOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefiningEqualOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningNotEqualOperator> getDefiningNotEqualOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefiningNotEqualOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningLessThanOperator> getDefiningLessThanOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefiningLessThanOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningLessThanOrEqualOperator> getDefiningLessThanOrEqualOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefiningLessThanOrEqualOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningGreaterThanOperator> getDefiningGreaterThanOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefiningGreaterThanOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningGreaterThanOrEqualOperator> getDefiningGreaterThanOrEqualOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefiningGreaterThanOrEqualOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningPlusOperator> getDefiningPlusOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefiningPlusOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningMinusOperator> getDefiningMinusOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefiningMinusOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningConcatenateOperator> getDefiningConcatenateOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefiningConcatenateOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningUnaryPlusOperator> getDefiningUnaryPlusOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefiningUnaryPlusOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningUnaryMinusOperator> getDefiningUnaryMinusOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefiningUnaryMinusOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningMultiplyOperator> getDefiningMultiplyOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefiningMultiplyOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningDivideOperator> getDefiningDivideOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefiningDivideOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningModOperator> getDefiningModOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefiningModOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningRemOperator> getDefiningRemOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefiningRemOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningExponentiateOperator> getDefiningExponentiateOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefiningExponentiateOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningAbsOperator> getDefiningAbsOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefiningAbsOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningNotOperator> getDefiningNotOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefiningNotOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiningExpandedName> getDefiningExpandedName() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefiningExpandedName());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Comment> getComment() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_Comment());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AllCallsRemotePragma> getAllCallsRemotePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_AllCallsRemotePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AsynchronousPragma> getAsynchronousPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_AsynchronousPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AtomicPragma> getAtomicPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_AtomicPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AtomicComponentsPragma> getAtomicComponentsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_AtomicComponentsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AttachHandlerPragma> getAttachHandlerPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_AttachHandlerPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlledPragma> getControlledPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_ControlledPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConventionPragma> getConventionPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_ConventionPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscardNamesPragma> getDiscardNamesPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DiscardNamesPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElaboratePragma> getElaboratePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_ElaboratePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElaborateAllPragma> getElaborateAllPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_ElaborateAllPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElaborateBodyPragma> getElaborateBodyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_ElaborateBodyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExportPragma> getExportPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_ExportPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ImportPragma> getImportPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_ImportPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InlinePragma> getInlinePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_InlinePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InspectionPointPragma> getInspectionPointPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_InspectionPointPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InterruptHandlerPragma> getInterruptHandlerPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_InterruptHandlerPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InterruptPriorityPragma> getInterruptPriorityPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_InterruptPriorityPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LinkerOptionsPragma> getLinkerOptionsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_LinkerOptionsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ListPragma> getListPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_ListPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LockingPolicyPragma> getLockingPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_LockingPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NormalizeScalarsPragma> getNormalizeScalarsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_NormalizeScalarsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OptimizePragma> getOptimizePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_OptimizePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackPragma> getPackPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_PackPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PagePragma> getPagePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_PagePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PreelaboratePragma> getPreelaboratePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_PreelaboratePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PriorityPragma> getPriorityPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_PriorityPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PurePragma> getPurePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_PurePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QueuingPolicyPragma> getQueuingPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_QueuingPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RemoteCallInterfacePragma> getRemoteCallInterfacePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_RemoteCallInterfacePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RemoteTypesPragma> getRemoteTypesPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_RemoteTypesPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RestrictionsPragma> getRestrictionsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_RestrictionsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReviewablePragma> getReviewablePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_ReviewablePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SharedPassivePragma> getSharedPassivePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_SharedPassivePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StorageSizePragma> getStorageSizePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_StorageSizePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SuppressPragma> getSuppressPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_SuppressPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskDispatchingPolicyPragma> getTaskDispatchingPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_TaskDispatchingPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VolatilePragma> getVolatilePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_VolatilePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VolatileComponentsPragma> getVolatileComponentsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_VolatileComponentsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssertPragma> getAssertPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_AssertPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssertionPolicyPragma> getAssertionPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_AssertionPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DetectBlockingPragma> getDetectBlockingPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DetectBlockingPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NoReturnPragma> getNoReturnPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_NoReturnPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PartitionElaborationPolicyPragma> getPartitionElaborationPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_PartitionElaborationPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PreelaborableInitializationPragma> getPreelaborableInitializationPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_PreelaborableInitializationPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PrioritySpecificDispatchingPragma> getPrioritySpecificDispatchingPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_PrioritySpecificDispatchingPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProfilePragma> getProfilePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_ProfilePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RelativeDeadlinePragma> getRelativeDeadlinePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_RelativeDeadlinePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UncheckedUnionPragma> getUncheckedUnionPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_UncheckedUnionPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnsuppressPragma> getUnsuppressPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_UnsuppressPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefaultStoragePoolPragma> getDefaultStoragePoolPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DefaultStoragePoolPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DispatchingDomainPragma> getDispatchingDomainPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_DispatchingDomainPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CpuPragma> getCpuPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_CpuPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IndependentPragma> getIndependentPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_IndependentPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IndependentComponentsPragma> getIndependentComponentsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_IndependentComponentsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ImplementationDefinedPragma> getImplementationDefinedPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_ImplementationDefinedPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnknownPragma> getUnknownPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefiningNameList_UnknownPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.DEFINING_NAME_LIST__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__NOT_AN_ELEMENT:
				return ((InternalEList<?>)getNotAnElement()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_IDENTIFIER:
				return ((InternalEList<?>)getDefiningIdentifier()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_CHARACTER_LITERAL:
				return ((InternalEList<?>)getDefiningCharacterLiteral()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_ENUMERATION_LITERAL:
				return ((InternalEList<?>)getDefiningEnumerationLiteral()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_AND_OPERATOR:
				return ((InternalEList<?>)getDefiningAndOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_OR_OPERATOR:
				return ((InternalEList<?>)getDefiningOrOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_XOR_OPERATOR:
				return ((InternalEList<?>)getDefiningXorOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_EQUAL_OPERATOR:
				return ((InternalEList<?>)getDefiningEqualOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_NOT_EQUAL_OPERATOR:
				return ((InternalEList<?>)getDefiningNotEqualOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_LESS_THAN_OPERATOR:
				return ((InternalEList<?>)getDefiningLessThanOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_LESS_THAN_OR_EQUAL_OPERATOR:
				return ((InternalEList<?>)getDefiningLessThanOrEqualOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_GREATER_THAN_OPERATOR:
				return ((InternalEList<?>)getDefiningGreaterThanOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_GREATER_THAN_OR_EQUAL_OPERATOR:
				return ((InternalEList<?>)getDefiningGreaterThanOrEqualOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_PLUS_OPERATOR:
				return ((InternalEList<?>)getDefiningPlusOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_MINUS_OPERATOR:
				return ((InternalEList<?>)getDefiningMinusOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_CONCATENATE_OPERATOR:
				return ((InternalEList<?>)getDefiningConcatenateOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_UNARY_PLUS_OPERATOR:
				return ((InternalEList<?>)getDefiningUnaryPlusOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_UNARY_MINUS_OPERATOR:
				return ((InternalEList<?>)getDefiningUnaryMinusOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_MULTIPLY_OPERATOR:
				return ((InternalEList<?>)getDefiningMultiplyOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_DIVIDE_OPERATOR:
				return ((InternalEList<?>)getDefiningDivideOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_MOD_OPERATOR:
				return ((InternalEList<?>)getDefiningModOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_REM_OPERATOR:
				return ((InternalEList<?>)getDefiningRemOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_EXPONENTIATE_OPERATOR:
				return ((InternalEList<?>)getDefiningExponentiateOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_ABS_OPERATOR:
				return ((InternalEList<?>)getDefiningAbsOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_NOT_OPERATOR:
				return ((InternalEList<?>)getDefiningNotOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_EXPANDED_NAME:
				return ((InternalEList<?>)getDefiningExpandedName()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__COMMENT:
				return ((InternalEList<?>)getComment()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__ALL_CALLS_REMOTE_PRAGMA:
				return ((InternalEList<?>)getAllCallsRemotePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__ASYNCHRONOUS_PRAGMA:
				return ((InternalEList<?>)getAsynchronousPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__ATOMIC_PRAGMA:
				return ((InternalEList<?>)getAtomicPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__ATOMIC_COMPONENTS_PRAGMA:
				return ((InternalEList<?>)getAtomicComponentsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__ATTACH_HANDLER_PRAGMA:
				return ((InternalEList<?>)getAttachHandlerPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__CONTROLLED_PRAGMA:
				return ((InternalEList<?>)getControlledPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__CONVENTION_PRAGMA:
				return ((InternalEList<?>)getConventionPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DISCARD_NAMES_PRAGMA:
				return ((InternalEList<?>)getDiscardNamesPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__ELABORATE_PRAGMA:
				return ((InternalEList<?>)getElaboratePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__ELABORATE_ALL_PRAGMA:
				return ((InternalEList<?>)getElaborateAllPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__ELABORATE_BODY_PRAGMA:
				return ((InternalEList<?>)getElaborateBodyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__EXPORT_PRAGMA:
				return ((InternalEList<?>)getExportPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__IMPORT_PRAGMA:
				return ((InternalEList<?>)getImportPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__INLINE_PRAGMA:
				return ((InternalEList<?>)getInlinePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__INSPECTION_POINT_PRAGMA:
				return ((InternalEList<?>)getInspectionPointPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__INTERRUPT_HANDLER_PRAGMA:
				return ((InternalEList<?>)getInterruptHandlerPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__INTERRUPT_PRIORITY_PRAGMA:
				return ((InternalEList<?>)getInterruptPriorityPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__LINKER_OPTIONS_PRAGMA:
				return ((InternalEList<?>)getLinkerOptionsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__LIST_PRAGMA:
				return ((InternalEList<?>)getListPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__LOCKING_POLICY_PRAGMA:
				return ((InternalEList<?>)getLockingPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__NORMALIZE_SCALARS_PRAGMA:
				return ((InternalEList<?>)getNormalizeScalarsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__OPTIMIZE_PRAGMA:
				return ((InternalEList<?>)getOptimizePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__PACK_PRAGMA:
				return ((InternalEList<?>)getPackPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__PAGE_PRAGMA:
				return ((InternalEList<?>)getPagePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__PREELABORATE_PRAGMA:
				return ((InternalEList<?>)getPreelaboratePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__PRIORITY_PRAGMA:
				return ((InternalEList<?>)getPriorityPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__PURE_PRAGMA:
				return ((InternalEList<?>)getPurePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__QUEUING_POLICY_PRAGMA:
				return ((InternalEList<?>)getQueuingPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				return ((InternalEList<?>)getRemoteCallInterfacePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__REMOTE_TYPES_PRAGMA:
				return ((InternalEList<?>)getRemoteTypesPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__RESTRICTIONS_PRAGMA:
				return ((InternalEList<?>)getRestrictionsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__REVIEWABLE_PRAGMA:
				return ((InternalEList<?>)getReviewablePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__SHARED_PASSIVE_PRAGMA:
				return ((InternalEList<?>)getSharedPassivePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__STORAGE_SIZE_PRAGMA:
				return ((InternalEList<?>)getStorageSizePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__SUPPRESS_PRAGMA:
				return ((InternalEList<?>)getSuppressPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				return ((InternalEList<?>)getTaskDispatchingPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__VOLATILE_PRAGMA:
				return ((InternalEList<?>)getVolatilePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__VOLATILE_COMPONENTS_PRAGMA:
				return ((InternalEList<?>)getVolatileComponentsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__ASSERT_PRAGMA:
				return ((InternalEList<?>)getAssertPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__ASSERTION_POLICY_PRAGMA:
				return ((InternalEList<?>)getAssertionPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DETECT_BLOCKING_PRAGMA:
				return ((InternalEList<?>)getDetectBlockingPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__NO_RETURN_PRAGMA:
				return ((InternalEList<?>)getNoReturnPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				return ((InternalEList<?>)getPartitionElaborationPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				return ((InternalEList<?>)getPreelaborableInitializationPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return ((InternalEList<?>)getPrioritySpecificDispatchingPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__PROFILE_PRAGMA:
				return ((InternalEList<?>)getProfilePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__RELATIVE_DEADLINE_PRAGMA:
				return ((InternalEList<?>)getRelativeDeadlinePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__UNCHECKED_UNION_PRAGMA:
				return ((InternalEList<?>)getUncheckedUnionPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__UNSUPPRESS_PRAGMA:
				return ((InternalEList<?>)getUnsuppressPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				return ((InternalEList<?>)getDefaultStoragePoolPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__DISPATCHING_DOMAIN_PRAGMA:
				return ((InternalEList<?>)getDispatchingDomainPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__CPU_PRAGMA:
				return ((InternalEList<?>)getCpuPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__INDEPENDENT_PRAGMA:
				return ((InternalEList<?>)getIndependentPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				return ((InternalEList<?>)getIndependentComponentsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				return ((InternalEList<?>)getImplementationDefinedPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINING_NAME_LIST__UNKNOWN_PRAGMA:
				return ((InternalEList<?>)getUnknownPragma()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.DEFINING_NAME_LIST__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case AdaPackage.DEFINING_NAME_LIST__NOT_AN_ELEMENT:
				return getNotAnElement();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_IDENTIFIER:
				return getDefiningIdentifier();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_CHARACTER_LITERAL:
				return getDefiningCharacterLiteral();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_ENUMERATION_LITERAL:
				return getDefiningEnumerationLiteral();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_AND_OPERATOR:
				return getDefiningAndOperator();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_OR_OPERATOR:
				return getDefiningOrOperator();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_XOR_OPERATOR:
				return getDefiningXorOperator();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_EQUAL_OPERATOR:
				return getDefiningEqualOperator();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_NOT_EQUAL_OPERATOR:
				return getDefiningNotEqualOperator();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_LESS_THAN_OPERATOR:
				return getDefiningLessThanOperator();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_LESS_THAN_OR_EQUAL_OPERATOR:
				return getDefiningLessThanOrEqualOperator();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_GREATER_THAN_OPERATOR:
				return getDefiningGreaterThanOperator();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_GREATER_THAN_OR_EQUAL_OPERATOR:
				return getDefiningGreaterThanOrEqualOperator();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_PLUS_OPERATOR:
				return getDefiningPlusOperator();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_MINUS_OPERATOR:
				return getDefiningMinusOperator();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_CONCATENATE_OPERATOR:
				return getDefiningConcatenateOperator();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_UNARY_PLUS_OPERATOR:
				return getDefiningUnaryPlusOperator();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_UNARY_MINUS_OPERATOR:
				return getDefiningUnaryMinusOperator();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_MULTIPLY_OPERATOR:
				return getDefiningMultiplyOperator();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_DIVIDE_OPERATOR:
				return getDefiningDivideOperator();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_MOD_OPERATOR:
				return getDefiningModOperator();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_REM_OPERATOR:
				return getDefiningRemOperator();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_EXPONENTIATE_OPERATOR:
				return getDefiningExponentiateOperator();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_ABS_OPERATOR:
				return getDefiningAbsOperator();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_NOT_OPERATOR:
				return getDefiningNotOperator();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_EXPANDED_NAME:
				return getDefiningExpandedName();
			case AdaPackage.DEFINING_NAME_LIST__COMMENT:
				return getComment();
			case AdaPackage.DEFINING_NAME_LIST__ALL_CALLS_REMOTE_PRAGMA:
				return getAllCallsRemotePragma();
			case AdaPackage.DEFINING_NAME_LIST__ASYNCHRONOUS_PRAGMA:
				return getAsynchronousPragma();
			case AdaPackage.DEFINING_NAME_LIST__ATOMIC_PRAGMA:
				return getAtomicPragma();
			case AdaPackage.DEFINING_NAME_LIST__ATOMIC_COMPONENTS_PRAGMA:
				return getAtomicComponentsPragma();
			case AdaPackage.DEFINING_NAME_LIST__ATTACH_HANDLER_PRAGMA:
				return getAttachHandlerPragma();
			case AdaPackage.DEFINING_NAME_LIST__CONTROLLED_PRAGMA:
				return getControlledPragma();
			case AdaPackage.DEFINING_NAME_LIST__CONVENTION_PRAGMA:
				return getConventionPragma();
			case AdaPackage.DEFINING_NAME_LIST__DISCARD_NAMES_PRAGMA:
				return getDiscardNamesPragma();
			case AdaPackage.DEFINING_NAME_LIST__ELABORATE_PRAGMA:
				return getElaboratePragma();
			case AdaPackage.DEFINING_NAME_LIST__ELABORATE_ALL_PRAGMA:
				return getElaborateAllPragma();
			case AdaPackage.DEFINING_NAME_LIST__ELABORATE_BODY_PRAGMA:
				return getElaborateBodyPragma();
			case AdaPackage.DEFINING_NAME_LIST__EXPORT_PRAGMA:
				return getExportPragma();
			case AdaPackage.DEFINING_NAME_LIST__IMPORT_PRAGMA:
				return getImportPragma();
			case AdaPackage.DEFINING_NAME_LIST__INLINE_PRAGMA:
				return getInlinePragma();
			case AdaPackage.DEFINING_NAME_LIST__INSPECTION_POINT_PRAGMA:
				return getInspectionPointPragma();
			case AdaPackage.DEFINING_NAME_LIST__INTERRUPT_HANDLER_PRAGMA:
				return getInterruptHandlerPragma();
			case AdaPackage.DEFINING_NAME_LIST__INTERRUPT_PRIORITY_PRAGMA:
				return getInterruptPriorityPragma();
			case AdaPackage.DEFINING_NAME_LIST__LINKER_OPTIONS_PRAGMA:
				return getLinkerOptionsPragma();
			case AdaPackage.DEFINING_NAME_LIST__LIST_PRAGMA:
				return getListPragma();
			case AdaPackage.DEFINING_NAME_LIST__LOCKING_POLICY_PRAGMA:
				return getLockingPolicyPragma();
			case AdaPackage.DEFINING_NAME_LIST__NORMALIZE_SCALARS_PRAGMA:
				return getNormalizeScalarsPragma();
			case AdaPackage.DEFINING_NAME_LIST__OPTIMIZE_PRAGMA:
				return getOptimizePragma();
			case AdaPackage.DEFINING_NAME_LIST__PACK_PRAGMA:
				return getPackPragma();
			case AdaPackage.DEFINING_NAME_LIST__PAGE_PRAGMA:
				return getPagePragma();
			case AdaPackage.DEFINING_NAME_LIST__PREELABORATE_PRAGMA:
				return getPreelaboratePragma();
			case AdaPackage.DEFINING_NAME_LIST__PRIORITY_PRAGMA:
				return getPriorityPragma();
			case AdaPackage.DEFINING_NAME_LIST__PURE_PRAGMA:
				return getPurePragma();
			case AdaPackage.DEFINING_NAME_LIST__QUEUING_POLICY_PRAGMA:
				return getQueuingPolicyPragma();
			case AdaPackage.DEFINING_NAME_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				return getRemoteCallInterfacePragma();
			case AdaPackage.DEFINING_NAME_LIST__REMOTE_TYPES_PRAGMA:
				return getRemoteTypesPragma();
			case AdaPackage.DEFINING_NAME_LIST__RESTRICTIONS_PRAGMA:
				return getRestrictionsPragma();
			case AdaPackage.DEFINING_NAME_LIST__REVIEWABLE_PRAGMA:
				return getReviewablePragma();
			case AdaPackage.DEFINING_NAME_LIST__SHARED_PASSIVE_PRAGMA:
				return getSharedPassivePragma();
			case AdaPackage.DEFINING_NAME_LIST__STORAGE_SIZE_PRAGMA:
				return getStorageSizePragma();
			case AdaPackage.DEFINING_NAME_LIST__SUPPRESS_PRAGMA:
				return getSuppressPragma();
			case AdaPackage.DEFINING_NAME_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				return getTaskDispatchingPolicyPragma();
			case AdaPackage.DEFINING_NAME_LIST__VOLATILE_PRAGMA:
				return getVolatilePragma();
			case AdaPackage.DEFINING_NAME_LIST__VOLATILE_COMPONENTS_PRAGMA:
				return getVolatileComponentsPragma();
			case AdaPackage.DEFINING_NAME_LIST__ASSERT_PRAGMA:
				return getAssertPragma();
			case AdaPackage.DEFINING_NAME_LIST__ASSERTION_POLICY_PRAGMA:
				return getAssertionPolicyPragma();
			case AdaPackage.DEFINING_NAME_LIST__DETECT_BLOCKING_PRAGMA:
				return getDetectBlockingPragma();
			case AdaPackage.DEFINING_NAME_LIST__NO_RETURN_PRAGMA:
				return getNoReturnPragma();
			case AdaPackage.DEFINING_NAME_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				return getPartitionElaborationPolicyPragma();
			case AdaPackage.DEFINING_NAME_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				return getPreelaborableInitializationPragma();
			case AdaPackage.DEFINING_NAME_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return getPrioritySpecificDispatchingPragma();
			case AdaPackage.DEFINING_NAME_LIST__PROFILE_PRAGMA:
				return getProfilePragma();
			case AdaPackage.DEFINING_NAME_LIST__RELATIVE_DEADLINE_PRAGMA:
				return getRelativeDeadlinePragma();
			case AdaPackage.DEFINING_NAME_LIST__UNCHECKED_UNION_PRAGMA:
				return getUncheckedUnionPragma();
			case AdaPackage.DEFINING_NAME_LIST__UNSUPPRESS_PRAGMA:
				return getUnsuppressPragma();
			case AdaPackage.DEFINING_NAME_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				return getDefaultStoragePoolPragma();
			case AdaPackage.DEFINING_NAME_LIST__DISPATCHING_DOMAIN_PRAGMA:
				return getDispatchingDomainPragma();
			case AdaPackage.DEFINING_NAME_LIST__CPU_PRAGMA:
				return getCpuPragma();
			case AdaPackage.DEFINING_NAME_LIST__INDEPENDENT_PRAGMA:
				return getIndependentPragma();
			case AdaPackage.DEFINING_NAME_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				return getIndependentComponentsPragma();
			case AdaPackage.DEFINING_NAME_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				return getImplementationDefinedPragma();
			case AdaPackage.DEFINING_NAME_LIST__UNKNOWN_PRAGMA:
				return getUnknownPragma();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.DEFINING_NAME_LIST__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__NOT_AN_ELEMENT:
				getNotAnElement().clear();
				getNotAnElement().addAll((Collection<? extends NotAnElement>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_IDENTIFIER:
				getDefiningIdentifier().clear();
				getDefiningIdentifier().addAll((Collection<? extends DefiningIdentifier>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_CHARACTER_LITERAL:
				getDefiningCharacterLiteral().clear();
				getDefiningCharacterLiteral().addAll((Collection<? extends DefiningCharacterLiteral>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_ENUMERATION_LITERAL:
				getDefiningEnumerationLiteral().clear();
				getDefiningEnumerationLiteral().addAll((Collection<? extends DefiningEnumerationLiteral>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_AND_OPERATOR:
				getDefiningAndOperator().clear();
				getDefiningAndOperator().addAll((Collection<? extends DefiningAndOperator>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_OR_OPERATOR:
				getDefiningOrOperator().clear();
				getDefiningOrOperator().addAll((Collection<? extends DefiningOrOperator>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_XOR_OPERATOR:
				getDefiningXorOperator().clear();
				getDefiningXorOperator().addAll((Collection<? extends DefiningXorOperator>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_EQUAL_OPERATOR:
				getDefiningEqualOperator().clear();
				getDefiningEqualOperator().addAll((Collection<? extends DefiningEqualOperator>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_NOT_EQUAL_OPERATOR:
				getDefiningNotEqualOperator().clear();
				getDefiningNotEqualOperator().addAll((Collection<? extends DefiningNotEqualOperator>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_LESS_THAN_OPERATOR:
				getDefiningLessThanOperator().clear();
				getDefiningLessThanOperator().addAll((Collection<? extends DefiningLessThanOperator>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_LESS_THAN_OR_EQUAL_OPERATOR:
				getDefiningLessThanOrEqualOperator().clear();
				getDefiningLessThanOrEqualOperator().addAll((Collection<? extends DefiningLessThanOrEqualOperator>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_GREATER_THAN_OPERATOR:
				getDefiningGreaterThanOperator().clear();
				getDefiningGreaterThanOperator().addAll((Collection<? extends DefiningGreaterThanOperator>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_GREATER_THAN_OR_EQUAL_OPERATOR:
				getDefiningGreaterThanOrEqualOperator().clear();
				getDefiningGreaterThanOrEqualOperator().addAll((Collection<? extends DefiningGreaterThanOrEqualOperator>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_PLUS_OPERATOR:
				getDefiningPlusOperator().clear();
				getDefiningPlusOperator().addAll((Collection<? extends DefiningPlusOperator>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_MINUS_OPERATOR:
				getDefiningMinusOperator().clear();
				getDefiningMinusOperator().addAll((Collection<? extends DefiningMinusOperator>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_CONCATENATE_OPERATOR:
				getDefiningConcatenateOperator().clear();
				getDefiningConcatenateOperator().addAll((Collection<? extends DefiningConcatenateOperator>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_UNARY_PLUS_OPERATOR:
				getDefiningUnaryPlusOperator().clear();
				getDefiningUnaryPlusOperator().addAll((Collection<? extends DefiningUnaryPlusOperator>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_UNARY_MINUS_OPERATOR:
				getDefiningUnaryMinusOperator().clear();
				getDefiningUnaryMinusOperator().addAll((Collection<? extends DefiningUnaryMinusOperator>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_MULTIPLY_OPERATOR:
				getDefiningMultiplyOperator().clear();
				getDefiningMultiplyOperator().addAll((Collection<? extends DefiningMultiplyOperator>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_DIVIDE_OPERATOR:
				getDefiningDivideOperator().clear();
				getDefiningDivideOperator().addAll((Collection<? extends DefiningDivideOperator>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_MOD_OPERATOR:
				getDefiningModOperator().clear();
				getDefiningModOperator().addAll((Collection<? extends DefiningModOperator>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_REM_OPERATOR:
				getDefiningRemOperator().clear();
				getDefiningRemOperator().addAll((Collection<? extends DefiningRemOperator>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_EXPONENTIATE_OPERATOR:
				getDefiningExponentiateOperator().clear();
				getDefiningExponentiateOperator().addAll((Collection<? extends DefiningExponentiateOperator>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_ABS_OPERATOR:
				getDefiningAbsOperator().clear();
				getDefiningAbsOperator().addAll((Collection<? extends DefiningAbsOperator>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_NOT_OPERATOR:
				getDefiningNotOperator().clear();
				getDefiningNotOperator().addAll((Collection<? extends DefiningNotOperator>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_EXPANDED_NAME:
				getDefiningExpandedName().clear();
				getDefiningExpandedName().addAll((Collection<? extends DefiningExpandedName>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__COMMENT:
				getComment().clear();
				getComment().addAll((Collection<? extends Comment>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__ALL_CALLS_REMOTE_PRAGMA:
				getAllCallsRemotePragma().clear();
				getAllCallsRemotePragma().addAll((Collection<? extends AllCallsRemotePragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__ASYNCHRONOUS_PRAGMA:
				getAsynchronousPragma().clear();
				getAsynchronousPragma().addAll((Collection<? extends AsynchronousPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__ATOMIC_PRAGMA:
				getAtomicPragma().clear();
				getAtomicPragma().addAll((Collection<? extends AtomicPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__ATOMIC_COMPONENTS_PRAGMA:
				getAtomicComponentsPragma().clear();
				getAtomicComponentsPragma().addAll((Collection<? extends AtomicComponentsPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__ATTACH_HANDLER_PRAGMA:
				getAttachHandlerPragma().clear();
				getAttachHandlerPragma().addAll((Collection<? extends AttachHandlerPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__CONTROLLED_PRAGMA:
				getControlledPragma().clear();
				getControlledPragma().addAll((Collection<? extends ControlledPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__CONVENTION_PRAGMA:
				getConventionPragma().clear();
				getConventionPragma().addAll((Collection<? extends ConventionPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DISCARD_NAMES_PRAGMA:
				getDiscardNamesPragma().clear();
				getDiscardNamesPragma().addAll((Collection<? extends DiscardNamesPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__ELABORATE_PRAGMA:
				getElaboratePragma().clear();
				getElaboratePragma().addAll((Collection<? extends ElaboratePragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__ELABORATE_ALL_PRAGMA:
				getElaborateAllPragma().clear();
				getElaborateAllPragma().addAll((Collection<? extends ElaborateAllPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__ELABORATE_BODY_PRAGMA:
				getElaborateBodyPragma().clear();
				getElaborateBodyPragma().addAll((Collection<? extends ElaborateBodyPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__EXPORT_PRAGMA:
				getExportPragma().clear();
				getExportPragma().addAll((Collection<? extends ExportPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__IMPORT_PRAGMA:
				getImportPragma().clear();
				getImportPragma().addAll((Collection<? extends ImportPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__INLINE_PRAGMA:
				getInlinePragma().clear();
				getInlinePragma().addAll((Collection<? extends InlinePragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__INSPECTION_POINT_PRAGMA:
				getInspectionPointPragma().clear();
				getInspectionPointPragma().addAll((Collection<? extends InspectionPointPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__INTERRUPT_HANDLER_PRAGMA:
				getInterruptHandlerPragma().clear();
				getInterruptHandlerPragma().addAll((Collection<? extends InterruptHandlerPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__INTERRUPT_PRIORITY_PRAGMA:
				getInterruptPriorityPragma().clear();
				getInterruptPriorityPragma().addAll((Collection<? extends InterruptPriorityPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__LINKER_OPTIONS_PRAGMA:
				getLinkerOptionsPragma().clear();
				getLinkerOptionsPragma().addAll((Collection<? extends LinkerOptionsPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__LIST_PRAGMA:
				getListPragma().clear();
				getListPragma().addAll((Collection<? extends ListPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__LOCKING_POLICY_PRAGMA:
				getLockingPolicyPragma().clear();
				getLockingPolicyPragma().addAll((Collection<? extends LockingPolicyPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__NORMALIZE_SCALARS_PRAGMA:
				getNormalizeScalarsPragma().clear();
				getNormalizeScalarsPragma().addAll((Collection<? extends NormalizeScalarsPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__OPTIMIZE_PRAGMA:
				getOptimizePragma().clear();
				getOptimizePragma().addAll((Collection<? extends OptimizePragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__PACK_PRAGMA:
				getPackPragma().clear();
				getPackPragma().addAll((Collection<? extends PackPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__PAGE_PRAGMA:
				getPagePragma().clear();
				getPagePragma().addAll((Collection<? extends PagePragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__PREELABORATE_PRAGMA:
				getPreelaboratePragma().clear();
				getPreelaboratePragma().addAll((Collection<? extends PreelaboratePragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__PRIORITY_PRAGMA:
				getPriorityPragma().clear();
				getPriorityPragma().addAll((Collection<? extends PriorityPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__PURE_PRAGMA:
				getPurePragma().clear();
				getPurePragma().addAll((Collection<? extends PurePragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__QUEUING_POLICY_PRAGMA:
				getQueuingPolicyPragma().clear();
				getQueuingPolicyPragma().addAll((Collection<? extends QueuingPolicyPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				getRemoteCallInterfacePragma().clear();
				getRemoteCallInterfacePragma().addAll((Collection<? extends RemoteCallInterfacePragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__REMOTE_TYPES_PRAGMA:
				getRemoteTypesPragma().clear();
				getRemoteTypesPragma().addAll((Collection<? extends RemoteTypesPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__RESTRICTIONS_PRAGMA:
				getRestrictionsPragma().clear();
				getRestrictionsPragma().addAll((Collection<? extends RestrictionsPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__REVIEWABLE_PRAGMA:
				getReviewablePragma().clear();
				getReviewablePragma().addAll((Collection<? extends ReviewablePragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__SHARED_PASSIVE_PRAGMA:
				getSharedPassivePragma().clear();
				getSharedPassivePragma().addAll((Collection<? extends SharedPassivePragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__STORAGE_SIZE_PRAGMA:
				getStorageSizePragma().clear();
				getStorageSizePragma().addAll((Collection<? extends StorageSizePragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__SUPPRESS_PRAGMA:
				getSuppressPragma().clear();
				getSuppressPragma().addAll((Collection<? extends SuppressPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				getTaskDispatchingPolicyPragma().clear();
				getTaskDispatchingPolicyPragma().addAll((Collection<? extends TaskDispatchingPolicyPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__VOLATILE_PRAGMA:
				getVolatilePragma().clear();
				getVolatilePragma().addAll((Collection<? extends VolatilePragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__VOLATILE_COMPONENTS_PRAGMA:
				getVolatileComponentsPragma().clear();
				getVolatileComponentsPragma().addAll((Collection<? extends VolatileComponentsPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__ASSERT_PRAGMA:
				getAssertPragma().clear();
				getAssertPragma().addAll((Collection<? extends AssertPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__ASSERTION_POLICY_PRAGMA:
				getAssertionPolicyPragma().clear();
				getAssertionPolicyPragma().addAll((Collection<? extends AssertionPolicyPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DETECT_BLOCKING_PRAGMA:
				getDetectBlockingPragma().clear();
				getDetectBlockingPragma().addAll((Collection<? extends DetectBlockingPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__NO_RETURN_PRAGMA:
				getNoReturnPragma().clear();
				getNoReturnPragma().addAll((Collection<? extends NoReturnPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				getPartitionElaborationPolicyPragma().clear();
				getPartitionElaborationPolicyPragma().addAll((Collection<? extends PartitionElaborationPolicyPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				getPreelaborableInitializationPragma().clear();
				getPreelaborableInitializationPragma().addAll((Collection<? extends PreelaborableInitializationPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				getPrioritySpecificDispatchingPragma().clear();
				getPrioritySpecificDispatchingPragma().addAll((Collection<? extends PrioritySpecificDispatchingPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__PROFILE_PRAGMA:
				getProfilePragma().clear();
				getProfilePragma().addAll((Collection<? extends ProfilePragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__RELATIVE_DEADLINE_PRAGMA:
				getRelativeDeadlinePragma().clear();
				getRelativeDeadlinePragma().addAll((Collection<? extends RelativeDeadlinePragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__UNCHECKED_UNION_PRAGMA:
				getUncheckedUnionPragma().clear();
				getUncheckedUnionPragma().addAll((Collection<? extends UncheckedUnionPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__UNSUPPRESS_PRAGMA:
				getUnsuppressPragma().clear();
				getUnsuppressPragma().addAll((Collection<? extends UnsuppressPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				getDefaultStoragePoolPragma().clear();
				getDefaultStoragePoolPragma().addAll((Collection<? extends DefaultStoragePoolPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__DISPATCHING_DOMAIN_PRAGMA:
				getDispatchingDomainPragma().clear();
				getDispatchingDomainPragma().addAll((Collection<? extends DispatchingDomainPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__CPU_PRAGMA:
				getCpuPragma().clear();
				getCpuPragma().addAll((Collection<? extends CpuPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__INDEPENDENT_PRAGMA:
				getIndependentPragma().clear();
				getIndependentPragma().addAll((Collection<? extends IndependentPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				getIndependentComponentsPragma().clear();
				getIndependentComponentsPragma().addAll((Collection<? extends IndependentComponentsPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				getImplementationDefinedPragma().clear();
				getImplementationDefinedPragma().addAll((Collection<? extends ImplementationDefinedPragma>)newValue);
				return;
			case AdaPackage.DEFINING_NAME_LIST__UNKNOWN_PRAGMA:
				getUnknownPragma().clear();
				getUnknownPragma().addAll((Collection<? extends UnknownPragma>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.DEFINING_NAME_LIST__GROUP:
				getGroup().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__NOT_AN_ELEMENT:
				getNotAnElement().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_IDENTIFIER:
				getDefiningIdentifier().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_CHARACTER_LITERAL:
				getDefiningCharacterLiteral().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_ENUMERATION_LITERAL:
				getDefiningEnumerationLiteral().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_AND_OPERATOR:
				getDefiningAndOperator().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_OR_OPERATOR:
				getDefiningOrOperator().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_XOR_OPERATOR:
				getDefiningXorOperator().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_EQUAL_OPERATOR:
				getDefiningEqualOperator().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_NOT_EQUAL_OPERATOR:
				getDefiningNotEqualOperator().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_LESS_THAN_OPERATOR:
				getDefiningLessThanOperator().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_LESS_THAN_OR_EQUAL_OPERATOR:
				getDefiningLessThanOrEqualOperator().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_GREATER_THAN_OPERATOR:
				getDefiningGreaterThanOperator().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_GREATER_THAN_OR_EQUAL_OPERATOR:
				getDefiningGreaterThanOrEqualOperator().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_PLUS_OPERATOR:
				getDefiningPlusOperator().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_MINUS_OPERATOR:
				getDefiningMinusOperator().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_CONCATENATE_OPERATOR:
				getDefiningConcatenateOperator().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_UNARY_PLUS_OPERATOR:
				getDefiningUnaryPlusOperator().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_UNARY_MINUS_OPERATOR:
				getDefiningUnaryMinusOperator().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_MULTIPLY_OPERATOR:
				getDefiningMultiplyOperator().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_DIVIDE_OPERATOR:
				getDefiningDivideOperator().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_MOD_OPERATOR:
				getDefiningModOperator().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_REM_OPERATOR:
				getDefiningRemOperator().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_EXPONENTIATE_OPERATOR:
				getDefiningExponentiateOperator().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_ABS_OPERATOR:
				getDefiningAbsOperator().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_NOT_OPERATOR:
				getDefiningNotOperator().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_EXPANDED_NAME:
				getDefiningExpandedName().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__COMMENT:
				getComment().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__ALL_CALLS_REMOTE_PRAGMA:
				getAllCallsRemotePragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__ASYNCHRONOUS_PRAGMA:
				getAsynchronousPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__ATOMIC_PRAGMA:
				getAtomicPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__ATOMIC_COMPONENTS_PRAGMA:
				getAtomicComponentsPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__ATTACH_HANDLER_PRAGMA:
				getAttachHandlerPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__CONTROLLED_PRAGMA:
				getControlledPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__CONVENTION_PRAGMA:
				getConventionPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DISCARD_NAMES_PRAGMA:
				getDiscardNamesPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__ELABORATE_PRAGMA:
				getElaboratePragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__ELABORATE_ALL_PRAGMA:
				getElaborateAllPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__ELABORATE_BODY_PRAGMA:
				getElaborateBodyPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__EXPORT_PRAGMA:
				getExportPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__IMPORT_PRAGMA:
				getImportPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__INLINE_PRAGMA:
				getInlinePragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__INSPECTION_POINT_PRAGMA:
				getInspectionPointPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__INTERRUPT_HANDLER_PRAGMA:
				getInterruptHandlerPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__INTERRUPT_PRIORITY_PRAGMA:
				getInterruptPriorityPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__LINKER_OPTIONS_PRAGMA:
				getLinkerOptionsPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__LIST_PRAGMA:
				getListPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__LOCKING_POLICY_PRAGMA:
				getLockingPolicyPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__NORMALIZE_SCALARS_PRAGMA:
				getNormalizeScalarsPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__OPTIMIZE_PRAGMA:
				getOptimizePragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__PACK_PRAGMA:
				getPackPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__PAGE_PRAGMA:
				getPagePragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__PREELABORATE_PRAGMA:
				getPreelaboratePragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__PRIORITY_PRAGMA:
				getPriorityPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__PURE_PRAGMA:
				getPurePragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__QUEUING_POLICY_PRAGMA:
				getQueuingPolicyPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				getRemoteCallInterfacePragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__REMOTE_TYPES_PRAGMA:
				getRemoteTypesPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__RESTRICTIONS_PRAGMA:
				getRestrictionsPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__REVIEWABLE_PRAGMA:
				getReviewablePragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__SHARED_PASSIVE_PRAGMA:
				getSharedPassivePragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__STORAGE_SIZE_PRAGMA:
				getStorageSizePragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__SUPPRESS_PRAGMA:
				getSuppressPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				getTaskDispatchingPolicyPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__VOLATILE_PRAGMA:
				getVolatilePragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__VOLATILE_COMPONENTS_PRAGMA:
				getVolatileComponentsPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__ASSERT_PRAGMA:
				getAssertPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__ASSERTION_POLICY_PRAGMA:
				getAssertionPolicyPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DETECT_BLOCKING_PRAGMA:
				getDetectBlockingPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__NO_RETURN_PRAGMA:
				getNoReturnPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				getPartitionElaborationPolicyPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				getPreelaborableInitializationPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				getPrioritySpecificDispatchingPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__PROFILE_PRAGMA:
				getProfilePragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__RELATIVE_DEADLINE_PRAGMA:
				getRelativeDeadlinePragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__UNCHECKED_UNION_PRAGMA:
				getUncheckedUnionPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__UNSUPPRESS_PRAGMA:
				getUnsuppressPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				getDefaultStoragePoolPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__DISPATCHING_DOMAIN_PRAGMA:
				getDispatchingDomainPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__CPU_PRAGMA:
				getCpuPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__INDEPENDENT_PRAGMA:
				getIndependentPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				getIndependentComponentsPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				getImplementationDefinedPragma().clear();
				return;
			case AdaPackage.DEFINING_NAME_LIST__UNKNOWN_PRAGMA:
				getUnknownPragma().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.DEFINING_NAME_LIST__GROUP:
				return group != null && !group.isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__NOT_AN_ELEMENT:
				return !getNotAnElement().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_IDENTIFIER:
				return !getDefiningIdentifier().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_CHARACTER_LITERAL:
				return !getDefiningCharacterLiteral().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_ENUMERATION_LITERAL:
				return !getDefiningEnumerationLiteral().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_AND_OPERATOR:
				return !getDefiningAndOperator().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_OR_OPERATOR:
				return !getDefiningOrOperator().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_XOR_OPERATOR:
				return !getDefiningXorOperator().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_EQUAL_OPERATOR:
				return !getDefiningEqualOperator().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_NOT_EQUAL_OPERATOR:
				return !getDefiningNotEqualOperator().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_LESS_THAN_OPERATOR:
				return !getDefiningLessThanOperator().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_LESS_THAN_OR_EQUAL_OPERATOR:
				return !getDefiningLessThanOrEqualOperator().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_GREATER_THAN_OPERATOR:
				return !getDefiningGreaterThanOperator().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_GREATER_THAN_OR_EQUAL_OPERATOR:
				return !getDefiningGreaterThanOrEqualOperator().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_PLUS_OPERATOR:
				return !getDefiningPlusOperator().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_MINUS_OPERATOR:
				return !getDefiningMinusOperator().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_CONCATENATE_OPERATOR:
				return !getDefiningConcatenateOperator().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_UNARY_PLUS_OPERATOR:
				return !getDefiningUnaryPlusOperator().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_UNARY_MINUS_OPERATOR:
				return !getDefiningUnaryMinusOperator().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_MULTIPLY_OPERATOR:
				return !getDefiningMultiplyOperator().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_DIVIDE_OPERATOR:
				return !getDefiningDivideOperator().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_MOD_OPERATOR:
				return !getDefiningModOperator().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_REM_OPERATOR:
				return !getDefiningRemOperator().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_EXPONENTIATE_OPERATOR:
				return !getDefiningExponentiateOperator().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_ABS_OPERATOR:
				return !getDefiningAbsOperator().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_NOT_OPERATOR:
				return !getDefiningNotOperator().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFINING_EXPANDED_NAME:
				return !getDefiningExpandedName().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__COMMENT:
				return !getComment().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__ALL_CALLS_REMOTE_PRAGMA:
				return !getAllCallsRemotePragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__ASYNCHRONOUS_PRAGMA:
				return !getAsynchronousPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__ATOMIC_PRAGMA:
				return !getAtomicPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__ATOMIC_COMPONENTS_PRAGMA:
				return !getAtomicComponentsPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__ATTACH_HANDLER_PRAGMA:
				return !getAttachHandlerPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__CONTROLLED_PRAGMA:
				return !getControlledPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__CONVENTION_PRAGMA:
				return !getConventionPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DISCARD_NAMES_PRAGMA:
				return !getDiscardNamesPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__ELABORATE_PRAGMA:
				return !getElaboratePragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__ELABORATE_ALL_PRAGMA:
				return !getElaborateAllPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__ELABORATE_BODY_PRAGMA:
				return !getElaborateBodyPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__EXPORT_PRAGMA:
				return !getExportPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__IMPORT_PRAGMA:
				return !getImportPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__INLINE_PRAGMA:
				return !getInlinePragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__INSPECTION_POINT_PRAGMA:
				return !getInspectionPointPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__INTERRUPT_HANDLER_PRAGMA:
				return !getInterruptHandlerPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__INTERRUPT_PRIORITY_PRAGMA:
				return !getInterruptPriorityPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__LINKER_OPTIONS_PRAGMA:
				return !getLinkerOptionsPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__LIST_PRAGMA:
				return !getListPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__LOCKING_POLICY_PRAGMA:
				return !getLockingPolicyPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__NORMALIZE_SCALARS_PRAGMA:
				return !getNormalizeScalarsPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__OPTIMIZE_PRAGMA:
				return !getOptimizePragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__PACK_PRAGMA:
				return !getPackPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__PAGE_PRAGMA:
				return !getPagePragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__PREELABORATE_PRAGMA:
				return !getPreelaboratePragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__PRIORITY_PRAGMA:
				return !getPriorityPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__PURE_PRAGMA:
				return !getPurePragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__QUEUING_POLICY_PRAGMA:
				return !getQueuingPolicyPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				return !getRemoteCallInterfacePragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__REMOTE_TYPES_PRAGMA:
				return !getRemoteTypesPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__RESTRICTIONS_PRAGMA:
				return !getRestrictionsPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__REVIEWABLE_PRAGMA:
				return !getReviewablePragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__SHARED_PASSIVE_PRAGMA:
				return !getSharedPassivePragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__STORAGE_SIZE_PRAGMA:
				return !getStorageSizePragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__SUPPRESS_PRAGMA:
				return !getSuppressPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				return !getTaskDispatchingPolicyPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__VOLATILE_PRAGMA:
				return !getVolatilePragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__VOLATILE_COMPONENTS_PRAGMA:
				return !getVolatileComponentsPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__ASSERT_PRAGMA:
				return !getAssertPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__ASSERTION_POLICY_PRAGMA:
				return !getAssertionPolicyPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DETECT_BLOCKING_PRAGMA:
				return !getDetectBlockingPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__NO_RETURN_PRAGMA:
				return !getNoReturnPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				return !getPartitionElaborationPolicyPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				return !getPreelaborableInitializationPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return !getPrioritySpecificDispatchingPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__PROFILE_PRAGMA:
				return !getProfilePragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__RELATIVE_DEADLINE_PRAGMA:
				return !getRelativeDeadlinePragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__UNCHECKED_UNION_PRAGMA:
				return !getUncheckedUnionPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__UNSUPPRESS_PRAGMA:
				return !getUnsuppressPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				return !getDefaultStoragePoolPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__DISPATCHING_DOMAIN_PRAGMA:
				return !getDispatchingDomainPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__CPU_PRAGMA:
				return !getCpuPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__INDEPENDENT_PRAGMA:
				return !getIndependentPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				return !getIndependentComponentsPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				return !getImplementationDefinedPragma().isEmpty();
			case AdaPackage.DEFINING_NAME_LIST__UNKNOWN_PRAGMA:
				return !getUnknownPragma().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(')');
		return result.toString();
	}

} //DefiningNameListImpl
