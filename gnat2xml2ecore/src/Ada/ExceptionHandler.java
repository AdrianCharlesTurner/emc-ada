/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exception Handler</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.ExceptionHandler#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.ExceptionHandler#getChoiceParameterSpecificationQ <em>Choice Parameter Specification Q</em>}</li>
 *   <li>{@link Ada.ExceptionHandler#getExceptionChoicesQl <em>Exception Choices Ql</em>}</li>
 *   <li>{@link Ada.ExceptionHandler#getHandlerStatementsQl <em>Handler Statements Ql</em>}</li>
 *   <li>{@link Ada.ExceptionHandler#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getExceptionHandler()
 * @model extendedMetaData="name='Exception_Handler' kind='elementOnly'"
 * @generated
 */
public interface ExceptionHandler extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getExceptionHandler_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.ExceptionHandler#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Choice Parameter Specification Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Choice Parameter Specification Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Choice Parameter Specification Q</em>' containment reference.
	 * @see #setChoiceParameterSpecificationQ(DeclarationClass)
	 * @see Ada.AdaPackage#getExceptionHandler_ChoiceParameterSpecificationQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='choice_parameter_specification_q' namespace='##targetNamespace'"
	 * @generated
	 */
	DeclarationClass getChoiceParameterSpecificationQ();

	/**
	 * Sets the value of the '{@link Ada.ExceptionHandler#getChoiceParameterSpecificationQ <em>Choice Parameter Specification Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Choice Parameter Specification Q</em>' containment reference.
	 * @see #getChoiceParameterSpecificationQ()
	 * @generated
	 */
	void setChoiceParameterSpecificationQ(DeclarationClass value);

	/**
	 * Returns the value of the '<em><b>Exception Choices Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exception Choices Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exception Choices Ql</em>' containment reference.
	 * @see #setExceptionChoicesQl(ElementList)
	 * @see Ada.AdaPackage#getExceptionHandler_ExceptionChoicesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='exception_choices_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementList getExceptionChoicesQl();

	/**
	 * Sets the value of the '{@link Ada.ExceptionHandler#getExceptionChoicesQl <em>Exception Choices Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exception Choices Ql</em>' containment reference.
	 * @see #getExceptionChoicesQl()
	 * @generated
	 */
	void setExceptionChoicesQl(ElementList value);

	/**
	 * Returns the value of the '<em><b>Handler Statements Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Handler Statements Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Handler Statements Ql</em>' containment reference.
	 * @see #setHandlerStatementsQl(StatementList)
	 * @see Ada.AdaPackage#getExceptionHandler_HandlerStatementsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='handler_statements_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	StatementList getHandlerStatementsQl();

	/**
	 * Sets the value of the '{@link Ada.ExceptionHandler#getHandlerStatementsQl <em>Handler Statements Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Handler Statements Ql</em>' containment reference.
	 * @see #getHandlerStatementsQl()
	 * @generated
	 */
	void setHandlerStatementsQl(StatementList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getExceptionHandler_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.ExceptionHandler#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // ExceptionHandler
