/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Raise Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.RaiseExpression#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.RaiseExpression#getRaisedExceptionQ <em>Raised Exception Q</em>}</li>
 *   <li>{@link Ada.RaiseExpression#getAssociatedMessageQ <em>Associated Message Q</em>}</li>
 *   <li>{@link Ada.RaiseExpression#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.RaiseExpression#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getRaiseExpression()
 * @model extendedMetaData="name='Raise_Expression' kind='elementOnly'"
 * @generated
 */
public interface RaiseExpression extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getRaiseExpression_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.RaiseExpression#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Raised Exception Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Raised Exception Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Raised Exception Q</em>' containment reference.
	 * @see #setRaisedExceptionQ(ExpressionClass)
	 * @see Ada.AdaPackage#getRaiseExpression_RaisedExceptionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='raised_exception_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getRaisedExceptionQ();

	/**
	 * Sets the value of the '{@link Ada.RaiseExpression#getRaisedExceptionQ <em>Raised Exception Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Raised Exception Q</em>' containment reference.
	 * @see #getRaisedExceptionQ()
	 * @generated
	 */
	void setRaisedExceptionQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Associated Message Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Associated Message Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Associated Message Q</em>' containment reference.
	 * @see #setAssociatedMessageQ(ExpressionClass)
	 * @see Ada.AdaPackage#getRaiseExpression_AssociatedMessageQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='associated_message_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getAssociatedMessageQ();

	/**
	 * Sets the value of the '{@link Ada.RaiseExpression#getAssociatedMessageQ <em>Associated Message Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Associated Message Q</em>' containment reference.
	 * @see #getAssociatedMessageQ()
	 * @generated
	 */
	void setAssociatedMessageQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getRaiseExpression_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.RaiseExpression#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see Ada.AdaPackage#getRaiseExpression_Type()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='type' namespace='##targetNamespace'"
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link Ada.RaiseExpression#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

} // RaiseExpression
