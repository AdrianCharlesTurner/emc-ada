/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Null Procedure Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.NullProcedureDeclaration#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.NullProcedureDeclaration#getIsOverridingDeclarationQ <em>Is Overriding Declaration Q</em>}</li>
 *   <li>{@link Ada.NullProcedureDeclaration#getIsNotOverridingDeclarationQ <em>Is Not Overriding Declaration Q</em>}</li>
 *   <li>{@link Ada.NullProcedureDeclaration#getNamesQl <em>Names Ql</em>}</li>
 *   <li>{@link Ada.NullProcedureDeclaration#getParameterProfileQl <em>Parameter Profile Ql</em>}</li>
 *   <li>{@link Ada.NullProcedureDeclaration#getAspectSpecificationsQl <em>Aspect Specifications Ql</em>}</li>
 *   <li>{@link Ada.NullProcedureDeclaration#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getNullProcedureDeclaration()
 * @model extendedMetaData="name='Null_Procedure_Declaration' kind='elementOnly'"
 * @generated
 */
public interface NullProcedureDeclaration extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getNullProcedureDeclaration_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.NullProcedureDeclaration#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Is Overriding Declaration Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Overriding Declaration Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Overriding Declaration Q</em>' containment reference.
	 * @see #setIsOverridingDeclarationQ(IsOverridingDeclarationQType5)
	 * @see Ada.AdaPackage#getNullProcedureDeclaration_IsOverridingDeclarationQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='is_overriding_declaration_q' namespace='##targetNamespace'"
	 * @generated
	 */
	IsOverridingDeclarationQType5 getIsOverridingDeclarationQ();

	/**
	 * Sets the value of the '{@link Ada.NullProcedureDeclaration#getIsOverridingDeclarationQ <em>Is Overriding Declaration Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Overriding Declaration Q</em>' containment reference.
	 * @see #getIsOverridingDeclarationQ()
	 * @generated
	 */
	void setIsOverridingDeclarationQ(IsOverridingDeclarationQType5 value);

	/**
	 * Returns the value of the '<em><b>Is Not Overriding Declaration Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Not Overriding Declaration Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Not Overriding Declaration Q</em>' containment reference.
	 * @see #setIsNotOverridingDeclarationQ(IsNotOverridingDeclarationQType3)
	 * @see Ada.AdaPackage#getNullProcedureDeclaration_IsNotOverridingDeclarationQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='is_not_overriding_declaration_q' namespace='##targetNamespace'"
	 * @generated
	 */
	IsNotOverridingDeclarationQType3 getIsNotOverridingDeclarationQ();

	/**
	 * Sets the value of the '{@link Ada.NullProcedureDeclaration#getIsNotOverridingDeclarationQ <em>Is Not Overriding Declaration Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Not Overriding Declaration Q</em>' containment reference.
	 * @see #getIsNotOverridingDeclarationQ()
	 * @generated
	 */
	void setIsNotOverridingDeclarationQ(IsNotOverridingDeclarationQType3 value);

	/**
	 * Returns the value of the '<em><b>Names Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Names Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Names Ql</em>' containment reference.
	 * @see #setNamesQl(DefiningNameList)
	 * @see Ada.AdaPackage#getNullProcedureDeclaration_NamesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='names_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameList getNamesQl();

	/**
	 * Sets the value of the '{@link Ada.NullProcedureDeclaration#getNamesQl <em>Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Names Ql</em>' containment reference.
	 * @see #getNamesQl()
	 * @generated
	 */
	void setNamesQl(DefiningNameList value);

	/**
	 * Returns the value of the '<em><b>Parameter Profile Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Profile Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Profile Ql</em>' containment reference.
	 * @see #setParameterProfileQl(ParameterSpecificationList)
	 * @see Ada.AdaPackage#getNullProcedureDeclaration_ParameterProfileQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='parameter_profile_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ParameterSpecificationList getParameterProfileQl();

	/**
	 * Sets the value of the '{@link Ada.NullProcedureDeclaration#getParameterProfileQl <em>Parameter Profile Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Profile Ql</em>' containment reference.
	 * @see #getParameterProfileQl()
	 * @generated
	 */
	void setParameterProfileQl(ParameterSpecificationList value);

	/**
	 * Returns the value of the '<em><b>Aspect Specifications Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aspect Specifications Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aspect Specifications Ql</em>' containment reference.
	 * @see #setAspectSpecificationsQl(ElementList)
	 * @see Ada.AdaPackage#getNullProcedureDeclaration_AspectSpecificationsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='aspect_specifications_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementList getAspectSpecificationsQl();

	/**
	 * Sets the value of the '{@link Ada.NullProcedureDeclaration#getAspectSpecificationsQl <em>Aspect Specifications Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aspect Specifications Ql</em>' containment reference.
	 * @see #getAspectSpecificationsQl()
	 * @generated
	 */
	void setAspectSpecificationsQl(ElementList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getNullProcedureDeclaration_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.NullProcedureDeclaration#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // NullProcedureDeclaration
