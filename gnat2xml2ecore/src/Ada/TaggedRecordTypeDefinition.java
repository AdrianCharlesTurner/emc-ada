/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tagged Record Type Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.TaggedRecordTypeDefinition#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.TaggedRecordTypeDefinition#getHasAbstractQ <em>Has Abstract Q</em>}</li>
 *   <li>{@link Ada.TaggedRecordTypeDefinition#getHasLimitedQ <em>Has Limited Q</em>}</li>
 *   <li>{@link Ada.TaggedRecordTypeDefinition#getRecordDefinitionQ <em>Record Definition Q</em>}</li>
 *   <li>{@link Ada.TaggedRecordTypeDefinition#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getTaggedRecordTypeDefinition()
 * @model extendedMetaData="name='Tagged_Record_Type_Definition' kind='elementOnly'"
 * @generated
 */
public interface TaggedRecordTypeDefinition extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getTaggedRecordTypeDefinition_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.TaggedRecordTypeDefinition#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Has Abstract Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Abstract Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Abstract Q</em>' containment reference.
	 * @see #setHasAbstractQ(HasAbstractQType3)
	 * @see Ada.AdaPackage#getTaggedRecordTypeDefinition_HasAbstractQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='has_abstract_q' namespace='##targetNamespace'"
	 * @generated
	 */
	HasAbstractQType3 getHasAbstractQ();

	/**
	 * Sets the value of the '{@link Ada.TaggedRecordTypeDefinition#getHasAbstractQ <em>Has Abstract Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Abstract Q</em>' containment reference.
	 * @see #getHasAbstractQ()
	 * @generated
	 */
	void setHasAbstractQ(HasAbstractQType3 value);

	/**
	 * Returns the value of the '<em><b>Has Limited Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Limited Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Limited Q</em>' containment reference.
	 * @see #setHasLimitedQ(HasLimitedQType10)
	 * @see Ada.AdaPackage#getTaggedRecordTypeDefinition_HasLimitedQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='has_limited_q' namespace='##targetNamespace'"
	 * @generated
	 */
	HasLimitedQType10 getHasLimitedQ();

	/**
	 * Sets the value of the '{@link Ada.TaggedRecordTypeDefinition#getHasLimitedQ <em>Has Limited Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Limited Q</em>' containment reference.
	 * @see #getHasLimitedQ()
	 * @generated
	 */
	void setHasLimitedQ(HasLimitedQType10 value);

	/**
	 * Returns the value of the '<em><b>Record Definition Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Definition Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Definition Q</em>' containment reference.
	 * @see #setRecordDefinitionQ(DefinitionClass)
	 * @see Ada.AdaPackage#getTaggedRecordTypeDefinition_RecordDefinitionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='record_definition_q' namespace='##targetNamespace'"
	 * @generated
	 */
	DefinitionClass getRecordDefinitionQ();

	/**
	 * Sets the value of the '{@link Ada.TaggedRecordTypeDefinition#getRecordDefinitionQ <em>Record Definition Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Record Definition Q</em>' containment reference.
	 * @see #getRecordDefinitionQ()
	 * @generated
	 */
	void setRecordDefinitionQ(DefinitionClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getTaggedRecordTypeDefinition_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.TaggedRecordTypeDefinition#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // TaggedRecordTypeDefinition
