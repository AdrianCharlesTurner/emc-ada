/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Max Size In Storage Elements Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.MaxSizeInStorageElementsAttribute#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.MaxSizeInStorageElementsAttribute#getPrefixQ <em>Prefix Q</em>}</li>
 *   <li>{@link Ada.MaxSizeInStorageElementsAttribute#getAttributeDesignatorIdentifierQ <em>Attribute Designator Identifier Q</em>}</li>
 *   <li>{@link Ada.MaxSizeInStorageElementsAttribute#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.MaxSizeInStorageElementsAttribute#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getMaxSizeInStorageElementsAttribute()
 * @model extendedMetaData="name='Max_Size_In_Storage_Elements_Attribute' kind='elementOnly'"
 * @generated
 */
public interface MaxSizeInStorageElementsAttribute extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getMaxSizeInStorageElementsAttribute_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.MaxSizeInStorageElementsAttribute#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Prefix Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Prefix Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Prefix Q</em>' containment reference.
	 * @see #setPrefixQ(ExpressionClass)
	 * @see Ada.AdaPackage#getMaxSizeInStorageElementsAttribute_PrefixQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='prefix_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getPrefixQ();

	/**
	 * Sets the value of the '{@link Ada.MaxSizeInStorageElementsAttribute#getPrefixQ <em>Prefix Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Prefix Q</em>' containment reference.
	 * @see #getPrefixQ()
	 * @generated
	 */
	void setPrefixQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Attribute Designator Identifier Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute Designator Identifier Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Designator Identifier Q</em>' containment reference.
	 * @see #setAttributeDesignatorIdentifierQ(ExpressionClass)
	 * @see Ada.AdaPackage#getMaxSizeInStorageElementsAttribute_AttributeDesignatorIdentifierQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='attribute_designator_identifier_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getAttributeDesignatorIdentifierQ();

	/**
	 * Sets the value of the '{@link Ada.MaxSizeInStorageElementsAttribute#getAttributeDesignatorIdentifierQ <em>Attribute Designator Identifier Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Designator Identifier Q</em>' containment reference.
	 * @see #getAttributeDesignatorIdentifierQ()
	 * @generated
	 */
	void setAttributeDesignatorIdentifierQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getMaxSizeInStorageElementsAttribute_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.MaxSizeInStorageElementsAttribute#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see Ada.AdaPackage#getMaxSizeInStorageElementsAttribute_Type()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='type' namespace='##targetNamespace'"
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link Ada.MaxSizeInStorageElementsAttribute#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

} // MaxSizeInStorageElementsAttribute
