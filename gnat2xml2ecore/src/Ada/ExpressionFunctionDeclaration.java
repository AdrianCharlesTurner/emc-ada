/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression Function Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.ExpressionFunctionDeclaration#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.ExpressionFunctionDeclaration#getNamesQl <em>Names Ql</em>}</li>
 *   <li>{@link Ada.ExpressionFunctionDeclaration#getParameterProfileQl <em>Parameter Profile Ql</em>}</li>
 *   <li>{@link Ada.ExpressionFunctionDeclaration#getResultProfileQ <em>Result Profile Q</em>}</li>
 *   <li>{@link Ada.ExpressionFunctionDeclaration#getResultExpressionQ <em>Result Expression Q</em>}</li>
 *   <li>{@link Ada.ExpressionFunctionDeclaration#getAspectSpecificationsQl <em>Aspect Specifications Ql</em>}</li>
 *   <li>{@link Ada.ExpressionFunctionDeclaration#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getExpressionFunctionDeclaration()
 * @model extendedMetaData="name='Expression_Function_Declaration' kind='elementOnly'"
 * @generated
 */
public interface ExpressionFunctionDeclaration extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getExpressionFunctionDeclaration_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.ExpressionFunctionDeclaration#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Names Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Names Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Names Ql</em>' containment reference.
	 * @see #setNamesQl(DefiningNameList)
	 * @see Ada.AdaPackage#getExpressionFunctionDeclaration_NamesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='names_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameList getNamesQl();

	/**
	 * Sets the value of the '{@link Ada.ExpressionFunctionDeclaration#getNamesQl <em>Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Names Ql</em>' containment reference.
	 * @see #getNamesQl()
	 * @generated
	 */
	void setNamesQl(DefiningNameList value);

	/**
	 * Returns the value of the '<em><b>Parameter Profile Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Profile Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Profile Ql</em>' containment reference.
	 * @see #setParameterProfileQl(ParameterSpecificationList)
	 * @see Ada.AdaPackage#getExpressionFunctionDeclaration_ParameterProfileQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='parameter_profile_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ParameterSpecificationList getParameterProfileQl();

	/**
	 * Sets the value of the '{@link Ada.ExpressionFunctionDeclaration#getParameterProfileQl <em>Parameter Profile Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Profile Ql</em>' containment reference.
	 * @see #getParameterProfileQl()
	 * @generated
	 */
	void setParameterProfileQl(ParameterSpecificationList value);

	/**
	 * Returns the value of the '<em><b>Result Profile Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Result Profile Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Result Profile Q</em>' containment reference.
	 * @see #setResultProfileQ(ElementClass)
	 * @see Ada.AdaPackage#getExpressionFunctionDeclaration_ResultProfileQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='result_profile_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementClass getResultProfileQ();

	/**
	 * Sets the value of the '{@link Ada.ExpressionFunctionDeclaration#getResultProfileQ <em>Result Profile Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Result Profile Q</em>' containment reference.
	 * @see #getResultProfileQ()
	 * @generated
	 */
	void setResultProfileQ(ElementClass value);

	/**
	 * Returns the value of the '<em><b>Result Expression Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Result Expression Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Result Expression Q</em>' containment reference.
	 * @see #setResultExpressionQ(ExpressionClass)
	 * @see Ada.AdaPackage#getExpressionFunctionDeclaration_ResultExpressionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='result_expression_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getResultExpressionQ();

	/**
	 * Sets the value of the '{@link Ada.ExpressionFunctionDeclaration#getResultExpressionQ <em>Result Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Result Expression Q</em>' containment reference.
	 * @see #getResultExpressionQ()
	 * @generated
	 */
	void setResultExpressionQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Aspect Specifications Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aspect Specifications Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aspect Specifications Ql</em>' containment reference.
	 * @see #setAspectSpecificationsQl(ElementList)
	 * @see Ada.AdaPackage#getExpressionFunctionDeclaration_AspectSpecificationsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='aspect_specifications_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementList getAspectSpecificationsQl();

	/**
	 * Sets the value of the '{@link Ada.ExpressionFunctionDeclaration#getAspectSpecificationsQl <em>Aspect Specifications Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aspect Specifications Ql</em>' containment reference.
	 * @see #getAspectSpecificationsQl()
	 * @generated
	 */
	void setAspectSpecificationsQl(ElementList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getExpressionFunctionDeclaration_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.ExpressionFunctionDeclaration#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // ExpressionFunctionDeclaration
