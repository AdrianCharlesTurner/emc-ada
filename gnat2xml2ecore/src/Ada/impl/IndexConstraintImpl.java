/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.DiscreteRangeList;
import Ada.IndexConstraint;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Index Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.IndexConstraintImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.IndexConstraintImpl#getDiscreteRangesQl <em>Discrete Ranges Ql</em>}</li>
 *   <li>{@link Ada.impl.IndexConstraintImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IndexConstraintImpl extends MinimalEObjectImpl.Container implements IndexConstraint {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getDiscreteRangesQl() <em>Discrete Ranges Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiscreteRangesQl()
	 * @generated
	 * @ordered
	 */
	protected DiscreteRangeList discreteRangesQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IndexConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getIndexConstraint();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.INDEX_CONSTRAINT__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.INDEX_CONSTRAINT__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.INDEX_CONSTRAINT__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.INDEX_CONSTRAINT__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscreteRangeList getDiscreteRangesQl() {
		return discreteRangesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscreteRangesQl(DiscreteRangeList newDiscreteRangesQl, NotificationChain msgs) {
		DiscreteRangeList oldDiscreteRangesQl = discreteRangesQl;
		discreteRangesQl = newDiscreteRangesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.INDEX_CONSTRAINT__DISCRETE_RANGES_QL, oldDiscreteRangesQl, newDiscreteRangesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscreteRangesQl(DiscreteRangeList newDiscreteRangesQl) {
		if (newDiscreteRangesQl != discreteRangesQl) {
			NotificationChain msgs = null;
			if (discreteRangesQl != null)
				msgs = ((InternalEObject)discreteRangesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.INDEX_CONSTRAINT__DISCRETE_RANGES_QL, null, msgs);
			if (newDiscreteRangesQl != null)
				msgs = ((InternalEObject)newDiscreteRangesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.INDEX_CONSTRAINT__DISCRETE_RANGES_QL, null, msgs);
			msgs = basicSetDiscreteRangesQl(newDiscreteRangesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.INDEX_CONSTRAINT__DISCRETE_RANGES_QL, newDiscreteRangesQl, newDiscreteRangesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.INDEX_CONSTRAINT__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.INDEX_CONSTRAINT__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.INDEX_CONSTRAINT__DISCRETE_RANGES_QL:
				return basicSetDiscreteRangesQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.INDEX_CONSTRAINT__SLOC:
				return getSloc();
			case AdaPackage.INDEX_CONSTRAINT__DISCRETE_RANGES_QL:
				return getDiscreteRangesQl();
			case AdaPackage.INDEX_CONSTRAINT__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.INDEX_CONSTRAINT__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.INDEX_CONSTRAINT__DISCRETE_RANGES_QL:
				setDiscreteRangesQl((DiscreteRangeList)newValue);
				return;
			case AdaPackage.INDEX_CONSTRAINT__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.INDEX_CONSTRAINT__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.INDEX_CONSTRAINT__DISCRETE_RANGES_QL:
				setDiscreteRangesQl((DiscreteRangeList)null);
				return;
			case AdaPackage.INDEX_CONSTRAINT__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.INDEX_CONSTRAINT__SLOC:
				return sloc != null;
			case AdaPackage.INDEX_CONSTRAINT__DISCRETE_RANGES_QL:
				return discreteRangesQl != null;
			case AdaPackage.INDEX_CONSTRAINT__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //IndexConstraintImpl
