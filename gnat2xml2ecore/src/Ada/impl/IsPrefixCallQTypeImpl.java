/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.IsPrefixCall;
import Ada.IsPrefixCallQType;
import Ada.NotAnElement;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Is Prefix Call QType</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.IsPrefixCallQTypeImpl#getIsPrefixCall <em>Is Prefix Call</em>}</li>
 *   <li>{@link Ada.impl.IsPrefixCallQTypeImpl#getNotAnElement <em>Not An Element</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IsPrefixCallQTypeImpl extends MinimalEObjectImpl.Container implements IsPrefixCallQType {
	/**
	 * The cached value of the '{@link #getIsPrefixCall() <em>Is Prefix Call</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsPrefixCall()
	 * @generated
	 * @ordered
	 */
	protected IsPrefixCall isPrefixCall;

	/**
	 * The cached value of the '{@link #getNotAnElement() <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotAnElement()
	 * @generated
	 * @ordered
	 */
	protected NotAnElement notAnElement;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IsPrefixCallQTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getIsPrefixCallQType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsPrefixCall getIsPrefixCall() {
		return isPrefixCall;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIsPrefixCall(IsPrefixCall newIsPrefixCall, NotificationChain msgs) {
		IsPrefixCall oldIsPrefixCall = isPrefixCall;
		isPrefixCall = newIsPrefixCall;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.IS_PREFIX_CALL_QTYPE__IS_PREFIX_CALL, oldIsPrefixCall, newIsPrefixCall);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsPrefixCall(IsPrefixCall newIsPrefixCall) {
		if (newIsPrefixCall != isPrefixCall) {
			NotificationChain msgs = null;
			if (isPrefixCall != null)
				msgs = ((InternalEObject)isPrefixCall).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.IS_PREFIX_CALL_QTYPE__IS_PREFIX_CALL, null, msgs);
			if (newIsPrefixCall != null)
				msgs = ((InternalEObject)newIsPrefixCall).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.IS_PREFIX_CALL_QTYPE__IS_PREFIX_CALL, null, msgs);
			msgs = basicSetIsPrefixCall(newIsPrefixCall, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.IS_PREFIX_CALL_QTYPE__IS_PREFIX_CALL, newIsPrefixCall, newIsPrefixCall));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotAnElement getNotAnElement() {
		return notAnElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNotAnElement(NotAnElement newNotAnElement, NotificationChain msgs) {
		NotAnElement oldNotAnElement = notAnElement;
		notAnElement = newNotAnElement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.IS_PREFIX_CALL_QTYPE__NOT_AN_ELEMENT, oldNotAnElement, newNotAnElement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotAnElement(NotAnElement newNotAnElement) {
		if (newNotAnElement != notAnElement) {
			NotificationChain msgs = null;
			if (notAnElement != null)
				msgs = ((InternalEObject)notAnElement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.IS_PREFIX_CALL_QTYPE__NOT_AN_ELEMENT, null, msgs);
			if (newNotAnElement != null)
				msgs = ((InternalEObject)newNotAnElement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.IS_PREFIX_CALL_QTYPE__NOT_AN_ELEMENT, null, msgs);
			msgs = basicSetNotAnElement(newNotAnElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.IS_PREFIX_CALL_QTYPE__NOT_AN_ELEMENT, newNotAnElement, newNotAnElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.IS_PREFIX_CALL_QTYPE__IS_PREFIX_CALL:
				return basicSetIsPrefixCall(null, msgs);
			case AdaPackage.IS_PREFIX_CALL_QTYPE__NOT_AN_ELEMENT:
				return basicSetNotAnElement(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.IS_PREFIX_CALL_QTYPE__IS_PREFIX_CALL:
				return getIsPrefixCall();
			case AdaPackage.IS_PREFIX_CALL_QTYPE__NOT_AN_ELEMENT:
				return getNotAnElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.IS_PREFIX_CALL_QTYPE__IS_PREFIX_CALL:
				setIsPrefixCall((IsPrefixCall)newValue);
				return;
			case AdaPackage.IS_PREFIX_CALL_QTYPE__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.IS_PREFIX_CALL_QTYPE__IS_PREFIX_CALL:
				setIsPrefixCall((IsPrefixCall)null);
				return;
			case AdaPackage.IS_PREFIX_CALL_QTYPE__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.IS_PREFIX_CALL_QTYPE__IS_PREFIX_CALL:
				return isPrefixCall != null;
			case AdaPackage.IS_PREFIX_CALL_QTYPE__NOT_AN_ELEMENT:
				return notAnElement != null;
		}
		return super.eIsSet(featureID);
	}

} //IsPrefixCallQTypeImpl
