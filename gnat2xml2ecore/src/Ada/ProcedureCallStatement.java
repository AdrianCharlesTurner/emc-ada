/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Procedure Call Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.ProcedureCallStatement#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.ProcedureCallStatement#getLabelNamesQl <em>Label Names Ql</em>}</li>
 *   <li>{@link Ada.ProcedureCallStatement#getCalledNameQ <em>Called Name Q</em>}</li>
 *   <li>{@link Ada.ProcedureCallStatement#getCallStatementParametersQl <em>Call Statement Parameters Ql</em>}</li>
 *   <li>{@link Ada.ProcedureCallStatement#getIsPrefixNotationQ <em>Is Prefix Notation Q</em>}</li>
 *   <li>{@link Ada.ProcedureCallStatement#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getProcedureCallStatement()
 * @model extendedMetaData="name='Procedure_Call_Statement' kind='elementOnly'"
 * @generated
 */
public interface ProcedureCallStatement extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getProcedureCallStatement_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.ProcedureCallStatement#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Label Names Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label Names Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label Names Ql</em>' containment reference.
	 * @see #setLabelNamesQl(DefiningNameList)
	 * @see Ada.AdaPackage#getProcedureCallStatement_LabelNamesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='label_names_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameList getLabelNamesQl();

	/**
	 * Sets the value of the '{@link Ada.ProcedureCallStatement#getLabelNamesQl <em>Label Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label Names Ql</em>' containment reference.
	 * @see #getLabelNamesQl()
	 * @generated
	 */
	void setLabelNamesQl(DefiningNameList value);

	/**
	 * Returns the value of the '<em><b>Called Name Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Called Name Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Called Name Q</em>' containment reference.
	 * @see #setCalledNameQ(ExpressionClass)
	 * @see Ada.AdaPackage#getProcedureCallStatement_CalledNameQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='called_name_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getCalledNameQ();

	/**
	 * Sets the value of the '{@link Ada.ProcedureCallStatement#getCalledNameQ <em>Called Name Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Called Name Q</em>' containment reference.
	 * @see #getCalledNameQ()
	 * @generated
	 */
	void setCalledNameQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Call Statement Parameters Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Call Statement Parameters Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Call Statement Parameters Ql</em>' containment reference.
	 * @see #setCallStatementParametersQl(AssociationList)
	 * @see Ada.AdaPackage#getProcedureCallStatement_CallStatementParametersQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='call_statement_parameters_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	AssociationList getCallStatementParametersQl();

	/**
	 * Sets the value of the '{@link Ada.ProcedureCallStatement#getCallStatementParametersQl <em>Call Statement Parameters Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Call Statement Parameters Ql</em>' containment reference.
	 * @see #getCallStatementParametersQl()
	 * @generated
	 */
	void setCallStatementParametersQl(AssociationList value);

	/**
	 * Returns the value of the '<em><b>Is Prefix Notation Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Prefix Notation Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Prefix Notation Q</em>' containment reference.
	 * @see #setIsPrefixNotationQ(IsPrefixNotationQType1)
	 * @see Ada.AdaPackage#getProcedureCallStatement_IsPrefixNotationQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='is_prefix_notation_q' namespace='##targetNamespace'"
	 * @generated
	 */
	IsPrefixNotationQType1 getIsPrefixNotationQ();

	/**
	 * Sets the value of the '{@link Ada.ProcedureCallStatement#getIsPrefixNotationQ <em>Is Prefix Notation Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Prefix Notation Q</em>' containment reference.
	 * @see #getIsPrefixNotationQ()
	 * @generated
	 */
	void setIsPrefixNotationQ(IsPrefixNotationQType1 value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getProcedureCallStatement_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.ProcedureCallStatement#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // ProcedureCallStatement
