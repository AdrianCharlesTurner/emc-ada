/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.ElementList;
import Ada.ExpressionClass;
import Ada.NotInMembershipTest;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Not In Membership Test</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.NotInMembershipTestImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.NotInMembershipTestImpl#getMembershipTestExpressionQ <em>Membership Test Expression Q</em>}</li>
 *   <li>{@link Ada.impl.NotInMembershipTestImpl#getMembershipTestChoicesQl <em>Membership Test Choices Ql</em>}</li>
 *   <li>{@link Ada.impl.NotInMembershipTestImpl#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.impl.NotInMembershipTestImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NotInMembershipTestImpl extends MinimalEObjectImpl.Container implements NotInMembershipTest {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getMembershipTestExpressionQ() <em>Membership Test Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMembershipTestExpressionQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass membershipTestExpressionQ;

	/**
	 * The cached value of the '{@link #getMembershipTestChoicesQl() <em>Membership Test Choices Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMembershipTestChoicesQl()
	 * @generated
	 * @ordered
	 */
	protected ElementList membershipTestChoicesQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NotInMembershipTestImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getNotInMembershipTest();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NOT_IN_MEMBERSHIP_TEST__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NOT_IN_MEMBERSHIP_TEST__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NOT_IN_MEMBERSHIP_TEST__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NOT_IN_MEMBERSHIP_TEST__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getMembershipTestExpressionQ() {
		return membershipTestExpressionQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMembershipTestExpressionQ(ExpressionClass newMembershipTestExpressionQ, NotificationChain msgs) {
		ExpressionClass oldMembershipTestExpressionQ = membershipTestExpressionQ;
		membershipTestExpressionQ = newMembershipTestExpressionQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NOT_IN_MEMBERSHIP_TEST__MEMBERSHIP_TEST_EXPRESSION_Q, oldMembershipTestExpressionQ, newMembershipTestExpressionQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMembershipTestExpressionQ(ExpressionClass newMembershipTestExpressionQ) {
		if (newMembershipTestExpressionQ != membershipTestExpressionQ) {
			NotificationChain msgs = null;
			if (membershipTestExpressionQ != null)
				msgs = ((InternalEObject)membershipTestExpressionQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NOT_IN_MEMBERSHIP_TEST__MEMBERSHIP_TEST_EXPRESSION_Q, null, msgs);
			if (newMembershipTestExpressionQ != null)
				msgs = ((InternalEObject)newMembershipTestExpressionQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NOT_IN_MEMBERSHIP_TEST__MEMBERSHIP_TEST_EXPRESSION_Q, null, msgs);
			msgs = basicSetMembershipTestExpressionQ(newMembershipTestExpressionQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NOT_IN_MEMBERSHIP_TEST__MEMBERSHIP_TEST_EXPRESSION_Q, newMembershipTestExpressionQ, newMembershipTestExpressionQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementList getMembershipTestChoicesQl() {
		return membershipTestChoicesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMembershipTestChoicesQl(ElementList newMembershipTestChoicesQl, NotificationChain msgs) {
		ElementList oldMembershipTestChoicesQl = membershipTestChoicesQl;
		membershipTestChoicesQl = newMembershipTestChoicesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NOT_IN_MEMBERSHIP_TEST__MEMBERSHIP_TEST_CHOICES_QL, oldMembershipTestChoicesQl, newMembershipTestChoicesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMembershipTestChoicesQl(ElementList newMembershipTestChoicesQl) {
		if (newMembershipTestChoicesQl != membershipTestChoicesQl) {
			NotificationChain msgs = null;
			if (membershipTestChoicesQl != null)
				msgs = ((InternalEObject)membershipTestChoicesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NOT_IN_MEMBERSHIP_TEST__MEMBERSHIP_TEST_CHOICES_QL, null, msgs);
			if (newMembershipTestChoicesQl != null)
				msgs = ((InternalEObject)newMembershipTestChoicesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NOT_IN_MEMBERSHIP_TEST__MEMBERSHIP_TEST_CHOICES_QL, null, msgs);
			msgs = basicSetMembershipTestChoicesQl(newMembershipTestChoicesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NOT_IN_MEMBERSHIP_TEST__MEMBERSHIP_TEST_CHOICES_QL, newMembershipTestChoicesQl, newMembershipTestChoicesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NOT_IN_MEMBERSHIP_TEST__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NOT_IN_MEMBERSHIP_TEST__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.NOT_IN_MEMBERSHIP_TEST__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.NOT_IN_MEMBERSHIP_TEST__MEMBERSHIP_TEST_EXPRESSION_Q:
				return basicSetMembershipTestExpressionQ(null, msgs);
			case AdaPackage.NOT_IN_MEMBERSHIP_TEST__MEMBERSHIP_TEST_CHOICES_QL:
				return basicSetMembershipTestChoicesQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.NOT_IN_MEMBERSHIP_TEST__SLOC:
				return getSloc();
			case AdaPackage.NOT_IN_MEMBERSHIP_TEST__MEMBERSHIP_TEST_EXPRESSION_Q:
				return getMembershipTestExpressionQ();
			case AdaPackage.NOT_IN_MEMBERSHIP_TEST__MEMBERSHIP_TEST_CHOICES_QL:
				return getMembershipTestChoicesQl();
			case AdaPackage.NOT_IN_MEMBERSHIP_TEST__CHECKS:
				return getChecks();
			case AdaPackage.NOT_IN_MEMBERSHIP_TEST__TYPE:
				return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.NOT_IN_MEMBERSHIP_TEST__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.NOT_IN_MEMBERSHIP_TEST__MEMBERSHIP_TEST_EXPRESSION_Q:
				setMembershipTestExpressionQ((ExpressionClass)newValue);
				return;
			case AdaPackage.NOT_IN_MEMBERSHIP_TEST__MEMBERSHIP_TEST_CHOICES_QL:
				setMembershipTestChoicesQl((ElementList)newValue);
				return;
			case AdaPackage.NOT_IN_MEMBERSHIP_TEST__CHECKS:
				setChecks((String)newValue);
				return;
			case AdaPackage.NOT_IN_MEMBERSHIP_TEST__TYPE:
				setType((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.NOT_IN_MEMBERSHIP_TEST__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.NOT_IN_MEMBERSHIP_TEST__MEMBERSHIP_TEST_EXPRESSION_Q:
				setMembershipTestExpressionQ((ExpressionClass)null);
				return;
			case AdaPackage.NOT_IN_MEMBERSHIP_TEST__MEMBERSHIP_TEST_CHOICES_QL:
				setMembershipTestChoicesQl((ElementList)null);
				return;
			case AdaPackage.NOT_IN_MEMBERSHIP_TEST__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
			case AdaPackage.NOT_IN_MEMBERSHIP_TEST__TYPE:
				setType(TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.NOT_IN_MEMBERSHIP_TEST__SLOC:
				return sloc != null;
			case AdaPackage.NOT_IN_MEMBERSHIP_TEST__MEMBERSHIP_TEST_EXPRESSION_Q:
				return membershipTestExpressionQ != null;
			case AdaPackage.NOT_IN_MEMBERSHIP_TEST__MEMBERSHIP_TEST_CHOICES_QL:
				return membershipTestChoicesQl != null;
			case AdaPackage.NOT_IN_MEMBERSHIP_TEST__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
			case AdaPackage.NOT_IN_MEMBERSHIP_TEST__TYPE:
				return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(", type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //NotInMembershipTestImpl
