/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generic Package Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.GenericPackageDeclaration#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.GenericPackageDeclaration#getGenericFormalPartQl <em>Generic Formal Part Ql</em>}</li>
 *   <li>{@link Ada.GenericPackageDeclaration#getNamesQl <em>Names Ql</em>}</li>
 *   <li>{@link Ada.GenericPackageDeclaration#getAspectSpecificationsQl <em>Aspect Specifications Ql</em>}</li>
 *   <li>{@link Ada.GenericPackageDeclaration#getVisiblePartDeclarativeItemsQl <em>Visible Part Declarative Items Ql</em>}</li>
 *   <li>{@link Ada.GenericPackageDeclaration#getPrivatePartDeclarativeItemsQl <em>Private Part Declarative Items Ql</em>}</li>
 *   <li>{@link Ada.GenericPackageDeclaration#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getGenericPackageDeclaration()
 * @model extendedMetaData="name='Generic_Package_Declaration' kind='elementOnly'"
 * @generated
 */
public interface GenericPackageDeclaration extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getGenericPackageDeclaration_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.GenericPackageDeclaration#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Generic Formal Part Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Formal Part Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Formal Part Ql</em>' containment reference.
	 * @see #setGenericFormalPartQl(ElementList)
	 * @see Ada.AdaPackage#getGenericPackageDeclaration_GenericFormalPartQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='generic_formal_part_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementList getGenericFormalPartQl();

	/**
	 * Sets the value of the '{@link Ada.GenericPackageDeclaration#getGenericFormalPartQl <em>Generic Formal Part Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generic Formal Part Ql</em>' containment reference.
	 * @see #getGenericFormalPartQl()
	 * @generated
	 */
	void setGenericFormalPartQl(ElementList value);

	/**
	 * Returns the value of the '<em><b>Names Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Names Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Names Ql</em>' containment reference.
	 * @see #setNamesQl(DefiningNameList)
	 * @see Ada.AdaPackage#getGenericPackageDeclaration_NamesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='names_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameList getNamesQl();

	/**
	 * Sets the value of the '{@link Ada.GenericPackageDeclaration#getNamesQl <em>Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Names Ql</em>' containment reference.
	 * @see #getNamesQl()
	 * @generated
	 */
	void setNamesQl(DefiningNameList value);

	/**
	 * Returns the value of the '<em><b>Aspect Specifications Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aspect Specifications Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aspect Specifications Ql</em>' containment reference.
	 * @see #setAspectSpecificationsQl(ElementList)
	 * @see Ada.AdaPackage#getGenericPackageDeclaration_AspectSpecificationsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='aspect_specifications_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementList getAspectSpecificationsQl();

	/**
	 * Sets the value of the '{@link Ada.GenericPackageDeclaration#getAspectSpecificationsQl <em>Aspect Specifications Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aspect Specifications Ql</em>' containment reference.
	 * @see #getAspectSpecificationsQl()
	 * @generated
	 */
	void setAspectSpecificationsQl(ElementList value);

	/**
	 * Returns the value of the '<em><b>Visible Part Declarative Items Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visible Part Declarative Items Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visible Part Declarative Items Ql</em>' containment reference.
	 * @see #setVisiblePartDeclarativeItemsQl(DeclarativeItemList)
	 * @see Ada.AdaPackage#getGenericPackageDeclaration_VisiblePartDeclarativeItemsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='visible_part_declarative_items_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DeclarativeItemList getVisiblePartDeclarativeItemsQl();

	/**
	 * Sets the value of the '{@link Ada.GenericPackageDeclaration#getVisiblePartDeclarativeItemsQl <em>Visible Part Declarative Items Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Visible Part Declarative Items Ql</em>' containment reference.
	 * @see #getVisiblePartDeclarativeItemsQl()
	 * @generated
	 */
	void setVisiblePartDeclarativeItemsQl(DeclarativeItemList value);

	/**
	 * Returns the value of the '<em><b>Private Part Declarative Items Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Private Part Declarative Items Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Private Part Declarative Items Ql</em>' containment reference.
	 * @see #setPrivatePartDeclarativeItemsQl(DeclarativeItemList)
	 * @see Ada.AdaPackage#getGenericPackageDeclaration_PrivatePartDeclarativeItemsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='private_part_declarative_items_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DeclarativeItemList getPrivatePartDeclarativeItemsQl();

	/**
	 * Sets the value of the '{@link Ada.GenericPackageDeclaration#getPrivatePartDeclarativeItemsQl <em>Private Part Declarative Items Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Private Part Declarative Items Ql</em>' containment reference.
	 * @see #getPrivatePartDeclarativeItemsQl()
	 * @generated
	 */
	void setPrivatePartDeclarativeItemsQl(DeclarativeItemList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getGenericPackageDeclaration_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.GenericPackageDeclaration#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // GenericPackageDeclaration
