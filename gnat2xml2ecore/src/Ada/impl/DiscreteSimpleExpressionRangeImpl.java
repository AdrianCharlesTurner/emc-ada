/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.DiscreteSimpleExpressionRange;
import Ada.ExpressionClass;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Discrete Simple Expression Range</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.DiscreteSimpleExpressionRangeImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.DiscreteSimpleExpressionRangeImpl#getLowerBoundQ <em>Lower Bound Q</em>}</li>
 *   <li>{@link Ada.impl.DiscreteSimpleExpressionRangeImpl#getUpperBoundQ <em>Upper Bound Q</em>}</li>
 *   <li>{@link Ada.impl.DiscreteSimpleExpressionRangeImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DiscreteSimpleExpressionRangeImpl extends MinimalEObjectImpl.Container implements DiscreteSimpleExpressionRange {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getLowerBoundQ() <em>Lower Bound Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLowerBoundQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass lowerBoundQ;

	/**
	 * The cached value of the '{@link #getUpperBoundQ() <em>Upper Bound Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpperBoundQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass upperBoundQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DiscreteSimpleExpressionRangeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getDiscreteSimpleExpressionRange();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getLowerBoundQ() {
		return lowerBoundQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLowerBoundQ(ExpressionClass newLowerBoundQ, NotificationChain msgs) {
		ExpressionClass oldLowerBoundQ = lowerBoundQ;
		lowerBoundQ = newLowerBoundQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__LOWER_BOUND_Q, oldLowerBoundQ, newLowerBoundQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLowerBoundQ(ExpressionClass newLowerBoundQ) {
		if (newLowerBoundQ != lowerBoundQ) {
			NotificationChain msgs = null;
			if (lowerBoundQ != null)
				msgs = ((InternalEObject)lowerBoundQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__LOWER_BOUND_Q, null, msgs);
			if (newLowerBoundQ != null)
				msgs = ((InternalEObject)newLowerBoundQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__LOWER_BOUND_Q, null, msgs);
			msgs = basicSetLowerBoundQ(newLowerBoundQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__LOWER_BOUND_Q, newLowerBoundQ, newLowerBoundQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getUpperBoundQ() {
		return upperBoundQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUpperBoundQ(ExpressionClass newUpperBoundQ, NotificationChain msgs) {
		ExpressionClass oldUpperBoundQ = upperBoundQ;
		upperBoundQ = newUpperBoundQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__UPPER_BOUND_Q, oldUpperBoundQ, newUpperBoundQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUpperBoundQ(ExpressionClass newUpperBoundQ) {
		if (newUpperBoundQ != upperBoundQ) {
			NotificationChain msgs = null;
			if (upperBoundQ != null)
				msgs = ((InternalEObject)upperBoundQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__UPPER_BOUND_Q, null, msgs);
			if (newUpperBoundQ != null)
				msgs = ((InternalEObject)newUpperBoundQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__UPPER_BOUND_Q, null, msgs);
			msgs = basicSetUpperBoundQ(newUpperBoundQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__UPPER_BOUND_Q, newUpperBoundQ, newUpperBoundQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__LOWER_BOUND_Q:
				return basicSetLowerBoundQ(null, msgs);
			case AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__UPPER_BOUND_Q:
				return basicSetUpperBoundQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__SLOC:
				return getSloc();
			case AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__LOWER_BOUND_Q:
				return getLowerBoundQ();
			case AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__UPPER_BOUND_Q:
				return getUpperBoundQ();
			case AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__LOWER_BOUND_Q:
				setLowerBoundQ((ExpressionClass)newValue);
				return;
			case AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__UPPER_BOUND_Q:
				setUpperBoundQ((ExpressionClass)newValue);
				return;
			case AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__LOWER_BOUND_Q:
				setLowerBoundQ((ExpressionClass)null);
				return;
			case AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__UPPER_BOUND_Q:
				setUpperBoundQ((ExpressionClass)null);
				return;
			case AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__SLOC:
				return sloc != null;
			case AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__LOWER_BOUND_Q:
				return lowerBoundQ != null;
			case AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__UPPER_BOUND_Q:
				return upperBoundQ != null;
			case AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //DiscreteSimpleExpressionRangeImpl
