/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.IsNotOverridingDeclarationQType7;
import Ada.NotAnElement;
import Ada.NotOverriding;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Is Not Overriding Declaration QType7</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.IsNotOverridingDeclarationQType7Impl#getNotOverriding <em>Not Overriding</em>}</li>
 *   <li>{@link Ada.impl.IsNotOverridingDeclarationQType7Impl#getNotAnElement <em>Not An Element</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IsNotOverridingDeclarationQType7Impl extends MinimalEObjectImpl.Container implements IsNotOverridingDeclarationQType7 {
	/**
	 * The cached value of the '{@link #getNotOverriding() <em>Not Overriding</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotOverriding()
	 * @generated
	 * @ordered
	 */
	protected NotOverriding notOverriding;

	/**
	 * The cached value of the '{@link #getNotAnElement() <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotAnElement()
	 * @generated
	 * @ordered
	 */
	protected NotAnElement notAnElement;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IsNotOverridingDeclarationQType7Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getIsNotOverridingDeclarationQType7();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotOverriding getNotOverriding() {
		return notOverriding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNotOverriding(NotOverriding newNotOverriding, NotificationChain msgs) {
		NotOverriding oldNotOverriding = notOverriding;
		notOverriding = newNotOverriding;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE7__NOT_OVERRIDING, oldNotOverriding, newNotOverriding);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotOverriding(NotOverriding newNotOverriding) {
		if (newNotOverriding != notOverriding) {
			NotificationChain msgs = null;
			if (notOverriding != null)
				msgs = ((InternalEObject)notOverriding).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE7__NOT_OVERRIDING, null, msgs);
			if (newNotOverriding != null)
				msgs = ((InternalEObject)newNotOverriding).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE7__NOT_OVERRIDING, null, msgs);
			msgs = basicSetNotOverriding(newNotOverriding, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE7__NOT_OVERRIDING, newNotOverriding, newNotOverriding));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotAnElement getNotAnElement() {
		return notAnElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNotAnElement(NotAnElement newNotAnElement, NotificationChain msgs) {
		NotAnElement oldNotAnElement = notAnElement;
		notAnElement = newNotAnElement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE7__NOT_AN_ELEMENT, oldNotAnElement, newNotAnElement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotAnElement(NotAnElement newNotAnElement) {
		if (newNotAnElement != notAnElement) {
			NotificationChain msgs = null;
			if (notAnElement != null)
				msgs = ((InternalEObject)notAnElement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE7__NOT_AN_ELEMENT, null, msgs);
			if (newNotAnElement != null)
				msgs = ((InternalEObject)newNotAnElement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE7__NOT_AN_ELEMENT, null, msgs);
			msgs = basicSetNotAnElement(newNotAnElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE7__NOT_AN_ELEMENT, newNotAnElement, newNotAnElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE7__NOT_OVERRIDING:
				return basicSetNotOverriding(null, msgs);
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE7__NOT_AN_ELEMENT:
				return basicSetNotAnElement(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE7__NOT_OVERRIDING:
				return getNotOverriding();
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE7__NOT_AN_ELEMENT:
				return getNotAnElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE7__NOT_OVERRIDING:
				setNotOverriding((NotOverriding)newValue);
				return;
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE7__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE7__NOT_OVERRIDING:
				setNotOverriding((NotOverriding)null);
				return;
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE7__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE7__NOT_OVERRIDING:
				return notOverriding != null;
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE7__NOT_AN_ELEMENT:
				return notAnElement != null;
		}
		return super.eIsSet(featureID);
	}

} //IsNotOverridingDeclarationQType7Impl
