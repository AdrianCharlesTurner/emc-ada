/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assignment Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.AssignmentStatement#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.AssignmentStatement#getLabelNamesQl <em>Label Names Ql</em>}</li>
 *   <li>{@link Ada.AssignmentStatement#getAssignmentVariableNameQ <em>Assignment Variable Name Q</em>}</li>
 *   <li>{@link Ada.AssignmentStatement#getAssignmentExpressionQ <em>Assignment Expression Q</em>}</li>
 *   <li>{@link Ada.AssignmentStatement#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getAssignmentStatement()
 * @model extendedMetaData="name='Assignment_Statement' kind='elementOnly'"
 * @generated
 */
public interface AssignmentStatement extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getAssignmentStatement_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.AssignmentStatement#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Label Names Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label Names Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label Names Ql</em>' containment reference.
	 * @see #setLabelNamesQl(DefiningNameList)
	 * @see Ada.AdaPackage#getAssignmentStatement_LabelNamesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='label_names_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameList getLabelNamesQl();

	/**
	 * Sets the value of the '{@link Ada.AssignmentStatement#getLabelNamesQl <em>Label Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label Names Ql</em>' containment reference.
	 * @see #getLabelNamesQl()
	 * @generated
	 */
	void setLabelNamesQl(DefiningNameList value);

	/**
	 * Returns the value of the '<em><b>Assignment Variable Name Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assignment Variable Name Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assignment Variable Name Q</em>' containment reference.
	 * @see #setAssignmentVariableNameQ(ExpressionClass)
	 * @see Ada.AdaPackage#getAssignmentStatement_AssignmentVariableNameQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='assignment_variable_name_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getAssignmentVariableNameQ();

	/**
	 * Sets the value of the '{@link Ada.AssignmentStatement#getAssignmentVariableNameQ <em>Assignment Variable Name Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assignment Variable Name Q</em>' containment reference.
	 * @see #getAssignmentVariableNameQ()
	 * @generated
	 */
	void setAssignmentVariableNameQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Assignment Expression Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assignment Expression Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assignment Expression Q</em>' containment reference.
	 * @see #setAssignmentExpressionQ(ExpressionClass)
	 * @see Ada.AdaPackage#getAssignmentStatement_AssignmentExpressionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='assignment_expression_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getAssignmentExpressionQ();

	/**
	 * Sets the value of the '{@link Ada.AssignmentStatement#getAssignmentExpressionQ <em>Assignment Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assignment Expression Q</em>' containment reference.
	 * @see #getAssignmentExpressionQ()
	 * @generated
	 */
	void setAssignmentExpressionQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getAssignmentStatement_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.AssignmentStatement#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // AssignmentStatement
