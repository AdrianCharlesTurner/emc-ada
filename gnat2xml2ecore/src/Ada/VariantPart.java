/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variant Part</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.VariantPart#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.VariantPart#getDiscriminantDirectNameQ <em>Discriminant Direct Name Q</em>}</li>
 *   <li>{@link Ada.VariantPart#getVariantsQl <em>Variants Ql</em>}</li>
 *   <li>{@link Ada.VariantPart#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getVariantPart()
 * @model extendedMetaData="name='Variant_Part' kind='elementOnly'"
 * @generated
 */
public interface VariantPart extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getVariantPart_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.VariantPart#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Discriminant Direct Name Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discriminant Direct Name Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discriminant Direct Name Q</em>' containment reference.
	 * @see #setDiscriminantDirectNameQ(NameClass)
	 * @see Ada.AdaPackage#getVariantPart_DiscriminantDirectNameQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='discriminant_direct_name_q' namespace='##targetNamespace'"
	 * @generated
	 */
	NameClass getDiscriminantDirectNameQ();

	/**
	 * Sets the value of the '{@link Ada.VariantPart#getDiscriminantDirectNameQ <em>Discriminant Direct Name Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discriminant Direct Name Q</em>' containment reference.
	 * @see #getDiscriminantDirectNameQ()
	 * @generated
	 */
	void setDiscriminantDirectNameQ(NameClass value);

	/**
	 * Returns the value of the '<em><b>Variants Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variants Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variants Ql</em>' containment reference.
	 * @see #setVariantsQl(VariantList)
	 * @see Ada.AdaPackage#getVariantPart_VariantsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='variants_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	VariantList getVariantsQl();

	/**
	 * Sets the value of the '{@link Ada.VariantPart#getVariantsQl <em>Variants Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variants Ql</em>' containment reference.
	 * @see #getVariantsQl()
	 * @generated
	 */
	void setVariantsQl(VariantList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getVariantPart_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.VariantPart#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // VariantPart
