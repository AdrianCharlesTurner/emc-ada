/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>If Path</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.IfPath#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.IfPath#getConditionExpressionQ <em>Condition Expression Q</em>}</li>
 *   <li>{@link Ada.IfPath#getSequenceOfStatementsQl <em>Sequence Of Statements Ql</em>}</li>
 *   <li>{@link Ada.IfPath#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getIfPath()
 * @model extendedMetaData="name='If_Path' kind='elementOnly'"
 * @generated
 */
public interface IfPath extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getIfPath_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.IfPath#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Condition Expression Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition Expression Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition Expression Q</em>' containment reference.
	 * @see #setConditionExpressionQ(ExpressionClass)
	 * @see Ada.AdaPackage#getIfPath_ConditionExpressionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='condition_expression_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getConditionExpressionQ();

	/**
	 * Sets the value of the '{@link Ada.IfPath#getConditionExpressionQ <em>Condition Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition Expression Q</em>' containment reference.
	 * @see #getConditionExpressionQ()
	 * @generated
	 */
	void setConditionExpressionQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Sequence Of Statements Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sequence Of Statements Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sequence Of Statements Ql</em>' containment reference.
	 * @see #setSequenceOfStatementsQl(StatementList)
	 * @see Ada.AdaPackage#getIfPath_SequenceOfStatementsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sequence_of_statements_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	StatementList getSequenceOfStatementsQl();

	/**
	 * Sets the value of the '{@link Ada.IfPath#getSequenceOfStatementsQl <em>Sequence Of Statements Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sequence Of Statements Ql</em>' containment reference.
	 * @see #getSequenceOfStatementsQl()
	 * @generated
	 */
	void setSequenceOfStatementsQl(StatementList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getIfPath_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.IfPath#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // IfPath
