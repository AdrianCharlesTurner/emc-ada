/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.ExpressionClass;
import Ada.PriorityAttribute;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Priority Attribute</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.PriorityAttributeImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.PriorityAttributeImpl#getPrefixQ <em>Prefix Q</em>}</li>
 *   <li>{@link Ada.impl.PriorityAttributeImpl#getAttributeDesignatorIdentifierQ <em>Attribute Designator Identifier Q</em>}</li>
 *   <li>{@link Ada.impl.PriorityAttributeImpl#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.impl.PriorityAttributeImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PriorityAttributeImpl extends MinimalEObjectImpl.Container implements PriorityAttribute {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getPrefixQ() <em>Prefix Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrefixQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass prefixQ;

	/**
	 * The cached value of the '{@link #getAttributeDesignatorIdentifierQ() <em>Attribute Designator Identifier Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributeDesignatorIdentifierQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass attributeDesignatorIdentifierQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PriorityAttributeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getPriorityAttribute();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.PRIORITY_ATTRIBUTE__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIORITY_ATTRIBUTE__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIORITY_ATTRIBUTE__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PRIORITY_ATTRIBUTE__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getPrefixQ() {
		return prefixQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrefixQ(ExpressionClass newPrefixQ, NotificationChain msgs) {
		ExpressionClass oldPrefixQ = prefixQ;
		prefixQ = newPrefixQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.PRIORITY_ATTRIBUTE__PREFIX_Q, oldPrefixQ, newPrefixQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrefixQ(ExpressionClass newPrefixQ) {
		if (newPrefixQ != prefixQ) {
			NotificationChain msgs = null;
			if (prefixQ != null)
				msgs = ((InternalEObject)prefixQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIORITY_ATTRIBUTE__PREFIX_Q, null, msgs);
			if (newPrefixQ != null)
				msgs = ((InternalEObject)newPrefixQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIORITY_ATTRIBUTE__PREFIX_Q, null, msgs);
			msgs = basicSetPrefixQ(newPrefixQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PRIORITY_ATTRIBUTE__PREFIX_Q, newPrefixQ, newPrefixQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getAttributeDesignatorIdentifierQ() {
		return attributeDesignatorIdentifierQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAttributeDesignatorIdentifierQ(ExpressionClass newAttributeDesignatorIdentifierQ, NotificationChain msgs) {
		ExpressionClass oldAttributeDesignatorIdentifierQ = attributeDesignatorIdentifierQ;
		attributeDesignatorIdentifierQ = newAttributeDesignatorIdentifierQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.PRIORITY_ATTRIBUTE__ATTRIBUTE_DESIGNATOR_IDENTIFIER_Q, oldAttributeDesignatorIdentifierQ, newAttributeDesignatorIdentifierQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttributeDesignatorIdentifierQ(ExpressionClass newAttributeDesignatorIdentifierQ) {
		if (newAttributeDesignatorIdentifierQ != attributeDesignatorIdentifierQ) {
			NotificationChain msgs = null;
			if (attributeDesignatorIdentifierQ != null)
				msgs = ((InternalEObject)attributeDesignatorIdentifierQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIORITY_ATTRIBUTE__ATTRIBUTE_DESIGNATOR_IDENTIFIER_Q, null, msgs);
			if (newAttributeDesignatorIdentifierQ != null)
				msgs = ((InternalEObject)newAttributeDesignatorIdentifierQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIORITY_ATTRIBUTE__ATTRIBUTE_DESIGNATOR_IDENTIFIER_Q, null, msgs);
			msgs = basicSetAttributeDesignatorIdentifierQ(newAttributeDesignatorIdentifierQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PRIORITY_ATTRIBUTE__ATTRIBUTE_DESIGNATOR_IDENTIFIER_Q, newAttributeDesignatorIdentifierQ, newAttributeDesignatorIdentifierQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PRIORITY_ATTRIBUTE__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PRIORITY_ATTRIBUTE__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.PRIORITY_ATTRIBUTE__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.PRIORITY_ATTRIBUTE__PREFIX_Q:
				return basicSetPrefixQ(null, msgs);
			case AdaPackage.PRIORITY_ATTRIBUTE__ATTRIBUTE_DESIGNATOR_IDENTIFIER_Q:
				return basicSetAttributeDesignatorIdentifierQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.PRIORITY_ATTRIBUTE__SLOC:
				return getSloc();
			case AdaPackage.PRIORITY_ATTRIBUTE__PREFIX_Q:
				return getPrefixQ();
			case AdaPackage.PRIORITY_ATTRIBUTE__ATTRIBUTE_DESIGNATOR_IDENTIFIER_Q:
				return getAttributeDesignatorIdentifierQ();
			case AdaPackage.PRIORITY_ATTRIBUTE__CHECKS:
				return getChecks();
			case AdaPackage.PRIORITY_ATTRIBUTE__TYPE:
				return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.PRIORITY_ATTRIBUTE__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.PRIORITY_ATTRIBUTE__PREFIX_Q:
				setPrefixQ((ExpressionClass)newValue);
				return;
			case AdaPackage.PRIORITY_ATTRIBUTE__ATTRIBUTE_DESIGNATOR_IDENTIFIER_Q:
				setAttributeDesignatorIdentifierQ((ExpressionClass)newValue);
				return;
			case AdaPackage.PRIORITY_ATTRIBUTE__CHECKS:
				setChecks((String)newValue);
				return;
			case AdaPackage.PRIORITY_ATTRIBUTE__TYPE:
				setType((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.PRIORITY_ATTRIBUTE__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.PRIORITY_ATTRIBUTE__PREFIX_Q:
				setPrefixQ((ExpressionClass)null);
				return;
			case AdaPackage.PRIORITY_ATTRIBUTE__ATTRIBUTE_DESIGNATOR_IDENTIFIER_Q:
				setAttributeDesignatorIdentifierQ((ExpressionClass)null);
				return;
			case AdaPackage.PRIORITY_ATTRIBUTE__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
			case AdaPackage.PRIORITY_ATTRIBUTE__TYPE:
				setType(TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.PRIORITY_ATTRIBUTE__SLOC:
				return sloc != null;
			case AdaPackage.PRIORITY_ATTRIBUTE__PREFIX_Q:
				return prefixQ != null;
			case AdaPackage.PRIORITY_ATTRIBUTE__ATTRIBUTE_DESIGNATOR_IDENTIFIER_Q:
				return attributeDesignatorIdentifierQ != null;
			case AdaPackage.PRIORITY_ATTRIBUTE__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
			case AdaPackage.PRIORITY_ATTRIBUTE__TYPE:
				return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(", type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //PriorityAttributeImpl
