/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.DeferredConstantDeclaration;
import Ada.DefiningNameList;
import Ada.DefinitionClass;
import Ada.ElementList;
import Ada.HasAliasedQType4;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Deferred Constant Declaration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.DeferredConstantDeclarationImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.DeferredConstantDeclarationImpl#getNamesQl <em>Names Ql</em>}</li>
 *   <li>{@link Ada.impl.DeferredConstantDeclarationImpl#getHasAliasedQ <em>Has Aliased Q</em>}</li>
 *   <li>{@link Ada.impl.DeferredConstantDeclarationImpl#getObjectDeclarationViewQ <em>Object Declaration View Q</em>}</li>
 *   <li>{@link Ada.impl.DeferredConstantDeclarationImpl#getAspectSpecificationsQl <em>Aspect Specifications Ql</em>}</li>
 *   <li>{@link Ada.impl.DeferredConstantDeclarationImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeferredConstantDeclarationImpl extends MinimalEObjectImpl.Container implements DeferredConstantDeclaration {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getNamesQl() <em>Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamesQl()
	 * @generated
	 * @ordered
	 */
	protected DefiningNameList namesQl;

	/**
	 * The cached value of the '{@link #getHasAliasedQ() <em>Has Aliased Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasAliasedQ()
	 * @generated
	 * @ordered
	 */
	protected HasAliasedQType4 hasAliasedQ;

	/**
	 * The cached value of the '{@link #getObjectDeclarationViewQ() <em>Object Declaration View Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectDeclarationViewQ()
	 * @generated
	 * @ordered
	 */
	protected DefinitionClass objectDeclarationViewQ;

	/**
	 * The cached value of the '{@link #getAspectSpecificationsQl() <em>Aspect Specifications Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAspectSpecificationsQl()
	 * @generated
	 * @ordered
	 */
	protected ElementList aspectSpecificationsQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeferredConstantDeclarationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getDeferredConstantDeclaration();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFERRED_CONSTANT_DECLARATION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFERRED_CONSTANT_DECLARATION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFERRED_CONSTANT_DECLARATION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFERRED_CONSTANT_DECLARATION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNameList getNamesQl() {
		return namesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNamesQl(DefiningNameList newNamesQl, NotificationChain msgs) {
		DefiningNameList oldNamesQl = namesQl;
		namesQl = newNamesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFERRED_CONSTANT_DECLARATION__NAMES_QL, oldNamesQl, newNamesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNamesQl(DefiningNameList newNamesQl) {
		if (newNamesQl != namesQl) {
			NotificationChain msgs = null;
			if (namesQl != null)
				msgs = ((InternalEObject)namesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFERRED_CONSTANT_DECLARATION__NAMES_QL, null, msgs);
			if (newNamesQl != null)
				msgs = ((InternalEObject)newNamesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFERRED_CONSTANT_DECLARATION__NAMES_QL, null, msgs);
			msgs = basicSetNamesQl(newNamesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFERRED_CONSTANT_DECLARATION__NAMES_QL, newNamesQl, newNamesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAliasedQType4 getHasAliasedQ() {
		return hasAliasedQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasAliasedQ(HasAliasedQType4 newHasAliasedQ, NotificationChain msgs) {
		HasAliasedQType4 oldHasAliasedQ = hasAliasedQ;
		hasAliasedQ = newHasAliasedQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFERRED_CONSTANT_DECLARATION__HAS_ALIASED_Q, oldHasAliasedQ, newHasAliasedQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasAliasedQ(HasAliasedQType4 newHasAliasedQ) {
		if (newHasAliasedQ != hasAliasedQ) {
			NotificationChain msgs = null;
			if (hasAliasedQ != null)
				msgs = ((InternalEObject)hasAliasedQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFERRED_CONSTANT_DECLARATION__HAS_ALIASED_Q, null, msgs);
			if (newHasAliasedQ != null)
				msgs = ((InternalEObject)newHasAliasedQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFERRED_CONSTANT_DECLARATION__HAS_ALIASED_Q, null, msgs);
			msgs = basicSetHasAliasedQ(newHasAliasedQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFERRED_CONSTANT_DECLARATION__HAS_ALIASED_Q, newHasAliasedQ, newHasAliasedQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefinitionClass getObjectDeclarationViewQ() {
		return objectDeclarationViewQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectDeclarationViewQ(DefinitionClass newObjectDeclarationViewQ, NotificationChain msgs) {
		DefinitionClass oldObjectDeclarationViewQ = objectDeclarationViewQ;
		objectDeclarationViewQ = newObjectDeclarationViewQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFERRED_CONSTANT_DECLARATION__OBJECT_DECLARATION_VIEW_Q, oldObjectDeclarationViewQ, newObjectDeclarationViewQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectDeclarationViewQ(DefinitionClass newObjectDeclarationViewQ) {
		if (newObjectDeclarationViewQ != objectDeclarationViewQ) {
			NotificationChain msgs = null;
			if (objectDeclarationViewQ != null)
				msgs = ((InternalEObject)objectDeclarationViewQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFERRED_CONSTANT_DECLARATION__OBJECT_DECLARATION_VIEW_Q, null, msgs);
			if (newObjectDeclarationViewQ != null)
				msgs = ((InternalEObject)newObjectDeclarationViewQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFERRED_CONSTANT_DECLARATION__OBJECT_DECLARATION_VIEW_Q, null, msgs);
			msgs = basicSetObjectDeclarationViewQ(newObjectDeclarationViewQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFERRED_CONSTANT_DECLARATION__OBJECT_DECLARATION_VIEW_Q, newObjectDeclarationViewQ, newObjectDeclarationViewQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementList getAspectSpecificationsQl() {
		return aspectSpecificationsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAspectSpecificationsQl(ElementList newAspectSpecificationsQl, NotificationChain msgs) {
		ElementList oldAspectSpecificationsQl = aspectSpecificationsQl;
		aspectSpecificationsQl = newAspectSpecificationsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFERRED_CONSTANT_DECLARATION__ASPECT_SPECIFICATIONS_QL, oldAspectSpecificationsQl, newAspectSpecificationsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAspectSpecificationsQl(ElementList newAspectSpecificationsQl) {
		if (newAspectSpecificationsQl != aspectSpecificationsQl) {
			NotificationChain msgs = null;
			if (aspectSpecificationsQl != null)
				msgs = ((InternalEObject)aspectSpecificationsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFERRED_CONSTANT_DECLARATION__ASPECT_SPECIFICATIONS_QL, null, msgs);
			if (newAspectSpecificationsQl != null)
				msgs = ((InternalEObject)newAspectSpecificationsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFERRED_CONSTANT_DECLARATION__ASPECT_SPECIFICATIONS_QL, null, msgs);
			msgs = basicSetAspectSpecificationsQl(newAspectSpecificationsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFERRED_CONSTANT_DECLARATION__ASPECT_SPECIFICATIONS_QL, newAspectSpecificationsQl, newAspectSpecificationsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFERRED_CONSTANT_DECLARATION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__NAMES_QL:
				return basicSetNamesQl(null, msgs);
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__HAS_ALIASED_Q:
				return basicSetHasAliasedQ(null, msgs);
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__OBJECT_DECLARATION_VIEW_Q:
				return basicSetObjectDeclarationViewQ(null, msgs);
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__ASPECT_SPECIFICATIONS_QL:
				return basicSetAspectSpecificationsQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__SLOC:
				return getSloc();
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__NAMES_QL:
				return getNamesQl();
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__HAS_ALIASED_Q:
				return getHasAliasedQ();
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__OBJECT_DECLARATION_VIEW_Q:
				return getObjectDeclarationViewQ();
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__ASPECT_SPECIFICATIONS_QL:
				return getAspectSpecificationsQl();
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__NAMES_QL:
				setNamesQl((DefiningNameList)newValue);
				return;
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__HAS_ALIASED_Q:
				setHasAliasedQ((HasAliasedQType4)newValue);
				return;
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__OBJECT_DECLARATION_VIEW_Q:
				setObjectDeclarationViewQ((DefinitionClass)newValue);
				return;
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__ASPECT_SPECIFICATIONS_QL:
				setAspectSpecificationsQl((ElementList)newValue);
				return;
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__NAMES_QL:
				setNamesQl((DefiningNameList)null);
				return;
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__HAS_ALIASED_Q:
				setHasAliasedQ((HasAliasedQType4)null);
				return;
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__OBJECT_DECLARATION_VIEW_Q:
				setObjectDeclarationViewQ((DefinitionClass)null);
				return;
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__ASPECT_SPECIFICATIONS_QL:
				setAspectSpecificationsQl((ElementList)null);
				return;
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__SLOC:
				return sloc != null;
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__NAMES_QL:
				return namesQl != null;
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__HAS_ALIASED_Q:
				return hasAliasedQ != null;
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__OBJECT_DECLARATION_VIEW_Q:
				return objectDeclarationViewQ != null;
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__ASPECT_SPECIFICATIONS_QL:
				return aspectSpecificationsQl != null;
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //DeferredConstantDeclarationImpl
