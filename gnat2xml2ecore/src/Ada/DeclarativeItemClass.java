/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Declarative Item Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.DeclarativeItemClass#getNotAnElement <em>Not An Element</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getOrdinaryTypeDeclaration <em>Ordinary Type Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getTaskTypeDeclaration <em>Task Type Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getProtectedTypeDeclaration <em>Protected Type Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getIncompleteTypeDeclaration <em>Incomplete Type Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getTaggedIncompleteTypeDeclaration <em>Tagged Incomplete Type Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getPrivateTypeDeclaration <em>Private Type Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getPrivateExtensionDeclaration <em>Private Extension Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getSubtypeDeclaration <em>Subtype Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getVariableDeclaration <em>Variable Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getConstantDeclaration <em>Constant Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getDeferredConstantDeclaration <em>Deferred Constant Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getSingleTaskDeclaration <em>Single Task Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getSingleProtectedDeclaration <em>Single Protected Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getIntegerNumberDeclaration <em>Integer Number Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getRealNumberDeclaration <em>Real Number Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getEnumerationLiteralSpecification <em>Enumeration Literal Specification</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getDiscriminantSpecification <em>Discriminant Specification</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getComponentDeclaration <em>Component Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getLoopParameterSpecification <em>Loop Parameter Specification</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getGeneralizedIteratorSpecification <em>Generalized Iterator Specification</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getElementIteratorSpecification <em>Element Iterator Specification</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getProcedureDeclaration <em>Procedure Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getFunctionDeclaration <em>Function Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getParameterSpecification <em>Parameter Specification</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getProcedureBodyDeclaration <em>Procedure Body Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getFunctionBodyDeclaration <em>Function Body Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getReturnVariableSpecification <em>Return Variable Specification</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getReturnConstantSpecification <em>Return Constant Specification</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getNullProcedureDeclaration <em>Null Procedure Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getExpressionFunctionDeclaration <em>Expression Function Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getPackageDeclaration <em>Package Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getPackageBodyDeclaration <em>Package Body Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getObjectRenamingDeclaration <em>Object Renaming Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getExceptionRenamingDeclaration <em>Exception Renaming Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getPackageRenamingDeclaration <em>Package Renaming Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getProcedureRenamingDeclaration <em>Procedure Renaming Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getFunctionRenamingDeclaration <em>Function Renaming Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getGenericPackageRenamingDeclaration <em>Generic Package Renaming Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getGenericProcedureRenamingDeclaration <em>Generic Procedure Renaming Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getGenericFunctionRenamingDeclaration <em>Generic Function Renaming Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getTaskBodyDeclaration <em>Task Body Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getProtectedBodyDeclaration <em>Protected Body Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getEntryDeclaration <em>Entry Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getEntryBodyDeclaration <em>Entry Body Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getEntryIndexSpecification <em>Entry Index Specification</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getProcedureBodyStub <em>Procedure Body Stub</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getFunctionBodyStub <em>Function Body Stub</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getPackageBodyStub <em>Package Body Stub</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getTaskBodyStub <em>Task Body Stub</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getProtectedBodyStub <em>Protected Body Stub</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getExceptionDeclaration <em>Exception Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getChoiceParameterSpecification <em>Choice Parameter Specification</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getGenericProcedureDeclaration <em>Generic Procedure Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getGenericFunctionDeclaration <em>Generic Function Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getGenericPackageDeclaration <em>Generic Package Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getPackageInstantiation <em>Package Instantiation</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getProcedureInstantiation <em>Procedure Instantiation</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getFunctionInstantiation <em>Function Instantiation</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getFormalObjectDeclaration <em>Formal Object Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getFormalTypeDeclaration <em>Formal Type Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getFormalIncompleteTypeDeclaration <em>Formal Incomplete Type Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getFormalProcedureDeclaration <em>Formal Procedure Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getFormalFunctionDeclaration <em>Formal Function Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getFormalPackageDeclaration <em>Formal Package Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getFormalPackageDeclarationWithBox <em>Formal Package Declaration With Box</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getUsePackageClause <em>Use Package Clause</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getUseTypeClause <em>Use Type Clause</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getUseAllTypeClause <em>Use All Type Clause</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getWithClause <em>With Clause</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getAttributeDefinitionClause <em>Attribute Definition Clause</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getEnumerationRepresentationClause <em>Enumeration Representation Clause</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getRecordRepresentationClause <em>Record Representation Clause</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getAtClause <em>At Clause</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getComponentClause <em>Component Clause</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getComment <em>Comment</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getAsynchronousPragma <em>Asynchronous Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getAtomicPragma <em>Atomic Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getAttachHandlerPragma <em>Attach Handler Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getControlledPragma <em>Controlled Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getConventionPragma <em>Convention Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getDiscardNamesPragma <em>Discard Names Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getElaboratePragma <em>Elaborate Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getElaborateAllPragma <em>Elaborate All Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getExportPragma <em>Export Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getImportPragma <em>Import Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getInlinePragma <em>Inline Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getInspectionPointPragma <em>Inspection Point Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getLinkerOptionsPragma <em>Linker Options Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getListPragma <em>List Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getLockingPolicyPragma <em>Locking Policy Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getOptimizePragma <em>Optimize Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getPackPragma <em>Pack Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getPagePragma <em>Page Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getPreelaboratePragma <em>Preelaborate Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getPriorityPragma <em>Priority Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getPurePragma <em>Pure Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getRemoteTypesPragma <em>Remote Types Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getRestrictionsPragma <em>Restrictions Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getReviewablePragma <em>Reviewable Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getSharedPassivePragma <em>Shared Passive Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getStorageSizePragma <em>Storage Size Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getSuppressPragma <em>Suppress Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getVolatilePragma <em>Volatile Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getAssertPragma <em>Assert Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getNoReturnPragma <em>No Return Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getProfilePragma <em>Profile Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getUnsuppressPragma <em>Unsuppress Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getCpuPragma <em>Cpu Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getIndependentPragma <em>Independent Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getIndependentComponentsPragma <em>Independent Components Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemClass#getUnknownPragma <em>Unknown Pragma</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getDeclarativeItemClass()
 * @model extendedMetaData="name='Declarative_Item_Class' kind='elementOnly'"
 * @generated
 */
public interface DeclarativeItemClass extends EObject {
	/**
	 * Returns the value of the '<em><b>Not An Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not An Element</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not An Element</em>' containment reference.
	 * @see #setNotAnElement(NotAnElement)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_NotAnElement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not_an_element' namespace='##targetNamespace'"
	 * @generated
	 */
	NotAnElement getNotAnElement();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getNotAnElement <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not An Element</em>' containment reference.
	 * @see #getNotAnElement()
	 * @generated
	 */
	void setNotAnElement(NotAnElement value);

	/**
	 * Returns the value of the '<em><b>Ordinary Type Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ordinary Type Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ordinary Type Declaration</em>' containment reference.
	 * @see #setOrdinaryTypeDeclaration(OrdinaryTypeDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_OrdinaryTypeDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ordinary_type_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	OrdinaryTypeDeclaration getOrdinaryTypeDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getOrdinaryTypeDeclaration <em>Ordinary Type Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ordinary Type Declaration</em>' containment reference.
	 * @see #getOrdinaryTypeDeclaration()
	 * @generated
	 */
	void setOrdinaryTypeDeclaration(OrdinaryTypeDeclaration value);

	/**
	 * Returns the value of the '<em><b>Task Type Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Type Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Type Declaration</em>' containment reference.
	 * @see #setTaskTypeDeclaration(TaskTypeDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_TaskTypeDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='task_type_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	TaskTypeDeclaration getTaskTypeDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getTaskTypeDeclaration <em>Task Type Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Type Declaration</em>' containment reference.
	 * @see #getTaskTypeDeclaration()
	 * @generated
	 */
	void setTaskTypeDeclaration(TaskTypeDeclaration value);

	/**
	 * Returns the value of the '<em><b>Protected Type Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protected Type Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protected Type Declaration</em>' containment reference.
	 * @see #setProtectedTypeDeclaration(ProtectedTypeDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ProtectedTypeDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='protected_type_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	ProtectedTypeDeclaration getProtectedTypeDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getProtectedTypeDeclaration <em>Protected Type Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Protected Type Declaration</em>' containment reference.
	 * @see #getProtectedTypeDeclaration()
	 * @generated
	 */
	void setProtectedTypeDeclaration(ProtectedTypeDeclaration value);

	/**
	 * Returns the value of the '<em><b>Incomplete Type Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incomplete Type Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incomplete Type Declaration</em>' containment reference.
	 * @see #setIncompleteTypeDeclaration(IncompleteTypeDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_IncompleteTypeDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='incomplete_type_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	IncompleteTypeDeclaration getIncompleteTypeDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getIncompleteTypeDeclaration <em>Incomplete Type Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Incomplete Type Declaration</em>' containment reference.
	 * @see #getIncompleteTypeDeclaration()
	 * @generated
	 */
	void setIncompleteTypeDeclaration(IncompleteTypeDeclaration value);

	/**
	 * Returns the value of the '<em><b>Tagged Incomplete Type Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tagged Incomplete Type Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tagged Incomplete Type Declaration</em>' containment reference.
	 * @see #setTaggedIncompleteTypeDeclaration(TaggedIncompleteTypeDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_TaggedIncompleteTypeDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='tagged_incomplete_type_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	TaggedIncompleteTypeDeclaration getTaggedIncompleteTypeDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getTaggedIncompleteTypeDeclaration <em>Tagged Incomplete Type Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tagged Incomplete Type Declaration</em>' containment reference.
	 * @see #getTaggedIncompleteTypeDeclaration()
	 * @generated
	 */
	void setTaggedIncompleteTypeDeclaration(TaggedIncompleteTypeDeclaration value);

	/**
	 * Returns the value of the '<em><b>Private Type Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Private Type Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Private Type Declaration</em>' containment reference.
	 * @see #setPrivateTypeDeclaration(PrivateTypeDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_PrivateTypeDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='private_type_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	PrivateTypeDeclaration getPrivateTypeDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getPrivateTypeDeclaration <em>Private Type Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Private Type Declaration</em>' containment reference.
	 * @see #getPrivateTypeDeclaration()
	 * @generated
	 */
	void setPrivateTypeDeclaration(PrivateTypeDeclaration value);

	/**
	 * Returns the value of the '<em><b>Private Extension Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Private Extension Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Private Extension Declaration</em>' containment reference.
	 * @see #setPrivateExtensionDeclaration(PrivateExtensionDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_PrivateExtensionDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='private_extension_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	PrivateExtensionDeclaration getPrivateExtensionDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getPrivateExtensionDeclaration <em>Private Extension Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Private Extension Declaration</em>' containment reference.
	 * @see #getPrivateExtensionDeclaration()
	 * @generated
	 */
	void setPrivateExtensionDeclaration(PrivateExtensionDeclaration value);

	/**
	 * Returns the value of the '<em><b>Subtype Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subtype Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subtype Declaration</em>' containment reference.
	 * @see #setSubtypeDeclaration(SubtypeDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_SubtypeDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='subtype_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	SubtypeDeclaration getSubtypeDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getSubtypeDeclaration <em>Subtype Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Subtype Declaration</em>' containment reference.
	 * @see #getSubtypeDeclaration()
	 * @generated
	 */
	void setSubtypeDeclaration(SubtypeDeclaration value);

	/**
	 * Returns the value of the '<em><b>Variable Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variable Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variable Declaration</em>' containment reference.
	 * @see #setVariableDeclaration(VariableDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_VariableDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='variable_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	VariableDeclaration getVariableDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getVariableDeclaration <em>Variable Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variable Declaration</em>' containment reference.
	 * @see #getVariableDeclaration()
	 * @generated
	 */
	void setVariableDeclaration(VariableDeclaration value);

	/**
	 * Returns the value of the '<em><b>Constant Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant Declaration</em>' containment reference.
	 * @see #setConstantDeclaration(ConstantDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ConstantDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='constant_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	ConstantDeclaration getConstantDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getConstantDeclaration <em>Constant Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant Declaration</em>' containment reference.
	 * @see #getConstantDeclaration()
	 * @generated
	 */
	void setConstantDeclaration(ConstantDeclaration value);

	/**
	 * Returns the value of the '<em><b>Deferred Constant Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deferred Constant Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deferred Constant Declaration</em>' containment reference.
	 * @see #setDeferredConstantDeclaration(DeferredConstantDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_DeferredConstantDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='deferred_constant_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	DeferredConstantDeclaration getDeferredConstantDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getDeferredConstantDeclaration <em>Deferred Constant Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deferred Constant Declaration</em>' containment reference.
	 * @see #getDeferredConstantDeclaration()
	 * @generated
	 */
	void setDeferredConstantDeclaration(DeferredConstantDeclaration value);

	/**
	 * Returns the value of the '<em><b>Single Task Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Single Task Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Single Task Declaration</em>' containment reference.
	 * @see #setSingleTaskDeclaration(SingleTaskDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_SingleTaskDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='single_task_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	SingleTaskDeclaration getSingleTaskDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getSingleTaskDeclaration <em>Single Task Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Single Task Declaration</em>' containment reference.
	 * @see #getSingleTaskDeclaration()
	 * @generated
	 */
	void setSingleTaskDeclaration(SingleTaskDeclaration value);

	/**
	 * Returns the value of the '<em><b>Single Protected Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Single Protected Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Single Protected Declaration</em>' containment reference.
	 * @see #setSingleProtectedDeclaration(SingleProtectedDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_SingleProtectedDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='single_protected_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	SingleProtectedDeclaration getSingleProtectedDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getSingleProtectedDeclaration <em>Single Protected Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Single Protected Declaration</em>' containment reference.
	 * @see #getSingleProtectedDeclaration()
	 * @generated
	 */
	void setSingleProtectedDeclaration(SingleProtectedDeclaration value);

	/**
	 * Returns the value of the '<em><b>Integer Number Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integer Number Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integer Number Declaration</em>' containment reference.
	 * @see #setIntegerNumberDeclaration(IntegerNumberDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_IntegerNumberDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='integer_number_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	IntegerNumberDeclaration getIntegerNumberDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getIntegerNumberDeclaration <em>Integer Number Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Integer Number Declaration</em>' containment reference.
	 * @see #getIntegerNumberDeclaration()
	 * @generated
	 */
	void setIntegerNumberDeclaration(IntegerNumberDeclaration value);

	/**
	 * Returns the value of the '<em><b>Real Number Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Real Number Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Real Number Declaration</em>' containment reference.
	 * @see #setRealNumberDeclaration(RealNumberDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_RealNumberDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='real_number_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	RealNumberDeclaration getRealNumberDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getRealNumberDeclaration <em>Real Number Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Real Number Declaration</em>' containment reference.
	 * @see #getRealNumberDeclaration()
	 * @generated
	 */
	void setRealNumberDeclaration(RealNumberDeclaration value);

	/**
	 * Returns the value of the '<em><b>Enumeration Literal Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumeration Literal Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumeration Literal Specification</em>' containment reference.
	 * @see #setEnumerationLiteralSpecification(EnumerationLiteralSpecification)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_EnumerationLiteralSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='enumeration_literal_specification' namespace='##targetNamespace'"
	 * @generated
	 */
	EnumerationLiteralSpecification getEnumerationLiteralSpecification();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getEnumerationLiteralSpecification <em>Enumeration Literal Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enumeration Literal Specification</em>' containment reference.
	 * @see #getEnumerationLiteralSpecification()
	 * @generated
	 */
	void setEnumerationLiteralSpecification(EnumerationLiteralSpecification value);

	/**
	 * Returns the value of the '<em><b>Discriminant Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discriminant Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discriminant Specification</em>' containment reference.
	 * @see #setDiscriminantSpecification(DiscriminantSpecification)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_DiscriminantSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='discriminant_specification' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscriminantSpecification getDiscriminantSpecification();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getDiscriminantSpecification <em>Discriminant Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discriminant Specification</em>' containment reference.
	 * @see #getDiscriminantSpecification()
	 * @generated
	 */
	void setDiscriminantSpecification(DiscriminantSpecification value);

	/**
	 * Returns the value of the '<em><b>Component Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Declaration</em>' containment reference.
	 * @see #setComponentDeclaration(ComponentDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ComponentDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='component_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	ComponentDeclaration getComponentDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getComponentDeclaration <em>Component Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component Declaration</em>' containment reference.
	 * @see #getComponentDeclaration()
	 * @generated
	 */
	void setComponentDeclaration(ComponentDeclaration value);

	/**
	 * Returns the value of the '<em><b>Loop Parameter Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Loop Parameter Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Loop Parameter Specification</em>' containment reference.
	 * @see #setLoopParameterSpecification(LoopParameterSpecification)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_LoopParameterSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='loop_parameter_specification' namespace='##targetNamespace'"
	 * @generated
	 */
	LoopParameterSpecification getLoopParameterSpecification();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getLoopParameterSpecification <em>Loop Parameter Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Loop Parameter Specification</em>' containment reference.
	 * @see #getLoopParameterSpecification()
	 * @generated
	 */
	void setLoopParameterSpecification(LoopParameterSpecification value);

	/**
	 * Returns the value of the '<em><b>Generalized Iterator Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generalized Iterator Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generalized Iterator Specification</em>' containment reference.
	 * @see #setGeneralizedIteratorSpecification(GeneralizedIteratorSpecification)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_GeneralizedIteratorSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='generalized_iterator_specification' namespace='##targetNamespace'"
	 * @generated
	 */
	GeneralizedIteratorSpecification getGeneralizedIteratorSpecification();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getGeneralizedIteratorSpecification <em>Generalized Iterator Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generalized Iterator Specification</em>' containment reference.
	 * @see #getGeneralizedIteratorSpecification()
	 * @generated
	 */
	void setGeneralizedIteratorSpecification(GeneralizedIteratorSpecification value);

	/**
	 * Returns the value of the '<em><b>Element Iterator Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element Iterator Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Iterator Specification</em>' containment reference.
	 * @see #setElementIteratorSpecification(ElementIteratorSpecification)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ElementIteratorSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='element_iterator_specification' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementIteratorSpecification getElementIteratorSpecification();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getElementIteratorSpecification <em>Element Iterator Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element Iterator Specification</em>' containment reference.
	 * @see #getElementIteratorSpecification()
	 * @generated
	 */
	void setElementIteratorSpecification(ElementIteratorSpecification value);

	/**
	 * Returns the value of the '<em><b>Procedure Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure Declaration</em>' containment reference.
	 * @see #setProcedureDeclaration(ProcedureDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ProcedureDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='procedure_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcedureDeclaration getProcedureDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getProcedureDeclaration <em>Procedure Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Procedure Declaration</em>' containment reference.
	 * @see #getProcedureDeclaration()
	 * @generated
	 */
	void setProcedureDeclaration(ProcedureDeclaration value);

	/**
	 * Returns the value of the '<em><b>Function Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Declaration</em>' containment reference.
	 * @see #setFunctionDeclaration(FunctionDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_FunctionDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='function_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	FunctionDeclaration getFunctionDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getFunctionDeclaration <em>Function Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function Declaration</em>' containment reference.
	 * @see #getFunctionDeclaration()
	 * @generated
	 */
	void setFunctionDeclaration(FunctionDeclaration value);

	/**
	 * Returns the value of the '<em><b>Parameter Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Specification</em>' containment reference.
	 * @see #setParameterSpecification(ParameterSpecification)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ParameterSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='parameter_specification' namespace='##targetNamespace'"
	 * @generated
	 */
	ParameterSpecification getParameterSpecification();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getParameterSpecification <em>Parameter Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Specification</em>' containment reference.
	 * @see #getParameterSpecification()
	 * @generated
	 */
	void setParameterSpecification(ParameterSpecification value);

	/**
	 * Returns the value of the '<em><b>Procedure Body Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure Body Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure Body Declaration</em>' containment reference.
	 * @see #setProcedureBodyDeclaration(ProcedureBodyDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ProcedureBodyDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='procedure_body_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcedureBodyDeclaration getProcedureBodyDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getProcedureBodyDeclaration <em>Procedure Body Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Procedure Body Declaration</em>' containment reference.
	 * @see #getProcedureBodyDeclaration()
	 * @generated
	 */
	void setProcedureBodyDeclaration(ProcedureBodyDeclaration value);

	/**
	 * Returns the value of the '<em><b>Function Body Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Body Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Body Declaration</em>' containment reference.
	 * @see #setFunctionBodyDeclaration(FunctionBodyDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_FunctionBodyDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='function_body_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	FunctionBodyDeclaration getFunctionBodyDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getFunctionBodyDeclaration <em>Function Body Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function Body Declaration</em>' containment reference.
	 * @see #getFunctionBodyDeclaration()
	 * @generated
	 */
	void setFunctionBodyDeclaration(FunctionBodyDeclaration value);

	/**
	 * Returns the value of the '<em><b>Return Variable Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Return Variable Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Return Variable Specification</em>' containment reference.
	 * @see #setReturnVariableSpecification(ReturnVariableSpecification)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ReturnVariableSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='return_variable_specification' namespace='##targetNamespace'"
	 * @generated
	 */
	ReturnVariableSpecification getReturnVariableSpecification();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getReturnVariableSpecification <em>Return Variable Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Return Variable Specification</em>' containment reference.
	 * @see #getReturnVariableSpecification()
	 * @generated
	 */
	void setReturnVariableSpecification(ReturnVariableSpecification value);

	/**
	 * Returns the value of the '<em><b>Return Constant Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Return Constant Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Return Constant Specification</em>' containment reference.
	 * @see #setReturnConstantSpecification(ReturnConstantSpecification)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ReturnConstantSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='return_constant_specification' namespace='##targetNamespace'"
	 * @generated
	 */
	ReturnConstantSpecification getReturnConstantSpecification();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getReturnConstantSpecification <em>Return Constant Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Return Constant Specification</em>' containment reference.
	 * @see #getReturnConstantSpecification()
	 * @generated
	 */
	void setReturnConstantSpecification(ReturnConstantSpecification value);

	/**
	 * Returns the value of the '<em><b>Null Procedure Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Null Procedure Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Null Procedure Declaration</em>' containment reference.
	 * @see #setNullProcedureDeclaration(NullProcedureDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_NullProcedureDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='null_procedure_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	NullProcedureDeclaration getNullProcedureDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getNullProcedureDeclaration <em>Null Procedure Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Null Procedure Declaration</em>' containment reference.
	 * @see #getNullProcedureDeclaration()
	 * @generated
	 */
	void setNullProcedureDeclaration(NullProcedureDeclaration value);

	/**
	 * Returns the value of the '<em><b>Expression Function Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression Function Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression Function Declaration</em>' containment reference.
	 * @see #setExpressionFunctionDeclaration(ExpressionFunctionDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ExpressionFunctionDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='expression_function_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionFunctionDeclaration getExpressionFunctionDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getExpressionFunctionDeclaration <em>Expression Function Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression Function Declaration</em>' containment reference.
	 * @see #getExpressionFunctionDeclaration()
	 * @generated
	 */
	void setExpressionFunctionDeclaration(ExpressionFunctionDeclaration value);

	/**
	 * Returns the value of the '<em><b>Package Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Declaration</em>' containment reference.
	 * @see #setPackageDeclaration(PackageDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_PackageDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='package_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	PackageDeclaration getPackageDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getPackageDeclaration <em>Package Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Package Declaration</em>' containment reference.
	 * @see #getPackageDeclaration()
	 * @generated
	 */
	void setPackageDeclaration(PackageDeclaration value);

	/**
	 * Returns the value of the '<em><b>Package Body Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Body Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Body Declaration</em>' containment reference.
	 * @see #setPackageBodyDeclaration(PackageBodyDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_PackageBodyDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='package_body_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	PackageBodyDeclaration getPackageBodyDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getPackageBodyDeclaration <em>Package Body Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Package Body Declaration</em>' containment reference.
	 * @see #getPackageBodyDeclaration()
	 * @generated
	 */
	void setPackageBodyDeclaration(PackageBodyDeclaration value);

	/**
	 * Returns the value of the '<em><b>Object Renaming Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Renaming Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Renaming Declaration</em>' containment reference.
	 * @see #setObjectRenamingDeclaration(ObjectRenamingDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ObjectRenamingDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='object_renaming_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectRenamingDeclaration getObjectRenamingDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getObjectRenamingDeclaration <em>Object Renaming Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Renaming Declaration</em>' containment reference.
	 * @see #getObjectRenamingDeclaration()
	 * @generated
	 */
	void setObjectRenamingDeclaration(ObjectRenamingDeclaration value);

	/**
	 * Returns the value of the '<em><b>Exception Renaming Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exception Renaming Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exception Renaming Declaration</em>' containment reference.
	 * @see #setExceptionRenamingDeclaration(ExceptionRenamingDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ExceptionRenamingDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='exception_renaming_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	ExceptionRenamingDeclaration getExceptionRenamingDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getExceptionRenamingDeclaration <em>Exception Renaming Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exception Renaming Declaration</em>' containment reference.
	 * @see #getExceptionRenamingDeclaration()
	 * @generated
	 */
	void setExceptionRenamingDeclaration(ExceptionRenamingDeclaration value);

	/**
	 * Returns the value of the '<em><b>Package Renaming Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Renaming Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Renaming Declaration</em>' containment reference.
	 * @see #setPackageRenamingDeclaration(PackageRenamingDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_PackageRenamingDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='package_renaming_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	PackageRenamingDeclaration getPackageRenamingDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getPackageRenamingDeclaration <em>Package Renaming Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Package Renaming Declaration</em>' containment reference.
	 * @see #getPackageRenamingDeclaration()
	 * @generated
	 */
	void setPackageRenamingDeclaration(PackageRenamingDeclaration value);

	/**
	 * Returns the value of the '<em><b>Procedure Renaming Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure Renaming Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure Renaming Declaration</em>' containment reference.
	 * @see #setProcedureRenamingDeclaration(ProcedureRenamingDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ProcedureRenamingDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='procedure_renaming_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcedureRenamingDeclaration getProcedureRenamingDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getProcedureRenamingDeclaration <em>Procedure Renaming Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Procedure Renaming Declaration</em>' containment reference.
	 * @see #getProcedureRenamingDeclaration()
	 * @generated
	 */
	void setProcedureRenamingDeclaration(ProcedureRenamingDeclaration value);

	/**
	 * Returns the value of the '<em><b>Function Renaming Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Renaming Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Renaming Declaration</em>' containment reference.
	 * @see #setFunctionRenamingDeclaration(FunctionRenamingDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_FunctionRenamingDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='function_renaming_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	FunctionRenamingDeclaration getFunctionRenamingDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getFunctionRenamingDeclaration <em>Function Renaming Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function Renaming Declaration</em>' containment reference.
	 * @see #getFunctionRenamingDeclaration()
	 * @generated
	 */
	void setFunctionRenamingDeclaration(FunctionRenamingDeclaration value);

	/**
	 * Returns the value of the '<em><b>Generic Package Renaming Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Package Renaming Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Package Renaming Declaration</em>' containment reference.
	 * @see #setGenericPackageRenamingDeclaration(GenericPackageRenamingDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_GenericPackageRenamingDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='generic_package_renaming_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	GenericPackageRenamingDeclaration getGenericPackageRenamingDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getGenericPackageRenamingDeclaration <em>Generic Package Renaming Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generic Package Renaming Declaration</em>' containment reference.
	 * @see #getGenericPackageRenamingDeclaration()
	 * @generated
	 */
	void setGenericPackageRenamingDeclaration(GenericPackageRenamingDeclaration value);

	/**
	 * Returns the value of the '<em><b>Generic Procedure Renaming Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Procedure Renaming Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Procedure Renaming Declaration</em>' containment reference.
	 * @see #setGenericProcedureRenamingDeclaration(GenericProcedureRenamingDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_GenericProcedureRenamingDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='generic_procedure_renaming_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	GenericProcedureRenamingDeclaration getGenericProcedureRenamingDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getGenericProcedureRenamingDeclaration <em>Generic Procedure Renaming Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generic Procedure Renaming Declaration</em>' containment reference.
	 * @see #getGenericProcedureRenamingDeclaration()
	 * @generated
	 */
	void setGenericProcedureRenamingDeclaration(GenericProcedureRenamingDeclaration value);

	/**
	 * Returns the value of the '<em><b>Generic Function Renaming Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Function Renaming Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Function Renaming Declaration</em>' containment reference.
	 * @see #setGenericFunctionRenamingDeclaration(GenericFunctionRenamingDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_GenericFunctionRenamingDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='generic_function_renaming_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	GenericFunctionRenamingDeclaration getGenericFunctionRenamingDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getGenericFunctionRenamingDeclaration <em>Generic Function Renaming Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generic Function Renaming Declaration</em>' containment reference.
	 * @see #getGenericFunctionRenamingDeclaration()
	 * @generated
	 */
	void setGenericFunctionRenamingDeclaration(GenericFunctionRenamingDeclaration value);

	/**
	 * Returns the value of the '<em><b>Task Body Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Body Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Body Declaration</em>' containment reference.
	 * @see #setTaskBodyDeclaration(TaskBodyDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_TaskBodyDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='task_body_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	TaskBodyDeclaration getTaskBodyDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getTaskBodyDeclaration <em>Task Body Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Body Declaration</em>' containment reference.
	 * @see #getTaskBodyDeclaration()
	 * @generated
	 */
	void setTaskBodyDeclaration(TaskBodyDeclaration value);

	/**
	 * Returns the value of the '<em><b>Protected Body Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protected Body Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protected Body Declaration</em>' containment reference.
	 * @see #setProtectedBodyDeclaration(ProtectedBodyDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ProtectedBodyDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='protected_body_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	ProtectedBodyDeclaration getProtectedBodyDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getProtectedBodyDeclaration <em>Protected Body Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Protected Body Declaration</em>' containment reference.
	 * @see #getProtectedBodyDeclaration()
	 * @generated
	 */
	void setProtectedBodyDeclaration(ProtectedBodyDeclaration value);

	/**
	 * Returns the value of the '<em><b>Entry Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry Declaration</em>' containment reference.
	 * @see #setEntryDeclaration(EntryDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_EntryDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='entry_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	EntryDeclaration getEntryDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getEntryDeclaration <em>Entry Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entry Declaration</em>' containment reference.
	 * @see #getEntryDeclaration()
	 * @generated
	 */
	void setEntryDeclaration(EntryDeclaration value);

	/**
	 * Returns the value of the '<em><b>Entry Body Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry Body Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry Body Declaration</em>' containment reference.
	 * @see #setEntryBodyDeclaration(EntryBodyDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_EntryBodyDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='entry_body_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	EntryBodyDeclaration getEntryBodyDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getEntryBodyDeclaration <em>Entry Body Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entry Body Declaration</em>' containment reference.
	 * @see #getEntryBodyDeclaration()
	 * @generated
	 */
	void setEntryBodyDeclaration(EntryBodyDeclaration value);

	/**
	 * Returns the value of the '<em><b>Entry Index Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry Index Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry Index Specification</em>' containment reference.
	 * @see #setEntryIndexSpecification(EntryIndexSpecification)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_EntryIndexSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='entry_index_specification' namespace='##targetNamespace'"
	 * @generated
	 */
	EntryIndexSpecification getEntryIndexSpecification();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getEntryIndexSpecification <em>Entry Index Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entry Index Specification</em>' containment reference.
	 * @see #getEntryIndexSpecification()
	 * @generated
	 */
	void setEntryIndexSpecification(EntryIndexSpecification value);

	/**
	 * Returns the value of the '<em><b>Procedure Body Stub</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure Body Stub</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure Body Stub</em>' containment reference.
	 * @see #setProcedureBodyStub(ProcedureBodyStub)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ProcedureBodyStub()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='procedure_body_stub' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcedureBodyStub getProcedureBodyStub();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getProcedureBodyStub <em>Procedure Body Stub</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Procedure Body Stub</em>' containment reference.
	 * @see #getProcedureBodyStub()
	 * @generated
	 */
	void setProcedureBodyStub(ProcedureBodyStub value);

	/**
	 * Returns the value of the '<em><b>Function Body Stub</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Body Stub</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Body Stub</em>' containment reference.
	 * @see #setFunctionBodyStub(FunctionBodyStub)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_FunctionBodyStub()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='function_body_stub' namespace='##targetNamespace'"
	 * @generated
	 */
	FunctionBodyStub getFunctionBodyStub();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getFunctionBodyStub <em>Function Body Stub</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function Body Stub</em>' containment reference.
	 * @see #getFunctionBodyStub()
	 * @generated
	 */
	void setFunctionBodyStub(FunctionBodyStub value);

	/**
	 * Returns the value of the '<em><b>Package Body Stub</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Body Stub</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Body Stub</em>' containment reference.
	 * @see #setPackageBodyStub(PackageBodyStub)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_PackageBodyStub()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='package_body_stub' namespace='##targetNamespace'"
	 * @generated
	 */
	PackageBodyStub getPackageBodyStub();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getPackageBodyStub <em>Package Body Stub</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Package Body Stub</em>' containment reference.
	 * @see #getPackageBodyStub()
	 * @generated
	 */
	void setPackageBodyStub(PackageBodyStub value);

	/**
	 * Returns the value of the '<em><b>Task Body Stub</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Body Stub</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Body Stub</em>' containment reference.
	 * @see #setTaskBodyStub(TaskBodyStub)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_TaskBodyStub()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='task_body_stub' namespace='##targetNamespace'"
	 * @generated
	 */
	TaskBodyStub getTaskBodyStub();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getTaskBodyStub <em>Task Body Stub</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Body Stub</em>' containment reference.
	 * @see #getTaskBodyStub()
	 * @generated
	 */
	void setTaskBodyStub(TaskBodyStub value);

	/**
	 * Returns the value of the '<em><b>Protected Body Stub</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protected Body Stub</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protected Body Stub</em>' containment reference.
	 * @see #setProtectedBodyStub(ProtectedBodyStub)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ProtectedBodyStub()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='protected_body_stub' namespace='##targetNamespace'"
	 * @generated
	 */
	ProtectedBodyStub getProtectedBodyStub();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getProtectedBodyStub <em>Protected Body Stub</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Protected Body Stub</em>' containment reference.
	 * @see #getProtectedBodyStub()
	 * @generated
	 */
	void setProtectedBodyStub(ProtectedBodyStub value);

	/**
	 * Returns the value of the '<em><b>Exception Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exception Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exception Declaration</em>' containment reference.
	 * @see #setExceptionDeclaration(ExceptionDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ExceptionDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='exception_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	ExceptionDeclaration getExceptionDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getExceptionDeclaration <em>Exception Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exception Declaration</em>' containment reference.
	 * @see #getExceptionDeclaration()
	 * @generated
	 */
	void setExceptionDeclaration(ExceptionDeclaration value);

	/**
	 * Returns the value of the '<em><b>Choice Parameter Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Choice Parameter Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Choice Parameter Specification</em>' containment reference.
	 * @see #setChoiceParameterSpecification(ChoiceParameterSpecification)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ChoiceParameterSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='choice_parameter_specification' namespace='##targetNamespace'"
	 * @generated
	 */
	ChoiceParameterSpecification getChoiceParameterSpecification();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getChoiceParameterSpecification <em>Choice Parameter Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Choice Parameter Specification</em>' containment reference.
	 * @see #getChoiceParameterSpecification()
	 * @generated
	 */
	void setChoiceParameterSpecification(ChoiceParameterSpecification value);

	/**
	 * Returns the value of the '<em><b>Generic Procedure Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Procedure Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Procedure Declaration</em>' containment reference.
	 * @see #setGenericProcedureDeclaration(GenericProcedureDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_GenericProcedureDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='generic_procedure_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	GenericProcedureDeclaration getGenericProcedureDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getGenericProcedureDeclaration <em>Generic Procedure Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generic Procedure Declaration</em>' containment reference.
	 * @see #getGenericProcedureDeclaration()
	 * @generated
	 */
	void setGenericProcedureDeclaration(GenericProcedureDeclaration value);

	/**
	 * Returns the value of the '<em><b>Generic Function Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Function Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Function Declaration</em>' containment reference.
	 * @see #setGenericFunctionDeclaration(GenericFunctionDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_GenericFunctionDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='generic_function_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	GenericFunctionDeclaration getGenericFunctionDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getGenericFunctionDeclaration <em>Generic Function Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generic Function Declaration</em>' containment reference.
	 * @see #getGenericFunctionDeclaration()
	 * @generated
	 */
	void setGenericFunctionDeclaration(GenericFunctionDeclaration value);

	/**
	 * Returns the value of the '<em><b>Generic Package Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Package Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Package Declaration</em>' containment reference.
	 * @see #setGenericPackageDeclaration(GenericPackageDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_GenericPackageDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='generic_package_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	GenericPackageDeclaration getGenericPackageDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getGenericPackageDeclaration <em>Generic Package Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generic Package Declaration</em>' containment reference.
	 * @see #getGenericPackageDeclaration()
	 * @generated
	 */
	void setGenericPackageDeclaration(GenericPackageDeclaration value);

	/**
	 * Returns the value of the '<em><b>Package Instantiation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Instantiation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Instantiation</em>' containment reference.
	 * @see #setPackageInstantiation(PackageInstantiation)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_PackageInstantiation()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='package_instantiation' namespace='##targetNamespace'"
	 * @generated
	 */
	PackageInstantiation getPackageInstantiation();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getPackageInstantiation <em>Package Instantiation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Package Instantiation</em>' containment reference.
	 * @see #getPackageInstantiation()
	 * @generated
	 */
	void setPackageInstantiation(PackageInstantiation value);

	/**
	 * Returns the value of the '<em><b>Procedure Instantiation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure Instantiation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure Instantiation</em>' containment reference.
	 * @see #setProcedureInstantiation(ProcedureInstantiation)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ProcedureInstantiation()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='procedure_instantiation' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcedureInstantiation getProcedureInstantiation();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getProcedureInstantiation <em>Procedure Instantiation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Procedure Instantiation</em>' containment reference.
	 * @see #getProcedureInstantiation()
	 * @generated
	 */
	void setProcedureInstantiation(ProcedureInstantiation value);

	/**
	 * Returns the value of the '<em><b>Function Instantiation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Instantiation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Instantiation</em>' containment reference.
	 * @see #setFunctionInstantiation(FunctionInstantiation)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_FunctionInstantiation()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='function_instantiation' namespace='##targetNamespace'"
	 * @generated
	 */
	FunctionInstantiation getFunctionInstantiation();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getFunctionInstantiation <em>Function Instantiation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function Instantiation</em>' containment reference.
	 * @see #getFunctionInstantiation()
	 * @generated
	 */
	void setFunctionInstantiation(FunctionInstantiation value);

	/**
	 * Returns the value of the '<em><b>Formal Object Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Object Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Object Declaration</em>' containment reference.
	 * @see #setFormalObjectDeclaration(FormalObjectDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_FormalObjectDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_object_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalObjectDeclaration getFormalObjectDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getFormalObjectDeclaration <em>Formal Object Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Object Declaration</em>' containment reference.
	 * @see #getFormalObjectDeclaration()
	 * @generated
	 */
	void setFormalObjectDeclaration(FormalObjectDeclaration value);

	/**
	 * Returns the value of the '<em><b>Formal Type Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Type Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Type Declaration</em>' containment reference.
	 * @see #setFormalTypeDeclaration(FormalTypeDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_FormalTypeDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_type_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalTypeDeclaration getFormalTypeDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getFormalTypeDeclaration <em>Formal Type Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Type Declaration</em>' containment reference.
	 * @see #getFormalTypeDeclaration()
	 * @generated
	 */
	void setFormalTypeDeclaration(FormalTypeDeclaration value);

	/**
	 * Returns the value of the '<em><b>Formal Incomplete Type Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Incomplete Type Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Incomplete Type Declaration</em>' containment reference.
	 * @see #setFormalIncompleteTypeDeclaration(FormalIncompleteTypeDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_FormalIncompleteTypeDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_incomplete_type_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalIncompleteTypeDeclaration getFormalIncompleteTypeDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getFormalIncompleteTypeDeclaration <em>Formal Incomplete Type Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Incomplete Type Declaration</em>' containment reference.
	 * @see #getFormalIncompleteTypeDeclaration()
	 * @generated
	 */
	void setFormalIncompleteTypeDeclaration(FormalIncompleteTypeDeclaration value);

	/**
	 * Returns the value of the '<em><b>Formal Procedure Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Procedure Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Procedure Declaration</em>' containment reference.
	 * @see #setFormalProcedureDeclaration(FormalProcedureDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_FormalProcedureDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_procedure_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalProcedureDeclaration getFormalProcedureDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getFormalProcedureDeclaration <em>Formal Procedure Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Procedure Declaration</em>' containment reference.
	 * @see #getFormalProcedureDeclaration()
	 * @generated
	 */
	void setFormalProcedureDeclaration(FormalProcedureDeclaration value);

	/**
	 * Returns the value of the '<em><b>Formal Function Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Function Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Function Declaration</em>' containment reference.
	 * @see #setFormalFunctionDeclaration(FormalFunctionDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_FormalFunctionDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_function_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalFunctionDeclaration getFormalFunctionDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getFormalFunctionDeclaration <em>Formal Function Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Function Declaration</em>' containment reference.
	 * @see #getFormalFunctionDeclaration()
	 * @generated
	 */
	void setFormalFunctionDeclaration(FormalFunctionDeclaration value);

	/**
	 * Returns the value of the '<em><b>Formal Package Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Package Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Package Declaration</em>' containment reference.
	 * @see #setFormalPackageDeclaration(FormalPackageDeclaration)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_FormalPackageDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_package_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalPackageDeclaration getFormalPackageDeclaration();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getFormalPackageDeclaration <em>Formal Package Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Package Declaration</em>' containment reference.
	 * @see #getFormalPackageDeclaration()
	 * @generated
	 */
	void setFormalPackageDeclaration(FormalPackageDeclaration value);

	/**
	 * Returns the value of the '<em><b>Formal Package Declaration With Box</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Package Declaration With Box</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Package Declaration With Box</em>' containment reference.
	 * @see #setFormalPackageDeclarationWithBox(FormalPackageDeclarationWithBox)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_FormalPackageDeclarationWithBox()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_package_declaration_with_box' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalPackageDeclarationWithBox getFormalPackageDeclarationWithBox();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getFormalPackageDeclarationWithBox <em>Formal Package Declaration With Box</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Package Declaration With Box</em>' containment reference.
	 * @see #getFormalPackageDeclarationWithBox()
	 * @generated
	 */
	void setFormalPackageDeclarationWithBox(FormalPackageDeclarationWithBox value);

	/**
	 * Returns the value of the '<em><b>Use Package Clause</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use Package Clause</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Package Clause</em>' containment reference.
	 * @see #setUsePackageClause(UsePackageClause)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_UsePackageClause()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='use_package_clause' namespace='##targetNamespace'"
	 * @generated
	 */
	UsePackageClause getUsePackageClause();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getUsePackageClause <em>Use Package Clause</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use Package Clause</em>' containment reference.
	 * @see #getUsePackageClause()
	 * @generated
	 */
	void setUsePackageClause(UsePackageClause value);

	/**
	 * Returns the value of the '<em><b>Use Type Clause</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use Type Clause</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Type Clause</em>' containment reference.
	 * @see #setUseTypeClause(UseTypeClause)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_UseTypeClause()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='use_type_clause' namespace='##targetNamespace'"
	 * @generated
	 */
	UseTypeClause getUseTypeClause();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getUseTypeClause <em>Use Type Clause</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use Type Clause</em>' containment reference.
	 * @see #getUseTypeClause()
	 * @generated
	 */
	void setUseTypeClause(UseTypeClause value);

	/**
	 * Returns the value of the '<em><b>Use All Type Clause</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use All Type Clause</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use All Type Clause</em>' containment reference.
	 * @see #setUseAllTypeClause(UseAllTypeClause)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_UseAllTypeClause()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='use_all_type_clause' namespace='##targetNamespace'"
	 * @generated
	 */
	UseAllTypeClause getUseAllTypeClause();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getUseAllTypeClause <em>Use All Type Clause</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use All Type Clause</em>' containment reference.
	 * @see #getUseAllTypeClause()
	 * @generated
	 */
	void setUseAllTypeClause(UseAllTypeClause value);

	/**
	 * Returns the value of the '<em><b>With Clause</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>With Clause</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>With Clause</em>' containment reference.
	 * @see #setWithClause(WithClause)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_WithClause()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='with_clause' namespace='##targetNamespace'"
	 * @generated
	 */
	WithClause getWithClause();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getWithClause <em>With Clause</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>With Clause</em>' containment reference.
	 * @see #getWithClause()
	 * @generated
	 */
	void setWithClause(WithClause value);

	/**
	 * Returns the value of the '<em><b>Attribute Definition Clause</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute Definition Clause</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Definition Clause</em>' containment reference.
	 * @see #setAttributeDefinitionClause(AttributeDefinitionClause)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_AttributeDefinitionClause()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='attribute_definition_clause' namespace='##targetNamespace'"
	 * @generated
	 */
	AttributeDefinitionClause getAttributeDefinitionClause();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getAttributeDefinitionClause <em>Attribute Definition Clause</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Definition Clause</em>' containment reference.
	 * @see #getAttributeDefinitionClause()
	 * @generated
	 */
	void setAttributeDefinitionClause(AttributeDefinitionClause value);

	/**
	 * Returns the value of the '<em><b>Enumeration Representation Clause</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumeration Representation Clause</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumeration Representation Clause</em>' containment reference.
	 * @see #setEnumerationRepresentationClause(EnumerationRepresentationClause)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_EnumerationRepresentationClause()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='enumeration_representation_clause' namespace='##targetNamespace'"
	 * @generated
	 */
	EnumerationRepresentationClause getEnumerationRepresentationClause();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getEnumerationRepresentationClause <em>Enumeration Representation Clause</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enumeration Representation Clause</em>' containment reference.
	 * @see #getEnumerationRepresentationClause()
	 * @generated
	 */
	void setEnumerationRepresentationClause(EnumerationRepresentationClause value);

	/**
	 * Returns the value of the '<em><b>Record Representation Clause</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Representation Clause</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Representation Clause</em>' containment reference.
	 * @see #setRecordRepresentationClause(RecordRepresentationClause)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_RecordRepresentationClause()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='record_representation_clause' namespace='##targetNamespace'"
	 * @generated
	 */
	RecordRepresentationClause getRecordRepresentationClause();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getRecordRepresentationClause <em>Record Representation Clause</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Record Representation Clause</em>' containment reference.
	 * @see #getRecordRepresentationClause()
	 * @generated
	 */
	void setRecordRepresentationClause(RecordRepresentationClause value);

	/**
	 * Returns the value of the '<em><b>At Clause</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>At Clause</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>At Clause</em>' containment reference.
	 * @see #setAtClause(AtClause)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_AtClause()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='at_clause' namespace='##targetNamespace'"
	 * @generated
	 */
	AtClause getAtClause();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getAtClause <em>At Clause</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>At Clause</em>' containment reference.
	 * @see #getAtClause()
	 * @generated
	 */
	void setAtClause(AtClause value);

	/**
	 * Returns the value of the '<em><b>Component Clause</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Clause</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Clause</em>' containment reference.
	 * @see #setComponentClause(ComponentClause)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ComponentClause()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='component_clause' namespace='##targetNamespace'"
	 * @generated
	 */
	ComponentClause getComponentClause();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getComponentClause <em>Component Clause</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component Clause</em>' containment reference.
	 * @see #getComponentClause()
	 * @generated
	 */
	void setComponentClause(ComponentClause value);

	/**
	 * Returns the value of the '<em><b>Comment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comment</em>' containment reference.
	 * @see #setComment(Comment)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_Comment()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='comment' namespace='##targetNamespace'"
	 * @generated
	 */
	Comment getComment();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getComment <em>Comment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comment</em>' containment reference.
	 * @see #getComment()
	 * @generated
	 */
	void setComment(Comment value);

	/**
	 * Returns the value of the '<em><b>All Calls Remote Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Calls Remote Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Calls Remote Pragma</em>' containment reference.
	 * @see #setAllCallsRemotePragma(AllCallsRemotePragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_AllCallsRemotePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='all_calls_remote_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AllCallsRemotePragma getAllCallsRemotePragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>All Calls Remote Pragma</em>' containment reference.
	 * @see #getAllCallsRemotePragma()
	 * @generated
	 */
	void setAllCallsRemotePragma(AllCallsRemotePragma value);

	/**
	 * Returns the value of the '<em><b>Asynchronous Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asynchronous Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asynchronous Pragma</em>' containment reference.
	 * @see #setAsynchronousPragma(AsynchronousPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_AsynchronousPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='asynchronous_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AsynchronousPragma getAsynchronousPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getAsynchronousPragma <em>Asynchronous Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Asynchronous Pragma</em>' containment reference.
	 * @see #getAsynchronousPragma()
	 * @generated
	 */
	void setAsynchronousPragma(AsynchronousPragma value);

	/**
	 * Returns the value of the '<em><b>Atomic Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Pragma</em>' containment reference.
	 * @see #setAtomicPragma(AtomicPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_AtomicPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='atomic_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AtomicPragma getAtomicPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getAtomicPragma <em>Atomic Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Atomic Pragma</em>' containment reference.
	 * @see #getAtomicPragma()
	 * @generated
	 */
	void setAtomicPragma(AtomicPragma value);

	/**
	 * Returns the value of the '<em><b>Atomic Components Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Components Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Components Pragma</em>' containment reference.
	 * @see #setAtomicComponentsPragma(AtomicComponentsPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_AtomicComponentsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='atomic_components_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AtomicComponentsPragma getAtomicComponentsPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Atomic Components Pragma</em>' containment reference.
	 * @see #getAtomicComponentsPragma()
	 * @generated
	 */
	void setAtomicComponentsPragma(AtomicComponentsPragma value);

	/**
	 * Returns the value of the '<em><b>Attach Handler Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attach Handler Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attach Handler Pragma</em>' containment reference.
	 * @see #setAttachHandlerPragma(AttachHandlerPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_AttachHandlerPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='attach_handler_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AttachHandlerPragma getAttachHandlerPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getAttachHandlerPragma <em>Attach Handler Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attach Handler Pragma</em>' containment reference.
	 * @see #getAttachHandlerPragma()
	 * @generated
	 */
	void setAttachHandlerPragma(AttachHandlerPragma value);

	/**
	 * Returns the value of the '<em><b>Controlled Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controlled Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controlled Pragma</em>' containment reference.
	 * @see #setControlledPragma(ControlledPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ControlledPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='controlled_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ControlledPragma getControlledPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getControlledPragma <em>Controlled Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Controlled Pragma</em>' containment reference.
	 * @see #getControlledPragma()
	 * @generated
	 */
	void setControlledPragma(ControlledPragma value);

	/**
	 * Returns the value of the '<em><b>Convention Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Convention Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Convention Pragma</em>' containment reference.
	 * @see #setConventionPragma(ConventionPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ConventionPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='convention_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ConventionPragma getConventionPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getConventionPragma <em>Convention Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Convention Pragma</em>' containment reference.
	 * @see #getConventionPragma()
	 * @generated
	 */
	void setConventionPragma(ConventionPragma value);

	/**
	 * Returns the value of the '<em><b>Discard Names Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discard Names Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discard Names Pragma</em>' containment reference.
	 * @see #setDiscardNamesPragma(DiscardNamesPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_DiscardNamesPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='discard_names_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscardNamesPragma getDiscardNamesPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getDiscardNamesPragma <em>Discard Names Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discard Names Pragma</em>' containment reference.
	 * @see #getDiscardNamesPragma()
	 * @generated
	 */
	void setDiscardNamesPragma(DiscardNamesPragma value);

	/**
	 * Returns the value of the '<em><b>Elaborate Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Pragma</em>' containment reference.
	 * @see #setElaboratePragma(ElaboratePragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ElaboratePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='elaborate_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ElaboratePragma getElaboratePragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getElaboratePragma <em>Elaborate Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Elaborate Pragma</em>' containment reference.
	 * @see #getElaboratePragma()
	 * @generated
	 */
	void setElaboratePragma(ElaboratePragma value);

	/**
	 * Returns the value of the '<em><b>Elaborate All Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate All Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate All Pragma</em>' containment reference.
	 * @see #setElaborateAllPragma(ElaborateAllPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ElaborateAllPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='elaborate_all_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ElaborateAllPragma getElaborateAllPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getElaborateAllPragma <em>Elaborate All Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Elaborate All Pragma</em>' containment reference.
	 * @see #getElaborateAllPragma()
	 * @generated
	 */
	void setElaborateAllPragma(ElaborateAllPragma value);

	/**
	 * Returns the value of the '<em><b>Elaborate Body Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Body Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Body Pragma</em>' containment reference.
	 * @see #setElaborateBodyPragma(ElaborateBodyPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ElaborateBodyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='elaborate_body_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ElaborateBodyPragma getElaborateBodyPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Elaborate Body Pragma</em>' containment reference.
	 * @see #getElaborateBodyPragma()
	 * @generated
	 */
	void setElaborateBodyPragma(ElaborateBodyPragma value);

	/**
	 * Returns the value of the '<em><b>Export Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Export Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Export Pragma</em>' containment reference.
	 * @see #setExportPragma(ExportPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ExportPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='export_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ExportPragma getExportPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getExportPragma <em>Export Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Export Pragma</em>' containment reference.
	 * @see #getExportPragma()
	 * @generated
	 */
	void setExportPragma(ExportPragma value);

	/**
	 * Returns the value of the '<em><b>Import Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Import Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Import Pragma</em>' containment reference.
	 * @see #setImportPragma(ImportPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ImportPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='import_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ImportPragma getImportPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getImportPragma <em>Import Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Import Pragma</em>' containment reference.
	 * @see #getImportPragma()
	 * @generated
	 */
	void setImportPragma(ImportPragma value);

	/**
	 * Returns the value of the '<em><b>Inline Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inline Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inline Pragma</em>' containment reference.
	 * @see #setInlinePragma(InlinePragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_InlinePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='inline_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	InlinePragma getInlinePragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getInlinePragma <em>Inline Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inline Pragma</em>' containment reference.
	 * @see #getInlinePragma()
	 * @generated
	 */
	void setInlinePragma(InlinePragma value);

	/**
	 * Returns the value of the '<em><b>Inspection Point Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inspection Point Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inspection Point Pragma</em>' containment reference.
	 * @see #setInspectionPointPragma(InspectionPointPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_InspectionPointPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='inspection_point_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	InspectionPointPragma getInspectionPointPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getInspectionPointPragma <em>Inspection Point Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inspection Point Pragma</em>' containment reference.
	 * @see #getInspectionPointPragma()
	 * @generated
	 */
	void setInspectionPointPragma(InspectionPointPragma value);

	/**
	 * Returns the value of the '<em><b>Interrupt Handler Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Handler Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Handler Pragma</em>' containment reference.
	 * @see #setInterruptHandlerPragma(InterruptHandlerPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_InterruptHandlerPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='interrupt_handler_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	InterruptHandlerPragma getInterruptHandlerPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interrupt Handler Pragma</em>' containment reference.
	 * @see #getInterruptHandlerPragma()
	 * @generated
	 */
	void setInterruptHandlerPragma(InterruptHandlerPragma value);

	/**
	 * Returns the value of the '<em><b>Interrupt Priority Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Priority Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Priority Pragma</em>' containment reference.
	 * @see #setInterruptPriorityPragma(InterruptPriorityPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_InterruptPriorityPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='interrupt_priority_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	InterruptPriorityPragma getInterruptPriorityPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interrupt Priority Pragma</em>' containment reference.
	 * @see #getInterruptPriorityPragma()
	 * @generated
	 */
	void setInterruptPriorityPragma(InterruptPriorityPragma value);

	/**
	 * Returns the value of the '<em><b>Linker Options Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Linker Options Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Linker Options Pragma</em>' containment reference.
	 * @see #setLinkerOptionsPragma(LinkerOptionsPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_LinkerOptionsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='linker_options_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	LinkerOptionsPragma getLinkerOptionsPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getLinkerOptionsPragma <em>Linker Options Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Linker Options Pragma</em>' containment reference.
	 * @see #getLinkerOptionsPragma()
	 * @generated
	 */
	void setLinkerOptionsPragma(LinkerOptionsPragma value);

	/**
	 * Returns the value of the '<em><b>List Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Pragma</em>' containment reference.
	 * @see #setListPragma(ListPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ListPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='list_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ListPragma getListPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getListPragma <em>List Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>List Pragma</em>' containment reference.
	 * @see #getListPragma()
	 * @generated
	 */
	void setListPragma(ListPragma value);

	/**
	 * Returns the value of the '<em><b>Locking Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Locking Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Locking Policy Pragma</em>' containment reference.
	 * @see #setLockingPolicyPragma(LockingPolicyPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_LockingPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='locking_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	LockingPolicyPragma getLockingPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getLockingPolicyPragma <em>Locking Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Locking Policy Pragma</em>' containment reference.
	 * @see #getLockingPolicyPragma()
	 * @generated
	 */
	void setLockingPolicyPragma(LockingPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Normalize Scalars Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Normalize Scalars Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Normalize Scalars Pragma</em>' containment reference.
	 * @see #setNormalizeScalarsPragma(NormalizeScalarsPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_NormalizeScalarsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='normalize_scalars_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	NormalizeScalarsPragma getNormalizeScalarsPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Normalize Scalars Pragma</em>' containment reference.
	 * @see #getNormalizeScalarsPragma()
	 * @generated
	 */
	void setNormalizeScalarsPragma(NormalizeScalarsPragma value);

	/**
	 * Returns the value of the '<em><b>Optimize Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Optimize Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Optimize Pragma</em>' containment reference.
	 * @see #setOptimizePragma(OptimizePragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_OptimizePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='optimize_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	OptimizePragma getOptimizePragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getOptimizePragma <em>Optimize Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Optimize Pragma</em>' containment reference.
	 * @see #getOptimizePragma()
	 * @generated
	 */
	void setOptimizePragma(OptimizePragma value);

	/**
	 * Returns the value of the '<em><b>Pack Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pack Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pack Pragma</em>' containment reference.
	 * @see #setPackPragma(PackPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_PackPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='pack_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PackPragma getPackPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getPackPragma <em>Pack Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pack Pragma</em>' containment reference.
	 * @see #getPackPragma()
	 * @generated
	 */
	void setPackPragma(PackPragma value);

	/**
	 * Returns the value of the '<em><b>Page Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Page Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Page Pragma</em>' containment reference.
	 * @see #setPagePragma(PagePragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_PagePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='page_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PagePragma getPagePragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getPagePragma <em>Page Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Page Pragma</em>' containment reference.
	 * @see #getPagePragma()
	 * @generated
	 */
	void setPagePragma(PagePragma value);

	/**
	 * Returns the value of the '<em><b>Preelaborate Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborate Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborate Pragma</em>' containment reference.
	 * @see #setPreelaboratePragma(PreelaboratePragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_PreelaboratePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='preelaborate_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PreelaboratePragma getPreelaboratePragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getPreelaboratePragma <em>Preelaborate Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preelaborate Pragma</em>' containment reference.
	 * @see #getPreelaboratePragma()
	 * @generated
	 */
	void setPreelaboratePragma(PreelaboratePragma value);

	/**
	 * Returns the value of the '<em><b>Priority Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Pragma</em>' containment reference.
	 * @see #setPriorityPragma(PriorityPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_PriorityPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='priority_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PriorityPragma getPriorityPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getPriorityPragma <em>Priority Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Priority Pragma</em>' containment reference.
	 * @see #getPriorityPragma()
	 * @generated
	 */
	void setPriorityPragma(PriorityPragma value);

	/**
	 * Returns the value of the '<em><b>Pure Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pure Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pure Pragma</em>' containment reference.
	 * @see #setPurePragma(PurePragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_PurePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='pure_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PurePragma getPurePragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getPurePragma <em>Pure Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pure Pragma</em>' containment reference.
	 * @see #getPurePragma()
	 * @generated
	 */
	void setPurePragma(PurePragma value);

	/**
	 * Returns the value of the '<em><b>Queuing Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Queuing Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Queuing Policy Pragma</em>' containment reference.
	 * @see #setQueuingPolicyPragma(QueuingPolicyPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_QueuingPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='queuing_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	QueuingPolicyPragma getQueuingPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Queuing Policy Pragma</em>' containment reference.
	 * @see #getQueuingPolicyPragma()
	 * @generated
	 */
	void setQueuingPolicyPragma(QueuingPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Remote Call Interface Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Call Interface Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Call Interface Pragma</em>' containment reference.
	 * @see #setRemoteCallInterfacePragma(RemoteCallInterfacePragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_RemoteCallInterfacePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='remote_call_interface_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	RemoteCallInterfacePragma getRemoteCallInterfacePragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Remote Call Interface Pragma</em>' containment reference.
	 * @see #getRemoteCallInterfacePragma()
	 * @generated
	 */
	void setRemoteCallInterfacePragma(RemoteCallInterfacePragma value);

	/**
	 * Returns the value of the '<em><b>Remote Types Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Types Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Types Pragma</em>' containment reference.
	 * @see #setRemoteTypesPragma(RemoteTypesPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_RemoteTypesPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='remote_types_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	RemoteTypesPragma getRemoteTypesPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getRemoteTypesPragma <em>Remote Types Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Remote Types Pragma</em>' containment reference.
	 * @see #getRemoteTypesPragma()
	 * @generated
	 */
	void setRemoteTypesPragma(RemoteTypesPragma value);

	/**
	 * Returns the value of the '<em><b>Restrictions Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Restrictions Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Restrictions Pragma</em>' containment reference.
	 * @see #setRestrictionsPragma(RestrictionsPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_RestrictionsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='restrictions_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	RestrictionsPragma getRestrictionsPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getRestrictionsPragma <em>Restrictions Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Restrictions Pragma</em>' containment reference.
	 * @see #getRestrictionsPragma()
	 * @generated
	 */
	void setRestrictionsPragma(RestrictionsPragma value);

	/**
	 * Returns the value of the '<em><b>Reviewable Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reviewable Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reviewable Pragma</em>' containment reference.
	 * @see #setReviewablePragma(ReviewablePragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ReviewablePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='reviewable_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ReviewablePragma getReviewablePragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getReviewablePragma <em>Reviewable Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reviewable Pragma</em>' containment reference.
	 * @see #getReviewablePragma()
	 * @generated
	 */
	void setReviewablePragma(ReviewablePragma value);

	/**
	 * Returns the value of the '<em><b>Shared Passive Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shared Passive Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shared Passive Pragma</em>' containment reference.
	 * @see #setSharedPassivePragma(SharedPassivePragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_SharedPassivePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='shared_passive_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	SharedPassivePragma getSharedPassivePragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getSharedPassivePragma <em>Shared Passive Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Shared Passive Pragma</em>' containment reference.
	 * @see #getSharedPassivePragma()
	 * @generated
	 */
	void setSharedPassivePragma(SharedPassivePragma value);

	/**
	 * Returns the value of the '<em><b>Storage Size Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Size Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Size Pragma</em>' containment reference.
	 * @see #setStorageSizePragma(StorageSizePragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_StorageSizePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='storage_size_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	StorageSizePragma getStorageSizePragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getStorageSizePragma <em>Storage Size Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Storage Size Pragma</em>' containment reference.
	 * @see #getStorageSizePragma()
	 * @generated
	 */
	void setStorageSizePragma(StorageSizePragma value);

	/**
	 * Returns the value of the '<em><b>Suppress Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Suppress Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Suppress Pragma</em>' containment reference.
	 * @see #setSuppressPragma(SuppressPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_SuppressPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='suppress_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	SuppressPragma getSuppressPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getSuppressPragma <em>Suppress Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Suppress Pragma</em>' containment reference.
	 * @see #getSuppressPragma()
	 * @generated
	 */
	void setSuppressPragma(SuppressPragma value);

	/**
	 * Returns the value of the '<em><b>Task Dispatching Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Dispatching Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Dispatching Policy Pragma</em>' containment reference.
	 * @see #setTaskDispatchingPolicyPragma(TaskDispatchingPolicyPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_TaskDispatchingPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='task_dispatching_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	TaskDispatchingPolicyPragma getTaskDispatchingPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Dispatching Policy Pragma</em>' containment reference.
	 * @see #getTaskDispatchingPolicyPragma()
	 * @generated
	 */
	void setTaskDispatchingPolicyPragma(TaskDispatchingPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Volatile Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Pragma</em>' containment reference.
	 * @see #setVolatilePragma(VolatilePragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_VolatilePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='volatile_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	VolatilePragma getVolatilePragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getVolatilePragma <em>Volatile Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Volatile Pragma</em>' containment reference.
	 * @see #getVolatilePragma()
	 * @generated
	 */
	void setVolatilePragma(VolatilePragma value);

	/**
	 * Returns the value of the '<em><b>Volatile Components Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Components Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Components Pragma</em>' containment reference.
	 * @see #setVolatileComponentsPragma(VolatileComponentsPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_VolatileComponentsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='volatile_components_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	VolatileComponentsPragma getVolatileComponentsPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Volatile Components Pragma</em>' containment reference.
	 * @see #getVolatileComponentsPragma()
	 * @generated
	 */
	void setVolatileComponentsPragma(VolatileComponentsPragma value);

	/**
	 * Returns the value of the '<em><b>Assert Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assert Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assert Pragma</em>' containment reference.
	 * @see #setAssertPragma(AssertPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_AssertPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='assert_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AssertPragma getAssertPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getAssertPragma <em>Assert Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assert Pragma</em>' containment reference.
	 * @see #getAssertPragma()
	 * @generated
	 */
	void setAssertPragma(AssertPragma value);

	/**
	 * Returns the value of the '<em><b>Assertion Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assertion Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assertion Policy Pragma</em>' containment reference.
	 * @see #setAssertionPolicyPragma(AssertionPolicyPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_AssertionPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='assertion_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AssertionPolicyPragma getAssertionPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assertion Policy Pragma</em>' containment reference.
	 * @see #getAssertionPolicyPragma()
	 * @generated
	 */
	void setAssertionPolicyPragma(AssertionPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Detect Blocking Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Detect Blocking Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Detect Blocking Pragma</em>' containment reference.
	 * @see #setDetectBlockingPragma(DetectBlockingPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_DetectBlockingPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='detect_blocking_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	DetectBlockingPragma getDetectBlockingPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Detect Blocking Pragma</em>' containment reference.
	 * @see #getDetectBlockingPragma()
	 * @generated
	 */
	void setDetectBlockingPragma(DetectBlockingPragma value);

	/**
	 * Returns the value of the '<em><b>No Return Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>No Return Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>No Return Pragma</em>' containment reference.
	 * @see #setNoReturnPragma(NoReturnPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_NoReturnPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='no_return_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	NoReturnPragma getNoReturnPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getNoReturnPragma <em>No Return Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>No Return Pragma</em>' containment reference.
	 * @see #getNoReturnPragma()
	 * @generated
	 */
	void setNoReturnPragma(NoReturnPragma value);

	/**
	 * Returns the value of the '<em><b>Partition Elaboration Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partition Elaboration Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partition Elaboration Policy Pragma</em>' containment reference.
	 * @see #setPartitionElaborationPolicyPragma(PartitionElaborationPolicyPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_PartitionElaborationPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='partition_elaboration_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PartitionElaborationPolicyPragma getPartitionElaborationPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Partition Elaboration Policy Pragma</em>' containment reference.
	 * @see #getPartitionElaborationPolicyPragma()
	 * @generated
	 */
	void setPartitionElaborationPolicyPragma(PartitionElaborationPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Preelaborable Initialization Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborable Initialization Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborable Initialization Pragma</em>' containment reference.
	 * @see #setPreelaborableInitializationPragma(PreelaborableInitializationPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_PreelaborableInitializationPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='preelaborable_initialization_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PreelaborableInitializationPragma getPreelaborableInitializationPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preelaborable Initialization Pragma</em>' containment reference.
	 * @see #getPreelaborableInitializationPragma()
	 * @generated
	 */
	void setPreelaborableInitializationPragma(PreelaborableInitializationPragma value);

	/**
	 * Returns the value of the '<em><b>Priority Specific Dispatching Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Specific Dispatching Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Specific Dispatching Pragma</em>' containment reference.
	 * @see #setPrioritySpecificDispatchingPragma(PrioritySpecificDispatchingPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_PrioritySpecificDispatchingPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='priority_specific_dispatching_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PrioritySpecificDispatchingPragma getPrioritySpecificDispatchingPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Priority Specific Dispatching Pragma</em>' containment reference.
	 * @see #getPrioritySpecificDispatchingPragma()
	 * @generated
	 */
	void setPrioritySpecificDispatchingPragma(PrioritySpecificDispatchingPragma value);

	/**
	 * Returns the value of the '<em><b>Profile Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Profile Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Profile Pragma</em>' containment reference.
	 * @see #setProfilePragma(ProfilePragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ProfilePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='profile_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ProfilePragma getProfilePragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getProfilePragma <em>Profile Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Profile Pragma</em>' containment reference.
	 * @see #getProfilePragma()
	 * @generated
	 */
	void setProfilePragma(ProfilePragma value);

	/**
	 * Returns the value of the '<em><b>Relative Deadline Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relative Deadline Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relative Deadline Pragma</em>' containment reference.
	 * @see #setRelativeDeadlinePragma(RelativeDeadlinePragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_RelativeDeadlinePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='relative_deadline_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	RelativeDeadlinePragma getRelativeDeadlinePragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relative Deadline Pragma</em>' containment reference.
	 * @see #getRelativeDeadlinePragma()
	 * @generated
	 */
	void setRelativeDeadlinePragma(RelativeDeadlinePragma value);

	/**
	 * Returns the value of the '<em><b>Unchecked Union Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unchecked Union Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unchecked Union Pragma</em>' containment reference.
	 * @see #setUncheckedUnionPragma(UncheckedUnionPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_UncheckedUnionPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unchecked_union_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	UncheckedUnionPragma getUncheckedUnionPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unchecked Union Pragma</em>' containment reference.
	 * @see #getUncheckedUnionPragma()
	 * @generated
	 */
	void setUncheckedUnionPragma(UncheckedUnionPragma value);

	/**
	 * Returns the value of the '<em><b>Unsuppress Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unsuppress Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unsuppress Pragma</em>' containment reference.
	 * @see #setUnsuppressPragma(UnsuppressPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_UnsuppressPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unsuppress_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	UnsuppressPragma getUnsuppressPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getUnsuppressPragma <em>Unsuppress Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unsuppress Pragma</em>' containment reference.
	 * @see #getUnsuppressPragma()
	 * @generated
	 */
	void setUnsuppressPragma(UnsuppressPragma value);

	/**
	 * Returns the value of the '<em><b>Default Storage Pool Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Storage Pool Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Storage Pool Pragma</em>' containment reference.
	 * @see #setDefaultStoragePoolPragma(DefaultStoragePoolPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_DefaultStoragePoolPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='default_storage_pool_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	DefaultStoragePoolPragma getDefaultStoragePoolPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Storage Pool Pragma</em>' containment reference.
	 * @see #getDefaultStoragePoolPragma()
	 * @generated
	 */
	void setDefaultStoragePoolPragma(DefaultStoragePoolPragma value);

	/**
	 * Returns the value of the '<em><b>Dispatching Domain Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dispatching Domain Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dispatching Domain Pragma</em>' containment reference.
	 * @see #setDispatchingDomainPragma(DispatchingDomainPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_DispatchingDomainPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='dispatching_domain_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	DispatchingDomainPragma getDispatchingDomainPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dispatching Domain Pragma</em>' containment reference.
	 * @see #getDispatchingDomainPragma()
	 * @generated
	 */
	void setDispatchingDomainPragma(DispatchingDomainPragma value);

	/**
	 * Returns the value of the '<em><b>Cpu Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cpu Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cpu Pragma</em>' containment reference.
	 * @see #setCpuPragma(CpuPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_CpuPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='cpu_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	CpuPragma getCpuPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getCpuPragma <em>Cpu Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cpu Pragma</em>' containment reference.
	 * @see #getCpuPragma()
	 * @generated
	 */
	void setCpuPragma(CpuPragma value);

	/**
	 * Returns the value of the '<em><b>Independent Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Pragma</em>' containment reference.
	 * @see #setIndependentPragma(IndependentPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_IndependentPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='independent_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	IndependentPragma getIndependentPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getIndependentPragma <em>Independent Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Independent Pragma</em>' containment reference.
	 * @see #getIndependentPragma()
	 * @generated
	 */
	void setIndependentPragma(IndependentPragma value);

	/**
	 * Returns the value of the '<em><b>Independent Components Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Components Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Components Pragma</em>' containment reference.
	 * @see #setIndependentComponentsPragma(IndependentComponentsPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_IndependentComponentsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='independent_components_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	IndependentComponentsPragma getIndependentComponentsPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getIndependentComponentsPragma <em>Independent Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Independent Components Pragma</em>' containment reference.
	 * @see #getIndependentComponentsPragma()
	 * @generated
	 */
	void setIndependentComponentsPragma(IndependentComponentsPragma value);

	/**
	 * Returns the value of the '<em><b>Implementation Defined Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implementation Defined Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implementation Defined Pragma</em>' containment reference.
	 * @see #setImplementationDefinedPragma(ImplementationDefinedPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_ImplementationDefinedPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='implementation_defined_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ImplementationDefinedPragma getImplementationDefinedPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Implementation Defined Pragma</em>' containment reference.
	 * @see #getImplementationDefinedPragma()
	 * @generated
	 */
	void setImplementationDefinedPragma(ImplementationDefinedPragma value);

	/**
	 * Returns the value of the '<em><b>Unknown Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unknown Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unknown Pragma</em>' containment reference.
	 * @see #setUnknownPragma(UnknownPragma)
	 * @see Ada.AdaPackage#getDeclarativeItemClass_UnknownPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unknown_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	UnknownPragma getUnknownPragma();

	/**
	 * Sets the value of the '{@link Ada.DeclarativeItemClass#getUnknownPragma <em>Unknown Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unknown Pragma</em>' containment reference.
	 * @see #getUnknownPragma()
	 * @generated
	 */
	void setUnknownPragma(UnknownPragma value);

} // DeclarativeItemClass
