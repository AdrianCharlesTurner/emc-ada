/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.ConstraintClass;
import Ada.DiscreteSubtypeIndicationAsSubtypeDefinition;
import Ada.ExpressionClass;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Discrete Subtype Indication As Subtype Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.DiscreteSubtypeIndicationAsSubtypeDefinitionImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.DiscreteSubtypeIndicationAsSubtypeDefinitionImpl#getSubtypeMarkQ <em>Subtype Mark Q</em>}</li>
 *   <li>{@link Ada.impl.DiscreteSubtypeIndicationAsSubtypeDefinitionImpl#getSubtypeConstraintQ <em>Subtype Constraint Q</em>}</li>
 *   <li>{@link Ada.impl.DiscreteSubtypeIndicationAsSubtypeDefinitionImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DiscreteSubtypeIndicationAsSubtypeDefinitionImpl extends MinimalEObjectImpl.Container implements DiscreteSubtypeIndicationAsSubtypeDefinition {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getSubtypeMarkQ() <em>Subtype Mark Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubtypeMarkQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass subtypeMarkQ;

	/**
	 * The cached value of the '{@link #getSubtypeConstraintQ() <em>Subtype Constraint Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubtypeConstraintQ()
	 * @generated
	 * @ordered
	 */
	protected ConstraintClass subtypeConstraintQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DiscreteSubtypeIndicationAsSubtypeDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getDiscreteSubtypeIndicationAsSubtypeDefinition();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getSubtypeMarkQ() {
		return subtypeMarkQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSubtypeMarkQ(ExpressionClass newSubtypeMarkQ, NotificationChain msgs) {
		ExpressionClass oldSubtypeMarkQ = subtypeMarkQ;
		subtypeMarkQ = newSubtypeMarkQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SUBTYPE_MARK_Q, oldSubtypeMarkQ, newSubtypeMarkQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubtypeMarkQ(ExpressionClass newSubtypeMarkQ) {
		if (newSubtypeMarkQ != subtypeMarkQ) {
			NotificationChain msgs = null;
			if (subtypeMarkQ != null)
				msgs = ((InternalEObject)subtypeMarkQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SUBTYPE_MARK_Q, null, msgs);
			if (newSubtypeMarkQ != null)
				msgs = ((InternalEObject)newSubtypeMarkQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SUBTYPE_MARK_Q, null, msgs);
			msgs = basicSetSubtypeMarkQ(newSubtypeMarkQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SUBTYPE_MARK_Q, newSubtypeMarkQ, newSubtypeMarkQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstraintClass getSubtypeConstraintQ() {
		return subtypeConstraintQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSubtypeConstraintQ(ConstraintClass newSubtypeConstraintQ, NotificationChain msgs) {
		ConstraintClass oldSubtypeConstraintQ = subtypeConstraintQ;
		subtypeConstraintQ = newSubtypeConstraintQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SUBTYPE_CONSTRAINT_Q, oldSubtypeConstraintQ, newSubtypeConstraintQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubtypeConstraintQ(ConstraintClass newSubtypeConstraintQ) {
		if (newSubtypeConstraintQ != subtypeConstraintQ) {
			NotificationChain msgs = null;
			if (subtypeConstraintQ != null)
				msgs = ((InternalEObject)subtypeConstraintQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SUBTYPE_CONSTRAINT_Q, null, msgs);
			if (newSubtypeConstraintQ != null)
				msgs = ((InternalEObject)newSubtypeConstraintQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SUBTYPE_CONSTRAINT_Q, null, msgs);
			msgs = basicSetSubtypeConstraintQ(newSubtypeConstraintQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SUBTYPE_CONSTRAINT_Q, newSubtypeConstraintQ, newSubtypeConstraintQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SUBTYPE_MARK_Q:
				return basicSetSubtypeMarkQ(null, msgs);
			case AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SUBTYPE_CONSTRAINT_Q:
				return basicSetSubtypeConstraintQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SLOC:
				return getSloc();
			case AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SUBTYPE_MARK_Q:
				return getSubtypeMarkQ();
			case AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SUBTYPE_CONSTRAINT_Q:
				return getSubtypeConstraintQ();
			case AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SUBTYPE_MARK_Q:
				setSubtypeMarkQ((ExpressionClass)newValue);
				return;
			case AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SUBTYPE_CONSTRAINT_Q:
				setSubtypeConstraintQ((ConstraintClass)newValue);
				return;
			case AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SUBTYPE_MARK_Q:
				setSubtypeMarkQ((ExpressionClass)null);
				return;
			case AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SUBTYPE_CONSTRAINT_Q:
				setSubtypeConstraintQ((ConstraintClass)null);
				return;
			case AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SLOC:
				return sloc != null;
			case AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SUBTYPE_MARK_Q:
				return subtypeMarkQ != null;
			case AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__SUBTYPE_CONSTRAINT_Q:
				return subtypeConstraintQ != null;
			case AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //DiscreteSubtypeIndicationAsSubtypeDefinitionImpl
