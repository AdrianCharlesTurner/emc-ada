/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Is Not Overriding Declaration QType10</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.IsNotOverridingDeclarationQType10#getNotOverriding <em>Not Overriding</em>}</li>
 *   <li>{@link Ada.IsNotOverridingDeclarationQType10#getNotAnElement <em>Not An Element</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getIsNotOverridingDeclarationQType10()
 * @model extendedMetaData="name='is_not_overriding_declaration_q_._10_._type' kind='elementOnly'"
 * @generated
 */
public interface IsNotOverridingDeclarationQType10 extends EObject {
	/**
	 * Returns the value of the '<em><b>Not Overriding</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not Overriding</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not Overriding</em>' containment reference.
	 * @see #setNotOverriding(NotOverriding)
	 * @see Ada.AdaPackage#getIsNotOverridingDeclarationQType10_NotOverriding()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not_overriding' namespace='##targetNamespace'"
	 * @generated
	 */
	NotOverriding getNotOverriding();

	/**
	 * Sets the value of the '{@link Ada.IsNotOverridingDeclarationQType10#getNotOverriding <em>Not Overriding</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not Overriding</em>' containment reference.
	 * @see #getNotOverriding()
	 * @generated
	 */
	void setNotOverriding(NotOverriding value);

	/**
	 * Returns the value of the '<em><b>Not An Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not An Element</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not An Element</em>' containment reference.
	 * @see #setNotAnElement(NotAnElement)
	 * @see Ada.AdaPackage#getIsNotOverridingDeclarationQType10_NotAnElement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not_an_element' namespace='##targetNamespace'"
	 * @generated
	 */
	NotAnElement getNotAnElement();

	/**
	 * Sets the value of the '{@link Ada.IsNotOverridingDeclarationQType10#getNotAnElement <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not An Element</em>' containment reference.
	 * @see #getNotAnElement()
	 * @generated
	 */
	void setNotAnElement(NotAnElement value);

} // IsNotOverridingDeclarationQType10
