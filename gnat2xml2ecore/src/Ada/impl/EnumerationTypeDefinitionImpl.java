/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.DeclarationList;
import Ada.EnumerationTypeDefinition;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Enumeration Type Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.EnumerationTypeDefinitionImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.EnumerationTypeDefinitionImpl#getEnumerationLiteralDeclarationsQl <em>Enumeration Literal Declarations Ql</em>}</li>
 *   <li>{@link Ada.impl.EnumerationTypeDefinitionImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EnumerationTypeDefinitionImpl extends MinimalEObjectImpl.Container implements EnumerationTypeDefinition {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getEnumerationLiteralDeclarationsQl() <em>Enumeration Literal Declarations Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnumerationLiteralDeclarationsQl()
	 * @generated
	 * @ordered
	 */
	protected DeclarationList enumerationLiteralDeclarationsQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EnumerationTypeDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getEnumerationTypeDefinition();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ENUMERATION_TYPE_DEFINITION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENUMERATION_TYPE_DEFINITION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENUMERATION_TYPE_DEFINITION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ENUMERATION_TYPE_DEFINITION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeclarationList getEnumerationLiteralDeclarationsQl() {
		return enumerationLiteralDeclarationsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEnumerationLiteralDeclarationsQl(DeclarationList newEnumerationLiteralDeclarationsQl, NotificationChain msgs) {
		DeclarationList oldEnumerationLiteralDeclarationsQl = enumerationLiteralDeclarationsQl;
		enumerationLiteralDeclarationsQl = newEnumerationLiteralDeclarationsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ENUMERATION_TYPE_DEFINITION__ENUMERATION_LITERAL_DECLARATIONS_QL, oldEnumerationLiteralDeclarationsQl, newEnumerationLiteralDeclarationsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnumerationLiteralDeclarationsQl(DeclarationList newEnumerationLiteralDeclarationsQl) {
		if (newEnumerationLiteralDeclarationsQl != enumerationLiteralDeclarationsQl) {
			NotificationChain msgs = null;
			if (enumerationLiteralDeclarationsQl != null)
				msgs = ((InternalEObject)enumerationLiteralDeclarationsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENUMERATION_TYPE_DEFINITION__ENUMERATION_LITERAL_DECLARATIONS_QL, null, msgs);
			if (newEnumerationLiteralDeclarationsQl != null)
				msgs = ((InternalEObject)newEnumerationLiteralDeclarationsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENUMERATION_TYPE_DEFINITION__ENUMERATION_LITERAL_DECLARATIONS_QL, null, msgs);
			msgs = basicSetEnumerationLiteralDeclarationsQl(newEnumerationLiteralDeclarationsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ENUMERATION_TYPE_DEFINITION__ENUMERATION_LITERAL_DECLARATIONS_QL, newEnumerationLiteralDeclarationsQl, newEnumerationLiteralDeclarationsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ENUMERATION_TYPE_DEFINITION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.ENUMERATION_TYPE_DEFINITION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.ENUMERATION_TYPE_DEFINITION__ENUMERATION_LITERAL_DECLARATIONS_QL:
				return basicSetEnumerationLiteralDeclarationsQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.ENUMERATION_TYPE_DEFINITION__SLOC:
				return getSloc();
			case AdaPackage.ENUMERATION_TYPE_DEFINITION__ENUMERATION_LITERAL_DECLARATIONS_QL:
				return getEnumerationLiteralDeclarationsQl();
			case AdaPackage.ENUMERATION_TYPE_DEFINITION__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.ENUMERATION_TYPE_DEFINITION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.ENUMERATION_TYPE_DEFINITION__ENUMERATION_LITERAL_DECLARATIONS_QL:
				setEnumerationLiteralDeclarationsQl((DeclarationList)newValue);
				return;
			case AdaPackage.ENUMERATION_TYPE_DEFINITION__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.ENUMERATION_TYPE_DEFINITION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.ENUMERATION_TYPE_DEFINITION__ENUMERATION_LITERAL_DECLARATIONS_QL:
				setEnumerationLiteralDeclarationsQl((DeclarationList)null);
				return;
			case AdaPackage.ENUMERATION_TYPE_DEFINITION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.ENUMERATION_TYPE_DEFINITION__SLOC:
				return sloc != null;
			case AdaPackage.ENUMERATION_TYPE_DEFINITION__ENUMERATION_LITERAL_DECLARATIONS_QL:
				return enumerationLiteralDeclarationsQl != null;
			case AdaPackage.ENUMERATION_TYPE_DEFINITION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //EnumerationTypeDefinitionImpl
