/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Protected Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.ProtectedDefinition#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.ProtectedDefinition#getVisiblePartItemsQl <em>Visible Part Items Ql</em>}</li>
 *   <li>{@link Ada.ProtectedDefinition#getPrivatePartItemsQl <em>Private Part Items Ql</em>}</li>
 *   <li>{@link Ada.ProtectedDefinition#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getProtectedDefinition()
 * @model extendedMetaData="name='Protected_Definition' kind='elementOnly'"
 * @generated
 */
public interface ProtectedDefinition extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getProtectedDefinition_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.ProtectedDefinition#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Visible Part Items Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Visible Part Items Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Visible Part Items Ql</em>' containment reference.
	 * @see #setVisiblePartItemsQl(DeclarativeItemList)
	 * @see Ada.AdaPackage#getProtectedDefinition_VisiblePartItemsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='visible_part_items_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DeclarativeItemList getVisiblePartItemsQl();

	/**
	 * Sets the value of the '{@link Ada.ProtectedDefinition#getVisiblePartItemsQl <em>Visible Part Items Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Visible Part Items Ql</em>' containment reference.
	 * @see #getVisiblePartItemsQl()
	 * @generated
	 */
	void setVisiblePartItemsQl(DeclarativeItemList value);

	/**
	 * Returns the value of the '<em><b>Private Part Items Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Private Part Items Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Private Part Items Ql</em>' containment reference.
	 * @see #setPrivatePartItemsQl(DeclarativeItemList)
	 * @see Ada.AdaPackage#getProtectedDefinition_PrivatePartItemsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='private_part_items_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DeclarativeItemList getPrivatePartItemsQl();

	/**
	 * Sets the value of the '{@link Ada.ProtectedDefinition#getPrivatePartItemsQl <em>Private Part Items Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Private Part Items Ql</em>' containment reference.
	 * @see #getPrivatePartItemsQl()
	 * @generated
	 */
	void setPrivatePartItemsQl(DeclarativeItemList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getProtectedDefinition_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.ProtectedDefinition#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // ProtectedDefinition
