/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Element Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.ElementClass#getNotAnElement <em>Not An Element</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiningIdentifier <em>Defining Identifier</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiningCharacterLiteral <em>Defining Character Literal</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiningEnumerationLiteral <em>Defining Enumeration Literal</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiningAndOperator <em>Defining And Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiningOrOperator <em>Defining Or Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiningXorOperator <em>Defining Xor Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiningEqualOperator <em>Defining Equal Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiningNotEqualOperator <em>Defining Not Equal Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiningLessThanOperator <em>Defining Less Than Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiningLessThanOrEqualOperator <em>Defining Less Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiningGreaterThanOperator <em>Defining Greater Than Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiningGreaterThanOrEqualOperator <em>Defining Greater Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiningPlusOperator <em>Defining Plus Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiningMinusOperator <em>Defining Minus Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiningConcatenateOperator <em>Defining Concatenate Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiningUnaryPlusOperator <em>Defining Unary Plus Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiningUnaryMinusOperator <em>Defining Unary Minus Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiningMultiplyOperator <em>Defining Multiply Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiningDivideOperator <em>Defining Divide Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiningModOperator <em>Defining Mod Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiningRemOperator <em>Defining Rem Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiningExponentiateOperator <em>Defining Exponentiate Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiningAbsOperator <em>Defining Abs Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiningNotOperator <em>Defining Not Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiningExpandedName <em>Defining Expanded Name</em>}</li>
 *   <li>{@link Ada.ElementClass#getOrdinaryTypeDeclaration <em>Ordinary Type Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getTaskTypeDeclaration <em>Task Type Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getProtectedTypeDeclaration <em>Protected Type Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getIncompleteTypeDeclaration <em>Incomplete Type Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getTaggedIncompleteTypeDeclaration <em>Tagged Incomplete Type Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getPrivateTypeDeclaration <em>Private Type Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getPrivateExtensionDeclaration <em>Private Extension Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getSubtypeDeclaration <em>Subtype Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getVariableDeclaration <em>Variable Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getConstantDeclaration <em>Constant Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getDeferredConstantDeclaration <em>Deferred Constant Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getSingleTaskDeclaration <em>Single Task Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getSingleProtectedDeclaration <em>Single Protected Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getIntegerNumberDeclaration <em>Integer Number Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getRealNumberDeclaration <em>Real Number Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getEnumerationLiteralSpecification <em>Enumeration Literal Specification</em>}</li>
 *   <li>{@link Ada.ElementClass#getDiscriminantSpecification <em>Discriminant Specification</em>}</li>
 *   <li>{@link Ada.ElementClass#getComponentDeclaration <em>Component Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getLoopParameterSpecification <em>Loop Parameter Specification</em>}</li>
 *   <li>{@link Ada.ElementClass#getGeneralizedIteratorSpecification <em>Generalized Iterator Specification</em>}</li>
 *   <li>{@link Ada.ElementClass#getElementIteratorSpecification <em>Element Iterator Specification</em>}</li>
 *   <li>{@link Ada.ElementClass#getProcedureDeclaration <em>Procedure Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getFunctionDeclaration <em>Function Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getParameterSpecification <em>Parameter Specification</em>}</li>
 *   <li>{@link Ada.ElementClass#getProcedureBodyDeclaration <em>Procedure Body Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getFunctionBodyDeclaration <em>Function Body Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getReturnVariableSpecification <em>Return Variable Specification</em>}</li>
 *   <li>{@link Ada.ElementClass#getReturnConstantSpecification <em>Return Constant Specification</em>}</li>
 *   <li>{@link Ada.ElementClass#getNullProcedureDeclaration <em>Null Procedure Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getExpressionFunctionDeclaration <em>Expression Function Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getPackageDeclaration <em>Package Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getPackageBodyDeclaration <em>Package Body Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getObjectRenamingDeclaration <em>Object Renaming Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getExceptionRenamingDeclaration <em>Exception Renaming Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getPackageRenamingDeclaration <em>Package Renaming Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getProcedureRenamingDeclaration <em>Procedure Renaming Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getFunctionRenamingDeclaration <em>Function Renaming Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getGenericPackageRenamingDeclaration <em>Generic Package Renaming Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getGenericProcedureRenamingDeclaration <em>Generic Procedure Renaming Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getGenericFunctionRenamingDeclaration <em>Generic Function Renaming Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getTaskBodyDeclaration <em>Task Body Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getProtectedBodyDeclaration <em>Protected Body Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getEntryDeclaration <em>Entry Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getEntryBodyDeclaration <em>Entry Body Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getEntryIndexSpecification <em>Entry Index Specification</em>}</li>
 *   <li>{@link Ada.ElementClass#getProcedureBodyStub <em>Procedure Body Stub</em>}</li>
 *   <li>{@link Ada.ElementClass#getFunctionBodyStub <em>Function Body Stub</em>}</li>
 *   <li>{@link Ada.ElementClass#getPackageBodyStub <em>Package Body Stub</em>}</li>
 *   <li>{@link Ada.ElementClass#getTaskBodyStub <em>Task Body Stub</em>}</li>
 *   <li>{@link Ada.ElementClass#getProtectedBodyStub <em>Protected Body Stub</em>}</li>
 *   <li>{@link Ada.ElementClass#getExceptionDeclaration <em>Exception Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getChoiceParameterSpecification <em>Choice Parameter Specification</em>}</li>
 *   <li>{@link Ada.ElementClass#getGenericProcedureDeclaration <em>Generic Procedure Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getGenericFunctionDeclaration <em>Generic Function Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getGenericPackageDeclaration <em>Generic Package Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getPackageInstantiation <em>Package Instantiation</em>}</li>
 *   <li>{@link Ada.ElementClass#getProcedureInstantiation <em>Procedure Instantiation</em>}</li>
 *   <li>{@link Ada.ElementClass#getFunctionInstantiation <em>Function Instantiation</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalObjectDeclaration <em>Formal Object Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalTypeDeclaration <em>Formal Type Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalIncompleteTypeDeclaration <em>Formal Incomplete Type Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalProcedureDeclaration <em>Formal Procedure Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalFunctionDeclaration <em>Formal Function Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalPackageDeclaration <em>Formal Package Declaration</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalPackageDeclarationWithBox <em>Formal Package Declaration With Box</em>}</li>
 *   <li>{@link Ada.ElementClass#getDerivedTypeDefinition <em>Derived Type Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getDerivedRecordExtensionDefinition <em>Derived Record Extension Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getEnumerationTypeDefinition <em>Enumeration Type Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getSignedIntegerTypeDefinition <em>Signed Integer Type Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getModularTypeDefinition <em>Modular Type Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getRootIntegerDefinition <em>Root Integer Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getRootRealDefinition <em>Root Real Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getUniversalIntegerDefinition <em>Universal Integer Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getUniversalRealDefinition <em>Universal Real Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getUniversalFixedDefinition <em>Universal Fixed Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getFloatingPointDefinition <em>Floating Point Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getOrdinaryFixedPointDefinition <em>Ordinary Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getDecimalFixedPointDefinition <em>Decimal Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getUnconstrainedArrayDefinition <em>Unconstrained Array Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getConstrainedArrayDefinition <em>Constrained Array Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getRecordTypeDefinition <em>Record Type Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getTaggedRecordTypeDefinition <em>Tagged Record Type Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getOrdinaryInterface <em>Ordinary Interface</em>}</li>
 *   <li>{@link Ada.ElementClass#getLimitedInterface <em>Limited Interface</em>}</li>
 *   <li>{@link Ada.ElementClass#getTaskInterface <em>Task Interface</em>}</li>
 *   <li>{@link Ada.ElementClass#getProtectedInterface <em>Protected Interface</em>}</li>
 *   <li>{@link Ada.ElementClass#getSynchronizedInterface <em>Synchronized Interface</em>}</li>
 *   <li>{@link Ada.ElementClass#getPoolSpecificAccessToVariable <em>Pool Specific Access To Variable</em>}</li>
 *   <li>{@link Ada.ElementClass#getAccessToVariable <em>Access To Variable</em>}</li>
 *   <li>{@link Ada.ElementClass#getAccessToConstant <em>Access To Constant</em>}</li>
 *   <li>{@link Ada.ElementClass#getAccessToProcedure <em>Access To Procedure</em>}</li>
 *   <li>{@link Ada.ElementClass#getAccessToProtectedProcedure <em>Access To Protected Procedure</em>}</li>
 *   <li>{@link Ada.ElementClass#getAccessToFunction <em>Access To Function</em>}</li>
 *   <li>{@link Ada.ElementClass#getAccessToProtectedFunction <em>Access To Protected Function</em>}</li>
 *   <li>{@link Ada.ElementClass#getSubtypeIndication <em>Subtype Indication</em>}</li>
 *   <li>{@link Ada.ElementClass#getRangeAttributeReference <em>Range Attribute Reference</em>}</li>
 *   <li>{@link Ada.ElementClass#getSimpleExpressionRange <em>Simple Expression Range</em>}</li>
 *   <li>{@link Ada.ElementClass#getDigitsConstraint <em>Digits Constraint</em>}</li>
 *   <li>{@link Ada.ElementClass#getDeltaConstraint <em>Delta Constraint</em>}</li>
 *   <li>{@link Ada.ElementClass#getIndexConstraint <em>Index Constraint</em>}</li>
 *   <li>{@link Ada.ElementClass#getDiscriminantConstraint <em>Discriminant Constraint</em>}</li>
 *   <li>{@link Ada.ElementClass#getComponentDefinition <em>Component Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getDiscreteSubtypeIndicationAsSubtypeDefinition <em>Discrete Subtype Indication As Subtype Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getDiscreteRangeAttributeReferenceAsSubtypeDefinition <em>Discrete Range Attribute Reference As Subtype Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getDiscreteSimpleExpressionRangeAsSubtypeDefinition <em>Discrete Simple Expression Range As Subtype Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getDiscreteSubtypeIndication <em>Discrete Subtype Indication</em>}</li>
 *   <li>{@link Ada.ElementClass#getDiscreteRangeAttributeReference <em>Discrete Range Attribute Reference</em>}</li>
 *   <li>{@link Ada.ElementClass#getDiscreteSimpleExpressionRange <em>Discrete Simple Expression Range</em>}</li>
 *   <li>{@link Ada.ElementClass#getUnknownDiscriminantPart <em>Unknown Discriminant Part</em>}</li>
 *   <li>{@link Ada.ElementClass#getKnownDiscriminantPart <em>Known Discriminant Part</em>}</li>
 *   <li>{@link Ada.ElementClass#getRecordDefinition <em>Record Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getNullRecordDefinition <em>Null Record Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getNullComponent <em>Null Component</em>}</li>
 *   <li>{@link Ada.ElementClass#getVariantPart <em>Variant Part</em>}</li>
 *   <li>{@link Ada.ElementClass#getVariant <em>Variant</em>}</li>
 *   <li>{@link Ada.ElementClass#getOthersChoice <em>Others Choice</em>}</li>
 *   <li>{@link Ada.ElementClass#getAnonymousAccessToVariable <em>Anonymous Access To Variable</em>}</li>
 *   <li>{@link Ada.ElementClass#getAnonymousAccessToConstant <em>Anonymous Access To Constant</em>}</li>
 *   <li>{@link Ada.ElementClass#getAnonymousAccessToProcedure <em>Anonymous Access To Procedure</em>}</li>
 *   <li>{@link Ada.ElementClass#getAnonymousAccessToProtectedProcedure <em>Anonymous Access To Protected Procedure</em>}</li>
 *   <li>{@link Ada.ElementClass#getAnonymousAccessToFunction <em>Anonymous Access To Function</em>}</li>
 *   <li>{@link Ada.ElementClass#getAnonymousAccessToProtectedFunction <em>Anonymous Access To Protected Function</em>}</li>
 *   <li>{@link Ada.ElementClass#getPrivateTypeDefinition <em>Private Type Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getTaggedPrivateTypeDefinition <em>Tagged Private Type Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getPrivateExtensionDefinition <em>Private Extension Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getTaskDefinition <em>Task Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getProtectedDefinition <em>Protected Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalPrivateTypeDefinition <em>Formal Private Type Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalTaggedPrivateTypeDefinition <em>Formal Tagged Private Type Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalDerivedTypeDefinition <em>Formal Derived Type Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalDiscreteTypeDefinition <em>Formal Discrete Type Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalSignedIntegerTypeDefinition <em>Formal Signed Integer Type Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalModularTypeDefinition <em>Formal Modular Type Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalFloatingPointDefinition <em>Formal Floating Point Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalOrdinaryFixedPointDefinition <em>Formal Ordinary Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalDecimalFixedPointDefinition <em>Formal Decimal Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalOrdinaryInterface <em>Formal Ordinary Interface</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalLimitedInterface <em>Formal Limited Interface</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalTaskInterface <em>Formal Task Interface</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalProtectedInterface <em>Formal Protected Interface</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalSynchronizedInterface <em>Formal Synchronized Interface</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalUnconstrainedArrayDefinition <em>Formal Unconstrained Array Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalConstrainedArrayDefinition <em>Formal Constrained Array Definition</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalPoolSpecificAccessToVariable <em>Formal Pool Specific Access To Variable</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalAccessToVariable <em>Formal Access To Variable</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalAccessToConstant <em>Formal Access To Constant</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalAccessToProcedure <em>Formal Access To Procedure</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalAccessToProtectedProcedure <em>Formal Access To Protected Procedure</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalAccessToFunction <em>Formal Access To Function</em>}</li>
 *   <li>{@link Ada.ElementClass#getFormalAccessToProtectedFunction <em>Formal Access To Protected Function</em>}</li>
 *   <li>{@link Ada.ElementClass#getAspectSpecification <em>Aspect Specification</em>}</li>
 *   <li>{@link Ada.ElementClass#getBoxExpression <em>Box Expression</em>}</li>
 *   <li>{@link Ada.ElementClass#getIntegerLiteral <em>Integer Literal</em>}</li>
 *   <li>{@link Ada.ElementClass#getRealLiteral <em>Real Literal</em>}</li>
 *   <li>{@link Ada.ElementClass#getStringLiteral <em>String Literal</em>}</li>
 *   <li>{@link Ada.ElementClass#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link Ada.ElementClass#getAndOperator <em>And Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getOrOperator <em>Or Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getXorOperator <em>Xor Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getEqualOperator <em>Equal Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getNotEqualOperator <em>Not Equal Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getLessThanOperator <em>Less Than Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getLessThanOrEqualOperator <em>Less Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getGreaterThanOperator <em>Greater Than Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getGreaterThanOrEqualOperator <em>Greater Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getPlusOperator <em>Plus Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getMinusOperator <em>Minus Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getConcatenateOperator <em>Concatenate Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getUnaryPlusOperator <em>Unary Plus Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getUnaryMinusOperator <em>Unary Minus Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getMultiplyOperator <em>Multiply Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getDivideOperator <em>Divide Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getModOperator <em>Mod Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getRemOperator <em>Rem Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getExponentiateOperator <em>Exponentiate Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getAbsOperator <em>Abs Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getNotOperator <em>Not Operator</em>}</li>
 *   <li>{@link Ada.ElementClass#getCharacterLiteral <em>Character Literal</em>}</li>
 *   <li>{@link Ada.ElementClass#getEnumerationLiteral <em>Enumeration Literal</em>}</li>
 *   <li>{@link Ada.ElementClass#getExplicitDereference <em>Explicit Dereference</em>}</li>
 *   <li>{@link Ada.ElementClass#getFunctionCall <em>Function Call</em>}</li>
 *   <li>{@link Ada.ElementClass#getIndexedComponent <em>Indexed Component</em>}</li>
 *   <li>{@link Ada.ElementClass#getSlice <em>Slice</em>}</li>
 *   <li>{@link Ada.ElementClass#getSelectedComponent <em>Selected Component</em>}</li>
 *   <li>{@link Ada.ElementClass#getAccessAttribute <em>Access Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getAddressAttribute <em>Address Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getAdjacentAttribute <em>Adjacent Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getAftAttribute <em>Aft Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getAlignmentAttribute <em>Alignment Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getBaseAttribute <em>Base Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getBitOrderAttribute <em>Bit Order Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getBodyVersionAttribute <em>Body Version Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getCallableAttribute <em>Callable Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getCallerAttribute <em>Caller Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getCeilingAttribute <em>Ceiling Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getClassAttribute <em>Class Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getComponentSizeAttribute <em>Component Size Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getComposeAttribute <em>Compose Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getConstrainedAttribute <em>Constrained Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getCopySignAttribute <em>Copy Sign Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getCountAttribute <em>Count Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefiniteAttribute <em>Definite Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getDeltaAttribute <em>Delta Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getDenormAttribute <em>Denorm Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getDigitsAttribute <em>Digits Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getExponentAttribute <em>Exponent Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getExternalTagAttribute <em>External Tag Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getFirstAttribute <em>First Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getFirstBitAttribute <em>First Bit Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getFloorAttribute <em>Floor Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getForeAttribute <em>Fore Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getFractionAttribute <em>Fraction Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getIdentityAttribute <em>Identity Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getImageAttribute <em>Image Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getInputAttribute <em>Input Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getLastAttribute <em>Last Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getLastBitAttribute <em>Last Bit Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getLeadingPartAttribute <em>Leading Part Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getLengthAttribute <em>Length Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getMachineAttribute <em>Machine Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getMachineEmaxAttribute <em>Machine Emax Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getMachineEminAttribute <em>Machine Emin Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getMachineMantissaAttribute <em>Machine Mantissa Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getMachineOverflowsAttribute <em>Machine Overflows Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getMachineRadixAttribute <em>Machine Radix Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getMachineRoundsAttribute <em>Machine Rounds Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getMaxAttribute <em>Max Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getMaxSizeInStorageElementsAttribute <em>Max Size In Storage Elements Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getMinAttribute <em>Min Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getModelAttribute <em>Model Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getModelEminAttribute <em>Model Emin Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getModelEpsilonAttribute <em>Model Epsilon Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getModelMantissaAttribute <em>Model Mantissa Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getModelSmallAttribute <em>Model Small Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getModulusAttribute <em>Modulus Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getOutputAttribute <em>Output Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getPartitionIdAttribute <em>Partition Id Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getPosAttribute <em>Pos Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getPositionAttribute <em>Position Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getPredAttribute <em>Pred Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getRangeAttribute <em>Range Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getReadAttribute <em>Read Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getRemainderAttribute <em>Remainder Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getRoundAttribute <em>Round Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getRoundingAttribute <em>Rounding Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getSafeFirstAttribute <em>Safe First Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getSafeLastAttribute <em>Safe Last Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getScaleAttribute <em>Scale Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getScalingAttribute <em>Scaling Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getSignedZerosAttribute <em>Signed Zeros Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getSizeAttribute <em>Size Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getSmallAttribute <em>Small Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getStoragePoolAttribute <em>Storage Pool Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getStorageSizeAttribute <em>Storage Size Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getSuccAttribute <em>Succ Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getTagAttribute <em>Tag Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getTerminatedAttribute <em>Terminated Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getTruncationAttribute <em>Truncation Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getUnbiasedRoundingAttribute <em>Unbiased Rounding Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getUncheckedAccessAttribute <em>Unchecked Access Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getValAttribute <em>Val Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getValidAttribute <em>Valid Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getValueAttribute <em>Value Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getVersionAttribute <em>Version Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getWideImageAttribute <em>Wide Image Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getWideValueAttribute <em>Wide Value Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getWideWidthAttribute <em>Wide Width Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getWidthAttribute <em>Width Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getWriteAttribute <em>Write Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getMachineRoundingAttribute <em>Machine Rounding Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getModAttribute <em>Mod Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getPriorityAttribute <em>Priority Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getStreamSizeAttribute <em>Stream Size Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getWideWideImageAttribute <em>Wide Wide Image Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getWideWideValueAttribute <em>Wide Wide Value Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getWideWideWidthAttribute <em>Wide Wide Width Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getMaxAlignmentForAllocationAttribute <em>Max Alignment For Allocation Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getOverlapsStorageAttribute <em>Overlaps Storage Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getImplementationDefinedAttribute <em>Implementation Defined Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getUnknownAttribute <em>Unknown Attribute</em>}</li>
 *   <li>{@link Ada.ElementClass#getRecordAggregate <em>Record Aggregate</em>}</li>
 *   <li>{@link Ada.ElementClass#getExtensionAggregate <em>Extension Aggregate</em>}</li>
 *   <li>{@link Ada.ElementClass#getPositionalArrayAggregate <em>Positional Array Aggregate</em>}</li>
 *   <li>{@link Ada.ElementClass#getNamedArrayAggregate <em>Named Array Aggregate</em>}</li>
 *   <li>{@link Ada.ElementClass#getAndThenShortCircuit <em>And Then Short Circuit</em>}</li>
 *   <li>{@link Ada.ElementClass#getOrElseShortCircuit <em>Or Else Short Circuit</em>}</li>
 *   <li>{@link Ada.ElementClass#getInMembershipTest <em>In Membership Test</em>}</li>
 *   <li>{@link Ada.ElementClass#getNotInMembershipTest <em>Not In Membership Test</em>}</li>
 *   <li>{@link Ada.ElementClass#getNullLiteral <em>Null Literal</em>}</li>
 *   <li>{@link Ada.ElementClass#getParenthesizedExpression <em>Parenthesized Expression</em>}</li>
 *   <li>{@link Ada.ElementClass#getRaiseExpression <em>Raise Expression</em>}</li>
 *   <li>{@link Ada.ElementClass#getTypeConversion <em>Type Conversion</em>}</li>
 *   <li>{@link Ada.ElementClass#getQualifiedExpression <em>Qualified Expression</em>}</li>
 *   <li>{@link Ada.ElementClass#getAllocationFromSubtype <em>Allocation From Subtype</em>}</li>
 *   <li>{@link Ada.ElementClass#getAllocationFromQualifiedExpression <em>Allocation From Qualified Expression</em>}</li>
 *   <li>{@link Ada.ElementClass#getCaseExpression <em>Case Expression</em>}</li>
 *   <li>{@link Ada.ElementClass#getIfExpression <em>If Expression</em>}</li>
 *   <li>{@link Ada.ElementClass#getForAllQuantifiedExpression <em>For All Quantified Expression</em>}</li>
 *   <li>{@link Ada.ElementClass#getForSomeQuantifiedExpression <em>For Some Quantified Expression</em>}</li>
 *   <li>{@link Ada.ElementClass#getPragmaArgumentAssociation <em>Pragma Argument Association</em>}</li>
 *   <li>{@link Ada.ElementClass#getDiscriminantAssociation <em>Discriminant Association</em>}</li>
 *   <li>{@link Ada.ElementClass#getRecordComponentAssociation <em>Record Component Association</em>}</li>
 *   <li>{@link Ada.ElementClass#getArrayComponentAssociation <em>Array Component Association</em>}</li>
 *   <li>{@link Ada.ElementClass#getParameterAssociation <em>Parameter Association</em>}</li>
 *   <li>{@link Ada.ElementClass#getGenericAssociation <em>Generic Association</em>}</li>
 *   <li>{@link Ada.ElementClass#getNullStatement <em>Null Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getAssignmentStatement <em>Assignment Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getIfStatement <em>If Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getCaseStatement <em>Case Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getLoopStatement <em>Loop Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getWhileLoopStatement <em>While Loop Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getForLoopStatement <em>For Loop Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getBlockStatement <em>Block Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getExitStatement <em>Exit Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getGotoStatement <em>Goto Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getProcedureCallStatement <em>Procedure Call Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getReturnStatement <em>Return Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getExtendedReturnStatement <em>Extended Return Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getAcceptStatement <em>Accept Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getEntryCallStatement <em>Entry Call Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getRequeueStatement <em>Requeue Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getRequeueStatementWithAbort <em>Requeue Statement With Abort</em>}</li>
 *   <li>{@link Ada.ElementClass#getDelayUntilStatement <em>Delay Until Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getDelayRelativeStatement <em>Delay Relative Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getTerminateAlternativeStatement <em>Terminate Alternative Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getSelectiveAcceptStatement <em>Selective Accept Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getTimedEntryCallStatement <em>Timed Entry Call Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getConditionalEntryCallStatement <em>Conditional Entry Call Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getAsynchronousSelectStatement <em>Asynchronous Select Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getAbortStatement <em>Abort Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getRaiseStatement <em>Raise Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getCodeStatement <em>Code Statement</em>}</li>
 *   <li>{@link Ada.ElementClass#getIfPath <em>If Path</em>}</li>
 *   <li>{@link Ada.ElementClass#getElsifPath <em>Elsif Path</em>}</li>
 *   <li>{@link Ada.ElementClass#getElsePath <em>Else Path</em>}</li>
 *   <li>{@link Ada.ElementClass#getCasePath <em>Case Path</em>}</li>
 *   <li>{@link Ada.ElementClass#getSelectPath <em>Select Path</em>}</li>
 *   <li>{@link Ada.ElementClass#getOrPath <em>Or Path</em>}</li>
 *   <li>{@link Ada.ElementClass#getThenAbortPath <em>Then Abort Path</em>}</li>
 *   <li>{@link Ada.ElementClass#getCaseExpressionPath <em>Case Expression Path</em>}</li>
 *   <li>{@link Ada.ElementClass#getIfExpressionPath <em>If Expression Path</em>}</li>
 *   <li>{@link Ada.ElementClass#getElsifExpressionPath <em>Elsif Expression Path</em>}</li>
 *   <li>{@link Ada.ElementClass#getElseExpressionPath <em>Else Expression Path</em>}</li>
 *   <li>{@link Ada.ElementClass#getUsePackageClause <em>Use Package Clause</em>}</li>
 *   <li>{@link Ada.ElementClass#getUseTypeClause <em>Use Type Clause</em>}</li>
 *   <li>{@link Ada.ElementClass#getUseAllTypeClause <em>Use All Type Clause</em>}</li>
 *   <li>{@link Ada.ElementClass#getWithClause <em>With Clause</em>}</li>
 *   <li>{@link Ada.ElementClass#getAttributeDefinitionClause <em>Attribute Definition Clause</em>}</li>
 *   <li>{@link Ada.ElementClass#getEnumerationRepresentationClause <em>Enumeration Representation Clause</em>}</li>
 *   <li>{@link Ada.ElementClass#getRecordRepresentationClause <em>Record Representation Clause</em>}</li>
 *   <li>{@link Ada.ElementClass#getAtClause <em>At Clause</em>}</li>
 *   <li>{@link Ada.ElementClass#getComponentClause <em>Component Clause</em>}</li>
 *   <li>{@link Ada.ElementClass#getExceptionHandler <em>Exception Handler</em>}</li>
 *   <li>{@link Ada.ElementClass#getComment <em>Comment</em>}</li>
 *   <li>{@link Ada.ElementClass#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getAsynchronousPragma <em>Asynchronous Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getAtomicPragma <em>Atomic Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getAttachHandlerPragma <em>Attach Handler Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getControlledPragma <em>Controlled Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getConventionPragma <em>Convention Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getDiscardNamesPragma <em>Discard Names Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getElaboratePragma <em>Elaborate Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getElaborateAllPragma <em>Elaborate All Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getExportPragma <em>Export Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getImportPragma <em>Import Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getInlinePragma <em>Inline Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getInspectionPointPragma <em>Inspection Point Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getLinkerOptionsPragma <em>Linker Options Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getListPragma <em>List Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getLockingPolicyPragma <em>Locking Policy Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getOptimizePragma <em>Optimize Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getPackPragma <em>Pack Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getPagePragma <em>Page Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getPreelaboratePragma <em>Preelaborate Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getPriorityPragma <em>Priority Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getPurePragma <em>Pure Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getRemoteTypesPragma <em>Remote Types Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getRestrictionsPragma <em>Restrictions Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getReviewablePragma <em>Reviewable Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getSharedPassivePragma <em>Shared Passive Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getStorageSizePragma <em>Storage Size Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getSuppressPragma <em>Suppress Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getVolatilePragma <em>Volatile Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getAssertPragma <em>Assert Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getNoReturnPragma <em>No Return Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getProfilePragma <em>Profile Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getUnsuppressPragma <em>Unsuppress Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getCpuPragma <em>Cpu Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getIndependentPragma <em>Independent Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getIndependentComponentsPragma <em>Independent Components Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}</li>
 *   <li>{@link Ada.ElementClass#getUnknownPragma <em>Unknown Pragma</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getElementClass()
 * @model extendedMetaData="name='Element_Class' kind='elementOnly'"
 * @generated
 */
public interface ElementClass extends EObject {
	/**
	 * Returns the value of the '<em><b>Not An Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not An Element</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not An Element</em>' containment reference.
	 * @see #setNotAnElement(NotAnElement)
	 * @see Ada.AdaPackage#getElementClass_NotAnElement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not_an_element' namespace='##targetNamespace'"
	 * @generated
	 */
	NotAnElement getNotAnElement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getNotAnElement <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not An Element</em>' containment reference.
	 * @see #getNotAnElement()
	 * @generated
	 */
	void setNotAnElement(NotAnElement value);

	/**
	 * Returns the value of the '<em><b>Defining Identifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Identifier</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Identifier</em>' containment reference.
	 * @see #setDefiningIdentifier(DefiningIdentifier)
	 * @see Ada.AdaPackage#getElementClass_DefiningIdentifier()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_identifier' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningIdentifier getDefiningIdentifier();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiningIdentifier <em>Defining Identifier</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Identifier</em>' containment reference.
	 * @see #getDefiningIdentifier()
	 * @generated
	 */
	void setDefiningIdentifier(DefiningIdentifier value);

	/**
	 * Returns the value of the '<em><b>Defining Character Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Character Literal</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Character Literal</em>' containment reference.
	 * @see #setDefiningCharacterLiteral(DefiningCharacterLiteral)
	 * @see Ada.AdaPackage#getElementClass_DefiningCharacterLiteral()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_character_literal' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningCharacterLiteral getDefiningCharacterLiteral();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiningCharacterLiteral <em>Defining Character Literal</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Character Literal</em>' containment reference.
	 * @see #getDefiningCharacterLiteral()
	 * @generated
	 */
	void setDefiningCharacterLiteral(DefiningCharacterLiteral value);

	/**
	 * Returns the value of the '<em><b>Defining Enumeration Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Enumeration Literal</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Enumeration Literal</em>' containment reference.
	 * @see #setDefiningEnumerationLiteral(DefiningEnumerationLiteral)
	 * @see Ada.AdaPackage#getElementClass_DefiningEnumerationLiteral()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_enumeration_literal' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningEnumerationLiteral getDefiningEnumerationLiteral();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiningEnumerationLiteral <em>Defining Enumeration Literal</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Enumeration Literal</em>' containment reference.
	 * @see #getDefiningEnumerationLiteral()
	 * @generated
	 */
	void setDefiningEnumerationLiteral(DefiningEnumerationLiteral value);

	/**
	 * Returns the value of the '<em><b>Defining And Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining And Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining And Operator</em>' containment reference.
	 * @see #setDefiningAndOperator(DefiningAndOperator)
	 * @see Ada.AdaPackage#getElementClass_DefiningAndOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_and_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningAndOperator getDefiningAndOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiningAndOperator <em>Defining And Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining And Operator</em>' containment reference.
	 * @see #getDefiningAndOperator()
	 * @generated
	 */
	void setDefiningAndOperator(DefiningAndOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Or Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Or Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Or Operator</em>' containment reference.
	 * @see #setDefiningOrOperator(DefiningOrOperator)
	 * @see Ada.AdaPackage#getElementClass_DefiningOrOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_or_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningOrOperator getDefiningOrOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiningOrOperator <em>Defining Or Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Or Operator</em>' containment reference.
	 * @see #getDefiningOrOperator()
	 * @generated
	 */
	void setDefiningOrOperator(DefiningOrOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Xor Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Xor Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Xor Operator</em>' containment reference.
	 * @see #setDefiningXorOperator(DefiningXorOperator)
	 * @see Ada.AdaPackage#getElementClass_DefiningXorOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_xor_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningXorOperator getDefiningXorOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiningXorOperator <em>Defining Xor Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Xor Operator</em>' containment reference.
	 * @see #getDefiningXorOperator()
	 * @generated
	 */
	void setDefiningXorOperator(DefiningXorOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Equal Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Equal Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Equal Operator</em>' containment reference.
	 * @see #setDefiningEqualOperator(DefiningEqualOperator)
	 * @see Ada.AdaPackage#getElementClass_DefiningEqualOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_equal_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningEqualOperator getDefiningEqualOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiningEqualOperator <em>Defining Equal Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Equal Operator</em>' containment reference.
	 * @see #getDefiningEqualOperator()
	 * @generated
	 */
	void setDefiningEqualOperator(DefiningEqualOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Not Equal Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Not Equal Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Not Equal Operator</em>' containment reference.
	 * @see #setDefiningNotEqualOperator(DefiningNotEqualOperator)
	 * @see Ada.AdaPackage#getElementClass_DefiningNotEqualOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_not_equal_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNotEqualOperator getDefiningNotEqualOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiningNotEqualOperator <em>Defining Not Equal Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Not Equal Operator</em>' containment reference.
	 * @see #getDefiningNotEqualOperator()
	 * @generated
	 */
	void setDefiningNotEqualOperator(DefiningNotEqualOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Less Than Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Less Than Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Less Than Operator</em>' containment reference.
	 * @see #setDefiningLessThanOperator(DefiningLessThanOperator)
	 * @see Ada.AdaPackage#getElementClass_DefiningLessThanOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_less_than_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningLessThanOperator getDefiningLessThanOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiningLessThanOperator <em>Defining Less Than Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Less Than Operator</em>' containment reference.
	 * @see #getDefiningLessThanOperator()
	 * @generated
	 */
	void setDefiningLessThanOperator(DefiningLessThanOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Less Than Or Equal Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Less Than Or Equal Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Less Than Or Equal Operator</em>' containment reference.
	 * @see #setDefiningLessThanOrEqualOperator(DefiningLessThanOrEqualOperator)
	 * @see Ada.AdaPackage#getElementClass_DefiningLessThanOrEqualOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_less_than_or_equal_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningLessThanOrEqualOperator getDefiningLessThanOrEqualOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiningLessThanOrEqualOperator <em>Defining Less Than Or Equal Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Less Than Or Equal Operator</em>' containment reference.
	 * @see #getDefiningLessThanOrEqualOperator()
	 * @generated
	 */
	void setDefiningLessThanOrEqualOperator(DefiningLessThanOrEqualOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Greater Than Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Greater Than Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Greater Than Operator</em>' containment reference.
	 * @see #setDefiningGreaterThanOperator(DefiningGreaterThanOperator)
	 * @see Ada.AdaPackage#getElementClass_DefiningGreaterThanOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_greater_than_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningGreaterThanOperator getDefiningGreaterThanOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiningGreaterThanOperator <em>Defining Greater Than Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Greater Than Operator</em>' containment reference.
	 * @see #getDefiningGreaterThanOperator()
	 * @generated
	 */
	void setDefiningGreaterThanOperator(DefiningGreaterThanOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Greater Than Or Equal Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Greater Than Or Equal Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Greater Than Or Equal Operator</em>' containment reference.
	 * @see #setDefiningGreaterThanOrEqualOperator(DefiningGreaterThanOrEqualOperator)
	 * @see Ada.AdaPackage#getElementClass_DefiningGreaterThanOrEqualOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_greater_than_or_equal_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningGreaterThanOrEqualOperator getDefiningGreaterThanOrEqualOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiningGreaterThanOrEqualOperator <em>Defining Greater Than Or Equal Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Greater Than Or Equal Operator</em>' containment reference.
	 * @see #getDefiningGreaterThanOrEqualOperator()
	 * @generated
	 */
	void setDefiningGreaterThanOrEqualOperator(DefiningGreaterThanOrEqualOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Plus Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Plus Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Plus Operator</em>' containment reference.
	 * @see #setDefiningPlusOperator(DefiningPlusOperator)
	 * @see Ada.AdaPackage#getElementClass_DefiningPlusOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_plus_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningPlusOperator getDefiningPlusOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiningPlusOperator <em>Defining Plus Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Plus Operator</em>' containment reference.
	 * @see #getDefiningPlusOperator()
	 * @generated
	 */
	void setDefiningPlusOperator(DefiningPlusOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Minus Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Minus Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Minus Operator</em>' containment reference.
	 * @see #setDefiningMinusOperator(DefiningMinusOperator)
	 * @see Ada.AdaPackage#getElementClass_DefiningMinusOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_minus_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningMinusOperator getDefiningMinusOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiningMinusOperator <em>Defining Minus Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Minus Operator</em>' containment reference.
	 * @see #getDefiningMinusOperator()
	 * @generated
	 */
	void setDefiningMinusOperator(DefiningMinusOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Concatenate Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Concatenate Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Concatenate Operator</em>' containment reference.
	 * @see #setDefiningConcatenateOperator(DefiningConcatenateOperator)
	 * @see Ada.AdaPackage#getElementClass_DefiningConcatenateOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_concatenate_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningConcatenateOperator getDefiningConcatenateOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiningConcatenateOperator <em>Defining Concatenate Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Concatenate Operator</em>' containment reference.
	 * @see #getDefiningConcatenateOperator()
	 * @generated
	 */
	void setDefiningConcatenateOperator(DefiningConcatenateOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Unary Plus Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Unary Plus Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Unary Plus Operator</em>' containment reference.
	 * @see #setDefiningUnaryPlusOperator(DefiningUnaryPlusOperator)
	 * @see Ada.AdaPackage#getElementClass_DefiningUnaryPlusOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_unary_plus_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningUnaryPlusOperator getDefiningUnaryPlusOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiningUnaryPlusOperator <em>Defining Unary Plus Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Unary Plus Operator</em>' containment reference.
	 * @see #getDefiningUnaryPlusOperator()
	 * @generated
	 */
	void setDefiningUnaryPlusOperator(DefiningUnaryPlusOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Unary Minus Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Unary Minus Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Unary Minus Operator</em>' containment reference.
	 * @see #setDefiningUnaryMinusOperator(DefiningUnaryMinusOperator)
	 * @see Ada.AdaPackage#getElementClass_DefiningUnaryMinusOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_unary_minus_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningUnaryMinusOperator getDefiningUnaryMinusOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiningUnaryMinusOperator <em>Defining Unary Minus Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Unary Minus Operator</em>' containment reference.
	 * @see #getDefiningUnaryMinusOperator()
	 * @generated
	 */
	void setDefiningUnaryMinusOperator(DefiningUnaryMinusOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Multiply Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Multiply Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Multiply Operator</em>' containment reference.
	 * @see #setDefiningMultiplyOperator(DefiningMultiplyOperator)
	 * @see Ada.AdaPackage#getElementClass_DefiningMultiplyOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_multiply_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningMultiplyOperator getDefiningMultiplyOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiningMultiplyOperator <em>Defining Multiply Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Multiply Operator</em>' containment reference.
	 * @see #getDefiningMultiplyOperator()
	 * @generated
	 */
	void setDefiningMultiplyOperator(DefiningMultiplyOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Divide Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Divide Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Divide Operator</em>' containment reference.
	 * @see #setDefiningDivideOperator(DefiningDivideOperator)
	 * @see Ada.AdaPackage#getElementClass_DefiningDivideOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_divide_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningDivideOperator getDefiningDivideOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiningDivideOperator <em>Defining Divide Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Divide Operator</em>' containment reference.
	 * @see #getDefiningDivideOperator()
	 * @generated
	 */
	void setDefiningDivideOperator(DefiningDivideOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Mod Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Mod Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Mod Operator</em>' containment reference.
	 * @see #setDefiningModOperator(DefiningModOperator)
	 * @see Ada.AdaPackage#getElementClass_DefiningModOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_mod_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningModOperator getDefiningModOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiningModOperator <em>Defining Mod Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Mod Operator</em>' containment reference.
	 * @see #getDefiningModOperator()
	 * @generated
	 */
	void setDefiningModOperator(DefiningModOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Rem Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Rem Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Rem Operator</em>' containment reference.
	 * @see #setDefiningRemOperator(DefiningRemOperator)
	 * @see Ada.AdaPackage#getElementClass_DefiningRemOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_rem_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningRemOperator getDefiningRemOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiningRemOperator <em>Defining Rem Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Rem Operator</em>' containment reference.
	 * @see #getDefiningRemOperator()
	 * @generated
	 */
	void setDefiningRemOperator(DefiningRemOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Exponentiate Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Exponentiate Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Exponentiate Operator</em>' containment reference.
	 * @see #setDefiningExponentiateOperator(DefiningExponentiateOperator)
	 * @see Ada.AdaPackage#getElementClass_DefiningExponentiateOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_exponentiate_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningExponentiateOperator getDefiningExponentiateOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiningExponentiateOperator <em>Defining Exponentiate Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Exponentiate Operator</em>' containment reference.
	 * @see #getDefiningExponentiateOperator()
	 * @generated
	 */
	void setDefiningExponentiateOperator(DefiningExponentiateOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Abs Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Abs Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Abs Operator</em>' containment reference.
	 * @see #setDefiningAbsOperator(DefiningAbsOperator)
	 * @see Ada.AdaPackage#getElementClass_DefiningAbsOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_abs_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningAbsOperator getDefiningAbsOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiningAbsOperator <em>Defining Abs Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Abs Operator</em>' containment reference.
	 * @see #getDefiningAbsOperator()
	 * @generated
	 */
	void setDefiningAbsOperator(DefiningAbsOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Not Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Not Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Not Operator</em>' containment reference.
	 * @see #setDefiningNotOperator(DefiningNotOperator)
	 * @see Ada.AdaPackage#getElementClass_DefiningNotOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_not_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNotOperator getDefiningNotOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiningNotOperator <em>Defining Not Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Not Operator</em>' containment reference.
	 * @see #getDefiningNotOperator()
	 * @generated
	 */
	void setDefiningNotOperator(DefiningNotOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Expanded Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Expanded Name</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Expanded Name</em>' containment reference.
	 * @see #setDefiningExpandedName(DefiningExpandedName)
	 * @see Ada.AdaPackage#getElementClass_DefiningExpandedName()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_expanded_name' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningExpandedName getDefiningExpandedName();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiningExpandedName <em>Defining Expanded Name</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Expanded Name</em>' containment reference.
	 * @see #getDefiningExpandedName()
	 * @generated
	 */
	void setDefiningExpandedName(DefiningExpandedName value);

	/**
	 * Returns the value of the '<em><b>Ordinary Type Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ordinary Type Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ordinary Type Declaration</em>' containment reference.
	 * @see #setOrdinaryTypeDeclaration(OrdinaryTypeDeclaration)
	 * @see Ada.AdaPackage#getElementClass_OrdinaryTypeDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ordinary_type_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	OrdinaryTypeDeclaration getOrdinaryTypeDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getOrdinaryTypeDeclaration <em>Ordinary Type Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ordinary Type Declaration</em>' containment reference.
	 * @see #getOrdinaryTypeDeclaration()
	 * @generated
	 */
	void setOrdinaryTypeDeclaration(OrdinaryTypeDeclaration value);

	/**
	 * Returns the value of the '<em><b>Task Type Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Type Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Type Declaration</em>' containment reference.
	 * @see #setTaskTypeDeclaration(TaskTypeDeclaration)
	 * @see Ada.AdaPackage#getElementClass_TaskTypeDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='task_type_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	TaskTypeDeclaration getTaskTypeDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getTaskTypeDeclaration <em>Task Type Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Type Declaration</em>' containment reference.
	 * @see #getTaskTypeDeclaration()
	 * @generated
	 */
	void setTaskTypeDeclaration(TaskTypeDeclaration value);

	/**
	 * Returns the value of the '<em><b>Protected Type Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protected Type Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protected Type Declaration</em>' containment reference.
	 * @see #setProtectedTypeDeclaration(ProtectedTypeDeclaration)
	 * @see Ada.AdaPackage#getElementClass_ProtectedTypeDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='protected_type_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	ProtectedTypeDeclaration getProtectedTypeDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getProtectedTypeDeclaration <em>Protected Type Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Protected Type Declaration</em>' containment reference.
	 * @see #getProtectedTypeDeclaration()
	 * @generated
	 */
	void setProtectedTypeDeclaration(ProtectedTypeDeclaration value);

	/**
	 * Returns the value of the '<em><b>Incomplete Type Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incomplete Type Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incomplete Type Declaration</em>' containment reference.
	 * @see #setIncompleteTypeDeclaration(IncompleteTypeDeclaration)
	 * @see Ada.AdaPackage#getElementClass_IncompleteTypeDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='incomplete_type_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	IncompleteTypeDeclaration getIncompleteTypeDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getIncompleteTypeDeclaration <em>Incomplete Type Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Incomplete Type Declaration</em>' containment reference.
	 * @see #getIncompleteTypeDeclaration()
	 * @generated
	 */
	void setIncompleteTypeDeclaration(IncompleteTypeDeclaration value);

	/**
	 * Returns the value of the '<em><b>Tagged Incomplete Type Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tagged Incomplete Type Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tagged Incomplete Type Declaration</em>' containment reference.
	 * @see #setTaggedIncompleteTypeDeclaration(TaggedIncompleteTypeDeclaration)
	 * @see Ada.AdaPackage#getElementClass_TaggedIncompleteTypeDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='tagged_incomplete_type_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	TaggedIncompleteTypeDeclaration getTaggedIncompleteTypeDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getTaggedIncompleteTypeDeclaration <em>Tagged Incomplete Type Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tagged Incomplete Type Declaration</em>' containment reference.
	 * @see #getTaggedIncompleteTypeDeclaration()
	 * @generated
	 */
	void setTaggedIncompleteTypeDeclaration(TaggedIncompleteTypeDeclaration value);

	/**
	 * Returns the value of the '<em><b>Private Type Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Private Type Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Private Type Declaration</em>' containment reference.
	 * @see #setPrivateTypeDeclaration(PrivateTypeDeclaration)
	 * @see Ada.AdaPackage#getElementClass_PrivateTypeDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='private_type_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	PrivateTypeDeclaration getPrivateTypeDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPrivateTypeDeclaration <em>Private Type Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Private Type Declaration</em>' containment reference.
	 * @see #getPrivateTypeDeclaration()
	 * @generated
	 */
	void setPrivateTypeDeclaration(PrivateTypeDeclaration value);

	/**
	 * Returns the value of the '<em><b>Private Extension Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Private Extension Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Private Extension Declaration</em>' containment reference.
	 * @see #setPrivateExtensionDeclaration(PrivateExtensionDeclaration)
	 * @see Ada.AdaPackage#getElementClass_PrivateExtensionDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='private_extension_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	PrivateExtensionDeclaration getPrivateExtensionDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPrivateExtensionDeclaration <em>Private Extension Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Private Extension Declaration</em>' containment reference.
	 * @see #getPrivateExtensionDeclaration()
	 * @generated
	 */
	void setPrivateExtensionDeclaration(PrivateExtensionDeclaration value);

	/**
	 * Returns the value of the '<em><b>Subtype Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subtype Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subtype Declaration</em>' containment reference.
	 * @see #setSubtypeDeclaration(SubtypeDeclaration)
	 * @see Ada.AdaPackage#getElementClass_SubtypeDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='subtype_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	SubtypeDeclaration getSubtypeDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getSubtypeDeclaration <em>Subtype Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Subtype Declaration</em>' containment reference.
	 * @see #getSubtypeDeclaration()
	 * @generated
	 */
	void setSubtypeDeclaration(SubtypeDeclaration value);

	/**
	 * Returns the value of the '<em><b>Variable Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variable Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variable Declaration</em>' containment reference.
	 * @see #setVariableDeclaration(VariableDeclaration)
	 * @see Ada.AdaPackage#getElementClass_VariableDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='variable_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	VariableDeclaration getVariableDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getVariableDeclaration <em>Variable Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variable Declaration</em>' containment reference.
	 * @see #getVariableDeclaration()
	 * @generated
	 */
	void setVariableDeclaration(VariableDeclaration value);

	/**
	 * Returns the value of the '<em><b>Constant Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant Declaration</em>' containment reference.
	 * @see #setConstantDeclaration(ConstantDeclaration)
	 * @see Ada.AdaPackage#getElementClass_ConstantDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='constant_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	ConstantDeclaration getConstantDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getConstantDeclaration <em>Constant Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant Declaration</em>' containment reference.
	 * @see #getConstantDeclaration()
	 * @generated
	 */
	void setConstantDeclaration(ConstantDeclaration value);

	/**
	 * Returns the value of the '<em><b>Deferred Constant Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deferred Constant Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deferred Constant Declaration</em>' containment reference.
	 * @see #setDeferredConstantDeclaration(DeferredConstantDeclaration)
	 * @see Ada.AdaPackage#getElementClass_DeferredConstantDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='deferred_constant_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	DeferredConstantDeclaration getDeferredConstantDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDeferredConstantDeclaration <em>Deferred Constant Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deferred Constant Declaration</em>' containment reference.
	 * @see #getDeferredConstantDeclaration()
	 * @generated
	 */
	void setDeferredConstantDeclaration(DeferredConstantDeclaration value);

	/**
	 * Returns the value of the '<em><b>Single Task Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Single Task Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Single Task Declaration</em>' containment reference.
	 * @see #setSingleTaskDeclaration(SingleTaskDeclaration)
	 * @see Ada.AdaPackage#getElementClass_SingleTaskDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='single_task_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	SingleTaskDeclaration getSingleTaskDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getSingleTaskDeclaration <em>Single Task Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Single Task Declaration</em>' containment reference.
	 * @see #getSingleTaskDeclaration()
	 * @generated
	 */
	void setSingleTaskDeclaration(SingleTaskDeclaration value);

	/**
	 * Returns the value of the '<em><b>Single Protected Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Single Protected Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Single Protected Declaration</em>' containment reference.
	 * @see #setSingleProtectedDeclaration(SingleProtectedDeclaration)
	 * @see Ada.AdaPackage#getElementClass_SingleProtectedDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='single_protected_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	SingleProtectedDeclaration getSingleProtectedDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getSingleProtectedDeclaration <em>Single Protected Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Single Protected Declaration</em>' containment reference.
	 * @see #getSingleProtectedDeclaration()
	 * @generated
	 */
	void setSingleProtectedDeclaration(SingleProtectedDeclaration value);

	/**
	 * Returns the value of the '<em><b>Integer Number Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integer Number Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integer Number Declaration</em>' containment reference.
	 * @see #setIntegerNumberDeclaration(IntegerNumberDeclaration)
	 * @see Ada.AdaPackage#getElementClass_IntegerNumberDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='integer_number_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	IntegerNumberDeclaration getIntegerNumberDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getIntegerNumberDeclaration <em>Integer Number Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Integer Number Declaration</em>' containment reference.
	 * @see #getIntegerNumberDeclaration()
	 * @generated
	 */
	void setIntegerNumberDeclaration(IntegerNumberDeclaration value);

	/**
	 * Returns the value of the '<em><b>Real Number Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Real Number Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Real Number Declaration</em>' containment reference.
	 * @see #setRealNumberDeclaration(RealNumberDeclaration)
	 * @see Ada.AdaPackage#getElementClass_RealNumberDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='real_number_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	RealNumberDeclaration getRealNumberDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getRealNumberDeclaration <em>Real Number Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Real Number Declaration</em>' containment reference.
	 * @see #getRealNumberDeclaration()
	 * @generated
	 */
	void setRealNumberDeclaration(RealNumberDeclaration value);

	/**
	 * Returns the value of the '<em><b>Enumeration Literal Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumeration Literal Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumeration Literal Specification</em>' containment reference.
	 * @see #setEnumerationLiteralSpecification(EnumerationLiteralSpecification)
	 * @see Ada.AdaPackage#getElementClass_EnumerationLiteralSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='enumeration_literal_specification' namespace='##targetNamespace'"
	 * @generated
	 */
	EnumerationLiteralSpecification getEnumerationLiteralSpecification();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getEnumerationLiteralSpecification <em>Enumeration Literal Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enumeration Literal Specification</em>' containment reference.
	 * @see #getEnumerationLiteralSpecification()
	 * @generated
	 */
	void setEnumerationLiteralSpecification(EnumerationLiteralSpecification value);

	/**
	 * Returns the value of the '<em><b>Discriminant Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discriminant Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discriminant Specification</em>' containment reference.
	 * @see #setDiscriminantSpecification(DiscriminantSpecification)
	 * @see Ada.AdaPackage#getElementClass_DiscriminantSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='discriminant_specification' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscriminantSpecification getDiscriminantSpecification();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDiscriminantSpecification <em>Discriminant Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discriminant Specification</em>' containment reference.
	 * @see #getDiscriminantSpecification()
	 * @generated
	 */
	void setDiscriminantSpecification(DiscriminantSpecification value);

	/**
	 * Returns the value of the '<em><b>Component Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Declaration</em>' containment reference.
	 * @see #setComponentDeclaration(ComponentDeclaration)
	 * @see Ada.AdaPackage#getElementClass_ComponentDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='component_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	ComponentDeclaration getComponentDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getComponentDeclaration <em>Component Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component Declaration</em>' containment reference.
	 * @see #getComponentDeclaration()
	 * @generated
	 */
	void setComponentDeclaration(ComponentDeclaration value);

	/**
	 * Returns the value of the '<em><b>Loop Parameter Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Loop Parameter Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Loop Parameter Specification</em>' containment reference.
	 * @see #setLoopParameterSpecification(LoopParameterSpecification)
	 * @see Ada.AdaPackage#getElementClass_LoopParameterSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='loop_parameter_specification' namespace='##targetNamespace'"
	 * @generated
	 */
	LoopParameterSpecification getLoopParameterSpecification();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getLoopParameterSpecification <em>Loop Parameter Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Loop Parameter Specification</em>' containment reference.
	 * @see #getLoopParameterSpecification()
	 * @generated
	 */
	void setLoopParameterSpecification(LoopParameterSpecification value);

	/**
	 * Returns the value of the '<em><b>Generalized Iterator Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generalized Iterator Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generalized Iterator Specification</em>' containment reference.
	 * @see #setGeneralizedIteratorSpecification(GeneralizedIteratorSpecification)
	 * @see Ada.AdaPackage#getElementClass_GeneralizedIteratorSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='generalized_iterator_specification' namespace='##targetNamespace'"
	 * @generated
	 */
	GeneralizedIteratorSpecification getGeneralizedIteratorSpecification();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getGeneralizedIteratorSpecification <em>Generalized Iterator Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generalized Iterator Specification</em>' containment reference.
	 * @see #getGeneralizedIteratorSpecification()
	 * @generated
	 */
	void setGeneralizedIteratorSpecification(GeneralizedIteratorSpecification value);

	/**
	 * Returns the value of the '<em><b>Element Iterator Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element Iterator Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Iterator Specification</em>' containment reference.
	 * @see #setElementIteratorSpecification(ElementIteratorSpecification)
	 * @see Ada.AdaPackage#getElementClass_ElementIteratorSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='element_iterator_specification' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementIteratorSpecification getElementIteratorSpecification();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getElementIteratorSpecification <em>Element Iterator Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element Iterator Specification</em>' containment reference.
	 * @see #getElementIteratorSpecification()
	 * @generated
	 */
	void setElementIteratorSpecification(ElementIteratorSpecification value);

	/**
	 * Returns the value of the '<em><b>Procedure Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure Declaration</em>' containment reference.
	 * @see #setProcedureDeclaration(ProcedureDeclaration)
	 * @see Ada.AdaPackage#getElementClass_ProcedureDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='procedure_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcedureDeclaration getProcedureDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getProcedureDeclaration <em>Procedure Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Procedure Declaration</em>' containment reference.
	 * @see #getProcedureDeclaration()
	 * @generated
	 */
	void setProcedureDeclaration(ProcedureDeclaration value);

	/**
	 * Returns the value of the '<em><b>Function Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Declaration</em>' containment reference.
	 * @see #setFunctionDeclaration(FunctionDeclaration)
	 * @see Ada.AdaPackage#getElementClass_FunctionDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='function_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	FunctionDeclaration getFunctionDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFunctionDeclaration <em>Function Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function Declaration</em>' containment reference.
	 * @see #getFunctionDeclaration()
	 * @generated
	 */
	void setFunctionDeclaration(FunctionDeclaration value);

	/**
	 * Returns the value of the '<em><b>Parameter Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Specification</em>' containment reference.
	 * @see #setParameterSpecification(ParameterSpecification)
	 * @see Ada.AdaPackage#getElementClass_ParameterSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='parameter_specification' namespace='##targetNamespace'"
	 * @generated
	 */
	ParameterSpecification getParameterSpecification();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getParameterSpecification <em>Parameter Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Specification</em>' containment reference.
	 * @see #getParameterSpecification()
	 * @generated
	 */
	void setParameterSpecification(ParameterSpecification value);

	/**
	 * Returns the value of the '<em><b>Procedure Body Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure Body Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure Body Declaration</em>' containment reference.
	 * @see #setProcedureBodyDeclaration(ProcedureBodyDeclaration)
	 * @see Ada.AdaPackage#getElementClass_ProcedureBodyDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='procedure_body_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcedureBodyDeclaration getProcedureBodyDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getProcedureBodyDeclaration <em>Procedure Body Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Procedure Body Declaration</em>' containment reference.
	 * @see #getProcedureBodyDeclaration()
	 * @generated
	 */
	void setProcedureBodyDeclaration(ProcedureBodyDeclaration value);

	/**
	 * Returns the value of the '<em><b>Function Body Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Body Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Body Declaration</em>' containment reference.
	 * @see #setFunctionBodyDeclaration(FunctionBodyDeclaration)
	 * @see Ada.AdaPackage#getElementClass_FunctionBodyDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='function_body_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	FunctionBodyDeclaration getFunctionBodyDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFunctionBodyDeclaration <em>Function Body Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function Body Declaration</em>' containment reference.
	 * @see #getFunctionBodyDeclaration()
	 * @generated
	 */
	void setFunctionBodyDeclaration(FunctionBodyDeclaration value);

	/**
	 * Returns the value of the '<em><b>Return Variable Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Return Variable Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Return Variable Specification</em>' containment reference.
	 * @see #setReturnVariableSpecification(ReturnVariableSpecification)
	 * @see Ada.AdaPackage#getElementClass_ReturnVariableSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='return_variable_specification' namespace='##targetNamespace'"
	 * @generated
	 */
	ReturnVariableSpecification getReturnVariableSpecification();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getReturnVariableSpecification <em>Return Variable Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Return Variable Specification</em>' containment reference.
	 * @see #getReturnVariableSpecification()
	 * @generated
	 */
	void setReturnVariableSpecification(ReturnVariableSpecification value);

	/**
	 * Returns the value of the '<em><b>Return Constant Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Return Constant Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Return Constant Specification</em>' containment reference.
	 * @see #setReturnConstantSpecification(ReturnConstantSpecification)
	 * @see Ada.AdaPackage#getElementClass_ReturnConstantSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='return_constant_specification' namespace='##targetNamespace'"
	 * @generated
	 */
	ReturnConstantSpecification getReturnConstantSpecification();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getReturnConstantSpecification <em>Return Constant Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Return Constant Specification</em>' containment reference.
	 * @see #getReturnConstantSpecification()
	 * @generated
	 */
	void setReturnConstantSpecification(ReturnConstantSpecification value);

	/**
	 * Returns the value of the '<em><b>Null Procedure Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Null Procedure Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Null Procedure Declaration</em>' containment reference.
	 * @see #setNullProcedureDeclaration(NullProcedureDeclaration)
	 * @see Ada.AdaPackage#getElementClass_NullProcedureDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='null_procedure_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	NullProcedureDeclaration getNullProcedureDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getNullProcedureDeclaration <em>Null Procedure Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Null Procedure Declaration</em>' containment reference.
	 * @see #getNullProcedureDeclaration()
	 * @generated
	 */
	void setNullProcedureDeclaration(NullProcedureDeclaration value);

	/**
	 * Returns the value of the '<em><b>Expression Function Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression Function Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression Function Declaration</em>' containment reference.
	 * @see #setExpressionFunctionDeclaration(ExpressionFunctionDeclaration)
	 * @see Ada.AdaPackage#getElementClass_ExpressionFunctionDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='expression_function_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionFunctionDeclaration getExpressionFunctionDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getExpressionFunctionDeclaration <em>Expression Function Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression Function Declaration</em>' containment reference.
	 * @see #getExpressionFunctionDeclaration()
	 * @generated
	 */
	void setExpressionFunctionDeclaration(ExpressionFunctionDeclaration value);

	/**
	 * Returns the value of the '<em><b>Package Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Declaration</em>' containment reference.
	 * @see #setPackageDeclaration(PackageDeclaration)
	 * @see Ada.AdaPackage#getElementClass_PackageDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='package_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	PackageDeclaration getPackageDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPackageDeclaration <em>Package Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Package Declaration</em>' containment reference.
	 * @see #getPackageDeclaration()
	 * @generated
	 */
	void setPackageDeclaration(PackageDeclaration value);

	/**
	 * Returns the value of the '<em><b>Package Body Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Body Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Body Declaration</em>' containment reference.
	 * @see #setPackageBodyDeclaration(PackageBodyDeclaration)
	 * @see Ada.AdaPackage#getElementClass_PackageBodyDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='package_body_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	PackageBodyDeclaration getPackageBodyDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPackageBodyDeclaration <em>Package Body Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Package Body Declaration</em>' containment reference.
	 * @see #getPackageBodyDeclaration()
	 * @generated
	 */
	void setPackageBodyDeclaration(PackageBodyDeclaration value);

	/**
	 * Returns the value of the '<em><b>Object Renaming Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Renaming Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Renaming Declaration</em>' containment reference.
	 * @see #setObjectRenamingDeclaration(ObjectRenamingDeclaration)
	 * @see Ada.AdaPackage#getElementClass_ObjectRenamingDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='object_renaming_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	ObjectRenamingDeclaration getObjectRenamingDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getObjectRenamingDeclaration <em>Object Renaming Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Renaming Declaration</em>' containment reference.
	 * @see #getObjectRenamingDeclaration()
	 * @generated
	 */
	void setObjectRenamingDeclaration(ObjectRenamingDeclaration value);

	/**
	 * Returns the value of the '<em><b>Exception Renaming Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exception Renaming Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exception Renaming Declaration</em>' containment reference.
	 * @see #setExceptionRenamingDeclaration(ExceptionRenamingDeclaration)
	 * @see Ada.AdaPackage#getElementClass_ExceptionRenamingDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='exception_renaming_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	ExceptionRenamingDeclaration getExceptionRenamingDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getExceptionRenamingDeclaration <em>Exception Renaming Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exception Renaming Declaration</em>' containment reference.
	 * @see #getExceptionRenamingDeclaration()
	 * @generated
	 */
	void setExceptionRenamingDeclaration(ExceptionRenamingDeclaration value);

	/**
	 * Returns the value of the '<em><b>Package Renaming Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Renaming Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Renaming Declaration</em>' containment reference.
	 * @see #setPackageRenamingDeclaration(PackageRenamingDeclaration)
	 * @see Ada.AdaPackage#getElementClass_PackageRenamingDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='package_renaming_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	PackageRenamingDeclaration getPackageRenamingDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPackageRenamingDeclaration <em>Package Renaming Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Package Renaming Declaration</em>' containment reference.
	 * @see #getPackageRenamingDeclaration()
	 * @generated
	 */
	void setPackageRenamingDeclaration(PackageRenamingDeclaration value);

	/**
	 * Returns the value of the '<em><b>Procedure Renaming Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure Renaming Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure Renaming Declaration</em>' containment reference.
	 * @see #setProcedureRenamingDeclaration(ProcedureRenamingDeclaration)
	 * @see Ada.AdaPackage#getElementClass_ProcedureRenamingDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='procedure_renaming_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcedureRenamingDeclaration getProcedureRenamingDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getProcedureRenamingDeclaration <em>Procedure Renaming Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Procedure Renaming Declaration</em>' containment reference.
	 * @see #getProcedureRenamingDeclaration()
	 * @generated
	 */
	void setProcedureRenamingDeclaration(ProcedureRenamingDeclaration value);

	/**
	 * Returns the value of the '<em><b>Function Renaming Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Renaming Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Renaming Declaration</em>' containment reference.
	 * @see #setFunctionRenamingDeclaration(FunctionRenamingDeclaration)
	 * @see Ada.AdaPackage#getElementClass_FunctionRenamingDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='function_renaming_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	FunctionRenamingDeclaration getFunctionRenamingDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFunctionRenamingDeclaration <em>Function Renaming Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function Renaming Declaration</em>' containment reference.
	 * @see #getFunctionRenamingDeclaration()
	 * @generated
	 */
	void setFunctionRenamingDeclaration(FunctionRenamingDeclaration value);

	/**
	 * Returns the value of the '<em><b>Generic Package Renaming Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Package Renaming Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Package Renaming Declaration</em>' containment reference.
	 * @see #setGenericPackageRenamingDeclaration(GenericPackageRenamingDeclaration)
	 * @see Ada.AdaPackage#getElementClass_GenericPackageRenamingDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='generic_package_renaming_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	GenericPackageRenamingDeclaration getGenericPackageRenamingDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getGenericPackageRenamingDeclaration <em>Generic Package Renaming Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generic Package Renaming Declaration</em>' containment reference.
	 * @see #getGenericPackageRenamingDeclaration()
	 * @generated
	 */
	void setGenericPackageRenamingDeclaration(GenericPackageRenamingDeclaration value);

	/**
	 * Returns the value of the '<em><b>Generic Procedure Renaming Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Procedure Renaming Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Procedure Renaming Declaration</em>' containment reference.
	 * @see #setGenericProcedureRenamingDeclaration(GenericProcedureRenamingDeclaration)
	 * @see Ada.AdaPackage#getElementClass_GenericProcedureRenamingDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='generic_procedure_renaming_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	GenericProcedureRenamingDeclaration getGenericProcedureRenamingDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getGenericProcedureRenamingDeclaration <em>Generic Procedure Renaming Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generic Procedure Renaming Declaration</em>' containment reference.
	 * @see #getGenericProcedureRenamingDeclaration()
	 * @generated
	 */
	void setGenericProcedureRenamingDeclaration(GenericProcedureRenamingDeclaration value);

	/**
	 * Returns the value of the '<em><b>Generic Function Renaming Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Function Renaming Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Function Renaming Declaration</em>' containment reference.
	 * @see #setGenericFunctionRenamingDeclaration(GenericFunctionRenamingDeclaration)
	 * @see Ada.AdaPackage#getElementClass_GenericFunctionRenamingDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='generic_function_renaming_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	GenericFunctionRenamingDeclaration getGenericFunctionRenamingDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getGenericFunctionRenamingDeclaration <em>Generic Function Renaming Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generic Function Renaming Declaration</em>' containment reference.
	 * @see #getGenericFunctionRenamingDeclaration()
	 * @generated
	 */
	void setGenericFunctionRenamingDeclaration(GenericFunctionRenamingDeclaration value);

	/**
	 * Returns the value of the '<em><b>Task Body Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Body Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Body Declaration</em>' containment reference.
	 * @see #setTaskBodyDeclaration(TaskBodyDeclaration)
	 * @see Ada.AdaPackage#getElementClass_TaskBodyDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='task_body_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	TaskBodyDeclaration getTaskBodyDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getTaskBodyDeclaration <em>Task Body Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Body Declaration</em>' containment reference.
	 * @see #getTaskBodyDeclaration()
	 * @generated
	 */
	void setTaskBodyDeclaration(TaskBodyDeclaration value);

	/**
	 * Returns the value of the '<em><b>Protected Body Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protected Body Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protected Body Declaration</em>' containment reference.
	 * @see #setProtectedBodyDeclaration(ProtectedBodyDeclaration)
	 * @see Ada.AdaPackage#getElementClass_ProtectedBodyDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='protected_body_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	ProtectedBodyDeclaration getProtectedBodyDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getProtectedBodyDeclaration <em>Protected Body Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Protected Body Declaration</em>' containment reference.
	 * @see #getProtectedBodyDeclaration()
	 * @generated
	 */
	void setProtectedBodyDeclaration(ProtectedBodyDeclaration value);

	/**
	 * Returns the value of the '<em><b>Entry Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry Declaration</em>' containment reference.
	 * @see #setEntryDeclaration(EntryDeclaration)
	 * @see Ada.AdaPackage#getElementClass_EntryDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='entry_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	EntryDeclaration getEntryDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getEntryDeclaration <em>Entry Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entry Declaration</em>' containment reference.
	 * @see #getEntryDeclaration()
	 * @generated
	 */
	void setEntryDeclaration(EntryDeclaration value);

	/**
	 * Returns the value of the '<em><b>Entry Body Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry Body Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry Body Declaration</em>' containment reference.
	 * @see #setEntryBodyDeclaration(EntryBodyDeclaration)
	 * @see Ada.AdaPackage#getElementClass_EntryBodyDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='entry_body_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	EntryBodyDeclaration getEntryBodyDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getEntryBodyDeclaration <em>Entry Body Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entry Body Declaration</em>' containment reference.
	 * @see #getEntryBodyDeclaration()
	 * @generated
	 */
	void setEntryBodyDeclaration(EntryBodyDeclaration value);

	/**
	 * Returns the value of the '<em><b>Entry Index Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry Index Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry Index Specification</em>' containment reference.
	 * @see #setEntryIndexSpecification(EntryIndexSpecification)
	 * @see Ada.AdaPackage#getElementClass_EntryIndexSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='entry_index_specification' namespace='##targetNamespace'"
	 * @generated
	 */
	EntryIndexSpecification getEntryIndexSpecification();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getEntryIndexSpecification <em>Entry Index Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entry Index Specification</em>' containment reference.
	 * @see #getEntryIndexSpecification()
	 * @generated
	 */
	void setEntryIndexSpecification(EntryIndexSpecification value);

	/**
	 * Returns the value of the '<em><b>Procedure Body Stub</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure Body Stub</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure Body Stub</em>' containment reference.
	 * @see #setProcedureBodyStub(ProcedureBodyStub)
	 * @see Ada.AdaPackage#getElementClass_ProcedureBodyStub()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='procedure_body_stub' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcedureBodyStub getProcedureBodyStub();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getProcedureBodyStub <em>Procedure Body Stub</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Procedure Body Stub</em>' containment reference.
	 * @see #getProcedureBodyStub()
	 * @generated
	 */
	void setProcedureBodyStub(ProcedureBodyStub value);

	/**
	 * Returns the value of the '<em><b>Function Body Stub</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Body Stub</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Body Stub</em>' containment reference.
	 * @see #setFunctionBodyStub(FunctionBodyStub)
	 * @see Ada.AdaPackage#getElementClass_FunctionBodyStub()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='function_body_stub' namespace='##targetNamespace'"
	 * @generated
	 */
	FunctionBodyStub getFunctionBodyStub();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFunctionBodyStub <em>Function Body Stub</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function Body Stub</em>' containment reference.
	 * @see #getFunctionBodyStub()
	 * @generated
	 */
	void setFunctionBodyStub(FunctionBodyStub value);

	/**
	 * Returns the value of the '<em><b>Package Body Stub</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Body Stub</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Body Stub</em>' containment reference.
	 * @see #setPackageBodyStub(PackageBodyStub)
	 * @see Ada.AdaPackage#getElementClass_PackageBodyStub()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='package_body_stub' namespace='##targetNamespace'"
	 * @generated
	 */
	PackageBodyStub getPackageBodyStub();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPackageBodyStub <em>Package Body Stub</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Package Body Stub</em>' containment reference.
	 * @see #getPackageBodyStub()
	 * @generated
	 */
	void setPackageBodyStub(PackageBodyStub value);

	/**
	 * Returns the value of the '<em><b>Task Body Stub</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Body Stub</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Body Stub</em>' containment reference.
	 * @see #setTaskBodyStub(TaskBodyStub)
	 * @see Ada.AdaPackage#getElementClass_TaskBodyStub()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='task_body_stub' namespace='##targetNamespace'"
	 * @generated
	 */
	TaskBodyStub getTaskBodyStub();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getTaskBodyStub <em>Task Body Stub</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Body Stub</em>' containment reference.
	 * @see #getTaskBodyStub()
	 * @generated
	 */
	void setTaskBodyStub(TaskBodyStub value);

	/**
	 * Returns the value of the '<em><b>Protected Body Stub</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protected Body Stub</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protected Body Stub</em>' containment reference.
	 * @see #setProtectedBodyStub(ProtectedBodyStub)
	 * @see Ada.AdaPackage#getElementClass_ProtectedBodyStub()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='protected_body_stub' namespace='##targetNamespace'"
	 * @generated
	 */
	ProtectedBodyStub getProtectedBodyStub();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getProtectedBodyStub <em>Protected Body Stub</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Protected Body Stub</em>' containment reference.
	 * @see #getProtectedBodyStub()
	 * @generated
	 */
	void setProtectedBodyStub(ProtectedBodyStub value);

	/**
	 * Returns the value of the '<em><b>Exception Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exception Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exception Declaration</em>' containment reference.
	 * @see #setExceptionDeclaration(ExceptionDeclaration)
	 * @see Ada.AdaPackage#getElementClass_ExceptionDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='exception_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	ExceptionDeclaration getExceptionDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getExceptionDeclaration <em>Exception Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exception Declaration</em>' containment reference.
	 * @see #getExceptionDeclaration()
	 * @generated
	 */
	void setExceptionDeclaration(ExceptionDeclaration value);

	/**
	 * Returns the value of the '<em><b>Choice Parameter Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Choice Parameter Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Choice Parameter Specification</em>' containment reference.
	 * @see #setChoiceParameterSpecification(ChoiceParameterSpecification)
	 * @see Ada.AdaPackage#getElementClass_ChoiceParameterSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='choice_parameter_specification' namespace='##targetNamespace'"
	 * @generated
	 */
	ChoiceParameterSpecification getChoiceParameterSpecification();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getChoiceParameterSpecification <em>Choice Parameter Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Choice Parameter Specification</em>' containment reference.
	 * @see #getChoiceParameterSpecification()
	 * @generated
	 */
	void setChoiceParameterSpecification(ChoiceParameterSpecification value);

	/**
	 * Returns the value of the '<em><b>Generic Procedure Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Procedure Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Procedure Declaration</em>' containment reference.
	 * @see #setGenericProcedureDeclaration(GenericProcedureDeclaration)
	 * @see Ada.AdaPackage#getElementClass_GenericProcedureDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='generic_procedure_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	GenericProcedureDeclaration getGenericProcedureDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getGenericProcedureDeclaration <em>Generic Procedure Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generic Procedure Declaration</em>' containment reference.
	 * @see #getGenericProcedureDeclaration()
	 * @generated
	 */
	void setGenericProcedureDeclaration(GenericProcedureDeclaration value);

	/**
	 * Returns the value of the '<em><b>Generic Function Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Function Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Function Declaration</em>' containment reference.
	 * @see #setGenericFunctionDeclaration(GenericFunctionDeclaration)
	 * @see Ada.AdaPackage#getElementClass_GenericFunctionDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='generic_function_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	GenericFunctionDeclaration getGenericFunctionDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getGenericFunctionDeclaration <em>Generic Function Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generic Function Declaration</em>' containment reference.
	 * @see #getGenericFunctionDeclaration()
	 * @generated
	 */
	void setGenericFunctionDeclaration(GenericFunctionDeclaration value);

	/**
	 * Returns the value of the '<em><b>Generic Package Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Package Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Package Declaration</em>' containment reference.
	 * @see #setGenericPackageDeclaration(GenericPackageDeclaration)
	 * @see Ada.AdaPackage#getElementClass_GenericPackageDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='generic_package_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	GenericPackageDeclaration getGenericPackageDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getGenericPackageDeclaration <em>Generic Package Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generic Package Declaration</em>' containment reference.
	 * @see #getGenericPackageDeclaration()
	 * @generated
	 */
	void setGenericPackageDeclaration(GenericPackageDeclaration value);

	/**
	 * Returns the value of the '<em><b>Package Instantiation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Instantiation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Instantiation</em>' containment reference.
	 * @see #setPackageInstantiation(PackageInstantiation)
	 * @see Ada.AdaPackage#getElementClass_PackageInstantiation()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='package_instantiation' namespace='##targetNamespace'"
	 * @generated
	 */
	PackageInstantiation getPackageInstantiation();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPackageInstantiation <em>Package Instantiation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Package Instantiation</em>' containment reference.
	 * @see #getPackageInstantiation()
	 * @generated
	 */
	void setPackageInstantiation(PackageInstantiation value);

	/**
	 * Returns the value of the '<em><b>Procedure Instantiation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure Instantiation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure Instantiation</em>' containment reference.
	 * @see #setProcedureInstantiation(ProcedureInstantiation)
	 * @see Ada.AdaPackage#getElementClass_ProcedureInstantiation()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='procedure_instantiation' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcedureInstantiation getProcedureInstantiation();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getProcedureInstantiation <em>Procedure Instantiation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Procedure Instantiation</em>' containment reference.
	 * @see #getProcedureInstantiation()
	 * @generated
	 */
	void setProcedureInstantiation(ProcedureInstantiation value);

	/**
	 * Returns the value of the '<em><b>Function Instantiation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Instantiation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Instantiation</em>' containment reference.
	 * @see #setFunctionInstantiation(FunctionInstantiation)
	 * @see Ada.AdaPackage#getElementClass_FunctionInstantiation()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='function_instantiation' namespace='##targetNamespace'"
	 * @generated
	 */
	FunctionInstantiation getFunctionInstantiation();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFunctionInstantiation <em>Function Instantiation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function Instantiation</em>' containment reference.
	 * @see #getFunctionInstantiation()
	 * @generated
	 */
	void setFunctionInstantiation(FunctionInstantiation value);

	/**
	 * Returns the value of the '<em><b>Formal Object Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Object Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Object Declaration</em>' containment reference.
	 * @see #setFormalObjectDeclaration(FormalObjectDeclaration)
	 * @see Ada.AdaPackage#getElementClass_FormalObjectDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_object_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalObjectDeclaration getFormalObjectDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalObjectDeclaration <em>Formal Object Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Object Declaration</em>' containment reference.
	 * @see #getFormalObjectDeclaration()
	 * @generated
	 */
	void setFormalObjectDeclaration(FormalObjectDeclaration value);

	/**
	 * Returns the value of the '<em><b>Formal Type Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Type Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Type Declaration</em>' containment reference.
	 * @see #setFormalTypeDeclaration(FormalTypeDeclaration)
	 * @see Ada.AdaPackage#getElementClass_FormalTypeDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_type_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalTypeDeclaration getFormalTypeDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalTypeDeclaration <em>Formal Type Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Type Declaration</em>' containment reference.
	 * @see #getFormalTypeDeclaration()
	 * @generated
	 */
	void setFormalTypeDeclaration(FormalTypeDeclaration value);

	/**
	 * Returns the value of the '<em><b>Formal Incomplete Type Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Incomplete Type Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Incomplete Type Declaration</em>' containment reference.
	 * @see #setFormalIncompleteTypeDeclaration(FormalIncompleteTypeDeclaration)
	 * @see Ada.AdaPackage#getElementClass_FormalIncompleteTypeDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_incomplete_type_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalIncompleteTypeDeclaration getFormalIncompleteTypeDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalIncompleteTypeDeclaration <em>Formal Incomplete Type Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Incomplete Type Declaration</em>' containment reference.
	 * @see #getFormalIncompleteTypeDeclaration()
	 * @generated
	 */
	void setFormalIncompleteTypeDeclaration(FormalIncompleteTypeDeclaration value);

	/**
	 * Returns the value of the '<em><b>Formal Procedure Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Procedure Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Procedure Declaration</em>' containment reference.
	 * @see #setFormalProcedureDeclaration(FormalProcedureDeclaration)
	 * @see Ada.AdaPackage#getElementClass_FormalProcedureDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_procedure_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalProcedureDeclaration getFormalProcedureDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalProcedureDeclaration <em>Formal Procedure Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Procedure Declaration</em>' containment reference.
	 * @see #getFormalProcedureDeclaration()
	 * @generated
	 */
	void setFormalProcedureDeclaration(FormalProcedureDeclaration value);

	/**
	 * Returns the value of the '<em><b>Formal Function Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Function Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Function Declaration</em>' containment reference.
	 * @see #setFormalFunctionDeclaration(FormalFunctionDeclaration)
	 * @see Ada.AdaPackage#getElementClass_FormalFunctionDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_function_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalFunctionDeclaration getFormalFunctionDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalFunctionDeclaration <em>Formal Function Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Function Declaration</em>' containment reference.
	 * @see #getFormalFunctionDeclaration()
	 * @generated
	 */
	void setFormalFunctionDeclaration(FormalFunctionDeclaration value);

	/**
	 * Returns the value of the '<em><b>Formal Package Declaration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Package Declaration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Package Declaration</em>' containment reference.
	 * @see #setFormalPackageDeclaration(FormalPackageDeclaration)
	 * @see Ada.AdaPackage#getElementClass_FormalPackageDeclaration()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_package_declaration' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalPackageDeclaration getFormalPackageDeclaration();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalPackageDeclaration <em>Formal Package Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Package Declaration</em>' containment reference.
	 * @see #getFormalPackageDeclaration()
	 * @generated
	 */
	void setFormalPackageDeclaration(FormalPackageDeclaration value);

	/**
	 * Returns the value of the '<em><b>Formal Package Declaration With Box</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Package Declaration With Box</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Package Declaration With Box</em>' containment reference.
	 * @see #setFormalPackageDeclarationWithBox(FormalPackageDeclarationWithBox)
	 * @see Ada.AdaPackage#getElementClass_FormalPackageDeclarationWithBox()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_package_declaration_with_box' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalPackageDeclarationWithBox getFormalPackageDeclarationWithBox();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalPackageDeclarationWithBox <em>Formal Package Declaration With Box</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Package Declaration With Box</em>' containment reference.
	 * @see #getFormalPackageDeclarationWithBox()
	 * @generated
	 */
	void setFormalPackageDeclarationWithBox(FormalPackageDeclarationWithBox value);

	/**
	 * Returns the value of the '<em><b>Derived Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived Type Definition</em>' containment reference.
	 * @see #setDerivedTypeDefinition(DerivedTypeDefinition)
	 * @see Ada.AdaPackage#getElementClass_DerivedTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='derived_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	DerivedTypeDefinition getDerivedTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDerivedTypeDefinition <em>Derived Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Derived Type Definition</em>' containment reference.
	 * @see #getDerivedTypeDefinition()
	 * @generated
	 */
	void setDerivedTypeDefinition(DerivedTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Derived Record Extension Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived Record Extension Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived Record Extension Definition</em>' containment reference.
	 * @see #setDerivedRecordExtensionDefinition(DerivedRecordExtensionDefinition)
	 * @see Ada.AdaPackage#getElementClass_DerivedRecordExtensionDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='derived_record_extension_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	DerivedRecordExtensionDefinition getDerivedRecordExtensionDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDerivedRecordExtensionDefinition <em>Derived Record Extension Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Derived Record Extension Definition</em>' containment reference.
	 * @see #getDerivedRecordExtensionDefinition()
	 * @generated
	 */
	void setDerivedRecordExtensionDefinition(DerivedRecordExtensionDefinition value);

	/**
	 * Returns the value of the '<em><b>Enumeration Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumeration Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumeration Type Definition</em>' containment reference.
	 * @see #setEnumerationTypeDefinition(EnumerationTypeDefinition)
	 * @see Ada.AdaPackage#getElementClass_EnumerationTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='enumeration_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	EnumerationTypeDefinition getEnumerationTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getEnumerationTypeDefinition <em>Enumeration Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enumeration Type Definition</em>' containment reference.
	 * @see #getEnumerationTypeDefinition()
	 * @generated
	 */
	void setEnumerationTypeDefinition(EnumerationTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Signed Integer Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signed Integer Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signed Integer Type Definition</em>' containment reference.
	 * @see #setSignedIntegerTypeDefinition(SignedIntegerTypeDefinition)
	 * @see Ada.AdaPackage#getElementClass_SignedIntegerTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='signed_integer_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	SignedIntegerTypeDefinition getSignedIntegerTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getSignedIntegerTypeDefinition <em>Signed Integer Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Signed Integer Type Definition</em>' containment reference.
	 * @see #getSignedIntegerTypeDefinition()
	 * @generated
	 */
	void setSignedIntegerTypeDefinition(SignedIntegerTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Modular Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Modular Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Modular Type Definition</em>' containment reference.
	 * @see #setModularTypeDefinition(ModularTypeDefinition)
	 * @see Ada.AdaPackage#getElementClass_ModularTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='modular_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	ModularTypeDefinition getModularTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getModularTypeDefinition <em>Modular Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Modular Type Definition</em>' containment reference.
	 * @see #getModularTypeDefinition()
	 * @generated
	 */
	void setModularTypeDefinition(ModularTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Root Integer Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Integer Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Integer Definition</em>' containment reference.
	 * @see #setRootIntegerDefinition(RootIntegerDefinition)
	 * @see Ada.AdaPackage#getElementClass_RootIntegerDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='root_integer_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	RootIntegerDefinition getRootIntegerDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getRootIntegerDefinition <em>Root Integer Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root Integer Definition</em>' containment reference.
	 * @see #getRootIntegerDefinition()
	 * @generated
	 */
	void setRootIntegerDefinition(RootIntegerDefinition value);

	/**
	 * Returns the value of the '<em><b>Root Real Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Real Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Real Definition</em>' containment reference.
	 * @see #setRootRealDefinition(RootRealDefinition)
	 * @see Ada.AdaPackage#getElementClass_RootRealDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='root_real_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	RootRealDefinition getRootRealDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getRootRealDefinition <em>Root Real Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root Real Definition</em>' containment reference.
	 * @see #getRootRealDefinition()
	 * @generated
	 */
	void setRootRealDefinition(RootRealDefinition value);

	/**
	 * Returns the value of the '<em><b>Universal Integer Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Universal Integer Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Universal Integer Definition</em>' containment reference.
	 * @see #setUniversalIntegerDefinition(UniversalIntegerDefinition)
	 * @see Ada.AdaPackage#getElementClass_UniversalIntegerDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='universal_integer_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	UniversalIntegerDefinition getUniversalIntegerDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getUniversalIntegerDefinition <em>Universal Integer Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Universal Integer Definition</em>' containment reference.
	 * @see #getUniversalIntegerDefinition()
	 * @generated
	 */
	void setUniversalIntegerDefinition(UniversalIntegerDefinition value);

	/**
	 * Returns the value of the '<em><b>Universal Real Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Universal Real Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Universal Real Definition</em>' containment reference.
	 * @see #setUniversalRealDefinition(UniversalRealDefinition)
	 * @see Ada.AdaPackage#getElementClass_UniversalRealDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='universal_real_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	UniversalRealDefinition getUniversalRealDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getUniversalRealDefinition <em>Universal Real Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Universal Real Definition</em>' containment reference.
	 * @see #getUniversalRealDefinition()
	 * @generated
	 */
	void setUniversalRealDefinition(UniversalRealDefinition value);

	/**
	 * Returns the value of the '<em><b>Universal Fixed Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Universal Fixed Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Universal Fixed Definition</em>' containment reference.
	 * @see #setUniversalFixedDefinition(UniversalFixedDefinition)
	 * @see Ada.AdaPackage#getElementClass_UniversalFixedDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='universal_fixed_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	UniversalFixedDefinition getUniversalFixedDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getUniversalFixedDefinition <em>Universal Fixed Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Universal Fixed Definition</em>' containment reference.
	 * @see #getUniversalFixedDefinition()
	 * @generated
	 */
	void setUniversalFixedDefinition(UniversalFixedDefinition value);

	/**
	 * Returns the value of the '<em><b>Floating Point Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Floating Point Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Floating Point Definition</em>' containment reference.
	 * @see #setFloatingPointDefinition(FloatingPointDefinition)
	 * @see Ada.AdaPackage#getElementClass_FloatingPointDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='floating_point_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	FloatingPointDefinition getFloatingPointDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFloatingPointDefinition <em>Floating Point Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Floating Point Definition</em>' containment reference.
	 * @see #getFloatingPointDefinition()
	 * @generated
	 */
	void setFloatingPointDefinition(FloatingPointDefinition value);

	/**
	 * Returns the value of the '<em><b>Ordinary Fixed Point Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ordinary Fixed Point Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ordinary Fixed Point Definition</em>' containment reference.
	 * @see #setOrdinaryFixedPointDefinition(OrdinaryFixedPointDefinition)
	 * @see Ada.AdaPackage#getElementClass_OrdinaryFixedPointDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ordinary_fixed_point_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	OrdinaryFixedPointDefinition getOrdinaryFixedPointDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getOrdinaryFixedPointDefinition <em>Ordinary Fixed Point Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ordinary Fixed Point Definition</em>' containment reference.
	 * @see #getOrdinaryFixedPointDefinition()
	 * @generated
	 */
	void setOrdinaryFixedPointDefinition(OrdinaryFixedPointDefinition value);

	/**
	 * Returns the value of the '<em><b>Decimal Fixed Point Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Decimal Fixed Point Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Decimal Fixed Point Definition</em>' containment reference.
	 * @see #setDecimalFixedPointDefinition(DecimalFixedPointDefinition)
	 * @see Ada.AdaPackage#getElementClass_DecimalFixedPointDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='decimal_fixed_point_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	DecimalFixedPointDefinition getDecimalFixedPointDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDecimalFixedPointDefinition <em>Decimal Fixed Point Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Decimal Fixed Point Definition</em>' containment reference.
	 * @see #getDecimalFixedPointDefinition()
	 * @generated
	 */
	void setDecimalFixedPointDefinition(DecimalFixedPointDefinition value);

	/**
	 * Returns the value of the '<em><b>Unconstrained Array Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unconstrained Array Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unconstrained Array Definition</em>' containment reference.
	 * @see #setUnconstrainedArrayDefinition(UnconstrainedArrayDefinition)
	 * @see Ada.AdaPackage#getElementClass_UnconstrainedArrayDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unconstrained_array_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	UnconstrainedArrayDefinition getUnconstrainedArrayDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getUnconstrainedArrayDefinition <em>Unconstrained Array Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unconstrained Array Definition</em>' containment reference.
	 * @see #getUnconstrainedArrayDefinition()
	 * @generated
	 */
	void setUnconstrainedArrayDefinition(UnconstrainedArrayDefinition value);

	/**
	 * Returns the value of the '<em><b>Constrained Array Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constrained Array Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constrained Array Definition</em>' containment reference.
	 * @see #setConstrainedArrayDefinition(ConstrainedArrayDefinition)
	 * @see Ada.AdaPackage#getElementClass_ConstrainedArrayDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='constrained_array_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	ConstrainedArrayDefinition getConstrainedArrayDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getConstrainedArrayDefinition <em>Constrained Array Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constrained Array Definition</em>' containment reference.
	 * @see #getConstrainedArrayDefinition()
	 * @generated
	 */
	void setConstrainedArrayDefinition(ConstrainedArrayDefinition value);

	/**
	 * Returns the value of the '<em><b>Record Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Type Definition</em>' containment reference.
	 * @see #setRecordTypeDefinition(RecordTypeDefinition)
	 * @see Ada.AdaPackage#getElementClass_RecordTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='record_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	RecordTypeDefinition getRecordTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getRecordTypeDefinition <em>Record Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Record Type Definition</em>' containment reference.
	 * @see #getRecordTypeDefinition()
	 * @generated
	 */
	void setRecordTypeDefinition(RecordTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Tagged Record Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tagged Record Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tagged Record Type Definition</em>' containment reference.
	 * @see #setTaggedRecordTypeDefinition(TaggedRecordTypeDefinition)
	 * @see Ada.AdaPackage#getElementClass_TaggedRecordTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='tagged_record_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	TaggedRecordTypeDefinition getTaggedRecordTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getTaggedRecordTypeDefinition <em>Tagged Record Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tagged Record Type Definition</em>' containment reference.
	 * @see #getTaggedRecordTypeDefinition()
	 * @generated
	 */
	void setTaggedRecordTypeDefinition(TaggedRecordTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Ordinary Interface</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ordinary Interface</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ordinary Interface</em>' containment reference.
	 * @see #setOrdinaryInterface(OrdinaryInterface)
	 * @see Ada.AdaPackage#getElementClass_OrdinaryInterface()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ordinary_interface' namespace='##targetNamespace'"
	 * @generated
	 */
	OrdinaryInterface getOrdinaryInterface();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getOrdinaryInterface <em>Ordinary Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ordinary Interface</em>' containment reference.
	 * @see #getOrdinaryInterface()
	 * @generated
	 */
	void setOrdinaryInterface(OrdinaryInterface value);

	/**
	 * Returns the value of the '<em><b>Limited Interface</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Limited Interface</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Limited Interface</em>' containment reference.
	 * @see #setLimitedInterface(LimitedInterface)
	 * @see Ada.AdaPackage#getElementClass_LimitedInterface()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='limited_interface' namespace='##targetNamespace'"
	 * @generated
	 */
	LimitedInterface getLimitedInterface();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getLimitedInterface <em>Limited Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Limited Interface</em>' containment reference.
	 * @see #getLimitedInterface()
	 * @generated
	 */
	void setLimitedInterface(LimitedInterface value);

	/**
	 * Returns the value of the '<em><b>Task Interface</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Interface</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Interface</em>' containment reference.
	 * @see #setTaskInterface(TaskInterface)
	 * @see Ada.AdaPackage#getElementClass_TaskInterface()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='task_interface' namespace='##targetNamespace'"
	 * @generated
	 */
	TaskInterface getTaskInterface();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getTaskInterface <em>Task Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Interface</em>' containment reference.
	 * @see #getTaskInterface()
	 * @generated
	 */
	void setTaskInterface(TaskInterface value);

	/**
	 * Returns the value of the '<em><b>Protected Interface</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protected Interface</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protected Interface</em>' containment reference.
	 * @see #setProtectedInterface(ProtectedInterface)
	 * @see Ada.AdaPackage#getElementClass_ProtectedInterface()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='protected_interface' namespace='##targetNamespace'"
	 * @generated
	 */
	ProtectedInterface getProtectedInterface();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getProtectedInterface <em>Protected Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Protected Interface</em>' containment reference.
	 * @see #getProtectedInterface()
	 * @generated
	 */
	void setProtectedInterface(ProtectedInterface value);

	/**
	 * Returns the value of the '<em><b>Synchronized Interface</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Synchronized Interface</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Synchronized Interface</em>' containment reference.
	 * @see #setSynchronizedInterface(SynchronizedInterface)
	 * @see Ada.AdaPackage#getElementClass_SynchronizedInterface()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='synchronized_interface' namespace='##targetNamespace'"
	 * @generated
	 */
	SynchronizedInterface getSynchronizedInterface();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getSynchronizedInterface <em>Synchronized Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Synchronized Interface</em>' containment reference.
	 * @see #getSynchronizedInterface()
	 * @generated
	 */
	void setSynchronizedInterface(SynchronizedInterface value);

	/**
	 * Returns the value of the '<em><b>Pool Specific Access To Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pool Specific Access To Variable</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pool Specific Access To Variable</em>' containment reference.
	 * @see #setPoolSpecificAccessToVariable(PoolSpecificAccessToVariable)
	 * @see Ada.AdaPackage#getElementClass_PoolSpecificAccessToVariable()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='pool_specific_access_to_variable' namespace='##targetNamespace'"
	 * @generated
	 */
	PoolSpecificAccessToVariable getPoolSpecificAccessToVariable();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPoolSpecificAccessToVariable <em>Pool Specific Access To Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pool Specific Access To Variable</em>' containment reference.
	 * @see #getPoolSpecificAccessToVariable()
	 * @generated
	 */
	void setPoolSpecificAccessToVariable(PoolSpecificAccessToVariable value);

	/**
	 * Returns the value of the '<em><b>Access To Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Variable</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Variable</em>' containment reference.
	 * @see #setAccessToVariable(AccessToVariable)
	 * @see Ada.AdaPackage#getElementClass_AccessToVariable()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='access_to_variable' namespace='##targetNamespace'"
	 * @generated
	 */
	AccessToVariable getAccessToVariable();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAccessToVariable <em>Access To Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access To Variable</em>' containment reference.
	 * @see #getAccessToVariable()
	 * @generated
	 */
	void setAccessToVariable(AccessToVariable value);

	/**
	 * Returns the value of the '<em><b>Access To Constant</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Constant</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Constant</em>' containment reference.
	 * @see #setAccessToConstant(AccessToConstant)
	 * @see Ada.AdaPackage#getElementClass_AccessToConstant()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='access_to_constant' namespace='##targetNamespace'"
	 * @generated
	 */
	AccessToConstant getAccessToConstant();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAccessToConstant <em>Access To Constant</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access To Constant</em>' containment reference.
	 * @see #getAccessToConstant()
	 * @generated
	 */
	void setAccessToConstant(AccessToConstant value);

	/**
	 * Returns the value of the '<em><b>Access To Procedure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Procedure</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Procedure</em>' containment reference.
	 * @see #setAccessToProcedure(AccessToProcedure)
	 * @see Ada.AdaPackage#getElementClass_AccessToProcedure()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='access_to_procedure' namespace='##targetNamespace'"
	 * @generated
	 */
	AccessToProcedure getAccessToProcedure();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAccessToProcedure <em>Access To Procedure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access To Procedure</em>' containment reference.
	 * @see #getAccessToProcedure()
	 * @generated
	 */
	void setAccessToProcedure(AccessToProcedure value);

	/**
	 * Returns the value of the '<em><b>Access To Protected Procedure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Protected Procedure</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Protected Procedure</em>' containment reference.
	 * @see #setAccessToProtectedProcedure(AccessToProtectedProcedure)
	 * @see Ada.AdaPackage#getElementClass_AccessToProtectedProcedure()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='access_to_protected_procedure' namespace='##targetNamespace'"
	 * @generated
	 */
	AccessToProtectedProcedure getAccessToProtectedProcedure();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAccessToProtectedProcedure <em>Access To Protected Procedure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access To Protected Procedure</em>' containment reference.
	 * @see #getAccessToProtectedProcedure()
	 * @generated
	 */
	void setAccessToProtectedProcedure(AccessToProtectedProcedure value);

	/**
	 * Returns the value of the '<em><b>Access To Function</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Function</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Function</em>' containment reference.
	 * @see #setAccessToFunction(AccessToFunction)
	 * @see Ada.AdaPackage#getElementClass_AccessToFunction()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='access_to_function' namespace='##targetNamespace'"
	 * @generated
	 */
	AccessToFunction getAccessToFunction();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAccessToFunction <em>Access To Function</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access To Function</em>' containment reference.
	 * @see #getAccessToFunction()
	 * @generated
	 */
	void setAccessToFunction(AccessToFunction value);

	/**
	 * Returns the value of the '<em><b>Access To Protected Function</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Protected Function</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Protected Function</em>' containment reference.
	 * @see #setAccessToProtectedFunction(AccessToProtectedFunction)
	 * @see Ada.AdaPackage#getElementClass_AccessToProtectedFunction()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='access_to_protected_function' namespace='##targetNamespace'"
	 * @generated
	 */
	AccessToProtectedFunction getAccessToProtectedFunction();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAccessToProtectedFunction <em>Access To Protected Function</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access To Protected Function</em>' containment reference.
	 * @see #getAccessToProtectedFunction()
	 * @generated
	 */
	void setAccessToProtectedFunction(AccessToProtectedFunction value);

	/**
	 * Returns the value of the '<em><b>Subtype Indication</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subtype Indication</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subtype Indication</em>' containment reference.
	 * @see #setSubtypeIndication(SubtypeIndication)
	 * @see Ada.AdaPackage#getElementClass_SubtypeIndication()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='subtype_indication' namespace='##targetNamespace'"
	 * @generated
	 */
	SubtypeIndication getSubtypeIndication();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getSubtypeIndication <em>Subtype Indication</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Subtype Indication</em>' containment reference.
	 * @see #getSubtypeIndication()
	 * @generated
	 */
	void setSubtypeIndication(SubtypeIndication value);

	/**
	 * Returns the value of the '<em><b>Range Attribute Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Range Attribute Reference</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range Attribute Reference</em>' containment reference.
	 * @see #setRangeAttributeReference(RangeAttributeReference)
	 * @see Ada.AdaPackage#getElementClass_RangeAttributeReference()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='range_attribute_reference' namespace='##targetNamespace'"
	 * @generated
	 */
	RangeAttributeReference getRangeAttributeReference();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getRangeAttributeReference <em>Range Attribute Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Range Attribute Reference</em>' containment reference.
	 * @see #getRangeAttributeReference()
	 * @generated
	 */
	void setRangeAttributeReference(RangeAttributeReference value);

	/**
	 * Returns the value of the '<em><b>Simple Expression Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simple Expression Range</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simple Expression Range</em>' containment reference.
	 * @see #setSimpleExpressionRange(SimpleExpressionRange)
	 * @see Ada.AdaPackage#getElementClass_SimpleExpressionRange()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='simple_expression_range' namespace='##targetNamespace'"
	 * @generated
	 */
	SimpleExpressionRange getSimpleExpressionRange();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getSimpleExpressionRange <em>Simple Expression Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Simple Expression Range</em>' containment reference.
	 * @see #getSimpleExpressionRange()
	 * @generated
	 */
	void setSimpleExpressionRange(SimpleExpressionRange value);

	/**
	 * Returns the value of the '<em><b>Digits Constraint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Digits Constraint</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Digits Constraint</em>' containment reference.
	 * @see #setDigitsConstraint(DigitsConstraint)
	 * @see Ada.AdaPackage#getElementClass_DigitsConstraint()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='digits_constraint' namespace='##targetNamespace'"
	 * @generated
	 */
	DigitsConstraint getDigitsConstraint();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDigitsConstraint <em>Digits Constraint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Digits Constraint</em>' containment reference.
	 * @see #getDigitsConstraint()
	 * @generated
	 */
	void setDigitsConstraint(DigitsConstraint value);

	/**
	 * Returns the value of the '<em><b>Delta Constraint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delta Constraint</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delta Constraint</em>' containment reference.
	 * @see #setDeltaConstraint(DeltaConstraint)
	 * @see Ada.AdaPackage#getElementClass_DeltaConstraint()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='delta_constraint' namespace='##targetNamespace'"
	 * @generated
	 */
	DeltaConstraint getDeltaConstraint();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDeltaConstraint <em>Delta Constraint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delta Constraint</em>' containment reference.
	 * @see #getDeltaConstraint()
	 * @generated
	 */
	void setDeltaConstraint(DeltaConstraint value);

	/**
	 * Returns the value of the '<em><b>Index Constraint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Index Constraint</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Index Constraint</em>' containment reference.
	 * @see #setIndexConstraint(IndexConstraint)
	 * @see Ada.AdaPackage#getElementClass_IndexConstraint()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='index_constraint' namespace='##targetNamespace'"
	 * @generated
	 */
	IndexConstraint getIndexConstraint();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getIndexConstraint <em>Index Constraint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Index Constraint</em>' containment reference.
	 * @see #getIndexConstraint()
	 * @generated
	 */
	void setIndexConstraint(IndexConstraint value);

	/**
	 * Returns the value of the '<em><b>Discriminant Constraint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discriminant Constraint</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discriminant Constraint</em>' containment reference.
	 * @see #setDiscriminantConstraint(DiscriminantConstraint)
	 * @see Ada.AdaPackage#getElementClass_DiscriminantConstraint()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='discriminant_constraint' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscriminantConstraint getDiscriminantConstraint();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDiscriminantConstraint <em>Discriminant Constraint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discriminant Constraint</em>' containment reference.
	 * @see #getDiscriminantConstraint()
	 * @generated
	 */
	void setDiscriminantConstraint(DiscriminantConstraint value);

	/**
	 * Returns the value of the '<em><b>Component Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Definition</em>' containment reference.
	 * @see #setComponentDefinition(ComponentDefinition)
	 * @see Ada.AdaPackage#getElementClass_ComponentDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='component_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	ComponentDefinition getComponentDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getComponentDefinition <em>Component Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component Definition</em>' containment reference.
	 * @see #getComponentDefinition()
	 * @generated
	 */
	void setComponentDefinition(ComponentDefinition value);

	/**
	 * Returns the value of the '<em><b>Discrete Subtype Indication As Subtype Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Subtype Indication As Subtype Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Subtype Indication As Subtype Definition</em>' containment reference.
	 * @see #setDiscreteSubtypeIndicationAsSubtypeDefinition(DiscreteSubtypeIndicationAsSubtypeDefinition)
	 * @see Ada.AdaPackage#getElementClass_DiscreteSubtypeIndicationAsSubtypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='discrete_subtype_indication_as_subtype_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscreteSubtypeIndicationAsSubtypeDefinition getDiscreteSubtypeIndicationAsSubtypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDiscreteSubtypeIndicationAsSubtypeDefinition <em>Discrete Subtype Indication As Subtype Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discrete Subtype Indication As Subtype Definition</em>' containment reference.
	 * @see #getDiscreteSubtypeIndicationAsSubtypeDefinition()
	 * @generated
	 */
	void setDiscreteSubtypeIndicationAsSubtypeDefinition(DiscreteSubtypeIndicationAsSubtypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Discrete Range Attribute Reference As Subtype Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Range Attribute Reference As Subtype Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Range Attribute Reference As Subtype Definition</em>' containment reference.
	 * @see #setDiscreteRangeAttributeReferenceAsSubtypeDefinition(DiscreteRangeAttributeReferenceAsSubtypeDefinition)
	 * @see Ada.AdaPackage#getElementClass_DiscreteRangeAttributeReferenceAsSubtypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='discrete_range_attribute_reference_as_subtype_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscreteRangeAttributeReferenceAsSubtypeDefinition getDiscreteRangeAttributeReferenceAsSubtypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDiscreteRangeAttributeReferenceAsSubtypeDefinition <em>Discrete Range Attribute Reference As Subtype Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discrete Range Attribute Reference As Subtype Definition</em>' containment reference.
	 * @see #getDiscreteRangeAttributeReferenceAsSubtypeDefinition()
	 * @generated
	 */
	void setDiscreteRangeAttributeReferenceAsSubtypeDefinition(DiscreteRangeAttributeReferenceAsSubtypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Discrete Simple Expression Range As Subtype Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Simple Expression Range As Subtype Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Simple Expression Range As Subtype Definition</em>' containment reference.
	 * @see #setDiscreteSimpleExpressionRangeAsSubtypeDefinition(DiscreteSimpleExpressionRangeAsSubtypeDefinition)
	 * @see Ada.AdaPackage#getElementClass_DiscreteSimpleExpressionRangeAsSubtypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='discrete_simple_expression_range_as_subtype_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscreteSimpleExpressionRangeAsSubtypeDefinition getDiscreteSimpleExpressionRangeAsSubtypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDiscreteSimpleExpressionRangeAsSubtypeDefinition <em>Discrete Simple Expression Range As Subtype Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discrete Simple Expression Range As Subtype Definition</em>' containment reference.
	 * @see #getDiscreteSimpleExpressionRangeAsSubtypeDefinition()
	 * @generated
	 */
	void setDiscreteSimpleExpressionRangeAsSubtypeDefinition(DiscreteSimpleExpressionRangeAsSubtypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Discrete Subtype Indication</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Subtype Indication</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Subtype Indication</em>' containment reference.
	 * @see #setDiscreteSubtypeIndication(DiscreteSubtypeIndication)
	 * @see Ada.AdaPackage#getElementClass_DiscreteSubtypeIndication()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='discrete_subtype_indication' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscreteSubtypeIndication getDiscreteSubtypeIndication();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDiscreteSubtypeIndication <em>Discrete Subtype Indication</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discrete Subtype Indication</em>' containment reference.
	 * @see #getDiscreteSubtypeIndication()
	 * @generated
	 */
	void setDiscreteSubtypeIndication(DiscreteSubtypeIndication value);

	/**
	 * Returns the value of the '<em><b>Discrete Range Attribute Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Range Attribute Reference</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Range Attribute Reference</em>' containment reference.
	 * @see #setDiscreteRangeAttributeReference(DiscreteRangeAttributeReference)
	 * @see Ada.AdaPackage#getElementClass_DiscreteRangeAttributeReference()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='discrete_range_attribute_reference' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscreteRangeAttributeReference getDiscreteRangeAttributeReference();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDiscreteRangeAttributeReference <em>Discrete Range Attribute Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discrete Range Attribute Reference</em>' containment reference.
	 * @see #getDiscreteRangeAttributeReference()
	 * @generated
	 */
	void setDiscreteRangeAttributeReference(DiscreteRangeAttributeReference value);

	/**
	 * Returns the value of the '<em><b>Discrete Simple Expression Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Simple Expression Range</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Simple Expression Range</em>' containment reference.
	 * @see #setDiscreteSimpleExpressionRange(DiscreteSimpleExpressionRange)
	 * @see Ada.AdaPackage#getElementClass_DiscreteSimpleExpressionRange()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='discrete_simple_expression_range' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscreteSimpleExpressionRange getDiscreteSimpleExpressionRange();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDiscreteSimpleExpressionRange <em>Discrete Simple Expression Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discrete Simple Expression Range</em>' containment reference.
	 * @see #getDiscreteSimpleExpressionRange()
	 * @generated
	 */
	void setDiscreteSimpleExpressionRange(DiscreteSimpleExpressionRange value);

	/**
	 * Returns the value of the '<em><b>Unknown Discriminant Part</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unknown Discriminant Part</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unknown Discriminant Part</em>' containment reference.
	 * @see #setUnknownDiscriminantPart(UnknownDiscriminantPart)
	 * @see Ada.AdaPackage#getElementClass_UnknownDiscriminantPart()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unknown_discriminant_part' namespace='##targetNamespace'"
	 * @generated
	 */
	UnknownDiscriminantPart getUnknownDiscriminantPart();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getUnknownDiscriminantPart <em>Unknown Discriminant Part</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unknown Discriminant Part</em>' containment reference.
	 * @see #getUnknownDiscriminantPart()
	 * @generated
	 */
	void setUnknownDiscriminantPart(UnknownDiscriminantPart value);

	/**
	 * Returns the value of the '<em><b>Known Discriminant Part</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Known Discriminant Part</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Known Discriminant Part</em>' containment reference.
	 * @see #setKnownDiscriminantPart(KnownDiscriminantPart)
	 * @see Ada.AdaPackage#getElementClass_KnownDiscriminantPart()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='known_discriminant_part' namespace='##targetNamespace'"
	 * @generated
	 */
	KnownDiscriminantPart getKnownDiscriminantPart();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getKnownDiscriminantPart <em>Known Discriminant Part</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Known Discriminant Part</em>' containment reference.
	 * @see #getKnownDiscriminantPart()
	 * @generated
	 */
	void setKnownDiscriminantPart(KnownDiscriminantPart value);

	/**
	 * Returns the value of the '<em><b>Record Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Definition</em>' containment reference.
	 * @see #setRecordDefinition(RecordDefinition)
	 * @see Ada.AdaPackage#getElementClass_RecordDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='record_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	RecordDefinition getRecordDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getRecordDefinition <em>Record Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Record Definition</em>' containment reference.
	 * @see #getRecordDefinition()
	 * @generated
	 */
	void setRecordDefinition(RecordDefinition value);

	/**
	 * Returns the value of the '<em><b>Null Record Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Null Record Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Null Record Definition</em>' containment reference.
	 * @see #setNullRecordDefinition(NullRecordDefinition)
	 * @see Ada.AdaPackage#getElementClass_NullRecordDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='null_record_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	NullRecordDefinition getNullRecordDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getNullRecordDefinition <em>Null Record Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Null Record Definition</em>' containment reference.
	 * @see #getNullRecordDefinition()
	 * @generated
	 */
	void setNullRecordDefinition(NullRecordDefinition value);

	/**
	 * Returns the value of the '<em><b>Null Component</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Null Component</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Null Component</em>' containment reference.
	 * @see #setNullComponent(NullComponent)
	 * @see Ada.AdaPackage#getElementClass_NullComponent()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='null_component' namespace='##targetNamespace'"
	 * @generated
	 */
	NullComponent getNullComponent();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getNullComponent <em>Null Component</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Null Component</em>' containment reference.
	 * @see #getNullComponent()
	 * @generated
	 */
	void setNullComponent(NullComponent value);

	/**
	 * Returns the value of the '<em><b>Variant Part</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variant Part</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variant Part</em>' containment reference.
	 * @see #setVariantPart(VariantPart)
	 * @see Ada.AdaPackage#getElementClass_VariantPart()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='variant_part' namespace='##targetNamespace'"
	 * @generated
	 */
	VariantPart getVariantPart();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getVariantPart <em>Variant Part</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variant Part</em>' containment reference.
	 * @see #getVariantPart()
	 * @generated
	 */
	void setVariantPart(VariantPart value);

	/**
	 * Returns the value of the '<em><b>Variant</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variant</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variant</em>' containment reference.
	 * @see #setVariant(Variant)
	 * @see Ada.AdaPackage#getElementClass_Variant()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='variant' namespace='##targetNamespace'"
	 * @generated
	 */
	Variant getVariant();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getVariant <em>Variant</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variant</em>' containment reference.
	 * @see #getVariant()
	 * @generated
	 */
	void setVariant(Variant value);

	/**
	 * Returns the value of the '<em><b>Others Choice</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Others Choice</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Others Choice</em>' containment reference.
	 * @see #setOthersChoice(OthersChoice)
	 * @see Ada.AdaPackage#getElementClass_OthersChoice()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='others_choice' namespace='##targetNamespace'"
	 * @generated
	 */
	OthersChoice getOthersChoice();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getOthersChoice <em>Others Choice</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Others Choice</em>' containment reference.
	 * @see #getOthersChoice()
	 * @generated
	 */
	void setOthersChoice(OthersChoice value);

	/**
	 * Returns the value of the '<em><b>Anonymous Access To Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Access To Variable</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Access To Variable</em>' containment reference.
	 * @see #setAnonymousAccessToVariable(AnonymousAccessToVariable)
	 * @see Ada.AdaPackage#getElementClass_AnonymousAccessToVariable()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='anonymous_access_to_variable' namespace='##targetNamespace'"
	 * @generated
	 */
	AnonymousAccessToVariable getAnonymousAccessToVariable();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAnonymousAccessToVariable <em>Anonymous Access To Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Anonymous Access To Variable</em>' containment reference.
	 * @see #getAnonymousAccessToVariable()
	 * @generated
	 */
	void setAnonymousAccessToVariable(AnonymousAccessToVariable value);

	/**
	 * Returns the value of the '<em><b>Anonymous Access To Constant</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Access To Constant</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Access To Constant</em>' containment reference.
	 * @see #setAnonymousAccessToConstant(AnonymousAccessToConstant)
	 * @see Ada.AdaPackage#getElementClass_AnonymousAccessToConstant()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='anonymous_access_to_constant' namespace='##targetNamespace'"
	 * @generated
	 */
	AnonymousAccessToConstant getAnonymousAccessToConstant();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAnonymousAccessToConstant <em>Anonymous Access To Constant</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Anonymous Access To Constant</em>' containment reference.
	 * @see #getAnonymousAccessToConstant()
	 * @generated
	 */
	void setAnonymousAccessToConstant(AnonymousAccessToConstant value);

	/**
	 * Returns the value of the '<em><b>Anonymous Access To Procedure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Access To Procedure</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Access To Procedure</em>' containment reference.
	 * @see #setAnonymousAccessToProcedure(AnonymousAccessToProcedure)
	 * @see Ada.AdaPackage#getElementClass_AnonymousAccessToProcedure()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='anonymous_access_to_procedure' namespace='##targetNamespace'"
	 * @generated
	 */
	AnonymousAccessToProcedure getAnonymousAccessToProcedure();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAnonymousAccessToProcedure <em>Anonymous Access To Procedure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Anonymous Access To Procedure</em>' containment reference.
	 * @see #getAnonymousAccessToProcedure()
	 * @generated
	 */
	void setAnonymousAccessToProcedure(AnonymousAccessToProcedure value);

	/**
	 * Returns the value of the '<em><b>Anonymous Access To Protected Procedure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Access To Protected Procedure</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Access To Protected Procedure</em>' containment reference.
	 * @see #setAnonymousAccessToProtectedProcedure(AnonymousAccessToProtectedProcedure)
	 * @see Ada.AdaPackage#getElementClass_AnonymousAccessToProtectedProcedure()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='anonymous_access_to_protected_procedure' namespace='##targetNamespace'"
	 * @generated
	 */
	AnonymousAccessToProtectedProcedure getAnonymousAccessToProtectedProcedure();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAnonymousAccessToProtectedProcedure <em>Anonymous Access To Protected Procedure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Anonymous Access To Protected Procedure</em>' containment reference.
	 * @see #getAnonymousAccessToProtectedProcedure()
	 * @generated
	 */
	void setAnonymousAccessToProtectedProcedure(AnonymousAccessToProtectedProcedure value);

	/**
	 * Returns the value of the '<em><b>Anonymous Access To Function</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Access To Function</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Access To Function</em>' containment reference.
	 * @see #setAnonymousAccessToFunction(AnonymousAccessToFunction)
	 * @see Ada.AdaPackage#getElementClass_AnonymousAccessToFunction()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='anonymous_access_to_function' namespace='##targetNamespace'"
	 * @generated
	 */
	AnonymousAccessToFunction getAnonymousAccessToFunction();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAnonymousAccessToFunction <em>Anonymous Access To Function</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Anonymous Access To Function</em>' containment reference.
	 * @see #getAnonymousAccessToFunction()
	 * @generated
	 */
	void setAnonymousAccessToFunction(AnonymousAccessToFunction value);

	/**
	 * Returns the value of the '<em><b>Anonymous Access To Protected Function</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Access To Protected Function</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Access To Protected Function</em>' containment reference.
	 * @see #setAnonymousAccessToProtectedFunction(AnonymousAccessToProtectedFunction)
	 * @see Ada.AdaPackage#getElementClass_AnonymousAccessToProtectedFunction()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='anonymous_access_to_protected_function' namespace='##targetNamespace'"
	 * @generated
	 */
	AnonymousAccessToProtectedFunction getAnonymousAccessToProtectedFunction();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAnonymousAccessToProtectedFunction <em>Anonymous Access To Protected Function</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Anonymous Access To Protected Function</em>' containment reference.
	 * @see #getAnonymousAccessToProtectedFunction()
	 * @generated
	 */
	void setAnonymousAccessToProtectedFunction(AnonymousAccessToProtectedFunction value);

	/**
	 * Returns the value of the '<em><b>Private Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Private Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Private Type Definition</em>' containment reference.
	 * @see #setPrivateTypeDefinition(PrivateTypeDefinition)
	 * @see Ada.AdaPackage#getElementClass_PrivateTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='private_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	PrivateTypeDefinition getPrivateTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPrivateTypeDefinition <em>Private Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Private Type Definition</em>' containment reference.
	 * @see #getPrivateTypeDefinition()
	 * @generated
	 */
	void setPrivateTypeDefinition(PrivateTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Tagged Private Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tagged Private Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tagged Private Type Definition</em>' containment reference.
	 * @see #setTaggedPrivateTypeDefinition(TaggedPrivateTypeDefinition)
	 * @see Ada.AdaPackage#getElementClass_TaggedPrivateTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='tagged_private_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	TaggedPrivateTypeDefinition getTaggedPrivateTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getTaggedPrivateTypeDefinition <em>Tagged Private Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tagged Private Type Definition</em>' containment reference.
	 * @see #getTaggedPrivateTypeDefinition()
	 * @generated
	 */
	void setTaggedPrivateTypeDefinition(TaggedPrivateTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Private Extension Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Private Extension Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Private Extension Definition</em>' containment reference.
	 * @see #setPrivateExtensionDefinition(PrivateExtensionDefinition)
	 * @see Ada.AdaPackage#getElementClass_PrivateExtensionDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='private_extension_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	PrivateExtensionDefinition getPrivateExtensionDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPrivateExtensionDefinition <em>Private Extension Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Private Extension Definition</em>' containment reference.
	 * @see #getPrivateExtensionDefinition()
	 * @generated
	 */
	void setPrivateExtensionDefinition(PrivateExtensionDefinition value);

	/**
	 * Returns the value of the '<em><b>Task Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Definition</em>' containment reference.
	 * @see #setTaskDefinition(TaskDefinition)
	 * @see Ada.AdaPackage#getElementClass_TaskDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='task_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	TaskDefinition getTaskDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getTaskDefinition <em>Task Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Definition</em>' containment reference.
	 * @see #getTaskDefinition()
	 * @generated
	 */
	void setTaskDefinition(TaskDefinition value);

	/**
	 * Returns the value of the '<em><b>Protected Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protected Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protected Definition</em>' containment reference.
	 * @see #setProtectedDefinition(ProtectedDefinition)
	 * @see Ada.AdaPackage#getElementClass_ProtectedDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='protected_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	ProtectedDefinition getProtectedDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getProtectedDefinition <em>Protected Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Protected Definition</em>' containment reference.
	 * @see #getProtectedDefinition()
	 * @generated
	 */
	void setProtectedDefinition(ProtectedDefinition value);

	/**
	 * Returns the value of the '<em><b>Formal Private Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Private Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Private Type Definition</em>' containment reference.
	 * @see #setFormalPrivateTypeDefinition(FormalPrivateTypeDefinition)
	 * @see Ada.AdaPackage#getElementClass_FormalPrivateTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_private_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalPrivateTypeDefinition getFormalPrivateTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalPrivateTypeDefinition <em>Formal Private Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Private Type Definition</em>' containment reference.
	 * @see #getFormalPrivateTypeDefinition()
	 * @generated
	 */
	void setFormalPrivateTypeDefinition(FormalPrivateTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Formal Tagged Private Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Tagged Private Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Tagged Private Type Definition</em>' containment reference.
	 * @see #setFormalTaggedPrivateTypeDefinition(FormalTaggedPrivateTypeDefinition)
	 * @see Ada.AdaPackage#getElementClass_FormalTaggedPrivateTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_tagged_private_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalTaggedPrivateTypeDefinition getFormalTaggedPrivateTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalTaggedPrivateTypeDefinition <em>Formal Tagged Private Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Tagged Private Type Definition</em>' containment reference.
	 * @see #getFormalTaggedPrivateTypeDefinition()
	 * @generated
	 */
	void setFormalTaggedPrivateTypeDefinition(FormalTaggedPrivateTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Formal Derived Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Derived Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Derived Type Definition</em>' containment reference.
	 * @see #setFormalDerivedTypeDefinition(FormalDerivedTypeDefinition)
	 * @see Ada.AdaPackage#getElementClass_FormalDerivedTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_derived_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalDerivedTypeDefinition getFormalDerivedTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalDerivedTypeDefinition <em>Formal Derived Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Derived Type Definition</em>' containment reference.
	 * @see #getFormalDerivedTypeDefinition()
	 * @generated
	 */
	void setFormalDerivedTypeDefinition(FormalDerivedTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Formal Discrete Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Discrete Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Discrete Type Definition</em>' containment reference.
	 * @see #setFormalDiscreteTypeDefinition(FormalDiscreteTypeDefinition)
	 * @see Ada.AdaPackage#getElementClass_FormalDiscreteTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_discrete_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalDiscreteTypeDefinition getFormalDiscreteTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalDiscreteTypeDefinition <em>Formal Discrete Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Discrete Type Definition</em>' containment reference.
	 * @see #getFormalDiscreteTypeDefinition()
	 * @generated
	 */
	void setFormalDiscreteTypeDefinition(FormalDiscreteTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Formal Signed Integer Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Signed Integer Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Signed Integer Type Definition</em>' containment reference.
	 * @see #setFormalSignedIntegerTypeDefinition(FormalSignedIntegerTypeDefinition)
	 * @see Ada.AdaPackage#getElementClass_FormalSignedIntegerTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_signed_integer_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalSignedIntegerTypeDefinition getFormalSignedIntegerTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalSignedIntegerTypeDefinition <em>Formal Signed Integer Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Signed Integer Type Definition</em>' containment reference.
	 * @see #getFormalSignedIntegerTypeDefinition()
	 * @generated
	 */
	void setFormalSignedIntegerTypeDefinition(FormalSignedIntegerTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Formal Modular Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Modular Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Modular Type Definition</em>' containment reference.
	 * @see #setFormalModularTypeDefinition(FormalModularTypeDefinition)
	 * @see Ada.AdaPackage#getElementClass_FormalModularTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_modular_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalModularTypeDefinition getFormalModularTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalModularTypeDefinition <em>Formal Modular Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Modular Type Definition</em>' containment reference.
	 * @see #getFormalModularTypeDefinition()
	 * @generated
	 */
	void setFormalModularTypeDefinition(FormalModularTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Formal Floating Point Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Floating Point Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Floating Point Definition</em>' containment reference.
	 * @see #setFormalFloatingPointDefinition(FormalFloatingPointDefinition)
	 * @see Ada.AdaPackage#getElementClass_FormalFloatingPointDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_floating_point_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalFloatingPointDefinition getFormalFloatingPointDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalFloatingPointDefinition <em>Formal Floating Point Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Floating Point Definition</em>' containment reference.
	 * @see #getFormalFloatingPointDefinition()
	 * @generated
	 */
	void setFormalFloatingPointDefinition(FormalFloatingPointDefinition value);

	/**
	 * Returns the value of the '<em><b>Formal Ordinary Fixed Point Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Ordinary Fixed Point Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Ordinary Fixed Point Definition</em>' containment reference.
	 * @see #setFormalOrdinaryFixedPointDefinition(FormalOrdinaryFixedPointDefinition)
	 * @see Ada.AdaPackage#getElementClass_FormalOrdinaryFixedPointDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_ordinary_fixed_point_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalOrdinaryFixedPointDefinition getFormalOrdinaryFixedPointDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalOrdinaryFixedPointDefinition <em>Formal Ordinary Fixed Point Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Ordinary Fixed Point Definition</em>' containment reference.
	 * @see #getFormalOrdinaryFixedPointDefinition()
	 * @generated
	 */
	void setFormalOrdinaryFixedPointDefinition(FormalOrdinaryFixedPointDefinition value);

	/**
	 * Returns the value of the '<em><b>Formal Decimal Fixed Point Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Decimal Fixed Point Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Decimal Fixed Point Definition</em>' containment reference.
	 * @see #setFormalDecimalFixedPointDefinition(FormalDecimalFixedPointDefinition)
	 * @see Ada.AdaPackage#getElementClass_FormalDecimalFixedPointDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_decimal_fixed_point_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalDecimalFixedPointDefinition getFormalDecimalFixedPointDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalDecimalFixedPointDefinition <em>Formal Decimal Fixed Point Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Decimal Fixed Point Definition</em>' containment reference.
	 * @see #getFormalDecimalFixedPointDefinition()
	 * @generated
	 */
	void setFormalDecimalFixedPointDefinition(FormalDecimalFixedPointDefinition value);

	/**
	 * Returns the value of the '<em><b>Formal Ordinary Interface</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Ordinary Interface</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Ordinary Interface</em>' containment reference.
	 * @see #setFormalOrdinaryInterface(FormalOrdinaryInterface)
	 * @see Ada.AdaPackage#getElementClass_FormalOrdinaryInterface()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_ordinary_interface' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalOrdinaryInterface getFormalOrdinaryInterface();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalOrdinaryInterface <em>Formal Ordinary Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Ordinary Interface</em>' containment reference.
	 * @see #getFormalOrdinaryInterface()
	 * @generated
	 */
	void setFormalOrdinaryInterface(FormalOrdinaryInterface value);

	/**
	 * Returns the value of the '<em><b>Formal Limited Interface</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Limited Interface</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Limited Interface</em>' containment reference.
	 * @see #setFormalLimitedInterface(FormalLimitedInterface)
	 * @see Ada.AdaPackage#getElementClass_FormalLimitedInterface()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_limited_interface' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalLimitedInterface getFormalLimitedInterface();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalLimitedInterface <em>Formal Limited Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Limited Interface</em>' containment reference.
	 * @see #getFormalLimitedInterface()
	 * @generated
	 */
	void setFormalLimitedInterface(FormalLimitedInterface value);

	/**
	 * Returns the value of the '<em><b>Formal Task Interface</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Task Interface</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Task Interface</em>' containment reference.
	 * @see #setFormalTaskInterface(FormalTaskInterface)
	 * @see Ada.AdaPackage#getElementClass_FormalTaskInterface()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_task_interface' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalTaskInterface getFormalTaskInterface();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalTaskInterface <em>Formal Task Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Task Interface</em>' containment reference.
	 * @see #getFormalTaskInterface()
	 * @generated
	 */
	void setFormalTaskInterface(FormalTaskInterface value);

	/**
	 * Returns the value of the '<em><b>Formal Protected Interface</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Protected Interface</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Protected Interface</em>' containment reference.
	 * @see #setFormalProtectedInterface(FormalProtectedInterface)
	 * @see Ada.AdaPackage#getElementClass_FormalProtectedInterface()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_protected_interface' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalProtectedInterface getFormalProtectedInterface();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalProtectedInterface <em>Formal Protected Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Protected Interface</em>' containment reference.
	 * @see #getFormalProtectedInterface()
	 * @generated
	 */
	void setFormalProtectedInterface(FormalProtectedInterface value);

	/**
	 * Returns the value of the '<em><b>Formal Synchronized Interface</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Synchronized Interface</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Synchronized Interface</em>' containment reference.
	 * @see #setFormalSynchronizedInterface(FormalSynchronizedInterface)
	 * @see Ada.AdaPackage#getElementClass_FormalSynchronizedInterface()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_synchronized_interface' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalSynchronizedInterface getFormalSynchronizedInterface();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalSynchronizedInterface <em>Formal Synchronized Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Synchronized Interface</em>' containment reference.
	 * @see #getFormalSynchronizedInterface()
	 * @generated
	 */
	void setFormalSynchronizedInterface(FormalSynchronizedInterface value);

	/**
	 * Returns the value of the '<em><b>Formal Unconstrained Array Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Unconstrained Array Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Unconstrained Array Definition</em>' containment reference.
	 * @see #setFormalUnconstrainedArrayDefinition(FormalUnconstrainedArrayDefinition)
	 * @see Ada.AdaPackage#getElementClass_FormalUnconstrainedArrayDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_unconstrained_array_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalUnconstrainedArrayDefinition getFormalUnconstrainedArrayDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalUnconstrainedArrayDefinition <em>Formal Unconstrained Array Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Unconstrained Array Definition</em>' containment reference.
	 * @see #getFormalUnconstrainedArrayDefinition()
	 * @generated
	 */
	void setFormalUnconstrainedArrayDefinition(FormalUnconstrainedArrayDefinition value);

	/**
	 * Returns the value of the '<em><b>Formal Constrained Array Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Constrained Array Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Constrained Array Definition</em>' containment reference.
	 * @see #setFormalConstrainedArrayDefinition(FormalConstrainedArrayDefinition)
	 * @see Ada.AdaPackage#getElementClass_FormalConstrainedArrayDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_constrained_array_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalConstrainedArrayDefinition getFormalConstrainedArrayDefinition();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalConstrainedArrayDefinition <em>Formal Constrained Array Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Constrained Array Definition</em>' containment reference.
	 * @see #getFormalConstrainedArrayDefinition()
	 * @generated
	 */
	void setFormalConstrainedArrayDefinition(FormalConstrainedArrayDefinition value);

	/**
	 * Returns the value of the '<em><b>Formal Pool Specific Access To Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Pool Specific Access To Variable</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Pool Specific Access To Variable</em>' containment reference.
	 * @see #setFormalPoolSpecificAccessToVariable(FormalPoolSpecificAccessToVariable)
	 * @see Ada.AdaPackage#getElementClass_FormalPoolSpecificAccessToVariable()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_pool_specific_access_to_variable' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalPoolSpecificAccessToVariable getFormalPoolSpecificAccessToVariable();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalPoolSpecificAccessToVariable <em>Formal Pool Specific Access To Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Pool Specific Access To Variable</em>' containment reference.
	 * @see #getFormalPoolSpecificAccessToVariable()
	 * @generated
	 */
	void setFormalPoolSpecificAccessToVariable(FormalPoolSpecificAccessToVariable value);

	/**
	 * Returns the value of the '<em><b>Formal Access To Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Access To Variable</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Access To Variable</em>' containment reference.
	 * @see #setFormalAccessToVariable(FormalAccessToVariable)
	 * @see Ada.AdaPackage#getElementClass_FormalAccessToVariable()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_access_to_variable' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalAccessToVariable getFormalAccessToVariable();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalAccessToVariable <em>Formal Access To Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Access To Variable</em>' containment reference.
	 * @see #getFormalAccessToVariable()
	 * @generated
	 */
	void setFormalAccessToVariable(FormalAccessToVariable value);

	/**
	 * Returns the value of the '<em><b>Formal Access To Constant</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Access To Constant</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Access To Constant</em>' containment reference.
	 * @see #setFormalAccessToConstant(FormalAccessToConstant)
	 * @see Ada.AdaPackage#getElementClass_FormalAccessToConstant()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_access_to_constant' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalAccessToConstant getFormalAccessToConstant();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalAccessToConstant <em>Formal Access To Constant</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Access To Constant</em>' containment reference.
	 * @see #getFormalAccessToConstant()
	 * @generated
	 */
	void setFormalAccessToConstant(FormalAccessToConstant value);

	/**
	 * Returns the value of the '<em><b>Formal Access To Procedure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Access To Procedure</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Access To Procedure</em>' containment reference.
	 * @see #setFormalAccessToProcedure(FormalAccessToProcedure)
	 * @see Ada.AdaPackage#getElementClass_FormalAccessToProcedure()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_access_to_procedure' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalAccessToProcedure getFormalAccessToProcedure();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalAccessToProcedure <em>Formal Access To Procedure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Access To Procedure</em>' containment reference.
	 * @see #getFormalAccessToProcedure()
	 * @generated
	 */
	void setFormalAccessToProcedure(FormalAccessToProcedure value);

	/**
	 * Returns the value of the '<em><b>Formal Access To Protected Procedure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Access To Protected Procedure</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Access To Protected Procedure</em>' containment reference.
	 * @see #setFormalAccessToProtectedProcedure(FormalAccessToProtectedProcedure)
	 * @see Ada.AdaPackage#getElementClass_FormalAccessToProtectedProcedure()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_access_to_protected_procedure' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalAccessToProtectedProcedure getFormalAccessToProtectedProcedure();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalAccessToProtectedProcedure <em>Formal Access To Protected Procedure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Access To Protected Procedure</em>' containment reference.
	 * @see #getFormalAccessToProtectedProcedure()
	 * @generated
	 */
	void setFormalAccessToProtectedProcedure(FormalAccessToProtectedProcedure value);

	/**
	 * Returns the value of the '<em><b>Formal Access To Function</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Access To Function</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Access To Function</em>' containment reference.
	 * @see #setFormalAccessToFunction(FormalAccessToFunction)
	 * @see Ada.AdaPackage#getElementClass_FormalAccessToFunction()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_access_to_function' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalAccessToFunction getFormalAccessToFunction();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalAccessToFunction <em>Formal Access To Function</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Access To Function</em>' containment reference.
	 * @see #getFormalAccessToFunction()
	 * @generated
	 */
	void setFormalAccessToFunction(FormalAccessToFunction value);

	/**
	 * Returns the value of the '<em><b>Formal Access To Protected Function</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Access To Protected Function</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Access To Protected Function</em>' containment reference.
	 * @see #setFormalAccessToProtectedFunction(FormalAccessToProtectedFunction)
	 * @see Ada.AdaPackage#getElementClass_FormalAccessToProtectedFunction()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_access_to_protected_function' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalAccessToProtectedFunction getFormalAccessToProtectedFunction();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFormalAccessToProtectedFunction <em>Formal Access To Protected Function</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Access To Protected Function</em>' containment reference.
	 * @see #getFormalAccessToProtectedFunction()
	 * @generated
	 */
	void setFormalAccessToProtectedFunction(FormalAccessToProtectedFunction value);

	/**
	 * Returns the value of the '<em><b>Aspect Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aspect Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aspect Specification</em>' containment reference.
	 * @see #setAspectSpecification(AspectSpecification)
	 * @see Ada.AdaPackage#getElementClass_AspectSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='aspect_specification' namespace='##targetNamespace'"
	 * @generated
	 */
	AspectSpecification getAspectSpecification();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAspectSpecification <em>Aspect Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aspect Specification</em>' containment reference.
	 * @see #getAspectSpecification()
	 * @generated
	 */
	void setAspectSpecification(AspectSpecification value);

	/**
	 * Returns the value of the '<em><b>Box Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Box Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Box Expression</em>' containment reference.
	 * @see #setBoxExpression(BoxExpression)
	 * @see Ada.AdaPackage#getElementClass_BoxExpression()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='box_expression' namespace='##targetNamespace'"
	 * @generated
	 */
	BoxExpression getBoxExpression();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getBoxExpression <em>Box Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Box Expression</em>' containment reference.
	 * @see #getBoxExpression()
	 * @generated
	 */
	void setBoxExpression(BoxExpression value);

	/**
	 * Returns the value of the '<em><b>Integer Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integer Literal</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integer Literal</em>' containment reference.
	 * @see #setIntegerLiteral(IntegerLiteral)
	 * @see Ada.AdaPackage#getElementClass_IntegerLiteral()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='integer_literal' namespace='##targetNamespace'"
	 * @generated
	 */
	IntegerLiteral getIntegerLiteral();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getIntegerLiteral <em>Integer Literal</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Integer Literal</em>' containment reference.
	 * @see #getIntegerLiteral()
	 * @generated
	 */
	void setIntegerLiteral(IntegerLiteral value);

	/**
	 * Returns the value of the '<em><b>Real Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Real Literal</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Real Literal</em>' containment reference.
	 * @see #setRealLiteral(RealLiteral)
	 * @see Ada.AdaPackage#getElementClass_RealLiteral()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='real_literal' namespace='##targetNamespace'"
	 * @generated
	 */
	RealLiteral getRealLiteral();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getRealLiteral <em>Real Literal</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Real Literal</em>' containment reference.
	 * @see #getRealLiteral()
	 * @generated
	 */
	void setRealLiteral(RealLiteral value);

	/**
	 * Returns the value of the '<em><b>String Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>String Literal</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String Literal</em>' containment reference.
	 * @see #setStringLiteral(StringLiteral)
	 * @see Ada.AdaPackage#getElementClass_StringLiteral()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='string_literal' namespace='##targetNamespace'"
	 * @generated
	 */
	StringLiteral getStringLiteral();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getStringLiteral <em>String Literal</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>String Literal</em>' containment reference.
	 * @see #getStringLiteral()
	 * @generated
	 */
	void setStringLiteral(StringLiteral value);

	/**
	 * Returns the value of the '<em><b>Identifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Identifier</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identifier</em>' containment reference.
	 * @see #setIdentifier(Identifier)
	 * @see Ada.AdaPackage#getElementClass_Identifier()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='identifier' namespace='##targetNamespace'"
	 * @generated
	 */
	Identifier getIdentifier();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getIdentifier <em>Identifier</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Identifier</em>' containment reference.
	 * @see #getIdentifier()
	 * @generated
	 */
	void setIdentifier(Identifier value);

	/**
	 * Returns the value of the '<em><b>And Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>And Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>And Operator</em>' containment reference.
	 * @see #setAndOperator(AndOperator)
	 * @see Ada.AdaPackage#getElementClass_AndOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='and_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	AndOperator getAndOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAndOperator <em>And Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>And Operator</em>' containment reference.
	 * @see #getAndOperator()
	 * @generated
	 */
	void setAndOperator(AndOperator value);

	/**
	 * Returns the value of the '<em><b>Or Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Or Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Or Operator</em>' containment reference.
	 * @see #setOrOperator(OrOperator)
	 * @see Ada.AdaPackage#getElementClass_OrOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='or_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	OrOperator getOrOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getOrOperator <em>Or Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Or Operator</em>' containment reference.
	 * @see #getOrOperator()
	 * @generated
	 */
	void setOrOperator(OrOperator value);

	/**
	 * Returns the value of the '<em><b>Xor Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Xor Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Xor Operator</em>' containment reference.
	 * @see #setXorOperator(XorOperator)
	 * @see Ada.AdaPackage#getElementClass_XorOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='xor_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	XorOperator getXorOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getXorOperator <em>Xor Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Xor Operator</em>' containment reference.
	 * @see #getXorOperator()
	 * @generated
	 */
	void setXorOperator(XorOperator value);

	/**
	 * Returns the value of the '<em><b>Equal Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equal Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equal Operator</em>' containment reference.
	 * @see #setEqualOperator(EqualOperator)
	 * @see Ada.AdaPackage#getElementClass_EqualOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='equal_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	EqualOperator getEqualOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getEqualOperator <em>Equal Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equal Operator</em>' containment reference.
	 * @see #getEqualOperator()
	 * @generated
	 */
	void setEqualOperator(EqualOperator value);

	/**
	 * Returns the value of the '<em><b>Not Equal Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not Equal Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not Equal Operator</em>' containment reference.
	 * @see #setNotEqualOperator(NotEqualOperator)
	 * @see Ada.AdaPackage#getElementClass_NotEqualOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not_equal_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	NotEqualOperator getNotEqualOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getNotEqualOperator <em>Not Equal Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not Equal Operator</em>' containment reference.
	 * @see #getNotEqualOperator()
	 * @generated
	 */
	void setNotEqualOperator(NotEqualOperator value);

	/**
	 * Returns the value of the '<em><b>Less Than Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Less Than Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Less Than Operator</em>' containment reference.
	 * @see #setLessThanOperator(LessThanOperator)
	 * @see Ada.AdaPackage#getElementClass_LessThanOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='less_than_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	LessThanOperator getLessThanOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getLessThanOperator <em>Less Than Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Less Than Operator</em>' containment reference.
	 * @see #getLessThanOperator()
	 * @generated
	 */
	void setLessThanOperator(LessThanOperator value);

	/**
	 * Returns the value of the '<em><b>Less Than Or Equal Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Less Than Or Equal Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Less Than Or Equal Operator</em>' containment reference.
	 * @see #setLessThanOrEqualOperator(LessThanOrEqualOperator)
	 * @see Ada.AdaPackage#getElementClass_LessThanOrEqualOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='less_than_or_equal_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	LessThanOrEqualOperator getLessThanOrEqualOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getLessThanOrEqualOperator <em>Less Than Or Equal Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Less Than Or Equal Operator</em>' containment reference.
	 * @see #getLessThanOrEqualOperator()
	 * @generated
	 */
	void setLessThanOrEqualOperator(LessThanOrEqualOperator value);

	/**
	 * Returns the value of the '<em><b>Greater Than Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Greater Than Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Greater Than Operator</em>' containment reference.
	 * @see #setGreaterThanOperator(GreaterThanOperator)
	 * @see Ada.AdaPackage#getElementClass_GreaterThanOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='greater_than_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	GreaterThanOperator getGreaterThanOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getGreaterThanOperator <em>Greater Than Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Greater Than Operator</em>' containment reference.
	 * @see #getGreaterThanOperator()
	 * @generated
	 */
	void setGreaterThanOperator(GreaterThanOperator value);

	/**
	 * Returns the value of the '<em><b>Greater Than Or Equal Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Greater Than Or Equal Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Greater Than Or Equal Operator</em>' containment reference.
	 * @see #setGreaterThanOrEqualOperator(GreaterThanOrEqualOperator)
	 * @see Ada.AdaPackage#getElementClass_GreaterThanOrEqualOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='greater_than_or_equal_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	GreaterThanOrEqualOperator getGreaterThanOrEqualOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getGreaterThanOrEqualOperator <em>Greater Than Or Equal Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Greater Than Or Equal Operator</em>' containment reference.
	 * @see #getGreaterThanOrEqualOperator()
	 * @generated
	 */
	void setGreaterThanOrEqualOperator(GreaterThanOrEqualOperator value);

	/**
	 * Returns the value of the '<em><b>Plus Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Plus Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Plus Operator</em>' containment reference.
	 * @see #setPlusOperator(PlusOperator)
	 * @see Ada.AdaPackage#getElementClass_PlusOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='plus_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	PlusOperator getPlusOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPlusOperator <em>Plus Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Plus Operator</em>' containment reference.
	 * @see #getPlusOperator()
	 * @generated
	 */
	void setPlusOperator(PlusOperator value);

	/**
	 * Returns the value of the '<em><b>Minus Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Minus Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Minus Operator</em>' containment reference.
	 * @see #setMinusOperator(MinusOperator)
	 * @see Ada.AdaPackage#getElementClass_MinusOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='minus_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	MinusOperator getMinusOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getMinusOperator <em>Minus Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Minus Operator</em>' containment reference.
	 * @see #getMinusOperator()
	 * @generated
	 */
	void setMinusOperator(MinusOperator value);

	/**
	 * Returns the value of the '<em><b>Concatenate Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Concatenate Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Concatenate Operator</em>' containment reference.
	 * @see #setConcatenateOperator(ConcatenateOperator)
	 * @see Ada.AdaPackage#getElementClass_ConcatenateOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='concatenate_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	ConcatenateOperator getConcatenateOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getConcatenateOperator <em>Concatenate Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Concatenate Operator</em>' containment reference.
	 * @see #getConcatenateOperator()
	 * @generated
	 */
	void setConcatenateOperator(ConcatenateOperator value);

	/**
	 * Returns the value of the '<em><b>Unary Plus Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unary Plus Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unary Plus Operator</em>' containment reference.
	 * @see #setUnaryPlusOperator(UnaryPlusOperator)
	 * @see Ada.AdaPackage#getElementClass_UnaryPlusOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unary_plus_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	UnaryPlusOperator getUnaryPlusOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getUnaryPlusOperator <em>Unary Plus Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unary Plus Operator</em>' containment reference.
	 * @see #getUnaryPlusOperator()
	 * @generated
	 */
	void setUnaryPlusOperator(UnaryPlusOperator value);

	/**
	 * Returns the value of the '<em><b>Unary Minus Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unary Minus Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unary Minus Operator</em>' containment reference.
	 * @see #setUnaryMinusOperator(UnaryMinusOperator)
	 * @see Ada.AdaPackage#getElementClass_UnaryMinusOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unary_minus_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	UnaryMinusOperator getUnaryMinusOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getUnaryMinusOperator <em>Unary Minus Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unary Minus Operator</em>' containment reference.
	 * @see #getUnaryMinusOperator()
	 * @generated
	 */
	void setUnaryMinusOperator(UnaryMinusOperator value);

	/**
	 * Returns the value of the '<em><b>Multiply Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Multiply Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multiply Operator</em>' containment reference.
	 * @see #setMultiplyOperator(MultiplyOperator)
	 * @see Ada.AdaPackage#getElementClass_MultiplyOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='multiply_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	MultiplyOperator getMultiplyOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getMultiplyOperator <em>Multiply Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Multiply Operator</em>' containment reference.
	 * @see #getMultiplyOperator()
	 * @generated
	 */
	void setMultiplyOperator(MultiplyOperator value);

	/**
	 * Returns the value of the '<em><b>Divide Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Divide Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Divide Operator</em>' containment reference.
	 * @see #setDivideOperator(DivideOperator)
	 * @see Ada.AdaPackage#getElementClass_DivideOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='divide_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DivideOperator getDivideOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDivideOperator <em>Divide Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Divide Operator</em>' containment reference.
	 * @see #getDivideOperator()
	 * @generated
	 */
	void setDivideOperator(DivideOperator value);

	/**
	 * Returns the value of the '<em><b>Mod Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mod Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mod Operator</em>' containment reference.
	 * @see #setModOperator(ModOperator)
	 * @see Ada.AdaPackage#getElementClass_ModOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='mod_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	ModOperator getModOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getModOperator <em>Mod Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mod Operator</em>' containment reference.
	 * @see #getModOperator()
	 * @generated
	 */
	void setModOperator(ModOperator value);

	/**
	 * Returns the value of the '<em><b>Rem Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rem Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rem Operator</em>' containment reference.
	 * @see #setRemOperator(RemOperator)
	 * @see Ada.AdaPackage#getElementClass_RemOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='rem_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	RemOperator getRemOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getRemOperator <em>Rem Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rem Operator</em>' containment reference.
	 * @see #getRemOperator()
	 * @generated
	 */
	void setRemOperator(RemOperator value);

	/**
	 * Returns the value of the '<em><b>Exponentiate Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exponentiate Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exponentiate Operator</em>' containment reference.
	 * @see #setExponentiateOperator(ExponentiateOperator)
	 * @see Ada.AdaPackage#getElementClass_ExponentiateOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='exponentiate_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	ExponentiateOperator getExponentiateOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getExponentiateOperator <em>Exponentiate Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exponentiate Operator</em>' containment reference.
	 * @see #getExponentiateOperator()
	 * @generated
	 */
	void setExponentiateOperator(ExponentiateOperator value);

	/**
	 * Returns the value of the '<em><b>Abs Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abs Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abs Operator</em>' containment reference.
	 * @see #setAbsOperator(AbsOperator)
	 * @see Ada.AdaPackage#getElementClass_AbsOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='abs_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	AbsOperator getAbsOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAbsOperator <em>Abs Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abs Operator</em>' containment reference.
	 * @see #getAbsOperator()
	 * @generated
	 */
	void setAbsOperator(AbsOperator value);

	/**
	 * Returns the value of the '<em><b>Not Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not Operator</em>' containment reference.
	 * @see #setNotOperator(NotOperator)
	 * @see Ada.AdaPackage#getElementClass_NotOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	NotOperator getNotOperator();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getNotOperator <em>Not Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not Operator</em>' containment reference.
	 * @see #getNotOperator()
	 * @generated
	 */
	void setNotOperator(NotOperator value);

	/**
	 * Returns the value of the '<em><b>Character Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Character Literal</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Character Literal</em>' containment reference.
	 * @see #setCharacterLiteral(CharacterLiteral)
	 * @see Ada.AdaPackage#getElementClass_CharacterLiteral()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='character_literal' namespace='##targetNamespace'"
	 * @generated
	 */
	CharacterLiteral getCharacterLiteral();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getCharacterLiteral <em>Character Literal</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Character Literal</em>' containment reference.
	 * @see #getCharacterLiteral()
	 * @generated
	 */
	void setCharacterLiteral(CharacterLiteral value);

	/**
	 * Returns the value of the '<em><b>Enumeration Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumeration Literal</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumeration Literal</em>' containment reference.
	 * @see #setEnumerationLiteral(EnumerationLiteral)
	 * @see Ada.AdaPackage#getElementClass_EnumerationLiteral()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='enumeration_literal' namespace='##targetNamespace'"
	 * @generated
	 */
	EnumerationLiteral getEnumerationLiteral();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getEnumerationLiteral <em>Enumeration Literal</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enumeration Literal</em>' containment reference.
	 * @see #getEnumerationLiteral()
	 * @generated
	 */
	void setEnumerationLiteral(EnumerationLiteral value);

	/**
	 * Returns the value of the '<em><b>Explicit Dereference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Explicit Dereference</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Explicit Dereference</em>' containment reference.
	 * @see #setExplicitDereference(ExplicitDereference)
	 * @see Ada.AdaPackage#getElementClass_ExplicitDereference()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='explicit_dereference' namespace='##targetNamespace'"
	 * @generated
	 */
	ExplicitDereference getExplicitDereference();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getExplicitDereference <em>Explicit Dereference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Explicit Dereference</em>' containment reference.
	 * @see #getExplicitDereference()
	 * @generated
	 */
	void setExplicitDereference(ExplicitDereference value);

	/**
	 * Returns the value of the '<em><b>Function Call</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Call</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Call</em>' containment reference.
	 * @see #setFunctionCall(FunctionCall)
	 * @see Ada.AdaPackage#getElementClass_FunctionCall()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='function_call' namespace='##targetNamespace'"
	 * @generated
	 */
	FunctionCall getFunctionCall();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFunctionCall <em>Function Call</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function Call</em>' containment reference.
	 * @see #getFunctionCall()
	 * @generated
	 */
	void setFunctionCall(FunctionCall value);

	/**
	 * Returns the value of the '<em><b>Indexed Component</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Indexed Component</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Indexed Component</em>' containment reference.
	 * @see #setIndexedComponent(IndexedComponent)
	 * @see Ada.AdaPackage#getElementClass_IndexedComponent()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='indexed_component' namespace='##targetNamespace'"
	 * @generated
	 */
	IndexedComponent getIndexedComponent();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getIndexedComponent <em>Indexed Component</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Indexed Component</em>' containment reference.
	 * @see #getIndexedComponent()
	 * @generated
	 */
	void setIndexedComponent(IndexedComponent value);

	/**
	 * Returns the value of the '<em><b>Slice</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Slice</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Slice</em>' containment reference.
	 * @see #setSlice(Slice)
	 * @see Ada.AdaPackage#getElementClass_Slice()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='slice' namespace='##targetNamespace'"
	 * @generated
	 */
	Slice getSlice();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getSlice <em>Slice</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Slice</em>' containment reference.
	 * @see #getSlice()
	 * @generated
	 */
	void setSlice(Slice value);

	/**
	 * Returns the value of the '<em><b>Selected Component</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selected Component</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selected Component</em>' containment reference.
	 * @see #setSelectedComponent(SelectedComponent)
	 * @see Ada.AdaPackage#getElementClass_SelectedComponent()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='selected_component' namespace='##targetNamespace'"
	 * @generated
	 */
	SelectedComponent getSelectedComponent();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getSelectedComponent <em>Selected Component</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selected Component</em>' containment reference.
	 * @see #getSelectedComponent()
	 * @generated
	 */
	void setSelectedComponent(SelectedComponent value);

	/**
	 * Returns the value of the '<em><b>Access Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access Attribute</em>' containment reference.
	 * @see #setAccessAttribute(AccessAttribute)
	 * @see Ada.AdaPackage#getElementClass_AccessAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='access_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	AccessAttribute getAccessAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAccessAttribute <em>Access Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access Attribute</em>' containment reference.
	 * @see #getAccessAttribute()
	 * @generated
	 */
	void setAccessAttribute(AccessAttribute value);

	/**
	 * Returns the value of the '<em><b>Address Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Address Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Address Attribute</em>' containment reference.
	 * @see #setAddressAttribute(AddressAttribute)
	 * @see Ada.AdaPackage#getElementClass_AddressAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='address_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	AddressAttribute getAddressAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAddressAttribute <em>Address Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Address Attribute</em>' containment reference.
	 * @see #getAddressAttribute()
	 * @generated
	 */
	void setAddressAttribute(AddressAttribute value);

	/**
	 * Returns the value of the '<em><b>Adjacent Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Adjacent Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Adjacent Attribute</em>' containment reference.
	 * @see #setAdjacentAttribute(AdjacentAttribute)
	 * @see Ada.AdaPackage#getElementClass_AdjacentAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='adjacent_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	AdjacentAttribute getAdjacentAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAdjacentAttribute <em>Adjacent Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Adjacent Attribute</em>' containment reference.
	 * @see #getAdjacentAttribute()
	 * @generated
	 */
	void setAdjacentAttribute(AdjacentAttribute value);

	/**
	 * Returns the value of the '<em><b>Aft Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aft Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aft Attribute</em>' containment reference.
	 * @see #setAftAttribute(AftAttribute)
	 * @see Ada.AdaPackage#getElementClass_AftAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='aft_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	AftAttribute getAftAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAftAttribute <em>Aft Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aft Attribute</em>' containment reference.
	 * @see #getAftAttribute()
	 * @generated
	 */
	void setAftAttribute(AftAttribute value);

	/**
	 * Returns the value of the '<em><b>Alignment Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alignment Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alignment Attribute</em>' containment reference.
	 * @see #setAlignmentAttribute(AlignmentAttribute)
	 * @see Ada.AdaPackage#getElementClass_AlignmentAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='alignment_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	AlignmentAttribute getAlignmentAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAlignmentAttribute <em>Alignment Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alignment Attribute</em>' containment reference.
	 * @see #getAlignmentAttribute()
	 * @generated
	 */
	void setAlignmentAttribute(AlignmentAttribute value);

	/**
	 * Returns the value of the '<em><b>Base Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Attribute</em>' containment reference.
	 * @see #setBaseAttribute(BaseAttribute)
	 * @see Ada.AdaPackage#getElementClass_BaseAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='base_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	BaseAttribute getBaseAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getBaseAttribute <em>Base Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Attribute</em>' containment reference.
	 * @see #getBaseAttribute()
	 * @generated
	 */
	void setBaseAttribute(BaseAttribute value);

	/**
	 * Returns the value of the '<em><b>Bit Order Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bit Order Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bit Order Attribute</em>' containment reference.
	 * @see #setBitOrderAttribute(BitOrderAttribute)
	 * @see Ada.AdaPackage#getElementClass_BitOrderAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='bit_order_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	BitOrderAttribute getBitOrderAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getBitOrderAttribute <em>Bit Order Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bit Order Attribute</em>' containment reference.
	 * @see #getBitOrderAttribute()
	 * @generated
	 */
	void setBitOrderAttribute(BitOrderAttribute value);

	/**
	 * Returns the value of the '<em><b>Body Version Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body Version Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body Version Attribute</em>' containment reference.
	 * @see #setBodyVersionAttribute(BodyVersionAttribute)
	 * @see Ada.AdaPackage#getElementClass_BodyVersionAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='body_version_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	BodyVersionAttribute getBodyVersionAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getBodyVersionAttribute <em>Body Version Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body Version Attribute</em>' containment reference.
	 * @see #getBodyVersionAttribute()
	 * @generated
	 */
	void setBodyVersionAttribute(BodyVersionAttribute value);

	/**
	 * Returns the value of the '<em><b>Callable Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Callable Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Callable Attribute</em>' containment reference.
	 * @see #setCallableAttribute(CallableAttribute)
	 * @see Ada.AdaPackage#getElementClass_CallableAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='callable_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	CallableAttribute getCallableAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getCallableAttribute <em>Callable Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Callable Attribute</em>' containment reference.
	 * @see #getCallableAttribute()
	 * @generated
	 */
	void setCallableAttribute(CallableAttribute value);

	/**
	 * Returns the value of the '<em><b>Caller Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Caller Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Caller Attribute</em>' containment reference.
	 * @see #setCallerAttribute(CallerAttribute)
	 * @see Ada.AdaPackage#getElementClass_CallerAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='caller_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	CallerAttribute getCallerAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getCallerAttribute <em>Caller Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Caller Attribute</em>' containment reference.
	 * @see #getCallerAttribute()
	 * @generated
	 */
	void setCallerAttribute(CallerAttribute value);

	/**
	 * Returns the value of the '<em><b>Ceiling Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ceiling Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ceiling Attribute</em>' containment reference.
	 * @see #setCeilingAttribute(CeilingAttribute)
	 * @see Ada.AdaPackage#getElementClass_CeilingAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ceiling_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	CeilingAttribute getCeilingAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getCeilingAttribute <em>Ceiling Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ceiling Attribute</em>' containment reference.
	 * @see #getCeilingAttribute()
	 * @generated
	 */
	void setCeilingAttribute(CeilingAttribute value);

	/**
	 * Returns the value of the '<em><b>Class Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Attribute</em>' containment reference.
	 * @see #setClassAttribute(ClassAttribute)
	 * @see Ada.AdaPackage#getElementClass_ClassAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='class_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ClassAttribute getClassAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getClassAttribute <em>Class Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class Attribute</em>' containment reference.
	 * @see #getClassAttribute()
	 * @generated
	 */
	void setClassAttribute(ClassAttribute value);

	/**
	 * Returns the value of the '<em><b>Component Size Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Size Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Size Attribute</em>' containment reference.
	 * @see #setComponentSizeAttribute(ComponentSizeAttribute)
	 * @see Ada.AdaPackage#getElementClass_ComponentSizeAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='component_size_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ComponentSizeAttribute getComponentSizeAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getComponentSizeAttribute <em>Component Size Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component Size Attribute</em>' containment reference.
	 * @see #getComponentSizeAttribute()
	 * @generated
	 */
	void setComponentSizeAttribute(ComponentSizeAttribute value);

	/**
	 * Returns the value of the '<em><b>Compose Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Compose Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Compose Attribute</em>' containment reference.
	 * @see #setComposeAttribute(ComposeAttribute)
	 * @see Ada.AdaPackage#getElementClass_ComposeAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='compose_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ComposeAttribute getComposeAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getComposeAttribute <em>Compose Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Compose Attribute</em>' containment reference.
	 * @see #getComposeAttribute()
	 * @generated
	 */
	void setComposeAttribute(ComposeAttribute value);

	/**
	 * Returns the value of the '<em><b>Constrained Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constrained Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constrained Attribute</em>' containment reference.
	 * @see #setConstrainedAttribute(ConstrainedAttribute)
	 * @see Ada.AdaPackage#getElementClass_ConstrainedAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='constrained_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ConstrainedAttribute getConstrainedAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getConstrainedAttribute <em>Constrained Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constrained Attribute</em>' containment reference.
	 * @see #getConstrainedAttribute()
	 * @generated
	 */
	void setConstrainedAttribute(ConstrainedAttribute value);

	/**
	 * Returns the value of the '<em><b>Copy Sign Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Copy Sign Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Copy Sign Attribute</em>' containment reference.
	 * @see #setCopySignAttribute(CopySignAttribute)
	 * @see Ada.AdaPackage#getElementClass_CopySignAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='copy_sign_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	CopySignAttribute getCopySignAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getCopySignAttribute <em>Copy Sign Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Copy Sign Attribute</em>' containment reference.
	 * @see #getCopySignAttribute()
	 * @generated
	 */
	void setCopySignAttribute(CopySignAttribute value);

	/**
	 * Returns the value of the '<em><b>Count Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Count Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Count Attribute</em>' containment reference.
	 * @see #setCountAttribute(CountAttribute)
	 * @see Ada.AdaPackage#getElementClass_CountAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='count_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	CountAttribute getCountAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getCountAttribute <em>Count Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Count Attribute</em>' containment reference.
	 * @see #getCountAttribute()
	 * @generated
	 */
	void setCountAttribute(CountAttribute value);

	/**
	 * Returns the value of the '<em><b>Definite Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Definite Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definite Attribute</em>' containment reference.
	 * @see #setDefiniteAttribute(DefiniteAttribute)
	 * @see Ada.AdaPackage#getElementClass_DefiniteAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='definite_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiniteAttribute getDefiniteAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefiniteAttribute <em>Definite Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Definite Attribute</em>' containment reference.
	 * @see #getDefiniteAttribute()
	 * @generated
	 */
	void setDefiniteAttribute(DefiniteAttribute value);

	/**
	 * Returns the value of the '<em><b>Delta Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delta Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delta Attribute</em>' containment reference.
	 * @see #setDeltaAttribute(DeltaAttribute)
	 * @see Ada.AdaPackage#getElementClass_DeltaAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='delta_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	DeltaAttribute getDeltaAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDeltaAttribute <em>Delta Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delta Attribute</em>' containment reference.
	 * @see #getDeltaAttribute()
	 * @generated
	 */
	void setDeltaAttribute(DeltaAttribute value);

	/**
	 * Returns the value of the '<em><b>Denorm Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Denorm Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Denorm Attribute</em>' containment reference.
	 * @see #setDenormAttribute(DenormAttribute)
	 * @see Ada.AdaPackage#getElementClass_DenormAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='denorm_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	DenormAttribute getDenormAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDenormAttribute <em>Denorm Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Denorm Attribute</em>' containment reference.
	 * @see #getDenormAttribute()
	 * @generated
	 */
	void setDenormAttribute(DenormAttribute value);

	/**
	 * Returns the value of the '<em><b>Digits Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Digits Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Digits Attribute</em>' containment reference.
	 * @see #setDigitsAttribute(DigitsAttribute)
	 * @see Ada.AdaPackage#getElementClass_DigitsAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='digits_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	DigitsAttribute getDigitsAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDigitsAttribute <em>Digits Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Digits Attribute</em>' containment reference.
	 * @see #getDigitsAttribute()
	 * @generated
	 */
	void setDigitsAttribute(DigitsAttribute value);

	/**
	 * Returns the value of the '<em><b>Exponent Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exponent Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exponent Attribute</em>' containment reference.
	 * @see #setExponentAttribute(ExponentAttribute)
	 * @see Ada.AdaPackage#getElementClass_ExponentAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='exponent_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ExponentAttribute getExponentAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getExponentAttribute <em>Exponent Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exponent Attribute</em>' containment reference.
	 * @see #getExponentAttribute()
	 * @generated
	 */
	void setExponentAttribute(ExponentAttribute value);

	/**
	 * Returns the value of the '<em><b>External Tag Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External Tag Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Tag Attribute</em>' containment reference.
	 * @see #setExternalTagAttribute(ExternalTagAttribute)
	 * @see Ada.AdaPackage#getElementClass_ExternalTagAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='external_tag_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ExternalTagAttribute getExternalTagAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getExternalTagAttribute <em>External Tag Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>External Tag Attribute</em>' containment reference.
	 * @see #getExternalTagAttribute()
	 * @generated
	 */
	void setExternalTagAttribute(ExternalTagAttribute value);

	/**
	 * Returns the value of the '<em><b>First Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Attribute</em>' containment reference.
	 * @see #setFirstAttribute(FirstAttribute)
	 * @see Ada.AdaPackage#getElementClass_FirstAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='first_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	FirstAttribute getFirstAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFirstAttribute <em>First Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First Attribute</em>' containment reference.
	 * @see #getFirstAttribute()
	 * @generated
	 */
	void setFirstAttribute(FirstAttribute value);

	/**
	 * Returns the value of the '<em><b>First Bit Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Bit Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Bit Attribute</em>' containment reference.
	 * @see #setFirstBitAttribute(FirstBitAttribute)
	 * @see Ada.AdaPackage#getElementClass_FirstBitAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='first_bit_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	FirstBitAttribute getFirstBitAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFirstBitAttribute <em>First Bit Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First Bit Attribute</em>' containment reference.
	 * @see #getFirstBitAttribute()
	 * @generated
	 */
	void setFirstBitAttribute(FirstBitAttribute value);

	/**
	 * Returns the value of the '<em><b>Floor Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Floor Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Floor Attribute</em>' containment reference.
	 * @see #setFloorAttribute(FloorAttribute)
	 * @see Ada.AdaPackage#getElementClass_FloorAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='floor_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	FloorAttribute getFloorAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFloorAttribute <em>Floor Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Floor Attribute</em>' containment reference.
	 * @see #getFloorAttribute()
	 * @generated
	 */
	void setFloorAttribute(FloorAttribute value);

	/**
	 * Returns the value of the '<em><b>Fore Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fore Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fore Attribute</em>' containment reference.
	 * @see #setForeAttribute(ForeAttribute)
	 * @see Ada.AdaPackage#getElementClass_ForeAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='fore_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ForeAttribute getForeAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getForeAttribute <em>Fore Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fore Attribute</em>' containment reference.
	 * @see #getForeAttribute()
	 * @generated
	 */
	void setForeAttribute(ForeAttribute value);

	/**
	 * Returns the value of the '<em><b>Fraction Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fraction Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fraction Attribute</em>' containment reference.
	 * @see #setFractionAttribute(FractionAttribute)
	 * @see Ada.AdaPackage#getElementClass_FractionAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='fraction_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	FractionAttribute getFractionAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getFractionAttribute <em>Fraction Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fraction Attribute</em>' containment reference.
	 * @see #getFractionAttribute()
	 * @generated
	 */
	void setFractionAttribute(FractionAttribute value);

	/**
	 * Returns the value of the '<em><b>Identity Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Identity Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identity Attribute</em>' containment reference.
	 * @see #setIdentityAttribute(IdentityAttribute)
	 * @see Ada.AdaPackage#getElementClass_IdentityAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='identity_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentityAttribute getIdentityAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getIdentityAttribute <em>Identity Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Identity Attribute</em>' containment reference.
	 * @see #getIdentityAttribute()
	 * @generated
	 */
	void setIdentityAttribute(IdentityAttribute value);

	/**
	 * Returns the value of the '<em><b>Image Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Image Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Image Attribute</em>' containment reference.
	 * @see #setImageAttribute(ImageAttribute)
	 * @see Ada.AdaPackage#getElementClass_ImageAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='image_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ImageAttribute getImageAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getImageAttribute <em>Image Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Image Attribute</em>' containment reference.
	 * @see #getImageAttribute()
	 * @generated
	 */
	void setImageAttribute(ImageAttribute value);

	/**
	 * Returns the value of the '<em><b>Input Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Attribute</em>' containment reference.
	 * @see #setInputAttribute(InputAttribute)
	 * @see Ada.AdaPackage#getElementClass_InputAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='input_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	InputAttribute getInputAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getInputAttribute <em>Input Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input Attribute</em>' containment reference.
	 * @see #getInputAttribute()
	 * @generated
	 */
	void setInputAttribute(InputAttribute value);

	/**
	 * Returns the value of the '<em><b>Last Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Attribute</em>' containment reference.
	 * @see #setLastAttribute(LastAttribute)
	 * @see Ada.AdaPackage#getElementClass_LastAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='last_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	LastAttribute getLastAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getLastAttribute <em>Last Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Attribute</em>' containment reference.
	 * @see #getLastAttribute()
	 * @generated
	 */
	void setLastAttribute(LastAttribute value);

	/**
	 * Returns the value of the '<em><b>Last Bit Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Bit Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Bit Attribute</em>' containment reference.
	 * @see #setLastBitAttribute(LastBitAttribute)
	 * @see Ada.AdaPackage#getElementClass_LastBitAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='last_bit_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	LastBitAttribute getLastBitAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getLastBitAttribute <em>Last Bit Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Bit Attribute</em>' containment reference.
	 * @see #getLastBitAttribute()
	 * @generated
	 */
	void setLastBitAttribute(LastBitAttribute value);

	/**
	 * Returns the value of the '<em><b>Leading Part Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Leading Part Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Leading Part Attribute</em>' containment reference.
	 * @see #setLeadingPartAttribute(LeadingPartAttribute)
	 * @see Ada.AdaPackage#getElementClass_LeadingPartAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='leading_part_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	LeadingPartAttribute getLeadingPartAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getLeadingPartAttribute <em>Leading Part Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Leading Part Attribute</em>' containment reference.
	 * @see #getLeadingPartAttribute()
	 * @generated
	 */
	void setLeadingPartAttribute(LeadingPartAttribute value);

	/**
	 * Returns the value of the '<em><b>Length Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Length Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Length Attribute</em>' containment reference.
	 * @see #setLengthAttribute(LengthAttribute)
	 * @see Ada.AdaPackage#getElementClass_LengthAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='length_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	LengthAttribute getLengthAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getLengthAttribute <em>Length Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Length Attribute</em>' containment reference.
	 * @see #getLengthAttribute()
	 * @generated
	 */
	void setLengthAttribute(LengthAttribute value);

	/**
	 * Returns the value of the '<em><b>Machine Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Attribute</em>' containment reference.
	 * @see #setMachineAttribute(MachineAttribute)
	 * @see Ada.AdaPackage#getElementClass_MachineAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='machine_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	MachineAttribute getMachineAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getMachineAttribute <em>Machine Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Machine Attribute</em>' containment reference.
	 * @see #getMachineAttribute()
	 * @generated
	 */
	void setMachineAttribute(MachineAttribute value);

	/**
	 * Returns the value of the '<em><b>Machine Emax Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Emax Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Emax Attribute</em>' containment reference.
	 * @see #setMachineEmaxAttribute(MachineEmaxAttribute)
	 * @see Ada.AdaPackage#getElementClass_MachineEmaxAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='machine_emax_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	MachineEmaxAttribute getMachineEmaxAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getMachineEmaxAttribute <em>Machine Emax Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Machine Emax Attribute</em>' containment reference.
	 * @see #getMachineEmaxAttribute()
	 * @generated
	 */
	void setMachineEmaxAttribute(MachineEmaxAttribute value);

	/**
	 * Returns the value of the '<em><b>Machine Emin Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Emin Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Emin Attribute</em>' containment reference.
	 * @see #setMachineEminAttribute(MachineEminAttribute)
	 * @see Ada.AdaPackage#getElementClass_MachineEminAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='machine_emin_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	MachineEminAttribute getMachineEminAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getMachineEminAttribute <em>Machine Emin Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Machine Emin Attribute</em>' containment reference.
	 * @see #getMachineEminAttribute()
	 * @generated
	 */
	void setMachineEminAttribute(MachineEminAttribute value);

	/**
	 * Returns the value of the '<em><b>Machine Mantissa Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Mantissa Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Mantissa Attribute</em>' containment reference.
	 * @see #setMachineMantissaAttribute(MachineMantissaAttribute)
	 * @see Ada.AdaPackage#getElementClass_MachineMantissaAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='machine_mantissa_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	MachineMantissaAttribute getMachineMantissaAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getMachineMantissaAttribute <em>Machine Mantissa Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Machine Mantissa Attribute</em>' containment reference.
	 * @see #getMachineMantissaAttribute()
	 * @generated
	 */
	void setMachineMantissaAttribute(MachineMantissaAttribute value);

	/**
	 * Returns the value of the '<em><b>Machine Overflows Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Overflows Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Overflows Attribute</em>' containment reference.
	 * @see #setMachineOverflowsAttribute(MachineOverflowsAttribute)
	 * @see Ada.AdaPackage#getElementClass_MachineOverflowsAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='machine_overflows_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	MachineOverflowsAttribute getMachineOverflowsAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getMachineOverflowsAttribute <em>Machine Overflows Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Machine Overflows Attribute</em>' containment reference.
	 * @see #getMachineOverflowsAttribute()
	 * @generated
	 */
	void setMachineOverflowsAttribute(MachineOverflowsAttribute value);

	/**
	 * Returns the value of the '<em><b>Machine Radix Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Radix Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Radix Attribute</em>' containment reference.
	 * @see #setMachineRadixAttribute(MachineRadixAttribute)
	 * @see Ada.AdaPackage#getElementClass_MachineRadixAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='machine_radix_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	MachineRadixAttribute getMachineRadixAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getMachineRadixAttribute <em>Machine Radix Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Machine Radix Attribute</em>' containment reference.
	 * @see #getMachineRadixAttribute()
	 * @generated
	 */
	void setMachineRadixAttribute(MachineRadixAttribute value);

	/**
	 * Returns the value of the '<em><b>Machine Rounds Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Rounds Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Rounds Attribute</em>' containment reference.
	 * @see #setMachineRoundsAttribute(MachineRoundsAttribute)
	 * @see Ada.AdaPackage#getElementClass_MachineRoundsAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='machine_rounds_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	MachineRoundsAttribute getMachineRoundsAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getMachineRoundsAttribute <em>Machine Rounds Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Machine Rounds Attribute</em>' containment reference.
	 * @see #getMachineRoundsAttribute()
	 * @generated
	 */
	void setMachineRoundsAttribute(MachineRoundsAttribute value);

	/**
	 * Returns the value of the '<em><b>Max Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Attribute</em>' containment reference.
	 * @see #setMaxAttribute(MaxAttribute)
	 * @see Ada.AdaPackage#getElementClass_MaxAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='max_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	MaxAttribute getMaxAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getMaxAttribute <em>Max Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Attribute</em>' containment reference.
	 * @see #getMaxAttribute()
	 * @generated
	 */
	void setMaxAttribute(MaxAttribute value);

	/**
	 * Returns the value of the '<em><b>Max Size In Storage Elements Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Size In Storage Elements Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Size In Storage Elements Attribute</em>' containment reference.
	 * @see #setMaxSizeInStorageElementsAttribute(MaxSizeInStorageElementsAttribute)
	 * @see Ada.AdaPackage#getElementClass_MaxSizeInStorageElementsAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='max_size_in_storage_elements_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	MaxSizeInStorageElementsAttribute getMaxSizeInStorageElementsAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getMaxSizeInStorageElementsAttribute <em>Max Size In Storage Elements Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Size In Storage Elements Attribute</em>' containment reference.
	 * @see #getMaxSizeInStorageElementsAttribute()
	 * @generated
	 */
	void setMaxSizeInStorageElementsAttribute(MaxSizeInStorageElementsAttribute value);

	/**
	 * Returns the value of the '<em><b>Min Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Attribute</em>' containment reference.
	 * @see #setMinAttribute(MinAttribute)
	 * @see Ada.AdaPackage#getElementClass_MinAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='min_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	MinAttribute getMinAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getMinAttribute <em>Min Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Attribute</em>' containment reference.
	 * @see #getMinAttribute()
	 * @generated
	 */
	void setMinAttribute(MinAttribute value);

	/**
	 * Returns the value of the '<em><b>Model Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Attribute</em>' containment reference.
	 * @see #setModelAttribute(ModelAttribute)
	 * @see Ada.AdaPackage#getElementClass_ModelAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='model_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ModelAttribute getModelAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getModelAttribute <em>Model Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Attribute</em>' containment reference.
	 * @see #getModelAttribute()
	 * @generated
	 */
	void setModelAttribute(ModelAttribute value);

	/**
	 * Returns the value of the '<em><b>Model Emin Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Emin Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Emin Attribute</em>' containment reference.
	 * @see #setModelEminAttribute(ModelEminAttribute)
	 * @see Ada.AdaPackage#getElementClass_ModelEminAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='model_emin_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ModelEminAttribute getModelEminAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getModelEminAttribute <em>Model Emin Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Emin Attribute</em>' containment reference.
	 * @see #getModelEminAttribute()
	 * @generated
	 */
	void setModelEminAttribute(ModelEminAttribute value);

	/**
	 * Returns the value of the '<em><b>Model Epsilon Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Epsilon Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Epsilon Attribute</em>' containment reference.
	 * @see #setModelEpsilonAttribute(ModelEpsilonAttribute)
	 * @see Ada.AdaPackage#getElementClass_ModelEpsilonAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='model_epsilon_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ModelEpsilonAttribute getModelEpsilonAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getModelEpsilonAttribute <em>Model Epsilon Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Epsilon Attribute</em>' containment reference.
	 * @see #getModelEpsilonAttribute()
	 * @generated
	 */
	void setModelEpsilonAttribute(ModelEpsilonAttribute value);

	/**
	 * Returns the value of the '<em><b>Model Mantissa Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Mantissa Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Mantissa Attribute</em>' containment reference.
	 * @see #setModelMantissaAttribute(ModelMantissaAttribute)
	 * @see Ada.AdaPackage#getElementClass_ModelMantissaAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='model_mantissa_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ModelMantissaAttribute getModelMantissaAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getModelMantissaAttribute <em>Model Mantissa Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Mantissa Attribute</em>' containment reference.
	 * @see #getModelMantissaAttribute()
	 * @generated
	 */
	void setModelMantissaAttribute(ModelMantissaAttribute value);

	/**
	 * Returns the value of the '<em><b>Model Small Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Small Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Small Attribute</em>' containment reference.
	 * @see #setModelSmallAttribute(ModelSmallAttribute)
	 * @see Ada.AdaPackage#getElementClass_ModelSmallAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='model_small_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ModelSmallAttribute getModelSmallAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getModelSmallAttribute <em>Model Small Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Small Attribute</em>' containment reference.
	 * @see #getModelSmallAttribute()
	 * @generated
	 */
	void setModelSmallAttribute(ModelSmallAttribute value);

	/**
	 * Returns the value of the '<em><b>Modulus Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Modulus Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Modulus Attribute</em>' containment reference.
	 * @see #setModulusAttribute(ModulusAttribute)
	 * @see Ada.AdaPackage#getElementClass_ModulusAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='modulus_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ModulusAttribute getModulusAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getModulusAttribute <em>Modulus Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Modulus Attribute</em>' containment reference.
	 * @see #getModulusAttribute()
	 * @generated
	 */
	void setModulusAttribute(ModulusAttribute value);

	/**
	 * Returns the value of the '<em><b>Output Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Attribute</em>' containment reference.
	 * @see #setOutputAttribute(OutputAttribute)
	 * @see Ada.AdaPackage#getElementClass_OutputAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='output_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	OutputAttribute getOutputAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getOutputAttribute <em>Output Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Attribute</em>' containment reference.
	 * @see #getOutputAttribute()
	 * @generated
	 */
	void setOutputAttribute(OutputAttribute value);

	/**
	 * Returns the value of the '<em><b>Partition Id Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partition Id Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partition Id Attribute</em>' containment reference.
	 * @see #setPartitionIdAttribute(PartitionIdAttribute)
	 * @see Ada.AdaPackage#getElementClass_PartitionIdAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='partition_id_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	PartitionIdAttribute getPartitionIdAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPartitionIdAttribute <em>Partition Id Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Partition Id Attribute</em>' containment reference.
	 * @see #getPartitionIdAttribute()
	 * @generated
	 */
	void setPartitionIdAttribute(PartitionIdAttribute value);

	/**
	 * Returns the value of the '<em><b>Pos Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pos Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pos Attribute</em>' containment reference.
	 * @see #setPosAttribute(PosAttribute)
	 * @see Ada.AdaPackage#getElementClass_PosAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='pos_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	PosAttribute getPosAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPosAttribute <em>Pos Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pos Attribute</em>' containment reference.
	 * @see #getPosAttribute()
	 * @generated
	 */
	void setPosAttribute(PosAttribute value);

	/**
	 * Returns the value of the '<em><b>Position Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Position Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position Attribute</em>' containment reference.
	 * @see #setPositionAttribute(PositionAttribute)
	 * @see Ada.AdaPackage#getElementClass_PositionAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='position_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	PositionAttribute getPositionAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPositionAttribute <em>Position Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Position Attribute</em>' containment reference.
	 * @see #getPositionAttribute()
	 * @generated
	 */
	void setPositionAttribute(PositionAttribute value);

	/**
	 * Returns the value of the '<em><b>Pred Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pred Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pred Attribute</em>' containment reference.
	 * @see #setPredAttribute(PredAttribute)
	 * @see Ada.AdaPackage#getElementClass_PredAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='pred_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	PredAttribute getPredAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPredAttribute <em>Pred Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pred Attribute</em>' containment reference.
	 * @see #getPredAttribute()
	 * @generated
	 */
	void setPredAttribute(PredAttribute value);

	/**
	 * Returns the value of the '<em><b>Range Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Range Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range Attribute</em>' containment reference.
	 * @see #setRangeAttribute(RangeAttribute)
	 * @see Ada.AdaPackage#getElementClass_RangeAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='range_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	RangeAttribute getRangeAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getRangeAttribute <em>Range Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Range Attribute</em>' containment reference.
	 * @see #getRangeAttribute()
	 * @generated
	 */
	void setRangeAttribute(RangeAttribute value);

	/**
	 * Returns the value of the '<em><b>Read Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Read Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Read Attribute</em>' containment reference.
	 * @see #setReadAttribute(ReadAttribute)
	 * @see Ada.AdaPackage#getElementClass_ReadAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='read_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ReadAttribute getReadAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getReadAttribute <em>Read Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Read Attribute</em>' containment reference.
	 * @see #getReadAttribute()
	 * @generated
	 */
	void setReadAttribute(ReadAttribute value);

	/**
	 * Returns the value of the '<em><b>Remainder Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remainder Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remainder Attribute</em>' containment reference.
	 * @see #setRemainderAttribute(RemainderAttribute)
	 * @see Ada.AdaPackage#getElementClass_RemainderAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='remainder_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	RemainderAttribute getRemainderAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getRemainderAttribute <em>Remainder Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Remainder Attribute</em>' containment reference.
	 * @see #getRemainderAttribute()
	 * @generated
	 */
	void setRemainderAttribute(RemainderAttribute value);

	/**
	 * Returns the value of the '<em><b>Round Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Round Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Round Attribute</em>' containment reference.
	 * @see #setRoundAttribute(RoundAttribute)
	 * @see Ada.AdaPackage#getElementClass_RoundAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='round_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	RoundAttribute getRoundAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getRoundAttribute <em>Round Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Round Attribute</em>' containment reference.
	 * @see #getRoundAttribute()
	 * @generated
	 */
	void setRoundAttribute(RoundAttribute value);

	/**
	 * Returns the value of the '<em><b>Rounding Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rounding Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rounding Attribute</em>' containment reference.
	 * @see #setRoundingAttribute(RoundingAttribute)
	 * @see Ada.AdaPackage#getElementClass_RoundingAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='rounding_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	RoundingAttribute getRoundingAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getRoundingAttribute <em>Rounding Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rounding Attribute</em>' containment reference.
	 * @see #getRoundingAttribute()
	 * @generated
	 */
	void setRoundingAttribute(RoundingAttribute value);

	/**
	 * Returns the value of the '<em><b>Safe First Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Safe First Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Safe First Attribute</em>' containment reference.
	 * @see #setSafeFirstAttribute(SafeFirstAttribute)
	 * @see Ada.AdaPackage#getElementClass_SafeFirstAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='safe_first_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	SafeFirstAttribute getSafeFirstAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getSafeFirstAttribute <em>Safe First Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Safe First Attribute</em>' containment reference.
	 * @see #getSafeFirstAttribute()
	 * @generated
	 */
	void setSafeFirstAttribute(SafeFirstAttribute value);

	/**
	 * Returns the value of the '<em><b>Safe Last Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Safe Last Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Safe Last Attribute</em>' containment reference.
	 * @see #setSafeLastAttribute(SafeLastAttribute)
	 * @see Ada.AdaPackage#getElementClass_SafeLastAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='safe_last_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	SafeLastAttribute getSafeLastAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getSafeLastAttribute <em>Safe Last Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Safe Last Attribute</em>' containment reference.
	 * @see #getSafeLastAttribute()
	 * @generated
	 */
	void setSafeLastAttribute(SafeLastAttribute value);

	/**
	 * Returns the value of the '<em><b>Scale Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scale Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scale Attribute</em>' containment reference.
	 * @see #setScaleAttribute(ScaleAttribute)
	 * @see Ada.AdaPackage#getElementClass_ScaleAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='scale_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ScaleAttribute getScaleAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getScaleAttribute <em>Scale Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scale Attribute</em>' containment reference.
	 * @see #getScaleAttribute()
	 * @generated
	 */
	void setScaleAttribute(ScaleAttribute value);

	/**
	 * Returns the value of the '<em><b>Scaling Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scaling Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scaling Attribute</em>' containment reference.
	 * @see #setScalingAttribute(ScalingAttribute)
	 * @see Ada.AdaPackage#getElementClass_ScalingAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='scaling_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ScalingAttribute getScalingAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getScalingAttribute <em>Scaling Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scaling Attribute</em>' containment reference.
	 * @see #getScalingAttribute()
	 * @generated
	 */
	void setScalingAttribute(ScalingAttribute value);

	/**
	 * Returns the value of the '<em><b>Signed Zeros Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signed Zeros Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signed Zeros Attribute</em>' containment reference.
	 * @see #setSignedZerosAttribute(SignedZerosAttribute)
	 * @see Ada.AdaPackage#getElementClass_SignedZerosAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='signed_zeros_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	SignedZerosAttribute getSignedZerosAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getSignedZerosAttribute <em>Signed Zeros Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Signed Zeros Attribute</em>' containment reference.
	 * @see #getSignedZerosAttribute()
	 * @generated
	 */
	void setSignedZerosAttribute(SignedZerosAttribute value);

	/**
	 * Returns the value of the '<em><b>Size Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Size Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Size Attribute</em>' containment reference.
	 * @see #setSizeAttribute(SizeAttribute)
	 * @see Ada.AdaPackage#getElementClass_SizeAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='size_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	SizeAttribute getSizeAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getSizeAttribute <em>Size Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Size Attribute</em>' containment reference.
	 * @see #getSizeAttribute()
	 * @generated
	 */
	void setSizeAttribute(SizeAttribute value);

	/**
	 * Returns the value of the '<em><b>Small Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Small Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Small Attribute</em>' containment reference.
	 * @see #setSmallAttribute(SmallAttribute)
	 * @see Ada.AdaPackage#getElementClass_SmallAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='small_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	SmallAttribute getSmallAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getSmallAttribute <em>Small Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Small Attribute</em>' containment reference.
	 * @see #getSmallAttribute()
	 * @generated
	 */
	void setSmallAttribute(SmallAttribute value);

	/**
	 * Returns the value of the '<em><b>Storage Pool Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Pool Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Pool Attribute</em>' containment reference.
	 * @see #setStoragePoolAttribute(StoragePoolAttribute)
	 * @see Ada.AdaPackage#getElementClass_StoragePoolAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='storage_pool_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	StoragePoolAttribute getStoragePoolAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getStoragePoolAttribute <em>Storage Pool Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Storage Pool Attribute</em>' containment reference.
	 * @see #getStoragePoolAttribute()
	 * @generated
	 */
	void setStoragePoolAttribute(StoragePoolAttribute value);

	/**
	 * Returns the value of the '<em><b>Storage Size Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Size Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Size Attribute</em>' containment reference.
	 * @see #setStorageSizeAttribute(StorageSizeAttribute)
	 * @see Ada.AdaPackage#getElementClass_StorageSizeAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='storage_size_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	StorageSizeAttribute getStorageSizeAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getStorageSizeAttribute <em>Storage Size Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Storage Size Attribute</em>' containment reference.
	 * @see #getStorageSizeAttribute()
	 * @generated
	 */
	void setStorageSizeAttribute(StorageSizeAttribute value);

	/**
	 * Returns the value of the '<em><b>Succ Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Succ Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Succ Attribute</em>' containment reference.
	 * @see #setSuccAttribute(SuccAttribute)
	 * @see Ada.AdaPackage#getElementClass_SuccAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='succ_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	SuccAttribute getSuccAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getSuccAttribute <em>Succ Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Succ Attribute</em>' containment reference.
	 * @see #getSuccAttribute()
	 * @generated
	 */
	void setSuccAttribute(SuccAttribute value);

	/**
	 * Returns the value of the '<em><b>Tag Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tag Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tag Attribute</em>' containment reference.
	 * @see #setTagAttribute(TagAttribute)
	 * @see Ada.AdaPackage#getElementClass_TagAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='tag_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	TagAttribute getTagAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getTagAttribute <em>Tag Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tag Attribute</em>' containment reference.
	 * @see #getTagAttribute()
	 * @generated
	 */
	void setTagAttribute(TagAttribute value);

	/**
	 * Returns the value of the '<em><b>Terminated Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Terminated Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Terminated Attribute</em>' containment reference.
	 * @see #setTerminatedAttribute(TerminatedAttribute)
	 * @see Ada.AdaPackage#getElementClass_TerminatedAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='terminated_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	TerminatedAttribute getTerminatedAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getTerminatedAttribute <em>Terminated Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Terminated Attribute</em>' containment reference.
	 * @see #getTerminatedAttribute()
	 * @generated
	 */
	void setTerminatedAttribute(TerminatedAttribute value);

	/**
	 * Returns the value of the '<em><b>Truncation Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Truncation Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Truncation Attribute</em>' containment reference.
	 * @see #setTruncationAttribute(TruncationAttribute)
	 * @see Ada.AdaPackage#getElementClass_TruncationAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='truncation_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	TruncationAttribute getTruncationAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getTruncationAttribute <em>Truncation Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Truncation Attribute</em>' containment reference.
	 * @see #getTruncationAttribute()
	 * @generated
	 */
	void setTruncationAttribute(TruncationAttribute value);

	/**
	 * Returns the value of the '<em><b>Unbiased Rounding Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unbiased Rounding Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unbiased Rounding Attribute</em>' containment reference.
	 * @see #setUnbiasedRoundingAttribute(UnbiasedRoundingAttribute)
	 * @see Ada.AdaPackage#getElementClass_UnbiasedRoundingAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unbiased_rounding_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	UnbiasedRoundingAttribute getUnbiasedRoundingAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getUnbiasedRoundingAttribute <em>Unbiased Rounding Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unbiased Rounding Attribute</em>' containment reference.
	 * @see #getUnbiasedRoundingAttribute()
	 * @generated
	 */
	void setUnbiasedRoundingAttribute(UnbiasedRoundingAttribute value);

	/**
	 * Returns the value of the '<em><b>Unchecked Access Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unchecked Access Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unchecked Access Attribute</em>' containment reference.
	 * @see #setUncheckedAccessAttribute(UncheckedAccessAttribute)
	 * @see Ada.AdaPackage#getElementClass_UncheckedAccessAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unchecked_access_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	UncheckedAccessAttribute getUncheckedAccessAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getUncheckedAccessAttribute <em>Unchecked Access Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unchecked Access Attribute</em>' containment reference.
	 * @see #getUncheckedAccessAttribute()
	 * @generated
	 */
	void setUncheckedAccessAttribute(UncheckedAccessAttribute value);

	/**
	 * Returns the value of the '<em><b>Val Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Val Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Val Attribute</em>' containment reference.
	 * @see #setValAttribute(ValAttribute)
	 * @see Ada.AdaPackage#getElementClass_ValAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='val_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ValAttribute getValAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getValAttribute <em>Val Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Val Attribute</em>' containment reference.
	 * @see #getValAttribute()
	 * @generated
	 */
	void setValAttribute(ValAttribute value);

	/**
	 * Returns the value of the '<em><b>Valid Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Valid Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Valid Attribute</em>' containment reference.
	 * @see #setValidAttribute(ValidAttribute)
	 * @see Ada.AdaPackage#getElementClass_ValidAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='valid_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ValidAttribute getValidAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getValidAttribute <em>Valid Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Valid Attribute</em>' containment reference.
	 * @see #getValidAttribute()
	 * @generated
	 */
	void setValidAttribute(ValidAttribute value);

	/**
	 * Returns the value of the '<em><b>Value Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Attribute</em>' containment reference.
	 * @see #setValueAttribute(ValueAttribute)
	 * @see Ada.AdaPackage#getElementClass_ValueAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='value_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ValueAttribute getValueAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getValueAttribute <em>Value Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Attribute</em>' containment reference.
	 * @see #getValueAttribute()
	 * @generated
	 */
	void setValueAttribute(ValueAttribute value);

	/**
	 * Returns the value of the '<em><b>Version Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version Attribute</em>' containment reference.
	 * @see #setVersionAttribute(VersionAttribute)
	 * @see Ada.AdaPackage#getElementClass_VersionAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='version_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	VersionAttribute getVersionAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getVersionAttribute <em>Version Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version Attribute</em>' containment reference.
	 * @see #getVersionAttribute()
	 * @generated
	 */
	void setVersionAttribute(VersionAttribute value);

	/**
	 * Returns the value of the '<em><b>Wide Image Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Image Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Image Attribute</em>' containment reference.
	 * @see #setWideImageAttribute(WideImageAttribute)
	 * @see Ada.AdaPackage#getElementClass_WideImageAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='wide_image_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	WideImageAttribute getWideImageAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getWideImageAttribute <em>Wide Image Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wide Image Attribute</em>' containment reference.
	 * @see #getWideImageAttribute()
	 * @generated
	 */
	void setWideImageAttribute(WideImageAttribute value);

	/**
	 * Returns the value of the '<em><b>Wide Value Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Value Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Value Attribute</em>' containment reference.
	 * @see #setWideValueAttribute(WideValueAttribute)
	 * @see Ada.AdaPackage#getElementClass_WideValueAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='wide_value_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	WideValueAttribute getWideValueAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getWideValueAttribute <em>Wide Value Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wide Value Attribute</em>' containment reference.
	 * @see #getWideValueAttribute()
	 * @generated
	 */
	void setWideValueAttribute(WideValueAttribute value);

	/**
	 * Returns the value of the '<em><b>Wide Width Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Width Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Width Attribute</em>' containment reference.
	 * @see #setWideWidthAttribute(WideWidthAttribute)
	 * @see Ada.AdaPackage#getElementClass_WideWidthAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='wide_width_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	WideWidthAttribute getWideWidthAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getWideWidthAttribute <em>Wide Width Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wide Width Attribute</em>' containment reference.
	 * @see #getWideWidthAttribute()
	 * @generated
	 */
	void setWideWidthAttribute(WideWidthAttribute value);

	/**
	 * Returns the value of the '<em><b>Width Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Width Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Width Attribute</em>' containment reference.
	 * @see #setWidthAttribute(WidthAttribute)
	 * @see Ada.AdaPackage#getElementClass_WidthAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='width_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	WidthAttribute getWidthAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getWidthAttribute <em>Width Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Width Attribute</em>' containment reference.
	 * @see #getWidthAttribute()
	 * @generated
	 */
	void setWidthAttribute(WidthAttribute value);

	/**
	 * Returns the value of the '<em><b>Write Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Write Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Write Attribute</em>' containment reference.
	 * @see #setWriteAttribute(WriteAttribute)
	 * @see Ada.AdaPackage#getElementClass_WriteAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='write_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	WriteAttribute getWriteAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getWriteAttribute <em>Write Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Write Attribute</em>' containment reference.
	 * @see #getWriteAttribute()
	 * @generated
	 */
	void setWriteAttribute(WriteAttribute value);

	/**
	 * Returns the value of the '<em><b>Machine Rounding Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Rounding Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Rounding Attribute</em>' containment reference.
	 * @see #setMachineRoundingAttribute(MachineRoundingAttribute)
	 * @see Ada.AdaPackage#getElementClass_MachineRoundingAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='machine_rounding_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	MachineRoundingAttribute getMachineRoundingAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getMachineRoundingAttribute <em>Machine Rounding Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Machine Rounding Attribute</em>' containment reference.
	 * @see #getMachineRoundingAttribute()
	 * @generated
	 */
	void setMachineRoundingAttribute(MachineRoundingAttribute value);

	/**
	 * Returns the value of the '<em><b>Mod Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mod Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mod Attribute</em>' containment reference.
	 * @see #setModAttribute(ModAttribute)
	 * @see Ada.AdaPackage#getElementClass_ModAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='mod_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ModAttribute getModAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getModAttribute <em>Mod Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mod Attribute</em>' containment reference.
	 * @see #getModAttribute()
	 * @generated
	 */
	void setModAttribute(ModAttribute value);

	/**
	 * Returns the value of the '<em><b>Priority Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Attribute</em>' containment reference.
	 * @see #setPriorityAttribute(PriorityAttribute)
	 * @see Ada.AdaPackage#getElementClass_PriorityAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='priority_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	PriorityAttribute getPriorityAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPriorityAttribute <em>Priority Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Priority Attribute</em>' containment reference.
	 * @see #getPriorityAttribute()
	 * @generated
	 */
	void setPriorityAttribute(PriorityAttribute value);

	/**
	 * Returns the value of the '<em><b>Stream Size Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stream Size Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stream Size Attribute</em>' containment reference.
	 * @see #setStreamSizeAttribute(StreamSizeAttribute)
	 * @see Ada.AdaPackage#getElementClass_StreamSizeAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='stream_size_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	StreamSizeAttribute getStreamSizeAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getStreamSizeAttribute <em>Stream Size Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Stream Size Attribute</em>' containment reference.
	 * @see #getStreamSizeAttribute()
	 * @generated
	 */
	void setStreamSizeAttribute(StreamSizeAttribute value);

	/**
	 * Returns the value of the '<em><b>Wide Wide Image Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Wide Image Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Wide Image Attribute</em>' containment reference.
	 * @see #setWideWideImageAttribute(WideWideImageAttribute)
	 * @see Ada.AdaPackage#getElementClass_WideWideImageAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='wide_wide_image_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	WideWideImageAttribute getWideWideImageAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getWideWideImageAttribute <em>Wide Wide Image Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wide Wide Image Attribute</em>' containment reference.
	 * @see #getWideWideImageAttribute()
	 * @generated
	 */
	void setWideWideImageAttribute(WideWideImageAttribute value);

	/**
	 * Returns the value of the '<em><b>Wide Wide Value Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Wide Value Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Wide Value Attribute</em>' containment reference.
	 * @see #setWideWideValueAttribute(WideWideValueAttribute)
	 * @see Ada.AdaPackage#getElementClass_WideWideValueAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='wide_wide_value_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	WideWideValueAttribute getWideWideValueAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getWideWideValueAttribute <em>Wide Wide Value Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wide Wide Value Attribute</em>' containment reference.
	 * @see #getWideWideValueAttribute()
	 * @generated
	 */
	void setWideWideValueAttribute(WideWideValueAttribute value);

	/**
	 * Returns the value of the '<em><b>Wide Wide Width Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Wide Width Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Wide Width Attribute</em>' containment reference.
	 * @see #setWideWideWidthAttribute(WideWideWidthAttribute)
	 * @see Ada.AdaPackage#getElementClass_WideWideWidthAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='wide_wide_width_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	WideWideWidthAttribute getWideWideWidthAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getWideWideWidthAttribute <em>Wide Wide Width Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wide Wide Width Attribute</em>' containment reference.
	 * @see #getWideWideWidthAttribute()
	 * @generated
	 */
	void setWideWideWidthAttribute(WideWideWidthAttribute value);

	/**
	 * Returns the value of the '<em><b>Max Alignment For Allocation Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Alignment For Allocation Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Alignment For Allocation Attribute</em>' containment reference.
	 * @see #setMaxAlignmentForAllocationAttribute(MaxAlignmentForAllocationAttribute)
	 * @see Ada.AdaPackage#getElementClass_MaxAlignmentForAllocationAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='max_alignment_for_allocation_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	MaxAlignmentForAllocationAttribute getMaxAlignmentForAllocationAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getMaxAlignmentForAllocationAttribute <em>Max Alignment For Allocation Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Alignment For Allocation Attribute</em>' containment reference.
	 * @see #getMaxAlignmentForAllocationAttribute()
	 * @generated
	 */
	void setMaxAlignmentForAllocationAttribute(MaxAlignmentForAllocationAttribute value);

	/**
	 * Returns the value of the '<em><b>Overlaps Storage Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overlaps Storage Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overlaps Storage Attribute</em>' containment reference.
	 * @see #setOverlapsStorageAttribute(OverlapsStorageAttribute)
	 * @see Ada.AdaPackage#getElementClass_OverlapsStorageAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='overlaps_storage_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	OverlapsStorageAttribute getOverlapsStorageAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getOverlapsStorageAttribute <em>Overlaps Storage Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Overlaps Storage Attribute</em>' containment reference.
	 * @see #getOverlapsStorageAttribute()
	 * @generated
	 */
	void setOverlapsStorageAttribute(OverlapsStorageAttribute value);

	/**
	 * Returns the value of the '<em><b>Implementation Defined Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implementation Defined Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implementation Defined Attribute</em>' containment reference.
	 * @see #setImplementationDefinedAttribute(ImplementationDefinedAttribute)
	 * @see Ada.AdaPackage#getElementClass_ImplementationDefinedAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='implementation_defined_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ImplementationDefinedAttribute getImplementationDefinedAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getImplementationDefinedAttribute <em>Implementation Defined Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Implementation Defined Attribute</em>' containment reference.
	 * @see #getImplementationDefinedAttribute()
	 * @generated
	 */
	void setImplementationDefinedAttribute(ImplementationDefinedAttribute value);

	/**
	 * Returns the value of the '<em><b>Unknown Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unknown Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unknown Attribute</em>' containment reference.
	 * @see #setUnknownAttribute(UnknownAttribute)
	 * @see Ada.AdaPackage#getElementClass_UnknownAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unknown_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	UnknownAttribute getUnknownAttribute();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getUnknownAttribute <em>Unknown Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unknown Attribute</em>' containment reference.
	 * @see #getUnknownAttribute()
	 * @generated
	 */
	void setUnknownAttribute(UnknownAttribute value);

	/**
	 * Returns the value of the '<em><b>Record Aggregate</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Aggregate</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Aggregate</em>' containment reference.
	 * @see #setRecordAggregate(RecordAggregate)
	 * @see Ada.AdaPackage#getElementClass_RecordAggregate()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='record_aggregate' namespace='##targetNamespace'"
	 * @generated
	 */
	RecordAggregate getRecordAggregate();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getRecordAggregate <em>Record Aggregate</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Record Aggregate</em>' containment reference.
	 * @see #getRecordAggregate()
	 * @generated
	 */
	void setRecordAggregate(RecordAggregate value);

	/**
	 * Returns the value of the '<em><b>Extension Aggregate</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extension Aggregate</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extension Aggregate</em>' containment reference.
	 * @see #setExtensionAggregate(ExtensionAggregate)
	 * @see Ada.AdaPackage#getElementClass_ExtensionAggregate()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='extension_aggregate' namespace='##targetNamespace'"
	 * @generated
	 */
	ExtensionAggregate getExtensionAggregate();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getExtensionAggregate <em>Extension Aggregate</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extension Aggregate</em>' containment reference.
	 * @see #getExtensionAggregate()
	 * @generated
	 */
	void setExtensionAggregate(ExtensionAggregate value);

	/**
	 * Returns the value of the '<em><b>Positional Array Aggregate</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Positional Array Aggregate</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Positional Array Aggregate</em>' containment reference.
	 * @see #setPositionalArrayAggregate(PositionalArrayAggregate)
	 * @see Ada.AdaPackage#getElementClass_PositionalArrayAggregate()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='positional_array_aggregate' namespace='##targetNamespace'"
	 * @generated
	 */
	PositionalArrayAggregate getPositionalArrayAggregate();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPositionalArrayAggregate <em>Positional Array Aggregate</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Positional Array Aggregate</em>' containment reference.
	 * @see #getPositionalArrayAggregate()
	 * @generated
	 */
	void setPositionalArrayAggregate(PositionalArrayAggregate value);

	/**
	 * Returns the value of the '<em><b>Named Array Aggregate</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Named Array Aggregate</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Named Array Aggregate</em>' containment reference.
	 * @see #setNamedArrayAggregate(NamedArrayAggregate)
	 * @see Ada.AdaPackage#getElementClass_NamedArrayAggregate()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='named_array_aggregate' namespace='##targetNamespace'"
	 * @generated
	 */
	NamedArrayAggregate getNamedArrayAggregate();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getNamedArrayAggregate <em>Named Array Aggregate</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Named Array Aggregate</em>' containment reference.
	 * @see #getNamedArrayAggregate()
	 * @generated
	 */
	void setNamedArrayAggregate(NamedArrayAggregate value);

	/**
	 * Returns the value of the '<em><b>And Then Short Circuit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>And Then Short Circuit</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>And Then Short Circuit</em>' containment reference.
	 * @see #setAndThenShortCircuit(AndThenShortCircuit)
	 * @see Ada.AdaPackage#getElementClass_AndThenShortCircuit()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='and_then_short_circuit' namespace='##targetNamespace'"
	 * @generated
	 */
	AndThenShortCircuit getAndThenShortCircuit();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAndThenShortCircuit <em>And Then Short Circuit</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>And Then Short Circuit</em>' containment reference.
	 * @see #getAndThenShortCircuit()
	 * @generated
	 */
	void setAndThenShortCircuit(AndThenShortCircuit value);

	/**
	 * Returns the value of the '<em><b>Or Else Short Circuit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Or Else Short Circuit</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Or Else Short Circuit</em>' containment reference.
	 * @see #setOrElseShortCircuit(OrElseShortCircuit)
	 * @see Ada.AdaPackage#getElementClass_OrElseShortCircuit()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='or_else_short_circuit' namespace='##targetNamespace'"
	 * @generated
	 */
	OrElseShortCircuit getOrElseShortCircuit();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getOrElseShortCircuit <em>Or Else Short Circuit</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Or Else Short Circuit</em>' containment reference.
	 * @see #getOrElseShortCircuit()
	 * @generated
	 */
	void setOrElseShortCircuit(OrElseShortCircuit value);

	/**
	 * Returns the value of the '<em><b>In Membership Test</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In Membership Test</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In Membership Test</em>' containment reference.
	 * @see #setInMembershipTest(InMembershipTest)
	 * @see Ada.AdaPackage#getElementClass_InMembershipTest()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='in_membership_test' namespace='##targetNamespace'"
	 * @generated
	 */
	InMembershipTest getInMembershipTest();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getInMembershipTest <em>In Membership Test</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>In Membership Test</em>' containment reference.
	 * @see #getInMembershipTest()
	 * @generated
	 */
	void setInMembershipTest(InMembershipTest value);

	/**
	 * Returns the value of the '<em><b>Not In Membership Test</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not In Membership Test</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not In Membership Test</em>' containment reference.
	 * @see #setNotInMembershipTest(NotInMembershipTest)
	 * @see Ada.AdaPackage#getElementClass_NotInMembershipTest()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not_in_membership_test' namespace='##targetNamespace'"
	 * @generated
	 */
	NotInMembershipTest getNotInMembershipTest();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getNotInMembershipTest <em>Not In Membership Test</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not In Membership Test</em>' containment reference.
	 * @see #getNotInMembershipTest()
	 * @generated
	 */
	void setNotInMembershipTest(NotInMembershipTest value);

	/**
	 * Returns the value of the '<em><b>Null Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Null Literal</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Null Literal</em>' containment reference.
	 * @see #setNullLiteral(NullLiteral)
	 * @see Ada.AdaPackage#getElementClass_NullLiteral()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='null_literal' namespace='##targetNamespace'"
	 * @generated
	 */
	NullLiteral getNullLiteral();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getNullLiteral <em>Null Literal</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Null Literal</em>' containment reference.
	 * @see #getNullLiteral()
	 * @generated
	 */
	void setNullLiteral(NullLiteral value);

	/**
	 * Returns the value of the '<em><b>Parenthesized Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parenthesized Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parenthesized Expression</em>' containment reference.
	 * @see #setParenthesizedExpression(ParenthesizedExpression)
	 * @see Ada.AdaPackage#getElementClass_ParenthesizedExpression()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='parenthesized_expression' namespace='##targetNamespace'"
	 * @generated
	 */
	ParenthesizedExpression getParenthesizedExpression();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getParenthesizedExpression <em>Parenthesized Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parenthesized Expression</em>' containment reference.
	 * @see #getParenthesizedExpression()
	 * @generated
	 */
	void setParenthesizedExpression(ParenthesizedExpression value);

	/**
	 * Returns the value of the '<em><b>Raise Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Raise Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Raise Expression</em>' containment reference.
	 * @see #setRaiseExpression(RaiseExpression)
	 * @see Ada.AdaPackage#getElementClass_RaiseExpression()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='raise_expression' namespace='##targetNamespace'"
	 * @generated
	 */
	RaiseExpression getRaiseExpression();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getRaiseExpression <em>Raise Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Raise Expression</em>' containment reference.
	 * @see #getRaiseExpression()
	 * @generated
	 */
	void setRaiseExpression(RaiseExpression value);

	/**
	 * Returns the value of the '<em><b>Type Conversion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Conversion</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Conversion</em>' containment reference.
	 * @see #setTypeConversion(TypeConversion)
	 * @see Ada.AdaPackage#getElementClass_TypeConversion()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='type_conversion' namespace='##targetNamespace'"
	 * @generated
	 */
	TypeConversion getTypeConversion();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getTypeConversion <em>Type Conversion</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type Conversion</em>' containment reference.
	 * @see #getTypeConversion()
	 * @generated
	 */
	void setTypeConversion(TypeConversion value);

	/**
	 * Returns the value of the '<em><b>Qualified Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Qualified Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Qualified Expression</em>' containment reference.
	 * @see #setQualifiedExpression(QualifiedExpression)
	 * @see Ada.AdaPackage#getElementClass_QualifiedExpression()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='qualified_expression' namespace='##targetNamespace'"
	 * @generated
	 */
	QualifiedExpression getQualifiedExpression();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getQualifiedExpression <em>Qualified Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Qualified Expression</em>' containment reference.
	 * @see #getQualifiedExpression()
	 * @generated
	 */
	void setQualifiedExpression(QualifiedExpression value);

	/**
	 * Returns the value of the '<em><b>Allocation From Subtype</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Allocation From Subtype</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Allocation From Subtype</em>' containment reference.
	 * @see #setAllocationFromSubtype(AllocationFromSubtype)
	 * @see Ada.AdaPackage#getElementClass_AllocationFromSubtype()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='allocation_from_subtype' namespace='##targetNamespace'"
	 * @generated
	 */
	AllocationFromSubtype getAllocationFromSubtype();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAllocationFromSubtype <em>Allocation From Subtype</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Allocation From Subtype</em>' containment reference.
	 * @see #getAllocationFromSubtype()
	 * @generated
	 */
	void setAllocationFromSubtype(AllocationFromSubtype value);

	/**
	 * Returns the value of the '<em><b>Allocation From Qualified Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Allocation From Qualified Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Allocation From Qualified Expression</em>' containment reference.
	 * @see #setAllocationFromQualifiedExpression(AllocationFromQualifiedExpression)
	 * @see Ada.AdaPackage#getElementClass_AllocationFromQualifiedExpression()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='allocation_from_qualified_expression' namespace='##targetNamespace'"
	 * @generated
	 */
	AllocationFromQualifiedExpression getAllocationFromQualifiedExpression();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAllocationFromQualifiedExpression <em>Allocation From Qualified Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Allocation From Qualified Expression</em>' containment reference.
	 * @see #getAllocationFromQualifiedExpression()
	 * @generated
	 */
	void setAllocationFromQualifiedExpression(AllocationFromQualifiedExpression value);

	/**
	 * Returns the value of the '<em><b>Case Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Case Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Case Expression</em>' containment reference.
	 * @see #setCaseExpression(CaseExpression)
	 * @see Ada.AdaPackage#getElementClass_CaseExpression()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='case_expression' namespace='##targetNamespace'"
	 * @generated
	 */
	CaseExpression getCaseExpression();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getCaseExpression <em>Case Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Case Expression</em>' containment reference.
	 * @see #getCaseExpression()
	 * @generated
	 */
	void setCaseExpression(CaseExpression value);

	/**
	 * Returns the value of the '<em><b>If Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>If Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>If Expression</em>' containment reference.
	 * @see #setIfExpression(IfExpression)
	 * @see Ada.AdaPackage#getElementClass_IfExpression()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='if_expression' namespace='##targetNamespace'"
	 * @generated
	 */
	IfExpression getIfExpression();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getIfExpression <em>If Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>If Expression</em>' containment reference.
	 * @see #getIfExpression()
	 * @generated
	 */
	void setIfExpression(IfExpression value);

	/**
	 * Returns the value of the '<em><b>For All Quantified Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>For All Quantified Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>For All Quantified Expression</em>' containment reference.
	 * @see #setForAllQuantifiedExpression(ForAllQuantifiedExpression)
	 * @see Ada.AdaPackage#getElementClass_ForAllQuantifiedExpression()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='for_all_quantified_expression' namespace='##targetNamespace'"
	 * @generated
	 */
	ForAllQuantifiedExpression getForAllQuantifiedExpression();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getForAllQuantifiedExpression <em>For All Quantified Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>For All Quantified Expression</em>' containment reference.
	 * @see #getForAllQuantifiedExpression()
	 * @generated
	 */
	void setForAllQuantifiedExpression(ForAllQuantifiedExpression value);

	/**
	 * Returns the value of the '<em><b>For Some Quantified Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>For Some Quantified Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>For Some Quantified Expression</em>' containment reference.
	 * @see #setForSomeQuantifiedExpression(ForSomeQuantifiedExpression)
	 * @see Ada.AdaPackage#getElementClass_ForSomeQuantifiedExpression()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='for_some_quantified_expression' namespace='##targetNamespace'"
	 * @generated
	 */
	ForSomeQuantifiedExpression getForSomeQuantifiedExpression();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getForSomeQuantifiedExpression <em>For Some Quantified Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>For Some Quantified Expression</em>' containment reference.
	 * @see #getForSomeQuantifiedExpression()
	 * @generated
	 */
	void setForSomeQuantifiedExpression(ForSomeQuantifiedExpression value);

	/**
	 * Returns the value of the '<em><b>Pragma Argument Association</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pragma Argument Association</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pragma Argument Association</em>' containment reference.
	 * @see #setPragmaArgumentAssociation(PragmaArgumentAssociation)
	 * @see Ada.AdaPackage#getElementClass_PragmaArgumentAssociation()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='pragma_argument_association' namespace='##targetNamespace'"
	 * @generated
	 */
	PragmaArgumentAssociation getPragmaArgumentAssociation();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPragmaArgumentAssociation <em>Pragma Argument Association</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pragma Argument Association</em>' containment reference.
	 * @see #getPragmaArgumentAssociation()
	 * @generated
	 */
	void setPragmaArgumentAssociation(PragmaArgumentAssociation value);

	/**
	 * Returns the value of the '<em><b>Discriminant Association</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discriminant Association</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discriminant Association</em>' containment reference.
	 * @see #setDiscriminantAssociation(DiscriminantAssociation)
	 * @see Ada.AdaPackage#getElementClass_DiscriminantAssociation()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='discriminant_association' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscriminantAssociation getDiscriminantAssociation();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDiscriminantAssociation <em>Discriminant Association</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discriminant Association</em>' containment reference.
	 * @see #getDiscriminantAssociation()
	 * @generated
	 */
	void setDiscriminantAssociation(DiscriminantAssociation value);

	/**
	 * Returns the value of the '<em><b>Record Component Association</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Component Association</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Component Association</em>' containment reference.
	 * @see #setRecordComponentAssociation(RecordComponentAssociation)
	 * @see Ada.AdaPackage#getElementClass_RecordComponentAssociation()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='record_component_association' namespace='##targetNamespace'"
	 * @generated
	 */
	RecordComponentAssociation getRecordComponentAssociation();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getRecordComponentAssociation <em>Record Component Association</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Record Component Association</em>' containment reference.
	 * @see #getRecordComponentAssociation()
	 * @generated
	 */
	void setRecordComponentAssociation(RecordComponentAssociation value);

	/**
	 * Returns the value of the '<em><b>Array Component Association</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Array Component Association</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Array Component Association</em>' containment reference.
	 * @see #setArrayComponentAssociation(ArrayComponentAssociation)
	 * @see Ada.AdaPackage#getElementClass_ArrayComponentAssociation()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='array_component_association' namespace='##targetNamespace'"
	 * @generated
	 */
	ArrayComponentAssociation getArrayComponentAssociation();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getArrayComponentAssociation <em>Array Component Association</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Array Component Association</em>' containment reference.
	 * @see #getArrayComponentAssociation()
	 * @generated
	 */
	void setArrayComponentAssociation(ArrayComponentAssociation value);

	/**
	 * Returns the value of the '<em><b>Parameter Association</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Association</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Association</em>' containment reference.
	 * @see #setParameterAssociation(ParameterAssociation)
	 * @see Ada.AdaPackage#getElementClass_ParameterAssociation()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='parameter_association' namespace='##targetNamespace'"
	 * @generated
	 */
	ParameterAssociation getParameterAssociation();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getParameterAssociation <em>Parameter Association</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Association</em>' containment reference.
	 * @see #getParameterAssociation()
	 * @generated
	 */
	void setParameterAssociation(ParameterAssociation value);

	/**
	 * Returns the value of the '<em><b>Generic Association</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Association</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Association</em>' containment reference.
	 * @see #setGenericAssociation(GenericAssociation)
	 * @see Ada.AdaPackage#getElementClass_GenericAssociation()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='generic_association' namespace='##targetNamespace'"
	 * @generated
	 */
	GenericAssociation getGenericAssociation();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getGenericAssociation <em>Generic Association</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generic Association</em>' containment reference.
	 * @see #getGenericAssociation()
	 * @generated
	 */
	void setGenericAssociation(GenericAssociation value);

	/**
	 * Returns the value of the '<em><b>Null Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Null Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Null Statement</em>' containment reference.
	 * @see #setNullStatement(NullStatement)
	 * @see Ada.AdaPackage#getElementClass_NullStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='null_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	NullStatement getNullStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getNullStatement <em>Null Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Null Statement</em>' containment reference.
	 * @see #getNullStatement()
	 * @generated
	 */
	void setNullStatement(NullStatement value);

	/**
	 * Returns the value of the '<em><b>Assignment Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assignment Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assignment Statement</em>' containment reference.
	 * @see #setAssignmentStatement(AssignmentStatement)
	 * @see Ada.AdaPackage#getElementClass_AssignmentStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='assignment_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	AssignmentStatement getAssignmentStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAssignmentStatement <em>Assignment Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assignment Statement</em>' containment reference.
	 * @see #getAssignmentStatement()
	 * @generated
	 */
	void setAssignmentStatement(AssignmentStatement value);

	/**
	 * Returns the value of the '<em><b>If Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>If Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>If Statement</em>' containment reference.
	 * @see #setIfStatement(IfStatement)
	 * @see Ada.AdaPackage#getElementClass_IfStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='if_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	IfStatement getIfStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getIfStatement <em>If Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>If Statement</em>' containment reference.
	 * @see #getIfStatement()
	 * @generated
	 */
	void setIfStatement(IfStatement value);

	/**
	 * Returns the value of the '<em><b>Case Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Case Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Case Statement</em>' containment reference.
	 * @see #setCaseStatement(CaseStatement)
	 * @see Ada.AdaPackage#getElementClass_CaseStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='case_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	CaseStatement getCaseStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getCaseStatement <em>Case Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Case Statement</em>' containment reference.
	 * @see #getCaseStatement()
	 * @generated
	 */
	void setCaseStatement(CaseStatement value);

	/**
	 * Returns the value of the '<em><b>Loop Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Loop Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Loop Statement</em>' containment reference.
	 * @see #setLoopStatement(LoopStatement)
	 * @see Ada.AdaPackage#getElementClass_LoopStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='loop_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	LoopStatement getLoopStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getLoopStatement <em>Loop Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Loop Statement</em>' containment reference.
	 * @see #getLoopStatement()
	 * @generated
	 */
	void setLoopStatement(LoopStatement value);

	/**
	 * Returns the value of the '<em><b>While Loop Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>While Loop Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>While Loop Statement</em>' containment reference.
	 * @see #setWhileLoopStatement(WhileLoopStatement)
	 * @see Ada.AdaPackage#getElementClass_WhileLoopStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='while_loop_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	WhileLoopStatement getWhileLoopStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getWhileLoopStatement <em>While Loop Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>While Loop Statement</em>' containment reference.
	 * @see #getWhileLoopStatement()
	 * @generated
	 */
	void setWhileLoopStatement(WhileLoopStatement value);

	/**
	 * Returns the value of the '<em><b>For Loop Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>For Loop Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>For Loop Statement</em>' containment reference.
	 * @see #setForLoopStatement(ForLoopStatement)
	 * @see Ada.AdaPackage#getElementClass_ForLoopStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='for_loop_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	ForLoopStatement getForLoopStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getForLoopStatement <em>For Loop Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>For Loop Statement</em>' containment reference.
	 * @see #getForLoopStatement()
	 * @generated
	 */
	void setForLoopStatement(ForLoopStatement value);

	/**
	 * Returns the value of the '<em><b>Block Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Block Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Block Statement</em>' containment reference.
	 * @see #setBlockStatement(BlockStatement)
	 * @see Ada.AdaPackage#getElementClass_BlockStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='block_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	BlockStatement getBlockStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getBlockStatement <em>Block Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Block Statement</em>' containment reference.
	 * @see #getBlockStatement()
	 * @generated
	 */
	void setBlockStatement(BlockStatement value);

	/**
	 * Returns the value of the '<em><b>Exit Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exit Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exit Statement</em>' containment reference.
	 * @see #setExitStatement(ExitStatement)
	 * @see Ada.AdaPackage#getElementClass_ExitStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='exit_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	ExitStatement getExitStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getExitStatement <em>Exit Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exit Statement</em>' containment reference.
	 * @see #getExitStatement()
	 * @generated
	 */
	void setExitStatement(ExitStatement value);

	/**
	 * Returns the value of the '<em><b>Goto Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Goto Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Goto Statement</em>' containment reference.
	 * @see #setGotoStatement(GotoStatement)
	 * @see Ada.AdaPackage#getElementClass_GotoStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='goto_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	GotoStatement getGotoStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getGotoStatement <em>Goto Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Goto Statement</em>' containment reference.
	 * @see #getGotoStatement()
	 * @generated
	 */
	void setGotoStatement(GotoStatement value);

	/**
	 * Returns the value of the '<em><b>Procedure Call Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure Call Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure Call Statement</em>' containment reference.
	 * @see #setProcedureCallStatement(ProcedureCallStatement)
	 * @see Ada.AdaPackage#getElementClass_ProcedureCallStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='procedure_call_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	ProcedureCallStatement getProcedureCallStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getProcedureCallStatement <em>Procedure Call Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Procedure Call Statement</em>' containment reference.
	 * @see #getProcedureCallStatement()
	 * @generated
	 */
	void setProcedureCallStatement(ProcedureCallStatement value);

	/**
	 * Returns the value of the '<em><b>Return Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Return Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Return Statement</em>' containment reference.
	 * @see #setReturnStatement(ReturnStatement)
	 * @see Ada.AdaPackage#getElementClass_ReturnStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='return_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	ReturnStatement getReturnStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getReturnStatement <em>Return Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Return Statement</em>' containment reference.
	 * @see #getReturnStatement()
	 * @generated
	 */
	void setReturnStatement(ReturnStatement value);

	/**
	 * Returns the value of the '<em><b>Extended Return Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extended Return Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extended Return Statement</em>' containment reference.
	 * @see #setExtendedReturnStatement(ExtendedReturnStatement)
	 * @see Ada.AdaPackage#getElementClass_ExtendedReturnStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='extended_return_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	ExtendedReturnStatement getExtendedReturnStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getExtendedReturnStatement <em>Extended Return Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extended Return Statement</em>' containment reference.
	 * @see #getExtendedReturnStatement()
	 * @generated
	 */
	void setExtendedReturnStatement(ExtendedReturnStatement value);

	/**
	 * Returns the value of the '<em><b>Accept Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Accept Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Accept Statement</em>' containment reference.
	 * @see #setAcceptStatement(AcceptStatement)
	 * @see Ada.AdaPackage#getElementClass_AcceptStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='accept_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	AcceptStatement getAcceptStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAcceptStatement <em>Accept Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Accept Statement</em>' containment reference.
	 * @see #getAcceptStatement()
	 * @generated
	 */
	void setAcceptStatement(AcceptStatement value);

	/**
	 * Returns the value of the '<em><b>Entry Call Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry Call Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry Call Statement</em>' containment reference.
	 * @see #setEntryCallStatement(EntryCallStatement)
	 * @see Ada.AdaPackage#getElementClass_EntryCallStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='entry_call_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	EntryCallStatement getEntryCallStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getEntryCallStatement <em>Entry Call Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entry Call Statement</em>' containment reference.
	 * @see #getEntryCallStatement()
	 * @generated
	 */
	void setEntryCallStatement(EntryCallStatement value);

	/**
	 * Returns the value of the '<em><b>Requeue Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requeue Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requeue Statement</em>' containment reference.
	 * @see #setRequeueStatement(RequeueStatement)
	 * @see Ada.AdaPackage#getElementClass_RequeueStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='requeue_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	RequeueStatement getRequeueStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getRequeueStatement <em>Requeue Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Requeue Statement</em>' containment reference.
	 * @see #getRequeueStatement()
	 * @generated
	 */
	void setRequeueStatement(RequeueStatement value);

	/**
	 * Returns the value of the '<em><b>Requeue Statement With Abort</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requeue Statement With Abort</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requeue Statement With Abort</em>' containment reference.
	 * @see #setRequeueStatementWithAbort(RequeueStatementWithAbort)
	 * @see Ada.AdaPackage#getElementClass_RequeueStatementWithAbort()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='requeue_statement_with_abort' namespace='##targetNamespace'"
	 * @generated
	 */
	RequeueStatementWithAbort getRequeueStatementWithAbort();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getRequeueStatementWithAbort <em>Requeue Statement With Abort</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Requeue Statement With Abort</em>' containment reference.
	 * @see #getRequeueStatementWithAbort()
	 * @generated
	 */
	void setRequeueStatementWithAbort(RequeueStatementWithAbort value);

	/**
	 * Returns the value of the '<em><b>Delay Until Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delay Until Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delay Until Statement</em>' containment reference.
	 * @see #setDelayUntilStatement(DelayUntilStatement)
	 * @see Ada.AdaPackage#getElementClass_DelayUntilStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='delay_until_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	DelayUntilStatement getDelayUntilStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDelayUntilStatement <em>Delay Until Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delay Until Statement</em>' containment reference.
	 * @see #getDelayUntilStatement()
	 * @generated
	 */
	void setDelayUntilStatement(DelayUntilStatement value);

	/**
	 * Returns the value of the '<em><b>Delay Relative Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delay Relative Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delay Relative Statement</em>' containment reference.
	 * @see #setDelayRelativeStatement(DelayRelativeStatement)
	 * @see Ada.AdaPackage#getElementClass_DelayRelativeStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='delay_relative_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	DelayRelativeStatement getDelayRelativeStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDelayRelativeStatement <em>Delay Relative Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delay Relative Statement</em>' containment reference.
	 * @see #getDelayRelativeStatement()
	 * @generated
	 */
	void setDelayRelativeStatement(DelayRelativeStatement value);

	/**
	 * Returns the value of the '<em><b>Terminate Alternative Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Terminate Alternative Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Terminate Alternative Statement</em>' containment reference.
	 * @see #setTerminateAlternativeStatement(TerminateAlternativeStatement)
	 * @see Ada.AdaPackage#getElementClass_TerminateAlternativeStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='terminate_alternative_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	TerminateAlternativeStatement getTerminateAlternativeStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getTerminateAlternativeStatement <em>Terminate Alternative Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Terminate Alternative Statement</em>' containment reference.
	 * @see #getTerminateAlternativeStatement()
	 * @generated
	 */
	void setTerminateAlternativeStatement(TerminateAlternativeStatement value);

	/**
	 * Returns the value of the '<em><b>Selective Accept Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selective Accept Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selective Accept Statement</em>' containment reference.
	 * @see #setSelectiveAcceptStatement(SelectiveAcceptStatement)
	 * @see Ada.AdaPackage#getElementClass_SelectiveAcceptStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='selective_accept_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	SelectiveAcceptStatement getSelectiveAcceptStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getSelectiveAcceptStatement <em>Selective Accept Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selective Accept Statement</em>' containment reference.
	 * @see #getSelectiveAcceptStatement()
	 * @generated
	 */
	void setSelectiveAcceptStatement(SelectiveAcceptStatement value);

	/**
	 * Returns the value of the '<em><b>Timed Entry Call Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timed Entry Call Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timed Entry Call Statement</em>' containment reference.
	 * @see #setTimedEntryCallStatement(TimedEntryCallStatement)
	 * @see Ada.AdaPackage#getElementClass_TimedEntryCallStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='timed_entry_call_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	TimedEntryCallStatement getTimedEntryCallStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getTimedEntryCallStatement <em>Timed Entry Call Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timed Entry Call Statement</em>' containment reference.
	 * @see #getTimedEntryCallStatement()
	 * @generated
	 */
	void setTimedEntryCallStatement(TimedEntryCallStatement value);

	/**
	 * Returns the value of the '<em><b>Conditional Entry Call Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conditional Entry Call Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conditional Entry Call Statement</em>' containment reference.
	 * @see #setConditionalEntryCallStatement(ConditionalEntryCallStatement)
	 * @see Ada.AdaPackage#getElementClass_ConditionalEntryCallStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='conditional_entry_call_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	ConditionalEntryCallStatement getConditionalEntryCallStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getConditionalEntryCallStatement <em>Conditional Entry Call Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Conditional Entry Call Statement</em>' containment reference.
	 * @see #getConditionalEntryCallStatement()
	 * @generated
	 */
	void setConditionalEntryCallStatement(ConditionalEntryCallStatement value);

	/**
	 * Returns the value of the '<em><b>Asynchronous Select Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asynchronous Select Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asynchronous Select Statement</em>' containment reference.
	 * @see #setAsynchronousSelectStatement(AsynchronousSelectStatement)
	 * @see Ada.AdaPackage#getElementClass_AsynchronousSelectStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='asynchronous_select_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	AsynchronousSelectStatement getAsynchronousSelectStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAsynchronousSelectStatement <em>Asynchronous Select Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Asynchronous Select Statement</em>' containment reference.
	 * @see #getAsynchronousSelectStatement()
	 * @generated
	 */
	void setAsynchronousSelectStatement(AsynchronousSelectStatement value);

	/**
	 * Returns the value of the '<em><b>Abort Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abort Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abort Statement</em>' containment reference.
	 * @see #setAbortStatement(AbortStatement)
	 * @see Ada.AdaPackage#getElementClass_AbortStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='abort_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	AbortStatement getAbortStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAbortStatement <em>Abort Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abort Statement</em>' containment reference.
	 * @see #getAbortStatement()
	 * @generated
	 */
	void setAbortStatement(AbortStatement value);

	/**
	 * Returns the value of the '<em><b>Raise Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Raise Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Raise Statement</em>' containment reference.
	 * @see #setRaiseStatement(RaiseStatement)
	 * @see Ada.AdaPackage#getElementClass_RaiseStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='raise_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	RaiseStatement getRaiseStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getRaiseStatement <em>Raise Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Raise Statement</em>' containment reference.
	 * @see #getRaiseStatement()
	 * @generated
	 */
	void setRaiseStatement(RaiseStatement value);

	/**
	 * Returns the value of the '<em><b>Code Statement</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Code Statement</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code Statement</em>' containment reference.
	 * @see #setCodeStatement(CodeStatement)
	 * @see Ada.AdaPackage#getElementClass_CodeStatement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='code_statement' namespace='##targetNamespace'"
	 * @generated
	 */
	CodeStatement getCodeStatement();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getCodeStatement <em>Code Statement</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code Statement</em>' containment reference.
	 * @see #getCodeStatement()
	 * @generated
	 */
	void setCodeStatement(CodeStatement value);

	/**
	 * Returns the value of the '<em><b>If Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>If Path</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>If Path</em>' containment reference.
	 * @see #setIfPath(IfPath)
	 * @see Ada.AdaPackage#getElementClass_IfPath()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='if_path' namespace='##targetNamespace'"
	 * @generated
	 */
	IfPath getIfPath();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getIfPath <em>If Path</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>If Path</em>' containment reference.
	 * @see #getIfPath()
	 * @generated
	 */
	void setIfPath(IfPath value);

	/**
	 * Returns the value of the '<em><b>Elsif Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elsif Path</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elsif Path</em>' containment reference.
	 * @see #setElsifPath(ElsifPath)
	 * @see Ada.AdaPackage#getElementClass_ElsifPath()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='elsif_path' namespace='##targetNamespace'"
	 * @generated
	 */
	ElsifPath getElsifPath();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getElsifPath <em>Elsif Path</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Elsif Path</em>' containment reference.
	 * @see #getElsifPath()
	 * @generated
	 */
	void setElsifPath(ElsifPath value);

	/**
	 * Returns the value of the '<em><b>Else Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Else Path</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Else Path</em>' containment reference.
	 * @see #setElsePath(ElsePath)
	 * @see Ada.AdaPackage#getElementClass_ElsePath()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='else_path' namespace='##targetNamespace'"
	 * @generated
	 */
	ElsePath getElsePath();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getElsePath <em>Else Path</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Else Path</em>' containment reference.
	 * @see #getElsePath()
	 * @generated
	 */
	void setElsePath(ElsePath value);

	/**
	 * Returns the value of the '<em><b>Case Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Case Path</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Case Path</em>' containment reference.
	 * @see #setCasePath(CasePath)
	 * @see Ada.AdaPackage#getElementClass_CasePath()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='case_path' namespace='##targetNamespace'"
	 * @generated
	 */
	CasePath getCasePath();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getCasePath <em>Case Path</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Case Path</em>' containment reference.
	 * @see #getCasePath()
	 * @generated
	 */
	void setCasePath(CasePath value);

	/**
	 * Returns the value of the '<em><b>Select Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Select Path</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Select Path</em>' containment reference.
	 * @see #setSelectPath(SelectPath)
	 * @see Ada.AdaPackage#getElementClass_SelectPath()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='select_path' namespace='##targetNamespace'"
	 * @generated
	 */
	SelectPath getSelectPath();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getSelectPath <em>Select Path</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Select Path</em>' containment reference.
	 * @see #getSelectPath()
	 * @generated
	 */
	void setSelectPath(SelectPath value);

	/**
	 * Returns the value of the '<em><b>Or Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Or Path</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Or Path</em>' containment reference.
	 * @see #setOrPath(OrPath)
	 * @see Ada.AdaPackage#getElementClass_OrPath()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='or_path' namespace='##targetNamespace'"
	 * @generated
	 */
	OrPath getOrPath();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getOrPath <em>Or Path</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Or Path</em>' containment reference.
	 * @see #getOrPath()
	 * @generated
	 */
	void setOrPath(OrPath value);

	/**
	 * Returns the value of the '<em><b>Then Abort Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Then Abort Path</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Then Abort Path</em>' containment reference.
	 * @see #setThenAbortPath(ThenAbortPath)
	 * @see Ada.AdaPackage#getElementClass_ThenAbortPath()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='then_abort_path' namespace='##targetNamespace'"
	 * @generated
	 */
	ThenAbortPath getThenAbortPath();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getThenAbortPath <em>Then Abort Path</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Then Abort Path</em>' containment reference.
	 * @see #getThenAbortPath()
	 * @generated
	 */
	void setThenAbortPath(ThenAbortPath value);

	/**
	 * Returns the value of the '<em><b>Case Expression Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Case Expression Path</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Case Expression Path</em>' containment reference.
	 * @see #setCaseExpressionPath(CaseExpressionPath)
	 * @see Ada.AdaPackage#getElementClass_CaseExpressionPath()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='case_expression_path' namespace='##targetNamespace'"
	 * @generated
	 */
	CaseExpressionPath getCaseExpressionPath();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getCaseExpressionPath <em>Case Expression Path</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Case Expression Path</em>' containment reference.
	 * @see #getCaseExpressionPath()
	 * @generated
	 */
	void setCaseExpressionPath(CaseExpressionPath value);

	/**
	 * Returns the value of the '<em><b>If Expression Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>If Expression Path</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>If Expression Path</em>' containment reference.
	 * @see #setIfExpressionPath(IfExpressionPath)
	 * @see Ada.AdaPackage#getElementClass_IfExpressionPath()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='if_expression_path' namespace='##targetNamespace'"
	 * @generated
	 */
	IfExpressionPath getIfExpressionPath();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getIfExpressionPath <em>If Expression Path</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>If Expression Path</em>' containment reference.
	 * @see #getIfExpressionPath()
	 * @generated
	 */
	void setIfExpressionPath(IfExpressionPath value);

	/**
	 * Returns the value of the '<em><b>Elsif Expression Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elsif Expression Path</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elsif Expression Path</em>' containment reference.
	 * @see #setElsifExpressionPath(ElsifExpressionPath)
	 * @see Ada.AdaPackage#getElementClass_ElsifExpressionPath()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='elsif_expression_path' namespace='##targetNamespace'"
	 * @generated
	 */
	ElsifExpressionPath getElsifExpressionPath();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getElsifExpressionPath <em>Elsif Expression Path</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Elsif Expression Path</em>' containment reference.
	 * @see #getElsifExpressionPath()
	 * @generated
	 */
	void setElsifExpressionPath(ElsifExpressionPath value);

	/**
	 * Returns the value of the '<em><b>Else Expression Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Else Expression Path</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Else Expression Path</em>' containment reference.
	 * @see #setElseExpressionPath(ElseExpressionPath)
	 * @see Ada.AdaPackage#getElementClass_ElseExpressionPath()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='else_expression_path' namespace='##targetNamespace'"
	 * @generated
	 */
	ElseExpressionPath getElseExpressionPath();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getElseExpressionPath <em>Else Expression Path</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Else Expression Path</em>' containment reference.
	 * @see #getElseExpressionPath()
	 * @generated
	 */
	void setElseExpressionPath(ElseExpressionPath value);

	/**
	 * Returns the value of the '<em><b>Use Package Clause</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use Package Clause</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Package Clause</em>' containment reference.
	 * @see #setUsePackageClause(UsePackageClause)
	 * @see Ada.AdaPackage#getElementClass_UsePackageClause()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='use_package_clause' namespace='##targetNamespace'"
	 * @generated
	 */
	UsePackageClause getUsePackageClause();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getUsePackageClause <em>Use Package Clause</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use Package Clause</em>' containment reference.
	 * @see #getUsePackageClause()
	 * @generated
	 */
	void setUsePackageClause(UsePackageClause value);

	/**
	 * Returns the value of the '<em><b>Use Type Clause</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use Type Clause</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Type Clause</em>' containment reference.
	 * @see #setUseTypeClause(UseTypeClause)
	 * @see Ada.AdaPackage#getElementClass_UseTypeClause()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='use_type_clause' namespace='##targetNamespace'"
	 * @generated
	 */
	UseTypeClause getUseTypeClause();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getUseTypeClause <em>Use Type Clause</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use Type Clause</em>' containment reference.
	 * @see #getUseTypeClause()
	 * @generated
	 */
	void setUseTypeClause(UseTypeClause value);

	/**
	 * Returns the value of the '<em><b>Use All Type Clause</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use All Type Clause</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use All Type Clause</em>' containment reference.
	 * @see #setUseAllTypeClause(UseAllTypeClause)
	 * @see Ada.AdaPackage#getElementClass_UseAllTypeClause()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='use_all_type_clause' namespace='##targetNamespace'"
	 * @generated
	 */
	UseAllTypeClause getUseAllTypeClause();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getUseAllTypeClause <em>Use All Type Clause</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use All Type Clause</em>' containment reference.
	 * @see #getUseAllTypeClause()
	 * @generated
	 */
	void setUseAllTypeClause(UseAllTypeClause value);

	/**
	 * Returns the value of the '<em><b>With Clause</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>With Clause</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>With Clause</em>' containment reference.
	 * @see #setWithClause(WithClause)
	 * @see Ada.AdaPackage#getElementClass_WithClause()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='with_clause' namespace='##targetNamespace'"
	 * @generated
	 */
	WithClause getWithClause();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getWithClause <em>With Clause</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>With Clause</em>' containment reference.
	 * @see #getWithClause()
	 * @generated
	 */
	void setWithClause(WithClause value);

	/**
	 * Returns the value of the '<em><b>Attribute Definition Clause</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute Definition Clause</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Definition Clause</em>' containment reference.
	 * @see #setAttributeDefinitionClause(AttributeDefinitionClause)
	 * @see Ada.AdaPackage#getElementClass_AttributeDefinitionClause()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='attribute_definition_clause' namespace='##targetNamespace'"
	 * @generated
	 */
	AttributeDefinitionClause getAttributeDefinitionClause();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAttributeDefinitionClause <em>Attribute Definition Clause</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Definition Clause</em>' containment reference.
	 * @see #getAttributeDefinitionClause()
	 * @generated
	 */
	void setAttributeDefinitionClause(AttributeDefinitionClause value);

	/**
	 * Returns the value of the '<em><b>Enumeration Representation Clause</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumeration Representation Clause</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumeration Representation Clause</em>' containment reference.
	 * @see #setEnumerationRepresentationClause(EnumerationRepresentationClause)
	 * @see Ada.AdaPackage#getElementClass_EnumerationRepresentationClause()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='enumeration_representation_clause' namespace='##targetNamespace'"
	 * @generated
	 */
	EnumerationRepresentationClause getEnumerationRepresentationClause();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getEnumerationRepresentationClause <em>Enumeration Representation Clause</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enumeration Representation Clause</em>' containment reference.
	 * @see #getEnumerationRepresentationClause()
	 * @generated
	 */
	void setEnumerationRepresentationClause(EnumerationRepresentationClause value);

	/**
	 * Returns the value of the '<em><b>Record Representation Clause</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Representation Clause</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Representation Clause</em>' containment reference.
	 * @see #setRecordRepresentationClause(RecordRepresentationClause)
	 * @see Ada.AdaPackage#getElementClass_RecordRepresentationClause()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='record_representation_clause' namespace='##targetNamespace'"
	 * @generated
	 */
	RecordRepresentationClause getRecordRepresentationClause();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getRecordRepresentationClause <em>Record Representation Clause</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Record Representation Clause</em>' containment reference.
	 * @see #getRecordRepresentationClause()
	 * @generated
	 */
	void setRecordRepresentationClause(RecordRepresentationClause value);

	/**
	 * Returns the value of the '<em><b>At Clause</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>At Clause</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>At Clause</em>' containment reference.
	 * @see #setAtClause(AtClause)
	 * @see Ada.AdaPackage#getElementClass_AtClause()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='at_clause' namespace='##targetNamespace'"
	 * @generated
	 */
	AtClause getAtClause();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAtClause <em>At Clause</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>At Clause</em>' containment reference.
	 * @see #getAtClause()
	 * @generated
	 */
	void setAtClause(AtClause value);

	/**
	 * Returns the value of the '<em><b>Component Clause</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Clause</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Clause</em>' containment reference.
	 * @see #setComponentClause(ComponentClause)
	 * @see Ada.AdaPackage#getElementClass_ComponentClause()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='component_clause' namespace='##targetNamespace'"
	 * @generated
	 */
	ComponentClause getComponentClause();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getComponentClause <em>Component Clause</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component Clause</em>' containment reference.
	 * @see #getComponentClause()
	 * @generated
	 */
	void setComponentClause(ComponentClause value);

	/**
	 * Returns the value of the '<em><b>Exception Handler</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exception Handler</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exception Handler</em>' containment reference.
	 * @see #setExceptionHandler(ExceptionHandler)
	 * @see Ada.AdaPackage#getElementClass_ExceptionHandler()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='exception_handler' namespace='##targetNamespace'"
	 * @generated
	 */
	ExceptionHandler getExceptionHandler();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getExceptionHandler <em>Exception Handler</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exception Handler</em>' containment reference.
	 * @see #getExceptionHandler()
	 * @generated
	 */
	void setExceptionHandler(ExceptionHandler value);

	/**
	 * Returns the value of the '<em><b>Comment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comment</em>' containment reference.
	 * @see #setComment(Comment)
	 * @see Ada.AdaPackage#getElementClass_Comment()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='comment' namespace='##targetNamespace'"
	 * @generated
	 */
	Comment getComment();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getComment <em>Comment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comment</em>' containment reference.
	 * @see #getComment()
	 * @generated
	 */
	void setComment(Comment value);

	/**
	 * Returns the value of the '<em><b>All Calls Remote Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Calls Remote Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Calls Remote Pragma</em>' containment reference.
	 * @see #setAllCallsRemotePragma(AllCallsRemotePragma)
	 * @see Ada.AdaPackage#getElementClass_AllCallsRemotePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='all_calls_remote_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AllCallsRemotePragma getAllCallsRemotePragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>All Calls Remote Pragma</em>' containment reference.
	 * @see #getAllCallsRemotePragma()
	 * @generated
	 */
	void setAllCallsRemotePragma(AllCallsRemotePragma value);

	/**
	 * Returns the value of the '<em><b>Asynchronous Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asynchronous Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asynchronous Pragma</em>' containment reference.
	 * @see #setAsynchronousPragma(AsynchronousPragma)
	 * @see Ada.AdaPackage#getElementClass_AsynchronousPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='asynchronous_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AsynchronousPragma getAsynchronousPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAsynchronousPragma <em>Asynchronous Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Asynchronous Pragma</em>' containment reference.
	 * @see #getAsynchronousPragma()
	 * @generated
	 */
	void setAsynchronousPragma(AsynchronousPragma value);

	/**
	 * Returns the value of the '<em><b>Atomic Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Pragma</em>' containment reference.
	 * @see #setAtomicPragma(AtomicPragma)
	 * @see Ada.AdaPackage#getElementClass_AtomicPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='atomic_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AtomicPragma getAtomicPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAtomicPragma <em>Atomic Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Atomic Pragma</em>' containment reference.
	 * @see #getAtomicPragma()
	 * @generated
	 */
	void setAtomicPragma(AtomicPragma value);

	/**
	 * Returns the value of the '<em><b>Atomic Components Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Components Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Components Pragma</em>' containment reference.
	 * @see #setAtomicComponentsPragma(AtomicComponentsPragma)
	 * @see Ada.AdaPackage#getElementClass_AtomicComponentsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='atomic_components_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AtomicComponentsPragma getAtomicComponentsPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Atomic Components Pragma</em>' containment reference.
	 * @see #getAtomicComponentsPragma()
	 * @generated
	 */
	void setAtomicComponentsPragma(AtomicComponentsPragma value);

	/**
	 * Returns the value of the '<em><b>Attach Handler Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attach Handler Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attach Handler Pragma</em>' containment reference.
	 * @see #setAttachHandlerPragma(AttachHandlerPragma)
	 * @see Ada.AdaPackage#getElementClass_AttachHandlerPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='attach_handler_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AttachHandlerPragma getAttachHandlerPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAttachHandlerPragma <em>Attach Handler Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attach Handler Pragma</em>' containment reference.
	 * @see #getAttachHandlerPragma()
	 * @generated
	 */
	void setAttachHandlerPragma(AttachHandlerPragma value);

	/**
	 * Returns the value of the '<em><b>Controlled Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controlled Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controlled Pragma</em>' containment reference.
	 * @see #setControlledPragma(ControlledPragma)
	 * @see Ada.AdaPackage#getElementClass_ControlledPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='controlled_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ControlledPragma getControlledPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getControlledPragma <em>Controlled Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Controlled Pragma</em>' containment reference.
	 * @see #getControlledPragma()
	 * @generated
	 */
	void setControlledPragma(ControlledPragma value);

	/**
	 * Returns the value of the '<em><b>Convention Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Convention Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Convention Pragma</em>' containment reference.
	 * @see #setConventionPragma(ConventionPragma)
	 * @see Ada.AdaPackage#getElementClass_ConventionPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='convention_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ConventionPragma getConventionPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getConventionPragma <em>Convention Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Convention Pragma</em>' containment reference.
	 * @see #getConventionPragma()
	 * @generated
	 */
	void setConventionPragma(ConventionPragma value);

	/**
	 * Returns the value of the '<em><b>Discard Names Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discard Names Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discard Names Pragma</em>' containment reference.
	 * @see #setDiscardNamesPragma(DiscardNamesPragma)
	 * @see Ada.AdaPackage#getElementClass_DiscardNamesPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='discard_names_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscardNamesPragma getDiscardNamesPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDiscardNamesPragma <em>Discard Names Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discard Names Pragma</em>' containment reference.
	 * @see #getDiscardNamesPragma()
	 * @generated
	 */
	void setDiscardNamesPragma(DiscardNamesPragma value);

	/**
	 * Returns the value of the '<em><b>Elaborate Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Pragma</em>' containment reference.
	 * @see #setElaboratePragma(ElaboratePragma)
	 * @see Ada.AdaPackage#getElementClass_ElaboratePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='elaborate_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ElaboratePragma getElaboratePragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getElaboratePragma <em>Elaborate Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Elaborate Pragma</em>' containment reference.
	 * @see #getElaboratePragma()
	 * @generated
	 */
	void setElaboratePragma(ElaboratePragma value);

	/**
	 * Returns the value of the '<em><b>Elaborate All Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate All Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate All Pragma</em>' containment reference.
	 * @see #setElaborateAllPragma(ElaborateAllPragma)
	 * @see Ada.AdaPackage#getElementClass_ElaborateAllPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='elaborate_all_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ElaborateAllPragma getElaborateAllPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getElaborateAllPragma <em>Elaborate All Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Elaborate All Pragma</em>' containment reference.
	 * @see #getElaborateAllPragma()
	 * @generated
	 */
	void setElaborateAllPragma(ElaborateAllPragma value);

	/**
	 * Returns the value of the '<em><b>Elaborate Body Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Body Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Body Pragma</em>' containment reference.
	 * @see #setElaborateBodyPragma(ElaborateBodyPragma)
	 * @see Ada.AdaPackage#getElementClass_ElaborateBodyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='elaborate_body_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ElaborateBodyPragma getElaborateBodyPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Elaborate Body Pragma</em>' containment reference.
	 * @see #getElaborateBodyPragma()
	 * @generated
	 */
	void setElaborateBodyPragma(ElaborateBodyPragma value);

	/**
	 * Returns the value of the '<em><b>Export Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Export Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Export Pragma</em>' containment reference.
	 * @see #setExportPragma(ExportPragma)
	 * @see Ada.AdaPackage#getElementClass_ExportPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='export_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ExportPragma getExportPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getExportPragma <em>Export Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Export Pragma</em>' containment reference.
	 * @see #getExportPragma()
	 * @generated
	 */
	void setExportPragma(ExportPragma value);

	/**
	 * Returns the value of the '<em><b>Import Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Import Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Import Pragma</em>' containment reference.
	 * @see #setImportPragma(ImportPragma)
	 * @see Ada.AdaPackage#getElementClass_ImportPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='import_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ImportPragma getImportPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getImportPragma <em>Import Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Import Pragma</em>' containment reference.
	 * @see #getImportPragma()
	 * @generated
	 */
	void setImportPragma(ImportPragma value);

	/**
	 * Returns the value of the '<em><b>Inline Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inline Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inline Pragma</em>' containment reference.
	 * @see #setInlinePragma(InlinePragma)
	 * @see Ada.AdaPackage#getElementClass_InlinePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='inline_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	InlinePragma getInlinePragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getInlinePragma <em>Inline Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inline Pragma</em>' containment reference.
	 * @see #getInlinePragma()
	 * @generated
	 */
	void setInlinePragma(InlinePragma value);

	/**
	 * Returns the value of the '<em><b>Inspection Point Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inspection Point Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inspection Point Pragma</em>' containment reference.
	 * @see #setInspectionPointPragma(InspectionPointPragma)
	 * @see Ada.AdaPackage#getElementClass_InspectionPointPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='inspection_point_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	InspectionPointPragma getInspectionPointPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getInspectionPointPragma <em>Inspection Point Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inspection Point Pragma</em>' containment reference.
	 * @see #getInspectionPointPragma()
	 * @generated
	 */
	void setInspectionPointPragma(InspectionPointPragma value);

	/**
	 * Returns the value of the '<em><b>Interrupt Handler Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Handler Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Handler Pragma</em>' containment reference.
	 * @see #setInterruptHandlerPragma(InterruptHandlerPragma)
	 * @see Ada.AdaPackage#getElementClass_InterruptHandlerPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='interrupt_handler_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	InterruptHandlerPragma getInterruptHandlerPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interrupt Handler Pragma</em>' containment reference.
	 * @see #getInterruptHandlerPragma()
	 * @generated
	 */
	void setInterruptHandlerPragma(InterruptHandlerPragma value);

	/**
	 * Returns the value of the '<em><b>Interrupt Priority Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Priority Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Priority Pragma</em>' containment reference.
	 * @see #setInterruptPriorityPragma(InterruptPriorityPragma)
	 * @see Ada.AdaPackage#getElementClass_InterruptPriorityPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='interrupt_priority_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	InterruptPriorityPragma getInterruptPriorityPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interrupt Priority Pragma</em>' containment reference.
	 * @see #getInterruptPriorityPragma()
	 * @generated
	 */
	void setInterruptPriorityPragma(InterruptPriorityPragma value);

	/**
	 * Returns the value of the '<em><b>Linker Options Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Linker Options Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Linker Options Pragma</em>' containment reference.
	 * @see #setLinkerOptionsPragma(LinkerOptionsPragma)
	 * @see Ada.AdaPackage#getElementClass_LinkerOptionsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='linker_options_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	LinkerOptionsPragma getLinkerOptionsPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getLinkerOptionsPragma <em>Linker Options Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Linker Options Pragma</em>' containment reference.
	 * @see #getLinkerOptionsPragma()
	 * @generated
	 */
	void setLinkerOptionsPragma(LinkerOptionsPragma value);

	/**
	 * Returns the value of the '<em><b>List Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Pragma</em>' containment reference.
	 * @see #setListPragma(ListPragma)
	 * @see Ada.AdaPackage#getElementClass_ListPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='list_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ListPragma getListPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getListPragma <em>List Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>List Pragma</em>' containment reference.
	 * @see #getListPragma()
	 * @generated
	 */
	void setListPragma(ListPragma value);

	/**
	 * Returns the value of the '<em><b>Locking Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Locking Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Locking Policy Pragma</em>' containment reference.
	 * @see #setLockingPolicyPragma(LockingPolicyPragma)
	 * @see Ada.AdaPackage#getElementClass_LockingPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='locking_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	LockingPolicyPragma getLockingPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getLockingPolicyPragma <em>Locking Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Locking Policy Pragma</em>' containment reference.
	 * @see #getLockingPolicyPragma()
	 * @generated
	 */
	void setLockingPolicyPragma(LockingPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Normalize Scalars Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Normalize Scalars Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Normalize Scalars Pragma</em>' containment reference.
	 * @see #setNormalizeScalarsPragma(NormalizeScalarsPragma)
	 * @see Ada.AdaPackage#getElementClass_NormalizeScalarsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='normalize_scalars_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	NormalizeScalarsPragma getNormalizeScalarsPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Normalize Scalars Pragma</em>' containment reference.
	 * @see #getNormalizeScalarsPragma()
	 * @generated
	 */
	void setNormalizeScalarsPragma(NormalizeScalarsPragma value);

	/**
	 * Returns the value of the '<em><b>Optimize Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Optimize Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Optimize Pragma</em>' containment reference.
	 * @see #setOptimizePragma(OptimizePragma)
	 * @see Ada.AdaPackage#getElementClass_OptimizePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='optimize_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	OptimizePragma getOptimizePragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getOptimizePragma <em>Optimize Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Optimize Pragma</em>' containment reference.
	 * @see #getOptimizePragma()
	 * @generated
	 */
	void setOptimizePragma(OptimizePragma value);

	/**
	 * Returns the value of the '<em><b>Pack Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pack Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pack Pragma</em>' containment reference.
	 * @see #setPackPragma(PackPragma)
	 * @see Ada.AdaPackage#getElementClass_PackPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='pack_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PackPragma getPackPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPackPragma <em>Pack Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pack Pragma</em>' containment reference.
	 * @see #getPackPragma()
	 * @generated
	 */
	void setPackPragma(PackPragma value);

	/**
	 * Returns the value of the '<em><b>Page Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Page Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Page Pragma</em>' containment reference.
	 * @see #setPagePragma(PagePragma)
	 * @see Ada.AdaPackage#getElementClass_PagePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='page_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PagePragma getPagePragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPagePragma <em>Page Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Page Pragma</em>' containment reference.
	 * @see #getPagePragma()
	 * @generated
	 */
	void setPagePragma(PagePragma value);

	/**
	 * Returns the value of the '<em><b>Preelaborate Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborate Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborate Pragma</em>' containment reference.
	 * @see #setPreelaboratePragma(PreelaboratePragma)
	 * @see Ada.AdaPackage#getElementClass_PreelaboratePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='preelaborate_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PreelaboratePragma getPreelaboratePragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPreelaboratePragma <em>Preelaborate Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preelaborate Pragma</em>' containment reference.
	 * @see #getPreelaboratePragma()
	 * @generated
	 */
	void setPreelaboratePragma(PreelaboratePragma value);

	/**
	 * Returns the value of the '<em><b>Priority Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Pragma</em>' containment reference.
	 * @see #setPriorityPragma(PriorityPragma)
	 * @see Ada.AdaPackage#getElementClass_PriorityPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='priority_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PriorityPragma getPriorityPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPriorityPragma <em>Priority Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Priority Pragma</em>' containment reference.
	 * @see #getPriorityPragma()
	 * @generated
	 */
	void setPriorityPragma(PriorityPragma value);

	/**
	 * Returns the value of the '<em><b>Pure Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pure Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pure Pragma</em>' containment reference.
	 * @see #setPurePragma(PurePragma)
	 * @see Ada.AdaPackage#getElementClass_PurePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='pure_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PurePragma getPurePragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPurePragma <em>Pure Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pure Pragma</em>' containment reference.
	 * @see #getPurePragma()
	 * @generated
	 */
	void setPurePragma(PurePragma value);

	/**
	 * Returns the value of the '<em><b>Queuing Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Queuing Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Queuing Policy Pragma</em>' containment reference.
	 * @see #setQueuingPolicyPragma(QueuingPolicyPragma)
	 * @see Ada.AdaPackage#getElementClass_QueuingPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='queuing_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	QueuingPolicyPragma getQueuingPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Queuing Policy Pragma</em>' containment reference.
	 * @see #getQueuingPolicyPragma()
	 * @generated
	 */
	void setQueuingPolicyPragma(QueuingPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Remote Call Interface Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Call Interface Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Call Interface Pragma</em>' containment reference.
	 * @see #setRemoteCallInterfacePragma(RemoteCallInterfacePragma)
	 * @see Ada.AdaPackage#getElementClass_RemoteCallInterfacePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='remote_call_interface_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	RemoteCallInterfacePragma getRemoteCallInterfacePragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Remote Call Interface Pragma</em>' containment reference.
	 * @see #getRemoteCallInterfacePragma()
	 * @generated
	 */
	void setRemoteCallInterfacePragma(RemoteCallInterfacePragma value);

	/**
	 * Returns the value of the '<em><b>Remote Types Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Types Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Types Pragma</em>' containment reference.
	 * @see #setRemoteTypesPragma(RemoteTypesPragma)
	 * @see Ada.AdaPackage#getElementClass_RemoteTypesPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='remote_types_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	RemoteTypesPragma getRemoteTypesPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getRemoteTypesPragma <em>Remote Types Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Remote Types Pragma</em>' containment reference.
	 * @see #getRemoteTypesPragma()
	 * @generated
	 */
	void setRemoteTypesPragma(RemoteTypesPragma value);

	/**
	 * Returns the value of the '<em><b>Restrictions Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Restrictions Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Restrictions Pragma</em>' containment reference.
	 * @see #setRestrictionsPragma(RestrictionsPragma)
	 * @see Ada.AdaPackage#getElementClass_RestrictionsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='restrictions_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	RestrictionsPragma getRestrictionsPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getRestrictionsPragma <em>Restrictions Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Restrictions Pragma</em>' containment reference.
	 * @see #getRestrictionsPragma()
	 * @generated
	 */
	void setRestrictionsPragma(RestrictionsPragma value);

	/**
	 * Returns the value of the '<em><b>Reviewable Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reviewable Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reviewable Pragma</em>' containment reference.
	 * @see #setReviewablePragma(ReviewablePragma)
	 * @see Ada.AdaPackage#getElementClass_ReviewablePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='reviewable_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ReviewablePragma getReviewablePragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getReviewablePragma <em>Reviewable Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reviewable Pragma</em>' containment reference.
	 * @see #getReviewablePragma()
	 * @generated
	 */
	void setReviewablePragma(ReviewablePragma value);

	/**
	 * Returns the value of the '<em><b>Shared Passive Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shared Passive Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shared Passive Pragma</em>' containment reference.
	 * @see #setSharedPassivePragma(SharedPassivePragma)
	 * @see Ada.AdaPackage#getElementClass_SharedPassivePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='shared_passive_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	SharedPassivePragma getSharedPassivePragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getSharedPassivePragma <em>Shared Passive Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Shared Passive Pragma</em>' containment reference.
	 * @see #getSharedPassivePragma()
	 * @generated
	 */
	void setSharedPassivePragma(SharedPassivePragma value);

	/**
	 * Returns the value of the '<em><b>Storage Size Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Size Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Size Pragma</em>' containment reference.
	 * @see #setStorageSizePragma(StorageSizePragma)
	 * @see Ada.AdaPackage#getElementClass_StorageSizePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='storage_size_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	StorageSizePragma getStorageSizePragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getStorageSizePragma <em>Storage Size Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Storage Size Pragma</em>' containment reference.
	 * @see #getStorageSizePragma()
	 * @generated
	 */
	void setStorageSizePragma(StorageSizePragma value);

	/**
	 * Returns the value of the '<em><b>Suppress Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Suppress Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Suppress Pragma</em>' containment reference.
	 * @see #setSuppressPragma(SuppressPragma)
	 * @see Ada.AdaPackage#getElementClass_SuppressPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='suppress_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	SuppressPragma getSuppressPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getSuppressPragma <em>Suppress Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Suppress Pragma</em>' containment reference.
	 * @see #getSuppressPragma()
	 * @generated
	 */
	void setSuppressPragma(SuppressPragma value);

	/**
	 * Returns the value of the '<em><b>Task Dispatching Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Dispatching Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Dispatching Policy Pragma</em>' containment reference.
	 * @see #setTaskDispatchingPolicyPragma(TaskDispatchingPolicyPragma)
	 * @see Ada.AdaPackage#getElementClass_TaskDispatchingPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='task_dispatching_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	TaskDispatchingPolicyPragma getTaskDispatchingPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Dispatching Policy Pragma</em>' containment reference.
	 * @see #getTaskDispatchingPolicyPragma()
	 * @generated
	 */
	void setTaskDispatchingPolicyPragma(TaskDispatchingPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Volatile Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Pragma</em>' containment reference.
	 * @see #setVolatilePragma(VolatilePragma)
	 * @see Ada.AdaPackage#getElementClass_VolatilePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='volatile_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	VolatilePragma getVolatilePragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getVolatilePragma <em>Volatile Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Volatile Pragma</em>' containment reference.
	 * @see #getVolatilePragma()
	 * @generated
	 */
	void setVolatilePragma(VolatilePragma value);

	/**
	 * Returns the value of the '<em><b>Volatile Components Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Components Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Components Pragma</em>' containment reference.
	 * @see #setVolatileComponentsPragma(VolatileComponentsPragma)
	 * @see Ada.AdaPackage#getElementClass_VolatileComponentsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='volatile_components_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	VolatileComponentsPragma getVolatileComponentsPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Volatile Components Pragma</em>' containment reference.
	 * @see #getVolatileComponentsPragma()
	 * @generated
	 */
	void setVolatileComponentsPragma(VolatileComponentsPragma value);

	/**
	 * Returns the value of the '<em><b>Assert Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assert Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assert Pragma</em>' containment reference.
	 * @see #setAssertPragma(AssertPragma)
	 * @see Ada.AdaPackage#getElementClass_AssertPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='assert_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AssertPragma getAssertPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAssertPragma <em>Assert Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assert Pragma</em>' containment reference.
	 * @see #getAssertPragma()
	 * @generated
	 */
	void setAssertPragma(AssertPragma value);

	/**
	 * Returns the value of the '<em><b>Assertion Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assertion Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assertion Policy Pragma</em>' containment reference.
	 * @see #setAssertionPolicyPragma(AssertionPolicyPragma)
	 * @see Ada.AdaPackage#getElementClass_AssertionPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='assertion_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AssertionPolicyPragma getAssertionPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assertion Policy Pragma</em>' containment reference.
	 * @see #getAssertionPolicyPragma()
	 * @generated
	 */
	void setAssertionPolicyPragma(AssertionPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Detect Blocking Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Detect Blocking Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Detect Blocking Pragma</em>' containment reference.
	 * @see #setDetectBlockingPragma(DetectBlockingPragma)
	 * @see Ada.AdaPackage#getElementClass_DetectBlockingPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='detect_blocking_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	DetectBlockingPragma getDetectBlockingPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Detect Blocking Pragma</em>' containment reference.
	 * @see #getDetectBlockingPragma()
	 * @generated
	 */
	void setDetectBlockingPragma(DetectBlockingPragma value);

	/**
	 * Returns the value of the '<em><b>No Return Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>No Return Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>No Return Pragma</em>' containment reference.
	 * @see #setNoReturnPragma(NoReturnPragma)
	 * @see Ada.AdaPackage#getElementClass_NoReturnPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='no_return_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	NoReturnPragma getNoReturnPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getNoReturnPragma <em>No Return Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>No Return Pragma</em>' containment reference.
	 * @see #getNoReturnPragma()
	 * @generated
	 */
	void setNoReturnPragma(NoReturnPragma value);

	/**
	 * Returns the value of the '<em><b>Partition Elaboration Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partition Elaboration Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partition Elaboration Policy Pragma</em>' containment reference.
	 * @see #setPartitionElaborationPolicyPragma(PartitionElaborationPolicyPragma)
	 * @see Ada.AdaPackage#getElementClass_PartitionElaborationPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='partition_elaboration_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PartitionElaborationPolicyPragma getPartitionElaborationPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Partition Elaboration Policy Pragma</em>' containment reference.
	 * @see #getPartitionElaborationPolicyPragma()
	 * @generated
	 */
	void setPartitionElaborationPolicyPragma(PartitionElaborationPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Preelaborable Initialization Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborable Initialization Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborable Initialization Pragma</em>' containment reference.
	 * @see #setPreelaborableInitializationPragma(PreelaborableInitializationPragma)
	 * @see Ada.AdaPackage#getElementClass_PreelaborableInitializationPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='preelaborable_initialization_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PreelaborableInitializationPragma getPreelaborableInitializationPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preelaborable Initialization Pragma</em>' containment reference.
	 * @see #getPreelaborableInitializationPragma()
	 * @generated
	 */
	void setPreelaborableInitializationPragma(PreelaborableInitializationPragma value);

	/**
	 * Returns the value of the '<em><b>Priority Specific Dispatching Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Specific Dispatching Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Specific Dispatching Pragma</em>' containment reference.
	 * @see #setPrioritySpecificDispatchingPragma(PrioritySpecificDispatchingPragma)
	 * @see Ada.AdaPackage#getElementClass_PrioritySpecificDispatchingPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='priority_specific_dispatching_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PrioritySpecificDispatchingPragma getPrioritySpecificDispatchingPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Priority Specific Dispatching Pragma</em>' containment reference.
	 * @see #getPrioritySpecificDispatchingPragma()
	 * @generated
	 */
	void setPrioritySpecificDispatchingPragma(PrioritySpecificDispatchingPragma value);

	/**
	 * Returns the value of the '<em><b>Profile Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Profile Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Profile Pragma</em>' containment reference.
	 * @see #setProfilePragma(ProfilePragma)
	 * @see Ada.AdaPackage#getElementClass_ProfilePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='profile_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ProfilePragma getProfilePragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getProfilePragma <em>Profile Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Profile Pragma</em>' containment reference.
	 * @see #getProfilePragma()
	 * @generated
	 */
	void setProfilePragma(ProfilePragma value);

	/**
	 * Returns the value of the '<em><b>Relative Deadline Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relative Deadline Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relative Deadline Pragma</em>' containment reference.
	 * @see #setRelativeDeadlinePragma(RelativeDeadlinePragma)
	 * @see Ada.AdaPackage#getElementClass_RelativeDeadlinePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='relative_deadline_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	RelativeDeadlinePragma getRelativeDeadlinePragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relative Deadline Pragma</em>' containment reference.
	 * @see #getRelativeDeadlinePragma()
	 * @generated
	 */
	void setRelativeDeadlinePragma(RelativeDeadlinePragma value);

	/**
	 * Returns the value of the '<em><b>Unchecked Union Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unchecked Union Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unchecked Union Pragma</em>' containment reference.
	 * @see #setUncheckedUnionPragma(UncheckedUnionPragma)
	 * @see Ada.AdaPackage#getElementClass_UncheckedUnionPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unchecked_union_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	UncheckedUnionPragma getUncheckedUnionPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unchecked Union Pragma</em>' containment reference.
	 * @see #getUncheckedUnionPragma()
	 * @generated
	 */
	void setUncheckedUnionPragma(UncheckedUnionPragma value);

	/**
	 * Returns the value of the '<em><b>Unsuppress Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unsuppress Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unsuppress Pragma</em>' containment reference.
	 * @see #setUnsuppressPragma(UnsuppressPragma)
	 * @see Ada.AdaPackage#getElementClass_UnsuppressPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unsuppress_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	UnsuppressPragma getUnsuppressPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getUnsuppressPragma <em>Unsuppress Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unsuppress Pragma</em>' containment reference.
	 * @see #getUnsuppressPragma()
	 * @generated
	 */
	void setUnsuppressPragma(UnsuppressPragma value);

	/**
	 * Returns the value of the '<em><b>Default Storage Pool Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Storage Pool Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Storage Pool Pragma</em>' containment reference.
	 * @see #setDefaultStoragePoolPragma(DefaultStoragePoolPragma)
	 * @see Ada.AdaPackage#getElementClass_DefaultStoragePoolPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='default_storage_pool_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	DefaultStoragePoolPragma getDefaultStoragePoolPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Storage Pool Pragma</em>' containment reference.
	 * @see #getDefaultStoragePoolPragma()
	 * @generated
	 */
	void setDefaultStoragePoolPragma(DefaultStoragePoolPragma value);

	/**
	 * Returns the value of the '<em><b>Dispatching Domain Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dispatching Domain Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dispatching Domain Pragma</em>' containment reference.
	 * @see #setDispatchingDomainPragma(DispatchingDomainPragma)
	 * @see Ada.AdaPackage#getElementClass_DispatchingDomainPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='dispatching_domain_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	DispatchingDomainPragma getDispatchingDomainPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dispatching Domain Pragma</em>' containment reference.
	 * @see #getDispatchingDomainPragma()
	 * @generated
	 */
	void setDispatchingDomainPragma(DispatchingDomainPragma value);

	/**
	 * Returns the value of the '<em><b>Cpu Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cpu Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cpu Pragma</em>' containment reference.
	 * @see #setCpuPragma(CpuPragma)
	 * @see Ada.AdaPackage#getElementClass_CpuPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='cpu_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	CpuPragma getCpuPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getCpuPragma <em>Cpu Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cpu Pragma</em>' containment reference.
	 * @see #getCpuPragma()
	 * @generated
	 */
	void setCpuPragma(CpuPragma value);

	/**
	 * Returns the value of the '<em><b>Independent Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Pragma</em>' containment reference.
	 * @see #setIndependentPragma(IndependentPragma)
	 * @see Ada.AdaPackage#getElementClass_IndependentPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='independent_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	IndependentPragma getIndependentPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getIndependentPragma <em>Independent Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Independent Pragma</em>' containment reference.
	 * @see #getIndependentPragma()
	 * @generated
	 */
	void setIndependentPragma(IndependentPragma value);

	/**
	 * Returns the value of the '<em><b>Independent Components Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Components Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Components Pragma</em>' containment reference.
	 * @see #setIndependentComponentsPragma(IndependentComponentsPragma)
	 * @see Ada.AdaPackage#getElementClass_IndependentComponentsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='independent_components_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	IndependentComponentsPragma getIndependentComponentsPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getIndependentComponentsPragma <em>Independent Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Independent Components Pragma</em>' containment reference.
	 * @see #getIndependentComponentsPragma()
	 * @generated
	 */
	void setIndependentComponentsPragma(IndependentComponentsPragma value);

	/**
	 * Returns the value of the '<em><b>Implementation Defined Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implementation Defined Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implementation Defined Pragma</em>' containment reference.
	 * @see #setImplementationDefinedPragma(ImplementationDefinedPragma)
	 * @see Ada.AdaPackage#getElementClass_ImplementationDefinedPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='implementation_defined_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ImplementationDefinedPragma getImplementationDefinedPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Implementation Defined Pragma</em>' containment reference.
	 * @see #getImplementationDefinedPragma()
	 * @generated
	 */
	void setImplementationDefinedPragma(ImplementationDefinedPragma value);

	/**
	 * Returns the value of the '<em><b>Unknown Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unknown Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unknown Pragma</em>' containment reference.
	 * @see #setUnknownPragma(UnknownPragma)
	 * @see Ada.AdaPackage#getElementClass_UnknownPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unknown_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	UnknownPragma getUnknownPragma();

	/**
	 * Sets the value of the '{@link Ada.ElementClass#getUnknownPragma <em>Unknown Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unknown Pragma</em>' containment reference.
	 * @see #getUnknownPragma()
	 * @generated
	 */
	void setUnknownPragma(UnknownPragma value);

} // ElementClass
