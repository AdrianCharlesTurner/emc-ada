/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.AllCallsRemotePragma;
import Ada.AssertPragma;
import Ada.AssertionPolicyPragma;
import Ada.AsynchronousPragma;
import Ada.AtomicComponentsPragma;
import Ada.AtomicPragma;
import Ada.AttachHandlerPragma;
import Ada.ChoiceParameterSpecification;
import Ada.Comment;
import Ada.ComponentDeclaration;
import Ada.ConstantDeclaration;
import Ada.ControlledPragma;
import Ada.ConventionPragma;
import Ada.CpuPragma;
import Ada.DeclarationList;
import Ada.DefaultStoragePoolPragma;
import Ada.DeferredConstantDeclaration;
import Ada.DetectBlockingPragma;
import Ada.DiscardNamesPragma;
import Ada.DiscriminantSpecification;
import Ada.DispatchingDomainPragma;
import Ada.ElaborateAllPragma;
import Ada.ElaborateBodyPragma;
import Ada.ElaboratePragma;
import Ada.ElementIteratorSpecification;
import Ada.EntryBodyDeclaration;
import Ada.EntryDeclaration;
import Ada.EntryIndexSpecification;
import Ada.EnumerationLiteralSpecification;
import Ada.ExceptionDeclaration;
import Ada.ExceptionRenamingDeclaration;
import Ada.ExportPragma;
import Ada.ExpressionFunctionDeclaration;
import Ada.FormalFunctionDeclaration;
import Ada.FormalIncompleteTypeDeclaration;
import Ada.FormalObjectDeclaration;
import Ada.FormalPackageDeclaration;
import Ada.FormalPackageDeclarationWithBox;
import Ada.FormalProcedureDeclaration;
import Ada.FormalTypeDeclaration;
import Ada.FunctionBodyDeclaration;
import Ada.FunctionBodyStub;
import Ada.FunctionDeclaration;
import Ada.FunctionInstantiation;
import Ada.FunctionRenamingDeclaration;
import Ada.GeneralizedIteratorSpecification;
import Ada.GenericFunctionDeclaration;
import Ada.GenericFunctionRenamingDeclaration;
import Ada.GenericPackageDeclaration;
import Ada.GenericPackageRenamingDeclaration;
import Ada.GenericProcedureDeclaration;
import Ada.GenericProcedureRenamingDeclaration;
import Ada.ImplementationDefinedPragma;
import Ada.ImportPragma;
import Ada.IncompleteTypeDeclaration;
import Ada.IndependentComponentsPragma;
import Ada.IndependentPragma;
import Ada.InlinePragma;
import Ada.InspectionPointPragma;
import Ada.IntegerNumberDeclaration;
import Ada.InterruptHandlerPragma;
import Ada.InterruptPriorityPragma;
import Ada.LinkerOptionsPragma;
import Ada.ListPragma;
import Ada.LockingPolicyPragma;
import Ada.LoopParameterSpecification;
import Ada.NoReturnPragma;
import Ada.NormalizeScalarsPragma;
import Ada.NotAnElement;
import Ada.NullProcedureDeclaration;
import Ada.ObjectRenamingDeclaration;
import Ada.OptimizePragma;
import Ada.OrdinaryTypeDeclaration;
import Ada.PackPragma;
import Ada.PackageBodyDeclaration;
import Ada.PackageBodyStub;
import Ada.PackageDeclaration;
import Ada.PackageInstantiation;
import Ada.PackageRenamingDeclaration;
import Ada.PagePragma;
import Ada.ParameterSpecification;
import Ada.PartitionElaborationPolicyPragma;
import Ada.PreelaborableInitializationPragma;
import Ada.PreelaboratePragma;
import Ada.PriorityPragma;
import Ada.PrioritySpecificDispatchingPragma;
import Ada.PrivateExtensionDeclaration;
import Ada.PrivateTypeDeclaration;
import Ada.ProcedureBodyDeclaration;
import Ada.ProcedureBodyStub;
import Ada.ProcedureDeclaration;
import Ada.ProcedureInstantiation;
import Ada.ProcedureRenamingDeclaration;
import Ada.ProfilePragma;
import Ada.ProtectedBodyDeclaration;
import Ada.ProtectedBodyStub;
import Ada.ProtectedTypeDeclaration;
import Ada.PurePragma;
import Ada.QueuingPolicyPragma;
import Ada.RealNumberDeclaration;
import Ada.RelativeDeadlinePragma;
import Ada.RemoteCallInterfacePragma;
import Ada.RemoteTypesPragma;
import Ada.RestrictionsPragma;
import Ada.ReturnConstantSpecification;
import Ada.ReturnVariableSpecification;
import Ada.ReviewablePragma;
import Ada.SharedPassivePragma;
import Ada.SingleProtectedDeclaration;
import Ada.SingleTaskDeclaration;
import Ada.StorageSizePragma;
import Ada.SubtypeDeclaration;
import Ada.SuppressPragma;
import Ada.TaggedIncompleteTypeDeclaration;
import Ada.TaskBodyDeclaration;
import Ada.TaskBodyStub;
import Ada.TaskDispatchingPolicyPragma;
import Ada.TaskTypeDeclaration;
import Ada.UncheckedUnionPragma;
import Ada.UnknownPragma;
import Ada.UnsuppressPragma;
import Ada.VariableDeclaration;
import Ada.VolatileComponentsPragma;
import Ada.VolatilePragma;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Declaration List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.DeclarationListImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getNotAnElement <em>Not An Element</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getOrdinaryTypeDeclaration <em>Ordinary Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getTaskTypeDeclaration <em>Task Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getProtectedTypeDeclaration <em>Protected Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getIncompleteTypeDeclaration <em>Incomplete Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getTaggedIncompleteTypeDeclaration <em>Tagged Incomplete Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getPrivateTypeDeclaration <em>Private Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getPrivateExtensionDeclaration <em>Private Extension Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getSubtypeDeclaration <em>Subtype Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getVariableDeclaration <em>Variable Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getConstantDeclaration <em>Constant Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getDeferredConstantDeclaration <em>Deferred Constant Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getSingleTaskDeclaration <em>Single Task Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getSingleProtectedDeclaration <em>Single Protected Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getIntegerNumberDeclaration <em>Integer Number Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getRealNumberDeclaration <em>Real Number Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getEnumerationLiteralSpecification <em>Enumeration Literal Specification</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getDiscriminantSpecification <em>Discriminant Specification</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getComponentDeclaration <em>Component Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getLoopParameterSpecification <em>Loop Parameter Specification</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getGeneralizedIteratorSpecification <em>Generalized Iterator Specification</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getElementIteratorSpecification <em>Element Iterator Specification</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getProcedureDeclaration <em>Procedure Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getFunctionDeclaration <em>Function Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getParameterSpecification <em>Parameter Specification</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getProcedureBodyDeclaration <em>Procedure Body Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getFunctionBodyDeclaration <em>Function Body Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getReturnVariableSpecification <em>Return Variable Specification</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getReturnConstantSpecification <em>Return Constant Specification</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getNullProcedureDeclaration <em>Null Procedure Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getExpressionFunctionDeclaration <em>Expression Function Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getPackageDeclaration <em>Package Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getPackageBodyDeclaration <em>Package Body Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getObjectRenamingDeclaration <em>Object Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getExceptionRenamingDeclaration <em>Exception Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getPackageRenamingDeclaration <em>Package Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getProcedureRenamingDeclaration <em>Procedure Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getFunctionRenamingDeclaration <em>Function Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getGenericPackageRenamingDeclaration <em>Generic Package Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getGenericProcedureRenamingDeclaration <em>Generic Procedure Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getGenericFunctionRenamingDeclaration <em>Generic Function Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getTaskBodyDeclaration <em>Task Body Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getProtectedBodyDeclaration <em>Protected Body Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getEntryDeclaration <em>Entry Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getEntryBodyDeclaration <em>Entry Body Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getEntryIndexSpecification <em>Entry Index Specification</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getProcedureBodyStub <em>Procedure Body Stub</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getFunctionBodyStub <em>Function Body Stub</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getPackageBodyStub <em>Package Body Stub</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getTaskBodyStub <em>Task Body Stub</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getProtectedBodyStub <em>Protected Body Stub</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getExceptionDeclaration <em>Exception Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getChoiceParameterSpecification <em>Choice Parameter Specification</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getGenericProcedureDeclaration <em>Generic Procedure Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getGenericFunctionDeclaration <em>Generic Function Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getGenericPackageDeclaration <em>Generic Package Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getPackageInstantiation <em>Package Instantiation</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getProcedureInstantiation <em>Procedure Instantiation</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getFunctionInstantiation <em>Function Instantiation</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getFormalObjectDeclaration <em>Formal Object Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getFormalTypeDeclaration <em>Formal Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getFormalIncompleteTypeDeclaration <em>Formal Incomplete Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getFormalProcedureDeclaration <em>Formal Procedure Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getFormalFunctionDeclaration <em>Formal Function Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getFormalPackageDeclaration <em>Formal Package Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getFormalPackageDeclarationWithBox <em>Formal Package Declaration With Box</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getAsynchronousPragma <em>Asynchronous Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getAtomicPragma <em>Atomic Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getAttachHandlerPragma <em>Attach Handler Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getControlledPragma <em>Controlled Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getConventionPragma <em>Convention Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getDiscardNamesPragma <em>Discard Names Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getElaboratePragma <em>Elaborate Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getElaborateAllPragma <em>Elaborate All Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getExportPragma <em>Export Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getImportPragma <em>Import Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getInlinePragma <em>Inline Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getInspectionPointPragma <em>Inspection Point Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getLinkerOptionsPragma <em>Linker Options Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getListPragma <em>List Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getLockingPolicyPragma <em>Locking Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getOptimizePragma <em>Optimize Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getPackPragma <em>Pack Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getPagePragma <em>Page Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getPreelaboratePragma <em>Preelaborate Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getPriorityPragma <em>Priority Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getPurePragma <em>Pure Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getRemoteTypesPragma <em>Remote Types Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getRestrictionsPragma <em>Restrictions Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getReviewablePragma <em>Reviewable Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getSharedPassivePragma <em>Shared Passive Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getStorageSizePragma <em>Storage Size Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getSuppressPragma <em>Suppress Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getVolatilePragma <em>Volatile Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getAssertPragma <em>Assert Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getNoReturnPragma <em>No Return Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getProfilePragma <em>Profile Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getUnsuppressPragma <em>Unsuppress Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getCpuPragma <em>Cpu Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getIndependentPragma <em>Independent Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getIndependentComponentsPragma <em>Independent Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationListImpl#getUnknownPragma <em>Unknown Pragma</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeclarationListImpl extends MinimalEObjectImpl.Container implements DeclarationList {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeclarationListImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getDeclarationList();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, AdaPackage.DECLARATION_LIST__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NotAnElement> getNotAnElement() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_NotAnElement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OrdinaryTypeDeclaration> getOrdinaryTypeDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_OrdinaryTypeDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskTypeDeclaration> getTaskTypeDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_TaskTypeDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProtectedTypeDeclaration> getProtectedTypeDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ProtectedTypeDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IncompleteTypeDeclaration> getIncompleteTypeDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_IncompleteTypeDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaggedIncompleteTypeDeclaration> getTaggedIncompleteTypeDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_TaggedIncompleteTypeDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PrivateTypeDeclaration> getPrivateTypeDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_PrivateTypeDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PrivateExtensionDeclaration> getPrivateExtensionDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_PrivateExtensionDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SubtypeDeclaration> getSubtypeDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_SubtypeDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VariableDeclaration> getVariableDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_VariableDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConstantDeclaration> getConstantDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ConstantDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DeferredConstantDeclaration> getDeferredConstantDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_DeferredConstantDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SingleTaskDeclaration> getSingleTaskDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_SingleTaskDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SingleProtectedDeclaration> getSingleProtectedDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_SingleProtectedDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IntegerNumberDeclaration> getIntegerNumberDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_IntegerNumberDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RealNumberDeclaration> getRealNumberDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_RealNumberDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EnumerationLiteralSpecification> getEnumerationLiteralSpecification() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_EnumerationLiteralSpecification());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscriminantSpecification> getDiscriminantSpecification() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_DiscriminantSpecification());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComponentDeclaration> getComponentDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ComponentDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LoopParameterSpecification> getLoopParameterSpecification() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_LoopParameterSpecification());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GeneralizedIteratorSpecification> getGeneralizedIteratorSpecification() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_GeneralizedIteratorSpecification());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElementIteratorSpecification> getElementIteratorSpecification() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ElementIteratorSpecification());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcedureDeclaration> getProcedureDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ProcedureDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FunctionDeclaration> getFunctionDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_FunctionDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ParameterSpecification> getParameterSpecification() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ParameterSpecification());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcedureBodyDeclaration> getProcedureBodyDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ProcedureBodyDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FunctionBodyDeclaration> getFunctionBodyDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_FunctionBodyDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReturnVariableSpecification> getReturnVariableSpecification() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ReturnVariableSpecification());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReturnConstantSpecification> getReturnConstantSpecification() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ReturnConstantSpecification());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NullProcedureDeclaration> getNullProcedureDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_NullProcedureDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExpressionFunctionDeclaration> getExpressionFunctionDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ExpressionFunctionDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackageDeclaration> getPackageDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_PackageDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackageBodyDeclaration> getPackageBodyDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_PackageBodyDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObjectRenamingDeclaration> getObjectRenamingDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ObjectRenamingDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExceptionRenamingDeclaration> getExceptionRenamingDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ExceptionRenamingDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackageRenamingDeclaration> getPackageRenamingDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_PackageRenamingDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcedureRenamingDeclaration> getProcedureRenamingDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ProcedureRenamingDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FunctionRenamingDeclaration> getFunctionRenamingDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_FunctionRenamingDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GenericPackageRenamingDeclaration> getGenericPackageRenamingDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_GenericPackageRenamingDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GenericProcedureRenamingDeclaration> getGenericProcedureRenamingDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_GenericProcedureRenamingDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GenericFunctionRenamingDeclaration> getGenericFunctionRenamingDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_GenericFunctionRenamingDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskBodyDeclaration> getTaskBodyDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_TaskBodyDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProtectedBodyDeclaration> getProtectedBodyDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ProtectedBodyDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EntryDeclaration> getEntryDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_EntryDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EntryBodyDeclaration> getEntryBodyDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_EntryBodyDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EntryIndexSpecification> getEntryIndexSpecification() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_EntryIndexSpecification());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcedureBodyStub> getProcedureBodyStub() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ProcedureBodyStub());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FunctionBodyStub> getFunctionBodyStub() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_FunctionBodyStub());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackageBodyStub> getPackageBodyStub() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_PackageBodyStub());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskBodyStub> getTaskBodyStub() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_TaskBodyStub());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProtectedBodyStub> getProtectedBodyStub() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ProtectedBodyStub());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExceptionDeclaration> getExceptionDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ExceptionDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ChoiceParameterSpecification> getChoiceParameterSpecification() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ChoiceParameterSpecification());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GenericProcedureDeclaration> getGenericProcedureDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_GenericProcedureDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GenericFunctionDeclaration> getGenericFunctionDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_GenericFunctionDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GenericPackageDeclaration> getGenericPackageDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_GenericPackageDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackageInstantiation> getPackageInstantiation() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_PackageInstantiation());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcedureInstantiation> getProcedureInstantiation() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ProcedureInstantiation());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FunctionInstantiation> getFunctionInstantiation() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_FunctionInstantiation());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalObjectDeclaration> getFormalObjectDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_FormalObjectDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalTypeDeclaration> getFormalTypeDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_FormalTypeDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalIncompleteTypeDeclaration> getFormalIncompleteTypeDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_FormalIncompleteTypeDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalProcedureDeclaration> getFormalProcedureDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_FormalProcedureDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalFunctionDeclaration> getFormalFunctionDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_FormalFunctionDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalPackageDeclaration> getFormalPackageDeclaration() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_FormalPackageDeclaration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalPackageDeclarationWithBox> getFormalPackageDeclarationWithBox() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_FormalPackageDeclarationWithBox());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Comment> getComment() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_Comment());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AllCallsRemotePragma> getAllCallsRemotePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_AllCallsRemotePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AsynchronousPragma> getAsynchronousPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_AsynchronousPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AtomicPragma> getAtomicPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_AtomicPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AtomicComponentsPragma> getAtomicComponentsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_AtomicComponentsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AttachHandlerPragma> getAttachHandlerPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_AttachHandlerPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlledPragma> getControlledPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ControlledPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConventionPragma> getConventionPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ConventionPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscardNamesPragma> getDiscardNamesPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_DiscardNamesPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElaboratePragma> getElaboratePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ElaboratePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElaborateAllPragma> getElaborateAllPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ElaborateAllPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElaborateBodyPragma> getElaborateBodyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ElaborateBodyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExportPragma> getExportPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ExportPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ImportPragma> getImportPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ImportPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InlinePragma> getInlinePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_InlinePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InspectionPointPragma> getInspectionPointPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_InspectionPointPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InterruptHandlerPragma> getInterruptHandlerPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_InterruptHandlerPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InterruptPriorityPragma> getInterruptPriorityPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_InterruptPriorityPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LinkerOptionsPragma> getLinkerOptionsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_LinkerOptionsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ListPragma> getListPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ListPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LockingPolicyPragma> getLockingPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_LockingPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NormalizeScalarsPragma> getNormalizeScalarsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_NormalizeScalarsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OptimizePragma> getOptimizePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_OptimizePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackPragma> getPackPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_PackPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PagePragma> getPagePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_PagePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PreelaboratePragma> getPreelaboratePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_PreelaboratePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PriorityPragma> getPriorityPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_PriorityPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PurePragma> getPurePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_PurePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QueuingPolicyPragma> getQueuingPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_QueuingPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RemoteCallInterfacePragma> getRemoteCallInterfacePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_RemoteCallInterfacePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RemoteTypesPragma> getRemoteTypesPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_RemoteTypesPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RestrictionsPragma> getRestrictionsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_RestrictionsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReviewablePragma> getReviewablePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ReviewablePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SharedPassivePragma> getSharedPassivePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_SharedPassivePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StorageSizePragma> getStorageSizePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_StorageSizePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SuppressPragma> getSuppressPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_SuppressPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskDispatchingPolicyPragma> getTaskDispatchingPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_TaskDispatchingPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VolatilePragma> getVolatilePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_VolatilePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VolatileComponentsPragma> getVolatileComponentsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_VolatileComponentsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssertPragma> getAssertPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_AssertPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssertionPolicyPragma> getAssertionPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_AssertionPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DetectBlockingPragma> getDetectBlockingPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_DetectBlockingPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NoReturnPragma> getNoReturnPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_NoReturnPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PartitionElaborationPolicyPragma> getPartitionElaborationPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_PartitionElaborationPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PreelaborableInitializationPragma> getPreelaborableInitializationPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_PreelaborableInitializationPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PrioritySpecificDispatchingPragma> getPrioritySpecificDispatchingPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_PrioritySpecificDispatchingPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProfilePragma> getProfilePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ProfilePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RelativeDeadlinePragma> getRelativeDeadlinePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_RelativeDeadlinePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UncheckedUnionPragma> getUncheckedUnionPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_UncheckedUnionPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnsuppressPragma> getUnsuppressPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_UnsuppressPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefaultStoragePoolPragma> getDefaultStoragePoolPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_DefaultStoragePoolPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DispatchingDomainPragma> getDispatchingDomainPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_DispatchingDomainPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CpuPragma> getCpuPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_CpuPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IndependentPragma> getIndependentPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_IndependentPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IndependentComponentsPragma> getIndependentComponentsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_IndependentComponentsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ImplementationDefinedPragma> getImplementationDefinedPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_ImplementationDefinedPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnknownPragma> getUnknownPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDeclarationList_UnknownPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.DECLARATION_LIST__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__NOT_AN_ELEMENT:
				return ((InternalEList<?>)getNotAnElement()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__ORDINARY_TYPE_DECLARATION:
				return ((InternalEList<?>)getOrdinaryTypeDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__TASK_TYPE_DECLARATION:
				return ((InternalEList<?>)getTaskTypeDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__PROTECTED_TYPE_DECLARATION:
				return ((InternalEList<?>)getProtectedTypeDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__INCOMPLETE_TYPE_DECLARATION:
				return ((InternalEList<?>)getIncompleteTypeDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__TAGGED_INCOMPLETE_TYPE_DECLARATION:
				return ((InternalEList<?>)getTaggedIncompleteTypeDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__PRIVATE_TYPE_DECLARATION:
				return ((InternalEList<?>)getPrivateTypeDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__PRIVATE_EXTENSION_DECLARATION:
				return ((InternalEList<?>)getPrivateExtensionDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__SUBTYPE_DECLARATION:
				return ((InternalEList<?>)getSubtypeDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__VARIABLE_DECLARATION:
				return ((InternalEList<?>)getVariableDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__CONSTANT_DECLARATION:
				return ((InternalEList<?>)getConstantDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__DEFERRED_CONSTANT_DECLARATION:
				return ((InternalEList<?>)getDeferredConstantDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__SINGLE_TASK_DECLARATION:
				return ((InternalEList<?>)getSingleTaskDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__SINGLE_PROTECTED_DECLARATION:
				return ((InternalEList<?>)getSingleProtectedDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__INTEGER_NUMBER_DECLARATION:
				return ((InternalEList<?>)getIntegerNumberDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__REAL_NUMBER_DECLARATION:
				return ((InternalEList<?>)getRealNumberDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__ENUMERATION_LITERAL_SPECIFICATION:
				return ((InternalEList<?>)getEnumerationLiteralSpecification()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__DISCRIMINANT_SPECIFICATION:
				return ((InternalEList<?>)getDiscriminantSpecification()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__COMPONENT_DECLARATION:
				return ((InternalEList<?>)getComponentDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__LOOP_PARAMETER_SPECIFICATION:
				return ((InternalEList<?>)getLoopParameterSpecification()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__GENERALIZED_ITERATOR_SPECIFICATION:
				return ((InternalEList<?>)getGeneralizedIteratorSpecification()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__ELEMENT_ITERATOR_SPECIFICATION:
				return ((InternalEList<?>)getElementIteratorSpecification()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__PROCEDURE_DECLARATION:
				return ((InternalEList<?>)getProcedureDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__FUNCTION_DECLARATION:
				return ((InternalEList<?>)getFunctionDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__PARAMETER_SPECIFICATION:
				return ((InternalEList<?>)getParameterSpecification()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__PROCEDURE_BODY_DECLARATION:
				return ((InternalEList<?>)getProcedureBodyDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__FUNCTION_BODY_DECLARATION:
				return ((InternalEList<?>)getFunctionBodyDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__RETURN_VARIABLE_SPECIFICATION:
				return ((InternalEList<?>)getReturnVariableSpecification()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__RETURN_CONSTANT_SPECIFICATION:
				return ((InternalEList<?>)getReturnConstantSpecification()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__NULL_PROCEDURE_DECLARATION:
				return ((InternalEList<?>)getNullProcedureDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__EXPRESSION_FUNCTION_DECLARATION:
				return ((InternalEList<?>)getExpressionFunctionDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__PACKAGE_DECLARATION:
				return ((InternalEList<?>)getPackageDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__PACKAGE_BODY_DECLARATION:
				return ((InternalEList<?>)getPackageBodyDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__OBJECT_RENAMING_DECLARATION:
				return ((InternalEList<?>)getObjectRenamingDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__EXCEPTION_RENAMING_DECLARATION:
				return ((InternalEList<?>)getExceptionRenamingDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__PACKAGE_RENAMING_DECLARATION:
				return ((InternalEList<?>)getPackageRenamingDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__PROCEDURE_RENAMING_DECLARATION:
				return ((InternalEList<?>)getProcedureRenamingDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__FUNCTION_RENAMING_DECLARATION:
				return ((InternalEList<?>)getFunctionRenamingDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__GENERIC_PACKAGE_RENAMING_DECLARATION:
				return ((InternalEList<?>)getGenericPackageRenamingDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__GENERIC_PROCEDURE_RENAMING_DECLARATION:
				return ((InternalEList<?>)getGenericProcedureRenamingDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__GENERIC_FUNCTION_RENAMING_DECLARATION:
				return ((InternalEList<?>)getGenericFunctionRenamingDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__TASK_BODY_DECLARATION:
				return ((InternalEList<?>)getTaskBodyDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__PROTECTED_BODY_DECLARATION:
				return ((InternalEList<?>)getProtectedBodyDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__ENTRY_DECLARATION:
				return ((InternalEList<?>)getEntryDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__ENTRY_BODY_DECLARATION:
				return ((InternalEList<?>)getEntryBodyDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__ENTRY_INDEX_SPECIFICATION:
				return ((InternalEList<?>)getEntryIndexSpecification()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__PROCEDURE_BODY_STUB:
				return ((InternalEList<?>)getProcedureBodyStub()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__FUNCTION_BODY_STUB:
				return ((InternalEList<?>)getFunctionBodyStub()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__PACKAGE_BODY_STUB:
				return ((InternalEList<?>)getPackageBodyStub()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__TASK_BODY_STUB:
				return ((InternalEList<?>)getTaskBodyStub()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__PROTECTED_BODY_STUB:
				return ((InternalEList<?>)getProtectedBodyStub()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__EXCEPTION_DECLARATION:
				return ((InternalEList<?>)getExceptionDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__CHOICE_PARAMETER_SPECIFICATION:
				return ((InternalEList<?>)getChoiceParameterSpecification()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__GENERIC_PROCEDURE_DECLARATION:
				return ((InternalEList<?>)getGenericProcedureDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__GENERIC_FUNCTION_DECLARATION:
				return ((InternalEList<?>)getGenericFunctionDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__GENERIC_PACKAGE_DECLARATION:
				return ((InternalEList<?>)getGenericPackageDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__PACKAGE_INSTANTIATION:
				return ((InternalEList<?>)getPackageInstantiation()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__PROCEDURE_INSTANTIATION:
				return ((InternalEList<?>)getProcedureInstantiation()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__FUNCTION_INSTANTIATION:
				return ((InternalEList<?>)getFunctionInstantiation()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__FORMAL_OBJECT_DECLARATION:
				return ((InternalEList<?>)getFormalObjectDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__FORMAL_TYPE_DECLARATION:
				return ((InternalEList<?>)getFormalTypeDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__FORMAL_INCOMPLETE_TYPE_DECLARATION:
				return ((InternalEList<?>)getFormalIncompleteTypeDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__FORMAL_PROCEDURE_DECLARATION:
				return ((InternalEList<?>)getFormalProcedureDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__FORMAL_FUNCTION_DECLARATION:
				return ((InternalEList<?>)getFormalFunctionDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__FORMAL_PACKAGE_DECLARATION:
				return ((InternalEList<?>)getFormalPackageDeclaration()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__FORMAL_PACKAGE_DECLARATION_WITH_BOX:
				return ((InternalEList<?>)getFormalPackageDeclarationWithBox()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__COMMENT:
				return ((InternalEList<?>)getComment()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__ALL_CALLS_REMOTE_PRAGMA:
				return ((InternalEList<?>)getAllCallsRemotePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__ASYNCHRONOUS_PRAGMA:
				return ((InternalEList<?>)getAsynchronousPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__ATOMIC_PRAGMA:
				return ((InternalEList<?>)getAtomicPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__ATOMIC_COMPONENTS_PRAGMA:
				return ((InternalEList<?>)getAtomicComponentsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__ATTACH_HANDLER_PRAGMA:
				return ((InternalEList<?>)getAttachHandlerPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__CONTROLLED_PRAGMA:
				return ((InternalEList<?>)getControlledPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__CONVENTION_PRAGMA:
				return ((InternalEList<?>)getConventionPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__DISCARD_NAMES_PRAGMA:
				return ((InternalEList<?>)getDiscardNamesPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__ELABORATE_PRAGMA:
				return ((InternalEList<?>)getElaboratePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__ELABORATE_ALL_PRAGMA:
				return ((InternalEList<?>)getElaborateAllPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__ELABORATE_BODY_PRAGMA:
				return ((InternalEList<?>)getElaborateBodyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__EXPORT_PRAGMA:
				return ((InternalEList<?>)getExportPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__IMPORT_PRAGMA:
				return ((InternalEList<?>)getImportPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__INLINE_PRAGMA:
				return ((InternalEList<?>)getInlinePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__INSPECTION_POINT_PRAGMA:
				return ((InternalEList<?>)getInspectionPointPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__INTERRUPT_HANDLER_PRAGMA:
				return ((InternalEList<?>)getInterruptHandlerPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__INTERRUPT_PRIORITY_PRAGMA:
				return ((InternalEList<?>)getInterruptPriorityPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__LINKER_OPTIONS_PRAGMA:
				return ((InternalEList<?>)getLinkerOptionsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__LIST_PRAGMA:
				return ((InternalEList<?>)getListPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__LOCKING_POLICY_PRAGMA:
				return ((InternalEList<?>)getLockingPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__NORMALIZE_SCALARS_PRAGMA:
				return ((InternalEList<?>)getNormalizeScalarsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__OPTIMIZE_PRAGMA:
				return ((InternalEList<?>)getOptimizePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__PACK_PRAGMA:
				return ((InternalEList<?>)getPackPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__PAGE_PRAGMA:
				return ((InternalEList<?>)getPagePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__PREELABORATE_PRAGMA:
				return ((InternalEList<?>)getPreelaboratePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__PRIORITY_PRAGMA:
				return ((InternalEList<?>)getPriorityPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__PURE_PRAGMA:
				return ((InternalEList<?>)getPurePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__QUEUING_POLICY_PRAGMA:
				return ((InternalEList<?>)getQueuingPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				return ((InternalEList<?>)getRemoteCallInterfacePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__REMOTE_TYPES_PRAGMA:
				return ((InternalEList<?>)getRemoteTypesPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__RESTRICTIONS_PRAGMA:
				return ((InternalEList<?>)getRestrictionsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__REVIEWABLE_PRAGMA:
				return ((InternalEList<?>)getReviewablePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__SHARED_PASSIVE_PRAGMA:
				return ((InternalEList<?>)getSharedPassivePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__STORAGE_SIZE_PRAGMA:
				return ((InternalEList<?>)getStorageSizePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__SUPPRESS_PRAGMA:
				return ((InternalEList<?>)getSuppressPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				return ((InternalEList<?>)getTaskDispatchingPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__VOLATILE_PRAGMA:
				return ((InternalEList<?>)getVolatilePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__VOLATILE_COMPONENTS_PRAGMA:
				return ((InternalEList<?>)getVolatileComponentsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__ASSERT_PRAGMA:
				return ((InternalEList<?>)getAssertPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__ASSERTION_POLICY_PRAGMA:
				return ((InternalEList<?>)getAssertionPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__DETECT_BLOCKING_PRAGMA:
				return ((InternalEList<?>)getDetectBlockingPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__NO_RETURN_PRAGMA:
				return ((InternalEList<?>)getNoReturnPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				return ((InternalEList<?>)getPartitionElaborationPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				return ((InternalEList<?>)getPreelaborableInitializationPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return ((InternalEList<?>)getPrioritySpecificDispatchingPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__PROFILE_PRAGMA:
				return ((InternalEList<?>)getProfilePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__RELATIVE_DEADLINE_PRAGMA:
				return ((InternalEList<?>)getRelativeDeadlinePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__UNCHECKED_UNION_PRAGMA:
				return ((InternalEList<?>)getUncheckedUnionPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__UNSUPPRESS_PRAGMA:
				return ((InternalEList<?>)getUnsuppressPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				return ((InternalEList<?>)getDefaultStoragePoolPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__DISPATCHING_DOMAIN_PRAGMA:
				return ((InternalEList<?>)getDispatchingDomainPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__CPU_PRAGMA:
				return ((InternalEList<?>)getCpuPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__INDEPENDENT_PRAGMA:
				return ((InternalEList<?>)getIndependentPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				return ((InternalEList<?>)getIndependentComponentsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				return ((InternalEList<?>)getImplementationDefinedPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DECLARATION_LIST__UNKNOWN_PRAGMA:
				return ((InternalEList<?>)getUnknownPragma()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.DECLARATION_LIST__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case AdaPackage.DECLARATION_LIST__NOT_AN_ELEMENT:
				return getNotAnElement();
			case AdaPackage.DECLARATION_LIST__ORDINARY_TYPE_DECLARATION:
				return getOrdinaryTypeDeclaration();
			case AdaPackage.DECLARATION_LIST__TASK_TYPE_DECLARATION:
				return getTaskTypeDeclaration();
			case AdaPackage.DECLARATION_LIST__PROTECTED_TYPE_DECLARATION:
				return getProtectedTypeDeclaration();
			case AdaPackage.DECLARATION_LIST__INCOMPLETE_TYPE_DECLARATION:
				return getIncompleteTypeDeclaration();
			case AdaPackage.DECLARATION_LIST__TAGGED_INCOMPLETE_TYPE_DECLARATION:
				return getTaggedIncompleteTypeDeclaration();
			case AdaPackage.DECLARATION_LIST__PRIVATE_TYPE_DECLARATION:
				return getPrivateTypeDeclaration();
			case AdaPackage.DECLARATION_LIST__PRIVATE_EXTENSION_DECLARATION:
				return getPrivateExtensionDeclaration();
			case AdaPackage.DECLARATION_LIST__SUBTYPE_DECLARATION:
				return getSubtypeDeclaration();
			case AdaPackage.DECLARATION_LIST__VARIABLE_DECLARATION:
				return getVariableDeclaration();
			case AdaPackage.DECLARATION_LIST__CONSTANT_DECLARATION:
				return getConstantDeclaration();
			case AdaPackage.DECLARATION_LIST__DEFERRED_CONSTANT_DECLARATION:
				return getDeferredConstantDeclaration();
			case AdaPackage.DECLARATION_LIST__SINGLE_TASK_DECLARATION:
				return getSingleTaskDeclaration();
			case AdaPackage.DECLARATION_LIST__SINGLE_PROTECTED_DECLARATION:
				return getSingleProtectedDeclaration();
			case AdaPackage.DECLARATION_LIST__INTEGER_NUMBER_DECLARATION:
				return getIntegerNumberDeclaration();
			case AdaPackage.DECLARATION_LIST__REAL_NUMBER_DECLARATION:
				return getRealNumberDeclaration();
			case AdaPackage.DECLARATION_LIST__ENUMERATION_LITERAL_SPECIFICATION:
				return getEnumerationLiteralSpecification();
			case AdaPackage.DECLARATION_LIST__DISCRIMINANT_SPECIFICATION:
				return getDiscriminantSpecification();
			case AdaPackage.DECLARATION_LIST__COMPONENT_DECLARATION:
				return getComponentDeclaration();
			case AdaPackage.DECLARATION_LIST__LOOP_PARAMETER_SPECIFICATION:
				return getLoopParameterSpecification();
			case AdaPackage.DECLARATION_LIST__GENERALIZED_ITERATOR_SPECIFICATION:
				return getGeneralizedIteratorSpecification();
			case AdaPackage.DECLARATION_LIST__ELEMENT_ITERATOR_SPECIFICATION:
				return getElementIteratorSpecification();
			case AdaPackage.DECLARATION_LIST__PROCEDURE_DECLARATION:
				return getProcedureDeclaration();
			case AdaPackage.DECLARATION_LIST__FUNCTION_DECLARATION:
				return getFunctionDeclaration();
			case AdaPackage.DECLARATION_LIST__PARAMETER_SPECIFICATION:
				return getParameterSpecification();
			case AdaPackage.DECLARATION_LIST__PROCEDURE_BODY_DECLARATION:
				return getProcedureBodyDeclaration();
			case AdaPackage.DECLARATION_LIST__FUNCTION_BODY_DECLARATION:
				return getFunctionBodyDeclaration();
			case AdaPackage.DECLARATION_LIST__RETURN_VARIABLE_SPECIFICATION:
				return getReturnVariableSpecification();
			case AdaPackage.DECLARATION_LIST__RETURN_CONSTANT_SPECIFICATION:
				return getReturnConstantSpecification();
			case AdaPackage.DECLARATION_LIST__NULL_PROCEDURE_DECLARATION:
				return getNullProcedureDeclaration();
			case AdaPackage.DECLARATION_LIST__EXPRESSION_FUNCTION_DECLARATION:
				return getExpressionFunctionDeclaration();
			case AdaPackage.DECLARATION_LIST__PACKAGE_DECLARATION:
				return getPackageDeclaration();
			case AdaPackage.DECLARATION_LIST__PACKAGE_BODY_DECLARATION:
				return getPackageBodyDeclaration();
			case AdaPackage.DECLARATION_LIST__OBJECT_RENAMING_DECLARATION:
				return getObjectRenamingDeclaration();
			case AdaPackage.DECLARATION_LIST__EXCEPTION_RENAMING_DECLARATION:
				return getExceptionRenamingDeclaration();
			case AdaPackage.DECLARATION_LIST__PACKAGE_RENAMING_DECLARATION:
				return getPackageRenamingDeclaration();
			case AdaPackage.DECLARATION_LIST__PROCEDURE_RENAMING_DECLARATION:
				return getProcedureRenamingDeclaration();
			case AdaPackage.DECLARATION_LIST__FUNCTION_RENAMING_DECLARATION:
				return getFunctionRenamingDeclaration();
			case AdaPackage.DECLARATION_LIST__GENERIC_PACKAGE_RENAMING_DECLARATION:
				return getGenericPackageRenamingDeclaration();
			case AdaPackage.DECLARATION_LIST__GENERIC_PROCEDURE_RENAMING_DECLARATION:
				return getGenericProcedureRenamingDeclaration();
			case AdaPackage.DECLARATION_LIST__GENERIC_FUNCTION_RENAMING_DECLARATION:
				return getGenericFunctionRenamingDeclaration();
			case AdaPackage.DECLARATION_LIST__TASK_BODY_DECLARATION:
				return getTaskBodyDeclaration();
			case AdaPackage.DECLARATION_LIST__PROTECTED_BODY_DECLARATION:
				return getProtectedBodyDeclaration();
			case AdaPackage.DECLARATION_LIST__ENTRY_DECLARATION:
				return getEntryDeclaration();
			case AdaPackage.DECLARATION_LIST__ENTRY_BODY_DECLARATION:
				return getEntryBodyDeclaration();
			case AdaPackage.DECLARATION_LIST__ENTRY_INDEX_SPECIFICATION:
				return getEntryIndexSpecification();
			case AdaPackage.DECLARATION_LIST__PROCEDURE_BODY_STUB:
				return getProcedureBodyStub();
			case AdaPackage.DECLARATION_LIST__FUNCTION_BODY_STUB:
				return getFunctionBodyStub();
			case AdaPackage.DECLARATION_LIST__PACKAGE_BODY_STUB:
				return getPackageBodyStub();
			case AdaPackage.DECLARATION_LIST__TASK_BODY_STUB:
				return getTaskBodyStub();
			case AdaPackage.DECLARATION_LIST__PROTECTED_BODY_STUB:
				return getProtectedBodyStub();
			case AdaPackage.DECLARATION_LIST__EXCEPTION_DECLARATION:
				return getExceptionDeclaration();
			case AdaPackage.DECLARATION_LIST__CHOICE_PARAMETER_SPECIFICATION:
				return getChoiceParameterSpecification();
			case AdaPackage.DECLARATION_LIST__GENERIC_PROCEDURE_DECLARATION:
				return getGenericProcedureDeclaration();
			case AdaPackage.DECLARATION_LIST__GENERIC_FUNCTION_DECLARATION:
				return getGenericFunctionDeclaration();
			case AdaPackage.DECLARATION_LIST__GENERIC_PACKAGE_DECLARATION:
				return getGenericPackageDeclaration();
			case AdaPackage.DECLARATION_LIST__PACKAGE_INSTANTIATION:
				return getPackageInstantiation();
			case AdaPackage.DECLARATION_LIST__PROCEDURE_INSTANTIATION:
				return getProcedureInstantiation();
			case AdaPackage.DECLARATION_LIST__FUNCTION_INSTANTIATION:
				return getFunctionInstantiation();
			case AdaPackage.DECLARATION_LIST__FORMAL_OBJECT_DECLARATION:
				return getFormalObjectDeclaration();
			case AdaPackage.DECLARATION_LIST__FORMAL_TYPE_DECLARATION:
				return getFormalTypeDeclaration();
			case AdaPackage.DECLARATION_LIST__FORMAL_INCOMPLETE_TYPE_DECLARATION:
				return getFormalIncompleteTypeDeclaration();
			case AdaPackage.DECLARATION_LIST__FORMAL_PROCEDURE_DECLARATION:
				return getFormalProcedureDeclaration();
			case AdaPackage.DECLARATION_LIST__FORMAL_FUNCTION_DECLARATION:
				return getFormalFunctionDeclaration();
			case AdaPackage.DECLARATION_LIST__FORMAL_PACKAGE_DECLARATION:
				return getFormalPackageDeclaration();
			case AdaPackage.DECLARATION_LIST__FORMAL_PACKAGE_DECLARATION_WITH_BOX:
				return getFormalPackageDeclarationWithBox();
			case AdaPackage.DECLARATION_LIST__COMMENT:
				return getComment();
			case AdaPackage.DECLARATION_LIST__ALL_CALLS_REMOTE_PRAGMA:
				return getAllCallsRemotePragma();
			case AdaPackage.DECLARATION_LIST__ASYNCHRONOUS_PRAGMA:
				return getAsynchronousPragma();
			case AdaPackage.DECLARATION_LIST__ATOMIC_PRAGMA:
				return getAtomicPragma();
			case AdaPackage.DECLARATION_LIST__ATOMIC_COMPONENTS_PRAGMA:
				return getAtomicComponentsPragma();
			case AdaPackage.DECLARATION_LIST__ATTACH_HANDLER_PRAGMA:
				return getAttachHandlerPragma();
			case AdaPackage.DECLARATION_LIST__CONTROLLED_PRAGMA:
				return getControlledPragma();
			case AdaPackage.DECLARATION_LIST__CONVENTION_PRAGMA:
				return getConventionPragma();
			case AdaPackage.DECLARATION_LIST__DISCARD_NAMES_PRAGMA:
				return getDiscardNamesPragma();
			case AdaPackage.DECLARATION_LIST__ELABORATE_PRAGMA:
				return getElaboratePragma();
			case AdaPackage.DECLARATION_LIST__ELABORATE_ALL_PRAGMA:
				return getElaborateAllPragma();
			case AdaPackage.DECLARATION_LIST__ELABORATE_BODY_PRAGMA:
				return getElaborateBodyPragma();
			case AdaPackage.DECLARATION_LIST__EXPORT_PRAGMA:
				return getExportPragma();
			case AdaPackage.DECLARATION_LIST__IMPORT_PRAGMA:
				return getImportPragma();
			case AdaPackage.DECLARATION_LIST__INLINE_PRAGMA:
				return getInlinePragma();
			case AdaPackage.DECLARATION_LIST__INSPECTION_POINT_PRAGMA:
				return getInspectionPointPragma();
			case AdaPackage.DECLARATION_LIST__INTERRUPT_HANDLER_PRAGMA:
				return getInterruptHandlerPragma();
			case AdaPackage.DECLARATION_LIST__INTERRUPT_PRIORITY_PRAGMA:
				return getInterruptPriorityPragma();
			case AdaPackage.DECLARATION_LIST__LINKER_OPTIONS_PRAGMA:
				return getLinkerOptionsPragma();
			case AdaPackage.DECLARATION_LIST__LIST_PRAGMA:
				return getListPragma();
			case AdaPackage.DECLARATION_LIST__LOCKING_POLICY_PRAGMA:
				return getLockingPolicyPragma();
			case AdaPackage.DECLARATION_LIST__NORMALIZE_SCALARS_PRAGMA:
				return getNormalizeScalarsPragma();
			case AdaPackage.DECLARATION_LIST__OPTIMIZE_PRAGMA:
				return getOptimizePragma();
			case AdaPackage.DECLARATION_LIST__PACK_PRAGMA:
				return getPackPragma();
			case AdaPackage.DECLARATION_LIST__PAGE_PRAGMA:
				return getPagePragma();
			case AdaPackage.DECLARATION_LIST__PREELABORATE_PRAGMA:
				return getPreelaboratePragma();
			case AdaPackage.DECLARATION_LIST__PRIORITY_PRAGMA:
				return getPriorityPragma();
			case AdaPackage.DECLARATION_LIST__PURE_PRAGMA:
				return getPurePragma();
			case AdaPackage.DECLARATION_LIST__QUEUING_POLICY_PRAGMA:
				return getQueuingPolicyPragma();
			case AdaPackage.DECLARATION_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				return getRemoteCallInterfacePragma();
			case AdaPackage.DECLARATION_LIST__REMOTE_TYPES_PRAGMA:
				return getRemoteTypesPragma();
			case AdaPackage.DECLARATION_LIST__RESTRICTIONS_PRAGMA:
				return getRestrictionsPragma();
			case AdaPackage.DECLARATION_LIST__REVIEWABLE_PRAGMA:
				return getReviewablePragma();
			case AdaPackage.DECLARATION_LIST__SHARED_PASSIVE_PRAGMA:
				return getSharedPassivePragma();
			case AdaPackage.DECLARATION_LIST__STORAGE_SIZE_PRAGMA:
				return getStorageSizePragma();
			case AdaPackage.DECLARATION_LIST__SUPPRESS_PRAGMA:
				return getSuppressPragma();
			case AdaPackage.DECLARATION_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				return getTaskDispatchingPolicyPragma();
			case AdaPackage.DECLARATION_LIST__VOLATILE_PRAGMA:
				return getVolatilePragma();
			case AdaPackage.DECLARATION_LIST__VOLATILE_COMPONENTS_PRAGMA:
				return getVolatileComponentsPragma();
			case AdaPackage.DECLARATION_LIST__ASSERT_PRAGMA:
				return getAssertPragma();
			case AdaPackage.DECLARATION_LIST__ASSERTION_POLICY_PRAGMA:
				return getAssertionPolicyPragma();
			case AdaPackage.DECLARATION_LIST__DETECT_BLOCKING_PRAGMA:
				return getDetectBlockingPragma();
			case AdaPackage.DECLARATION_LIST__NO_RETURN_PRAGMA:
				return getNoReturnPragma();
			case AdaPackage.DECLARATION_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				return getPartitionElaborationPolicyPragma();
			case AdaPackage.DECLARATION_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				return getPreelaborableInitializationPragma();
			case AdaPackage.DECLARATION_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return getPrioritySpecificDispatchingPragma();
			case AdaPackage.DECLARATION_LIST__PROFILE_PRAGMA:
				return getProfilePragma();
			case AdaPackage.DECLARATION_LIST__RELATIVE_DEADLINE_PRAGMA:
				return getRelativeDeadlinePragma();
			case AdaPackage.DECLARATION_LIST__UNCHECKED_UNION_PRAGMA:
				return getUncheckedUnionPragma();
			case AdaPackage.DECLARATION_LIST__UNSUPPRESS_PRAGMA:
				return getUnsuppressPragma();
			case AdaPackage.DECLARATION_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				return getDefaultStoragePoolPragma();
			case AdaPackage.DECLARATION_LIST__DISPATCHING_DOMAIN_PRAGMA:
				return getDispatchingDomainPragma();
			case AdaPackage.DECLARATION_LIST__CPU_PRAGMA:
				return getCpuPragma();
			case AdaPackage.DECLARATION_LIST__INDEPENDENT_PRAGMA:
				return getIndependentPragma();
			case AdaPackage.DECLARATION_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				return getIndependentComponentsPragma();
			case AdaPackage.DECLARATION_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				return getImplementationDefinedPragma();
			case AdaPackage.DECLARATION_LIST__UNKNOWN_PRAGMA:
				return getUnknownPragma();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.DECLARATION_LIST__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case AdaPackage.DECLARATION_LIST__NOT_AN_ELEMENT:
				getNotAnElement().clear();
				getNotAnElement().addAll((Collection<? extends NotAnElement>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__ORDINARY_TYPE_DECLARATION:
				getOrdinaryTypeDeclaration().clear();
				getOrdinaryTypeDeclaration().addAll((Collection<? extends OrdinaryTypeDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__TASK_TYPE_DECLARATION:
				getTaskTypeDeclaration().clear();
				getTaskTypeDeclaration().addAll((Collection<? extends TaskTypeDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__PROTECTED_TYPE_DECLARATION:
				getProtectedTypeDeclaration().clear();
				getProtectedTypeDeclaration().addAll((Collection<? extends ProtectedTypeDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__INCOMPLETE_TYPE_DECLARATION:
				getIncompleteTypeDeclaration().clear();
				getIncompleteTypeDeclaration().addAll((Collection<? extends IncompleteTypeDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__TAGGED_INCOMPLETE_TYPE_DECLARATION:
				getTaggedIncompleteTypeDeclaration().clear();
				getTaggedIncompleteTypeDeclaration().addAll((Collection<? extends TaggedIncompleteTypeDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__PRIVATE_TYPE_DECLARATION:
				getPrivateTypeDeclaration().clear();
				getPrivateTypeDeclaration().addAll((Collection<? extends PrivateTypeDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__PRIVATE_EXTENSION_DECLARATION:
				getPrivateExtensionDeclaration().clear();
				getPrivateExtensionDeclaration().addAll((Collection<? extends PrivateExtensionDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__SUBTYPE_DECLARATION:
				getSubtypeDeclaration().clear();
				getSubtypeDeclaration().addAll((Collection<? extends SubtypeDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__VARIABLE_DECLARATION:
				getVariableDeclaration().clear();
				getVariableDeclaration().addAll((Collection<? extends VariableDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__CONSTANT_DECLARATION:
				getConstantDeclaration().clear();
				getConstantDeclaration().addAll((Collection<? extends ConstantDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__DEFERRED_CONSTANT_DECLARATION:
				getDeferredConstantDeclaration().clear();
				getDeferredConstantDeclaration().addAll((Collection<? extends DeferredConstantDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__SINGLE_TASK_DECLARATION:
				getSingleTaskDeclaration().clear();
				getSingleTaskDeclaration().addAll((Collection<? extends SingleTaskDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__SINGLE_PROTECTED_DECLARATION:
				getSingleProtectedDeclaration().clear();
				getSingleProtectedDeclaration().addAll((Collection<? extends SingleProtectedDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__INTEGER_NUMBER_DECLARATION:
				getIntegerNumberDeclaration().clear();
				getIntegerNumberDeclaration().addAll((Collection<? extends IntegerNumberDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__REAL_NUMBER_DECLARATION:
				getRealNumberDeclaration().clear();
				getRealNumberDeclaration().addAll((Collection<? extends RealNumberDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__ENUMERATION_LITERAL_SPECIFICATION:
				getEnumerationLiteralSpecification().clear();
				getEnumerationLiteralSpecification().addAll((Collection<? extends EnumerationLiteralSpecification>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__DISCRIMINANT_SPECIFICATION:
				getDiscriminantSpecification().clear();
				getDiscriminantSpecification().addAll((Collection<? extends DiscriminantSpecification>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__COMPONENT_DECLARATION:
				getComponentDeclaration().clear();
				getComponentDeclaration().addAll((Collection<? extends ComponentDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__LOOP_PARAMETER_SPECIFICATION:
				getLoopParameterSpecification().clear();
				getLoopParameterSpecification().addAll((Collection<? extends LoopParameterSpecification>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__GENERALIZED_ITERATOR_SPECIFICATION:
				getGeneralizedIteratorSpecification().clear();
				getGeneralizedIteratorSpecification().addAll((Collection<? extends GeneralizedIteratorSpecification>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__ELEMENT_ITERATOR_SPECIFICATION:
				getElementIteratorSpecification().clear();
				getElementIteratorSpecification().addAll((Collection<? extends ElementIteratorSpecification>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__PROCEDURE_DECLARATION:
				getProcedureDeclaration().clear();
				getProcedureDeclaration().addAll((Collection<? extends ProcedureDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__FUNCTION_DECLARATION:
				getFunctionDeclaration().clear();
				getFunctionDeclaration().addAll((Collection<? extends FunctionDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__PARAMETER_SPECIFICATION:
				getParameterSpecification().clear();
				getParameterSpecification().addAll((Collection<? extends ParameterSpecification>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__PROCEDURE_BODY_DECLARATION:
				getProcedureBodyDeclaration().clear();
				getProcedureBodyDeclaration().addAll((Collection<? extends ProcedureBodyDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__FUNCTION_BODY_DECLARATION:
				getFunctionBodyDeclaration().clear();
				getFunctionBodyDeclaration().addAll((Collection<? extends FunctionBodyDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__RETURN_VARIABLE_SPECIFICATION:
				getReturnVariableSpecification().clear();
				getReturnVariableSpecification().addAll((Collection<? extends ReturnVariableSpecification>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__RETURN_CONSTANT_SPECIFICATION:
				getReturnConstantSpecification().clear();
				getReturnConstantSpecification().addAll((Collection<? extends ReturnConstantSpecification>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__NULL_PROCEDURE_DECLARATION:
				getNullProcedureDeclaration().clear();
				getNullProcedureDeclaration().addAll((Collection<? extends NullProcedureDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__EXPRESSION_FUNCTION_DECLARATION:
				getExpressionFunctionDeclaration().clear();
				getExpressionFunctionDeclaration().addAll((Collection<? extends ExpressionFunctionDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__PACKAGE_DECLARATION:
				getPackageDeclaration().clear();
				getPackageDeclaration().addAll((Collection<? extends PackageDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__PACKAGE_BODY_DECLARATION:
				getPackageBodyDeclaration().clear();
				getPackageBodyDeclaration().addAll((Collection<? extends PackageBodyDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__OBJECT_RENAMING_DECLARATION:
				getObjectRenamingDeclaration().clear();
				getObjectRenamingDeclaration().addAll((Collection<? extends ObjectRenamingDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__EXCEPTION_RENAMING_DECLARATION:
				getExceptionRenamingDeclaration().clear();
				getExceptionRenamingDeclaration().addAll((Collection<? extends ExceptionRenamingDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__PACKAGE_RENAMING_DECLARATION:
				getPackageRenamingDeclaration().clear();
				getPackageRenamingDeclaration().addAll((Collection<? extends PackageRenamingDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__PROCEDURE_RENAMING_DECLARATION:
				getProcedureRenamingDeclaration().clear();
				getProcedureRenamingDeclaration().addAll((Collection<? extends ProcedureRenamingDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__FUNCTION_RENAMING_DECLARATION:
				getFunctionRenamingDeclaration().clear();
				getFunctionRenamingDeclaration().addAll((Collection<? extends FunctionRenamingDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__GENERIC_PACKAGE_RENAMING_DECLARATION:
				getGenericPackageRenamingDeclaration().clear();
				getGenericPackageRenamingDeclaration().addAll((Collection<? extends GenericPackageRenamingDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__GENERIC_PROCEDURE_RENAMING_DECLARATION:
				getGenericProcedureRenamingDeclaration().clear();
				getGenericProcedureRenamingDeclaration().addAll((Collection<? extends GenericProcedureRenamingDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__GENERIC_FUNCTION_RENAMING_DECLARATION:
				getGenericFunctionRenamingDeclaration().clear();
				getGenericFunctionRenamingDeclaration().addAll((Collection<? extends GenericFunctionRenamingDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__TASK_BODY_DECLARATION:
				getTaskBodyDeclaration().clear();
				getTaskBodyDeclaration().addAll((Collection<? extends TaskBodyDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__PROTECTED_BODY_DECLARATION:
				getProtectedBodyDeclaration().clear();
				getProtectedBodyDeclaration().addAll((Collection<? extends ProtectedBodyDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__ENTRY_DECLARATION:
				getEntryDeclaration().clear();
				getEntryDeclaration().addAll((Collection<? extends EntryDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__ENTRY_BODY_DECLARATION:
				getEntryBodyDeclaration().clear();
				getEntryBodyDeclaration().addAll((Collection<? extends EntryBodyDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__ENTRY_INDEX_SPECIFICATION:
				getEntryIndexSpecification().clear();
				getEntryIndexSpecification().addAll((Collection<? extends EntryIndexSpecification>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__PROCEDURE_BODY_STUB:
				getProcedureBodyStub().clear();
				getProcedureBodyStub().addAll((Collection<? extends ProcedureBodyStub>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__FUNCTION_BODY_STUB:
				getFunctionBodyStub().clear();
				getFunctionBodyStub().addAll((Collection<? extends FunctionBodyStub>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__PACKAGE_BODY_STUB:
				getPackageBodyStub().clear();
				getPackageBodyStub().addAll((Collection<? extends PackageBodyStub>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__TASK_BODY_STUB:
				getTaskBodyStub().clear();
				getTaskBodyStub().addAll((Collection<? extends TaskBodyStub>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__PROTECTED_BODY_STUB:
				getProtectedBodyStub().clear();
				getProtectedBodyStub().addAll((Collection<? extends ProtectedBodyStub>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__EXCEPTION_DECLARATION:
				getExceptionDeclaration().clear();
				getExceptionDeclaration().addAll((Collection<? extends ExceptionDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__CHOICE_PARAMETER_SPECIFICATION:
				getChoiceParameterSpecification().clear();
				getChoiceParameterSpecification().addAll((Collection<? extends ChoiceParameterSpecification>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__GENERIC_PROCEDURE_DECLARATION:
				getGenericProcedureDeclaration().clear();
				getGenericProcedureDeclaration().addAll((Collection<? extends GenericProcedureDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__GENERIC_FUNCTION_DECLARATION:
				getGenericFunctionDeclaration().clear();
				getGenericFunctionDeclaration().addAll((Collection<? extends GenericFunctionDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__GENERIC_PACKAGE_DECLARATION:
				getGenericPackageDeclaration().clear();
				getGenericPackageDeclaration().addAll((Collection<? extends GenericPackageDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__PACKAGE_INSTANTIATION:
				getPackageInstantiation().clear();
				getPackageInstantiation().addAll((Collection<? extends PackageInstantiation>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__PROCEDURE_INSTANTIATION:
				getProcedureInstantiation().clear();
				getProcedureInstantiation().addAll((Collection<? extends ProcedureInstantiation>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__FUNCTION_INSTANTIATION:
				getFunctionInstantiation().clear();
				getFunctionInstantiation().addAll((Collection<? extends FunctionInstantiation>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__FORMAL_OBJECT_DECLARATION:
				getFormalObjectDeclaration().clear();
				getFormalObjectDeclaration().addAll((Collection<? extends FormalObjectDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__FORMAL_TYPE_DECLARATION:
				getFormalTypeDeclaration().clear();
				getFormalTypeDeclaration().addAll((Collection<? extends FormalTypeDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__FORMAL_INCOMPLETE_TYPE_DECLARATION:
				getFormalIncompleteTypeDeclaration().clear();
				getFormalIncompleteTypeDeclaration().addAll((Collection<? extends FormalIncompleteTypeDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__FORMAL_PROCEDURE_DECLARATION:
				getFormalProcedureDeclaration().clear();
				getFormalProcedureDeclaration().addAll((Collection<? extends FormalProcedureDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__FORMAL_FUNCTION_DECLARATION:
				getFormalFunctionDeclaration().clear();
				getFormalFunctionDeclaration().addAll((Collection<? extends FormalFunctionDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__FORMAL_PACKAGE_DECLARATION:
				getFormalPackageDeclaration().clear();
				getFormalPackageDeclaration().addAll((Collection<? extends FormalPackageDeclaration>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__FORMAL_PACKAGE_DECLARATION_WITH_BOX:
				getFormalPackageDeclarationWithBox().clear();
				getFormalPackageDeclarationWithBox().addAll((Collection<? extends FormalPackageDeclarationWithBox>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__COMMENT:
				getComment().clear();
				getComment().addAll((Collection<? extends Comment>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__ALL_CALLS_REMOTE_PRAGMA:
				getAllCallsRemotePragma().clear();
				getAllCallsRemotePragma().addAll((Collection<? extends AllCallsRemotePragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__ASYNCHRONOUS_PRAGMA:
				getAsynchronousPragma().clear();
				getAsynchronousPragma().addAll((Collection<? extends AsynchronousPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__ATOMIC_PRAGMA:
				getAtomicPragma().clear();
				getAtomicPragma().addAll((Collection<? extends AtomicPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__ATOMIC_COMPONENTS_PRAGMA:
				getAtomicComponentsPragma().clear();
				getAtomicComponentsPragma().addAll((Collection<? extends AtomicComponentsPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__ATTACH_HANDLER_PRAGMA:
				getAttachHandlerPragma().clear();
				getAttachHandlerPragma().addAll((Collection<? extends AttachHandlerPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__CONTROLLED_PRAGMA:
				getControlledPragma().clear();
				getControlledPragma().addAll((Collection<? extends ControlledPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__CONVENTION_PRAGMA:
				getConventionPragma().clear();
				getConventionPragma().addAll((Collection<? extends ConventionPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__DISCARD_NAMES_PRAGMA:
				getDiscardNamesPragma().clear();
				getDiscardNamesPragma().addAll((Collection<? extends DiscardNamesPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__ELABORATE_PRAGMA:
				getElaboratePragma().clear();
				getElaboratePragma().addAll((Collection<? extends ElaboratePragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__ELABORATE_ALL_PRAGMA:
				getElaborateAllPragma().clear();
				getElaborateAllPragma().addAll((Collection<? extends ElaborateAllPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__ELABORATE_BODY_PRAGMA:
				getElaborateBodyPragma().clear();
				getElaborateBodyPragma().addAll((Collection<? extends ElaborateBodyPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__EXPORT_PRAGMA:
				getExportPragma().clear();
				getExportPragma().addAll((Collection<? extends ExportPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__IMPORT_PRAGMA:
				getImportPragma().clear();
				getImportPragma().addAll((Collection<? extends ImportPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__INLINE_PRAGMA:
				getInlinePragma().clear();
				getInlinePragma().addAll((Collection<? extends InlinePragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__INSPECTION_POINT_PRAGMA:
				getInspectionPointPragma().clear();
				getInspectionPointPragma().addAll((Collection<? extends InspectionPointPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__INTERRUPT_HANDLER_PRAGMA:
				getInterruptHandlerPragma().clear();
				getInterruptHandlerPragma().addAll((Collection<? extends InterruptHandlerPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__INTERRUPT_PRIORITY_PRAGMA:
				getInterruptPriorityPragma().clear();
				getInterruptPriorityPragma().addAll((Collection<? extends InterruptPriorityPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__LINKER_OPTIONS_PRAGMA:
				getLinkerOptionsPragma().clear();
				getLinkerOptionsPragma().addAll((Collection<? extends LinkerOptionsPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__LIST_PRAGMA:
				getListPragma().clear();
				getListPragma().addAll((Collection<? extends ListPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__LOCKING_POLICY_PRAGMA:
				getLockingPolicyPragma().clear();
				getLockingPolicyPragma().addAll((Collection<? extends LockingPolicyPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__NORMALIZE_SCALARS_PRAGMA:
				getNormalizeScalarsPragma().clear();
				getNormalizeScalarsPragma().addAll((Collection<? extends NormalizeScalarsPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__OPTIMIZE_PRAGMA:
				getOptimizePragma().clear();
				getOptimizePragma().addAll((Collection<? extends OptimizePragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__PACK_PRAGMA:
				getPackPragma().clear();
				getPackPragma().addAll((Collection<? extends PackPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__PAGE_PRAGMA:
				getPagePragma().clear();
				getPagePragma().addAll((Collection<? extends PagePragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__PREELABORATE_PRAGMA:
				getPreelaboratePragma().clear();
				getPreelaboratePragma().addAll((Collection<? extends PreelaboratePragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__PRIORITY_PRAGMA:
				getPriorityPragma().clear();
				getPriorityPragma().addAll((Collection<? extends PriorityPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__PURE_PRAGMA:
				getPurePragma().clear();
				getPurePragma().addAll((Collection<? extends PurePragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__QUEUING_POLICY_PRAGMA:
				getQueuingPolicyPragma().clear();
				getQueuingPolicyPragma().addAll((Collection<? extends QueuingPolicyPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				getRemoteCallInterfacePragma().clear();
				getRemoteCallInterfacePragma().addAll((Collection<? extends RemoteCallInterfacePragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__REMOTE_TYPES_PRAGMA:
				getRemoteTypesPragma().clear();
				getRemoteTypesPragma().addAll((Collection<? extends RemoteTypesPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__RESTRICTIONS_PRAGMA:
				getRestrictionsPragma().clear();
				getRestrictionsPragma().addAll((Collection<? extends RestrictionsPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__REVIEWABLE_PRAGMA:
				getReviewablePragma().clear();
				getReviewablePragma().addAll((Collection<? extends ReviewablePragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__SHARED_PASSIVE_PRAGMA:
				getSharedPassivePragma().clear();
				getSharedPassivePragma().addAll((Collection<? extends SharedPassivePragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__STORAGE_SIZE_PRAGMA:
				getStorageSizePragma().clear();
				getStorageSizePragma().addAll((Collection<? extends StorageSizePragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__SUPPRESS_PRAGMA:
				getSuppressPragma().clear();
				getSuppressPragma().addAll((Collection<? extends SuppressPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				getTaskDispatchingPolicyPragma().clear();
				getTaskDispatchingPolicyPragma().addAll((Collection<? extends TaskDispatchingPolicyPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__VOLATILE_PRAGMA:
				getVolatilePragma().clear();
				getVolatilePragma().addAll((Collection<? extends VolatilePragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__VOLATILE_COMPONENTS_PRAGMA:
				getVolatileComponentsPragma().clear();
				getVolatileComponentsPragma().addAll((Collection<? extends VolatileComponentsPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__ASSERT_PRAGMA:
				getAssertPragma().clear();
				getAssertPragma().addAll((Collection<? extends AssertPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__ASSERTION_POLICY_PRAGMA:
				getAssertionPolicyPragma().clear();
				getAssertionPolicyPragma().addAll((Collection<? extends AssertionPolicyPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__DETECT_BLOCKING_PRAGMA:
				getDetectBlockingPragma().clear();
				getDetectBlockingPragma().addAll((Collection<? extends DetectBlockingPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__NO_RETURN_PRAGMA:
				getNoReturnPragma().clear();
				getNoReturnPragma().addAll((Collection<? extends NoReturnPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				getPartitionElaborationPolicyPragma().clear();
				getPartitionElaborationPolicyPragma().addAll((Collection<? extends PartitionElaborationPolicyPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				getPreelaborableInitializationPragma().clear();
				getPreelaborableInitializationPragma().addAll((Collection<? extends PreelaborableInitializationPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				getPrioritySpecificDispatchingPragma().clear();
				getPrioritySpecificDispatchingPragma().addAll((Collection<? extends PrioritySpecificDispatchingPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__PROFILE_PRAGMA:
				getProfilePragma().clear();
				getProfilePragma().addAll((Collection<? extends ProfilePragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__RELATIVE_DEADLINE_PRAGMA:
				getRelativeDeadlinePragma().clear();
				getRelativeDeadlinePragma().addAll((Collection<? extends RelativeDeadlinePragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__UNCHECKED_UNION_PRAGMA:
				getUncheckedUnionPragma().clear();
				getUncheckedUnionPragma().addAll((Collection<? extends UncheckedUnionPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__UNSUPPRESS_PRAGMA:
				getUnsuppressPragma().clear();
				getUnsuppressPragma().addAll((Collection<? extends UnsuppressPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				getDefaultStoragePoolPragma().clear();
				getDefaultStoragePoolPragma().addAll((Collection<? extends DefaultStoragePoolPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__DISPATCHING_DOMAIN_PRAGMA:
				getDispatchingDomainPragma().clear();
				getDispatchingDomainPragma().addAll((Collection<? extends DispatchingDomainPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__CPU_PRAGMA:
				getCpuPragma().clear();
				getCpuPragma().addAll((Collection<? extends CpuPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__INDEPENDENT_PRAGMA:
				getIndependentPragma().clear();
				getIndependentPragma().addAll((Collection<? extends IndependentPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				getIndependentComponentsPragma().clear();
				getIndependentComponentsPragma().addAll((Collection<? extends IndependentComponentsPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				getImplementationDefinedPragma().clear();
				getImplementationDefinedPragma().addAll((Collection<? extends ImplementationDefinedPragma>)newValue);
				return;
			case AdaPackage.DECLARATION_LIST__UNKNOWN_PRAGMA:
				getUnknownPragma().clear();
				getUnknownPragma().addAll((Collection<? extends UnknownPragma>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.DECLARATION_LIST__GROUP:
				getGroup().clear();
				return;
			case AdaPackage.DECLARATION_LIST__NOT_AN_ELEMENT:
				getNotAnElement().clear();
				return;
			case AdaPackage.DECLARATION_LIST__ORDINARY_TYPE_DECLARATION:
				getOrdinaryTypeDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__TASK_TYPE_DECLARATION:
				getTaskTypeDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__PROTECTED_TYPE_DECLARATION:
				getProtectedTypeDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__INCOMPLETE_TYPE_DECLARATION:
				getIncompleteTypeDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__TAGGED_INCOMPLETE_TYPE_DECLARATION:
				getTaggedIncompleteTypeDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__PRIVATE_TYPE_DECLARATION:
				getPrivateTypeDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__PRIVATE_EXTENSION_DECLARATION:
				getPrivateExtensionDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__SUBTYPE_DECLARATION:
				getSubtypeDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__VARIABLE_DECLARATION:
				getVariableDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__CONSTANT_DECLARATION:
				getConstantDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__DEFERRED_CONSTANT_DECLARATION:
				getDeferredConstantDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__SINGLE_TASK_DECLARATION:
				getSingleTaskDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__SINGLE_PROTECTED_DECLARATION:
				getSingleProtectedDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__INTEGER_NUMBER_DECLARATION:
				getIntegerNumberDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__REAL_NUMBER_DECLARATION:
				getRealNumberDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__ENUMERATION_LITERAL_SPECIFICATION:
				getEnumerationLiteralSpecification().clear();
				return;
			case AdaPackage.DECLARATION_LIST__DISCRIMINANT_SPECIFICATION:
				getDiscriminantSpecification().clear();
				return;
			case AdaPackage.DECLARATION_LIST__COMPONENT_DECLARATION:
				getComponentDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__LOOP_PARAMETER_SPECIFICATION:
				getLoopParameterSpecification().clear();
				return;
			case AdaPackage.DECLARATION_LIST__GENERALIZED_ITERATOR_SPECIFICATION:
				getGeneralizedIteratorSpecification().clear();
				return;
			case AdaPackage.DECLARATION_LIST__ELEMENT_ITERATOR_SPECIFICATION:
				getElementIteratorSpecification().clear();
				return;
			case AdaPackage.DECLARATION_LIST__PROCEDURE_DECLARATION:
				getProcedureDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__FUNCTION_DECLARATION:
				getFunctionDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__PARAMETER_SPECIFICATION:
				getParameterSpecification().clear();
				return;
			case AdaPackage.DECLARATION_LIST__PROCEDURE_BODY_DECLARATION:
				getProcedureBodyDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__FUNCTION_BODY_DECLARATION:
				getFunctionBodyDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__RETURN_VARIABLE_SPECIFICATION:
				getReturnVariableSpecification().clear();
				return;
			case AdaPackage.DECLARATION_LIST__RETURN_CONSTANT_SPECIFICATION:
				getReturnConstantSpecification().clear();
				return;
			case AdaPackage.DECLARATION_LIST__NULL_PROCEDURE_DECLARATION:
				getNullProcedureDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__EXPRESSION_FUNCTION_DECLARATION:
				getExpressionFunctionDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__PACKAGE_DECLARATION:
				getPackageDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__PACKAGE_BODY_DECLARATION:
				getPackageBodyDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__OBJECT_RENAMING_DECLARATION:
				getObjectRenamingDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__EXCEPTION_RENAMING_DECLARATION:
				getExceptionRenamingDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__PACKAGE_RENAMING_DECLARATION:
				getPackageRenamingDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__PROCEDURE_RENAMING_DECLARATION:
				getProcedureRenamingDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__FUNCTION_RENAMING_DECLARATION:
				getFunctionRenamingDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__GENERIC_PACKAGE_RENAMING_DECLARATION:
				getGenericPackageRenamingDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__GENERIC_PROCEDURE_RENAMING_DECLARATION:
				getGenericProcedureRenamingDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__GENERIC_FUNCTION_RENAMING_DECLARATION:
				getGenericFunctionRenamingDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__TASK_BODY_DECLARATION:
				getTaskBodyDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__PROTECTED_BODY_DECLARATION:
				getProtectedBodyDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__ENTRY_DECLARATION:
				getEntryDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__ENTRY_BODY_DECLARATION:
				getEntryBodyDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__ENTRY_INDEX_SPECIFICATION:
				getEntryIndexSpecification().clear();
				return;
			case AdaPackage.DECLARATION_LIST__PROCEDURE_BODY_STUB:
				getProcedureBodyStub().clear();
				return;
			case AdaPackage.DECLARATION_LIST__FUNCTION_BODY_STUB:
				getFunctionBodyStub().clear();
				return;
			case AdaPackage.DECLARATION_LIST__PACKAGE_BODY_STUB:
				getPackageBodyStub().clear();
				return;
			case AdaPackage.DECLARATION_LIST__TASK_BODY_STUB:
				getTaskBodyStub().clear();
				return;
			case AdaPackage.DECLARATION_LIST__PROTECTED_BODY_STUB:
				getProtectedBodyStub().clear();
				return;
			case AdaPackage.DECLARATION_LIST__EXCEPTION_DECLARATION:
				getExceptionDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__CHOICE_PARAMETER_SPECIFICATION:
				getChoiceParameterSpecification().clear();
				return;
			case AdaPackage.DECLARATION_LIST__GENERIC_PROCEDURE_DECLARATION:
				getGenericProcedureDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__GENERIC_FUNCTION_DECLARATION:
				getGenericFunctionDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__GENERIC_PACKAGE_DECLARATION:
				getGenericPackageDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__PACKAGE_INSTANTIATION:
				getPackageInstantiation().clear();
				return;
			case AdaPackage.DECLARATION_LIST__PROCEDURE_INSTANTIATION:
				getProcedureInstantiation().clear();
				return;
			case AdaPackage.DECLARATION_LIST__FUNCTION_INSTANTIATION:
				getFunctionInstantiation().clear();
				return;
			case AdaPackage.DECLARATION_LIST__FORMAL_OBJECT_DECLARATION:
				getFormalObjectDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__FORMAL_TYPE_DECLARATION:
				getFormalTypeDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__FORMAL_INCOMPLETE_TYPE_DECLARATION:
				getFormalIncompleteTypeDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__FORMAL_PROCEDURE_DECLARATION:
				getFormalProcedureDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__FORMAL_FUNCTION_DECLARATION:
				getFormalFunctionDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__FORMAL_PACKAGE_DECLARATION:
				getFormalPackageDeclaration().clear();
				return;
			case AdaPackage.DECLARATION_LIST__FORMAL_PACKAGE_DECLARATION_WITH_BOX:
				getFormalPackageDeclarationWithBox().clear();
				return;
			case AdaPackage.DECLARATION_LIST__COMMENT:
				getComment().clear();
				return;
			case AdaPackage.DECLARATION_LIST__ALL_CALLS_REMOTE_PRAGMA:
				getAllCallsRemotePragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__ASYNCHRONOUS_PRAGMA:
				getAsynchronousPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__ATOMIC_PRAGMA:
				getAtomicPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__ATOMIC_COMPONENTS_PRAGMA:
				getAtomicComponentsPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__ATTACH_HANDLER_PRAGMA:
				getAttachHandlerPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__CONTROLLED_PRAGMA:
				getControlledPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__CONVENTION_PRAGMA:
				getConventionPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__DISCARD_NAMES_PRAGMA:
				getDiscardNamesPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__ELABORATE_PRAGMA:
				getElaboratePragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__ELABORATE_ALL_PRAGMA:
				getElaborateAllPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__ELABORATE_BODY_PRAGMA:
				getElaborateBodyPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__EXPORT_PRAGMA:
				getExportPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__IMPORT_PRAGMA:
				getImportPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__INLINE_PRAGMA:
				getInlinePragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__INSPECTION_POINT_PRAGMA:
				getInspectionPointPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__INTERRUPT_HANDLER_PRAGMA:
				getInterruptHandlerPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__INTERRUPT_PRIORITY_PRAGMA:
				getInterruptPriorityPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__LINKER_OPTIONS_PRAGMA:
				getLinkerOptionsPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__LIST_PRAGMA:
				getListPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__LOCKING_POLICY_PRAGMA:
				getLockingPolicyPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__NORMALIZE_SCALARS_PRAGMA:
				getNormalizeScalarsPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__OPTIMIZE_PRAGMA:
				getOptimizePragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__PACK_PRAGMA:
				getPackPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__PAGE_PRAGMA:
				getPagePragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__PREELABORATE_PRAGMA:
				getPreelaboratePragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__PRIORITY_PRAGMA:
				getPriorityPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__PURE_PRAGMA:
				getPurePragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__QUEUING_POLICY_PRAGMA:
				getQueuingPolicyPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				getRemoteCallInterfacePragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__REMOTE_TYPES_PRAGMA:
				getRemoteTypesPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__RESTRICTIONS_PRAGMA:
				getRestrictionsPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__REVIEWABLE_PRAGMA:
				getReviewablePragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__SHARED_PASSIVE_PRAGMA:
				getSharedPassivePragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__STORAGE_SIZE_PRAGMA:
				getStorageSizePragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__SUPPRESS_PRAGMA:
				getSuppressPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				getTaskDispatchingPolicyPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__VOLATILE_PRAGMA:
				getVolatilePragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__VOLATILE_COMPONENTS_PRAGMA:
				getVolatileComponentsPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__ASSERT_PRAGMA:
				getAssertPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__ASSERTION_POLICY_PRAGMA:
				getAssertionPolicyPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__DETECT_BLOCKING_PRAGMA:
				getDetectBlockingPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__NO_RETURN_PRAGMA:
				getNoReturnPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				getPartitionElaborationPolicyPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				getPreelaborableInitializationPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				getPrioritySpecificDispatchingPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__PROFILE_PRAGMA:
				getProfilePragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__RELATIVE_DEADLINE_PRAGMA:
				getRelativeDeadlinePragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__UNCHECKED_UNION_PRAGMA:
				getUncheckedUnionPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__UNSUPPRESS_PRAGMA:
				getUnsuppressPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				getDefaultStoragePoolPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__DISPATCHING_DOMAIN_PRAGMA:
				getDispatchingDomainPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__CPU_PRAGMA:
				getCpuPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__INDEPENDENT_PRAGMA:
				getIndependentPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				getIndependentComponentsPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				getImplementationDefinedPragma().clear();
				return;
			case AdaPackage.DECLARATION_LIST__UNKNOWN_PRAGMA:
				getUnknownPragma().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.DECLARATION_LIST__GROUP:
				return group != null && !group.isEmpty();
			case AdaPackage.DECLARATION_LIST__NOT_AN_ELEMENT:
				return !getNotAnElement().isEmpty();
			case AdaPackage.DECLARATION_LIST__ORDINARY_TYPE_DECLARATION:
				return !getOrdinaryTypeDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__TASK_TYPE_DECLARATION:
				return !getTaskTypeDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__PROTECTED_TYPE_DECLARATION:
				return !getProtectedTypeDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__INCOMPLETE_TYPE_DECLARATION:
				return !getIncompleteTypeDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__TAGGED_INCOMPLETE_TYPE_DECLARATION:
				return !getTaggedIncompleteTypeDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__PRIVATE_TYPE_DECLARATION:
				return !getPrivateTypeDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__PRIVATE_EXTENSION_DECLARATION:
				return !getPrivateExtensionDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__SUBTYPE_DECLARATION:
				return !getSubtypeDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__VARIABLE_DECLARATION:
				return !getVariableDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__CONSTANT_DECLARATION:
				return !getConstantDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__DEFERRED_CONSTANT_DECLARATION:
				return !getDeferredConstantDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__SINGLE_TASK_DECLARATION:
				return !getSingleTaskDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__SINGLE_PROTECTED_DECLARATION:
				return !getSingleProtectedDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__INTEGER_NUMBER_DECLARATION:
				return !getIntegerNumberDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__REAL_NUMBER_DECLARATION:
				return !getRealNumberDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__ENUMERATION_LITERAL_SPECIFICATION:
				return !getEnumerationLiteralSpecification().isEmpty();
			case AdaPackage.DECLARATION_LIST__DISCRIMINANT_SPECIFICATION:
				return !getDiscriminantSpecification().isEmpty();
			case AdaPackage.DECLARATION_LIST__COMPONENT_DECLARATION:
				return !getComponentDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__LOOP_PARAMETER_SPECIFICATION:
				return !getLoopParameterSpecification().isEmpty();
			case AdaPackage.DECLARATION_LIST__GENERALIZED_ITERATOR_SPECIFICATION:
				return !getGeneralizedIteratorSpecification().isEmpty();
			case AdaPackage.DECLARATION_LIST__ELEMENT_ITERATOR_SPECIFICATION:
				return !getElementIteratorSpecification().isEmpty();
			case AdaPackage.DECLARATION_LIST__PROCEDURE_DECLARATION:
				return !getProcedureDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__FUNCTION_DECLARATION:
				return !getFunctionDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__PARAMETER_SPECIFICATION:
				return !getParameterSpecification().isEmpty();
			case AdaPackage.DECLARATION_LIST__PROCEDURE_BODY_DECLARATION:
				return !getProcedureBodyDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__FUNCTION_BODY_DECLARATION:
				return !getFunctionBodyDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__RETURN_VARIABLE_SPECIFICATION:
				return !getReturnVariableSpecification().isEmpty();
			case AdaPackage.DECLARATION_LIST__RETURN_CONSTANT_SPECIFICATION:
				return !getReturnConstantSpecification().isEmpty();
			case AdaPackage.DECLARATION_LIST__NULL_PROCEDURE_DECLARATION:
				return !getNullProcedureDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__EXPRESSION_FUNCTION_DECLARATION:
				return !getExpressionFunctionDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__PACKAGE_DECLARATION:
				return !getPackageDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__PACKAGE_BODY_DECLARATION:
				return !getPackageBodyDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__OBJECT_RENAMING_DECLARATION:
				return !getObjectRenamingDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__EXCEPTION_RENAMING_DECLARATION:
				return !getExceptionRenamingDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__PACKAGE_RENAMING_DECLARATION:
				return !getPackageRenamingDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__PROCEDURE_RENAMING_DECLARATION:
				return !getProcedureRenamingDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__FUNCTION_RENAMING_DECLARATION:
				return !getFunctionRenamingDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__GENERIC_PACKAGE_RENAMING_DECLARATION:
				return !getGenericPackageRenamingDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__GENERIC_PROCEDURE_RENAMING_DECLARATION:
				return !getGenericProcedureRenamingDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__GENERIC_FUNCTION_RENAMING_DECLARATION:
				return !getGenericFunctionRenamingDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__TASK_BODY_DECLARATION:
				return !getTaskBodyDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__PROTECTED_BODY_DECLARATION:
				return !getProtectedBodyDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__ENTRY_DECLARATION:
				return !getEntryDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__ENTRY_BODY_DECLARATION:
				return !getEntryBodyDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__ENTRY_INDEX_SPECIFICATION:
				return !getEntryIndexSpecification().isEmpty();
			case AdaPackage.DECLARATION_LIST__PROCEDURE_BODY_STUB:
				return !getProcedureBodyStub().isEmpty();
			case AdaPackage.DECLARATION_LIST__FUNCTION_BODY_STUB:
				return !getFunctionBodyStub().isEmpty();
			case AdaPackage.DECLARATION_LIST__PACKAGE_BODY_STUB:
				return !getPackageBodyStub().isEmpty();
			case AdaPackage.DECLARATION_LIST__TASK_BODY_STUB:
				return !getTaskBodyStub().isEmpty();
			case AdaPackage.DECLARATION_LIST__PROTECTED_BODY_STUB:
				return !getProtectedBodyStub().isEmpty();
			case AdaPackage.DECLARATION_LIST__EXCEPTION_DECLARATION:
				return !getExceptionDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__CHOICE_PARAMETER_SPECIFICATION:
				return !getChoiceParameterSpecification().isEmpty();
			case AdaPackage.DECLARATION_LIST__GENERIC_PROCEDURE_DECLARATION:
				return !getGenericProcedureDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__GENERIC_FUNCTION_DECLARATION:
				return !getGenericFunctionDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__GENERIC_PACKAGE_DECLARATION:
				return !getGenericPackageDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__PACKAGE_INSTANTIATION:
				return !getPackageInstantiation().isEmpty();
			case AdaPackage.DECLARATION_LIST__PROCEDURE_INSTANTIATION:
				return !getProcedureInstantiation().isEmpty();
			case AdaPackage.DECLARATION_LIST__FUNCTION_INSTANTIATION:
				return !getFunctionInstantiation().isEmpty();
			case AdaPackage.DECLARATION_LIST__FORMAL_OBJECT_DECLARATION:
				return !getFormalObjectDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__FORMAL_TYPE_DECLARATION:
				return !getFormalTypeDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__FORMAL_INCOMPLETE_TYPE_DECLARATION:
				return !getFormalIncompleteTypeDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__FORMAL_PROCEDURE_DECLARATION:
				return !getFormalProcedureDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__FORMAL_FUNCTION_DECLARATION:
				return !getFormalFunctionDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__FORMAL_PACKAGE_DECLARATION:
				return !getFormalPackageDeclaration().isEmpty();
			case AdaPackage.DECLARATION_LIST__FORMAL_PACKAGE_DECLARATION_WITH_BOX:
				return !getFormalPackageDeclarationWithBox().isEmpty();
			case AdaPackage.DECLARATION_LIST__COMMENT:
				return !getComment().isEmpty();
			case AdaPackage.DECLARATION_LIST__ALL_CALLS_REMOTE_PRAGMA:
				return !getAllCallsRemotePragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__ASYNCHRONOUS_PRAGMA:
				return !getAsynchronousPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__ATOMIC_PRAGMA:
				return !getAtomicPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__ATOMIC_COMPONENTS_PRAGMA:
				return !getAtomicComponentsPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__ATTACH_HANDLER_PRAGMA:
				return !getAttachHandlerPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__CONTROLLED_PRAGMA:
				return !getControlledPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__CONVENTION_PRAGMA:
				return !getConventionPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__DISCARD_NAMES_PRAGMA:
				return !getDiscardNamesPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__ELABORATE_PRAGMA:
				return !getElaboratePragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__ELABORATE_ALL_PRAGMA:
				return !getElaborateAllPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__ELABORATE_BODY_PRAGMA:
				return !getElaborateBodyPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__EXPORT_PRAGMA:
				return !getExportPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__IMPORT_PRAGMA:
				return !getImportPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__INLINE_PRAGMA:
				return !getInlinePragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__INSPECTION_POINT_PRAGMA:
				return !getInspectionPointPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__INTERRUPT_HANDLER_PRAGMA:
				return !getInterruptHandlerPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__INTERRUPT_PRIORITY_PRAGMA:
				return !getInterruptPriorityPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__LINKER_OPTIONS_PRAGMA:
				return !getLinkerOptionsPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__LIST_PRAGMA:
				return !getListPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__LOCKING_POLICY_PRAGMA:
				return !getLockingPolicyPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__NORMALIZE_SCALARS_PRAGMA:
				return !getNormalizeScalarsPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__OPTIMIZE_PRAGMA:
				return !getOptimizePragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__PACK_PRAGMA:
				return !getPackPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__PAGE_PRAGMA:
				return !getPagePragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__PREELABORATE_PRAGMA:
				return !getPreelaboratePragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__PRIORITY_PRAGMA:
				return !getPriorityPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__PURE_PRAGMA:
				return !getPurePragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__QUEUING_POLICY_PRAGMA:
				return !getQueuingPolicyPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				return !getRemoteCallInterfacePragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__REMOTE_TYPES_PRAGMA:
				return !getRemoteTypesPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__RESTRICTIONS_PRAGMA:
				return !getRestrictionsPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__REVIEWABLE_PRAGMA:
				return !getReviewablePragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__SHARED_PASSIVE_PRAGMA:
				return !getSharedPassivePragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__STORAGE_SIZE_PRAGMA:
				return !getStorageSizePragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__SUPPRESS_PRAGMA:
				return !getSuppressPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				return !getTaskDispatchingPolicyPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__VOLATILE_PRAGMA:
				return !getVolatilePragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__VOLATILE_COMPONENTS_PRAGMA:
				return !getVolatileComponentsPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__ASSERT_PRAGMA:
				return !getAssertPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__ASSERTION_POLICY_PRAGMA:
				return !getAssertionPolicyPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__DETECT_BLOCKING_PRAGMA:
				return !getDetectBlockingPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__NO_RETURN_PRAGMA:
				return !getNoReturnPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				return !getPartitionElaborationPolicyPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				return !getPreelaborableInitializationPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return !getPrioritySpecificDispatchingPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__PROFILE_PRAGMA:
				return !getProfilePragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__RELATIVE_DEADLINE_PRAGMA:
				return !getRelativeDeadlinePragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__UNCHECKED_UNION_PRAGMA:
				return !getUncheckedUnionPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__UNSUPPRESS_PRAGMA:
				return !getUnsuppressPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				return !getDefaultStoragePoolPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__DISPATCHING_DOMAIN_PRAGMA:
				return !getDispatchingDomainPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__CPU_PRAGMA:
				return !getCpuPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__INDEPENDENT_PRAGMA:
				return !getIndependentPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				return !getIndependentComponentsPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				return !getImplementationDefinedPragma().isEmpty();
			case AdaPackage.DECLARATION_LIST__UNKNOWN_PRAGMA:
				return !getUnknownPragma().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(')');
		return result.toString();
	}

} //DeclarationListImpl
