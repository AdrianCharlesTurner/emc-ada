/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Has Synchronized QType1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.HasSynchronizedQType1#getSynchronized <em>Synchronized</em>}</li>
 *   <li>{@link Ada.HasSynchronizedQType1#getNotAnElement <em>Not An Element</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getHasSynchronizedQType1()
 * @model extendedMetaData="name='has_synchronized_q_._1_._type' kind='elementOnly'"
 * @generated
 */
public interface HasSynchronizedQType1 extends EObject {
	/**
	 * Returns the value of the '<em><b>Synchronized</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Synchronized</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Synchronized</em>' containment reference.
	 * @see #setSynchronized(Synchronized)
	 * @see Ada.AdaPackage#getHasSynchronizedQType1_Synchronized()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='synchronized' namespace='##targetNamespace'"
	 * @generated
	 */
	Synchronized getSynchronized();

	/**
	 * Sets the value of the '{@link Ada.HasSynchronizedQType1#getSynchronized <em>Synchronized</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Synchronized</em>' containment reference.
	 * @see #getSynchronized()
	 * @generated
	 */
	void setSynchronized(Synchronized value);

	/**
	 * Returns the value of the '<em><b>Not An Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not An Element</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not An Element</em>' containment reference.
	 * @see #setNotAnElement(NotAnElement)
	 * @see Ada.AdaPackage#getHasSynchronizedQType1_NotAnElement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not_an_element' namespace='##targetNamespace'"
	 * @generated
	 */
	NotAnElement getNotAnElement();

	/**
	 * Sets the value of the '{@link Ada.HasSynchronizedQType1#getNotAnElement <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not An Element</em>' containment reference.
	 * @see #getNotAnElement()
	 * @generated
	 */
	void setNotAnElement(NotAnElement value);

} // HasSynchronizedQType1
