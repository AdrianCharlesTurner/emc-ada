/**
 */
package Ada;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Definition List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.DefinitionList#getGroup <em>Group</em>}</li>
 *   <li>{@link Ada.DefinitionList#getNotAnElement <em>Not An Element</em>}</li>
 *   <li>{@link Ada.DefinitionList#getDerivedTypeDefinition <em>Derived Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getDerivedRecordExtensionDefinition <em>Derived Record Extension Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getEnumerationTypeDefinition <em>Enumeration Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getSignedIntegerTypeDefinition <em>Signed Integer Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getModularTypeDefinition <em>Modular Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getRootIntegerDefinition <em>Root Integer Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getRootRealDefinition <em>Root Real Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getUniversalIntegerDefinition <em>Universal Integer Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getUniversalRealDefinition <em>Universal Real Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getUniversalFixedDefinition <em>Universal Fixed Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getFloatingPointDefinition <em>Floating Point Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getOrdinaryFixedPointDefinition <em>Ordinary Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getDecimalFixedPointDefinition <em>Decimal Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getUnconstrainedArrayDefinition <em>Unconstrained Array Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getConstrainedArrayDefinition <em>Constrained Array Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getRecordTypeDefinition <em>Record Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getTaggedRecordTypeDefinition <em>Tagged Record Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getOrdinaryInterface <em>Ordinary Interface</em>}</li>
 *   <li>{@link Ada.DefinitionList#getLimitedInterface <em>Limited Interface</em>}</li>
 *   <li>{@link Ada.DefinitionList#getTaskInterface <em>Task Interface</em>}</li>
 *   <li>{@link Ada.DefinitionList#getProtectedInterface <em>Protected Interface</em>}</li>
 *   <li>{@link Ada.DefinitionList#getSynchronizedInterface <em>Synchronized Interface</em>}</li>
 *   <li>{@link Ada.DefinitionList#getPoolSpecificAccessToVariable <em>Pool Specific Access To Variable</em>}</li>
 *   <li>{@link Ada.DefinitionList#getAccessToVariable <em>Access To Variable</em>}</li>
 *   <li>{@link Ada.DefinitionList#getAccessToConstant <em>Access To Constant</em>}</li>
 *   <li>{@link Ada.DefinitionList#getAccessToProcedure <em>Access To Procedure</em>}</li>
 *   <li>{@link Ada.DefinitionList#getAccessToProtectedProcedure <em>Access To Protected Procedure</em>}</li>
 *   <li>{@link Ada.DefinitionList#getAccessToFunction <em>Access To Function</em>}</li>
 *   <li>{@link Ada.DefinitionList#getAccessToProtectedFunction <em>Access To Protected Function</em>}</li>
 *   <li>{@link Ada.DefinitionList#getSubtypeIndication <em>Subtype Indication</em>}</li>
 *   <li>{@link Ada.DefinitionList#getRangeAttributeReference <em>Range Attribute Reference</em>}</li>
 *   <li>{@link Ada.DefinitionList#getSimpleExpressionRange <em>Simple Expression Range</em>}</li>
 *   <li>{@link Ada.DefinitionList#getDigitsConstraint <em>Digits Constraint</em>}</li>
 *   <li>{@link Ada.DefinitionList#getDeltaConstraint <em>Delta Constraint</em>}</li>
 *   <li>{@link Ada.DefinitionList#getIndexConstraint <em>Index Constraint</em>}</li>
 *   <li>{@link Ada.DefinitionList#getDiscriminantConstraint <em>Discriminant Constraint</em>}</li>
 *   <li>{@link Ada.DefinitionList#getComponentDefinition <em>Component Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getDiscreteSubtypeIndicationAsSubtypeDefinition <em>Discrete Subtype Indication As Subtype Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getDiscreteRangeAttributeReferenceAsSubtypeDefinition <em>Discrete Range Attribute Reference As Subtype Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getDiscreteSimpleExpressionRangeAsSubtypeDefinition <em>Discrete Simple Expression Range As Subtype Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getDiscreteSubtypeIndication <em>Discrete Subtype Indication</em>}</li>
 *   <li>{@link Ada.DefinitionList#getDiscreteRangeAttributeReference <em>Discrete Range Attribute Reference</em>}</li>
 *   <li>{@link Ada.DefinitionList#getDiscreteSimpleExpressionRange <em>Discrete Simple Expression Range</em>}</li>
 *   <li>{@link Ada.DefinitionList#getUnknownDiscriminantPart <em>Unknown Discriminant Part</em>}</li>
 *   <li>{@link Ada.DefinitionList#getKnownDiscriminantPart <em>Known Discriminant Part</em>}</li>
 *   <li>{@link Ada.DefinitionList#getRecordDefinition <em>Record Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getNullRecordDefinition <em>Null Record Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getNullComponent <em>Null Component</em>}</li>
 *   <li>{@link Ada.DefinitionList#getVariantPart <em>Variant Part</em>}</li>
 *   <li>{@link Ada.DefinitionList#getVariant <em>Variant</em>}</li>
 *   <li>{@link Ada.DefinitionList#getOthersChoice <em>Others Choice</em>}</li>
 *   <li>{@link Ada.DefinitionList#getAnonymousAccessToVariable <em>Anonymous Access To Variable</em>}</li>
 *   <li>{@link Ada.DefinitionList#getAnonymousAccessToConstant <em>Anonymous Access To Constant</em>}</li>
 *   <li>{@link Ada.DefinitionList#getAnonymousAccessToProcedure <em>Anonymous Access To Procedure</em>}</li>
 *   <li>{@link Ada.DefinitionList#getAnonymousAccessToProtectedProcedure <em>Anonymous Access To Protected Procedure</em>}</li>
 *   <li>{@link Ada.DefinitionList#getAnonymousAccessToFunction <em>Anonymous Access To Function</em>}</li>
 *   <li>{@link Ada.DefinitionList#getAnonymousAccessToProtectedFunction <em>Anonymous Access To Protected Function</em>}</li>
 *   <li>{@link Ada.DefinitionList#getPrivateTypeDefinition <em>Private Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getTaggedPrivateTypeDefinition <em>Tagged Private Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getPrivateExtensionDefinition <em>Private Extension Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getTaskDefinition <em>Task Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getProtectedDefinition <em>Protected Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getFormalPrivateTypeDefinition <em>Formal Private Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getFormalTaggedPrivateTypeDefinition <em>Formal Tagged Private Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getFormalDerivedTypeDefinition <em>Formal Derived Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getFormalDiscreteTypeDefinition <em>Formal Discrete Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getFormalSignedIntegerTypeDefinition <em>Formal Signed Integer Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getFormalModularTypeDefinition <em>Formal Modular Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getFormalFloatingPointDefinition <em>Formal Floating Point Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getFormalOrdinaryFixedPointDefinition <em>Formal Ordinary Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getFormalDecimalFixedPointDefinition <em>Formal Decimal Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getFormalOrdinaryInterface <em>Formal Ordinary Interface</em>}</li>
 *   <li>{@link Ada.DefinitionList#getFormalLimitedInterface <em>Formal Limited Interface</em>}</li>
 *   <li>{@link Ada.DefinitionList#getFormalTaskInterface <em>Formal Task Interface</em>}</li>
 *   <li>{@link Ada.DefinitionList#getFormalProtectedInterface <em>Formal Protected Interface</em>}</li>
 *   <li>{@link Ada.DefinitionList#getFormalSynchronizedInterface <em>Formal Synchronized Interface</em>}</li>
 *   <li>{@link Ada.DefinitionList#getFormalUnconstrainedArrayDefinition <em>Formal Unconstrained Array Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getFormalConstrainedArrayDefinition <em>Formal Constrained Array Definition</em>}</li>
 *   <li>{@link Ada.DefinitionList#getFormalPoolSpecificAccessToVariable <em>Formal Pool Specific Access To Variable</em>}</li>
 *   <li>{@link Ada.DefinitionList#getFormalAccessToVariable <em>Formal Access To Variable</em>}</li>
 *   <li>{@link Ada.DefinitionList#getFormalAccessToConstant <em>Formal Access To Constant</em>}</li>
 *   <li>{@link Ada.DefinitionList#getFormalAccessToProcedure <em>Formal Access To Procedure</em>}</li>
 *   <li>{@link Ada.DefinitionList#getFormalAccessToProtectedProcedure <em>Formal Access To Protected Procedure</em>}</li>
 *   <li>{@link Ada.DefinitionList#getFormalAccessToFunction <em>Formal Access To Function</em>}</li>
 *   <li>{@link Ada.DefinitionList#getFormalAccessToProtectedFunction <em>Formal Access To Protected Function</em>}</li>
 *   <li>{@link Ada.DefinitionList#getAspectSpecification <em>Aspect Specification</em>}</li>
 *   <li>{@link Ada.DefinitionList#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link Ada.DefinitionList#getSelectedComponent <em>Selected Component</em>}</li>
 *   <li>{@link Ada.DefinitionList#getBaseAttribute <em>Base Attribute</em>}</li>
 *   <li>{@link Ada.DefinitionList#getClassAttribute <em>Class Attribute</em>}</li>
 *   <li>{@link Ada.DefinitionList#getComment <em>Comment</em>}</li>
 *   <li>{@link Ada.DefinitionList#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getAsynchronousPragma <em>Asynchronous Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getAtomicPragma <em>Atomic Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getAttachHandlerPragma <em>Attach Handler Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getControlledPragma <em>Controlled Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getConventionPragma <em>Convention Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getDiscardNamesPragma <em>Discard Names Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getElaboratePragma <em>Elaborate Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getElaborateAllPragma <em>Elaborate All Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getExportPragma <em>Export Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getImportPragma <em>Import Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getInlinePragma <em>Inline Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getInspectionPointPragma <em>Inspection Point Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getLinkerOptionsPragma <em>Linker Options Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getListPragma <em>List Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getLockingPolicyPragma <em>Locking Policy Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getOptimizePragma <em>Optimize Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getPackPragma <em>Pack Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getPagePragma <em>Page Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getPreelaboratePragma <em>Preelaborate Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getPriorityPragma <em>Priority Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getPurePragma <em>Pure Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getRemoteTypesPragma <em>Remote Types Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getRestrictionsPragma <em>Restrictions Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getReviewablePragma <em>Reviewable Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getSharedPassivePragma <em>Shared Passive Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getStorageSizePragma <em>Storage Size Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getSuppressPragma <em>Suppress Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getVolatilePragma <em>Volatile Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getAssertPragma <em>Assert Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getNoReturnPragma <em>No Return Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getProfilePragma <em>Profile Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getUnsuppressPragma <em>Unsuppress Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getCpuPragma <em>Cpu Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getIndependentPragma <em>Independent Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getIndependentComponentsPragma <em>Independent Components Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionList#getUnknownPragma <em>Unknown Pragma</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getDefinitionList()
 * @model extendedMetaData="name='Definition_List' kind='elementOnly'"
 * @generated
 */
public interface DefinitionList extends EObject {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' attribute list.
	 * @see Ada.AdaPackage#getDefinitionList_Group()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:0'"
	 * @generated
	 */
	FeatureMap getGroup();

	/**
	 * Returns the value of the '<em><b>Not An Element</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NotAnElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not An Element</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not An Element</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_NotAnElement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='not_an_element' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NotAnElement> getNotAnElement();

	/**
	 * Returns the value of the '<em><b>Derived Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DerivedTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_DerivedTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='derived_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DerivedTypeDefinition> getDerivedTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Derived Record Extension Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DerivedRecordExtensionDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived Record Extension Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived Record Extension Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_DerivedRecordExtensionDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='derived_record_extension_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DerivedRecordExtensionDefinition> getDerivedRecordExtensionDefinition();

	/**
	 * Returns the value of the '<em><b>Enumeration Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.EnumerationTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumeration Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumeration Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_EnumerationTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='enumeration_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<EnumerationTypeDefinition> getEnumerationTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Signed Integer Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SignedIntegerTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signed Integer Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signed Integer Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_SignedIntegerTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='signed_integer_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SignedIntegerTypeDefinition> getSignedIntegerTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Modular Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ModularTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Modular Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Modular Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_ModularTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='modular_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ModularTypeDefinition> getModularTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Root Integer Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RootIntegerDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Integer Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Integer Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_RootIntegerDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='root_integer_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RootIntegerDefinition> getRootIntegerDefinition();

	/**
	 * Returns the value of the '<em><b>Root Real Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RootRealDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Real Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Real Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_RootRealDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='root_real_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RootRealDefinition> getRootRealDefinition();

	/**
	 * Returns the value of the '<em><b>Universal Integer Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UniversalIntegerDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Universal Integer Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Universal Integer Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_UniversalIntegerDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='universal_integer_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UniversalIntegerDefinition> getUniversalIntegerDefinition();

	/**
	 * Returns the value of the '<em><b>Universal Real Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UniversalRealDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Universal Real Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Universal Real Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_UniversalRealDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='universal_real_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UniversalRealDefinition> getUniversalRealDefinition();

	/**
	 * Returns the value of the '<em><b>Universal Fixed Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UniversalFixedDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Universal Fixed Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Universal Fixed Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_UniversalFixedDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='universal_fixed_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UniversalFixedDefinition> getUniversalFixedDefinition();

	/**
	 * Returns the value of the '<em><b>Floating Point Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FloatingPointDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Floating Point Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Floating Point Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_FloatingPointDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='floating_point_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FloatingPointDefinition> getFloatingPointDefinition();

	/**
	 * Returns the value of the '<em><b>Ordinary Fixed Point Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OrdinaryFixedPointDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ordinary Fixed Point Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ordinary Fixed Point Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_OrdinaryFixedPointDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ordinary_fixed_point_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OrdinaryFixedPointDefinition> getOrdinaryFixedPointDefinition();

	/**
	 * Returns the value of the '<em><b>Decimal Fixed Point Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DecimalFixedPointDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Decimal Fixed Point Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Decimal Fixed Point Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_DecimalFixedPointDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='decimal_fixed_point_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DecimalFixedPointDefinition> getDecimalFixedPointDefinition();

	/**
	 * Returns the value of the '<em><b>Unconstrained Array Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnconstrainedArrayDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unconstrained Array Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unconstrained Array Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_UnconstrainedArrayDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unconstrained_array_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnconstrainedArrayDefinition> getUnconstrainedArrayDefinition();

	/**
	 * Returns the value of the '<em><b>Constrained Array Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ConstrainedArrayDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constrained Array Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constrained Array Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_ConstrainedArrayDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='constrained_array_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ConstrainedArrayDefinition> getConstrainedArrayDefinition();

	/**
	 * Returns the value of the '<em><b>Record Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RecordTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_RecordTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='record_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RecordTypeDefinition> getRecordTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Tagged Record Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TaggedRecordTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tagged Record Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tagged Record Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_TaggedRecordTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='tagged_record_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TaggedRecordTypeDefinition> getTaggedRecordTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Ordinary Interface</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OrdinaryInterface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ordinary Interface</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ordinary Interface</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_OrdinaryInterface()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ordinary_interface' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OrdinaryInterface> getOrdinaryInterface();

	/**
	 * Returns the value of the '<em><b>Limited Interface</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LimitedInterface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Limited Interface</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Limited Interface</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_LimitedInterface()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='limited_interface' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LimitedInterface> getLimitedInterface();

	/**
	 * Returns the value of the '<em><b>Task Interface</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TaskInterface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Interface</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Interface</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_TaskInterface()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='task_interface' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TaskInterface> getTaskInterface();

	/**
	 * Returns the value of the '<em><b>Protected Interface</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProtectedInterface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protected Interface</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protected Interface</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_ProtectedInterface()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='protected_interface' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProtectedInterface> getProtectedInterface();

	/**
	 * Returns the value of the '<em><b>Synchronized Interface</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SynchronizedInterface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Synchronized Interface</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Synchronized Interface</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_SynchronizedInterface()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='synchronized_interface' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SynchronizedInterface> getSynchronizedInterface();

	/**
	 * Returns the value of the '<em><b>Pool Specific Access To Variable</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PoolSpecificAccessToVariable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pool Specific Access To Variable</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pool Specific Access To Variable</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_PoolSpecificAccessToVariable()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='pool_specific_access_to_variable' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PoolSpecificAccessToVariable> getPoolSpecificAccessToVariable();

	/**
	 * Returns the value of the '<em><b>Access To Variable</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AccessToVariable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Variable</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Variable</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_AccessToVariable()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='access_to_variable' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AccessToVariable> getAccessToVariable();

	/**
	 * Returns the value of the '<em><b>Access To Constant</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AccessToConstant}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Constant</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Constant</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_AccessToConstant()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='access_to_constant' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AccessToConstant> getAccessToConstant();

	/**
	 * Returns the value of the '<em><b>Access To Procedure</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AccessToProcedure}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Procedure</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Procedure</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_AccessToProcedure()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='access_to_procedure' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AccessToProcedure> getAccessToProcedure();

	/**
	 * Returns the value of the '<em><b>Access To Protected Procedure</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AccessToProtectedProcedure}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Protected Procedure</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Protected Procedure</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_AccessToProtectedProcedure()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='access_to_protected_procedure' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AccessToProtectedProcedure> getAccessToProtectedProcedure();

	/**
	 * Returns the value of the '<em><b>Access To Function</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AccessToFunction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Function</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Function</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_AccessToFunction()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='access_to_function' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AccessToFunction> getAccessToFunction();

	/**
	 * Returns the value of the '<em><b>Access To Protected Function</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AccessToProtectedFunction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Protected Function</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Protected Function</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_AccessToProtectedFunction()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='access_to_protected_function' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AccessToProtectedFunction> getAccessToProtectedFunction();

	/**
	 * Returns the value of the '<em><b>Subtype Indication</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SubtypeIndication}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subtype Indication</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subtype Indication</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_SubtypeIndication()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='subtype_indication' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SubtypeIndication> getSubtypeIndication();

	/**
	 * Returns the value of the '<em><b>Range Attribute Reference</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RangeAttributeReference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Range Attribute Reference</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range Attribute Reference</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_RangeAttributeReference()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='range_attribute_reference' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RangeAttributeReference> getRangeAttributeReference();

	/**
	 * Returns the value of the '<em><b>Simple Expression Range</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SimpleExpressionRange}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simple Expression Range</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simple Expression Range</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_SimpleExpressionRange()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='simple_expression_range' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SimpleExpressionRange> getSimpleExpressionRange();

	/**
	 * Returns the value of the '<em><b>Digits Constraint</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DigitsConstraint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Digits Constraint</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Digits Constraint</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_DigitsConstraint()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='digits_constraint' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DigitsConstraint> getDigitsConstraint();

	/**
	 * Returns the value of the '<em><b>Delta Constraint</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DeltaConstraint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delta Constraint</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delta Constraint</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_DeltaConstraint()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='delta_constraint' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DeltaConstraint> getDeltaConstraint();

	/**
	 * Returns the value of the '<em><b>Index Constraint</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IndexConstraint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Index Constraint</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Index Constraint</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_IndexConstraint()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='index_constraint' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IndexConstraint> getIndexConstraint();

	/**
	 * Returns the value of the '<em><b>Discriminant Constraint</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscriminantConstraint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discriminant Constraint</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discriminant Constraint</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_DiscriminantConstraint()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discriminant_constraint' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscriminantConstraint> getDiscriminantConstraint();

	/**
	 * Returns the value of the '<em><b>Component Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ComponentDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_ComponentDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='component_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ComponentDefinition> getComponentDefinition();

	/**
	 * Returns the value of the '<em><b>Discrete Subtype Indication As Subtype Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscreteSubtypeIndicationAsSubtypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Subtype Indication As Subtype Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Subtype Indication As Subtype Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_DiscreteSubtypeIndicationAsSubtypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discrete_subtype_indication_as_subtype_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscreteSubtypeIndicationAsSubtypeDefinition> getDiscreteSubtypeIndicationAsSubtypeDefinition();

	/**
	 * Returns the value of the '<em><b>Discrete Range Attribute Reference As Subtype Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscreteRangeAttributeReferenceAsSubtypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Range Attribute Reference As Subtype Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Range Attribute Reference As Subtype Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_DiscreteRangeAttributeReferenceAsSubtypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discrete_range_attribute_reference_as_subtype_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscreteRangeAttributeReferenceAsSubtypeDefinition> getDiscreteRangeAttributeReferenceAsSubtypeDefinition();

	/**
	 * Returns the value of the '<em><b>Discrete Simple Expression Range As Subtype Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscreteSimpleExpressionRangeAsSubtypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Simple Expression Range As Subtype Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Simple Expression Range As Subtype Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_DiscreteSimpleExpressionRangeAsSubtypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discrete_simple_expression_range_as_subtype_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscreteSimpleExpressionRangeAsSubtypeDefinition> getDiscreteSimpleExpressionRangeAsSubtypeDefinition();

	/**
	 * Returns the value of the '<em><b>Discrete Subtype Indication</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscreteSubtypeIndication}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Subtype Indication</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Subtype Indication</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_DiscreteSubtypeIndication()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discrete_subtype_indication' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscreteSubtypeIndication> getDiscreteSubtypeIndication();

	/**
	 * Returns the value of the '<em><b>Discrete Range Attribute Reference</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscreteRangeAttributeReference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Range Attribute Reference</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Range Attribute Reference</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_DiscreteRangeAttributeReference()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discrete_range_attribute_reference' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscreteRangeAttributeReference> getDiscreteRangeAttributeReference();

	/**
	 * Returns the value of the '<em><b>Discrete Simple Expression Range</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscreteSimpleExpressionRange}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Simple Expression Range</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Simple Expression Range</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_DiscreteSimpleExpressionRange()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discrete_simple_expression_range' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscreteSimpleExpressionRange> getDiscreteSimpleExpressionRange();

	/**
	 * Returns the value of the '<em><b>Unknown Discriminant Part</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnknownDiscriminantPart}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unknown Discriminant Part</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unknown Discriminant Part</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_UnknownDiscriminantPart()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unknown_discriminant_part' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnknownDiscriminantPart> getUnknownDiscriminantPart();

	/**
	 * Returns the value of the '<em><b>Known Discriminant Part</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.KnownDiscriminantPart}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Known Discriminant Part</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Known Discriminant Part</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_KnownDiscriminantPart()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='known_discriminant_part' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<KnownDiscriminantPart> getKnownDiscriminantPart();

	/**
	 * Returns the value of the '<em><b>Record Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RecordDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_RecordDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='record_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RecordDefinition> getRecordDefinition();

	/**
	 * Returns the value of the '<em><b>Null Record Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NullRecordDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Null Record Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Null Record Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_NullRecordDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='null_record_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NullRecordDefinition> getNullRecordDefinition();

	/**
	 * Returns the value of the '<em><b>Null Component</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NullComponent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Null Component</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Null Component</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_NullComponent()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='null_component' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NullComponent> getNullComponent();

	/**
	 * Returns the value of the '<em><b>Variant Part</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.VariantPart}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variant Part</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variant Part</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_VariantPart()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='variant_part' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<VariantPart> getVariantPart();

	/**
	 * Returns the value of the '<em><b>Variant</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.Variant}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variant</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variant</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_Variant()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='variant' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<Variant> getVariant();

	/**
	 * Returns the value of the '<em><b>Others Choice</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OthersChoice}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Others Choice</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Others Choice</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_OthersChoice()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='others_choice' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OthersChoice> getOthersChoice();

	/**
	 * Returns the value of the '<em><b>Anonymous Access To Variable</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AnonymousAccessToVariable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Access To Variable</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Access To Variable</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_AnonymousAccessToVariable()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='anonymous_access_to_variable' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AnonymousAccessToVariable> getAnonymousAccessToVariable();

	/**
	 * Returns the value of the '<em><b>Anonymous Access To Constant</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AnonymousAccessToConstant}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Access To Constant</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Access To Constant</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_AnonymousAccessToConstant()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='anonymous_access_to_constant' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AnonymousAccessToConstant> getAnonymousAccessToConstant();

	/**
	 * Returns the value of the '<em><b>Anonymous Access To Procedure</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AnonymousAccessToProcedure}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Access To Procedure</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Access To Procedure</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_AnonymousAccessToProcedure()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='anonymous_access_to_procedure' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AnonymousAccessToProcedure> getAnonymousAccessToProcedure();

	/**
	 * Returns the value of the '<em><b>Anonymous Access To Protected Procedure</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AnonymousAccessToProtectedProcedure}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Access To Protected Procedure</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Access To Protected Procedure</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_AnonymousAccessToProtectedProcedure()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='anonymous_access_to_protected_procedure' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AnonymousAccessToProtectedProcedure> getAnonymousAccessToProtectedProcedure();

	/**
	 * Returns the value of the '<em><b>Anonymous Access To Function</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AnonymousAccessToFunction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Access To Function</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Access To Function</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_AnonymousAccessToFunction()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='anonymous_access_to_function' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AnonymousAccessToFunction> getAnonymousAccessToFunction();

	/**
	 * Returns the value of the '<em><b>Anonymous Access To Protected Function</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AnonymousAccessToProtectedFunction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Access To Protected Function</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Access To Protected Function</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_AnonymousAccessToProtectedFunction()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='anonymous_access_to_protected_function' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AnonymousAccessToProtectedFunction> getAnonymousAccessToProtectedFunction();

	/**
	 * Returns the value of the '<em><b>Private Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PrivateTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Private Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Private Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_PrivateTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='private_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PrivateTypeDefinition> getPrivateTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Tagged Private Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TaggedPrivateTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tagged Private Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tagged Private Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_TaggedPrivateTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='tagged_private_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TaggedPrivateTypeDefinition> getTaggedPrivateTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Private Extension Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PrivateExtensionDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Private Extension Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Private Extension Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_PrivateExtensionDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='private_extension_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PrivateExtensionDefinition> getPrivateExtensionDefinition();

	/**
	 * Returns the value of the '<em><b>Task Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TaskDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_TaskDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='task_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TaskDefinition> getTaskDefinition();

	/**
	 * Returns the value of the '<em><b>Protected Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProtectedDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protected Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protected Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_ProtectedDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='protected_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProtectedDefinition> getProtectedDefinition();

	/**
	 * Returns the value of the '<em><b>Formal Private Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalPrivateTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Private Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Private Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_FormalPrivateTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_private_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalPrivateTypeDefinition> getFormalPrivateTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Formal Tagged Private Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalTaggedPrivateTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Tagged Private Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Tagged Private Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_FormalTaggedPrivateTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_tagged_private_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalTaggedPrivateTypeDefinition> getFormalTaggedPrivateTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Formal Derived Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalDerivedTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Derived Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Derived Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_FormalDerivedTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_derived_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalDerivedTypeDefinition> getFormalDerivedTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Formal Discrete Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalDiscreteTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Discrete Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Discrete Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_FormalDiscreteTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_discrete_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalDiscreteTypeDefinition> getFormalDiscreteTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Formal Signed Integer Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalSignedIntegerTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Signed Integer Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Signed Integer Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_FormalSignedIntegerTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_signed_integer_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalSignedIntegerTypeDefinition> getFormalSignedIntegerTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Formal Modular Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalModularTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Modular Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Modular Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_FormalModularTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_modular_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalModularTypeDefinition> getFormalModularTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Formal Floating Point Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalFloatingPointDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Floating Point Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Floating Point Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_FormalFloatingPointDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_floating_point_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalFloatingPointDefinition> getFormalFloatingPointDefinition();

	/**
	 * Returns the value of the '<em><b>Formal Ordinary Fixed Point Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalOrdinaryFixedPointDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Ordinary Fixed Point Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Ordinary Fixed Point Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_FormalOrdinaryFixedPointDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_ordinary_fixed_point_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalOrdinaryFixedPointDefinition> getFormalOrdinaryFixedPointDefinition();

	/**
	 * Returns the value of the '<em><b>Formal Decimal Fixed Point Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalDecimalFixedPointDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Decimal Fixed Point Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Decimal Fixed Point Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_FormalDecimalFixedPointDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_decimal_fixed_point_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalDecimalFixedPointDefinition> getFormalDecimalFixedPointDefinition();

	/**
	 * Returns the value of the '<em><b>Formal Ordinary Interface</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalOrdinaryInterface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Ordinary Interface</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Ordinary Interface</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_FormalOrdinaryInterface()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_ordinary_interface' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalOrdinaryInterface> getFormalOrdinaryInterface();

	/**
	 * Returns the value of the '<em><b>Formal Limited Interface</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalLimitedInterface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Limited Interface</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Limited Interface</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_FormalLimitedInterface()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_limited_interface' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalLimitedInterface> getFormalLimitedInterface();

	/**
	 * Returns the value of the '<em><b>Formal Task Interface</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalTaskInterface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Task Interface</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Task Interface</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_FormalTaskInterface()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_task_interface' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalTaskInterface> getFormalTaskInterface();

	/**
	 * Returns the value of the '<em><b>Formal Protected Interface</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalProtectedInterface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Protected Interface</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Protected Interface</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_FormalProtectedInterface()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_protected_interface' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalProtectedInterface> getFormalProtectedInterface();

	/**
	 * Returns the value of the '<em><b>Formal Synchronized Interface</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalSynchronizedInterface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Synchronized Interface</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Synchronized Interface</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_FormalSynchronizedInterface()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_synchronized_interface' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalSynchronizedInterface> getFormalSynchronizedInterface();

	/**
	 * Returns the value of the '<em><b>Formal Unconstrained Array Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalUnconstrainedArrayDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Unconstrained Array Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Unconstrained Array Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_FormalUnconstrainedArrayDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_unconstrained_array_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalUnconstrainedArrayDefinition> getFormalUnconstrainedArrayDefinition();

	/**
	 * Returns the value of the '<em><b>Formal Constrained Array Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalConstrainedArrayDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Constrained Array Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Constrained Array Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_FormalConstrainedArrayDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_constrained_array_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalConstrainedArrayDefinition> getFormalConstrainedArrayDefinition();

	/**
	 * Returns the value of the '<em><b>Formal Pool Specific Access To Variable</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalPoolSpecificAccessToVariable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Pool Specific Access To Variable</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Pool Specific Access To Variable</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_FormalPoolSpecificAccessToVariable()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_pool_specific_access_to_variable' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalPoolSpecificAccessToVariable> getFormalPoolSpecificAccessToVariable();

	/**
	 * Returns the value of the '<em><b>Formal Access To Variable</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalAccessToVariable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Access To Variable</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Access To Variable</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_FormalAccessToVariable()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_access_to_variable' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalAccessToVariable> getFormalAccessToVariable();

	/**
	 * Returns the value of the '<em><b>Formal Access To Constant</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalAccessToConstant}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Access To Constant</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Access To Constant</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_FormalAccessToConstant()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_access_to_constant' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalAccessToConstant> getFormalAccessToConstant();

	/**
	 * Returns the value of the '<em><b>Formal Access To Procedure</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalAccessToProcedure}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Access To Procedure</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Access To Procedure</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_FormalAccessToProcedure()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_access_to_procedure' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalAccessToProcedure> getFormalAccessToProcedure();

	/**
	 * Returns the value of the '<em><b>Formal Access To Protected Procedure</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalAccessToProtectedProcedure}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Access To Protected Procedure</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Access To Protected Procedure</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_FormalAccessToProtectedProcedure()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_access_to_protected_procedure' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalAccessToProtectedProcedure> getFormalAccessToProtectedProcedure();

	/**
	 * Returns the value of the '<em><b>Formal Access To Function</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalAccessToFunction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Access To Function</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Access To Function</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_FormalAccessToFunction()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_access_to_function' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalAccessToFunction> getFormalAccessToFunction();

	/**
	 * Returns the value of the '<em><b>Formal Access To Protected Function</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalAccessToProtectedFunction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Access To Protected Function</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Access To Protected Function</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_FormalAccessToProtectedFunction()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_access_to_protected_function' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalAccessToProtectedFunction> getFormalAccessToProtectedFunction();

	/**
	 * Returns the value of the '<em><b>Aspect Specification</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AspectSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aspect Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aspect Specification</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_AspectSpecification()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='aspect_specification' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AspectSpecification> getAspectSpecification();

	/**
	 * Returns the value of the '<em><b>Identifier</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.Identifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Identifier</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identifier</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_Identifier()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='identifier' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<Identifier> getIdentifier();

	/**
	 * Returns the value of the '<em><b>Selected Component</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SelectedComponent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selected Component</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selected Component</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_SelectedComponent()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='selected_component' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SelectedComponent> getSelectedComponent();

	/**
	 * Returns the value of the '<em><b>Base Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.BaseAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_BaseAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='base_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<BaseAttribute> getBaseAttribute();

	/**
	 * Returns the value of the '<em><b>Class Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ClassAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_ClassAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='class_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ClassAttribute> getClassAttribute();

	/**
	 * Returns the value of the '<em><b>Comment</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.Comment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comment</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comment</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_Comment()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='comment' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<Comment> getComment();

	/**
	 * Returns the value of the '<em><b>All Calls Remote Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AllCallsRemotePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Calls Remote Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Calls Remote Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_AllCallsRemotePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='all_calls_remote_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AllCallsRemotePragma> getAllCallsRemotePragma();

	/**
	 * Returns the value of the '<em><b>Asynchronous Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AsynchronousPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asynchronous Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asynchronous Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_AsynchronousPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='asynchronous_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AsynchronousPragma> getAsynchronousPragma();

	/**
	 * Returns the value of the '<em><b>Atomic Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AtomicPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_AtomicPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='atomic_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AtomicPragma> getAtomicPragma();

	/**
	 * Returns the value of the '<em><b>Atomic Components Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AtomicComponentsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Components Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Components Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_AtomicComponentsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='atomic_components_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AtomicComponentsPragma> getAtomicComponentsPragma();

	/**
	 * Returns the value of the '<em><b>Attach Handler Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AttachHandlerPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attach Handler Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attach Handler Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_AttachHandlerPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='attach_handler_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AttachHandlerPragma> getAttachHandlerPragma();

	/**
	 * Returns the value of the '<em><b>Controlled Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ControlledPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controlled Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controlled Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_ControlledPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='controlled_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ControlledPragma> getControlledPragma();

	/**
	 * Returns the value of the '<em><b>Convention Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ConventionPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Convention Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Convention Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_ConventionPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='convention_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ConventionPragma> getConventionPragma();

	/**
	 * Returns the value of the '<em><b>Discard Names Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscardNamesPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discard Names Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discard Names Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_DiscardNamesPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discard_names_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscardNamesPragma> getDiscardNamesPragma();

	/**
	 * Returns the value of the '<em><b>Elaborate Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElaboratePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_ElaboratePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='elaborate_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElaboratePragma> getElaboratePragma();

	/**
	 * Returns the value of the '<em><b>Elaborate All Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElaborateAllPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate All Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate All Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_ElaborateAllPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='elaborate_all_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElaborateAllPragma> getElaborateAllPragma();

	/**
	 * Returns the value of the '<em><b>Elaborate Body Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElaborateBodyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Body Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Body Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_ElaborateBodyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='elaborate_body_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElaborateBodyPragma> getElaborateBodyPragma();

	/**
	 * Returns the value of the '<em><b>Export Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExportPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Export Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Export Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_ExportPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='export_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExportPragma> getExportPragma();

	/**
	 * Returns the value of the '<em><b>Import Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ImportPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Import Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Import Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_ImportPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='import_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ImportPragma> getImportPragma();

	/**
	 * Returns the value of the '<em><b>Inline Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InlinePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inline Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inline Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_InlinePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='inline_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InlinePragma> getInlinePragma();

	/**
	 * Returns the value of the '<em><b>Inspection Point Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InspectionPointPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inspection Point Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inspection Point Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_InspectionPointPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='inspection_point_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InspectionPointPragma> getInspectionPointPragma();

	/**
	 * Returns the value of the '<em><b>Interrupt Handler Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InterruptHandlerPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Handler Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Handler Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_InterruptHandlerPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='interrupt_handler_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InterruptHandlerPragma> getInterruptHandlerPragma();

	/**
	 * Returns the value of the '<em><b>Interrupt Priority Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InterruptPriorityPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Priority Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Priority Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_InterruptPriorityPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='interrupt_priority_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InterruptPriorityPragma> getInterruptPriorityPragma();

	/**
	 * Returns the value of the '<em><b>Linker Options Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LinkerOptionsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Linker Options Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Linker Options Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_LinkerOptionsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='linker_options_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LinkerOptionsPragma> getLinkerOptionsPragma();

	/**
	 * Returns the value of the '<em><b>List Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ListPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_ListPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='list_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ListPragma> getListPragma();

	/**
	 * Returns the value of the '<em><b>Locking Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LockingPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Locking Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Locking Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_LockingPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='locking_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LockingPolicyPragma> getLockingPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Normalize Scalars Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NormalizeScalarsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Normalize Scalars Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Normalize Scalars Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_NormalizeScalarsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='normalize_scalars_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NormalizeScalarsPragma> getNormalizeScalarsPragma();

	/**
	 * Returns the value of the '<em><b>Optimize Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OptimizePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Optimize Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Optimize Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_OptimizePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='optimize_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OptimizePragma> getOptimizePragma();

	/**
	 * Returns the value of the '<em><b>Pack Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PackPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pack Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pack Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_PackPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='pack_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PackPragma> getPackPragma();

	/**
	 * Returns the value of the '<em><b>Page Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PagePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Page Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Page Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_PagePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='page_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PagePragma> getPagePragma();

	/**
	 * Returns the value of the '<em><b>Preelaborate Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PreelaboratePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborate Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborate Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_PreelaboratePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='preelaborate_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PreelaboratePragma> getPreelaboratePragma();

	/**
	 * Returns the value of the '<em><b>Priority Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PriorityPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_PriorityPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='priority_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PriorityPragma> getPriorityPragma();

	/**
	 * Returns the value of the '<em><b>Pure Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PurePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pure Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pure Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_PurePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='pure_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PurePragma> getPurePragma();

	/**
	 * Returns the value of the '<em><b>Queuing Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.QueuingPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Queuing Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Queuing Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_QueuingPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='queuing_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<QueuingPolicyPragma> getQueuingPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Remote Call Interface Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RemoteCallInterfacePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Call Interface Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Call Interface Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_RemoteCallInterfacePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='remote_call_interface_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RemoteCallInterfacePragma> getRemoteCallInterfacePragma();

	/**
	 * Returns the value of the '<em><b>Remote Types Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RemoteTypesPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Types Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Types Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_RemoteTypesPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='remote_types_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RemoteTypesPragma> getRemoteTypesPragma();

	/**
	 * Returns the value of the '<em><b>Restrictions Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RestrictionsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Restrictions Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Restrictions Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_RestrictionsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='restrictions_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RestrictionsPragma> getRestrictionsPragma();

	/**
	 * Returns the value of the '<em><b>Reviewable Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ReviewablePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reviewable Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reviewable Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_ReviewablePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='reviewable_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ReviewablePragma> getReviewablePragma();

	/**
	 * Returns the value of the '<em><b>Shared Passive Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SharedPassivePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shared Passive Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shared Passive Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_SharedPassivePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='shared_passive_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SharedPassivePragma> getSharedPassivePragma();

	/**
	 * Returns the value of the '<em><b>Storage Size Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.StorageSizePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Size Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Size Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_StorageSizePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='storage_size_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<StorageSizePragma> getStorageSizePragma();

	/**
	 * Returns the value of the '<em><b>Suppress Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SuppressPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Suppress Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Suppress Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_SuppressPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='suppress_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SuppressPragma> getSuppressPragma();

	/**
	 * Returns the value of the '<em><b>Task Dispatching Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TaskDispatchingPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Dispatching Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Dispatching Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_TaskDispatchingPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='task_dispatching_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TaskDispatchingPolicyPragma> getTaskDispatchingPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Volatile Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.VolatilePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_VolatilePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='volatile_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<VolatilePragma> getVolatilePragma();

	/**
	 * Returns the value of the '<em><b>Volatile Components Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.VolatileComponentsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Components Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Components Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_VolatileComponentsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='volatile_components_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<VolatileComponentsPragma> getVolatileComponentsPragma();

	/**
	 * Returns the value of the '<em><b>Assert Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AssertPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assert Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assert Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_AssertPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='assert_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AssertPragma> getAssertPragma();

	/**
	 * Returns the value of the '<em><b>Assertion Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AssertionPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assertion Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assertion Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_AssertionPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='assertion_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AssertionPolicyPragma> getAssertionPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Detect Blocking Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DetectBlockingPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Detect Blocking Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Detect Blocking Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_DetectBlockingPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='detect_blocking_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DetectBlockingPragma> getDetectBlockingPragma();

	/**
	 * Returns the value of the '<em><b>No Return Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NoReturnPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>No Return Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>No Return Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_NoReturnPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='no_return_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NoReturnPragma> getNoReturnPragma();

	/**
	 * Returns the value of the '<em><b>Partition Elaboration Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PartitionElaborationPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partition Elaboration Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partition Elaboration Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_PartitionElaborationPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='partition_elaboration_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PartitionElaborationPolicyPragma> getPartitionElaborationPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Preelaborable Initialization Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PreelaborableInitializationPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborable Initialization Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborable Initialization Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_PreelaborableInitializationPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='preelaborable_initialization_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PreelaborableInitializationPragma> getPreelaborableInitializationPragma();

	/**
	 * Returns the value of the '<em><b>Priority Specific Dispatching Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PrioritySpecificDispatchingPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Specific Dispatching Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Specific Dispatching Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_PrioritySpecificDispatchingPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='priority_specific_dispatching_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PrioritySpecificDispatchingPragma> getPrioritySpecificDispatchingPragma();

	/**
	 * Returns the value of the '<em><b>Profile Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProfilePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Profile Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Profile Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_ProfilePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='profile_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProfilePragma> getProfilePragma();

	/**
	 * Returns the value of the '<em><b>Relative Deadline Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RelativeDeadlinePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relative Deadline Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relative Deadline Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_RelativeDeadlinePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='relative_deadline_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RelativeDeadlinePragma> getRelativeDeadlinePragma();

	/**
	 * Returns the value of the '<em><b>Unchecked Union Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UncheckedUnionPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unchecked Union Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unchecked Union Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_UncheckedUnionPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unchecked_union_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UncheckedUnionPragma> getUncheckedUnionPragma();

	/**
	 * Returns the value of the '<em><b>Unsuppress Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnsuppressPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unsuppress Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unsuppress Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_UnsuppressPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unsuppress_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnsuppressPragma> getUnsuppressPragma();

	/**
	 * Returns the value of the '<em><b>Default Storage Pool Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefaultStoragePoolPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Storage Pool Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Storage Pool Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_DefaultStoragePoolPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='default_storage_pool_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefaultStoragePoolPragma> getDefaultStoragePoolPragma();

	/**
	 * Returns the value of the '<em><b>Dispatching Domain Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DispatchingDomainPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dispatching Domain Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dispatching Domain Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_DispatchingDomainPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='dispatching_domain_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DispatchingDomainPragma> getDispatchingDomainPragma();

	/**
	 * Returns the value of the '<em><b>Cpu Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CpuPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cpu Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cpu Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_CpuPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='cpu_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CpuPragma> getCpuPragma();

	/**
	 * Returns the value of the '<em><b>Independent Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IndependentPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_IndependentPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='independent_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IndependentPragma> getIndependentPragma();

	/**
	 * Returns the value of the '<em><b>Independent Components Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IndependentComponentsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Components Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Components Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_IndependentComponentsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='independent_components_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IndependentComponentsPragma> getIndependentComponentsPragma();

	/**
	 * Returns the value of the '<em><b>Implementation Defined Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ImplementationDefinedPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implementation Defined Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implementation Defined Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_ImplementationDefinedPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='implementation_defined_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ImplementationDefinedPragma> getImplementationDefinedPragma();

	/**
	 * Returns the value of the '<em><b>Unknown Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnknownPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unknown Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unknown Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDefinitionList_UnknownPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unknown_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnknownPragma> getUnknownPragma();

} // DefinitionList
