/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.FormalTaggedPrivateTypeDefinition;
import Ada.HasAbstractQType6;
import Ada.HasLimitedQType1;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Formal Tagged Private Type Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.FormalTaggedPrivateTypeDefinitionImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.FormalTaggedPrivateTypeDefinitionImpl#getHasAbstractQ <em>Has Abstract Q</em>}</li>
 *   <li>{@link Ada.impl.FormalTaggedPrivateTypeDefinitionImpl#getHasLimitedQ <em>Has Limited Q</em>}</li>
 *   <li>{@link Ada.impl.FormalTaggedPrivateTypeDefinitionImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FormalTaggedPrivateTypeDefinitionImpl extends MinimalEObjectImpl.Container implements FormalTaggedPrivateTypeDefinition {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getHasAbstractQ() <em>Has Abstract Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasAbstractQ()
	 * @generated
	 * @ordered
	 */
	protected HasAbstractQType6 hasAbstractQ;

	/**
	 * The cached value of the '{@link #getHasLimitedQ() <em>Has Limited Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasLimitedQ()
	 * @generated
	 * @ordered
	 */
	protected HasLimitedQType1 hasLimitedQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FormalTaggedPrivateTypeDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getFormalTaggedPrivateTypeDefinition();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAbstractQType6 getHasAbstractQ() {
		return hasAbstractQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasAbstractQ(HasAbstractQType6 newHasAbstractQ, NotificationChain msgs) {
		HasAbstractQType6 oldHasAbstractQ = hasAbstractQ;
		hasAbstractQ = newHasAbstractQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__HAS_ABSTRACT_Q, oldHasAbstractQ, newHasAbstractQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasAbstractQ(HasAbstractQType6 newHasAbstractQ) {
		if (newHasAbstractQ != hasAbstractQ) {
			NotificationChain msgs = null;
			if (hasAbstractQ != null)
				msgs = ((InternalEObject)hasAbstractQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__HAS_ABSTRACT_Q, null, msgs);
			if (newHasAbstractQ != null)
				msgs = ((InternalEObject)newHasAbstractQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__HAS_ABSTRACT_Q, null, msgs);
			msgs = basicSetHasAbstractQ(newHasAbstractQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__HAS_ABSTRACT_Q, newHasAbstractQ, newHasAbstractQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasLimitedQType1 getHasLimitedQ() {
		return hasLimitedQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasLimitedQ(HasLimitedQType1 newHasLimitedQ, NotificationChain msgs) {
		HasLimitedQType1 oldHasLimitedQ = hasLimitedQ;
		hasLimitedQ = newHasLimitedQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__HAS_LIMITED_Q, oldHasLimitedQ, newHasLimitedQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasLimitedQ(HasLimitedQType1 newHasLimitedQ) {
		if (newHasLimitedQ != hasLimitedQ) {
			NotificationChain msgs = null;
			if (hasLimitedQ != null)
				msgs = ((InternalEObject)hasLimitedQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__HAS_LIMITED_Q, null, msgs);
			if (newHasLimitedQ != null)
				msgs = ((InternalEObject)newHasLimitedQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__HAS_LIMITED_Q, null, msgs);
			msgs = basicSetHasLimitedQ(newHasLimitedQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__HAS_LIMITED_Q, newHasLimitedQ, newHasLimitedQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__HAS_ABSTRACT_Q:
				return basicSetHasAbstractQ(null, msgs);
			case AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__HAS_LIMITED_Q:
				return basicSetHasLimitedQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__SLOC:
				return getSloc();
			case AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__HAS_ABSTRACT_Q:
				return getHasAbstractQ();
			case AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__HAS_LIMITED_Q:
				return getHasLimitedQ();
			case AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__HAS_ABSTRACT_Q:
				setHasAbstractQ((HasAbstractQType6)newValue);
				return;
			case AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__HAS_LIMITED_Q:
				setHasLimitedQ((HasLimitedQType1)newValue);
				return;
			case AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__HAS_ABSTRACT_Q:
				setHasAbstractQ((HasAbstractQType6)null);
				return;
			case AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__HAS_LIMITED_Q:
				setHasLimitedQ((HasLimitedQType1)null);
				return;
			case AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__SLOC:
				return sloc != null;
			case AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__HAS_ABSTRACT_Q:
				return hasAbstractQ != null;
			case AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__HAS_LIMITED_Q:
				return hasLimitedQ != null;
			case AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //FormalTaggedPrivateTypeDefinitionImpl
