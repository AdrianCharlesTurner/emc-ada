/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>For All Quantified Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.ForAllQuantifiedExpression#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.ForAllQuantifiedExpression#getIteratorSpecificationQ <em>Iterator Specification Q</em>}</li>
 *   <li>{@link Ada.ForAllQuantifiedExpression#getPredicateQ <em>Predicate Q</em>}</li>
 *   <li>{@link Ada.ForAllQuantifiedExpression#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.ForAllQuantifiedExpression#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getForAllQuantifiedExpression()
 * @model extendedMetaData="name='For_All_Quantified_Expression' kind='elementOnly'"
 * @generated
 */
public interface ForAllQuantifiedExpression extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getForAllQuantifiedExpression_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.ForAllQuantifiedExpression#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Iterator Specification Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iterator Specification Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iterator Specification Q</em>' containment reference.
	 * @see #setIteratorSpecificationQ(DeclarationClass)
	 * @see Ada.AdaPackage#getForAllQuantifiedExpression_IteratorSpecificationQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='iterator_specification_q' namespace='##targetNamespace'"
	 * @generated
	 */
	DeclarationClass getIteratorSpecificationQ();

	/**
	 * Sets the value of the '{@link Ada.ForAllQuantifiedExpression#getIteratorSpecificationQ <em>Iterator Specification Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iterator Specification Q</em>' containment reference.
	 * @see #getIteratorSpecificationQ()
	 * @generated
	 */
	void setIteratorSpecificationQ(DeclarationClass value);

	/**
	 * Returns the value of the '<em><b>Predicate Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Predicate Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Predicate Q</em>' containment reference.
	 * @see #setPredicateQ(ExpressionClass)
	 * @see Ada.AdaPackage#getForAllQuantifiedExpression_PredicateQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='predicate_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getPredicateQ();

	/**
	 * Sets the value of the '{@link Ada.ForAllQuantifiedExpression#getPredicateQ <em>Predicate Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Predicate Q</em>' containment reference.
	 * @see #getPredicateQ()
	 * @generated
	 */
	void setPredicateQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getForAllQuantifiedExpression_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.ForAllQuantifiedExpression#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see Ada.AdaPackage#getForAllQuantifiedExpression_Type()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='type' namespace='##targetNamespace'"
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link Ada.ForAllQuantifiedExpression#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

} // ForAllQuantifiedExpression
