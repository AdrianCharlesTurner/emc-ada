/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Formal Object Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.FormalObjectDeclaration#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.FormalObjectDeclaration#getNamesQl <em>Names Ql</em>}</li>
 *   <li>{@link Ada.FormalObjectDeclaration#getHasNullExclusionQ <em>Has Null Exclusion Q</em>}</li>
 *   <li>{@link Ada.FormalObjectDeclaration#getObjectDeclarationViewQ <em>Object Declaration View Q</em>}</li>
 *   <li>{@link Ada.FormalObjectDeclaration#getInitializationExpressionQ <em>Initialization Expression Q</em>}</li>
 *   <li>{@link Ada.FormalObjectDeclaration#getAspectSpecificationsQl <em>Aspect Specifications Ql</em>}</li>
 *   <li>{@link Ada.FormalObjectDeclaration#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.FormalObjectDeclaration#getMode <em>Mode</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getFormalObjectDeclaration()
 * @model extendedMetaData="name='Formal_Object_Declaration' kind='elementOnly'"
 * @generated
 */
public interface FormalObjectDeclaration extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getFormalObjectDeclaration_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.FormalObjectDeclaration#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Names Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Names Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Names Ql</em>' containment reference.
	 * @see #setNamesQl(DefiningNameList)
	 * @see Ada.AdaPackage#getFormalObjectDeclaration_NamesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='names_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameList getNamesQl();

	/**
	 * Sets the value of the '{@link Ada.FormalObjectDeclaration#getNamesQl <em>Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Names Ql</em>' containment reference.
	 * @see #getNamesQl()
	 * @generated
	 */
	void setNamesQl(DefiningNameList value);

	/**
	 * Returns the value of the '<em><b>Has Null Exclusion Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Null Exclusion Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Null Exclusion Q</em>' containment reference.
	 * @see #setHasNullExclusionQ(HasNullExclusionQType22)
	 * @see Ada.AdaPackage#getFormalObjectDeclaration_HasNullExclusionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='has_null_exclusion_q' namespace='##targetNamespace'"
	 * @generated
	 */
	HasNullExclusionQType22 getHasNullExclusionQ();

	/**
	 * Sets the value of the '{@link Ada.FormalObjectDeclaration#getHasNullExclusionQ <em>Has Null Exclusion Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Null Exclusion Q</em>' containment reference.
	 * @see #getHasNullExclusionQ()
	 * @generated
	 */
	void setHasNullExclusionQ(HasNullExclusionQType22 value);

	/**
	 * Returns the value of the '<em><b>Object Declaration View Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Declaration View Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Declaration View Q</em>' containment reference.
	 * @see #setObjectDeclarationViewQ(DefinitionClass)
	 * @see Ada.AdaPackage#getFormalObjectDeclaration_ObjectDeclarationViewQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='object_declaration_view_q' namespace='##targetNamespace'"
	 * @generated
	 */
	DefinitionClass getObjectDeclarationViewQ();

	/**
	 * Sets the value of the '{@link Ada.FormalObjectDeclaration#getObjectDeclarationViewQ <em>Object Declaration View Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Declaration View Q</em>' containment reference.
	 * @see #getObjectDeclarationViewQ()
	 * @generated
	 */
	void setObjectDeclarationViewQ(DefinitionClass value);

	/**
	 * Returns the value of the '<em><b>Initialization Expression Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initialization Expression Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initialization Expression Q</em>' containment reference.
	 * @see #setInitializationExpressionQ(ExpressionClass)
	 * @see Ada.AdaPackage#getFormalObjectDeclaration_InitializationExpressionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='initialization_expression_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getInitializationExpressionQ();

	/**
	 * Sets the value of the '{@link Ada.FormalObjectDeclaration#getInitializationExpressionQ <em>Initialization Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initialization Expression Q</em>' containment reference.
	 * @see #getInitializationExpressionQ()
	 * @generated
	 */
	void setInitializationExpressionQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Aspect Specifications Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aspect Specifications Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aspect Specifications Ql</em>' containment reference.
	 * @see #setAspectSpecificationsQl(ElementList)
	 * @see Ada.AdaPackage#getFormalObjectDeclaration_AspectSpecificationsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='aspect_specifications_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementList getAspectSpecificationsQl();

	/**
	 * Sets the value of the '{@link Ada.FormalObjectDeclaration#getAspectSpecificationsQl <em>Aspect Specifications Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aspect Specifications Ql</em>' containment reference.
	 * @see #getAspectSpecificationsQl()
	 * @generated
	 */
	void setAspectSpecificationsQl(ElementList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getFormalObjectDeclaration_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.FormalObjectDeclaration#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

	/**
	 * Returns the value of the '<em><b>Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mode</em>' attribute.
	 * @see #setMode(String)
	 * @see Ada.AdaPackage#getFormalObjectDeclaration_Mode()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='mode' namespace='##targetNamespace'"
	 * @generated
	 */
	String getMode();

	/**
	 * Sets the value of the '{@link Ada.FormalObjectDeclaration#getMode <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mode</em>' attribute.
	 * @see #getMode()
	 * @generated
	 */
	void setMode(String value);

} // FormalObjectDeclaration
