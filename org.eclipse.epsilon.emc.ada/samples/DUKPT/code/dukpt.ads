--------------------------------------------------------------------
-- Copyright <<Organisation owning copyright>>
-- <<Project Name>>
--
-- File:
--    $Id: dukpt.ads 2379 2017-02-28 10:02:48Z aturner@EUROPE $
--
-- Description:
--   ...
--------------------------------------------------------------------
--------------------------------------------------------------------
-- Package DUKPT
--
-- Description:
--
-- Possible Improvements:
--    Have a state record to hold/init all the state variables.
--------------------------------------------------------------------
with Crypto.Types;
with Interfaces;

package DUKPT is

   use Crypto.Types;
   use Interfaces;

   --------------------------------------------------------------------
   -- DUKPT Types
   -- Types derived from ANS X9.24-1:2009 Section A.1

   -- Future Key Registers
   type B_Block136 is private;
   type Future_Key_Register_Type is private;

   -- Inital Key Serial Number
   type B_Block80 is private;

   -- For Shift Register
   type Natural_0_20 is private;
   type Natural_0_21 is private;

   --------------------------------------------------------------------
   -- Description:
   --    This procedure performs a non-reversible transformation on the
   --    IKSN using the BSK as the key. The result is the IPEK.
   --
   --    See ANS X9.24-1:2009 Section A.6
   --
   --    This subprogram initialises no state so that it can be re-used
   --    from the server side.
   --------------------------------------------------------------------
   procedure Derive_IPEK(BDK  : in     Crypto.Types.B_Block128;
                         IKSN : in     Crypto.Types.B_Block128;
                         IPEK :    out Crypto.Types.B_Block128);

   --------------------------------------------------------------------
   -- Description:
   --    This procedure fill the FKR with derived keys using the
   --    non-reversible transformation process. The FKR is hel in DUKPT
   --    state. There are no outputs. The IPEK is not included in
   --    the initialised FKR so it is not used as a transaction key.
   --------------------------------------------------------------------
   procedure Load_Initial_Keys(IPEK : in     Crypto.Types.B_Block128;
                               IKSN : in     Crypto.Types.B_Block128);


   --------------------------------------------------------------------
   -- Description:
   --    This procedure gets the next transaction key from the FKR and
   --    increments the EC. The KSN returned has the EC portion updated
   --    so that the server side can retireive the correct transaction key.
   --    The transaction key is further derived into the Encrypt_Key
   --    and the MAC_Key for use in encryption and a keyed hash signature.
   --------------------------------------------------------------------
   procedure Get_Sender_Transaction_Keys(KSN         :    out Crypto.Types.B_Block128;
                                         Encrypt_Key :    out Crypto.Types.B_Block128;
                                         MAC_Key     :    out Crypto.Types.B_Block128);

   --------------------------------------------------------------------
   -- Description:
   --    This is the stateless server side procedure for reteiving the
   --    transaction key and derived Encrypt_Key and MAC_Key. It requires
   --    the IPEK for the device, which is obtained by using the procedure
   --    Derive_IPEK(). The same received KSN must be used by both procedures
   --    as the EC portion must match.
   --------------------------------------------------------------------
   procedure Retrieve_Transaction_Keys(IPEK        : in      Crypto.Types.B_Block128;
                                       KSN         : in      Crypto.Types.B_Block128;
                                       Encrypt_Key :     out Crypto.Types.B_Block128;
                                       MAC_Key     :     out Crypto.Types.B_Block128);

private
   --------------------------------------------------------------------
   -- DUKPT Private Types
   -- Types derived from ANS X9.24-1:2009 Section A.1

   -- Future Key Registers
   subtype B_Block136_Range is Natural range 0..16;
   type B_Block136 is array (B_Block136_Range) of Byte;
   type Future_Key_Register_Type is array (0 .. 20) of B_Block136;

   -- Inital Key Serial Number
   subtype B_Block80_Range is Natural range 0..9;
   type B_Block80 is array (B_Block80_Range) of Byte;

   -- For Shift Register
   type Natural_0_20 is new Natural range 0 .. 20;
   type Natural_0_21 is new Natural range 0 .. 21;

   --------------------------------------------------------------------
   -- DUKPT State
   --    An option is to change to stateless package by moving it to the
   --    application layer. Burden is then on the user - not as clean.
   EC       : Unsigned_32;             -- Encryption Counter
   SR       : Unsigned_32;             -- Shift Register
   KR       : Crypto.Types.B_Block128; -- Key Register
   CKP      : Natural_0_20;            -- Current Key Pointer
   FKR      : Future_Key_Register_Type;-- Future Key Register
   KSNR     : Crypto.Types.B_Block128; -- Container for IKSN and EC concat

   --------------------------------------------------------------------
   -- DUKPT Constants
   MAC_XOR     : constant Crypto.Types.B_Block128 := (16#00#, 16#00#, 16#00#, 16#00#,
                                                      16#00#, 16#00#, 16#FF#, 16#00#,
                                                      16#00#, 16#00#, 16#00#, 16#00#,
                                                      16#00#, 16#00#, 16#FF#, 16#00#);
   ENCRYPT_XOR : constant Crypto.Types.B_Block128 := (16#00#, 16#00#, 16#00#, 16#00#,
                                                      16#00#, 16#FF#, 16#00#, 16#00#,
                                                      16#00#, 16#00#, 16#00#, 16#00#,
                                                      16#00#, 16#FF#, 16#00#, 16#00#);

   --------------------------------------------------------------------
   -- Description:
   --    This is a stateless version of Load_Initial_Keys().
   --    The stateless version is usful for state control.
   --------------------------------------------------------------------
   procedure Load_Initial_Keys_Stateless(IPEK      : in     Crypto.Types.B_Block128;
                                         IKSN      : in     Crypto.Types.B_Block128;
                                         EC_Temp   :    out Interfaces.Unsigned_32;
                                         KSNR_Temp :    out Crypto.Types.B_Block128;
                                         FKR_Temp  :    out Future_Key_Register_Type);



   --------------------------------------------------------------------
   -- Description:
   --    This routine provides a performance optimisation as described
   --    in ANS X9.24-1A.3. This optimisation works when the Encryption
   --    Counter size has an odd number of bits (the example in the ANSI
   --    standard uses a 21 bit EC).
   --------------------------------------------------------------------
   procedure New_Key(CKP_Temp  : in     Natural_0_20;
                     EC_Temp   : in out Interfaces.Unsigned_32;
                     KSNR_Temp : in out Crypto.Types.B_Block128;
                     SR_Temp   : in     Interfaces.Unsigned_32;
                     FKR_Temp  : in out Future_Key_Register_Type);

   --------------------------------------------------------------------
   -- Description:
   --    After a key is used or during initial key generation, this
   --    routine adds additional keys to the future key registers.
   --------------------------------------------------------------------
   procedure New_Key_3(CKP_Temp  : in     Natural_0_20;
                       EC_Temp   : in out Interfaces.Unsigned_32;
                       KSNR_Temp : in out Crypto.Types.B_Block128;
                       SR_Temp   : in     Interfaces.Unsigned_32;
                       FKR_Temp  : in out Future_Key_Register_Type);

   --------------------------------------------------------------------
   -- Description:
   --    This is a stateless version of Get_Sender_Transaction_Keys().
   --    The stateless version is usful for state control.
   --------------------------------------------------------------------
   procedure Get_Sender_Transaction_Keys_Stateless(EC_Temp     : in out Interfaces.Unsigned_32;
                                                   KSNR_Temp   : in out Crypto.Types.B_Block128;
                                                   FKR_Temp    : in out Future_Key_Register_Type;
                                                   Encrypt_Key : out    Crypto.Types.B_Block128;
                                                   MAC_Key     : out    Crypto.Types.B_Block128);



   --------------------------------------------------------------------
   -- Description:
   --    This routine picks the next transaction key from the Future
   --    Key Registers, which is further transformed. Then a new
   --    transaction key is generated and added to the Future Key Register.
   --------------------------------------------------------------------
   procedure Derive_Keys_From_Current_Key(CKP_Temp    : in     Natural_0_20;
                                          EC_Temp     : in out Interfaces.Unsigned_32;
                                          KSNR_Temp   : in out Crypto.Types.B_Block128;
                                          SR_Temp     : in     Interfaces.Unsigned_32;
                                          FKR_Temp    : in out Future_Key_Register_Type;
                                          Encrypt_Key : out    Crypto.Types.B_Block128;
                                          MAC_Key     : out    Crypto.Types.B_Block128);

   --------------------------------------------------------------------
   -- Description:
   --    It is important that transaction keys are used in a specific order


   --    to the index of the next future key register to use.
   --------------------------------------------------------------------
   procedure Set_Bit(EC_Temp       : in      Interfaces.Unsigned_32;
                     SR_Temp       :     out Interfaces.Unsigned_32;
                     Set_Bit_Index :     out Natural_0_20);

   --------------------------------------------------------------------
   -- Description:
   --    This routine can be any non-reversible process to generate keys
   --    of the desired length.
   --    The steps here describe how a block cipher encryption protocol

   --------------------------------------------------------------------
   procedure Non_Reversible_Key_Generation_Process(Key     : in     Crypto.Types.B_Block128;
                                                   Subject : in out Crypto.Types.B_Block128);



end DUKPT;
