/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Discrete Subtype Indication</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.DiscreteSubtypeIndication#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.DiscreteSubtypeIndication#getSubtypeMarkQ <em>Subtype Mark Q</em>}</li>
 *   <li>{@link Ada.DiscreteSubtypeIndication#getSubtypeConstraintQ <em>Subtype Constraint Q</em>}</li>
 *   <li>{@link Ada.DiscreteSubtypeIndication#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getDiscreteSubtypeIndication()
 * @model extendedMetaData="name='Discrete_Subtype_Indication' kind='elementOnly'"
 * @generated
 */
public interface DiscreteSubtypeIndication extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getDiscreteSubtypeIndication_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.DiscreteSubtypeIndication#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Subtype Mark Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subtype Mark Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subtype Mark Q</em>' containment reference.
	 * @see #setSubtypeMarkQ(ExpressionClass)
	 * @see Ada.AdaPackage#getDiscreteSubtypeIndication_SubtypeMarkQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='subtype_mark_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getSubtypeMarkQ();

	/**
	 * Sets the value of the '{@link Ada.DiscreteSubtypeIndication#getSubtypeMarkQ <em>Subtype Mark Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Subtype Mark Q</em>' containment reference.
	 * @see #getSubtypeMarkQ()
	 * @generated
	 */
	void setSubtypeMarkQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Subtype Constraint Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subtype Constraint Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subtype Constraint Q</em>' containment reference.
	 * @see #setSubtypeConstraintQ(ConstraintClass)
	 * @see Ada.AdaPackage#getDiscreteSubtypeIndication_SubtypeConstraintQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='subtype_constraint_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ConstraintClass getSubtypeConstraintQ();

	/**
	 * Sets the value of the '{@link Ada.DiscreteSubtypeIndication#getSubtypeConstraintQ <em>Subtype Constraint Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Subtype Constraint Q</em>' containment reference.
	 * @see #getSubtypeConstraintQ()
	 * @generated
	 */
	void setSubtypeConstraintQ(ConstraintClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getDiscreteSubtypeIndication_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.DiscreteSubtypeIndication#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // DiscreteSubtypeIndication
