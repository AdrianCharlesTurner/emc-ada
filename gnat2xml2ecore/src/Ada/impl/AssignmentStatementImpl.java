/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.AssignmentStatement;
import Ada.DefiningNameList;
import Ada.ExpressionClass;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Assignment Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.AssignmentStatementImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.AssignmentStatementImpl#getLabelNamesQl <em>Label Names Ql</em>}</li>
 *   <li>{@link Ada.impl.AssignmentStatementImpl#getAssignmentVariableNameQ <em>Assignment Variable Name Q</em>}</li>
 *   <li>{@link Ada.impl.AssignmentStatementImpl#getAssignmentExpressionQ <em>Assignment Expression Q</em>}</li>
 *   <li>{@link Ada.impl.AssignmentStatementImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AssignmentStatementImpl extends MinimalEObjectImpl.Container implements AssignmentStatement {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getLabelNamesQl() <em>Label Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabelNamesQl()
	 * @generated
	 * @ordered
	 */
	protected DefiningNameList labelNamesQl;

	/**
	 * The cached value of the '{@link #getAssignmentVariableNameQ() <em>Assignment Variable Name Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssignmentVariableNameQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass assignmentVariableNameQ;

	/**
	 * The cached value of the '{@link #getAssignmentExpressionQ() <em>Assignment Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssignmentExpressionQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass assignmentExpressionQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssignmentStatementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getAssignmentStatement();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSIGNMENT_STATEMENT__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSIGNMENT_STATEMENT__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSIGNMENT_STATEMENT__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSIGNMENT_STATEMENT__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNameList getLabelNamesQl() {
		return labelNamesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLabelNamesQl(DefiningNameList newLabelNamesQl, NotificationChain msgs) {
		DefiningNameList oldLabelNamesQl = labelNamesQl;
		labelNamesQl = newLabelNamesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSIGNMENT_STATEMENT__LABEL_NAMES_QL, oldLabelNamesQl, newLabelNamesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabelNamesQl(DefiningNameList newLabelNamesQl) {
		if (newLabelNamesQl != labelNamesQl) {
			NotificationChain msgs = null;
			if (labelNamesQl != null)
				msgs = ((InternalEObject)labelNamesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSIGNMENT_STATEMENT__LABEL_NAMES_QL, null, msgs);
			if (newLabelNamesQl != null)
				msgs = ((InternalEObject)newLabelNamesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSIGNMENT_STATEMENT__LABEL_NAMES_QL, null, msgs);
			msgs = basicSetLabelNamesQl(newLabelNamesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSIGNMENT_STATEMENT__LABEL_NAMES_QL, newLabelNamesQl, newLabelNamesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getAssignmentVariableNameQ() {
		return assignmentVariableNameQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssignmentVariableNameQ(ExpressionClass newAssignmentVariableNameQ, NotificationChain msgs) {
		ExpressionClass oldAssignmentVariableNameQ = assignmentVariableNameQ;
		assignmentVariableNameQ = newAssignmentVariableNameQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSIGNMENT_STATEMENT__ASSIGNMENT_VARIABLE_NAME_Q, oldAssignmentVariableNameQ, newAssignmentVariableNameQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssignmentVariableNameQ(ExpressionClass newAssignmentVariableNameQ) {
		if (newAssignmentVariableNameQ != assignmentVariableNameQ) {
			NotificationChain msgs = null;
			if (assignmentVariableNameQ != null)
				msgs = ((InternalEObject)assignmentVariableNameQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSIGNMENT_STATEMENT__ASSIGNMENT_VARIABLE_NAME_Q, null, msgs);
			if (newAssignmentVariableNameQ != null)
				msgs = ((InternalEObject)newAssignmentVariableNameQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSIGNMENT_STATEMENT__ASSIGNMENT_VARIABLE_NAME_Q, null, msgs);
			msgs = basicSetAssignmentVariableNameQ(newAssignmentVariableNameQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSIGNMENT_STATEMENT__ASSIGNMENT_VARIABLE_NAME_Q, newAssignmentVariableNameQ, newAssignmentVariableNameQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getAssignmentExpressionQ() {
		return assignmentExpressionQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssignmentExpressionQ(ExpressionClass newAssignmentExpressionQ, NotificationChain msgs) {
		ExpressionClass oldAssignmentExpressionQ = assignmentExpressionQ;
		assignmentExpressionQ = newAssignmentExpressionQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSIGNMENT_STATEMENT__ASSIGNMENT_EXPRESSION_Q, oldAssignmentExpressionQ, newAssignmentExpressionQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssignmentExpressionQ(ExpressionClass newAssignmentExpressionQ) {
		if (newAssignmentExpressionQ != assignmentExpressionQ) {
			NotificationChain msgs = null;
			if (assignmentExpressionQ != null)
				msgs = ((InternalEObject)assignmentExpressionQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSIGNMENT_STATEMENT__ASSIGNMENT_EXPRESSION_Q, null, msgs);
			if (newAssignmentExpressionQ != null)
				msgs = ((InternalEObject)newAssignmentExpressionQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSIGNMENT_STATEMENT__ASSIGNMENT_EXPRESSION_Q, null, msgs);
			msgs = basicSetAssignmentExpressionQ(newAssignmentExpressionQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSIGNMENT_STATEMENT__ASSIGNMENT_EXPRESSION_Q, newAssignmentExpressionQ, newAssignmentExpressionQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSIGNMENT_STATEMENT__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.ASSIGNMENT_STATEMENT__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.ASSIGNMENT_STATEMENT__LABEL_NAMES_QL:
				return basicSetLabelNamesQl(null, msgs);
			case AdaPackage.ASSIGNMENT_STATEMENT__ASSIGNMENT_VARIABLE_NAME_Q:
				return basicSetAssignmentVariableNameQ(null, msgs);
			case AdaPackage.ASSIGNMENT_STATEMENT__ASSIGNMENT_EXPRESSION_Q:
				return basicSetAssignmentExpressionQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.ASSIGNMENT_STATEMENT__SLOC:
				return getSloc();
			case AdaPackage.ASSIGNMENT_STATEMENT__LABEL_NAMES_QL:
				return getLabelNamesQl();
			case AdaPackage.ASSIGNMENT_STATEMENT__ASSIGNMENT_VARIABLE_NAME_Q:
				return getAssignmentVariableNameQ();
			case AdaPackage.ASSIGNMENT_STATEMENT__ASSIGNMENT_EXPRESSION_Q:
				return getAssignmentExpressionQ();
			case AdaPackage.ASSIGNMENT_STATEMENT__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.ASSIGNMENT_STATEMENT__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.ASSIGNMENT_STATEMENT__LABEL_NAMES_QL:
				setLabelNamesQl((DefiningNameList)newValue);
				return;
			case AdaPackage.ASSIGNMENT_STATEMENT__ASSIGNMENT_VARIABLE_NAME_Q:
				setAssignmentVariableNameQ((ExpressionClass)newValue);
				return;
			case AdaPackage.ASSIGNMENT_STATEMENT__ASSIGNMENT_EXPRESSION_Q:
				setAssignmentExpressionQ((ExpressionClass)newValue);
				return;
			case AdaPackage.ASSIGNMENT_STATEMENT__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.ASSIGNMENT_STATEMENT__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.ASSIGNMENT_STATEMENT__LABEL_NAMES_QL:
				setLabelNamesQl((DefiningNameList)null);
				return;
			case AdaPackage.ASSIGNMENT_STATEMENT__ASSIGNMENT_VARIABLE_NAME_Q:
				setAssignmentVariableNameQ((ExpressionClass)null);
				return;
			case AdaPackage.ASSIGNMENT_STATEMENT__ASSIGNMENT_EXPRESSION_Q:
				setAssignmentExpressionQ((ExpressionClass)null);
				return;
			case AdaPackage.ASSIGNMENT_STATEMENT__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.ASSIGNMENT_STATEMENT__SLOC:
				return sloc != null;
			case AdaPackage.ASSIGNMENT_STATEMENT__LABEL_NAMES_QL:
				return labelNamesQl != null;
			case AdaPackage.ASSIGNMENT_STATEMENT__ASSIGNMENT_VARIABLE_NAME_Q:
				return assignmentVariableNameQ != null;
			case AdaPackage.ASSIGNMENT_STATEMENT__ASSIGNMENT_EXPRESSION_Q:
				return assignmentExpressionQ != null;
			case AdaPackage.ASSIGNMENT_STATEMENT__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //AssignmentStatementImpl
