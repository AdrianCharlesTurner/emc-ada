/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aspect Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.AspectSpecification#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.AspectSpecification#getAspectMarkQ <em>Aspect Mark Q</em>}</li>
 *   <li>{@link Ada.AspectSpecification#getAspectDefinitionQ <em>Aspect Definition Q</em>}</li>
 *   <li>{@link Ada.AspectSpecification#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getAspectSpecification()
 * @model extendedMetaData="name='Aspect_Specification' kind='elementOnly'"
 * @generated
 */
public interface AspectSpecification extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getAspectSpecification_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.AspectSpecification#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Aspect Mark Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aspect Mark Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aspect Mark Q</em>' containment reference.
	 * @see #setAspectMarkQ(ElementClass)
	 * @see Ada.AdaPackage#getAspectSpecification_AspectMarkQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='aspect_mark_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementClass getAspectMarkQ();

	/**
	 * Sets the value of the '{@link Ada.AspectSpecification#getAspectMarkQ <em>Aspect Mark Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aspect Mark Q</em>' containment reference.
	 * @see #getAspectMarkQ()
	 * @generated
	 */
	void setAspectMarkQ(ElementClass value);

	/**
	 * Returns the value of the '<em><b>Aspect Definition Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aspect Definition Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aspect Definition Q</em>' containment reference.
	 * @see #setAspectDefinitionQ(ElementClass)
	 * @see Ada.AdaPackage#getAspectSpecification_AspectDefinitionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='aspect_definition_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementClass getAspectDefinitionQ();

	/**
	 * Sets the value of the '{@link Ada.AspectSpecification#getAspectDefinitionQ <em>Aspect Definition Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aspect Definition Q</em>' containment reference.
	 * @see #getAspectDefinitionQ()
	 * @generated
	 */
	void setAspectDefinitionQ(ElementClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getAspectSpecification_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.AspectSpecification#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // AspectSpecification
