/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Formal Pool Specific Access To Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.FormalPoolSpecificAccessToVariable#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.FormalPoolSpecificAccessToVariable#getHasNullExclusionQ <em>Has Null Exclusion Q</em>}</li>
 *   <li>{@link Ada.FormalPoolSpecificAccessToVariable#getAccessToObjectDefinitionQ <em>Access To Object Definition Q</em>}</li>
 *   <li>{@link Ada.FormalPoolSpecificAccessToVariable#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getFormalPoolSpecificAccessToVariable()
 * @model extendedMetaData="name='Formal_Pool_Specific_Access_To_Variable' kind='elementOnly'"
 * @generated
 */
public interface FormalPoolSpecificAccessToVariable extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getFormalPoolSpecificAccessToVariable_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.FormalPoolSpecificAccessToVariable#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Has Null Exclusion Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Null Exclusion Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Null Exclusion Q</em>' containment reference.
	 * @see #setHasNullExclusionQ(HasNullExclusionQType14)
	 * @see Ada.AdaPackage#getFormalPoolSpecificAccessToVariable_HasNullExclusionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='has_null_exclusion_q' namespace='##targetNamespace'"
	 * @generated
	 */
	HasNullExclusionQType14 getHasNullExclusionQ();

	/**
	 * Sets the value of the '{@link Ada.FormalPoolSpecificAccessToVariable#getHasNullExclusionQ <em>Has Null Exclusion Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Null Exclusion Q</em>' containment reference.
	 * @see #getHasNullExclusionQ()
	 * @generated
	 */
	void setHasNullExclusionQ(HasNullExclusionQType14 value);

	/**
	 * Returns the value of the '<em><b>Access To Object Definition Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Object Definition Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Object Definition Q</em>' containment reference.
	 * @see #setAccessToObjectDefinitionQ(ElementClass)
	 * @see Ada.AdaPackage#getFormalPoolSpecificAccessToVariable_AccessToObjectDefinitionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='access_to_object_definition_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementClass getAccessToObjectDefinitionQ();

	/**
	 * Sets the value of the '{@link Ada.FormalPoolSpecificAccessToVariable#getAccessToObjectDefinitionQ <em>Access To Object Definition Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access To Object Definition Q</em>' containment reference.
	 * @see #getAccessToObjectDefinitionQ()
	 * @generated
	 */
	void setAccessToObjectDefinitionQ(ElementClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getFormalPoolSpecificAccessToVariable_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.FormalPoolSpecificAccessToVariable#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // FormalPoolSpecificAccessToVariable
