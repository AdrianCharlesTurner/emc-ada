/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Procedure Body Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.ProcedureBodyDeclaration#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.ProcedureBodyDeclaration#getIsOverridingDeclarationQ <em>Is Overriding Declaration Q</em>}</li>
 *   <li>{@link Ada.ProcedureBodyDeclaration#getIsNotOverridingDeclarationQ <em>Is Not Overriding Declaration Q</em>}</li>
 *   <li>{@link Ada.ProcedureBodyDeclaration#getNamesQl <em>Names Ql</em>}</li>
 *   <li>{@link Ada.ProcedureBodyDeclaration#getParameterProfileQl <em>Parameter Profile Ql</em>}</li>
 *   <li>{@link Ada.ProcedureBodyDeclaration#getAspectSpecificationsQl <em>Aspect Specifications Ql</em>}</li>
 *   <li>{@link Ada.ProcedureBodyDeclaration#getBodyDeclarativeItemsQl <em>Body Declarative Items Ql</em>}</li>
 *   <li>{@link Ada.ProcedureBodyDeclaration#getBodyStatementsQl <em>Body Statements Ql</em>}</li>
 *   <li>{@link Ada.ProcedureBodyDeclaration#getBodyExceptionHandlersQl <em>Body Exception Handlers Ql</em>}</li>
 *   <li>{@link Ada.ProcedureBodyDeclaration#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getProcedureBodyDeclaration()
 * @model extendedMetaData="name='Procedure_Body_Declaration' kind='elementOnly'"
 * @generated
 */
public interface ProcedureBodyDeclaration extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getProcedureBodyDeclaration_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.ProcedureBodyDeclaration#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Is Overriding Declaration Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Overriding Declaration Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Overriding Declaration Q</em>' containment reference.
	 * @see #setIsOverridingDeclarationQ(IsOverridingDeclarationQType3)
	 * @see Ada.AdaPackage#getProcedureBodyDeclaration_IsOverridingDeclarationQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='is_overriding_declaration_q' namespace='##targetNamespace'"
	 * @generated
	 */
	IsOverridingDeclarationQType3 getIsOverridingDeclarationQ();

	/**
	 * Sets the value of the '{@link Ada.ProcedureBodyDeclaration#getIsOverridingDeclarationQ <em>Is Overriding Declaration Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Overriding Declaration Q</em>' containment reference.
	 * @see #getIsOverridingDeclarationQ()
	 * @generated
	 */
	void setIsOverridingDeclarationQ(IsOverridingDeclarationQType3 value);

	/**
	 * Returns the value of the '<em><b>Is Not Overriding Declaration Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Not Overriding Declaration Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Not Overriding Declaration Q</em>' containment reference.
	 * @see #setIsNotOverridingDeclarationQ(IsNotOverridingDeclarationQType1)
	 * @see Ada.AdaPackage#getProcedureBodyDeclaration_IsNotOverridingDeclarationQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='is_not_overriding_declaration_q' namespace='##targetNamespace'"
	 * @generated
	 */
	IsNotOverridingDeclarationQType1 getIsNotOverridingDeclarationQ();

	/**
	 * Sets the value of the '{@link Ada.ProcedureBodyDeclaration#getIsNotOverridingDeclarationQ <em>Is Not Overriding Declaration Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Not Overriding Declaration Q</em>' containment reference.
	 * @see #getIsNotOverridingDeclarationQ()
	 * @generated
	 */
	void setIsNotOverridingDeclarationQ(IsNotOverridingDeclarationQType1 value);

	/**
	 * Returns the value of the '<em><b>Names Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Names Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Names Ql</em>' containment reference.
	 * @see #setNamesQl(DefiningNameList)
	 * @see Ada.AdaPackage#getProcedureBodyDeclaration_NamesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='names_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameList getNamesQl();

	/**
	 * Sets the value of the '{@link Ada.ProcedureBodyDeclaration#getNamesQl <em>Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Names Ql</em>' containment reference.
	 * @see #getNamesQl()
	 * @generated
	 */
	void setNamesQl(DefiningNameList value);

	/**
	 * Returns the value of the '<em><b>Parameter Profile Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Profile Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Profile Ql</em>' containment reference.
	 * @see #setParameterProfileQl(ParameterSpecificationList)
	 * @see Ada.AdaPackage#getProcedureBodyDeclaration_ParameterProfileQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='parameter_profile_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ParameterSpecificationList getParameterProfileQl();

	/**
	 * Sets the value of the '{@link Ada.ProcedureBodyDeclaration#getParameterProfileQl <em>Parameter Profile Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Profile Ql</em>' containment reference.
	 * @see #getParameterProfileQl()
	 * @generated
	 */
	void setParameterProfileQl(ParameterSpecificationList value);

	/**
	 * Returns the value of the '<em><b>Aspect Specifications Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aspect Specifications Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aspect Specifications Ql</em>' containment reference.
	 * @see #setAspectSpecificationsQl(ElementList)
	 * @see Ada.AdaPackage#getProcedureBodyDeclaration_AspectSpecificationsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='aspect_specifications_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementList getAspectSpecificationsQl();

	/**
	 * Sets the value of the '{@link Ada.ProcedureBodyDeclaration#getAspectSpecificationsQl <em>Aspect Specifications Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aspect Specifications Ql</em>' containment reference.
	 * @see #getAspectSpecificationsQl()
	 * @generated
	 */
	void setAspectSpecificationsQl(ElementList value);

	/**
	 * Returns the value of the '<em><b>Body Declarative Items Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body Declarative Items Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body Declarative Items Ql</em>' containment reference.
	 * @see #setBodyDeclarativeItemsQl(ElementList)
	 * @see Ada.AdaPackage#getProcedureBodyDeclaration_BodyDeclarativeItemsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='body_declarative_items_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementList getBodyDeclarativeItemsQl();

	/**
	 * Sets the value of the '{@link Ada.ProcedureBodyDeclaration#getBodyDeclarativeItemsQl <em>Body Declarative Items Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body Declarative Items Ql</em>' containment reference.
	 * @see #getBodyDeclarativeItemsQl()
	 * @generated
	 */
	void setBodyDeclarativeItemsQl(ElementList value);

	/**
	 * Returns the value of the '<em><b>Body Statements Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body Statements Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body Statements Ql</em>' containment reference.
	 * @see #setBodyStatementsQl(StatementList)
	 * @see Ada.AdaPackage#getProcedureBodyDeclaration_BodyStatementsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='body_statements_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	StatementList getBodyStatementsQl();

	/**
	 * Sets the value of the '{@link Ada.ProcedureBodyDeclaration#getBodyStatementsQl <em>Body Statements Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body Statements Ql</em>' containment reference.
	 * @see #getBodyStatementsQl()
	 * @generated
	 */
	void setBodyStatementsQl(StatementList value);

	/**
	 * Returns the value of the '<em><b>Body Exception Handlers Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body Exception Handlers Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body Exception Handlers Ql</em>' containment reference.
	 * @see #setBodyExceptionHandlersQl(ExceptionHandlerList)
	 * @see Ada.AdaPackage#getProcedureBodyDeclaration_BodyExceptionHandlersQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='body_exception_handlers_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ExceptionHandlerList getBodyExceptionHandlersQl();

	/**
	 * Sets the value of the '{@link Ada.ProcedureBodyDeclaration#getBodyExceptionHandlersQl <em>Body Exception Handlers Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body Exception Handlers Ql</em>' containment reference.
	 * @see #getBodyExceptionHandlersQl()
	 * @generated
	 */
	void setBodyExceptionHandlersQl(ExceptionHandlerList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getProcedureBodyDeclaration_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.ProcedureBodyDeclaration#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // ProcedureBodyDeclaration
