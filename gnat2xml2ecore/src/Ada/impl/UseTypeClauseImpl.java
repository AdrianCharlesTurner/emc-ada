/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.NameList;
import Ada.SourceLocation;
import Ada.UseTypeClause;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Use Type Clause</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.UseTypeClauseImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.UseTypeClauseImpl#getClauseNamesQl <em>Clause Names Ql</em>}</li>
 *   <li>{@link Ada.impl.UseTypeClauseImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UseTypeClauseImpl extends MinimalEObjectImpl.Container implements UseTypeClause {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getClauseNamesQl() <em>Clause Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClauseNamesQl()
	 * @generated
	 * @ordered
	 */
	protected NameList clauseNamesQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UseTypeClauseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getUseTypeClause();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.USE_TYPE_CLAUSE__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.USE_TYPE_CLAUSE__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.USE_TYPE_CLAUSE__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.USE_TYPE_CLAUSE__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NameList getClauseNamesQl() {
		return clauseNamesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClauseNamesQl(NameList newClauseNamesQl, NotificationChain msgs) {
		NameList oldClauseNamesQl = clauseNamesQl;
		clauseNamesQl = newClauseNamesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.USE_TYPE_CLAUSE__CLAUSE_NAMES_QL, oldClauseNamesQl, newClauseNamesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClauseNamesQl(NameList newClauseNamesQl) {
		if (newClauseNamesQl != clauseNamesQl) {
			NotificationChain msgs = null;
			if (clauseNamesQl != null)
				msgs = ((InternalEObject)clauseNamesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.USE_TYPE_CLAUSE__CLAUSE_NAMES_QL, null, msgs);
			if (newClauseNamesQl != null)
				msgs = ((InternalEObject)newClauseNamesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.USE_TYPE_CLAUSE__CLAUSE_NAMES_QL, null, msgs);
			msgs = basicSetClauseNamesQl(newClauseNamesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.USE_TYPE_CLAUSE__CLAUSE_NAMES_QL, newClauseNamesQl, newClauseNamesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.USE_TYPE_CLAUSE__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.USE_TYPE_CLAUSE__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.USE_TYPE_CLAUSE__CLAUSE_NAMES_QL:
				return basicSetClauseNamesQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.USE_TYPE_CLAUSE__SLOC:
				return getSloc();
			case AdaPackage.USE_TYPE_CLAUSE__CLAUSE_NAMES_QL:
				return getClauseNamesQl();
			case AdaPackage.USE_TYPE_CLAUSE__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.USE_TYPE_CLAUSE__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.USE_TYPE_CLAUSE__CLAUSE_NAMES_QL:
				setClauseNamesQl((NameList)newValue);
				return;
			case AdaPackage.USE_TYPE_CLAUSE__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.USE_TYPE_CLAUSE__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.USE_TYPE_CLAUSE__CLAUSE_NAMES_QL:
				setClauseNamesQl((NameList)null);
				return;
			case AdaPackage.USE_TYPE_CLAUSE__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.USE_TYPE_CLAUSE__SLOC:
				return sloc != null;
			case AdaPackage.USE_TYPE_CLAUSE__CLAUSE_NAMES_QL:
				return clauseNamesQl != null;
			case AdaPackage.USE_TYPE_CLAUSE__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //UseTypeClauseImpl
