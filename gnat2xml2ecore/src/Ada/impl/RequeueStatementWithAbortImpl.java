/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.DefiningNameList;
import Ada.NameClass;
import Ada.RequeueStatementWithAbort;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Requeue Statement With Abort</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.RequeueStatementWithAbortImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.RequeueStatementWithAbortImpl#getLabelNamesQl <em>Label Names Ql</em>}</li>
 *   <li>{@link Ada.impl.RequeueStatementWithAbortImpl#getRequeueEntryNameQ <em>Requeue Entry Name Q</em>}</li>
 *   <li>{@link Ada.impl.RequeueStatementWithAbortImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RequeueStatementWithAbortImpl extends MinimalEObjectImpl.Container implements RequeueStatementWithAbort {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getLabelNamesQl() <em>Label Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabelNamesQl()
	 * @generated
	 * @ordered
	 */
	protected DefiningNameList labelNamesQl;

	/**
	 * The cached value of the '{@link #getRequeueEntryNameQ() <em>Requeue Entry Name Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequeueEntryNameQ()
	 * @generated
	 * @ordered
	 */
	protected NameClass requeueEntryNameQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequeueStatementWithAbortImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getRequeueStatementWithAbort();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNameList getLabelNamesQl() {
		return labelNamesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLabelNamesQl(DefiningNameList newLabelNamesQl, NotificationChain msgs) {
		DefiningNameList oldLabelNamesQl = labelNamesQl;
		labelNamesQl = newLabelNamesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__LABEL_NAMES_QL, oldLabelNamesQl, newLabelNamesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabelNamesQl(DefiningNameList newLabelNamesQl) {
		if (newLabelNamesQl != labelNamesQl) {
			NotificationChain msgs = null;
			if (labelNamesQl != null)
				msgs = ((InternalEObject)labelNamesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__LABEL_NAMES_QL, null, msgs);
			if (newLabelNamesQl != null)
				msgs = ((InternalEObject)newLabelNamesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__LABEL_NAMES_QL, null, msgs);
			msgs = basicSetLabelNamesQl(newLabelNamesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__LABEL_NAMES_QL, newLabelNamesQl, newLabelNamesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NameClass getRequeueEntryNameQ() {
		return requeueEntryNameQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRequeueEntryNameQ(NameClass newRequeueEntryNameQ, NotificationChain msgs) {
		NameClass oldRequeueEntryNameQ = requeueEntryNameQ;
		requeueEntryNameQ = newRequeueEntryNameQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__REQUEUE_ENTRY_NAME_Q, oldRequeueEntryNameQ, newRequeueEntryNameQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequeueEntryNameQ(NameClass newRequeueEntryNameQ) {
		if (newRequeueEntryNameQ != requeueEntryNameQ) {
			NotificationChain msgs = null;
			if (requeueEntryNameQ != null)
				msgs = ((InternalEObject)requeueEntryNameQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__REQUEUE_ENTRY_NAME_Q, null, msgs);
			if (newRequeueEntryNameQ != null)
				msgs = ((InternalEObject)newRequeueEntryNameQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__REQUEUE_ENTRY_NAME_Q, null, msgs);
			msgs = basicSetRequeueEntryNameQ(newRequeueEntryNameQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__REQUEUE_ENTRY_NAME_Q, newRequeueEntryNameQ, newRequeueEntryNameQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__LABEL_NAMES_QL:
				return basicSetLabelNamesQl(null, msgs);
			case AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__REQUEUE_ENTRY_NAME_Q:
				return basicSetRequeueEntryNameQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__SLOC:
				return getSloc();
			case AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__LABEL_NAMES_QL:
				return getLabelNamesQl();
			case AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__REQUEUE_ENTRY_NAME_Q:
				return getRequeueEntryNameQ();
			case AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__LABEL_NAMES_QL:
				setLabelNamesQl((DefiningNameList)newValue);
				return;
			case AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__REQUEUE_ENTRY_NAME_Q:
				setRequeueEntryNameQ((NameClass)newValue);
				return;
			case AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__LABEL_NAMES_QL:
				setLabelNamesQl((DefiningNameList)null);
				return;
			case AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__REQUEUE_ENTRY_NAME_Q:
				setRequeueEntryNameQ((NameClass)null);
				return;
			case AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__SLOC:
				return sloc != null;
			case AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__LABEL_NAMES_QL:
				return labelNamesQl != null;
			case AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__REQUEUE_ENTRY_NAME_Q:
				return requeueEntryNameQ != null;
			case AdaPackage.REQUEUE_STATEMENT_WITH_ABORT__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //RequeueStatementWithAbortImpl
