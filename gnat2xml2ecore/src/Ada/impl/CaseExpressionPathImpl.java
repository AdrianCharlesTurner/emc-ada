/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.CaseExpressionPath;
import Ada.ElementList;
import Ada.ExpressionClass;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Case Expression Path</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.CaseExpressionPathImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.CaseExpressionPathImpl#getCasePathAlternativeChoicesQl <em>Case Path Alternative Choices Ql</em>}</li>
 *   <li>{@link Ada.impl.CaseExpressionPathImpl#getDependentExpressionQ <em>Dependent Expression Q</em>}</li>
 *   <li>{@link Ada.impl.CaseExpressionPathImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CaseExpressionPathImpl extends MinimalEObjectImpl.Container implements CaseExpressionPath {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getCasePathAlternativeChoicesQl() <em>Case Path Alternative Choices Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCasePathAlternativeChoicesQl()
	 * @generated
	 * @ordered
	 */
	protected ElementList casePathAlternativeChoicesQl;

	/**
	 * The cached value of the '{@link #getDependentExpressionQ() <em>Dependent Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependentExpressionQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass dependentExpressionQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CaseExpressionPathImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getCaseExpressionPath();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.CASE_EXPRESSION_PATH__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.CASE_EXPRESSION_PATH__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.CASE_EXPRESSION_PATH__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.CASE_EXPRESSION_PATH__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementList getCasePathAlternativeChoicesQl() {
		return casePathAlternativeChoicesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCasePathAlternativeChoicesQl(ElementList newCasePathAlternativeChoicesQl, NotificationChain msgs) {
		ElementList oldCasePathAlternativeChoicesQl = casePathAlternativeChoicesQl;
		casePathAlternativeChoicesQl = newCasePathAlternativeChoicesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.CASE_EXPRESSION_PATH__CASE_PATH_ALTERNATIVE_CHOICES_QL, oldCasePathAlternativeChoicesQl, newCasePathAlternativeChoicesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCasePathAlternativeChoicesQl(ElementList newCasePathAlternativeChoicesQl) {
		if (newCasePathAlternativeChoicesQl != casePathAlternativeChoicesQl) {
			NotificationChain msgs = null;
			if (casePathAlternativeChoicesQl != null)
				msgs = ((InternalEObject)casePathAlternativeChoicesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.CASE_EXPRESSION_PATH__CASE_PATH_ALTERNATIVE_CHOICES_QL, null, msgs);
			if (newCasePathAlternativeChoicesQl != null)
				msgs = ((InternalEObject)newCasePathAlternativeChoicesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.CASE_EXPRESSION_PATH__CASE_PATH_ALTERNATIVE_CHOICES_QL, null, msgs);
			msgs = basicSetCasePathAlternativeChoicesQl(newCasePathAlternativeChoicesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.CASE_EXPRESSION_PATH__CASE_PATH_ALTERNATIVE_CHOICES_QL, newCasePathAlternativeChoicesQl, newCasePathAlternativeChoicesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getDependentExpressionQ() {
		return dependentExpressionQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDependentExpressionQ(ExpressionClass newDependentExpressionQ, NotificationChain msgs) {
		ExpressionClass oldDependentExpressionQ = dependentExpressionQ;
		dependentExpressionQ = newDependentExpressionQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.CASE_EXPRESSION_PATH__DEPENDENT_EXPRESSION_Q, oldDependentExpressionQ, newDependentExpressionQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDependentExpressionQ(ExpressionClass newDependentExpressionQ) {
		if (newDependentExpressionQ != dependentExpressionQ) {
			NotificationChain msgs = null;
			if (dependentExpressionQ != null)
				msgs = ((InternalEObject)dependentExpressionQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.CASE_EXPRESSION_PATH__DEPENDENT_EXPRESSION_Q, null, msgs);
			if (newDependentExpressionQ != null)
				msgs = ((InternalEObject)newDependentExpressionQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.CASE_EXPRESSION_PATH__DEPENDENT_EXPRESSION_Q, null, msgs);
			msgs = basicSetDependentExpressionQ(newDependentExpressionQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.CASE_EXPRESSION_PATH__DEPENDENT_EXPRESSION_Q, newDependentExpressionQ, newDependentExpressionQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.CASE_EXPRESSION_PATH__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.CASE_EXPRESSION_PATH__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.CASE_EXPRESSION_PATH__CASE_PATH_ALTERNATIVE_CHOICES_QL:
				return basicSetCasePathAlternativeChoicesQl(null, msgs);
			case AdaPackage.CASE_EXPRESSION_PATH__DEPENDENT_EXPRESSION_Q:
				return basicSetDependentExpressionQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.CASE_EXPRESSION_PATH__SLOC:
				return getSloc();
			case AdaPackage.CASE_EXPRESSION_PATH__CASE_PATH_ALTERNATIVE_CHOICES_QL:
				return getCasePathAlternativeChoicesQl();
			case AdaPackage.CASE_EXPRESSION_PATH__DEPENDENT_EXPRESSION_Q:
				return getDependentExpressionQ();
			case AdaPackage.CASE_EXPRESSION_PATH__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.CASE_EXPRESSION_PATH__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.CASE_EXPRESSION_PATH__CASE_PATH_ALTERNATIVE_CHOICES_QL:
				setCasePathAlternativeChoicesQl((ElementList)newValue);
				return;
			case AdaPackage.CASE_EXPRESSION_PATH__DEPENDENT_EXPRESSION_Q:
				setDependentExpressionQ((ExpressionClass)newValue);
				return;
			case AdaPackage.CASE_EXPRESSION_PATH__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.CASE_EXPRESSION_PATH__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.CASE_EXPRESSION_PATH__CASE_PATH_ALTERNATIVE_CHOICES_QL:
				setCasePathAlternativeChoicesQl((ElementList)null);
				return;
			case AdaPackage.CASE_EXPRESSION_PATH__DEPENDENT_EXPRESSION_Q:
				setDependentExpressionQ((ExpressionClass)null);
				return;
			case AdaPackage.CASE_EXPRESSION_PATH__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.CASE_EXPRESSION_PATH__SLOC:
				return sloc != null;
			case AdaPackage.CASE_EXPRESSION_PATH__CASE_PATH_ALTERNATIVE_CHOICES_QL:
				return casePathAlternativeChoicesQl != null;
			case AdaPackage.CASE_EXPRESSION_PATH__DEPENDENT_EXPRESSION_Q:
				return dependentExpressionQ != null;
			case AdaPackage.CASE_EXPRESSION_PATH__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //CaseExpressionPathImpl
