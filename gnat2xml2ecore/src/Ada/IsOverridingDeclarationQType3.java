/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Is Overriding Declaration QType3</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.IsOverridingDeclarationQType3#getOverriding <em>Overriding</em>}</li>
 *   <li>{@link Ada.IsOverridingDeclarationQType3#getNotAnElement <em>Not An Element</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getIsOverridingDeclarationQType3()
 * @model extendedMetaData="name='is_overriding_declaration_q_._3_._type' kind='elementOnly'"
 * @generated
 */
public interface IsOverridingDeclarationQType3 extends EObject {
	/**
	 * Returns the value of the '<em><b>Overriding</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overriding</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overriding</em>' containment reference.
	 * @see #setOverriding(Overriding)
	 * @see Ada.AdaPackage#getIsOverridingDeclarationQType3_Overriding()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='overriding' namespace='##targetNamespace'"
	 * @generated
	 */
	Overriding getOverriding();

	/**
	 * Sets the value of the '{@link Ada.IsOverridingDeclarationQType3#getOverriding <em>Overriding</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Overriding</em>' containment reference.
	 * @see #getOverriding()
	 * @generated
	 */
	void setOverriding(Overriding value);

	/**
	 * Returns the value of the '<em><b>Not An Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not An Element</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not An Element</em>' containment reference.
	 * @see #setNotAnElement(NotAnElement)
	 * @see Ada.AdaPackage#getIsOverridingDeclarationQType3_NotAnElement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not_an_element' namespace='##targetNamespace'"
	 * @generated
	 */
	NotAnElement getNotAnElement();

	/**
	 * Sets the value of the '{@link Ada.IsOverridingDeclarationQType3#getNotAnElement <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not An Element</em>' containment reference.
	 * @see #getNotAnElement()
	 * @generated
	 */
	void setNotAnElement(NotAnElement value);

} // IsOverridingDeclarationQType3
