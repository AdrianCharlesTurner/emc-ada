/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.ElementClass;
import Ada.ExpressionList;
import Ada.SourceLocation;
import Ada.UnconstrainedArrayDefinition;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Unconstrained Array Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.UnconstrainedArrayDefinitionImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.UnconstrainedArrayDefinitionImpl#getIndexSubtypeDefinitionsQl <em>Index Subtype Definitions Ql</em>}</li>
 *   <li>{@link Ada.impl.UnconstrainedArrayDefinitionImpl#getArrayComponentDefinitionQ <em>Array Component Definition Q</em>}</li>
 *   <li>{@link Ada.impl.UnconstrainedArrayDefinitionImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UnconstrainedArrayDefinitionImpl extends MinimalEObjectImpl.Container implements UnconstrainedArrayDefinition {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getIndexSubtypeDefinitionsQl() <em>Index Subtype Definitions Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndexSubtypeDefinitionsQl()
	 * @generated
	 * @ordered
	 */
	protected ExpressionList indexSubtypeDefinitionsQl;

	/**
	 * The cached value of the '{@link #getArrayComponentDefinitionQ() <em>Array Component Definition Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArrayComponentDefinitionQ()
	 * @generated
	 * @ordered
	 */
	protected ElementClass arrayComponentDefinitionQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UnconstrainedArrayDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getUnconstrainedArrayDefinition();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionList getIndexSubtypeDefinitionsQl() {
		return indexSubtypeDefinitionsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIndexSubtypeDefinitionsQl(ExpressionList newIndexSubtypeDefinitionsQl, NotificationChain msgs) {
		ExpressionList oldIndexSubtypeDefinitionsQl = indexSubtypeDefinitionsQl;
		indexSubtypeDefinitionsQl = newIndexSubtypeDefinitionsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__INDEX_SUBTYPE_DEFINITIONS_QL, oldIndexSubtypeDefinitionsQl, newIndexSubtypeDefinitionsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndexSubtypeDefinitionsQl(ExpressionList newIndexSubtypeDefinitionsQl) {
		if (newIndexSubtypeDefinitionsQl != indexSubtypeDefinitionsQl) {
			NotificationChain msgs = null;
			if (indexSubtypeDefinitionsQl != null)
				msgs = ((InternalEObject)indexSubtypeDefinitionsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__INDEX_SUBTYPE_DEFINITIONS_QL, null, msgs);
			if (newIndexSubtypeDefinitionsQl != null)
				msgs = ((InternalEObject)newIndexSubtypeDefinitionsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__INDEX_SUBTYPE_DEFINITIONS_QL, null, msgs);
			msgs = basicSetIndexSubtypeDefinitionsQl(newIndexSubtypeDefinitionsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__INDEX_SUBTYPE_DEFINITIONS_QL, newIndexSubtypeDefinitionsQl, newIndexSubtypeDefinitionsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementClass getArrayComponentDefinitionQ() {
		return arrayComponentDefinitionQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetArrayComponentDefinitionQ(ElementClass newArrayComponentDefinitionQ, NotificationChain msgs) {
		ElementClass oldArrayComponentDefinitionQ = arrayComponentDefinitionQ;
		arrayComponentDefinitionQ = newArrayComponentDefinitionQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__ARRAY_COMPONENT_DEFINITION_Q, oldArrayComponentDefinitionQ, newArrayComponentDefinitionQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setArrayComponentDefinitionQ(ElementClass newArrayComponentDefinitionQ) {
		if (newArrayComponentDefinitionQ != arrayComponentDefinitionQ) {
			NotificationChain msgs = null;
			if (arrayComponentDefinitionQ != null)
				msgs = ((InternalEObject)arrayComponentDefinitionQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__ARRAY_COMPONENT_DEFINITION_Q, null, msgs);
			if (newArrayComponentDefinitionQ != null)
				msgs = ((InternalEObject)newArrayComponentDefinitionQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__ARRAY_COMPONENT_DEFINITION_Q, null, msgs);
			msgs = basicSetArrayComponentDefinitionQ(newArrayComponentDefinitionQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__ARRAY_COMPONENT_DEFINITION_Q, newArrayComponentDefinitionQ, newArrayComponentDefinitionQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__INDEX_SUBTYPE_DEFINITIONS_QL:
				return basicSetIndexSubtypeDefinitionsQl(null, msgs);
			case AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__ARRAY_COMPONENT_DEFINITION_Q:
				return basicSetArrayComponentDefinitionQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__SLOC:
				return getSloc();
			case AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__INDEX_SUBTYPE_DEFINITIONS_QL:
				return getIndexSubtypeDefinitionsQl();
			case AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__ARRAY_COMPONENT_DEFINITION_Q:
				return getArrayComponentDefinitionQ();
			case AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__INDEX_SUBTYPE_DEFINITIONS_QL:
				setIndexSubtypeDefinitionsQl((ExpressionList)newValue);
				return;
			case AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__ARRAY_COMPONENT_DEFINITION_Q:
				setArrayComponentDefinitionQ((ElementClass)newValue);
				return;
			case AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__INDEX_SUBTYPE_DEFINITIONS_QL:
				setIndexSubtypeDefinitionsQl((ExpressionList)null);
				return;
			case AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__ARRAY_COMPONENT_DEFINITION_Q:
				setArrayComponentDefinitionQ((ElementClass)null);
				return;
			case AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__SLOC:
				return sloc != null;
			case AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__INDEX_SUBTYPE_DEFINITIONS_QL:
				return indexSubtypeDefinitionsQl != null;
			case AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__ARRAY_COMPONENT_DEFINITION_Q:
				return arrayComponentDefinitionQ != null;
			case AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //UnconstrainedArrayDefinitionImpl
