/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Anonymous Access To Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.AnonymousAccessToVariable#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.AnonymousAccessToVariable#getHasNullExclusionQ <em>Has Null Exclusion Q</em>}</li>
 *   <li>{@link Ada.AnonymousAccessToVariable#getAnonymousAccessToObjectSubtypeMarkQ <em>Anonymous Access To Object Subtype Mark Q</em>}</li>
 *   <li>{@link Ada.AnonymousAccessToVariable#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getAnonymousAccessToVariable()
 * @model extendedMetaData="name='Anonymous_Access_To_Variable' kind='elementOnly'"
 * @generated
 */
public interface AnonymousAccessToVariable extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getAnonymousAccessToVariable_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.AnonymousAccessToVariable#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Has Null Exclusion Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Null Exclusion Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Null Exclusion Q</em>' containment reference.
	 * @see #setHasNullExclusionQ(HasNullExclusionQType7)
	 * @see Ada.AdaPackage#getAnonymousAccessToVariable_HasNullExclusionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='has_null_exclusion_q' namespace='##targetNamespace'"
	 * @generated
	 */
	HasNullExclusionQType7 getHasNullExclusionQ();

	/**
	 * Sets the value of the '{@link Ada.AnonymousAccessToVariable#getHasNullExclusionQ <em>Has Null Exclusion Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Null Exclusion Q</em>' containment reference.
	 * @see #getHasNullExclusionQ()
	 * @generated
	 */
	void setHasNullExclusionQ(HasNullExclusionQType7 value);

	/**
	 * Returns the value of the '<em><b>Anonymous Access To Object Subtype Mark Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Access To Object Subtype Mark Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Access To Object Subtype Mark Q</em>' containment reference.
	 * @see #setAnonymousAccessToObjectSubtypeMarkQ(ExpressionClass)
	 * @see Ada.AdaPackage#getAnonymousAccessToVariable_AnonymousAccessToObjectSubtypeMarkQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='anonymous_access_to_object_subtype_mark_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getAnonymousAccessToObjectSubtypeMarkQ();

	/**
	 * Sets the value of the '{@link Ada.AnonymousAccessToVariable#getAnonymousAccessToObjectSubtypeMarkQ <em>Anonymous Access To Object Subtype Mark Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Anonymous Access To Object Subtype Mark Q</em>' containment reference.
	 * @see #getAnonymousAccessToObjectSubtypeMarkQ()
	 * @generated
	 */
	void setAnonymousAccessToObjectSubtypeMarkQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getAnonymousAccessToVariable_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.AnonymousAccessToVariable#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // AnonymousAccessToVariable
