/**
 */
package Ada;

import java.math.BigInteger;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Source Location</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.SourceLocation#getCol <em>Col</em>}</li>
 *   <li>{@link Ada.SourceLocation#getEndcol <em>Endcol</em>}</li>
 *   <li>{@link Ada.SourceLocation#getEndline <em>Endline</em>}</li>
 *   <li>{@link Ada.SourceLocation#getLine <em>Line</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getSourceLocation()
 * @model extendedMetaData="name='Source_Location' kind='empty'"
 * @generated
 */
public interface SourceLocation extends EObject {
	/**
	 * Returns the value of the '<em><b>Col</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Col</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Col</em>' attribute.
	 * @see #setCol(BigInteger)
	 * @see Ada.AdaPackage#getSourceLocation_Col()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.PositiveInteger" required="true"
	 *        extendedMetaData="kind='attribute' name='col' namespace='##targetNamespace'"
	 * @generated
	 */
	BigInteger getCol();

	/**
	 * Sets the value of the '{@link Ada.SourceLocation#getCol <em>Col</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Col</em>' attribute.
	 * @see #getCol()
	 * @generated
	 */
	void setCol(BigInteger value);

	/**
	 * Returns the value of the '<em><b>Endcol</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Endcol</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Endcol</em>' attribute.
	 * @see #setEndcol(BigInteger)
	 * @see Ada.AdaPackage#getSourceLocation_Endcol()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NonNegativeInteger" required="true"
	 *        extendedMetaData="kind='attribute' name='endcol' namespace='##targetNamespace'"
	 * @generated
	 */
	BigInteger getEndcol();

	/**
	 * Sets the value of the '{@link Ada.SourceLocation#getEndcol <em>Endcol</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Endcol</em>' attribute.
	 * @see #getEndcol()
	 * @generated
	 */
	void setEndcol(BigInteger value);

	/**
	 * Returns the value of the '<em><b>Endline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Endline</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Endline</em>' attribute.
	 * @see #setEndline(BigInteger)
	 * @see Ada.AdaPackage#getSourceLocation_Endline()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NonNegativeInteger" required="true"
	 *        extendedMetaData="kind='attribute' name='endline' namespace='##targetNamespace'"
	 * @generated
	 */
	BigInteger getEndline();

	/**
	 * Sets the value of the '{@link Ada.SourceLocation#getEndline <em>Endline</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Endline</em>' attribute.
	 * @see #getEndline()
	 * @generated
	 */
	void setEndline(BigInteger value);

	/**
	 * Returns the value of the '<em><b>Line</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Line</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Line</em>' attribute.
	 * @see #setLine(BigInteger)
	 * @see Ada.AdaPackage#getSourceLocation_Line()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.PositiveInteger" required="true"
	 *        extendedMetaData="kind='attribute' name='line' namespace='##targetNamespace'"
	 * @generated
	 */
	BigInteger getLine();

	/**
	 * Sets the value of the '{@link Ada.SourceLocation#getLine <em>Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Line</em>' attribute.
	 * @see #getLine()
	 * @generated
	 */
	void setLine(BigInteger value);

} // SourceLocation
