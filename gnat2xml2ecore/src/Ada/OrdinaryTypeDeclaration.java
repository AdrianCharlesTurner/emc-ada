/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ordinary Type Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.OrdinaryTypeDeclaration#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.OrdinaryTypeDeclaration#getNamesQl <em>Names Ql</em>}</li>
 *   <li>{@link Ada.OrdinaryTypeDeclaration#getDiscriminantPartQ <em>Discriminant Part Q</em>}</li>
 *   <li>{@link Ada.OrdinaryTypeDeclaration#getTypeDeclarationViewQ <em>Type Declaration View Q</em>}</li>
 *   <li>{@link Ada.OrdinaryTypeDeclaration#getAspectSpecificationsQl <em>Aspect Specifications Ql</em>}</li>
 *   <li>{@link Ada.OrdinaryTypeDeclaration#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getOrdinaryTypeDeclaration()
 * @model extendedMetaData="name='Ordinary_Type_Declaration' kind='elementOnly'"
 * @generated
 */
public interface OrdinaryTypeDeclaration extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getOrdinaryTypeDeclaration_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.OrdinaryTypeDeclaration#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Names Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Names Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Names Ql</em>' containment reference.
	 * @see #setNamesQl(DefiningNameList)
	 * @see Ada.AdaPackage#getOrdinaryTypeDeclaration_NamesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='names_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameList getNamesQl();

	/**
	 * Sets the value of the '{@link Ada.OrdinaryTypeDeclaration#getNamesQl <em>Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Names Ql</em>' containment reference.
	 * @see #getNamesQl()
	 * @generated
	 */
	void setNamesQl(DefiningNameList value);

	/**
	 * Returns the value of the '<em><b>Discriminant Part Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discriminant Part Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discriminant Part Q</em>' containment reference.
	 * @see #setDiscriminantPartQ(DefinitionClass)
	 * @see Ada.AdaPackage#getOrdinaryTypeDeclaration_DiscriminantPartQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='discriminant_part_q' namespace='##targetNamespace'"
	 * @generated
	 */
	DefinitionClass getDiscriminantPartQ();

	/**
	 * Sets the value of the '{@link Ada.OrdinaryTypeDeclaration#getDiscriminantPartQ <em>Discriminant Part Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discriminant Part Q</em>' containment reference.
	 * @see #getDiscriminantPartQ()
	 * @generated
	 */
	void setDiscriminantPartQ(DefinitionClass value);

	/**
	 * Returns the value of the '<em><b>Type Declaration View Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Declaration View Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Declaration View Q</em>' containment reference.
	 * @see #setTypeDeclarationViewQ(DefinitionClass)
	 * @see Ada.AdaPackage#getOrdinaryTypeDeclaration_TypeDeclarationViewQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='type_declaration_view_q' namespace='##targetNamespace'"
	 * @generated
	 */
	DefinitionClass getTypeDeclarationViewQ();

	/**
	 * Sets the value of the '{@link Ada.OrdinaryTypeDeclaration#getTypeDeclarationViewQ <em>Type Declaration View Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type Declaration View Q</em>' containment reference.
	 * @see #getTypeDeclarationViewQ()
	 * @generated
	 */
	void setTypeDeclarationViewQ(DefinitionClass value);

	/**
	 * Returns the value of the '<em><b>Aspect Specifications Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aspect Specifications Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aspect Specifications Ql</em>' containment reference.
	 * @see #setAspectSpecificationsQl(ElementList)
	 * @see Ada.AdaPackage#getOrdinaryTypeDeclaration_AspectSpecificationsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='aspect_specifications_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementList getAspectSpecificationsQl();

	/**
	 * Sets the value of the '{@link Ada.OrdinaryTypeDeclaration#getAspectSpecificationsQl <em>Aspect Specifications Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aspect Specifications Ql</em>' containment reference.
	 * @see #getAspectSpecificationsQl()
	 * @generated
	 */
	void setAspectSpecificationsQl(ElementList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getOrdinaryTypeDeclaration_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.OrdinaryTypeDeclaration#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // OrdinaryTypeDeclaration
