/**
 */
package Ada.impl;

import Ada.AbortStatement;
import Ada.AbsOperator;
import Ada.Abstract;
import Ada.AcceptStatement;
import Ada.AccessAttribute;
import Ada.AccessToConstant;
import Ada.AccessToFunction;
import Ada.AccessToProcedure;
import Ada.AccessToProtectedFunction;
import Ada.AccessToProtectedProcedure;
import Ada.AccessToVariable;
import Ada.AdaPackage;
import Ada.AddressAttribute;
import Ada.AdjacentAttribute;
import Ada.AftAttribute;
import Ada.Aliased;
import Ada.AlignmentAttribute;
import Ada.AllCallsRemotePragma;
import Ada.AllocationFromQualifiedExpression;
import Ada.AllocationFromSubtype;
import Ada.AndOperator;
import Ada.AndThenShortCircuit;
import Ada.AnonymousAccessToConstant;
import Ada.AnonymousAccessToFunction;
import Ada.AnonymousAccessToProcedure;
import Ada.AnonymousAccessToProtectedFunction;
import Ada.AnonymousAccessToProtectedProcedure;
import Ada.AnonymousAccessToVariable;
import Ada.ArrayComponentAssociation;
import Ada.AspectSpecification;
import Ada.AssertPragma;
import Ada.AssertionPolicyPragma;
import Ada.AssignmentStatement;
import Ada.AsynchronousPragma;
import Ada.AsynchronousSelectStatement;
import Ada.AtClause;
import Ada.AtomicComponentsPragma;
import Ada.AtomicPragma;
import Ada.AttachHandlerPragma;
import Ada.AttributeDefinitionClause;
import Ada.BaseAttribute;
import Ada.BitOrderAttribute;
import Ada.BlockStatement;
import Ada.BodyVersionAttribute;
import Ada.BoxExpression;
import Ada.CallableAttribute;
import Ada.CallerAttribute;
import Ada.CaseExpression;
import Ada.CaseExpressionPath;
import Ada.CasePath;
import Ada.CaseStatement;
import Ada.CeilingAttribute;
import Ada.CharacterLiteral;
import Ada.ChoiceParameterSpecification;
import Ada.ClassAttribute;
import Ada.CodeStatement;
import Ada.Comment;
import Ada.CompilationUnit;
import Ada.ComponentClause;
import Ada.ComponentDeclaration;
import Ada.ComponentDefinition;
import Ada.ComponentSizeAttribute;
import Ada.ComposeAttribute;
import Ada.ConcatenateOperator;
import Ada.ConditionalEntryCallStatement;
import Ada.ConstantDeclaration;
import Ada.ConstrainedArrayDefinition;
import Ada.ConstrainedAttribute;
import Ada.ControlledPragma;
import Ada.ConventionPragma;
import Ada.CopySignAttribute;
import Ada.CountAttribute;
import Ada.CpuPragma;
import Ada.DecimalFixedPointDefinition;
import Ada.DefaultStoragePoolPragma;
import Ada.DeferredConstantDeclaration;
import Ada.DefiningAbsOperator;
import Ada.DefiningAndOperator;
import Ada.DefiningCharacterLiteral;
import Ada.DefiningConcatenateOperator;
import Ada.DefiningDivideOperator;
import Ada.DefiningEnumerationLiteral;
import Ada.DefiningEqualOperator;
import Ada.DefiningExpandedName;
import Ada.DefiningExponentiateOperator;
import Ada.DefiningGreaterThanOperator;
import Ada.DefiningGreaterThanOrEqualOperator;
import Ada.DefiningIdentifier;
import Ada.DefiningLessThanOperator;
import Ada.DefiningLessThanOrEqualOperator;
import Ada.DefiningMinusOperator;
import Ada.DefiningModOperator;
import Ada.DefiningMultiplyOperator;
import Ada.DefiningNotEqualOperator;
import Ada.DefiningNotOperator;
import Ada.DefiningOrOperator;
import Ada.DefiningPlusOperator;
import Ada.DefiningRemOperator;
import Ada.DefiningUnaryMinusOperator;
import Ada.DefiningUnaryPlusOperator;
import Ada.DefiningXorOperator;
import Ada.DefiniteAttribute;
import Ada.DelayRelativeStatement;
import Ada.DelayUntilStatement;
import Ada.DeltaAttribute;
import Ada.DeltaConstraint;
import Ada.DenormAttribute;
import Ada.DerivedRecordExtensionDefinition;
import Ada.DerivedTypeDefinition;
import Ada.DetectBlockingPragma;
import Ada.DigitsAttribute;
import Ada.DigitsConstraint;
import Ada.DiscardNamesPragma;
import Ada.DiscreteRangeAttributeReference;
import Ada.DiscreteRangeAttributeReferenceAsSubtypeDefinition;
import Ada.DiscreteSimpleExpressionRange;
import Ada.DiscreteSimpleExpressionRangeAsSubtypeDefinition;
import Ada.DiscreteSubtypeIndication;
import Ada.DiscreteSubtypeIndicationAsSubtypeDefinition;
import Ada.DiscriminantAssociation;
import Ada.DiscriminantConstraint;
import Ada.DiscriminantSpecification;
import Ada.DispatchingDomainPragma;
import Ada.DivideOperator;
import Ada.DocumentRoot;
import Ada.ElaborateAllPragma;
import Ada.ElaborateBodyPragma;
import Ada.ElaboratePragma;
import Ada.ElementIteratorSpecification;
import Ada.ElseExpressionPath;
import Ada.ElsePath;
import Ada.ElsifExpressionPath;
import Ada.ElsifPath;
import Ada.EntryBodyDeclaration;
import Ada.EntryCallStatement;
import Ada.EntryDeclaration;
import Ada.EntryIndexSpecification;
import Ada.EnumerationLiteral;
import Ada.EnumerationLiteralSpecification;
import Ada.EnumerationRepresentationClause;
import Ada.EnumerationTypeDefinition;
import Ada.EqualOperator;
import Ada.ExceptionDeclaration;
import Ada.ExceptionHandler;
import Ada.ExceptionRenamingDeclaration;
import Ada.ExitStatement;
import Ada.ExplicitDereference;
import Ada.ExponentAttribute;
import Ada.ExponentiateOperator;
import Ada.ExportPragma;
import Ada.ExpressionFunctionDeclaration;
import Ada.ExtendedReturnStatement;
import Ada.ExtensionAggregate;
import Ada.ExternalTagAttribute;
import Ada.FirstAttribute;
import Ada.FirstBitAttribute;
import Ada.FloatingPointDefinition;
import Ada.FloorAttribute;
import Ada.ForAllQuantifiedExpression;
import Ada.ForLoopStatement;
import Ada.ForSomeQuantifiedExpression;
import Ada.ForeAttribute;
import Ada.FormalAccessToConstant;
import Ada.FormalAccessToFunction;
import Ada.FormalAccessToProcedure;
import Ada.FormalAccessToProtectedFunction;
import Ada.FormalAccessToProtectedProcedure;
import Ada.FormalAccessToVariable;
import Ada.FormalConstrainedArrayDefinition;
import Ada.FormalDecimalFixedPointDefinition;
import Ada.FormalDerivedTypeDefinition;
import Ada.FormalDiscreteTypeDefinition;
import Ada.FormalFloatingPointDefinition;
import Ada.FormalFunctionDeclaration;
import Ada.FormalIncompleteTypeDeclaration;
import Ada.FormalLimitedInterface;
import Ada.FormalModularTypeDefinition;
import Ada.FormalObjectDeclaration;
import Ada.FormalOrdinaryFixedPointDefinition;
import Ada.FormalOrdinaryInterface;
import Ada.FormalPackageDeclaration;
import Ada.FormalPackageDeclarationWithBox;
import Ada.FormalPoolSpecificAccessToVariable;
import Ada.FormalPrivateTypeDefinition;
import Ada.FormalProcedureDeclaration;
import Ada.FormalProtectedInterface;
import Ada.FormalSignedIntegerTypeDefinition;
import Ada.FormalSynchronizedInterface;
import Ada.FormalTaggedPrivateTypeDefinition;
import Ada.FormalTaskInterface;
import Ada.FormalTypeDeclaration;
import Ada.FormalUnconstrainedArrayDefinition;
import Ada.FractionAttribute;
import Ada.FunctionBodyDeclaration;
import Ada.FunctionBodyStub;
import Ada.FunctionCall;
import Ada.FunctionDeclaration;
import Ada.FunctionInstantiation;
import Ada.FunctionRenamingDeclaration;
import Ada.GeneralizedIteratorSpecification;
import Ada.GenericAssociation;
import Ada.GenericFunctionDeclaration;
import Ada.GenericFunctionRenamingDeclaration;
import Ada.GenericPackageDeclaration;
import Ada.GenericPackageRenamingDeclaration;
import Ada.GenericProcedureDeclaration;
import Ada.GenericProcedureRenamingDeclaration;
import Ada.GotoStatement;
import Ada.GreaterThanOperator;
import Ada.GreaterThanOrEqualOperator;
import Ada.Identifier;
import Ada.IdentityAttribute;
import Ada.IfExpression;
import Ada.IfExpressionPath;
import Ada.IfPath;
import Ada.IfStatement;
import Ada.ImageAttribute;
import Ada.ImplementationDefinedAttribute;
import Ada.ImplementationDefinedPragma;
import Ada.ImportPragma;
import Ada.InMembershipTest;
import Ada.IncompleteTypeDeclaration;
import Ada.IndependentComponentsPragma;
import Ada.IndependentPragma;
import Ada.IndexConstraint;
import Ada.IndexedComponent;
import Ada.InlinePragma;
import Ada.InputAttribute;
import Ada.InspectionPointPragma;
import Ada.IntegerLiteral;
import Ada.IntegerNumberDeclaration;
import Ada.InterruptHandlerPragma;
import Ada.InterruptPriorityPragma;
import Ada.IsPrefixCall;
import Ada.IsPrefixNotation;
import Ada.KnownDiscriminantPart;
import Ada.LastAttribute;
import Ada.LastBitAttribute;
import Ada.LeadingPartAttribute;
import Ada.LengthAttribute;
import Ada.LessThanOperator;
import Ada.LessThanOrEqualOperator;
import Ada.Limited;
import Ada.LimitedInterface;
import Ada.LinkerOptionsPragma;
import Ada.ListPragma;
import Ada.LockingPolicyPragma;
import Ada.LoopParameterSpecification;
import Ada.LoopStatement;
import Ada.MachineAttribute;
import Ada.MachineEmaxAttribute;
import Ada.MachineEminAttribute;
import Ada.MachineMantissaAttribute;
import Ada.MachineOverflowsAttribute;
import Ada.MachineRadixAttribute;
import Ada.MachineRoundingAttribute;
import Ada.MachineRoundsAttribute;
import Ada.MaxAlignmentForAllocationAttribute;
import Ada.MaxAttribute;
import Ada.MaxSizeInStorageElementsAttribute;
import Ada.MinAttribute;
import Ada.MinusOperator;
import Ada.ModAttribute;
import Ada.ModOperator;
import Ada.ModelAttribute;
import Ada.ModelEminAttribute;
import Ada.ModelEpsilonAttribute;
import Ada.ModelMantissaAttribute;
import Ada.ModelSmallAttribute;
import Ada.ModularTypeDefinition;
import Ada.ModulusAttribute;
import Ada.MultiplyOperator;
import Ada.NamedArrayAggregate;
import Ada.NoReturnPragma;
import Ada.NormalizeScalarsPragma;
import Ada.NotAnElement;
import Ada.NotEqualOperator;
import Ada.NotInMembershipTest;
import Ada.NotNullReturn;
import Ada.NotOperator;
import Ada.NotOverriding;
import Ada.NullComponent;
import Ada.NullExclusion;
import Ada.NullLiteral;
import Ada.NullProcedureDeclaration;
import Ada.NullRecordDefinition;
import Ada.NullStatement;
import Ada.ObjectRenamingDeclaration;
import Ada.OptimizePragma;
import Ada.OrElseShortCircuit;
import Ada.OrOperator;
import Ada.OrPath;
import Ada.OrdinaryFixedPointDefinition;
import Ada.OrdinaryInterface;
import Ada.OrdinaryTypeDeclaration;
import Ada.OthersChoice;
import Ada.OutputAttribute;
import Ada.OverlapsStorageAttribute;
import Ada.Overriding;
import Ada.PackPragma;
import Ada.PackageBodyDeclaration;
import Ada.PackageBodyStub;
import Ada.PackageDeclaration;
import Ada.PackageInstantiation;
import Ada.PackageRenamingDeclaration;
import Ada.PagePragma;
import Ada.ParameterAssociation;
import Ada.ParameterSpecification;
import Ada.ParenthesizedExpression;
import Ada.PartitionElaborationPolicyPragma;
import Ada.PartitionIdAttribute;
import Ada.PlusOperator;
import Ada.PoolSpecificAccessToVariable;
import Ada.PosAttribute;
import Ada.PositionAttribute;
import Ada.PositionalArrayAggregate;
import Ada.PragmaArgumentAssociation;
import Ada.PredAttribute;
import Ada.PreelaborableInitializationPragma;
import Ada.PreelaboratePragma;
import Ada.PriorityAttribute;
import Ada.PriorityPragma;
import Ada.PrioritySpecificDispatchingPragma;
import Ada.Private;
import Ada.PrivateExtensionDeclaration;
import Ada.PrivateExtensionDefinition;
import Ada.PrivateTypeDeclaration;
import Ada.PrivateTypeDefinition;
import Ada.ProcedureBodyDeclaration;
import Ada.ProcedureBodyStub;
import Ada.ProcedureCallStatement;
import Ada.ProcedureDeclaration;
import Ada.ProcedureInstantiation;
import Ada.ProcedureRenamingDeclaration;
import Ada.ProfilePragma;
import Ada.ProtectedBodyDeclaration;
import Ada.ProtectedBodyStub;
import Ada.ProtectedDefinition;
import Ada.ProtectedInterface;
import Ada.ProtectedTypeDeclaration;
import Ada.PurePragma;
import Ada.QualifiedExpression;
import Ada.QueuingPolicyPragma;
import Ada.RaiseExpression;
import Ada.RaiseStatement;
import Ada.RangeAttribute;
import Ada.RangeAttributeReference;
import Ada.ReadAttribute;
import Ada.RealLiteral;
import Ada.RealNumberDeclaration;
import Ada.RecordAggregate;
import Ada.RecordComponentAssociation;
import Ada.RecordDefinition;
import Ada.RecordRepresentationClause;
import Ada.RecordTypeDefinition;
import Ada.RelativeDeadlinePragma;
import Ada.RemOperator;
import Ada.RemainderAttribute;
import Ada.RemoteCallInterfacePragma;
import Ada.RemoteTypesPragma;
import Ada.RequeueStatement;
import Ada.RequeueStatementWithAbort;
import Ada.RestrictionsPragma;
import Ada.ReturnConstantSpecification;
import Ada.ReturnStatement;
import Ada.ReturnVariableSpecification;
import Ada.Reverse;
import Ada.ReviewablePragma;
import Ada.RootIntegerDefinition;
import Ada.RootRealDefinition;
import Ada.RoundAttribute;
import Ada.RoundingAttribute;
import Ada.SafeFirstAttribute;
import Ada.SafeLastAttribute;
import Ada.ScaleAttribute;
import Ada.ScalingAttribute;
import Ada.SelectPath;
import Ada.SelectedComponent;
import Ada.SelectiveAcceptStatement;
import Ada.SharedPassivePragma;
import Ada.SignedIntegerTypeDefinition;
import Ada.SignedZerosAttribute;
import Ada.SimpleExpressionRange;
import Ada.SingleProtectedDeclaration;
import Ada.SingleTaskDeclaration;
import Ada.SizeAttribute;
import Ada.Slice;
import Ada.SmallAttribute;
import Ada.StoragePoolAttribute;
import Ada.StorageSizeAttribute;
import Ada.StorageSizePragma;
import Ada.StreamSizeAttribute;
import Ada.StringLiteral;
import Ada.SubtypeDeclaration;
import Ada.SubtypeIndication;
import Ada.SuccAttribute;
import Ada.SuppressPragma;
import Ada.Synchronized;
import Ada.SynchronizedInterface;
import Ada.TagAttribute;
import Ada.Tagged;
import Ada.TaggedIncompleteTypeDeclaration;
import Ada.TaggedPrivateTypeDefinition;
import Ada.TaggedRecordTypeDefinition;
import Ada.TaskBodyDeclaration;
import Ada.TaskBodyStub;
import Ada.TaskDefinition;
import Ada.TaskDispatchingPolicyPragma;
import Ada.TaskInterface;
import Ada.TaskTypeDeclaration;
import Ada.TerminateAlternativeStatement;
import Ada.TerminatedAttribute;
import Ada.ThenAbortPath;
import Ada.TimedEntryCallStatement;
import Ada.TruncationAttribute;
import Ada.TypeConversion;
import Ada.UnaryMinusOperator;
import Ada.UnaryPlusOperator;
import Ada.UnbiasedRoundingAttribute;
import Ada.UncheckedAccessAttribute;
import Ada.UncheckedUnionPragma;
import Ada.UnconstrainedArrayDefinition;
import Ada.UniversalFixedDefinition;
import Ada.UniversalIntegerDefinition;
import Ada.UniversalRealDefinition;
import Ada.UnknownAttribute;
import Ada.UnknownDiscriminantPart;
import Ada.UnknownPragma;
import Ada.UnsuppressPragma;
import Ada.UseAllTypeClause;
import Ada.UsePackageClause;
import Ada.UseTypeClause;
import Ada.ValAttribute;
import Ada.ValidAttribute;
import Ada.ValueAttribute;
import Ada.VariableDeclaration;
import Ada.Variant;
import Ada.VariantPart;
import Ada.VersionAttribute;
import Ada.VolatileComponentsPragma;
import Ada.VolatilePragma;
import Ada.WhileLoopStatement;
import Ada.WideImageAttribute;
import Ada.WideValueAttribute;
import Ada.WideWideImageAttribute;
import Ada.WideWideValueAttribute;
import Ada.WideWideWidthAttribute;
import Ada.WideWidthAttribute;
import Ada.WidthAttribute;
import Ada.WithClause;
import Ada.WriteAttribute;
import Ada.XorOperator;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EStringToStringMapEntryImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Document Root</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.DocumentRootImpl#getMixed <em>Mixed</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getXSISchemaLocation <em>XSI Schema Location</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAbortStatement <em>Abort Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAbsOperator <em>Abs Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAbstract <em>Abstract</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAcceptStatement <em>Accept Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAccessAttribute <em>Access Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAccessToConstant <em>Access To Constant</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAccessToFunction <em>Access To Function</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAccessToProcedure <em>Access To Procedure</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAccessToProtectedFunction <em>Access To Protected Function</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAccessToProtectedProcedure <em>Access To Protected Procedure</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAccessToVariable <em>Access To Variable</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAddressAttribute <em>Address Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAdjacentAttribute <em>Adjacent Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAftAttribute <em>Aft Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAliased <em>Aliased</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAlignmentAttribute <em>Alignment Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAllocationFromQualifiedExpression <em>Allocation From Qualified Expression</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAllocationFromSubtype <em>Allocation From Subtype</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAndOperator <em>And Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAndThenShortCircuit <em>And Then Short Circuit</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAnonymousAccessToConstant <em>Anonymous Access To Constant</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAnonymousAccessToFunction <em>Anonymous Access To Function</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAnonymousAccessToProcedure <em>Anonymous Access To Procedure</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAnonymousAccessToProtectedFunction <em>Anonymous Access To Protected Function</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAnonymousAccessToProtectedProcedure <em>Anonymous Access To Protected Procedure</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAnonymousAccessToVariable <em>Anonymous Access To Variable</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getArrayComponentAssociation <em>Array Component Association</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAspectSpecification <em>Aspect Specification</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAssertPragma <em>Assert Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAssignmentStatement <em>Assignment Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAsynchronousPragma <em>Asynchronous Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAsynchronousSelectStatement <em>Asynchronous Select Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAtClause <em>At Clause</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAtomicPragma <em>Atomic Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAttachHandlerPragma <em>Attach Handler Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getAttributeDefinitionClause <em>Attribute Definition Clause</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getBaseAttribute <em>Base Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getBitOrderAttribute <em>Bit Order Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getBlockStatement <em>Block Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getBodyVersionAttribute <em>Body Version Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getBoxExpression <em>Box Expression</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getCallableAttribute <em>Callable Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getCallerAttribute <em>Caller Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getCaseExpression <em>Case Expression</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getCaseExpressionPath <em>Case Expression Path</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getCasePath <em>Case Path</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getCaseStatement <em>Case Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getCeilingAttribute <em>Ceiling Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getCharacterLiteral <em>Character Literal</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getChoiceParameterSpecification <em>Choice Parameter Specification</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getClassAttribute <em>Class Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getCodeStatement <em>Code Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getCompilationUnit <em>Compilation Unit</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getComponentClause <em>Component Clause</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getComponentDeclaration <em>Component Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getComponentDefinition <em>Component Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getComponentSizeAttribute <em>Component Size Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getComposeAttribute <em>Compose Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getConcatenateOperator <em>Concatenate Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getConditionalEntryCallStatement <em>Conditional Entry Call Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getConstantDeclaration <em>Constant Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getConstrainedArrayDefinition <em>Constrained Array Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getConstrainedAttribute <em>Constrained Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getControlledPragma <em>Controlled Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getConventionPragma <em>Convention Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getCopySignAttribute <em>Copy Sign Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getCountAttribute <em>Count Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getCpuPragma <em>Cpu Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDecimalFixedPointDefinition <em>Decimal Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDeferredConstantDeclaration <em>Deferred Constant Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiningAbsOperator <em>Defining Abs Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiningAndOperator <em>Defining And Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiningCharacterLiteral <em>Defining Character Literal</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiningConcatenateOperator <em>Defining Concatenate Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiningDivideOperator <em>Defining Divide Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiningEnumerationLiteral <em>Defining Enumeration Literal</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiningEqualOperator <em>Defining Equal Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiningExpandedName <em>Defining Expanded Name</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiningExponentiateOperator <em>Defining Exponentiate Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiningGreaterThanOperator <em>Defining Greater Than Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiningGreaterThanOrEqualOperator <em>Defining Greater Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiningIdentifier <em>Defining Identifier</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiningLessThanOperator <em>Defining Less Than Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiningLessThanOrEqualOperator <em>Defining Less Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiningMinusOperator <em>Defining Minus Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiningModOperator <em>Defining Mod Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiningMultiplyOperator <em>Defining Multiply Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiningNotEqualOperator <em>Defining Not Equal Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiningNotOperator <em>Defining Not Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiningOrOperator <em>Defining Or Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiningPlusOperator <em>Defining Plus Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiningRemOperator <em>Defining Rem Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiningUnaryMinusOperator <em>Defining Unary Minus Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiningUnaryPlusOperator <em>Defining Unary Plus Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiningXorOperator <em>Defining Xor Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDefiniteAttribute <em>Definite Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDelayRelativeStatement <em>Delay Relative Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDelayUntilStatement <em>Delay Until Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDeltaAttribute <em>Delta Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDeltaConstraint <em>Delta Constraint</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDenormAttribute <em>Denorm Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDerivedRecordExtensionDefinition <em>Derived Record Extension Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDerivedTypeDefinition <em>Derived Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDigitsAttribute <em>Digits Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDigitsConstraint <em>Digits Constraint</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDiscardNamesPragma <em>Discard Names Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDiscreteRangeAttributeReference <em>Discrete Range Attribute Reference</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDiscreteRangeAttributeReferenceAsSubtypeDefinition <em>Discrete Range Attribute Reference As Subtype Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDiscreteSimpleExpressionRange <em>Discrete Simple Expression Range</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDiscreteSimpleExpressionRangeAsSubtypeDefinition <em>Discrete Simple Expression Range As Subtype Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDiscreteSubtypeIndication <em>Discrete Subtype Indication</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDiscreteSubtypeIndicationAsSubtypeDefinition <em>Discrete Subtype Indication As Subtype Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDiscriminantAssociation <em>Discriminant Association</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDiscriminantConstraint <em>Discriminant Constraint</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDiscriminantSpecification <em>Discriminant Specification</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getDivideOperator <em>Divide Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getElaborateAllPragma <em>Elaborate All Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getElaboratePragma <em>Elaborate Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getElementIteratorSpecification <em>Element Iterator Specification</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getElseExpressionPath <em>Else Expression Path</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getElsePath <em>Else Path</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getElsifExpressionPath <em>Elsif Expression Path</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getElsifPath <em>Elsif Path</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getEntryBodyDeclaration <em>Entry Body Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getEntryCallStatement <em>Entry Call Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getEntryDeclaration <em>Entry Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getEntryIndexSpecification <em>Entry Index Specification</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getEnumerationLiteral <em>Enumeration Literal</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getEnumerationLiteralSpecification <em>Enumeration Literal Specification</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getEnumerationRepresentationClause <em>Enumeration Representation Clause</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getEnumerationTypeDefinition <em>Enumeration Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getEqualOperator <em>Equal Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getExceptionDeclaration <em>Exception Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getExceptionHandler <em>Exception Handler</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getExceptionRenamingDeclaration <em>Exception Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getExitStatement <em>Exit Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getExplicitDereference <em>Explicit Dereference</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getExponentAttribute <em>Exponent Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getExponentiateOperator <em>Exponentiate Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getExportPragma <em>Export Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getExpressionFunctionDeclaration <em>Expression Function Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getExtendedReturnStatement <em>Extended Return Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getExtensionAggregate <em>Extension Aggregate</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getExternalTagAttribute <em>External Tag Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFirstAttribute <em>First Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFirstBitAttribute <em>First Bit Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFloatingPointDefinition <em>Floating Point Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFloorAttribute <em>Floor Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getForAllQuantifiedExpression <em>For All Quantified Expression</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getForLoopStatement <em>For Loop Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getForSomeQuantifiedExpression <em>For Some Quantified Expression</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getForeAttribute <em>Fore Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalAccessToConstant <em>Formal Access To Constant</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalAccessToFunction <em>Formal Access To Function</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalAccessToProcedure <em>Formal Access To Procedure</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalAccessToProtectedFunction <em>Formal Access To Protected Function</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalAccessToProtectedProcedure <em>Formal Access To Protected Procedure</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalAccessToVariable <em>Formal Access To Variable</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalConstrainedArrayDefinition <em>Formal Constrained Array Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalDecimalFixedPointDefinition <em>Formal Decimal Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalDerivedTypeDefinition <em>Formal Derived Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalDiscreteTypeDefinition <em>Formal Discrete Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalFloatingPointDefinition <em>Formal Floating Point Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalFunctionDeclaration <em>Formal Function Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalIncompleteTypeDeclaration <em>Formal Incomplete Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalLimitedInterface <em>Formal Limited Interface</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalModularTypeDefinition <em>Formal Modular Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalObjectDeclaration <em>Formal Object Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalOrdinaryFixedPointDefinition <em>Formal Ordinary Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalOrdinaryInterface <em>Formal Ordinary Interface</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalPackageDeclaration <em>Formal Package Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalPackageDeclarationWithBox <em>Formal Package Declaration With Box</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalPoolSpecificAccessToVariable <em>Formal Pool Specific Access To Variable</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalPrivateTypeDefinition <em>Formal Private Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalProcedureDeclaration <em>Formal Procedure Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalProtectedInterface <em>Formal Protected Interface</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalSignedIntegerTypeDefinition <em>Formal Signed Integer Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalSynchronizedInterface <em>Formal Synchronized Interface</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalTaggedPrivateTypeDefinition <em>Formal Tagged Private Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalTaskInterface <em>Formal Task Interface</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalTypeDeclaration <em>Formal Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFormalUnconstrainedArrayDefinition <em>Formal Unconstrained Array Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFractionAttribute <em>Fraction Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFunctionBodyDeclaration <em>Function Body Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFunctionBodyStub <em>Function Body Stub</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFunctionCall <em>Function Call</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFunctionDeclaration <em>Function Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFunctionInstantiation <em>Function Instantiation</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getFunctionRenamingDeclaration <em>Function Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getGeneralizedIteratorSpecification <em>Generalized Iterator Specification</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getGenericAssociation <em>Generic Association</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getGenericFunctionDeclaration <em>Generic Function Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getGenericFunctionRenamingDeclaration <em>Generic Function Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getGenericPackageDeclaration <em>Generic Package Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getGenericPackageRenamingDeclaration <em>Generic Package Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getGenericProcedureDeclaration <em>Generic Procedure Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getGenericProcedureRenamingDeclaration <em>Generic Procedure Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getGotoStatement <em>Goto Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getGreaterThanOperator <em>Greater Than Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getGreaterThanOrEqualOperator <em>Greater Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getIdentityAttribute <em>Identity Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getIfExpression <em>If Expression</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getIfExpressionPath <em>If Expression Path</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getIfPath <em>If Path</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getIfStatement <em>If Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getImageAttribute <em>Image Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getImplementationDefinedAttribute <em>Implementation Defined Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getImportPragma <em>Import Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getInMembershipTest <em>In Membership Test</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getIncompleteTypeDeclaration <em>Incomplete Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getIndependentComponentsPragma <em>Independent Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getIndependentPragma <em>Independent Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getIndexConstraint <em>Index Constraint</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getIndexedComponent <em>Indexed Component</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getInlinePragma <em>Inline Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getInputAttribute <em>Input Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getInspectionPointPragma <em>Inspection Point Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getIntegerLiteral <em>Integer Literal</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getIntegerNumberDeclaration <em>Integer Number Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getIsPrefixCall <em>Is Prefix Call</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getIsPrefixNotation <em>Is Prefix Notation</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getKnownDiscriminantPart <em>Known Discriminant Part</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getLastAttribute <em>Last Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getLastBitAttribute <em>Last Bit Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getLeadingPartAttribute <em>Leading Part Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getLengthAttribute <em>Length Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getLessThanOperator <em>Less Than Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getLessThanOrEqualOperator <em>Less Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getLimited <em>Limited</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getLimitedInterface <em>Limited Interface</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getLinkerOptionsPragma <em>Linker Options Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getListPragma <em>List Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getLockingPolicyPragma <em>Locking Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getLoopParameterSpecification <em>Loop Parameter Specification</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getLoopStatement <em>Loop Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getMachineAttribute <em>Machine Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getMachineEmaxAttribute <em>Machine Emax Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getMachineEminAttribute <em>Machine Emin Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getMachineMantissaAttribute <em>Machine Mantissa Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getMachineOverflowsAttribute <em>Machine Overflows Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getMachineRadixAttribute <em>Machine Radix Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getMachineRoundingAttribute <em>Machine Rounding Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getMachineRoundsAttribute <em>Machine Rounds Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getMaxAlignmentForAllocationAttribute <em>Max Alignment For Allocation Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getMaxAttribute <em>Max Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getMaxSizeInStorageElementsAttribute <em>Max Size In Storage Elements Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getMinAttribute <em>Min Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getMinusOperator <em>Minus Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getModAttribute <em>Mod Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getModOperator <em>Mod Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getModelAttribute <em>Model Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getModelEminAttribute <em>Model Emin Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getModelEpsilonAttribute <em>Model Epsilon Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getModelMantissaAttribute <em>Model Mantissa Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getModelSmallAttribute <em>Model Small Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getModularTypeDefinition <em>Modular Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getModulusAttribute <em>Modulus Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getMultiplyOperator <em>Multiply Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getNamedArrayAggregate <em>Named Array Aggregate</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getNoReturnPragma <em>No Return Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getNotAnElement <em>Not An Element</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getNotEqualOperator <em>Not Equal Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getNotInMembershipTest <em>Not In Membership Test</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getNotNullReturn <em>Not Null Return</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getNotOperator <em>Not Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getNotOverriding <em>Not Overriding</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getNullComponent <em>Null Component</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getNullExclusion <em>Null Exclusion</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getNullLiteral <em>Null Literal</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getNullProcedureDeclaration <em>Null Procedure Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getNullRecordDefinition <em>Null Record Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getNullStatement <em>Null Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getObjectRenamingDeclaration <em>Object Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getOptimizePragma <em>Optimize Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getOrElseShortCircuit <em>Or Else Short Circuit</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getOrOperator <em>Or Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getOrPath <em>Or Path</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getOrdinaryFixedPointDefinition <em>Ordinary Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getOrdinaryInterface <em>Ordinary Interface</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getOrdinaryTypeDeclaration <em>Ordinary Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getOthersChoice <em>Others Choice</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getOutputAttribute <em>Output Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getOverlapsStorageAttribute <em>Overlaps Storage Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getOverriding <em>Overriding</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPackPragma <em>Pack Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPackageBodyDeclaration <em>Package Body Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPackageBodyStub <em>Package Body Stub</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPackageDeclaration <em>Package Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPackageInstantiation <em>Package Instantiation</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPackageRenamingDeclaration <em>Package Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPagePragma <em>Page Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getParameterAssociation <em>Parameter Association</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getParameterSpecification <em>Parameter Specification</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getParenthesizedExpression <em>Parenthesized Expression</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPartitionIdAttribute <em>Partition Id Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPlusOperator <em>Plus Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPoolSpecificAccessToVariable <em>Pool Specific Access To Variable</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPosAttribute <em>Pos Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPositionAttribute <em>Position Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPositionalArrayAggregate <em>Positional Array Aggregate</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPragmaArgumentAssociation <em>Pragma Argument Association</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPredAttribute <em>Pred Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPreelaboratePragma <em>Preelaborate Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPriorityAttribute <em>Priority Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPriorityPragma <em>Priority Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPrivate <em>Private</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPrivateExtensionDeclaration <em>Private Extension Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPrivateExtensionDefinition <em>Private Extension Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPrivateTypeDeclaration <em>Private Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPrivateTypeDefinition <em>Private Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getProcedureBodyDeclaration <em>Procedure Body Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getProcedureBodyStub <em>Procedure Body Stub</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getProcedureCallStatement <em>Procedure Call Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getProcedureDeclaration <em>Procedure Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getProcedureInstantiation <em>Procedure Instantiation</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getProcedureRenamingDeclaration <em>Procedure Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getProfilePragma <em>Profile Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getProtectedBodyDeclaration <em>Protected Body Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getProtectedBodyStub <em>Protected Body Stub</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getProtectedDefinition <em>Protected Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getProtectedInterface <em>Protected Interface</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getProtectedTypeDeclaration <em>Protected Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getPurePragma <em>Pure Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getQualifiedExpression <em>Qualified Expression</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getRaiseExpression <em>Raise Expression</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getRaiseStatement <em>Raise Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getRangeAttribute <em>Range Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getRangeAttributeReference <em>Range Attribute Reference</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getReadAttribute <em>Read Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getRealLiteral <em>Real Literal</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getRealNumberDeclaration <em>Real Number Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getRecordAggregate <em>Record Aggregate</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getRecordComponentAssociation <em>Record Component Association</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getRecordDefinition <em>Record Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getRecordRepresentationClause <em>Record Representation Clause</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getRecordTypeDefinition <em>Record Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getRemOperator <em>Rem Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getRemainderAttribute <em>Remainder Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getRemoteTypesPragma <em>Remote Types Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getRequeueStatement <em>Requeue Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getRequeueStatementWithAbort <em>Requeue Statement With Abort</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getRestrictionsPragma <em>Restrictions Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getReturnConstantSpecification <em>Return Constant Specification</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getReturnStatement <em>Return Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getReturnVariableSpecification <em>Return Variable Specification</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getReverse <em>Reverse</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getReviewablePragma <em>Reviewable Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getRootIntegerDefinition <em>Root Integer Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getRootRealDefinition <em>Root Real Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getRoundAttribute <em>Round Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getRoundingAttribute <em>Rounding Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getSafeFirstAttribute <em>Safe First Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getSafeLastAttribute <em>Safe Last Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getScaleAttribute <em>Scale Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getScalingAttribute <em>Scaling Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getSelectPath <em>Select Path</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getSelectedComponent <em>Selected Component</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getSelectiveAcceptStatement <em>Selective Accept Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getSharedPassivePragma <em>Shared Passive Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getSignedIntegerTypeDefinition <em>Signed Integer Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getSignedZerosAttribute <em>Signed Zeros Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getSimpleExpressionRange <em>Simple Expression Range</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getSingleProtectedDeclaration <em>Single Protected Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getSingleTaskDeclaration <em>Single Task Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getSizeAttribute <em>Size Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getSlice <em>Slice</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getSmallAttribute <em>Small Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getStoragePoolAttribute <em>Storage Pool Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getStorageSizeAttribute <em>Storage Size Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getStorageSizePragma <em>Storage Size Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getStreamSizeAttribute <em>Stream Size Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getStringLiteral <em>String Literal</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getSubtypeDeclaration <em>Subtype Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getSubtypeIndication <em>Subtype Indication</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getSuccAttribute <em>Succ Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getSuppressPragma <em>Suppress Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getSynchronized <em>Synchronized</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getSynchronizedInterface <em>Synchronized Interface</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getTagAttribute <em>Tag Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getTagged <em>Tagged</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getTaggedIncompleteTypeDeclaration <em>Tagged Incomplete Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getTaggedPrivateTypeDefinition <em>Tagged Private Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getTaggedRecordTypeDefinition <em>Tagged Record Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getTaskBodyDeclaration <em>Task Body Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getTaskBodyStub <em>Task Body Stub</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getTaskDefinition <em>Task Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getTaskInterface <em>Task Interface</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getTaskTypeDeclaration <em>Task Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getTerminateAlternativeStatement <em>Terminate Alternative Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getTerminatedAttribute <em>Terminated Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getThenAbortPath <em>Then Abort Path</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getTimedEntryCallStatement <em>Timed Entry Call Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getTruncationAttribute <em>Truncation Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getTypeConversion <em>Type Conversion</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getUnaryMinusOperator <em>Unary Minus Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getUnaryPlusOperator <em>Unary Plus Operator</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getUnbiasedRoundingAttribute <em>Unbiased Rounding Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getUncheckedAccessAttribute <em>Unchecked Access Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getUnconstrainedArrayDefinition <em>Unconstrained Array Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getUniversalFixedDefinition <em>Universal Fixed Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getUniversalIntegerDefinition <em>Universal Integer Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getUniversalRealDefinition <em>Universal Real Definition</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getUnknownAttribute <em>Unknown Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getUnknownDiscriminantPart <em>Unknown Discriminant Part</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getUnknownPragma <em>Unknown Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getUnsuppressPragma <em>Unsuppress Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getUseAllTypeClause <em>Use All Type Clause</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getUsePackageClause <em>Use Package Clause</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getUseTypeClause <em>Use Type Clause</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getValAttribute <em>Val Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getValidAttribute <em>Valid Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getValueAttribute <em>Value Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getVariableDeclaration <em>Variable Declaration</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getVariant <em>Variant</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getVariantPart <em>Variant Part</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getVersionAttribute <em>Version Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getVolatilePragma <em>Volatile Pragma</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getWhileLoopStatement <em>While Loop Statement</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getWideImageAttribute <em>Wide Image Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getWideValueAttribute <em>Wide Value Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getWideWideImageAttribute <em>Wide Wide Image Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getWideWideValueAttribute <em>Wide Wide Value Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getWideWideWidthAttribute <em>Wide Wide Width Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getWideWidthAttribute <em>Wide Width Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getWidthAttribute <em>Width Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getWithClause <em>With Clause</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getWriteAttribute <em>Write Attribute</em>}</li>
 *   <li>{@link Ada.impl.DocumentRootImpl#getXorOperator <em>Xor Operator</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DocumentRootImpl extends MinimalEObjectImpl.Container implements DocumentRoot {
	/**
	 * The cached value of the '{@link #getMixed() <em>Mixed</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMixed()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap mixed;

	/**
	 * The cached value of the '{@link #getXMLNSPrefixMap() <em>XMLNS Prefix Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXMLNSPrefixMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> xMLNSPrefixMap;

	/**
	 * The cached value of the '{@link #getXSISchemaLocation() <em>XSI Schema Location</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXSISchemaLocation()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> xSISchemaLocation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DocumentRootImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getDocumentRoot();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getMixed() {
		if (mixed == null) {
			mixed = new BasicFeatureMap(this, AdaPackage.DOCUMENT_ROOT__MIXED);
		}
		return mixed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, String> getXMLNSPrefixMap() {
		if (xMLNSPrefixMap == null) {
			xMLNSPrefixMap = new EcoreEMap<String,String>(EcorePackage.Literals.ESTRING_TO_STRING_MAP_ENTRY, EStringToStringMapEntryImpl.class, this, AdaPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
		}
		return xMLNSPrefixMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, String> getXSISchemaLocation() {
		if (xSISchemaLocation == null) {
			xSISchemaLocation = new EcoreEMap<String,String>(EcorePackage.Literals.ESTRING_TO_STRING_MAP_ENTRY, EStringToStringMapEntryImpl.class, this, AdaPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
		}
		return xSISchemaLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbortStatement getAbortStatement() {
		return (AbortStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AbortStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAbortStatement(AbortStatement newAbortStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AbortStatement(), newAbortStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbortStatement(AbortStatement newAbortStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AbortStatement(), newAbortStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbsOperator getAbsOperator() {
		return (AbsOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AbsOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAbsOperator(AbsOperator newAbsOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AbsOperator(), newAbsOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbsOperator(AbsOperator newAbsOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AbsOperator(), newAbsOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Abstract getAbstract() {
		return (Abstract)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_Abstract(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAbstract(Abstract newAbstract, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_Abstract(), newAbstract, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstract(Abstract newAbstract) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_Abstract(), newAbstract);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcceptStatement getAcceptStatement() {
		return (AcceptStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AcceptStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcceptStatement(AcceptStatement newAcceptStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AcceptStatement(), newAcceptStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcceptStatement(AcceptStatement newAcceptStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AcceptStatement(), newAcceptStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessAttribute getAccessAttribute() {
		return (AccessAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AccessAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccessAttribute(AccessAttribute newAccessAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AccessAttribute(), newAccessAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessAttribute(AccessAttribute newAccessAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AccessAttribute(), newAccessAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessToConstant getAccessToConstant() {
		return (AccessToConstant)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AccessToConstant(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccessToConstant(AccessToConstant newAccessToConstant, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AccessToConstant(), newAccessToConstant, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessToConstant(AccessToConstant newAccessToConstant) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AccessToConstant(), newAccessToConstant);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessToFunction getAccessToFunction() {
		return (AccessToFunction)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AccessToFunction(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccessToFunction(AccessToFunction newAccessToFunction, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AccessToFunction(), newAccessToFunction, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessToFunction(AccessToFunction newAccessToFunction) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AccessToFunction(), newAccessToFunction);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessToProcedure getAccessToProcedure() {
		return (AccessToProcedure)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AccessToProcedure(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccessToProcedure(AccessToProcedure newAccessToProcedure, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AccessToProcedure(), newAccessToProcedure, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessToProcedure(AccessToProcedure newAccessToProcedure) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AccessToProcedure(), newAccessToProcedure);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessToProtectedFunction getAccessToProtectedFunction() {
		return (AccessToProtectedFunction)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AccessToProtectedFunction(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccessToProtectedFunction(AccessToProtectedFunction newAccessToProtectedFunction, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AccessToProtectedFunction(), newAccessToProtectedFunction, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessToProtectedFunction(AccessToProtectedFunction newAccessToProtectedFunction) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AccessToProtectedFunction(), newAccessToProtectedFunction);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessToProtectedProcedure getAccessToProtectedProcedure() {
		return (AccessToProtectedProcedure)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AccessToProtectedProcedure(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccessToProtectedProcedure(AccessToProtectedProcedure newAccessToProtectedProcedure, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AccessToProtectedProcedure(), newAccessToProtectedProcedure, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessToProtectedProcedure(AccessToProtectedProcedure newAccessToProtectedProcedure) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AccessToProtectedProcedure(), newAccessToProtectedProcedure);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessToVariable getAccessToVariable() {
		return (AccessToVariable)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AccessToVariable(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccessToVariable(AccessToVariable newAccessToVariable, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AccessToVariable(), newAccessToVariable, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessToVariable(AccessToVariable newAccessToVariable) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AccessToVariable(), newAccessToVariable);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AddressAttribute getAddressAttribute() {
		return (AddressAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AddressAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAddressAttribute(AddressAttribute newAddressAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AddressAttribute(), newAddressAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAddressAttribute(AddressAttribute newAddressAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AddressAttribute(), newAddressAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdjacentAttribute getAdjacentAttribute() {
		return (AdjacentAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AdjacentAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAdjacentAttribute(AdjacentAttribute newAdjacentAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AdjacentAttribute(), newAdjacentAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAdjacentAttribute(AdjacentAttribute newAdjacentAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AdjacentAttribute(), newAdjacentAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AftAttribute getAftAttribute() {
		return (AftAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AftAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAftAttribute(AftAttribute newAftAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AftAttribute(), newAftAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAftAttribute(AftAttribute newAftAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AftAttribute(), newAftAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Aliased getAliased() {
		return (Aliased)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_Aliased(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAliased(Aliased newAliased, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_Aliased(), newAliased, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAliased(Aliased newAliased) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_Aliased(), newAliased);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlignmentAttribute getAlignmentAttribute() {
		return (AlignmentAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AlignmentAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAlignmentAttribute(AlignmentAttribute newAlignmentAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AlignmentAttribute(), newAlignmentAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAlignmentAttribute(AlignmentAttribute newAlignmentAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AlignmentAttribute(), newAlignmentAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AllCallsRemotePragma getAllCallsRemotePragma() {
		return (AllCallsRemotePragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AllCallsRemotePragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAllCallsRemotePragma(AllCallsRemotePragma newAllCallsRemotePragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AllCallsRemotePragma(), newAllCallsRemotePragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAllCallsRemotePragma(AllCallsRemotePragma newAllCallsRemotePragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AllCallsRemotePragma(), newAllCallsRemotePragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AllocationFromQualifiedExpression getAllocationFromQualifiedExpression() {
		return (AllocationFromQualifiedExpression)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AllocationFromQualifiedExpression(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAllocationFromQualifiedExpression(AllocationFromQualifiedExpression newAllocationFromQualifiedExpression, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AllocationFromQualifiedExpression(), newAllocationFromQualifiedExpression, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAllocationFromQualifiedExpression(AllocationFromQualifiedExpression newAllocationFromQualifiedExpression) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AllocationFromQualifiedExpression(), newAllocationFromQualifiedExpression);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AllocationFromSubtype getAllocationFromSubtype() {
		return (AllocationFromSubtype)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AllocationFromSubtype(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAllocationFromSubtype(AllocationFromSubtype newAllocationFromSubtype, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AllocationFromSubtype(), newAllocationFromSubtype, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAllocationFromSubtype(AllocationFromSubtype newAllocationFromSubtype) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AllocationFromSubtype(), newAllocationFromSubtype);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AndOperator getAndOperator() {
		return (AndOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AndOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAndOperator(AndOperator newAndOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AndOperator(), newAndOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAndOperator(AndOperator newAndOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AndOperator(), newAndOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AndThenShortCircuit getAndThenShortCircuit() {
		return (AndThenShortCircuit)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AndThenShortCircuit(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAndThenShortCircuit(AndThenShortCircuit newAndThenShortCircuit, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AndThenShortCircuit(), newAndThenShortCircuit, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAndThenShortCircuit(AndThenShortCircuit newAndThenShortCircuit) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AndThenShortCircuit(), newAndThenShortCircuit);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnonymousAccessToConstant getAnonymousAccessToConstant() {
		return (AnonymousAccessToConstant)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AnonymousAccessToConstant(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnonymousAccessToConstant(AnonymousAccessToConstant newAnonymousAccessToConstant, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AnonymousAccessToConstant(), newAnonymousAccessToConstant, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnonymousAccessToConstant(AnonymousAccessToConstant newAnonymousAccessToConstant) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AnonymousAccessToConstant(), newAnonymousAccessToConstant);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnonymousAccessToFunction getAnonymousAccessToFunction() {
		return (AnonymousAccessToFunction)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AnonymousAccessToFunction(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnonymousAccessToFunction(AnonymousAccessToFunction newAnonymousAccessToFunction, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AnonymousAccessToFunction(), newAnonymousAccessToFunction, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnonymousAccessToFunction(AnonymousAccessToFunction newAnonymousAccessToFunction) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AnonymousAccessToFunction(), newAnonymousAccessToFunction);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnonymousAccessToProcedure getAnonymousAccessToProcedure() {
		return (AnonymousAccessToProcedure)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AnonymousAccessToProcedure(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnonymousAccessToProcedure(AnonymousAccessToProcedure newAnonymousAccessToProcedure, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AnonymousAccessToProcedure(), newAnonymousAccessToProcedure, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnonymousAccessToProcedure(AnonymousAccessToProcedure newAnonymousAccessToProcedure) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AnonymousAccessToProcedure(), newAnonymousAccessToProcedure);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnonymousAccessToProtectedFunction getAnonymousAccessToProtectedFunction() {
		return (AnonymousAccessToProtectedFunction)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AnonymousAccessToProtectedFunction(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnonymousAccessToProtectedFunction(AnonymousAccessToProtectedFunction newAnonymousAccessToProtectedFunction, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AnonymousAccessToProtectedFunction(), newAnonymousAccessToProtectedFunction, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnonymousAccessToProtectedFunction(AnonymousAccessToProtectedFunction newAnonymousAccessToProtectedFunction) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AnonymousAccessToProtectedFunction(), newAnonymousAccessToProtectedFunction);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnonymousAccessToProtectedProcedure getAnonymousAccessToProtectedProcedure() {
		return (AnonymousAccessToProtectedProcedure)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AnonymousAccessToProtectedProcedure(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnonymousAccessToProtectedProcedure(AnonymousAccessToProtectedProcedure newAnonymousAccessToProtectedProcedure, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AnonymousAccessToProtectedProcedure(), newAnonymousAccessToProtectedProcedure, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnonymousAccessToProtectedProcedure(AnonymousAccessToProtectedProcedure newAnonymousAccessToProtectedProcedure) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AnonymousAccessToProtectedProcedure(), newAnonymousAccessToProtectedProcedure);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnonymousAccessToVariable getAnonymousAccessToVariable() {
		return (AnonymousAccessToVariable)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AnonymousAccessToVariable(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnonymousAccessToVariable(AnonymousAccessToVariable newAnonymousAccessToVariable, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AnonymousAccessToVariable(), newAnonymousAccessToVariable, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnonymousAccessToVariable(AnonymousAccessToVariable newAnonymousAccessToVariable) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AnonymousAccessToVariable(), newAnonymousAccessToVariable);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArrayComponentAssociation getArrayComponentAssociation() {
		return (ArrayComponentAssociation)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ArrayComponentAssociation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetArrayComponentAssociation(ArrayComponentAssociation newArrayComponentAssociation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ArrayComponentAssociation(), newArrayComponentAssociation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setArrayComponentAssociation(ArrayComponentAssociation newArrayComponentAssociation) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ArrayComponentAssociation(), newArrayComponentAssociation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AspectSpecification getAspectSpecification() {
		return (AspectSpecification)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AspectSpecification(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAspectSpecification(AspectSpecification newAspectSpecification, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AspectSpecification(), newAspectSpecification, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAspectSpecification(AspectSpecification newAspectSpecification) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AspectSpecification(), newAspectSpecification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertPragma getAssertPragma() {
		return (AssertPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AssertPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssertPragma(AssertPragma newAssertPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AssertPragma(), newAssertPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssertPragma(AssertPragma newAssertPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AssertPragma(), newAssertPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertionPolicyPragma getAssertionPolicyPragma() {
		return (AssertionPolicyPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AssertionPolicyPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssertionPolicyPragma(AssertionPolicyPragma newAssertionPolicyPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AssertionPolicyPragma(), newAssertionPolicyPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssertionPolicyPragma(AssertionPolicyPragma newAssertionPolicyPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AssertionPolicyPragma(), newAssertionPolicyPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssignmentStatement getAssignmentStatement() {
		return (AssignmentStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AssignmentStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssignmentStatement(AssignmentStatement newAssignmentStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AssignmentStatement(), newAssignmentStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssignmentStatement(AssignmentStatement newAssignmentStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AssignmentStatement(), newAssignmentStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AsynchronousPragma getAsynchronousPragma() {
		return (AsynchronousPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AsynchronousPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAsynchronousPragma(AsynchronousPragma newAsynchronousPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AsynchronousPragma(), newAsynchronousPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAsynchronousPragma(AsynchronousPragma newAsynchronousPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AsynchronousPragma(), newAsynchronousPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AsynchronousSelectStatement getAsynchronousSelectStatement() {
		return (AsynchronousSelectStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AsynchronousSelectStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAsynchronousSelectStatement(AsynchronousSelectStatement newAsynchronousSelectStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AsynchronousSelectStatement(), newAsynchronousSelectStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAsynchronousSelectStatement(AsynchronousSelectStatement newAsynchronousSelectStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AsynchronousSelectStatement(), newAsynchronousSelectStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtClause getAtClause() {
		return (AtClause)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AtClause(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAtClause(AtClause newAtClause, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AtClause(), newAtClause, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAtClause(AtClause newAtClause) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AtClause(), newAtClause);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtomicComponentsPragma getAtomicComponentsPragma() {
		return (AtomicComponentsPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AtomicComponentsPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAtomicComponentsPragma(AtomicComponentsPragma newAtomicComponentsPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AtomicComponentsPragma(), newAtomicComponentsPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAtomicComponentsPragma(AtomicComponentsPragma newAtomicComponentsPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AtomicComponentsPragma(), newAtomicComponentsPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtomicPragma getAtomicPragma() {
		return (AtomicPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AtomicPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAtomicPragma(AtomicPragma newAtomicPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AtomicPragma(), newAtomicPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAtomicPragma(AtomicPragma newAtomicPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AtomicPragma(), newAtomicPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttachHandlerPragma getAttachHandlerPragma() {
		return (AttachHandlerPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AttachHandlerPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAttachHandlerPragma(AttachHandlerPragma newAttachHandlerPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AttachHandlerPragma(), newAttachHandlerPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttachHandlerPragma(AttachHandlerPragma newAttachHandlerPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AttachHandlerPragma(), newAttachHandlerPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeDefinitionClause getAttributeDefinitionClause() {
		return (AttributeDefinitionClause)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_AttributeDefinitionClause(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAttributeDefinitionClause(AttributeDefinitionClause newAttributeDefinitionClause, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_AttributeDefinitionClause(), newAttributeDefinitionClause, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttributeDefinitionClause(AttributeDefinitionClause newAttributeDefinitionClause) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_AttributeDefinitionClause(), newAttributeDefinitionClause);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseAttribute getBaseAttribute() {
		return (BaseAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_BaseAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBaseAttribute(BaseAttribute newBaseAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_BaseAttribute(), newBaseAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBaseAttribute(BaseAttribute newBaseAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_BaseAttribute(), newBaseAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BitOrderAttribute getBitOrderAttribute() {
		return (BitOrderAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_BitOrderAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBitOrderAttribute(BitOrderAttribute newBitOrderAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_BitOrderAttribute(), newBitOrderAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBitOrderAttribute(BitOrderAttribute newBitOrderAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_BitOrderAttribute(), newBitOrderAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlockStatement getBlockStatement() {
		return (BlockStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_BlockStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBlockStatement(BlockStatement newBlockStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_BlockStatement(), newBlockStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBlockStatement(BlockStatement newBlockStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_BlockStatement(), newBlockStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BodyVersionAttribute getBodyVersionAttribute() {
		return (BodyVersionAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_BodyVersionAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBodyVersionAttribute(BodyVersionAttribute newBodyVersionAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_BodyVersionAttribute(), newBodyVersionAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBodyVersionAttribute(BodyVersionAttribute newBodyVersionAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_BodyVersionAttribute(), newBodyVersionAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BoxExpression getBoxExpression() {
		return (BoxExpression)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_BoxExpression(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBoxExpression(BoxExpression newBoxExpression, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_BoxExpression(), newBoxExpression, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBoxExpression(BoxExpression newBoxExpression) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_BoxExpression(), newBoxExpression);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallableAttribute getCallableAttribute() {
		return (CallableAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_CallableAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCallableAttribute(CallableAttribute newCallableAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_CallableAttribute(), newCallableAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCallableAttribute(CallableAttribute newCallableAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_CallableAttribute(), newCallableAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallerAttribute getCallerAttribute() {
		return (CallerAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_CallerAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCallerAttribute(CallerAttribute newCallerAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_CallerAttribute(), newCallerAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCallerAttribute(CallerAttribute newCallerAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_CallerAttribute(), newCallerAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CaseExpression getCaseExpression() {
		return (CaseExpression)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_CaseExpression(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCaseExpression(CaseExpression newCaseExpression, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_CaseExpression(), newCaseExpression, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCaseExpression(CaseExpression newCaseExpression) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_CaseExpression(), newCaseExpression);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CaseExpressionPath getCaseExpressionPath() {
		return (CaseExpressionPath)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_CaseExpressionPath(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCaseExpressionPath(CaseExpressionPath newCaseExpressionPath, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_CaseExpressionPath(), newCaseExpressionPath, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCaseExpressionPath(CaseExpressionPath newCaseExpressionPath) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_CaseExpressionPath(), newCaseExpressionPath);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CasePath getCasePath() {
		return (CasePath)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_CasePath(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCasePath(CasePath newCasePath, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_CasePath(), newCasePath, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCasePath(CasePath newCasePath) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_CasePath(), newCasePath);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CaseStatement getCaseStatement() {
		return (CaseStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_CaseStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCaseStatement(CaseStatement newCaseStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_CaseStatement(), newCaseStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCaseStatement(CaseStatement newCaseStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_CaseStatement(), newCaseStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CeilingAttribute getCeilingAttribute() {
		return (CeilingAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_CeilingAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCeilingAttribute(CeilingAttribute newCeilingAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_CeilingAttribute(), newCeilingAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCeilingAttribute(CeilingAttribute newCeilingAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_CeilingAttribute(), newCeilingAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CharacterLiteral getCharacterLiteral() {
		return (CharacterLiteral)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_CharacterLiteral(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCharacterLiteral(CharacterLiteral newCharacterLiteral, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_CharacterLiteral(), newCharacterLiteral, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCharacterLiteral(CharacterLiteral newCharacterLiteral) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_CharacterLiteral(), newCharacterLiteral);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChoiceParameterSpecification getChoiceParameterSpecification() {
		return (ChoiceParameterSpecification)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ChoiceParameterSpecification(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChoiceParameterSpecification(ChoiceParameterSpecification newChoiceParameterSpecification, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ChoiceParameterSpecification(), newChoiceParameterSpecification, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChoiceParameterSpecification(ChoiceParameterSpecification newChoiceParameterSpecification) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ChoiceParameterSpecification(), newChoiceParameterSpecification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassAttribute getClassAttribute() {
		return (ClassAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ClassAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClassAttribute(ClassAttribute newClassAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ClassAttribute(), newClassAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassAttribute(ClassAttribute newClassAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ClassAttribute(), newClassAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeStatement getCodeStatement() {
		return (CodeStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_CodeStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCodeStatement(CodeStatement newCodeStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_CodeStatement(), newCodeStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCodeStatement(CodeStatement newCodeStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_CodeStatement(), newCodeStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Comment getComment() {
		return (Comment)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_Comment(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComment(Comment newComment, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_Comment(), newComment, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComment(Comment newComment) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_Comment(), newComment);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompilationUnit getCompilationUnit() {
		return (CompilationUnit)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_CompilationUnit(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCompilationUnit(CompilationUnit newCompilationUnit, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_CompilationUnit(), newCompilationUnit, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCompilationUnit(CompilationUnit newCompilationUnit) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_CompilationUnit(), newCompilationUnit);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentClause getComponentClause() {
		return (ComponentClause)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ComponentClause(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComponentClause(ComponentClause newComponentClause, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ComponentClause(), newComponentClause, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponentClause(ComponentClause newComponentClause) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ComponentClause(), newComponentClause);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentDeclaration getComponentDeclaration() {
		return (ComponentDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ComponentDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComponentDeclaration(ComponentDeclaration newComponentDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ComponentDeclaration(), newComponentDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponentDeclaration(ComponentDeclaration newComponentDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ComponentDeclaration(), newComponentDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentDefinition getComponentDefinition() {
		return (ComponentDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ComponentDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComponentDefinition(ComponentDefinition newComponentDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ComponentDefinition(), newComponentDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponentDefinition(ComponentDefinition newComponentDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ComponentDefinition(), newComponentDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentSizeAttribute getComponentSizeAttribute() {
		return (ComponentSizeAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ComponentSizeAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComponentSizeAttribute(ComponentSizeAttribute newComponentSizeAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ComponentSizeAttribute(), newComponentSizeAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponentSizeAttribute(ComponentSizeAttribute newComponentSizeAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ComponentSizeAttribute(), newComponentSizeAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComposeAttribute getComposeAttribute() {
		return (ComposeAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ComposeAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComposeAttribute(ComposeAttribute newComposeAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ComposeAttribute(), newComposeAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComposeAttribute(ComposeAttribute newComposeAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ComposeAttribute(), newComposeAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConcatenateOperator getConcatenateOperator() {
		return (ConcatenateOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ConcatenateOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConcatenateOperator(ConcatenateOperator newConcatenateOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ConcatenateOperator(), newConcatenateOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConcatenateOperator(ConcatenateOperator newConcatenateOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ConcatenateOperator(), newConcatenateOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionalEntryCallStatement getConditionalEntryCallStatement() {
		return (ConditionalEntryCallStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ConditionalEntryCallStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConditionalEntryCallStatement(ConditionalEntryCallStatement newConditionalEntryCallStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ConditionalEntryCallStatement(), newConditionalEntryCallStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConditionalEntryCallStatement(ConditionalEntryCallStatement newConditionalEntryCallStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ConditionalEntryCallStatement(), newConditionalEntryCallStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstantDeclaration getConstantDeclaration() {
		return (ConstantDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ConstantDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConstantDeclaration(ConstantDeclaration newConstantDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ConstantDeclaration(), newConstantDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstantDeclaration(ConstantDeclaration newConstantDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ConstantDeclaration(), newConstantDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstrainedArrayDefinition getConstrainedArrayDefinition() {
		return (ConstrainedArrayDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ConstrainedArrayDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConstrainedArrayDefinition(ConstrainedArrayDefinition newConstrainedArrayDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ConstrainedArrayDefinition(), newConstrainedArrayDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstrainedArrayDefinition(ConstrainedArrayDefinition newConstrainedArrayDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ConstrainedArrayDefinition(), newConstrainedArrayDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstrainedAttribute getConstrainedAttribute() {
		return (ConstrainedAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ConstrainedAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConstrainedAttribute(ConstrainedAttribute newConstrainedAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ConstrainedAttribute(), newConstrainedAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstrainedAttribute(ConstrainedAttribute newConstrainedAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ConstrainedAttribute(), newConstrainedAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlledPragma getControlledPragma() {
		return (ControlledPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ControlledPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetControlledPragma(ControlledPragma newControlledPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ControlledPragma(), newControlledPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setControlledPragma(ControlledPragma newControlledPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ControlledPragma(), newControlledPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConventionPragma getConventionPragma() {
		return (ConventionPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ConventionPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConventionPragma(ConventionPragma newConventionPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ConventionPragma(), newConventionPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConventionPragma(ConventionPragma newConventionPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ConventionPragma(), newConventionPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CopySignAttribute getCopySignAttribute() {
		return (CopySignAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_CopySignAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCopySignAttribute(CopySignAttribute newCopySignAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_CopySignAttribute(), newCopySignAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCopySignAttribute(CopySignAttribute newCopySignAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_CopySignAttribute(), newCopySignAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CountAttribute getCountAttribute() {
		return (CountAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_CountAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCountAttribute(CountAttribute newCountAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_CountAttribute(), newCountAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCountAttribute(CountAttribute newCountAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_CountAttribute(), newCountAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CpuPragma getCpuPragma() {
		return (CpuPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_CpuPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCpuPragma(CpuPragma newCpuPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_CpuPragma(), newCpuPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCpuPragma(CpuPragma newCpuPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_CpuPragma(), newCpuPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DecimalFixedPointDefinition getDecimalFixedPointDefinition() {
		return (DecimalFixedPointDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DecimalFixedPointDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDecimalFixedPointDefinition(DecimalFixedPointDefinition newDecimalFixedPointDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DecimalFixedPointDefinition(), newDecimalFixedPointDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDecimalFixedPointDefinition(DecimalFixedPointDefinition newDecimalFixedPointDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DecimalFixedPointDefinition(), newDecimalFixedPointDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefaultStoragePoolPragma getDefaultStoragePoolPragma() {
		return (DefaultStoragePoolPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefaultStoragePoolPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefaultStoragePoolPragma(DefaultStoragePoolPragma newDefaultStoragePoolPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefaultStoragePoolPragma(), newDefaultStoragePoolPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultStoragePoolPragma(DefaultStoragePoolPragma newDefaultStoragePoolPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefaultStoragePoolPragma(), newDefaultStoragePoolPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeferredConstantDeclaration getDeferredConstantDeclaration() {
		return (DeferredConstantDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DeferredConstantDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDeferredConstantDeclaration(DeferredConstantDeclaration newDeferredConstantDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DeferredConstantDeclaration(), newDeferredConstantDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeferredConstantDeclaration(DeferredConstantDeclaration newDeferredConstantDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DeferredConstantDeclaration(), newDeferredConstantDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningAbsOperator getDefiningAbsOperator() {
		return (DefiningAbsOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiningAbsOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningAbsOperator(DefiningAbsOperator newDefiningAbsOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiningAbsOperator(), newDefiningAbsOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningAbsOperator(DefiningAbsOperator newDefiningAbsOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiningAbsOperator(), newDefiningAbsOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningAndOperator getDefiningAndOperator() {
		return (DefiningAndOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiningAndOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningAndOperator(DefiningAndOperator newDefiningAndOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiningAndOperator(), newDefiningAndOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningAndOperator(DefiningAndOperator newDefiningAndOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiningAndOperator(), newDefiningAndOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningCharacterLiteral getDefiningCharacterLiteral() {
		return (DefiningCharacterLiteral)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiningCharacterLiteral(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningCharacterLiteral(DefiningCharacterLiteral newDefiningCharacterLiteral, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiningCharacterLiteral(), newDefiningCharacterLiteral, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningCharacterLiteral(DefiningCharacterLiteral newDefiningCharacterLiteral) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiningCharacterLiteral(), newDefiningCharacterLiteral);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningConcatenateOperator getDefiningConcatenateOperator() {
		return (DefiningConcatenateOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiningConcatenateOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningConcatenateOperator(DefiningConcatenateOperator newDefiningConcatenateOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiningConcatenateOperator(), newDefiningConcatenateOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningConcatenateOperator(DefiningConcatenateOperator newDefiningConcatenateOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiningConcatenateOperator(), newDefiningConcatenateOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningDivideOperator getDefiningDivideOperator() {
		return (DefiningDivideOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiningDivideOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningDivideOperator(DefiningDivideOperator newDefiningDivideOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiningDivideOperator(), newDefiningDivideOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningDivideOperator(DefiningDivideOperator newDefiningDivideOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiningDivideOperator(), newDefiningDivideOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningEnumerationLiteral getDefiningEnumerationLiteral() {
		return (DefiningEnumerationLiteral)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiningEnumerationLiteral(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningEnumerationLiteral(DefiningEnumerationLiteral newDefiningEnumerationLiteral, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiningEnumerationLiteral(), newDefiningEnumerationLiteral, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningEnumerationLiteral(DefiningEnumerationLiteral newDefiningEnumerationLiteral) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiningEnumerationLiteral(), newDefiningEnumerationLiteral);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningEqualOperator getDefiningEqualOperator() {
		return (DefiningEqualOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiningEqualOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningEqualOperator(DefiningEqualOperator newDefiningEqualOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiningEqualOperator(), newDefiningEqualOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningEqualOperator(DefiningEqualOperator newDefiningEqualOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiningEqualOperator(), newDefiningEqualOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningExpandedName getDefiningExpandedName() {
		return (DefiningExpandedName)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiningExpandedName(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningExpandedName(DefiningExpandedName newDefiningExpandedName, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiningExpandedName(), newDefiningExpandedName, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningExpandedName(DefiningExpandedName newDefiningExpandedName) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiningExpandedName(), newDefiningExpandedName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningExponentiateOperator getDefiningExponentiateOperator() {
		return (DefiningExponentiateOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiningExponentiateOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningExponentiateOperator(DefiningExponentiateOperator newDefiningExponentiateOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiningExponentiateOperator(), newDefiningExponentiateOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningExponentiateOperator(DefiningExponentiateOperator newDefiningExponentiateOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiningExponentiateOperator(), newDefiningExponentiateOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningGreaterThanOperator getDefiningGreaterThanOperator() {
		return (DefiningGreaterThanOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiningGreaterThanOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningGreaterThanOperator(DefiningGreaterThanOperator newDefiningGreaterThanOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiningGreaterThanOperator(), newDefiningGreaterThanOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningGreaterThanOperator(DefiningGreaterThanOperator newDefiningGreaterThanOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiningGreaterThanOperator(), newDefiningGreaterThanOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningGreaterThanOrEqualOperator getDefiningGreaterThanOrEqualOperator() {
		return (DefiningGreaterThanOrEqualOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiningGreaterThanOrEqualOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningGreaterThanOrEqualOperator(DefiningGreaterThanOrEqualOperator newDefiningGreaterThanOrEqualOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiningGreaterThanOrEqualOperator(), newDefiningGreaterThanOrEqualOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningGreaterThanOrEqualOperator(DefiningGreaterThanOrEqualOperator newDefiningGreaterThanOrEqualOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiningGreaterThanOrEqualOperator(), newDefiningGreaterThanOrEqualOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningIdentifier getDefiningIdentifier() {
		return (DefiningIdentifier)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiningIdentifier(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningIdentifier(DefiningIdentifier newDefiningIdentifier, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiningIdentifier(), newDefiningIdentifier, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningIdentifier(DefiningIdentifier newDefiningIdentifier) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiningIdentifier(), newDefiningIdentifier);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningLessThanOperator getDefiningLessThanOperator() {
		return (DefiningLessThanOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiningLessThanOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningLessThanOperator(DefiningLessThanOperator newDefiningLessThanOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiningLessThanOperator(), newDefiningLessThanOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningLessThanOperator(DefiningLessThanOperator newDefiningLessThanOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiningLessThanOperator(), newDefiningLessThanOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningLessThanOrEqualOperator getDefiningLessThanOrEqualOperator() {
		return (DefiningLessThanOrEqualOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiningLessThanOrEqualOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningLessThanOrEqualOperator(DefiningLessThanOrEqualOperator newDefiningLessThanOrEqualOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiningLessThanOrEqualOperator(), newDefiningLessThanOrEqualOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningLessThanOrEqualOperator(DefiningLessThanOrEqualOperator newDefiningLessThanOrEqualOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiningLessThanOrEqualOperator(), newDefiningLessThanOrEqualOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningMinusOperator getDefiningMinusOperator() {
		return (DefiningMinusOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiningMinusOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningMinusOperator(DefiningMinusOperator newDefiningMinusOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiningMinusOperator(), newDefiningMinusOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningMinusOperator(DefiningMinusOperator newDefiningMinusOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiningMinusOperator(), newDefiningMinusOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningModOperator getDefiningModOperator() {
		return (DefiningModOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiningModOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningModOperator(DefiningModOperator newDefiningModOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiningModOperator(), newDefiningModOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningModOperator(DefiningModOperator newDefiningModOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiningModOperator(), newDefiningModOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningMultiplyOperator getDefiningMultiplyOperator() {
		return (DefiningMultiplyOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiningMultiplyOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningMultiplyOperator(DefiningMultiplyOperator newDefiningMultiplyOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiningMultiplyOperator(), newDefiningMultiplyOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningMultiplyOperator(DefiningMultiplyOperator newDefiningMultiplyOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiningMultiplyOperator(), newDefiningMultiplyOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNotEqualOperator getDefiningNotEqualOperator() {
		return (DefiningNotEqualOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiningNotEqualOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningNotEqualOperator(DefiningNotEqualOperator newDefiningNotEqualOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiningNotEqualOperator(), newDefiningNotEqualOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningNotEqualOperator(DefiningNotEqualOperator newDefiningNotEqualOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiningNotEqualOperator(), newDefiningNotEqualOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNotOperator getDefiningNotOperator() {
		return (DefiningNotOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiningNotOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningNotOperator(DefiningNotOperator newDefiningNotOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiningNotOperator(), newDefiningNotOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningNotOperator(DefiningNotOperator newDefiningNotOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiningNotOperator(), newDefiningNotOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningOrOperator getDefiningOrOperator() {
		return (DefiningOrOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiningOrOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningOrOperator(DefiningOrOperator newDefiningOrOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiningOrOperator(), newDefiningOrOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningOrOperator(DefiningOrOperator newDefiningOrOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiningOrOperator(), newDefiningOrOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningPlusOperator getDefiningPlusOperator() {
		return (DefiningPlusOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiningPlusOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningPlusOperator(DefiningPlusOperator newDefiningPlusOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiningPlusOperator(), newDefiningPlusOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningPlusOperator(DefiningPlusOperator newDefiningPlusOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiningPlusOperator(), newDefiningPlusOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningRemOperator getDefiningRemOperator() {
		return (DefiningRemOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiningRemOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningRemOperator(DefiningRemOperator newDefiningRemOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiningRemOperator(), newDefiningRemOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningRemOperator(DefiningRemOperator newDefiningRemOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiningRemOperator(), newDefiningRemOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningUnaryMinusOperator getDefiningUnaryMinusOperator() {
		return (DefiningUnaryMinusOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiningUnaryMinusOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningUnaryMinusOperator(DefiningUnaryMinusOperator newDefiningUnaryMinusOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiningUnaryMinusOperator(), newDefiningUnaryMinusOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningUnaryMinusOperator(DefiningUnaryMinusOperator newDefiningUnaryMinusOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiningUnaryMinusOperator(), newDefiningUnaryMinusOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningUnaryPlusOperator getDefiningUnaryPlusOperator() {
		return (DefiningUnaryPlusOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiningUnaryPlusOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningUnaryPlusOperator(DefiningUnaryPlusOperator newDefiningUnaryPlusOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiningUnaryPlusOperator(), newDefiningUnaryPlusOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningUnaryPlusOperator(DefiningUnaryPlusOperator newDefiningUnaryPlusOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiningUnaryPlusOperator(), newDefiningUnaryPlusOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningXorOperator getDefiningXorOperator() {
		return (DefiningXorOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiningXorOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningXorOperator(DefiningXorOperator newDefiningXorOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiningXorOperator(), newDefiningXorOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningXorOperator(DefiningXorOperator newDefiningXorOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiningXorOperator(), newDefiningXorOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiniteAttribute getDefiniteAttribute() {
		return (DefiniteAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DefiniteAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiniteAttribute(DefiniteAttribute newDefiniteAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DefiniteAttribute(), newDefiniteAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiniteAttribute(DefiniteAttribute newDefiniteAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DefiniteAttribute(), newDefiniteAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DelayRelativeStatement getDelayRelativeStatement() {
		return (DelayRelativeStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DelayRelativeStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDelayRelativeStatement(DelayRelativeStatement newDelayRelativeStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DelayRelativeStatement(), newDelayRelativeStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDelayRelativeStatement(DelayRelativeStatement newDelayRelativeStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DelayRelativeStatement(), newDelayRelativeStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DelayUntilStatement getDelayUntilStatement() {
		return (DelayUntilStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DelayUntilStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDelayUntilStatement(DelayUntilStatement newDelayUntilStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DelayUntilStatement(), newDelayUntilStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDelayUntilStatement(DelayUntilStatement newDelayUntilStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DelayUntilStatement(), newDelayUntilStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeltaAttribute getDeltaAttribute() {
		return (DeltaAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DeltaAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDeltaAttribute(DeltaAttribute newDeltaAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DeltaAttribute(), newDeltaAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeltaAttribute(DeltaAttribute newDeltaAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DeltaAttribute(), newDeltaAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeltaConstraint getDeltaConstraint() {
		return (DeltaConstraint)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DeltaConstraint(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDeltaConstraint(DeltaConstraint newDeltaConstraint, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DeltaConstraint(), newDeltaConstraint, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeltaConstraint(DeltaConstraint newDeltaConstraint) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DeltaConstraint(), newDeltaConstraint);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DenormAttribute getDenormAttribute() {
		return (DenormAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DenormAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDenormAttribute(DenormAttribute newDenormAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DenormAttribute(), newDenormAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDenormAttribute(DenormAttribute newDenormAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DenormAttribute(), newDenormAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DerivedRecordExtensionDefinition getDerivedRecordExtensionDefinition() {
		return (DerivedRecordExtensionDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DerivedRecordExtensionDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDerivedRecordExtensionDefinition(DerivedRecordExtensionDefinition newDerivedRecordExtensionDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DerivedRecordExtensionDefinition(), newDerivedRecordExtensionDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDerivedRecordExtensionDefinition(DerivedRecordExtensionDefinition newDerivedRecordExtensionDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DerivedRecordExtensionDefinition(), newDerivedRecordExtensionDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DerivedTypeDefinition getDerivedTypeDefinition() {
		return (DerivedTypeDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DerivedTypeDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDerivedTypeDefinition(DerivedTypeDefinition newDerivedTypeDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DerivedTypeDefinition(), newDerivedTypeDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDerivedTypeDefinition(DerivedTypeDefinition newDerivedTypeDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DerivedTypeDefinition(), newDerivedTypeDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DetectBlockingPragma getDetectBlockingPragma() {
		return (DetectBlockingPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DetectBlockingPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDetectBlockingPragma(DetectBlockingPragma newDetectBlockingPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DetectBlockingPragma(), newDetectBlockingPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDetectBlockingPragma(DetectBlockingPragma newDetectBlockingPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DetectBlockingPragma(), newDetectBlockingPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DigitsAttribute getDigitsAttribute() {
		return (DigitsAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DigitsAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDigitsAttribute(DigitsAttribute newDigitsAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DigitsAttribute(), newDigitsAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDigitsAttribute(DigitsAttribute newDigitsAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DigitsAttribute(), newDigitsAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DigitsConstraint getDigitsConstraint() {
		return (DigitsConstraint)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DigitsConstraint(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDigitsConstraint(DigitsConstraint newDigitsConstraint, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DigitsConstraint(), newDigitsConstraint, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDigitsConstraint(DigitsConstraint newDigitsConstraint) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DigitsConstraint(), newDigitsConstraint);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscardNamesPragma getDiscardNamesPragma() {
		return (DiscardNamesPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DiscardNamesPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscardNamesPragma(DiscardNamesPragma newDiscardNamesPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DiscardNamesPragma(), newDiscardNamesPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscardNamesPragma(DiscardNamesPragma newDiscardNamesPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DiscardNamesPragma(), newDiscardNamesPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscreteRangeAttributeReference getDiscreteRangeAttributeReference() {
		return (DiscreteRangeAttributeReference)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DiscreteRangeAttributeReference(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscreteRangeAttributeReference(DiscreteRangeAttributeReference newDiscreteRangeAttributeReference, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DiscreteRangeAttributeReference(), newDiscreteRangeAttributeReference, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscreteRangeAttributeReference(DiscreteRangeAttributeReference newDiscreteRangeAttributeReference) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DiscreteRangeAttributeReference(), newDiscreteRangeAttributeReference);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscreteRangeAttributeReferenceAsSubtypeDefinition getDiscreteRangeAttributeReferenceAsSubtypeDefinition() {
		return (DiscreteRangeAttributeReferenceAsSubtypeDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DiscreteRangeAttributeReferenceAsSubtypeDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscreteRangeAttributeReferenceAsSubtypeDefinition(DiscreteRangeAttributeReferenceAsSubtypeDefinition newDiscreteRangeAttributeReferenceAsSubtypeDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DiscreteRangeAttributeReferenceAsSubtypeDefinition(), newDiscreteRangeAttributeReferenceAsSubtypeDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscreteRangeAttributeReferenceAsSubtypeDefinition(DiscreteRangeAttributeReferenceAsSubtypeDefinition newDiscreteRangeAttributeReferenceAsSubtypeDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DiscreteRangeAttributeReferenceAsSubtypeDefinition(), newDiscreteRangeAttributeReferenceAsSubtypeDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscreteSimpleExpressionRange getDiscreteSimpleExpressionRange() {
		return (DiscreteSimpleExpressionRange)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DiscreteSimpleExpressionRange(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscreteSimpleExpressionRange(DiscreteSimpleExpressionRange newDiscreteSimpleExpressionRange, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DiscreteSimpleExpressionRange(), newDiscreteSimpleExpressionRange, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscreteSimpleExpressionRange(DiscreteSimpleExpressionRange newDiscreteSimpleExpressionRange) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DiscreteSimpleExpressionRange(), newDiscreteSimpleExpressionRange);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscreteSimpleExpressionRangeAsSubtypeDefinition getDiscreteSimpleExpressionRangeAsSubtypeDefinition() {
		return (DiscreteSimpleExpressionRangeAsSubtypeDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DiscreteSimpleExpressionRangeAsSubtypeDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscreteSimpleExpressionRangeAsSubtypeDefinition(DiscreteSimpleExpressionRangeAsSubtypeDefinition newDiscreteSimpleExpressionRangeAsSubtypeDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DiscreteSimpleExpressionRangeAsSubtypeDefinition(), newDiscreteSimpleExpressionRangeAsSubtypeDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscreteSimpleExpressionRangeAsSubtypeDefinition(DiscreteSimpleExpressionRangeAsSubtypeDefinition newDiscreteSimpleExpressionRangeAsSubtypeDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DiscreteSimpleExpressionRangeAsSubtypeDefinition(), newDiscreteSimpleExpressionRangeAsSubtypeDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscreteSubtypeIndication getDiscreteSubtypeIndication() {
		return (DiscreteSubtypeIndication)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DiscreteSubtypeIndication(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscreteSubtypeIndication(DiscreteSubtypeIndication newDiscreteSubtypeIndication, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DiscreteSubtypeIndication(), newDiscreteSubtypeIndication, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscreteSubtypeIndication(DiscreteSubtypeIndication newDiscreteSubtypeIndication) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DiscreteSubtypeIndication(), newDiscreteSubtypeIndication);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscreteSubtypeIndicationAsSubtypeDefinition getDiscreteSubtypeIndicationAsSubtypeDefinition() {
		return (DiscreteSubtypeIndicationAsSubtypeDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DiscreteSubtypeIndicationAsSubtypeDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscreteSubtypeIndicationAsSubtypeDefinition(DiscreteSubtypeIndicationAsSubtypeDefinition newDiscreteSubtypeIndicationAsSubtypeDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DiscreteSubtypeIndicationAsSubtypeDefinition(), newDiscreteSubtypeIndicationAsSubtypeDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscreteSubtypeIndicationAsSubtypeDefinition(DiscreteSubtypeIndicationAsSubtypeDefinition newDiscreteSubtypeIndicationAsSubtypeDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DiscreteSubtypeIndicationAsSubtypeDefinition(), newDiscreteSubtypeIndicationAsSubtypeDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscriminantAssociation getDiscriminantAssociation() {
		return (DiscriminantAssociation)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DiscriminantAssociation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscriminantAssociation(DiscriminantAssociation newDiscriminantAssociation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DiscriminantAssociation(), newDiscriminantAssociation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscriminantAssociation(DiscriminantAssociation newDiscriminantAssociation) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DiscriminantAssociation(), newDiscriminantAssociation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscriminantConstraint getDiscriminantConstraint() {
		return (DiscriminantConstraint)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DiscriminantConstraint(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscriminantConstraint(DiscriminantConstraint newDiscriminantConstraint, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DiscriminantConstraint(), newDiscriminantConstraint, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscriminantConstraint(DiscriminantConstraint newDiscriminantConstraint) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DiscriminantConstraint(), newDiscriminantConstraint);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscriminantSpecification getDiscriminantSpecification() {
		return (DiscriminantSpecification)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DiscriminantSpecification(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscriminantSpecification(DiscriminantSpecification newDiscriminantSpecification, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DiscriminantSpecification(), newDiscriminantSpecification, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscriminantSpecification(DiscriminantSpecification newDiscriminantSpecification) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DiscriminantSpecification(), newDiscriminantSpecification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DispatchingDomainPragma getDispatchingDomainPragma() {
		return (DispatchingDomainPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DispatchingDomainPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDispatchingDomainPragma(DispatchingDomainPragma newDispatchingDomainPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DispatchingDomainPragma(), newDispatchingDomainPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDispatchingDomainPragma(DispatchingDomainPragma newDispatchingDomainPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DispatchingDomainPragma(), newDispatchingDomainPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DivideOperator getDivideOperator() {
		return (DivideOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_DivideOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDivideOperator(DivideOperator newDivideOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_DivideOperator(), newDivideOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDivideOperator(DivideOperator newDivideOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_DivideOperator(), newDivideOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElaborateAllPragma getElaborateAllPragma() {
		return (ElaborateAllPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ElaborateAllPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElaborateAllPragma(ElaborateAllPragma newElaborateAllPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ElaborateAllPragma(), newElaborateAllPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElaborateAllPragma(ElaborateAllPragma newElaborateAllPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ElaborateAllPragma(), newElaborateAllPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElaborateBodyPragma getElaborateBodyPragma() {
		return (ElaborateBodyPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ElaborateBodyPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElaborateBodyPragma(ElaborateBodyPragma newElaborateBodyPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ElaborateBodyPragma(), newElaborateBodyPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElaborateBodyPragma(ElaborateBodyPragma newElaborateBodyPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ElaborateBodyPragma(), newElaborateBodyPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElaboratePragma getElaboratePragma() {
		return (ElaboratePragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ElaboratePragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElaboratePragma(ElaboratePragma newElaboratePragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ElaboratePragma(), newElaboratePragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElaboratePragma(ElaboratePragma newElaboratePragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ElaboratePragma(), newElaboratePragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementIteratorSpecification getElementIteratorSpecification() {
		return (ElementIteratorSpecification)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ElementIteratorSpecification(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElementIteratorSpecification(ElementIteratorSpecification newElementIteratorSpecification, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ElementIteratorSpecification(), newElementIteratorSpecification, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElementIteratorSpecification(ElementIteratorSpecification newElementIteratorSpecification) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ElementIteratorSpecification(), newElementIteratorSpecification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElseExpressionPath getElseExpressionPath() {
		return (ElseExpressionPath)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ElseExpressionPath(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElseExpressionPath(ElseExpressionPath newElseExpressionPath, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ElseExpressionPath(), newElseExpressionPath, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElseExpressionPath(ElseExpressionPath newElseExpressionPath) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ElseExpressionPath(), newElseExpressionPath);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElsePath getElsePath() {
		return (ElsePath)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ElsePath(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElsePath(ElsePath newElsePath, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ElsePath(), newElsePath, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElsePath(ElsePath newElsePath) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ElsePath(), newElsePath);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElsifExpressionPath getElsifExpressionPath() {
		return (ElsifExpressionPath)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ElsifExpressionPath(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElsifExpressionPath(ElsifExpressionPath newElsifExpressionPath, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ElsifExpressionPath(), newElsifExpressionPath, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElsifExpressionPath(ElsifExpressionPath newElsifExpressionPath) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ElsifExpressionPath(), newElsifExpressionPath);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElsifPath getElsifPath() {
		return (ElsifPath)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ElsifPath(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElsifPath(ElsifPath newElsifPath, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ElsifPath(), newElsifPath, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElsifPath(ElsifPath newElsifPath) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ElsifPath(), newElsifPath);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntryBodyDeclaration getEntryBodyDeclaration() {
		return (EntryBodyDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_EntryBodyDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEntryBodyDeclaration(EntryBodyDeclaration newEntryBodyDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_EntryBodyDeclaration(), newEntryBodyDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntryBodyDeclaration(EntryBodyDeclaration newEntryBodyDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_EntryBodyDeclaration(), newEntryBodyDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntryCallStatement getEntryCallStatement() {
		return (EntryCallStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_EntryCallStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEntryCallStatement(EntryCallStatement newEntryCallStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_EntryCallStatement(), newEntryCallStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntryCallStatement(EntryCallStatement newEntryCallStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_EntryCallStatement(), newEntryCallStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntryDeclaration getEntryDeclaration() {
		return (EntryDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_EntryDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEntryDeclaration(EntryDeclaration newEntryDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_EntryDeclaration(), newEntryDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntryDeclaration(EntryDeclaration newEntryDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_EntryDeclaration(), newEntryDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntryIndexSpecification getEntryIndexSpecification() {
		return (EntryIndexSpecification)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_EntryIndexSpecification(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEntryIndexSpecification(EntryIndexSpecification newEntryIndexSpecification, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_EntryIndexSpecification(), newEntryIndexSpecification, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntryIndexSpecification(EntryIndexSpecification newEntryIndexSpecification) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_EntryIndexSpecification(), newEntryIndexSpecification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationLiteral getEnumerationLiteral() {
		return (EnumerationLiteral)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_EnumerationLiteral(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEnumerationLiteral(EnumerationLiteral newEnumerationLiteral, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_EnumerationLiteral(), newEnumerationLiteral, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnumerationLiteral(EnumerationLiteral newEnumerationLiteral) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_EnumerationLiteral(), newEnumerationLiteral);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationLiteralSpecification getEnumerationLiteralSpecification() {
		return (EnumerationLiteralSpecification)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_EnumerationLiteralSpecification(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEnumerationLiteralSpecification(EnumerationLiteralSpecification newEnumerationLiteralSpecification, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_EnumerationLiteralSpecification(), newEnumerationLiteralSpecification, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnumerationLiteralSpecification(EnumerationLiteralSpecification newEnumerationLiteralSpecification) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_EnumerationLiteralSpecification(), newEnumerationLiteralSpecification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationRepresentationClause getEnumerationRepresentationClause() {
		return (EnumerationRepresentationClause)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_EnumerationRepresentationClause(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEnumerationRepresentationClause(EnumerationRepresentationClause newEnumerationRepresentationClause, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_EnumerationRepresentationClause(), newEnumerationRepresentationClause, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnumerationRepresentationClause(EnumerationRepresentationClause newEnumerationRepresentationClause) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_EnumerationRepresentationClause(), newEnumerationRepresentationClause);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationTypeDefinition getEnumerationTypeDefinition() {
		return (EnumerationTypeDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_EnumerationTypeDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEnumerationTypeDefinition(EnumerationTypeDefinition newEnumerationTypeDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_EnumerationTypeDefinition(), newEnumerationTypeDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnumerationTypeDefinition(EnumerationTypeDefinition newEnumerationTypeDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_EnumerationTypeDefinition(), newEnumerationTypeDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EqualOperator getEqualOperator() {
		return (EqualOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_EqualOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEqualOperator(EqualOperator newEqualOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_EqualOperator(), newEqualOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEqualOperator(EqualOperator newEqualOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_EqualOperator(), newEqualOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExceptionDeclaration getExceptionDeclaration() {
		return (ExceptionDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ExceptionDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExceptionDeclaration(ExceptionDeclaration newExceptionDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ExceptionDeclaration(), newExceptionDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExceptionDeclaration(ExceptionDeclaration newExceptionDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ExceptionDeclaration(), newExceptionDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExceptionHandler getExceptionHandler() {
		return (ExceptionHandler)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ExceptionHandler(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExceptionHandler(ExceptionHandler newExceptionHandler, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ExceptionHandler(), newExceptionHandler, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExceptionHandler(ExceptionHandler newExceptionHandler) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ExceptionHandler(), newExceptionHandler);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExceptionRenamingDeclaration getExceptionRenamingDeclaration() {
		return (ExceptionRenamingDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ExceptionRenamingDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExceptionRenamingDeclaration(ExceptionRenamingDeclaration newExceptionRenamingDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ExceptionRenamingDeclaration(), newExceptionRenamingDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExceptionRenamingDeclaration(ExceptionRenamingDeclaration newExceptionRenamingDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ExceptionRenamingDeclaration(), newExceptionRenamingDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExitStatement getExitStatement() {
		return (ExitStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ExitStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExitStatement(ExitStatement newExitStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ExitStatement(), newExitStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExitStatement(ExitStatement newExitStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ExitStatement(), newExitStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExplicitDereference getExplicitDereference() {
		return (ExplicitDereference)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ExplicitDereference(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExplicitDereference(ExplicitDereference newExplicitDereference, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ExplicitDereference(), newExplicitDereference, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExplicitDereference(ExplicitDereference newExplicitDereference) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ExplicitDereference(), newExplicitDereference);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExponentAttribute getExponentAttribute() {
		return (ExponentAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ExponentAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExponentAttribute(ExponentAttribute newExponentAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ExponentAttribute(), newExponentAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExponentAttribute(ExponentAttribute newExponentAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ExponentAttribute(), newExponentAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExponentiateOperator getExponentiateOperator() {
		return (ExponentiateOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ExponentiateOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExponentiateOperator(ExponentiateOperator newExponentiateOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ExponentiateOperator(), newExponentiateOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExponentiateOperator(ExponentiateOperator newExponentiateOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ExponentiateOperator(), newExponentiateOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExportPragma getExportPragma() {
		return (ExportPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ExportPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExportPragma(ExportPragma newExportPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ExportPragma(), newExportPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExportPragma(ExportPragma newExportPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ExportPragma(), newExportPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionFunctionDeclaration getExpressionFunctionDeclaration() {
		return (ExpressionFunctionDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ExpressionFunctionDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExpressionFunctionDeclaration(ExpressionFunctionDeclaration newExpressionFunctionDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ExpressionFunctionDeclaration(), newExpressionFunctionDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpressionFunctionDeclaration(ExpressionFunctionDeclaration newExpressionFunctionDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ExpressionFunctionDeclaration(), newExpressionFunctionDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExtendedReturnStatement getExtendedReturnStatement() {
		return (ExtendedReturnStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ExtendedReturnStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExtendedReturnStatement(ExtendedReturnStatement newExtendedReturnStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ExtendedReturnStatement(), newExtendedReturnStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExtendedReturnStatement(ExtendedReturnStatement newExtendedReturnStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ExtendedReturnStatement(), newExtendedReturnStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExtensionAggregate getExtensionAggregate() {
		return (ExtensionAggregate)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ExtensionAggregate(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExtensionAggregate(ExtensionAggregate newExtensionAggregate, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ExtensionAggregate(), newExtensionAggregate, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExtensionAggregate(ExtensionAggregate newExtensionAggregate) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ExtensionAggregate(), newExtensionAggregate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExternalTagAttribute getExternalTagAttribute() {
		return (ExternalTagAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ExternalTagAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExternalTagAttribute(ExternalTagAttribute newExternalTagAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ExternalTagAttribute(), newExternalTagAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExternalTagAttribute(ExternalTagAttribute newExternalTagAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ExternalTagAttribute(), newExternalTagAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FirstAttribute getFirstAttribute() {
		return (FirstAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FirstAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFirstAttribute(FirstAttribute newFirstAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FirstAttribute(), newFirstAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirstAttribute(FirstAttribute newFirstAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FirstAttribute(), newFirstAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FirstBitAttribute getFirstBitAttribute() {
		return (FirstBitAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FirstBitAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFirstBitAttribute(FirstBitAttribute newFirstBitAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FirstBitAttribute(), newFirstBitAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirstBitAttribute(FirstBitAttribute newFirstBitAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FirstBitAttribute(), newFirstBitAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FloatingPointDefinition getFloatingPointDefinition() {
		return (FloatingPointDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FloatingPointDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFloatingPointDefinition(FloatingPointDefinition newFloatingPointDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FloatingPointDefinition(), newFloatingPointDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFloatingPointDefinition(FloatingPointDefinition newFloatingPointDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FloatingPointDefinition(), newFloatingPointDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FloorAttribute getFloorAttribute() {
		return (FloorAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FloorAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFloorAttribute(FloorAttribute newFloorAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FloorAttribute(), newFloorAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFloorAttribute(FloorAttribute newFloorAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FloorAttribute(), newFloorAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ForAllQuantifiedExpression getForAllQuantifiedExpression() {
		return (ForAllQuantifiedExpression)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ForAllQuantifiedExpression(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetForAllQuantifiedExpression(ForAllQuantifiedExpression newForAllQuantifiedExpression, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ForAllQuantifiedExpression(), newForAllQuantifiedExpression, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setForAllQuantifiedExpression(ForAllQuantifiedExpression newForAllQuantifiedExpression) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ForAllQuantifiedExpression(), newForAllQuantifiedExpression);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ForLoopStatement getForLoopStatement() {
		return (ForLoopStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ForLoopStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetForLoopStatement(ForLoopStatement newForLoopStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ForLoopStatement(), newForLoopStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setForLoopStatement(ForLoopStatement newForLoopStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ForLoopStatement(), newForLoopStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ForSomeQuantifiedExpression getForSomeQuantifiedExpression() {
		return (ForSomeQuantifiedExpression)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ForSomeQuantifiedExpression(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetForSomeQuantifiedExpression(ForSomeQuantifiedExpression newForSomeQuantifiedExpression, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ForSomeQuantifiedExpression(), newForSomeQuantifiedExpression, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setForSomeQuantifiedExpression(ForSomeQuantifiedExpression newForSomeQuantifiedExpression) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ForSomeQuantifiedExpression(), newForSomeQuantifiedExpression);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ForeAttribute getForeAttribute() {
		return (ForeAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ForeAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetForeAttribute(ForeAttribute newForeAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ForeAttribute(), newForeAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setForeAttribute(ForeAttribute newForeAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ForeAttribute(), newForeAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalAccessToConstant getFormalAccessToConstant() {
		return (FormalAccessToConstant)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalAccessToConstant(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalAccessToConstant(FormalAccessToConstant newFormalAccessToConstant, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalAccessToConstant(), newFormalAccessToConstant, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalAccessToConstant(FormalAccessToConstant newFormalAccessToConstant) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalAccessToConstant(), newFormalAccessToConstant);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalAccessToFunction getFormalAccessToFunction() {
		return (FormalAccessToFunction)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalAccessToFunction(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalAccessToFunction(FormalAccessToFunction newFormalAccessToFunction, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalAccessToFunction(), newFormalAccessToFunction, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalAccessToFunction(FormalAccessToFunction newFormalAccessToFunction) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalAccessToFunction(), newFormalAccessToFunction);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalAccessToProcedure getFormalAccessToProcedure() {
		return (FormalAccessToProcedure)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalAccessToProcedure(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalAccessToProcedure(FormalAccessToProcedure newFormalAccessToProcedure, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalAccessToProcedure(), newFormalAccessToProcedure, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalAccessToProcedure(FormalAccessToProcedure newFormalAccessToProcedure) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalAccessToProcedure(), newFormalAccessToProcedure);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalAccessToProtectedFunction getFormalAccessToProtectedFunction() {
		return (FormalAccessToProtectedFunction)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalAccessToProtectedFunction(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalAccessToProtectedFunction(FormalAccessToProtectedFunction newFormalAccessToProtectedFunction, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalAccessToProtectedFunction(), newFormalAccessToProtectedFunction, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalAccessToProtectedFunction(FormalAccessToProtectedFunction newFormalAccessToProtectedFunction) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalAccessToProtectedFunction(), newFormalAccessToProtectedFunction);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalAccessToProtectedProcedure getFormalAccessToProtectedProcedure() {
		return (FormalAccessToProtectedProcedure)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalAccessToProtectedProcedure(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalAccessToProtectedProcedure(FormalAccessToProtectedProcedure newFormalAccessToProtectedProcedure, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalAccessToProtectedProcedure(), newFormalAccessToProtectedProcedure, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalAccessToProtectedProcedure(FormalAccessToProtectedProcedure newFormalAccessToProtectedProcedure) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalAccessToProtectedProcedure(), newFormalAccessToProtectedProcedure);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalAccessToVariable getFormalAccessToVariable() {
		return (FormalAccessToVariable)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalAccessToVariable(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalAccessToVariable(FormalAccessToVariable newFormalAccessToVariable, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalAccessToVariable(), newFormalAccessToVariable, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalAccessToVariable(FormalAccessToVariable newFormalAccessToVariable) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalAccessToVariable(), newFormalAccessToVariable);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalConstrainedArrayDefinition getFormalConstrainedArrayDefinition() {
		return (FormalConstrainedArrayDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalConstrainedArrayDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalConstrainedArrayDefinition(FormalConstrainedArrayDefinition newFormalConstrainedArrayDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalConstrainedArrayDefinition(), newFormalConstrainedArrayDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalConstrainedArrayDefinition(FormalConstrainedArrayDefinition newFormalConstrainedArrayDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalConstrainedArrayDefinition(), newFormalConstrainedArrayDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalDecimalFixedPointDefinition getFormalDecimalFixedPointDefinition() {
		return (FormalDecimalFixedPointDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalDecimalFixedPointDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalDecimalFixedPointDefinition(FormalDecimalFixedPointDefinition newFormalDecimalFixedPointDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalDecimalFixedPointDefinition(), newFormalDecimalFixedPointDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalDecimalFixedPointDefinition(FormalDecimalFixedPointDefinition newFormalDecimalFixedPointDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalDecimalFixedPointDefinition(), newFormalDecimalFixedPointDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalDerivedTypeDefinition getFormalDerivedTypeDefinition() {
		return (FormalDerivedTypeDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalDerivedTypeDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalDerivedTypeDefinition(FormalDerivedTypeDefinition newFormalDerivedTypeDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalDerivedTypeDefinition(), newFormalDerivedTypeDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalDerivedTypeDefinition(FormalDerivedTypeDefinition newFormalDerivedTypeDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalDerivedTypeDefinition(), newFormalDerivedTypeDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalDiscreteTypeDefinition getFormalDiscreteTypeDefinition() {
		return (FormalDiscreteTypeDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalDiscreteTypeDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalDiscreteTypeDefinition(FormalDiscreteTypeDefinition newFormalDiscreteTypeDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalDiscreteTypeDefinition(), newFormalDiscreteTypeDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalDiscreteTypeDefinition(FormalDiscreteTypeDefinition newFormalDiscreteTypeDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalDiscreteTypeDefinition(), newFormalDiscreteTypeDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalFloatingPointDefinition getFormalFloatingPointDefinition() {
		return (FormalFloatingPointDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalFloatingPointDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalFloatingPointDefinition(FormalFloatingPointDefinition newFormalFloatingPointDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalFloatingPointDefinition(), newFormalFloatingPointDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalFloatingPointDefinition(FormalFloatingPointDefinition newFormalFloatingPointDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalFloatingPointDefinition(), newFormalFloatingPointDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalFunctionDeclaration getFormalFunctionDeclaration() {
		return (FormalFunctionDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalFunctionDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalFunctionDeclaration(FormalFunctionDeclaration newFormalFunctionDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalFunctionDeclaration(), newFormalFunctionDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalFunctionDeclaration(FormalFunctionDeclaration newFormalFunctionDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalFunctionDeclaration(), newFormalFunctionDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalIncompleteTypeDeclaration getFormalIncompleteTypeDeclaration() {
		return (FormalIncompleteTypeDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalIncompleteTypeDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalIncompleteTypeDeclaration(FormalIncompleteTypeDeclaration newFormalIncompleteTypeDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalIncompleteTypeDeclaration(), newFormalIncompleteTypeDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalIncompleteTypeDeclaration(FormalIncompleteTypeDeclaration newFormalIncompleteTypeDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalIncompleteTypeDeclaration(), newFormalIncompleteTypeDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalLimitedInterface getFormalLimitedInterface() {
		return (FormalLimitedInterface)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalLimitedInterface(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalLimitedInterface(FormalLimitedInterface newFormalLimitedInterface, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalLimitedInterface(), newFormalLimitedInterface, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalLimitedInterface(FormalLimitedInterface newFormalLimitedInterface) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalLimitedInterface(), newFormalLimitedInterface);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalModularTypeDefinition getFormalModularTypeDefinition() {
		return (FormalModularTypeDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalModularTypeDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalModularTypeDefinition(FormalModularTypeDefinition newFormalModularTypeDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalModularTypeDefinition(), newFormalModularTypeDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalModularTypeDefinition(FormalModularTypeDefinition newFormalModularTypeDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalModularTypeDefinition(), newFormalModularTypeDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalObjectDeclaration getFormalObjectDeclaration() {
		return (FormalObjectDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalObjectDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalObjectDeclaration(FormalObjectDeclaration newFormalObjectDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalObjectDeclaration(), newFormalObjectDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalObjectDeclaration(FormalObjectDeclaration newFormalObjectDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalObjectDeclaration(), newFormalObjectDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalOrdinaryFixedPointDefinition getFormalOrdinaryFixedPointDefinition() {
		return (FormalOrdinaryFixedPointDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalOrdinaryFixedPointDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalOrdinaryFixedPointDefinition(FormalOrdinaryFixedPointDefinition newFormalOrdinaryFixedPointDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalOrdinaryFixedPointDefinition(), newFormalOrdinaryFixedPointDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalOrdinaryFixedPointDefinition(FormalOrdinaryFixedPointDefinition newFormalOrdinaryFixedPointDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalOrdinaryFixedPointDefinition(), newFormalOrdinaryFixedPointDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalOrdinaryInterface getFormalOrdinaryInterface() {
		return (FormalOrdinaryInterface)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalOrdinaryInterface(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalOrdinaryInterface(FormalOrdinaryInterface newFormalOrdinaryInterface, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalOrdinaryInterface(), newFormalOrdinaryInterface, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalOrdinaryInterface(FormalOrdinaryInterface newFormalOrdinaryInterface) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalOrdinaryInterface(), newFormalOrdinaryInterface);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalPackageDeclaration getFormalPackageDeclaration() {
		return (FormalPackageDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalPackageDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalPackageDeclaration(FormalPackageDeclaration newFormalPackageDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalPackageDeclaration(), newFormalPackageDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalPackageDeclaration(FormalPackageDeclaration newFormalPackageDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalPackageDeclaration(), newFormalPackageDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalPackageDeclarationWithBox getFormalPackageDeclarationWithBox() {
		return (FormalPackageDeclarationWithBox)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalPackageDeclarationWithBox(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalPackageDeclarationWithBox(FormalPackageDeclarationWithBox newFormalPackageDeclarationWithBox, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalPackageDeclarationWithBox(), newFormalPackageDeclarationWithBox, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalPackageDeclarationWithBox(FormalPackageDeclarationWithBox newFormalPackageDeclarationWithBox) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalPackageDeclarationWithBox(), newFormalPackageDeclarationWithBox);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalPoolSpecificAccessToVariable getFormalPoolSpecificAccessToVariable() {
		return (FormalPoolSpecificAccessToVariable)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalPoolSpecificAccessToVariable(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalPoolSpecificAccessToVariable(FormalPoolSpecificAccessToVariable newFormalPoolSpecificAccessToVariable, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalPoolSpecificAccessToVariable(), newFormalPoolSpecificAccessToVariable, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalPoolSpecificAccessToVariable(FormalPoolSpecificAccessToVariable newFormalPoolSpecificAccessToVariable) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalPoolSpecificAccessToVariable(), newFormalPoolSpecificAccessToVariable);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalPrivateTypeDefinition getFormalPrivateTypeDefinition() {
		return (FormalPrivateTypeDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalPrivateTypeDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalPrivateTypeDefinition(FormalPrivateTypeDefinition newFormalPrivateTypeDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalPrivateTypeDefinition(), newFormalPrivateTypeDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalPrivateTypeDefinition(FormalPrivateTypeDefinition newFormalPrivateTypeDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalPrivateTypeDefinition(), newFormalPrivateTypeDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalProcedureDeclaration getFormalProcedureDeclaration() {
		return (FormalProcedureDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalProcedureDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalProcedureDeclaration(FormalProcedureDeclaration newFormalProcedureDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalProcedureDeclaration(), newFormalProcedureDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalProcedureDeclaration(FormalProcedureDeclaration newFormalProcedureDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalProcedureDeclaration(), newFormalProcedureDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalProtectedInterface getFormalProtectedInterface() {
		return (FormalProtectedInterface)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalProtectedInterface(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalProtectedInterface(FormalProtectedInterface newFormalProtectedInterface, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalProtectedInterface(), newFormalProtectedInterface, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalProtectedInterface(FormalProtectedInterface newFormalProtectedInterface) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalProtectedInterface(), newFormalProtectedInterface);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalSignedIntegerTypeDefinition getFormalSignedIntegerTypeDefinition() {
		return (FormalSignedIntegerTypeDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalSignedIntegerTypeDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalSignedIntegerTypeDefinition(FormalSignedIntegerTypeDefinition newFormalSignedIntegerTypeDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalSignedIntegerTypeDefinition(), newFormalSignedIntegerTypeDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalSignedIntegerTypeDefinition(FormalSignedIntegerTypeDefinition newFormalSignedIntegerTypeDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalSignedIntegerTypeDefinition(), newFormalSignedIntegerTypeDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalSynchronizedInterface getFormalSynchronizedInterface() {
		return (FormalSynchronizedInterface)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalSynchronizedInterface(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalSynchronizedInterface(FormalSynchronizedInterface newFormalSynchronizedInterface, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalSynchronizedInterface(), newFormalSynchronizedInterface, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalSynchronizedInterface(FormalSynchronizedInterface newFormalSynchronizedInterface) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalSynchronizedInterface(), newFormalSynchronizedInterface);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalTaggedPrivateTypeDefinition getFormalTaggedPrivateTypeDefinition() {
		return (FormalTaggedPrivateTypeDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalTaggedPrivateTypeDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalTaggedPrivateTypeDefinition(FormalTaggedPrivateTypeDefinition newFormalTaggedPrivateTypeDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalTaggedPrivateTypeDefinition(), newFormalTaggedPrivateTypeDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalTaggedPrivateTypeDefinition(FormalTaggedPrivateTypeDefinition newFormalTaggedPrivateTypeDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalTaggedPrivateTypeDefinition(), newFormalTaggedPrivateTypeDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalTaskInterface getFormalTaskInterface() {
		return (FormalTaskInterface)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalTaskInterface(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalTaskInterface(FormalTaskInterface newFormalTaskInterface, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalTaskInterface(), newFormalTaskInterface, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalTaskInterface(FormalTaskInterface newFormalTaskInterface) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalTaskInterface(), newFormalTaskInterface);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalTypeDeclaration getFormalTypeDeclaration() {
		return (FormalTypeDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalTypeDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalTypeDeclaration(FormalTypeDeclaration newFormalTypeDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalTypeDeclaration(), newFormalTypeDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalTypeDeclaration(FormalTypeDeclaration newFormalTypeDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalTypeDeclaration(), newFormalTypeDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalUnconstrainedArrayDefinition getFormalUnconstrainedArrayDefinition() {
		return (FormalUnconstrainedArrayDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FormalUnconstrainedArrayDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalUnconstrainedArrayDefinition(FormalUnconstrainedArrayDefinition newFormalUnconstrainedArrayDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FormalUnconstrainedArrayDefinition(), newFormalUnconstrainedArrayDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalUnconstrainedArrayDefinition(FormalUnconstrainedArrayDefinition newFormalUnconstrainedArrayDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FormalUnconstrainedArrayDefinition(), newFormalUnconstrainedArrayDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FractionAttribute getFractionAttribute() {
		return (FractionAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FractionAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFractionAttribute(FractionAttribute newFractionAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FractionAttribute(), newFractionAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFractionAttribute(FractionAttribute newFractionAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FractionAttribute(), newFractionAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionBodyDeclaration getFunctionBodyDeclaration() {
		return (FunctionBodyDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FunctionBodyDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFunctionBodyDeclaration(FunctionBodyDeclaration newFunctionBodyDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FunctionBodyDeclaration(), newFunctionBodyDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFunctionBodyDeclaration(FunctionBodyDeclaration newFunctionBodyDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FunctionBodyDeclaration(), newFunctionBodyDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionBodyStub getFunctionBodyStub() {
		return (FunctionBodyStub)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FunctionBodyStub(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFunctionBodyStub(FunctionBodyStub newFunctionBodyStub, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FunctionBodyStub(), newFunctionBodyStub, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFunctionBodyStub(FunctionBodyStub newFunctionBodyStub) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FunctionBodyStub(), newFunctionBodyStub);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionCall getFunctionCall() {
		return (FunctionCall)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FunctionCall(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFunctionCall(FunctionCall newFunctionCall, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FunctionCall(), newFunctionCall, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFunctionCall(FunctionCall newFunctionCall) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FunctionCall(), newFunctionCall);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionDeclaration getFunctionDeclaration() {
		return (FunctionDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FunctionDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFunctionDeclaration(FunctionDeclaration newFunctionDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FunctionDeclaration(), newFunctionDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFunctionDeclaration(FunctionDeclaration newFunctionDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FunctionDeclaration(), newFunctionDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionInstantiation getFunctionInstantiation() {
		return (FunctionInstantiation)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FunctionInstantiation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFunctionInstantiation(FunctionInstantiation newFunctionInstantiation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FunctionInstantiation(), newFunctionInstantiation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFunctionInstantiation(FunctionInstantiation newFunctionInstantiation) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FunctionInstantiation(), newFunctionInstantiation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionRenamingDeclaration getFunctionRenamingDeclaration() {
		return (FunctionRenamingDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_FunctionRenamingDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFunctionRenamingDeclaration(FunctionRenamingDeclaration newFunctionRenamingDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_FunctionRenamingDeclaration(), newFunctionRenamingDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFunctionRenamingDeclaration(FunctionRenamingDeclaration newFunctionRenamingDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_FunctionRenamingDeclaration(), newFunctionRenamingDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GeneralizedIteratorSpecification getGeneralizedIteratorSpecification() {
		return (GeneralizedIteratorSpecification)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_GeneralizedIteratorSpecification(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGeneralizedIteratorSpecification(GeneralizedIteratorSpecification newGeneralizedIteratorSpecification, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_GeneralizedIteratorSpecification(), newGeneralizedIteratorSpecification, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGeneralizedIteratorSpecification(GeneralizedIteratorSpecification newGeneralizedIteratorSpecification) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_GeneralizedIteratorSpecification(), newGeneralizedIteratorSpecification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericAssociation getGenericAssociation() {
		return (GenericAssociation)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_GenericAssociation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGenericAssociation(GenericAssociation newGenericAssociation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_GenericAssociation(), newGenericAssociation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGenericAssociation(GenericAssociation newGenericAssociation) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_GenericAssociation(), newGenericAssociation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericFunctionDeclaration getGenericFunctionDeclaration() {
		return (GenericFunctionDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_GenericFunctionDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGenericFunctionDeclaration(GenericFunctionDeclaration newGenericFunctionDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_GenericFunctionDeclaration(), newGenericFunctionDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGenericFunctionDeclaration(GenericFunctionDeclaration newGenericFunctionDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_GenericFunctionDeclaration(), newGenericFunctionDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericFunctionRenamingDeclaration getGenericFunctionRenamingDeclaration() {
		return (GenericFunctionRenamingDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_GenericFunctionRenamingDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGenericFunctionRenamingDeclaration(GenericFunctionRenamingDeclaration newGenericFunctionRenamingDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_GenericFunctionRenamingDeclaration(), newGenericFunctionRenamingDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGenericFunctionRenamingDeclaration(GenericFunctionRenamingDeclaration newGenericFunctionRenamingDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_GenericFunctionRenamingDeclaration(), newGenericFunctionRenamingDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericPackageDeclaration getGenericPackageDeclaration() {
		return (GenericPackageDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_GenericPackageDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGenericPackageDeclaration(GenericPackageDeclaration newGenericPackageDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_GenericPackageDeclaration(), newGenericPackageDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGenericPackageDeclaration(GenericPackageDeclaration newGenericPackageDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_GenericPackageDeclaration(), newGenericPackageDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericPackageRenamingDeclaration getGenericPackageRenamingDeclaration() {
		return (GenericPackageRenamingDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_GenericPackageRenamingDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGenericPackageRenamingDeclaration(GenericPackageRenamingDeclaration newGenericPackageRenamingDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_GenericPackageRenamingDeclaration(), newGenericPackageRenamingDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGenericPackageRenamingDeclaration(GenericPackageRenamingDeclaration newGenericPackageRenamingDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_GenericPackageRenamingDeclaration(), newGenericPackageRenamingDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericProcedureDeclaration getGenericProcedureDeclaration() {
		return (GenericProcedureDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_GenericProcedureDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGenericProcedureDeclaration(GenericProcedureDeclaration newGenericProcedureDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_GenericProcedureDeclaration(), newGenericProcedureDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGenericProcedureDeclaration(GenericProcedureDeclaration newGenericProcedureDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_GenericProcedureDeclaration(), newGenericProcedureDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericProcedureRenamingDeclaration getGenericProcedureRenamingDeclaration() {
		return (GenericProcedureRenamingDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_GenericProcedureRenamingDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGenericProcedureRenamingDeclaration(GenericProcedureRenamingDeclaration newGenericProcedureRenamingDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_GenericProcedureRenamingDeclaration(), newGenericProcedureRenamingDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGenericProcedureRenamingDeclaration(GenericProcedureRenamingDeclaration newGenericProcedureRenamingDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_GenericProcedureRenamingDeclaration(), newGenericProcedureRenamingDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GotoStatement getGotoStatement() {
		return (GotoStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_GotoStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGotoStatement(GotoStatement newGotoStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_GotoStatement(), newGotoStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGotoStatement(GotoStatement newGotoStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_GotoStatement(), newGotoStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GreaterThanOperator getGreaterThanOperator() {
		return (GreaterThanOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_GreaterThanOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGreaterThanOperator(GreaterThanOperator newGreaterThanOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_GreaterThanOperator(), newGreaterThanOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGreaterThanOperator(GreaterThanOperator newGreaterThanOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_GreaterThanOperator(), newGreaterThanOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GreaterThanOrEqualOperator getGreaterThanOrEqualOperator() {
		return (GreaterThanOrEqualOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_GreaterThanOrEqualOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGreaterThanOrEqualOperator(GreaterThanOrEqualOperator newGreaterThanOrEqualOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_GreaterThanOrEqualOperator(), newGreaterThanOrEqualOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGreaterThanOrEqualOperator(GreaterThanOrEqualOperator newGreaterThanOrEqualOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_GreaterThanOrEqualOperator(), newGreaterThanOrEqualOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Identifier getIdentifier() {
		return (Identifier)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_Identifier(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIdentifier(Identifier newIdentifier, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_Identifier(), newIdentifier, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdentifier(Identifier newIdentifier) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_Identifier(), newIdentifier);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentityAttribute getIdentityAttribute() {
		return (IdentityAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_IdentityAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIdentityAttribute(IdentityAttribute newIdentityAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_IdentityAttribute(), newIdentityAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdentityAttribute(IdentityAttribute newIdentityAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_IdentityAttribute(), newIdentityAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfExpression getIfExpression() {
		return (IfExpression)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_IfExpression(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIfExpression(IfExpression newIfExpression, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_IfExpression(), newIfExpression, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIfExpression(IfExpression newIfExpression) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_IfExpression(), newIfExpression);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfExpressionPath getIfExpressionPath() {
		return (IfExpressionPath)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_IfExpressionPath(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIfExpressionPath(IfExpressionPath newIfExpressionPath, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_IfExpressionPath(), newIfExpressionPath, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIfExpressionPath(IfExpressionPath newIfExpressionPath) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_IfExpressionPath(), newIfExpressionPath);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfPath getIfPath() {
		return (IfPath)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_IfPath(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIfPath(IfPath newIfPath, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_IfPath(), newIfPath, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIfPath(IfPath newIfPath) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_IfPath(), newIfPath);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfStatement getIfStatement() {
		return (IfStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_IfStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIfStatement(IfStatement newIfStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_IfStatement(), newIfStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIfStatement(IfStatement newIfStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_IfStatement(), newIfStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImageAttribute getImageAttribute() {
		return (ImageAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ImageAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImageAttribute(ImageAttribute newImageAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ImageAttribute(), newImageAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImageAttribute(ImageAttribute newImageAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ImageAttribute(), newImageAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImplementationDefinedAttribute getImplementationDefinedAttribute() {
		return (ImplementationDefinedAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ImplementationDefinedAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImplementationDefinedAttribute(ImplementationDefinedAttribute newImplementationDefinedAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ImplementationDefinedAttribute(), newImplementationDefinedAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImplementationDefinedAttribute(ImplementationDefinedAttribute newImplementationDefinedAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ImplementationDefinedAttribute(), newImplementationDefinedAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImplementationDefinedPragma getImplementationDefinedPragma() {
		return (ImplementationDefinedPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ImplementationDefinedPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImplementationDefinedPragma(ImplementationDefinedPragma newImplementationDefinedPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ImplementationDefinedPragma(), newImplementationDefinedPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImplementationDefinedPragma(ImplementationDefinedPragma newImplementationDefinedPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ImplementationDefinedPragma(), newImplementationDefinedPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImportPragma getImportPragma() {
		return (ImportPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ImportPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImportPragma(ImportPragma newImportPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ImportPragma(), newImportPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImportPragma(ImportPragma newImportPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ImportPragma(), newImportPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InMembershipTest getInMembershipTest() {
		return (InMembershipTest)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_InMembershipTest(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInMembershipTest(InMembershipTest newInMembershipTest, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_InMembershipTest(), newInMembershipTest, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInMembershipTest(InMembershipTest newInMembershipTest) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_InMembershipTest(), newInMembershipTest);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IncompleteTypeDeclaration getIncompleteTypeDeclaration() {
		return (IncompleteTypeDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_IncompleteTypeDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIncompleteTypeDeclaration(IncompleteTypeDeclaration newIncompleteTypeDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_IncompleteTypeDeclaration(), newIncompleteTypeDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIncompleteTypeDeclaration(IncompleteTypeDeclaration newIncompleteTypeDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_IncompleteTypeDeclaration(), newIncompleteTypeDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IndependentComponentsPragma getIndependentComponentsPragma() {
		return (IndependentComponentsPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_IndependentComponentsPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIndependentComponentsPragma(IndependentComponentsPragma newIndependentComponentsPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_IndependentComponentsPragma(), newIndependentComponentsPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndependentComponentsPragma(IndependentComponentsPragma newIndependentComponentsPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_IndependentComponentsPragma(), newIndependentComponentsPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IndependentPragma getIndependentPragma() {
		return (IndependentPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_IndependentPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIndependentPragma(IndependentPragma newIndependentPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_IndependentPragma(), newIndependentPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndependentPragma(IndependentPragma newIndependentPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_IndependentPragma(), newIndependentPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IndexConstraint getIndexConstraint() {
		return (IndexConstraint)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_IndexConstraint(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIndexConstraint(IndexConstraint newIndexConstraint, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_IndexConstraint(), newIndexConstraint, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndexConstraint(IndexConstraint newIndexConstraint) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_IndexConstraint(), newIndexConstraint);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IndexedComponent getIndexedComponent() {
		return (IndexedComponent)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_IndexedComponent(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIndexedComponent(IndexedComponent newIndexedComponent, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_IndexedComponent(), newIndexedComponent, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndexedComponent(IndexedComponent newIndexedComponent) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_IndexedComponent(), newIndexedComponent);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InlinePragma getInlinePragma() {
		return (InlinePragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_InlinePragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInlinePragma(InlinePragma newInlinePragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_InlinePragma(), newInlinePragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInlinePragma(InlinePragma newInlinePragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_InlinePragma(), newInlinePragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InputAttribute getInputAttribute() {
		return (InputAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_InputAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInputAttribute(InputAttribute newInputAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_InputAttribute(), newInputAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInputAttribute(InputAttribute newInputAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_InputAttribute(), newInputAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InspectionPointPragma getInspectionPointPragma() {
		return (InspectionPointPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_InspectionPointPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInspectionPointPragma(InspectionPointPragma newInspectionPointPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_InspectionPointPragma(), newInspectionPointPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInspectionPointPragma(InspectionPointPragma newInspectionPointPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_InspectionPointPragma(), newInspectionPointPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntegerLiteral getIntegerLiteral() {
		return (IntegerLiteral)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_IntegerLiteral(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIntegerLiteral(IntegerLiteral newIntegerLiteral, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_IntegerLiteral(), newIntegerLiteral, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntegerLiteral(IntegerLiteral newIntegerLiteral) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_IntegerLiteral(), newIntegerLiteral);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntegerNumberDeclaration getIntegerNumberDeclaration() {
		return (IntegerNumberDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_IntegerNumberDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIntegerNumberDeclaration(IntegerNumberDeclaration newIntegerNumberDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_IntegerNumberDeclaration(), newIntegerNumberDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntegerNumberDeclaration(IntegerNumberDeclaration newIntegerNumberDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_IntegerNumberDeclaration(), newIntegerNumberDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterruptHandlerPragma getInterruptHandlerPragma() {
		return (InterruptHandlerPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_InterruptHandlerPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInterruptHandlerPragma(InterruptHandlerPragma newInterruptHandlerPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_InterruptHandlerPragma(), newInterruptHandlerPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterruptHandlerPragma(InterruptHandlerPragma newInterruptHandlerPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_InterruptHandlerPragma(), newInterruptHandlerPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterruptPriorityPragma getInterruptPriorityPragma() {
		return (InterruptPriorityPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_InterruptPriorityPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInterruptPriorityPragma(InterruptPriorityPragma newInterruptPriorityPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_InterruptPriorityPragma(), newInterruptPriorityPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterruptPriorityPragma(InterruptPriorityPragma newInterruptPriorityPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_InterruptPriorityPragma(), newInterruptPriorityPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsPrefixCall getIsPrefixCall() {
		return (IsPrefixCall)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_IsPrefixCall(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIsPrefixCall(IsPrefixCall newIsPrefixCall, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_IsPrefixCall(), newIsPrefixCall, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsPrefixCall(IsPrefixCall newIsPrefixCall) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_IsPrefixCall(), newIsPrefixCall);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsPrefixNotation getIsPrefixNotation() {
		return (IsPrefixNotation)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_IsPrefixNotation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIsPrefixNotation(IsPrefixNotation newIsPrefixNotation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_IsPrefixNotation(), newIsPrefixNotation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsPrefixNotation(IsPrefixNotation newIsPrefixNotation) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_IsPrefixNotation(), newIsPrefixNotation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public KnownDiscriminantPart getKnownDiscriminantPart() {
		return (KnownDiscriminantPart)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_KnownDiscriminantPart(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetKnownDiscriminantPart(KnownDiscriminantPart newKnownDiscriminantPart, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_KnownDiscriminantPart(), newKnownDiscriminantPart, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKnownDiscriminantPart(KnownDiscriminantPart newKnownDiscriminantPart) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_KnownDiscriminantPart(), newKnownDiscriminantPart);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LastAttribute getLastAttribute() {
		return (LastAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_LastAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLastAttribute(LastAttribute newLastAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_LastAttribute(), newLastAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastAttribute(LastAttribute newLastAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_LastAttribute(), newLastAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LastBitAttribute getLastBitAttribute() {
		return (LastBitAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_LastBitAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLastBitAttribute(LastBitAttribute newLastBitAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_LastBitAttribute(), newLastBitAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastBitAttribute(LastBitAttribute newLastBitAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_LastBitAttribute(), newLastBitAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeadingPartAttribute getLeadingPartAttribute() {
		return (LeadingPartAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_LeadingPartAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLeadingPartAttribute(LeadingPartAttribute newLeadingPartAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_LeadingPartAttribute(), newLeadingPartAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLeadingPartAttribute(LeadingPartAttribute newLeadingPartAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_LeadingPartAttribute(), newLeadingPartAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LengthAttribute getLengthAttribute() {
		return (LengthAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_LengthAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLengthAttribute(LengthAttribute newLengthAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_LengthAttribute(), newLengthAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLengthAttribute(LengthAttribute newLengthAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_LengthAttribute(), newLengthAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LessThanOperator getLessThanOperator() {
		return (LessThanOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_LessThanOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLessThanOperator(LessThanOperator newLessThanOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_LessThanOperator(), newLessThanOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLessThanOperator(LessThanOperator newLessThanOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_LessThanOperator(), newLessThanOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LessThanOrEqualOperator getLessThanOrEqualOperator() {
		return (LessThanOrEqualOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_LessThanOrEqualOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLessThanOrEqualOperator(LessThanOrEqualOperator newLessThanOrEqualOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_LessThanOrEqualOperator(), newLessThanOrEqualOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLessThanOrEqualOperator(LessThanOrEqualOperator newLessThanOrEqualOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_LessThanOrEqualOperator(), newLessThanOrEqualOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Limited getLimited() {
		return (Limited)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_Limited(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLimited(Limited newLimited, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_Limited(), newLimited, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLimited(Limited newLimited) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_Limited(), newLimited);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LimitedInterface getLimitedInterface() {
		return (LimitedInterface)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_LimitedInterface(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLimitedInterface(LimitedInterface newLimitedInterface, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_LimitedInterface(), newLimitedInterface, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLimitedInterface(LimitedInterface newLimitedInterface) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_LimitedInterface(), newLimitedInterface);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkerOptionsPragma getLinkerOptionsPragma() {
		return (LinkerOptionsPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_LinkerOptionsPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLinkerOptionsPragma(LinkerOptionsPragma newLinkerOptionsPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_LinkerOptionsPragma(), newLinkerOptionsPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLinkerOptionsPragma(LinkerOptionsPragma newLinkerOptionsPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_LinkerOptionsPragma(), newLinkerOptionsPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ListPragma getListPragma() {
		return (ListPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ListPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetListPragma(ListPragma newListPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ListPragma(), newListPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setListPragma(ListPragma newListPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ListPragma(), newListPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LockingPolicyPragma getLockingPolicyPragma() {
		return (LockingPolicyPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_LockingPolicyPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLockingPolicyPragma(LockingPolicyPragma newLockingPolicyPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_LockingPolicyPragma(), newLockingPolicyPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLockingPolicyPragma(LockingPolicyPragma newLockingPolicyPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_LockingPolicyPragma(), newLockingPolicyPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LoopParameterSpecification getLoopParameterSpecification() {
		return (LoopParameterSpecification)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_LoopParameterSpecification(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLoopParameterSpecification(LoopParameterSpecification newLoopParameterSpecification, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_LoopParameterSpecification(), newLoopParameterSpecification, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLoopParameterSpecification(LoopParameterSpecification newLoopParameterSpecification) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_LoopParameterSpecification(), newLoopParameterSpecification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LoopStatement getLoopStatement() {
		return (LoopStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_LoopStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLoopStatement(LoopStatement newLoopStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_LoopStatement(), newLoopStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLoopStatement(LoopStatement newLoopStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_LoopStatement(), newLoopStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MachineAttribute getMachineAttribute() {
		return (MachineAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_MachineAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMachineAttribute(MachineAttribute newMachineAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_MachineAttribute(), newMachineAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMachineAttribute(MachineAttribute newMachineAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_MachineAttribute(), newMachineAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MachineEmaxAttribute getMachineEmaxAttribute() {
		return (MachineEmaxAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_MachineEmaxAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMachineEmaxAttribute(MachineEmaxAttribute newMachineEmaxAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_MachineEmaxAttribute(), newMachineEmaxAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMachineEmaxAttribute(MachineEmaxAttribute newMachineEmaxAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_MachineEmaxAttribute(), newMachineEmaxAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MachineEminAttribute getMachineEminAttribute() {
		return (MachineEminAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_MachineEminAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMachineEminAttribute(MachineEminAttribute newMachineEminAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_MachineEminAttribute(), newMachineEminAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMachineEminAttribute(MachineEminAttribute newMachineEminAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_MachineEminAttribute(), newMachineEminAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MachineMantissaAttribute getMachineMantissaAttribute() {
		return (MachineMantissaAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_MachineMantissaAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMachineMantissaAttribute(MachineMantissaAttribute newMachineMantissaAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_MachineMantissaAttribute(), newMachineMantissaAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMachineMantissaAttribute(MachineMantissaAttribute newMachineMantissaAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_MachineMantissaAttribute(), newMachineMantissaAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MachineOverflowsAttribute getMachineOverflowsAttribute() {
		return (MachineOverflowsAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_MachineOverflowsAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMachineOverflowsAttribute(MachineOverflowsAttribute newMachineOverflowsAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_MachineOverflowsAttribute(), newMachineOverflowsAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMachineOverflowsAttribute(MachineOverflowsAttribute newMachineOverflowsAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_MachineOverflowsAttribute(), newMachineOverflowsAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MachineRadixAttribute getMachineRadixAttribute() {
		return (MachineRadixAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_MachineRadixAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMachineRadixAttribute(MachineRadixAttribute newMachineRadixAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_MachineRadixAttribute(), newMachineRadixAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMachineRadixAttribute(MachineRadixAttribute newMachineRadixAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_MachineRadixAttribute(), newMachineRadixAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MachineRoundingAttribute getMachineRoundingAttribute() {
		return (MachineRoundingAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_MachineRoundingAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMachineRoundingAttribute(MachineRoundingAttribute newMachineRoundingAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_MachineRoundingAttribute(), newMachineRoundingAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMachineRoundingAttribute(MachineRoundingAttribute newMachineRoundingAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_MachineRoundingAttribute(), newMachineRoundingAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MachineRoundsAttribute getMachineRoundsAttribute() {
		return (MachineRoundsAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_MachineRoundsAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMachineRoundsAttribute(MachineRoundsAttribute newMachineRoundsAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_MachineRoundsAttribute(), newMachineRoundsAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMachineRoundsAttribute(MachineRoundsAttribute newMachineRoundsAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_MachineRoundsAttribute(), newMachineRoundsAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaxAlignmentForAllocationAttribute getMaxAlignmentForAllocationAttribute() {
		return (MaxAlignmentForAllocationAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_MaxAlignmentForAllocationAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMaxAlignmentForAllocationAttribute(MaxAlignmentForAllocationAttribute newMaxAlignmentForAllocationAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_MaxAlignmentForAllocationAttribute(), newMaxAlignmentForAllocationAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxAlignmentForAllocationAttribute(MaxAlignmentForAllocationAttribute newMaxAlignmentForAllocationAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_MaxAlignmentForAllocationAttribute(), newMaxAlignmentForAllocationAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaxAttribute getMaxAttribute() {
		return (MaxAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_MaxAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMaxAttribute(MaxAttribute newMaxAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_MaxAttribute(), newMaxAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxAttribute(MaxAttribute newMaxAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_MaxAttribute(), newMaxAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaxSizeInStorageElementsAttribute getMaxSizeInStorageElementsAttribute() {
		return (MaxSizeInStorageElementsAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_MaxSizeInStorageElementsAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMaxSizeInStorageElementsAttribute(MaxSizeInStorageElementsAttribute newMaxSizeInStorageElementsAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_MaxSizeInStorageElementsAttribute(), newMaxSizeInStorageElementsAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxSizeInStorageElementsAttribute(MaxSizeInStorageElementsAttribute newMaxSizeInStorageElementsAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_MaxSizeInStorageElementsAttribute(), newMaxSizeInStorageElementsAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MinAttribute getMinAttribute() {
		return (MinAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_MinAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMinAttribute(MinAttribute newMinAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_MinAttribute(), newMinAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinAttribute(MinAttribute newMinAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_MinAttribute(), newMinAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MinusOperator getMinusOperator() {
		return (MinusOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_MinusOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMinusOperator(MinusOperator newMinusOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_MinusOperator(), newMinusOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinusOperator(MinusOperator newMinusOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_MinusOperator(), newMinusOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModAttribute getModAttribute() {
		return (ModAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ModAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetModAttribute(ModAttribute newModAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ModAttribute(), newModAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModAttribute(ModAttribute newModAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ModAttribute(), newModAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModOperator getModOperator() {
		return (ModOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ModOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetModOperator(ModOperator newModOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ModOperator(), newModOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModOperator(ModOperator newModOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ModOperator(), newModOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelAttribute getModelAttribute() {
		return (ModelAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ModelAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetModelAttribute(ModelAttribute newModelAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ModelAttribute(), newModelAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelAttribute(ModelAttribute newModelAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ModelAttribute(), newModelAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelEminAttribute getModelEminAttribute() {
		return (ModelEminAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ModelEminAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetModelEminAttribute(ModelEminAttribute newModelEminAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ModelEminAttribute(), newModelEminAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelEminAttribute(ModelEminAttribute newModelEminAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ModelEminAttribute(), newModelEminAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelEpsilonAttribute getModelEpsilonAttribute() {
		return (ModelEpsilonAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ModelEpsilonAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetModelEpsilonAttribute(ModelEpsilonAttribute newModelEpsilonAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ModelEpsilonAttribute(), newModelEpsilonAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelEpsilonAttribute(ModelEpsilonAttribute newModelEpsilonAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ModelEpsilonAttribute(), newModelEpsilonAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelMantissaAttribute getModelMantissaAttribute() {
		return (ModelMantissaAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ModelMantissaAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetModelMantissaAttribute(ModelMantissaAttribute newModelMantissaAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ModelMantissaAttribute(), newModelMantissaAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelMantissaAttribute(ModelMantissaAttribute newModelMantissaAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ModelMantissaAttribute(), newModelMantissaAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelSmallAttribute getModelSmallAttribute() {
		return (ModelSmallAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ModelSmallAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetModelSmallAttribute(ModelSmallAttribute newModelSmallAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ModelSmallAttribute(), newModelSmallAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelSmallAttribute(ModelSmallAttribute newModelSmallAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ModelSmallAttribute(), newModelSmallAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModularTypeDefinition getModularTypeDefinition() {
		return (ModularTypeDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ModularTypeDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetModularTypeDefinition(ModularTypeDefinition newModularTypeDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ModularTypeDefinition(), newModularTypeDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModularTypeDefinition(ModularTypeDefinition newModularTypeDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ModularTypeDefinition(), newModularTypeDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModulusAttribute getModulusAttribute() {
		return (ModulusAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ModulusAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetModulusAttribute(ModulusAttribute newModulusAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ModulusAttribute(), newModulusAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModulusAttribute(ModulusAttribute newModulusAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ModulusAttribute(), newModulusAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiplyOperator getMultiplyOperator() {
		return (MultiplyOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_MultiplyOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMultiplyOperator(MultiplyOperator newMultiplyOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_MultiplyOperator(), newMultiplyOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMultiplyOperator(MultiplyOperator newMultiplyOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_MultiplyOperator(), newMultiplyOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NamedArrayAggregate getNamedArrayAggregate() {
		return (NamedArrayAggregate)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_NamedArrayAggregate(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNamedArrayAggregate(NamedArrayAggregate newNamedArrayAggregate, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_NamedArrayAggregate(), newNamedArrayAggregate, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNamedArrayAggregate(NamedArrayAggregate newNamedArrayAggregate) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_NamedArrayAggregate(), newNamedArrayAggregate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NoReturnPragma getNoReturnPragma() {
		return (NoReturnPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_NoReturnPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNoReturnPragma(NoReturnPragma newNoReturnPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_NoReturnPragma(), newNoReturnPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNoReturnPragma(NoReturnPragma newNoReturnPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_NoReturnPragma(), newNoReturnPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NormalizeScalarsPragma getNormalizeScalarsPragma() {
		return (NormalizeScalarsPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_NormalizeScalarsPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNormalizeScalarsPragma(NormalizeScalarsPragma newNormalizeScalarsPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_NormalizeScalarsPragma(), newNormalizeScalarsPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNormalizeScalarsPragma(NormalizeScalarsPragma newNormalizeScalarsPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_NormalizeScalarsPragma(), newNormalizeScalarsPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotAnElement getNotAnElement() {
		return (NotAnElement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_NotAnElement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNotAnElement(NotAnElement newNotAnElement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_NotAnElement(), newNotAnElement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotAnElement(NotAnElement newNotAnElement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_NotAnElement(), newNotAnElement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotEqualOperator getNotEqualOperator() {
		return (NotEqualOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_NotEqualOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNotEqualOperator(NotEqualOperator newNotEqualOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_NotEqualOperator(), newNotEqualOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotEqualOperator(NotEqualOperator newNotEqualOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_NotEqualOperator(), newNotEqualOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotInMembershipTest getNotInMembershipTest() {
		return (NotInMembershipTest)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_NotInMembershipTest(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNotInMembershipTest(NotInMembershipTest newNotInMembershipTest, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_NotInMembershipTest(), newNotInMembershipTest, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotInMembershipTest(NotInMembershipTest newNotInMembershipTest) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_NotInMembershipTest(), newNotInMembershipTest);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotNullReturn getNotNullReturn() {
		return (NotNullReturn)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_NotNullReturn(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNotNullReturn(NotNullReturn newNotNullReturn, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_NotNullReturn(), newNotNullReturn, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotNullReturn(NotNullReturn newNotNullReturn) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_NotNullReturn(), newNotNullReturn);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotOperator getNotOperator() {
		return (NotOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_NotOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNotOperator(NotOperator newNotOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_NotOperator(), newNotOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotOperator(NotOperator newNotOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_NotOperator(), newNotOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotOverriding getNotOverriding() {
		return (NotOverriding)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_NotOverriding(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNotOverriding(NotOverriding newNotOverriding, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_NotOverriding(), newNotOverriding, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotOverriding(NotOverriding newNotOverriding) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_NotOverriding(), newNotOverriding);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NullComponent getNullComponent() {
		return (NullComponent)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_NullComponent(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNullComponent(NullComponent newNullComponent, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_NullComponent(), newNullComponent, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNullComponent(NullComponent newNullComponent) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_NullComponent(), newNullComponent);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NullExclusion getNullExclusion() {
		return (NullExclusion)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_NullExclusion(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNullExclusion(NullExclusion newNullExclusion, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_NullExclusion(), newNullExclusion, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNullExclusion(NullExclusion newNullExclusion) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_NullExclusion(), newNullExclusion);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NullLiteral getNullLiteral() {
		return (NullLiteral)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_NullLiteral(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNullLiteral(NullLiteral newNullLiteral, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_NullLiteral(), newNullLiteral, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNullLiteral(NullLiteral newNullLiteral) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_NullLiteral(), newNullLiteral);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NullProcedureDeclaration getNullProcedureDeclaration() {
		return (NullProcedureDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_NullProcedureDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNullProcedureDeclaration(NullProcedureDeclaration newNullProcedureDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_NullProcedureDeclaration(), newNullProcedureDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNullProcedureDeclaration(NullProcedureDeclaration newNullProcedureDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_NullProcedureDeclaration(), newNullProcedureDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NullRecordDefinition getNullRecordDefinition() {
		return (NullRecordDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_NullRecordDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNullRecordDefinition(NullRecordDefinition newNullRecordDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_NullRecordDefinition(), newNullRecordDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNullRecordDefinition(NullRecordDefinition newNullRecordDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_NullRecordDefinition(), newNullRecordDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NullStatement getNullStatement() {
		return (NullStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_NullStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNullStatement(NullStatement newNullStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_NullStatement(), newNullStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNullStatement(NullStatement newNullStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_NullStatement(), newNullStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectRenamingDeclaration getObjectRenamingDeclaration() {
		return (ObjectRenamingDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ObjectRenamingDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectRenamingDeclaration(ObjectRenamingDeclaration newObjectRenamingDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ObjectRenamingDeclaration(), newObjectRenamingDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectRenamingDeclaration(ObjectRenamingDeclaration newObjectRenamingDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ObjectRenamingDeclaration(), newObjectRenamingDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OptimizePragma getOptimizePragma() {
		return (OptimizePragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_OptimizePragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOptimizePragma(OptimizePragma newOptimizePragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_OptimizePragma(), newOptimizePragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOptimizePragma(OptimizePragma newOptimizePragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_OptimizePragma(), newOptimizePragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrElseShortCircuit getOrElseShortCircuit() {
		return (OrElseShortCircuit)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_OrElseShortCircuit(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOrElseShortCircuit(OrElseShortCircuit newOrElseShortCircuit, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_OrElseShortCircuit(), newOrElseShortCircuit, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrElseShortCircuit(OrElseShortCircuit newOrElseShortCircuit) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_OrElseShortCircuit(), newOrElseShortCircuit);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrOperator getOrOperator() {
		return (OrOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_OrOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOrOperator(OrOperator newOrOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_OrOperator(), newOrOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrOperator(OrOperator newOrOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_OrOperator(), newOrOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrPath getOrPath() {
		return (OrPath)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_OrPath(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOrPath(OrPath newOrPath, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_OrPath(), newOrPath, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrPath(OrPath newOrPath) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_OrPath(), newOrPath);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrdinaryFixedPointDefinition getOrdinaryFixedPointDefinition() {
		return (OrdinaryFixedPointDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_OrdinaryFixedPointDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOrdinaryFixedPointDefinition(OrdinaryFixedPointDefinition newOrdinaryFixedPointDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_OrdinaryFixedPointDefinition(), newOrdinaryFixedPointDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrdinaryFixedPointDefinition(OrdinaryFixedPointDefinition newOrdinaryFixedPointDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_OrdinaryFixedPointDefinition(), newOrdinaryFixedPointDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrdinaryInterface getOrdinaryInterface() {
		return (OrdinaryInterface)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_OrdinaryInterface(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOrdinaryInterface(OrdinaryInterface newOrdinaryInterface, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_OrdinaryInterface(), newOrdinaryInterface, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrdinaryInterface(OrdinaryInterface newOrdinaryInterface) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_OrdinaryInterface(), newOrdinaryInterface);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrdinaryTypeDeclaration getOrdinaryTypeDeclaration() {
		return (OrdinaryTypeDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_OrdinaryTypeDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOrdinaryTypeDeclaration(OrdinaryTypeDeclaration newOrdinaryTypeDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_OrdinaryTypeDeclaration(), newOrdinaryTypeDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrdinaryTypeDeclaration(OrdinaryTypeDeclaration newOrdinaryTypeDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_OrdinaryTypeDeclaration(), newOrdinaryTypeDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OthersChoice getOthersChoice() {
		return (OthersChoice)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_OthersChoice(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOthersChoice(OthersChoice newOthersChoice, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_OthersChoice(), newOthersChoice, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOthersChoice(OthersChoice newOthersChoice) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_OthersChoice(), newOthersChoice);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutputAttribute getOutputAttribute() {
		return (OutputAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_OutputAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOutputAttribute(OutputAttribute newOutputAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_OutputAttribute(), newOutputAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutputAttribute(OutputAttribute newOutputAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_OutputAttribute(), newOutputAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OverlapsStorageAttribute getOverlapsStorageAttribute() {
		return (OverlapsStorageAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_OverlapsStorageAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOverlapsStorageAttribute(OverlapsStorageAttribute newOverlapsStorageAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_OverlapsStorageAttribute(), newOverlapsStorageAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOverlapsStorageAttribute(OverlapsStorageAttribute newOverlapsStorageAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_OverlapsStorageAttribute(), newOverlapsStorageAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Overriding getOverriding() {
		return (Overriding)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_Overriding(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOverriding(Overriding newOverriding, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_Overriding(), newOverriding, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOverriding(Overriding newOverriding) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_Overriding(), newOverriding);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackPragma getPackPragma() {
		return (PackPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PackPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPackPragma(PackPragma newPackPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PackPragma(), newPackPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPackPragma(PackPragma newPackPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PackPragma(), newPackPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackageBodyDeclaration getPackageBodyDeclaration() {
		return (PackageBodyDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PackageBodyDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPackageBodyDeclaration(PackageBodyDeclaration newPackageBodyDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PackageBodyDeclaration(), newPackageBodyDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPackageBodyDeclaration(PackageBodyDeclaration newPackageBodyDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PackageBodyDeclaration(), newPackageBodyDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackageBodyStub getPackageBodyStub() {
		return (PackageBodyStub)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PackageBodyStub(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPackageBodyStub(PackageBodyStub newPackageBodyStub, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PackageBodyStub(), newPackageBodyStub, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPackageBodyStub(PackageBodyStub newPackageBodyStub) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PackageBodyStub(), newPackageBodyStub);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackageDeclaration getPackageDeclaration() {
		return (PackageDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PackageDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPackageDeclaration(PackageDeclaration newPackageDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PackageDeclaration(), newPackageDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPackageDeclaration(PackageDeclaration newPackageDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PackageDeclaration(), newPackageDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackageInstantiation getPackageInstantiation() {
		return (PackageInstantiation)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PackageInstantiation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPackageInstantiation(PackageInstantiation newPackageInstantiation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PackageInstantiation(), newPackageInstantiation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPackageInstantiation(PackageInstantiation newPackageInstantiation) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PackageInstantiation(), newPackageInstantiation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackageRenamingDeclaration getPackageRenamingDeclaration() {
		return (PackageRenamingDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PackageRenamingDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPackageRenamingDeclaration(PackageRenamingDeclaration newPackageRenamingDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PackageRenamingDeclaration(), newPackageRenamingDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPackageRenamingDeclaration(PackageRenamingDeclaration newPackageRenamingDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PackageRenamingDeclaration(), newPackageRenamingDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PagePragma getPagePragma() {
		return (PagePragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PagePragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPagePragma(PagePragma newPagePragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PagePragma(), newPagePragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPagePragma(PagePragma newPagePragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PagePragma(), newPagePragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterAssociation getParameterAssociation() {
		return (ParameterAssociation)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ParameterAssociation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParameterAssociation(ParameterAssociation newParameterAssociation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ParameterAssociation(), newParameterAssociation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterAssociation(ParameterAssociation newParameterAssociation) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ParameterAssociation(), newParameterAssociation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterSpecification getParameterSpecification() {
		return (ParameterSpecification)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ParameterSpecification(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParameterSpecification(ParameterSpecification newParameterSpecification, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ParameterSpecification(), newParameterSpecification, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterSpecification(ParameterSpecification newParameterSpecification) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ParameterSpecification(), newParameterSpecification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParenthesizedExpression getParenthesizedExpression() {
		return (ParenthesizedExpression)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ParenthesizedExpression(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParenthesizedExpression(ParenthesizedExpression newParenthesizedExpression, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ParenthesizedExpression(), newParenthesizedExpression, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParenthesizedExpression(ParenthesizedExpression newParenthesizedExpression) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ParenthesizedExpression(), newParenthesizedExpression);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PartitionElaborationPolicyPragma getPartitionElaborationPolicyPragma() {
		return (PartitionElaborationPolicyPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PartitionElaborationPolicyPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPartitionElaborationPolicyPragma(PartitionElaborationPolicyPragma newPartitionElaborationPolicyPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PartitionElaborationPolicyPragma(), newPartitionElaborationPolicyPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPartitionElaborationPolicyPragma(PartitionElaborationPolicyPragma newPartitionElaborationPolicyPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PartitionElaborationPolicyPragma(), newPartitionElaborationPolicyPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PartitionIdAttribute getPartitionIdAttribute() {
		return (PartitionIdAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PartitionIdAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPartitionIdAttribute(PartitionIdAttribute newPartitionIdAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PartitionIdAttribute(), newPartitionIdAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPartitionIdAttribute(PartitionIdAttribute newPartitionIdAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PartitionIdAttribute(), newPartitionIdAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PlusOperator getPlusOperator() {
		return (PlusOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PlusOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPlusOperator(PlusOperator newPlusOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PlusOperator(), newPlusOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPlusOperator(PlusOperator newPlusOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PlusOperator(), newPlusOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PoolSpecificAccessToVariable getPoolSpecificAccessToVariable() {
		return (PoolSpecificAccessToVariable)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PoolSpecificAccessToVariable(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPoolSpecificAccessToVariable(PoolSpecificAccessToVariable newPoolSpecificAccessToVariable, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PoolSpecificAccessToVariable(), newPoolSpecificAccessToVariable, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPoolSpecificAccessToVariable(PoolSpecificAccessToVariable newPoolSpecificAccessToVariable) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PoolSpecificAccessToVariable(), newPoolSpecificAccessToVariable);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PosAttribute getPosAttribute() {
		return (PosAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PosAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPosAttribute(PosAttribute newPosAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PosAttribute(), newPosAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPosAttribute(PosAttribute newPosAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PosAttribute(), newPosAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PositionAttribute getPositionAttribute() {
		return (PositionAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PositionAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPositionAttribute(PositionAttribute newPositionAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PositionAttribute(), newPositionAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPositionAttribute(PositionAttribute newPositionAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PositionAttribute(), newPositionAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PositionalArrayAggregate getPositionalArrayAggregate() {
		return (PositionalArrayAggregate)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PositionalArrayAggregate(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPositionalArrayAggregate(PositionalArrayAggregate newPositionalArrayAggregate, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PositionalArrayAggregate(), newPositionalArrayAggregate, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPositionalArrayAggregate(PositionalArrayAggregate newPositionalArrayAggregate) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PositionalArrayAggregate(), newPositionalArrayAggregate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PragmaArgumentAssociation getPragmaArgumentAssociation() {
		return (PragmaArgumentAssociation)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PragmaArgumentAssociation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPragmaArgumentAssociation(PragmaArgumentAssociation newPragmaArgumentAssociation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PragmaArgumentAssociation(), newPragmaArgumentAssociation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPragmaArgumentAssociation(PragmaArgumentAssociation newPragmaArgumentAssociation) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PragmaArgumentAssociation(), newPragmaArgumentAssociation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PredAttribute getPredAttribute() {
		return (PredAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PredAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPredAttribute(PredAttribute newPredAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PredAttribute(), newPredAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPredAttribute(PredAttribute newPredAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PredAttribute(), newPredAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PreelaborableInitializationPragma getPreelaborableInitializationPragma() {
		return (PreelaborableInitializationPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PreelaborableInitializationPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPreelaborableInitializationPragma(PreelaborableInitializationPragma newPreelaborableInitializationPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PreelaborableInitializationPragma(), newPreelaborableInitializationPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreelaborableInitializationPragma(PreelaborableInitializationPragma newPreelaborableInitializationPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PreelaborableInitializationPragma(), newPreelaborableInitializationPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PreelaboratePragma getPreelaboratePragma() {
		return (PreelaboratePragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PreelaboratePragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPreelaboratePragma(PreelaboratePragma newPreelaboratePragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PreelaboratePragma(), newPreelaboratePragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreelaboratePragma(PreelaboratePragma newPreelaboratePragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PreelaboratePragma(), newPreelaboratePragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PriorityAttribute getPriorityAttribute() {
		return (PriorityAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PriorityAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPriorityAttribute(PriorityAttribute newPriorityAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PriorityAttribute(), newPriorityAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPriorityAttribute(PriorityAttribute newPriorityAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PriorityAttribute(), newPriorityAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PriorityPragma getPriorityPragma() {
		return (PriorityPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PriorityPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPriorityPragma(PriorityPragma newPriorityPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PriorityPragma(), newPriorityPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPriorityPragma(PriorityPragma newPriorityPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PriorityPragma(), newPriorityPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrioritySpecificDispatchingPragma getPrioritySpecificDispatchingPragma() {
		return (PrioritySpecificDispatchingPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PrioritySpecificDispatchingPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrioritySpecificDispatchingPragma(PrioritySpecificDispatchingPragma newPrioritySpecificDispatchingPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PrioritySpecificDispatchingPragma(), newPrioritySpecificDispatchingPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrioritySpecificDispatchingPragma(PrioritySpecificDispatchingPragma newPrioritySpecificDispatchingPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PrioritySpecificDispatchingPragma(), newPrioritySpecificDispatchingPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Private getPrivate() {
		return (Private)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_Private(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrivate(Private newPrivate, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_Private(), newPrivate, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrivate(Private newPrivate) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_Private(), newPrivate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrivateExtensionDeclaration getPrivateExtensionDeclaration() {
		return (PrivateExtensionDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PrivateExtensionDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrivateExtensionDeclaration(PrivateExtensionDeclaration newPrivateExtensionDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PrivateExtensionDeclaration(), newPrivateExtensionDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrivateExtensionDeclaration(PrivateExtensionDeclaration newPrivateExtensionDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PrivateExtensionDeclaration(), newPrivateExtensionDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrivateExtensionDefinition getPrivateExtensionDefinition() {
		return (PrivateExtensionDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PrivateExtensionDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrivateExtensionDefinition(PrivateExtensionDefinition newPrivateExtensionDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PrivateExtensionDefinition(), newPrivateExtensionDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrivateExtensionDefinition(PrivateExtensionDefinition newPrivateExtensionDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PrivateExtensionDefinition(), newPrivateExtensionDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrivateTypeDeclaration getPrivateTypeDeclaration() {
		return (PrivateTypeDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PrivateTypeDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrivateTypeDeclaration(PrivateTypeDeclaration newPrivateTypeDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PrivateTypeDeclaration(), newPrivateTypeDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrivateTypeDeclaration(PrivateTypeDeclaration newPrivateTypeDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PrivateTypeDeclaration(), newPrivateTypeDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrivateTypeDefinition getPrivateTypeDefinition() {
		return (PrivateTypeDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PrivateTypeDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrivateTypeDefinition(PrivateTypeDefinition newPrivateTypeDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PrivateTypeDefinition(), newPrivateTypeDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrivateTypeDefinition(PrivateTypeDefinition newPrivateTypeDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PrivateTypeDefinition(), newPrivateTypeDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureBodyDeclaration getProcedureBodyDeclaration() {
		return (ProcedureBodyDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ProcedureBodyDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcedureBodyDeclaration(ProcedureBodyDeclaration newProcedureBodyDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ProcedureBodyDeclaration(), newProcedureBodyDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcedureBodyDeclaration(ProcedureBodyDeclaration newProcedureBodyDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ProcedureBodyDeclaration(), newProcedureBodyDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureBodyStub getProcedureBodyStub() {
		return (ProcedureBodyStub)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ProcedureBodyStub(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcedureBodyStub(ProcedureBodyStub newProcedureBodyStub, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ProcedureBodyStub(), newProcedureBodyStub, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcedureBodyStub(ProcedureBodyStub newProcedureBodyStub) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ProcedureBodyStub(), newProcedureBodyStub);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureCallStatement getProcedureCallStatement() {
		return (ProcedureCallStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ProcedureCallStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcedureCallStatement(ProcedureCallStatement newProcedureCallStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ProcedureCallStatement(), newProcedureCallStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcedureCallStatement(ProcedureCallStatement newProcedureCallStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ProcedureCallStatement(), newProcedureCallStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureDeclaration getProcedureDeclaration() {
		return (ProcedureDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ProcedureDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcedureDeclaration(ProcedureDeclaration newProcedureDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ProcedureDeclaration(), newProcedureDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcedureDeclaration(ProcedureDeclaration newProcedureDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ProcedureDeclaration(), newProcedureDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureInstantiation getProcedureInstantiation() {
		return (ProcedureInstantiation)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ProcedureInstantiation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcedureInstantiation(ProcedureInstantiation newProcedureInstantiation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ProcedureInstantiation(), newProcedureInstantiation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcedureInstantiation(ProcedureInstantiation newProcedureInstantiation) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ProcedureInstantiation(), newProcedureInstantiation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureRenamingDeclaration getProcedureRenamingDeclaration() {
		return (ProcedureRenamingDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ProcedureRenamingDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcedureRenamingDeclaration(ProcedureRenamingDeclaration newProcedureRenamingDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ProcedureRenamingDeclaration(), newProcedureRenamingDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcedureRenamingDeclaration(ProcedureRenamingDeclaration newProcedureRenamingDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ProcedureRenamingDeclaration(), newProcedureRenamingDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProfilePragma getProfilePragma() {
		return (ProfilePragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ProfilePragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProfilePragma(ProfilePragma newProfilePragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ProfilePragma(), newProfilePragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProfilePragma(ProfilePragma newProfilePragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ProfilePragma(), newProfilePragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProtectedBodyDeclaration getProtectedBodyDeclaration() {
		return (ProtectedBodyDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ProtectedBodyDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProtectedBodyDeclaration(ProtectedBodyDeclaration newProtectedBodyDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ProtectedBodyDeclaration(), newProtectedBodyDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProtectedBodyDeclaration(ProtectedBodyDeclaration newProtectedBodyDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ProtectedBodyDeclaration(), newProtectedBodyDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProtectedBodyStub getProtectedBodyStub() {
		return (ProtectedBodyStub)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ProtectedBodyStub(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProtectedBodyStub(ProtectedBodyStub newProtectedBodyStub, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ProtectedBodyStub(), newProtectedBodyStub, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProtectedBodyStub(ProtectedBodyStub newProtectedBodyStub) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ProtectedBodyStub(), newProtectedBodyStub);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProtectedDefinition getProtectedDefinition() {
		return (ProtectedDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ProtectedDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProtectedDefinition(ProtectedDefinition newProtectedDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ProtectedDefinition(), newProtectedDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProtectedDefinition(ProtectedDefinition newProtectedDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ProtectedDefinition(), newProtectedDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProtectedInterface getProtectedInterface() {
		return (ProtectedInterface)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ProtectedInterface(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProtectedInterface(ProtectedInterface newProtectedInterface, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ProtectedInterface(), newProtectedInterface, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProtectedInterface(ProtectedInterface newProtectedInterface) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ProtectedInterface(), newProtectedInterface);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProtectedTypeDeclaration getProtectedTypeDeclaration() {
		return (ProtectedTypeDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ProtectedTypeDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProtectedTypeDeclaration(ProtectedTypeDeclaration newProtectedTypeDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ProtectedTypeDeclaration(), newProtectedTypeDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProtectedTypeDeclaration(ProtectedTypeDeclaration newProtectedTypeDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ProtectedTypeDeclaration(), newProtectedTypeDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PurePragma getPurePragma() {
		return (PurePragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_PurePragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPurePragma(PurePragma newPurePragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_PurePragma(), newPurePragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPurePragma(PurePragma newPurePragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_PurePragma(), newPurePragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QualifiedExpression getQualifiedExpression() {
		return (QualifiedExpression)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_QualifiedExpression(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetQualifiedExpression(QualifiedExpression newQualifiedExpression, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_QualifiedExpression(), newQualifiedExpression, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQualifiedExpression(QualifiedExpression newQualifiedExpression) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_QualifiedExpression(), newQualifiedExpression);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QueuingPolicyPragma getQueuingPolicyPragma() {
		return (QueuingPolicyPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_QueuingPolicyPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetQueuingPolicyPragma(QueuingPolicyPragma newQueuingPolicyPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_QueuingPolicyPragma(), newQueuingPolicyPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQueuingPolicyPragma(QueuingPolicyPragma newQueuingPolicyPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_QueuingPolicyPragma(), newQueuingPolicyPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RaiseExpression getRaiseExpression() {
		return (RaiseExpression)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_RaiseExpression(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRaiseExpression(RaiseExpression newRaiseExpression, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_RaiseExpression(), newRaiseExpression, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRaiseExpression(RaiseExpression newRaiseExpression) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_RaiseExpression(), newRaiseExpression);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RaiseStatement getRaiseStatement() {
		return (RaiseStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_RaiseStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRaiseStatement(RaiseStatement newRaiseStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_RaiseStatement(), newRaiseStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRaiseStatement(RaiseStatement newRaiseStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_RaiseStatement(), newRaiseStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RangeAttribute getRangeAttribute() {
		return (RangeAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_RangeAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRangeAttribute(RangeAttribute newRangeAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_RangeAttribute(), newRangeAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRangeAttribute(RangeAttribute newRangeAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_RangeAttribute(), newRangeAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RangeAttributeReference getRangeAttributeReference() {
		return (RangeAttributeReference)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_RangeAttributeReference(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRangeAttributeReference(RangeAttributeReference newRangeAttributeReference, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_RangeAttributeReference(), newRangeAttributeReference, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRangeAttributeReference(RangeAttributeReference newRangeAttributeReference) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_RangeAttributeReference(), newRangeAttributeReference);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReadAttribute getReadAttribute() {
		return (ReadAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ReadAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReadAttribute(ReadAttribute newReadAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ReadAttribute(), newReadAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReadAttribute(ReadAttribute newReadAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ReadAttribute(), newReadAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RealLiteral getRealLiteral() {
		return (RealLiteral)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_RealLiteral(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRealLiteral(RealLiteral newRealLiteral, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_RealLiteral(), newRealLiteral, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRealLiteral(RealLiteral newRealLiteral) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_RealLiteral(), newRealLiteral);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RealNumberDeclaration getRealNumberDeclaration() {
		return (RealNumberDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_RealNumberDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRealNumberDeclaration(RealNumberDeclaration newRealNumberDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_RealNumberDeclaration(), newRealNumberDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRealNumberDeclaration(RealNumberDeclaration newRealNumberDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_RealNumberDeclaration(), newRealNumberDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordAggregate getRecordAggregate() {
		return (RecordAggregate)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_RecordAggregate(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRecordAggregate(RecordAggregate newRecordAggregate, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_RecordAggregate(), newRecordAggregate, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRecordAggregate(RecordAggregate newRecordAggregate) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_RecordAggregate(), newRecordAggregate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordComponentAssociation getRecordComponentAssociation() {
		return (RecordComponentAssociation)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_RecordComponentAssociation(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRecordComponentAssociation(RecordComponentAssociation newRecordComponentAssociation, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_RecordComponentAssociation(), newRecordComponentAssociation, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRecordComponentAssociation(RecordComponentAssociation newRecordComponentAssociation) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_RecordComponentAssociation(), newRecordComponentAssociation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordDefinition getRecordDefinition() {
		return (RecordDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_RecordDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRecordDefinition(RecordDefinition newRecordDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_RecordDefinition(), newRecordDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRecordDefinition(RecordDefinition newRecordDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_RecordDefinition(), newRecordDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordRepresentationClause getRecordRepresentationClause() {
		return (RecordRepresentationClause)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_RecordRepresentationClause(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRecordRepresentationClause(RecordRepresentationClause newRecordRepresentationClause, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_RecordRepresentationClause(), newRecordRepresentationClause, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRecordRepresentationClause(RecordRepresentationClause newRecordRepresentationClause) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_RecordRepresentationClause(), newRecordRepresentationClause);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordTypeDefinition getRecordTypeDefinition() {
		return (RecordTypeDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_RecordTypeDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRecordTypeDefinition(RecordTypeDefinition newRecordTypeDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_RecordTypeDefinition(), newRecordTypeDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRecordTypeDefinition(RecordTypeDefinition newRecordTypeDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_RecordTypeDefinition(), newRecordTypeDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelativeDeadlinePragma getRelativeDeadlinePragma() {
		return (RelativeDeadlinePragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_RelativeDeadlinePragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRelativeDeadlinePragma(RelativeDeadlinePragma newRelativeDeadlinePragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_RelativeDeadlinePragma(), newRelativeDeadlinePragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRelativeDeadlinePragma(RelativeDeadlinePragma newRelativeDeadlinePragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_RelativeDeadlinePragma(), newRelativeDeadlinePragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RemOperator getRemOperator() {
		return (RemOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_RemOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRemOperator(RemOperator newRemOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_RemOperator(), newRemOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRemOperator(RemOperator newRemOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_RemOperator(), newRemOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RemainderAttribute getRemainderAttribute() {
		return (RemainderAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_RemainderAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRemainderAttribute(RemainderAttribute newRemainderAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_RemainderAttribute(), newRemainderAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRemainderAttribute(RemainderAttribute newRemainderAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_RemainderAttribute(), newRemainderAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RemoteCallInterfacePragma getRemoteCallInterfacePragma() {
		return (RemoteCallInterfacePragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_RemoteCallInterfacePragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRemoteCallInterfacePragma(RemoteCallInterfacePragma newRemoteCallInterfacePragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_RemoteCallInterfacePragma(), newRemoteCallInterfacePragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRemoteCallInterfacePragma(RemoteCallInterfacePragma newRemoteCallInterfacePragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_RemoteCallInterfacePragma(), newRemoteCallInterfacePragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RemoteTypesPragma getRemoteTypesPragma() {
		return (RemoteTypesPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_RemoteTypesPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRemoteTypesPragma(RemoteTypesPragma newRemoteTypesPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_RemoteTypesPragma(), newRemoteTypesPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRemoteTypesPragma(RemoteTypesPragma newRemoteTypesPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_RemoteTypesPragma(), newRemoteTypesPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequeueStatement getRequeueStatement() {
		return (RequeueStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_RequeueStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRequeueStatement(RequeueStatement newRequeueStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_RequeueStatement(), newRequeueStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequeueStatement(RequeueStatement newRequeueStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_RequeueStatement(), newRequeueStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequeueStatementWithAbort getRequeueStatementWithAbort() {
		return (RequeueStatementWithAbort)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_RequeueStatementWithAbort(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRequeueStatementWithAbort(RequeueStatementWithAbort newRequeueStatementWithAbort, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_RequeueStatementWithAbort(), newRequeueStatementWithAbort, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequeueStatementWithAbort(RequeueStatementWithAbort newRequeueStatementWithAbort) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_RequeueStatementWithAbort(), newRequeueStatementWithAbort);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RestrictionsPragma getRestrictionsPragma() {
		return (RestrictionsPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_RestrictionsPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRestrictionsPragma(RestrictionsPragma newRestrictionsPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_RestrictionsPragma(), newRestrictionsPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRestrictionsPragma(RestrictionsPragma newRestrictionsPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_RestrictionsPragma(), newRestrictionsPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReturnConstantSpecification getReturnConstantSpecification() {
		return (ReturnConstantSpecification)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ReturnConstantSpecification(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReturnConstantSpecification(ReturnConstantSpecification newReturnConstantSpecification, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ReturnConstantSpecification(), newReturnConstantSpecification, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReturnConstantSpecification(ReturnConstantSpecification newReturnConstantSpecification) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ReturnConstantSpecification(), newReturnConstantSpecification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReturnStatement getReturnStatement() {
		return (ReturnStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ReturnStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReturnStatement(ReturnStatement newReturnStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ReturnStatement(), newReturnStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReturnStatement(ReturnStatement newReturnStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ReturnStatement(), newReturnStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReturnVariableSpecification getReturnVariableSpecification() {
		return (ReturnVariableSpecification)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ReturnVariableSpecification(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReturnVariableSpecification(ReturnVariableSpecification newReturnVariableSpecification, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ReturnVariableSpecification(), newReturnVariableSpecification, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReturnVariableSpecification(ReturnVariableSpecification newReturnVariableSpecification) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ReturnVariableSpecification(), newReturnVariableSpecification);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reverse getReverse() {
		return (Reverse)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_Reverse(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReverse(Reverse newReverse, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_Reverse(), newReverse, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReverse(Reverse newReverse) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_Reverse(), newReverse);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReviewablePragma getReviewablePragma() {
		return (ReviewablePragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ReviewablePragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReviewablePragma(ReviewablePragma newReviewablePragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ReviewablePragma(), newReviewablePragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReviewablePragma(ReviewablePragma newReviewablePragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ReviewablePragma(), newReviewablePragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RootIntegerDefinition getRootIntegerDefinition() {
		return (RootIntegerDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_RootIntegerDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRootIntegerDefinition(RootIntegerDefinition newRootIntegerDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_RootIntegerDefinition(), newRootIntegerDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRootIntegerDefinition(RootIntegerDefinition newRootIntegerDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_RootIntegerDefinition(), newRootIntegerDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RootRealDefinition getRootRealDefinition() {
		return (RootRealDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_RootRealDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRootRealDefinition(RootRealDefinition newRootRealDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_RootRealDefinition(), newRootRealDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRootRealDefinition(RootRealDefinition newRootRealDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_RootRealDefinition(), newRootRealDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoundAttribute getRoundAttribute() {
		return (RoundAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_RoundAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRoundAttribute(RoundAttribute newRoundAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_RoundAttribute(), newRoundAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoundAttribute(RoundAttribute newRoundAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_RoundAttribute(), newRoundAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoundingAttribute getRoundingAttribute() {
		return (RoundingAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_RoundingAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRoundingAttribute(RoundingAttribute newRoundingAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_RoundingAttribute(), newRoundingAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoundingAttribute(RoundingAttribute newRoundingAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_RoundingAttribute(), newRoundingAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SafeFirstAttribute getSafeFirstAttribute() {
		return (SafeFirstAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_SafeFirstAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSafeFirstAttribute(SafeFirstAttribute newSafeFirstAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_SafeFirstAttribute(), newSafeFirstAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSafeFirstAttribute(SafeFirstAttribute newSafeFirstAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_SafeFirstAttribute(), newSafeFirstAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SafeLastAttribute getSafeLastAttribute() {
		return (SafeLastAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_SafeLastAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSafeLastAttribute(SafeLastAttribute newSafeLastAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_SafeLastAttribute(), newSafeLastAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSafeLastAttribute(SafeLastAttribute newSafeLastAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_SafeLastAttribute(), newSafeLastAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScaleAttribute getScaleAttribute() {
		return (ScaleAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ScaleAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetScaleAttribute(ScaleAttribute newScaleAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ScaleAttribute(), newScaleAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScaleAttribute(ScaleAttribute newScaleAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ScaleAttribute(), newScaleAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScalingAttribute getScalingAttribute() {
		return (ScalingAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ScalingAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetScalingAttribute(ScalingAttribute newScalingAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ScalingAttribute(), newScalingAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScalingAttribute(ScalingAttribute newScalingAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ScalingAttribute(), newScalingAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SelectPath getSelectPath() {
		return (SelectPath)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_SelectPath(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSelectPath(SelectPath newSelectPath, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_SelectPath(), newSelectPath, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelectPath(SelectPath newSelectPath) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_SelectPath(), newSelectPath);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SelectedComponent getSelectedComponent() {
		return (SelectedComponent)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_SelectedComponent(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSelectedComponent(SelectedComponent newSelectedComponent, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_SelectedComponent(), newSelectedComponent, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelectedComponent(SelectedComponent newSelectedComponent) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_SelectedComponent(), newSelectedComponent);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SelectiveAcceptStatement getSelectiveAcceptStatement() {
		return (SelectiveAcceptStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_SelectiveAcceptStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSelectiveAcceptStatement(SelectiveAcceptStatement newSelectiveAcceptStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_SelectiveAcceptStatement(), newSelectiveAcceptStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelectiveAcceptStatement(SelectiveAcceptStatement newSelectiveAcceptStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_SelectiveAcceptStatement(), newSelectiveAcceptStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SharedPassivePragma getSharedPassivePragma() {
		return (SharedPassivePragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_SharedPassivePragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSharedPassivePragma(SharedPassivePragma newSharedPassivePragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_SharedPassivePragma(), newSharedPassivePragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSharedPassivePragma(SharedPassivePragma newSharedPassivePragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_SharedPassivePragma(), newSharedPassivePragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignedIntegerTypeDefinition getSignedIntegerTypeDefinition() {
		return (SignedIntegerTypeDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_SignedIntegerTypeDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSignedIntegerTypeDefinition(SignedIntegerTypeDefinition newSignedIntegerTypeDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_SignedIntegerTypeDefinition(), newSignedIntegerTypeDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSignedIntegerTypeDefinition(SignedIntegerTypeDefinition newSignedIntegerTypeDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_SignedIntegerTypeDefinition(), newSignedIntegerTypeDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignedZerosAttribute getSignedZerosAttribute() {
		return (SignedZerosAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_SignedZerosAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSignedZerosAttribute(SignedZerosAttribute newSignedZerosAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_SignedZerosAttribute(), newSignedZerosAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSignedZerosAttribute(SignedZerosAttribute newSignedZerosAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_SignedZerosAttribute(), newSignedZerosAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleExpressionRange getSimpleExpressionRange() {
		return (SimpleExpressionRange)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_SimpleExpressionRange(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSimpleExpressionRange(SimpleExpressionRange newSimpleExpressionRange, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_SimpleExpressionRange(), newSimpleExpressionRange, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSimpleExpressionRange(SimpleExpressionRange newSimpleExpressionRange) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_SimpleExpressionRange(), newSimpleExpressionRange);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SingleProtectedDeclaration getSingleProtectedDeclaration() {
		return (SingleProtectedDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_SingleProtectedDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSingleProtectedDeclaration(SingleProtectedDeclaration newSingleProtectedDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_SingleProtectedDeclaration(), newSingleProtectedDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSingleProtectedDeclaration(SingleProtectedDeclaration newSingleProtectedDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_SingleProtectedDeclaration(), newSingleProtectedDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SingleTaskDeclaration getSingleTaskDeclaration() {
		return (SingleTaskDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_SingleTaskDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSingleTaskDeclaration(SingleTaskDeclaration newSingleTaskDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_SingleTaskDeclaration(), newSingleTaskDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSingleTaskDeclaration(SingleTaskDeclaration newSingleTaskDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_SingleTaskDeclaration(), newSingleTaskDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SizeAttribute getSizeAttribute() {
		return (SizeAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_SizeAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSizeAttribute(SizeAttribute newSizeAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_SizeAttribute(), newSizeAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSizeAttribute(SizeAttribute newSizeAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_SizeAttribute(), newSizeAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Slice getSlice() {
		return (Slice)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_Slice(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSlice(Slice newSlice, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_Slice(), newSlice, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSlice(Slice newSlice) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_Slice(), newSlice);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SmallAttribute getSmallAttribute() {
		return (SmallAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_SmallAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSmallAttribute(SmallAttribute newSmallAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_SmallAttribute(), newSmallAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSmallAttribute(SmallAttribute newSmallAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_SmallAttribute(), newSmallAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StoragePoolAttribute getStoragePoolAttribute() {
		return (StoragePoolAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_StoragePoolAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStoragePoolAttribute(StoragePoolAttribute newStoragePoolAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_StoragePoolAttribute(), newStoragePoolAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStoragePoolAttribute(StoragePoolAttribute newStoragePoolAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_StoragePoolAttribute(), newStoragePoolAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StorageSizeAttribute getStorageSizeAttribute() {
		return (StorageSizeAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_StorageSizeAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStorageSizeAttribute(StorageSizeAttribute newStorageSizeAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_StorageSizeAttribute(), newStorageSizeAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStorageSizeAttribute(StorageSizeAttribute newStorageSizeAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_StorageSizeAttribute(), newStorageSizeAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StorageSizePragma getStorageSizePragma() {
		return (StorageSizePragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_StorageSizePragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStorageSizePragma(StorageSizePragma newStorageSizePragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_StorageSizePragma(), newStorageSizePragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStorageSizePragma(StorageSizePragma newStorageSizePragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_StorageSizePragma(), newStorageSizePragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StreamSizeAttribute getStreamSizeAttribute() {
		return (StreamSizeAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_StreamSizeAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStreamSizeAttribute(StreamSizeAttribute newStreamSizeAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_StreamSizeAttribute(), newStreamSizeAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStreamSizeAttribute(StreamSizeAttribute newStreamSizeAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_StreamSizeAttribute(), newStreamSizeAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StringLiteral getStringLiteral() {
		return (StringLiteral)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_StringLiteral(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStringLiteral(StringLiteral newStringLiteral, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_StringLiteral(), newStringLiteral, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStringLiteral(StringLiteral newStringLiteral) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_StringLiteral(), newStringLiteral);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubtypeDeclaration getSubtypeDeclaration() {
		return (SubtypeDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_SubtypeDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSubtypeDeclaration(SubtypeDeclaration newSubtypeDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_SubtypeDeclaration(), newSubtypeDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubtypeDeclaration(SubtypeDeclaration newSubtypeDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_SubtypeDeclaration(), newSubtypeDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubtypeIndication getSubtypeIndication() {
		return (SubtypeIndication)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_SubtypeIndication(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSubtypeIndication(SubtypeIndication newSubtypeIndication, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_SubtypeIndication(), newSubtypeIndication, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubtypeIndication(SubtypeIndication newSubtypeIndication) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_SubtypeIndication(), newSubtypeIndication);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SuccAttribute getSuccAttribute() {
		return (SuccAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_SuccAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSuccAttribute(SuccAttribute newSuccAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_SuccAttribute(), newSuccAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuccAttribute(SuccAttribute newSuccAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_SuccAttribute(), newSuccAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SuppressPragma getSuppressPragma() {
		return (SuppressPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_SuppressPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSuppressPragma(SuppressPragma newSuppressPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_SuppressPragma(), newSuppressPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuppressPragma(SuppressPragma newSuppressPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_SuppressPragma(), newSuppressPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Synchronized getSynchronized() {
		return (Synchronized)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_Synchronized(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSynchronized(Synchronized newSynchronized, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_Synchronized(), newSynchronized, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSynchronized(Synchronized newSynchronized) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_Synchronized(), newSynchronized);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SynchronizedInterface getSynchronizedInterface() {
		return (SynchronizedInterface)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_SynchronizedInterface(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSynchronizedInterface(SynchronizedInterface newSynchronizedInterface, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_SynchronizedInterface(), newSynchronizedInterface, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSynchronizedInterface(SynchronizedInterface newSynchronizedInterface) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_SynchronizedInterface(), newSynchronizedInterface);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TagAttribute getTagAttribute() {
		return (TagAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_TagAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTagAttribute(TagAttribute newTagAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_TagAttribute(), newTagAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTagAttribute(TagAttribute newTagAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_TagAttribute(), newTagAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Tagged getTagged() {
		return (Tagged)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_Tagged(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTagged(Tagged newTagged, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_Tagged(), newTagged, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTagged(Tagged newTagged) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_Tagged(), newTagged);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaggedIncompleteTypeDeclaration getTaggedIncompleteTypeDeclaration() {
		return (TaggedIncompleteTypeDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_TaggedIncompleteTypeDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTaggedIncompleteTypeDeclaration(TaggedIncompleteTypeDeclaration newTaggedIncompleteTypeDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_TaggedIncompleteTypeDeclaration(), newTaggedIncompleteTypeDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaggedIncompleteTypeDeclaration(TaggedIncompleteTypeDeclaration newTaggedIncompleteTypeDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_TaggedIncompleteTypeDeclaration(), newTaggedIncompleteTypeDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaggedPrivateTypeDefinition getTaggedPrivateTypeDefinition() {
		return (TaggedPrivateTypeDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_TaggedPrivateTypeDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTaggedPrivateTypeDefinition(TaggedPrivateTypeDefinition newTaggedPrivateTypeDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_TaggedPrivateTypeDefinition(), newTaggedPrivateTypeDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaggedPrivateTypeDefinition(TaggedPrivateTypeDefinition newTaggedPrivateTypeDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_TaggedPrivateTypeDefinition(), newTaggedPrivateTypeDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaggedRecordTypeDefinition getTaggedRecordTypeDefinition() {
		return (TaggedRecordTypeDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_TaggedRecordTypeDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTaggedRecordTypeDefinition(TaggedRecordTypeDefinition newTaggedRecordTypeDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_TaggedRecordTypeDefinition(), newTaggedRecordTypeDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaggedRecordTypeDefinition(TaggedRecordTypeDefinition newTaggedRecordTypeDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_TaggedRecordTypeDefinition(), newTaggedRecordTypeDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskBodyDeclaration getTaskBodyDeclaration() {
		return (TaskBodyDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_TaskBodyDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTaskBodyDeclaration(TaskBodyDeclaration newTaskBodyDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_TaskBodyDeclaration(), newTaskBodyDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaskBodyDeclaration(TaskBodyDeclaration newTaskBodyDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_TaskBodyDeclaration(), newTaskBodyDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskBodyStub getTaskBodyStub() {
		return (TaskBodyStub)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_TaskBodyStub(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTaskBodyStub(TaskBodyStub newTaskBodyStub, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_TaskBodyStub(), newTaskBodyStub, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaskBodyStub(TaskBodyStub newTaskBodyStub) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_TaskBodyStub(), newTaskBodyStub);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskDefinition getTaskDefinition() {
		return (TaskDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_TaskDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTaskDefinition(TaskDefinition newTaskDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_TaskDefinition(), newTaskDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaskDefinition(TaskDefinition newTaskDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_TaskDefinition(), newTaskDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskDispatchingPolicyPragma getTaskDispatchingPolicyPragma() {
		return (TaskDispatchingPolicyPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_TaskDispatchingPolicyPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTaskDispatchingPolicyPragma(TaskDispatchingPolicyPragma newTaskDispatchingPolicyPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_TaskDispatchingPolicyPragma(), newTaskDispatchingPolicyPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaskDispatchingPolicyPragma(TaskDispatchingPolicyPragma newTaskDispatchingPolicyPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_TaskDispatchingPolicyPragma(), newTaskDispatchingPolicyPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskInterface getTaskInterface() {
		return (TaskInterface)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_TaskInterface(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTaskInterface(TaskInterface newTaskInterface, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_TaskInterface(), newTaskInterface, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaskInterface(TaskInterface newTaskInterface) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_TaskInterface(), newTaskInterface);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskTypeDeclaration getTaskTypeDeclaration() {
		return (TaskTypeDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_TaskTypeDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTaskTypeDeclaration(TaskTypeDeclaration newTaskTypeDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_TaskTypeDeclaration(), newTaskTypeDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaskTypeDeclaration(TaskTypeDeclaration newTaskTypeDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_TaskTypeDeclaration(), newTaskTypeDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TerminateAlternativeStatement getTerminateAlternativeStatement() {
		return (TerminateAlternativeStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_TerminateAlternativeStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTerminateAlternativeStatement(TerminateAlternativeStatement newTerminateAlternativeStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_TerminateAlternativeStatement(), newTerminateAlternativeStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTerminateAlternativeStatement(TerminateAlternativeStatement newTerminateAlternativeStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_TerminateAlternativeStatement(), newTerminateAlternativeStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TerminatedAttribute getTerminatedAttribute() {
		return (TerminatedAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_TerminatedAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTerminatedAttribute(TerminatedAttribute newTerminatedAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_TerminatedAttribute(), newTerminatedAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTerminatedAttribute(TerminatedAttribute newTerminatedAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_TerminatedAttribute(), newTerminatedAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ThenAbortPath getThenAbortPath() {
		return (ThenAbortPath)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ThenAbortPath(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetThenAbortPath(ThenAbortPath newThenAbortPath, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ThenAbortPath(), newThenAbortPath, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setThenAbortPath(ThenAbortPath newThenAbortPath) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ThenAbortPath(), newThenAbortPath);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedEntryCallStatement getTimedEntryCallStatement() {
		return (TimedEntryCallStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_TimedEntryCallStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTimedEntryCallStatement(TimedEntryCallStatement newTimedEntryCallStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_TimedEntryCallStatement(), newTimedEntryCallStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimedEntryCallStatement(TimedEntryCallStatement newTimedEntryCallStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_TimedEntryCallStatement(), newTimedEntryCallStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TruncationAttribute getTruncationAttribute() {
		return (TruncationAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_TruncationAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTruncationAttribute(TruncationAttribute newTruncationAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_TruncationAttribute(), newTruncationAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTruncationAttribute(TruncationAttribute newTruncationAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_TruncationAttribute(), newTruncationAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeConversion getTypeConversion() {
		return (TypeConversion)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_TypeConversion(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTypeConversion(TypeConversion newTypeConversion, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_TypeConversion(), newTypeConversion, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypeConversion(TypeConversion newTypeConversion) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_TypeConversion(), newTypeConversion);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnaryMinusOperator getUnaryMinusOperator() {
		return (UnaryMinusOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_UnaryMinusOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnaryMinusOperator(UnaryMinusOperator newUnaryMinusOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_UnaryMinusOperator(), newUnaryMinusOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnaryMinusOperator(UnaryMinusOperator newUnaryMinusOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_UnaryMinusOperator(), newUnaryMinusOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnaryPlusOperator getUnaryPlusOperator() {
		return (UnaryPlusOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_UnaryPlusOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnaryPlusOperator(UnaryPlusOperator newUnaryPlusOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_UnaryPlusOperator(), newUnaryPlusOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnaryPlusOperator(UnaryPlusOperator newUnaryPlusOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_UnaryPlusOperator(), newUnaryPlusOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnbiasedRoundingAttribute getUnbiasedRoundingAttribute() {
		return (UnbiasedRoundingAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_UnbiasedRoundingAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnbiasedRoundingAttribute(UnbiasedRoundingAttribute newUnbiasedRoundingAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_UnbiasedRoundingAttribute(), newUnbiasedRoundingAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnbiasedRoundingAttribute(UnbiasedRoundingAttribute newUnbiasedRoundingAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_UnbiasedRoundingAttribute(), newUnbiasedRoundingAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UncheckedAccessAttribute getUncheckedAccessAttribute() {
		return (UncheckedAccessAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_UncheckedAccessAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUncheckedAccessAttribute(UncheckedAccessAttribute newUncheckedAccessAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_UncheckedAccessAttribute(), newUncheckedAccessAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUncheckedAccessAttribute(UncheckedAccessAttribute newUncheckedAccessAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_UncheckedAccessAttribute(), newUncheckedAccessAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UncheckedUnionPragma getUncheckedUnionPragma() {
		return (UncheckedUnionPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_UncheckedUnionPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUncheckedUnionPragma(UncheckedUnionPragma newUncheckedUnionPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_UncheckedUnionPragma(), newUncheckedUnionPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUncheckedUnionPragma(UncheckedUnionPragma newUncheckedUnionPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_UncheckedUnionPragma(), newUncheckedUnionPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnconstrainedArrayDefinition getUnconstrainedArrayDefinition() {
		return (UnconstrainedArrayDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_UnconstrainedArrayDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnconstrainedArrayDefinition(UnconstrainedArrayDefinition newUnconstrainedArrayDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_UnconstrainedArrayDefinition(), newUnconstrainedArrayDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnconstrainedArrayDefinition(UnconstrainedArrayDefinition newUnconstrainedArrayDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_UnconstrainedArrayDefinition(), newUnconstrainedArrayDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UniversalFixedDefinition getUniversalFixedDefinition() {
		return (UniversalFixedDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_UniversalFixedDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUniversalFixedDefinition(UniversalFixedDefinition newUniversalFixedDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_UniversalFixedDefinition(), newUniversalFixedDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUniversalFixedDefinition(UniversalFixedDefinition newUniversalFixedDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_UniversalFixedDefinition(), newUniversalFixedDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UniversalIntegerDefinition getUniversalIntegerDefinition() {
		return (UniversalIntegerDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_UniversalIntegerDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUniversalIntegerDefinition(UniversalIntegerDefinition newUniversalIntegerDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_UniversalIntegerDefinition(), newUniversalIntegerDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUniversalIntegerDefinition(UniversalIntegerDefinition newUniversalIntegerDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_UniversalIntegerDefinition(), newUniversalIntegerDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UniversalRealDefinition getUniversalRealDefinition() {
		return (UniversalRealDefinition)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_UniversalRealDefinition(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUniversalRealDefinition(UniversalRealDefinition newUniversalRealDefinition, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_UniversalRealDefinition(), newUniversalRealDefinition, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUniversalRealDefinition(UniversalRealDefinition newUniversalRealDefinition) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_UniversalRealDefinition(), newUniversalRealDefinition);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnknownAttribute getUnknownAttribute() {
		return (UnknownAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_UnknownAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnknownAttribute(UnknownAttribute newUnknownAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_UnknownAttribute(), newUnknownAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnknownAttribute(UnknownAttribute newUnknownAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_UnknownAttribute(), newUnknownAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnknownDiscriminantPart getUnknownDiscriminantPart() {
		return (UnknownDiscriminantPart)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_UnknownDiscriminantPart(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnknownDiscriminantPart(UnknownDiscriminantPart newUnknownDiscriminantPart, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_UnknownDiscriminantPart(), newUnknownDiscriminantPart, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnknownDiscriminantPart(UnknownDiscriminantPart newUnknownDiscriminantPart) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_UnknownDiscriminantPart(), newUnknownDiscriminantPart);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnknownPragma getUnknownPragma() {
		return (UnknownPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_UnknownPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnknownPragma(UnknownPragma newUnknownPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_UnknownPragma(), newUnknownPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnknownPragma(UnknownPragma newUnknownPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_UnknownPragma(), newUnknownPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnsuppressPragma getUnsuppressPragma() {
		return (UnsuppressPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_UnsuppressPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnsuppressPragma(UnsuppressPragma newUnsuppressPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_UnsuppressPragma(), newUnsuppressPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnsuppressPragma(UnsuppressPragma newUnsuppressPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_UnsuppressPragma(), newUnsuppressPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseAllTypeClause getUseAllTypeClause() {
		return (UseAllTypeClause)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_UseAllTypeClause(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUseAllTypeClause(UseAllTypeClause newUseAllTypeClause, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_UseAllTypeClause(), newUseAllTypeClause, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUseAllTypeClause(UseAllTypeClause newUseAllTypeClause) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_UseAllTypeClause(), newUseAllTypeClause);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UsePackageClause getUsePackageClause() {
		return (UsePackageClause)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_UsePackageClause(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUsePackageClause(UsePackageClause newUsePackageClause, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_UsePackageClause(), newUsePackageClause, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUsePackageClause(UsePackageClause newUsePackageClause) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_UsePackageClause(), newUsePackageClause);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseTypeClause getUseTypeClause() {
		return (UseTypeClause)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_UseTypeClause(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUseTypeClause(UseTypeClause newUseTypeClause, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_UseTypeClause(), newUseTypeClause, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUseTypeClause(UseTypeClause newUseTypeClause) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_UseTypeClause(), newUseTypeClause);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValAttribute getValAttribute() {
		return (ValAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ValAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetValAttribute(ValAttribute newValAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ValAttribute(), newValAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValAttribute(ValAttribute newValAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ValAttribute(), newValAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValidAttribute getValidAttribute() {
		return (ValidAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ValidAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetValidAttribute(ValidAttribute newValidAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ValidAttribute(), newValidAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValidAttribute(ValidAttribute newValidAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ValidAttribute(), newValidAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueAttribute getValueAttribute() {
		return (ValueAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_ValueAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetValueAttribute(ValueAttribute newValueAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_ValueAttribute(), newValueAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueAttribute(ValueAttribute newValueAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_ValueAttribute(), newValueAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariableDeclaration getVariableDeclaration() {
		return (VariableDeclaration)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_VariableDeclaration(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVariableDeclaration(VariableDeclaration newVariableDeclaration, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_VariableDeclaration(), newVariableDeclaration, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVariableDeclaration(VariableDeclaration newVariableDeclaration) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_VariableDeclaration(), newVariableDeclaration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variant getVariant() {
		return (Variant)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_Variant(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVariant(Variant newVariant, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_Variant(), newVariant, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVariant(Variant newVariant) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_Variant(), newVariant);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariantPart getVariantPart() {
		return (VariantPart)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_VariantPart(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVariantPart(VariantPart newVariantPart, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_VariantPart(), newVariantPart, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVariantPart(VariantPart newVariantPart) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_VariantPart(), newVariantPart);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VersionAttribute getVersionAttribute() {
		return (VersionAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_VersionAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVersionAttribute(VersionAttribute newVersionAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_VersionAttribute(), newVersionAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersionAttribute(VersionAttribute newVersionAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_VersionAttribute(), newVersionAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VolatileComponentsPragma getVolatileComponentsPragma() {
		return (VolatileComponentsPragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_VolatileComponentsPragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVolatileComponentsPragma(VolatileComponentsPragma newVolatileComponentsPragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_VolatileComponentsPragma(), newVolatileComponentsPragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVolatileComponentsPragma(VolatileComponentsPragma newVolatileComponentsPragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_VolatileComponentsPragma(), newVolatileComponentsPragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VolatilePragma getVolatilePragma() {
		return (VolatilePragma)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_VolatilePragma(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVolatilePragma(VolatilePragma newVolatilePragma, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_VolatilePragma(), newVolatilePragma, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVolatilePragma(VolatilePragma newVolatilePragma) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_VolatilePragma(), newVolatilePragma);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WhileLoopStatement getWhileLoopStatement() {
		return (WhileLoopStatement)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_WhileLoopStatement(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWhileLoopStatement(WhileLoopStatement newWhileLoopStatement, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_WhileLoopStatement(), newWhileLoopStatement, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWhileLoopStatement(WhileLoopStatement newWhileLoopStatement) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_WhileLoopStatement(), newWhileLoopStatement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WideImageAttribute getWideImageAttribute() {
		return (WideImageAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_WideImageAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWideImageAttribute(WideImageAttribute newWideImageAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_WideImageAttribute(), newWideImageAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWideImageAttribute(WideImageAttribute newWideImageAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_WideImageAttribute(), newWideImageAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WideValueAttribute getWideValueAttribute() {
		return (WideValueAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_WideValueAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWideValueAttribute(WideValueAttribute newWideValueAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_WideValueAttribute(), newWideValueAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWideValueAttribute(WideValueAttribute newWideValueAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_WideValueAttribute(), newWideValueAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WideWideImageAttribute getWideWideImageAttribute() {
		return (WideWideImageAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_WideWideImageAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWideWideImageAttribute(WideWideImageAttribute newWideWideImageAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_WideWideImageAttribute(), newWideWideImageAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWideWideImageAttribute(WideWideImageAttribute newWideWideImageAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_WideWideImageAttribute(), newWideWideImageAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WideWideValueAttribute getWideWideValueAttribute() {
		return (WideWideValueAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_WideWideValueAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWideWideValueAttribute(WideWideValueAttribute newWideWideValueAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_WideWideValueAttribute(), newWideWideValueAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWideWideValueAttribute(WideWideValueAttribute newWideWideValueAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_WideWideValueAttribute(), newWideWideValueAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WideWideWidthAttribute getWideWideWidthAttribute() {
		return (WideWideWidthAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_WideWideWidthAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWideWideWidthAttribute(WideWideWidthAttribute newWideWideWidthAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_WideWideWidthAttribute(), newWideWideWidthAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWideWideWidthAttribute(WideWideWidthAttribute newWideWideWidthAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_WideWideWidthAttribute(), newWideWideWidthAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WideWidthAttribute getWideWidthAttribute() {
		return (WideWidthAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_WideWidthAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWideWidthAttribute(WideWidthAttribute newWideWidthAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_WideWidthAttribute(), newWideWidthAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWideWidthAttribute(WideWidthAttribute newWideWidthAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_WideWidthAttribute(), newWideWidthAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WidthAttribute getWidthAttribute() {
		return (WidthAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_WidthAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWidthAttribute(WidthAttribute newWidthAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_WidthAttribute(), newWidthAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWidthAttribute(WidthAttribute newWidthAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_WidthAttribute(), newWidthAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WithClause getWithClause() {
		return (WithClause)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_WithClause(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWithClause(WithClause newWithClause, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_WithClause(), newWithClause, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWithClause(WithClause newWithClause) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_WithClause(), newWithClause);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WriteAttribute getWriteAttribute() {
		return (WriteAttribute)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_WriteAttribute(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWriteAttribute(WriteAttribute newWriteAttribute, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_WriteAttribute(), newWriteAttribute, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWriteAttribute(WriteAttribute newWriteAttribute) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_WriteAttribute(), newWriteAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XorOperator getXorOperator() {
		return (XorOperator)getMixed().get(AdaPackage.eINSTANCE.getDocumentRoot_XorOperator(), true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetXorOperator(XorOperator newXorOperator, NotificationChain msgs) {
		return ((FeatureMap.Internal)getMixed()).basicAdd(AdaPackage.eINSTANCE.getDocumentRoot_XorOperator(), newXorOperator, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setXorOperator(XorOperator newXorOperator) {
		((FeatureMap.Internal)getMixed()).set(AdaPackage.eINSTANCE.getDocumentRoot_XorOperator(), newXorOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.DOCUMENT_ROOT__MIXED:
				return ((InternalEList<?>)getMixed()).basicRemove(otherEnd, msgs);
			case AdaPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				return ((InternalEList<?>)getXMLNSPrefixMap()).basicRemove(otherEnd, msgs);
			case AdaPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				return ((InternalEList<?>)getXSISchemaLocation()).basicRemove(otherEnd, msgs);
			case AdaPackage.DOCUMENT_ROOT__ABORT_STATEMENT:
				return basicSetAbortStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ABS_OPERATOR:
				return basicSetAbsOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ABSTRACT:
				return basicSetAbstract(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ACCEPT_STATEMENT:
				return basicSetAcceptStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ACCESS_ATTRIBUTE:
				return basicSetAccessAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_CONSTANT:
				return basicSetAccessToConstant(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_FUNCTION:
				return basicSetAccessToFunction(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_PROCEDURE:
				return basicSetAccessToProcedure(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_PROTECTED_FUNCTION:
				return basicSetAccessToProtectedFunction(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_PROTECTED_PROCEDURE:
				return basicSetAccessToProtectedProcedure(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_VARIABLE:
				return basicSetAccessToVariable(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ADDRESS_ATTRIBUTE:
				return basicSetAddressAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ADJACENT_ATTRIBUTE:
				return basicSetAdjacentAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__AFT_ATTRIBUTE:
				return basicSetAftAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ALIASED:
				return basicSetAliased(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ALIGNMENT_ATTRIBUTE:
				return basicSetAlignmentAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ALL_CALLS_REMOTE_PRAGMA:
				return basicSetAllCallsRemotePragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ALLOCATION_FROM_QUALIFIED_EXPRESSION:
				return basicSetAllocationFromQualifiedExpression(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ALLOCATION_FROM_SUBTYPE:
				return basicSetAllocationFromSubtype(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__AND_OPERATOR:
				return basicSetAndOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__AND_THEN_SHORT_CIRCUIT:
				return basicSetAndThenShortCircuit(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_CONSTANT:
				return basicSetAnonymousAccessToConstant(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_FUNCTION:
				return basicSetAnonymousAccessToFunction(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_PROCEDURE:
				return basicSetAnonymousAccessToProcedure(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION:
				return basicSetAnonymousAccessToProtectedFunction(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE:
				return basicSetAnonymousAccessToProtectedProcedure(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_VARIABLE:
				return basicSetAnonymousAccessToVariable(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ARRAY_COMPONENT_ASSOCIATION:
				return basicSetArrayComponentAssociation(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ASPECT_SPECIFICATION:
				return basicSetAspectSpecification(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ASSERT_PRAGMA:
				return basicSetAssertPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ASSERTION_POLICY_PRAGMA:
				return basicSetAssertionPolicyPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ASSIGNMENT_STATEMENT:
				return basicSetAssignmentStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ASYNCHRONOUS_PRAGMA:
				return basicSetAsynchronousPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ASYNCHRONOUS_SELECT_STATEMENT:
				return basicSetAsynchronousSelectStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__AT_CLAUSE:
				return basicSetAtClause(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ATOMIC_COMPONENTS_PRAGMA:
				return basicSetAtomicComponentsPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ATOMIC_PRAGMA:
				return basicSetAtomicPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ATTACH_HANDLER_PRAGMA:
				return basicSetAttachHandlerPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ATTRIBUTE_DEFINITION_CLAUSE:
				return basicSetAttributeDefinitionClause(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__BASE_ATTRIBUTE:
				return basicSetBaseAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__BIT_ORDER_ATTRIBUTE:
				return basicSetBitOrderAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__BLOCK_STATEMENT:
				return basicSetBlockStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__BODY_VERSION_ATTRIBUTE:
				return basicSetBodyVersionAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__BOX_EXPRESSION:
				return basicSetBoxExpression(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__CALLABLE_ATTRIBUTE:
				return basicSetCallableAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__CALLER_ATTRIBUTE:
				return basicSetCallerAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__CASE_EXPRESSION:
				return basicSetCaseExpression(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__CASE_EXPRESSION_PATH:
				return basicSetCaseExpressionPath(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__CASE_PATH:
				return basicSetCasePath(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__CASE_STATEMENT:
				return basicSetCaseStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__CEILING_ATTRIBUTE:
				return basicSetCeilingAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__CHARACTER_LITERAL:
				return basicSetCharacterLiteral(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__CHOICE_PARAMETER_SPECIFICATION:
				return basicSetChoiceParameterSpecification(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__CLASS_ATTRIBUTE:
				return basicSetClassAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__CODE_STATEMENT:
				return basicSetCodeStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__COMMENT:
				return basicSetComment(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__COMPILATION_UNIT:
				return basicSetCompilationUnit(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__COMPONENT_CLAUSE:
				return basicSetComponentClause(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__COMPONENT_DECLARATION:
				return basicSetComponentDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__COMPONENT_DEFINITION:
				return basicSetComponentDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__COMPONENT_SIZE_ATTRIBUTE:
				return basicSetComponentSizeAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__COMPOSE_ATTRIBUTE:
				return basicSetComposeAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__CONCATENATE_OPERATOR:
				return basicSetConcatenateOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__CONDITIONAL_ENTRY_CALL_STATEMENT:
				return basicSetConditionalEntryCallStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__CONSTANT_DECLARATION:
				return basicSetConstantDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__CONSTRAINED_ARRAY_DEFINITION:
				return basicSetConstrainedArrayDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__CONSTRAINED_ATTRIBUTE:
				return basicSetConstrainedAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__CONTROLLED_PRAGMA:
				return basicSetControlledPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__CONVENTION_PRAGMA:
				return basicSetConventionPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__COPY_SIGN_ATTRIBUTE:
				return basicSetCopySignAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__COUNT_ATTRIBUTE:
				return basicSetCountAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__CPU_PRAGMA:
				return basicSetCpuPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DECIMAL_FIXED_POINT_DEFINITION:
				return basicSetDecimalFixedPointDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFAULT_STORAGE_POOL_PRAGMA:
				return basicSetDefaultStoragePoolPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFERRED_CONSTANT_DECLARATION:
				return basicSetDeferredConstantDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINING_ABS_OPERATOR:
				return basicSetDefiningAbsOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINING_AND_OPERATOR:
				return basicSetDefiningAndOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINING_CHARACTER_LITERAL:
				return basicSetDefiningCharacterLiteral(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINING_CONCATENATE_OPERATOR:
				return basicSetDefiningConcatenateOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINING_DIVIDE_OPERATOR:
				return basicSetDefiningDivideOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINING_ENUMERATION_LITERAL:
				return basicSetDefiningEnumerationLiteral(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINING_EQUAL_OPERATOR:
				return basicSetDefiningEqualOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINING_EXPANDED_NAME:
				return basicSetDefiningExpandedName(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINING_EXPONENTIATE_OPERATOR:
				return basicSetDefiningExponentiateOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINING_GREATER_THAN_OPERATOR:
				return basicSetDefiningGreaterThanOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINING_GREATER_THAN_OR_EQUAL_OPERATOR:
				return basicSetDefiningGreaterThanOrEqualOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINING_IDENTIFIER:
				return basicSetDefiningIdentifier(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINING_LESS_THAN_OPERATOR:
				return basicSetDefiningLessThanOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINING_LESS_THAN_OR_EQUAL_OPERATOR:
				return basicSetDefiningLessThanOrEqualOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINING_MINUS_OPERATOR:
				return basicSetDefiningMinusOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINING_MOD_OPERATOR:
				return basicSetDefiningModOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINING_MULTIPLY_OPERATOR:
				return basicSetDefiningMultiplyOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINING_NOT_EQUAL_OPERATOR:
				return basicSetDefiningNotEqualOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINING_NOT_OPERATOR:
				return basicSetDefiningNotOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINING_OR_OPERATOR:
				return basicSetDefiningOrOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINING_PLUS_OPERATOR:
				return basicSetDefiningPlusOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINING_REM_OPERATOR:
				return basicSetDefiningRemOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINING_UNARY_MINUS_OPERATOR:
				return basicSetDefiningUnaryMinusOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINING_UNARY_PLUS_OPERATOR:
				return basicSetDefiningUnaryPlusOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINING_XOR_OPERATOR:
				return basicSetDefiningXorOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DEFINITE_ATTRIBUTE:
				return basicSetDefiniteAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DELAY_RELATIVE_STATEMENT:
				return basicSetDelayRelativeStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DELAY_UNTIL_STATEMENT:
				return basicSetDelayUntilStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DELTA_ATTRIBUTE:
				return basicSetDeltaAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DELTA_CONSTRAINT:
				return basicSetDeltaConstraint(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DENORM_ATTRIBUTE:
				return basicSetDenormAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DERIVED_RECORD_EXTENSION_DEFINITION:
				return basicSetDerivedRecordExtensionDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DERIVED_TYPE_DEFINITION:
				return basicSetDerivedTypeDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DETECT_BLOCKING_PRAGMA:
				return basicSetDetectBlockingPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DIGITS_ATTRIBUTE:
				return basicSetDigitsAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DIGITS_CONSTRAINT:
				return basicSetDigitsConstraint(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DISCARD_NAMES_PRAGMA:
				return basicSetDiscardNamesPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_RANGE_ATTRIBUTE_REFERENCE:
				return basicSetDiscreteRangeAttributeReference(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION:
				return basicSetDiscreteRangeAttributeReferenceAsSubtypeDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_SIMPLE_EXPRESSION_RANGE:
				return basicSetDiscreteSimpleExpressionRange(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION:
				return basicSetDiscreteSimpleExpressionRangeAsSubtypeDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_SUBTYPE_INDICATION:
				return basicSetDiscreteSubtypeIndication(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION:
				return basicSetDiscreteSubtypeIndicationAsSubtypeDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DISCRIMINANT_ASSOCIATION:
				return basicSetDiscriminantAssociation(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DISCRIMINANT_CONSTRAINT:
				return basicSetDiscriminantConstraint(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DISCRIMINANT_SPECIFICATION:
				return basicSetDiscriminantSpecification(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DISPATCHING_DOMAIN_PRAGMA:
				return basicSetDispatchingDomainPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__DIVIDE_OPERATOR:
				return basicSetDivideOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ELABORATE_ALL_PRAGMA:
				return basicSetElaborateAllPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ELABORATE_BODY_PRAGMA:
				return basicSetElaborateBodyPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ELABORATE_PRAGMA:
				return basicSetElaboratePragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ELEMENT_ITERATOR_SPECIFICATION:
				return basicSetElementIteratorSpecification(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ELSE_EXPRESSION_PATH:
				return basicSetElseExpressionPath(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ELSE_PATH:
				return basicSetElsePath(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ELSIF_EXPRESSION_PATH:
				return basicSetElsifExpressionPath(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ELSIF_PATH:
				return basicSetElsifPath(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ENTRY_BODY_DECLARATION:
				return basicSetEntryBodyDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ENTRY_CALL_STATEMENT:
				return basicSetEntryCallStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ENTRY_DECLARATION:
				return basicSetEntryDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ENTRY_INDEX_SPECIFICATION:
				return basicSetEntryIndexSpecification(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ENUMERATION_LITERAL:
				return basicSetEnumerationLiteral(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ENUMERATION_LITERAL_SPECIFICATION:
				return basicSetEnumerationLiteralSpecification(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ENUMERATION_REPRESENTATION_CLAUSE:
				return basicSetEnumerationRepresentationClause(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ENUMERATION_TYPE_DEFINITION:
				return basicSetEnumerationTypeDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__EQUAL_OPERATOR:
				return basicSetEqualOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__EXCEPTION_DECLARATION:
				return basicSetExceptionDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__EXCEPTION_HANDLER:
				return basicSetExceptionHandler(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__EXCEPTION_RENAMING_DECLARATION:
				return basicSetExceptionRenamingDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__EXIT_STATEMENT:
				return basicSetExitStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__EXPLICIT_DEREFERENCE:
				return basicSetExplicitDereference(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__EXPONENT_ATTRIBUTE:
				return basicSetExponentAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__EXPONENTIATE_OPERATOR:
				return basicSetExponentiateOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__EXPORT_PRAGMA:
				return basicSetExportPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__EXPRESSION_FUNCTION_DECLARATION:
				return basicSetExpressionFunctionDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__EXTENDED_RETURN_STATEMENT:
				return basicSetExtendedReturnStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__EXTENSION_AGGREGATE:
				return basicSetExtensionAggregate(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__EXTERNAL_TAG_ATTRIBUTE:
				return basicSetExternalTagAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FIRST_ATTRIBUTE:
				return basicSetFirstAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FIRST_BIT_ATTRIBUTE:
				return basicSetFirstBitAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FLOATING_POINT_DEFINITION:
				return basicSetFloatingPointDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FLOOR_ATTRIBUTE:
				return basicSetFloorAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FOR_ALL_QUANTIFIED_EXPRESSION:
				return basicSetForAllQuantifiedExpression(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FOR_LOOP_STATEMENT:
				return basicSetForLoopStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FOR_SOME_QUANTIFIED_EXPRESSION:
				return basicSetForSomeQuantifiedExpression(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORE_ATTRIBUTE:
				return basicSetForeAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_CONSTANT:
				return basicSetFormalAccessToConstant(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_FUNCTION:
				return basicSetFormalAccessToFunction(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_PROCEDURE:
				return basicSetFormalAccessToProcedure(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_PROTECTED_FUNCTION:
				return basicSetFormalAccessToProtectedFunction(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_PROTECTED_PROCEDURE:
				return basicSetFormalAccessToProtectedProcedure(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_VARIABLE:
				return basicSetFormalAccessToVariable(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_CONSTRAINED_ARRAY_DEFINITION:
				return basicSetFormalConstrainedArrayDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_DECIMAL_FIXED_POINT_DEFINITION:
				return basicSetFormalDecimalFixedPointDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_DERIVED_TYPE_DEFINITION:
				return basicSetFormalDerivedTypeDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_DISCRETE_TYPE_DEFINITION:
				return basicSetFormalDiscreteTypeDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_FLOATING_POINT_DEFINITION:
				return basicSetFormalFloatingPointDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_FUNCTION_DECLARATION:
				return basicSetFormalFunctionDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_INCOMPLETE_TYPE_DECLARATION:
				return basicSetFormalIncompleteTypeDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_LIMITED_INTERFACE:
				return basicSetFormalLimitedInterface(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_MODULAR_TYPE_DEFINITION:
				return basicSetFormalModularTypeDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_OBJECT_DECLARATION:
				return basicSetFormalObjectDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ORDINARY_FIXED_POINT_DEFINITION:
				return basicSetFormalOrdinaryFixedPointDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ORDINARY_INTERFACE:
				return basicSetFormalOrdinaryInterface(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_PACKAGE_DECLARATION:
				return basicSetFormalPackageDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_PACKAGE_DECLARATION_WITH_BOX:
				return basicSetFormalPackageDeclarationWithBox(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				return basicSetFormalPoolSpecificAccessToVariable(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_PRIVATE_TYPE_DEFINITION:
				return basicSetFormalPrivateTypeDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_PROCEDURE_DECLARATION:
				return basicSetFormalProcedureDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_PROTECTED_INTERFACE:
				return basicSetFormalProtectedInterface(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_SIGNED_INTEGER_TYPE_DEFINITION:
				return basicSetFormalSignedIntegerTypeDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_SYNCHRONIZED_INTERFACE:
				return basicSetFormalSynchronizedInterface(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION:
				return basicSetFormalTaggedPrivateTypeDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_TASK_INTERFACE:
				return basicSetFormalTaskInterface(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_TYPE_DECLARATION:
				return basicSetFormalTypeDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FORMAL_UNCONSTRAINED_ARRAY_DEFINITION:
				return basicSetFormalUnconstrainedArrayDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FRACTION_ATTRIBUTE:
				return basicSetFractionAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_BODY_DECLARATION:
				return basicSetFunctionBodyDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_BODY_STUB:
				return basicSetFunctionBodyStub(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_CALL:
				return basicSetFunctionCall(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_DECLARATION:
				return basicSetFunctionDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_INSTANTIATION:
				return basicSetFunctionInstantiation(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_RENAMING_DECLARATION:
				return basicSetFunctionRenamingDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__GENERALIZED_ITERATOR_SPECIFICATION:
				return basicSetGeneralizedIteratorSpecification(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__GENERIC_ASSOCIATION:
				return basicSetGenericAssociation(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__GENERIC_FUNCTION_DECLARATION:
				return basicSetGenericFunctionDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__GENERIC_FUNCTION_RENAMING_DECLARATION:
				return basicSetGenericFunctionRenamingDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__GENERIC_PACKAGE_DECLARATION:
				return basicSetGenericPackageDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__GENERIC_PACKAGE_RENAMING_DECLARATION:
				return basicSetGenericPackageRenamingDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__GENERIC_PROCEDURE_DECLARATION:
				return basicSetGenericProcedureDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__GENERIC_PROCEDURE_RENAMING_DECLARATION:
				return basicSetGenericProcedureRenamingDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__GOTO_STATEMENT:
				return basicSetGotoStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__GREATER_THAN_OPERATOR:
				return basicSetGreaterThanOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__GREATER_THAN_OR_EQUAL_OPERATOR:
				return basicSetGreaterThanOrEqualOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__IDENTIFIER:
				return basicSetIdentifier(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__IDENTITY_ATTRIBUTE:
				return basicSetIdentityAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__IF_EXPRESSION:
				return basicSetIfExpression(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__IF_EXPRESSION_PATH:
				return basicSetIfExpressionPath(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__IF_PATH:
				return basicSetIfPath(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__IF_STATEMENT:
				return basicSetIfStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__IMAGE_ATTRIBUTE:
				return basicSetImageAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__IMPLEMENTATION_DEFINED_ATTRIBUTE:
				return basicSetImplementationDefinedAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__IMPLEMENTATION_DEFINED_PRAGMA:
				return basicSetImplementationDefinedPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__IMPORT_PRAGMA:
				return basicSetImportPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__IN_MEMBERSHIP_TEST:
				return basicSetInMembershipTest(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__INCOMPLETE_TYPE_DECLARATION:
				return basicSetIncompleteTypeDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__INDEPENDENT_COMPONENTS_PRAGMA:
				return basicSetIndependentComponentsPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__INDEPENDENT_PRAGMA:
				return basicSetIndependentPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__INDEX_CONSTRAINT:
				return basicSetIndexConstraint(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__INDEXED_COMPONENT:
				return basicSetIndexedComponent(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__INLINE_PRAGMA:
				return basicSetInlinePragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__INPUT_ATTRIBUTE:
				return basicSetInputAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__INSPECTION_POINT_PRAGMA:
				return basicSetInspectionPointPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__INTEGER_LITERAL:
				return basicSetIntegerLiteral(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__INTEGER_NUMBER_DECLARATION:
				return basicSetIntegerNumberDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__INTERRUPT_HANDLER_PRAGMA:
				return basicSetInterruptHandlerPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__INTERRUPT_PRIORITY_PRAGMA:
				return basicSetInterruptPriorityPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__IS_PREFIX_CALL:
				return basicSetIsPrefixCall(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__IS_PREFIX_NOTATION:
				return basicSetIsPrefixNotation(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__KNOWN_DISCRIMINANT_PART:
				return basicSetKnownDiscriminantPart(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__LAST_ATTRIBUTE:
				return basicSetLastAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__LAST_BIT_ATTRIBUTE:
				return basicSetLastBitAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__LEADING_PART_ATTRIBUTE:
				return basicSetLeadingPartAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__LENGTH_ATTRIBUTE:
				return basicSetLengthAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__LESS_THAN_OPERATOR:
				return basicSetLessThanOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__LESS_THAN_OR_EQUAL_OPERATOR:
				return basicSetLessThanOrEqualOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__LIMITED:
				return basicSetLimited(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__LIMITED_INTERFACE:
				return basicSetLimitedInterface(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__LINKER_OPTIONS_PRAGMA:
				return basicSetLinkerOptionsPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__LIST_PRAGMA:
				return basicSetListPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__LOCKING_POLICY_PRAGMA:
				return basicSetLockingPolicyPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__LOOP_PARAMETER_SPECIFICATION:
				return basicSetLoopParameterSpecification(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__LOOP_STATEMENT:
				return basicSetLoopStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__MACHINE_ATTRIBUTE:
				return basicSetMachineAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__MACHINE_EMAX_ATTRIBUTE:
				return basicSetMachineEmaxAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__MACHINE_EMIN_ATTRIBUTE:
				return basicSetMachineEminAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__MACHINE_MANTISSA_ATTRIBUTE:
				return basicSetMachineMantissaAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__MACHINE_OVERFLOWS_ATTRIBUTE:
				return basicSetMachineOverflowsAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__MACHINE_RADIX_ATTRIBUTE:
				return basicSetMachineRadixAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__MACHINE_ROUNDING_ATTRIBUTE:
				return basicSetMachineRoundingAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__MACHINE_ROUNDS_ATTRIBUTE:
				return basicSetMachineRoundsAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE:
				return basicSetMaxAlignmentForAllocationAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__MAX_ATTRIBUTE:
				return basicSetMaxAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE:
				return basicSetMaxSizeInStorageElementsAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__MIN_ATTRIBUTE:
				return basicSetMinAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__MINUS_OPERATOR:
				return basicSetMinusOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__MOD_ATTRIBUTE:
				return basicSetModAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__MOD_OPERATOR:
				return basicSetModOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__MODEL_ATTRIBUTE:
				return basicSetModelAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__MODEL_EMIN_ATTRIBUTE:
				return basicSetModelEminAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__MODEL_EPSILON_ATTRIBUTE:
				return basicSetModelEpsilonAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__MODEL_MANTISSA_ATTRIBUTE:
				return basicSetModelMantissaAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__MODEL_SMALL_ATTRIBUTE:
				return basicSetModelSmallAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__MODULAR_TYPE_DEFINITION:
				return basicSetModularTypeDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__MODULUS_ATTRIBUTE:
				return basicSetModulusAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__MULTIPLY_OPERATOR:
				return basicSetMultiplyOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__NAMED_ARRAY_AGGREGATE:
				return basicSetNamedArrayAggregate(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__NO_RETURN_PRAGMA:
				return basicSetNoReturnPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__NORMALIZE_SCALARS_PRAGMA:
				return basicSetNormalizeScalarsPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__NOT_AN_ELEMENT:
				return basicSetNotAnElement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__NOT_EQUAL_OPERATOR:
				return basicSetNotEqualOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__NOT_IN_MEMBERSHIP_TEST:
				return basicSetNotInMembershipTest(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__NOT_NULL_RETURN:
				return basicSetNotNullReturn(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__NOT_OPERATOR:
				return basicSetNotOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__NOT_OVERRIDING:
				return basicSetNotOverriding(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__NULL_COMPONENT:
				return basicSetNullComponent(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__NULL_EXCLUSION:
				return basicSetNullExclusion(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__NULL_LITERAL:
				return basicSetNullLiteral(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__NULL_PROCEDURE_DECLARATION:
				return basicSetNullProcedureDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__NULL_RECORD_DEFINITION:
				return basicSetNullRecordDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__NULL_STATEMENT:
				return basicSetNullStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__OBJECT_RENAMING_DECLARATION:
				return basicSetObjectRenamingDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__OPTIMIZE_PRAGMA:
				return basicSetOptimizePragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__OR_ELSE_SHORT_CIRCUIT:
				return basicSetOrElseShortCircuit(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__OR_OPERATOR:
				return basicSetOrOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__OR_PATH:
				return basicSetOrPath(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ORDINARY_FIXED_POINT_DEFINITION:
				return basicSetOrdinaryFixedPointDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ORDINARY_INTERFACE:
				return basicSetOrdinaryInterface(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ORDINARY_TYPE_DECLARATION:
				return basicSetOrdinaryTypeDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__OTHERS_CHOICE:
				return basicSetOthersChoice(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__OUTPUT_ATTRIBUTE:
				return basicSetOutputAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__OVERLAPS_STORAGE_ATTRIBUTE:
				return basicSetOverlapsStorageAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__OVERRIDING:
				return basicSetOverriding(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PACK_PRAGMA:
				return basicSetPackPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PACKAGE_BODY_DECLARATION:
				return basicSetPackageBodyDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PACKAGE_BODY_STUB:
				return basicSetPackageBodyStub(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PACKAGE_DECLARATION:
				return basicSetPackageDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PACKAGE_INSTANTIATION:
				return basicSetPackageInstantiation(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PACKAGE_RENAMING_DECLARATION:
				return basicSetPackageRenamingDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PAGE_PRAGMA:
				return basicSetPagePragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PARAMETER_ASSOCIATION:
				return basicSetParameterAssociation(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PARAMETER_SPECIFICATION:
				return basicSetParameterSpecification(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PARENTHESIZED_EXPRESSION:
				return basicSetParenthesizedExpression(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PARTITION_ELABORATION_POLICY_PRAGMA:
				return basicSetPartitionElaborationPolicyPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PARTITION_ID_ATTRIBUTE:
				return basicSetPartitionIdAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PLUS_OPERATOR:
				return basicSetPlusOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				return basicSetPoolSpecificAccessToVariable(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__POS_ATTRIBUTE:
				return basicSetPosAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__POSITION_ATTRIBUTE:
				return basicSetPositionAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__POSITIONAL_ARRAY_AGGREGATE:
				return basicSetPositionalArrayAggregate(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PRAGMA_ARGUMENT_ASSOCIATION:
				return basicSetPragmaArgumentAssociation(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PRED_ATTRIBUTE:
				return basicSetPredAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PREELABORABLE_INITIALIZATION_PRAGMA:
				return basicSetPreelaborableInitializationPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PREELABORATE_PRAGMA:
				return basicSetPreelaboratePragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PRIORITY_ATTRIBUTE:
				return basicSetPriorityAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PRIORITY_PRAGMA:
				return basicSetPriorityPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return basicSetPrioritySpecificDispatchingPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PRIVATE:
				return basicSetPrivate(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PRIVATE_EXTENSION_DECLARATION:
				return basicSetPrivateExtensionDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PRIVATE_EXTENSION_DEFINITION:
				return basicSetPrivateExtensionDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PRIVATE_TYPE_DECLARATION:
				return basicSetPrivateTypeDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PRIVATE_TYPE_DEFINITION:
				return basicSetPrivateTypeDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_BODY_DECLARATION:
				return basicSetProcedureBodyDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_BODY_STUB:
				return basicSetProcedureBodyStub(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_CALL_STATEMENT:
				return basicSetProcedureCallStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_DECLARATION:
				return basicSetProcedureDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_INSTANTIATION:
				return basicSetProcedureInstantiation(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_RENAMING_DECLARATION:
				return basicSetProcedureRenamingDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PROFILE_PRAGMA:
				return basicSetProfilePragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PROTECTED_BODY_DECLARATION:
				return basicSetProtectedBodyDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PROTECTED_BODY_STUB:
				return basicSetProtectedBodyStub(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PROTECTED_DEFINITION:
				return basicSetProtectedDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PROTECTED_INTERFACE:
				return basicSetProtectedInterface(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PROTECTED_TYPE_DECLARATION:
				return basicSetProtectedTypeDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__PURE_PRAGMA:
				return basicSetPurePragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__QUALIFIED_EXPRESSION:
				return basicSetQualifiedExpression(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__QUEUING_POLICY_PRAGMA:
				return basicSetQueuingPolicyPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__RAISE_EXPRESSION:
				return basicSetRaiseExpression(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__RAISE_STATEMENT:
				return basicSetRaiseStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__RANGE_ATTRIBUTE:
				return basicSetRangeAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__RANGE_ATTRIBUTE_REFERENCE:
				return basicSetRangeAttributeReference(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__READ_ATTRIBUTE:
				return basicSetReadAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__REAL_LITERAL:
				return basicSetRealLiteral(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__REAL_NUMBER_DECLARATION:
				return basicSetRealNumberDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__RECORD_AGGREGATE:
				return basicSetRecordAggregate(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__RECORD_COMPONENT_ASSOCIATION:
				return basicSetRecordComponentAssociation(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__RECORD_DEFINITION:
				return basicSetRecordDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__RECORD_REPRESENTATION_CLAUSE:
				return basicSetRecordRepresentationClause(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__RECORD_TYPE_DEFINITION:
				return basicSetRecordTypeDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__RELATIVE_DEADLINE_PRAGMA:
				return basicSetRelativeDeadlinePragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__REM_OPERATOR:
				return basicSetRemOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__REMAINDER_ATTRIBUTE:
				return basicSetRemainderAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__REMOTE_CALL_INTERFACE_PRAGMA:
				return basicSetRemoteCallInterfacePragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__REMOTE_TYPES_PRAGMA:
				return basicSetRemoteTypesPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__REQUEUE_STATEMENT:
				return basicSetRequeueStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__REQUEUE_STATEMENT_WITH_ABORT:
				return basicSetRequeueStatementWithAbort(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__RESTRICTIONS_PRAGMA:
				return basicSetRestrictionsPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__RETURN_CONSTANT_SPECIFICATION:
				return basicSetReturnConstantSpecification(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__RETURN_STATEMENT:
				return basicSetReturnStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__RETURN_VARIABLE_SPECIFICATION:
				return basicSetReturnVariableSpecification(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__REVERSE:
				return basicSetReverse(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__REVIEWABLE_PRAGMA:
				return basicSetReviewablePragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ROOT_INTEGER_DEFINITION:
				return basicSetRootIntegerDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ROOT_REAL_DEFINITION:
				return basicSetRootRealDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ROUND_ATTRIBUTE:
				return basicSetRoundAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__ROUNDING_ATTRIBUTE:
				return basicSetRoundingAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__SAFE_FIRST_ATTRIBUTE:
				return basicSetSafeFirstAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__SAFE_LAST_ATTRIBUTE:
				return basicSetSafeLastAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__SCALE_ATTRIBUTE:
				return basicSetScaleAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__SCALING_ATTRIBUTE:
				return basicSetScalingAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__SELECT_PATH:
				return basicSetSelectPath(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__SELECTED_COMPONENT:
				return basicSetSelectedComponent(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__SELECTIVE_ACCEPT_STATEMENT:
				return basicSetSelectiveAcceptStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__SHARED_PASSIVE_PRAGMA:
				return basicSetSharedPassivePragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__SIGNED_INTEGER_TYPE_DEFINITION:
				return basicSetSignedIntegerTypeDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__SIGNED_ZEROS_ATTRIBUTE:
				return basicSetSignedZerosAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__SIMPLE_EXPRESSION_RANGE:
				return basicSetSimpleExpressionRange(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__SINGLE_PROTECTED_DECLARATION:
				return basicSetSingleProtectedDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__SINGLE_TASK_DECLARATION:
				return basicSetSingleTaskDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__SIZE_ATTRIBUTE:
				return basicSetSizeAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__SLICE:
				return basicSetSlice(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__SMALL_ATTRIBUTE:
				return basicSetSmallAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__STORAGE_POOL_ATTRIBUTE:
				return basicSetStoragePoolAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__STORAGE_SIZE_ATTRIBUTE:
				return basicSetStorageSizeAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__STORAGE_SIZE_PRAGMA:
				return basicSetStorageSizePragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__STREAM_SIZE_ATTRIBUTE:
				return basicSetStreamSizeAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__STRING_LITERAL:
				return basicSetStringLiteral(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__SUBTYPE_DECLARATION:
				return basicSetSubtypeDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__SUBTYPE_INDICATION:
				return basicSetSubtypeIndication(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__SUCC_ATTRIBUTE:
				return basicSetSuccAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__SUPPRESS_PRAGMA:
				return basicSetSuppressPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__SYNCHRONIZED:
				return basicSetSynchronized(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__SYNCHRONIZED_INTERFACE:
				return basicSetSynchronizedInterface(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__TAG_ATTRIBUTE:
				return basicSetTagAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__TAGGED:
				return basicSetTagged(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__TAGGED_INCOMPLETE_TYPE_DECLARATION:
				return basicSetTaggedIncompleteTypeDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__TAGGED_PRIVATE_TYPE_DEFINITION:
				return basicSetTaggedPrivateTypeDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__TAGGED_RECORD_TYPE_DEFINITION:
				return basicSetTaggedRecordTypeDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__TASK_BODY_DECLARATION:
				return basicSetTaskBodyDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__TASK_BODY_STUB:
				return basicSetTaskBodyStub(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__TASK_DEFINITION:
				return basicSetTaskDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__TASK_DISPATCHING_POLICY_PRAGMA:
				return basicSetTaskDispatchingPolicyPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__TASK_INTERFACE:
				return basicSetTaskInterface(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__TASK_TYPE_DECLARATION:
				return basicSetTaskTypeDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__TERMINATE_ALTERNATIVE_STATEMENT:
				return basicSetTerminateAlternativeStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__TERMINATED_ATTRIBUTE:
				return basicSetTerminatedAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__THEN_ABORT_PATH:
				return basicSetThenAbortPath(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__TIMED_ENTRY_CALL_STATEMENT:
				return basicSetTimedEntryCallStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__TRUNCATION_ATTRIBUTE:
				return basicSetTruncationAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__TYPE_CONVERSION:
				return basicSetTypeConversion(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__UNARY_MINUS_OPERATOR:
				return basicSetUnaryMinusOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__UNARY_PLUS_OPERATOR:
				return basicSetUnaryPlusOperator(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__UNBIASED_ROUNDING_ATTRIBUTE:
				return basicSetUnbiasedRoundingAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__UNCHECKED_ACCESS_ATTRIBUTE:
				return basicSetUncheckedAccessAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__UNCHECKED_UNION_PRAGMA:
				return basicSetUncheckedUnionPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__UNCONSTRAINED_ARRAY_DEFINITION:
				return basicSetUnconstrainedArrayDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__UNIVERSAL_FIXED_DEFINITION:
				return basicSetUniversalFixedDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__UNIVERSAL_INTEGER_DEFINITION:
				return basicSetUniversalIntegerDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__UNIVERSAL_REAL_DEFINITION:
				return basicSetUniversalRealDefinition(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__UNKNOWN_ATTRIBUTE:
				return basicSetUnknownAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__UNKNOWN_DISCRIMINANT_PART:
				return basicSetUnknownDiscriminantPart(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__UNKNOWN_PRAGMA:
				return basicSetUnknownPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__UNSUPPRESS_PRAGMA:
				return basicSetUnsuppressPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__USE_ALL_TYPE_CLAUSE:
				return basicSetUseAllTypeClause(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__USE_PACKAGE_CLAUSE:
				return basicSetUsePackageClause(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__USE_TYPE_CLAUSE:
				return basicSetUseTypeClause(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__VAL_ATTRIBUTE:
				return basicSetValAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__VALID_ATTRIBUTE:
				return basicSetValidAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__VALUE_ATTRIBUTE:
				return basicSetValueAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__VARIABLE_DECLARATION:
				return basicSetVariableDeclaration(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__VARIANT:
				return basicSetVariant(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__VARIANT_PART:
				return basicSetVariantPart(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__VERSION_ATTRIBUTE:
				return basicSetVersionAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__VOLATILE_COMPONENTS_PRAGMA:
				return basicSetVolatileComponentsPragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__VOLATILE_PRAGMA:
				return basicSetVolatilePragma(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__WHILE_LOOP_STATEMENT:
				return basicSetWhileLoopStatement(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__WIDE_IMAGE_ATTRIBUTE:
				return basicSetWideImageAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__WIDE_VALUE_ATTRIBUTE:
				return basicSetWideValueAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__WIDE_WIDE_IMAGE_ATTRIBUTE:
				return basicSetWideWideImageAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__WIDE_WIDE_VALUE_ATTRIBUTE:
				return basicSetWideWideValueAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__WIDE_WIDE_WIDTH_ATTRIBUTE:
				return basicSetWideWideWidthAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__WIDE_WIDTH_ATTRIBUTE:
				return basicSetWideWidthAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__WIDTH_ATTRIBUTE:
				return basicSetWidthAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__WITH_CLAUSE:
				return basicSetWithClause(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__WRITE_ATTRIBUTE:
				return basicSetWriteAttribute(null, msgs);
			case AdaPackage.DOCUMENT_ROOT__XOR_OPERATOR:
				return basicSetXorOperator(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.DOCUMENT_ROOT__MIXED:
				if (coreType) return getMixed();
				return ((FeatureMap.Internal)getMixed()).getWrapper();
			case AdaPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				if (coreType) return getXMLNSPrefixMap();
				else return getXMLNSPrefixMap().map();
			case AdaPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				if (coreType) return getXSISchemaLocation();
				else return getXSISchemaLocation().map();
			case AdaPackage.DOCUMENT_ROOT__ABORT_STATEMENT:
				return getAbortStatement();
			case AdaPackage.DOCUMENT_ROOT__ABS_OPERATOR:
				return getAbsOperator();
			case AdaPackage.DOCUMENT_ROOT__ABSTRACT:
				return getAbstract();
			case AdaPackage.DOCUMENT_ROOT__ACCEPT_STATEMENT:
				return getAcceptStatement();
			case AdaPackage.DOCUMENT_ROOT__ACCESS_ATTRIBUTE:
				return getAccessAttribute();
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_CONSTANT:
				return getAccessToConstant();
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_FUNCTION:
				return getAccessToFunction();
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_PROCEDURE:
				return getAccessToProcedure();
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_PROTECTED_FUNCTION:
				return getAccessToProtectedFunction();
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_PROTECTED_PROCEDURE:
				return getAccessToProtectedProcedure();
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_VARIABLE:
				return getAccessToVariable();
			case AdaPackage.DOCUMENT_ROOT__ADDRESS_ATTRIBUTE:
				return getAddressAttribute();
			case AdaPackage.DOCUMENT_ROOT__ADJACENT_ATTRIBUTE:
				return getAdjacentAttribute();
			case AdaPackage.DOCUMENT_ROOT__AFT_ATTRIBUTE:
				return getAftAttribute();
			case AdaPackage.DOCUMENT_ROOT__ALIASED:
				return getAliased();
			case AdaPackage.DOCUMENT_ROOT__ALIGNMENT_ATTRIBUTE:
				return getAlignmentAttribute();
			case AdaPackage.DOCUMENT_ROOT__ALL_CALLS_REMOTE_PRAGMA:
				return getAllCallsRemotePragma();
			case AdaPackage.DOCUMENT_ROOT__ALLOCATION_FROM_QUALIFIED_EXPRESSION:
				return getAllocationFromQualifiedExpression();
			case AdaPackage.DOCUMENT_ROOT__ALLOCATION_FROM_SUBTYPE:
				return getAllocationFromSubtype();
			case AdaPackage.DOCUMENT_ROOT__AND_OPERATOR:
				return getAndOperator();
			case AdaPackage.DOCUMENT_ROOT__AND_THEN_SHORT_CIRCUIT:
				return getAndThenShortCircuit();
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_CONSTANT:
				return getAnonymousAccessToConstant();
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_FUNCTION:
				return getAnonymousAccessToFunction();
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_PROCEDURE:
				return getAnonymousAccessToProcedure();
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION:
				return getAnonymousAccessToProtectedFunction();
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE:
				return getAnonymousAccessToProtectedProcedure();
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_VARIABLE:
				return getAnonymousAccessToVariable();
			case AdaPackage.DOCUMENT_ROOT__ARRAY_COMPONENT_ASSOCIATION:
				return getArrayComponentAssociation();
			case AdaPackage.DOCUMENT_ROOT__ASPECT_SPECIFICATION:
				return getAspectSpecification();
			case AdaPackage.DOCUMENT_ROOT__ASSERT_PRAGMA:
				return getAssertPragma();
			case AdaPackage.DOCUMENT_ROOT__ASSERTION_POLICY_PRAGMA:
				return getAssertionPolicyPragma();
			case AdaPackage.DOCUMENT_ROOT__ASSIGNMENT_STATEMENT:
				return getAssignmentStatement();
			case AdaPackage.DOCUMENT_ROOT__ASYNCHRONOUS_PRAGMA:
				return getAsynchronousPragma();
			case AdaPackage.DOCUMENT_ROOT__ASYNCHRONOUS_SELECT_STATEMENT:
				return getAsynchronousSelectStatement();
			case AdaPackage.DOCUMENT_ROOT__AT_CLAUSE:
				return getAtClause();
			case AdaPackage.DOCUMENT_ROOT__ATOMIC_COMPONENTS_PRAGMA:
				return getAtomicComponentsPragma();
			case AdaPackage.DOCUMENT_ROOT__ATOMIC_PRAGMA:
				return getAtomicPragma();
			case AdaPackage.DOCUMENT_ROOT__ATTACH_HANDLER_PRAGMA:
				return getAttachHandlerPragma();
			case AdaPackage.DOCUMENT_ROOT__ATTRIBUTE_DEFINITION_CLAUSE:
				return getAttributeDefinitionClause();
			case AdaPackage.DOCUMENT_ROOT__BASE_ATTRIBUTE:
				return getBaseAttribute();
			case AdaPackage.DOCUMENT_ROOT__BIT_ORDER_ATTRIBUTE:
				return getBitOrderAttribute();
			case AdaPackage.DOCUMENT_ROOT__BLOCK_STATEMENT:
				return getBlockStatement();
			case AdaPackage.DOCUMENT_ROOT__BODY_VERSION_ATTRIBUTE:
				return getBodyVersionAttribute();
			case AdaPackage.DOCUMENT_ROOT__BOX_EXPRESSION:
				return getBoxExpression();
			case AdaPackage.DOCUMENT_ROOT__CALLABLE_ATTRIBUTE:
				return getCallableAttribute();
			case AdaPackage.DOCUMENT_ROOT__CALLER_ATTRIBUTE:
				return getCallerAttribute();
			case AdaPackage.DOCUMENT_ROOT__CASE_EXPRESSION:
				return getCaseExpression();
			case AdaPackage.DOCUMENT_ROOT__CASE_EXPRESSION_PATH:
				return getCaseExpressionPath();
			case AdaPackage.DOCUMENT_ROOT__CASE_PATH:
				return getCasePath();
			case AdaPackage.DOCUMENT_ROOT__CASE_STATEMENT:
				return getCaseStatement();
			case AdaPackage.DOCUMENT_ROOT__CEILING_ATTRIBUTE:
				return getCeilingAttribute();
			case AdaPackage.DOCUMENT_ROOT__CHARACTER_LITERAL:
				return getCharacterLiteral();
			case AdaPackage.DOCUMENT_ROOT__CHOICE_PARAMETER_SPECIFICATION:
				return getChoiceParameterSpecification();
			case AdaPackage.DOCUMENT_ROOT__CLASS_ATTRIBUTE:
				return getClassAttribute();
			case AdaPackage.DOCUMENT_ROOT__CODE_STATEMENT:
				return getCodeStatement();
			case AdaPackage.DOCUMENT_ROOT__COMMENT:
				return getComment();
			case AdaPackage.DOCUMENT_ROOT__COMPILATION_UNIT:
				return getCompilationUnit();
			case AdaPackage.DOCUMENT_ROOT__COMPONENT_CLAUSE:
				return getComponentClause();
			case AdaPackage.DOCUMENT_ROOT__COMPONENT_DECLARATION:
				return getComponentDeclaration();
			case AdaPackage.DOCUMENT_ROOT__COMPONENT_DEFINITION:
				return getComponentDefinition();
			case AdaPackage.DOCUMENT_ROOT__COMPONENT_SIZE_ATTRIBUTE:
				return getComponentSizeAttribute();
			case AdaPackage.DOCUMENT_ROOT__COMPOSE_ATTRIBUTE:
				return getComposeAttribute();
			case AdaPackage.DOCUMENT_ROOT__CONCATENATE_OPERATOR:
				return getConcatenateOperator();
			case AdaPackage.DOCUMENT_ROOT__CONDITIONAL_ENTRY_CALL_STATEMENT:
				return getConditionalEntryCallStatement();
			case AdaPackage.DOCUMENT_ROOT__CONSTANT_DECLARATION:
				return getConstantDeclaration();
			case AdaPackage.DOCUMENT_ROOT__CONSTRAINED_ARRAY_DEFINITION:
				return getConstrainedArrayDefinition();
			case AdaPackage.DOCUMENT_ROOT__CONSTRAINED_ATTRIBUTE:
				return getConstrainedAttribute();
			case AdaPackage.DOCUMENT_ROOT__CONTROLLED_PRAGMA:
				return getControlledPragma();
			case AdaPackage.DOCUMENT_ROOT__CONVENTION_PRAGMA:
				return getConventionPragma();
			case AdaPackage.DOCUMENT_ROOT__COPY_SIGN_ATTRIBUTE:
				return getCopySignAttribute();
			case AdaPackage.DOCUMENT_ROOT__COUNT_ATTRIBUTE:
				return getCountAttribute();
			case AdaPackage.DOCUMENT_ROOT__CPU_PRAGMA:
				return getCpuPragma();
			case AdaPackage.DOCUMENT_ROOT__DECIMAL_FIXED_POINT_DEFINITION:
				return getDecimalFixedPointDefinition();
			case AdaPackage.DOCUMENT_ROOT__DEFAULT_STORAGE_POOL_PRAGMA:
				return getDefaultStoragePoolPragma();
			case AdaPackage.DOCUMENT_ROOT__DEFERRED_CONSTANT_DECLARATION:
				return getDeferredConstantDeclaration();
			case AdaPackage.DOCUMENT_ROOT__DEFINING_ABS_OPERATOR:
				return getDefiningAbsOperator();
			case AdaPackage.DOCUMENT_ROOT__DEFINING_AND_OPERATOR:
				return getDefiningAndOperator();
			case AdaPackage.DOCUMENT_ROOT__DEFINING_CHARACTER_LITERAL:
				return getDefiningCharacterLiteral();
			case AdaPackage.DOCUMENT_ROOT__DEFINING_CONCATENATE_OPERATOR:
				return getDefiningConcatenateOperator();
			case AdaPackage.DOCUMENT_ROOT__DEFINING_DIVIDE_OPERATOR:
				return getDefiningDivideOperator();
			case AdaPackage.DOCUMENT_ROOT__DEFINING_ENUMERATION_LITERAL:
				return getDefiningEnumerationLiteral();
			case AdaPackage.DOCUMENT_ROOT__DEFINING_EQUAL_OPERATOR:
				return getDefiningEqualOperator();
			case AdaPackage.DOCUMENT_ROOT__DEFINING_EXPANDED_NAME:
				return getDefiningExpandedName();
			case AdaPackage.DOCUMENT_ROOT__DEFINING_EXPONENTIATE_OPERATOR:
				return getDefiningExponentiateOperator();
			case AdaPackage.DOCUMENT_ROOT__DEFINING_GREATER_THAN_OPERATOR:
				return getDefiningGreaterThanOperator();
			case AdaPackage.DOCUMENT_ROOT__DEFINING_GREATER_THAN_OR_EQUAL_OPERATOR:
				return getDefiningGreaterThanOrEqualOperator();
			case AdaPackage.DOCUMENT_ROOT__DEFINING_IDENTIFIER:
				return getDefiningIdentifier();
			case AdaPackage.DOCUMENT_ROOT__DEFINING_LESS_THAN_OPERATOR:
				return getDefiningLessThanOperator();
			case AdaPackage.DOCUMENT_ROOT__DEFINING_LESS_THAN_OR_EQUAL_OPERATOR:
				return getDefiningLessThanOrEqualOperator();
			case AdaPackage.DOCUMENT_ROOT__DEFINING_MINUS_OPERATOR:
				return getDefiningMinusOperator();
			case AdaPackage.DOCUMENT_ROOT__DEFINING_MOD_OPERATOR:
				return getDefiningModOperator();
			case AdaPackage.DOCUMENT_ROOT__DEFINING_MULTIPLY_OPERATOR:
				return getDefiningMultiplyOperator();
			case AdaPackage.DOCUMENT_ROOT__DEFINING_NOT_EQUAL_OPERATOR:
				return getDefiningNotEqualOperator();
			case AdaPackage.DOCUMENT_ROOT__DEFINING_NOT_OPERATOR:
				return getDefiningNotOperator();
			case AdaPackage.DOCUMENT_ROOT__DEFINING_OR_OPERATOR:
				return getDefiningOrOperator();
			case AdaPackage.DOCUMENT_ROOT__DEFINING_PLUS_OPERATOR:
				return getDefiningPlusOperator();
			case AdaPackage.DOCUMENT_ROOT__DEFINING_REM_OPERATOR:
				return getDefiningRemOperator();
			case AdaPackage.DOCUMENT_ROOT__DEFINING_UNARY_MINUS_OPERATOR:
				return getDefiningUnaryMinusOperator();
			case AdaPackage.DOCUMENT_ROOT__DEFINING_UNARY_PLUS_OPERATOR:
				return getDefiningUnaryPlusOperator();
			case AdaPackage.DOCUMENT_ROOT__DEFINING_XOR_OPERATOR:
				return getDefiningXorOperator();
			case AdaPackage.DOCUMENT_ROOT__DEFINITE_ATTRIBUTE:
				return getDefiniteAttribute();
			case AdaPackage.DOCUMENT_ROOT__DELAY_RELATIVE_STATEMENT:
				return getDelayRelativeStatement();
			case AdaPackage.DOCUMENT_ROOT__DELAY_UNTIL_STATEMENT:
				return getDelayUntilStatement();
			case AdaPackage.DOCUMENT_ROOT__DELTA_ATTRIBUTE:
				return getDeltaAttribute();
			case AdaPackage.DOCUMENT_ROOT__DELTA_CONSTRAINT:
				return getDeltaConstraint();
			case AdaPackage.DOCUMENT_ROOT__DENORM_ATTRIBUTE:
				return getDenormAttribute();
			case AdaPackage.DOCUMENT_ROOT__DERIVED_RECORD_EXTENSION_DEFINITION:
				return getDerivedRecordExtensionDefinition();
			case AdaPackage.DOCUMENT_ROOT__DERIVED_TYPE_DEFINITION:
				return getDerivedTypeDefinition();
			case AdaPackage.DOCUMENT_ROOT__DETECT_BLOCKING_PRAGMA:
				return getDetectBlockingPragma();
			case AdaPackage.DOCUMENT_ROOT__DIGITS_ATTRIBUTE:
				return getDigitsAttribute();
			case AdaPackage.DOCUMENT_ROOT__DIGITS_CONSTRAINT:
				return getDigitsConstraint();
			case AdaPackage.DOCUMENT_ROOT__DISCARD_NAMES_PRAGMA:
				return getDiscardNamesPragma();
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_RANGE_ATTRIBUTE_REFERENCE:
				return getDiscreteRangeAttributeReference();
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION:
				return getDiscreteRangeAttributeReferenceAsSubtypeDefinition();
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_SIMPLE_EXPRESSION_RANGE:
				return getDiscreteSimpleExpressionRange();
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION:
				return getDiscreteSimpleExpressionRangeAsSubtypeDefinition();
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_SUBTYPE_INDICATION:
				return getDiscreteSubtypeIndication();
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION:
				return getDiscreteSubtypeIndicationAsSubtypeDefinition();
			case AdaPackage.DOCUMENT_ROOT__DISCRIMINANT_ASSOCIATION:
				return getDiscriminantAssociation();
			case AdaPackage.DOCUMENT_ROOT__DISCRIMINANT_CONSTRAINT:
				return getDiscriminantConstraint();
			case AdaPackage.DOCUMENT_ROOT__DISCRIMINANT_SPECIFICATION:
				return getDiscriminantSpecification();
			case AdaPackage.DOCUMENT_ROOT__DISPATCHING_DOMAIN_PRAGMA:
				return getDispatchingDomainPragma();
			case AdaPackage.DOCUMENT_ROOT__DIVIDE_OPERATOR:
				return getDivideOperator();
			case AdaPackage.DOCUMENT_ROOT__ELABORATE_ALL_PRAGMA:
				return getElaborateAllPragma();
			case AdaPackage.DOCUMENT_ROOT__ELABORATE_BODY_PRAGMA:
				return getElaborateBodyPragma();
			case AdaPackage.DOCUMENT_ROOT__ELABORATE_PRAGMA:
				return getElaboratePragma();
			case AdaPackage.DOCUMENT_ROOT__ELEMENT_ITERATOR_SPECIFICATION:
				return getElementIteratorSpecification();
			case AdaPackage.DOCUMENT_ROOT__ELSE_EXPRESSION_PATH:
				return getElseExpressionPath();
			case AdaPackage.DOCUMENT_ROOT__ELSE_PATH:
				return getElsePath();
			case AdaPackage.DOCUMENT_ROOT__ELSIF_EXPRESSION_PATH:
				return getElsifExpressionPath();
			case AdaPackage.DOCUMENT_ROOT__ELSIF_PATH:
				return getElsifPath();
			case AdaPackage.DOCUMENT_ROOT__ENTRY_BODY_DECLARATION:
				return getEntryBodyDeclaration();
			case AdaPackage.DOCUMENT_ROOT__ENTRY_CALL_STATEMENT:
				return getEntryCallStatement();
			case AdaPackage.DOCUMENT_ROOT__ENTRY_DECLARATION:
				return getEntryDeclaration();
			case AdaPackage.DOCUMENT_ROOT__ENTRY_INDEX_SPECIFICATION:
				return getEntryIndexSpecification();
			case AdaPackage.DOCUMENT_ROOT__ENUMERATION_LITERAL:
				return getEnumerationLiteral();
			case AdaPackage.DOCUMENT_ROOT__ENUMERATION_LITERAL_SPECIFICATION:
				return getEnumerationLiteralSpecification();
			case AdaPackage.DOCUMENT_ROOT__ENUMERATION_REPRESENTATION_CLAUSE:
				return getEnumerationRepresentationClause();
			case AdaPackage.DOCUMENT_ROOT__ENUMERATION_TYPE_DEFINITION:
				return getEnumerationTypeDefinition();
			case AdaPackage.DOCUMENT_ROOT__EQUAL_OPERATOR:
				return getEqualOperator();
			case AdaPackage.DOCUMENT_ROOT__EXCEPTION_DECLARATION:
				return getExceptionDeclaration();
			case AdaPackage.DOCUMENT_ROOT__EXCEPTION_HANDLER:
				return getExceptionHandler();
			case AdaPackage.DOCUMENT_ROOT__EXCEPTION_RENAMING_DECLARATION:
				return getExceptionRenamingDeclaration();
			case AdaPackage.DOCUMENT_ROOT__EXIT_STATEMENT:
				return getExitStatement();
			case AdaPackage.DOCUMENT_ROOT__EXPLICIT_DEREFERENCE:
				return getExplicitDereference();
			case AdaPackage.DOCUMENT_ROOT__EXPONENT_ATTRIBUTE:
				return getExponentAttribute();
			case AdaPackage.DOCUMENT_ROOT__EXPONENTIATE_OPERATOR:
				return getExponentiateOperator();
			case AdaPackage.DOCUMENT_ROOT__EXPORT_PRAGMA:
				return getExportPragma();
			case AdaPackage.DOCUMENT_ROOT__EXPRESSION_FUNCTION_DECLARATION:
				return getExpressionFunctionDeclaration();
			case AdaPackage.DOCUMENT_ROOT__EXTENDED_RETURN_STATEMENT:
				return getExtendedReturnStatement();
			case AdaPackage.DOCUMENT_ROOT__EXTENSION_AGGREGATE:
				return getExtensionAggregate();
			case AdaPackage.DOCUMENT_ROOT__EXTERNAL_TAG_ATTRIBUTE:
				return getExternalTagAttribute();
			case AdaPackage.DOCUMENT_ROOT__FIRST_ATTRIBUTE:
				return getFirstAttribute();
			case AdaPackage.DOCUMENT_ROOT__FIRST_BIT_ATTRIBUTE:
				return getFirstBitAttribute();
			case AdaPackage.DOCUMENT_ROOT__FLOATING_POINT_DEFINITION:
				return getFloatingPointDefinition();
			case AdaPackage.DOCUMENT_ROOT__FLOOR_ATTRIBUTE:
				return getFloorAttribute();
			case AdaPackage.DOCUMENT_ROOT__FOR_ALL_QUANTIFIED_EXPRESSION:
				return getForAllQuantifiedExpression();
			case AdaPackage.DOCUMENT_ROOT__FOR_LOOP_STATEMENT:
				return getForLoopStatement();
			case AdaPackage.DOCUMENT_ROOT__FOR_SOME_QUANTIFIED_EXPRESSION:
				return getForSomeQuantifiedExpression();
			case AdaPackage.DOCUMENT_ROOT__FORE_ATTRIBUTE:
				return getForeAttribute();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_CONSTANT:
				return getFormalAccessToConstant();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_FUNCTION:
				return getFormalAccessToFunction();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_PROCEDURE:
				return getFormalAccessToProcedure();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_PROTECTED_FUNCTION:
				return getFormalAccessToProtectedFunction();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_PROTECTED_PROCEDURE:
				return getFormalAccessToProtectedProcedure();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_VARIABLE:
				return getFormalAccessToVariable();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_CONSTRAINED_ARRAY_DEFINITION:
				return getFormalConstrainedArrayDefinition();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_DECIMAL_FIXED_POINT_DEFINITION:
				return getFormalDecimalFixedPointDefinition();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_DERIVED_TYPE_DEFINITION:
				return getFormalDerivedTypeDefinition();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_DISCRETE_TYPE_DEFINITION:
				return getFormalDiscreteTypeDefinition();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_FLOATING_POINT_DEFINITION:
				return getFormalFloatingPointDefinition();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_FUNCTION_DECLARATION:
				return getFormalFunctionDeclaration();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_INCOMPLETE_TYPE_DECLARATION:
				return getFormalIncompleteTypeDeclaration();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_LIMITED_INTERFACE:
				return getFormalLimitedInterface();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_MODULAR_TYPE_DEFINITION:
				return getFormalModularTypeDefinition();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_OBJECT_DECLARATION:
				return getFormalObjectDeclaration();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ORDINARY_FIXED_POINT_DEFINITION:
				return getFormalOrdinaryFixedPointDefinition();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ORDINARY_INTERFACE:
				return getFormalOrdinaryInterface();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_PACKAGE_DECLARATION:
				return getFormalPackageDeclaration();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_PACKAGE_DECLARATION_WITH_BOX:
				return getFormalPackageDeclarationWithBox();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				return getFormalPoolSpecificAccessToVariable();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_PRIVATE_TYPE_DEFINITION:
				return getFormalPrivateTypeDefinition();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_PROCEDURE_DECLARATION:
				return getFormalProcedureDeclaration();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_PROTECTED_INTERFACE:
				return getFormalProtectedInterface();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_SIGNED_INTEGER_TYPE_DEFINITION:
				return getFormalSignedIntegerTypeDefinition();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_SYNCHRONIZED_INTERFACE:
				return getFormalSynchronizedInterface();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION:
				return getFormalTaggedPrivateTypeDefinition();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_TASK_INTERFACE:
				return getFormalTaskInterface();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_TYPE_DECLARATION:
				return getFormalTypeDeclaration();
			case AdaPackage.DOCUMENT_ROOT__FORMAL_UNCONSTRAINED_ARRAY_DEFINITION:
				return getFormalUnconstrainedArrayDefinition();
			case AdaPackage.DOCUMENT_ROOT__FRACTION_ATTRIBUTE:
				return getFractionAttribute();
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_BODY_DECLARATION:
				return getFunctionBodyDeclaration();
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_BODY_STUB:
				return getFunctionBodyStub();
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_CALL:
				return getFunctionCall();
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_DECLARATION:
				return getFunctionDeclaration();
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_INSTANTIATION:
				return getFunctionInstantiation();
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_RENAMING_DECLARATION:
				return getFunctionRenamingDeclaration();
			case AdaPackage.DOCUMENT_ROOT__GENERALIZED_ITERATOR_SPECIFICATION:
				return getGeneralizedIteratorSpecification();
			case AdaPackage.DOCUMENT_ROOT__GENERIC_ASSOCIATION:
				return getGenericAssociation();
			case AdaPackage.DOCUMENT_ROOT__GENERIC_FUNCTION_DECLARATION:
				return getGenericFunctionDeclaration();
			case AdaPackage.DOCUMENT_ROOT__GENERIC_FUNCTION_RENAMING_DECLARATION:
				return getGenericFunctionRenamingDeclaration();
			case AdaPackage.DOCUMENT_ROOT__GENERIC_PACKAGE_DECLARATION:
				return getGenericPackageDeclaration();
			case AdaPackage.DOCUMENT_ROOT__GENERIC_PACKAGE_RENAMING_DECLARATION:
				return getGenericPackageRenamingDeclaration();
			case AdaPackage.DOCUMENT_ROOT__GENERIC_PROCEDURE_DECLARATION:
				return getGenericProcedureDeclaration();
			case AdaPackage.DOCUMENT_ROOT__GENERIC_PROCEDURE_RENAMING_DECLARATION:
				return getGenericProcedureRenamingDeclaration();
			case AdaPackage.DOCUMENT_ROOT__GOTO_STATEMENT:
				return getGotoStatement();
			case AdaPackage.DOCUMENT_ROOT__GREATER_THAN_OPERATOR:
				return getGreaterThanOperator();
			case AdaPackage.DOCUMENT_ROOT__GREATER_THAN_OR_EQUAL_OPERATOR:
				return getGreaterThanOrEqualOperator();
			case AdaPackage.DOCUMENT_ROOT__IDENTIFIER:
				return getIdentifier();
			case AdaPackage.DOCUMENT_ROOT__IDENTITY_ATTRIBUTE:
				return getIdentityAttribute();
			case AdaPackage.DOCUMENT_ROOT__IF_EXPRESSION:
				return getIfExpression();
			case AdaPackage.DOCUMENT_ROOT__IF_EXPRESSION_PATH:
				return getIfExpressionPath();
			case AdaPackage.DOCUMENT_ROOT__IF_PATH:
				return getIfPath();
			case AdaPackage.DOCUMENT_ROOT__IF_STATEMENT:
				return getIfStatement();
			case AdaPackage.DOCUMENT_ROOT__IMAGE_ATTRIBUTE:
				return getImageAttribute();
			case AdaPackage.DOCUMENT_ROOT__IMPLEMENTATION_DEFINED_ATTRIBUTE:
				return getImplementationDefinedAttribute();
			case AdaPackage.DOCUMENT_ROOT__IMPLEMENTATION_DEFINED_PRAGMA:
				return getImplementationDefinedPragma();
			case AdaPackage.DOCUMENT_ROOT__IMPORT_PRAGMA:
				return getImportPragma();
			case AdaPackage.DOCUMENT_ROOT__IN_MEMBERSHIP_TEST:
				return getInMembershipTest();
			case AdaPackage.DOCUMENT_ROOT__INCOMPLETE_TYPE_DECLARATION:
				return getIncompleteTypeDeclaration();
			case AdaPackage.DOCUMENT_ROOT__INDEPENDENT_COMPONENTS_PRAGMA:
				return getIndependentComponentsPragma();
			case AdaPackage.DOCUMENT_ROOT__INDEPENDENT_PRAGMA:
				return getIndependentPragma();
			case AdaPackage.DOCUMENT_ROOT__INDEX_CONSTRAINT:
				return getIndexConstraint();
			case AdaPackage.DOCUMENT_ROOT__INDEXED_COMPONENT:
				return getIndexedComponent();
			case AdaPackage.DOCUMENT_ROOT__INLINE_PRAGMA:
				return getInlinePragma();
			case AdaPackage.DOCUMENT_ROOT__INPUT_ATTRIBUTE:
				return getInputAttribute();
			case AdaPackage.DOCUMENT_ROOT__INSPECTION_POINT_PRAGMA:
				return getInspectionPointPragma();
			case AdaPackage.DOCUMENT_ROOT__INTEGER_LITERAL:
				return getIntegerLiteral();
			case AdaPackage.DOCUMENT_ROOT__INTEGER_NUMBER_DECLARATION:
				return getIntegerNumberDeclaration();
			case AdaPackage.DOCUMENT_ROOT__INTERRUPT_HANDLER_PRAGMA:
				return getInterruptHandlerPragma();
			case AdaPackage.DOCUMENT_ROOT__INTERRUPT_PRIORITY_PRAGMA:
				return getInterruptPriorityPragma();
			case AdaPackage.DOCUMENT_ROOT__IS_PREFIX_CALL:
				return getIsPrefixCall();
			case AdaPackage.DOCUMENT_ROOT__IS_PREFIX_NOTATION:
				return getIsPrefixNotation();
			case AdaPackage.DOCUMENT_ROOT__KNOWN_DISCRIMINANT_PART:
				return getKnownDiscriminantPart();
			case AdaPackage.DOCUMENT_ROOT__LAST_ATTRIBUTE:
				return getLastAttribute();
			case AdaPackage.DOCUMENT_ROOT__LAST_BIT_ATTRIBUTE:
				return getLastBitAttribute();
			case AdaPackage.DOCUMENT_ROOT__LEADING_PART_ATTRIBUTE:
				return getLeadingPartAttribute();
			case AdaPackage.DOCUMENT_ROOT__LENGTH_ATTRIBUTE:
				return getLengthAttribute();
			case AdaPackage.DOCUMENT_ROOT__LESS_THAN_OPERATOR:
				return getLessThanOperator();
			case AdaPackage.DOCUMENT_ROOT__LESS_THAN_OR_EQUAL_OPERATOR:
				return getLessThanOrEqualOperator();
			case AdaPackage.DOCUMENT_ROOT__LIMITED:
				return getLimited();
			case AdaPackage.DOCUMENT_ROOT__LIMITED_INTERFACE:
				return getLimitedInterface();
			case AdaPackage.DOCUMENT_ROOT__LINKER_OPTIONS_PRAGMA:
				return getLinkerOptionsPragma();
			case AdaPackage.DOCUMENT_ROOT__LIST_PRAGMA:
				return getListPragma();
			case AdaPackage.DOCUMENT_ROOT__LOCKING_POLICY_PRAGMA:
				return getLockingPolicyPragma();
			case AdaPackage.DOCUMENT_ROOT__LOOP_PARAMETER_SPECIFICATION:
				return getLoopParameterSpecification();
			case AdaPackage.DOCUMENT_ROOT__LOOP_STATEMENT:
				return getLoopStatement();
			case AdaPackage.DOCUMENT_ROOT__MACHINE_ATTRIBUTE:
				return getMachineAttribute();
			case AdaPackage.DOCUMENT_ROOT__MACHINE_EMAX_ATTRIBUTE:
				return getMachineEmaxAttribute();
			case AdaPackage.DOCUMENT_ROOT__MACHINE_EMIN_ATTRIBUTE:
				return getMachineEminAttribute();
			case AdaPackage.DOCUMENT_ROOT__MACHINE_MANTISSA_ATTRIBUTE:
				return getMachineMantissaAttribute();
			case AdaPackage.DOCUMENT_ROOT__MACHINE_OVERFLOWS_ATTRIBUTE:
				return getMachineOverflowsAttribute();
			case AdaPackage.DOCUMENT_ROOT__MACHINE_RADIX_ATTRIBUTE:
				return getMachineRadixAttribute();
			case AdaPackage.DOCUMENT_ROOT__MACHINE_ROUNDING_ATTRIBUTE:
				return getMachineRoundingAttribute();
			case AdaPackage.DOCUMENT_ROOT__MACHINE_ROUNDS_ATTRIBUTE:
				return getMachineRoundsAttribute();
			case AdaPackage.DOCUMENT_ROOT__MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE:
				return getMaxAlignmentForAllocationAttribute();
			case AdaPackage.DOCUMENT_ROOT__MAX_ATTRIBUTE:
				return getMaxAttribute();
			case AdaPackage.DOCUMENT_ROOT__MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE:
				return getMaxSizeInStorageElementsAttribute();
			case AdaPackage.DOCUMENT_ROOT__MIN_ATTRIBUTE:
				return getMinAttribute();
			case AdaPackage.DOCUMENT_ROOT__MINUS_OPERATOR:
				return getMinusOperator();
			case AdaPackage.DOCUMENT_ROOT__MOD_ATTRIBUTE:
				return getModAttribute();
			case AdaPackage.DOCUMENT_ROOT__MOD_OPERATOR:
				return getModOperator();
			case AdaPackage.DOCUMENT_ROOT__MODEL_ATTRIBUTE:
				return getModelAttribute();
			case AdaPackage.DOCUMENT_ROOT__MODEL_EMIN_ATTRIBUTE:
				return getModelEminAttribute();
			case AdaPackage.DOCUMENT_ROOT__MODEL_EPSILON_ATTRIBUTE:
				return getModelEpsilonAttribute();
			case AdaPackage.DOCUMENT_ROOT__MODEL_MANTISSA_ATTRIBUTE:
				return getModelMantissaAttribute();
			case AdaPackage.DOCUMENT_ROOT__MODEL_SMALL_ATTRIBUTE:
				return getModelSmallAttribute();
			case AdaPackage.DOCUMENT_ROOT__MODULAR_TYPE_DEFINITION:
				return getModularTypeDefinition();
			case AdaPackage.DOCUMENT_ROOT__MODULUS_ATTRIBUTE:
				return getModulusAttribute();
			case AdaPackage.DOCUMENT_ROOT__MULTIPLY_OPERATOR:
				return getMultiplyOperator();
			case AdaPackage.DOCUMENT_ROOT__NAMED_ARRAY_AGGREGATE:
				return getNamedArrayAggregate();
			case AdaPackage.DOCUMENT_ROOT__NO_RETURN_PRAGMA:
				return getNoReturnPragma();
			case AdaPackage.DOCUMENT_ROOT__NORMALIZE_SCALARS_PRAGMA:
				return getNormalizeScalarsPragma();
			case AdaPackage.DOCUMENT_ROOT__NOT_AN_ELEMENT:
				return getNotAnElement();
			case AdaPackage.DOCUMENT_ROOT__NOT_EQUAL_OPERATOR:
				return getNotEqualOperator();
			case AdaPackage.DOCUMENT_ROOT__NOT_IN_MEMBERSHIP_TEST:
				return getNotInMembershipTest();
			case AdaPackage.DOCUMENT_ROOT__NOT_NULL_RETURN:
				return getNotNullReturn();
			case AdaPackage.DOCUMENT_ROOT__NOT_OPERATOR:
				return getNotOperator();
			case AdaPackage.DOCUMENT_ROOT__NOT_OVERRIDING:
				return getNotOverriding();
			case AdaPackage.DOCUMENT_ROOT__NULL_COMPONENT:
				return getNullComponent();
			case AdaPackage.DOCUMENT_ROOT__NULL_EXCLUSION:
				return getNullExclusion();
			case AdaPackage.DOCUMENT_ROOT__NULL_LITERAL:
				return getNullLiteral();
			case AdaPackage.DOCUMENT_ROOT__NULL_PROCEDURE_DECLARATION:
				return getNullProcedureDeclaration();
			case AdaPackage.DOCUMENT_ROOT__NULL_RECORD_DEFINITION:
				return getNullRecordDefinition();
			case AdaPackage.DOCUMENT_ROOT__NULL_STATEMENT:
				return getNullStatement();
			case AdaPackage.DOCUMENT_ROOT__OBJECT_RENAMING_DECLARATION:
				return getObjectRenamingDeclaration();
			case AdaPackage.DOCUMENT_ROOT__OPTIMIZE_PRAGMA:
				return getOptimizePragma();
			case AdaPackage.DOCUMENT_ROOT__OR_ELSE_SHORT_CIRCUIT:
				return getOrElseShortCircuit();
			case AdaPackage.DOCUMENT_ROOT__OR_OPERATOR:
				return getOrOperator();
			case AdaPackage.DOCUMENT_ROOT__OR_PATH:
				return getOrPath();
			case AdaPackage.DOCUMENT_ROOT__ORDINARY_FIXED_POINT_DEFINITION:
				return getOrdinaryFixedPointDefinition();
			case AdaPackage.DOCUMENT_ROOT__ORDINARY_INTERFACE:
				return getOrdinaryInterface();
			case AdaPackage.DOCUMENT_ROOT__ORDINARY_TYPE_DECLARATION:
				return getOrdinaryTypeDeclaration();
			case AdaPackage.DOCUMENT_ROOT__OTHERS_CHOICE:
				return getOthersChoice();
			case AdaPackage.DOCUMENT_ROOT__OUTPUT_ATTRIBUTE:
				return getOutputAttribute();
			case AdaPackage.DOCUMENT_ROOT__OVERLAPS_STORAGE_ATTRIBUTE:
				return getOverlapsStorageAttribute();
			case AdaPackage.DOCUMENT_ROOT__OVERRIDING:
				return getOverriding();
			case AdaPackage.DOCUMENT_ROOT__PACK_PRAGMA:
				return getPackPragma();
			case AdaPackage.DOCUMENT_ROOT__PACKAGE_BODY_DECLARATION:
				return getPackageBodyDeclaration();
			case AdaPackage.DOCUMENT_ROOT__PACKAGE_BODY_STUB:
				return getPackageBodyStub();
			case AdaPackage.DOCUMENT_ROOT__PACKAGE_DECLARATION:
				return getPackageDeclaration();
			case AdaPackage.DOCUMENT_ROOT__PACKAGE_INSTANTIATION:
				return getPackageInstantiation();
			case AdaPackage.DOCUMENT_ROOT__PACKAGE_RENAMING_DECLARATION:
				return getPackageRenamingDeclaration();
			case AdaPackage.DOCUMENT_ROOT__PAGE_PRAGMA:
				return getPagePragma();
			case AdaPackage.DOCUMENT_ROOT__PARAMETER_ASSOCIATION:
				return getParameterAssociation();
			case AdaPackage.DOCUMENT_ROOT__PARAMETER_SPECIFICATION:
				return getParameterSpecification();
			case AdaPackage.DOCUMENT_ROOT__PARENTHESIZED_EXPRESSION:
				return getParenthesizedExpression();
			case AdaPackage.DOCUMENT_ROOT__PARTITION_ELABORATION_POLICY_PRAGMA:
				return getPartitionElaborationPolicyPragma();
			case AdaPackage.DOCUMENT_ROOT__PARTITION_ID_ATTRIBUTE:
				return getPartitionIdAttribute();
			case AdaPackage.DOCUMENT_ROOT__PLUS_OPERATOR:
				return getPlusOperator();
			case AdaPackage.DOCUMENT_ROOT__POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				return getPoolSpecificAccessToVariable();
			case AdaPackage.DOCUMENT_ROOT__POS_ATTRIBUTE:
				return getPosAttribute();
			case AdaPackage.DOCUMENT_ROOT__POSITION_ATTRIBUTE:
				return getPositionAttribute();
			case AdaPackage.DOCUMENT_ROOT__POSITIONAL_ARRAY_AGGREGATE:
				return getPositionalArrayAggregate();
			case AdaPackage.DOCUMENT_ROOT__PRAGMA_ARGUMENT_ASSOCIATION:
				return getPragmaArgumentAssociation();
			case AdaPackage.DOCUMENT_ROOT__PRED_ATTRIBUTE:
				return getPredAttribute();
			case AdaPackage.DOCUMENT_ROOT__PREELABORABLE_INITIALIZATION_PRAGMA:
				return getPreelaborableInitializationPragma();
			case AdaPackage.DOCUMENT_ROOT__PREELABORATE_PRAGMA:
				return getPreelaboratePragma();
			case AdaPackage.DOCUMENT_ROOT__PRIORITY_ATTRIBUTE:
				return getPriorityAttribute();
			case AdaPackage.DOCUMENT_ROOT__PRIORITY_PRAGMA:
				return getPriorityPragma();
			case AdaPackage.DOCUMENT_ROOT__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return getPrioritySpecificDispatchingPragma();
			case AdaPackage.DOCUMENT_ROOT__PRIVATE:
				return getPrivate();
			case AdaPackage.DOCUMENT_ROOT__PRIVATE_EXTENSION_DECLARATION:
				return getPrivateExtensionDeclaration();
			case AdaPackage.DOCUMENT_ROOT__PRIVATE_EXTENSION_DEFINITION:
				return getPrivateExtensionDefinition();
			case AdaPackage.DOCUMENT_ROOT__PRIVATE_TYPE_DECLARATION:
				return getPrivateTypeDeclaration();
			case AdaPackage.DOCUMENT_ROOT__PRIVATE_TYPE_DEFINITION:
				return getPrivateTypeDefinition();
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_BODY_DECLARATION:
				return getProcedureBodyDeclaration();
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_BODY_STUB:
				return getProcedureBodyStub();
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_CALL_STATEMENT:
				return getProcedureCallStatement();
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_DECLARATION:
				return getProcedureDeclaration();
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_INSTANTIATION:
				return getProcedureInstantiation();
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_RENAMING_DECLARATION:
				return getProcedureRenamingDeclaration();
			case AdaPackage.DOCUMENT_ROOT__PROFILE_PRAGMA:
				return getProfilePragma();
			case AdaPackage.DOCUMENT_ROOT__PROTECTED_BODY_DECLARATION:
				return getProtectedBodyDeclaration();
			case AdaPackage.DOCUMENT_ROOT__PROTECTED_BODY_STUB:
				return getProtectedBodyStub();
			case AdaPackage.DOCUMENT_ROOT__PROTECTED_DEFINITION:
				return getProtectedDefinition();
			case AdaPackage.DOCUMENT_ROOT__PROTECTED_INTERFACE:
				return getProtectedInterface();
			case AdaPackage.DOCUMENT_ROOT__PROTECTED_TYPE_DECLARATION:
				return getProtectedTypeDeclaration();
			case AdaPackage.DOCUMENT_ROOT__PURE_PRAGMA:
				return getPurePragma();
			case AdaPackage.DOCUMENT_ROOT__QUALIFIED_EXPRESSION:
				return getQualifiedExpression();
			case AdaPackage.DOCUMENT_ROOT__QUEUING_POLICY_PRAGMA:
				return getQueuingPolicyPragma();
			case AdaPackage.DOCUMENT_ROOT__RAISE_EXPRESSION:
				return getRaiseExpression();
			case AdaPackage.DOCUMENT_ROOT__RAISE_STATEMENT:
				return getRaiseStatement();
			case AdaPackage.DOCUMENT_ROOT__RANGE_ATTRIBUTE:
				return getRangeAttribute();
			case AdaPackage.DOCUMENT_ROOT__RANGE_ATTRIBUTE_REFERENCE:
				return getRangeAttributeReference();
			case AdaPackage.DOCUMENT_ROOT__READ_ATTRIBUTE:
				return getReadAttribute();
			case AdaPackage.DOCUMENT_ROOT__REAL_LITERAL:
				return getRealLiteral();
			case AdaPackage.DOCUMENT_ROOT__REAL_NUMBER_DECLARATION:
				return getRealNumberDeclaration();
			case AdaPackage.DOCUMENT_ROOT__RECORD_AGGREGATE:
				return getRecordAggregate();
			case AdaPackage.DOCUMENT_ROOT__RECORD_COMPONENT_ASSOCIATION:
				return getRecordComponentAssociation();
			case AdaPackage.DOCUMENT_ROOT__RECORD_DEFINITION:
				return getRecordDefinition();
			case AdaPackage.DOCUMENT_ROOT__RECORD_REPRESENTATION_CLAUSE:
				return getRecordRepresentationClause();
			case AdaPackage.DOCUMENT_ROOT__RECORD_TYPE_DEFINITION:
				return getRecordTypeDefinition();
			case AdaPackage.DOCUMENT_ROOT__RELATIVE_DEADLINE_PRAGMA:
				return getRelativeDeadlinePragma();
			case AdaPackage.DOCUMENT_ROOT__REM_OPERATOR:
				return getRemOperator();
			case AdaPackage.DOCUMENT_ROOT__REMAINDER_ATTRIBUTE:
				return getRemainderAttribute();
			case AdaPackage.DOCUMENT_ROOT__REMOTE_CALL_INTERFACE_PRAGMA:
				return getRemoteCallInterfacePragma();
			case AdaPackage.DOCUMENT_ROOT__REMOTE_TYPES_PRAGMA:
				return getRemoteTypesPragma();
			case AdaPackage.DOCUMENT_ROOT__REQUEUE_STATEMENT:
				return getRequeueStatement();
			case AdaPackage.DOCUMENT_ROOT__REQUEUE_STATEMENT_WITH_ABORT:
				return getRequeueStatementWithAbort();
			case AdaPackage.DOCUMENT_ROOT__RESTRICTIONS_PRAGMA:
				return getRestrictionsPragma();
			case AdaPackage.DOCUMENT_ROOT__RETURN_CONSTANT_SPECIFICATION:
				return getReturnConstantSpecification();
			case AdaPackage.DOCUMENT_ROOT__RETURN_STATEMENT:
				return getReturnStatement();
			case AdaPackage.DOCUMENT_ROOT__RETURN_VARIABLE_SPECIFICATION:
				return getReturnVariableSpecification();
			case AdaPackage.DOCUMENT_ROOT__REVERSE:
				return getReverse();
			case AdaPackage.DOCUMENT_ROOT__REVIEWABLE_PRAGMA:
				return getReviewablePragma();
			case AdaPackage.DOCUMENT_ROOT__ROOT_INTEGER_DEFINITION:
				return getRootIntegerDefinition();
			case AdaPackage.DOCUMENT_ROOT__ROOT_REAL_DEFINITION:
				return getRootRealDefinition();
			case AdaPackage.DOCUMENT_ROOT__ROUND_ATTRIBUTE:
				return getRoundAttribute();
			case AdaPackage.DOCUMENT_ROOT__ROUNDING_ATTRIBUTE:
				return getRoundingAttribute();
			case AdaPackage.DOCUMENT_ROOT__SAFE_FIRST_ATTRIBUTE:
				return getSafeFirstAttribute();
			case AdaPackage.DOCUMENT_ROOT__SAFE_LAST_ATTRIBUTE:
				return getSafeLastAttribute();
			case AdaPackage.DOCUMENT_ROOT__SCALE_ATTRIBUTE:
				return getScaleAttribute();
			case AdaPackage.DOCUMENT_ROOT__SCALING_ATTRIBUTE:
				return getScalingAttribute();
			case AdaPackage.DOCUMENT_ROOT__SELECT_PATH:
				return getSelectPath();
			case AdaPackage.DOCUMENT_ROOT__SELECTED_COMPONENT:
				return getSelectedComponent();
			case AdaPackage.DOCUMENT_ROOT__SELECTIVE_ACCEPT_STATEMENT:
				return getSelectiveAcceptStatement();
			case AdaPackage.DOCUMENT_ROOT__SHARED_PASSIVE_PRAGMA:
				return getSharedPassivePragma();
			case AdaPackage.DOCUMENT_ROOT__SIGNED_INTEGER_TYPE_DEFINITION:
				return getSignedIntegerTypeDefinition();
			case AdaPackage.DOCUMENT_ROOT__SIGNED_ZEROS_ATTRIBUTE:
				return getSignedZerosAttribute();
			case AdaPackage.DOCUMENT_ROOT__SIMPLE_EXPRESSION_RANGE:
				return getSimpleExpressionRange();
			case AdaPackage.DOCUMENT_ROOT__SINGLE_PROTECTED_DECLARATION:
				return getSingleProtectedDeclaration();
			case AdaPackage.DOCUMENT_ROOT__SINGLE_TASK_DECLARATION:
				return getSingleTaskDeclaration();
			case AdaPackage.DOCUMENT_ROOT__SIZE_ATTRIBUTE:
				return getSizeAttribute();
			case AdaPackage.DOCUMENT_ROOT__SLICE:
				return getSlice();
			case AdaPackage.DOCUMENT_ROOT__SMALL_ATTRIBUTE:
				return getSmallAttribute();
			case AdaPackage.DOCUMENT_ROOT__STORAGE_POOL_ATTRIBUTE:
				return getStoragePoolAttribute();
			case AdaPackage.DOCUMENT_ROOT__STORAGE_SIZE_ATTRIBUTE:
				return getStorageSizeAttribute();
			case AdaPackage.DOCUMENT_ROOT__STORAGE_SIZE_PRAGMA:
				return getStorageSizePragma();
			case AdaPackage.DOCUMENT_ROOT__STREAM_SIZE_ATTRIBUTE:
				return getStreamSizeAttribute();
			case AdaPackage.DOCUMENT_ROOT__STRING_LITERAL:
				return getStringLiteral();
			case AdaPackage.DOCUMENT_ROOT__SUBTYPE_DECLARATION:
				return getSubtypeDeclaration();
			case AdaPackage.DOCUMENT_ROOT__SUBTYPE_INDICATION:
				return getSubtypeIndication();
			case AdaPackage.DOCUMENT_ROOT__SUCC_ATTRIBUTE:
				return getSuccAttribute();
			case AdaPackage.DOCUMENT_ROOT__SUPPRESS_PRAGMA:
				return getSuppressPragma();
			case AdaPackage.DOCUMENT_ROOT__SYNCHRONIZED:
				return getSynchronized();
			case AdaPackage.DOCUMENT_ROOT__SYNCHRONIZED_INTERFACE:
				return getSynchronizedInterface();
			case AdaPackage.DOCUMENT_ROOT__TAG_ATTRIBUTE:
				return getTagAttribute();
			case AdaPackage.DOCUMENT_ROOT__TAGGED:
				return getTagged();
			case AdaPackage.DOCUMENT_ROOT__TAGGED_INCOMPLETE_TYPE_DECLARATION:
				return getTaggedIncompleteTypeDeclaration();
			case AdaPackage.DOCUMENT_ROOT__TAGGED_PRIVATE_TYPE_DEFINITION:
				return getTaggedPrivateTypeDefinition();
			case AdaPackage.DOCUMENT_ROOT__TAGGED_RECORD_TYPE_DEFINITION:
				return getTaggedRecordTypeDefinition();
			case AdaPackage.DOCUMENT_ROOT__TASK_BODY_DECLARATION:
				return getTaskBodyDeclaration();
			case AdaPackage.DOCUMENT_ROOT__TASK_BODY_STUB:
				return getTaskBodyStub();
			case AdaPackage.DOCUMENT_ROOT__TASK_DEFINITION:
				return getTaskDefinition();
			case AdaPackage.DOCUMENT_ROOT__TASK_DISPATCHING_POLICY_PRAGMA:
				return getTaskDispatchingPolicyPragma();
			case AdaPackage.DOCUMENT_ROOT__TASK_INTERFACE:
				return getTaskInterface();
			case AdaPackage.DOCUMENT_ROOT__TASK_TYPE_DECLARATION:
				return getTaskTypeDeclaration();
			case AdaPackage.DOCUMENT_ROOT__TERMINATE_ALTERNATIVE_STATEMENT:
				return getTerminateAlternativeStatement();
			case AdaPackage.DOCUMENT_ROOT__TERMINATED_ATTRIBUTE:
				return getTerminatedAttribute();
			case AdaPackage.DOCUMENT_ROOT__THEN_ABORT_PATH:
				return getThenAbortPath();
			case AdaPackage.DOCUMENT_ROOT__TIMED_ENTRY_CALL_STATEMENT:
				return getTimedEntryCallStatement();
			case AdaPackage.DOCUMENT_ROOT__TRUNCATION_ATTRIBUTE:
				return getTruncationAttribute();
			case AdaPackage.DOCUMENT_ROOT__TYPE_CONVERSION:
				return getTypeConversion();
			case AdaPackage.DOCUMENT_ROOT__UNARY_MINUS_OPERATOR:
				return getUnaryMinusOperator();
			case AdaPackage.DOCUMENT_ROOT__UNARY_PLUS_OPERATOR:
				return getUnaryPlusOperator();
			case AdaPackage.DOCUMENT_ROOT__UNBIASED_ROUNDING_ATTRIBUTE:
				return getUnbiasedRoundingAttribute();
			case AdaPackage.DOCUMENT_ROOT__UNCHECKED_ACCESS_ATTRIBUTE:
				return getUncheckedAccessAttribute();
			case AdaPackage.DOCUMENT_ROOT__UNCHECKED_UNION_PRAGMA:
				return getUncheckedUnionPragma();
			case AdaPackage.DOCUMENT_ROOT__UNCONSTRAINED_ARRAY_DEFINITION:
				return getUnconstrainedArrayDefinition();
			case AdaPackage.DOCUMENT_ROOT__UNIVERSAL_FIXED_DEFINITION:
				return getUniversalFixedDefinition();
			case AdaPackage.DOCUMENT_ROOT__UNIVERSAL_INTEGER_DEFINITION:
				return getUniversalIntegerDefinition();
			case AdaPackage.DOCUMENT_ROOT__UNIVERSAL_REAL_DEFINITION:
				return getUniversalRealDefinition();
			case AdaPackage.DOCUMENT_ROOT__UNKNOWN_ATTRIBUTE:
				return getUnknownAttribute();
			case AdaPackage.DOCUMENT_ROOT__UNKNOWN_DISCRIMINANT_PART:
				return getUnknownDiscriminantPart();
			case AdaPackage.DOCUMENT_ROOT__UNKNOWN_PRAGMA:
				return getUnknownPragma();
			case AdaPackage.DOCUMENT_ROOT__UNSUPPRESS_PRAGMA:
				return getUnsuppressPragma();
			case AdaPackage.DOCUMENT_ROOT__USE_ALL_TYPE_CLAUSE:
				return getUseAllTypeClause();
			case AdaPackage.DOCUMENT_ROOT__USE_PACKAGE_CLAUSE:
				return getUsePackageClause();
			case AdaPackage.DOCUMENT_ROOT__USE_TYPE_CLAUSE:
				return getUseTypeClause();
			case AdaPackage.DOCUMENT_ROOT__VAL_ATTRIBUTE:
				return getValAttribute();
			case AdaPackage.DOCUMENT_ROOT__VALID_ATTRIBUTE:
				return getValidAttribute();
			case AdaPackage.DOCUMENT_ROOT__VALUE_ATTRIBUTE:
				return getValueAttribute();
			case AdaPackage.DOCUMENT_ROOT__VARIABLE_DECLARATION:
				return getVariableDeclaration();
			case AdaPackage.DOCUMENT_ROOT__VARIANT:
				return getVariant();
			case AdaPackage.DOCUMENT_ROOT__VARIANT_PART:
				return getVariantPart();
			case AdaPackage.DOCUMENT_ROOT__VERSION_ATTRIBUTE:
				return getVersionAttribute();
			case AdaPackage.DOCUMENT_ROOT__VOLATILE_COMPONENTS_PRAGMA:
				return getVolatileComponentsPragma();
			case AdaPackage.DOCUMENT_ROOT__VOLATILE_PRAGMA:
				return getVolatilePragma();
			case AdaPackage.DOCUMENT_ROOT__WHILE_LOOP_STATEMENT:
				return getWhileLoopStatement();
			case AdaPackage.DOCUMENT_ROOT__WIDE_IMAGE_ATTRIBUTE:
				return getWideImageAttribute();
			case AdaPackage.DOCUMENT_ROOT__WIDE_VALUE_ATTRIBUTE:
				return getWideValueAttribute();
			case AdaPackage.DOCUMENT_ROOT__WIDE_WIDE_IMAGE_ATTRIBUTE:
				return getWideWideImageAttribute();
			case AdaPackage.DOCUMENT_ROOT__WIDE_WIDE_VALUE_ATTRIBUTE:
				return getWideWideValueAttribute();
			case AdaPackage.DOCUMENT_ROOT__WIDE_WIDE_WIDTH_ATTRIBUTE:
				return getWideWideWidthAttribute();
			case AdaPackage.DOCUMENT_ROOT__WIDE_WIDTH_ATTRIBUTE:
				return getWideWidthAttribute();
			case AdaPackage.DOCUMENT_ROOT__WIDTH_ATTRIBUTE:
				return getWidthAttribute();
			case AdaPackage.DOCUMENT_ROOT__WITH_CLAUSE:
				return getWithClause();
			case AdaPackage.DOCUMENT_ROOT__WRITE_ATTRIBUTE:
				return getWriteAttribute();
			case AdaPackage.DOCUMENT_ROOT__XOR_OPERATOR:
				return getXorOperator();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.DOCUMENT_ROOT__MIXED:
				((FeatureMap.Internal)getMixed()).set(newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				((EStructuralFeature.Setting)getXMLNSPrefixMap()).set(newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				((EStructuralFeature.Setting)getXSISchemaLocation()).set(newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ABORT_STATEMENT:
				setAbortStatement((AbortStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ABS_OPERATOR:
				setAbsOperator((AbsOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ABSTRACT:
				setAbstract((Abstract)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ACCEPT_STATEMENT:
				setAcceptStatement((AcceptStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ACCESS_ATTRIBUTE:
				setAccessAttribute((AccessAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_CONSTANT:
				setAccessToConstant((AccessToConstant)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_FUNCTION:
				setAccessToFunction((AccessToFunction)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_PROCEDURE:
				setAccessToProcedure((AccessToProcedure)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_PROTECTED_FUNCTION:
				setAccessToProtectedFunction((AccessToProtectedFunction)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_PROTECTED_PROCEDURE:
				setAccessToProtectedProcedure((AccessToProtectedProcedure)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_VARIABLE:
				setAccessToVariable((AccessToVariable)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ADDRESS_ATTRIBUTE:
				setAddressAttribute((AddressAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ADJACENT_ATTRIBUTE:
				setAdjacentAttribute((AdjacentAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__AFT_ATTRIBUTE:
				setAftAttribute((AftAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ALIASED:
				setAliased((Aliased)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ALIGNMENT_ATTRIBUTE:
				setAlignmentAttribute((AlignmentAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ALL_CALLS_REMOTE_PRAGMA:
				setAllCallsRemotePragma((AllCallsRemotePragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ALLOCATION_FROM_QUALIFIED_EXPRESSION:
				setAllocationFromQualifiedExpression((AllocationFromQualifiedExpression)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ALLOCATION_FROM_SUBTYPE:
				setAllocationFromSubtype((AllocationFromSubtype)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__AND_OPERATOR:
				setAndOperator((AndOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__AND_THEN_SHORT_CIRCUIT:
				setAndThenShortCircuit((AndThenShortCircuit)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_CONSTANT:
				setAnonymousAccessToConstant((AnonymousAccessToConstant)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_FUNCTION:
				setAnonymousAccessToFunction((AnonymousAccessToFunction)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_PROCEDURE:
				setAnonymousAccessToProcedure((AnonymousAccessToProcedure)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION:
				setAnonymousAccessToProtectedFunction((AnonymousAccessToProtectedFunction)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE:
				setAnonymousAccessToProtectedProcedure((AnonymousAccessToProtectedProcedure)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_VARIABLE:
				setAnonymousAccessToVariable((AnonymousAccessToVariable)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ARRAY_COMPONENT_ASSOCIATION:
				setArrayComponentAssociation((ArrayComponentAssociation)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ASPECT_SPECIFICATION:
				setAspectSpecification((AspectSpecification)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ASSERT_PRAGMA:
				setAssertPragma((AssertPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ASSERTION_POLICY_PRAGMA:
				setAssertionPolicyPragma((AssertionPolicyPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ASSIGNMENT_STATEMENT:
				setAssignmentStatement((AssignmentStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ASYNCHRONOUS_PRAGMA:
				setAsynchronousPragma((AsynchronousPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ASYNCHRONOUS_SELECT_STATEMENT:
				setAsynchronousSelectStatement((AsynchronousSelectStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__AT_CLAUSE:
				setAtClause((AtClause)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ATOMIC_COMPONENTS_PRAGMA:
				setAtomicComponentsPragma((AtomicComponentsPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ATOMIC_PRAGMA:
				setAtomicPragma((AtomicPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ATTACH_HANDLER_PRAGMA:
				setAttachHandlerPragma((AttachHandlerPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ATTRIBUTE_DEFINITION_CLAUSE:
				setAttributeDefinitionClause((AttributeDefinitionClause)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__BASE_ATTRIBUTE:
				setBaseAttribute((BaseAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__BIT_ORDER_ATTRIBUTE:
				setBitOrderAttribute((BitOrderAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__BLOCK_STATEMENT:
				setBlockStatement((BlockStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__BODY_VERSION_ATTRIBUTE:
				setBodyVersionAttribute((BodyVersionAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__BOX_EXPRESSION:
				setBoxExpression((BoxExpression)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__CALLABLE_ATTRIBUTE:
				setCallableAttribute((CallableAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__CALLER_ATTRIBUTE:
				setCallerAttribute((CallerAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__CASE_EXPRESSION:
				setCaseExpression((CaseExpression)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__CASE_EXPRESSION_PATH:
				setCaseExpressionPath((CaseExpressionPath)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__CASE_PATH:
				setCasePath((CasePath)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__CASE_STATEMENT:
				setCaseStatement((CaseStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__CEILING_ATTRIBUTE:
				setCeilingAttribute((CeilingAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__CHARACTER_LITERAL:
				setCharacterLiteral((CharacterLiteral)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__CHOICE_PARAMETER_SPECIFICATION:
				setChoiceParameterSpecification((ChoiceParameterSpecification)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__CLASS_ATTRIBUTE:
				setClassAttribute((ClassAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__CODE_STATEMENT:
				setCodeStatement((CodeStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__COMMENT:
				setComment((Comment)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__COMPILATION_UNIT:
				setCompilationUnit((CompilationUnit)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__COMPONENT_CLAUSE:
				setComponentClause((ComponentClause)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__COMPONENT_DECLARATION:
				setComponentDeclaration((ComponentDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__COMPONENT_DEFINITION:
				setComponentDefinition((ComponentDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__COMPONENT_SIZE_ATTRIBUTE:
				setComponentSizeAttribute((ComponentSizeAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__COMPOSE_ATTRIBUTE:
				setComposeAttribute((ComposeAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__CONCATENATE_OPERATOR:
				setConcatenateOperator((ConcatenateOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__CONDITIONAL_ENTRY_CALL_STATEMENT:
				setConditionalEntryCallStatement((ConditionalEntryCallStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__CONSTANT_DECLARATION:
				setConstantDeclaration((ConstantDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__CONSTRAINED_ARRAY_DEFINITION:
				setConstrainedArrayDefinition((ConstrainedArrayDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__CONSTRAINED_ATTRIBUTE:
				setConstrainedAttribute((ConstrainedAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__CONTROLLED_PRAGMA:
				setControlledPragma((ControlledPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__CONVENTION_PRAGMA:
				setConventionPragma((ConventionPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__COPY_SIGN_ATTRIBUTE:
				setCopySignAttribute((CopySignAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__COUNT_ATTRIBUTE:
				setCountAttribute((CountAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__CPU_PRAGMA:
				setCpuPragma((CpuPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DECIMAL_FIXED_POINT_DEFINITION:
				setDecimalFixedPointDefinition((DecimalFixedPointDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFAULT_STORAGE_POOL_PRAGMA:
				setDefaultStoragePoolPragma((DefaultStoragePoolPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFERRED_CONSTANT_DECLARATION:
				setDeferredConstantDeclaration((DeferredConstantDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_ABS_OPERATOR:
				setDefiningAbsOperator((DefiningAbsOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_AND_OPERATOR:
				setDefiningAndOperator((DefiningAndOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_CHARACTER_LITERAL:
				setDefiningCharacterLiteral((DefiningCharacterLiteral)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_CONCATENATE_OPERATOR:
				setDefiningConcatenateOperator((DefiningConcatenateOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_DIVIDE_OPERATOR:
				setDefiningDivideOperator((DefiningDivideOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_ENUMERATION_LITERAL:
				setDefiningEnumerationLiteral((DefiningEnumerationLiteral)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_EQUAL_OPERATOR:
				setDefiningEqualOperator((DefiningEqualOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_EXPANDED_NAME:
				setDefiningExpandedName((DefiningExpandedName)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_EXPONENTIATE_OPERATOR:
				setDefiningExponentiateOperator((DefiningExponentiateOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_GREATER_THAN_OPERATOR:
				setDefiningGreaterThanOperator((DefiningGreaterThanOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_GREATER_THAN_OR_EQUAL_OPERATOR:
				setDefiningGreaterThanOrEqualOperator((DefiningGreaterThanOrEqualOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_IDENTIFIER:
				setDefiningIdentifier((DefiningIdentifier)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_LESS_THAN_OPERATOR:
				setDefiningLessThanOperator((DefiningLessThanOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_LESS_THAN_OR_EQUAL_OPERATOR:
				setDefiningLessThanOrEqualOperator((DefiningLessThanOrEqualOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_MINUS_OPERATOR:
				setDefiningMinusOperator((DefiningMinusOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_MOD_OPERATOR:
				setDefiningModOperator((DefiningModOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_MULTIPLY_OPERATOR:
				setDefiningMultiplyOperator((DefiningMultiplyOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_NOT_EQUAL_OPERATOR:
				setDefiningNotEqualOperator((DefiningNotEqualOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_NOT_OPERATOR:
				setDefiningNotOperator((DefiningNotOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_OR_OPERATOR:
				setDefiningOrOperator((DefiningOrOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_PLUS_OPERATOR:
				setDefiningPlusOperator((DefiningPlusOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_REM_OPERATOR:
				setDefiningRemOperator((DefiningRemOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_UNARY_MINUS_OPERATOR:
				setDefiningUnaryMinusOperator((DefiningUnaryMinusOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_UNARY_PLUS_OPERATOR:
				setDefiningUnaryPlusOperator((DefiningUnaryPlusOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_XOR_OPERATOR:
				setDefiningXorOperator((DefiningXorOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINITE_ATTRIBUTE:
				setDefiniteAttribute((DefiniteAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DELAY_RELATIVE_STATEMENT:
				setDelayRelativeStatement((DelayRelativeStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DELAY_UNTIL_STATEMENT:
				setDelayUntilStatement((DelayUntilStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DELTA_ATTRIBUTE:
				setDeltaAttribute((DeltaAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DELTA_CONSTRAINT:
				setDeltaConstraint((DeltaConstraint)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DENORM_ATTRIBUTE:
				setDenormAttribute((DenormAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DERIVED_RECORD_EXTENSION_DEFINITION:
				setDerivedRecordExtensionDefinition((DerivedRecordExtensionDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DERIVED_TYPE_DEFINITION:
				setDerivedTypeDefinition((DerivedTypeDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DETECT_BLOCKING_PRAGMA:
				setDetectBlockingPragma((DetectBlockingPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DIGITS_ATTRIBUTE:
				setDigitsAttribute((DigitsAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DIGITS_CONSTRAINT:
				setDigitsConstraint((DigitsConstraint)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DISCARD_NAMES_PRAGMA:
				setDiscardNamesPragma((DiscardNamesPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_RANGE_ATTRIBUTE_REFERENCE:
				setDiscreteRangeAttributeReference((DiscreteRangeAttributeReference)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION:
				setDiscreteRangeAttributeReferenceAsSubtypeDefinition((DiscreteRangeAttributeReferenceAsSubtypeDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_SIMPLE_EXPRESSION_RANGE:
				setDiscreteSimpleExpressionRange((DiscreteSimpleExpressionRange)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION:
				setDiscreteSimpleExpressionRangeAsSubtypeDefinition((DiscreteSimpleExpressionRangeAsSubtypeDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_SUBTYPE_INDICATION:
				setDiscreteSubtypeIndication((DiscreteSubtypeIndication)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION:
				setDiscreteSubtypeIndicationAsSubtypeDefinition((DiscreteSubtypeIndicationAsSubtypeDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DISCRIMINANT_ASSOCIATION:
				setDiscriminantAssociation((DiscriminantAssociation)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DISCRIMINANT_CONSTRAINT:
				setDiscriminantConstraint((DiscriminantConstraint)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DISCRIMINANT_SPECIFICATION:
				setDiscriminantSpecification((DiscriminantSpecification)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DISPATCHING_DOMAIN_PRAGMA:
				setDispatchingDomainPragma((DispatchingDomainPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__DIVIDE_OPERATOR:
				setDivideOperator((DivideOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ELABORATE_ALL_PRAGMA:
				setElaborateAllPragma((ElaborateAllPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ELABORATE_BODY_PRAGMA:
				setElaborateBodyPragma((ElaborateBodyPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ELABORATE_PRAGMA:
				setElaboratePragma((ElaboratePragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ELEMENT_ITERATOR_SPECIFICATION:
				setElementIteratorSpecification((ElementIteratorSpecification)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ELSE_EXPRESSION_PATH:
				setElseExpressionPath((ElseExpressionPath)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ELSE_PATH:
				setElsePath((ElsePath)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ELSIF_EXPRESSION_PATH:
				setElsifExpressionPath((ElsifExpressionPath)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ELSIF_PATH:
				setElsifPath((ElsifPath)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ENTRY_BODY_DECLARATION:
				setEntryBodyDeclaration((EntryBodyDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ENTRY_CALL_STATEMENT:
				setEntryCallStatement((EntryCallStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ENTRY_DECLARATION:
				setEntryDeclaration((EntryDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ENTRY_INDEX_SPECIFICATION:
				setEntryIndexSpecification((EntryIndexSpecification)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ENUMERATION_LITERAL:
				setEnumerationLiteral((EnumerationLiteral)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ENUMERATION_LITERAL_SPECIFICATION:
				setEnumerationLiteralSpecification((EnumerationLiteralSpecification)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ENUMERATION_REPRESENTATION_CLAUSE:
				setEnumerationRepresentationClause((EnumerationRepresentationClause)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ENUMERATION_TYPE_DEFINITION:
				setEnumerationTypeDefinition((EnumerationTypeDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__EQUAL_OPERATOR:
				setEqualOperator((EqualOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__EXCEPTION_DECLARATION:
				setExceptionDeclaration((ExceptionDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__EXCEPTION_HANDLER:
				setExceptionHandler((ExceptionHandler)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__EXCEPTION_RENAMING_DECLARATION:
				setExceptionRenamingDeclaration((ExceptionRenamingDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__EXIT_STATEMENT:
				setExitStatement((ExitStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__EXPLICIT_DEREFERENCE:
				setExplicitDereference((ExplicitDereference)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__EXPONENT_ATTRIBUTE:
				setExponentAttribute((ExponentAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__EXPONENTIATE_OPERATOR:
				setExponentiateOperator((ExponentiateOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__EXPORT_PRAGMA:
				setExportPragma((ExportPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__EXPRESSION_FUNCTION_DECLARATION:
				setExpressionFunctionDeclaration((ExpressionFunctionDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__EXTENDED_RETURN_STATEMENT:
				setExtendedReturnStatement((ExtendedReturnStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__EXTENSION_AGGREGATE:
				setExtensionAggregate((ExtensionAggregate)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__EXTERNAL_TAG_ATTRIBUTE:
				setExternalTagAttribute((ExternalTagAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FIRST_ATTRIBUTE:
				setFirstAttribute((FirstAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FIRST_BIT_ATTRIBUTE:
				setFirstBitAttribute((FirstBitAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FLOATING_POINT_DEFINITION:
				setFloatingPointDefinition((FloatingPointDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FLOOR_ATTRIBUTE:
				setFloorAttribute((FloorAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FOR_ALL_QUANTIFIED_EXPRESSION:
				setForAllQuantifiedExpression((ForAllQuantifiedExpression)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FOR_LOOP_STATEMENT:
				setForLoopStatement((ForLoopStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FOR_SOME_QUANTIFIED_EXPRESSION:
				setForSomeQuantifiedExpression((ForSomeQuantifiedExpression)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORE_ATTRIBUTE:
				setForeAttribute((ForeAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_CONSTANT:
				setFormalAccessToConstant((FormalAccessToConstant)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_FUNCTION:
				setFormalAccessToFunction((FormalAccessToFunction)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_PROCEDURE:
				setFormalAccessToProcedure((FormalAccessToProcedure)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_PROTECTED_FUNCTION:
				setFormalAccessToProtectedFunction((FormalAccessToProtectedFunction)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_PROTECTED_PROCEDURE:
				setFormalAccessToProtectedProcedure((FormalAccessToProtectedProcedure)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_VARIABLE:
				setFormalAccessToVariable((FormalAccessToVariable)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_CONSTRAINED_ARRAY_DEFINITION:
				setFormalConstrainedArrayDefinition((FormalConstrainedArrayDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_DECIMAL_FIXED_POINT_DEFINITION:
				setFormalDecimalFixedPointDefinition((FormalDecimalFixedPointDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_DERIVED_TYPE_DEFINITION:
				setFormalDerivedTypeDefinition((FormalDerivedTypeDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_DISCRETE_TYPE_DEFINITION:
				setFormalDiscreteTypeDefinition((FormalDiscreteTypeDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_FLOATING_POINT_DEFINITION:
				setFormalFloatingPointDefinition((FormalFloatingPointDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_FUNCTION_DECLARATION:
				setFormalFunctionDeclaration((FormalFunctionDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_INCOMPLETE_TYPE_DECLARATION:
				setFormalIncompleteTypeDeclaration((FormalIncompleteTypeDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_LIMITED_INTERFACE:
				setFormalLimitedInterface((FormalLimitedInterface)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_MODULAR_TYPE_DEFINITION:
				setFormalModularTypeDefinition((FormalModularTypeDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_OBJECT_DECLARATION:
				setFormalObjectDeclaration((FormalObjectDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ORDINARY_FIXED_POINT_DEFINITION:
				setFormalOrdinaryFixedPointDefinition((FormalOrdinaryFixedPointDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ORDINARY_INTERFACE:
				setFormalOrdinaryInterface((FormalOrdinaryInterface)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_PACKAGE_DECLARATION:
				setFormalPackageDeclaration((FormalPackageDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_PACKAGE_DECLARATION_WITH_BOX:
				setFormalPackageDeclarationWithBox((FormalPackageDeclarationWithBox)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				setFormalPoolSpecificAccessToVariable((FormalPoolSpecificAccessToVariable)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_PRIVATE_TYPE_DEFINITION:
				setFormalPrivateTypeDefinition((FormalPrivateTypeDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_PROCEDURE_DECLARATION:
				setFormalProcedureDeclaration((FormalProcedureDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_PROTECTED_INTERFACE:
				setFormalProtectedInterface((FormalProtectedInterface)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_SIGNED_INTEGER_TYPE_DEFINITION:
				setFormalSignedIntegerTypeDefinition((FormalSignedIntegerTypeDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_SYNCHRONIZED_INTERFACE:
				setFormalSynchronizedInterface((FormalSynchronizedInterface)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION:
				setFormalTaggedPrivateTypeDefinition((FormalTaggedPrivateTypeDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_TASK_INTERFACE:
				setFormalTaskInterface((FormalTaskInterface)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_TYPE_DECLARATION:
				setFormalTypeDeclaration((FormalTypeDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_UNCONSTRAINED_ARRAY_DEFINITION:
				setFormalUnconstrainedArrayDefinition((FormalUnconstrainedArrayDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FRACTION_ATTRIBUTE:
				setFractionAttribute((FractionAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_BODY_DECLARATION:
				setFunctionBodyDeclaration((FunctionBodyDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_BODY_STUB:
				setFunctionBodyStub((FunctionBodyStub)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_CALL:
				setFunctionCall((FunctionCall)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_DECLARATION:
				setFunctionDeclaration((FunctionDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_INSTANTIATION:
				setFunctionInstantiation((FunctionInstantiation)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_RENAMING_DECLARATION:
				setFunctionRenamingDeclaration((FunctionRenamingDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__GENERALIZED_ITERATOR_SPECIFICATION:
				setGeneralizedIteratorSpecification((GeneralizedIteratorSpecification)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__GENERIC_ASSOCIATION:
				setGenericAssociation((GenericAssociation)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__GENERIC_FUNCTION_DECLARATION:
				setGenericFunctionDeclaration((GenericFunctionDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__GENERIC_FUNCTION_RENAMING_DECLARATION:
				setGenericFunctionRenamingDeclaration((GenericFunctionRenamingDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__GENERIC_PACKAGE_DECLARATION:
				setGenericPackageDeclaration((GenericPackageDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__GENERIC_PACKAGE_RENAMING_DECLARATION:
				setGenericPackageRenamingDeclaration((GenericPackageRenamingDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__GENERIC_PROCEDURE_DECLARATION:
				setGenericProcedureDeclaration((GenericProcedureDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__GENERIC_PROCEDURE_RENAMING_DECLARATION:
				setGenericProcedureRenamingDeclaration((GenericProcedureRenamingDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__GOTO_STATEMENT:
				setGotoStatement((GotoStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__GREATER_THAN_OPERATOR:
				setGreaterThanOperator((GreaterThanOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__GREATER_THAN_OR_EQUAL_OPERATOR:
				setGreaterThanOrEqualOperator((GreaterThanOrEqualOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__IDENTIFIER:
				setIdentifier((Identifier)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__IDENTITY_ATTRIBUTE:
				setIdentityAttribute((IdentityAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__IF_EXPRESSION:
				setIfExpression((IfExpression)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__IF_EXPRESSION_PATH:
				setIfExpressionPath((IfExpressionPath)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__IF_PATH:
				setIfPath((IfPath)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__IF_STATEMENT:
				setIfStatement((IfStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__IMAGE_ATTRIBUTE:
				setImageAttribute((ImageAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__IMPLEMENTATION_DEFINED_ATTRIBUTE:
				setImplementationDefinedAttribute((ImplementationDefinedAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__IMPLEMENTATION_DEFINED_PRAGMA:
				setImplementationDefinedPragma((ImplementationDefinedPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__IMPORT_PRAGMA:
				setImportPragma((ImportPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__IN_MEMBERSHIP_TEST:
				setInMembershipTest((InMembershipTest)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__INCOMPLETE_TYPE_DECLARATION:
				setIncompleteTypeDeclaration((IncompleteTypeDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__INDEPENDENT_COMPONENTS_PRAGMA:
				setIndependentComponentsPragma((IndependentComponentsPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__INDEPENDENT_PRAGMA:
				setIndependentPragma((IndependentPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__INDEX_CONSTRAINT:
				setIndexConstraint((IndexConstraint)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__INDEXED_COMPONENT:
				setIndexedComponent((IndexedComponent)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__INLINE_PRAGMA:
				setInlinePragma((InlinePragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__INPUT_ATTRIBUTE:
				setInputAttribute((InputAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__INSPECTION_POINT_PRAGMA:
				setInspectionPointPragma((InspectionPointPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__INTEGER_LITERAL:
				setIntegerLiteral((IntegerLiteral)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__INTEGER_NUMBER_DECLARATION:
				setIntegerNumberDeclaration((IntegerNumberDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__INTERRUPT_HANDLER_PRAGMA:
				setInterruptHandlerPragma((InterruptHandlerPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__INTERRUPT_PRIORITY_PRAGMA:
				setInterruptPriorityPragma((InterruptPriorityPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__IS_PREFIX_CALL:
				setIsPrefixCall((IsPrefixCall)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__IS_PREFIX_NOTATION:
				setIsPrefixNotation((IsPrefixNotation)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__KNOWN_DISCRIMINANT_PART:
				setKnownDiscriminantPart((KnownDiscriminantPart)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__LAST_ATTRIBUTE:
				setLastAttribute((LastAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__LAST_BIT_ATTRIBUTE:
				setLastBitAttribute((LastBitAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__LEADING_PART_ATTRIBUTE:
				setLeadingPartAttribute((LeadingPartAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__LENGTH_ATTRIBUTE:
				setLengthAttribute((LengthAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__LESS_THAN_OPERATOR:
				setLessThanOperator((LessThanOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__LESS_THAN_OR_EQUAL_OPERATOR:
				setLessThanOrEqualOperator((LessThanOrEqualOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__LIMITED:
				setLimited((Limited)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__LIMITED_INTERFACE:
				setLimitedInterface((LimitedInterface)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__LINKER_OPTIONS_PRAGMA:
				setLinkerOptionsPragma((LinkerOptionsPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__LIST_PRAGMA:
				setListPragma((ListPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__LOCKING_POLICY_PRAGMA:
				setLockingPolicyPragma((LockingPolicyPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__LOOP_PARAMETER_SPECIFICATION:
				setLoopParameterSpecification((LoopParameterSpecification)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__LOOP_STATEMENT:
				setLoopStatement((LoopStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__MACHINE_ATTRIBUTE:
				setMachineAttribute((MachineAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__MACHINE_EMAX_ATTRIBUTE:
				setMachineEmaxAttribute((MachineEmaxAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__MACHINE_EMIN_ATTRIBUTE:
				setMachineEminAttribute((MachineEminAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__MACHINE_MANTISSA_ATTRIBUTE:
				setMachineMantissaAttribute((MachineMantissaAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__MACHINE_OVERFLOWS_ATTRIBUTE:
				setMachineOverflowsAttribute((MachineOverflowsAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__MACHINE_RADIX_ATTRIBUTE:
				setMachineRadixAttribute((MachineRadixAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__MACHINE_ROUNDING_ATTRIBUTE:
				setMachineRoundingAttribute((MachineRoundingAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__MACHINE_ROUNDS_ATTRIBUTE:
				setMachineRoundsAttribute((MachineRoundsAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE:
				setMaxAlignmentForAllocationAttribute((MaxAlignmentForAllocationAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__MAX_ATTRIBUTE:
				setMaxAttribute((MaxAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE:
				setMaxSizeInStorageElementsAttribute((MaxSizeInStorageElementsAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__MIN_ATTRIBUTE:
				setMinAttribute((MinAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__MINUS_OPERATOR:
				setMinusOperator((MinusOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__MOD_ATTRIBUTE:
				setModAttribute((ModAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__MOD_OPERATOR:
				setModOperator((ModOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__MODEL_ATTRIBUTE:
				setModelAttribute((ModelAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__MODEL_EMIN_ATTRIBUTE:
				setModelEminAttribute((ModelEminAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__MODEL_EPSILON_ATTRIBUTE:
				setModelEpsilonAttribute((ModelEpsilonAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__MODEL_MANTISSA_ATTRIBUTE:
				setModelMantissaAttribute((ModelMantissaAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__MODEL_SMALL_ATTRIBUTE:
				setModelSmallAttribute((ModelSmallAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__MODULAR_TYPE_DEFINITION:
				setModularTypeDefinition((ModularTypeDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__MODULUS_ATTRIBUTE:
				setModulusAttribute((ModulusAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__MULTIPLY_OPERATOR:
				setMultiplyOperator((MultiplyOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__NAMED_ARRAY_AGGREGATE:
				setNamedArrayAggregate((NamedArrayAggregate)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__NO_RETURN_PRAGMA:
				setNoReturnPragma((NoReturnPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__NORMALIZE_SCALARS_PRAGMA:
				setNormalizeScalarsPragma((NormalizeScalarsPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__NOT_EQUAL_OPERATOR:
				setNotEqualOperator((NotEqualOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__NOT_IN_MEMBERSHIP_TEST:
				setNotInMembershipTest((NotInMembershipTest)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__NOT_NULL_RETURN:
				setNotNullReturn((NotNullReturn)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__NOT_OPERATOR:
				setNotOperator((NotOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__NOT_OVERRIDING:
				setNotOverriding((NotOverriding)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__NULL_COMPONENT:
				setNullComponent((NullComponent)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__NULL_EXCLUSION:
				setNullExclusion((NullExclusion)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__NULL_LITERAL:
				setNullLiteral((NullLiteral)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__NULL_PROCEDURE_DECLARATION:
				setNullProcedureDeclaration((NullProcedureDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__NULL_RECORD_DEFINITION:
				setNullRecordDefinition((NullRecordDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__NULL_STATEMENT:
				setNullStatement((NullStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__OBJECT_RENAMING_DECLARATION:
				setObjectRenamingDeclaration((ObjectRenamingDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__OPTIMIZE_PRAGMA:
				setOptimizePragma((OptimizePragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__OR_ELSE_SHORT_CIRCUIT:
				setOrElseShortCircuit((OrElseShortCircuit)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__OR_OPERATOR:
				setOrOperator((OrOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__OR_PATH:
				setOrPath((OrPath)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ORDINARY_FIXED_POINT_DEFINITION:
				setOrdinaryFixedPointDefinition((OrdinaryFixedPointDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ORDINARY_INTERFACE:
				setOrdinaryInterface((OrdinaryInterface)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ORDINARY_TYPE_DECLARATION:
				setOrdinaryTypeDeclaration((OrdinaryTypeDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__OTHERS_CHOICE:
				setOthersChoice((OthersChoice)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__OUTPUT_ATTRIBUTE:
				setOutputAttribute((OutputAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__OVERLAPS_STORAGE_ATTRIBUTE:
				setOverlapsStorageAttribute((OverlapsStorageAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__OVERRIDING:
				setOverriding((Overriding)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PACK_PRAGMA:
				setPackPragma((PackPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PACKAGE_BODY_DECLARATION:
				setPackageBodyDeclaration((PackageBodyDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PACKAGE_BODY_STUB:
				setPackageBodyStub((PackageBodyStub)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PACKAGE_DECLARATION:
				setPackageDeclaration((PackageDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PACKAGE_INSTANTIATION:
				setPackageInstantiation((PackageInstantiation)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PACKAGE_RENAMING_DECLARATION:
				setPackageRenamingDeclaration((PackageRenamingDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PAGE_PRAGMA:
				setPagePragma((PagePragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PARAMETER_ASSOCIATION:
				setParameterAssociation((ParameterAssociation)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PARAMETER_SPECIFICATION:
				setParameterSpecification((ParameterSpecification)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PARENTHESIZED_EXPRESSION:
				setParenthesizedExpression((ParenthesizedExpression)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PARTITION_ELABORATION_POLICY_PRAGMA:
				setPartitionElaborationPolicyPragma((PartitionElaborationPolicyPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PARTITION_ID_ATTRIBUTE:
				setPartitionIdAttribute((PartitionIdAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PLUS_OPERATOR:
				setPlusOperator((PlusOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				setPoolSpecificAccessToVariable((PoolSpecificAccessToVariable)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__POS_ATTRIBUTE:
				setPosAttribute((PosAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__POSITION_ATTRIBUTE:
				setPositionAttribute((PositionAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__POSITIONAL_ARRAY_AGGREGATE:
				setPositionalArrayAggregate((PositionalArrayAggregate)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PRAGMA_ARGUMENT_ASSOCIATION:
				setPragmaArgumentAssociation((PragmaArgumentAssociation)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PRED_ATTRIBUTE:
				setPredAttribute((PredAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PREELABORABLE_INITIALIZATION_PRAGMA:
				setPreelaborableInitializationPragma((PreelaborableInitializationPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PREELABORATE_PRAGMA:
				setPreelaboratePragma((PreelaboratePragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PRIORITY_ATTRIBUTE:
				setPriorityAttribute((PriorityAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PRIORITY_PRAGMA:
				setPriorityPragma((PriorityPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				setPrioritySpecificDispatchingPragma((PrioritySpecificDispatchingPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PRIVATE:
				setPrivate((Private)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PRIVATE_EXTENSION_DECLARATION:
				setPrivateExtensionDeclaration((PrivateExtensionDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PRIVATE_EXTENSION_DEFINITION:
				setPrivateExtensionDefinition((PrivateExtensionDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PRIVATE_TYPE_DECLARATION:
				setPrivateTypeDeclaration((PrivateTypeDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PRIVATE_TYPE_DEFINITION:
				setPrivateTypeDefinition((PrivateTypeDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_BODY_DECLARATION:
				setProcedureBodyDeclaration((ProcedureBodyDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_BODY_STUB:
				setProcedureBodyStub((ProcedureBodyStub)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_CALL_STATEMENT:
				setProcedureCallStatement((ProcedureCallStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_DECLARATION:
				setProcedureDeclaration((ProcedureDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_INSTANTIATION:
				setProcedureInstantiation((ProcedureInstantiation)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_RENAMING_DECLARATION:
				setProcedureRenamingDeclaration((ProcedureRenamingDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PROFILE_PRAGMA:
				setProfilePragma((ProfilePragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PROTECTED_BODY_DECLARATION:
				setProtectedBodyDeclaration((ProtectedBodyDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PROTECTED_BODY_STUB:
				setProtectedBodyStub((ProtectedBodyStub)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PROTECTED_DEFINITION:
				setProtectedDefinition((ProtectedDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PROTECTED_INTERFACE:
				setProtectedInterface((ProtectedInterface)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PROTECTED_TYPE_DECLARATION:
				setProtectedTypeDeclaration((ProtectedTypeDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__PURE_PRAGMA:
				setPurePragma((PurePragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__QUALIFIED_EXPRESSION:
				setQualifiedExpression((QualifiedExpression)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__QUEUING_POLICY_PRAGMA:
				setQueuingPolicyPragma((QueuingPolicyPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__RAISE_EXPRESSION:
				setRaiseExpression((RaiseExpression)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__RAISE_STATEMENT:
				setRaiseStatement((RaiseStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__RANGE_ATTRIBUTE:
				setRangeAttribute((RangeAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__RANGE_ATTRIBUTE_REFERENCE:
				setRangeAttributeReference((RangeAttributeReference)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__READ_ATTRIBUTE:
				setReadAttribute((ReadAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__REAL_LITERAL:
				setRealLiteral((RealLiteral)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__REAL_NUMBER_DECLARATION:
				setRealNumberDeclaration((RealNumberDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__RECORD_AGGREGATE:
				setRecordAggregate((RecordAggregate)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__RECORD_COMPONENT_ASSOCIATION:
				setRecordComponentAssociation((RecordComponentAssociation)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__RECORD_DEFINITION:
				setRecordDefinition((RecordDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__RECORD_REPRESENTATION_CLAUSE:
				setRecordRepresentationClause((RecordRepresentationClause)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__RECORD_TYPE_DEFINITION:
				setRecordTypeDefinition((RecordTypeDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__RELATIVE_DEADLINE_PRAGMA:
				setRelativeDeadlinePragma((RelativeDeadlinePragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__REM_OPERATOR:
				setRemOperator((RemOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__REMAINDER_ATTRIBUTE:
				setRemainderAttribute((RemainderAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__REMOTE_CALL_INTERFACE_PRAGMA:
				setRemoteCallInterfacePragma((RemoteCallInterfacePragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__REMOTE_TYPES_PRAGMA:
				setRemoteTypesPragma((RemoteTypesPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__REQUEUE_STATEMENT:
				setRequeueStatement((RequeueStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__REQUEUE_STATEMENT_WITH_ABORT:
				setRequeueStatementWithAbort((RequeueStatementWithAbort)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__RESTRICTIONS_PRAGMA:
				setRestrictionsPragma((RestrictionsPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__RETURN_CONSTANT_SPECIFICATION:
				setReturnConstantSpecification((ReturnConstantSpecification)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__RETURN_STATEMENT:
				setReturnStatement((ReturnStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__RETURN_VARIABLE_SPECIFICATION:
				setReturnVariableSpecification((ReturnVariableSpecification)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__REVERSE:
				setReverse((Reverse)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__REVIEWABLE_PRAGMA:
				setReviewablePragma((ReviewablePragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ROOT_INTEGER_DEFINITION:
				setRootIntegerDefinition((RootIntegerDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ROOT_REAL_DEFINITION:
				setRootRealDefinition((RootRealDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ROUND_ATTRIBUTE:
				setRoundAttribute((RoundAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__ROUNDING_ATTRIBUTE:
				setRoundingAttribute((RoundingAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__SAFE_FIRST_ATTRIBUTE:
				setSafeFirstAttribute((SafeFirstAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__SAFE_LAST_ATTRIBUTE:
				setSafeLastAttribute((SafeLastAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__SCALE_ATTRIBUTE:
				setScaleAttribute((ScaleAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__SCALING_ATTRIBUTE:
				setScalingAttribute((ScalingAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__SELECT_PATH:
				setSelectPath((SelectPath)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__SELECTED_COMPONENT:
				setSelectedComponent((SelectedComponent)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__SELECTIVE_ACCEPT_STATEMENT:
				setSelectiveAcceptStatement((SelectiveAcceptStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__SHARED_PASSIVE_PRAGMA:
				setSharedPassivePragma((SharedPassivePragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__SIGNED_INTEGER_TYPE_DEFINITION:
				setSignedIntegerTypeDefinition((SignedIntegerTypeDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__SIGNED_ZEROS_ATTRIBUTE:
				setSignedZerosAttribute((SignedZerosAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__SIMPLE_EXPRESSION_RANGE:
				setSimpleExpressionRange((SimpleExpressionRange)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__SINGLE_PROTECTED_DECLARATION:
				setSingleProtectedDeclaration((SingleProtectedDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__SINGLE_TASK_DECLARATION:
				setSingleTaskDeclaration((SingleTaskDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__SIZE_ATTRIBUTE:
				setSizeAttribute((SizeAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__SLICE:
				setSlice((Slice)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__SMALL_ATTRIBUTE:
				setSmallAttribute((SmallAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__STORAGE_POOL_ATTRIBUTE:
				setStoragePoolAttribute((StoragePoolAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__STORAGE_SIZE_ATTRIBUTE:
				setStorageSizeAttribute((StorageSizeAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__STORAGE_SIZE_PRAGMA:
				setStorageSizePragma((StorageSizePragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__STREAM_SIZE_ATTRIBUTE:
				setStreamSizeAttribute((StreamSizeAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__STRING_LITERAL:
				setStringLiteral((StringLiteral)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__SUBTYPE_DECLARATION:
				setSubtypeDeclaration((SubtypeDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__SUBTYPE_INDICATION:
				setSubtypeIndication((SubtypeIndication)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__SUCC_ATTRIBUTE:
				setSuccAttribute((SuccAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__SUPPRESS_PRAGMA:
				setSuppressPragma((SuppressPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__SYNCHRONIZED:
				setSynchronized((Synchronized)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__SYNCHRONIZED_INTERFACE:
				setSynchronizedInterface((SynchronizedInterface)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__TAG_ATTRIBUTE:
				setTagAttribute((TagAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__TAGGED:
				setTagged((Tagged)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__TAGGED_INCOMPLETE_TYPE_DECLARATION:
				setTaggedIncompleteTypeDeclaration((TaggedIncompleteTypeDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__TAGGED_PRIVATE_TYPE_DEFINITION:
				setTaggedPrivateTypeDefinition((TaggedPrivateTypeDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__TAGGED_RECORD_TYPE_DEFINITION:
				setTaggedRecordTypeDefinition((TaggedRecordTypeDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__TASK_BODY_DECLARATION:
				setTaskBodyDeclaration((TaskBodyDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__TASK_BODY_STUB:
				setTaskBodyStub((TaskBodyStub)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__TASK_DEFINITION:
				setTaskDefinition((TaskDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__TASK_DISPATCHING_POLICY_PRAGMA:
				setTaskDispatchingPolicyPragma((TaskDispatchingPolicyPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__TASK_INTERFACE:
				setTaskInterface((TaskInterface)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__TASK_TYPE_DECLARATION:
				setTaskTypeDeclaration((TaskTypeDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__TERMINATE_ALTERNATIVE_STATEMENT:
				setTerminateAlternativeStatement((TerminateAlternativeStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__TERMINATED_ATTRIBUTE:
				setTerminatedAttribute((TerminatedAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__THEN_ABORT_PATH:
				setThenAbortPath((ThenAbortPath)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__TIMED_ENTRY_CALL_STATEMENT:
				setTimedEntryCallStatement((TimedEntryCallStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__TRUNCATION_ATTRIBUTE:
				setTruncationAttribute((TruncationAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__TYPE_CONVERSION:
				setTypeConversion((TypeConversion)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNARY_MINUS_OPERATOR:
				setUnaryMinusOperator((UnaryMinusOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNARY_PLUS_OPERATOR:
				setUnaryPlusOperator((UnaryPlusOperator)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNBIASED_ROUNDING_ATTRIBUTE:
				setUnbiasedRoundingAttribute((UnbiasedRoundingAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNCHECKED_ACCESS_ATTRIBUTE:
				setUncheckedAccessAttribute((UncheckedAccessAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNCHECKED_UNION_PRAGMA:
				setUncheckedUnionPragma((UncheckedUnionPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNCONSTRAINED_ARRAY_DEFINITION:
				setUnconstrainedArrayDefinition((UnconstrainedArrayDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNIVERSAL_FIXED_DEFINITION:
				setUniversalFixedDefinition((UniversalFixedDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNIVERSAL_INTEGER_DEFINITION:
				setUniversalIntegerDefinition((UniversalIntegerDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNIVERSAL_REAL_DEFINITION:
				setUniversalRealDefinition((UniversalRealDefinition)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNKNOWN_ATTRIBUTE:
				setUnknownAttribute((UnknownAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNKNOWN_DISCRIMINANT_PART:
				setUnknownDiscriminantPart((UnknownDiscriminantPart)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNKNOWN_PRAGMA:
				setUnknownPragma((UnknownPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNSUPPRESS_PRAGMA:
				setUnsuppressPragma((UnsuppressPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__USE_ALL_TYPE_CLAUSE:
				setUseAllTypeClause((UseAllTypeClause)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__USE_PACKAGE_CLAUSE:
				setUsePackageClause((UsePackageClause)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__USE_TYPE_CLAUSE:
				setUseTypeClause((UseTypeClause)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__VAL_ATTRIBUTE:
				setValAttribute((ValAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__VALID_ATTRIBUTE:
				setValidAttribute((ValidAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__VALUE_ATTRIBUTE:
				setValueAttribute((ValueAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__VARIABLE_DECLARATION:
				setVariableDeclaration((VariableDeclaration)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__VARIANT:
				setVariant((Variant)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__VARIANT_PART:
				setVariantPart((VariantPart)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__VERSION_ATTRIBUTE:
				setVersionAttribute((VersionAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__VOLATILE_COMPONENTS_PRAGMA:
				setVolatileComponentsPragma((VolatileComponentsPragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__VOLATILE_PRAGMA:
				setVolatilePragma((VolatilePragma)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__WHILE_LOOP_STATEMENT:
				setWhileLoopStatement((WhileLoopStatement)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__WIDE_IMAGE_ATTRIBUTE:
				setWideImageAttribute((WideImageAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__WIDE_VALUE_ATTRIBUTE:
				setWideValueAttribute((WideValueAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__WIDE_WIDE_IMAGE_ATTRIBUTE:
				setWideWideImageAttribute((WideWideImageAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__WIDE_WIDE_VALUE_ATTRIBUTE:
				setWideWideValueAttribute((WideWideValueAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__WIDE_WIDE_WIDTH_ATTRIBUTE:
				setWideWideWidthAttribute((WideWideWidthAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__WIDE_WIDTH_ATTRIBUTE:
				setWideWidthAttribute((WideWidthAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__WIDTH_ATTRIBUTE:
				setWidthAttribute((WidthAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__WITH_CLAUSE:
				setWithClause((WithClause)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__WRITE_ATTRIBUTE:
				setWriteAttribute((WriteAttribute)newValue);
				return;
			case AdaPackage.DOCUMENT_ROOT__XOR_OPERATOR:
				setXorOperator((XorOperator)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.DOCUMENT_ROOT__MIXED:
				getMixed().clear();
				return;
			case AdaPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				getXMLNSPrefixMap().clear();
				return;
			case AdaPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				getXSISchemaLocation().clear();
				return;
			case AdaPackage.DOCUMENT_ROOT__ABORT_STATEMENT:
				setAbortStatement((AbortStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ABS_OPERATOR:
				setAbsOperator((AbsOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ABSTRACT:
				setAbstract((Abstract)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ACCEPT_STATEMENT:
				setAcceptStatement((AcceptStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ACCESS_ATTRIBUTE:
				setAccessAttribute((AccessAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_CONSTANT:
				setAccessToConstant((AccessToConstant)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_FUNCTION:
				setAccessToFunction((AccessToFunction)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_PROCEDURE:
				setAccessToProcedure((AccessToProcedure)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_PROTECTED_FUNCTION:
				setAccessToProtectedFunction((AccessToProtectedFunction)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_PROTECTED_PROCEDURE:
				setAccessToProtectedProcedure((AccessToProtectedProcedure)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_VARIABLE:
				setAccessToVariable((AccessToVariable)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ADDRESS_ATTRIBUTE:
				setAddressAttribute((AddressAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ADJACENT_ATTRIBUTE:
				setAdjacentAttribute((AdjacentAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__AFT_ATTRIBUTE:
				setAftAttribute((AftAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ALIASED:
				setAliased((Aliased)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ALIGNMENT_ATTRIBUTE:
				setAlignmentAttribute((AlignmentAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ALL_CALLS_REMOTE_PRAGMA:
				setAllCallsRemotePragma((AllCallsRemotePragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ALLOCATION_FROM_QUALIFIED_EXPRESSION:
				setAllocationFromQualifiedExpression((AllocationFromQualifiedExpression)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ALLOCATION_FROM_SUBTYPE:
				setAllocationFromSubtype((AllocationFromSubtype)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__AND_OPERATOR:
				setAndOperator((AndOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__AND_THEN_SHORT_CIRCUIT:
				setAndThenShortCircuit((AndThenShortCircuit)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_CONSTANT:
				setAnonymousAccessToConstant((AnonymousAccessToConstant)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_FUNCTION:
				setAnonymousAccessToFunction((AnonymousAccessToFunction)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_PROCEDURE:
				setAnonymousAccessToProcedure((AnonymousAccessToProcedure)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION:
				setAnonymousAccessToProtectedFunction((AnonymousAccessToProtectedFunction)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE:
				setAnonymousAccessToProtectedProcedure((AnonymousAccessToProtectedProcedure)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_VARIABLE:
				setAnonymousAccessToVariable((AnonymousAccessToVariable)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ARRAY_COMPONENT_ASSOCIATION:
				setArrayComponentAssociation((ArrayComponentAssociation)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ASPECT_SPECIFICATION:
				setAspectSpecification((AspectSpecification)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ASSERT_PRAGMA:
				setAssertPragma((AssertPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ASSERTION_POLICY_PRAGMA:
				setAssertionPolicyPragma((AssertionPolicyPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ASSIGNMENT_STATEMENT:
				setAssignmentStatement((AssignmentStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ASYNCHRONOUS_PRAGMA:
				setAsynchronousPragma((AsynchronousPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ASYNCHRONOUS_SELECT_STATEMENT:
				setAsynchronousSelectStatement((AsynchronousSelectStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__AT_CLAUSE:
				setAtClause((AtClause)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ATOMIC_COMPONENTS_PRAGMA:
				setAtomicComponentsPragma((AtomicComponentsPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ATOMIC_PRAGMA:
				setAtomicPragma((AtomicPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ATTACH_HANDLER_PRAGMA:
				setAttachHandlerPragma((AttachHandlerPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ATTRIBUTE_DEFINITION_CLAUSE:
				setAttributeDefinitionClause((AttributeDefinitionClause)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__BASE_ATTRIBUTE:
				setBaseAttribute((BaseAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__BIT_ORDER_ATTRIBUTE:
				setBitOrderAttribute((BitOrderAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__BLOCK_STATEMENT:
				setBlockStatement((BlockStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__BODY_VERSION_ATTRIBUTE:
				setBodyVersionAttribute((BodyVersionAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__BOX_EXPRESSION:
				setBoxExpression((BoxExpression)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__CALLABLE_ATTRIBUTE:
				setCallableAttribute((CallableAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__CALLER_ATTRIBUTE:
				setCallerAttribute((CallerAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__CASE_EXPRESSION:
				setCaseExpression((CaseExpression)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__CASE_EXPRESSION_PATH:
				setCaseExpressionPath((CaseExpressionPath)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__CASE_PATH:
				setCasePath((CasePath)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__CASE_STATEMENT:
				setCaseStatement((CaseStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__CEILING_ATTRIBUTE:
				setCeilingAttribute((CeilingAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__CHARACTER_LITERAL:
				setCharacterLiteral((CharacterLiteral)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__CHOICE_PARAMETER_SPECIFICATION:
				setChoiceParameterSpecification((ChoiceParameterSpecification)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__CLASS_ATTRIBUTE:
				setClassAttribute((ClassAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__CODE_STATEMENT:
				setCodeStatement((CodeStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__COMMENT:
				setComment((Comment)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__COMPILATION_UNIT:
				setCompilationUnit((CompilationUnit)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__COMPONENT_CLAUSE:
				setComponentClause((ComponentClause)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__COMPONENT_DECLARATION:
				setComponentDeclaration((ComponentDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__COMPONENT_DEFINITION:
				setComponentDefinition((ComponentDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__COMPONENT_SIZE_ATTRIBUTE:
				setComponentSizeAttribute((ComponentSizeAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__COMPOSE_ATTRIBUTE:
				setComposeAttribute((ComposeAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__CONCATENATE_OPERATOR:
				setConcatenateOperator((ConcatenateOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__CONDITIONAL_ENTRY_CALL_STATEMENT:
				setConditionalEntryCallStatement((ConditionalEntryCallStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__CONSTANT_DECLARATION:
				setConstantDeclaration((ConstantDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__CONSTRAINED_ARRAY_DEFINITION:
				setConstrainedArrayDefinition((ConstrainedArrayDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__CONSTRAINED_ATTRIBUTE:
				setConstrainedAttribute((ConstrainedAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__CONTROLLED_PRAGMA:
				setControlledPragma((ControlledPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__CONVENTION_PRAGMA:
				setConventionPragma((ConventionPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__COPY_SIGN_ATTRIBUTE:
				setCopySignAttribute((CopySignAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__COUNT_ATTRIBUTE:
				setCountAttribute((CountAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__CPU_PRAGMA:
				setCpuPragma((CpuPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DECIMAL_FIXED_POINT_DEFINITION:
				setDecimalFixedPointDefinition((DecimalFixedPointDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFAULT_STORAGE_POOL_PRAGMA:
				setDefaultStoragePoolPragma((DefaultStoragePoolPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFERRED_CONSTANT_DECLARATION:
				setDeferredConstantDeclaration((DeferredConstantDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_ABS_OPERATOR:
				setDefiningAbsOperator((DefiningAbsOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_AND_OPERATOR:
				setDefiningAndOperator((DefiningAndOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_CHARACTER_LITERAL:
				setDefiningCharacterLiteral((DefiningCharacterLiteral)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_CONCATENATE_OPERATOR:
				setDefiningConcatenateOperator((DefiningConcatenateOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_DIVIDE_OPERATOR:
				setDefiningDivideOperator((DefiningDivideOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_ENUMERATION_LITERAL:
				setDefiningEnumerationLiteral((DefiningEnumerationLiteral)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_EQUAL_OPERATOR:
				setDefiningEqualOperator((DefiningEqualOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_EXPANDED_NAME:
				setDefiningExpandedName((DefiningExpandedName)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_EXPONENTIATE_OPERATOR:
				setDefiningExponentiateOperator((DefiningExponentiateOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_GREATER_THAN_OPERATOR:
				setDefiningGreaterThanOperator((DefiningGreaterThanOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_GREATER_THAN_OR_EQUAL_OPERATOR:
				setDefiningGreaterThanOrEqualOperator((DefiningGreaterThanOrEqualOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_IDENTIFIER:
				setDefiningIdentifier((DefiningIdentifier)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_LESS_THAN_OPERATOR:
				setDefiningLessThanOperator((DefiningLessThanOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_LESS_THAN_OR_EQUAL_OPERATOR:
				setDefiningLessThanOrEqualOperator((DefiningLessThanOrEqualOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_MINUS_OPERATOR:
				setDefiningMinusOperator((DefiningMinusOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_MOD_OPERATOR:
				setDefiningModOperator((DefiningModOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_MULTIPLY_OPERATOR:
				setDefiningMultiplyOperator((DefiningMultiplyOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_NOT_EQUAL_OPERATOR:
				setDefiningNotEqualOperator((DefiningNotEqualOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_NOT_OPERATOR:
				setDefiningNotOperator((DefiningNotOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_OR_OPERATOR:
				setDefiningOrOperator((DefiningOrOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_PLUS_OPERATOR:
				setDefiningPlusOperator((DefiningPlusOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_REM_OPERATOR:
				setDefiningRemOperator((DefiningRemOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_UNARY_MINUS_OPERATOR:
				setDefiningUnaryMinusOperator((DefiningUnaryMinusOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_UNARY_PLUS_OPERATOR:
				setDefiningUnaryPlusOperator((DefiningUnaryPlusOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_XOR_OPERATOR:
				setDefiningXorOperator((DefiningXorOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DEFINITE_ATTRIBUTE:
				setDefiniteAttribute((DefiniteAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DELAY_RELATIVE_STATEMENT:
				setDelayRelativeStatement((DelayRelativeStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DELAY_UNTIL_STATEMENT:
				setDelayUntilStatement((DelayUntilStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DELTA_ATTRIBUTE:
				setDeltaAttribute((DeltaAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DELTA_CONSTRAINT:
				setDeltaConstraint((DeltaConstraint)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DENORM_ATTRIBUTE:
				setDenormAttribute((DenormAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DERIVED_RECORD_EXTENSION_DEFINITION:
				setDerivedRecordExtensionDefinition((DerivedRecordExtensionDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DERIVED_TYPE_DEFINITION:
				setDerivedTypeDefinition((DerivedTypeDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DETECT_BLOCKING_PRAGMA:
				setDetectBlockingPragma((DetectBlockingPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DIGITS_ATTRIBUTE:
				setDigitsAttribute((DigitsAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DIGITS_CONSTRAINT:
				setDigitsConstraint((DigitsConstraint)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DISCARD_NAMES_PRAGMA:
				setDiscardNamesPragma((DiscardNamesPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_RANGE_ATTRIBUTE_REFERENCE:
				setDiscreteRangeAttributeReference((DiscreteRangeAttributeReference)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION:
				setDiscreteRangeAttributeReferenceAsSubtypeDefinition((DiscreteRangeAttributeReferenceAsSubtypeDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_SIMPLE_EXPRESSION_RANGE:
				setDiscreteSimpleExpressionRange((DiscreteSimpleExpressionRange)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION:
				setDiscreteSimpleExpressionRangeAsSubtypeDefinition((DiscreteSimpleExpressionRangeAsSubtypeDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_SUBTYPE_INDICATION:
				setDiscreteSubtypeIndication((DiscreteSubtypeIndication)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION:
				setDiscreteSubtypeIndicationAsSubtypeDefinition((DiscreteSubtypeIndicationAsSubtypeDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DISCRIMINANT_ASSOCIATION:
				setDiscriminantAssociation((DiscriminantAssociation)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DISCRIMINANT_CONSTRAINT:
				setDiscriminantConstraint((DiscriminantConstraint)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DISCRIMINANT_SPECIFICATION:
				setDiscriminantSpecification((DiscriminantSpecification)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DISPATCHING_DOMAIN_PRAGMA:
				setDispatchingDomainPragma((DispatchingDomainPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__DIVIDE_OPERATOR:
				setDivideOperator((DivideOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ELABORATE_ALL_PRAGMA:
				setElaborateAllPragma((ElaborateAllPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ELABORATE_BODY_PRAGMA:
				setElaborateBodyPragma((ElaborateBodyPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ELABORATE_PRAGMA:
				setElaboratePragma((ElaboratePragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ELEMENT_ITERATOR_SPECIFICATION:
				setElementIteratorSpecification((ElementIteratorSpecification)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ELSE_EXPRESSION_PATH:
				setElseExpressionPath((ElseExpressionPath)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ELSE_PATH:
				setElsePath((ElsePath)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ELSIF_EXPRESSION_PATH:
				setElsifExpressionPath((ElsifExpressionPath)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ELSIF_PATH:
				setElsifPath((ElsifPath)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ENTRY_BODY_DECLARATION:
				setEntryBodyDeclaration((EntryBodyDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ENTRY_CALL_STATEMENT:
				setEntryCallStatement((EntryCallStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ENTRY_DECLARATION:
				setEntryDeclaration((EntryDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ENTRY_INDEX_SPECIFICATION:
				setEntryIndexSpecification((EntryIndexSpecification)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ENUMERATION_LITERAL:
				setEnumerationLiteral((EnumerationLiteral)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ENUMERATION_LITERAL_SPECIFICATION:
				setEnumerationLiteralSpecification((EnumerationLiteralSpecification)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ENUMERATION_REPRESENTATION_CLAUSE:
				setEnumerationRepresentationClause((EnumerationRepresentationClause)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ENUMERATION_TYPE_DEFINITION:
				setEnumerationTypeDefinition((EnumerationTypeDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__EQUAL_OPERATOR:
				setEqualOperator((EqualOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__EXCEPTION_DECLARATION:
				setExceptionDeclaration((ExceptionDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__EXCEPTION_HANDLER:
				setExceptionHandler((ExceptionHandler)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__EXCEPTION_RENAMING_DECLARATION:
				setExceptionRenamingDeclaration((ExceptionRenamingDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__EXIT_STATEMENT:
				setExitStatement((ExitStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__EXPLICIT_DEREFERENCE:
				setExplicitDereference((ExplicitDereference)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__EXPONENT_ATTRIBUTE:
				setExponentAttribute((ExponentAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__EXPONENTIATE_OPERATOR:
				setExponentiateOperator((ExponentiateOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__EXPORT_PRAGMA:
				setExportPragma((ExportPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__EXPRESSION_FUNCTION_DECLARATION:
				setExpressionFunctionDeclaration((ExpressionFunctionDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__EXTENDED_RETURN_STATEMENT:
				setExtendedReturnStatement((ExtendedReturnStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__EXTENSION_AGGREGATE:
				setExtensionAggregate((ExtensionAggregate)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__EXTERNAL_TAG_ATTRIBUTE:
				setExternalTagAttribute((ExternalTagAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FIRST_ATTRIBUTE:
				setFirstAttribute((FirstAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FIRST_BIT_ATTRIBUTE:
				setFirstBitAttribute((FirstBitAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FLOATING_POINT_DEFINITION:
				setFloatingPointDefinition((FloatingPointDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FLOOR_ATTRIBUTE:
				setFloorAttribute((FloorAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FOR_ALL_QUANTIFIED_EXPRESSION:
				setForAllQuantifiedExpression((ForAllQuantifiedExpression)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FOR_LOOP_STATEMENT:
				setForLoopStatement((ForLoopStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FOR_SOME_QUANTIFIED_EXPRESSION:
				setForSomeQuantifiedExpression((ForSomeQuantifiedExpression)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORE_ATTRIBUTE:
				setForeAttribute((ForeAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_CONSTANT:
				setFormalAccessToConstant((FormalAccessToConstant)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_FUNCTION:
				setFormalAccessToFunction((FormalAccessToFunction)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_PROCEDURE:
				setFormalAccessToProcedure((FormalAccessToProcedure)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_PROTECTED_FUNCTION:
				setFormalAccessToProtectedFunction((FormalAccessToProtectedFunction)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_PROTECTED_PROCEDURE:
				setFormalAccessToProtectedProcedure((FormalAccessToProtectedProcedure)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_VARIABLE:
				setFormalAccessToVariable((FormalAccessToVariable)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_CONSTRAINED_ARRAY_DEFINITION:
				setFormalConstrainedArrayDefinition((FormalConstrainedArrayDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_DECIMAL_FIXED_POINT_DEFINITION:
				setFormalDecimalFixedPointDefinition((FormalDecimalFixedPointDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_DERIVED_TYPE_DEFINITION:
				setFormalDerivedTypeDefinition((FormalDerivedTypeDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_DISCRETE_TYPE_DEFINITION:
				setFormalDiscreteTypeDefinition((FormalDiscreteTypeDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_FLOATING_POINT_DEFINITION:
				setFormalFloatingPointDefinition((FormalFloatingPointDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_FUNCTION_DECLARATION:
				setFormalFunctionDeclaration((FormalFunctionDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_INCOMPLETE_TYPE_DECLARATION:
				setFormalIncompleteTypeDeclaration((FormalIncompleteTypeDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_LIMITED_INTERFACE:
				setFormalLimitedInterface((FormalLimitedInterface)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_MODULAR_TYPE_DEFINITION:
				setFormalModularTypeDefinition((FormalModularTypeDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_OBJECT_DECLARATION:
				setFormalObjectDeclaration((FormalObjectDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ORDINARY_FIXED_POINT_DEFINITION:
				setFormalOrdinaryFixedPointDefinition((FormalOrdinaryFixedPointDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ORDINARY_INTERFACE:
				setFormalOrdinaryInterface((FormalOrdinaryInterface)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_PACKAGE_DECLARATION:
				setFormalPackageDeclaration((FormalPackageDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_PACKAGE_DECLARATION_WITH_BOX:
				setFormalPackageDeclarationWithBox((FormalPackageDeclarationWithBox)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				setFormalPoolSpecificAccessToVariable((FormalPoolSpecificAccessToVariable)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_PRIVATE_TYPE_DEFINITION:
				setFormalPrivateTypeDefinition((FormalPrivateTypeDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_PROCEDURE_DECLARATION:
				setFormalProcedureDeclaration((FormalProcedureDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_PROTECTED_INTERFACE:
				setFormalProtectedInterface((FormalProtectedInterface)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_SIGNED_INTEGER_TYPE_DEFINITION:
				setFormalSignedIntegerTypeDefinition((FormalSignedIntegerTypeDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_SYNCHRONIZED_INTERFACE:
				setFormalSynchronizedInterface((FormalSynchronizedInterface)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION:
				setFormalTaggedPrivateTypeDefinition((FormalTaggedPrivateTypeDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_TASK_INTERFACE:
				setFormalTaskInterface((FormalTaskInterface)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_TYPE_DECLARATION:
				setFormalTypeDeclaration((FormalTypeDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_UNCONSTRAINED_ARRAY_DEFINITION:
				setFormalUnconstrainedArrayDefinition((FormalUnconstrainedArrayDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FRACTION_ATTRIBUTE:
				setFractionAttribute((FractionAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_BODY_DECLARATION:
				setFunctionBodyDeclaration((FunctionBodyDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_BODY_STUB:
				setFunctionBodyStub((FunctionBodyStub)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_CALL:
				setFunctionCall((FunctionCall)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_DECLARATION:
				setFunctionDeclaration((FunctionDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_INSTANTIATION:
				setFunctionInstantiation((FunctionInstantiation)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_RENAMING_DECLARATION:
				setFunctionRenamingDeclaration((FunctionRenamingDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__GENERALIZED_ITERATOR_SPECIFICATION:
				setGeneralizedIteratorSpecification((GeneralizedIteratorSpecification)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__GENERIC_ASSOCIATION:
				setGenericAssociation((GenericAssociation)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__GENERIC_FUNCTION_DECLARATION:
				setGenericFunctionDeclaration((GenericFunctionDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__GENERIC_FUNCTION_RENAMING_DECLARATION:
				setGenericFunctionRenamingDeclaration((GenericFunctionRenamingDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__GENERIC_PACKAGE_DECLARATION:
				setGenericPackageDeclaration((GenericPackageDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__GENERIC_PACKAGE_RENAMING_DECLARATION:
				setGenericPackageRenamingDeclaration((GenericPackageRenamingDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__GENERIC_PROCEDURE_DECLARATION:
				setGenericProcedureDeclaration((GenericProcedureDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__GENERIC_PROCEDURE_RENAMING_DECLARATION:
				setGenericProcedureRenamingDeclaration((GenericProcedureRenamingDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__GOTO_STATEMENT:
				setGotoStatement((GotoStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__GREATER_THAN_OPERATOR:
				setGreaterThanOperator((GreaterThanOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__GREATER_THAN_OR_EQUAL_OPERATOR:
				setGreaterThanOrEqualOperator((GreaterThanOrEqualOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__IDENTIFIER:
				setIdentifier((Identifier)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__IDENTITY_ATTRIBUTE:
				setIdentityAttribute((IdentityAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__IF_EXPRESSION:
				setIfExpression((IfExpression)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__IF_EXPRESSION_PATH:
				setIfExpressionPath((IfExpressionPath)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__IF_PATH:
				setIfPath((IfPath)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__IF_STATEMENT:
				setIfStatement((IfStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__IMAGE_ATTRIBUTE:
				setImageAttribute((ImageAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__IMPLEMENTATION_DEFINED_ATTRIBUTE:
				setImplementationDefinedAttribute((ImplementationDefinedAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__IMPLEMENTATION_DEFINED_PRAGMA:
				setImplementationDefinedPragma((ImplementationDefinedPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__IMPORT_PRAGMA:
				setImportPragma((ImportPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__IN_MEMBERSHIP_TEST:
				setInMembershipTest((InMembershipTest)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__INCOMPLETE_TYPE_DECLARATION:
				setIncompleteTypeDeclaration((IncompleteTypeDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__INDEPENDENT_COMPONENTS_PRAGMA:
				setIndependentComponentsPragma((IndependentComponentsPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__INDEPENDENT_PRAGMA:
				setIndependentPragma((IndependentPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__INDEX_CONSTRAINT:
				setIndexConstraint((IndexConstraint)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__INDEXED_COMPONENT:
				setIndexedComponent((IndexedComponent)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__INLINE_PRAGMA:
				setInlinePragma((InlinePragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__INPUT_ATTRIBUTE:
				setInputAttribute((InputAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__INSPECTION_POINT_PRAGMA:
				setInspectionPointPragma((InspectionPointPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__INTEGER_LITERAL:
				setIntegerLiteral((IntegerLiteral)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__INTEGER_NUMBER_DECLARATION:
				setIntegerNumberDeclaration((IntegerNumberDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__INTERRUPT_HANDLER_PRAGMA:
				setInterruptHandlerPragma((InterruptHandlerPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__INTERRUPT_PRIORITY_PRAGMA:
				setInterruptPriorityPragma((InterruptPriorityPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__IS_PREFIX_CALL:
				setIsPrefixCall((IsPrefixCall)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__IS_PREFIX_NOTATION:
				setIsPrefixNotation((IsPrefixNotation)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__KNOWN_DISCRIMINANT_PART:
				setKnownDiscriminantPart((KnownDiscriminantPart)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__LAST_ATTRIBUTE:
				setLastAttribute((LastAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__LAST_BIT_ATTRIBUTE:
				setLastBitAttribute((LastBitAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__LEADING_PART_ATTRIBUTE:
				setLeadingPartAttribute((LeadingPartAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__LENGTH_ATTRIBUTE:
				setLengthAttribute((LengthAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__LESS_THAN_OPERATOR:
				setLessThanOperator((LessThanOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__LESS_THAN_OR_EQUAL_OPERATOR:
				setLessThanOrEqualOperator((LessThanOrEqualOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__LIMITED:
				setLimited((Limited)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__LIMITED_INTERFACE:
				setLimitedInterface((LimitedInterface)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__LINKER_OPTIONS_PRAGMA:
				setLinkerOptionsPragma((LinkerOptionsPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__LIST_PRAGMA:
				setListPragma((ListPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__LOCKING_POLICY_PRAGMA:
				setLockingPolicyPragma((LockingPolicyPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__LOOP_PARAMETER_SPECIFICATION:
				setLoopParameterSpecification((LoopParameterSpecification)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__LOOP_STATEMENT:
				setLoopStatement((LoopStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__MACHINE_ATTRIBUTE:
				setMachineAttribute((MachineAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__MACHINE_EMAX_ATTRIBUTE:
				setMachineEmaxAttribute((MachineEmaxAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__MACHINE_EMIN_ATTRIBUTE:
				setMachineEminAttribute((MachineEminAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__MACHINE_MANTISSA_ATTRIBUTE:
				setMachineMantissaAttribute((MachineMantissaAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__MACHINE_OVERFLOWS_ATTRIBUTE:
				setMachineOverflowsAttribute((MachineOverflowsAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__MACHINE_RADIX_ATTRIBUTE:
				setMachineRadixAttribute((MachineRadixAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__MACHINE_ROUNDING_ATTRIBUTE:
				setMachineRoundingAttribute((MachineRoundingAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__MACHINE_ROUNDS_ATTRIBUTE:
				setMachineRoundsAttribute((MachineRoundsAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE:
				setMaxAlignmentForAllocationAttribute((MaxAlignmentForAllocationAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__MAX_ATTRIBUTE:
				setMaxAttribute((MaxAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE:
				setMaxSizeInStorageElementsAttribute((MaxSizeInStorageElementsAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__MIN_ATTRIBUTE:
				setMinAttribute((MinAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__MINUS_OPERATOR:
				setMinusOperator((MinusOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__MOD_ATTRIBUTE:
				setModAttribute((ModAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__MOD_OPERATOR:
				setModOperator((ModOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__MODEL_ATTRIBUTE:
				setModelAttribute((ModelAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__MODEL_EMIN_ATTRIBUTE:
				setModelEminAttribute((ModelEminAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__MODEL_EPSILON_ATTRIBUTE:
				setModelEpsilonAttribute((ModelEpsilonAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__MODEL_MANTISSA_ATTRIBUTE:
				setModelMantissaAttribute((ModelMantissaAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__MODEL_SMALL_ATTRIBUTE:
				setModelSmallAttribute((ModelSmallAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__MODULAR_TYPE_DEFINITION:
				setModularTypeDefinition((ModularTypeDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__MODULUS_ATTRIBUTE:
				setModulusAttribute((ModulusAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__MULTIPLY_OPERATOR:
				setMultiplyOperator((MultiplyOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__NAMED_ARRAY_AGGREGATE:
				setNamedArrayAggregate((NamedArrayAggregate)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__NO_RETURN_PRAGMA:
				setNoReturnPragma((NoReturnPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__NORMALIZE_SCALARS_PRAGMA:
				setNormalizeScalarsPragma((NormalizeScalarsPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__NOT_EQUAL_OPERATOR:
				setNotEqualOperator((NotEqualOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__NOT_IN_MEMBERSHIP_TEST:
				setNotInMembershipTest((NotInMembershipTest)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__NOT_NULL_RETURN:
				setNotNullReturn((NotNullReturn)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__NOT_OPERATOR:
				setNotOperator((NotOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__NOT_OVERRIDING:
				setNotOverriding((NotOverriding)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__NULL_COMPONENT:
				setNullComponent((NullComponent)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__NULL_EXCLUSION:
				setNullExclusion((NullExclusion)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__NULL_LITERAL:
				setNullLiteral((NullLiteral)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__NULL_PROCEDURE_DECLARATION:
				setNullProcedureDeclaration((NullProcedureDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__NULL_RECORD_DEFINITION:
				setNullRecordDefinition((NullRecordDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__NULL_STATEMENT:
				setNullStatement((NullStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__OBJECT_RENAMING_DECLARATION:
				setObjectRenamingDeclaration((ObjectRenamingDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__OPTIMIZE_PRAGMA:
				setOptimizePragma((OptimizePragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__OR_ELSE_SHORT_CIRCUIT:
				setOrElseShortCircuit((OrElseShortCircuit)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__OR_OPERATOR:
				setOrOperator((OrOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__OR_PATH:
				setOrPath((OrPath)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ORDINARY_FIXED_POINT_DEFINITION:
				setOrdinaryFixedPointDefinition((OrdinaryFixedPointDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ORDINARY_INTERFACE:
				setOrdinaryInterface((OrdinaryInterface)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ORDINARY_TYPE_DECLARATION:
				setOrdinaryTypeDeclaration((OrdinaryTypeDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__OTHERS_CHOICE:
				setOthersChoice((OthersChoice)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__OUTPUT_ATTRIBUTE:
				setOutputAttribute((OutputAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__OVERLAPS_STORAGE_ATTRIBUTE:
				setOverlapsStorageAttribute((OverlapsStorageAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__OVERRIDING:
				setOverriding((Overriding)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PACK_PRAGMA:
				setPackPragma((PackPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PACKAGE_BODY_DECLARATION:
				setPackageBodyDeclaration((PackageBodyDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PACKAGE_BODY_STUB:
				setPackageBodyStub((PackageBodyStub)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PACKAGE_DECLARATION:
				setPackageDeclaration((PackageDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PACKAGE_INSTANTIATION:
				setPackageInstantiation((PackageInstantiation)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PACKAGE_RENAMING_DECLARATION:
				setPackageRenamingDeclaration((PackageRenamingDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PAGE_PRAGMA:
				setPagePragma((PagePragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PARAMETER_ASSOCIATION:
				setParameterAssociation((ParameterAssociation)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PARAMETER_SPECIFICATION:
				setParameterSpecification((ParameterSpecification)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PARENTHESIZED_EXPRESSION:
				setParenthesizedExpression((ParenthesizedExpression)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PARTITION_ELABORATION_POLICY_PRAGMA:
				setPartitionElaborationPolicyPragma((PartitionElaborationPolicyPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PARTITION_ID_ATTRIBUTE:
				setPartitionIdAttribute((PartitionIdAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PLUS_OPERATOR:
				setPlusOperator((PlusOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				setPoolSpecificAccessToVariable((PoolSpecificAccessToVariable)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__POS_ATTRIBUTE:
				setPosAttribute((PosAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__POSITION_ATTRIBUTE:
				setPositionAttribute((PositionAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__POSITIONAL_ARRAY_AGGREGATE:
				setPositionalArrayAggregate((PositionalArrayAggregate)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PRAGMA_ARGUMENT_ASSOCIATION:
				setPragmaArgumentAssociation((PragmaArgumentAssociation)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PRED_ATTRIBUTE:
				setPredAttribute((PredAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PREELABORABLE_INITIALIZATION_PRAGMA:
				setPreelaborableInitializationPragma((PreelaborableInitializationPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PREELABORATE_PRAGMA:
				setPreelaboratePragma((PreelaboratePragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PRIORITY_ATTRIBUTE:
				setPriorityAttribute((PriorityAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PRIORITY_PRAGMA:
				setPriorityPragma((PriorityPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				setPrioritySpecificDispatchingPragma((PrioritySpecificDispatchingPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PRIVATE:
				setPrivate((Private)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PRIVATE_EXTENSION_DECLARATION:
				setPrivateExtensionDeclaration((PrivateExtensionDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PRIVATE_EXTENSION_DEFINITION:
				setPrivateExtensionDefinition((PrivateExtensionDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PRIVATE_TYPE_DECLARATION:
				setPrivateTypeDeclaration((PrivateTypeDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PRIVATE_TYPE_DEFINITION:
				setPrivateTypeDefinition((PrivateTypeDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_BODY_DECLARATION:
				setProcedureBodyDeclaration((ProcedureBodyDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_BODY_STUB:
				setProcedureBodyStub((ProcedureBodyStub)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_CALL_STATEMENT:
				setProcedureCallStatement((ProcedureCallStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_DECLARATION:
				setProcedureDeclaration((ProcedureDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_INSTANTIATION:
				setProcedureInstantiation((ProcedureInstantiation)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_RENAMING_DECLARATION:
				setProcedureRenamingDeclaration((ProcedureRenamingDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PROFILE_PRAGMA:
				setProfilePragma((ProfilePragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PROTECTED_BODY_DECLARATION:
				setProtectedBodyDeclaration((ProtectedBodyDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PROTECTED_BODY_STUB:
				setProtectedBodyStub((ProtectedBodyStub)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PROTECTED_DEFINITION:
				setProtectedDefinition((ProtectedDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PROTECTED_INTERFACE:
				setProtectedInterface((ProtectedInterface)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PROTECTED_TYPE_DECLARATION:
				setProtectedTypeDeclaration((ProtectedTypeDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__PURE_PRAGMA:
				setPurePragma((PurePragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__QUALIFIED_EXPRESSION:
				setQualifiedExpression((QualifiedExpression)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__QUEUING_POLICY_PRAGMA:
				setQueuingPolicyPragma((QueuingPolicyPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__RAISE_EXPRESSION:
				setRaiseExpression((RaiseExpression)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__RAISE_STATEMENT:
				setRaiseStatement((RaiseStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__RANGE_ATTRIBUTE:
				setRangeAttribute((RangeAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__RANGE_ATTRIBUTE_REFERENCE:
				setRangeAttributeReference((RangeAttributeReference)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__READ_ATTRIBUTE:
				setReadAttribute((ReadAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__REAL_LITERAL:
				setRealLiteral((RealLiteral)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__REAL_NUMBER_DECLARATION:
				setRealNumberDeclaration((RealNumberDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__RECORD_AGGREGATE:
				setRecordAggregate((RecordAggregate)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__RECORD_COMPONENT_ASSOCIATION:
				setRecordComponentAssociation((RecordComponentAssociation)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__RECORD_DEFINITION:
				setRecordDefinition((RecordDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__RECORD_REPRESENTATION_CLAUSE:
				setRecordRepresentationClause((RecordRepresentationClause)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__RECORD_TYPE_DEFINITION:
				setRecordTypeDefinition((RecordTypeDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__RELATIVE_DEADLINE_PRAGMA:
				setRelativeDeadlinePragma((RelativeDeadlinePragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__REM_OPERATOR:
				setRemOperator((RemOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__REMAINDER_ATTRIBUTE:
				setRemainderAttribute((RemainderAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__REMOTE_CALL_INTERFACE_PRAGMA:
				setRemoteCallInterfacePragma((RemoteCallInterfacePragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__REMOTE_TYPES_PRAGMA:
				setRemoteTypesPragma((RemoteTypesPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__REQUEUE_STATEMENT:
				setRequeueStatement((RequeueStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__REQUEUE_STATEMENT_WITH_ABORT:
				setRequeueStatementWithAbort((RequeueStatementWithAbort)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__RESTRICTIONS_PRAGMA:
				setRestrictionsPragma((RestrictionsPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__RETURN_CONSTANT_SPECIFICATION:
				setReturnConstantSpecification((ReturnConstantSpecification)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__RETURN_STATEMENT:
				setReturnStatement((ReturnStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__RETURN_VARIABLE_SPECIFICATION:
				setReturnVariableSpecification((ReturnVariableSpecification)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__REVERSE:
				setReverse((Reverse)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__REVIEWABLE_PRAGMA:
				setReviewablePragma((ReviewablePragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ROOT_INTEGER_DEFINITION:
				setRootIntegerDefinition((RootIntegerDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ROOT_REAL_DEFINITION:
				setRootRealDefinition((RootRealDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ROUND_ATTRIBUTE:
				setRoundAttribute((RoundAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__ROUNDING_ATTRIBUTE:
				setRoundingAttribute((RoundingAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__SAFE_FIRST_ATTRIBUTE:
				setSafeFirstAttribute((SafeFirstAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__SAFE_LAST_ATTRIBUTE:
				setSafeLastAttribute((SafeLastAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__SCALE_ATTRIBUTE:
				setScaleAttribute((ScaleAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__SCALING_ATTRIBUTE:
				setScalingAttribute((ScalingAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__SELECT_PATH:
				setSelectPath((SelectPath)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__SELECTED_COMPONENT:
				setSelectedComponent((SelectedComponent)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__SELECTIVE_ACCEPT_STATEMENT:
				setSelectiveAcceptStatement((SelectiveAcceptStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__SHARED_PASSIVE_PRAGMA:
				setSharedPassivePragma((SharedPassivePragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__SIGNED_INTEGER_TYPE_DEFINITION:
				setSignedIntegerTypeDefinition((SignedIntegerTypeDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__SIGNED_ZEROS_ATTRIBUTE:
				setSignedZerosAttribute((SignedZerosAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__SIMPLE_EXPRESSION_RANGE:
				setSimpleExpressionRange((SimpleExpressionRange)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__SINGLE_PROTECTED_DECLARATION:
				setSingleProtectedDeclaration((SingleProtectedDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__SINGLE_TASK_DECLARATION:
				setSingleTaskDeclaration((SingleTaskDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__SIZE_ATTRIBUTE:
				setSizeAttribute((SizeAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__SLICE:
				setSlice((Slice)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__SMALL_ATTRIBUTE:
				setSmallAttribute((SmallAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__STORAGE_POOL_ATTRIBUTE:
				setStoragePoolAttribute((StoragePoolAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__STORAGE_SIZE_ATTRIBUTE:
				setStorageSizeAttribute((StorageSizeAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__STORAGE_SIZE_PRAGMA:
				setStorageSizePragma((StorageSizePragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__STREAM_SIZE_ATTRIBUTE:
				setStreamSizeAttribute((StreamSizeAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__STRING_LITERAL:
				setStringLiteral((StringLiteral)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__SUBTYPE_DECLARATION:
				setSubtypeDeclaration((SubtypeDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__SUBTYPE_INDICATION:
				setSubtypeIndication((SubtypeIndication)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__SUCC_ATTRIBUTE:
				setSuccAttribute((SuccAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__SUPPRESS_PRAGMA:
				setSuppressPragma((SuppressPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__SYNCHRONIZED:
				setSynchronized((Synchronized)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__SYNCHRONIZED_INTERFACE:
				setSynchronizedInterface((SynchronizedInterface)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__TAG_ATTRIBUTE:
				setTagAttribute((TagAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__TAGGED:
				setTagged((Tagged)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__TAGGED_INCOMPLETE_TYPE_DECLARATION:
				setTaggedIncompleteTypeDeclaration((TaggedIncompleteTypeDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__TAGGED_PRIVATE_TYPE_DEFINITION:
				setTaggedPrivateTypeDefinition((TaggedPrivateTypeDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__TAGGED_RECORD_TYPE_DEFINITION:
				setTaggedRecordTypeDefinition((TaggedRecordTypeDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__TASK_BODY_DECLARATION:
				setTaskBodyDeclaration((TaskBodyDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__TASK_BODY_STUB:
				setTaskBodyStub((TaskBodyStub)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__TASK_DEFINITION:
				setTaskDefinition((TaskDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__TASK_DISPATCHING_POLICY_PRAGMA:
				setTaskDispatchingPolicyPragma((TaskDispatchingPolicyPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__TASK_INTERFACE:
				setTaskInterface((TaskInterface)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__TASK_TYPE_DECLARATION:
				setTaskTypeDeclaration((TaskTypeDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__TERMINATE_ALTERNATIVE_STATEMENT:
				setTerminateAlternativeStatement((TerminateAlternativeStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__TERMINATED_ATTRIBUTE:
				setTerminatedAttribute((TerminatedAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__THEN_ABORT_PATH:
				setThenAbortPath((ThenAbortPath)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__TIMED_ENTRY_CALL_STATEMENT:
				setTimedEntryCallStatement((TimedEntryCallStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__TRUNCATION_ATTRIBUTE:
				setTruncationAttribute((TruncationAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__TYPE_CONVERSION:
				setTypeConversion((TypeConversion)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNARY_MINUS_OPERATOR:
				setUnaryMinusOperator((UnaryMinusOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNARY_PLUS_OPERATOR:
				setUnaryPlusOperator((UnaryPlusOperator)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNBIASED_ROUNDING_ATTRIBUTE:
				setUnbiasedRoundingAttribute((UnbiasedRoundingAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNCHECKED_ACCESS_ATTRIBUTE:
				setUncheckedAccessAttribute((UncheckedAccessAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNCHECKED_UNION_PRAGMA:
				setUncheckedUnionPragma((UncheckedUnionPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNCONSTRAINED_ARRAY_DEFINITION:
				setUnconstrainedArrayDefinition((UnconstrainedArrayDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNIVERSAL_FIXED_DEFINITION:
				setUniversalFixedDefinition((UniversalFixedDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNIVERSAL_INTEGER_DEFINITION:
				setUniversalIntegerDefinition((UniversalIntegerDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNIVERSAL_REAL_DEFINITION:
				setUniversalRealDefinition((UniversalRealDefinition)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNKNOWN_ATTRIBUTE:
				setUnknownAttribute((UnknownAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNKNOWN_DISCRIMINANT_PART:
				setUnknownDiscriminantPart((UnknownDiscriminantPart)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNKNOWN_PRAGMA:
				setUnknownPragma((UnknownPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__UNSUPPRESS_PRAGMA:
				setUnsuppressPragma((UnsuppressPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__USE_ALL_TYPE_CLAUSE:
				setUseAllTypeClause((UseAllTypeClause)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__USE_PACKAGE_CLAUSE:
				setUsePackageClause((UsePackageClause)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__USE_TYPE_CLAUSE:
				setUseTypeClause((UseTypeClause)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__VAL_ATTRIBUTE:
				setValAttribute((ValAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__VALID_ATTRIBUTE:
				setValidAttribute((ValidAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__VALUE_ATTRIBUTE:
				setValueAttribute((ValueAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__VARIABLE_DECLARATION:
				setVariableDeclaration((VariableDeclaration)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__VARIANT:
				setVariant((Variant)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__VARIANT_PART:
				setVariantPart((VariantPart)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__VERSION_ATTRIBUTE:
				setVersionAttribute((VersionAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__VOLATILE_COMPONENTS_PRAGMA:
				setVolatileComponentsPragma((VolatileComponentsPragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__VOLATILE_PRAGMA:
				setVolatilePragma((VolatilePragma)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__WHILE_LOOP_STATEMENT:
				setWhileLoopStatement((WhileLoopStatement)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__WIDE_IMAGE_ATTRIBUTE:
				setWideImageAttribute((WideImageAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__WIDE_VALUE_ATTRIBUTE:
				setWideValueAttribute((WideValueAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__WIDE_WIDE_IMAGE_ATTRIBUTE:
				setWideWideImageAttribute((WideWideImageAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__WIDE_WIDE_VALUE_ATTRIBUTE:
				setWideWideValueAttribute((WideWideValueAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__WIDE_WIDE_WIDTH_ATTRIBUTE:
				setWideWideWidthAttribute((WideWideWidthAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__WIDE_WIDTH_ATTRIBUTE:
				setWideWidthAttribute((WideWidthAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__WIDTH_ATTRIBUTE:
				setWidthAttribute((WidthAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__WITH_CLAUSE:
				setWithClause((WithClause)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__WRITE_ATTRIBUTE:
				setWriteAttribute((WriteAttribute)null);
				return;
			case AdaPackage.DOCUMENT_ROOT__XOR_OPERATOR:
				setXorOperator((XorOperator)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.DOCUMENT_ROOT__MIXED:
				return mixed != null && !mixed.isEmpty();
			case AdaPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP:
				return xMLNSPrefixMap != null && !xMLNSPrefixMap.isEmpty();
			case AdaPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION:
				return xSISchemaLocation != null && !xSISchemaLocation.isEmpty();
			case AdaPackage.DOCUMENT_ROOT__ABORT_STATEMENT:
				return getAbortStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__ABS_OPERATOR:
				return getAbsOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__ABSTRACT:
				return getAbstract() != null;
			case AdaPackage.DOCUMENT_ROOT__ACCEPT_STATEMENT:
				return getAcceptStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__ACCESS_ATTRIBUTE:
				return getAccessAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_CONSTANT:
				return getAccessToConstant() != null;
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_FUNCTION:
				return getAccessToFunction() != null;
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_PROCEDURE:
				return getAccessToProcedure() != null;
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_PROTECTED_FUNCTION:
				return getAccessToProtectedFunction() != null;
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_PROTECTED_PROCEDURE:
				return getAccessToProtectedProcedure() != null;
			case AdaPackage.DOCUMENT_ROOT__ACCESS_TO_VARIABLE:
				return getAccessToVariable() != null;
			case AdaPackage.DOCUMENT_ROOT__ADDRESS_ATTRIBUTE:
				return getAddressAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__ADJACENT_ATTRIBUTE:
				return getAdjacentAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__AFT_ATTRIBUTE:
				return getAftAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__ALIASED:
				return getAliased() != null;
			case AdaPackage.DOCUMENT_ROOT__ALIGNMENT_ATTRIBUTE:
				return getAlignmentAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__ALL_CALLS_REMOTE_PRAGMA:
				return getAllCallsRemotePragma() != null;
			case AdaPackage.DOCUMENT_ROOT__ALLOCATION_FROM_QUALIFIED_EXPRESSION:
				return getAllocationFromQualifiedExpression() != null;
			case AdaPackage.DOCUMENT_ROOT__ALLOCATION_FROM_SUBTYPE:
				return getAllocationFromSubtype() != null;
			case AdaPackage.DOCUMENT_ROOT__AND_OPERATOR:
				return getAndOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__AND_THEN_SHORT_CIRCUIT:
				return getAndThenShortCircuit() != null;
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_CONSTANT:
				return getAnonymousAccessToConstant() != null;
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_FUNCTION:
				return getAnonymousAccessToFunction() != null;
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_PROCEDURE:
				return getAnonymousAccessToProcedure() != null;
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION:
				return getAnonymousAccessToProtectedFunction() != null;
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE:
				return getAnonymousAccessToProtectedProcedure() != null;
			case AdaPackage.DOCUMENT_ROOT__ANONYMOUS_ACCESS_TO_VARIABLE:
				return getAnonymousAccessToVariable() != null;
			case AdaPackage.DOCUMENT_ROOT__ARRAY_COMPONENT_ASSOCIATION:
				return getArrayComponentAssociation() != null;
			case AdaPackage.DOCUMENT_ROOT__ASPECT_SPECIFICATION:
				return getAspectSpecification() != null;
			case AdaPackage.DOCUMENT_ROOT__ASSERT_PRAGMA:
				return getAssertPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__ASSERTION_POLICY_PRAGMA:
				return getAssertionPolicyPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__ASSIGNMENT_STATEMENT:
				return getAssignmentStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__ASYNCHRONOUS_PRAGMA:
				return getAsynchronousPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__ASYNCHRONOUS_SELECT_STATEMENT:
				return getAsynchronousSelectStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__AT_CLAUSE:
				return getAtClause() != null;
			case AdaPackage.DOCUMENT_ROOT__ATOMIC_COMPONENTS_PRAGMA:
				return getAtomicComponentsPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__ATOMIC_PRAGMA:
				return getAtomicPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__ATTACH_HANDLER_PRAGMA:
				return getAttachHandlerPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__ATTRIBUTE_DEFINITION_CLAUSE:
				return getAttributeDefinitionClause() != null;
			case AdaPackage.DOCUMENT_ROOT__BASE_ATTRIBUTE:
				return getBaseAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__BIT_ORDER_ATTRIBUTE:
				return getBitOrderAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__BLOCK_STATEMENT:
				return getBlockStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__BODY_VERSION_ATTRIBUTE:
				return getBodyVersionAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__BOX_EXPRESSION:
				return getBoxExpression() != null;
			case AdaPackage.DOCUMENT_ROOT__CALLABLE_ATTRIBUTE:
				return getCallableAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__CALLER_ATTRIBUTE:
				return getCallerAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__CASE_EXPRESSION:
				return getCaseExpression() != null;
			case AdaPackage.DOCUMENT_ROOT__CASE_EXPRESSION_PATH:
				return getCaseExpressionPath() != null;
			case AdaPackage.DOCUMENT_ROOT__CASE_PATH:
				return getCasePath() != null;
			case AdaPackage.DOCUMENT_ROOT__CASE_STATEMENT:
				return getCaseStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__CEILING_ATTRIBUTE:
				return getCeilingAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__CHARACTER_LITERAL:
				return getCharacterLiteral() != null;
			case AdaPackage.DOCUMENT_ROOT__CHOICE_PARAMETER_SPECIFICATION:
				return getChoiceParameterSpecification() != null;
			case AdaPackage.DOCUMENT_ROOT__CLASS_ATTRIBUTE:
				return getClassAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__CODE_STATEMENT:
				return getCodeStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__COMMENT:
				return getComment() != null;
			case AdaPackage.DOCUMENT_ROOT__COMPILATION_UNIT:
				return getCompilationUnit() != null;
			case AdaPackage.DOCUMENT_ROOT__COMPONENT_CLAUSE:
				return getComponentClause() != null;
			case AdaPackage.DOCUMENT_ROOT__COMPONENT_DECLARATION:
				return getComponentDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__COMPONENT_DEFINITION:
				return getComponentDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__COMPONENT_SIZE_ATTRIBUTE:
				return getComponentSizeAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__COMPOSE_ATTRIBUTE:
				return getComposeAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__CONCATENATE_OPERATOR:
				return getConcatenateOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__CONDITIONAL_ENTRY_CALL_STATEMENT:
				return getConditionalEntryCallStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__CONSTANT_DECLARATION:
				return getConstantDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__CONSTRAINED_ARRAY_DEFINITION:
				return getConstrainedArrayDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__CONSTRAINED_ATTRIBUTE:
				return getConstrainedAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__CONTROLLED_PRAGMA:
				return getControlledPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__CONVENTION_PRAGMA:
				return getConventionPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__COPY_SIGN_ATTRIBUTE:
				return getCopySignAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__COUNT_ATTRIBUTE:
				return getCountAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__CPU_PRAGMA:
				return getCpuPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__DECIMAL_FIXED_POINT_DEFINITION:
				return getDecimalFixedPointDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFAULT_STORAGE_POOL_PRAGMA:
				return getDefaultStoragePoolPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFERRED_CONSTANT_DECLARATION:
				return getDeferredConstantDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_ABS_OPERATOR:
				return getDefiningAbsOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_AND_OPERATOR:
				return getDefiningAndOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_CHARACTER_LITERAL:
				return getDefiningCharacterLiteral() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_CONCATENATE_OPERATOR:
				return getDefiningConcatenateOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_DIVIDE_OPERATOR:
				return getDefiningDivideOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_ENUMERATION_LITERAL:
				return getDefiningEnumerationLiteral() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_EQUAL_OPERATOR:
				return getDefiningEqualOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_EXPANDED_NAME:
				return getDefiningExpandedName() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_EXPONENTIATE_OPERATOR:
				return getDefiningExponentiateOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_GREATER_THAN_OPERATOR:
				return getDefiningGreaterThanOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_GREATER_THAN_OR_EQUAL_OPERATOR:
				return getDefiningGreaterThanOrEqualOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_IDENTIFIER:
				return getDefiningIdentifier() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_LESS_THAN_OPERATOR:
				return getDefiningLessThanOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_LESS_THAN_OR_EQUAL_OPERATOR:
				return getDefiningLessThanOrEqualOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_MINUS_OPERATOR:
				return getDefiningMinusOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_MOD_OPERATOR:
				return getDefiningModOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_MULTIPLY_OPERATOR:
				return getDefiningMultiplyOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_NOT_EQUAL_OPERATOR:
				return getDefiningNotEqualOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_NOT_OPERATOR:
				return getDefiningNotOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_OR_OPERATOR:
				return getDefiningOrOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_PLUS_OPERATOR:
				return getDefiningPlusOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_REM_OPERATOR:
				return getDefiningRemOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_UNARY_MINUS_OPERATOR:
				return getDefiningUnaryMinusOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_UNARY_PLUS_OPERATOR:
				return getDefiningUnaryPlusOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINING_XOR_OPERATOR:
				return getDefiningXorOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__DEFINITE_ATTRIBUTE:
				return getDefiniteAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__DELAY_RELATIVE_STATEMENT:
				return getDelayRelativeStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__DELAY_UNTIL_STATEMENT:
				return getDelayUntilStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__DELTA_ATTRIBUTE:
				return getDeltaAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__DELTA_CONSTRAINT:
				return getDeltaConstraint() != null;
			case AdaPackage.DOCUMENT_ROOT__DENORM_ATTRIBUTE:
				return getDenormAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__DERIVED_RECORD_EXTENSION_DEFINITION:
				return getDerivedRecordExtensionDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__DERIVED_TYPE_DEFINITION:
				return getDerivedTypeDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__DETECT_BLOCKING_PRAGMA:
				return getDetectBlockingPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__DIGITS_ATTRIBUTE:
				return getDigitsAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__DIGITS_CONSTRAINT:
				return getDigitsConstraint() != null;
			case AdaPackage.DOCUMENT_ROOT__DISCARD_NAMES_PRAGMA:
				return getDiscardNamesPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_RANGE_ATTRIBUTE_REFERENCE:
				return getDiscreteRangeAttributeReference() != null;
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION:
				return getDiscreteRangeAttributeReferenceAsSubtypeDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_SIMPLE_EXPRESSION_RANGE:
				return getDiscreteSimpleExpressionRange() != null;
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION:
				return getDiscreteSimpleExpressionRangeAsSubtypeDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_SUBTYPE_INDICATION:
				return getDiscreteSubtypeIndication() != null;
			case AdaPackage.DOCUMENT_ROOT__DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION:
				return getDiscreteSubtypeIndicationAsSubtypeDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__DISCRIMINANT_ASSOCIATION:
				return getDiscriminantAssociation() != null;
			case AdaPackage.DOCUMENT_ROOT__DISCRIMINANT_CONSTRAINT:
				return getDiscriminantConstraint() != null;
			case AdaPackage.DOCUMENT_ROOT__DISCRIMINANT_SPECIFICATION:
				return getDiscriminantSpecification() != null;
			case AdaPackage.DOCUMENT_ROOT__DISPATCHING_DOMAIN_PRAGMA:
				return getDispatchingDomainPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__DIVIDE_OPERATOR:
				return getDivideOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__ELABORATE_ALL_PRAGMA:
				return getElaborateAllPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__ELABORATE_BODY_PRAGMA:
				return getElaborateBodyPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__ELABORATE_PRAGMA:
				return getElaboratePragma() != null;
			case AdaPackage.DOCUMENT_ROOT__ELEMENT_ITERATOR_SPECIFICATION:
				return getElementIteratorSpecification() != null;
			case AdaPackage.DOCUMENT_ROOT__ELSE_EXPRESSION_PATH:
				return getElseExpressionPath() != null;
			case AdaPackage.DOCUMENT_ROOT__ELSE_PATH:
				return getElsePath() != null;
			case AdaPackage.DOCUMENT_ROOT__ELSIF_EXPRESSION_PATH:
				return getElsifExpressionPath() != null;
			case AdaPackage.DOCUMENT_ROOT__ELSIF_PATH:
				return getElsifPath() != null;
			case AdaPackage.DOCUMENT_ROOT__ENTRY_BODY_DECLARATION:
				return getEntryBodyDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__ENTRY_CALL_STATEMENT:
				return getEntryCallStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__ENTRY_DECLARATION:
				return getEntryDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__ENTRY_INDEX_SPECIFICATION:
				return getEntryIndexSpecification() != null;
			case AdaPackage.DOCUMENT_ROOT__ENUMERATION_LITERAL:
				return getEnumerationLiteral() != null;
			case AdaPackage.DOCUMENT_ROOT__ENUMERATION_LITERAL_SPECIFICATION:
				return getEnumerationLiteralSpecification() != null;
			case AdaPackage.DOCUMENT_ROOT__ENUMERATION_REPRESENTATION_CLAUSE:
				return getEnumerationRepresentationClause() != null;
			case AdaPackage.DOCUMENT_ROOT__ENUMERATION_TYPE_DEFINITION:
				return getEnumerationTypeDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__EQUAL_OPERATOR:
				return getEqualOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__EXCEPTION_DECLARATION:
				return getExceptionDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__EXCEPTION_HANDLER:
				return getExceptionHandler() != null;
			case AdaPackage.DOCUMENT_ROOT__EXCEPTION_RENAMING_DECLARATION:
				return getExceptionRenamingDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__EXIT_STATEMENT:
				return getExitStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__EXPLICIT_DEREFERENCE:
				return getExplicitDereference() != null;
			case AdaPackage.DOCUMENT_ROOT__EXPONENT_ATTRIBUTE:
				return getExponentAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__EXPONENTIATE_OPERATOR:
				return getExponentiateOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__EXPORT_PRAGMA:
				return getExportPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__EXPRESSION_FUNCTION_DECLARATION:
				return getExpressionFunctionDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__EXTENDED_RETURN_STATEMENT:
				return getExtendedReturnStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__EXTENSION_AGGREGATE:
				return getExtensionAggregate() != null;
			case AdaPackage.DOCUMENT_ROOT__EXTERNAL_TAG_ATTRIBUTE:
				return getExternalTagAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__FIRST_ATTRIBUTE:
				return getFirstAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__FIRST_BIT_ATTRIBUTE:
				return getFirstBitAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__FLOATING_POINT_DEFINITION:
				return getFloatingPointDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__FLOOR_ATTRIBUTE:
				return getFloorAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__FOR_ALL_QUANTIFIED_EXPRESSION:
				return getForAllQuantifiedExpression() != null;
			case AdaPackage.DOCUMENT_ROOT__FOR_LOOP_STATEMENT:
				return getForLoopStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__FOR_SOME_QUANTIFIED_EXPRESSION:
				return getForSomeQuantifiedExpression() != null;
			case AdaPackage.DOCUMENT_ROOT__FORE_ATTRIBUTE:
				return getForeAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_CONSTANT:
				return getFormalAccessToConstant() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_FUNCTION:
				return getFormalAccessToFunction() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_PROCEDURE:
				return getFormalAccessToProcedure() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_PROTECTED_FUNCTION:
				return getFormalAccessToProtectedFunction() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_PROTECTED_PROCEDURE:
				return getFormalAccessToProtectedProcedure() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ACCESS_TO_VARIABLE:
				return getFormalAccessToVariable() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_CONSTRAINED_ARRAY_DEFINITION:
				return getFormalConstrainedArrayDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_DECIMAL_FIXED_POINT_DEFINITION:
				return getFormalDecimalFixedPointDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_DERIVED_TYPE_DEFINITION:
				return getFormalDerivedTypeDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_DISCRETE_TYPE_DEFINITION:
				return getFormalDiscreteTypeDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_FLOATING_POINT_DEFINITION:
				return getFormalFloatingPointDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_FUNCTION_DECLARATION:
				return getFormalFunctionDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_INCOMPLETE_TYPE_DECLARATION:
				return getFormalIncompleteTypeDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_LIMITED_INTERFACE:
				return getFormalLimitedInterface() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_MODULAR_TYPE_DEFINITION:
				return getFormalModularTypeDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_OBJECT_DECLARATION:
				return getFormalObjectDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ORDINARY_FIXED_POINT_DEFINITION:
				return getFormalOrdinaryFixedPointDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_ORDINARY_INTERFACE:
				return getFormalOrdinaryInterface() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_PACKAGE_DECLARATION:
				return getFormalPackageDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_PACKAGE_DECLARATION_WITH_BOX:
				return getFormalPackageDeclarationWithBox() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				return getFormalPoolSpecificAccessToVariable() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_PRIVATE_TYPE_DEFINITION:
				return getFormalPrivateTypeDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_PROCEDURE_DECLARATION:
				return getFormalProcedureDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_PROTECTED_INTERFACE:
				return getFormalProtectedInterface() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_SIGNED_INTEGER_TYPE_DEFINITION:
				return getFormalSignedIntegerTypeDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_SYNCHRONIZED_INTERFACE:
				return getFormalSynchronizedInterface() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION:
				return getFormalTaggedPrivateTypeDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_TASK_INTERFACE:
				return getFormalTaskInterface() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_TYPE_DECLARATION:
				return getFormalTypeDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__FORMAL_UNCONSTRAINED_ARRAY_DEFINITION:
				return getFormalUnconstrainedArrayDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__FRACTION_ATTRIBUTE:
				return getFractionAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_BODY_DECLARATION:
				return getFunctionBodyDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_BODY_STUB:
				return getFunctionBodyStub() != null;
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_CALL:
				return getFunctionCall() != null;
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_DECLARATION:
				return getFunctionDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_INSTANTIATION:
				return getFunctionInstantiation() != null;
			case AdaPackage.DOCUMENT_ROOT__FUNCTION_RENAMING_DECLARATION:
				return getFunctionRenamingDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__GENERALIZED_ITERATOR_SPECIFICATION:
				return getGeneralizedIteratorSpecification() != null;
			case AdaPackage.DOCUMENT_ROOT__GENERIC_ASSOCIATION:
				return getGenericAssociation() != null;
			case AdaPackage.DOCUMENT_ROOT__GENERIC_FUNCTION_DECLARATION:
				return getGenericFunctionDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__GENERIC_FUNCTION_RENAMING_DECLARATION:
				return getGenericFunctionRenamingDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__GENERIC_PACKAGE_DECLARATION:
				return getGenericPackageDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__GENERIC_PACKAGE_RENAMING_DECLARATION:
				return getGenericPackageRenamingDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__GENERIC_PROCEDURE_DECLARATION:
				return getGenericProcedureDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__GENERIC_PROCEDURE_RENAMING_DECLARATION:
				return getGenericProcedureRenamingDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__GOTO_STATEMENT:
				return getGotoStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__GREATER_THAN_OPERATOR:
				return getGreaterThanOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__GREATER_THAN_OR_EQUAL_OPERATOR:
				return getGreaterThanOrEqualOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__IDENTIFIER:
				return getIdentifier() != null;
			case AdaPackage.DOCUMENT_ROOT__IDENTITY_ATTRIBUTE:
				return getIdentityAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__IF_EXPRESSION:
				return getIfExpression() != null;
			case AdaPackage.DOCUMENT_ROOT__IF_EXPRESSION_PATH:
				return getIfExpressionPath() != null;
			case AdaPackage.DOCUMENT_ROOT__IF_PATH:
				return getIfPath() != null;
			case AdaPackage.DOCUMENT_ROOT__IF_STATEMENT:
				return getIfStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__IMAGE_ATTRIBUTE:
				return getImageAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__IMPLEMENTATION_DEFINED_ATTRIBUTE:
				return getImplementationDefinedAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__IMPLEMENTATION_DEFINED_PRAGMA:
				return getImplementationDefinedPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__IMPORT_PRAGMA:
				return getImportPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__IN_MEMBERSHIP_TEST:
				return getInMembershipTest() != null;
			case AdaPackage.DOCUMENT_ROOT__INCOMPLETE_TYPE_DECLARATION:
				return getIncompleteTypeDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__INDEPENDENT_COMPONENTS_PRAGMA:
				return getIndependentComponentsPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__INDEPENDENT_PRAGMA:
				return getIndependentPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__INDEX_CONSTRAINT:
				return getIndexConstraint() != null;
			case AdaPackage.DOCUMENT_ROOT__INDEXED_COMPONENT:
				return getIndexedComponent() != null;
			case AdaPackage.DOCUMENT_ROOT__INLINE_PRAGMA:
				return getInlinePragma() != null;
			case AdaPackage.DOCUMENT_ROOT__INPUT_ATTRIBUTE:
				return getInputAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__INSPECTION_POINT_PRAGMA:
				return getInspectionPointPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__INTEGER_LITERAL:
				return getIntegerLiteral() != null;
			case AdaPackage.DOCUMENT_ROOT__INTEGER_NUMBER_DECLARATION:
				return getIntegerNumberDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__INTERRUPT_HANDLER_PRAGMA:
				return getInterruptHandlerPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__INTERRUPT_PRIORITY_PRAGMA:
				return getInterruptPriorityPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__IS_PREFIX_CALL:
				return getIsPrefixCall() != null;
			case AdaPackage.DOCUMENT_ROOT__IS_PREFIX_NOTATION:
				return getIsPrefixNotation() != null;
			case AdaPackage.DOCUMENT_ROOT__KNOWN_DISCRIMINANT_PART:
				return getKnownDiscriminantPart() != null;
			case AdaPackage.DOCUMENT_ROOT__LAST_ATTRIBUTE:
				return getLastAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__LAST_BIT_ATTRIBUTE:
				return getLastBitAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__LEADING_PART_ATTRIBUTE:
				return getLeadingPartAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__LENGTH_ATTRIBUTE:
				return getLengthAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__LESS_THAN_OPERATOR:
				return getLessThanOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__LESS_THAN_OR_EQUAL_OPERATOR:
				return getLessThanOrEqualOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__LIMITED:
				return getLimited() != null;
			case AdaPackage.DOCUMENT_ROOT__LIMITED_INTERFACE:
				return getLimitedInterface() != null;
			case AdaPackage.DOCUMENT_ROOT__LINKER_OPTIONS_PRAGMA:
				return getLinkerOptionsPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__LIST_PRAGMA:
				return getListPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__LOCKING_POLICY_PRAGMA:
				return getLockingPolicyPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__LOOP_PARAMETER_SPECIFICATION:
				return getLoopParameterSpecification() != null;
			case AdaPackage.DOCUMENT_ROOT__LOOP_STATEMENT:
				return getLoopStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__MACHINE_ATTRIBUTE:
				return getMachineAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__MACHINE_EMAX_ATTRIBUTE:
				return getMachineEmaxAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__MACHINE_EMIN_ATTRIBUTE:
				return getMachineEminAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__MACHINE_MANTISSA_ATTRIBUTE:
				return getMachineMantissaAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__MACHINE_OVERFLOWS_ATTRIBUTE:
				return getMachineOverflowsAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__MACHINE_RADIX_ATTRIBUTE:
				return getMachineRadixAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__MACHINE_ROUNDING_ATTRIBUTE:
				return getMachineRoundingAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__MACHINE_ROUNDS_ATTRIBUTE:
				return getMachineRoundsAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE:
				return getMaxAlignmentForAllocationAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__MAX_ATTRIBUTE:
				return getMaxAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE:
				return getMaxSizeInStorageElementsAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__MIN_ATTRIBUTE:
				return getMinAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__MINUS_OPERATOR:
				return getMinusOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__MOD_ATTRIBUTE:
				return getModAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__MOD_OPERATOR:
				return getModOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__MODEL_ATTRIBUTE:
				return getModelAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__MODEL_EMIN_ATTRIBUTE:
				return getModelEminAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__MODEL_EPSILON_ATTRIBUTE:
				return getModelEpsilonAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__MODEL_MANTISSA_ATTRIBUTE:
				return getModelMantissaAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__MODEL_SMALL_ATTRIBUTE:
				return getModelSmallAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__MODULAR_TYPE_DEFINITION:
				return getModularTypeDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__MODULUS_ATTRIBUTE:
				return getModulusAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__MULTIPLY_OPERATOR:
				return getMultiplyOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__NAMED_ARRAY_AGGREGATE:
				return getNamedArrayAggregate() != null;
			case AdaPackage.DOCUMENT_ROOT__NO_RETURN_PRAGMA:
				return getNoReturnPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__NORMALIZE_SCALARS_PRAGMA:
				return getNormalizeScalarsPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__NOT_AN_ELEMENT:
				return getNotAnElement() != null;
			case AdaPackage.DOCUMENT_ROOT__NOT_EQUAL_OPERATOR:
				return getNotEqualOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__NOT_IN_MEMBERSHIP_TEST:
				return getNotInMembershipTest() != null;
			case AdaPackage.DOCUMENT_ROOT__NOT_NULL_RETURN:
				return getNotNullReturn() != null;
			case AdaPackage.DOCUMENT_ROOT__NOT_OPERATOR:
				return getNotOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__NOT_OVERRIDING:
				return getNotOverriding() != null;
			case AdaPackage.DOCUMENT_ROOT__NULL_COMPONENT:
				return getNullComponent() != null;
			case AdaPackage.DOCUMENT_ROOT__NULL_EXCLUSION:
				return getNullExclusion() != null;
			case AdaPackage.DOCUMENT_ROOT__NULL_LITERAL:
				return getNullLiteral() != null;
			case AdaPackage.DOCUMENT_ROOT__NULL_PROCEDURE_DECLARATION:
				return getNullProcedureDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__NULL_RECORD_DEFINITION:
				return getNullRecordDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__NULL_STATEMENT:
				return getNullStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__OBJECT_RENAMING_DECLARATION:
				return getObjectRenamingDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__OPTIMIZE_PRAGMA:
				return getOptimizePragma() != null;
			case AdaPackage.DOCUMENT_ROOT__OR_ELSE_SHORT_CIRCUIT:
				return getOrElseShortCircuit() != null;
			case AdaPackage.DOCUMENT_ROOT__OR_OPERATOR:
				return getOrOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__OR_PATH:
				return getOrPath() != null;
			case AdaPackage.DOCUMENT_ROOT__ORDINARY_FIXED_POINT_DEFINITION:
				return getOrdinaryFixedPointDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__ORDINARY_INTERFACE:
				return getOrdinaryInterface() != null;
			case AdaPackage.DOCUMENT_ROOT__ORDINARY_TYPE_DECLARATION:
				return getOrdinaryTypeDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__OTHERS_CHOICE:
				return getOthersChoice() != null;
			case AdaPackage.DOCUMENT_ROOT__OUTPUT_ATTRIBUTE:
				return getOutputAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__OVERLAPS_STORAGE_ATTRIBUTE:
				return getOverlapsStorageAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__OVERRIDING:
				return getOverriding() != null;
			case AdaPackage.DOCUMENT_ROOT__PACK_PRAGMA:
				return getPackPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__PACKAGE_BODY_DECLARATION:
				return getPackageBodyDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__PACKAGE_BODY_STUB:
				return getPackageBodyStub() != null;
			case AdaPackage.DOCUMENT_ROOT__PACKAGE_DECLARATION:
				return getPackageDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__PACKAGE_INSTANTIATION:
				return getPackageInstantiation() != null;
			case AdaPackage.DOCUMENT_ROOT__PACKAGE_RENAMING_DECLARATION:
				return getPackageRenamingDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__PAGE_PRAGMA:
				return getPagePragma() != null;
			case AdaPackage.DOCUMENT_ROOT__PARAMETER_ASSOCIATION:
				return getParameterAssociation() != null;
			case AdaPackage.DOCUMENT_ROOT__PARAMETER_SPECIFICATION:
				return getParameterSpecification() != null;
			case AdaPackage.DOCUMENT_ROOT__PARENTHESIZED_EXPRESSION:
				return getParenthesizedExpression() != null;
			case AdaPackage.DOCUMENT_ROOT__PARTITION_ELABORATION_POLICY_PRAGMA:
				return getPartitionElaborationPolicyPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__PARTITION_ID_ATTRIBUTE:
				return getPartitionIdAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__PLUS_OPERATOR:
				return getPlusOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				return getPoolSpecificAccessToVariable() != null;
			case AdaPackage.DOCUMENT_ROOT__POS_ATTRIBUTE:
				return getPosAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__POSITION_ATTRIBUTE:
				return getPositionAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__POSITIONAL_ARRAY_AGGREGATE:
				return getPositionalArrayAggregate() != null;
			case AdaPackage.DOCUMENT_ROOT__PRAGMA_ARGUMENT_ASSOCIATION:
				return getPragmaArgumentAssociation() != null;
			case AdaPackage.DOCUMENT_ROOT__PRED_ATTRIBUTE:
				return getPredAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__PREELABORABLE_INITIALIZATION_PRAGMA:
				return getPreelaborableInitializationPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__PREELABORATE_PRAGMA:
				return getPreelaboratePragma() != null;
			case AdaPackage.DOCUMENT_ROOT__PRIORITY_ATTRIBUTE:
				return getPriorityAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__PRIORITY_PRAGMA:
				return getPriorityPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return getPrioritySpecificDispatchingPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__PRIVATE:
				return getPrivate() != null;
			case AdaPackage.DOCUMENT_ROOT__PRIVATE_EXTENSION_DECLARATION:
				return getPrivateExtensionDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__PRIVATE_EXTENSION_DEFINITION:
				return getPrivateExtensionDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__PRIVATE_TYPE_DECLARATION:
				return getPrivateTypeDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__PRIVATE_TYPE_DEFINITION:
				return getPrivateTypeDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_BODY_DECLARATION:
				return getProcedureBodyDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_BODY_STUB:
				return getProcedureBodyStub() != null;
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_CALL_STATEMENT:
				return getProcedureCallStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_DECLARATION:
				return getProcedureDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_INSTANTIATION:
				return getProcedureInstantiation() != null;
			case AdaPackage.DOCUMENT_ROOT__PROCEDURE_RENAMING_DECLARATION:
				return getProcedureRenamingDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__PROFILE_PRAGMA:
				return getProfilePragma() != null;
			case AdaPackage.DOCUMENT_ROOT__PROTECTED_BODY_DECLARATION:
				return getProtectedBodyDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__PROTECTED_BODY_STUB:
				return getProtectedBodyStub() != null;
			case AdaPackage.DOCUMENT_ROOT__PROTECTED_DEFINITION:
				return getProtectedDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__PROTECTED_INTERFACE:
				return getProtectedInterface() != null;
			case AdaPackage.DOCUMENT_ROOT__PROTECTED_TYPE_DECLARATION:
				return getProtectedTypeDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__PURE_PRAGMA:
				return getPurePragma() != null;
			case AdaPackage.DOCUMENT_ROOT__QUALIFIED_EXPRESSION:
				return getQualifiedExpression() != null;
			case AdaPackage.DOCUMENT_ROOT__QUEUING_POLICY_PRAGMA:
				return getQueuingPolicyPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__RAISE_EXPRESSION:
				return getRaiseExpression() != null;
			case AdaPackage.DOCUMENT_ROOT__RAISE_STATEMENT:
				return getRaiseStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__RANGE_ATTRIBUTE:
				return getRangeAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__RANGE_ATTRIBUTE_REFERENCE:
				return getRangeAttributeReference() != null;
			case AdaPackage.DOCUMENT_ROOT__READ_ATTRIBUTE:
				return getReadAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__REAL_LITERAL:
				return getRealLiteral() != null;
			case AdaPackage.DOCUMENT_ROOT__REAL_NUMBER_DECLARATION:
				return getRealNumberDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__RECORD_AGGREGATE:
				return getRecordAggregate() != null;
			case AdaPackage.DOCUMENT_ROOT__RECORD_COMPONENT_ASSOCIATION:
				return getRecordComponentAssociation() != null;
			case AdaPackage.DOCUMENT_ROOT__RECORD_DEFINITION:
				return getRecordDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__RECORD_REPRESENTATION_CLAUSE:
				return getRecordRepresentationClause() != null;
			case AdaPackage.DOCUMENT_ROOT__RECORD_TYPE_DEFINITION:
				return getRecordTypeDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__RELATIVE_DEADLINE_PRAGMA:
				return getRelativeDeadlinePragma() != null;
			case AdaPackage.DOCUMENT_ROOT__REM_OPERATOR:
				return getRemOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__REMAINDER_ATTRIBUTE:
				return getRemainderAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__REMOTE_CALL_INTERFACE_PRAGMA:
				return getRemoteCallInterfacePragma() != null;
			case AdaPackage.DOCUMENT_ROOT__REMOTE_TYPES_PRAGMA:
				return getRemoteTypesPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__REQUEUE_STATEMENT:
				return getRequeueStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__REQUEUE_STATEMENT_WITH_ABORT:
				return getRequeueStatementWithAbort() != null;
			case AdaPackage.DOCUMENT_ROOT__RESTRICTIONS_PRAGMA:
				return getRestrictionsPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__RETURN_CONSTANT_SPECIFICATION:
				return getReturnConstantSpecification() != null;
			case AdaPackage.DOCUMENT_ROOT__RETURN_STATEMENT:
				return getReturnStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__RETURN_VARIABLE_SPECIFICATION:
				return getReturnVariableSpecification() != null;
			case AdaPackage.DOCUMENT_ROOT__REVERSE:
				return getReverse() != null;
			case AdaPackage.DOCUMENT_ROOT__REVIEWABLE_PRAGMA:
				return getReviewablePragma() != null;
			case AdaPackage.DOCUMENT_ROOT__ROOT_INTEGER_DEFINITION:
				return getRootIntegerDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__ROOT_REAL_DEFINITION:
				return getRootRealDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__ROUND_ATTRIBUTE:
				return getRoundAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__ROUNDING_ATTRIBUTE:
				return getRoundingAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__SAFE_FIRST_ATTRIBUTE:
				return getSafeFirstAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__SAFE_LAST_ATTRIBUTE:
				return getSafeLastAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__SCALE_ATTRIBUTE:
				return getScaleAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__SCALING_ATTRIBUTE:
				return getScalingAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__SELECT_PATH:
				return getSelectPath() != null;
			case AdaPackage.DOCUMENT_ROOT__SELECTED_COMPONENT:
				return getSelectedComponent() != null;
			case AdaPackage.DOCUMENT_ROOT__SELECTIVE_ACCEPT_STATEMENT:
				return getSelectiveAcceptStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__SHARED_PASSIVE_PRAGMA:
				return getSharedPassivePragma() != null;
			case AdaPackage.DOCUMENT_ROOT__SIGNED_INTEGER_TYPE_DEFINITION:
				return getSignedIntegerTypeDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__SIGNED_ZEROS_ATTRIBUTE:
				return getSignedZerosAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__SIMPLE_EXPRESSION_RANGE:
				return getSimpleExpressionRange() != null;
			case AdaPackage.DOCUMENT_ROOT__SINGLE_PROTECTED_DECLARATION:
				return getSingleProtectedDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__SINGLE_TASK_DECLARATION:
				return getSingleTaskDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__SIZE_ATTRIBUTE:
				return getSizeAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__SLICE:
				return getSlice() != null;
			case AdaPackage.DOCUMENT_ROOT__SMALL_ATTRIBUTE:
				return getSmallAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__STORAGE_POOL_ATTRIBUTE:
				return getStoragePoolAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__STORAGE_SIZE_ATTRIBUTE:
				return getStorageSizeAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__STORAGE_SIZE_PRAGMA:
				return getStorageSizePragma() != null;
			case AdaPackage.DOCUMENT_ROOT__STREAM_SIZE_ATTRIBUTE:
				return getStreamSizeAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__STRING_LITERAL:
				return getStringLiteral() != null;
			case AdaPackage.DOCUMENT_ROOT__SUBTYPE_DECLARATION:
				return getSubtypeDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__SUBTYPE_INDICATION:
				return getSubtypeIndication() != null;
			case AdaPackage.DOCUMENT_ROOT__SUCC_ATTRIBUTE:
				return getSuccAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__SUPPRESS_PRAGMA:
				return getSuppressPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__SYNCHRONIZED:
				return getSynchronized() != null;
			case AdaPackage.DOCUMENT_ROOT__SYNCHRONIZED_INTERFACE:
				return getSynchronizedInterface() != null;
			case AdaPackage.DOCUMENT_ROOT__TAG_ATTRIBUTE:
				return getTagAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__TAGGED:
				return getTagged() != null;
			case AdaPackage.DOCUMENT_ROOT__TAGGED_INCOMPLETE_TYPE_DECLARATION:
				return getTaggedIncompleteTypeDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__TAGGED_PRIVATE_TYPE_DEFINITION:
				return getTaggedPrivateTypeDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__TAGGED_RECORD_TYPE_DEFINITION:
				return getTaggedRecordTypeDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__TASK_BODY_DECLARATION:
				return getTaskBodyDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__TASK_BODY_STUB:
				return getTaskBodyStub() != null;
			case AdaPackage.DOCUMENT_ROOT__TASK_DEFINITION:
				return getTaskDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__TASK_DISPATCHING_POLICY_PRAGMA:
				return getTaskDispatchingPolicyPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__TASK_INTERFACE:
				return getTaskInterface() != null;
			case AdaPackage.DOCUMENT_ROOT__TASK_TYPE_DECLARATION:
				return getTaskTypeDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__TERMINATE_ALTERNATIVE_STATEMENT:
				return getTerminateAlternativeStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__TERMINATED_ATTRIBUTE:
				return getTerminatedAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__THEN_ABORT_PATH:
				return getThenAbortPath() != null;
			case AdaPackage.DOCUMENT_ROOT__TIMED_ENTRY_CALL_STATEMENT:
				return getTimedEntryCallStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__TRUNCATION_ATTRIBUTE:
				return getTruncationAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__TYPE_CONVERSION:
				return getTypeConversion() != null;
			case AdaPackage.DOCUMENT_ROOT__UNARY_MINUS_OPERATOR:
				return getUnaryMinusOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__UNARY_PLUS_OPERATOR:
				return getUnaryPlusOperator() != null;
			case AdaPackage.DOCUMENT_ROOT__UNBIASED_ROUNDING_ATTRIBUTE:
				return getUnbiasedRoundingAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__UNCHECKED_ACCESS_ATTRIBUTE:
				return getUncheckedAccessAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__UNCHECKED_UNION_PRAGMA:
				return getUncheckedUnionPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__UNCONSTRAINED_ARRAY_DEFINITION:
				return getUnconstrainedArrayDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__UNIVERSAL_FIXED_DEFINITION:
				return getUniversalFixedDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__UNIVERSAL_INTEGER_DEFINITION:
				return getUniversalIntegerDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__UNIVERSAL_REAL_DEFINITION:
				return getUniversalRealDefinition() != null;
			case AdaPackage.DOCUMENT_ROOT__UNKNOWN_ATTRIBUTE:
				return getUnknownAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__UNKNOWN_DISCRIMINANT_PART:
				return getUnknownDiscriminantPart() != null;
			case AdaPackage.DOCUMENT_ROOT__UNKNOWN_PRAGMA:
				return getUnknownPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__UNSUPPRESS_PRAGMA:
				return getUnsuppressPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__USE_ALL_TYPE_CLAUSE:
				return getUseAllTypeClause() != null;
			case AdaPackage.DOCUMENT_ROOT__USE_PACKAGE_CLAUSE:
				return getUsePackageClause() != null;
			case AdaPackage.DOCUMENT_ROOT__USE_TYPE_CLAUSE:
				return getUseTypeClause() != null;
			case AdaPackage.DOCUMENT_ROOT__VAL_ATTRIBUTE:
				return getValAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__VALID_ATTRIBUTE:
				return getValidAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__VALUE_ATTRIBUTE:
				return getValueAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__VARIABLE_DECLARATION:
				return getVariableDeclaration() != null;
			case AdaPackage.DOCUMENT_ROOT__VARIANT:
				return getVariant() != null;
			case AdaPackage.DOCUMENT_ROOT__VARIANT_PART:
				return getVariantPart() != null;
			case AdaPackage.DOCUMENT_ROOT__VERSION_ATTRIBUTE:
				return getVersionAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__VOLATILE_COMPONENTS_PRAGMA:
				return getVolatileComponentsPragma() != null;
			case AdaPackage.DOCUMENT_ROOT__VOLATILE_PRAGMA:
				return getVolatilePragma() != null;
			case AdaPackage.DOCUMENT_ROOT__WHILE_LOOP_STATEMENT:
				return getWhileLoopStatement() != null;
			case AdaPackage.DOCUMENT_ROOT__WIDE_IMAGE_ATTRIBUTE:
				return getWideImageAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__WIDE_VALUE_ATTRIBUTE:
				return getWideValueAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__WIDE_WIDE_IMAGE_ATTRIBUTE:
				return getWideWideImageAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__WIDE_WIDE_VALUE_ATTRIBUTE:
				return getWideWideValueAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__WIDE_WIDE_WIDTH_ATTRIBUTE:
				return getWideWideWidthAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__WIDE_WIDTH_ATTRIBUTE:
				return getWideWidthAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__WIDTH_ATTRIBUTE:
				return getWidthAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__WITH_CLAUSE:
				return getWithClause() != null;
			case AdaPackage.DOCUMENT_ROOT__WRITE_ATTRIBUTE:
				return getWriteAttribute() != null;
			case AdaPackage.DOCUMENT_ROOT__XOR_OPERATOR:
				return getXorOperator() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (mixed: ");
		result.append(mixed);
		result.append(')');
		return result.toString();
	}

} //DocumentRootImpl
