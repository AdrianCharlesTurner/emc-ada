/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.IsNotNullReturnQType4;
import Ada.NotAnElement;
import Ada.NotNullReturn;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Is Not Null Return QType4</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.IsNotNullReturnQType4Impl#getNotNullReturn <em>Not Null Return</em>}</li>
 *   <li>{@link Ada.impl.IsNotNullReturnQType4Impl#getNotAnElement <em>Not An Element</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IsNotNullReturnQType4Impl extends MinimalEObjectImpl.Container implements IsNotNullReturnQType4 {
	/**
	 * The cached value of the '{@link #getNotNullReturn() <em>Not Null Return</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotNullReturn()
	 * @generated
	 * @ordered
	 */
	protected NotNullReturn notNullReturn;

	/**
	 * The cached value of the '{@link #getNotAnElement() <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotAnElement()
	 * @generated
	 * @ordered
	 */
	protected NotAnElement notAnElement;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IsNotNullReturnQType4Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getIsNotNullReturnQType4();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotNullReturn getNotNullReturn() {
		return notNullReturn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNotNullReturn(NotNullReturn newNotNullReturn, NotificationChain msgs) {
		NotNullReturn oldNotNullReturn = notNullReturn;
		notNullReturn = newNotNullReturn;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.IS_NOT_NULL_RETURN_QTYPE4__NOT_NULL_RETURN, oldNotNullReturn, newNotNullReturn);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotNullReturn(NotNullReturn newNotNullReturn) {
		if (newNotNullReturn != notNullReturn) {
			NotificationChain msgs = null;
			if (notNullReturn != null)
				msgs = ((InternalEObject)notNullReturn).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.IS_NOT_NULL_RETURN_QTYPE4__NOT_NULL_RETURN, null, msgs);
			if (newNotNullReturn != null)
				msgs = ((InternalEObject)newNotNullReturn).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.IS_NOT_NULL_RETURN_QTYPE4__NOT_NULL_RETURN, null, msgs);
			msgs = basicSetNotNullReturn(newNotNullReturn, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.IS_NOT_NULL_RETURN_QTYPE4__NOT_NULL_RETURN, newNotNullReturn, newNotNullReturn));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotAnElement getNotAnElement() {
		return notAnElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNotAnElement(NotAnElement newNotAnElement, NotificationChain msgs) {
		NotAnElement oldNotAnElement = notAnElement;
		notAnElement = newNotAnElement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.IS_NOT_NULL_RETURN_QTYPE4__NOT_AN_ELEMENT, oldNotAnElement, newNotAnElement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotAnElement(NotAnElement newNotAnElement) {
		if (newNotAnElement != notAnElement) {
			NotificationChain msgs = null;
			if (notAnElement != null)
				msgs = ((InternalEObject)notAnElement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.IS_NOT_NULL_RETURN_QTYPE4__NOT_AN_ELEMENT, null, msgs);
			if (newNotAnElement != null)
				msgs = ((InternalEObject)newNotAnElement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.IS_NOT_NULL_RETURN_QTYPE4__NOT_AN_ELEMENT, null, msgs);
			msgs = basicSetNotAnElement(newNotAnElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.IS_NOT_NULL_RETURN_QTYPE4__NOT_AN_ELEMENT, newNotAnElement, newNotAnElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE4__NOT_NULL_RETURN:
				return basicSetNotNullReturn(null, msgs);
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE4__NOT_AN_ELEMENT:
				return basicSetNotAnElement(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE4__NOT_NULL_RETURN:
				return getNotNullReturn();
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE4__NOT_AN_ELEMENT:
				return getNotAnElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE4__NOT_NULL_RETURN:
				setNotNullReturn((NotNullReturn)newValue);
				return;
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE4__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE4__NOT_NULL_RETURN:
				setNotNullReturn((NotNullReturn)null);
				return;
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE4__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE4__NOT_NULL_RETURN:
				return notNullReturn != null;
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE4__NOT_AN_ELEMENT:
				return notAnElement != null;
		}
		return super.eIsSet(featureID);
	}

} //IsNotNullReturnQType4Impl
