/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.ExpressionClass;
import Ada.ExpressionList;
import Ada.IndexedComponent;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Indexed Component</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.IndexedComponentImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.IndexedComponentImpl#getPrefixQ <em>Prefix Q</em>}</li>
 *   <li>{@link Ada.impl.IndexedComponentImpl#getIndexExpressionsQl <em>Index Expressions Ql</em>}</li>
 *   <li>{@link Ada.impl.IndexedComponentImpl#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.impl.IndexedComponentImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IndexedComponentImpl extends MinimalEObjectImpl.Container implements IndexedComponent {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getPrefixQ() <em>Prefix Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrefixQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass prefixQ;

	/**
	 * The cached value of the '{@link #getIndexExpressionsQl() <em>Index Expressions Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndexExpressionsQl()
	 * @generated
	 * @ordered
	 */
	protected ExpressionList indexExpressionsQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IndexedComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getIndexedComponent();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.INDEXED_COMPONENT__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.INDEXED_COMPONENT__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.INDEXED_COMPONENT__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.INDEXED_COMPONENT__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getPrefixQ() {
		return prefixQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrefixQ(ExpressionClass newPrefixQ, NotificationChain msgs) {
		ExpressionClass oldPrefixQ = prefixQ;
		prefixQ = newPrefixQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.INDEXED_COMPONENT__PREFIX_Q, oldPrefixQ, newPrefixQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrefixQ(ExpressionClass newPrefixQ) {
		if (newPrefixQ != prefixQ) {
			NotificationChain msgs = null;
			if (prefixQ != null)
				msgs = ((InternalEObject)prefixQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.INDEXED_COMPONENT__PREFIX_Q, null, msgs);
			if (newPrefixQ != null)
				msgs = ((InternalEObject)newPrefixQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.INDEXED_COMPONENT__PREFIX_Q, null, msgs);
			msgs = basicSetPrefixQ(newPrefixQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.INDEXED_COMPONENT__PREFIX_Q, newPrefixQ, newPrefixQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionList getIndexExpressionsQl() {
		return indexExpressionsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIndexExpressionsQl(ExpressionList newIndexExpressionsQl, NotificationChain msgs) {
		ExpressionList oldIndexExpressionsQl = indexExpressionsQl;
		indexExpressionsQl = newIndexExpressionsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.INDEXED_COMPONENT__INDEX_EXPRESSIONS_QL, oldIndexExpressionsQl, newIndexExpressionsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndexExpressionsQl(ExpressionList newIndexExpressionsQl) {
		if (newIndexExpressionsQl != indexExpressionsQl) {
			NotificationChain msgs = null;
			if (indexExpressionsQl != null)
				msgs = ((InternalEObject)indexExpressionsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.INDEXED_COMPONENT__INDEX_EXPRESSIONS_QL, null, msgs);
			if (newIndexExpressionsQl != null)
				msgs = ((InternalEObject)newIndexExpressionsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.INDEXED_COMPONENT__INDEX_EXPRESSIONS_QL, null, msgs);
			msgs = basicSetIndexExpressionsQl(newIndexExpressionsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.INDEXED_COMPONENT__INDEX_EXPRESSIONS_QL, newIndexExpressionsQl, newIndexExpressionsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.INDEXED_COMPONENT__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.INDEXED_COMPONENT__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.INDEXED_COMPONENT__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.INDEXED_COMPONENT__PREFIX_Q:
				return basicSetPrefixQ(null, msgs);
			case AdaPackage.INDEXED_COMPONENT__INDEX_EXPRESSIONS_QL:
				return basicSetIndexExpressionsQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.INDEXED_COMPONENT__SLOC:
				return getSloc();
			case AdaPackage.INDEXED_COMPONENT__PREFIX_Q:
				return getPrefixQ();
			case AdaPackage.INDEXED_COMPONENT__INDEX_EXPRESSIONS_QL:
				return getIndexExpressionsQl();
			case AdaPackage.INDEXED_COMPONENT__CHECKS:
				return getChecks();
			case AdaPackage.INDEXED_COMPONENT__TYPE:
				return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.INDEXED_COMPONENT__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.INDEXED_COMPONENT__PREFIX_Q:
				setPrefixQ((ExpressionClass)newValue);
				return;
			case AdaPackage.INDEXED_COMPONENT__INDEX_EXPRESSIONS_QL:
				setIndexExpressionsQl((ExpressionList)newValue);
				return;
			case AdaPackage.INDEXED_COMPONENT__CHECKS:
				setChecks((String)newValue);
				return;
			case AdaPackage.INDEXED_COMPONENT__TYPE:
				setType((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.INDEXED_COMPONENT__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.INDEXED_COMPONENT__PREFIX_Q:
				setPrefixQ((ExpressionClass)null);
				return;
			case AdaPackage.INDEXED_COMPONENT__INDEX_EXPRESSIONS_QL:
				setIndexExpressionsQl((ExpressionList)null);
				return;
			case AdaPackage.INDEXED_COMPONENT__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
			case AdaPackage.INDEXED_COMPONENT__TYPE:
				setType(TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.INDEXED_COMPONENT__SLOC:
				return sloc != null;
			case AdaPackage.INDEXED_COMPONENT__PREFIX_Q:
				return prefixQ != null;
			case AdaPackage.INDEXED_COMPONENT__INDEX_EXPRESSIONS_QL:
				return indexExpressionsQl != null;
			case AdaPackage.INDEXED_COMPONENT__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
			case AdaPackage.INDEXED_COMPONENT__TYPE:
				return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(", type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //IndexedComponentImpl
