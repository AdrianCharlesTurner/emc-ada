/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Has Aliased QType8</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.HasAliasedQType8#getAliased <em>Aliased</em>}</li>
 *   <li>{@link Ada.HasAliasedQType8#getNotAnElement <em>Not An Element</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getHasAliasedQType8()
 * @model extendedMetaData="name='has_aliased_q_._8_._type' kind='elementOnly'"
 * @generated
 */
public interface HasAliasedQType8 extends EObject {
	/**
	 * Returns the value of the '<em><b>Aliased</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aliased</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aliased</em>' containment reference.
	 * @see #setAliased(Aliased)
	 * @see Ada.AdaPackage#getHasAliasedQType8_Aliased()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='aliased' namespace='##targetNamespace'"
	 * @generated
	 */
	Aliased getAliased();

	/**
	 * Sets the value of the '{@link Ada.HasAliasedQType8#getAliased <em>Aliased</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aliased</em>' containment reference.
	 * @see #getAliased()
	 * @generated
	 */
	void setAliased(Aliased value);

	/**
	 * Returns the value of the '<em><b>Not An Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not An Element</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not An Element</em>' containment reference.
	 * @see #setNotAnElement(NotAnElement)
	 * @see Ada.AdaPackage#getHasAliasedQType8_NotAnElement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not_an_element' namespace='##targetNamespace'"
	 * @generated
	 */
	NotAnElement getNotAnElement();

	/**
	 * Sets the value of the '{@link Ada.HasAliasedQType8#getNotAnElement <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not An Element</em>' containment reference.
	 * @see #getNotAnElement()
	 * @generated
	 */
	void setNotAnElement(NotAnElement value);

} // HasAliasedQType8
