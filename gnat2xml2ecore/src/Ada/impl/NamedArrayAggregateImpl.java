/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.AssociationList;
import Ada.NamedArrayAggregate;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Named Array Aggregate</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.NamedArrayAggregateImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.NamedArrayAggregateImpl#getArrayComponentAssociationsQl <em>Array Component Associations Ql</em>}</li>
 *   <li>{@link Ada.impl.NamedArrayAggregateImpl#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.impl.NamedArrayAggregateImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NamedArrayAggregateImpl extends MinimalEObjectImpl.Container implements NamedArrayAggregate {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getArrayComponentAssociationsQl() <em>Array Component Associations Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArrayComponentAssociationsQl()
	 * @generated
	 * @ordered
	 */
	protected AssociationList arrayComponentAssociationsQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NamedArrayAggregateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getNamedArrayAggregate();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAMED_ARRAY_AGGREGATE__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAMED_ARRAY_AGGREGATE__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAMED_ARRAY_AGGREGATE__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAMED_ARRAY_AGGREGATE__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationList getArrayComponentAssociationsQl() {
		return arrayComponentAssociationsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetArrayComponentAssociationsQl(AssociationList newArrayComponentAssociationsQl, NotificationChain msgs) {
		AssociationList oldArrayComponentAssociationsQl = arrayComponentAssociationsQl;
		arrayComponentAssociationsQl = newArrayComponentAssociationsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAMED_ARRAY_AGGREGATE__ARRAY_COMPONENT_ASSOCIATIONS_QL, oldArrayComponentAssociationsQl, newArrayComponentAssociationsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setArrayComponentAssociationsQl(AssociationList newArrayComponentAssociationsQl) {
		if (newArrayComponentAssociationsQl != arrayComponentAssociationsQl) {
			NotificationChain msgs = null;
			if (arrayComponentAssociationsQl != null)
				msgs = ((InternalEObject)arrayComponentAssociationsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAMED_ARRAY_AGGREGATE__ARRAY_COMPONENT_ASSOCIATIONS_QL, null, msgs);
			if (newArrayComponentAssociationsQl != null)
				msgs = ((InternalEObject)newArrayComponentAssociationsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAMED_ARRAY_AGGREGATE__ARRAY_COMPONENT_ASSOCIATIONS_QL, null, msgs);
			msgs = basicSetArrayComponentAssociationsQl(newArrayComponentAssociationsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAMED_ARRAY_AGGREGATE__ARRAY_COMPONENT_ASSOCIATIONS_QL, newArrayComponentAssociationsQl, newArrayComponentAssociationsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAMED_ARRAY_AGGREGATE__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAMED_ARRAY_AGGREGATE__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.NAMED_ARRAY_AGGREGATE__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.NAMED_ARRAY_AGGREGATE__ARRAY_COMPONENT_ASSOCIATIONS_QL:
				return basicSetArrayComponentAssociationsQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.NAMED_ARRAY_AGGREGATE__SLOC:
				return getSloc();
			case AdaPackage.NAMED_ARRAY_AGGREGATE__ARRAY_COMPONENT_ASSOCIATIONS_QL:
				return getArrayComponentAssociationsQl();
			case AdaPackage.NAMED_ARRAY_AGGREGATE__CHECKS:
				return getChecks();
			case AdaPackage.NAMED_ARRAY_AGGREGATE__TYPE:
				return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.NAMED_ARRAY_AGGREGATE__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.NAMED_ARRAY_AGGREGATE__ARRAY_COMPONENT_ASSOCIATIONS_QL:
				setArrayComponentAssociationsQl((AssociationList)newValue);
				return;
			case AdaPackage.NAMED_ARRAY_AGGREGATE__CHECKS:
				setChecks((String)newValue);
				return;
			case AdaPackage.NAMED_ARRAY_AGGREGATE__TYPE:
				setType((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.NAMED_ARRAY_AGGREGATE__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.NAMED_ARRAY_AGGREGATE__ARRAY_COMPONENT_ASSOCIATIONS_QL:
				setArrayComponentAssociationsQl((AssociationList)null);
				return;
			case AdaPackage.NAMED_ARRAY_AGGREGATE__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
			case AdaPackage.NAMED_ARRAY_AGGREGATE__TYPE:
				setType(TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.NAMED_ARRAY_AGGREGATE__SLOC:
				return sloc != null;
			case AdaPackage.NAMED_ARRAY_AGGREGATE__ARRAY_COMPONENT_ASSOCIATIONS_QL:
				return arrayComponentAssociationsQl != null;
			case AdaPackage.NAMED_ARRAY_AGGREGATE__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
			case AdaPackage.NAMED_ARRAY_AGGREGATE__TYPE:
				return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(", type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //NamedArrayAggregateImpl
