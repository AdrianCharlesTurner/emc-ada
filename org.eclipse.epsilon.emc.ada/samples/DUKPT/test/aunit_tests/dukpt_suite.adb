with DUKPT.Test;           use DUKPT.Test;
with DUKPT.Utilities.Test; use DUKPT.Utilities.Test;
with AUnit.Test_Caller;

package body DUKPT_Suite is

   package Caller is new AUnit.Test_Caller (DUKPT.Test.Initial_Test_Data);

   function Suite return Access_Test_Suite is
      Ret : constant Access_Test_Suite := new Test_Suite;
   begin
      Ret.Add_Test
        (Caller.Create ("Basic IPEK Derivation", Test_Derive_IPEK_001'Access));
      Ret.Add_Test
        (Caller.Create ("Match Key 1 Encryption and Decryption", Test_DUKPT_Key_1'Access));
      Ret.Add_Test
        (Caller.Create ("Match Key 2 Encryption and Decryption", Test_DUKPT_Key_2'Access));
      Ret.Add_Test
        (Caller.Create ("Match Key 10 Encryption and Decryption", Test_DUKPT_Key_10'Access));
      Ret.Add_Test
        (Caller.Create ("Match Key 2097151 (21-bit max) Encryption and Decryption", Test_DUKPT_Key_2097151'Access));
      return Ret;
   end Suite;

end DUKPT_Suite;
