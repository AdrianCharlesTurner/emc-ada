/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Has Null Exclusion QType1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.HasNullExclusionQType1#getNullExclusion <em>Null Exclusion</em>}</li>
 *   <li>{@link Ada.HasNullExclusionQType1#getNotAnElement <em>Not An Element</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getHasNullExclusionQType1()
 * @model extendedMetaData="name='has_null_exclusion_q_._1_._type' kind='elementOnly'"
 * @generated
 */
public interface HasNullExclusionQType1 extends EObject {
	/**
	 * Returns the value of the '<em><b>Null Exclusion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Null Exclusion</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Null Exclusion</em>' containment reference.
	 * @see #setNullExclusion(NullExclusion)
	 * @see Ada.AdaPackage#getHasNullExclusionQType1_NullExclusion()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='null_exclusion' namespace='##targetNamespace'"
	 * @generated
	 */
	NullExclusion getNullExclusion();

	/**
	 * Sets the value of the '{@link Ada.HasNullExclusionQType1#getNullExclusion <em>Null Exclusion</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Null Exclusion</em>' containment reference.
	 * @see #getNullExclusion()
	 * @generated
	 */
	void setNullExclusion(NullExclusion value);

	/**
	 * Returns the value of the '<em><b>Not An Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not An Element</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not An Element</em>' containment reference.
	 * @see #setNotAnElement(NotAnElement)
	 * @see Ada.AdaPackage#getHasNullExclusionQType1_NotAnElement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not_an_element' namespace='##targetNamespace'"
	 * @generated
	 */
	NotAnElement getNotAnElement();

	/**
	 * Sets the value of the '{@link Ada.HasNullExclusionQType1#getNotAnElement <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not An Element</em>' containment reference.
	 * @see #getNotAnElement()
	 * @generated
	 */
	void setNotAnElement(NotAnElement value);

} // HasNullExclusionQType1
