/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unary Plus Operator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.UnaryPlusOperator#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.UnaryPlusOperator#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.UnaryPlusOperator#getRef <em>Ref</em>}</li>
 *   <li>{@link Ada.UnaryPlusOperator#getRefName <em>Ref Name</em>}</li>
 *   <li>{@link Ada.UnaryPlusOperator#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getUnaryPlusOperator()
 * @model extendedMetaData="name='Unary_Plus_Operator' kind='elementOnly'"
 * @generated
 */
public interface UnaryPlusOperator extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getUnaryPlusOperator_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.UnaryPlusOperator#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getUnaryPlusOperator_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.UnaryPlusOperator#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

	/**
	 * Returns the value of the '<em><b>Ref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ref</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ref</em>' attribute.
	 * @see #setRef(String)
	 * @see Ada.AdaPackage#getUnaryPlusOperator_Ref()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='ref' namespace='##targetNamespace'"
	 * @generated
	 */
	String getRef();

	/**
	 * Sets the value of the '{@link Ada.UnaryPlusOperator#getRef <em>Ref</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ref</em>' attribute.
	 * @see #getRef()
	 * @generated
	 */
	void setRef(String value);

	/**
	 * Returns the value of the '<em><b>Ref Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ref Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ref Name</em>' attribute.
	 * @see #setRefName(String)
	 * @see Ada.AdaPackage#getUnaryPlusOperator_RefName()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='ref_name' namespace='##targetNamespace'"
	 * @generated
	 */
	String getRefName();

	/**
	 * Sets the value of the '{@link Ada.UnaryPlusOperator#getRefName <em>Ref Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ref Name</em>' attribute.
	 * @see #getRefName()
	 * @generated
	 */
	void setRefName(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see Ada.AdaPackage#getUnaryPlusOperator_Type()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='type' namespace='##targetNamespace'"
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link Ada.UnaryPlusOperator#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

} // UnaryPlusOperator
