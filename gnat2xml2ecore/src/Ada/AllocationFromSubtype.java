/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Allocation From Subtype</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.AllocationFromSubtype#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.AllocationFromSubtype#getSubpoolNameQ <em>Subpool Name Q</em>}</li>
 *   <li>{@link Ada.AllocationFromSubtype#getAllocatorSubtypeIndicationQ <em>Allocator Subtype Indication Q</em>}</li>
 *   <li>{@link Ada.AllocationFromSubtype#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.AllocationFromSubtype#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getAllocationFromSubtype()
 * @model extendedMetaData="name='Allocation_From_Subtype' kind='elementOnly'"
 * @generated
 */
public interface AllocationFromSubtype extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getAllocationFromSubtype_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.AllocationFromSubtype#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Subpool Name Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subpool Name Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subpool Name Q</em>' containment reference.
	 * @see #setSubpoolNameQ(ExpressionClass)
	 * @see Ada.AdaPackage#getAllocationFromSubtype_SubpoolNameQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='subpool_name_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getSubpoolNameQ();

	/**
	 * Sets the value of the '{@link Ada.AllocationFromSubtype#getSubpoolNameQ <em>Subpool Name Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Subpool Name Q</em>' containment reference.
	 * @see #getSubpoolNameQ()
	 * @generated
	 */
	void setSubpoolNameQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Allocator Subtype Indication Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Allocator Subtype Indication Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Allocator Subtype Indication Q</em>' containment reference.
	 * @see #setAllocatorSubtypeIndicationQ(ElementClass)
	 * @see Ada.AdaPackage#getAllocationFromSubtype_AllocatorSubtypeIndicationQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='allocator_subtype_indication_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementClass getAllocatorSubtypeIndicationQ();

	/**
	 * Sets the value of the '{@link Ada.AllocationFromSubtype#getAllocatorSubtypeIndicationQ <em>Allocator Subtype Indication Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Allocator Subtype Indication Q</em>' containment reference.
	 * @see #getAllocatorSubtypeIndicationQ()
	 * @generated
	 */
	void setAllocatorSubtypeIndicationQ(ElementClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getAllocationFromSubtype_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.AllocationFromSubtype#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see Ada.AdaPackage#getAllocationFromSubtype_Type()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='type' namespace='##targetNamespace'"
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link Ada.AllocationFromSubtype#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

} // AllocationFromSubtype
