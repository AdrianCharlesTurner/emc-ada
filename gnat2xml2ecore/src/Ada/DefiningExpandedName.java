/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Defining Expanded Name</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.DefiningExpandedName#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.DefiningExpandedName#getDefiningPrefixQ <em>Defining Prefix Q</em>}</li>
 *   <li>{@link Ada.DefiningExpandedName#getDefiningSelectorQ <em>Defining Selector Q</em>}</li>
 *   <li>{@link Ada.DefiningExpandedName#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.DefiningExpandedName#getDef <em>Def</em>}</li>
 *   <li>{@link Ada.DefiningExpandedName#getDefName <em>Def Name</em>}</li>
 *   <li>{@link Ada.DefiningExpandedName#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getDefiningExpandedName()
 * @model extendedMetaData="name='Defining_Expanded_Name' kind='elementOnly'"
 * @generated
 */
public interface DefiningExpandedName extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getDefiningExpandedName_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.DefiningExpandedName#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Defining Prefix Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Prefix Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Prefix Q</em>' containment reference.
	 * @see #setDefiningPrefixQ(NameClass)
	 * @see Ada.AdaPackage#getDefiningExpandedName_DefiningPrefixQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='defining_prefix_q' namespace='##targetNamespace'"
	 * @generated
	 */
	NameClass getDefiningPrefixQ();

	/**
	 * Sets the value of the '{@link Ada.DefiningExpandedName#getDefiningPrefixQ <em>Defining Prefix Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Prefix Q</em>' containment reference.
	 * @see #getDefiningPrefixQ()
	 * @generated
	 */
	void setDefiningPrefixQ(NameClass value);

	/**
	 * Returns the value of the '<em><b>Defining Selector Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Selector Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Selector Q</em>' containment reference.
	 * @see #setDefiningSelectorQ(DefiningNameClass)
	 * @see Ada.AdaPackage#getDefiningExpandedName_DefiningSelectorQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='defining_selector_q' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameClass getDefiningSelectorQ();

	/**
	 * Sets the value of the '{@link Ada.DefiningExpandedName#getDefiningSelectorQ <em>Defining Selector Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Selector Q</em>' containment reference.
	 * @see #getDefiningSelectorQ()
	 * @generated
	 */
	void setDefiningSelectorQ(DefiningNameClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getDefiningExpandedName_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.DefiningExpandedName#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

	/**
	 * Returns the value of the '<em><b>Def</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Def</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Def</em>' attribute.
	 * @see #setDef(String)
	 * @see Ada.AdaPackage#getDefiningExpandedName_Def()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='def' namespace='##targetNamespace'"
	 * @generated
	 */
	String getDef();

	/**
	 * Sets the value of the '{@link Ada.DefiningExpandedName#getDef <em>Def</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Def</em>' attribute.
	 * @see #getDef()
	 * @generated
	 */
	void setDef(String value);

	/**
	 * Returns the value of the '<em><b>Def Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Def Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Def Name</em>' attribute.
	 * @see #setDefName(String)
	 * @see Ada.AdaPackage#getDefiningExpandedName_DefName()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='def_name' namespace='##targetNamespace'"
	 * @generated
	 */
	String getDefName();

	/**
	 * Sets the value of the '{@link Ada.DefiningExpandedName#getDefName <em>Def Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Def Name</em>' attribute.
	 * @see #getDefName()
	 * @generated
	 */
	void setDefName(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see Ada.AdaPackage#getDefiningExpandedName_Type()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='type' namespace='##targetNamespace'"
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link Ada.DefiningExpandedName#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

} // DefiningExpandedName
