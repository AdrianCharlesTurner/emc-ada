/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Case Expression Path</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.CaseExpressionPath#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.CaseExpressionPath#getCasePathAlternativeChoicesQl <em>Case Path Alternative Choices Ql</em>}</li>
 *   <li>{@link Ada.CaseExpressionPath#getDependentExpressionQ <em>Dependent Expression Q</em>}</li>
 *   <li>{@link Ada.CaseExpressionPath#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getCaseExpressionPath()
 * @model extendedMetaData="name='Case_Expression_Path' kind='elementOnly'"
 * @generated
 */
public interface CaseExpressionPath extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getCaseExpressionPath_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.CaseExpressionPath#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Case Path Alternative Choices Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Case Path Alternative Choices Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Case Path Alternative Choices Ql</em>' containment reference.
	 * @see #setCasePathAlternativeChoicesQl(ElementList)
	 * @see Ada.AdaPackage#getCaseExpressionPath_CasePathAlternativeChoicesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='case_path_alternative_choices_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementList getCasePathAlternativeChoicesQl();

	/**
	 * Sets the value of the '{@link Ada.CaseExpressionPath#getCasePathAlternativeChoicesQl <em>Case Path Alternative Choices Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Case Path Alternative Choices Ql</em>' containment reference.
	 * @see #getCasePathAlternativeChoicesQl()
	 * @generated
	 */
	void setCasePathAlternativeChoicesQl(ElementList value);

	/**
	 * Returns the value of the '<em><b>Dependent Expression Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dependent Expression Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dependent Expression Q</em>' containment reference.
	 * @see #setDependentExpressionQ(ExpressionClass)
	 * @see Ada.AdaPackage#getCaseExpressionPath_DependentExpressionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='dependent_expression_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getDependentExpressionQ();

	/**
	 * Sets the value of the '{@link Ada.CaseExpressionPath#getDependentExpressionQ <em>Dependent Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dependent Expression Q</em>' containment reference.
	 * @see #getDependentExpressionQ()
	 * @generated
	 */
	void setDependentExpressionQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getCaseExpressionPath_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.CaseExpressionPath#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // CaseExpressionPath
