/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Has Tagged QType</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.HasTaggedQType#getTagged <em>Tagged</em>}</li>
 *   <li>{@link Ada.HasTaggedQType#getNotAnElement <em>Not An Element</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getHasTaggedQType()
 * @model extendedMetaData="name='has_tagged_q_._type' kind='elementOnly'"
 * @generated
 */
public interface HasTaggedQType extends EObject {
	/**
	 * Returns the value of the '<em><b>Tagged</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tagged</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tagged</em>' containment reference.
	 * @see #setTagged(Tagged)
	 * @see Ada.AdaPackage#getHasTaggedQType_Tagged()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='tagged' namespace='##targetNamespace'"
	 * @generated
	 */
	Tagged getTagged();

	/**
	 * Sets the value of the '{@link Ada.HasTaggedQType#getTagged <em>Tagged</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tagged</em>' containment reference.
	 * @see #getTagged()
	 * @generated
	 */
	void setTagged(Tagged value);

	/**
	 * Returns the value of the '<em><b>Not An Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not An Element</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not An Element</em>' containment reference.
	 * @see #setNotAnElement(NotAnElement)
	 * @see Ada.AdaPackage#getHasTaggedQType_NotAnElement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not_an_element' namespace='##targetNamespace'"
	 * @generated
	 */
	NotAnElement getNotAnElement();

	/**
	 * Sets the value of the '{@link Ada.HasTaggedQType#getNotAnElement <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not An Element</em>' containment reference.
	 * @see #getNotAnElement()
	 * @generated
	 */
	void setNotAnElement(NotAnElement value);

} // HasTaggedQType
