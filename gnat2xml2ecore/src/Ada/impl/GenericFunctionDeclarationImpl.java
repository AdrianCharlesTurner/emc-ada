/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.DefiningNameList;
import Ada.ElementClass;
import Ada.ElementList;
import Ada.GenericFunctionDeclaration;
import Ada.IsNotNullReturnQType3;
import Ada.ParameterSpecificationList;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Generic Function Declaration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.GenericFunctionDeclarationImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.GenericFunctionDeclarationImpl#getGenericFormalPartQl <em>Generic Formal Part Ql</em>}</li>
 *   <li>{@link Ada.impl.GenericFunctionDeclarationImpl#getNamesQl <em>Names Ql</em>}</li>
 *   <li>{@link Ada.impl.GenericFunctionDeclarationImpl#getParameterProfileQl <em>Parameter Profile Ql</em>}</li>
 *   <li>{@link Ada.impl.GenericFunctionDeclarationImpl#getIsNotNullReturnQ <em>Is Not Null Return Q</em>}</li>
 *   <li>{@link Ada.impl.GenericFunctionDeclarationImpl#getResultProfileQ <em>Result Profile Q</em>}</li>
 *   <li>{@link Ada.impl.GenericFunctionDeclarationImpl#getAspectSpecificationsQl <em>Aspect Specifications Ql</em>}</li>
 *   <li>{@link Ada.impl.GenericFunctionDeclarationImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GenericFunctionDeclarationImpl extends MinimalEObjectImpl.Container implements GenericFunctionDeclaration {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getGenericFormalPartQl() <em>Generic Formal Part Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGenericFormalPartQl()
	 * @generated
	 * @ordered
	 */
	protected ElementList genericFormalPartQl;

	/**
	 * The cached value of the '{@link #getNamesQl() <em>Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamesQl()
	 * @generated
	 * @ordered
	 */
	protected DefiningNameList namesQl;

	/**
	 * The cached value of the '{@link #getParameterProfileQl() <em>Parameter Profile Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterProfileQl()
	 * @generated
	 * @ordered
	 */
	protected ParameterSpecificationList parameterProfileQl;

	/**
	 * The cached value of the '{@link #getIsNotNullReturnQ() <em>Is Not Null Return Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsNotNullReturnQ()
	 * @generated
	 * @ordered
	 */
	protected IsNotNullReturnQType3 isNotNullReturnQ;

	/**
	 * The cached value of the '{@link #getResultProfileQ() <em>Result Profile Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResultProfileQ()
	 * @generated
	 * @ordered
	 */
	protected ElementClass resultProfileQ;

	/**
	 * The cached value of the '{@link #getAspectSpecificationsQl() <em>Aspect Specifications Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAspectSpecificationsQl()
	 * @generated
	 * @ordered
	 */
	protected ElementList aspectSpecificationsQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GenericFunctionDeclarationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getGenericFunctionDeclaration();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.GENERIC_FUNCTION_DECLARATION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.GENERIC_FUNCTION_DECLARATION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.GENERIC_FUNCTION_DECLARATION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.GENERIC_FUNCTION_DECLARATION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementList getGenericFormalPartQl() {
		return genericFormalPartQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGenericFormalPartQl(ElementList newGenericFormalPartQl, NotificationChain msgs) {
		ElementList oldGenericFormalPartQl = genericFormalPartQl;
		genericFormalPartQl = newGenericFormalPartQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.GENERIC_FUNCTION_DECLARATION__GENERIC_FORMAL_PART_QL, oldGenericFormalPartQl, newGenericFormalPartQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGenericFormalPartQl(ElementList newGenericFormalPartQl) {
		if (newGenericFormalPartQl != genericFormalPartQl) {
			NotificationChain msgs = null;
			if (genericFormalPartQl != null)
				msgs = ((InternalEObject)genericFormalPartQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.GENERIC_FUNCTION_DECLARATION__GENERIC_FORMAL_PART_QL, null, msgs);
			if (newGenericFormalPartQl != null)
				msgs = ((InternalEObject)newGenericFormalPartQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.GENERIC_FUNCTION_DECLARATION__GENERIC_FORMAL_PART_QL, null, msgs);
			msgs = basicSetGenericFormalPartQl(newGenericFormalPartQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.GENERIC_FUNCTION_DECLARATION__GENERIC_FORMAL_PART_QL, newGenericFormalPartQl, newGenericFormalPartQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNameList getNamesQl() {
		return namesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNamesQl(DefiningNameList newNamesQl, NotificationChain msgs) {
		DefiningNameList oldNamesQl = namesQl;
		namesQl = newNamesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.GENERIC_FUNCTION_DECLARATION__NAMES_QL, oldNamesQl, newNamesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNamesQl(DefiningNameList newNamesQl) {
		if (newNamesQl != namesQl) {
			NotificationChain msgs = null;
			if (namesQl != null)
				msgs = ((InternalEObject)namesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.GENERIC_FUNCTION_DECLARATION__NAMES_QL, null, msgs);
			if (newNamesQl != null)
				msgs = ((InternalEObject)newNamesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.GENERIC_FUNCTION_DECLARATION__NAMES_QL, null, msgs);
			msgs = basicSetNamesQl(newNamesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.GENERIC_FUNCTION_DECLARATION__NAMES_QL, newNamesQl, newNamesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterSpecificationList getParameterProfileQl() {
		return parameterProfileQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParameterProfileQl(ParameterSpecificationList newParameterProfileQl, NotificationChain msgs) {
		ParameterSpecificationList oldParameterProfileQl = parameterProfileQl;
		parameterProfileQl = newParameterProfileQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.GENERIC_FUNCTION_DECLARATION__PARAMETER_PROFILE_QL, oldParameterProfileQl, newParameterProfileQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterProfileQl(ParameterSpecificationList newParameterProfileQl) {
		if (newParameterProfileQl != parameterProfileQl) {
			NotificationChain msgs = null;
			if (parameterProfileQl != null)
				msgs = ((InternalEObject)parameterProfileQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.GENERIC_FUNCTION_DECLARATION__PARAMETER_PROFILE_QL, null, msgs);
			if (newParameterProfileQl != null)
				msgs = ((InternalEObject)newParameterProfileQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.GENERIC_FUNCTION_DECLARATION__PARAMETER_PROFILE_QL, null, msgs);
			msgs = basicSetParameterProfileQl(newParameterProfileQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.GENERIC_FUNCTION_DECLARATION__PARAMETER_PROFILE_QL, newParameterProfileQl, newParameterProfileQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotNullReturnQType3 getIsNotNullReturnQ() {
		return isNotNullReturnQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIsNotNullReturnQ(IsNotNullReturnQType3 newIsNotNullReturnQ, NotificationChain msgs) {
		IsNotNullReturnQType3 oldIsNotNullReturnQ = isNotNullReturnQ;
		isNotNullReturnQ = newIsNotNullReturnQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.GENERIC_FUNCTION_DECLARATION__IS_NOT_NULL_RETURN_Q, oldIsNotNullReturnQ, newIsNotNullReturnQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsNotNullReturnQ(IsNotNullReturnQType3 newIsNotNullReturnQ) {
		if (newIsNotNullReturnQ != isNotNullReturnQ) {
			NotificationChain msgs = null;
			if (isNotNullReturnQ != null)
				msgs = ((InternalEObject)isNotNullReturnQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.GENERIC_FUNCTION_DECLARATION__IS_NOT_NULL_RETURN_Q, null, msgs);
			if (newIsNotNullReturnQ != null)
				msgs = ((InternalEObject)newIsNotNullReturnQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.GENERIC_FUNCTION_DECLARATION__IS_NOT_NULL_RETURN_Q, null, msgs);
			msgs = basicSetIsNotNullReturnQ(newIsNotNullReturnQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.GENERIC_FUNCTION_DECLARATION__IS_NOT_NULL_RETURN_Q, newIsNotNullReturnQ, newIsNotNullReturnQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementClass getResultProfileQ() {
		return resultProfileQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetResultProfileQ(ElementClass newResultProfileQ, NotificationChain msgs) {
		ElementClass oldResultProfileQ = resultProfileQ;
		resultProfileQ = newResultProfileQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.GENERIC_FUNCTION_DECLARATION__RESULT_PROFILE_Q, oldResultProfileQ, newResultProfileQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResultProfileQ(ElementClass newResultProfileQ) {
		if (newResultProfileQ != resultProfileQ) {
			NotificationChain msgs = null;
			if (resultProfileQ != null)
				msgs = ((InternalEObject)resultProfileQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.GENERIC_FUNCTION_DECLARATION__RESULT_PROFILE_Q, null, msgs);
			if (newResultProfileQ != null)
				msgs = ((InternalEObject)newResultProfileQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.GENERIC_FUNCTION_DECLARATION__RESULT_PROFILE_Q, null, msgs);
			msgs = basicSetResultProfileQ(newResultProfileQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.GENERIC_FUNCTION_DECLARATION__RESULT_PROFILE_Q, newResultProfileQ, newResultProfileQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementList getAspectSpecificationsQl() {
		return aspectSpecificationsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAspectSpecificationsQl(ElementList newAspectSpecificationsQl, NotificationChain msgs) {
		ElementList oldAspectSpecificationsQl = aspectSpecificationsQl;
		aspectSpecificationsQl = newAspectSpecificationsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.GENERIC_FUNCTION_DECLARATION__ASPECT_SPECIFICATIONS_QL, oldAspectSpecificationsQl, newAspectSpecificationsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAspectSpecificationsQl(ElementList newAspectSpecificationsQl) {
		if (newAspectSpecificationsQl != aspectSpecificationsQl) {
			NotificationChain msgs = null;
			if (aspectSpecificationsQl != null)
				msgs = ((InternalEObject)aspectSpecificationsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.GENERIC_FUNCTION_DECLARATION__ASPECT_SPECIFICATIONS_QL, null, msgs);
			if (newAspectSpecificationsQl != null)
				msgs = ((InternalEObject)newAspectSpecificationsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.GENERIC_FUNCTION_DECLARATION__ASPECT_SPECIFICATIONS_QL, null, msgs);
			msgs = basicSetAspectSpecificationsQl(newAspectSpecificationsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.GENERIC_FUNCTION_DECLARATION__ASPECT_SPECIFICATIONS_QL, newAspectSpecificationsQl, newAspectSpecificationsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.GENERIC_FUNCTION_DECLARATION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__GENERIC_FORMAL_PART_QL:
				return basicSetGenericFormalPartQl(null, msgs);
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__NAMES_QL:
				return basicSetNamesQl(null, msgs);
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__PARAMETER_PROFILE_QL:
				return basicSetParameterProfileQl(null, msgs);
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__IS_NOT_NULL_RETURN_Q:
				return basicSetIsNotNullReturnQ(null, msgs);
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__RESULT_PROFILE_Q:
				return basicSetResultProfileQ(null, msgs);
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__ASPECT_SPECIFICATIONS_QL:
				return basicSetAspectSpecificationsQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__SLOC:
				return getSloc();
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__GENERIC_FORMAL_PART_QL:
				return getGenericFormalPartQl();
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__NAMES_QL:
				return getNamesQl();
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__PARAMETER_PROFILE_QL:
				return getParameterProfileQl();
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__IS_NOT_NULL_RETURN_Q:
				return getIsNotNullReturnQ();
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__RESULT_PROFILE_Q:
				return getResultProfileQ();
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__ASPECT_SPECIFICATIONS_QL:
				return getAspectSpecificationsQl();
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__GENERIC_FORMAL_PART_QL:
				setGenericFormalPartQl((ElementList)newValue);
				return;
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__NAMES_QL:
				setNamesQl((DefiningNameList)newValue);
				return;
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__PARAMETER_PROFILE_QL:
				setParameterProfileQl((ParameterSpecificationList)newValue);
				return;
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__IS_NOT_NULL_RETURN_Q:
				setIsNotNullReturnQ((IsNotNullReturnQType3)newValue);
				return;
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__RESULT_PROFILE_Q:
				setResultProfileQ((ElementClass)newValue);
				return;
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__ASPECT_SPECIFICATIONS_QL:
				setAspectSpecificationsQl((ElementList)newValue);
				return;
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__GENERIC_FORMAL_PART_QL:
				setGenericFormalPartQl((ElementList)null);
				return;
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__NAMES_QL:
				setNamesQl((DefiningNameList)null);
				return;
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__PARAMETER_PROFILE_QL:
				setParameterProfileQl((ParameterSpecificationList)null);
				return;
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__IS_NOT_NULL_RETURN_Q:
				setIsNotNullReturnQ((IsNotNullReturnQType3)null);
				return;
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__RESULT_PROFILE_Q:
				setResultProfileQ((ElementClass)null);
				return;
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__ASPECT_SPECIFICATIONS_QL:
				setAspectSpecificationsQl((ElementList)null);
				return;
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__SLOC:
				return sloc != null;
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__GENERIC_FORMAL_PART_QL:
				return genericFormalPartQl != null;
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__NAMES_QL:
				return namesQl != null;
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__PARAMETER_PROFILE_QL:
				return parameterProfileQl != null;
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__IS_NOT_NULL_RETURN_Q:
				return isNotNullReturnQ != null;
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__RESULT_PROFILE_Q:
				return resultProfileQ != null;
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__ASPECT_SPECIFICATIONS_QL:
				return aspectSpecificationsQl != null;
			case AdaPackage.GENERIC_FUNCTION_DECLARATION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //GenericFunctionDeclarationImpl
