/**
 */
package Ada.impl;

import Ada.AccessAttribute;
import Ada.AdaPackage;
import Ada.AddressAttribute;
import Ada.AdjacentAttribute;
import Ada.AftAttribute;
import Ada.AlignmentAttribute;
import Ada.AllCallsRemotePragma;
import Ada.AssertPragma;
import Ada.AssertionPolicyPragma;
import Ada.AsynchronousPragma;
import Ada.AtomicComponentsPragma;
import Ada.AtomicPragma;
import Ada.AttachHandlerPragma;
import Ada.BaseAttribute;
import Ada.BitOrderAttribute;
import Ada.BodyVersionAttribute;
import Ada.CallableAttribute;
import Ada.CallerAttribute;
import Ada.CeilingAttribute;
import Ada.ClassAttribute;
import Ada.Comment;
import Ada.ComponentSizeAttribute;
import Ada.ComposeAttribute;
import Ada.ConstrainedAttribute;
import Ada.ControlledPragma;
import Ada.ConventionPragma;
import Ada.CopySignAttribute;
import Ada.CountAttribute;
import Ada.CpuPragma;
import Ada.DefaultStoragePoolPragma;
import Ada.DefiniteAttribute;
import Ada.DeltaAttribute;
import Ada.DenormAttribute;
import Ada.DetectBlockingPragma;
import Ada.DigitsAttribute;
import Ada.DiscardNamesPragma;
import Ada.DispatchingDomainPragma;
import Ada.ElaborateAllPragma;
import Ada.ElaborateBodyPragma;
import Ada.ElaboratePragma;
import Ada.ExponentAttribute;
import Ada.ExportPragma;
import Ada.ExternalTagAttribute;
import Ada.FirstAttribute;
import Ada.FirstBitAttribute;
import Ada.FloorAttribute;
import Ada.ForeAttribute;
import Ada.FractionAttribute;
import Ada.Identifier;
import Ada.IdentityAttribute;
import Ada.ImageAttribute;
import Ada.ImplementationDefinedAttribute;
import Ada.ImplementationDefinedPragma;
import Ada.ImportPragma;
import Ada.IndependentComponentsPragma;
import Ada.IndependentPragma;
import Ada.InlinePragma;
import Ada.InputAttribute;
import Ada.InspectionPointPragma;
import Ada.InterruptHandlerPragma;
import Ada.InterruptPriorityPragma;
import Ada.LastAttribute;
import Ada.LastBitAttribute;
import Ada.LeadingPartAttribute;
import Ada.LengthAttribute;
import Ada.LinkerOptionsPragma;
import Ada.ListPragma;
import Ada.LockingPolicyPragma;
import Ada.MachineAttribute;
import Ada.MachineEmaxAttribute;
import Ada.MachineEminAttribute;
import Ada.MachineMantissaAttribute;
import Ada.MachineOverflowsAttribute;
import Ada.MachineRadixAttribute;
import Ada.MachineRoundingAttribute;
import Ada.MachineRoundsAttribute;
import Ada.MaxAlignmentForAllocationAttribute;
import Ada.MaxAttribute;
import Ada.MaxSizeInStorageElementsAttribute;
import Ada.MinAttribute;
import Ada.ModAttribute;
import Ada.ModelAttribute;
import Ada.ModelEminAttribute;
import Ada.ModelEpsilonAttribute;
import Ada.ModelMantissaAttribute;
import Ada.ModelSmallAttribute;
import Ada.ModulusAttribute;
import Ada.NameClass;
import Ada.NoReturnPragma;
import Ada.NormalizeScalarsPragma;
import Ada.NotAnElement;
import Ada.OptimizePragma;
import Ada.OutputAttribute;
import Ada.OverlapsStorageAttribute;
import Ada.PackPragma;
import Ada.PagePragma;
import Ada.PartitionElaborationPolicyPragma;
import Ada.PartitionIdAttribute;
import Ada.PosAttribute;
import Ada.PositionAttribute;
import Ada.PredAttribute;
import Ada.PreelaborableInitializationPragma;
import Ada.PreelaboratePragma;
import Ada.PriorityAttribute;
import Ada.PriorityPragma;
import Ada.PrioritySpecificDispatchingPragma;
import Ada.ProfilePragma;
import Ada.PurePragma;
import Ada.QueuingPolicyPragma;
import Ada.RangeAttribute;
import Ada.ReadAttribute;
import Ada.RelativeDeadlinePragma;
import Ada.RemainderAttribute;
import Ada.RemoteCallInterfacePragma;
import Ada.RemoteTypesPragma;
import Ada.RestrictionsPragma;
import Ada.ReviewablePragma;
import Ada.RoundAttribute;
import Ada.RoundingAttribute;
import Ada.SafeFirstAttribute;
import Ada.SafeLastAttribute;
import Ada.ScaleAttribute;
import Ada.ScalingAttribute;
import Ada.SelectedComponent;
import Ada.SharedPassivePragma;
import Ada.SignedZerosAttribute;
import Ada.SizeAttribute;
import Ada.SmallAttribute;
import Ada.StoragePoolAttribute;
import Ada.StorageSizeAttribute;
import Ada.StorageSizePragma;
import Ada.StreamSizeAttribute;
import Ada.SuccAttribute;
import Ada.SuppressPragma;
import Ada.TagAttribute;
import Ada.TaskDispatchingPolicyPragma;
import Ada.TerminatedAttribute;
import Ada.TruncationAttribute;
import Ada.UnbiasedRoundingAttribute;
import Ada.UncheckedAccessAttribute;
import Ada.UncheckedUnionPragma;
import Ada.UnknownAttribute;
import Ada.UnknownPragma;
import Ada.UnsuppressPragma;
import Ada.ValAttribute;
import Ada.ValidAttribute;
import Ada.ValueAttribute;
import Ada.VersionAttribute;
import Ada.VolatileComponentsPragma;
import Ada.VolatilePragma;
import Ada.WideImageAttribute;
import Ada.WideValueAttribute;
import Ada.WideWideImageAttribute;
import Ada.WideWideValueAttribute;
import Ada.WideWideWidthAttribute;
import Ada.WideWidthAttribute;
import Ada.WidthAttribute;
import Ada.WriteAttribute;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Name Class</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.NameClassImpl#getNotAnElement <em>Not An Element</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getSelectedComponent <em>Selected Component</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getAccessAttribute <em>Access Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getAddressAttribute <em>Address Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getAdjacentAttribute <em>Adjacent Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getAftAttribute <em>Aft Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getAlignmentAttribute <em>Alignment Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getBaseAttribute <em>Base Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getBitOrderAttribute <em>Bit Order Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getBodyVersionAttribute <em>Body Version Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getCallableAttribute <em>Callable Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getCallerAttribute <em>Caller Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getCeilingAttribute <em>Ceiling Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getClassAttribute <em>Class Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getComponentSizeAttribute <em>Component Size Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getComposeAttribute <em>Compose Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getConstrainedAttribute <em>Constrained Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getCopySignAttribute <em>Copy Sign Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getCountAttribute <em>Count Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getDefiniteAttribute <em>Definite Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getDeltaAttribute <em>Delta Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getDenormAttribute <em>Denorm Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getDigitsAttribute <em>Digits Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getExponentAttribute <em>Exponent Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getExternalTagAttribute <em>External Tag Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getFirstAttribute <em>First Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getFirstBitAttribute <em>First Bit Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getFloorAttribute <em>Floor Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getForeAttribute <em>Fore Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getFractionAttribute <em>Fraction Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getIdentityAttribute <em>Identity Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getImageAttribute <em>Image Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getInputAttribute <em>Input Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getLastAttribute <em>Last Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getLastBitAttribute <em>Last Bit Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getLeadingPartAttribute <em>Leading Part Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getLengthAttribute <em>Length Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getMachineAttribute <em>Machine Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getMachineEmaxAttribute <em>Machine Emax Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getMachineEminAttribute <em>Machine Emin Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getMachineMantissaAttribute <em>Machine Mantissa Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getMachineOverflowsAttribute <em>Machine Overflows Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getMachineRadixAttribute <em>Machine Radix Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getMachineRoundsAttribute <em>Machine Rounds Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getMaxAttribute <em>Max Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getMaxSizeInStorageElementsAttribute <em>Max Size In Storage Elements Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getMinAttribute <em>Min Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getModelAttribute <em>Model Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getModelEminAttribute <em>Model Emin Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getModelEpsilonAttribute <em>Model Epsilon Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getModelMantissaAttribute <em>Model Mantissa Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getModelSmallAttribute <em>Model Small Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getModulusAttribute <em>Modulus Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getOutputAttribute <em>Output Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getPartitionIdAttribute <em>Partition Id Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getPosAttribute <em>Pos Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getPositionAttribute <em>Position Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getPredAttribute <em>Pred Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getRangeAttribute <em>Range Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getReadAttribute <em>Read Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getRemainderAttribute <em>Remainder Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getRoundAttribute <em>Round Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getRoundingAttribute <em>Rounding Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getSafeFirstAttribute <em>Safe First Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getSafeLastAttribute <em>Safe Last Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getScaleAttribute <em>Scale Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getScalingAttribute <em>Scaling Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getSignedZerosAttribute <em>Signed Zeros Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getSizeAttribute <em>Size Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getSmallAttribute <em>Small Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getStoragePoolAttribute <em>Storage Pool Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getStorageSizeAttribute <em>Storage Size Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getSuccAttribute <em>Succ Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getTagAttribute <em>Tag Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getTerminatedAttribute <em>Terminated Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getTruncationAttribute <em>Truncation Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getUnbiasedRoundingAttribute <em>Unbiased Rounding Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getUncheckedAccessAttribute <em>Unchecked Access Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getValAttribute <em>Val Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getValidAttribute <em>Valid Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getValueAttribute <em>Value Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getVersionAttribute <em>Version Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getWideImageAttribute <em>Wide Image Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getWideValueAttribute <em>Wide Value Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getWideWidthAttribute <em>Wide Width Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getWidthAttribute <em>Width Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getWriteAttribute <em>Write Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getMachineRoundingAttribute <em>Machine Rounding Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getModAttribute <em>Mod Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getPriorityAttribute <em>Priority Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getStreamSizeAttribute <em>Stream Size Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getWideWideImageAttribute <em>Wide Wide Image Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getWideWideValueAttribute <em>Wide Wide Value Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getWideWideWidthAttribute <em>Wide Wide Width Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getMaxAlignmentForAllocationAttribute <em>Max Alignment For Allocation Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getOverlapsStorageAttribute <em>Overlaps Storage Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getImplementationDefinedAttribute <em>Implementation Defined Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getUnknownAttribute <em>Unknown Attribute</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getAsynchronousPragma <em>Asynchronous Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getAtomicPragma <em>Atomic Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getAttachHandlerPragma <em>Attach Handler Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getControlledPragma <em>Controlled Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getConventionPragma <em>Convention Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getDiscardNamesPragma <em>Discard Names Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getElaboratePragma <em>Elaborate Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getElaborateAllPragma <em>Elaborate All Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getExportPragma <em>Export Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getImportPragma <em>Import Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getInlinePragma <em>Inline Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getInspectionPointPragma <em>Inspection Point Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getLinkerOptionsPragma <em>Linker Options Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getListPragma <em>List Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getLockingPolicyPragma <em>Locking Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getOptimizePragma <em>Optimize Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getPackPragma <em>Pack Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getPagePragma <em>Page Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getPreelaboratePragma <em>Preelaborate Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getPriorityPragma <em>Priority Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getPurePragma <em>Pure Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getRemoteTypesPragma <em>Remote Types Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getRestrictionsPragma <em>Restrictions Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getReviewablePragma <em>Reviewable Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getSharedPassivePragma <em>Shared Passive Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getStorageSizePragma <em>Storage Size Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getSuppressPragma <em>Suppress Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getVolatilePragma <em>Volatile Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getAssertPragma <em>Assert Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getNoReturnPragma <em>No Return Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getProfilePragma <em>Profile Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getUnsuppressPragma <em>Unsuppress Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getCpuPragma <em>Cpu Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getIndependentPragma <em>Independent Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getIndependentComponentsPragma <em>Independent Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}</li>
 *   <li>{@link Ada.impl.NameClassImpl#getUnknownPragma <em>Unknown Pragma</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NameClassImpl extends MinimalEObjectImpl.Container implements NameClass {
	/**
	 * The cached value of the '{@link #getNotAnElement() <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotAnElement()
	 * @generated
	 * @ordered
	 */
	protected NotAnElement notAnElement;

	/**
	 * The cached value of the '{@link #getIdentifier() <em>Identifier</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdentifier()
	 * @generated
	 * @ordered
	 */
	protected Identifier identifier;

	/**
	 * The cached value of the '{@link #getSelectedComponent() <em>Selected Component</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelectedComponent()
	 * @generated
	 * @ordered
	 */
	protected SelectedComponent selectedComponent;

	/**
	 * The cached value of the '{@link #getAccessAttribute() <em>Access Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessAttribute()
	 * @generated
	 * @ordered
	 */
	protected AccessAttribute accessAttribute;

	/**
	 * The cached value of the '{@link #getAddressAttribute() <em>Address Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAddressAttribute()
	 * @generated
	 * @ordered
	 */
	protected AddressAttribute addressAttribute;

	/**
	 * The cached value of the '{@link #getAdjacentAttribute() <em>Adjacent Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAdjacentAttribute()
	 * @generated
	 * @ordered
	 */
	protected AdjacentAttribute adjacentAttribute;

	/**
	 * The cached value of the '{@link #getAftAttribute() <em>Aft Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAftAttribute()
	 * @generated
	 * @ordered
	 */
	protected AftAttribute aftAttribute;

	/**
	 * The cached value of the '{@link #getAlignmentAttribute() <em>Alignment Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlignmentAttribute()
	 * @generated
	 * @ordered
	 */
	protected AlignmentAttribute alignmentAttribute;

	/**
	 * The cached value of the '{@link #getBaseAttribute() <em>Base Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseAttribute()
	 * @generated
	 * @ordered
	 */
	protected BaseAttribute baseAttribute;

	/**
	 * The cached value of the '{@link #getBitOrderAttribute() <em>Bit Order Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBitOrderAttribute()
	 * @generated
	 * @ordered
	 */
	protected BitOrderAttribute bitOrderAttribute;

	/**
	 * The cached value of the '{@link #getBodyVersionAttribute() <em>Body Version Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBodyVersionAttribute()
	 * @generated
	 * @ordered
	 */
	protected BodyVersionAttribute bodyVersionAttribute;

	/**
	 * The cached value of the '{@link #getCallableAttribute() <em>Callable Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCallableAttribute()
	 * @generated
	 * @ordered
	 */
	protected CallableAttribute callableAttribute;

	/**
	 * The cached value of the '{@link #getCallerAttribute() <em>Caller Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCallerAttribute()
	 * @generated
	 * @ordered
	 */
	protected CallerAttribute callerAttribute;

	/**
	 * The cached value of the '{@link #getCeilingAttribute() <em>Ceiling Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCeilingAttribute()
	 * @generated
	 * @ordered
	 */
	protected CeilingAttribute ceilingAttribute;

	/**
	 * The cached value of the '{@link #getClassAttribute() <em>Class Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassAttribute()
	 * @generated
	 * @ordered
	 */
	protected ClassAttribute classAttribute;

	/**
	 * The cached value of the '{@link #getComponentSizeAttribute() <em>Component Size Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentSizeAttribute()
	 * @generated
	 * @ordered
	 */
	protected ComponentSizeAttribute componentSizeAttribute;

	/**
	 * The cached value of the '{@link #getComposeAttribute() <em>Compose Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComposeAttribute()
	 * @generated
	 * @ordered
	 */
	protected ComposeAttribute composeAttribute;

	/**
	 * The cached value of the '{@link #getConstrainedAttribute() <em>Constrained Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstrainedAttribute()
	 * @generated
	 * @ordered
	 */
	protected ConstrainedAttribute constrainedAttribute;

	/**
	 * The cached value of the '{@link #getCopySignAttribute() <em>Copy Sign Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCopySignAttribute()
	 * @generated
	 * @ordered
	 */
	protected CopySignAttribute copySignAttribute;

	/**
	 * The cached value of the '{@link #getCountAttribute() <em>Count Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCountAttribute()
	 * @generated
	 * @ordered
	 */
	protected CountAttribute countAttribute;

	/**
	 * The cached value of the '{@link #getDefiniteAttribute() <em>Definite Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefiniteAttribute()
	 * @generated
	 * @ordered
	 */
	protected DefiniteAttribute definiteAttribute;

	/**
	 * The cached value of the '{@link #getDeltaAttribute() <em>Delta Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeltaAttribute()
	 * @generated
	 * @ordered
	 */
	protected DeltaAttribute deltaAttribute;

	/**
	 * The cached value of the '{@link #getDenormAttribute() <em>Denorm Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDenormAttribute()
	 * @generated
	 * @ordered
	 */
	protected DenormAttribute denormAttribute;

	/**
	 * The cached value of the '{@link #getDigitsAttribute() <em>Digits Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDigitsAttribute()
	 * @generated
	 * @ordered
	 */
	protected DigitsAttribute digitsAttribute;

	/**
	 * The cached value of the '{@link #getExponentAttribute() <em>Exponent Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExponentAttribute()
	 * @generated
	 * @ordered
	 */
	protected ExponentAttribute exponentAttribute;

	/**
	 * The cached value of the '{@link #getExternalTagAttribute() <em>External Tag Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExternalTagAttribute()
	 * @generated
	 * @ordered
	 */
	protected ExternalTagAttribute externalTagAttribute;

	/**
	 * The cached value of the '{@link #getFirstAttribute() <em>First Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstAttribute()
	 * @generated
	 * @ordered
	 */
	protected FirstAttribute firstAttribute;

	/**
	 * The cached value of the '{@link #getFirstBitAttribute() <em>First Bit Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstBitAttribute()
	 * @generated
	 * @ordered
	 */
	protected FirstBitAttribute firstBitAttribute;

	/**
	 * The cached value of the '{@link #getFloorAttribute() <em>Floor Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFloorAttribute()
	 * @generated
	 * @ordered
	 */
	protected FloorAttribute floorAttribute;

	/**
	 * The cached value of the '{@link #getForeAttribute() <em>Fore Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getForeAttribute()
	 * @generated
	 * @ordered
	 */
	protected ForeAttribute foreAttribute;

	/**
	 * The cached value of the '{@link #getFractionAttribute() <em>Fraction Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFractionAttribute()
	 * @generated
	 * @ordered
	 */
	protected FractionAttribute fractionAttribute;

	/**
	 * The cached value of the '{@link #getIdentityAttribute() <em>Identity Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdentityAttribute()
	 * @generated
	 * @ordered
	 */
	protected IdentityAttribute identityAttribute;

	/**
	 * The cached value of the '{@link #getImageAttribute() <em>Image Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImageAttribute()
	 * @generated
	 * @ordered
	 */
	protected ImageAttribute imageAttribute;

	/**
	 * The cached value of the '{@link #getInputAttribute() <em>Input Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputAttribute()
	 * @generated
	 * @ordered
	 */
	protected InputAttribute inputAttribute;

	/**
	 * The cached value of the '{@link #getLastAttribute() <em>Last Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastAttribute()
	 * @generated
	 * @ordered
	 */
	protected LastAttribute lastAttribute;

	/**
	 * The cached value of the '{@link #getLastBitAttribute() <em>Last Bit Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastBitAttribute()
	 * @generated
	 * @ordered
	 */
	protected LastBitAttribute lastBitAttribute;

	/**
	 * The cached value of the '{@link #getLeadingPartAttribute() <em>Leading Part Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeadingPartAttribute()
	 * @generated
	 * @ordered
	 */
	protected LeadingPartAttribute leadingPartAttribute;

	/**
	 * The cached value of the '{@link #getLengthAttribute() <em>Length Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLengthAttribute()
	 * @generated
	 * @ordered
	 */
	protected LengthAttribute lengthAttribute;

	/**
	 * The cached value of the '{@link #getMachineAttribute() <em>Machine Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMachineAttribute()
	 * @generated
	 * @ordered
	 */
	protected MachineAttribute machineAttribute;

	/**
	 * The cached value of the '{@link #getMachineEmaxAttribute() <em>Machine Emax Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMachineEmaxAttribute()
	 * @generated
	 * @ordered
	 */
	protected MachineEmaxAttribute machineEmaxAttribute;

	/**
	 * The cached value of the '{@link #getMachineEminAttribute() <em>Machine Emin Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMachineEminAttribute()
	 * @generated
	 * @ordered
	 */
	protected MachineEminAttribute machineEminAttribute;

	/**
	 * The cached value of the '{@link #getMachineMantissaAttribute() <em>Machine Mantissa Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMachineMantissaAttribute()
	 * @generated
	 * @ordered
	 */
	protected MachineMantissaAttribute machineMantissaAttribute;

	/**
	 * The cached value of the '{@link #getMachineOverflowsAttribute() <em>Machine Overflows Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMachineOverflowsAttribute()
	 * @generated
	 * @ordered
	 */
	protected MachineOverflowsAttribute machineOverflowsAttribute;

	/**
	 * The cached value of the '{@link #getMachineRadixAttribute() <em>Machine Radix Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMachineRadixAttribute()
	 * @generated
	 * @ordered
	 */
	protected MachineRadixAttribute machineRadixAttribute;

	/**
	 * The cached value of the '{@link #getMachineRoundsAttribute() <em>Machine Rounds Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMachineRoundsAttribute()
	 * @generated
	 * @ordered
	 */
	protected MachineRoundsAttribute machineRoundsAttribute;

	/**
	 * The cached value of the '{@link #getMaxAttribute() <em>Max Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxAttribute()
	 * @generated
	 * @ordered
	 */
	protected MaxAttribute maxAttribute;

	/**
	 * The cached value of the '{@link #getMaxSizeInStorageElementsAttribute() <em>Max Size In Storage Elements Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxSizeInStorageElementsAttribute()
	 * @generated
	 * @ordered
	 */
	protected MaxSizeInStorageElementsAttribute maxSizeInStorageElementsAttribute;

	/**
	 * The cached value of the '{@link #getMinAttribute() <em>Min Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinAttribute()
	 * @generated
	 * @ordered
	 */
	protected MinAttribute minAttribute;

	/**
	 * The cached value of the '{@link #getModelAttribute() <em>Model Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelAttribute()
	 * @generated
	 * @ordered
	 */
	protected ModelAttribute modelAttribute;

	/**
	 * The cached value of the '{@link #getModelEminAttribute() <em>Model Emin Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelEminAttribute()
	 * @generated
	 * @ordered
	 */
	protected ModelEminAttribute modelEminAttribute;

	/**
	 * The cached value of the '{@link #getModelEpsilonAttribute() <em>Model Epsilon Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelEpsilonAttribute()
	 * @generated
	 * @ordered
	 */
	protected ModelEpsilonAttribute modelEpsilonAttribute;

	/**
	 * The cached value of the '{@link #getModelMantissaAttribute() <em>Model Mantissa Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelMantissaAttribute()
	 * @generated
	 * @ordered
	 */
	protected ModelMantissaAttribute modelMantissaAttribute;

	/**
	 * The cached value of the '{@link #getModelSmallAttribute() <em>Model Small Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelSmallAttribute()
	 * @generated
	 * @ordered
	 */
	protected ModelSmallAttribute modelSmallAttribute;

	/**
	 * The cached value of the '{@link #getModulusAttribute() <em>Modulus Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModulusAttribute()
	 * @generated
	 * @ordered
	 */
	protected ModulusAttribute modulusAttribute;

	/**
	 * The cached value of the '{@link #getOutputAttribute() <em>Output Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputAttribute()
	 * @generated
	 * @ordered
	 */
	protected OutputAttribute outputAttribute;

	/**
	 * The cached value of the '{@link #getPartitionIdAttribute() <em>Partition Id Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPartitionIdAttribute()
	 * @generated
	 * @ordered
	 */
	protected PartitionIdAttribute partitionIdAttribute;

	/**
	 * The cached value of the '{@link #getPosAttribute() <em>Pos Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPosAttribute()
	 * @generated
	 * @ordered
	 */
	protected PosAttribute posAttribute;

	/**
	 * The cached value of the '{@link #getPositionAttribute() <em>Position Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPositionAttribute()
	 * @generated
	 * @ordered
	 */
	protected PositionAttribute positionAttribute;

	/**
	 * The cached value of the '{@link #getPredAttribute() <em>Pred Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPredAttribute()
	 * @generated
	 * @ordered
	 */
	protected PredAttribute predAttribute;

	/**
	 * The cached value of the '{@link #getRangeAttribute() <em>Range Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRangeAttribute()
	 * @generated
	 * @ordered
	 */
	protected RangeAttribute rangeAttribute;

	/**
	 * The cached value of the '{@link #getReadAttribute() <em>Read Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReadAttribute()
	 * @generated
	 * @ordered
	 */
	protected ReadAttribute readAttribute;

	/**
	 * The cached value of the '{@link #getRemainderAttribute() <em>Remainder Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRemainderAttribute()
	 * @generated
	 * @ordered
	 */
	protected RemainderAttribute remainderAttribute;

	/**
	 * The cached value of the '{@link #getRoundAttribute() <em>Round Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoundAttribute()
	 * @generated
	 * @ordered
	 */
	protected RoundAttribute roundAttribute;

	/**
	 * The cached value of the '{@link #getRoundingAttribute() <em>Rounding Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoundingAttribute()
	 * @generated
	 * @ordered
	 */
	protected RoundingAttribute roundingAttribute;

	/**
	 * The cached value of the '{@link #getSafeFirstAttribute() <em>Safe First Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSafeFirstAttribute()
	 * @generated
	 * @ordered
	 */
	protected SafeFirstAttribute safeFirstAttribute;

	/**
	 * The cached value of the '{@link #getSafeLastAttribute() <em>Safe Last Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSafeLastAttribute()
	 * @generated
	 * @ordered
	 */
	protected SafeLastAttribute safeLastAttribute;

	/**
	 * The cached value of the '{@link #getScaleAttribute() <em>Scale Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScaleAttribute()
	 * @generated
	 * @ordered
	 */
	protected ScaleAttribute scaleAttribute;

	/**
	 * The cached value of the '{@link #getScalingAttribute() <em>Scaling Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScalingAttribute()
	 * @generated
	 * @ordered
	 */
	protected ScalingAttribute scalingAttribute;

	/**
	 * The cached value of the '{@link #getSignedZerosAttribute() <em>Signed Zeros Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignedZerosAttribute()
	 * @generated
	 * @ordered
	 */
	protected SignedZerosAttribute signedZerosAttribute;

	/**
	 * The cached value of the '{@link #getSizeAttribute() <em>Size Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSizeAttribute()
	 * @generated
	 * @ordered
	 */
	protected SizeAttribute sizeAttribute;

	/**
	 * The cached value of the '{@link #getSmallAttribute() <em>Small Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSmallAttribute()
	 * @generated
	 * @ordered
	 */
	protected SmallAttribute smallAttribute;

	/**
	 * The cached value of the '{@link #getStoragePoolAttribute() <em>Storage Pool Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStoragePoolAttribute()
	 * @generated
	 * @ordered
	 */
	protected StoragePoolAttribute storagePoolAttribute;

	/**
	 * The cached value of the '{@link #getStorageSizeAttribute() <em>Storage Size Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStorageSizeAttribute()
	 * @generated
	 * @ordered
	 */
	protected StorageSizeAttribute storageSizeAttribute;

	/**
	 * The cached value of the '{@link #getSuccAttribute() <em>Succ Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuccAttribute()
	 * @generated
	 * @ordered
	 */
	protected SuccAttribute succAttribute;

	/**
	 * The cached value of the '{@link #getTagAttribute() <em>Tag Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTagAttribute()
	 * @generated
	 * @ordered
	 */
	protected TagAttribute tagAttribute;

	/**
	 * The cached value of the '{@link #getTerminatedAttribute() <em>Terminated Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTerminatedAttribute()
	 * @generated
	 * @ordered
	 */
	protected TerminatedAttribute terminatedAttribute;

	/**
	 * The cached value of the '{@link #getTruncationAttribute() <em>Truncation Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTruncationAttribute()
	 * @generated
	 * @ordered
	 */
	protected TruncationAttribute truncationAttribute;

	/**
	 * The cached value of the '{@link #getUnbiasedRoundingAttribute() <em>Unbiased Rounding Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnbiasedRoundingAttribute()
	 * @generated
	 * @ordered
	 */
	protected UnbiasedRoundingAttribute unbiasedRoundingAttribute;

	/**
	 * The cached value of the '{@link #getUncheckedAccessAttribute() <em>Unchecked Access Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUncheckedAccessAttribute()
	 * @generated
	 * @ordered
	 */
	protected UncheckedAccessAttribute uncheckedAccessAttribute;

	/**
	 * The cached value of the '{@link #getValAttribute() <em>Val Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValAttribute()
	 * @generated
	 * @ordered
	 */
	protected ValAttribute valAttribute;

	/**
	 * The cached value of the '{@link #getValidAttribute() <em>Valid Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValidAttribute()
	 * @generated
	 * @ordered
	 */
	protected ValidAttribute validAttribute;

	/**
	 * The cached value of the '{@link #getValueAttribute() <em>Value Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueAttribute()
	 * @generated
	 * @ordered
	 */
	protected ValueAttribute valueAttribute;

	/**
	 * The cached value of the '{@link #getVersionAttribute() <em>Version Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersionAttribute()
	 * @generated
	 * @ordered
	 */
	protected VersionAttribute versionAttribute;

	/**
	 * The cached value of the '{@link #getWideImageAttribute() <em>Wide Image Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWideImageAttribute()
	 * @generated
	 * @ordered
	 */
	protected WideImageAttribute wideImageAttribute;

	/**
	 * The cached value of the '{@link #getWideValueAttribute() <em>Wide Value Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWideValueAttribute()
	 * @generated
	 * @ordered
	 */
	protected WideValueAttribute wideValueAttribute;

	/**
	 * The cached value of the '{@link #getWideWidthAttribute() <em>Wide Width Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWideWidthAttribute()
	 * @generated
	 * @ordered
	 */
	protected WideWidthAttribute wideWidthAttribute;

	/**
	 * The cached value of the '{@link #getWidthAttribute() <em>Width Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWidthAttribute()
	 * @generated
	 * @ordered
	 */
	protected WidthAttribute widthAttribute;

	/**
	 * The cached value of the '{@link #getWriteAttribute() <em>Write Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWriteAttribute()
	 * @generated
	 * @ordered
	 */
	protected WriteAttribute writeAttribute;

	/**
	 * The cached value of the '{@link #getMachineRoundingAttribute() <em>Machine Rounding Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMachineRoundingAttribute()
	 * @generated
	 * @ordered
	 */
	protected MachineRoundingAttribute machineRoundingAttribute;

	/**
	 * The cached value of the '{@link #getModAttribute() <em>Mod Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModAttribute()
	 * @generated
	 * @ordered
	 */
	protected ModAttribute modAttribute;

	/**
	 * The cached value of the '{@link #getPriorityAttribute() <em>Priority Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPriorityAttribute()
	 * @generated
	 * @ordered
	 */
	protected PriorityAttribute priorityAttribute;

	/**
	 * The cached value of the '{@link #getStreamSizeAttribute() <em>Stream Size Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStreamSizeAttribute()
	 * @generated
	 * @ordered
	 */
	protected StreamSizeAttribute streamSizeAttribute;

	/**
	 * The cached value of the '{@link #getWideWideImageAttribute() <em>Wide Wide Image Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWideWideImageAttribute()
	 * @generated
	 * @ordered
	 */
	protected WideWideImageAttribute wideWideImageAttribute;

	/**
	 * The cached value of the '{@link #getWideWideValueAttribute() <em>Wide Wide Value Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWideWideValueAttribute()
	 * @generated
	 * @ordered
	 */
	protected WideWideValueAttribute wideWideValueAttribute;

	/**
	 * The cached value of the '{@link #getWideWideWidthAttribute() <em>Wide Wide Width Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWideWideWidthAttribute()
	 * @generated
	 * @ordered
	 */
	protected WideWideWidthAttribute wideWideWidthAttribute;

	/**
	 * The cached value of the '{@link #getMaxAlignmentForAllocationAttribute() <em>Max Alignment For Allocation Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxAlignmentForAllocationAttribute()
	 * @generated
	 * @ordered
	 */
	protected MaxAlignmentForAllocationAttribute maxAlignmentForAllocationAttribute;

	/**
	 * The cached value of the '{@link #getOverlapsStorageAttribute() <em>Overlaps Storage Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverlapsStorageAttribute()
	 * @generated
	 * @ordered
	 */
	protected OverlapsStorageAttribute overlapsStorageAttribute;

	/**
	 * The cached value of the '{@link #getImplementationDefinedAttribute() <em>Implementation Defined Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImplementationDefinedAttribute()
	 * @generated
	 * @ordered
	 */
	protected ImplementationDefinedAttribute implementationDefinedAttribute;

	/**
	 * The cached value of the '{@link #getUnknownAttribute() <em>Unknown Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnknownAttribute()
	 * @generated
	 * @ordered
	 */
	protected UnknownAttribute unknownAttribute;

	/**
	 * The cached value of the '{@link #getComment() <em>Comment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected Comment comment;

	/**
	 * The cached value of the '{@link #getAllCallsRemotePragma() <em>All Calls Remote Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllCallsRemotePragma()
	 * @generated
	 * @ordered
	 */
	protected AllCallsRemotePragma allCallsRemotePragma;

	/**
	 * The cached value of the '{@link #getAsynchronousPragma() <em>Asynchronous Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsynchronousPragma()
	 * @generated
	 * @ordered
	 */
	protected AsynchronousPragma asynchronousPragma;

	/**
	 * The cached value of the '{@link #getAtomicPragma() <em>Atomic Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAtomicPragma()
	 * @generated
	 * @ordered
	 */
	protected AtomicPragma atomicPragma;

	/**
	 * The cached value of the '{@link #getAtomicComponentsPragma() <em>Atomic Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAtomicComponentsPragma()
	 * @generated
	 * @ordered
	 */
	protected AtomicComponentsPragma atomicComponentsPragma;

	/**
	 * The cached value of the '{@link #getAttachHandlerPragma() <em>Attach Handler Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttachHandlerPragma()
	 * @generated
	 * @ordered
	 */
	protected AttachHandlerPragma attachHandlerPragma;

	/**
	 * The cached value of the '{@link #getControlledPragma() <em>Controlled Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getControlledPragma()
	 * @generated
	 * @ordered
	 */
	protected ControlledPragma controlledPragma;

	/**
	 * The cached value of the '{@link #getConventionPragma() <em>Convention Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConventionPragma()
	 * @generated
	 * @ordered
	 */
	protected ConventionPragma conventionPragma;

	/**
	 * The cached value of the '{@link #getDiscardNamesPragma() <em>Discard Names Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiscardNamesPragma()
	 * @generated
	 * @ordered
	 */
	protected DiscardNamesPragma discardNamesPragma;

	/**
	 * The cached value of the '{@link #getElaboratePragma() <em>Elaborate Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElaboratePragma()
	 * @generated
	 * @ordered
	 */
	protected ElaboratePragma elaboratePragma;

	/**
	 * The cached value of the '{@link #getElaborateAllPragma() <em>Elaborate All Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElaborateAllPragma()
	 * @generated
	 * @ordered
	 */
	protected ElaborateAllPragma elaborateAllPragma;

	/**
	 * The cached value of the '{@link #getElaborateBodyPragma() <em>Elaborate Body Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElaborateBodyPragma()
	 * @generated
	 * @ordered
	 */
	protected ElaborateBodyPragma elaborateBodyPragma;

	/**
	 * The cached value of the '{@link #getExportPragma() <em>Export Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExportPragma()
	 * @generated
	 * @ordered
	 */
	protected ExportPragma exportPragma;

	/**
	 * The cached value of the '{@link #getImportPragma() <em>Import Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImportPragma()
	 * @generated
	 * @ordered
	 */
	protected ImportPragma importPragma;

	/**
	 * The cached value of the '{@link #getInlinePragma() <em>Inline Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInlinePragma()
	 * @generated
	 * @ordered
	 */
	protected InlinePragma inlinePragma;

	/**
	 * The cached value of the '{@link #getInspectionPointPragma() <em>Inspection Point Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInspectionPointPragma()
	 * @generated
	 * @ordered
	 */
	protected InspectionPointPragma inspectionPointPragma;

	/**
	 * The cached value of the '{@link #getInterruptHandlerPragma() <em>Interrupt Handler Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterruptHandlerPragma()
	 * @generated
	 * @ordered
	 */
	protected InterruptHandlerPragma interruptHandlerPragma;

	/**
	 * The cached value of the '{@link #getInterruptPriorityPragma() <em>Interrupt Priority Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterruptPriorityPragma()
	 * @generated
	 * @ordered
	 */
	protected InterruptPriorityPragma interruptPriorityPragma;

	/**
	 * The cached value of the '{@link #getLinkerOptionsPragma() <em>Linker Options Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkerOptionsPragma()
	 * @generated
	 * @ordered
	 */
	protected LinkerOptionsPragma linkerOptionsPragma;

	/**
	 * The cached value of the '{@link #getListPragma() <em>List Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListPragma()
	 * @generated
	 * @ordered
	 */
	protected ListPragma listPragma;

	/**
	 * The cached value of the '{@link #getLockingPolicyPragma() <em>Locking Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLockingPolicyPragma()
	 * @generated
	 * @ordered
	 */
	protected LockingPolicyPragma lockingPolicyPragma;

	/**
	 * The cached value of the '{@link #getNormalizeScalarsPragma() <em>Normalize Scalars Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNormalizeScalarsPragma()
	 * @generated
	 * @ordered
	 */
	protected NormalizeScalarsPragma normalizeScalarsPragma;

	/**
	 * The cached value of the '{@link #getOptimizePragma() <em>Optimize Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOptimizePragma()
	 * @generated
	 * @ordered
	 */
	protected OptimizePragma optimizePragma;

	/**
	 * The cached value of the '{@link #getPackPragma() <em>Pack Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackPragma()
	 * @generated
	 * @ordered
	 */
	protected PackPragma packPragma;

	/**
	 * The cached value of the '{@link #getPagePragma() <em>Page Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPagePragma()
	 * @generated
	 * @ordered
	 */
	protected PagePragma pagePragma;

	/**
	 * The cached value of the '{@link #getPreelaboratePragma() <em>Preelaborate Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreelaboratePragma()
	 * @generated
	 * @ordered
	 */
	protected PreelaboratePragma preelaboratePragma;

	/**
	 * The cached value of the '{@link #getPriorityPragma() <em>Priority Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPriorityPragma()
	 * @generated
	 * @ordered
	 */
	protected PriorityPragma priorityPragma;

	/**
	 * The cached value of the '{@link #getPurePragma() <em>Pure Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPurePragma()
	 * @generated
	 * @ordered
	 */
	protected PurePragma purePragma;

	/**
	 * The cached value of the '{@link #getQueuingPolicyPragma() <em>Queuing Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQueuingPolicyPragma()
	 * @generated
	 * @ordered
	 */
	protected QueuingPolicyPragma queuingPolicyPragma;

	/**
	 * The cached value of the '{@link #getRemoteCallInterfacePragma() <em>Remote Call Interface Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRemoteCallInterfacePragma()
	 * @generated
	 * @ordered
	 */
	protected RemoteCallInterfacePragma remoteCallInterfacePragma;

	/**
	 * The cached value of the '{@link #getRemoteTypesPragma() <em>Remote Types Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRemoteTypesPragma()
	 * @generated
	 * @ordered
	 */
	protected RemoteTypesPragma remoteTypesPragma;

	/**
	 * The cached value of the '{@link #getRestrictionsPragma() <em>Restrictions Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRestrictionsPragma()
	 * @generated
	 * @ordered
	 */
	protected RestrictionsPragma restrictionsPragma;

	/**
	 * The cached value of the '{@link #getReviewablePragma() <em>Reviewable Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReviewablePragma()
	 * @generated
	 * @ordered
	 */
	protected ReviewablePragma reviewablePragma;

	/**
	 * The cached value of the '{@link #getSharedPassivePragma() <em>Shared Passive Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSharedPassivePragma()
	 * @generated
	 * @ordered
	 */
	protected SharedPassivePragma sharedPassivePragma;

	/**
	 * The cached value of the '{@link #getStorageSizePragma() <em>Storage Size Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStorageSizePragma()
	 * @generated
	 * @ordered
	 */
	protected StorageSizePragma storageSizePragma;

	/**
	 * The cached value of the '{@link #getSuppressPragma() <em>Suppress Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuppressPragma()
	 * @generated
	 * @ordered
	 */
	protected SuppressPragma suppressPragma;

	/**
	 * The cached value of the '{@link #getTaskDispatchingPolicyPragma() <em>Task Dispatching Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskDispatchingPolicyPragma()
	 * @generated
	 * @ordered
	 */
	protected TaskDispatchingPolicyPragma taskDispatchingPolicyPragma;

	/**
	 * The cached value of the '{@link #getVolatilePragma() <em>Volatile Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVolatilePragma()
	 * @generated
	 * @ordered
	 */
	protected VolatilePragma volatilePragma;

	/**
	 * The cached value of the '{@link #getVolatileComponentsPragma() <em>Volatile Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVolatileComponentsPragma()
	 * @generated
	 * @ordered
	 */
	protected VolatileComponentsPragma volatileComponentsPragma;

	/**
	 * The cached value of the '{@link #getAssertPragma() <em>Assert Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssertPragma()
	 * @generated
	 * @ordered
	 */
	protected AssertPragma assertPragma;

	/**
	 * The cached value of the '{@link #getAssertionPolicyPragma() <em>Assertion Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssertionPolicyPragma()
	 * @generated
	 * @ordered
	 */
	protected AssertionPolicyPragma assertionPolicyPragma;

	/**
	 * The cached value of the '{@link #getDetectBlockingPragma() <em>Detect Blocking Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDetectBlockingPragma()
	 * @generated
	 * @ordered
	 */
	protected DetectBlockingPragma detectBlockingPragma;

	/**
	 * The cached value of the '{@link #getNoReturnPragma() <em>No Return Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNoReturnPragma()
	 * @generated
	 * @ordered
	 */
	protected NoReturnPragma noReturnPragma;

	/**
	 * The cached value of the '{@link #getPartitionElaborationPolicyPragma() <em>Partition Elaboration Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPartitionElaborationPolicyPragma()
	 * @generated
	 * @ordered
	 */
	protected PartitionElaborationPolicyPragma partitionElaborationPolicyPragma;

	/**
	 * The cached value of the '{@link #getPreelaborableInitializationPragma() <em>Preelaborable Initialization Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreelaborableInitializationPragma()
	 * @generated
	 * @ordered
	 */
	protected PreelaborableInitializationPragma preelaborableInitializationPragma;

	/**
	 * The cached value of the '{@link #getPrioritySpecificDispatchingPragma() <em>Priority Specific Dispatching Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrioritySpecificDispatchingPragma()
	 * @generated
	 * @ordered
	 */
	protected PrioritySpecificDispatchingPragma prioritySpecificDispatchingPragma;

	/**
	 * The cached value of the '{@link #getProfilePragma() <em>Profile Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProfilePragma()
	 * @generated
	 * @ordered
	 */
	protected ProfilePragma profilePragma;

	/**
	 * The cached value of the '{@link #getRelativeDeadlinePragma() <em>Relative Deadline Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelativeDeadlinePragma()
	 * @generated
	 * @ordered
	 */
	protected RelativeDeadlinePragma relativeDeadlinePragma;

	/**
	 * The cached value of the '{@link #getUncheckedUnionPragma() <em>Unchecked Union Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUncheckedUnionPragma()
	 * @generated
	 * @ordered
	 */
	protected UncheckedUnionPragma uncheckedUnionPragma;

	/**
	 * The cached value of the '{@link #getUnsuppressPragma() <em>Unsuppress Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnsuppressPragma()
	 * @generated
	 * @ordered
	 */
	protected UnsuppressPragma unsuppressPragma;

	/**
	 * The cached value of the '{@link #getDefaultStoragePoolPragma() <em>Default Storage Pool Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultStoragePoolPragma()
	 * @generated
	 * @ordered
	 */
	protected DefaultStoragePoolPragma defaultStoragePoolPragma;

	/**
	 * The cached value of the '{@link #getDispatchingDomainPragma() <em>Dispatching Domain Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDispatchingDomainPragma()
	 * @generated
	 * @ordered
	 */
	protected DispatchingDomainPragma dispatchingDomainPragma;

	/**
	 * The cached value of the '{@link #getCpuPragma() <em>Cpu Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCpuPragma()
	 * @generated
	 * @ordered
	 */
	protected CpuPragma cpuPragma;

	/**
	 * The cached value of the '{@link #getIndependentPragma() <em>Independent Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndependentPragma()
	 * @generated
	 * @ordered
	 */
	protected IndependentPragma independentPragma;

	/**
	 * The cached value of the '{@link #getIndependentComponentsPragma() <em>Independent Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndependentComponentsPragma()
	 * @generated
	 * @ordered
	 */
	protected IndependentComponentsPragma independentComponentsPragma;

	/**
	 * The cached value of the '{@link #getImplementationDefinedPragma() <em>Implementation Defined Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImplementationDefinedPragma()
	 * @generated
	 * @ordered
	 */
	protected ImplementationDefinedPragma implementationDefinedPragma;

	/**
	 * The cached value of the '{@link #getUnknownPragma() <em>Unknown Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnknownPragma()
	 * @generated
	 * @ordered
	 */
	protected UnknownPragma unknownPragma;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NameClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getNameClass();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotAnElement getNotAnElement() {
		return notAnElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNotAnElement(NotAnElement newNotAnElement, NotificationChain msgs) {
		NotAnElement oldNotAnElement = notAnElement;
		notAnElement = newNotAnElement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__NOT_AN_ELEMENT, oldNotAnElement, newNotAnElement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotAnElement(NotAnElement newNotAnElement) {
		if (newNotAnElement != notAnElement) {
			NotificationChain msgs = null;
			if (notAnElement != null)
				msgs = ((InternalEObject)notAnElement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__NOT_AN_ELEMENT, null, msgs);
			if (newNotAnElement != null)
				msgs = ((InternalEObject)newNotAnElement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__NOT_AN_ELEMENT, null, msgs);
			msgs = basicSetNotAnElement(newNotAnElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__NOT_AN_ELEMENT, newNotAnElement, newNotAnElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Identifier getIdentifier() {
		return identifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIdentifier(Identifier newIdentifier, NotificationChain msgs) {
		Identifier oldIdentifier = identifier;
		identifier = newIdentifier;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__IDENTIFIER, oldIdentifier, newIdentifier);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdentifier(Identifier newIdentifier) {
		if (newIdentifier != identifier) {
			NotificationChain msgs = null;
			if (identifier != null)
				msgs = ((InternalEObject)identifier).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__IDENTIFIER, null, msgs);
			if (newIdentifier != null)
				msgs = ((InternalEObject)newIdentifier).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__IDENTIFIER, null, msgs);
			msgs = basicSetIdentifier(newIdentifier, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__IDENTIFIER, newIdentifier, newIdentifier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SelectedComponent getSelectedComponent() {
		return selectedComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSelectedComponent(SelectedComponent newSelectedComponent, NotificationChain msgs) {
		SelectedComponent oldSelectedComponent = selectedComponent;
		selectedComponent = newSelectedComponent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__SELECTED_COMPONENT, oldSelectedComponent, newSelectedComponent);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelectedComponent(SelectedComponent newSelectedComponent) {
		if (newSelectedComponent != selectedComponent) {
			NotificationChain msgs = null;
			if (selectedComponent != null)
				msgs = ((InternalEObject)selectedComponent).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__SELECTED_COMPONENT, null, msgs);
			if (newSelectedComponent != null)
				msgs = ((InternalEObject)newSelectedComponent).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__SELECTED_COMPONENT, null, msgs);
			msgs = basicSetSelectedComponent(newSelectedComponent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__SELECTED_COMPONENT, newSelectedComponent, newSelectedComponent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessAttribute getAccessAttribute() {
		return accessAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccessAttribute(AccessAttribute newAccessAttribute, NotificationChain msgs) {
		AccessAttribute oldAccessAttribute = accessAttribute;
		accessAttribute = newAccessAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ACCESS_ATTRIBUTE, oldAccessAttribute, newAccessAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessAttribute(AccessAttribute newAccessAttribute) {
		if (newAccessAttribute != accessAttribute) {
			NotificationChain msgs = null;
			if (accessAttribute != null)
				msgs = ((InternalEObject)accessAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ACCESS_ATTRIBUTE, null, msgs);
			if (newAccessAttribute != null)
				msgs = ((InternalEObject)newAccessAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ACCESS_ATTRIBUTE, null, msgs);
			msgs = basicSetAccessAttribute(newAccessAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ACCESS_ATTRIBUTE, newAccessAttribute, newAccessAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AddressAttribute getAddressAttribute() {
		return addressAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAddressAttribute(AddressAttribute newAddressAttribute, NotificationChain msgs) {
		AddressAttribute oldAddressAttribute = addressAttribute;
		addressAttribute = newAddressAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ADDRESS_ATTRIBUTE, oldAddressAttribute, newAddressAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAddressAttribute(AddressAttribute newAddressAttribute) {
		if (newAddressAttribute != addressAttribute) {
			NotificationChain msgs = null;
			if (addressAttribute != null)
				msgs = ((InternalEObject)addressAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ADDRESS_ATTRIBUTE, null, msgs);
			if (newAddressAttribute != null)
				msgs = ((InternalEObject)newAddressAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ADDRESS_ATTRIBUTE, null, msgs);
			msgs = basicSetAddressAttribute(newAddressAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ADDRESS_ATTRIBUTE, newAddressAttribute, newAddressAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdjacentAttribute getAdjacentAttribute() {
		return adjacentAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAdjacentAttribute(AdjacentAttribute newAdjacentAttribute, NotificationChain msgs) {
		AdjacentAttribute oldAdjacentAttribute = adjacentAttribute;
		adjacentAttribute = newAdjacentAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ADJACENT_ATTRIBUTE, oldAdjacentAttribute, newAdjacentAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAdjacentAttribute(AdjacentAttribute newAdjacentAttribute) {
		if (newAdjacentAttribute != adjacentAttribute) {
			NotificationChain msgs = null;
			if (adjacentAttribute != null)
				msgs = ((InternalEObject)adjacentAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ADJACENT_ATTRIBUTE, null, msgs);
			if (newAdjacentAttribute != null)
				msgs = ((InternalEObject)newAdjacentAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ADJACENT_ATTRIBUTE, null, msgs);
			msgs = basicSetAdjacentAttribute(newAdjacentAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ADJACENT_ATTRIBUTE, newAdjacentAttribute, newAdjacentAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AftAttribute getAftAttribute() {
		return aftAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAftAttribute(AftAttribute newAftAttribute, NotificationChain msgs) {
		AftAttribute oldAftAttribute = aftAttribute;
		aftAttribute = newAftAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__AFT_ATTRIBUTE, oldAftAttribute, newAftAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAftAttribute(AftAttribute newAftAttribute) {
		if (newAftAttribute != aftAttribute) {
			NotificationChain msgs = null;
			if (aftAttribute != null)
				msgs = ((InternalEObject)aftAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__AFT_ATTRIBUTE, null, msgs);
			if (newAftAttribute != null)
				msgs = ((InternalEObject)newAftAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__AFT_ATTRIBUTE, null, msgs);
			msgs = basicSetAftAttribute(newAftAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__AFT_ATTRIBUTE, newAftAttribute, newAftAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlignmentAttribute getAlignmentAttribute() {
		return alignmentAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAlignmentAttribute(AlignmentAttribute newAlignmentAttribute, NotificationChain msgs) {
		AlignmentAttribute oldAlignmentAttribute = alignmentAttribute;
		alignmentAttribute = newAlignmentAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ALIGNMENT_ATTRIBUTE, oldAlignmentAttribute, newAlignmentAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAlignmentAttribute(AlignmentAttribute newAlignmentAttribute) {
		if (newAlignmentAttribute != alignmentAttribute) {
			NotificationChain msgs = null;
			if (alignmentAttribute != null)
				msgs = ((InternalEObject)alignmentAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ALIGNMENT_ATTRIBUTE, null, msgs);
			if (newAlignmentAttribute != null)
				msgs = ((InternalEObject)newAlignmentAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ALIGNMENT_ATTRIBUTE, null, msgs);
			msgs = basicSetAlignmentAttribute(newAlignmentAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ALIGNMENT_ATTRIBUTE, newAlignmentAttribute, newAlignmentAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseAttribute getBaseAttribute() {
		return baseAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBaseAttribute(BaseAttribute newBaseAttribute, NotificationChain msgs) {
		BaseAttribute oldBaseAttribute = baseAttribute;
		baseAttribute = newBaseAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__BASE_ATTRIBUTE, oldBaseAttribute, newBaseAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBaseAttribute(BaseAttribute newBaseAttribute) {
		if (newBaseAttribute != baseAttribute) {
			NotificationChain msgs = null;
			if (baseAttribute != null)
				msgs = ((InternalEObject)baseAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__BASE_ATTRIBUTE, null, msgs);
			if (newBaseAttribute != null)
				msgs = ((InternalEObject)newBaseAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__BASE_ATTRIBUTE, null, msgs);
			msgs = basicSetBaseAttribute(newBaseAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__BASE_ATTRIBUTE, newBaseAttribute, newBaseAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BitOrderAttribute getBitOrderAttribute() {
		return bitOrderAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBitOrderAttribute(BitOrderAttribute newBitOrderAttribute, NotificationChain msgs) {
		BitOrderAttribute oldBitOrderAttribute = bitOrderAttribute;
		bitOrderAttribute = newBitOrderAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__BIT_ORDER_ATTRIBUTE, oldBitOrderAttribute, newBitOrderAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBitOrderAttribute(BitOrderAttribute newBitOrderAttribute) {
		if (newBitOrderAttribute != bitOrderAttribute) {
			NotificationChain msgs = null;
			if (bitOrderAttribute != null)
				msgs = ((InternalEObject)bitOrderAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__BIT_ORDER_ATTRIBUTE, null, msgs);
			if (newBitOrderAttribute != null)
				msgs = ((InternalEObject)newBitOrderAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__BIT_ORDER_ATTRIBUTE, null, msgs);
			msgs = basicSetBitOrderAttribute(newBitOrderAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__BIT_ORDER_ATTRIBUTE, newBitOrderAttribute, newBitOrderAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BodyVersionAttribute getBodyVersionAttribute() {
		return bodyVersionAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBodyVersionAttribute(BodyVersionAttribute newBodyVersionAttribute, NotificationChain msgs) {
		BodyVersionAttribute oldBodyVersionAttribute = bodyVersionAttribute;
		bodyVersionAttribute = newBodyVersionAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__BODY_VERSION_ATTRIBUTE, oldBodyVersionAttribute, newBodyVersionAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBodyVersionAttribute(BodyVersionAttribute newBodyVersionAttribute) {
		if (newBodyVersionAttribute != bodyVersionAttribute) {
			NotificationChain msgs = null;
			if (bodyVersionAttribute != null)
				msgs = ((InternalEObject)bodyVersionAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__BODY_VERSION_ATTRIBUTE, null, msgs);
			if (newBodyVersionAttribute != null)
				msgs = ((InternalEObject)newBodyVersionAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__BODY_VERSION_ATTRIBUTE, null, msgs);
			msgs = basicSetBodyVersionAttribute(newBodyVersionAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__BODY_VERSION_ATTRIBUTE, newBodyVersionAttribute, newBodyVersionAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallableAttribute getCallableAttribute() {
		return callableAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCallableAttribute(CallableAttribute newCallableAttribute, NotificationChain msgs) {
		CallableAttribute oldCallableAttribute = callableAttribute;
		callableAttribute = newCallableAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__CALLABLE_ATTRIBUTE, oldCallableAttribute, newCallableAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCallableAttribute(CallableAttribute newCallableAttribute) {
		if (newCallableAttribute != callableAttribute) {
			NotificationChain msgs = null;
			if (callableAttribute != null)
				msgs = ((InternalEObject)callableAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__CALLABLE_ATTRIBUTE, null, msgs);
			if (newCallableAttribute != null)
				msgs = ((InternalEObject)newCallableAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__CALLABLE_ATTRIBUTE, null, msgs);
			msgs = basicSetCallableAttribute(newCallableAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__CALLABLE_ATTRIBUTE, newCallableAttribute, newCallableAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallerAttribute getCallerAttribute() {
		return callerAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCallerAttribute(CallerAttribute newCallerAttribute, NotificationChain msgs) {
		CallerAttribute oldCallerAttribute = callerAttribute;
		callerAttribute = newCallerAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__CALLER_ATTRIBUTE, oldCallerAttribute, newCallerAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCallerAttribute(CallerAttribute newCallerAttribute) {
		if (newCallerAttribute != callerAttribute) {
			NotificationChain msgs = null;
			if (callerAttribute != null)
				msgs = ((InternalEObject)callerAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__CALLER_ATTRIBUTE, null, msgs);
			if (newCallerAttribute != null)
				msgs = ((InternalEObject)newCallerAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__CALLER_ATTRIBUTE, null, msgs);
			msgs = basicSetCallerAttribute(newCallerAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__CALLER_ATTRIBUTE, newCallerAttribute, newCallerAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CeilingAttribute getCeilingAttribute() {
		return ceilingAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCeilingAttribute(CeilingAttribute newCeilingAttribute, NotificationChain msgs) {
		CeilingAttribute oldCeilingAttribute = ceilingAttribute;
		ceilingAttribute = newCeilingAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__CEILING_ATTRIBUTE, oldCeilingAttribute, newCeilingAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCeilingAttribute(CeilingAttribute newCeilingAttribute) {
		if (newCeilingAttribute != ceilingAttribute) {
			NotificationChain msgs = null;
			if (ceilingAttribute != null)
				msgs = ((InternalEObject)ceilingAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__CEILING_ATTRIBUTE, null, msgs);
			if (newCeilingAttribute != null)
				msgs = ((InternalEObject)newCeilingAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__CEILING_ATTRIBUTE, null, msgs);
			msgs = basicSetCeilingAttribute(newCeilingAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__CEILING_ATTRIBUTE, newCeilingAttribute, newCeilingAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassAttribute getClassAttribute() {
		return classAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClassAttribute(ClassAttribute newClassAttribute, NotificationChain msgs) {
		ClassAttribute oldClassAttribute = classAttribute;
		classAttribute = newClassAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__CLASS_ATTRIBUTE, oldClassAttribute, newClassAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassAttribute(ClassAttribute newClassAttribute) {
		if (newClassAttribute != classAttribute) {
			NotificationChain msgs = null;
			if (classAttribute != null)
				msgs = ((InternalEObject)classAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__CLASS_ATTRIBUTE, null, msgs);
			if (newClassAttribute != null)
				msgs = ((InternalEObject)newClassAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__CLASS_ATTRIBUTE, null, msgs);
			msgs = basicSetClassAttribute(newClassAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__CLASS_ATTRIBUTE, newClassAttribute, newClassAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentSizeAttribute getComponentSizeAttribute() {
		return componentSizeAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComponentSizeAttribute(ComponentSizeAttribute newComponentSizeAttribute, NotificationChain msgs) {
		ComponentSizeAttribute oldComponentSizeAttribute = componentSizeAttribute;
		componentSizeAttribute = newComponentSizeAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__COMPONENT_SIZE_ATTRIBUTE, oldComponentSizeAttribute, newComponentSizeAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponentSizeAttribute(ComponentSizeAttribute newComponentSizeAttribute) {
		if (newComponentSizeAttribute != componentSizeAttribute) {
			NotificationChain msgs = null;
			if (componentSizeAttribute != null)
				msgs = ((InternalEObject)componentSizeAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__COMPONENT_SIZE_ATTRIBUTE, null, msgs);
			if (newComponentSizeAttribute != null)
				msgs = ((InternalEObject)newComponentSizeAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__COMPONENT_SIZE_ATTRIBUTE, null, msgs);
			msgs = basicSetComponentSizeAttribute(newComponentSizeAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__COMPONENT_SIZE_ATTRIBUTE, newComponentSizeAttribute, newComponentSizeAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComposeAttribute getComposeAttribute() {
		return composeAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComposeAttribute(ComposeAttribute newComposeAttribute, NotificationChain msgs) {
		ComposeAttribute oldComposeAttribute = composeAttribute;
		composeAttribute = newComposeAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__COMPOSE_ATTRIBUTE, oldComposeAttribute, newComposeAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComposeAttribute(ComposeAttribute newComposeAttribute) {
		if (newComposeAttribute != composeAttribute) {
			NotificationChain msgs = null;
			if (composeAttribute != null)
				msgs = ((InternalEObject)composeAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__COMPOSE_ATTRIBUTE, null, msgs);
			if (newComposeAttribute != null)
				msgs = ((InternalEObject)newComposeAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__COMPOSE_ATTRIBUTE, null, msgs);
			msgs = basicSetComposeAttribute(newComposeAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__COMPOSE_ATTRIBUTE, newComposeAttribute, newComposeAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstrainedAttribute getConstrainedAttribute() {
		return constrainedAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConstrainedAttribute(ConstrainedAttribute newConstrainedAttribute, NotificationChain msgs) {
		ConstrainedAttribute oldConstrainedAttribute = constrainedAttribute;
		constrainedAttribute = newConstrainedAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__CONSTRAINED_ATTRIBUTE, oldConstrainedAttribute, newConstrainedAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstrainedAttribute(ConstrainedAttribute newConstrainedAttribute) {
		if (newConstrainedAttribute != constrainedAttribute) {
			NotificationChain msgs = null;
			if (constrainedAttribute != null)
				msgs = ((InternalEObject)constrainedAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__CONSTRAINED_ATTRIBUTE, null, msgs);
			if (newConstrainedAttribute != null)
				msgs = ((InternalEObject)newConstrainedAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__CONSTRAINED_ATTRIBUTE, null, msgs);
			msgs = basicSetConstrainedAttribute(newConstrainedAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__CONSTRAINED_ATTRIBUTE, newConstrainedAttribute, newConstrainedAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CopySignAttribute getCopySignAttribute() {
		return copySignAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCopySignAttribute(CopySignAttribute newCopySignAttribute, NotificationChain msgs) {
		CopySignAttribute oldCopySignAttribute = copySignAttribute;
		copySignAttribute = newCopySignAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__COPY_SIGN_ATTRIBUTE, oldCopySignAttribute, newCopySignAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCopySignAttribute(CopySignAttribute newCopySignAttribute) {
		if (newCopySignAttribute != copySignAttribute) {
			NotificationChain msgs = null;
			if (copySignAttribute != null)
				msgs = ((InternalEObject)copySignAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__COPY_SIGN_ATTRIBUTE, null, msgs);
			if (newCopySignAttribute != null)
				msgs = ((InternalEObject)newCopySignAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__COPY_SIGN_ATTRIBUTE, null, msgs);
			msgs = basicSetCopySignAttribute(newCopySignAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__COPY_SIGN_ATTRIBUTE, newCopySignAttribute, newCopySignAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CountAttribute getCountAttribute() {
		return countAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCountAttribute(CountAttribute newCountAttribute, NotificationChain msgs) {
		CountAttribute oldCountAttribute = countAttribute;
		countAttribute = newCountAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__COUNT_ATTRIBUTE, oldCountAttribute, newCountAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCountAttribute(CountAttribute newCountAttribute) {
		if (newCountAttribute != countAttribute) {
			NotificationChain msgs = null;
			if (countAttribute != null)
				msgs = ((InternalEObject)countAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__COUNT_ATTRIBUTE, null, msgs);
			if (newCountAttribute != null)
				msgs = ((InternalEObject)newCountAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__COUNT_ATTRIBUTE, null, msgs);
			msgs = basicSetCountAttribute(newCountAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__COUNT_ATTRIBUTE, newCountAttribute, newCountAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiniteAttribute getDefiniteAttribute() {
		return definiteAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiniteAttribute(DefiniteAttribute newDefiniteAttribute, NotificationChain msgs) {
		DefiniteAttribute oldDefiniteAttribute = definiteAttribute;
		definiteAttribute = newDefiniteAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__DEFINITE_ATTRIBUTE, oldDefiniteAttribute, newDefiniteAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiniteAttribute(DefiniteAttribute newDefiniteAttribute) {
		if (newDefiniteAttribute != definiteAttribute) {
			NotificationChain msgs = null;
			if (definiteAttribute != null)
				msgs = ((InternalEObject)definiteAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__DEFINITE_ATTRIBUTE, null, msgs);
			if (newDefiniteAttribute != null)
				msgs = ((InternalEObject)newDefiniteAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__DEFINITE_ATTRIBUTE, null, msgs);
			msgs = basicSetDefiniteAttribute(newDefiniteAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__DEFINITE_ATTRIBUTE, newDefiniteAttribute, newDefiniteAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeltaAttribute getDeltaAttribute() {
		return deltaAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDeltaAttribute(DeltaAttribute newDeltaAttribute, NotificationChain msgs) {
		DeltaAttribute oldDeltaAttribute = deltaAttribute;
		deltaAttribute = newDeltaAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__DELTA_ATTRIBUTE, oldDeltaAttribute, newDeltaAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeltaAttribute(DeltaAttribute newDeltaAttribute) {
		if (newDeltaAttribute != deltaAttribute) {
			NotificationChain msgs = null;
			if (deltaAttribute != null)
				msgs = ((InternalEObject)deltaAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__DELTA_ATTRIBUTE, null, msgs);
			if (newDeltaAttribute != null)
				msgs = ((InternalEObject)newDeltaAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__DELTA_ATTRIBUTE, null, msgs);
			msgs = basicSetDeltaAttribute(newDeltaAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__DELTA_ATTRIBUTE, newDeltaAttribute, newDeltaAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DenormAttribute getDenormAttribute() {
		return denormAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDenormAttribute(DenormAttribute newDenormAttribute, NotificationChain msgs) {
		DenormAttribute oldDenormAttribute = denormAttribute;
		denormAttribute = newDenormAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__DENORM_ATTRIBUTE, oldDenormAttribute, newDenormAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDenormAttribute(DenormAttribute newDenormAttribute) {
		if (newDenormAttribute != denormAttribute) {
			NotificationChain msgs = null;
			if (denormAttribute != null)
				msgs = ((InternalEObject)denormAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__DENORM_ATTRIBUTE, null, msgs);
			if (newDenormAttribute != null)
				msgs = ((InternalEObject)newDenormAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__DENORM_ATTRIBUTE, null, msgs);
			msgs = basicSetDenormAttribute(newDenormAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__DENORM_ATTRIBUTE, newDenormAttribute, newDenormAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DigitsAttribute getDigitsAttribute() {
		return digitsAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDigitsAttribute(DigitsAttribute newDigitsAttribute, NotificationChain msgs) {
		DigitsAttribute oldDigitsAttribute = digitsAttribute;
		digitsAttribute = newDigitsAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__DIGITS_ATTRIBUTE, oldDigitsAttribute, newDigitsAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDigitsAttribute(DigitsAttribute newDigitsAttribute) {
		if (newDigitsAttribute != digitsAttribute) {
			NotificationChain msgs = null;
			if (digitsAttribute != null)
				msgs = ((InternalEObject)digitsAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__DIGITS_ATTRIBUTE, null, msgs);
			if (newDigitsAttribute != null)
				msgs = ((InternalEObject)newDigitsAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__DIGITS_ATTRIBUTE, null, msgs);
			msgs = basicSetDigitsAttribute(newDigitsAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__DIGITS_ATTRIBUTE, newDigitsAttribute, newDigitsAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExponentAttribute getExponentAttribute() {
		return exponentAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExponentAttribute(ExponentAttribute newExponentAttribute, NotificationChain msgs) {
		ExponentAttribute oldExponentAttribute = exponentAttribute;
		exponentAttribute = newExponentAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__EXPONENT_ATTRIBUTE, oldExponentAttribute, newExponentAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExponentAttribute(ExponentAttribute newExponentAttribute) {
		if (newExponentAttribute != exponentAttribute) {
			NotificationChain msgs = null;
			if (exponentAttribute != null)
				msgs = ((InternalEObject)exponentAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__EXPONENT_ATTRIBUTE, null, msgs);
			if (newExponentAttribute != null)
				msgs = ((InternalEObject)newExponentAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__EXPONENT_ATTRIBUTE, null, msgs);
			msgs = basicSetExponentAttribute(newExponentAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__EXPONENT_ATTRIBUTE, newExponentAttribute, newExponentAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExternalTagAttribute getExternalTagAttribute() {
		return externalTagAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExternalTagAttribute(ExternalTagAttribute newExternalTagAttribute, NotificationChain msgs) {
		ExternalTagAttribute oldExternalTagAttribute = externalTagAttribute;
		externalTagAttribute = newExternalTagAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__EXTERNAL_TAG_ATTRIBUTE, oldExternalTagAttribute, newExternalTagAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExternalTagAttribute(ExternalTagAttribute newExternalTagAttribute) {
		if (newExternalTagAttribute != externalTagAttribute) {
			NotificationChain msgs = null;
			if (externalTagAttribute != null)
				msgs = ((InternalEObject)externalTagAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__EXTERNAL_TAG_ATTRIBUTE, null, msgs);
			if (newExternalTagAttribute != null)
				msgs = ((InternalEObject)newExternalTagAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__EXTERNAL_TAG_ATTRIBUTE, null, msgs);
			msgs = basicSetExternalTagAttribute(newExternalTagAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__EXTERNAL_TAG_ATTRIBUTE, newExternalTagAttribute, newExternalTagAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FirstAttribute getFirstAttribute() {
		return firstAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFirstAttribute(FirstAttribute newFirstAttribute, NotificationChain msgs) {
		FirstAttribute oldFirstAttribute = firstAttribute;
		firstAttribute = newFirstAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__FIRST_ATTRIBUTE, oldFirstAttribute, newFirstAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirstAttribute(FirstAttribute newFirstAttribute) {
		if (newFirstAttribute != firstAttribute) {
			NotificationChain msgs = null;
			if (firstAttribute != null)
				msgs = ((InternalEObject)firstAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__FIRST_ATTRIBUTE, null, msgs);
			if (newFirstAttribute != null)
				msgs = ((InternalEObject)newFirstAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__FIRST_ATTRIBUTE, null, msgs);
			msgs = basicSetFirstAttribute(newFirstAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__FIRST_ATTRIBUTE, newFirstAttribute, newFirstAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FirstBitAttribute getFirstBitAttribute() {
		return firstBitAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFirstBitAttribute(FirstBitAttribute newFirstBitAttribute, NotificationChain msgs) {
		FirstBitAttribute oldFirstBitAttribute = firstBitAttribute;
		firstBitAttribute = newFirstBitAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__FIRST_BIT_ATTRIBUTE, oldFirstBitAttribute, newFirstBitAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirstBitAttribute(FirstBitAttribute newFirstBitAttribute) {
		if (newFirstBitAttribute != firstBitAttribute) {
			NotificationChain msgs = null;
			if (firstBitAttribute != null)
				msgs = ((InternalEObject)firstBitAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__FIRST_BIT_ATTRIBUTE, null, msgs);
			if (newFirstBitAttribute != null)
				msgs = ((InternalEObject)newFirstBitAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__FIRST_BIT_ATTRIBUTE, null, msgs);
			msgs = basicSetFirstBitAttribute(newFirstBitAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__FIRST_BIT_ATTRIBUTE, newFirstBitAttribute, newFirstBitAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FloorAttribute getFloorAttribute() {
		return floorAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFloorAttribute(FloorAttribute newFloorAttribute, NotificationChain msgs) {
		FloorAttribute oldFloorAttribute = floorAttribute;
		floorAttribute = newFloorAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__FLOOR_ATTRIBUTE, oldFloorAttribute, newFloorAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFloorAttribute(FloorAttribute newFloorAttribute) {
		if (newFloorAttribute != floorAttribute) {
			NotificationChain msgs = null;
			if (floorAttribute != null)
				msgs = ((InternalEObject)floorAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__FLOOR_ATTRIBUTE, null, msgs);
			if (newFloorAttribute != null)
				msgs = ((InternalEObject)newFloorAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__FLOOR_ATTRIBUTE, null, msgs);
			msgs = basicSetFloorAttribute(newFloorAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__FLOOR_ATTRIBUTE, newFloorAttribute, newFloorAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ForeAttribute getForeAttribute() {
		return foreAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetForeAttribute(ForeAttribute newForeAttribute, NotificationChain msgs) {
		ForeAttribute oldForeAttribute = foreAttribute;
		foreAttribute = newForeAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__FORE_ATTRIBUTE, oldForeAttribute, newForeAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setForeAttribute(ForeAttribute newForeAttribute) {
		if (newForeAttribute != foreAttribute) {
			NotificationChain msgs = null;
			if (foreAttribute != null)
				msgs = ((InternalEObject)foreAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__FORE_ATTRIBUTE, null, msgs);
			if (newForeAttribute != null)
				msgs = ((InternalEObject)newForeAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__FORE_ATTRIBUTE, null, msgs);
			msgs = basicSetForeAttribute(newForeAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__FORE_ATTRIBUTE, newForeAttribute, newForeAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FractionAttribute getFractionAttribute() {
		return fractionAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFractionAttribute(FractionAttribute newFractionAttribute, NotificationChain msgs) {
		FractionAttribute oldFractionAttribute = fractionAttribute;
		fractionAttribute = newFractionAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__FRACTION_ATTRIBUTE, oldFractionAttribute, newFractionAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFractionAttribute(FractionAttribute newFractionAttribute) {
		if (newFractionAttribute != fractionAttribute) {
			NotificationChain msgs = null;
			if (fractionAttribute != null)
				msgs = ((InternalEObject)fractionAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__FRACTION_ATTRIBUTE, null, msgs);
			if (newFractionAttribute != null)
				msgs = ((InternalEObject)newFractionAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__FRACTION_ATTRIBUTE, null, msgs);
			msgs = basicSetFractionAttribute(newFractionAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__FRACTION_ATTRIBUTE, newFractionAttribute, newFractionAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentityAttribute getIdentityAttribute() {
		return identityAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIdentityAttribute(IdentityAttribute newIdentityAttribute, NotificationChain msgs) {
		IdentityAttribute oldIdentityAttribute = identityAttribute;
		identityAttribute = newIdentityAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__IDENTITY_ATTRIBUTE, oldIdentityAttribute, newIdentityAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdentityAttribute(IdentityAttribute newIdentityAttribute) {
		if (newIdentityAttribute != identityAttribute) {
			NotificationChain msgs = null;
			if (identityAttribute != null)
				msgs = ((InternalEObject)identityAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__IDENTITY_ATTRIBUTE, null, msgs);
			if (newIdentityAttribute != null)
				msgs = ((InternalEObject)newIdentityAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__IDENTITY_ATTRIBUTE, null, msgs);
			msgs = basicSetIdentityAttribute(newIdentityAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__IDENTITY_ATTRIBUTE, newIdentityAttribute, newIdentityAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImageAttribute getImageAttribute() {
		return imageAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImageAttribute(ImageAttribute newImageAttribute, NotificationChain msgs) {
		ImageAttribute oldImageAttribute = imageAttribute;
		imageAttribute = newImageAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__IMAGE_ATTRIBUTE, oldImageAttribute, newImageAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImageAttribute(ImageAttribute newImageAttribute) {
		if (newImageAttribute != imageAttribute) {
			NotificationChain msgs = null;
			if (imageAttribute != null)
				msgs = ((InternalEObject)imageAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__IMAGE_ATTRIBUTE, null, msgs);
			if (newImageAttribute != null)
				msgs = ((InternalEObject)newImageAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__IMAGE_ATTRIBUTE, null, msgs);
			msgs = basicSetImageAttribute(newImageAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__IMAGE_ATTRIBUTE, newImageAttribute, newImageAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InputAttribute getInputAttribute() {
		return inputAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInputAttribute(InputAttribute newInputAttribute, NotificationChain msgs) {
		InputAttribute oldInputAttribute = inputAttribute;
		inputAttribute = newInputAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__INPUT_ATTRIBUTE, oldInputAttribute, newInputAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInputAttribute(InputAttribute newInputAttribute) {
		if (newInputAttribute != inputAttribute) {
			NotificationChain msgs = null;
			if (inputAttribute != null)
				msgs = ((InternalEObject)inputAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__INPUT_ATTRIBUTE, null, msgs);
			if (newInputAttribute != null)
				msgs = ((InternalEObject)newInputAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__INPUT_ATTRIBUTE, null, msgs);
			msgs = basicSetInputAttribute(newInputAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__INPUT_ATTRIBUTE, newInputAttribute, newInputAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LastAttribute getLastAttribute() {
		return lastAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLastAttribute(LastAttribute newLastAttribute, NotificationChain msgs) {
		LastAttribute oldLastAttribute = lastAttribute;
		lastAttribute = newLastAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__LAST_ATTRIBUTE, oldLastAttribute, newLastAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastAttribute(LastAttribute newLastAttribute) {
		if (newLastAttribute != lastAttribute) {
			NotificationChain msgs = null;
			if (lastAttribute != null)
				msgs = ((InternalEObject)lastAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__LAST_ATTRIBUTE, null, msgs);
			if (newLastAttribute != null)
				msgs = ((InternalEObject)newLastAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__LAST_ATTRIBUTE, null, msgs);
			msgs = basicSetLastAttribute(newLastAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__LAST_ATTRIBUTE, newLastAttribute, newLastAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LastBitAttribute getLastBitAttribute() {
		return lastBitAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLastBitAttribute(LastBitAttribute newLastBitAttribute, NotificationChain msgs) {
		LastBitAttribute oldLastBitAttribute = lastBitAttribute;
		lastBitAttribute = newLastBitAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__LAST_BIT_ATTRIBUTE, oldLastBitAttribute, newLastBitAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastBitAttribute(LastBitAttribute newLastBitAttribute) {
		if (newLastBitAttribute != lastBitAttribute) {
			NotificationChain msgs = null;
			if (lastBitAttribute != null)
				msgs = ((InternalEObject)lastBitAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__LAST_BIT_ATTRIBUTE, null, msgs);
			if (newLastBitAttribute != null)
				msgs = ((InternalEObject)newLastBitAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__LAST_BIT_ATTRIBUTE, null, msgs);
			msgs = basicSetLastBitAttribute(newLastBitAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__LAST_BIT_ATTRIBUTE, newLastBitAttribute, newLastBitAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeadingPartAttribute getLeadingPartAttribute() {
		return leadingPartAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLeadingPartAttribute(LeadingPartAttribute newLeadingPartAttribute, NotificationChain msgs) {
		LeadingPartAttribute oldLeadingPartAttribute = leadingPartAttribute;
		leadingPartAttribute = newLeadingPartAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__LEADING_PART_ATTRIBUTE, oldLeadingPartAttribute, newLeadingPartAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLeadingPartAttribute(LeadingPartAttribute newLeadingPartAttribute) {
		if (newLeadingPartAttribute != leadingPartAttribute) {
			NotificationChain msgs = null;
			if (leadingPartAttribute != null)
				msgs = ((InternalEObject)leadingPartAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__LEADING_PART_ATTRIBUTE, null, msgs);
			if (newLeadingPartAttribute != null)
				msgs = ((InternalEObject)newLeadingPartAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__LEADING_PART_ATTRIBUTE, null, msgs);
			msgs = basicSetLeadingPartAttribute(newLeadingPartAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__LEADING_PART_ATTRIBUTE, newLeadingPartAttribute, newLeadingPartAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LengthAttribute getLengthAttribute() {
		return lengthAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLengthAttribute(LengthAttribute newLengthAttribute, NotificationChain msgs) {
		LengthAttribute oldLengthAttribute = lengthAttribute;
		lengthAttribute = newLengthAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__LENGTH_ATTRIBUTE, oldLengthAttribute, newLengthAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLengthAttribute(LengthAttribute newLengthAttribute) {
		if (newLengthAttribute != lengthAttribute) {
			NotificationChain msgs = null;
			if (lengthAttribute != null)
				msgs = ((InternalEObject)lengthAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__LENGTH_ATTRIBUTE, null, msgs);
			if (newLengthAttribute != null)
				msgs = ((InternalEObject)newLengthAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__LENGTH_ATTRIBUTE, null, msgs);
			msgs = basicSetLengthAttribute(newLengthAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__LENGTH_ATTRIBUTE, newLengthAttribute, newLengthAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MachineAttribute getMachineAttribute() {
		return machineAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMachineAttribute(MachineAttribute newMachineAttribute, NotificationChain msgs) {
		MachineAttribute oldMachineAttribute = machineAttribute;
		machineAttribute = newMachineAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MACHINE_ATTRIBUTE, oldMachineAttribute, newMachineAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMachineAttribute(MachineAttribute newMachineAttribute) {
		if (newMachineAttribute != machineAttribute) {
			NotificationChain msgs = null;
			if (machineAttribute != null)
				msgs = ((InternalEObject)machineAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MACHINE_ATTRIBUTE, null, msgs);
			if (newMachineAttribute != null)
				msgs = ((InternalEObject)newMachineAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MACHINE_ATTRIBUTE, null, msgs);
			msgs = basicSetMachineAttribute(newMachineAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MACHINE_ATTRIBUTE, newMachineAttribute, newMachineAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MachineEmaxAttribute getMachineEmaxAttribute() {
		return machineEmaxAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMachineEmaxAttribute(MachineEmaxAttribute newMachineEmaxAttribute, NotificationChain msgs) {
		MachineEmaxAttribute oldMachineEmaxAttribute = machineEmaxAttribute;
		machineEmaxAttribute = newMachineEmaxAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MACHINE_EMAX_ATTRIBUTE, oldMachineEmaxAttribute, newMachineEmaxAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMachineEmaxAttribute(MachineEmaxAttribute newMachineEmaxAttribute) {
		if (newMachineEmaxAttribute != machineEmaxAttribute) {
			NotificationChain msgs = null;
			if (machineEmaxAttribute != null)
				msgs = ((InternalEObject)machineEmaxAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MACHINE_EMAX_ATTRIBUTE, null, msgs);
			if (newMachineEmaxAttribute != null)
				msgs = ((InternalEObject)newMachineEmaxAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MACHINE_EMAX_ATTRIBUTE, null, msgs);
			msgs = basicSetMachineEmaxAttribute(newMachineEmaxAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MACHINE_EMAX_ATTRIBUTE, newMachineEmaxAttribute, newMachineEmaxAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MachineEminAttribute getMachineEminAttribute() {
		return machineEminAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMachineEminAttribute(MachineEminAttribute newMachineEminAttribute, NotificationChain msgs) {
		MachineEminAttribute oldMachineEminAttribute = machineEminAttribute;
		machineEminAttribute = newMachineEminAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MACHINE_EMIN_ATTRIBUTE, oldMachineEminAttribute, newMachineEminAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMachineEminAttribute(MachineEminAttribute newMachineEminAttribute) {
		if (newMachineEminAttribute != machineEminAttribute) {
			NotificationChain msgs = null;
			if (machineEminAttribute != null)
				msgs = ((InternalEObject)machineEminAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MACHINE_EMIN_ATTRIBUTE, null, msgs);
			if (newMachineEminAttribute != null)
				msgs = ((InternalEObject)newMachineEminAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MACHINE_EMIN_ATTRIBUTE, null, msgs);
			msgs = basicSetMachineEminAttribute(newMachineEminAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MACHINE_EMIN_ATTRIBUTE, newMachineEminAttribute, newMachineEminAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MachineMantissaAttribute getMachineMantissaAttribute() {
		return machineMantissaAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMachineMantissaAttribute(MachineMantissaAttribute newMachineMantissaAttribute, NotificationChain msgs) {
		MachineMantissaAttribute oldMachineMantissaAttribute = machineMantissaAttribute;
		machineMantissaAttribute = newMachineMantissaAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MACHINE_MANTISSA_ATTRIBUTE, oldMachineMantissaAttribute, newMachineMantissaAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMachineMantissaAttribute(MachineMantissaAttribute newMachineMantissaAttribute) {
		if (newMachineMantissaAttribute != machineMantissaAttribute) {
			NotificationChain msgs = null;
			if (machineMantissaAttribute != null)
				msgs = ((InternalEObject)machineMantissaAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MACHINE_MANTISSA_ATTRIBUTE, null, msgs);
			if (newMachineMantissaAttribute != null)
				msgs = ((InternalEObject)newMachineMantissaAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MACHINE_MANTISSA_ATTRIBUTE, null, msgs);
			msgs = basicSetMachineMantissaAttribute(newMachineMantissaAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MACHINE_MANTISSA_ATTRIBUTE, newMachineMantissaAttribute, newMachineMantissaAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MachineOverflowsAttribute getMachineOverflowsAttribute() {
		return machineOverflowsAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMachineOverflowsAttribute(MachineOverflowsAttribute newMachineOverflowsAttribute, NotificationChain msgs) {
		MachineOverflowsAttribute oldMachineOverflowsAttribute = machineOverflowsAttribute;
		machineOverflowsAttribute = newMachineOverflowsAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MACHINE_OVERFLOWS_ATTRIBUTE, oldMachineOverflowsAttribute, newMachineOverflowsAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMachineOverflowsAttribute(MachineOverflowsAttribute newMachineOverflowsAttribute) {
		if (newMachineOverflowsAttribute != machineOverflowsAttribute) {
			NotificationChain msgs = null;
			if (machineOverflowsAttribute != null)
				msgs = ((InternalEObject)machineOverflowsAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MACHINE_OVERFLOWS_ATTRIBUTE, null, msgs);
			if (newMachineOverflowsAttribute != null)
				msgs = ((InternalEObject)newMachineOverflowsAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MACHINE_OVERFLOWS_ATTRIBUTE, null, msgs);
			msgs = basicSetMachineOverflowsAttribute(newMachineOverflowsAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MACHINE_OVERFLOWS_ATTRIBUTE, newMachineOverflowsAttribute, newMachineOverflowsAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MachineRadixAttribute getMachineRadixAttribute() {
		return machineRadixAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMachineRadixAttribute(MachineRadixAttribute newMachineRadixAttribute, NotificationChain msgs) {
		MachineRadixAttribute oldMachineRadixAttribute = machineRadixAttribute;
		machineRadixAttribute = newMachineRadixAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MACHINE_RADIX_ATTRIBUTE, oldMachineRadixAttribute, newMachineRadixAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMachineRadixAttribute(MachineRadixAttribute newMachineRadixAttribute) {
		if (newMachineRadixAttribute != machineRadixAttribute) {
			NotificationChain msgs = null;
			if (machineRadixAttribute != null)
				msgs = ((InternalEObject)machineRadixAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MACHINE_RADIX_ATTRIBUTE, null, msgs);
			if (newMachineRadixAttribute != null)
				msgs = ((InternalEObject)newMachineRadixAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MACHINE_RADIX_ATTRIBUTE, null, msgs);
			msgs = basicSetMachineRadixAttribute(newMachineRadixAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MACHINE_RADIX_ATTRIBUTE, newMachineRadixAttribute, newMachineRadixAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MachineRoundsAttribute getMachineRoundsAttribute() {
		return machineRoundsAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMachineRoundsAttribute(MachineRoundsAttribute newMachineRoundsAttribute, NotificationChain msgs) {
		MachineRoundsAttribute oldMachineRoundsAttribute = machineRoundsAttribute;
		machineRoundsAttribute = newMachineRoundsAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MACHINE_ROUNDS_ATTRIBUTE, oldMachineRoundsAttribute, newMachineRoundsAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMachineRoundsAttribute(MachineRoundsAttribute newMachineRoundsAttribute) {
		if (newMachineRoundsAttribute != machineRoundsAttribute) {
			NotificationChain msgs = null;
			if (machineRoundsAttribute != null)
				msgs = ((InternalEObject)machineRoundsAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MACHINE_ROUNDS_ATTRIBUTE, null, msgs);
			if (newMachineRoundsAttribute != null)
				msgs = ((InternalEObject)newMachineRoundsAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MACHINE_ROUNDS_ATTRIBUTE, null, msgs);
			msgs = basicSetMachineRoundsAttribute(newMachineRoundsAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MACHINE_ROUNDS_ATTRIBUTE, newMachineRoundsAttribute, newMachineRoundsAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaxAttribute getMaxAttribute() {
		return maxAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMaxAttribute(MaxAttribute newMaxAttribute, NotificationChain msgs) {
		MaxAttribute oldMaxAttribute = maxAttribute;
		maxAttribute = newMaxAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MAX_ATTRIBUTE, oldMaxAttribute, newMaxAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxAttribute(MaxAttribute newMaxAttribute) {
		if (newMaxAttribute != maxAttribute) {
			NotificationChain msgs = null;
			if (maxAttribute != null)
				msgs = ((InternalEObject)maxAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MAX_ATTRIBUTE, null, msgs);
			if (newMaxAttribute != null)
				msgs = ((InternalEObject)newMaxAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MAX_ATTRIBUTE, null, msgs);
			msgs = basicSetMaxAttribute(newMaxAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MAX_ATTRIBUTE, newMaxAttribute, newMaxAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaxSizeInStorageElementsAttribute getMaxSizeInStorageElementsAttribute() {
		return maxSizeInStorageElementsAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMaxSizeInStorageElementsAttribute(MaxSizeInStorageElementsAttribute newMaxSizeInStorageElementsAttribute, NotificationChain msgs) {
		MaxSizeInStorageElementsAttribute oldMaxSizeInStorageElementsAttribute = maxSizeInStorageElementsAttribute;
		maxSizeInStorageElementsAttribute = newMaxSizeInStorageElementsAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE, oldMaxSizeInStorageElementsAttribute, newMaxSizeInStorageElementsAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxSizeInStorageElementsAttribute(MaxSizeInStorageElementsAttribute newMaxSizeInStorageElementsAttribute) {
		if (newMaxSizeInStorageElementsAttribute != maxSizeInStorageElementsAttribute) {
			NotificationChain msgs = null;
			if (maxSizeInStorageElementsAttribute != null)
				msgs = ((InternalEObject)maxSizeInStorageElementsAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE, null, msgs);
			if (newMaxSizeInStorageElementsAttribute != null)
				msgs = ((InternalEObject)newMaxSizeInStorageElementsAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE, null, msgs);
			msgs = basicSetMaxSizeInStorageElementsAttribute(newMaxSizeInStorageElementsAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE, newMaxSizeInStorageElementsAttribute, newMaxSizeInStorageElementsAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MinAttribute getMinAttribute() {
		return minAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMinAttribute(MinAttribute newMinAttribute, NotificationChain msgs) {
		MinAttribute oldMinAttribute = minAttribute;
		minAttribute = newMinAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MIN_ATTRIBUTE, oldMinAttribute, newMinAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinAttribute(MinAttribute newMinAttribute) {
		if (newMinAttribute != minAttribute) {
			NotificationChain msgs = null;
			if (minAttribute != null)
				msgs = ((InternalEObject)minAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MIN_ATTRIBUTE, null, msgs);
			if (newMinAttribute != null)
				msgs = ((InternalEObject)newMinAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MIN_ATTRIBUTE, null, msgs);
			msgs = basicSetMinAttribute(newMinAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MIN_ATTRIBUTE, newMinAttribute, newMinAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelAttribute getModelAttribute() {
		return modelAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetModelAttribute(ModelAttribute newModelAttribute, NotificationChain msgs) {
		ModelAttribute oldModelAttribute = modelAttribute;
		modelAttribute = newModelAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MODEL_ATTRIBUTE, oldModelAttribute, newModelAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelAttribute(ModelAttribute newModelAttribute) {
		if (newModelAttribute != modelAttribute) {
			NotificationChain msgs = null;
			if (modelAttribute != null)
				msgs = ((InternalEObject)modelAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MODEL_ATTRIBUTE, null, msgs);
			if (newModelAttribute != null)
				msgs = ((InternalEObject)newModelAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MODEL_ATTRIBUTE, null, msgs);
			msgs = basicSetModelAttribute(newModelAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MODEL_ATTRIBUTE, newModelAttribute, newModelAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelEminAttribute getModelEminAttribute() {
		return modelEminAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetModelEminAttribute(ModelEminAttribute newModelEminAttribute, NotificationChain msgs) {
		ModelEminAttribute oldModelEminAttribute = modelEminAttribute;
		modelEminAttribute = newModelEminAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MODEL_EMIN_ATTRIBUTE, oldModelEminAttribute, newModelEminAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelEminAttribute(ModelEminAttribute newModelEminAttribute) {
		if (newModelEminAttribute != modelEminAttribute) {
			NotificationChain msgs = null;
			if (modelEminAttribute != null)
				msgs = ((InternalEObject)modelEminAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MODEL_EMIN_ATTRIBUTE, null, msgs);
			if (newModelEminAttribute != null)
				msgs = ((InternalEObject)newModelEminAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MODEL_EMIN_ATTRIBUTE, null, msgs);
			msgs = basicSetModelEminAttribute(newModelEminAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MODEL_EMIN_ATTRIBUTE, newModelEminAttribute, newModelEminAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelEpsilonAttribute getModelEpsilonAttribute() {
		return modelEpsilonAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetModelEpsilonAttribute(ModelEpsilonAttribute newModelEpsilonAttribute, NotificationChain msgs) {
		ModelEpsilonAttribute oldModelEpsilonAttribute = modelEpsilonAttribute;
		modelEpsilonAttribute = newModelEpsilonAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MODEL_EPSILON_ATTRIBUTE, oldModelEpsilonAttribute, newModelEpsilonAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelEpsilonAttribute(ModelEpsilonAttribute newModelEpsilonAttribute) {
		if (newModelEpsilonAttribute != modelEpsilonAttribute) {
			NotificationChain msgs = null;
			if (modelEpsilonAttribute != null)
				msgs = ((InternalEObject)modelEpsilonAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MODEL_EPSILON_ATTRIBUTE, null, msgs);
			if (newModelEpsilonAttribute != null)
				msgs = ((InternalEObject)newModelEpsilonAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MODEL_EPSILON_ATTRIBUTE, null, msgs);
			msgs = basicSetModelEpsilonAttribute(newModelEpsilonAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MODEL_EPSILON_ATTRIBUTE, newModelEpsilonAttribute, newModelEpsilonAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelMantissaAttribute getModelMantissaAttribute() {
		return modelMantissaAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetModelMantissaAttribute(ModelMantissaAttribute newModelMantissaAttribute, NotificationChain msgs) {
		ModelMantissaAttribute oldModelMantissaAttribute = modelMantissaAttribute;
		modelMantissaAttribute = newModelMantissaAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MODEL_MANTISSA_ATTRIBUTE, oldModelMantissaAttribute, newModelMantissaAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelMantissaAttribute(ModelMantissaAttribute newModelMantissaAttribute) {
		if (newModelMantissaAttribute != modelMantissaAttribute) {
			NotificationChain msgs = null;
			if (modelMantissaAttribute != null)
				msgs = ((InternalEObject)modelMantissaAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MODEL_MANTISSA_ATTRIBUTE, null, msgs);
			if (newModelMantissaAttribute != null)
				msgs = ((InternalEObject)newModelMantissaAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MODEL_MANTISSA_ATTRIBUTE, null, msgs);
			msgs = basicSetModelMantissaAttribute(newModelMantissaAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MODEL_MANTISSA_ATTRIBUTE, newModelMantissaAttribute, newModelMantissaAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelSmallAttribute getModelSmallAttribute() {
		return modelSmallAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetModelSmallAttribute(ModelSmallAttribute newModelSmallAttribute, NotificationChain msgs) {
		ModelSmallAttribute oldModelSmallAttribute = modelSmallAttribute;
		modelSmallAttribute = newModelSmallAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MODEL_SMALL_ATTRIBUTE, oldModelSmallAttribute, newModelSmallAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelSmallAttribute(ModelSmallAttribute newModelSmallAttribute) {
		if (newModelSmallAttribute != modelSmallAttribute) {
			NotificationChain msgs = null;
			if (modelSmallAttribute != null)
				msgs = ((InternalEObject)modelSmallAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MODEL_SMALL_ATTRIBUTE, null, msgs);
			if (newModelSmallAttribute != null)
				msgs = ((InternalEObject)newModelSmallAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MODEL_SMALL_ATTRIBUTE, null, msgs);
			msgs = basicSetModelSmallAttribute(newModelSmallAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MODEL_SMALL_ATTRIBUTE, newModelSmallAttribute, newModelSmallAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModulusAttribute getModulusAttribute() {
		return modulusAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetModulusAttribute(ModulusAttribute newModulusAttribute, NotificationChain msgs) {
		ModulusAttribute oldModulusAttribute = modulusAttribute;
		modulusAttribute = newModulusAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MODULUS_ATTRIBUTE, oldModulusAttribute, newModulusAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModulusAttribute(ModulusAttribute newModulusAttribute) {
		if (newModulusAttribute != modulusAttribute) {
			NotificationChain msgs = null;
			if (modulusAttribute != null)
				msgs = ((InternalEObject)modulusAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MODULUS_ATTRIBUTE, null, msgs);
			if (newModulusAttribute != null)
				msgs = ((InternalEObject)newModulusAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MODULUS_ATTRIBUTE, null, msgs);
			msgs = basicSetModulusAttribute(newModulusAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MODULUS_ATTRIBUTE, newModulusAttribute, newModulusAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutputAttribute getOutputAttribute() {
		return outputAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOutputAttribute(OutputAttribute newOutputAttribute, NotificationChain msgs) {
		OutputAttribute oldOutputAttribute = outputAttribute;
		outputAttribute = newOutputAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__OUTPUT_ATTRIBUTE, oldOutputAttribute, newOutputAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutputAttribute(OutputAttribute newOutputAttribute) {
		if (newOutputAttribute != outputAttribute) {
			NotificationChain msgs = null;
			if (outputAttribute != null)
				msgs = ((InternalEObject)outputAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__OUTPUT_ATTRIBUTE, null, msgs);
			if (newOutputAttribute != null)
				msgs = ((InternalEObject)newOutputAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__OUTPUT_ATTRIBUTE, null, msgs);
			msgs = basicSetOutputAttribute(newOutputAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__OUTPUT_ATTRIBUTE, newOutputAttribute, newOutputAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PartitionIdAttribute getPartitionIdAttribute() {
		return partitionIdAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPartitionIdAttribute(PartitionIdAttribute newPartitionIdAttribute, NotificationChain msgs) {
		PartitionIdAttribute oldPartitionIdAttribute = partitionIdAttribute;
		partitionIdAttribute = newPartitionIdAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__PARTITION_ID_ATTRIBUTE, oldPartitionIdAttribute, newPartitionIdAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPartitionIdAttribute(PartitionIdAttribute newPartitionIdAttribute) {
		if (newPartitionIdAttribute != partitionIdAttribute) {
			NotificationChain msgs = null;
			if (partitionIdAttribute != null)
				msgs = ((InternalEObject)partitionIdAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__PARTITION_ID_ATTRIBUTE, null, msgs);
			if (newPartitionIdAttribute != null)
				msgs = ((InternalEObject)newPartitionIdAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__PARTITION_ID_ATTRIBUTE, null, msgs);
			msgs = basicSetPartitionIdAttribute(newPartitionIdAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__PARTITION_ID_ATTRIBUTE, newPartitionIdAttribute, newPartitionIdAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PosAttribute getPosAttribute() {
		return posAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPosAttribute(PosAttribute newPosAttribute, NotificationChain msgs) {
		PosAttribute oldPosAttribute = posAttribute;
		posAttribute = newPosAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__POS_ATTRIBUTE, oldPosAttribute, newPosAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPosAttribute(PosAttribute newPosAttribute) {
		if (newPosAttribute != posAttribute) {
			NotificationChain msgs = null;
			if (posAttribute != null)
				msgs = ((InternalEObject)posAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__POS_ATTRIBUTE, null, msgs);
			if (newPosAttribute != null)
				msgs = ((InternalEObject)newPosAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__POS_ATTRIBUTE, null, msgs);
			msgs = basicSetPosAttribute(newPosAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__POS_ATTRIBUTE, newPosAttribute, newPosAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PositionAttribute getPositionAttribute() {
		return positionAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPositionAttribute(PositionAttribute newPositionAttribute, NotificationChain msgs) {
		PositionAttribute oldPositionAttribute = positionAttribute;
		positionAttribute = newPositionAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__POSITION_ATTRIBUTE, oldPositionAttribute, newPositionAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPositionAttribute(PositionAttribute newPositionAttribute) {
		if (newPositionAttribute != positionAttribute) {
			NotificationChain msgs = null;
			if (positionAttribute != null)
				msgs = ((InternalEObject)positionAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__POSITION_ATTRIBUTE, null, msgs);
			if (newPositionAttribute != null)
				msgs = ((InternalEObject)newPositionAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__POSITION_ATTRIBUTE, null, msgs);
			msgs = basicSetPositionAttribute(newPositionAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__POSITION_ATTRIBUTE, newPositionAttribute, newPositionAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PredAttribute getPredAttribute() {
		return predAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPredAttribute(PredAttribute newPredAttribute, NotificationChain msgs) {
		PredAttribute oldPredAttribute = predAttribute;
		predAttribute = newPredAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__PRED_ATTRIBUTE, oldPredAttribute, newPredAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPredAttribute(PredAttribute newPredAttribute) {
		if (newPredAttribute != predAttribute) {
			NotificationChain msgs = null;
			if (predAttribute != null)
				msgs = ((InternalEObject)predAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__PRED_ATTRIBUTE, null, msgs);
			if (newPredAttribute != null)
				msgs = ((InternalEObject)newPredAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__PRED_ATTRIBUTE, null, msgs);
			msgs = basicSetPredAttribute(newPredAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__PRED_ATTRIBUTE, newPredAttribute, newPredAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RangeAttribute getRangeAttribute() {
		return rangeAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRangeAttribute(RangeAttribute newRangeAttribute, NotificationChain msgs) {
		RangeAttribute oldRangeAttribute = rangeAttribute;
		rangeAttribute = newRangeAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__RANGE_ATTRIBUTE, oldRangeAttribute, newRangeAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRangeAttribute(RangeAttribute newRangeAttribute) {
		if (newRangeAttribute != rangeAttribute) {
			NotificationChain msgs = null;
			if (rangeAttribute != null)
				msgs = ((InternalEObject)rangeAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__RANGE_ATTRIBUTE, null, msgs);
			if (newRangeAttribute != null)
				msgs = ((InternalEObject)newRangeAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__RANGE_ATTRIBUTE, null, msgs);
			msgs = basicSetRangeAttribute(newRangeAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__RANGE_ATTRIBUTE, newRangeAttribute, newRangeAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReadAttribute getReadAttribute() {
		return readAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReadAttribute(ReadAttribute newReadAttribute, NotificationChain msgs) {
		ReadAttribute oldReadAttribute = readAttribute;
		readAttribute = newReadAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__READ_ATTRIBUTE, oldReadAttribute, newReadAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReadAttribute(ReadAttribute newReadAttribute) {
		if (newReadAttribute != readAttribute) {
			NotificationChain msgs = null;
			if (readAttribute != null)
				msgs = ((InternalEObject)readAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__READ_ATTRIBUTE, null, msgs);
			if (newReadAttribute != null)
				msgs = ((InternalEObject)newReadAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__READ_ATTRIBUTE, null, msgs);
			msgs = basicSetReadAttribute(newReadAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__READ_ATTRIBUTE, newReadAttribute, newReadAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RemainderAttribute getRemainderAttribute() {
		return remainderAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRemainderAttribute(RemainderAttribute newRemainderAttribute, NotificationChain msgs) {
		RemainderAttribute oldRemainderAttribute = remainderAttribute;
		remainderAttribute = newRemainderAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__REMAINDER_ATTRIBUTE, oldRemainderAttribute, newRemainderAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRemainderAttribute(RemainderAttribute newRemainderAttribute) {
		if (newRemainderAttribute != remainderAttribute) {
			NotificationChain msgs = null;
			if (remainderAttribute != null)
				msgs = ((InternalEObject)remainderAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__REMAINDER_ATTRIBUTE, null, msgs);
			if (newRemainderAttribute != null)
				msgs = ((InternalEObject)newRemainderAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__REMAINDER_ATTRIBUTE, null, msgs);
			msgs = basicSetRemainderAttribute(newRemainderAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__REMAINDER_ATTRIBUTE, newRemainderAttribute, newRemainderAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoundAttribute getRoundAttribute() {
		return roundAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRoundAttribute(RoundAttribute newRoundAttribute, NotificationChain msgs) {
		RoundAttribute oldRoundAttribute = roundAttribute;
		roundAttribute = newRoundAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ROUND_ATTRIBUTE, oldRoundAttribute, newRoundAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoundAttribute(RoundAttribute newRoundAttribute) {
		if (newRoundAttribute != roundAttribute) {
			NotificationChain msgs = null;
			if (roundAttribute != null)
				msgs = ((InternalEObject)roundAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ROUND_ATTRIBUTE, null, msgs);
			if (newRoundAttribute != null)
				msgs = ((InternalEObject)newRoundAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ROUND_ATTRIBUTE, null, msgs);
			msgs = basicSetRoundAttribute(newRoundAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ROUND_ATTRIBUTE, newRoundAttribute, newRoundAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoundingAttribute getRoundingAttribute() {
		return roundingAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRoundingAttribute(RoundingAttribute newRoundingAttribute, NotificationChain msgs) {
		RoundingAttribute oldRoundingAttribute = roundingAttribute;
		roundingAttribute = newRoundingAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ROUNDING_ATTRIBUTE, oldRoundingAttribute, newRoundingAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoundingAttribute(RoundingAttribute newRoundingAttribute) {
		if (newRoundingAttribute != roundingAttribute) {
			NotificationChain msgs = null;
			if (roundingAttribute != null)
				msgs = ((InternalEObject)roundingAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ROUNDING_ATTRIBUTE, null, msgs);
			if (newRoundingAttribute != null)
				msgs = ((InternalEObject)newRoundingAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ROUNDING_ATTRIBUTE, null, msgs);
			msgs = basicSetRoundingAttribute(newRoundingAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ROUNDING_ATTRIBUTE, newRoundingAttribute, newRoundingAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SafeFirstAttribute getSafeFirstAttribute() {
		return safeFirstAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSafeFirstAttribute(SafeFirstAttribute newSafeFirstAttribute, NotificationChain msgs) {
		SafeFirstAttribute oldSafeFirstAttribute = safeFirstAttribute;
		safeFirstAttribute = newSafeFirstAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__SAFE_FIRST_ATTRIBUTE, oldSafeFirstAttribute, newSafeFirstAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSafeFirstAttribute(SafeFirstAttribute newSafeFirstAttribute) {
		if (newSafeFirstAttribute != safeFirstAttribute) {
			NotificationChain msgs = null;
			if (safeFirstAttribute != null)
				msgs = ((InternalEObject)safeFirstAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__SAFE_FIRST_ATTRIBUTE, null, msgs);
			if (newSafeFirstAttribute != null)
				msgs = ((InternalEObject)newSafeFirstAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__SAFE_FIRST_ATTRIBUTE, null, msgs);
			msgs = basicSetSafeFirstAttribute(newSafeFirstAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__SAFE_FIRST_ATTRIBUTE, newSafeFirstAttribute, newSafeFirstAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SafeLastAttribute getSafeLastAttribute() {
		return safeLastAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSafeLastAttribute(SafeLastAttribute newSafeLastAttribute, NotificationChain msgs) {
		SafeLastAttribute oldSafeLastAttribute = safeLastAttribute;
		safeLastAttribute = newSafeLastAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__SAFE_LAST_ATTRIBUTE, oldSafeLastAttribute, newSafeLastAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSafeLastAttribute(SafeLastAttribute newSafeLastAttribute) {
		if (newSafeLastAttribute != safeLastAttribute) {
			NotificationChain msgs = null;
			if (safeLastAttribute != null)
				msgs = ((InternalEObject)safeLastAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__SAFE_LAST_ATTRIBUTE, null, msgs);
			if (newSafeLastAttribute != null)
				msgs = ((InternalEObject)newSafeLastAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__SAFE_LAST_ATTRIBUTE, null, msgs);
			msgs = basicSetSafeLastAttribute(newSafeLastAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__SAFE_LAST_ATTRIBUTE, newSafeLastAttribute, newSafeLastAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScaleAttribute getScaleAttribute() {
		return scaleAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetScaleAttribute(ScaleAttribute newScaleAttribute, NotificationChain msgs) {
		ScaleAttribute oldScaleAttribute = scaleAttribute;
		scaleAttribute = newScaleAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__SCALE_ATTRIBUTE, oldScaleAttribute, newScaleAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScaleAttribute(ScaleAttribute newScaleAttribute) {
		if (newScaleAttribute != scaleAttribute) {
			NotificationChain msgs = null;
			if (scaleAttribute != null)
				msgs = ((InternalEObject)scaleAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__SCALE_ATTRIBUTE, null, msgs);
			if (newScaleAttribute != null)
				msgs = ((InternalEObject)newScaleAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__SCALE_ATTRIBUTE, null, msgs);
			msgs = basicSetScaleAttribute(newScaleAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__SCALE_ATTRIBUTE, newScaleAttribute, newScaleAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScalingAttribute getScalingAttribute() {
		return scalingAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetScalingAttribute(ScalingAttribute newScalingAttribute, NotificationChain msgs) {
		ScalingAttribute oldScalingAttribute = scalingAttribute;
		scalingAttribute = newScalingAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__SCALING_ATTRIBUTE, oldScalingAttribute, newScalingAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScalingAttribute(ScalingAttribute newScalingAttribute) {
		if (newScalingAttribute != scalingAttribute) {
			NotificationChain msgs = null;
			if (scalingAttribute != null)
				msgs = ((InternalEObject)scalingAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__SCALING_ATTRIBUTE, null, msgs);
			if (newScalingAttribute != null)
				msgs = ((InternalEObject)newScalingAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__SCALING_ATTRIBUTE, null, msgs);
			msgs = basicSetScalingAttribute(newScalingAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__SCALING_ATTRIBUTE, newScalingAttribute, newScalingAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignedZerosAttribute getSignedZerosAttribute() {
		return signedZerosAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSignedZerosAttribute(SignedZerosAttribute newSignedZerosAttribute, NotificationChain msgs) {
		SignedZerosAttribute oldSignedZerosAttribute = signedZerosAttribute;
		signedZerosAttribute = newSignedZerosAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__SIGNED_ZEROS_ATTRIBUTE, oldSignedZerosAttribute, newSignedZerosAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSignedZerosAttribute(SignedZerosAttribute newSignedZerosAttribute) {
		if (newSignedZerosAttribute != signedZerosAttribute) {
			NotificationChain msgs = null;
			if (signedZerosAttribute != null)
				msgs = ((InternalEObject)signedZerosAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__SIGNED_ZEROS_ATTRIBUTE, null, msgs);
			if (newSignedZerosAttribute != null)
				msgs = ((InternalEObject)newSignedZerosAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__SIGNED_ZEROS_ATTRIBUTE, null, msgs);
			msgs = basicSetSignedZerosAttribute(newSignedZerosAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__SIGNED_ZEROS_ATTRIBUTE, newSignedZerosAttribute, newSignedZerosAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SizeAttribute getSizeAttribute() {
		return sizeAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSizeAttribute(SizeAttribute newSizeAttribute, NotificationChain msgs) {
		SizeAttribute oldSizeAttribute = sizeAttribute;
		sizeAttribute = newSizeAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__SIZE_ATTRIBUTE, oldSizeAttribute, newSizeAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSizeAttribute(SizeAttribute newSizeAttribute) {
		if (newSizeAttribute != sizeAttribute) {
			NotificationChain msgs = null;
			if (sizeAttribute != null)
				msgs = ((InternalEObject)sizeAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__SIZE_ATTRIBUTE, null, msgs);
			if (newSizeAttribute != null)
				msgs = ((InternalEObject)newSizeAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__SIZE_ATTRIBUTE, null, msgs);
			msgs = basicSetSizeAttribute(newSizeAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__SIZE_ATTRIBUTE, newSizeAttribute, newSizeAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SmallAttribute getSmallAttribute() {
		return smallAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSmallAttribute(SmallAttribute newSmallAttribute, NotificationChain msgs) {
		SmallAttribute oldSmallAttribute = smallAttribute;
		smallAttribute = newSmallAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__SMALL_ATTRIBUTE, oldSmallAttribute, newSmallAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSmallAttribute(SmallAttribute newSmallAttribute) {
		if (newSmallAttribute != smallAttribute) {
			NotificationChain msgs = null;
			if (smallAttribute != null)
				msgs = ((InternalEObject)smallAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__SMALL_ATTRIBUTE, null, msgs);
			if (newSmallAttribute != null)
				msgs = ((InternalEObject)newSmallAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__SMALL_ATTRIBUTE, null, msgs);
			msgs = basicSetSmallAttribute(newSmallAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__SMALL_ATTRIBUTE, newSmallAttribute, newSmallAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StoragePoolAttribute getStoragePoolAttribute() {
		return storagePoolAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStoragePoolAttribute(StoragePoolAttribute newStoragePoolAttribute, NotificationChain msgs) {
		StoragePoolAttribute oldStoragePoolAttribute = storagePoolAttribute;
		storagePoolAttribute = newStoragePoolAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__STORAGE_POOL_ATTRIBUTE, oldStoragePoolAttribute, newStoragePoolAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStoragePoolAttribute(StoragePoolAttribute newStoragePoolAttribute) {
		if (newStoragePoolAttribute != storagePoolAttribute) {
			NotificationChain msgs = null;
			if (storagePoolAttribute != null)
				msgs = ((InternalEObject)storagePoolAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__STORAGE_POOL_ATTRIBUTE, null, msgs);
			if (newStoragePoolAttribute != null)
				msgs = ((InternalEObject)newStoragePoolAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__STORAGE_POOL_ATTRIBUTE, null, msgs);
			msgs = basicSetStoragePoolAttribute(newStoragePoolAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__STORAGE_POOL_ATTRIBUTE, newStoragePoolAttribute, newStoragePoolAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StorageSizeAttribute getStorageSizeAttribute() {
		return storageSizeAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStorageSizeAttribute(StorageSizeAttribute newStorageSizeAttribute, NotificationChain msgs) {
		StorageSizeAttribute oldStorageSizeAttribute = storageSizeAttribute;
		storageSizeAttribute = newStorageSizeAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__STORAGE_SIZE_ATTRIBUTE, oldStorageSizeAttribute, newStorageSizeAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStorageSizeAttribute(StorageSizeAttribute newStorageSizeAttribute) {
		if (newStorageSizeAttribute != storageSizeAttribute) {
			NotificationChain msgs = null;
			if (storageSizeAttribute != null)
				msgs = ((InternalEObject)storageSizeAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__STORAGE_SIZE_ATTRIBUTE, null, msgs);
			if (newStorageSizeAttribute != null)
				msgs = ((InternalEObject)newStorageSizeAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__STORAGE_SIZE_ATTRIBUTE, null, msgs);
			msgs = basicSetStorageSizeAttribute(newStorageSizeAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__STORAGE_SIZE_ATTRIBUTE, newStorageSizeAttribute, newStorageSizeAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SuccAttribute getSuccAttribute() {
		return succAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSuccAttribute(SuccAttribute newSuccAttribute, NotificationChain msgs) {
		SuccAttribute oldSuccAttribute = succAttribute;
		succAttribute = newSuccAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__SUCC_ATTRIBUTE, oldSuccAttribute, newSuccAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuccAttribute(SuccAttribute newSuccAttribute) {
		if (newSuccAttribute != succAttribute) {
			NotificationChain msgs = null;
			if (succAttribute != null)
				msgs = ((InternalEObject)succAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__SUCC_ATTRIBUTE, null, msgs);
			if (newSuccAttribute != null)
				msgs = ((InternalEObject)newSuccAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__SUCC_ATTRIBUTE, null, msgs);
			msgs = basicSetSuccAttribute(newSuccAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__SUCC_ATTRIBUTE, newSuccAttribute, newSuccAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TagAttribute getTagAttribute() {
		return tagAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTagAttribute(TagAttribute newTagAttribute, NotificationChain msgs) {
		TagAttribute oldTagAttribute = tagAttribute;
		tagAttribute = newTagAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__TAG_ATTRIBUTE, oldTagAttribute, newTagAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTagAttribute(TagAttribute newTagAttribute) {
		if (newTagAttribute != tagAttribute) {
			NotificationChain msgs = null;
			if (tagAttribute != null)
				msgs = ((InternalEObject)tagAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__TAG_ATTRIBUTE, null, msgs);
			if (newTagAttribute != null)
				msgs = ((InternalEObject)newTagAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__TAG_ATTRIBUTE, null, msgs);
			msgs = basicSetTagAttribute(newTagAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__TAG_ATTRIBUTE, newTagAttribute, newTagAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TerminatedAttribute getTerminatedAttribute() {
		return terminatedAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTerminatedAttribute(TerminatedAttribute newTerminatedAttribute, NotificationChain msgs) {
		TerminatedAttribute oldTerminatedAttribute = terminatedAttribute;
		terminatedAttribute = newTerminatedAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__TERMINATED_ATTRIBUTE, oldTerminatedAttribute, newTerminatedAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTerminatedAttribute(TerminatedAttribute newTerminatedAttribute) {
		if (newTerminatedAttribute != terminatedAttribute) {
			NotificationChain msgs = null;
			if (terminatedAttribute != null)
				msgs = ((InternalEObject)terminatedAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__TERMINATED_ATTRIBUTE, null, msgs);
			if (newTerminatedAttribute != null)
				msgs = ((InternalEObject)newTerminatedAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__TERMINATED_ATTRIBUTE, null, msgs);
			msgs = basicSetTerminatedAttribute(newTerminatedAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__TERMINATED_ATTRIBUTE, newTerminatedAttribute, newTerminatedAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TruncationAttribute getTruncationAttribute() {
		return truncationAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTruncationAttribute(TruncationAttribute newTruncationAttribute, NotificationChain msgs) {
		TruncationAttribute oldTruncationAttribute = truncationAttribute;
		truncationAttribute = newTruncationAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__TRUNCATION_ATTRIBUTE, oldTruncationAttribute, newTruncationAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTruncationAttribute(TruncationAttribute newTruncationAttribute) {
		if (newTruncationAttribute != truncationAttribute) {
			NotificationChain msgs = null;
			if (truncationAttribute != null)
				msgs = ((InternalEObject)truncationAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__TRUNCATION_ATTRIBUTE, null, msgs);
			if (newTruncationAttribute != null)
				msgs = ((InternalEObject)newTruncationAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__TRUNCATION_ATTRIBUTE, null, msgs);
			msgs = basicSetTruncationAttribute(newTruncationAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__TRUNCATION_ATTRIBUTE, newTruncationAttribute, newTruncationAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnbiasedRoundingAttribute getUnbiasedRoundingAttribute() {
		return unbiasedRoundingAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnbiasedRoundingAttribute(UnbiasedRoundingAttribute newUnbiasedRoundingAttribute, NotificationChain msgs) {
		UnbiasedRoundingAttribute oldUnbiasedRoundingAttribute = unbiasedRoundingAttribute;
		unbiasedRoundingAttribute = newUnbiasedRoundingAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__UNBIASED_ROUNDING_ATTRIBUTE, oldUnbiasedRoundingAttribute, newUnbiasedRoundingAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnbiasedRoundingAttribute(UnbiasedRoundingAttribute newUnbiasedRoundingAttribute) {
		if (newUnbiasedRoundingAttribute != unbiasedRoundingAttribute) {
			NotificationChain msgs = null;
			if (unbiasedRoundingAttribute != null)
				msgs = ((InternalEObject)unbiasedRoundingAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__UNBIASED_ROUNDING_ATTRIBUTE, null, msgs);
			if (newUnbiasedRoundingAttribute != null)
				msgs = ((InternalEObject)newUnbiasedRoundingAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__UNBIASED_ROUNDING_ATTRIBUTE, null, msgs);
			msgs = basicSetUnbiasedRoundingAttribute(newUnbiasedRoundingAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__UNBIASED_ROUNDING_ATTRIBUTE, newUnbiasedRoundingAttribute, newUnbiasedRoundingAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UncheckedAccessAttribute getUncheckedAccessAttribute() {
		return uncheckedAccessAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUncheckedAccessAttribute(UncheckedAccessAttribute newUncheckedAccessAttribute, NotificationChain msgs) {
		UncheckedAccessAttribute oldUncheckedAccessAttribute = uncheckedAccessAttribute;
		uncheckedAccessAttribute = newUncheckedAccessAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__UNCHECKED_ACCESS_ATTRIBUTE, oldUncheckedAccessAttribute, newUncheckedAccessAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUncheckedAccessAttribute(UncheckedAccessAttribute newUncheckedAccessAttribute) {
		if (newUncheckedAccessAttribute != uncheckedAccessAttribute) {
			NotificationChain msgs = null;
			if (uncheckedAccessAttribute != null)
				msgs = ((InternalEObject)uncheckedAccessAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__UNCHECKED_ACCESS_ATTRIBUTE, null, msgs);
			if (newUncheckedAccessAttribute != null)
				msgs = ((InternalEObject)newUncheckedAccessAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__UNCHECKED_ACCESS_ATTRIBUTE, null, msgs);
			msgs = basicSetUncheckedAccessAttribute(newUncheckedAccessAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__UNCHECKED_ACCESS_ATTRIBUTE, newUncheckedAccessAttribute, newUncheckedAccessAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValAttribute getValAttribute() {
		return valAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetValAttribute(ValAttribute newValAttribute, NotificationChain msgs) {
		ValAttribute oldValAttribute = valAttribute;
		valAttribute = newValAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__VAL_ATTRIBUTE, oldValAttribute, newValAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValAttribute(ValAttribute newValAttribute) {
		if (newValAttribute != valAttribute) {
			NotificationChain msgs = null;
			if (valAttribute != null)
				msgs = ((InternalEObject)valAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__VAL_ATTRIBUTE, null, msgs);
			if (newValAttribute != null)
				msgs = ((InternalEObject)newValAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__VAL_ATTRIBUTE, null, msgs);
			msgs = basicSetValAttribute(newValAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__VAL_ATTRIBUTE, newValAttribute, newValAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValidAttribute getValidAttribute() {
		return validAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetValidAttribute(ValidAttribute newValidAttribute, NotificationChain msgs) {
		ValidAttribute oldValidAttribute = validAttribute;
		validAttribute = newValidAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__VALID_ATTRIBUTE, oldValidAttribute, newValidAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValidAttribute(ValidAttribute newValidAttribute) {
		if (newValidAttribute != validAttribute) {
			NotificationChain msgs = null;
			if (validAttribute != null)
				msgs = ((InternalEObject)validAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__VALID_ATTRIBUTE, null, msgs);
			if (newValidAttribute != null)
				msgs = ((InternalEObject)newValidAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__VALID_ATTRIBUTE, null, msgs);
			msgs = basicSetValidAttribute(newValidAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__VALID_ATTRIBUTE, newValidAttribute, newValidAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueAttribute getValueAttribute() {
		return valueAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetValueAttribute(ValueAttribute newValueAttribute, NotificationChain msgs) {
		ValueAttribute oldValueAttribute = valueAttribute;
		valueAttribute = newValueAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__VALUE_ATTRIBUTE, oldValueAttribute, newValueAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueAttribute(ValueAttribute newValueAttribute) {
		if (newValueAttribute != valueAttribute) {
			NotificationChain msgs = null;
			if (valueAttribute != null)
				msgs = ((InternalEObject)valueAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__VALUE_ATTRIBUTE, null, msgs);
			if (newValueAttribute != null)
				msgs = ((InternalEObject)newValueAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__VALUE_ATTRIBUTE, null, msgs);
			msgs = basicSetValueAttribute(newValueAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__VALUE_ATTRIBUTE, newValueAttribute, newValueAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VersionAttribute getVersionAttribute() {
		return versionAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVersionAttribute(VersionAttribute newVersionAttribute, NotificationChain msgs) {
		VersionAttribute oldVersionAttribute = versionAttribute;
		versionAttribute = newVersionAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__VERSION_ATTRIBUTE, oldVersionAttribute, newVersionAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersionAttribute(VersionAttribute newVersionAttribute) {
		if (newVersionAttribute != versionAttribute) {
			NotificationChain msgs = null;
			if (versionAttribute != null)
				msgs = ((InternalEObject)versionAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__VERSION_ATTRIBUTE, null, msgs);
			if (newVersionAttribute != null)
				msgs = ((InternalEObject)newVersionAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__VERSION_ATTRIBUTE, null, msgs);
			msgs = basicSetVersionAttribute(newVersionAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__VERSION_ATTRIBUTE, newVersionAttribute, newVersionAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WideImageAttribute getWideImageAttribute() {
		return wideImageAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWideImageAttribute(WideImageAttribute newWideImageAttribute, NotificationChain msgs) {
		WideImageAttribute oldWideImageAttribute = wideImageAttribute;
		wideImageAttribute = newWideImageAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__WIDE_IMAGE_ATTRIBUTE, oldWideImageAttribute, newWideImageAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWideImageAttribute(WideImageAttribute newWideImageAttribute) {
		if (newWideImageAttribute != wideImageAttribute) {
			NotificationChain msgs = null;
			if (wideImageAttribute != null)
				msgs = ((InternalEObject)wideImageAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__WIDE_IMAGE_ATTRIBUTE, null, msgs);
			if (newWideImageAttribute != null)
				msgs = ((InternalEObject)newWideImageAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__WIDE_IMAGE_ATTRIBUTE, null, msgs);
			msgs = basicSetWideImageAttribute(newWideImageAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__WIDE_IMAGE_ATTRIBUTE, newWideImageAttribute, newWideImageAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WideValueAttribute getWideValueAttribute() {
		return wideValueAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWideValueAttribute(WideValueAttribute newWideValueAttribute, NotificationChain msgs) {
		WideValueAttribute oldWideValueAttribute = wideValueAttribute;
		wideValueAttribute = newWideValueAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__WIDE_VALUE_ATTRIBUTE, oldWideValueAttribute, newWideValueAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWideValueAttribute(WideValueAttribute newWideValueAttribute) {
		if (newWideValueAttribute != wideValueAttribute) {
			NotificationChain msgs = null;
			if (wideValueAttribute != null)
				msgs = ((InternalEObject)wideValueAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__WIDE_VALUE_ATTRIBUTE, null, msgs);
			if (newWideValueAttribute != null)
				msgs = ((InternalEObject)newWideValueAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__WIDE_VALUE_ATTRIBUTE, null, msgs);
			msgs = basicSetWideValueAttribute(newWideValueAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__WIDE_VALUE_ATTRIBUTE, newWideValueAttribute, newWideValueAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WideWidthAttribute getWideWidthAttribute() {
		return wideWidthAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWideWidthAttribute(WideWidthAttribute newWideWidthAttribute, NotificationChain msgs) {
		WideWidthAttribute oldWideWidthAttribute = wideWidthAttribute;
		wideWidthAttribute = newWideWidthAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__WIDE_WIDTH_ATTRIBUTE, oldWideWidthAttribute, newWideWidthAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWideWidthAttribute(WideWidthAttribute newWideWidthAttribute) {
		if (newWideWidthAttribute != wideWidthAttribute) {
			NotificationChain msgs = null;
			if (wideWidthAttribute != null)
				msgs = ((InternalEObject)wideWidthAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__WIDE_WIDTH_ATTRIBUTE, null, msgs);
			if (newWideWidthAttribute != null)
				msgs = ((InternalEObject)newWideWidthAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__WIDE_WIDTH_ATTRIBUTE, null, msgs);
			msgs = basicSetWideWidthAttribute(newWideWidthAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__WIDE_WIDTH_ATTRIBUTE, newWideWidthAttribute, newWideWidthAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WidthAttribute getWidthAttribute() {
		return widthAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWidthAttribute(WidthAttribute newWidthAttribute, NotificationChain msgs) {
		WidthAttribute oldWidthAttribute = widthAttribute;
		widthAttribute = newWidthAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__WIDTH_ATTRIBUTE, oldWidthAttribute, newWidthAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWidthAttribute(WidthAttribute newWidthAttribute) {
		if (newWidthAttribute != widthAttribute) {
			NotificationChain msgs = null;
			if (widthAttribute != null)
				msgs = ((InternalEObject)widthAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__WIDTH_ATTRIBUTE, null, msgs);
			if (newWidthAttribute != null)
				msgs = ((InternalEObject)newWidthAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__WIDTH_ATTRIBUTE, null, msgs);
			msgs = basicSetWidthAttribute(newWidthAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__WIDTH_ATTRIBUTE, newWidthAttribute, newWidthAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WriteAttribute getWriteAttribute() {
		return writeAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWriteAttribute(WriteAttribute newWriteAttribute, NotificationChain msgs) {
		WriteAttribute oldWriteAttribute = writeAttribute;
		writeAttribute = newWriteAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__WRITE_ATTRIBUTE, oldWriteAttribute, newWriteAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWriteAttribute(WriteAttribute newWriteAttribute) {
		if (newWriteAttribute != writeAttribute) {
			NotificationChain msgs = null;
			if (writeAttribute != null)
				msgs = ((InternalEObject)writeAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__WRITE_ATTRIBUTE, null, msgs);
			if (newWriteAttribute != null)
				msgs = ((InternalEObject)newWriteAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__WRITE_ATTRIBUTE, null, msgs);
			msgs = basicSetWriteAttribute(newWriteAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__WRITE_ATTRIBUTE, newWriteAttribute, newWriteAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MachineRoundingAttribute getMachineRoundingAttribute() {
		return machineRoundingAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMachineRoundingAttribute(MachineRoundingAttribute newMachineRoundingAttribute, NotificationChain msgs) {
		MachineRoundingAttribute oldMachineRoundingAttribute = machineRoundingAttribute;
		machineRoundingAttribute = newMachineRoundingAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MACHINE_ROUNDING_ATTRIBUTE, oldMachineRoundingAttribute, newMachineRoundingAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMachineRoundingAttribute(MachineRoundingAttribute newMachineRoundingAttribute) {
		if (newMachineRoundingAttribute != machineRoundingAttribute) {
			NotificationChain msgs = null;
			if (machineRoundingAttribute != null)
				msgs = ((InternalEObject)machineRoundingAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MACHINE_ROUNDING_ATTRIBUTE, null, msgs);
			if (newMachineRoundingAttribute != null)
				msgs = ((InternalEObject)newMachineRoundingAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MACHINE_ROUNDING_ATTRIBUTE, null, msgs);
			msgs = basicSetMachineRoundingAttribute(newMachineRoundingAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MACHINE_ROUNDING_ATTRIBUTE, newMachineRoundingAttribute, newMachineRoundingAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModAttribute getModAttribute() {
		return modAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetModAttribute(ModAttribute newModAttribute, NotificationChain msgs) {
		ModAttribute oldModAttribute = modAttribute;
		modAttribute = newModAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MOD_ATTRIBUTE, oldModAttribute, newModAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModAttribute(ModAttribute newModAttribute) {
		if (newModAttribute != modAttribute) {
			NotificationChain msgs = null;
			if (modAttribute != null)
				msgs = ((InternalEObject)modAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MOD_ATTRIBUTE, null, msgs);
			if (newModAttribute != null)
				msgs = ((InternalEObject)newModAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MOD_ATTRIBUTE, null, msgs);
			msgs = basicSetModAttribute(newModAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MOD_ATTRIBUTE, newModAttribute, newModAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PriorityAttribute getPriorityAttribute() {
		return priorityAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPriorityAttribute(PriorityAttribute newPriorityAttribute, NotificationChain msgs) {
		PriorityAttribute oldPriorityAttribute = priorityAttribute;
		priorityAttribute = newPriorityAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__PRIORITY_ATTRIBUTE, oldPriorityAttribute, newPriorityAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPriorityAttribute(PriorityAttribute newPriorityAttribute) {
		if (newPriorityAttribute != priorityAttribute) {
			NotificationChain msgs = null;
			if (priorityAttribute != null)
				msgs = ((InternalEObject)priorityAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__PRIORITY_ATTRIBUTE, null, msgs);
			if (newPriorityAttribute != null)
				msgs = ((InternalEObject)newPriorityAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__PRIORITY_ATTRIBUTE, null, msgs);
			msgs = basicSetPriorityAttribute(newPriorityAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__PRIORITY_ATTRIBUTE, newPriorityAttribute, newPriorityAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StreamSizeAttribute getStreamSizeAttribute() {
		return streamSizeAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStreamSizeAttribute(StreamSizeAttribute newStreamSizeAttribute, NotificationChain msgs) {
		StreamSizeAttribute oldStreamSizeAttribute = streamSizeAttribute;
		streamSizeAttribute = newStreamSizeAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__STREAM_SIZE_ATTRIBUTE, oldStreamSizeAttribute, newStreamSizeAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStreamSizeAttribute(StreamSizeAttribute newStreamSizeAttribute) {
		if (newStreamSizeAttribute != streamSizeAttribute) {
			NotificationChain msgs = null;
			if (streamSizeAttribute != null)
				msgs = ((InternalEObject)streamSizeAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__STREAM_SIZE_ATTRIBUTE, null, msgs);
			if (newStreamSizeAttribute != null)
				msgs = ((InternalEObject)newStreamSizeAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__STREAM_SIZE_ATTRIBUTE, null, msgs);
			msgs = basicSetStreamSizeAttribute(newStreamSizeAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__STREAM_SIZE_ATTRIBUTE, newStreamSizeAttribute, newStreamSizeAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WideWideImageAttribute getWideWideImageAttribute() {
		return wideWideImageAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWideWideImageAttribute(WideWideImageAttribute newWideWideImageAttribute, NotificationChain msgs) {
		WideWideImageAttribute oldWideWideImageAttribute = wideWideImageAttribute;
		wideWideImageAttribute = newWideWideImageAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__WIDE_WIDE_IMAGE_ATTRIBUTE, oldWideWideImageAttribute, newWideWideImageAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWideWideImageAttribute(WideWideImageAttribute newWideWideImageAttribute) {
		if (newWideWideImageAttribute != wideWideImageAttribute) {
			NotificationChain msgs = null;
			if (wideWideImageAttribute != null)
				msgs = ((InternalEObject)wideWideImageAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__WIDE_WIDE_IMAGE_ATTRIBUTE, null, msgs);
			if (newWideWideImageAttribute != null)
				msgs = ((InternalEObject)newWideWideImageAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__WIDE_WIDE_IMAGE_ATTRIBUTE, null, msgs);
			msgs = basicSetWideWideImageAttribute(newWideWideImageAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__WIDE_WIDE_IMAGE_ATTRIBUTE, newWideWideImageAttribute, newWideWideImageAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WideWideValueAttribute getWideWideValueAttribute() {
		return wideWideValueAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWideWideValueAttribute(WideWideValueAttribute newWideWideValueAttribute, NotificationChain msgs) {
		WideWideValueAttribute oldWideWideValueAttribute = wideWideValueAttribute;
		wideWideValueAttribute = newWideWideValueAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__WIDE_WIDE_VALUE_ATTRIBUTE, oldWideWideValueAttribute, newWideWideValueAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWideWideValueAttribute(WideWideValueAttribute newWideWideValueAttribute) {
		if (newWideWideValueAttribute != wideWideValueAttribute) {
			NotificationChain msgs = null;
			if (wideWideValueAttribute != null)
				msgs = ((InternalEObject)wideWideValueAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__WIDE_WIDE_VALUE_ATTRIBUTE, null, msgs);
			if (newWideWideValueAttribute != null)
				msgs = ((InternalEObject)newWideWideValueAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__WIDE_WIDE_VALUE_ATTRIBUTE, null, msgs);
			msgs = basicSetWideWideValueAttribute(newWideWideValueAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__WIDE_WIDE_VALUE_ATTRIBUTE, newWideWideValueAttribute, newWideWideValueAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WideWideWidthAttribute getWideWideWidthAttribute() {
		return wideWideWidthAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWideWideWidthAttribute(WideWideWidthAttribute newWideWideWidthAttribute, NotificationChain msgs) {
		WideWideWidthAttribute oldWideWideWidthAttribute = wideWideWidthAttribute;
		wideWideWidthAttribute = newWideWideWidthAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__WIDE_WIDE_WIDTH_ATTRIBUTE, oldWideWideWidthAttribute, newWideWideWidthAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWideWideWidthAttribute(WideWideWidthAttribute newWideWideWidthAttribute) {
		if (newWideWideWidthAttribute != wideWideWidthAttribute) {
			NotificationChain msgs = null;
			if (wideWideWidthAttribute != null)
				msgs = ((InternalEObject)wideWideWidthAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__WIDE_WIDE_WIDTH_ATTRIBUTE, null, msgs);
			if (newWideWideWidthAttribute != null)
				msgs = ((InternalEObject)newWideWideWidthAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__WIDE_WIDE_WIDTH_ATTRIBUTE, null, msgs);
			msgs = basicSetWideWideWidthAttribute(newWideWideWidthAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__WIDE_WIDE_WIDTH_ATTRIBUTE, newWideWideWidthAttribute, newWideWideWidthAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaxAlignmentForAllocationAttribute getMaxAlignmentForAllocationAttribute() {
		return maxAlignmentForAllocationAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMaxAlignmentForAllocationAttribute(MaxAlignmentForAllocationAttribute newMaxAlignmentForAllocationAttribute, NotificationChain msgs) {
		MaxAlignmentForAllocationAttribute oldMaxAlignmentForAllocationAttribute = maxAlignmentForAllocationAttribute;
		maxAlignmentForAllocationAttribute = newMaxAlignmentForAllocationAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE, oldMaxAlignmentForAllocationAttribute, newMaxAlignmentForAllocationAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxAlignmentForAllocationAttribute(MaxAlignmentForAllocationAttribute newMaxAlignmentForAllocationAttribute) {
		if (newMaxAlignmentForAllocationAttribute != maxAlignmentForAllocationAttribute) {
			NotificationChain msgs = null;
			if (maxAlignmentForAllocationAttribute != null)
				msgs = ((InternalEObject)maxAlignmentForAllocationAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE, null, msgs);
			if (newMaxAlignmentForAllocationAttribute != null)
				msgs = ((InternalEObject)newMaxAlignmentForAllocationAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE, null, msgs);
			msgs = basicSetMaxAlignmentForAllocationAttribute(newMaxAlignmentForAllocationAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE, newMaxAlignmentForAllocationAttribute, newMaxAlignmentForAllocationAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OverlapsStorageAttribute getOverlapsStorageAttribute() {
		return overlapsStorageAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOverlapsStorageAttribute(OverlapsStorageAttribute newOverlapsStorageAttribute, NotificationChain msgs) {
		OverlapsStorageAttribute oldOverlapsStorageAttribute = overlapsStorageAttribute;
		overlapsStorageAttribute = newOverlapsStorageAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__OVERLAPS_STORAGE_ATTRIBUTE, oldOverlapsStorageAttribute, newOverlapsStorageAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOverlapsStorageAttribute(OverlapsStorageAttribute newOverlapsStorageAttribute) {
		if (newOverlapsStorageAttribute != overlapsStorageAttribute) {
			NotificationChain msgs = null;
			if (overlapsStorageAttribute != null)
				msgs = ((InternalEObject)overlapsStorageAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__OVERLAPS_STORAGE_ATTRIBUTE, null, msgs);
			if (newOverlapsStorageAttribute != null)
				msgs = ((InternalEObject)newOverlapsStorageAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__OVERLAPS_STORAGE_ATTRIBUTE, null, msgs);
			msgs = basicSetOverlapsStorageAttribute(newOverlapsStorageAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__OVERLAPS_STORAGE_ATTRIBUTE, newOverlapsStorageAttribute, newOverlapsStorageAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImplementationDefinedAttribute getImplementationDefinedAttribute() {
		return implementationDefinedAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImplementationDefinedAttribute(ImplementationDefinedAttribute newImplementationDefinedAttribute, NotificationChain msgs) {
		ImplementationDefinedAttribute oldImplementationDefinedAttribute = implementationDefinedAttribute;
		implementationDefinedAttribute = newImplementationDefinedAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__IMPLEMENTATION_DEFINED_ATTRIBUTE, oldImplementationDefinedAttribute, newImplementationDefinedAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImplementationDefinedAttribute(ImplementationDefinedAttribute newImplementationDefinedAttribute) {
		if (newImplementationDefinedAttribute != implementationDefinedAttribute) {
			NotificationChain msgs = null;
			if (implementationDefinedAttribute != null)
				msgs = ((InternalEObject)implementationDefinedAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__IMPLEMENTATION_DEFINED_ATTRIBUTE, null, msgs);
			if (newImplementationDefinedAttribute != null)
				msgs = ((InternalEObject)newImplementationDefinedAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__IMPLEMENTATION_DEFINED_ATTRIBUTE, null, msgs);
			msgs = basicSetImplementationDefinedAttribute(newImplementationDefinedAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__IMPLEMENTATION_DEFINED_ATTRIBUTE, newImplementationDefinedAttribute, newImplementationDefinedAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnknownAttribute getUnknownAttribute() {
		return unknownAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnknownAttribute(UnknownAttribute newUnknownAttribute, NotificationChain msgs) {
		UnknownAttribute oldUnknownAttribute = unknownAttribute;
		unknownAttribute = newUnknownAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__UNKNOWN_ATTRIBUTE, oldUnknownAttribute, newUnknownAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnknownAttribute(UnknownAttribute newUnknownAttribute) {
		if (newUnknownAttribute != unknownAttribute) {
			NotificationChain msgs = null;
			if (unknownAttribute != null)
				msgs = ((InternalEObject)unknownAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__UNKNOWN_ATTRIBUTE, null, msgs);
			if (newUnknownAttribute != null)
				msgs = ((InternalEObject)newUnknownAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__UNKNOWN_ATTRIBUTE, null, msgs);
			msgs = basicSetUnknownAttribute(newUnknownAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__UNKNOWN_ATTRIBUTE, newUnknownAttribute, newUnknownAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Comment getComment() {
		return comment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComment(Comment newComment, NotificationChain msgs) {
		Comment oldComment = comment;
		comment = newComment;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__COMMENT, oldComment, newComment);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComment(Comment newComment) {
		if (newComment != comment) {
			NotificationChain msgs = null;
			if (comment != null)
				msgs = ((InternalEObject)comment).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__COMMENT, null, msgs);
			if (newComment != null)
				msgs = ((InternalEObject)newComment).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__COMMENT, null, msgs);
			msgs = basicSetComment(newComment, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__COMMENT, newComment, newComment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AllCallsRemotePragma getAllCallsRemotePragma() {
		return allCallsRemotePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAllCallsRemotePragma(AllCallsRemotePragma newAllCallsRemotePragma, NotificationChain msgs) {
		AllCallsRemotePragma oldAllCallsRemotePragma = allCallsRemotePragma;
		allCallsRemotePragma = newAllCallsRemotePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ALL_CALLS_REMOTE_PRAGMA, oldAllCallsRemotePragma, newAllCallsRemotePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAllCallsRemotePragma(AllCallsRemotePragma newAllCallsRemotePragma) {
		if (newAllCallsRemotePragma != allCallsRemotePragma) {
			NotificationChain msgs = null;
			if (allCallsRemotePragma != null)
				msgs = ((InternalEObject)allCallsRemotePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ALL_CALLS_REMOTE_PRAGMA, null, msgs);
			if (newAllCallsRemotePragma != null)
				msgs = ((InternalEObject)newAllCallsRemotePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ALL_CALLS_REMOTE_PRAGMA, null, msgs);
			msgs = basicSetAllCallsRemotePragma(newAllCallsRemotePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ALL_CALLS_REMOTE_PRAGMA, newAllCallsRemotePragma, newAllCallsRemotePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AsynchronousPragma getAsynchronousPragma() {
		return asynchronousPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAsynchronousPragma(AsynchronousPragma newAsynchronousPragma, NotificationChain msgs) {
		AsynchronousPragma oldAsynchronousPragma = asynchronousPragma;
		asynchronousPragma = newAsynchronousPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ASYNCHRONOUS_PRAGMA, oldAsynchronousPragma, newAsynchronousPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAsynchronousPragma(AsynchronousPragma newAsynchronousPragma) {
		if (newAsynchronousPragma != asynchronousPragma) {
			NotificationChain msgs = null;
			if (asynchronousPragma != null)
				msgs = ((InternalEObject)asynchronousPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ASYNCHRONOUS_PRAGMA, null, msgs);
			if (newAsynchronousPragma != null)
				msgs = ((InternalEObject)newAsynchronousPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ASYNCHRONOUS_PRAGMA, null, msgs);
			msgs = basicSetAsynchronousPragma(newAsynchronousPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ASYNCHRONOUS_PRAGMA, newAsynchronousPragma, newAsynchronousPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtomicPragma getAtomicPragma() {
		return atomicPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAtomicPragma(AtomicPragma newAtomicPragma, NotificationChain msgs) {
		AtomicPragma oldAtomicPragma = atomicPragma;
		atomicPragma = newAtomicPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ATOMIC_PRAGMA, oldAtomicPragma, newAtomicPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAtomicPragma(AtomicPragma newAtomicPragma) {
		if (newAtomicPragma != atomicPragma) {
			NotificationChain msgs = null;
			if (atomicPragma != null)
				msgs = ((InternalEObject)atomicPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ATOMIC_PRAGMA, null, msgs);
			if (newAtomicPragma != null)
				msgs = ((InternalEObject)newAtomicPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ATOMIC_PRAGMA, null, msgs);
			msgs = basicSetAtomicPragma(newAtomicPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ATOMIC_PRAGMA, newAtomicPragma, newAtomicPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtomicComponentsPragma getAtomicComponentsPragma() {
		return atomicComponentsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAtomicComponentsPragma(AtomicComponentsPragma newAtomicComponentsPragma, NotificationChain msgs) {
		AtomicComponentsPragma oldAtomicComponentsPragma = atomicComponentsPragma;
		atomicComponentsPragma = newAtomicComponentsPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ATOMIC_COMPONENTS_PRAGMA, oldAtomicComponentsPragma, newAtomicComponentsPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAtomicComponentsPragma(AtomicComponentsPragma newAtomicComponentsPragma) {
		if (newAtomicComponentsPragma != atomicComponentsPragma) {
			NotificationChain msgs = null;
			if (atomicComponentsPragma != null)
				msgs = ((InternalEObject)atomicComponentsPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ATOMIC_COMPONENTS_PRAGMA, null, msgs);
			if (newAtomicComponentsPragma != null)
				msgs = ((InternalEObject)newAtomicComponentsPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ATOMIC_COMPONENTS_PRAGMA, null, msgs);
			msgs = basicSetAtomicComponentsPragma(newAtomicComponentsPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ATOMIC_COMPONENTS_PRAGMA, newAtomicComponentsPragma, newAtomicComponentsPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttachHandlerPragma getAttachHandlerPragma() {
		return attachHandlerPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAttachHandlerPragma(AttachHandlerPragma newAttachHandlerPragma, NotificationChain msgs) {
		AttachHandlerPragma oldAttachHandlerPragma = attachHandlerPragma;
		attachHandlerPragma = newAttachHandlerPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ATTACH_HANDLER_PRAGMA, oldAttachHandlerPragma, newAttachHandlerPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttachHandlerPragma(AttachHandlerPragma newAttachHandlerPragma) {
		if (newAttachHandlerPragma != attachHandlerPragma) {
			NotificationChain msgs = null;
			if (attachHandlerPragma != null)
				msgs = ((InternalEObject)attachHandlerPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ATTACH_HANDLER_PRAGMA, null, msgs);
			if (newAttachHandlerPragma != null)
				msgs = ((InternalEObject)newAttachHandlerPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ATTACH_HANDLER_PRAGMA, null, msgs);
			msgs = basicSetAttachHandlerPragma(newAttachHandlerPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ATTACH_HANDLER_PRAGMA, newAttachHandlerPragma, newAttachHandlerPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlledPragma getControlledPragma() {
		return controlledPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetControlledPragma(ControlledPragma newControlledPragma, NotificationChain msgs) {
		ControlledPragma oldControlledPragma = controlledPragma;
		controlledPragma = newControlledPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__CONTROLLED_PRAGMA, oldControlledPragma, newControlledPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setControlledPragma(ControlledPragma newControlledPragma) {
		if (newControlledPragma != controlledPragma) {
			NotificationChain msgs = null;
			if (controlledPragma != null)
				msgs = ((InternalEObject)controlledPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__CONTROLLED_PRAGMA, null, msgs);
			if (newControlledPragma != null)
				msgs = ((InternalEObject)newControlledPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__CONTROLLED_PRAGMA, null, msgs);
			msgs = basicSetControlledPragma(newControlledPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__CONTROLLED_PRAGMA, newControlledPragma, newControlledPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConventionPragma getConventionPragma() {
		return conventionPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConventionPragma(ConventionPragma newConventionPragma, NotificationChain msgs) {
		ConventionPragma oldConventionPragma = conventionPragma;
		conventionPragma = newConventionPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__CONVENTION_PRAGMA, oldConventionPragma, newConventionPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConventionPragma(ConventionPragma newConventionPragma) {
		if (newConventionPragma != conventionPragma) {
			NotificationChain msgs = null;
			if (conventionPragma != null)
				msgs = ((InternalEObject)conventionPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__CONVENTION_PRAGMA, null, msgs);
			if (newConventionPragma != null)
				msgs = ((InternalEObject)newConventionPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__CONVENTION_PRAGMA, null, msgs);
			msgs = basicSetConventionPragma(newConventionPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__CONVENTION_PRAGMA, newConventionPragma, newConventionPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscardNamesPragma getDiscardNamesPragma() {
		return discardNamesPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscardNamesPragma(DiscardNamesPragma newDiscardNamesPragma, NotificationChain msgs) {
		DiscardNamesPragma oldDiscardNamesPragma = discardNamesPragma;
		discardNamesPragma = newDiscardNamesPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__DISCARD_NAMES_PRAGMA, oldDiscardNamesPragma, newDiscardNamesPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscardNamesPragma(DiscardNamesPragma newDiscardNamesPragma) {
		if (newDiscardNamesPragma != discardNamesPragma) {
			NotificationChain msgs = null;
			if (discardNamesPragma != null)
				msgs = ((InternalEObject)discardNamesPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__DISCARD_NAMES_PRAGMA, null, msgs);
			if (newDiscardNamesPragma != null)
				msgs = ((InternalEObject)newDiscardNamesPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__DISCARD_NAMES_PRAGMA, null, msgs);
			msgs = basicSetDiscardNamesPragma(newDiscardNamesPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__DISCARD_NAMES_PRAGMA, newDiscardNamesPragma, newDiscardNamesPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElaboratePragma getElaboratePragma() {
		return elaboratePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElaboratePragma(ElaboratePragma newElaboratePragma, NotificationChain msgs) {
		ElaboratePragma oldElaboratePragma = elaboratePragma;
		elaboratePragma = newElaboratePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ELABORATE_PRAGMA, oldElaboratePragma, newElaboratePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElaboratePragma(ElaboratePragma newElaboratePragma) {
		if (newElaboratePragma != elaboratePragma) {
			NotificationChain msgs = null;
			if (elaboratePragma != null)
				msgs = ((InternalEObject)elaboratePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ELABORATE_PRAGMA, null, msgs);
			if (newElaboratePragma != null)
				msgs = ((InternalEObject)newElaboratePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ELABORATE_PRAGMA, null, msgs);
			msgs = basicSetElaboratePragma(newElaboratePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ELABORATE_PRAGMA, newElaboratePragma, newElaboratePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElaborateAllPragma getElaborateAllPragma() {
		return elaborateAllPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElaborateAllPragma(ElaborateAllPragma newElaborateAllPragma, NotificationChain msgs) {
		ElaborateAllPragma oldElaborateAllPragma = elaborateAllPragma;
		elaborateAllPragma = newElaborateAllPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ELABORATE_ALL_PRAGMA, oldElaborateAllPragma, newElaborateAllPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElaborateAllPragma(ElaborateAllPragma newElaborateAllPragma) {
		if (newElaborateAllPragma != elaborateAllPragma) {
			NotificationChain msgs = null;
			if (elaborateAllPragma != null)
				msgs = ((InternalEObject)elaborateAllPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ELABORATE_ALL_PRAGMA, null, msgs);
			if (newElaborateAllPragma != null)
				msgs = ((InternalEObject)newElaborateAllPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ELABORATE_ALL_PRAGMA, null, msgs);
			msgs = basicSetElaborateAllPragma(newElaborateAllPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ELABORATE_ALL_PRAGMA, newElaborateAllPragma, newElaborateAllPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElaborateBodyPragma getElaborateBodyPragma() {
		return elaborateBodyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElaborateBodyPragma(ElaborateBodyPragma newElaborateBodyPragma, NotificationChain msgs) {
		ElaborateBodyPragma oldElaborateBodyPragma = elaborateBodyPragma;
		elaborateBodyPragma = newElaborateBodyPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ELABORATE_BODY_PRAGMA, oldElaborateBodyPragma, newElaborateBodyPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElaborateBodyPragma(ElaborateBodyPragma newElaborateBodyPragma) {
		if (newElaborateBodyPragma != elaborateBodyPragma) {
			NotificationChain msgs = null;
			if (elaborateBodyPragma != null)
				msgs = ((InternalEObject)elaborateBodyPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ELABORATE_BODY_PRAGMA, null, msgs);
			if (newElaborateBodyPragma != null)
				msgs = ((InternalEObject)newElaborateBodyPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ELABORATE_BODY_PRAGMA, null, msgs);
			msgs = basicSetElaborateBodyPragma(newElaborateBodyPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ELABORATE_BODY_PRAGMA, newElaborateBodyPragma, newElaborateBodyPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExportPragma getExportPragma() {
		return exportPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExportPragma(ExportPragma newExportPragma, NotificationChain msgs) {
		ExportPragma oldExportPragma = exportPragma;
		exportPragma = newExportPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__EXPORT_PRAGMA, oldExportPragma, newExportPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExportPragma(ExportPragma newExportPragma) {
		if (newExportPragma != exportPragma) {
			NotificationChain msgs = null;
			if (exportPragma != null)
				msgs = ((InternalEObject)exportPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__EXPORT_PRAGMA, null, msgs);
			if (newExportPragma != null)
				msgs = ((InternalEObject)newExportPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__EXPORT_PRAGMA, null, msgs);
			msgs = basicSetExportPragma(newExportPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__EXPORT_PRAGMA, newExportPragma, newExportPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImportPragma getImportPragma() {
		return importPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImportPragma(ImportPragma newImportPragma, NotificationChain msgs) {
		ImportPragma oldImportPragma = importPragma;
		importPragma = newImportPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__IMPORT_PRAGMA, oldImportPragma, newImportPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImportPragma(ImportPragma newImportPragma) {
		if (newImportPragma != importPragma) {
			NotificationChain msgs = null;
			if (importPragma != null)
				msgs = ((InternalEObject)importPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__IMPORT_PRAGMA, null, msgs);
			if (newImportPragma != null)
				msgs = ((InternalEObject)newImportPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__IMPORT_PRAGMA, null, msgs);
			msgs = basicSetImportPragma(newImportPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__IMPORT_PRAGMA, newImportPragma, newImportPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InlinePragma getInlinePragma() {
		return inlinePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInlinePragma(InlinePragma newInlinePragma, NotificationChain msgs) {
		InlinePragma oldInlinePragma = inlinePragma;
		inlinePragma = newInlinePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__INLINE_PRAGMA, oldInlinePragma, newInlinePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInlinePragma(InlinePragma newInlinePragma) {
		if (newInlinePragma != inlinePragma) {
			NotificationChain msgs = null;
			if (inlinePragma != null)
				msgs = ((InternalEObject)inlinePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__INLINE_PRAGMA, null, msgs);
			if (newInlinePragma != null)
				msgs = ((InternalEObject)newInlinePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__INLINE_PRAGMA, null, msgs);
			msgs = basicSetInlinePragma(newInlinePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__INLINE_PRAGMA, newInlinePragma, newInlinePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InspectionPointPragma getInspectionPointPragma() {
		return inspectionPointPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInspectionPointPragma(InspectionPointPragma newInspectionPointPragma, NotificationChain msgs) {
		InspectionPointPragma oldInspectionPointPragma = inspectionPointPragma;
		inspectionPointPragma = newInspectionPointPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__INSPECTION_POINT_PRAGMA, oldInspectionPointPragma, newInspectionPointPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInspectionPointPragma(InspectionPointPragma newInspectionPointPragma) {
		if (newInspectionPointPragma != inspectionPointPragma) {
			NotificationChain msgs = null;
			if (inspectionPointPragma != null)
				msgs = ((InternalEObject)inspectionPointPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__INSPECTION_POINT_PRAGMA, null, msgs);
			if (newInspectionPointPragma != null)
				msgs = ((InternalEObject)newInspectionPointPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__INSPECTION_POINT_PRAGMA, null, msgs);
			msgs = basicSetInspectionPointPragma(newInspectionPointPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__INSPECTION_POINT_PRAGMA, newInspectionPointPragma, newInspectionPointPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterruptHandlerPragma getInterruptHandlerPragma() {
		return interruptHandlerPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInterruptHandlerPragma(InterruptHandlerPragma newInterruptHandlerPragma, NotificationChain msgs) {
		InterruptHandlerPragma oldInterruptHandlerPragma = interruptHandlerPragma;
		interruptHandlerPragma = newInterruptHandlerPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__INTERRUPT_HANDLER_PRAGMA, oldInterruptHandlerPragma, newInterruptHandlerPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterruptHandlerPragma(InterruptHandlerPragma newInterruptHandlerPragma) {
		if (newInterruptHandlerPragma != interruptHandlerPragma) {
			NotificationChain msgs = null;
			if (interruptHandlerPragma != null)
				msgs = ((InternalEObject)interruptHandlerPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__INTERRUPT_HANDLER_PRAGMA, null, msgs);
			if (newInterruptHandlerPragma != null)
				msgs = ((InternalEObject)newInterruptHandlerPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__INTERRUPT_HANDLER_PRAGMA, null, msgs);
			msgs = basicSetInterruptHandlerPragma(newInterruptHandlerPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__INTERRUPT_HANDLER_PRAGMA, newInterruptHandlerPragma, newInterruptHandlerPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterruptPriorityPragma getInterruptPriorityPragma() {
		return interruptPriorityPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInterruptPriorityPragma(InterruptPriorityPragma newInterruptPriorityPragma, NotificationChain msgs) {
		InterruptPriorityPragma oldInterruptPriorityPragma = interruptPriorityPragma;
		interruptPriorityPragma = newInterruptPriorityPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__INTERRUPT_PRIORITY_PRAGMA, oldInterruptPriorityPragma, newInterruptPriorityPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterruptPriorityPragma(InterruptPriorityPragma newInterruptPriorityPragma) {
		if (newInterruptPriorityPragma != interruptPriorityPragma) {
			NotificationChain msgs = null;
			if (interruptPriorityPragma != null)
				msgs = ((InternalEObject)interruptPriorityPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__INTERRUPT_PRIORITY_PRAGMA, null, msgs);
			if (newInterruptPriorityPragma != null)
				msgs = ((InternalEObject)newInterruptPriorityPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__INTERRUPT_PRIORITY_PRAGMA, null, msgs);
			msgs = basicSetInterruptPriorityPragma(newInterruptPriorityPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__INTERRUPT_PRIORITY_PRAGMA, newInterruptPriorityPragma, newInterruptPriorityPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkerOptionsPragma getLinkerOptionsPragma() {
		return linkerOptionsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLinkerOptionsPragma(LinkerOptionsPragma newLinkerOptionsPragma, NotificationChain msgs) {
		LinkerOptionsPragma oldLinkerOptionsPragma = linkerOptionsPragma;
		linkerOptionsPragma = newLinkerOptionsPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__LINKER_OPTIONS_PRAGMA, oldLinkerOptionsPragma, newLinkerOptionsPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLinkerOptionsPragma(LinkerOptionsPragma newLinkerOptionsPragma) {
		if (newLinkerOptionsPragma != linkerOptionsPragma) {
			NotificationChain msgs = null;
			if (linkerOptionsPragma != null)
				msgs = ((InternalEObject)linkerOptionsPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__LINKER_OPTIONS_PRAGMA, null, msgs);
			if (newLinkerOptionsPragma != null)
				msgs = ((InternalEObject)newLinkerOptionsPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__LINKER_OPTIONS_PRAGMA, null, msgs);
			msgs = basicSetLinkerOptionsPragma(newLinkerOptionsPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__LINKER_OPTIONS_PRAGMA, newLinkerOptionsPragma, newLinkerOptionsPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ListPragma getListPragma() {
		return listPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetListPragma(ListPragma newListPragma, NotificationChain msgs) {
		ListPragma oldListPragma = listPragma;
		listPragma = newListPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__LIST_PRAGMA, oldListPragma, newListPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setListPragma(ListPragma newListPragma) {
		if (newListPragma != listPragma) {
			NotificationChain msgs = null;
			if (listPragma != null)
				msgs = ((InternalEObject)listPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__LIST_PRAGMA, null, msgs);
			if (newListPragma != null)
				msgs = ((InternalEObject)newListPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__LIST_PRAGMA, null, msgs);
			msgs = basicSetListPragma(newListPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__LIST_PRAGMA, newListPragma, newListPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LockingPolicyPragma getLockingPolicyPragma() {
		return lockingPolicyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLockingPolicyPragma(LockingPolicyPragma newLockingPolicyPragma, NotificationChain msgs) {
		LockingPolicyPragma oldLockingPolicyPragma = lockingPolicyPragma;
		lockingPolicyPragma = newLockingPolicyPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__LOCKING_POLICY_PRAGMA, oldLockingPolicyPragma, newLockingPolicyPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLockingPolicyPragma(LockingPolicyPragma newLockingPolicyPragma) {
		if (newLockingPolicyPragma != lockingPolicyPragma) {
			NotificationChain msgs = null;
			if (lockingPolicyPragma != null)
				msgs = ((InternalEObject)lockingPolicyPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__LOCKING_POLICY_PRAGMA, null, msgs);
			if (newLockingPolicyPragma != null)
				msgs = ((InternalEObject)newLockingPolicyPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__LOCKING_POLICY_PRAGMA, null, msgs);
			msgs = basicSetLockingPolicyPragma(newLockingPolicyPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__LOCKING_POLICY_PRAGMA, newLockingPolicyPragma, newLockingPolicyPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NormalizeScalarsPragma getNormalizeScalarsPragma() {
		return normalizeScalarsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNormalizeScalarsPragma(NormalizeScalarsPragma newNormalizeScalarsPragma, NotificationChain msgs) {
		NormalizeScalarsPragma oldNormalizeScalarsPragma = normalizeScalarsPragma;
		normalizeScalarsPragma = newNormalizeScalarsPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__NORMALIZE_SCALARS_PRAGMA, oldNormalizeScalarsPragma, newNormalizeScalarsPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNormalizeScalarsPragma(NormalizeScalarsPragma newNormalizeScalarsPragma) {
		if (newNormalizeScalarsPragma != normalizeScalarsPragma) {
			NotificationChain msgs = null;
			if (normalizeScalarsPragma != null)
				msgs = ((InternalEObject)normalizeScalarsPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__NORMALIZE_SCALARS_PRAGMA, null, msgs);
			if (newNormalizeScalarsPragma != null)
				msgs = ((InternalEObject)newNormalizeScalarsPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__NORMALIZE_SCALARS_PRAGMA, null, msgs);
			msgs = basicSetNormalizeScalarsPragma(newNormalizeScalarsPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__NORMALIZE_SCALARS_PRAGMA, newNormalizeScalarsPragma, newNormalizeScalarsPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OptimizePragma getOptimizePragma() {
		return optimizePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOptimizePragma(OptimizePragma newOptimizePragma, NotificationChain msgs) {
		OptimizePragma oldOptimizePragma = optimizePragma;
		optimizePragma = newOptimizePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__OPTIMIZE_PRAGMA, oldOptimizePragma, newOptimizePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOptimizePragma(OptimizePragma newOptimizePragma) {
		if (newOptimizePragma != optimizePragma) {
			NotificationChain msgs = null;
			if (optimizePragma != null)
				msgs = ((InternalEObject)optimizePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__OPTIMIZE_PRAGMA, null, msgs);
			if (newOptimizePragma != null)
				msgs = ((InternalEObject)newOptimizePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__OPTIMIZE_PRAGMA, null, msgs);
			msgs = basicSetOptimizePragma(newOptimizePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__OPTIMIZE_PRAGMA, newOptimizePragma, newOptimizePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackPragma getPackPragma() {
		return packPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPackPragma(PackPragma newPackPragma, NotificationChain msgs) {
		PackPragma oldPackPragma = packPragma;
		packPragma = newPackPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__PACK_PRAGMA, oldPackPragma, newPackPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPackPragma(PackPragma newPackPragma) {
		if (newPackPragma != packPragma) {
			NotificationChain msgs = null;
			if (packPragma != null)
				msgs = ((InternalEObject)packPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__PACK_PRAGMA, null, msgs);
			if (newPackPragma != null)
				msgs = ((InternalEObject)newPackPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__PACK_PRAGMA, null, msgs);
			msgs = basicSetPackPragma(newPackPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__PACK_PRAGMA, newPackPragma, newPackPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PagePragma getPagePragma() {
		return pagePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPagePragma(PagePragma newPagePragma, NotificationChain msgs) {
		PagePragma oldPagePragma = pagePragma;
		pagePragma = newPagePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__PAGE_PRAGMA, oldPagePragma, newPagePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPagePragma(PagePragma newPagePragma) {
		if (newPagePragma != pagePragma) {
			NotificationChain msgs = null;
			if (pagePragma != null)
				msgs = ((InternalEObject)pagePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__PAGE_PRAGMA, null, msgs);
			if (newPagePragma != null)
				msgs = ((InternalEObject)newPagePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__PAGE_PRAGMA, null, msgs);
			msgs = basicSetPagePragma(newPagePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__PAGE_PRAGMA, newPagePragma, newPagePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PreelaboratePragma getPreelaboratePragma() {
		return preelaboratePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPreelaboratePragma(PreelaboratePragma newPreelaboratePragma, NotificationChain msgs) {
		PreelaboratePragma oldPreelaboratePragma = preelaboratePragma;
		preelaboratePragma = newPreelaboratePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__PREELABORATE_PRAGMA, oldPreelaboratePragma, newPreelaboratePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreelaboratePragma(PreelaboratePragma newPreelaboratePragma) {
		if (newPreelaboratePragma != preelaboratePragma) {
			NotificationChain msgs = null;
			if (preelaboratePragma != null)
				msgs = ((InternalEObject)preelaboratePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__PREELABORATE_PRAGMA, null, msgs);
			if (newPreelaboratePragma != null)
				msgs = ((InternalEObject)newPreelaboratePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__PREELABORATE_PRAGMA, null, msgs);
			msgs = basicSetPreelaboratePragma(newPreelaboratePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__PREELABORATE_PRAGMA, newPreelaboratePragma, newPreelaboratePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PriorityPragma getPriorityPragma() {
		return priorityPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPriorityPragma(PriorityPragma newPriorityPragma, NotificationChain msgs) {
		PriorityPragma oldPriorityPragma = priorityPragma;
		priorityPragma = newPriorityPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__PRIORITY_PRAGMA, oldPriorityPragma, newPriorityPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPriorityPragma(PriorityPragma newPriorityPragma) {
		if (newPriorityPragma != priorityPragma) {
			NotificationChain msgs = null;
			if (priorityPragma != null)
				msgs = ((InternalEObject)priorityPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__PRIORITY_PRAGMA, null, msgs);
			if (newPriorityPragma != null)
				msgs = ((InternalEObject)newPriorityPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__PRIORITY_PRAGMA, null, msgs);
			msgs = basicSetPriorityPragma(newPriorityPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__PRIORITY_PRAGMA, newPriorityPragma, newPriorityPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PurePragma getPurePragma() {
		return purePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPurePragma(PurePragma newPurePragma, NotificationChain msgs) {
		PurePragma oldPurePragma = purePragma;
		purePragma = newPurePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__PURE_PRAGMA, oldPurePragma, newPurePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPurePragma(PurePragma newPurePragma) {
		if (newPurePragma != purePragma) {
			NotificationChain msgs = null;
			if (purePragma != null)
				msgs = ((InternalEObject)purePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__PURE_PRAGMA, null, msgs);
			if (newPurePragma != null)
				msgs = ((InternalEObject)newPurePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__PURE_PRAGMA, null, msgs);
			msgs = basicSetPurePragma(newPurePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__PURE_PRAGMA, newPurePragma, newPurePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QueuingPolicyPragma getQueuingPolicyPragma() {
		return queuingPolicyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetQueuingPolicyPragma(QueuingPolicyPragma newQueuingPolicyPragma, NotificationChain msgs) {
		QueuingPolicyPragma oldQueuingPolicyPragma = queuingPolicyPragma;
		queuingPolicyPragma = newQueuingPolicyPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__QUEUING_POLICY_PRAGMA, oldQueuingPolicyPragma, newQueuingPolicyPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQueuingPolicyPragma(QueuingPolicyPragma newQueuingPolicyPragma) {
		if (newQueuingPolicyPragma != queuingPolicyPragma) {
			NotificationChain msgs = null;
			if (queuingPolicyPragma != null)
				msgs = ((InternalEObject)queuingPolicyPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__QUEUING_POLICY_PRAGMA, null, msgs);
			if (newQueuingPolicyPragma != null)
				msgs = ((InternalEObject)newQueuingPolicyPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__QUEUING_POLICY_PRAGMA, null, msgs);
			msgs = basicSetQueuingPolicyPragma(newQueuingPolicyPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__QUEUING_POLICY_PRAGMA, newQueuingPolicyPragma, newQueuingPolicyPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RemoteCallInterfacePragma getRemoteCallInterfacePragma() {
		return remoteCallInterfacePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRemoteCallInterfacePragma(RemoteCallInterfacePragma newRemoteCallInterfacePragma, NotificationChain msgs) {
		RemoteCallInterfacePragma oldRemoteCallInterfacePragma = remoteCallInterfacePragma;
		remoteCallInterfacePragma = newRemoteCallInterfacePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__REMOTE_CALL_INTERFACE_PRAGMA, oldRemoteCallInterfacePragma, newRemoteCallInterfacePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRemoteCallInterfacePragma(RemoteCallInterfacePragma newRemoteCallInterfacePragma) {
		if (newRemoteCallInterfacePragma != remoteCallInterfacePragma) {
			NotificationChain msgs = null;
			if (remoteCallInterfacePragma != null)
				msgs = ((InternalEObject)remoteCallInterfacePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__REMOTE_CALL_INTERFACE_PRAGMA, null, msgs);
			if (newRemoteCallInterfacePragma != null)
				msgs = ((InternalEObject)newRemoteCallInterfacePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__REMOTE_CALL_INTERFACE_PRAGMA, null, msgs);
			msgs = basicSetRemoteCallInterfacePragma(newRemoteCallInterfacePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__REMOTE_CALL_INTERFACE_PRAGMA, newRemoteCallInterfacePragma, newRemoteCallInterfacePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RemoteTypesPragma getRemoteTypesPragma() {
		return remoteTypesPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRemoteTypesPragma(RemoteTypesPragma newRemoteTypesPragma, NotificationChain msgs) {
		RemoteTypesPragma oldRemoteTypesPragma = remoteTypesPragma;
		remoteTypesPragma = newRemoteTypesPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__REMOTE_TYPES_PRAGMA, oldRemoteTypesPragma, newRemoteTypesPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRemoteTypesPragma(RemoteTypesPragma newRemoteTypesPragma) {
		if (newRemoteTypesPragma != remoteTypesPragma) {
			NotificationChain msgs = null;
			if (remoteTypesPragma != null)
				msgs = ((InternalEObject)remoteTypesPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__REMOTE_TYPES_PRAGMA, null, msgs);
			if (newRemoteTypesPragma != null)
				msgs = ((InternalEObject)newRemoteTypesPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__REMOTE_TYPES_PRAGMA, null, msgs);
			msgs = basicSetRemoteTypesPragma(newRemoteTypesPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__REMOTE_TYPES_PRAGMA, newRemoteTypesPragma, newRemoteTypesPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RestrictionsPragma getRestrictionsPragma() {
		return restrictionsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRestrictionsPragma(RestrictionsPragma newRestrictionsPragma, NotificationChain msgs) {
		RestrictionsPragma oldRestrictionsPragma = restrictionsPragma;
		restrictionsPragma = newRestrictionsPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__RESTRICTIONS_PRAGMA, oldRestrictionsPragma, newRestrictionsPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRestrictionsPragma(RestrictionsPragma newRestrictionsPragma) {
		if (newRestrictionsPragma != restrictionsPragma) {
			NotificationChain msgs = null;
			if (restrictionsPragma != null)
				msgs = ((InternalEObject)restrictionsPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__RESTRICTIONS_PRAGMA, null, msgs);
			if (newRestrictionsPragma != null)
				msgs = ((InternalEObject)newRestrictionsPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__RESTRICTIONS_PRAGMA, null, msgs);
			msgs = basicSetRestrictionsPragma(newRestrictionsPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__RESTRICTIONS_PRAGMA, newRestrictionsPragma, newRestrictionsPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReviewablePragma getReviewablePragma() {
		return reviewablePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReviewablePragma(ReviewablePragma newReviewablePragma, NotificationChain msgs) {
		ReviewablePragma oldReviewablePragma = reviewablePragma;
		reviewablePragma = newReviewablePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__REVIEWABLE_PRAGMA, oldReviewablePragma, newReviewablePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReviewablePragma(ReviewablePragma newReviewablePragma) {
		if (newReviewablePragma != reviewablePragma) {
			NotificationChain msgs = null;
			if (reviewablePragma != null)
				msgs = ((InternalEObject)reviewablePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__REVIEWABLE_PRAGMA, null, msgs);
			if (newReviewablePragma != null)
				msgs = ((InternalEObject)newReviewablePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__REVIEWABLE_PRAGMA, null, msgs);
			msgs = basicSetReviewablePragma(newReviewablePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__REVIEWABLE_PRAGMA, newReviewablePragma, newReviewablePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SharedPassivePragma getSharedPassivePragma() {
		return sharedPassivePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSharedPassivePragma(SharedPassivePragma newSharedPassivePragma, NotificationChain msgs) {
		SharedPassivePragma oldSharedPassivePragma = sharedPassivePragma;
		sharedPassivePragma = newSharedPassivePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__SHARED_PASSIVE_PRAGMA, oldSharedPassivePragma, newSharedPassivePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSharedPassivePragma(SharedPassivePragma newSharedPassivePragma) {
		if (newSharedPassivePragma != sharedPassivePragma) {
			NotificationChain msgs = null;
			if (sharedPassivePragma != null)
				msgs = ((InternalEObject)sharedPassivePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__SHARED_PASSIVE_PRAGMA, null, msgs);
			if (newSharedPassivePragma != null)
				msgs = ((InternalEObject)newSharedPassivePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__SHARED_PASSIVE_PRAGMA, null, msgs);
			msgs = basicSetSharedPassivePragma(newSharedPassivePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__SHARED_PASSIVE_PRAGMA, newSharedPassivePragma, newSharedPassivePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StorageSizePragma getStorageSizePragma() {
		return storageSizePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStorageSizePragma(StorageSizePragma newStorageSizePragma, NotificationChain msgs) {
		StorageSizePragma oldStorageSizePragma = storageSizePragma;
		storageSizePragma = newStorageSizePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__STORAGE_SIZE_PRAGMA, oldStorageSizePragma, newStorageSizePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStorageSizePragma(StorageSizePragma newStorageSizePragma) {
		if (newStorageSizePragma != storageSizePragma) {
			NotificationChain msgs = null;
			if (storageSizePragma != null)
				msgs = ((InternalEObject)storageSizePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__STORAGE_SIZE_PRAGMA, null, msgs);
			if (newStorageSizePragma != null)
				msgs = ((InternalEObject)newStorageSizePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__STORAGE_SIZE_PRAGMA, null, msgs);
			msgs = basicSetStorageSizePragma(newStorageSizePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__STORAGE_SIZE_PRAGMA, newStorageSizePragma, newStorageSizePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SuppressPragma getSuppressPragma() {
		return suppressPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSuppressPragma(SuppressPragma newSuppressPragma, NotificationChain msgs) {
		SuppressPragma oldSuppressPragma = suppressPragma;
		suppressPragma = newSuppressPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__SUPPRESS_PRAGMA, oldSuppressPragma, newSuppressPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuppressPragma(SuppressPragma newSuppressPragma) {
		if (newSuppressPragma != suppressPragma) {
			NotificationChain msgs = null;
			if (suppressPragma != null)
				msgs = ((InternalEObject)suppressPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__SUPPRESS_PRAGMA, null, msgs);
			if (newSuppressPragma != null)
				msgs = ((InternalEObject)newSuppressPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__SUPPRESS_PRAGMA, null, msgs);
			msgs = basicSetSuppressPragma(newSuppressPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__SUPPRESS_PRAGMA, newSuppressPragma, newSuppressPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskDispatchingPolicyPragma getTaskDispatchingPolicyPragma() {
		return taskDispatchingPolicyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTaskDispatchingPolicyPragma(TaskDispatchingPolicyPragma newTaskDispatchingPolicyPragma, NotificationChain msgs) {
		TaskDispatchingPolicyPragma oldTaskDispatchingPolicyPragma = taskDispatchingPolicyPragma;
		taskDispatchingPolicyPragma = newTaskDispatchingPolicyPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__TASK_DISPATCHING_POLICY_PRAGMA, oldTaskDispatchingPolicyPragma, newTaskDispatchingPolicyPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaskDispatchingPolicyPragma(TaskDispatchingPolicyPragma newTaskDispatchingPolicyPragma) {
		if (newTaskDispatchingPolicyPragma != taskDispatchingPolicyPragma) {
			NotificationChain msgs = null;
			if (taskDispatchingPolicyPragma != null)
				msgs = ((InternalEObject)taskDispatchingPolicyPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__TASK_DISPATCHING_POLICY_PRAGMA, null, msgs);
			if (newTaskDispatchingPolicyPragma != null)
				msgs = ((InternalEObject)newTaskDispatchingPolicyPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__TASK_DISPATCHING_POLICY_PRAGMA, null, msgs);
			msgs = basicSetTaskDispatchingPolicyPragma(newTaskDispatchingPolicyPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__TASK_DISPATCHING_POLICY_PRAGMA, newTaskDispatchingPolicyPragma, newTaskDispatchingPolicyPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VolatilePragma getVolatilePragma() {
		return volatilePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVolatilePragma(VolatilePragma newVolatilePragma, NotificationChain msgs) {
		VolatilePragma oldVolatilePragma = volatilePragma;
		volatilePragma = newVolatilePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__VOLATILE_PRAGMA, oldVolatilePragma, newVolatilePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVolatilePragma(VolatilePragma newVolatilePragma) {
		if (newVolatilePragma != volatilePragma) {
			NotificationChain msgs = null;
			if (volatilePragma != null)
				msgs = ((InternalEObject)volatilePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__VOLATILE_PRAGMA, null, msgs);
			if (newVolatilePragma != null)
				msgs = ((InternalEObject)newVolatilePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__VOLATILE_PRAGMA, null, msgs);
			msgs = basicSetVolatilePragma(newVolatilePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__VOLATILE_PRAGMA, newVolatilePragma, newVolatilePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VolatileComponentsPragma getVolatileComponentsPragma() {
		return volatileComponentsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVolatileComponentsPragma(VolatileComponentsPragma newVolatileComponentsPragma, NotificationChain msgs) {
		VolatileComponentsPragma oldVolatileComponentsPragma = volatileComponentsPragma;
		volatileComponentsPragma = newVolatileComponentsPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__VOLATILE_COMPONENTS_PRAGMA, oldVolatileComponentsPragma, newVolatileComponentsPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVolatileComponentsPragma(VolatileComponentsPragma newVolatileComponentsPragma) {
		if (newVolatileComponentsPragma != volatileComponentsPragma) {
			NotificationChain msgs = null;
			if (volatileComponentsPragma != null)
				msgs = ((InternalEObject)volatileComponentsPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__VOLATILE_COMPONENTS_PRAGMA, null, msgs);
			if (newVolatileComponentsPragma != null)
				msgs = ((InternalEObject)newVolatileComponentsPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__VOLATILE_COMPONENTS_PRAGMA, null, msgs);
			msgs = basicSetVolatileComponentsPragma(newVolatileComponentsPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__VOLATILE_COMPONENTS_PRAGMA, newVolatileComponentsPragma, newVolatileComponentsPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertPragma getAssertPragma() {
		return assertPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssertPragma(AssertPragma newAssertPragma, NotificationChain msgs) {
		AssertPragma oldAssertPragma = assertPragma;
		assertPragma = newAssertPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ASSERT_PRAGMA, oldAssertPragma, newAssertPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssertPragma(AssertPragma newAssertPragma) {
		if (newAssertPragma != assertPragma) {
			NotificationChain msgs = null;
			if (assertPragma != null)
				msgs = ((InternalEObject)assertPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ASSERT_PRAGMA, null, msgs);
			if (newAssertPragma != null)
				msgs = ((InternalEObject)newAssertPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ASSERT_PRAGMA, null, msgs);
			msgs = basicSetAssertPragma(newAssertPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ASSERT_PRAGMA, newAssertPragma, newAssertPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertionPolicyPragma getAssertionPolicyPragma() {
		return assertionPolicyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssertionPolicyPragma(AssertionPolicyPragma newAssertionPolicyPragma, NotificationChain msgs) {
		AssertionPolicyPragma oldAssertionPolicyPragma = assertionPolicyPragma;
		assertionPolicyPragma = newAssertionPolicyPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ASSERTION_POLICY_PRAGMA, oldAssertionPolicyPragma, newAssertionPolicyPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssertionPolicyPragma(AssertionPolicyPragma newAssertionPolicyPragma) {
		if (newAssertionPolicyPragma != assertionPolicyPragma) {
			NotificationChain msgs = null;
			if (assertionPolicyPragma != null)
				msgs = ((InternalEObject)assertionPolicyPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ASSERTION_POLICY_PRAGMA, null, msgs);
			if (newAssertionPolicyPragma != null)
				msgs = ((InternalEObject)newAssertionPolicyPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__ASSERTION_POLICY_PRAGMA, null, msgs);
			msgs = basicSetAssertionPolicyPragma(newAssertionPolicyPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__ASSERTION_POLICY_PRAGMA, newAssertionPolicyPragma, newAssertionPolicyPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DetectBlockingPragma getDetectBlockingPragma() {
		return detectBlockingPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDetectBlockingPragma(DetectBlockingPragma newDetectBlockingPragma, NotificationChain msgs) {
		DetectBlockingPragma oldDetectBlockingPragma = detectBlockingPragma;
		detectBlockingPragma = newDetectBlockingPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__DETECT_BLOCKING_PRAGMA, oldDetectBlockingPragma, newDetectBlockingPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDetectBlockingPragma(DetectBlockingPragma newDetectBlockingPragma) {
		if (newDetectBlockingPragma != detectBlockingPragma) {
			NotificationChain msgs = null;
			if (detectBlockingPragma != null)
				msgs = ((InternalEObject)detectBlockingPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__DETECT_BLOCKING_PRAGMA, null, msgs);
			if (newDetectBlockingPragma != null)
				msgs = ((InternalEObject)newDetectBlockingPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__DETECT_BLOCKING_PRAGMA, null, msgs);
			msgs = basicSetDetectBlockingPragma(newDetectBlockingPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__DETECT_BLOCKING_PRAGMA, newDetectBlockingPragma, newDetectBlockingPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NoReturnPragma getNoReturnPragma() {
		return noReturnPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNoReturnPragma(NoReturnPragma newNoReturnPragma, NotificationChain msgs) {
		NoReturnPragma oldNoReturnPragma = noReturnPragma;
		noReturnPragma = newNoReturnPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__NO_RETURN_PRAGMA, oldNoReturnPragma, newNoReturnPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNoReturnPragma(NoReturnPragma newNoReturnPragma) {
		if (newNoReturnPragma != noReturnPragma) {
			NotificationChain msgs = null;
			if (noReturnPragma != null)
				msgs = ((InternalEObject)noReturnPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__NO_RETURN_PRAGMA, null, msgs);
			if (newNoReturnPragma != null)
				msgs = ((InternalEObject)newNoReturnPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__NO_RETURN_PRAGMA, null, msgs);
			msgs = basicSetNoReturnPragma(newNoReturnPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__NO_RETURN_PRAGMA, newNoReturnPragma, newNoReturnPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PartitionElaborationPolicyPragma getPartitionElaborationPolicyPragma() {
		return partitionElaborationPolicyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPartitionElaborationPolicyPragma(PartitionElaborationPolicyPragma newPartitionElaborationPolicyPragma, NotificationChain msgs) {
		PartitionElaborationPolicyPragma oldPartitionElaborationPolicyPragma = partitionElaborationPolicyPragma;
		partitionElaborationPolicyPragma = newPartitionElaborationPolicyPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA, oldPartitionElaborationPolicyPragma, newPartitionElaborationPolicyPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPartitionElaborationPolicyPragma(PartitionElaborationPolicyPragma newPartitionElaborationPolicyPragma) {
		if (newPartitionElaborationPolicyPragma != partitionElaborationPolicyPragma) {
			NotificationChain msgs = null;
			if (partitionElaborationPolicyPragma != null)
				msgs = ((InternalEObject)partitionElaborationPolicyPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA, null, msgs);
			if (newPartitionElaborationPolicyPragma != null)
				msgs = ((InternalEObject)newPartitionElaborationPolicyPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA, null, msgs);
			msgs = basicSetPartitionElaborationPolicyPragma(newPartitionElaborationPolicyPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA, newPartitionElaborationPolicyPragma, newPartitionElaborationPolicyPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PreelaborableInitializationPragma getPreelaborableInitializationPragma() {
		return preelaborableInitializationPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPreelaborableInitializationPragma(PreelaborableInitializationPragma newPreelaborableInitializationPragma, NotificationChain msgs) {
		PreelaborableInitializationPragma oldPreelaborableInitializationPragma = preelaborableInitializationPragma;
		preelaborableInitializationPragma = newPreelaborableInitializationPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA, oldPreelaborableInitializationPragma, newPreelaborableInitializationPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreelaborableInitializationPragma(PreelaborableInitializationPragma newPreelaborableInitializationPragma) {
		if (newPreelaborableInitializationPragma != preelaborableInitializationPragma) {
			NotificationChain msgs = null;
			if (preelaborableInitializationPragma != null)
				msgs = ((InternalEObject)preelaborableInitializationPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA, null, msgs);
			if (newPreelaborableInitializationPragma != null)
				msgs = ((InternalEObject)newPreelaborableInitializationPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA, null, msgs);
			msgs = basicSetPreelaborableInitializationPragma(newPreelaborableInitializationPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA, newPreelaborableInitializationPragma, newPreelaborableInitializationPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrioritySpecificDispatchingPragma getPrioritySpecificDispatchingPragma() {
		return prioritySpecificDispatchingPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrioritySpecificDispatchingPragma(PrioritySpecificDispatchingPragma newPrioritySpecificDispatchingPragma, NotificationChain msgs) {
		PrioritySpecificDispatchingPragma oldPrioritySpecificDispatchingPragma = prioritySpecificDispatchingPragma;
		prioritySpecificDispatchingPragma = newPrioritySpecificDispatchingPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA, oldPrioritySpecificDispatchingPragma, newPrioritySpecificDispatchingPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrioritySpecificDispatchingPragma(PrioritySpecificDispatchingPragma newPrioritySpecificDispatchingPragma) {
		if (newPrioritySpecificDispatchingPragma != prioritySpecificDispatchingPragma) {
			NotificationChain msgs = null;
			if (prioritySpecificDispatchingPragma != null)
				msgs = ((InternalEObject)prioritySpecificDispatchingPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA, null, msgs);
			if (newPrioritySpecificDispatchingPragma != null)
				msgs = ((InternalEObject)newPrioritySpecificDispatchingPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA, null, msgs);
			msgs = basicSetPrioritySpecificDispatchingPragma(newPrioritySpecificDispatchingPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA, newPrioritySpecificDispatchingPragma, newPrioritySpecificDispatchingPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProfilePragma getProfilePragma() {
		return profilePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProfilePragma(ProfilePragma newProfilePragma, NotificationChain msgs) {
		ProfilePragma oldProfilePragma = profilePragma;
		profilePragma = newProfilePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__PROFILE_PRAGMA, oldProfilePragma, newProfilePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProfilePragma(ProfilePragma newProfilePragma) {
		if (newProfilePragma != profilePragma) {
			NotificationChain msgs = null;
			if (profilePragma != null)
				msgs = ((InternalEObject)profilePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__PROFILE_PRAGMA, null, msgs);
			if (newProfilePragma != null)
				msgs = ((InternalEObject)newProfilePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__PROFILE_PRAGMA, null, msgs);
			msgs = basicSetProfilePragma(newProfilePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__PROFILE_PRAGMA, newProfilePragma, newProfilePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelativeDeadlinePragma getRelativeDeadlinePragma() {
		return relativeDeadlinePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRelativeDeadlinePragma(RelativeDeadlinePragma newRelativeDeadlinePragma, NotificationChain msgs) {
		RelativeDeadlinePragma oldRelativeDeadlinePragma = relativeDeadlinePragma;
		relativeDeadlinePragma = newRelativeDeadlinePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__RELATIVE_DEADLINE_PRAGMA, oldRelativeDeadlinePragma, newRelativeDeadlinePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRelativeDeadlinePragma(RelativeDeadlinePragma newRelativeDeadlinePragma) {
		if (newRelativeDeadlinePragma != relativeDeadlinePragma) {
			NotificationChain msgs = null;
			if (relativeDeadlinePragma != null)
				msgs = ((InternalEObject)relativeDeadlinePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__RELATIVE_DEADLINE_PRAGMA, null, msgs);
			if (newRelativeDeadlinePragma != null)
				msgs = ((InternalEObject)newRelativeDeadlinePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__RELATIVE_DEADLINE_PRAGMA, null, msgs);
			msgs = basicSetRelativeDeadlinePragma(newRelativeDeadlinePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__RELATIVE_DEADLINE_PRAGMA, newRelativeDeadlinePragma, newRelativeDeadlinePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UncheckedUnionPragma getUncheckedUnionPragma() {
		return uncheckedUnionPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUncheckedUnionPragma(UncheckedUnionPragma newUncheckedUnionPragma, NotificationChain msgs) {
		UncheckedUnionPragma oldUncheckedUnionPragma = uncheckedUnionPragma;
		uncheckedUnionPragma = newUncheckedUnionPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__UNCHECKED_UNION_PRAGMA, oldUncheckedUnionPragma, newUncheckedUnionPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUncheckedUnionPragma(UncheckedUnionPragma newUncheckedUnionPragma) {
		if (newUncheckedUnionPragma != uncheckedUnionPragma) {
			NotificationChain msgs = null;
			if (uncheckedUnionPragma != null)
				msgs = ((InternalEObject)uncheckedUnionPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__UNCHECKED_UNION_PRAGMA, null, msgs);
			if (newUncheckedUnionPragma != null)
				msgs = ((InternalEObject)newUncheckedUnionPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__UNCHECKED_UNION_PRAGMA, null, msgs);
			msgs = basicSetUncheckedUnionPragma(newUncheckedUnionPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__UNCHECKED_UNION_PRAGMA, newUncheckedUnionPragma, newUncheckedUnionPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnsuppressPragma getUnsuppressPragma() {
		return unsuppressPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnsuppressPragma(UnsuppressPragma newUnsuppressPragma, NotificationChain msgs) {
		UnsuppressPragma oldUnsuppressPragma = unsuppressPragma;
		unsuppressPragma = newUnsuppressPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__UNSUPPRESS_PRAGMA, oldUnsuppressPragma, newUnsuppressPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnsuppressPragma(UnsuppressPragma newUnsuppressPragma) {
		if (newUnsuppressPragma != unsuppressPragma) {
			NotificationChain msgs = null;
			if (unsuppressPragma != null)
				msgs = ((InternalEObject)unsuppressPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__UNSUPPRESS_PRAGMA, null, msgs);
			if (newUnsuppressPragma != null)
				msgs = ((InternalEObject)newUnsuppressPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__UNSUPPRESS_PRAGMA, null, msgs);
			msgs = basicSetUnsuppressPragma(newUnsuppressPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__UNSUPPRESS_PRAGMA, newUnsuppressPragma, newUnsuppressPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefaultStoragePoolPragma getDefaultStoragePoolPragma() {
		return defaultStoragePoolPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefaultStoragePoolPragma(DefaultStoragePoolPragma newDefaultStoragePoolPragma, NotificationChain msgs) {
		DefaultStoragePoolPragma oldDefaultStoragePoolPragma = defaultStoragePoolPragma;
		defaultStoragePoolPragma = newDefaultStoragePoolPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__DEFAULT_STORAGE_POOL_PRAGMA, oldDefaultStoragePoolPragma, newDefaultStoragePoolPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultStoragePoolPragma(DefaultStoragePoolPragma newDefaultStoragePoolPragma) {
		if (newDefaultStoragePoolPragma != defaultStoragePoolPragma) {
			NotificationChain msgs = null;
			if (defaultStoragePoolPragma != null)
				msgs = ((InternalEObject)defaultStoragePoolPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__DEFAULT_STORAGE_POOL_PRAGMA, null, msgs);
			if (newDefaultStoragePoolPragma != null)
				msgs = ((InternalEObject)newDefaultStoragePoolPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__DEFAULT_STORAGE_POOL_PRAGMA, null, msgs);
			msgs = basicSetDefaultStoragePoolPragma(newDefaultStoragePoolPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__DEFAULT_STORAGE_POOL_PRAGMA, newDefaultStoragePoolPragma, newDefaultStoragePoolPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DispatchingDomainPragma getDispatchingDomainPragma() {
		return dispatchingDomainPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDispatchingDomainPragma(DispatchingDomainPragma newDispatchingDomainPragma, NotificationChain msgs) {
		DispatchingDomainPragma oldDispatchingDomainPragma = dispatchingDomainPragma;
		dispatchingDomainPragma = newDispatchingDomainPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__DISPATCHING_DOMAIN_PRAGMA, oldDispatchingDomainPragma, newDispatchingDomainPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDispatchingDomainPragma(DispatchingDomainPragma newDispatchingDomainPragma) {
		if (newDispatchingDomainPragma != dispatchingDomainPragma) {
			NotificationChain msgs = null;
			if (dispatchingDomainPragma != null)
				msgs = ((InternalEObject)dispatchingDomainPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__DISPATCHING_DOMAIN_PRAGMA, null, msgs);
			if (newDispatchingDomainPragma != null)
				msgs = ((InternalEObject)newDispatchingDomainPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__DISPATCHING_DOMAIN_PRAGMA, null, msgs);
			msgs = basicSetDispatchingDomainPragma(newDispatchingDomainPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__DISPATCHING_DOMAIN_PRAGMA, newDispatchingDomainPragma, newDispatchingDomainPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CpuPragma getCpuPragma() {
		return cpuPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCpuPragma(CpuPragma newCpuPragma, NotificationChain msgs) {
		CpuPragma oldCpuPragma = cpuPragma;
		cpuPragma = newCpuPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__CPU_PRAGMA, oldCpuPragma, newCpuPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCpuPragma(CpuPragma newCpuPragma) {
		if (newCpuPragma != cpuPragma) {
			NotificationChain msgs = null;
			if (cpuPragma != null)
				msgs = ((InternalEObject)cpuPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__CPU_PRAGMA, null, msgs);
			if (newCpuPragma != null)
				msgs = ((InternalEObject)newCpuPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__CPU_PRAGMA, null, msgs);
			msgs = basicSetCpuPragma(newCpuPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__CPU_PRAGMA, newCpuPragma, newCpuPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IndependentPragma getIndependentPragma() {
		return independentPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIndependentPragma(IndependentPragma newIndependentPragma, NotificationChain msgs) {
		IndependentPragma oldIndependentPragma = independentPragma;
		independentPragma = newIndependentPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__INDEPENDENT_PRAGMA, oldIndependentPragma, newIndependentPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndependentPragma(IndependentPragma newIndependentPragma) {
		if (newIndependentPragma != independentPragma) {
			NotificationChain msgs = null;
			if (independentPragma != null)
				msgs = ((InternalEObject)independentPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__INDEPENDENT_PRAGMA, null, msgs);
			if (newIndependentPragma != null)
				msgs = ((InternalEObject)newIndependentPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__INDEPENDENT_PRAGMA, null, msgs);
			msgs = basicSetIndependentPragma(newIndependentPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__INDEPENDENT_PRAGMA, newIndependentPragma, newIndependentPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IndependentComponentsPragma getIndependentComponentsPragma() {
		return independentComponentsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIndependentComponentsPragma(IndependentComponentsPragma newIndependentComponentsPragma, NotificationChain msgs) {
		IndependentComponentsPragma oldIndependentComponentsPragma = independentComponentsPragma;
		independentComponentsPragma = newIndependentComponentsPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__INDEPENDENT_COMPONENTS_PRAGMA, oldIndependentComponentsPragma, newIndependentComponentsPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndependentComponentsPragma(IndependentComponentsPragma newIndependentComponentsPragma) {
		if (newIndependentComponentsPragma != independentComponentsPragma) {
			NotificationChain msgs = null;
			if (independentComponentsPragma != null)
				msgs = ((InternalEObject)independentComponentsPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__INDEPENDENT_COMPONENTS_PRAGMA, null, msgs);
			if (newIndependentComponentsPragma != null)
				msgs = ((InternalEObject)newIndependentComponentsPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__INDEPENDENT_COMPONENTS_PRAGMA, null, msgs);
			msgs = basicSetIndependentComponentsPragma(newIndependentComponentsPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__INDEPENDENT_COMPONENTS_PRAGMA, newIndependentComponentsPragma, newIndependentComponentsPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImplementationDefinedPragma getImplementationDefinedPragma() {
		return implementationDefinedPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImplementationDefinedPragma(ImplementationDefinedPragma newImplementationDefinedPragma, NotificationChain msgs) {
		ImplementationDefinedPragma oldImplementationDefinedPragma = implementationDefinedPragma;
		implementationDefinedPragma = newImplementationDefinedPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__IMPLEMENTATION_DEFINED_PRAGMA, oldImplementationDefinedPragma, newImplementationDefinedPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImplementationDefinedPragma(ImplementationDefinedPragma newImplementationDefinedPragma) {
		if (newImplementationDefinedPragma != implementationDefinedPragma) {
			NotificationChain msgs = null;
			if (implementationDefinedPragma != null)
				msgs = ((InternalEObject)implementationDefinedPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__IMPLEMENTATION_DEFINED_PRAGMA, null, msgs);
			if (newImplementationDefinedPragma != null)
				msgs = ((InternalEObject)newImplementationDefinedPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__IMPLEMENTATION_DEFINED_PRAGMA, null, msgs);
			msgs = basicSetImplementationDefinedPragma(newImplementationDefinedPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__IMPLEMENTATION_DEFINED_PRAGMA, newImplementationDefinedPragma, newImplementationDefinedPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnknownPragma getUnknownPragma() {
		return unknownPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnknownPragma(UnknownPragma newUnknownPragma, NotificationChain msgs) {
		UnknownPragma oldUnknownPragma = unknownPragma;
		unknownPragma = newUnknownPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__UNKNOWN_PRAGMA, oldUnknownPragma, newUnknownPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnknownPragma(UnknownPragma newUnknownPragma) {
		if (newUnknownPragma != unknownPragma) {
			NotificationChain msgs = null;
			if (unknownPragma != null)
				msgs = ((InternalEObject)unknownPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__UNKNOWN_PRAGMA, null, msgs);
			if (newUnknownPragma != null)
				msgs = ((InternalEObject)newUnknownPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.NAME_CLASS__UNKNOWN_PRAGMA, null, msgs);
			msgs = basicSetUnknownPragma(newUnknownPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.NAME_CLASS__UNKNOWN_PRAGMA, newUnknownPragma, newUnknownPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.NAME_CLASS__NOT_AN_ELEMENT:
				return basicSetNotAnElement(null, msgs);
			case AdaPackage.NAME_CLASS__IDENTIFIER:
				return basicSetIdentifier(null, msgs);
			case AdaPackage.NAME_CLASS__SELECTED_COMPONENT:
				return basicSetSelectedComponent(null, msgs);
			case AdaPackage.NAME_CLASS__ACCESS_ATTRIBUTE:
				return basicSetAccessAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__ADDRESS_ATTRIBUTE:
				return basicSetAddressAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__ADJACENT_ATTRIBUTE:
				return basicSetAdjacentAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__AFT_ATTRIBUTE:
				return basicSetAftAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__ALIGNMENT_ATTRIBUTE:
				return basicSetAlignmentAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__BASE_ATTRIBUTE:
				return basicSetBaseAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__BIT_ORDER_ATTRIBUTE:
				return basicSetBitOrderAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__BODY_VERSION_ATTRIBUTE:
				return basicSetBodyVersionAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__CALLABLE_ATTRIBUTE:
				return basicSetCallableAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__CALLER_ATTRIBUTE:
				return basicSetCallerAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__CEILING_ATTRIBUTE:
				return basicSetCeilingAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__CLASS_ATTRIBUTE:
				return basicSetClassAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__COMPONENT_SIZE_ATTRIBUTE:
				return basicSetComponentSizeAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__COMPOSE_ATTRIBUTE:
				return basicSetComposeAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__CONSTRAINED_ATTRIBUTE:
				return basicSetConstrainedAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__COPY_SIGN_ATTRIBUTE:
				return basicSetCopySignAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__COUNT_ATTRIBUTE:
				return basicSetCountAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__DEFINITE_ATTRIBUTE:
				return basicSetDefiniteAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__DELTA_ATTRIBUTE:
				return basicSetDeltaAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__DENORM_ATTRIBUTE:
				return basicSetDenormAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__DIGITS_ATTRIBUTE:
				return basicSetDigitsAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__EXPONENT_ATTRIBUTE:
				return basicSetExponentAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__EXTERNAL_TAG_ATTRIBUTE:
				return basicSetExternalTagAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__FIRST_ATTRIBUTE:
				return basicSetFirstAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__FIRST_BIT_ATTRIBUTE:
				return basicSetFirstBitAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__FLOOR_ATTRIBUTE:
				return basicSetFloorAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__FORE_ATTRIBUTE:
				return basicSetForeAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__FRACTION_ATTRIBUTE:
				return basicSetFractionAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__IDENTITY_ATTRIBUTE:
				return basicSetIdentityAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__IMAGE_ATTRIBUTE:
				return basicSetImageAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__INPUT_ATTRIBUTE:
				return basicSetInputAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__LAST_ATTRIBUTE:
				return basicSetLastAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__LAST_BIT_ATTRIBUTE:
				return basicSetLastBitAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__LEADING_PART_ATTRIBUTE:
				return basicSetLeadingPartAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__LENGTH_ATTRIBUTE:
				return basicSetLengthAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__MACHINE_ATTRIBUTE:
				return basicSetMachineAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__MACHINE_EMAX_ATTRIBUTE:
				return basicSetMachineEmaxAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__MACHINE_EMIN_ATTRIBUTE:
				return basicSetMachineEminAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__MACHINE_MANTISSA_ATTRIBUTE:
				return basicSetMachineMantissaAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__MACHINE_OVERFLOWS_ATTRIBUTE:
				return basicSetMachineOverflowsAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__MACHINE_RADIX_ATTRIBUTE:
				return basicSetMachineRadixAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__MACHINE_ROUNDS_ATTRIBUTE:
				return basicSetMachineRoundsAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__MAX_ATTRIBUTE:
				return basicSetMaxAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE:
				return basicSetMaxSizeInStorageElementsAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__MIN_ATTRIBUTE:
				return basicSetMinAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__MODEL_ATTRIBUTE:
				return basicSetModelAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__MODEL_EMIN_ATTRIBUTE:
				return basicSetModelEminAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__MODEL_EPSILON_ATTRIBUTE:
				return basicSetModelEpsilonAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__MODEL_MANTISSA_ATTRIBUTE:
				return basicSetModelMantissaAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__MODEL_SMALL_ATTRIBUTE:
				return basicSetModelSmallAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__MODULUS_ATTRIBUTE:
				return basicSetModulusAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__OUTPUT_ATTRIBUTE:
				return basicSetOutputAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__PARTITION_ID_ATTRIBUTE:
				return basicSetPartitionIdAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__POS_ATTRIBUTE:
				return basicSetPosAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__POSITION_ATTRIBUTE:
				return basicSetPositionAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__PRED_ATTRIBUTE:
				return basicSetPredAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__RANGE_ATTRIBUTE:
				return basicSetRangeAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__READ_ATTRIBUTE:
				return basicSetReadAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__REMAINDER_ATTRIBUTE:
				return basicSetRemainderAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__ROUND_ATTRIBUTE:
				return basicSetRoundAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__ROUNDING_ATTRIBUTE:
				return basicSetRoundingAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__SAFE_FIRST_ATTRIBUTE:
				return basicSetSafeFirstAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__SAFE_LAST_ATTRIBUTE:
				return basicSetSafeLastAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__SCALE_ATTRIBUTE:
				return basicSetScaleAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__SCALING_ATTRIBUTE:
				return basicSetScalingAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__SIGNED_ZEROS_ATTRIBUTE:
				return basicSetSignedZerosAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__SIZE_ATTRIBUTE:
				return basicSetSizeAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__SMALL_ATTRIBUTE:
				return basicSetSmallAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__STORAGE_POOL_ATTRIBUTE:
				return basicSetStoragePoolAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__STORAGE_SIZE_ATTRIBUTE:
				return basicSetStorageSizeAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__SUCC_ATTRIBUTE:
				return basicSetSuccAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__TAG_ATTRIBUTE:
				return basicSetTagAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__TERMINATED_ATTRIBUTE:
				return basicSetTerminatedAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__TRUNCATION_ATTRIBUTE:
				return basicSetTruncationAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__UNBIASED_ROUNDING_ATTRIBUTE:
				return basicSetUnbiasedRoundingAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__UNCHECKED_ACCESS_ATTRIBUTE:
				return basicSetUncheckedAccessAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__VAL_ATTRIBUTE:
				return basicSetValAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__VALID_ATTRIBUTE:
				return basicSetValidAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__VALUE_ATTRIBUTE:
				return basicSetValueAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__VERSION_ATTRIBUTE:
				return basicSetVersionAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__WIDE_IMAGE_ATTRIBUTE:
				return basicSetWideImageAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__WIDE_VALUE_ATTRIBUTE:
				return basicSetWideValueAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__WIDE_WIDTH_ATTRIBUTE:
				return basicSetWideWidthAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__WIDTH_ATTRIBUTE:
				return basicSetWidthAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__WRITE_ATTRIBUTE:
				return basicSetWriteAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__MACHINE_ROUNDING_ATTRIBUTE:
				return basicSetMachineRoundingAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__MOD_ATTRIBUTE:
				return basicSetModAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__PRIORITY_ATTRIBUTE:
				return basicSetPriorityAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__STREAM_SIZE_ATTRIBUTE:
				return basicSetStreamSizeAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__WIDE_WIDE_IMAGE_ATTRIBUTE:
				return basicSetWideWideImageAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__WIDE_WIDE_VALUE_ATTRIBUTE:
				return basicSetWideWideValueAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__WIDE_WIDE_WIDTH_ATTRIBUTE:
				return basicSetWideWideWidthAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE:
				return basicSetMaxAlignmentForAllocationAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__OVERLAPS_STORAGE_ATTRIBUTE:
				return basicSetOverlapsStorageAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__IMPLEMENTATION_DEFINED_ATTRIBUTE:
				return basicSetImplementationDefinedAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__UNKNOWN_ATTRIBUTE:
				return basicSetUnknownAttribute(null, msgs);
			case AdaPackage.NAME_CLASS__COMMENT:
				return basicSetComment(null, msgs);
			case AdaPackage.NAME_CLASS__ALL_CALLS_REMOTE_PRAGMA:
				return basicSetAllCallsRemotePragma(null, msgs);
			case AdaPackage.NAME_CLASS__ASYNCHRONOUS_PRAGMA:
				return basicSetAsynchronousPragma(null, msgs);
			case AdaPackage.NAME_CLASS__ATOMIC_PRAGMA:
				return basicSetAtomicPragma(null, msgs);
			case AdaPackage.NAME_CLASS__ATOMIC_COMPONENTS_PRAGMA:
				return basicSetAtomicComponentsPragma(null, msgs);
			case AdaPackage.NAME_CLASS__ATTACH_HANDLER_PRAGMA:
				return basicSetAttachHandlerPragma(null, msgs);
			case AdaPackage.NAME_CLASS__CONTROLLED_PRAGMA:
				return basicSetControlledPragma(null, msgs);
			case AdaPackage.NAME_CLASS__CONVENTION_PRAGMA:
				return basicSetConventionPragma(null, msgs);
			case AdaPackage.NAME_CLASS__DISCARD_NAMES_PRAGMA:
				return basicSetDiscardNamesPragma(null, msgs);
			case AdaPackage.NAME_CLASS__ELABORATE_PRAGMA:
				return basicSetElaboratePragma(null, msgs);
			case AdaPackage.NAME_CLASS__ELABORATE_ALL_PRAGMA:
				return basicSetElaborateAllPragma(null, msgs);
			case AdaPackage.NAME_CLASS__ELABORATE_BODY_PRAGMA:
				return basicSetElaborateBodyPragma(null, msgs);
			case AdaPackage.NAME_CLASS__EXPORT_PRAGMA:
				return basicSetExportPragma(null, msgs);
			case AdaPackage.NAME_CLASS__IMPORT_PRAGMA:
				return basicSetImportPragma(null, msgs);
			case AdaPackage.NAME_CLASS__INLINE_PRAGMA:
				return basicSetInlinePragma(null, msgs);
			case AdaPackage.NAME_CLASS__INSPECTION_POINT_PRAGMA:
				return basicSetInspectionPointPragma(null, msgs);
			case AdaPackage.NAME_CLASS__INTERRUPT_HANDLER_PRAGMA:
				return basicSetInterruptHandlerPragma(null, msgs);
			case AdaPackage.NAME_CLASS__INTERRUPT_PRIORITY_PRAGMA:
				return basicSetInterruptPriorityPragma(null, msgs);
			case AdaPackage.NAME_CLASS__LINKER_OPTIONS_PRAGMA:
				return basicSetLinkerOptionsPragma(null, msgs);
			case AdaPackage.NAME_CLASS__LIST_PRAGMA:
				return basicSetListPragma(null, msgs);
			case AdaPackage.NAME_CLASS__LOCKING_POLICY_PRAGMA:
				return basicSetLockingPolicyPragma(null, msgs);
			case AdaPackage.NAME_CLASS__NORMALIZE_SCALARS_PRAGMA:
				return basicSetNormalizeScalarsPragma(null, msgs);
			case AdaPackage.NAME_CLASS__OPTIMIZE_PRAGMA:
				return basicSetOptimizePragma(null, msgs);
			case AdaPackage.NAME_CLASS__PACK_PRAGMA:
				return basicSetPackPragma(null, msgs);
			case AdaPackage.NAME_CLASS__PAGE_PRAGMA:
				return basicSetPagePragma(null, msgs);
			case AdaPackage.NAME_CLASS__PREELABORATE_PRAGMA:
				return basicSetPreelaboratePragma(null, msgs);
			case AdaPackage.NAME_CLASS__PRIORITY_PRAGMA:
				return basicSetPriorityPragma(null, msgs);
			case AdaPackage.NAME_CLASS__PURE_PRAGMA:
				return basicSetPurePragma(null, msgs);
			case AdaPackage.NAME_CLASS__QUEUING_POLICY_PRAGMA:
				return basicSetQueuingPolicyPragma(null, msgs);
			case AdaPackage.NAME_CLASS__REMOTE_CALL_INTERFACE_PRAGMA:
				return basicSetRemoteCallInterfacePragma(null, msgs);
			case AdaPackage.NAME_CLASS__REMOTE_TYPES_PRAGMA:
				return basicSetRemoteTypesPragma(null, msgs);
			case AdaPackage.NAME_CLASS__RESTRICTIONS_PRAGMA:
				return basicSetRestrictionsPragma(null, msgs);
			case AdaPackage.NAME_CLASS__REVIEWABLE_PRAGMA:
				return basicSetReviewablePragma(null, msgs);
			case AdaPackage.NAME_CLASS__SHARED_PASSIVE_PRAGMA:
				return basicSetSharedPassivePragma(null, msgs);
			case AdaPackage.NAME_CLASS__STORAGE_SIZE_PRAGMA:
				return basicSetStorageSizePragma(null, msgs);
			case AdaPackage.NAME_CLASS__SUPPRESS_PRAGMA:
				return basicSetSuppressPragma(null, msgs);
			case AdaPackage.NAME_CLASS__TASK_DISPATCHING_POLICY_PRAGMA:
				return basicSetTaskDispatchingPolicyPragma(null, msgs);
			case AdaPackage.NAME_CLASS__VOLATILE_PRAGMA:
				return basicSetVolatilePragma(null, msgs);
			case AdaPackage.NAME_CLASS__VOLATILE_COMPONENTS_PRAGMA:
				return basicSetVolatileComponentsPragma(null, msgs);
			case AdaPackage.NAME_CLASS__ASSERT_PRAGMA:
				return basicSetAssertPragma(null, msgs);
			case AdaPackage.NAME_CLASS__ASSERTION_POLICY_PRAGMA:
				return basicSetAssertionPolicyPragma(null, msgs);
			case AdaPackage.NAME_CLASS__DETECT_BLOCKING_PRAGMA:
				return basicSetDetectBlockingPragma(null, msgs);
			case AdaPackage.NAME_CLASS__NO_RETURN_PRAGMA:
				return basicSetNoReturnPragma(null, msgs);
			case AdaPackage.NAME_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA:
				return basicSetPartitionElaborationPolicyPragma(null, msgs);
			case AdaPackage.NAME_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA:
				return basicSetPreelaborableInitializationPragma(null, msgs);
			case AdaPackage.NAME_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return basicSetPrioritySpecificDispatchingPragma(null, msgs);
			case AdaPackage.NAME_CLASS__PROFILE_PRAGMA:
				return basicSetProfilePragma(null, msgs);
			case AdaPackage.NAME_CLASS__RELATIVE_DEADLINE_PRAGMA:
				return basicSetRelativeDeadlinePragma(null, msgs);
			case AdaPackage.NAME_CLASS__UNCHECKED_UNION_PRAGMA:
				return basicSetUncheckedUnionPragma(null, msgs);
			case AdaPackage.NAME_CLASS__UNSUPPRESS_PRAGMA:
				return basicSetUnsuppressPragma(null, msgs);
			case AdaPackage.NAME_CLASS__DEFAULT_STORAGE_POOL_PRAGMA:
				return basicSetDefaultStoragePoolPragma(null, msgs);
			case AdaPackage.NAME_CLASS__DISPATCHING_DOMAIN_PRAGMA:
				return basicSetDispatchingDomainPragma(null, msgs);
			case AdaPackage.NAME_CLASS__CPU_PRAGMA:
				return basicSetCpuPragma(null, msgs);
			case AdaPackage.NAME_CLASS__INDEPENDENT_PRAGMA:
				return basicSetIndependentPragma(null, msgs);
			case AdaPackage.NAME_CLASS__INDEPENDENT_COMPONENTS_PRAGMA:
				return basicSetIndependentComponentsPragma(null, msgs);
			case AdaPackage.NAME_CLASS__IMPLEMENTATION_DEFINED_PRAGMA:
				return basicSetImplementationDefinedPragma(null, msgs);
			case AdaPackage.NAME_CLASS__UNKNOWN_PRAGMA:
				return basicSetUnknownPragma(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.NAME_CLASS__NOT_AN_ELEMENT:
				return getNotAnElement();
			case AdaPackage.NAME_CLASS__IDENTIFIER:
				return getIdentifier();
			case AdaPackage.NAME_CLASS__SELECTED_COMPONENT:
				return getSelectedComponent();
			case AdaPackage.NAME_CLASS__ACCESS_ATTRIBUTE:
				return getAccessAttribute();
			case AdaPackage.NAME_CLASS__ADDRESS_ATTRIBUTE:
				return getAddressAttribute();
			case AdaPackage.NAME_CLASS__ADJACENT_ATTRIBUTE:
				return getAdjacentAttribute();
			case AdaPackage.NAME_CLASS__AFT_ATTRIBUTE:
				return getAftAttribute();
			case AdaPackage.NAME_CLASS__ALIGNMENT_ATTRIBUTE:
				return getAlignmentAttribute();
			case AdaPackage.NAME_CLASS__BASE_ATTRIBUTE:
				return getBaseAttribute();
			case AdaPackage.NAME_CLASS__BIT_ORDER_ATTRIBUTE:
				return getBitOrderAttribute();
			case AdaPackage.NAME_CLASS__BODY_VERSION_ATTRIBUTE:
				return getBodyVersionAttribute();
			case AdaPackage.NAME_CLASS__CALLABLE_ATTRIBUTE:
				return getCallableAttribute();
			case AdaPackage.NAME_CLASS__CALLER_ATTRIBUTE:
				return getCallerAttribute();
			case AdaPackage.NAME_CLASS__CEILING_ATTRIBUTE:
				return getCeilingAttribute();
			case AdaPackage.NAME_CLASS__CLASS_ATTRIBUTE:
				return getClassAttribute();
			case AdaPackage.NAME_CLASS__COMPONENT_SIZE_ATTRIBUTE:
				return getComponentSizeAttribute();
			case AdaPackage.NAME_CLASS__COMPOSE_ATTRIBUTE:
				return getComposeAttribute();
			case AdaPackage.NAME_CLASS__CONSTRAINED_ATTRIBUTE:
				return getConstrainedAttribute();
			case AdaPackage.NAME_CLASS__COPY_SIGN_ATTRIBUTE:
				return getCopySignAttribute();
			case AdaPackage.NAME_CLASS__COUNT_ATTRIBUTE:
				return getCountAttribute();
			case AdaPackage.NAME_CLASS__DEFINITE_ATTRIBUTE:
				return getDefiniteAttribute();
			case AdaPackage.NAME_CLASS__DELTA_ATTRIBUTE:
				return getDeltaAttribute();
			case AdaPackage.NAME_CLASS__DENORM_ATTRIBUTE:
				return getDenormAttribute();
			case AdaPackage.NAME_CLASS__DIGITS_ATTRIBUTE:
				return getDigitsAttribute();
			case AdaPackage.NAME_CLASS__EXPONENT_ATTRIBUTE:
				return getExponentAttribute();
			case AdaPackage.NAME_CLASS__EXTERNAL_TAG_ATTRIBUTE:
				return getExternalTagAttribute();
			case AdaPackage.NAME_CLASS__FIRST_ATTRIBUTE:
				return getFirstAttribute();
			case AdaPackage.NAME_CLASS__FIRST_BIT_ATTRIBUTE:
				return getFirstBitAttribute();
			case AdaPackage.NAME_CLASS__FLOOR_ATTRIBUTE:
				return getFloorAttribute();
			case AdaPackage.NAME_CLASS__FORE_ATTRIBUTE:
				return getForeAttribute();
			case AdaPackage.NAME_CLASS__FRACTION_ATTRIBUTE:
				return getFractionAttribute();
			case AdaPackage.NAME_CLASS__IDENTITY_ATTRIBUTE:
				return getIdentityAttribute();
			case AdaPackage.NAME_CLASS__IMAGE_ATTRIBUTE:
				return getImageAttribute();
			case AdaPackage.NAME_CLASS__INPUT_ATTRIBUTE:
				return getInputAttribute();
			case AdaPackage.NAME_CLASS__LAST_ATTRIBUTE:
				return getLastAttribute();
			case AdaPackage.NAME_CLASS__LAST_BIT_ATTRIBUTE:
				return getLastBitAttribute();
			case AdaPackage.NAME_CLASS__LEADING_PART_ATTRIBUTE:
				return getLeadingPartAttribute();
			case AdaPackage.NAME_CLASS__LENGTH_ATTRIBUTE:
				return getLengthAttribute();
			case AdaPackage.NAME_CLASS__MACHINE_ATTRIBUTE:
				return getMachineAttribute();
			case AdaPackage.NAME_CLASS__MACHINE_EMAX_ATTRIBUTE:
				return getMachineEmaxAttribute();
			case AdaPackage.NAME_CLASS__MACHINE_EMIN_ATTRIBUTE:
				return getMachineEminAttribute();
			case AdaPackage.NAME_CLASS__MACHINE_MANTISSA_ATTRIBUTE:
				return getMachineMantissaAttribute();
			case AdaPackage.NAME_CLASS__MACHINE_OVERFLOWS_ATTRIBUTE:
				return getMachineOverflowsAttribute();
			case AdaPackage.NAME_CLASS__MACHINE_RADIX_ATTRIBUTE:
				return getMachineRadixAttribute();
			case AdaPackage.NAME_CLASS__MACHINE_ROUNDS_ATTRIBUTE:
				return getMachineRoundsAttribute();
			case AdaPackage.NAME_CLASS__MAX_ATTRIBUTE:
				return getMaxAttribute();
			case AdaPackage.NAME_CLASS__MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE:
				return getMaxSizeInStorageElementsAttribute();
			case AdaPackage.NAME_CLASS__MIN_ATTRIBUTE:
				return getMinAttribute();
			case AdaPackage.NAME_CLASS__MODEL_ATTRIBUTE:
				return getModelAttribute();
			case AdaPackage.NAME_CLASS__MODEL_EMIN_ATTRIBUTE:
				return getModelEminAttribute();
			case AdaPackage.NAME_CLASS__MODEL_EPSILON_ATTRIBUTE:
				return getModelEpsilonAttribute();
			case AdaPackage.NAME_CLASS__MODEL_MANTISSA_ATTRIBUTE:
				return getModelMantissaAttribute();
			case AdaPackage.NAME_CLASS__MODEL_SMALL_ATTRIBUTE:
				return getModelSmallAttribute();
			case AdaPackage.NAME_CLASS__MODULUS_ATTRIBUTE:
				return getModulusAttribute();
			case AdaPackage.NAME_CLASS__OUTPUT_ATTRIBUTE:
				return getOutputAttribute();
			case AdaPackage.NAME_CLASS__PARTITION_ID_ATTRIBUTE:
				return getPartitionIdAttribute();
			case AdaPackage.NAME_CLASS__POS_ATTRIBUTE:
				return getPosAttribute();
			case AdaPackage.NAME_CLASS__POSITION_ATTRIBUTE:
				return getPositionAttribute();
			case AdaPackage.NAME_CLASS__PRED_ATTRIBUTE:
				return getPredAttribute();
			case AdaPackage.NAME_CLASS__RANGE_ATTRIBUTE:
				return getRangeAttribute();
			case AdaPackage.NAME_CLASS__READ_ATTRIBUTE:
				return getReadAttribute();
			case AdaPackage.NAME_CLASS__REMAINDER_ATTRIBUTE:
				return getRemainderAttribute();
			case AdaPackage.NAME_CLASS__ROUND_ATTRIBUTE:
				return getRoundAttribute();
			case AdaPackage.NAME_CLASS__ROUNDING_ATTRIBUTE:
				return getRoundingAttribute();
			case AdaPackage.NAME_CLASS__SAFE_FIRST_ATTRIBUTE:
				return getSafeFirstAttribute();
			case AdaPackage.NAME_CLASS__SAFE_LAST_ATTRIBUTE:
				return getSafeLastAttribute();
			case AdaPackage.NAME_CLASS__SCALE_ATTRIBUTE:
				return getScaleAttribute();
			case AdaPackage.NAME_CLASS__SCALING_ATTRIBUTE:
				return getScalingAttribute();
			case AdaPackage.NAME_CLASS__SIGNED_ZEROS_ATTRIBUTE:
				return getSignedZerosAttribute();
			case AdaPackage.NAME_CLASS__SIZE_ATTRIBUTE:
				return getSizeAttribute();
			case AdaPackage.NAME_CLASS__SMALL_ATTRIBUTE:
				return getSmallAttribute();
			case AdaPackage.NAME_CLASS__STORAGE_POOL_ATTRIBUTE:
				return getStoragePoolAttribute();
			case AdaPackage.NAME_CLASS__STORAGE_SIZE_ATTRIBUTE:
				return getStorageSizeAttribute();
			case AdaPackage.NAME_CLASS__SUCC_ATTRIBUTE:
				return getSuccAttribute();
			case AdaPackage.NAME_CLASS__TAG_ATTRIBUTE:
				return getTagAttribute();
			case AdaPackage.NAME_CLASS__TERMINATED_ATTRIBUTE:
				return getTerminatedAttribute();
			case AdaPackage.NAME_CLASS__TRUNCATION_ATTRIBUTE:
				return getTruncationAttribute();
			case AdaPackage.NAME_CLASS__UNBIASED_ROUNDING_ATTRIBUTE:
				return getUnbiasedRoundingAttribute();
			case AdaPackage.NAME_CLASS__UNCHECKED_ACCESS_ATTRIBUTE:
				return getUncheckedAccessAttribute();
			case AdaPackage.NAME_CLASS__VAL_ATTRIBUTE:
				return getValAttribute();
			case AdaPackage.NAME_CLASS__VALID_ATTRIBUTE:
				return getValidAttribute();
			case AdaPackage.NAME_CLASS__VALUE_ATTRIBUTE:
				return getValueAttribute();
			case AdaPackage.NAME_CLASS__VERSION_ATTRIBUTE:
				return getVersionAttribute();
			case AdaPackage.NAME_CLASS__WIDE_IMAGE_ATTRIBUTE:
				return getWideImageAttribute();
			case AdaPackage.NAME_CLASS__WIDE_VALUE_ATTRIBUTE:
				return getWideValueAttribute();
			case AdaPackage.NAME_CLASS__WIDE_WIDTH_ATTRIBUTE:
				return getWideWidthAttribute();
			case AdaPackage.NAME_CLASS__WIDTH_ATTRIBUTE:
				return getWidthAttribute();
			case AdaPackage.NAME_CLASS__WRITE_ATTRIBUTE:
				return getWriteAttribute();
			case AdaPackage.NAME_CLASS__MACHINE_ROUNDING_ATTRIBUTE:
				return getMachineRoundingAttribute();
			case AdaPackage.NAME_CLASS__MOD_ATTRIBUTE:
				return getModAttribute();
			case AdaPackage.NAME_CLASS__PRIORITY_ATTRIBUTE:
				return getPriorityAttribute();
			case AdaPackage.NAME_CLASS__STREAM_SIZE_ATTRIBUTE:
				return getStreamSizeAttribute();
			case AdaPackage.NAME_CLASS__WIDE_WIDE_IMAGE_ATTRIBUTE:
				return getWideWideImageAttribute();
			case AdaPackage.NAME_CLASS__WIDE_WIDE_VALUE_ATTRIBUTE:
				return getWideWideValueAttribute();
			case AdaPackage.NAME_CLASS__WIDE_WIDE_WIDTH_ATTRIBUTE:
				return getWideWideWidthAttribute();
			case AdaPackage.NAME_CLASS__MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE:
				return getMaxAlignmentForAllocationAttribute();
			case AdaPackage.NAME_CLASS__OVERLAPS_STORAGE_ATTRIBUTE:
				return getOverlapsStorageAttribute();
			case AdaPackage.NAME_CLASS__IMPLEMENTATION_DEFINED_ATTRIBUTE:
				return getImplementationDefinedAttribute();
			case AdaPackage.NAME_CLASS__UNKNOWN_ATTRIBUTE:
				return getUnknownAttribute();
			case AdaPackage.NAME_CLASS__COMMENT:
				return getComment();
			case AdaPackage.NAME_CLASS__ALL_CALLS_REMOTE_PRAGMA:
				return getAllCallsRemotePragma();
			case AdaPackage.NAME_CLASS__ASYNCHRONOUS_PRAGMA:
				return getAsynchronousPragma();
			case AdaPackage.NAME_CLASS__ATOMIC_PRAGMA:
				return getAtomicPragma();
			case AdaPackage.NAME_CLASS__ATOMIC_COMPONENTS_PRAGMA:
				return getAtomicComponentsPragma();
			case AdaPackage.NAME_CLASS__ATTACH_HANDLER_PRAGMA:
				return getAttachHandlerPragma();
			case AdaPackage.NAME_CLASS__CONTROLLED_PRAGMA:
				return getControlledPragma();
			case AdaPackage.NAME_CLASS__CONVENTION_PRAGMA:
				return getConventionPragma();
			case AdaPackage.NAME_CLASS__DISCARD_NAMES_PRAGMA:
				return getDiscardNamesPragma();
			case AdaPackage.NAME_CLASS__ELABORATE_PRAGMA:
				return getElaboratePragma();
			case AdaPackage.NAME_CLASS__ELABORATE_ALL_PRAGMA:
				return getElaborateAllPragma();
			case AdaPackage.NAME_CLASS__ELABORATE_BODY_PRAGMA:
				return getElaborateBodyPragma();
			case AdaPackage.NAME_CLASS__EXPORT_PRAGMA:
				return getExportPragma();
			case AdaPackage.NAME_CLASS__IMPORT_PRAGMA:
				return getImportPragma();
			case AdaPackage.NAME_CLASS__INLINE_PRAGMA:
				return getInlinePragma();
			case AdaPackage.NAME_CLASS__INSPECTION_POINT_PRAGMA:
				return getInspectionPointPragma();
			case AdaPackage.NAME_CLASS__INTERRUPT_HANDLER_PRAGMA:
				return getInterruptHandlerPragma();
			case AdaPackage.NAME_CLASS__INTERRUPT_PRIORITY_PRAGMA:
				return getInterruptPriorityPragma();
			case AdaPackage.NAME_CLASS__LINKER_OPTIONS_PRAGMA:
				return getLinkerOptionsPragma();
			case AdaPackage.NAME_CLASS__LIST_PRAGMA:
				return getListPragma();
			case AdaPackage.NAME_CLASS__LOCKING_POLICY_PRAGMA:
				return getLockingPolicyPragma();
			case AdaPackage.NAME_CLASS__NORMALIZE_SCALARS_PRAGMA:
				return getNormalizeScalarsPragma();
			case AdaPackage.NAME_CLASS__OPTIMIZE_PRAGMA:
				return getOptimizePragma();
			case AdaPackage.NAME_CLASS__PACK_PRAGMA:
				return getPackPragma();
			case AdaPackage.NAME_CLASS__PAGE_PRAGMA:
				return getPagePragma();
			case AdaPackage.NAME_CLASS__PREELABORATE_PRAGMA:
				return getPreelaboratePragma();
			case AdaPackage.NAME_CLASS__PRIORITY_PRAGMA:
				return getPriorityPragma();
			case AdaPackage.NAME_CLASS__PURE_PRAGMA:
				return getPurePragma();
			case AdaPackage.NAME_CLASS__QUEUING_POLICY_PRAGMA:
				return getQueuingPolicyPragma();
			case AdaPackage.NAME_CLASS__REMOTE_CALL_INTERFACE_PRAGMA:
				return getRemoteCallInterfacePragma();
			case AdaPackage.NAME_CLASS__REMOTE_TYPES_PRAGMA:
				return getRemoteTypesPragma();
			case AdaPackage.NAME_CLASS__RESTRICTIONS_PRAGMA:
				return getRestrictionsPragma();
			case AdaPackage.NAME_CLASS__REVIEWABLE_PRAGMA:
				return getReviewablePragma();
			case AdaPackage.NAME_CLASS__SHARED_PASSIVE_PRAGMA:
				return getSharedPassivePragma();
			case AdaPackage.NAME_CLASS__STORAGE_SIZE_PRAGMA:
				return getStorageSizePragma();
			case AdaPackage.NAME_CLASS__SUPPRESS_PRAGMA:
				return getSuppressPragma();
			case AdaPackage.NAME_CLASS__TASK_DISPATCHING_POLICY_PRAGMA:
				return getTaskDispatchingPolicyPragma();
			case AdaPackage.NAME_CLASS__VOLATILE_PRAGMA:
				return getVolatilePragma();
			case AdaPackage.NAME_CLASS__VOLATILE_COMPONENTS_PRAGMA:
				return getVolatileComponentsPragma();
			case AdaPackage.NAME_CLASS__ASSERT_PRAGMA:
				return getAssertPragma();
			case AdaPackage.NAME_CLASS__ASSERTION_POLICY_PRAGMA:
				return getAssertionPolicyPragma();
			case AdaPackage.NAME_CLASS__DETECT_BLOCKING_PRAGMA:
				return getDetectBlockingPragma();
			case AdaPackage.NAME_CLASS__NO_RETURN_PRAGMA:
				return getNoReturnPragma();
			case AdaPackage.NAME_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA:
				return getPartitionElaborationPolicyPragma();
			case AdaPackage.NAME_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA:
				return getPreelaborableInitializationPragma();
			case AdaPackage.NAME_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return getPrioritySpecificDispatchingPragma();
			case AdaPackage.NAME_CLASS__PROFILE_PRAGMA:
				return getProfilePragma();
			case AdaPackage.NAME_CLASS__RELATIVE_DEADLINE_PRAGMA:
				return getRelativeDeadlinePragma();
			case AdaPackage.NAME_CLASS__UNCHECKED_UNION_PRAGMA:
				return getUncheckedUnionPragma();
			case AdaPackage.NAME_CLASS__UNSUPPRESS_PRAGMA:
				return getUnsuppressPragma();
			case AdaPackage.NAME_CLASS__DEFAULT_STORAGE_POOL_PRAGMA:
				return getDefaultStoragePoolPragma();
			case AdaPackage.NAME_CLASS__DISPATCHING_DOMAIN_PRAGMA:
				return getDispatchingDomainPragma();
			case AdaPackage.NAME_CLASS__CPU_PRAGMA:
				return getCpuPragma();
			case AdaPackage.NAME_CLASS__INDEPENDENT_PRAGMA:
				return getIndependentPragma();
			case AdaPackage.NAME_CLASS__INDEPENDENT_COMPONENTS_PRAGMA:
				return getIndependentComponentsPragma();
			case AdaPackage.NAME_CLASS__IMPLEMENTATION_DEFINED_PRAGMA:
				return getImplementationDefinedPragma();
			case AdaPackage.NAME_CLASS__UNKNOWN_PRAGMA:
				return getUnknownPragma();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.NAME_CLASS__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)newValue);
				return;
			case AdaPackage.NAME_CLASS__IDENTIFIER:
				setIdentifier((Identifier)newValue);
				return;
			case AdaPackage.NAME_CLASS__SELECTED_COMPONENT:
				setSelectedComponent((SelectedComponent)newValue);
				return;
			case AdaPackage.NAME_CLASS__ACCESS_ATTRIBUTE:
				setAccessAttribute((AccessAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__ADDRESS_ATTRIBUTE:
				setAddressAttribute((AddressAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__ADJACENT_ATTRIBUTE:
				setAdjacentAttribute((AdjacentAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__AFT_ATTRIBUTE:
				setAftAttribute((AftAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__ALIGNMENT_ATTRIBUTE:
				setAlignmentAttribute((AlignmentAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__BASE_ATTRIBUTE:
				setBaseAttribute((BaseAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__BIT_ORDER_ATTRIBUTE:
				setBitOrderAttribute((BitOrderAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__BODY_VERSION_ATTRIBUTE:
				setBodyVersionAttribute((BodyVersionAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__CALLABLE_ATTRIBUTE:
				setCallableAttribute((CallableAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__CALLER_ATTRIBUTE:
				setCallerAttribute((CallerAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__CEILING_ATTRIBUTE:
				setCeilingAttribute((CeilingAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__CLASS_ATTRIBUTE:
				setClassAttribute((ClassAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__COMPONENT_SIZE_ATTRIBUTE:
				setComponentSizeAttribute((ComponentSizeAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__COMPOSE_ATTRIBUTE:
				setComposeAttribute((ComposeAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__CONSTRAINED_ATTRIBUTE:
				setConstrainedAttribute((ConstrainedAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__COPY_SIGN_ATTRIBUTE:
				setCopySignAttribute((CopySignAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__COUNT_ATTRIBUTE:
				setCountAttribute((CountAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__DEFINITE_ATTRIBUTE:
				setDefiniteAttribute((DefiniteAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__DELTA_ATTRIBUTE:
				setDeltaAttribute((DeltaAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__DENORM_ATTRIBUTE:
				setDenormAttribute((DenormAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__DIGITS_ATTRIBUTE:
				setDigitsAttribute((DigitsAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__EXPONENT_ATTRIBUTE:
				setExponentAttribute((ExponentAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__EXTERNAL_TAG_ATTRIBUTE:
				setExternalTagAttribute((ExternalTagAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__FIRST_ATTRIBUTE:
				setFirstAttribute((FirstAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__FIRST_BIT_ATTRIBUTE:
				setFirstBitAttribute((FirstBitAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__FLOOR_ATTRIBUTE:
				setFloorAttribute((FloorAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__FORE_ATTRIBUTE:
				setForeAttribute((ForeAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__FRACTION_ATTRIBUTE:
				setFractionAttribute((FractionAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__IDENTITY_ATTRIBUTE:
				setIdentityAttribute((IdentityAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__IMAGE_ATTRIBUTE:
				setImageAttribute((ImageAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__INPUT_ATTRIBUTE:
				setInputAttribute((InputAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__LAST_ATTRIBUTE:
				setLastAttribute((LastAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__LAST_BIT_ATTRIBUTE:
				setLastBitAttribute((LastBitAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__LEADING_PART_ATTRIBUTE:
				setLeadingPartAttribute((LeadingPartAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__LENGTH_ATTRIBUTE:
				setLengthAttribute((LengthAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__MACHINE_ATTRIBUTE:
				setMachineAttribute((MachineAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__MACHINE_EMAX_ATTRIBUTE:
				setMachineEmaxAttribute((MachineEmaxAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__MACHINE_EMIN_ATTRIBUTE:
				setMachineEminAttribute((MachineEminAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__MACHINE_MANTISSA_ATTRIBUTE:
				setMachineMantissaAttribute((MachineMantissaAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__MACHINE_OVERFLOWS_ATTRIBUTE:
				setMachineOverflowsAttribute((MachineOverflowsAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__MACHINE_RADIX_ATTRIBUTE:
				setMachineRadixAttribute((MachineRadixAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__MACHINE_ROUNDS_ATTRIBUTE:
				setMachineRoundsAttribute((MachineRoundsAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__MAX_ATTRIBUTE:
				setMaxAttribute((MaxAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE:
				setMaxSizeInStorageElementsAttribute((MaxSizeInStorageElementsAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__MIN_ATTRIBUTE:
				setMinAttribute((MinAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__MODEL_ATTRIBUTE:
				setModelAttribute((ModelAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__MODEL_EMIN_ATTRIBUTE:
				setModelEminAttribute((ModelEminAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__MODEL_EPSILON_ATTRIBUTE:
				setModelEpsilonAttribute((ModelEpsilonAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__MODEL_MANTISSA_ATTRIBUTE:
				setModelMantissaAttribute((ModelMantissaAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__MODEL_SMALL_ATTRIBUTE:
				setModelSmallAttribute((ModelSmallAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__MODULUS_ATTRIBUTE:
				setModulusAttribute((ModulusAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__OUTPUT_ATTRIBUTE:
				setOutputAttribute((OutputAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__PARTITION_ID_ATTRIBUTE:
				setPartitionIdAttribute((PartitionIdAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__POS_ATTRIBUTE:
				setPosAttribute((PosAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__POSITION_ATTRIBUTE:
				setPositionAttribute((PositionAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__PRED_ATTRIBUTE:
				setPredAttribute((PredAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__RANGE_ATTRIBUTE:
				setRangeAttribute((RangeAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__READ_ATTRIBUTE:
				setReadAttribute((ReadAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__REMAINDER_ATTRIBUTE:
				setRemainderAttribute((RemainderAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__ROUND_ATTRIBUTE:
				setRoundAttribute((RoundAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__ROUNDING_ATTRIBUTE:
				setRoundingAttribute((RoundingAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__SAFE_FIRST_ATTRIBUTE:
				setSafeFirstAttribute((SafeFirstAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__SAFE_LAST_ATTRIBUTE:
				setSafeLastAttribute((SafeLastAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__SCALE_ATTRIBUTE:
				setScaleAttribute((ScaleAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__SCALING_ATTRIBUTE:
				setScalingAttribute((ScalingAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__SIGNED_ZEROS_ATTRIBUTE:
				setSignedZerosAttribute((SignedZerosAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__SIZE_ATTRIBUTE:
				setSizeAttribute((SizeAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__SMALL_ATTRIBUTE:
				setSmallAttribute((SmallAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__STORAGE_POOL_ATTRIBUTE:
				setStoragePoolAttribute((StoragePoolAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__STORAGE_SIZE_ATTRIBUTE:
				setStorageSizeAttribute((StorageSizeAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__SUCC_ATTRIBUTE:
				setSuccAttribute((SuccAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__TAG_ATTRIBUTE:
				setTagAttribute((TagAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__TERMINATED_ATTRIBUTE:
				setTerminatedAttribute((TerminatedAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__TRUNCATION_ATTRIBUTE:
				setTruncationAttribute((TruncationAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__UNBIASED_ROUNDING_ATTRIBUTE:
				setUnbiasedRoundingAttribute((UnbiasedRoundingAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__UNCHECKED_ACCESS_ATTRIBUTE:
				setUncheckedAccessAttribute((UncheckedAccessAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__VAL_ATTRIBUTE:
				setValAttribute((ValAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__VALID_ATTRIBUTE:
				setValidAttribute((ValidAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__VALUE_ATTRIBUTE:
				setValueAttribute((ValueAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__VERSION_ATTRIBUTE:
				setVersionAttribute((VersionAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__WIDE_IMAGE_ATTRIBUTE:
				setWideImageAttribute((WideImageAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__WIDE_VALUE_ATTRIBUTE:
				setWideValueAttribute((WideValueAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__WIDE_WIDTH_ATTRIBUTE:
				setWideWidthAttribute((WideWidthAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__WIDTH_ATTRIBUTE:
				setWidthAttribute((WidthAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__WRITE_ATTRIBUTE:
				setWriteAttribute((WriteAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__MACHINE_ROUNDING_ATTRIBUTE:
				setMachineRoundingAttribute((MachineRoundingAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__MOD_ATTRIBUTE:
				setModAttribute((ModAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__PRIORITY_ATTRIBUTE:
				setPriorityAttribute((PriorityAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__STREAM_SIZE_ATTRIBUTE:
				setStreamSizeAttribute((StreamSizeAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__WIDE_WIDE_IMAGE_ATTRIBUTE:
				setWideWideImageAttribute((WideWideImageAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__WIDE_WIDE_VALUE_ATTRIBUTE:
				setWideWideValueAttribute((WideWideValueAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__WIDE_WIDE_WIDTH_ATTRIBUTE:
				setWideWideWidthAttribute((WideWideWidthAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE:
				setMaxAlignmentForAllocationAttribute((MaxAlignmentForAllocationAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__OVERLAPS_STORAGE_ATTRIBUTE:
				setOverlapsStorageAttribute((OverlapsStorageAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__IMPLEMENTATION_DEFINED_ATTRIBUTE:
				setImplementationDefinedAttribute((ImplementationDefinedAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__UNKNOWN_ATTRIBUTE:
				setUnknownAttribute((UnknownAttribute)newValue);
				return;
			case AdaPackage.NAME_CLASS__COMMENT:
				setComment((Comment)newValue);
				return;
			case AdaPackage.NAME_CLASS__ALL_CALLS_REMOTE_PRAGMA:
				setAllCallsRemotePragma((AllCallsRemotePragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__ASYNCHRONOUS_PRAGMA:
				setAsynchronousPragma((AsynchronousPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__ATOMIC_PRAGMA:
				setAtomicPragma((AtomicPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__ATOMIC_COMPONENTS_PRAGMA:
				setAtomicComponentsPragma((AtomicComponentsPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__ATTACH_HANDLER_PRAGMA:
				setAttachHandlerPragma((AttachHandlerPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__CONTROLLED_PRAGMA:
				setControlledPragma((ControlledPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__CONVENTION_PRAGMA:
				setConventionPragma((ConventionPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__DISCARD_NAMES_PRAGMA:
				setDiscardNamesPragma((DiscardNamesPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__ELABORATE_PRAGMA:
				setElaboratePragma((ElaboratePragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__ELABORATE_ALL_PRAGMA:
				setElaborateAllPragma((ElaborateAllPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__ELABORATE_BODY_PRAGMA:
				setElaborateBodyPragma((ElaborateBodyPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__EXPORT_PRAGMA:
				setExportPragma((ExportPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__IMPORT_PRAGMA:
				setImportPragma((ImportPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__INLINE_PRAGMA:
				setInlinePragma((InlinePragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__INSPECTION_POINT_PRAGMA:
				setInspectionPointPragma((InspectionPointPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__INTERRUPT_HANDLER_PRAGMA:
				setInterruptHandlerPragma((InterruptHandlerPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__INTERRUPT_PRIORITY_PRAGMA:
				setInterruptPriorityPragma((InterruptPriorityPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__LINKER_OPTIONS_PRAGMA:
				setLinkerOptionsPragma((LinkerOptionsPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__LIST_PRAGMA:
				setListPragma((ListPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__LOCKING_POLICY_PRAGMA:
				setLockingPolicyPragma((LockingPolicyPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__NORMALIZE_SCALARS_PRAGMA:
				setNormalizeScalarsPragma((NormalizeScalarsPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__OPTIMIZE_PRAGMA:
				setOptimizePragma((OptimizePragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__PACK_PRAGMA:
				setPackPragma((PackPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__PAGE_PRAGMA:
				setPagePragma((PagePragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__PREELABORATE_PRAGMA:
				setPreelaboratePragma((PreelaboratePragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__PRIORITY_PRAGMA:
				setPriorityPragma((PriorityPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__PURE_PRAGMA:
				setPurePragma((PurePragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__QUEUING_POLICY_PRAGMA:
				setQueuingPolicyPragma((QueuingPolicyPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__REMOTE_CALL_INTERFACE_PRAGMA:
				setRemoteCallInterfacePragma((RemoteCallInterfacePragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__REMOTE_TYPES_PRAGMA:
				setRemoteTypesPragma((RemoteTypesPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__RESTRICTIONS_PRAGMA:
				setRestrictionsPragma((RestrictionsPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__REVIEWABLE_PRAGMA:
				setReviewablePragma((ReviewablePragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__SHARED_PASSIVE_PRAGMA:
				setSharedPassivePragma((SharedPassivePragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__STORAGE_SIZE_PRAGMA:
				setStorageSizePragma((StorageSizePragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__SUPPRESS_PRAGMA:
				setSuppressPragma((SuppressPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__TASK_DISPATCHING_POLICY_PRAGMA:
				setTaskDispatchingPolicyPragma((TaskDispatchingPolicyPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__VOLATILE_PRAGMA:
				setVolatilePragma((VolatilePragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__VOLATILE_COMPONENTS_PRAGMA:
				setVolatileComponentsPragma((VolatileComponentsPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__ASSERT_PRAGMA:
				setAssertPragma((AssertPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__ASSERTION_POLICY_PRAGMA:
				setAssertionPolicyPragma((AssertionPolicyPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__DETECT_BLOCKING_PRAGMA:
				setDetectBlockingPragma((DetectBlockingPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__NO_RETURN_PRAGMA:
				setNoReturnPragma((NoReturnPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA:
				setPartitionElaborationPolicyPragma((PartitionElaborationPolicyPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA:
				setPreelaborableInitializationPragma((PreelaborableInitializationPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				setPrioritySpecificDispatchingPragma((PrioritySpecificDispatchingPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__PROFILE_PRAGMA:
				setProfilePragma((ProfilePragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__RELATIVE_DEADLINE_PRAGMA:
				setRelativeDeadlinePragma((RelativeDeadlinePragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__UNCHECKED_UNION_PRAGMA:
				setUncheckedUnionPragma((UncheckedUnionPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__UNSUPPRESS_PRAGMA:
				setUnsuppressPragma((UnsuppressPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__DEFAULT_STORAGE_POOL_PRAGMA:
				setDefaultStoragePoolPragma((DefaultStoragePoolPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__DISPATCHING_DOMAIN_PRAGMA:
				setDispatchingDomainPragma((DispatchingDomainPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__CPU_PRAGMA:
				setCpuPragma((CpuPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__INDEPENDENT_PRAGMA:
				setIndependentPragma((IndependentPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__INDEPENDENT_COMPONENTS_PRAGMA:
				setIndependentComponentsPragma((IndependentComponentsPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__IMPLEMENTATION_DEFINED_PRAGMA:
				setImplementationDefinedPragma((ImplementationDefinedPragma)newValue);
				return;
			case AdaPackage.NAME_CLASS__UNKNOWN_PRAGMA:
				setUnknownPragma((UnknownPragma)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.NAME_CLASS__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)null);
				return;
			case AdaPackage.NAME_CLASS__IDENTIFIER:
				setIdentifier((Identifier)null);
				return;
			case AdaPackage.NAME_CLASS__SELECTED_COMPONENT:
				setSelectedComponent((SelectedComponent)null);
				return;
			case AdaPackage.NAME_CLASS__ACCESS_ATTRIBUTE:
				setAccessAttribute((AccessAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__ADDRESS_ATTRIBUTE:
				setAddressAttribute((AddressAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__ADJACENT_ATTRIBUTE:
				setAdjacentAttribute((AdjacentAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__AFT_ATTRIBUTE:
				setAftAttribute((AftAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__ALIGNMENT_ATTRIBUTE:
				setAlignmentAttribute((AlignmentAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__BASE_ATTRIBUTE:
				setBaseAttribute((BaseAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__BIT_ORDER_ATTRIBUTE:
				setBitOrderAttribute((BitOrderAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__BODY_VERSION_ATTRIBUTE:
				setBodyVersionAttribute((BodyVersionAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__CALLABLE_ATTRIBUTE:
				setCallableAttribute((CallableAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__CALLER_ATTRIBUTE:
				setCallerAttribute((CallerAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__CEILING_ATTRIBUTE:
				setCeilingAttribute((CeilingAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__CLASS_ATTRIBUTE:
				setClassAttribute((ClassAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__COMPONENT_SIZE_ATTRIBUTE:
				setComponentSizeAttribute((ComponentSizeAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__COMPOSE_ATTRIBUTE:
				setComposeAttribute((ComposeAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__CONSTRAINED_ATTRIBUTE:
				setConstrainedAttribute((ConstrainedAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__COPY_SIGN_ATTRIBUTE:
				setCopySignAttribute((CopySignAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__COUNT_ATTRIBUTE:
				setCountAttribute((CountAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__DEFINITE_ATTRIBUTE:
				setDefiniteAttribute((DefiniteAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__DELTA_ATTRIBUTE:
				setDeltaAttribute((DeltaAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__DENORM_ATTRIBUTE:
				setDenormAttribute((DenormAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__DIGITS_ATTRIBUTE:
				setDigitsAttribute((DigitsAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__EXPONENT_ATTRIBUTE:
				setExponentAttribute((ExponentAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__EXTERNAL_TAG_ATTRIBUTE:
				setExternalTagAttribute((ExternalTagAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__FIRST_ATTRIBUTE:
				setFirstAttribute((FirstAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__FIRST_BIT_ATTRIBUTE:
				setFirstBitAttribute((FirstBitAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__FLOOR_ATTRIBUTE:
				setFloorAttribute((FloorAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__FORE_ATTRIBUTE:
				setForeAttribute((ForeAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__FRACTION_ATTRIBUTE:
				setFractionAttribute((FractionAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__IDENTITY_ATTRIBUTE:
				setIdentityAttribute((IdentityAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__IMAGE_ATTRIBUTE:
				setImageAttribute((ImageAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__INPUT_ATTRIBUTE:
				setInputAttribute((InputAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__LAST_ATTRIBUTE:
				setLastAttribute((LastAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__LAST_BIT_ATTRIBUTE:
				setLastBitAttribute((LastBitAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__LEADING_PART_ATTRIBUTE:
				setLeadingPartAttribute((LeadingPartAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__LENGTH_ATTRIBUTE:
				setLengthAttribute((LengthAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__MACHINE_ATTRIBUTE:
				setMachineAttribute((MachineAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__MACHINE_EMAX_ATTRIBUTE:
				setMachineEmaxAttribute((MachineEmaxAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__MACHINE_EMIN_ATTRIBUTE:
				setMachineEminAttribute((MachineEminAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__MACHINE_MANTISSA_ATTRIBUTE:
				setMachineMantissaAttribute((MachineMantissaAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__MACHINE_OVERFLOWS_ATTRIBUTE:
				setMachineOverflowsAttribute((MachineOverflowsAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__MACHINE_RADIX_ATTRIBUTE:
				setMachineRadixAttribute((MachineRadixAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__MACHINE_ROUNDS_ATTRIBUTE:
				setMachineRoundsAttribute((MachineRoundsAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__MAX_ATTRIBUTE:
				setMaxAttribute((MaxAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE:
				setMaxSizeInStorageElementsAttribute((MaxSizeInStorageElementsAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__MIN_ATTRIBUTE:
				setMinAttribute((MinAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__MODEL_ATTRIBUTE:
				setModelAttribute((ModelAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__MODEL_EMIN_ATTRIBUTE:
				setModelEminAttribute((ModelEminAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__MODEL_EPSILON_ATTRIBUTE:
				setModelEpsilonAttribute((ModelEpsilonAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__MODEL_MANTISSA_ATTRIBUTE:
				setModelMantissaAttribute((ModelMantissaAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__MODEL_SMALL_ATTRIBUTE:
				setModelSmallAttribute((ModelSmallAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__MODULUS_ATTRIBUTE:
				setModulusAttribute((ModulusAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__OUTPUT_ATTRIBUTE:
				setOutputAttribute((OutputAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__PARTITION_ID_ATTRIBUTE:
				setPartitionIdAttribute((PartitionIdAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__POS_ATTRIBUTE:
				setPosAttribute((PosAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__POSITION_ATTRIBUTE:
				setPositionAttribute((PositionAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__PRED_ATTRIBUTE:
				setPredAttribute((PredAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__RANGE_ATTRIBUTE:
				setRangeAttribute((RangeAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__READ_ATTRIBUTE:
				setReadAttribute((ReadAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__REMAINDER_ATTRIBUTE:
				setRemainderAttribute((RemainderAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__ROUND_ATTRIBUTE:
				setRoundAttribute((RoundAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__ROUNDING_ATTRIBUTE:
				setRoundingAttribute((RoundingAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__SAFE_FIRST_ATTRIBUTE:
				setSafeFirstAttribute((SafeFirstAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__SAFE_LAST_ATTRIBUTE:
				setSafeLastAttribute((SafeLastAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__SCALE_ATTRIBUTE:
				setScaleAttribute((ScaleAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__SCALING_ATTRIBUTE:
				setScalingAttribute((ScalingAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__SIGNED_ZEROS_ATTRIBUTE:
				setSignedZerosAttribute((SignedZerosAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__SIZE_ATTRIBUTE:
				setSizeAttribute((SizeAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__SMALL_ATTRIBUTE:
				setSmallAttribute((SmallAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__STORAGE_POOL_ATTRIBUTE:
				setStoragePoolAttribute((StoragePoolAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__STORAGE_SIZE_ATTRIBUTE:
				setStorageSizeAttribute((StorageSizeAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__SUCC_ATTRIBUTE:
				setSuccAttribute((SuccAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__TAG_ATTRIBUTE:
				setTagAttribute((TagAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__TERMINATED_ATTRIBUTE:
				setTerminatedAttribute((TerminatedAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__TRUNCATION_ATTRIBUTE:
				setTruncationAttribute((TruncationAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__UNBIASED_ROUNDING_ATTRIBUTE:
				setUnbiasedRoundingAttribute((UnbiasedRoundingAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__UNCHECKED_ACCESS_ATTRIBUTE:
				setUncheckedAccessAttribute((UncheckedAccessAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__VAL_ATTRIBUTE:
				setValAttribute((ValAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__VALID_ATTRIBUTE:
				setValidAttribute((ValidAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__VALUE_ATTRIBUTE:
				setValueAttribute((ValueAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__VERSION_ATTRIBUTE:
				setVersionAttribute((VersionAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__WIDE_IMAGE_ATTRIBUTE:
				setWideImageAttribute((WideImageAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__WIDE_VALUE_ATTRIBUTE:
				setWideValueAttribute((WideValueAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__WIDE_WIDTH_ATTRIBUTE:
				setWideWidthAttribute((WideWidthAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__WIDTH_ATTRIBUTE:
				setWidthAttribute((WidthAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__WRITE_ATTRIBUTE:
				setWriteAttribute((WriteAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__MACHINE_ROUNDING_ATTRIBUTE:
				setMachineRoundingAttribute((MachineRoundingAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__MOD_ATTRIBUTE:
				setModAttribute((ModAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__PRIORITY_ATTRIBUTE:
				setPriorityAttribute((PriorityAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__STREAM_SIZE_ATTRIBUTE:
				setStreamSizeAttribute((StreamSizeAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__WIDE_WIDE_IMAGE_ATTRIBUTE:
				setWideWideImageAttribute((WideWideImageAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__WIDE_WIDE_VALUE_ATTRIBUTE:
				setWideWideValueAttribute((WideWideValueAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__WIDE_WIDE_WIDTH_ATTRIBUTE:
				setWideWideWidthAttribute((WideWideWidthAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE:
				setMaxAlignmentForAllocationAttribute((MaxAlignmentForAllocationAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__OVERLAPS_STORAGE_ATTRIBUTE:
				setOverlapsStorageAttribute((OverlapsStorageAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__IMPLEMENTATION_DEFINED_ATTRIBUTE:
				setImplementationDefinedAttribute((ImplementationDefinedAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__UNKNOWN_ATTRIBUTE:
				setUnknownAttribute((UnknownAttribute)null);
				return;
			case AdaPackage.NAME_CLASS__COMMENT:
				setComment((Comment)null);
				return;
			case AdaPackage.NAME_CLASS__ALL_CALLS_REMOTE_PRAGMA:
				setAllCallsRemotePragma((AllCallsRemotePragma)null);
				return;
			case AdaPackage.NAME_CLASS__ASYNCHRONOUS_PRAGMA:
				setAsynchronousPragma((AsynchronousPragma)null);
				return;
			case AdaPackage.NAME_CLASS__ATOMIC_PRAGMA:
				setAtomicPragma((AtomicPragma)null);
				return;
			case AdaPackage.NAME_CLASS__ATOMIC_COMPONENTS_PRAGMA:
				setAtomicComponentsPragma((AtomicComponentsPragma)null);
				return;
			case AdaPackage.NAME_CLASS__ATTACH_HANDLER_PRAGMA:
				setAttachHandlerPragma((AttachHandlerPragma)null);
				return;
			case AdaPackage.NAME_CLASS__CONTROLLED_PRAGMA:
				setControlledPragma((ControlledPragma)null);
				return;
			case AdaPackage.NAME_CLASS__CONVENTION_PRAGMA:
				setConventionPragma((ConventionPragma)null);
				return;
			case AdaPackage.NAME_CLASS__DISCARD_NAMES_PRAGMA:
				setDiscardNamesPragma((DiscardNamesPragma)null);
				return;
			case AdaPackage.NAME_CLASS__ELABORATE_PRAGMA:
				setElaboratePragma((ElaboratePragma)null);
				return;
			case AdaPackage.NAME_CLASS__ELABORATE_ALL_PRAGMA:
				setElaborateAllPragma((ElaborateAllPragma)null);
				return;
			case AdaPackage.NAME_CLASS__ELABORATE_BODY_PRAGMA:
				setElaborateBodyPragma((ElaborateBodyPragma)null);
				return;
			case AdaPackage.NAME_CLASS__EXPORT_PRAGMA:
				setExportPragma((ExportPragma)null);
				return;
			case AdaPackage.NAME_CLASS__IMPORT_PRAGMA:
				setImportPragma((ImportPragma)null);
				return;
			case AdaPackage.NAME_CLASS__INLINE_PRAGMA:
				setInlinePragma((InlinePragma)null);
				return;
			case AdaPackage.NAME_CLASS__INSPECTION_POINT_PRAGMA:
				setInspectionPointPragma((InspectionPointPragma)null);
				return;
			case AdaPackage.NAME_CLASS__INTERRUPT_HANDLER_PRAGMA:
				setInterruptHandlerPragma((InterruptHandlerPragma)null);
				return;
			case AdaPackage.NAME_CLASS__INTERRUPT_PRIORITY_PRAGMA:
				setInterruptPriorityPragma((InterruptPriorityPragma)null);
				return;
			case AdaPackage.NAME_CLASS__LINKER_OPTIONS_PRAGMA:
				setLinkerOptionsPragma((LinkerOptionsPragma)null);
				return;
			case AdaPackage.NAME_CLASS__LIST_PRAGMA:
				setListPragma((ListPragma)null);
				return;
			case AdaPackage.NAME_CLASS__LOCKING_POLICY_PRAGMA:
				setLockingPolicyPragma((LockingPolicyPragma)null);
				return;
			case AdaPackage.NAME_CLASS__NORMALIZE_SCALARS_PRAGMA:
				setNormalizeScalarsPragma((NormalizeScalarsPragma)null);
				return;
			case AdaPackage.NAME_CLASS__OPTIMIZE_PRAGMA:
				setOptimizePragma((OptimizePragma)null);
				return;
			case AdaPackage.NAME_CLASS__PACK_PRAGMA:
				setPackPragma((PackPragma)null);
				return;
			case AdaPackage.NAME_CLASS__PAGE_PRAGMA:
				setPagePragma((PagePragma)null);
				return;
			case AdaPackage.NAME_CLASS__PREELABORATE_PRAGMA:
				setPreelaboratePragma((PreelaboratePragma)null);
				return;
			case AdaPackage.NAME_CLASS__PRIORITY_PRAGMA:
				setPriorityPragma((PriorityPragma)null);
				return;
			case AdaPackage.NAME_CLASS__PURE_PRAGMA:
				setPurePragma((PurePragma)null);
				return;
			case AdaPackage.NAME_CLASS__QUEUING_POLICY_PRAGMA:
				setQueuingPolicyPragma((QueuingPolicyPragma)null);
				return;
			case AdaPackage.NAME_CLASS__REMOTE_CALL_INTERFACE_PRAGMA:
				setRemoteCallInterfacePragma((RemoteCallInterfacePragma)null);
				return;
			case AdaPackage.NAME_CLASS__REMOTE_TYPES_PRAGMA:
				setRemoteTypesPragma((RemoteTypesPragma)null);
				return;
			case AdaPackage.NAME_CLASS__RESTRICTIONS_PRAGMA:
				setRestrictionsPragma((RestrictionsPragma)null);
				return;
			case AdaPackage.NAME_CLASS__REVIEWABLE_PRAGMA:
				setReviewablePragma((ReviewablePragma)null);
				return;
			case AdaPackage.NAME_CLASS__SHARED_PASSIVE_PRAGMA:
				setSharedPassivePragma((SharedPassivePragma)null);
				return;
			case AdaPackage.NAME_CLASS__STORAGE_SIZE_PRAGMA:
				setStorageSizePragma((StorageSizePragma)null);
				return;
			case AdaPackage.NAME_CLASS__SUPPRESS_PRAGMA:
				setSuppressPragma((SuppressPragma)null);
				return;
			case AdaPackage.NAME_CLASS__TASK_DISPATCHING_POLICY_PRAGMA:
				setTaskDispatchingPolicyPragma((TaskDispatchingPolicyPragma)null);
				return;
			case AdaPackage.NAME_CLASS__VOLATILE_PRAGMA:
				setVolatilePragma((VolatilePragma)null);
				return;
			case AdaPackage.NAME_CLASS__VOLATILE_COMPONENTS_PRAGMA:
				setVolatileComponentsPragma((VolatileComponentsPragma)null);
				return;
			case AdaPackage.NAME_CLASS__ASSERT_PRAGMA:
				setAssertPragma((AssertPragma)null);
				return;
			case AdaPackage.NAME_CLASS__ASSERTION_POLICY_PRAGMA:
				setAssertionPolicyPragma((AssertionPolicyPragma)null);
				return;
			case AdaPackage.NAME_CLASS__DETECT_BLOCKING_PRAGMA:
				setDetectBlockingPragma((DetectBlockingPragma)null);
				return;
			case AdaPackage.NAME_CLASS__NO_RETURN_PRAGMA:
				setNoReturnPragma((NoReturnPragma)null);
				return;
			case AdaPackage.NAME_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA:
				setPartitionElaborationPolicyPragma((PartitionElaborationPolicyPragma)null);
				return;
			case AdaPackage.NAME_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA:
				setPreelaborableInitializationPragma((PreelaborableInitializationPragma)null);
				return;
			case AdaPackage.NAME_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				setPrioritySpecificDispatchingPragma((PrioritySpecificDispatchingPragma)null);
				return;
			case AdaPackage.NAME_CLASS__PROFILE_PRAGMA:
				setProfilePragma((ProfilePragma)null);
				return;
			case AdaPackage.NAME_CLASS__RELATIVE_DEADLINE_PRAGMA:
				setRelativeDeadlinePragma((RelativeDeadlinePragma)null);
				return;
			case AdaPackage.NAME_CLASS__UNCHECKED_UNION_PRAGMA:
				setUncheckedUnionPragma((UncheckedUnionPragma)null);
				return;
			case AdaPackage.NAME_CLASS__UNSUPPRESS_PRAGMA:
				setUnsuppressPragma((UnsuppressPragma)null);
				return;
			case AdaPackage.NAME_CLASS__DEFAULT_STORAGE_POOL_PRAGMA:
				setDefaultStoragePoolPragma((DefaultStoragePoolPragma)null);
				return;
			case AdaPackage.NAME_CLASS__DISPATCHING_DOMAIN_PRAGMA:
				setDispatchingDomainPragma((DispatchingDomainPragma)null);
				return;
			case AdaPackage.NAME_CLASS__CPU_PRAGMA:
				setCpuPragma((CpuPragma)null);
				return;
			case AdaPackage.NAME_CLASS__INDEPENDENT_PRAGMA:
				setIndependentPragma((IndependentPragma)null);
				return;
			case AdaPackage.NAME_CLASS__INDEPENDENT_COMPONENTS_PRAGMA:
				setIndependentComponentsPragma((IndependentComponentsPragma)null);
				return;
			case AdaPackage.NAME_CLASS__IMPLEMENTATION_DEFINED_PRAGMA:
				setImplementationDefinedPragma((ImplementationDefinedPragma)null);
				return;
			case AdaPackage.NAME_CLASS__UNKNOWN_PRAGMA:
				setUnknownPragma((UnknownPragma)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.NAME_CLASS__NOT_AN_ELEMENT:
				return notAnElement != null;
			case AdaPackage.NAME_CLASS__IDENTIFIER:
				return identifier != null;
			case AdaPackage.NAME_CLASS__SELECTED_COMPONENT:
				return selectedComponent != null;
			case AdaPackage.NAME_CLASS__ACCESS_ATTRIBUTE:
				return accessAttribute != null;
			case AdaPackage.NAME_CLASS__ADDRESS_ATTRIBUTE:
				return addressAttribute != null;
			case AdaPackage.NAME_CLASS__ADJACENT_ATTRIBUTE:
				return adjacentAttribute != null;
			case AdaPackage.NAME_CLASS__AFT_ATTRIBUTE:
				return aftAttribute != null;
			case AdaPackage.NAME_CLASS__ALIGNMENT_ATTRIBUTE:
				return alignmentAttribute != null;
			case AdaPackage.NAME_CLASS__BASE_ATTRIBUTE:
				return baseAttribute != null;
			case AdaPackage.NAME_CLASS__BIT_ORDER_ATTRIBUTE:
				return bitOrderAttribute != null;
			case AdaPackage.NAME_CLASS__BODY_VERSION_ATTRIBUTE:
				return bodyVersionAttribute != null;
			case AdaPackage.NAME_CLASS__CALLABLE_ATTRIBUTE:
				return callableAttribute != null;
			case AdaPackage.NAME_CLASS__CALLER_ATTRIBUTE:
				return callerAttribute != null;
			case AdaPackage.NAME_CLASS__CEILING_ATTRIBUTE:
				return ceilingAttribute != null;
			case AdaPackage.NAME_CLASS__CLASS_ATTRIBUTE:
				return classAttribute != null;
			case AdaPackage.NAME_CLASS__COMPONENT_SIZE_ATTRIBUTE:
				return componentSizeAttribute != null;
			case AdaPackage.NAME_CLASS__COMPOSE_ATTRIBUTE:
				return composeAttribute != null;
			case AdaPackage.NAME_CLASS__CONSTRAINED_ATTRIBUTE:
				return constrainedAttribute != null;
			case AdaPackage.NAME_CLASS__COPY_SIGN_ATTRIBUTE:
				return copySignAttribute != null;
			case AdaPackage.NAME_CLASS__COUNT_ATTRIBUTE:
				return countAttribute != null;
			case AdaPackage.NAME_CLASS__DEFINITE_ATTRIBUTE:
				return definiteAttribute != null;
			case AdaPackage.NAME_CLASS__DELTA_ATTRIBUTE:
				return deltaAttribute != null;
			case AdaPackage.NAME_CLASS__DENORM_ATTRIBUTE:
				return denormAttribute != null;
			case AdaPackage.NAME_CLASS__DIGITS_ATTRIBUTE:
				return digitsAttribute != null;
			case AdaPackage.NAME_CLASS__EXPONENT_ATTRIBUTE:
				return exponentAttribute != null;
			case AdaPackage.NAME_CLASS__EXTERNAL_TAG_ATTRIBUTE:
				return externalTagAttribute != null;
			case AdaPackage.NAME_CLASS__FIRST_ATTRIBUTE:
				return firstAttribute != null;
			case AdaPackage.NAME_CLASS__FIRST_BIT_ATTRIBUTE:
				return firstBitAttribute != null;
			case AdaPackage.NAME_CLASS__FLOOR_ATTRIBUTE:
				return floorAttribute != null;
			case AdaPackage.NAME_CLASS__FORE_ATTRIBUTE:
				return foreAttribute != null;
			case AdaPackage.NAME_CLASS__FRACTION_ATTRIBUTE:
				return fractionAttribute != null;
			case AdaPackage.NAME_CLASS__IDENTITY_ATTRIBUTE:
				return identityAttribute != null;
			case AdaPackage.NAME_CLASS__IMAGE_ATTRIBUTE:
				return imageAttribute != null;
			case AdaPackage.NAME_CLASS__INPUT_ATTRIBUTE:
				return inputAttribute != null;
			case AdaPackage.NAME_CLASS__LAST_ATTRIBUTE:
				return lastAttribute != null;
			case AdaPackage.NAME_CLASS__LAST_BIT_ATTRIBUTE:
				return lastBitAttribute != null;
			case AdaPackage.NAME_CLASS__LEADING_PART_ATTRIBUTE:
				return leadingPartAttribute != null;
			case AdaPackage.NAME_CLASS__LENGTH_ATTRIBUTE:
				return lengthAttribute != null;
			case AdaPackage.NAME_CLASS__MACHINE_ATTRIBUTE:
				return machineAttribute != null;
			case AdaPackage.NAME_CLASS__MACHINE_EMAX_ATTRIBUTE:
				return machineEmaxAttribute != null;
			case AdaPackage.NAME_CLASS__MACHINE_EMIN_ATTRIBUTE:
				return machineEminAttribute != null;
			case AdaPackage.NAME_CLASS__MACHINE_MANTISSA_ATTRIBUTE:
				return machineMantissaAttribute != null;
			case AdaPackage.NAME_CLASS__MACHINE_OVERFLOWS_ATTRIBUTE:
				return machineOverflowsAttribute != null;
			case AdaPackage.NAME_CLASS__MACHINE_RADIX_ATTRIBUTE:
				return machineRadixAttribute != null;
			case AdaPackage.NAME_CLASS__MACHINE_ROUNDS_ATTRIBUTE:
				return machineRoundsAttribute != null;
			case AdaPackage.NAME_CLASS__MAX_ATTRIBUTE:
				return maxAttribute != null;
			case AdaPackage.NAME_CLASS__MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE:
				return maxSizeInStorageElementsAttribute != null;
			case AdaPackage.NAME_CLASS__MIN_ATTRIBUTE:
				return minAttribute != null;
			case AdaPackage.NAME_CLASS__MODEL_ATTRIBUTE:
				return modelAttribute != null;
			case AdaPackage.NAME_CLASS__MODEL_EMIN_ATTRIBUTE:
				return modelEminAttribute != null;
			case AdaPackage.NAME_CLASS__MODEL_EPSILON_ATTRIBUTE:
				return modelEpsilonAttribute != null;
			case AdaPackage.NAME_CLASS__MODEL_MANTISSA_ATTRIBUTE:
				return modelMantissaAttribute != null;
			case AdaPackage.NAME_CLASS__MODEL_SMALL_ATTRIBUTE:
				return modelSmallAttribute != null;
			case AdaPackage.NAME_CLASS__MODULUS_ATTRIBUTE:
				return modulusAttribute != null;
			case AdaPackage.NAME_CLASS__OUTPUT_ATTRIBUTE:
				return outputAttribute != null;
			case AdaPackage.NAME_CLASS__PARTITION_ID_ATTRIBUTE:
				return partitionIdAttribute != null;
			case AdaPackage.NAME_CLASS__POS_ATTRIBUTE:
				return posAttribute != null;
			case AdaPackage.NAME_CLASS__POSITION_ATTRIBUTE:
				return positionAttribute != null;
			case AdaPackage.NAME_CLASS__PRED_ATTRIBUTE:
				return predAttribute != null;
			case AdaPackage.NAME_CLASS__RANGE_ATTRIBUTE:
				return rangeAttribute != null;
			case AdaPackage.NAME_CLASS__READ_ATTRIBUTE:
				return readAttribute != null;
			case AdaPackage.NAME_CLASS__REMAINDER_ATTRIBUTE:
				return remainderAttribute != null;
			case AdaPackage.NAME_CLASS__ROUND_ATTRIBUTE:
				return roundAttribute != null;
			case AdaPackage.NAME_CLASS__ROUNDING_ATTRIBUTE:
				return roundingAttribute != null;
			case AdaPackage.NAME_CLASS__SAFE_FIRST_ATTRIBUTE:
				return safeFirstAttribute != null;
			case AdaPackage.NAME_CLASS__SAFE_LAST_ATTRIBUTE:
				return safeLastAttribute != null;
			case AdaPackage.NAME_CLASS__SCALE_ATTRIBUTE:
				return scaleAttribute != null;
			case AdaPackage.NAME_CLASS__SCALING_ATTRIBUTE:
				return scalingAttribute != null;
			case AdaPackage.NAME_CLASS__SIGNED_ZEROS_ATTRIBUTE:
				return signedZerosAttribute != null;
			case AdaPackage.NAME_CLASS__SIZE_ATTRIBUTE:
				return sizeAttribute != null;
			case AdaPackage.NAME_CLASS__SMALL_ATTRIBUTE:
				return smallAttribute != null;
			case AdaPackage.NAME_CLASS__STORAGE_POOL_ATTRIBUTE:
				return storagePoolAttribute != null;
			case AdaPackage.NAME_CLASS__STORAGE_SIZE_ATTRIBUTE:
				return storageSizeAttribute != null;
			case AdaPackage.NAME_CLASS__SUCC_ATTRIBUTE:
				return succAttribute != null;
			case AdaPackage.NAME_CLASS__TAG_ATTRIBUTE:
				return tagAttribute != null;
			case AdaPackage.NAME_CLASS__TERMINATED_ATTRIBUTE:
				return terminatedAttribute != null;
			case AdaPackage.NAME_CLASS__TRUNCATION_ATTRIBUTE:
				return truncationAttribute != null;
			case AdaPackage.NAME_CLASS__UNBIASED_ROUNDING_ATTRIBUTE:
				return unbiasedRoundingAttribute != null;
			case AdaPackage.NAME_CLASS__UNCHECKED_ACCESS_ATTRIBUTE:
				return uncheckedAccessAttribute != null;
			case AdaPackage.NAME_CLASS__VAL_ATTRIBUTE:
				return valAttribute != null;
			case AdaPackage.NAME_CLASS__VALID_ATTRIBUTE:
				return validAttribute != null;
			case AdaPackage.NAME_CLASS__VALUE_ATTRIBUTE:
				return valueAttribute != null;
			case AdaPackage.NAME_CLASS__VERSION_ATTRIBUTE:
				return versionAttribute != null;
			case AdaPackage.NAME_CLASS__WIDE_IMAGE_ATTRIBUTE:
				return wideImageAttribute != null;
			case AdaPackage.NAME_CLASS__WIDE_VALUE_ATTRIBUTE:
				return wideValueAttribute != null;
			case AdaPackage.NAME_CLASS__WIDE_WIDTH_ATTRIBUTE:
				return wideWidthAttribute != null;
			case AdaPackage.NAME_CLASS__WIDTH_ATTRIBUTE:
				return widthAttribute != null;
			case AdaPackage.NAME_CLASS__WRITE_ATTRIBUTE:
				return writeAttribute != null;
			case AdaPackage.NAME_CLASS__MACHINE_ROUNDING_ATTRIBUTE:
				return machineRoundingAttribute != null;
			case AdaPackage.NAME_CLASS__MOD_ATTRIBUTE:
				return modAttribute != null;
			case AdaPackage.NAME_CLASS__PRIORITY_ATTRIBUTE:
				return priorityAttribute != null;
			case AdaPackage.NAME_CLASS__STREAM_SIZE_ATTRIBUTE:
				return streamSizeAttribute != null;
			case AdaPackage.NAME_CLASS__WIDE_WIDE_IMAGE_ATTRIBUTE:
				return wideWideImageAttribute != null;
			case AdaPackage.NAME_CLASS__WIDE_WIDE_VALUE_ATTRIBUTE:
				return wideWideValueAttribute != null;
			case AdaPackage.NAME_CLASS__WIDE_WIDE_WIDTH_ATTRIBUTE:
				return wideWideWidthAttribute != null;
			case AdaPackage.NAME_CLASS__MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE:
				return maxAlignmentForAllocationAttribute != null;
			case AdaPackage.NAME_CLASS__OVERLAPS_STORAGE_ATTRIBUTE:
				return overlapsStorageAttribute != null;
			case AdaPackage.NAME_CLASS__IMPLEMENTATION_DEFINED_ATTRIBUTE:
				return implementationDefinedAttribute != null;
			case AdaPackage.NAME_CLASS__UNKNOWN_ATTRIBUTE:
				return unknownAttribute != null;
			case AdaPackage.NAME_CLASS__COMMENT:
				return comment != null;
			case AdaPackage.NAME_CLASS__ALL_CALLS_REMOTE_PRAGMA:
				return allCallsRemotePragma != null;
			case AdaPackage.NAME_CLASS__ASYNCHRONOUS_PRAGMA:
				return asynchronousPragma != null;
			case AdaPackage.NAME_CLASS__ATOMIC_PRAGMA:
				return atomicPragma != null;
			case AdaPackage.NAME_CLASS__ATOMIC_COMPONENTS_PRAGMA:
				return atomicComponentsPragma != null;
			case AdaPackage.NAME_CLASS__ATTACH_HANDLER_PRAGMA:
				return attachHandlerPragma != null;
			case AdaPackage.NAME_CLASS__CONTROLLED_PRAGMA:
				return controlledPragma != null;
			case AdaPackage.NAME_CLASS__CONVENTION_PRAGMA:
				return conventionPragma != null;
			case AdaPackage.NAME_CLASS__DISCARD_NAMES_PRAGMA:
				return discardNamesPragma != null;
			case AdaPackage.NAME_CLASS__ELABORATE_PRAGMA:
				return elaboratePragma != null;
			case AdaPackage.NAME_CLASS__ELABORATE_ALL_PRAGMA:
				return elaborateAllPragma != null;
			case AdaPackage.NAME_CLASS__ELABORATE_BODY_PRAGMA:
				return elaborateBodyPragma != null;
			case AdaPackage.NAME_CLASS__EXPORT_PRAGMA:
				return exportPragma != null;
			case AdaPackage.NAME_CLASS__IMPORT_PRAGMA:
				return importPragma != null;
			case AdaPackage.NAME_CLASS__INLINE_PRAGMA:
				return inlinePragma != null;
			case AdaPackage.NAME_CLASS__INSPECTION_POINT_PRAGMA:
				return inspectionPointPragma != null;
			case AdaPackage.NAME_CLASS__INTERRUPT_HANDLER_PRAGMA:
				return interruptHandlerPragma != null;
			case AdaPackage.NAME_CLASS__INTERRUPT_PRIORITY_PRAGMA:
				return interruptPriorityPragma != null;
			case AdaPackage.NAME_CLASS__LINKER_OPTIONS_PRAGMA:
				return linkerOptionsPragma != null;
			case AdaPackage.NAME_CLASS__LIST_PRAGMA:
				return listPragma != null;
			case AdaPackage.NAME_CLASS__LOCKING_POLICY_PRAGMA:
				return lockingPolicyPragma != null;
			case AdaPackage.NAME_CLASS__NORMALIZE_SCALARS_PRAGMA:
				return normalizeScalarsPragma != null;
			case AdaPackage.NAME_CLASS__OPTIMIZE_PRAGMA:
				return optimizePragma != null;
			case AdaPackage.NAME_CLASS__PACK_PRAGMA:
				return packPragma != null;
			case AdaPackage.NAME_CLASS__PAGE_PRAGMA:
				return pagePragma != null;
			case AdaPackage.NAME_CLASS__PREELABORATE_PRAGMA:
				return preelaboratePragma != null;
			case AdaPackage.NAME_CLASS__PRIORITY_PRAGMA:
				return priorityPragma != null;
			case AdaPackage.NAME_CLASS__PURE_PRAGMA:
				return purePragma != null;
			case AdaPackage.NAME_CLASS__QUEUING_POLICY_PRAGMA:
				return queuingPolicyPragma != null;
			case AdaPackage.NAME_CLASS__REMOTE_CALL_INTERFACE_PRAGMA:
				return remoteCallInterfacePragma != null;
			case AdaPackage.NAME_CLASS__REMOTE_TYPES_PRAGMA:
				return remoteTypesPragma != null;
			case AdaPackage.NAME_CLASS__RESTRICTIONS_PRAGMA:
				return restrictionsPragma != null;
			case AdaPackage.NAME_CLASS__REVIEWABLE_PRAGMA:
				return reviewablePragma != null;
			case AdaPackage.NAME_CLASS__SHARED_PASSIVE_PRAGMA:
				return sharedPassivePragma != null;
			case AdaPackage.NAME_CLASS__STORAGE_SIZE_PRAGMA:
				return storageSizePragma != null;
			case AdaPackage.NAME_CLASS__SUPPRESS_PRAGMA:
				return suppressPragma != null;
			case AdaPackage.NAME_CLASS__TASK_DISPATCHING_POLICY_PRAGMA:
				return taskDispatchingPolicyPragma != null;
			case AdaPackage.NAME_CLASS__VOLATILE_PRAGMA:
				return volatilePragma != null;
			case AdaPackage.NAME_CLASS__VOLATILE_COMPONENTS_PRAGMA:
				return volatileComponentsPragma != null;
			case AdaPackage.NAME_CLASS__ASSERT_PRAGMA:
				return assertPragma != null;
			case AdaPackage.NAME_CLASS__ASSERTION_POLICY_PRAGMA:
				return assertionPolicyPragma != null;
			case AdaPackage.NAME_CLASS__DETECT_BLOCKING_PRAGMA:
				return detectBlockingPragma != null;
			case AdaPackage.NAME_CLASS__NO_RETURN_PRAGMA:
				return noReturnPragma != null;
			case AdaPackage.NAME_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA:
				return partitionElaborationPolicyPragma != null;
			case AdaPackage.NAME_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA:
				return preelaborableInitializationPragma != null;
			case AdaPackage.NAME_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return prioritySpecificDispatchingPragma != null;
			case AdaPackage.NAME_CLASS__PROFILE_PRAGMA:
				return profilePragma != null;
			case AdaPackage.NAME_CLASS__RELATIVE_DEADLINE_PRAGMA:
				return relativeDeadlinePragma != null;
			case AdaPackage.NAME_CLASS__UNCHECKED_UNION_PRAGMA:
				return uncheckedUnionPragma != null;
			case AdaPackage.NAME_CLASS__UNSUPPRESS_PRAGMA:
				return unsuppressPragma != null;
			case AdaPackage.NAME_CLASS__DEFAULT_STORAGE_POOL_PRAGMA:
				return defaultStoragePoolPragma != null;
			case AdaPackage.NAME_CLASS__DISPATCHING_DOMAIN_PRAGMA:
				return dispatchingDomainPragma != null;
			case AdaPackage.NAME_CLASS__CPU_PRAGMA:
				return cpuPragma != null;
			case AdaPackage.NAME_CLASS__INDEPENDENT_PRAGMA:
				return independentPragma != null;
			case AdaPackage.NAME_CLASS__INDEPENDENT_COMPONENTS_PRAGMA:
				return independentComponentsPragma != null;
			case AdaPackage.NAME_CLASS__IMPLEMENTATION_DEFINED_PRAGMA:
				return implementationDefinedPragma != null;
			case AdaPackage.NAME_CLASS__UNKNOWN_PRAGMA:
				return unknownPragma != null;
		}
		return super.eIsSet(featureID);
	}

} //NameClassImpl
