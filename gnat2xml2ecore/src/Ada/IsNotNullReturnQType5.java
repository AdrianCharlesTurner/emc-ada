/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Is Not Null Return QType5</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.IsNotNullReturnQType5#getNotNullReturn <em>Not Null Return</em>}</li>
 *   <li>{@link Ada.IsNotNullReturnQType5#getNotAnElement <em>Not An Element</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getIsNotNullReturnQType5()
 * @model extendedMetaData="name='is_not_null_return_q_._5_._type' kind='elementOnly'"
 * @generated
 */
public interface IsNotNullReturnQType5 extends EObject {
	/**
	 * Returns the value of the '<em><b>Not Null Return</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not Null Return</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not Null Return</em>' containment reference.
	 * @see #setNotNullReturn(NotNullReturn)
	 * @see Ada.AdaPackage#getIsNotNullReturnQType5_NotNullReturn()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not_null_return' namespace='##targetNamespace'"
	 * @generated
	 */
	NotNullReturn getNotNullReturn();

	/**
	 * Sets the value of the '{@link Ada.IsNotNullReturnQType5#getNotNullReturn <em>Not Null Return</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not Null Return</em>' containment reference.
	 * @see #getNotNullReturn()
	 * @generated
	 */
	void setNotNullReturn(NotNullReturn value);

	/**
	 * Returns the value of the '<em><b>Not An Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not An Element</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not An Element</em>' containment reference.
	 * @see #setNotAnElement(NotAnElement)
	 * @see Ada.AdaPackage#getIsNotNullReturnQType5_NotAnElement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not_an_element' namespace='##targetNamespace'"
	 * @generated
	 */
	NotAnElement getNotAnElement();

	/**
	 * Sets the value of the '{@link Ada.IsNotNullReturnQType5#getNotAnElement <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not An Element</em>' containment reference.
	 * @see #getNotAnElement()
	 * @generated
	 */
	void setNotAnElement(NotAnElement value);

} // IsNotNullReturnQType5
