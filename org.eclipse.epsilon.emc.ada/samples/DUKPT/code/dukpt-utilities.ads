--------------------------------------------------------------------
-- Copyright <<Organisation owning copyright>>
-- <<Project Name>>
--
-- File:
--    $Id: dukpt-utilities.ads 2379 2017-02-28 10:02:48Z aturner@EUROPE $
--
-- Description:
--   ...
--------------------------------------------------------------------
--------------------------------------------------------------------
-- Package DUKPT
--
-- Description:
--
--------------------------------------------------------------------

package DUKPT.Utilities
is
   --------------------------------------------------------------------
   -- Description:
   --
   --------------------------------------------------------------------
   procedure Generate_LRC_Byte(FKR_Block : in out B_Block136);

   --------------------------------------------------------------------
   -- Description:
   --
   --------------------------------------------------------------------
   procedure Validate_LRC_Byte(FKR_Block : in     B_Block136;
                               LRC_Valid :    out Boolean);

   --------------------------------------------------------------------
   -- Description:
   --
   --------------------------------------------------------------------
   procedure Invalidate_LRC_Byte(FKR_Block : in out B_Block136);

   --------------------------------------------------------------------
   -- Description:
   --
   --------------------------------------------------------------------
   procedure Increment_EC(EC        : in out Interfaces.Unsigned_32;
                          Increment : in     Interfaces.Unsigned_32);

   --------------------------------------------------------------------
   -- Description:
   --
   --------------------------------------------------------------------
   procedure Update_KSNR(EC   : in     Interfaces.Unsigned_32;
                         KSNR : in out Crypto.Types.B_Block128);

   --------------------------------------------------------------------
   -- Description:
   --
   --------------------------------------------------------------------
   function To_Binary(N: Natural) return String
     with Global => null;

   --------------------------------------------------------------------
   -- Description:
   --
   --------------------------------------------------------------------
   function To_Hex(N: Natural) return String
     with Global => null;


   --------------------------------------------------------------------
   -- Description:
   --
   --------------------------------------------------------------------
   function Pop_Count(N: Interfaces.Unsigned_32) return Natural;

   --------------------------------------------------------------------
   -- Description:
   --
   --------------------------------------------------------------------
   function Chunk_32(Block : Interfaces.Unsigned_32) return Crypto.Types.B_Block32;

   --------------------------------------------------------------------
   -- Description:
   --
   --------------------------------------------------------------------
   function Shift_Register_Position(SR : Interfaces.Unsigned_32) return Natural_0_20;

   --------------------------------------------------------------------
   -- Description:
   --
   --------------------------------------------------------------------
   procedure Get_N_Most_Sig_Bits(Subject        : in     Interfaces.Unsigned_32;
                                 N              : in     Interfaces.Unsigned_32;
                                 Population     : in     Interfaces.Unsigned_32;
                                 Sub_N_Most_Sig :    out Interfaces.Unsigned_32;
                                 Shift_Register :    out Interfaces.Unsigned_32);

end DUKPT.Utilities;
