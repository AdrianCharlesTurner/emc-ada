/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.AllCallsRemotePragma;
import Ada.ArrayComponentAssociation;
import Ada.AssertPragma;
import Ada.AssertionPolicyPragma;
import Ada.AssociationClass;
import Ada.AsynchronousPragma;
import Ada.AtomicComponentsPragma;
import Ada.AtomicPragma;
import Ada.AttachHandlerPragma;
import Ada.Comment;
import Ada.ControlledPragma;
import Ada.ConventionPragma;
import Ada.CpuPragma;
import Ada.DefaultStoragePoolPragma;
import Ada.DetectBlockingPragma;
import Ada.DiscardNamesPragma;
import Ada.DiscriminantAssociation;
import Ada.DispatchingDomainPragma;
import Ada.ElaborateAllPragma;
import Ada.ElaborateBodyPragma;
import Ada.ElaboratePragma;
import Ada.ExportPragma;
import Ada.GenericAssociation;
import Ada.ImplementationDefinedPragma;
import Ada.ImportPragma;
import Ada.IndependentComponentsPragma;
import Ada.IndependentPragma;
import Ada.InlinePragma;
import Ada.InspectionPointPragma;
import Ada.InterruptHandlerPragma;
import Ada.InterruptPriorityPragma;
import Ada.LinkerOptionsPragma;
import Ada.ListPragma;
import Ada.LockingPolicyPragma;
import Ada.NoReturnPragma;
import Ada.NormalizeScalarsPragma;
import Ada.NotAnElement;
import Ada.OptimizePragma;
import Ada.PackPragma;
import Ada.PagePragma;
import Ada.ParameterAssociation;
import Ada.PartitionElaborationPolicyPragma;
import Ada.PragmaArgumentAssociation;
import Ada.PreelaborableInitializationPragma;
import Ada.PreelaboratePragma;
import Ada.PriorityPragma;
import Ada.PrioritySpecificDispatchingPragma;
import Ada.ProfilePragma;
import Ada.PurePragma;
import Ada.QueuingPolicyPragma;
import Ada.RecordComponentAssociation;
import Ada.RelativeDeadlinePragma;
import Ada.RemoteCallInterfacePragma;
import Ada.RemoteTypesPragma;
import Ada.RestrictionsPragma;
import Ada.ReviewablePragma;
import Ada.SharedPassivePragma;
import Ada.StorageSizePragma;
import Ada.SuppressPragma;
import Ada.TaskDispatchingPolicyPragma;
import Ada.UncheckedUnionPragma;
import Ada.UnknownPragma;
import Ada.UnsuppressPragma;
import Ada.VolatileComponentsPragma;
import Ada.VolatilePragma;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Association Class</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.AssociationClassImpl#getNotAnElement <em>Not An Element</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getPragmaArgumentAssociation <em>Pragma Argument Association</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getDiscriminantAssociation <em>Discriminant Association</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getRecordComponentAssociation <em>Record Component Association</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getArrayComponentAssociation <em>Array Component Association</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getParameterAssociation <em>Parameter Association</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getGenericAssociation <em>Generic Association</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getAsynchronousPragma <em>Asynchronous Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getAtomicPragma <em>Atomic Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getAttachHandlerPragma <em>Attach Handler Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getControlledPragma <em>Controlled Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getConventionPragma <em>Convention Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getDiscardNamesPragma <em>Discard Names Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getElaboratePragma <em>Elaborate Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getElaborateAllPragma <em>Elaborate All Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getExportPragma <em>Export Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getImportPragma <em>Import Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getInlinePragma <em>Inline Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getInspectionPointPragma <em>Inspection Point Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getLinkerOptionsPragma <em>Linker Options Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getListPragma <em>List Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getLockingPolicyPragma <em>Locking Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getOptimizePragma <em>Optimize Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getPackPragma <em>Pack Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getPagePragma <em>Page Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getPreelaboratePragma <em>Preelaborate Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getPriorityPragma <em>Priority Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getPurePragma <em>Pure Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getRemoteTypesPragma <em>Remote Types Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getRestrictionsPragma <em>Restrictions Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getReviewablePragma <em>Reviewable Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getSharedPassivePragma <em>Shared Passive Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getStorageSizePragma <em>Storage Size Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getSuppressPragma <em>Suppress Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getVolatilePragma <em>Volatile Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getAssertPragma <em>Assert Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getNoReturnPragma <em>No Return Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getProfilePragma <em>Profile Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getUnsuppressPragma <em>Unsuppress Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getCpuPragma <em>Cpu Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getIndependentPragma <em>Independent Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getIndependentComponentsPragma <em>Independent Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}</li>
 *   <li>{@link Ada.impl.AssociationClassImpl#getUnknownPragma <em>Unknown Pragma</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AssociationClassImpl extends MinimalEObjectImpl.Container implements AssociationClass {
	/**
	 * The cached value of the '{@link #getNotAnElement() <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotAnElement()
	 * @generated
	 * @ordered
	 */
	protected NotAnElement notAnElement;

	/**
	 * The cached value of the '{@link #getPragmaArgumentAssociation() <em>Pragma Argument Association</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPragmaArgumentAssociation()
	 * @generated
	 * @ordered
	 */
	protected PragmaArgumentAssociation pragmaArgumentAssociation;

	/**
	 * The cached value of the '{@link #getDiscriminantAssociation() <em>Discriminant Association</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiscriminantAssociation()
	 * @generated
	 * @ordered
	 */
	protected DiscriminantAssociation discriminantAssociation;

	/**
	 * The cached value of the '{@link #getRecordComponentAssociation() <em>Record Component Association</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRecordComponentAssociation()
	 * @generated
	 * @ordered
	 */
	protected RecordComponentAssociation recordComponentAssociation;

	/**
	 * The cached value of the '{@link #getArrayComponentAssociation() <em>Array Component Association</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArrayComponentAssociation()
	 * @generated
	 * @ordered
	 */
	protected ArrayComponentAssociation arrayComponentAssociation;

	/**
	 * The cached value of the '{@link #getParameterAssociation() <em>Parameter Association</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterAssociation()
	 * @generated
	 * @ordered
	 */
	protected ParameterAssociation parameterAssociation;

	/**
	 * The cached value of the '{@link #getGenericAssociation() <em>Generic Association</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGenericAssociation()
	 * @generated
	 * @ordered
	 */
	protected GenericAssociation genericAssociation;

	/**
	 * The cached value of the '{@link #getComment() <em>Comment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected Comment comment;

	/**
	 * The cached value of the '{@link #getAllCallsRemotePragma() <em>All Calls Remote Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllCallsRemotePragma()
	 * @generated
	 * @ordered
	 */
	protected AllCallsRemotePragma allCallsRemotePragma;

	/**
	 * The cached value of the '{@link #getAsynchronousPragma() <em>Asynchronous Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsynchronousPragma()
	 * @generated
	 * @ordered
	 */
	protected AsynchronousPragma asynchronousPragma;

	/**
	 * The cached value of the '{@link #getAtomicPragma() <em>Atomic Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAtomicPragma()
	 * @generated
	 * @ordered
	 */
	protected AtomicPragma atomicPragma;

	/**
	 * The cached value of the '{@link #getAtomicComponentsPragma() <em>Atomic Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAtomicComponentsPragma()
	 * @generated
	 * @ordered
	 */
	protected AtomicComponentsPragma atomicComponentsPragma;

	/**
	 * The cached value of the '{@link #getAttachHandlerPragma() <em>Attach Handler Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttachHandlerPragma()
	 * @generated
	 * @ordered
	 */
	protected AttachHandlerPragma attachHandlerPragma;

	/**
	 * The cached value of the '{@link #getControlledPragma() <em>Controlled Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getControlledPragma()
	 * @generated
	 * @ordered
	 */
	protected ControlledPragma controlledPragma;

	/**
	 * The cached value of the '{@link #getConventionPragma() <em>Convention Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConventionPragma()
	 * @generated
	 * @ordered
	 */
	protected ConventionPragma conventionPragma;

	/**
	 * The cached value of the '{@link #getDiscardNamesPragma() <em>Discard Names Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiscardNamesPragma()
	 * @generated
	 * @ordered
	 */
	protected DiscardNamesPragma discardNamesPragma;

	/**
	 * The cached value of the '{@link #getElaboratePragma() <em>Elaborate Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElaboratePragma()
	 * @generated
	 * @ordered
	 */
	protected ElaboratePragma elaboratePragma;

	/**
	 * The cached value of the '{@link #getElaborateAllPragma() <em>Elaborate All Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElaborateAllPragma()
	 * @generated
	 * @ordered
	 */
	protected ElaborateAllPragma elaborateAllPragma;

	/**
	 * The cached value of the '{@link #getElaborateBodyPragma() <em>Elaborate Body Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElaborateBodyPragma()
	 * @generated
	 * @ordered
	 */
	protected ElaborateBodyPragma elaborateBodyPragma;

	/**
	 * The cached value of the '{@link #getExportPragma() <em>Export Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExportPragma()
	 * @generated
	 * @ordered
	 */
	protected ExportPragma exportPragma;

	/**
	 * The cached value of the '{@link #getImportPragma() <em>Import Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImportPragma()
	 * @generated
	 * @ordered
	 */
	protected ImportPragma importPragma;

	/**
	 * The cached value of the '{@link #getInlinePragma() <em>Inline Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInlinePragma()
	 * @generated
	 * @ordered
	 */
	protected InlinePragma inlinePragma;

	/**
	 * The cached value of the '{@link #getInspectionPointPragma() <em>Inspection Point Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInspectionPointPragma()
	 * @generated
	 * @ordered
	 */
	protected InspectionPointPragma inspectionPointPragma;

	/**
	 * The cached value of the '{@link #getInterruptHandlerPragma() <em>Interrupt Handler Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterruptHandlerPragma()
	 * @generated
	 * @ordered
	 */
	protected InterruptHandlerPragma interruptHandlerPragma;

	/**
	 * The cached value of the '{@link #getInterruptPriorityPragma() <em>Interrupt Priority Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterruptPriorityPragma()
	 * @generated
	 * @ordered
	 */
	protected InterruptPriorityPragma interruptPriorityPragma;

	/**
	 * The cached value of the '{@link #getLinkerOptionsPragma() <em>Linker Options Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkerOptionsPragma()
	 * @generated
	 * @ordered
	 */
	protected LinkerOptionsPragma linkerOptionsPragma;

	/**
	 * The cached value of the '{@link #getListPragma() <em>List Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListPragma()
	 * @generated
	 * @ordered
	 */
	protected ListPragma listPragma;

	/**
	 * The cached value of the '{@link #getLockingPolicyPragma() <em>Locking Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLockingPolicyPragma()
	 * @generated
	 * @ordered
	 */
	protected LockingPolicyPragma lockingPolicyPragma;

	/**
	 * The cached value of the '{@link #getNormalizeScalarsPragma() <em>Normalize Scalars Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNormalizeScalarsPragma()
	 * @generated
	 * @ordered
	 */
	protected NormalizeScalarsPragma normalizeScalarsPragma;

	/**
	 * The cached value of the '{@link #getOptimizePragma() <em>Optimize Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOptimizePragma()
	 * @generated
	 * @ordered
	 */
	protected OptimizePragma optimizePragma;

	/**
	 * The cached value of the '{@link #getPackPragma() <em>Pack Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackPragma()
	 * @generated
	 * @ordered
	 */
	protected PackPragma packPragma;

	/**
	 * The cached value of the '{@link #getPagePragma() <em>Page Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPagePragma()
	 * @generated
	 * @ordered
	 */
	protected PagePragma pagePragma;

	/**
	 * The cached value of the '{@link #getPreelaboratePragma() <em>Preelaborate Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreelaboratePragma()
	 * @generated
	 * @ordered
	 */
	protected PreelaboratePragma preelaboratePragma;

	/**
	 * The cached value of the '{@link #getPriorityPragma() <em>Priority Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPriorityPragma()
	 * @generated
	 * @ordered
	 */
	protected PriorityPragma priorityPragma;

	/**
	 * The cached value of the '{@link #getPurePragma() <em>Pure Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPurePragma()
	 * @generated
	 * @ordered
	 */
	protected PurePragma purePragma;

	/**
	 * The cached value of the '{@link #getQueuingPolicyPragma() <em>Queuing Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQueuingPolicyPragma()
	 * @generated
	 * @ordered
	 */
	protected QueuingPolicyPragma queuingPolicyPragma;

	/**
	 * The cached value of the '{@link #getRemoteCallInterfacePragma() <em>Remote Call Interface Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRemoteCallInterfacePragma()
	 * @generated
	 * @ordered
	 */
	protected RemoteCallInterfacePragma remoteCallInterfacePragma;

	/**
	 * The cached value of the '{@link #getRemoteTypesPragma() <em>Remote Types Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRemoteTypesPragma()
	 * @generated
	 * @ordered
	 */
	protected RemoteTypesPragma remoteTypesPragma;

	/**
	 * The cached value of the '{@link #getRestrictionsPragma() <em>Restrictions Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRestrictionsPragma()
	 * @generated
	 * @ordered
	 */
	protected RestrictionsPragma restrictionsPragma;

	/**
	 * The cached value of the '{@link #getReviewablePragma() <em>Reviewable Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReviewablePragma()
	 * @generated
	 * @ordered
	 */
	protected ReviewablePragma reviewablePragma;

	/**
	 * The cached value of the '{@link #getSharedPassivePragma() <em>Shared Passive Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSharedPassivePragma()
	 * @generated
	 * @ordered
	 */
	protected SharedPassivePragma sharedPassivePragma;

	/**
	 * The cached value of the '{@link #getStorageSizePragma() <em>Storage Size Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStorageSizePragma()
	 * @generated
	 * @ordered
	 */
	protected StorageSizePragma storageSizePragma;

	/**
	 * The cached value of the '{@link #getSuppressPragma() <em>Suppress Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuppressPragma()
	 * @generated
	 * @ordered
	 */
	protected SuppressPragma suppressPragma;

	/**
	 * The cached value of the '{@link #getTaskDispatchingPolicyPragma() <em>Task Dispatching Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskDispatchingPolicyPragma()
	 * @generated
	 * @ordered
	 */
	protected TaskDispatchingPolicyPragma taskDispatchingPolicyPragma;

	/**
	 * The cached value of the '{@link #getVolatilePragma() <em>Volatile Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVolatilePragma()
	 * @generated
	 * @ordered
	 */
	protected VolatilePragma volatilePragma;

	/**
	 * The cached value of the '{@link #getVolatileComponentsPragma() <em>Volatile Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVolatileComponentsPragma()
	 * @generated
	 * @ordered
	 */
	protected VolatileComponentsPragma volatileComponentsPragma;

	/**
	 * The cached value of the '{@link #getAssertPragma() <em>Assert Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssertPragma()
	 * @generated
	 * @ordered
	 */
	protected AssertPragma assertPragma;

	/**
	 * The cached value of the '{@link #getAssertionPolicyPragma() <em>Assertion Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssertionPolicyPragma()
	 * @generated
	 * @ordered
	 */
	protected AssertionPolicyPragma assertionPolicyPragma;

	/**
	 * The cached value of the '{@link #getDetectBlockingPragma() <em>Detect Blocking Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDetectBlockingPragma()
	 * @generated
	 * @ordered
	 */
	protected DetectBlockingPragma detectBlockingPragma;

	/**
	 * The cached value of the '{@link #getNoReturnPragma() <em>No Return Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNoReturnPragma()
	 * @generated
	 * @ordered
	 */
	protected NoReturnPragma noReturnPragma;

	/**
	 * The cached value of the '{@link #getPartitionElaborationPolicyPragma() <em>Partition Elaboration Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPartitionElaborationPolicyPragma()
	 * @generated
	 * @ordered
	 */
	protected PartitionElaborationPolicyPragma partitionElaborationPolicyPragma;

	/**
	 * The cached value of the '{@link #getPreelaborableInitializationPragma() <em>Preelaborable Initialization Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreelaborableInitializationPragma()
	 * @generated
	 * @ordered
	 */
	protected PreelaborableInitializationPragma preelaborableInitializationPragma;

	/**
	 * The cached value of the '{@link #getPrioritySpecificDispatchingPragma() <em>Priority Specific Dispatching Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrioritySpecificDispatchingPragma()
	 * @generated
	 * @ordered
	 */
	protected PrioritySpecificDispatchingPragma prioritySpecificDispatchingPragma;

	/**
	 * The cached value of the '{@link #getProfilePragma() <em>Profile Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProfilePragma()
	 * @generated
	 * @ordered
	 */
	protected ProfilePragma profilePragma;

	/**
	 * The cached value of the '{@link #getRelativeDeadlinePragma() <em>Relative Deadline Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelativeDeadlinePragma()
	 * @generated
	 * @ordered
	 */
	protected RelativeDeadlinePragma relativeDeadlinePragma;

	/**
	 * The cached value of the '{@link #getUncheckedUnionPragma() <em>Unchecked Union Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUncheckedUnionPragma()
	 * @generated
	 * @ordered
	 */
	protected UncheckedUnionPragma uncheckedUnionPragma;

	/**
	 * The cached value of the '{@link #getUnsuppressPragma() <em>Unsuppress Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnsuppressPragma()
	 * @generated
	 * @ordered
	 */
	protected UnsuppressPragma unsuppressPragma;

	/**
	 * The cached value of the '{@link #getDefaultStoragePoolPragma() <em>Default Storage Pool Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultStoragePoolPragma()
	 * @generated
	 * @ordered
	 */
	protected DefaultStoragePoolPragma defaultStoragePoolPragma;

	/**
	 * The cached value of the '{@link #getDispatchingDomainPragma() <em>Dispatching Domain Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDispatchingDomainPragma()
	 * @generated
	 * @ordered
	 */
	protected DispatchingDomainPragma dispatchingDomainPragma;

	/**
	 * The cached value of the '{@link #getCpuPragma() <em>Cpu Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCpuPragma()
	 * @generated
	 * @ordered
	 */
	protected CpuPragma cpuPragma;

	/**
	 * The cached value of the '{@link #getIndependentPragma() <em>Independent Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndependentPragma()
	 * @generated
	 * @ordered
	 */
	protected IndependentPragma independentPragma;

	/**
	 * The cached value of the '{@link #getIndependentComponentsPragma() <em>Independent Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndependentComponentsPragma()
	 * @generated
	 * @ordered
	 */
	protected IndependentComponentsPragma independentComponentsPragma;

	/**
	 * The cached value of the '{@link #getImplementationDefinedPragma() <em>Implementation Defined Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImplementationDefinedPragma()
	 * @generated
	 * @ordered
	 */
	protected ImplementationDefinedPragma implementationDefinedPragma;

	/**
	 * The cached value of the '{@link #getUnknownPragma() <em>Unknown Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnknownPragma()
	 * @generated
	 * @ordered
	 */
	protected UnknownPragma unknownPragma;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssociationClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getAssociationClass();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotAnElement getNotAnElement() {
		return notAnElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNotAnElement(NotAnElement newNotAnElement, NotificationChain msgs) {
		NotAnElement oldNotAnElement = notAnElement;
		notAnElement = newNotAnElement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__NOT_AN_ELEMENT, oldNotAnElement, newNotAnElement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotAnElement(NotAnElement newNotAnElement) {
		if (newNotAnElement != notAnElement) {
			NotificationChain msgs = null;
			if (notAnElement != null)
				msgs = ((InternalEObject)notAnElement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__NOT_AN_ELEMENT, null, msgs);
			if (newNotAnElement != null)
				msgs = ((InternalEObject)newNotAnElement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__NOT_AN_ELEMENT, null, msgs);
			msgs = basicSetNotAnElement(newNotAnElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__NOT_AN_ELEMENT, newNotAnElement, newNotAnElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PragmaArgumentAssociation getPragmaArgumentAssociation() {
		return pragmaArgumentAssociation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPragmaArgumentAssociation(PragmaArgumentAssociation newPragmaArgumentAssociation, NotificationChain msgs) {
		PragmaArgumentAssociation oldPragmaArgumentAssociation = pragmaArgumentAssociation;
		pragmaArgumentAssociation = newPragmaArgumentAssociation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__PRAGMA_ARGUMENT_ASSOCIATION, oldPragmaArgumentAssociation, newPragmaArgumentAssociation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPragmaArgumentAssociation(PragmaArgumentAssociation newPragmaArgumentAssociation) {
		if (newPragmaArgumentAssociation != pragmaArgumentAssociation) {
			NotificationChain msgs = null;
			if (pragmaArgumentAssociation != null)
				msgs = ((InternalEObject)pragmaArgumentAssociation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__PRAGMA_ARGUMENT_ASSOCIATION, null, msgs);
			if (newPragmaArgumentAssociation != null)
				msgs = ((InternalEObject)newPragmaArgumentAssociation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__PRAGMA_ARGUMENT_ASSOCIATION, null, msgs);
			msgs = basicSetPragmaArgumentAssociation(newPragmaArgumentAssociation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__PRAGMA_ARGUMENT_ASSOCIATION, newPragmaArgumentAssociation, newPragmaArgumentAssociation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscriminantAssociation getDiscriminantAssociation() {
		return discriminantAssociation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscriminantAssociation(DiscriminantAssociation newDiscriminantAssociation, NotificationChain msgs) {
		DiscriminantAssociation oldDiscriminantAssociation = discriminantAssociation;
		discriminantAssociation = newDiscriminantAssociation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__DISCRIMINANT_ASSOCIATION, oldDiscriminantAssociation, newDiscriminantAssociation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscriminantAssociation(DiscriminantAssociation newDiscriminantAssociation) {
		if (newDiscriminantAssociation != discriminantAssociation) {
			NotificationChain msgs = null;
			if (discriminantAssociation != null)
				msgs = ((InternalEObject)discriminantAssociation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__DISCRIMINANT_ASSOCIATION, null, msgs);
			if (newDiscriminantAssociation != null)
				msgs = ((InternalEObject)newDiscriminantAssociation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__DISCRIMINANT_ASSOCIATION, null, msgs);
			msgs = basicSetDiscriminantAssociation(newDiscriminantAssociation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__DISCRIMINANT_ASSOCIATION, newDiscriminantAssociation, newDiscriminantAssociation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordComponentAssociation getRecordComponentAssociation() {
		return recordComponentAssociation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRecordComponentAssociation(RecordComponentAssociation newRecordComponentAssociation, NotificationChain msgs) {
		RecordComponentAssociation oldRecordComponentAssociation = recordComponentAssociation;
		recordComponentAssociation = newRecordComponentAssociation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__RECORD_COMPONENT_ASSOCIATION, oldRecordComponentAssociation, newRecordComponentAssociation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRecordComponentAssociation(RecordComponentAssociation newRecordComponentAssociation) {
		if (newRecordComponentAssociation != recordComponentAssociation) {
			NotificationChain msgs = null;
			if (recordComponentAssociation != null)
				msgs = ((InternalEObject)recordComponentAssociation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__RECORD_COMPONENT_ASSOCIATION, null, msgs);
			if (newRecordComponentAssociation != null)
				msgs = ((InternalEObject)newRecordComponentAssociation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__RECORD_COMPONENT_ASSOCIATION, null, msgs);
			msgs = basicSetRecordComponentAssociation(newRecordComponentAssociation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__RECORD_COMPONENT_ASSOCIATION, newRecordComponentAssociation, newRecordComponentAssociation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArrayComponentAssociation getArrayComponentAssociation() {
		return arrayComponentAssociation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetArrayComponentAssociation(ArrayComponentAssociation newArrayComponentAssociation, NotificationChain msgs) {
		ArrayComponentAssociation oldArrayComponentAssociation = arrayComponentAssociation;
		arrayComponentAssociation = newArrayComponentAssociation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__ARRAY_COMPONENT_ASSOCIATION, oldArrayComponentAssociation, newArrayComponentAssociation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setArrayComponentAssociation(ArrayComponentAssociation newArrayComponentAssociation) {
		if (newArrayComponentAssociation != arrayComponentAssociation) {
			NotificationChain msgs = null;
			if (arrayComponentAssociation != null)
				msgs = ((InternalEObject)arrayComponentAssociation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__ARRAY_COMPONENT_ASSOCIATION, null, msgs);
			if (newArrayComponentAssociation != null)
				msgs = ((InternalEObject)newArrayComponentAssociation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__ARRAY_COMPONENT_ASSOCIATION, null, msgs);
			msgs = basicSetArrayComponentAssociation(newArrayComponentAssociation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__ARRAY_COMPONENT_ASSOCIATION, newArrayComponentAssociation, newArrayComponentAssociation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterAssociation getParameterAssociation() {
		return parameterAssociation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParameterAssociation(ParameterAssociation newParameterAssociation, NotificationChain msgs) {
		ParameterAssociation oldParameterAssociation = parameterAssociation;
		parameterAssociation = newParameterAssociation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__PARAMETER_ASSOCIATION, oldParameterAssociation, newParameterAssociation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterAssociation(ParameterAssociation newParameterAssociation) {
		if (newParameterAssociation != parameterAssociation) {
			NotificationChain msgs = null;
			if (parameterAssociation != null)
				msgs = ((InternalEObject)parameterAssociation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__PARAMETER_ASSOCIATION, null, msgs);
			if (newParameterAssociation != null)
				msgs = ((InternalEObject)newParameterAssociation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__PARAMETER_ASSOCIATION, null, msgs);
			msgs = basicSetParameterAssociation(newParameterAssociation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__PARAMETER_ASSOCIATION, newParameterAssociation, newParameterAssociation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericAssociation getGenericAssociation() {
		return genericAssociation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGenericAssociation(GenericAssociation newGenericAssociation, NotificationChain msgs) {
		GenericAssociation oldGenericAssociation = genericAssociation;
		genericAssociation = newGenericAssociation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__GENERIC_ASSOCIATION, oldGenericAssociation, newGenericAssociation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGenericAssociation(GenericAssociation newGenericAssociation) {
		if (newGenericAssociation != genericAssociation) {
			NotificationChain msgs = null;
			if (genericAssociation != null)
				msgs = ((InternalEObject)genericAssociation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__GENERIC_ASSOCIATION, null, msgs);
			if (newGenericAssociation != null)
				msgs = ((InternalEObject)newGenericAssociation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__GENERIC_ASSOCIATION, null, msgs);
			msgs = basicSetGenericAssociation(newGenericAssociation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__GENERIC_ASSOCIATION, newGenericAssociation, newGenericAssociation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Comment getComment() {
		return comment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComment(Comment newComment, NotificationChain msgs) {
		Comment oldComment = comment;
		comment = newComment;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__COMMENT, oldComment, newComment);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComment(Comment newComment) {
		if (newComment != comment) {
			NotificationChain msgs = null;
			if (comment != null)
				msgs = ((InternalEObject)comment).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__COMMENT, null, msgs);
			if (newComment != null)
				msgs = ((InternalEObject)newComment).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__COMMENT, null, msgs);
			msgs = basicSetComment(newComment, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__COMMENT, newComment, newComment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AllCallsRemotePragma getAllCallsRemotePragma() {
		return allCallsRemotePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAllCallsRemotePragma(AllCallsRemotePragma newAllCallsRemotePragma, NotificationChain msgs) {
		AllCallsRemotePragma oldAllCallsRemotePragma = allCallsRemotePragma;
		allCallsRemotePragma = newAllCallsRemotePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__ALL_CALLS_REMOTE_PRAGMA, oldAllCallsRemotePragma, newAllCallsRemotePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAllCallsRemotePragma(AllCallsRemotePragma newAllCallsRemotePragma) {
		if (newAllCallsRemotePragma != allCallsRemotePragma) {
			NotificationChain msgs = null;
			if (allCallsRemotePragma != null)
				msgs = ((InternalEObject)allCallsRemotePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__ALL_CALLS_REMOTE_PRAGMA, null, msgs);
			if (newAllCallsRemotePragma != null)
				msgs = ((InternalEObject)newAllCallsRemotePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__ALL_CALLS_REMOTE_PRAGMA, null, msgs);
			msgs = basicSetAllCallsRemotePragma(newAllCallsRemotePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__ALL_CALLS_REMOTE_PRAGMA, newAllCallsRemotePragma, newAllCallsRemotePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AsynchronousPragma getAsynchronousPragma() {
		return asynchronousPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAsynchronousPragma(AsynchronousPragma newAsynchronousPragma, NotificationChain msgs) {
		AsynchronousPragma oldAsynchronousPragma = asynchronousPragma;
		asynchronousPragma = newAsynchronousPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__ASYNCHRONOUS_PRAGMA, oldAsynchronousPragma, newAsynchronousPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAsynchronousPragma(AsynchronousPragma newAsynchronousPragma) {
		if (newAsynchronousPragma != asynchronousPragma) {
			NotificationChain msgs = null;
			if (asynchronousPragma != null)
				msgs = ((InternalEObject)asynchronousPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__ASYNCHRONOUS_PRAGMA, null, msgs);
			if (newAsynchronousPragma != null)
				msgs = ((InternalEObject)newAsynchronousPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__ASYNCHRONOUS_PRAGMA, null, msgs);
			msgs = basicSetAsynchronousPragma(newAsynchronousPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__ASYNCHRONOUS_PRAGMA, newAsynchronousPragma, newAsynchronousPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtomicPragma getAtomicPragma() {
		return atomicPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAtomicPragma(AtomicPragma newAtomicPragma, NotificationChain msgs) {
		AtomicPragma oldAtomicPragma = atomicPragma;
		atomicPragma = newAtomicPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__ATOMIC_PRAGMA, oldAtomicPragma, newAtomicPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAtomicPragma(AtomicPragma newAtomicPragma) {
		if (newAtomicPragma != atomicPragma) {
			NotificationChain msgs = null;
			if (atomicPragma != null)
				msgs = ((InternalEObject)atomicPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__ATOMIC_PRAGMA, null, msgs);
			if (newAtomicPragma != null)
				msgs = ((InternalEObject)newAtomicPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__ATOMIC_PRAGMA, null, msgs);
			msgs = basicSetAtomicPragma(newAtomicPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__ATOMIC_PRAGMA, newAtomicPragma, newAtomicPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtomicComponentsPragma getAtomicComponentsPragma() {
		return atomicComponentsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAtomicComponentsPragma(AtomicComponentsPragma newAtomicComponentsPragma, NotificationChain msgs) {
		AtomicComponentsPragma oldAtomicComponentsPragma = atomicComponentsPragma;
		atomicComponentsPragma = newAtomicComponentsPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__ATOMIC_COMPONENTS_PRAGMA, oldAtomicComponentsPragma, newAtomicComponentsPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAtomicComponentsPragma(AtomicComponentsPragma newAtomicComponentsPragma) {
		if (newAtomicComponentsPragma != atomicComponentsPragma) {
			NotificationChain msgs = null;
			if (atomicComponentsPragma != null)
				msgs = ((InternalEObject)atomicComponentsPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__ATOMIC_COMPONENTS_PRAGMA, null, msgs);
			if (newAtomicComponentsPragma != null)
				msgs = ((InternalEObject)newAtomicComponentsPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__ATOMIC_COMPONENTS_PRAGMA, null, msgs);
			msgs = basicSetAtomicComponentsPragma(newAtomicComponentsPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__ATOMIC_COMPONENTS_PRAGMA, newAtomicComponentsPragma, newAtomicComponentsPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttachHandlerPragma getAttachHandlerPragma() {
		return attachHandlerPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAttachHandlerPragma(AttachHandlerPragma newAttachHandlerPragma, NotificationChain msgs) {
		AttachHandlerPragma oldAttachHandlerPragma = attachHandlerPragma;
		attachHandlerPragma = newAttachHandlerPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__ATTACH_HANDLER_PRAGMA, oldAttachHandlerPragma, newAttachHandlerPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttachHandlerPragma(AttachHandlerPragma newAttachHandlerPragma) {
		if (newAttachHandlerPragma != attachHandlerPragma) {
			NotificationChain msgs = null;
			if (attachHandlerPragma != null)
				msgs = ((InternalEObject)attachHandlerPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__ATTACH_HANDLER_PRAGMA, null, msgs);
			if (newAttachHandlerPragma != null)
				msgs = ((InternalEObject)newAttachHandlerPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__ATTACH_HANDLER_PRAGMA, null, msgs);
			msgs = basicSetAttachHandlerPragma(newAttachHandlerPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__ATTACH_HANDLER_PRAGMA, newAttachHandlerPragma, newAttachHandlerPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlledPragma getControlledPragma() {
		return controlledPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetControlledPragma(ControlledPragma newControlledPragma, NotificationChain msgs) {
		ControlledPragma oldControlledPragma = controlledPragma;
		controlledPragma = newControlledPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__CONTROLLED_PRAGMA, oldControlledPragma, newControlledPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setControlledPragma(ControlledPragma newControlledPragma) {
		if (newControlledPragma != controlledPragma) {
			NotificationChain msgs = null;
			if (controlledPragma != null)
				msgs = ((InternalEObject)controlledPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__CONTROLLED_PRAGMA, null, msgs);
			if (newControlledPragma != null)
				msgs = ((InternalEObject)newControlledPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__CONTROLLED_PRAGMA, null, msgs);
			msgs = basicSetControlledPragma(newControlledPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__CONTROLLED_PRAGMA, newControlledPragma, newControlledPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConventionPragma getConventionPragma() {
		return conventionPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConventionPragma(ConventionPragma newConventionPragma, NotificationChain msgs) {
		ConventionPragma oldConventionPragma = conventionPragma;
		conventionPragma = newConventionPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__CONVENTION_PRAGMA, oldConventionPragma, newConventionPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConventionPragma(ConventionPragma newConventionPragma) {
		if (newConventionPragma != conventionPragma) {
			NotificationChain msgs = null;
			if (conventionPragma != null)
				msgs = ((InternalEObject)conventionPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__CONVENTION_PRAGMA, null, msgs);
			if (newConventionPragma != null)
				msgs = ((InternalEObject)newConventionPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__CONVENTION_PRAGMA, null, msgs);
			msgs = basicSetConventionPragma(newConventionPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__CONVENTION_PRAGMA, newConventionPragma, newConventionPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscardNamesPragma getDiscardNamesPragma() {
		return discardNamesPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscardNamesPragma(DiscardNamesPragma newDiscardNamesPragma, NotificationChain msgs) {
		DiscardNamesPragma oldDiscardNamesPragma = discardNamesPragma;
		discardNamesPragma = newDiscardNamesPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__DISCARD_NAMES_PRAGMA, oldDiscardNamesPragma, newDiscardNamesPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscardNamesPragma(DiscardNamesPragma newDiscardNamesPragma) {
		if (newDiscardNamesPragma != discardNamesPragma) {
			NotificationChain msgs = null;
			if (discardNamesPragma != null)
				msgs = ((InternalEObject)discardNamesPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__DISCARD_NAMES_PRAGMA, null, msgs);
			if (newDiscardNamesPragma != null)
				msgs = ((InternalEObject)newDiscardNamesPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__DISCARD_NAMES_PRAGMA, null, msgs);
			msgs = basicSetDiscardNamesPragma(newDiscardNamesPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__DISCARD_NAMES_PRAGMA, newDiscardNamesPragma, newDiscardNamesPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElaboratePragma getElaboratePragma() {
		return elaboratePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElaboratePragma(ElaboratePragma newElaboratePragma, NotificationChain msgs) {
		ElaboratePragma oldElaboratePragma = elaboratePragma;
		elaboratePragma = newElaboratePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__ELABORATE_PRAGMA, oldElaboratePragma, newElaboratePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElaboratePragma(ElaboratePragma newElaboratePragma) {
		if (newElaboratePragma != elaboratePragma) {
			NotificationChain msgs = null;
			if (elaboratePragma != null)
				msgs = ((InternalEObject)elaboratePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__ELABORATE_PRAGMA, null, msgs);
			if (newElaboratePragma != null)
				msgs = ((InternalEObject)newElaboratePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__ELABORATE_PRAGMA, null, msgs);
			msgs = basicSetElaboratePragma(newElaboratePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__ELABORATE_PRAGMA, newElaboratePragma, newElaboratePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElaborateAllPragma getElaborateAllPragma() {
		return elaborateAllPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElaborateAllPragma(ElaborateAllPragma newElaborateAllPragma, NotificationChain msgs) {
		ElaborateAllPragma oldElaborateAllPragma = elaborateAllPragma;
		elaborateAllPragma = newElaborateAllPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__ELABORATE_ALL_PRAGMA, oldElaborateAllPragma, newElaborateAllPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElaborateAllPragma(ElaborateAllPragma newElaborateAllPragma) {
		if (newElaborateAllPragma != elaborateAllPragma) {
			NotificationChain msgs = null;
			if (elaborateAllPragma != null)
				msgs = ((InternalEObject)elaborateAllPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__ELABORATE_ALL_PRAGMA, null, msgs);
			if (newElaborateAllPragma != null)
				msgs = ((InternalEObject)newElaborateAllPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__ELABORATE_ALL_PRAGMA, null, msgs);
			msgs = basicSetElaborateAllPragma(newElaborateAllPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__ELABORATE_ALL_PRAGMA, newElaborateAllPragma, newElaborateAllPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElaborateBodyPragma getElaborateBodyPragma() {
		return elaborateBodyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElaborateBodyPragma(ElaborateBodyPragma newElaborateBodyPragma, NotificationChain msgs) {
		ElaborateBodyPragma oldElaborateBodyPragma = elaborateBodyPragma;
		elaborateBodyPragma = newElaborateBodyPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__ELABORATE_BODY_PRAGMA, oldElaborateBodyPragma, newElaborateBodyPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElaborateBodyPragma(ElaborateBodyPragma newElaborateBodyPragma) {
		if (newElaborateBodyPragma != elaborateBodyPragma) {
			NotificationChain msgs = null;
			if (elaborateBodyPragma != null)
				msgs = ((InternalEObject)elaborateBodyPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__ELABORATE_BODY_PRAGMA, null, msgs);
			if (newElaborateBodyPragma != null)
				msgs = ((InternalEObject)newElaborateBodyPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__ELABORATE_BODY_PRAGMA, null, msgs);
			msgs = basicSetElaborateBodyPragma(newElaborateBodyPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__ELABORATE_BODY_PRAGMA, newElaborateBodyPragma, newElaborateBodyPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExportPragma getExportPragma() {
		return exportPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExportPragma(ExportPragma newExportPragma, NotificationChain msgs) {
		ExportPragma oldExportPragma = exportPragma;
		exportPragma = newExportPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__EXPORT_PRAGMA, oldExportPragma, newExportPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExportPragma(ExportPragma newExportPragma) {
		if (newExportPragma != exportPragma) {
			NotificationChain msgs = null;
			if (exportPragma != null)
				msgs = ((InternalEObject)exportPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__EXPORT_PRAGMA, null, msgs);
			if (newExportPragma != null)
				msgs = ((InternalEObject)newExportPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__EXPORT_PRAGMA, null, msgs);
			msgs = basicSetExportPragma(newExportPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__EXPORT_PRAGMA, newExportPragma, newExportPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImportPragma getImportPragma() {
		return importPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImportPragma(ImportPragma newImportPragma, NotificationChain msgs) {
		ImportPragma oldImportPragma = importPragma;
		importPragma = newImportPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__IMPORT_PRAGMA, oldImportPragma, newImportPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImportPragma(ImportPragma newImportPragma) {
		if (newImportPragma != importPragma) {
			NotificationChain msgs = null;
			if (importPragma != null)
				msgs = ((InternalEObject)importPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__IMPORT_PRAGMA, null, msgs);
			if (newImportPragma != null)
				msgs = ((InternalEObject)newImportPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__IMPORT_PRAGMA, null, msgs);
			msgs = basicSetImportPragma(newImportPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__IMPORT_PRAGMA, newImportPragma, newImportPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InlinePragma getInlinePragma() {
		return inlinePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInlinePragma(InlinePragma newInlinePragma, NotificationChain msgs) {
		InlinePragma oldInlinePragma = inlinePragma;
		inlinePragma = newInlinePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__INLINE_PRAGMA, oldInlinePragma, newInlinePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInlinePragma(InlinePragma newInlinePragma) {
		if (newInlinePragma != inlinePragma) {
			NotificationChain msgs = null;
			if (inlinePragma != null)
				msgs = ((InternalEObject)inlinePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__INLINE_PRAGMA, null, msgs);
			if (newInlinePragma != null)
				msgs = ((InternalEObject)newInlinePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__INLINE_PRAGMA, null, msgs);
			msgs = basicSetInlinePragma(newInlinePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__INLINE_PRAGMA, newInlinePragma, newInlinePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InspectionPointPragma getInspectionPointPragma() {
		return inspectionPointPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInspectionPointPragma(InspectionPointPragma newInspectionPointPragma, NotificationChain msgs) {
		InspectionPointPragma oldInspectionPointPragma = inspectionPointPragma;
		inspectionPointPragma = newInspectionPointPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__INSPECTION_POINT_PRAGMA, oldInspectionPointPragma, newInspectionPointPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInspectionPointPragma(InspectionPointPragma newInspectionPointPragma) {
		if (newInspectionPointPragma != inspectionPointPragma) {
			NotificationChain msgs = null;
			if (inspectionPointPragma != null)
				msgs = ((InternalEObject)inspectionPointPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__INSPECTION_POINT_PRAGMA, null, msgs);
			if (newInspectionPointPragma != null)
				msgs = ((InternalEObject)newInspectionPointPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__INSPECTION_POINT_PRAGMA, null, msgs);
			msgs = basicSetInspectionPointPragma(newInspectionPointPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__INSPECTION_POINT_PRAGMA, newInspectionPointPragma, newInspectionPointPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterruptHandlerPragma getInterruptHandlerPragma() {
		return interruptHandlerPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInterruptHandlerPragma(InterruptHandlerPragma newInterruptHandlerPragma, NotificationChain msgs) {
		InterruptHandlerPragma oldInterruptHandlerPragma = interruptHandlerPragma;
		interruptHandlerPragma = newInterruptHandlerPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__INTERRUPT_HANDLER_PRAGMA, oldInterruptHandlerPragma, newInterruptHandlerPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterruptHandlerPragma(InterruptHandlerPragma newInterruptHandlerPragma) {
		if (newInterruptHandlerPragma != interruptHandlerPragma) {
			NotificationChain msgs = null;
			if (interruptHandlerPragma != null)
				msgs = ((InternalEObject)interruptHandlerPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__INTERRUPT_HANDLER_PRAGMA, null, msgs);
			if (newInterruptHandlerPragma != null)
				msgs = ((InternalEObject)newInterruptHandlerPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__INTERRUPT_HANDLER_PRAGMA, null, msgs);
			msgs = basicSetInterruptHandlerPragma(newInterruptHandlerPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__INTERRUPT_HANDLER_PRAGMA, newInterruptHandlerPragma, newInterruptHandlerPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterruptPriorityPragma getInterruptPriorityPragma() {
		return interruptPriorityPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInterruptPriorityPragma(InterruptPriorityPragma newInterruptPriorityPragma, NotificationChain msgs) {
		InterruptPriorityPragma oldInterruptPriorityPragma = interruptPriorityPragma;
		interruptPriorityPragma = newInterruptPriorityPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__INTERRUPT_PRIORITY_PRAGMA, oldInterruptPriorityPragma, newInterruptPriorityPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterruptPriorityPragma(InterruptPriorityPragma newInterruptPriorityPragma) {
		if (newInterruptPriorityPragma != interruptPriorityPragma) {
			NotificationChain msgs = null;
			if (interruptPriorityPragma != null)
				msgs = ((InternalEObject)interruptPriorityPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__INTERRUPT_PRIORITY_PRAGMA, null, msgs);
			if (newInterruptPriorityPragma != null)
				msgs = ((InternalEObject)newInterruptPriorityPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__INTERRUPT_PRIORITY_PRAGMA, null, msgs);
			msgs = basicSetInterruptPriorityPragma(newInterruptPriorityPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__INTERRUPT_PRIORITY_PRAGMA, newInterruptPriorityPragma, newInterruptPriorityPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkerOptionsPragma getLinkerOptionsPragma() {
		return linkerOptionsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLinkerOptionsPragma(LinkerOptionsPragma newLinkerOptionsPragma, NotificationChain msgs) {
		LinkerOptionsPragma oldLinkerOptionsPragma = linkerOptionsPragma;
		linkerOptionsPragma = newLinkerOptionsPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__LINKER_OPTIONS_PRAGMA, oldLinkerOptionsPragma, newLinkerOptionsPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLinkerOptionsPragma(LinkerOptionsPragma newLinkerOptionsPragma) {
		if (newLinkerOptionsPragma != linkerOptionsPragma) {
			NotificationChain msgs = null;
			if (linkerOptionsPragma != null)
				msgs = ((InternalEObject)linkerOptionsPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__LINKER_OPTIONS_PRAGMA, null, msgs);
			if (newLinkerOptionsPragma != null)
				msgs = ((InternalEObject)newLinkerOptionsPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__LINKER_OPTIONS_PRAGMA, null, msgs);
			msgs = basicSetLinkerOptionsPragma(newLinkerOptionsPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__LINKER_OPTIONS_PRAGMA, newLinkerOptionsPragma, newLinkerOptionsPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ListPragma getListPragma() {
		return listPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetListPragma(ListPragma newListPragma, NotificationChain msgs) {
		ListPragma oldListPragma = listPragma;
		listPragma = newListPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__LIST_PRAGMA, oldListPragma, newListPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setListPragma(ListPragma newListPragma) {
		if (newListPragma != listPragma) {
			NotificationChain msgs = null;
			if (listPragma != null)
				msgs = ((InternalEObject)listPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__LIST_PRAGMA, null, msgs);
			if (newListPragma != null)
				msgs = ((InternalEObject)newListPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__LIST_PRAGMA, null, msgs);
			msgs = basicSetListPragma(newListPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__LIST_PRAGMA, newListPragma, newListPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LockingPolicyPragma getLockingPolicyPragma() {
		return lockingPolicyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLockingPolicyPragma(LockingPolicyPragma newLockingPolicyPragma, NotificationChain msgs) {
		LockingPolicyPragma oldLockingPolicyPragma = lockingPolicyPragma;
		lockingPolicyPragma = newLockingPolicyPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__LOCKING_POLICY_PRAGMA, oldLockingPolicyPragma, newLockingPolicyPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLockingPolicyPragma(LockingPolicyPragma newLockingPolicyPragma) {
		if (newLockingPolicyPragma != lockingPolicyPragma) {
			NotificationChain msgs = null;
			if (lockingPolicyPragma != null)
				msgs = ((InternalEObject)lockingPolicyPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__LOCKING_POLICY_PRAGMA, null, msgs);
			if (newLockingPolicyPragma != null)
				msgs = ((InternalEObject)newLockingPolicyPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__LOCKING_POLICY_PRAGMA, null, msgs);
			msgs = basicSetLockingPolicyPragma(newLockingPolicyPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__LOCKING_POLICY_PRAGMA, newLockingPolicyPragma, newLockingPolicyPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NormalizeScalarsPragma getNormalizeScalarsPragma() {
		return normalizeScalarsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNormalizeScalarsPragma(NormalizeScalarsPragma newNormalizeScalarsPragma, NotificationChain msgs) {
		NormalizeScalarsPragma oldNormalizeScalarsPragma = normalizeScalarsPragma;
		normalizeScalarsPragma = newNormalizeScalarsPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__NORMALIZE_SCALARS_PRAGMA, oldNormalizeScalarsPragma, newNormalizeScalarsPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNormalizeScalarsPragma(NormalizeScalarsPragma newNormalizeScalarsPragma) {
		if (newNormalizeScalarsPragma != normalizeScalarsPragma) {
			NotificationChain msgs = null;
			if (normalizeScalarsPragma != null)
				msgs = ((InternalEObject)normalizeScalarsPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__NORMALIZE_SCALARS_PRAGMA, null, msgs);
			if (newNormalizeScalarsPragma != null)
				msgs = ((InternalEObject)newNormalizeScalarsPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__NORMALIZE_SCALARS_PRAGMA, null, msgs);
			msgs = basicSetNormalizeScalarsPragma(newNormalizeScalarsPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__NORMALIZE_SCALARS_PRAGMA, newNormalizeScalarsPragma, newNormalizeScalarsPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OptimizePragma getOptimizePragma() {
		return optimizePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOptimizePragma(OptimizePragma newOptimizePragma, NotificationChain msgs) {
		OptimizePragma oldOptimizePragma = optimizePragma;
		optimizePragma = newOptimizePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__OPTIMIZE_PRAGMA, oldOptimizePragma, newOptimizePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOptimizePragma(OptimizePragma newOptimizePragma) {
		if (newOptimizePragma != optimizePragma) {
			NotificationChain msgs = null;
			if (optimizePragma != null)
				msgs = ((InternalEObject)optimizePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__OPTIMIZE_PRAGMA, null, msgs);
			if (newOptimizePragma != null)
				msgs = ((InternalEObject)newOptimizePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__OPTIMIZE_PRAGMA, null, msgs);
			msgs = basicSetOptimizePragma(newOptimizePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__OPTIMIZE_PRAGMA, newOptimizePragma, newOptimizePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackPragma getPackPragma() {
		return packPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPackPragma(PackPragma newPackPragma, NotificationChain msgs) {
		PackPragma oldPackPragma = packPragma;
		packPragma = newPackPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__PACK_PRAGMA, oldPackPragma, newPackPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPackPragma(PackPragma newPackPragma) {
		if (newPackPragma != packPragma) {
			NotificationChain msgs = null;
			if (packPragma != null)
				msgs = ((InternalEObject)packPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__PACK_PRAGMA, null, msgs);
			if (newPackPragma != null)
				msgs = ((InternalEObject)newPackPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__PACK_PRAGMA, null, msgs);
			msgs = basicSetPackPragma(newPackPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__PACK_PRAGMA, newPackPragma, newPackPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PagePragma getPagePragma() {
		return pagePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPagePragma(PagePragma newPagePragma, NotificationChain msgs) {
		PagePragma oldPagePragma = pagePragma;
		pagePragma = newPagePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__PAGE_PRAGMA, oldPagePragma, newPagePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPagePragma(PagePragma newPagePragma) {
		if (newPagePragma != pagePragma) {
			NotificationChain msgs = null;
			if (pagePragma != null)
				msgs = ((InternalEObject)pagePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__PAGE_PRAGMA, null, msgs);
			if (newPagePragma != null)
				msgs = ((InternalEObject)newPagePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__PAGE_PRAGMA, null, msgs);
			msgs = basicSetPagePragma(newPagePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__PAGE_PRAGMA, newPagePragma, newPagePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PreelaboratePragma getPreelaboratePragma() {
		return preelaboratePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPreelaboratePragma(PreelaboratePragma newPreelaboratePragma, NotificationChain msgs) {
		PreelaboratePragma oldPreelaboratePragma = preelaboratePragma;
		preelaboratePragma = newPreelaboratePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__PREELABORATE_PRAGMA, oldPreelaboratePragma, newPreelaboratePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreelaboratePragma(PreelaboratePragma newPreelaboratePragma) {
		if (newPreelaboratePragma != preelaboratePragma) {
			NotificationChain msgs = null;
			if (preelaboratePragma != null)
				msgs = ((InternalEObject)preelaboratePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__PREELABORATE_PRAGMA, null, msgs);
			if (newPreelaboratePragma != null)
				msgs = ((InternalEObject)newPreelaboratePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__PREELABORATE_PRAGMA, null, msgs);
			msgs = basicSetPreelaboratePragma(newPreelaboratePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__PREELABORATE_PRAGMA, newPreelaboratePragma, newPreelaboratePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PriorityPragma getPriorityPragma() {
		return priorityPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPriorityPragma(PriorityPragma newPriorityPragma, NotificationChain msgs) {
		PriorityPragma oldPriorityPragma = priorityPragma;
		priorityPragma = newPriorityPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__PRIORITY_PRAGMA, oldPriorityPragma, newPriorityPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPriorityPragma(PriorityPragma newPriorityPragma) {
		if (newPriorityPragma != priorityPragma) {
			NotificationChain msgs = null;
			if (priorityPragma != null)
				msgs = ((InternalEObject)priorityPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__PRIORITY_PRAGMA, null, msgs);
			if (newPriorityPragma != null)
				msgs = ((InternalEObject)newPriorityPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__PRIORITY_PRAGMA, null, msgs);
			msgs = basicSetPriorityPragma(newPriorityPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__PRIORITY_PRAGMA, newPriorityPragma, newPriorityPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PurePragma getPurePragma() {
		return purePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPurePragma(PurePragma newPurePragma, NotificationChain msgs) {
		PurePragma oldPurePragma = purePragma;
		purePragma = newPurePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__PURE_PRAGMA, oldPurePragma, newPurePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPurePragma(PurePragma newPurePragma) {
		if (newPurePragma != purePragma) {
			NotificationChain msgs = null;
			if (purePragma != null)
				msgs = ((InternalEObject)purePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__PURE_PRAGMA, null, msgs);
			if (newPurePragma != null)
				msgs = ((InternalEObject)newPurePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__PURE_PRAGMA, null, msgs);
			msgs = basicSetPurePragma(newPurePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__PURE_PRAGMA, newPurePragma, newPurePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QueuingPolicyPragma getQueuingPolicyPragma() {
		return queuingPolicyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetQueuingPolicyPragma(QueuingPolicyPragma newQueuingPolicyPragma, NotificationChain msgs) {
		QueuingPolicyPragma oldQueuingPolicyPragma = queuingPolicyPragma;
		queuingPolicyPragma = newQueuingPolicyPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__QUEUING_POLICY_PRAGMA, oldQueuingPolicyPragma, newQueuingPolicyPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQueuingPolicyPragma(QueuingPolicyPragma newQueuingPolicyPragma) {
		if (newQueuingPolicyPragma != queuingPolicyPragma) {
			NotificationChain msgs = null;
			if (queuingPolicyPragma != null)
				msgs = ((InternalEObject)queuingPolicyPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__QUEUING_POLICY_PRAGMA, null, msgs);
			if (newQueuingPolicyPragma != null)
				msgs = ((InternalEObject)newQueuingPolicyPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__QUEUING_POLICY_PRAGMA, null, msgs);
			msgs = basicSetQueuingPolicyPragma(newQueuingPolicyPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__QUEUING_POLICY_PRAGMA, newQueuingPolicyPragma, newQueuingPolicyPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RemoteCallInterfacePragma getRemoteCallInterfacePragma() {
		return remoteCallInterfacePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRemoteCallInterfacePragma(RemoteCallInterfacePragma newRemoteCallInterfacePragma, NotificationChain msgs) {
		RemoteCallInterfacePragma oldRemoteCallInterfacePragma = remoteCallInterfacePragma;
		remoteCallInterfacePragma = newRemoteCallInterfacePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA, oldRemoteCallInterfacePragma, newRemoteCallInterfacePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRemoteCallInterfacePragma(RemoteCallInterfacePragma newRemoteCallInterfacePragma) {
		if (newRemoteCallInterfacePragma != remoteCallInterfacePragma) {
			NotificationChain msgs = null;
			if (remoteCallInterfacePragma != null)
				msgs = ((InternalEObject)remoteCallInterfacePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA, null, msgs);
			if (newRemoteCallInterfacePragma != null)
				msgs = ((InternalEObject)newRemoteCallInterfacePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA, null, msgs);
			msgs = basicSetRemoteCallInterfacePragma(newRemoteCallInterfacePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA, newRemoteCallInterfacePragma, newRemoteCallInterfacePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RemoteTypesPragma getRemoteTypesPragma() {
		return remoteTypesPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRemoteTypesPragma(RemoteTypesPragma newRemoteTypesPragma, NotificationChain msgs) {
		RemoteTypesPragma oldRemoteTypesPragma = remoteTypesPragma;
		remoteTypesPragma = newRemoteTypesPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__REMOTE_TYPES_PRAGMA, oldRemoteTypesPragma, newRemoteTypesPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRemoteTypesPragma(RemoteTypesPragma newRemoteTypesPragma) {
		if (newRemoteTypesPragma != remoteTypesPragma) {
			NotificationChain msgs = null;
			if (remoteTypesPragma != null)
				msgs = ((InternalEObject)remoteTypesPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__REMOTE_TYPES_PRAGMA, null, msgs);
			if (newRemoteTypesPragma != null)
				msgs = ((InternalEObject)newRemoteTypesPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__REMOTE_TYPES_PRAGMA, null, msgs);
			msgs = basicSetRemoteTypesPragma(newRemoteTypesPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__REMOTE_TYPES_PRAGMA, newRemoteTypesPragma, newRemoteTypesPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RestrictionsPragma getRestrictionsPragma() {
		return restrictionsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRestrictionsPragma(RestrictionsPragma newRestrictionsPragma, NotificationChain msgs) {
		RestrictionsPragma oldRestrictionsPragma = restrictionsPragma;
		restrictionsPragma = newRestrictionsPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__RESTRICTIONS_PRAGMA, oldRestrictionsPragma, newRestrictionsPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRestrictionsPragma(RestrictionsPragma newRestrictionsPragma) {
		if (newRestrictionsPragma != restrictionsPragma) {
			NotificationChain msgs = null;
			if (restrictionsPragma != null)
				msgs = ((InternalEObject)restrictionsPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__RESTRICTIONS_PRAGMA, null, msgs);
			if (newRestrictionsPragma != null)
				msgs = ((InternalEObject)newRestrictionsPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__RESTRICTIONS_PRAGMA, null, msgs);
			msgs = basicSetRestrictionsPragma(newRestrictionsPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__RESTRICTIONS_PRAGMA, newRestrictionsPragma, newRestrictionsPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReviewablePragma getReviewablePragma() {
		return reviewablePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReviewablePragma(ReviewablePragma newReviewablePragma, NotificationChain msgs) {
		ReviewablePragma oldReviewablePragma = reviewablePragma;
		reviewablePragma = newReviewablePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__REVIEWABLE_PRAGMA, oldReviewablePragma, newReviewablePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReviewablePragma(ReviewablePragma newReviewablePragma) {
		if (newReviewablePragma != reviewablePragma) {
			NotificationChain msgs = null;
			if (reviewablePragma != null)
				msgs = ((InternalEObject)reviewablePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__REVIEWABLE_PRAGMA, null, msgs);
			if (newReviewablePragma != null)
				msgs = ((InternalEObject)newReviewablePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__REVIEWABLE_PRAGMA, null, msgs);
			msgs = basicSetReviewablePragma(newReviewablePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__REVIEWABLE_PRAGMA, newReviewablePragma, newReviewablePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SharedPassivePragma getSharedPassivePragma() {
		return sharedPassivePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSharedPassivePragma(SharedPassivePragma newSharedPassivePragma, NotificationChain msgs) {
		SharedPassivePragma oldSharedPassivePragma = sharedPassivePragma;
		sharedPassivePragma = newSharedPassivePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__SHARED_PASSIVE_PRAGMA, oldSharedPassivePragma, newSharedPassivePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSharedPassivePragma(SharedPassivePragma newSharedPassivePragma) {
		if (newSharedPassivePragma != sharedPassivePragma) {
			NotificationChain msgs = null;
			if (sharedPassivePragma != null)
				msgs = ((InternalEObject)sharedPassivePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__SHARED_PASSIVE_PRAGMA, null, msgs);
			if (newSharedPassivePragma != null)
				msgs = ((InternalEObject)newSharedPassivePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__SHARED_PASSIVE_PRAGMA, null, msgs);
			msgs = basicSetSharedPassivePragma(newSharedPassivePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__SHARED_PASSIVE_PRAGMA, newSharedPassivePragma, newSharedPassivePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StorageSizePragma getStorageSizePragma() {
		return storageSizePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStorageSizePragma(StorageSizePragma newStorageSizePragma, NotificationChain msgs) {
		StorageSizePragma oldStorageSizePragma = storageSizePragma;
		storageSizePragma = newStorageSizePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__STORAGE_SIZE_PRAGMA, oldStorageSizePragma, newStorageSizePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStorageSizePragma(StorageSizePragma newStorageSizePragma) {
		if (newStorageSizePragma != storageSizePragma) {
			NotificationChain msgs = null;
			if (storageSizePragma != null)
				msgs = ((InternalEObject)storageSizePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__STORAGE_SIZE_PRAGMA, null, msgs);
			if (newStorageSizePragma != null)
				msgs = ((InternalEObject)newStorageSizePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__STORAGE_SIZE_PRAGMA, null, msgs);
			msgs = basicSetStorageSizePragma(newStorageSizePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__STORAGE_SIZE_PRAGMA, newStorageSizePragma, newStorageSizePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SuppressPragma getSuppressPragma() {
		return suppressPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSuppressPragma(SuppressPragma newSuppressPragma, NotificationChain msgs) {
		SuppressPragma oldSuppressPragma = suppressPragma;
		suppressPragma = newSuppressPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__SUPPRESS_PRAGMA, oldSuppressPragma, newSuppressPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuppressPragma(SuppressPragma newSuppressPragma) {
		if (newSuppressPragma != suppressPragma) {
			NotificationChain msgs = null;
			if (suppressPragma != null)
				msgs = ((InternalEObject)suppressPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__SUPPRESS_PRAGMA, null, msgs);
			if (newSuppressPragma != null)
				msgs = ((InternalEObject)newSuppressPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__SUPPRESS_PRAGMA, null, msgs);
			msgs = basicSetSuppressPragma(newSuppressPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__SUPPRESS_PRAGMA, newSuppressPragma, newSuppressPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskDispatchingPolicyPragma getTaskDispatchingPolicyPragma() {
		return taskDispatchingPolicyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTaskDispatchingPolicyPragma(TaskDispatchingPolicyPragma newTaskDispatchingPolicyPragma, NotificationChain msgs) {
		TaskDispatchingPolicyPragma oldTaskDispatchingPolicyPragma = taskDispatchingPolicyPragma;
		taskDispatchingPolicyPragma = newTaskDispatchingPolicyPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA, oldTaskDispatchingPolicyPragma, newTaskDispatchingPolicyPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaskDispatchingPolicyPragma(TaskDispatchingPolicyPragma newTaskDispatchingPolicyPragma) {
		if (newTaskDispatchingPolicyPragma != taskDispatchingPolicyPragma) {
			NotificationChain msgs = null;
			if (taskDispatchingPolicyPragma != null)
				msgs = ((InternalEObject)taskDispatchingPolicyPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA, null, msgs);
			if (newTaskDispatchingPolicyPragma != null)
				msgs = ((InternalEObject)newTaskDispatchingPolicyPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA, null, msgs);
			msgs = basicSetTaskDispatchingPolicyPragma(newTaskDispatchingPolicyPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA, newTaskDispatchingPolicyPragma, newTaskDispatchingPolicyPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VolatilePragma getVolatilePragma() {
		return volatilePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVolatilePragma(VolatilePragma newVolatilePragma, NotificationChain msgs) {
		VolatilePragma oldVolatilePragma = volatilePragma;
		volatilePragma = newVolatilePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__VOLATILE_PRAGMA, oldVolatilePragma, newVolatilePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVolatilePragma(VolatilePragma newVolatilePragma) {
		if (newVolatilePragma != volatilePragma) {
			NotificationChain msgs = null;
			if (volatilePragma != null)
				msgs = ((InternalEObject)volatilePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__VOLATILE_PRAGMA, null, msgs);
			if (newVolatilePragma != null)
				msgs = ((InternalEObject)newVolatilePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__VOLATILE_PRAGMA, null, msgs);
			msgs = basicSetVolatilePragma(newVolatilePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__VOLATILE_PRAGMA, newVolatilePragma, newVolatilePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VolatileComponentsPragma getVolatileComponentsPragma() {
		return volatileComponentsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVolatileComponentsPragma(VolatileComponentsPragma newVolatileComponentsPragma, NotificationChain msgs) {
		VolatileComponentsPragma oldVolatileComponentsPragma = volatileComponentsPragma;
		volatileComponentsPragma = newVolatileComponentsPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__VOLATILE_COMPONENTS_PRAGMA, oldVolatileComponentsPragma, newVolatileComponentsPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVolatileComponentsPragma(VolatileComponentsPragma newVolatileComponentsPragma) {
		if (newVolatileComponentsPragma != volatileComponentsPragma) {
			NotificationChain msgs = null;
			if (volatileComponentsPragma != null)
				msgs = ((InternalEObject)volatileComponentsPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__VOLATILE_COMPONENTS_PRAGMA, null, msgs);
			if (newVolatileComponentsPragma != null)
				msgs = ((InternalEObject)newVolatileComponentsPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__VOLATILE_COMPONENTS_PRAGMA, null, msgs);
			msgs = basicSetVolatileComponentsPragma(newVolatileComponentsPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__VOLATILE_COMPONENTS_PRAGMA, newVolatileComponentsPragma, newVolatileComponentsPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertPragma getAssertPragma() {
		return assertPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssertPragma(AssertPragma newAssertPragma, NotificationChain msgs) {
		AssertPragma oldAssertPragma = assertPragma;
		assertPragma = newAssertPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__ASSERT_PRAGMA, oldAssertPragma, newAssertPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssertPragma(AssertPragma newAssertPragma) {
		if (newAssertPragma != assertPragma) {
			NotificationChain msgs = null;
			if (assertPragma != null)
				msgs = ((InternalEObject)assertPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__ASSERT_PRAGMA, null, msgs);
			if (newAssertPragma != null)
				msgs = ((InternalEObject)newAssertPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__ASSERT_PRAGMA, null, msgs);
			msgs = basicSetAssertPragma(newAssertPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__ASSERT_PRAGMA, newAssertPragma, newAssertPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertionPolicyPragma getAssertionPolicyPragma() {
		return assertionPolicyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssertionPolicyPragma(AssertionPolicyPragma newAssertionPolicyPragma, NotificationChain msgs) {
		AssertionPolicyPragma oldAssertionPolicyPragma = assertionPolicyPragma;
		assertionPolicyPragma = newAssertionPolicyPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__ASSERTION_POLICY_PRAGMA, oldAssertionPolicyPragma, newAssertionPolicyPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssertionPolicyPragma(AssertionPolicyPragma newAssertionPolicyPragma) {
		if (newAssertionPolicyPragma != assertionPolicyPragma) {
			NotificationChain msgs = null;
			if (assertionPolicyPragma != null)
				msgs = ((InternalEObject)assertionPolicyPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__ASSERTION_POLICY_PRAGMA, null, msgs);
			if (newAssertionPolicyPragma != null)
				msgs = ((InternalEObject)newAssertionPolicyPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__ASSERTION_POLICY_PRAGMA, null, msgs);
			msgs = basicSetAssertionPolicyPragma(newAssertionPolicyPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__ASSERTION_POLICY_PRAGMA, newAssertionPolicyPragma, newAssertionPolicyPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DetectBlockingPragma getDetectBlockingPragma() {
		return detectBlockingPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDetectBlockingPragma(DetectBlockingPragma newDetectBlockingPragma, NotificationChain msgs) {
		DetectBlockingPragma oldDetectBlockingPragma = detectBlockingPragma;
		detectBlockingPragma = newDetectBlockingPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__DETECT_BLOCKING_PRAGMA, oldDetectBlockingPragma, newDetectBlockingPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDetectBlockingPragma(DetectBlockingPragma newDetectBlockingPragma) {
		if (newDetectBlockingPragma != detectBlockingPragma) {
			NotificationChain msgs = null;
			if (detectBlockingPragma != null)
				msgs = ((InternalEObject)detectBlockingPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__DETECT_BLOCKING_PRAGMA, null, msgs);
			if (newDetectBlockingPragma != null)
				msgs = ((InternalEObject)newDetectBlockingPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__DETECT_BLOCKING_PRAGMA, null, msgs);
			msgs = basicSetDetectBlockingPragma(newDetectBlockingPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__DETECT_BLOCKING_PRAGMA, newDetectBlockingPragma, newDetectBlockingPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NoReturnPragma getNoReturnPragma() {
		return noReturnPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNoReturnPragma(NoReturnPragma newNoReturnPragma, NotificationChain msgs) {
		NoReturnPragma oldNoReturnPragma = noReturnPragma;
		noReturnPragma = newNoReturnPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__NO_RETURN_PRAGMA, oldNoReturnPragma, newNoReturnPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNoReturnPragma(NoReturnPragma newNoReturnPragma) {
		if (newNoReturnPragma != noReturnPragma) {
			NotificationChain msgs = null;
			if (noReturnPragma != null)
				msgs = ((InternalEObject)noReturnPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__NO_RETURN_PRAGMA, null, msgs);
			if (newNoReturnPragma != null)
				msgs = ((InternalEObject)newNoReturnPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__NO_RETURN_PRAGMA, null, msgs);
			msgs = basicSetNoReturnPragma(newNoReturnPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__NO_RETURN_PRAGMA, newNoReturnPragma, newNoReturnPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PartitionElaborationPolicyPragma getPartitionElaborationPolicyPragma() {
		return partitionElaborationPolicyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPartitionElaborationPolicyPragma(PartitionElaborationPolicyPragma newPartitionElaborationPolicyPragma, NotificationChain msgs) {
		PartitionElaborationPolicyPragma oldPartitionElaborationPolicyPragma = partitionElaborationPolicyPragma;
		partitionElaborationPolicyPragma = newPartitionElaborationPolicyPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA, oldPartitionElaborationPolicyPragma, newPartitionElaborationPolicyPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPartitionElaborationPolicyPragma(PartitionElaborationPolicyPragma newPartitionElaborationPolicyPragma) {
		if (newPartitionElaborationPolicyPragma != partitionElaborationPolicyPragma) {
			NotificationChain msgs = null;
			if (partitionElaborationPolicyPragma != null)
				msgs = ((InternalEObject)partitionElaborationPolicyPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA, null, msgs);
			if (newPartitionElaborationPolicyPragma != null)
				msgs = ((InternalEObject)newPartitionElaborationPolicyPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA, null, msgs);
			msgs = basicSetPartitionElaborationPolicyPragma(newPartitionElaborationPolicyPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA, newPartitionElaborationPolicyPragma, newPartitionElaborationPolicyPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PreelaborableInitializationPragma getPreelaborableInitializationPragma() {
		return preelaborableInitializationPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPreelaborableInitializationPragma(PreelaborableInitializationPragma newPreelaborableInitializationPragma, NotificationChain msgs) {
		PreelaborableInitializationPragma oldPreelaborableInitializationPragma = preelaborableInitializationPragma;
		preelaborableInitializationPragma = newPreelaborableInitializationPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA, oldPreelaborableInitializationPragma, newPreelaborableInitializationPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreelaborableInitializationPragma(PreelaborableInitializationPragma newPreelaborableInitializationPragma) {
		if (newPreelaborableInitializationPragma != preelaborableInitializationPragma) {
			NotificationChain msgs = null;
			if (preelaborableInitializationPragma != null)
				msgs = ((InternalEObject)preelaborableInitializationPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA, null, msgs);
			if (newPreelaborableInitializationPragma != null)
				msgs = ((InternalEObject)newPreelaborableInitializationPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA, null, msgs);
			msgs = basicSetPreelaborableInitializationPragma(newPreelaborableInitializationPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA, newPreelaborableInitializationPragma, newPreelaborableInitializationPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrioritySpecificDispatchingPragma getPrioritySpecificDispatchingPragma() {
		return prioritySpecificDispatchingPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrioritySpecificDispatchingPragma(PrioritySpecificDispatchingPragma newPrioritySpecificDispatchingPragma, NotificationChain msgs) {
		PrioritySpecificDispatchingPragma oldPrioritySpecificDispatchingPragma = prioritySpecificDispatchingPragma;
		prioritySpecificDispatchingPragma = newPrioritySpecificDispatchingPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA, oldPrioritySpecificDispatchingPragma, newPrioritySpecificDispatchingPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrioritySpecificDispatchingPragma(PrioritySpecificDispatchingPragma newPrioritySpecificDispatchingPragma) {
		if (newPrioritySpecificDispatchingPragma != prioritySpecificDispatchingPragma) {
			NotificationChain msgs = null;
			if (prioritySpecificDispatchingPragma != null)
				msgs = ((InternalEObject)prioritySpecificDispatchingPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA, null, msgs);
			if (newPrioritySpecificDispatchingPragma != null)
				msgs = ((InternalEObject)newPrioritySpecificDispatchingPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA, null, msgs);
			msgs = basicSetPrioritySpecificDispatchingPragma(newPrioritySpecificDispatchingPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA, newPrioritySpecificDispatchingPragma, newPrioritySpecificDispatchingPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProfilePragma getProfilePragma() {
		return profilePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProfilePragma(ProfilePragma newProfilePragma, NotificationChain msgs) {
		ProfilePragma oldProfilePragma = profilePragma;
		profilePragma = newProfilePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__PROFILE_PRAGMA, oldProfilePragma, newProfilePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProfilePragma(ProfilePragma newProfilePragma) {
		if (newProfilePragma != profilePragma) {
			NotificationChain msgs = null;
			if (profilePragma != null)
				msgs = ((InternalEObject)profilePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__PROFILE_PRAGMA, null, msgs);
			if (newProfilePragma != null)
				msgs = ((InternalEObject)newProfilePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__PROFILE_PRAGMA, null, msgs);
			msgs = basicSetProfilePragma(newProfilePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__PROFILE_PRAGMA, newProfilePragma, newProfilePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelativeDeadlinePragma getRelativeDeadlinePragma() {
		return relativeDeadlinePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRelativeDeadlinePragma(RelativeDeadlinePragma newRelativeDeadlinePragma, NotificationChain msgs) {
		RelativeDeadlinePragma oldRelativeDeadlinePragma = relativeDeadlinePragma;
		relativeDeadlinePragma = newRelativeDeadlinePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__RELATIVE_DEADLINE_PRAGMA, oldRelativeDeadlinePragma, newRelativeDeadlinePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRelativeDeadlinePragma(RelativeDeadlinePragma newRelativeDeadlinePragma) {
		if (newRelativeDeadlinePragma != relativeDeadlinePragma) {
			NotificationChain msgs = null;
			if (relativeDeadlinePragma != null)
				msgs = ((InternalEObject)relativeDeadlinePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__RELATIVE_DEADLINE_PRAGMA, null, msgs);
			if (newRelativeDeadlinePragma != null)
				msgs = ((InternalEObject)newRelativeDeadlinePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__RELATIVE_DEADLINE_PRAGMA, null, msgs);
			msgs = basicSetRelativeDeadlinePragma(newRelativeDeadlinePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__RELATIVE_DEADLINE_PRAGMA, newRelativeDeadlinePragma, newRelativeDeadlinePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UncheckedUnionPragma getUncheckedUnionPragma() {
		return uncheckedUnionPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUncheckedUnionPragma(UncheckedUnionPragma newUncheckedUnionPragma, NotificationChain msgs) {
		UncheckedUnionPragma oldUncheckedUnionPragma = uncheckedUnionPragma;
		uncheckedUnionPragma = newUncheckedUnionPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__UNCHECKED_UNION_PRAGMA, oldUncheckedUnionPragma, newUncheckedUnionPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUncheckedUnionPragma(UncheckedUnionPragma newUncheckedUnionPragma) {
		if (newUncheckedUnionPragma != uncheckedUnionPragma) {
			NotificationChain msgs = null;
			if (uncheckedUnionPragma != null)
				msgs = ((InternalEObject)uncheckedUnionPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__UNCHECKED_UNION_PRAGMA, null, msgs);
			if (newUncheckedUnionPragma != null)
				msgs = ((InternalEObject)newUncheckedUnionPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__UNCHECKED_UNION_PRAGMA, null, msgs);
			msgs = basicSetUncheckedUnionPragma(newUncheckedUnionPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__UNCHECKED_UNION_PRAGMA, newUncheckedUnionPragma, newUncheckedUnionPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnsuppressPragma getUnsuppressPragma() {
		return unsuppressPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnsuppressPragma(UnsuppressPragma newUnsuppressPragma, NotificationChain msgs) {
		UnsuppressPragma oldUnsuppressPragma = unsuppressPragma;
		unsuppressPragma = newUnsuppressPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__UNSUPPRESS_PRAGMA, oldUnsuppressPragma, newUnsuppressPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnsuppressPragma(UnsuppressPragma newUnsuppressPragma) {
		if (newUnsuppressPragma != unsuppressPragma) {
			NotificationChain msgs = null;
			if (unsuppressPragma != null)
				msgs = ((InternalEObject)unsuppressPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__UNSUPPRESS_PRAGMA, null, msgs);
			if (newUnsuppressPragma != null)
				msgs = ((InternalEObject)newUnsuppressPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__UNSUPPRESS_PRAGMA, null, msgs);
			msgs = basicSetUnsuppressPragma(newUnsuppressPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__UNSUPPRESS_PRAGMA, newUnsuppressPragma, newUnsuppressPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefaultStoragePoolPragma getDefaultStoragePoolPragma() {
		return defaultStoragePoolPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefaultStoragePoolPragma(DefaultStoragePoolPragma newDefaultStoragePoolPragma, NotificationChain msgs) {
		DefaultStoragePoolPragma oldDefaultStoragePoolPragma = defaultStoragePoolPragma;
		defaultStoragePoolPragma = newDefaultStoragePoolPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA, oldDefaultStoragePoolPragma, newDefaultStoragePoolPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultStoragePoolPragma(DefaultStoragePoolPragma newDefaultStoragePoolPragma) {
		if (newDefaultStoragePoolPragma != defaultStoragePoolPragma) {
			NotificationChain msgs = null;
			if (defaultStoragePoolPragma != null)
				msgs = ((InternalEObject)defaultStoragePoolPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA, null, msgs);
			if (newDefaultStoragePoolPragma != null)
				msgs = ((InternalEObject)newDefaultStoragePoolPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA, null, msgs);
			msgs = basicSetDefaultStoragePoolPragma(newDefaultStoragePoolPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA, newDefaultStoragePoolPragma, newDefaultStoragePoolPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DispatchingDomainPragma getDispatchingDomainPragma() {
		return dispatchingDomainPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDispatchingDomainPragma(DispatchingDomainPragma newDispatchingDomainPragma, NotificationChain msgs) {
		DispatchingDomainPragma oldDispatchingDomainPragma = dispatchingDomainPragma;
		dispatchingDomainPragma = newDispatchingDomainPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__DISPATCHING_DOMAIN_PRAGMA, oldDispatchingDomainPragma, newDispatchingDomainPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDispatchingDomainPragma(DispatchingDomainPragma newDispatchingDomainPragma) {
		if (newDispatchingDomainPragma != dispatchingDomainPragma) {
			NotificationChain msgs = null;
			if (dispatchingDomainPragma != null)
				msgs = ((InternalEObject)dispatchingDomainPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__DISPATCHING_DOMAIN_PRAGMA, null, msgs);
			if (newDispatchingDomainPragma != null)
				msgs = ((InternalEObject)newDispatchingDomainPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__DISPATCHING_DOMAIN_PRAGMA, null, msgs);
			msgs = basicSetDispatchingDomainPragma(newDispatchingDomainPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__DISPATCHING_DOMAIN_PRAGMA, newDispatchingDomainPragma, newDispatchingDomainPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CpuPragma getCpuPragma() {
		return cpuPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCpuPragma(CpuPragma newCpuPragma, NotificationChain msgs) {
		CpuPragma oldCpuPragma = cpuPragma;
		cpuPragma = newCpuPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__CPU_PRAGMA, oldCpuPragma, newCpuPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCpuPragma(CpuPragma newCpuPragma) {
		if (newCpuPragma != cpuPragma) {
			NotificationChain msgs = null;
			if (cpuPragma != null)
				msgs = ((InternalEObject)cpuPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__CPU_PRAGMA, null, msgs);
			if (newCpuPragma != null)
				msgs = ((InternalEObject)newCpuPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__CPU_PRAGMA, null, msgs);
			msgs = basicSetCpuPragma(newCpuPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__CPU_PRAGMA, newCpuPragma, newCpuPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IndependentPragma getIndependentPragma() {
		return independentPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIndependentPragma(IndependentPragma newIndependentPragma, NotificationChain msgs) {
		IndependentPragma oldIndependentPragma = independentPragma;
		independentPragma = newIndependentPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__INDEPENDENT_PRAGMA, oldIndependentPragma, newIndependentPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndependentPragma(IndependentPragma newIndependentPragma) {
		if (newIndependentPragma != independentPragma) {
			NotificationChain msgs = null;
			if (independentPragma != null)
				msgs = ((InternalEObject)independentPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__INDEPENDENT_PRAGMA, null, msgs);
			if (newIndependentPragma != null)
				msgs = ((InternalEObject)newIndependentPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__INDEPENDENT_PRAGMA, null, msgs);
			msgs = basicSetIndependentPragma(newIndependentPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__INDEPENDENT_PRAGMA, newIndependentPragma, newIndependentPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IndependentComponentsPragma getIndependentComponentsPragma() {
		return independentComponentsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIndependentComponentsPragma(IndependentComponentsPragma newIndependentComponentsPragma, NotificationChain msgs) {
		IndependentComponentsPragma oldIndependentComponentsPragma = independentComponentsPragma;
		independentComponentsPragma = newIndependentComponentsPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA, oldIndependentComponentsPragma, newIndependentComponentsPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndependentComponentsPragma(IndependentComponentsPragma newIndependentComponentsPragma) {
		if (newIndependentComponentsPragma != independentComponentsPragma) {
			NotificationChain msgs = null;
			if (independentComponentsPragma != null)
				msgs = ((InternalEObject)independentComponentsPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA, null, msgs);
			if (newIndependentComponentsPragma != null)
				msgs = ((InternalEObject)newIndependentComponentsPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA, null, msgs);
			msgs = basicSetIndependentComponentsPragma(newIndependentComponentsPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA, newIndependentComponentsPragma, newIndependentComponentsPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImplementationDefinedPragma getImplementationDefinedPragma() {
		return implementationDefinedPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImplementationDefinedPragma(ImplementationDefinedPragma newImplementationDefinedPragma, NotificationChain msgs) {
		ImplementationDefinedPragma oldImplementationDefinedPragma = implementationDefinedPragma;
		implementationDefinedPragma = newImplementationDefinedPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA, oldImplementationDefinedPragma, newImplementationDefinedPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImplementationDefinedPragma(ImplementationDefinedPragma newImplementationDefinedPragma) {
		if (newImplementationDefinedPragma != implementationDefinedPragma) {
			NotificationChain msgs = null;
			if (implementationDefinedPragma != null)
				msgs = ((InternalEObject)implementationDefinedPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA, null, msgs);
			if (newImplementationDefinedPragma != null)
				msgs = ((InternalEObject)newImplementationDefinedPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA, null, msgs);
			msgs = basicSetImplementationDefinedPragma(newImplementationDefinedPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA, newImplementationDefinedPragma, newImplementationDefinedPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnknownPragma getUnknownPragma() {
		return unknownPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnknownPragma(UnknownPragma newUnknownPragma, NotificationChain msgs) {
		UnknownPragma oldUnknownPragma = unknownPragma;
		unknownPragma = newUnknownPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__UNKNOWN_PRAGMA, oldUnknownPragma, newUnknownPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnknownPragma(UnknownPragma newUnknownPragma) {
		if (newUnknownPragma != unknownPragma) {
			NotificationChain msgs = null;
			if (unknownPragma != null)
				msgs = ((InternalEObject)unknownPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__UNKNOWN_PRAGMA, null, msgs);
			if (newUnknownPragma != null)
				msgs = ((InternalEObject)newUnknownPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASSOCIATION_CLASS__UNKNOWN_PRAGMA, null, msgs);
			msgs = basicSetUnknownPragma(newUnknownPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASSOCIATION_CLASS__UNKNOWN_PRAGMA, newUnknownPragma, newUnknownPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.ASSOCIATION_CLASS__NOT_AN_ELEMENT:
				return basicSetNotAnElement(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__PRAGMA_ARGUMENT_ASSOCIATION:
				return basicSetPragmaArgumentAssociation(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__DISCRIMINANT_ASSOCIATION:
				return basicSetDiscriminantAssociation(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__RECORD_COMPONENT_ASSOCIATION:
				return basicSetRecordComponentAssociation(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__ARRAY_COMPONENT_ASSOCIATION:
				return basicSetArrayComponentAssociation(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__PARAMETER_ASSOCIATION:
				return basicSetParameterAssociation(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__GENERIC_ASSOCIATION:
				return basicSetGenericAssociation(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__COMMENT:
				return basicSetComment(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__ALL_CALLS_REMOTE_PRAGMA:
				return basicSetAllCallsRemotePragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__ASYNCHRONOUS_PRAGMA:
				return basicSetAsynchronousPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__ATOMIC_PRAGMA:
				return basicSetAtomicPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__ATOMIC_COMPONENTS_PRAGMA:
				return basicSetAtomicComponentsPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__ATTACH_HANDLER_PRAGMA:
				return basicSetAttachHandlerPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__CONTROLLED_PRAGMA:
				return basicSetControlledPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__CONVENTION_PRAGMA:
				return basicSetConventionPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__DISCARD_NAMES_PRAGMA:
				return basicSetDiscardNamesPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__ELABORATE_PRAGMA:
				return basicSetElaboratePragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__ELABORATE_ALL_PRAGMA:
				return basicSetElaborateAllPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__ELABORATE_BODY_PRAGMA:
				return basicSetElaborateBodyPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__EXPORT_PRAGMA:
				return basicSetExportPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__IMPORT_PRAGMA:
				return basicSetImportPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__INLINE_PRAGMA:
				return basicSetInlinePragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__INSPECTION_POINT_PRAGMA:
				return basicSetInspectionPointPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__INTERRUPT_HANDLER_PRAGMA:
				return basicSetInterruptHandlerPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__INTERRUPT_PRIORITY_PRAGMA:
				return basicSetInterruptPriorityPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__LINKER_OPTIONS_PRAGMA:
				return basicSetLinkerOptionsPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__LIST_PRAGMA:
				return basicSetListPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__LOCKING_POLICY_PRAGMA:
				return basicSetLockingPolicyPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__NORMALIZE_SCALARS_PRAGMA:
				return basicSetNormalizeScalarsPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__OPTIMIZE_PRAGMA:
				return basicSetOptimizePragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__PACK_PRAGMA:
				return basicSetPackPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__PAGE_PRAGMA:
				return basicSetPagePragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__PREELABORATE_PRAGMA:
				return basicSetPreelaboratePragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__PRIORITY_PRAGMA:
				return basicSetPriorityPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__PURE_PRAGMA:
				return basicSetPurePragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__QUEUING_POLICY_PRAGMA:
				return basicSetQueuingPolicyPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA:
				return basicSetRemoteCallInterfacePragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__REMOTE_TYPES_PRAGMA:
				return basicSetRemoteTypesPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__RESTRICTIONS_PRAGMA:
				return basicSetRestrictionsPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__REVIEWABLE_PRAGMA:
				return basicSetReviewablePragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__SHARED_PASSIVE_PRAGMA:
				return basicSetSharedPassivePragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__STORAGE_SIZE_PRAGMA:
				return basicSetStorageSizePragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__SUPPRESS_PRAGMA:
				return basicSetSuppressPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA:
				return basicSetTaskDispatchingPolicyPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__VOLATILE_PRAGMA:
				return basicSetVolatilePragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__VOLATILE_COMPONENTS_PRAGMA:
				return basicSetVolatileComponentsPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__ASSERT_PRAGMA:
				return basicSetAssertPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__ASSERTION_POLICY_PRAGMA:
				return basicSetAssertionPolicyPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__DETECT_BLOCKING_PRAGMA:
				return basicSetDetectBlockingPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__NO_RETURN_PRAGMA:
				return basicSetNoReturnPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA:
				return basicSetPartitionElaborationPolicyPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA:
				return basicSetPreelaborableInitializationPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return basicSetPrioritySpecificDispatchingPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__PROFILE_PRAGMA:
				return basicSetProfilePragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__RELATIVE_DEADLINE_PRAGMA:
				return basicSetRelativeDeadlinePragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__UNCHECKED_UNION_PRAGMA:
				return basicSetUncheckedUnionPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__UNSUPPRESS_PRAGMA:
				return basicSetUnsuppressPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA:
				return basicSetDefaultStoragePoolPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__DISPATCHING_DOMAIN_PRAGMA:
				return basicSetDispatchingDomainPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__CPU_PRAGMA:
				return basicSetCpuPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__INDEPENDENT_PRAGMA:
				return basicSetIndependentPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA:
				return basicSetIndependentComponentsPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA:
				return basicSetImplementationDefinedPragma(null, msgs);
			case AdaPackage.ASSOCIATION_CLASS__UNKNOWN_PRAGMA:
				return basicSetUnknownPragma(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.ASSOCIATION_CLASS__NOT_AN_ELEMENT:
				return getNotAnElement();
			case AdaPackage.ASSOCIATION_CLASS__PRAGMA_ARGUMENT_ASSOCIATION:
				return getPragmaArgumentAssociation();
			case AdaPackage.ASSOCIATION_CLASS__DISCRIMINANT_ASSOCIATION:
				return getDiscriminantAssociation();
			case AdaPackage.ASSOCIATION_CLASS__RECORD_COMPONENT_ASSOCIATION:
				return getRecordComponentAssociation();
			case AdaPackage.ASSOCIATION_CLASS__ARRAY_COMPONENT_ASSOCIATION:
				return getArrayComponentAssociation();
			case AdaPackage.ASSOCIATION_CLASS__PARAMETER_ASSOCIATION:
				return getParameterAssociation();
			case AdaPackage.ASSOCIATION_CLASS__GENERIC_ASSOCIATION:
				return getGenericAssociation();
			case AdaPackage.ASSOCIATION_CLASS__COMMENT:
				return getComment();
			case AdaPackage.ASSOCIATION_CLASS__ALL_CALLS_REMOTE_PRAGMA:
				return getAllCallsRemotePragma();
			case AdaPackage.ASSOCIATION_CLASS__ASYNCHRONOUS_PRAGMA:
				return getAsynchronousPragma();
			case AdaPackage.ASSOCIATION_CLASS__ATOMIC_PRAGMA:
				return getAtomicPragma();
			case AdaPackage.ASSOCIATION_CLASS__ATOMIC_COMPONENTS_PRAGMA:
				return getAtomicComponentsPragma();
			case AdaPackage.ASSOCIATION_CLASS__ATTACH_HANDLER_PRAGMA:
				return getAttachHandlerPragma();
			case AdaPackage.ASSOCIATION_CLASS__CONTROLLED_PRAGMA:
				return getControlledPragma();
			case AdaPackage.ASSOCIATION_CLASS__CONVENTION_PRAGMA:
				return getConventionPragma();
			case AdaPackage.ASSOCIATION_CLASS__DISCARD_NAMES_PRAGMA:
				return getDiscardNamesPragma();
			case AdaPackage.ASSOCIATION_CLASS__ELABORATE_PRAGMA:
				return getElaboratePragma();
			case AdaPackage.ASSOCIATION_CLASS__ELABORATE_ALL_PRAGMA:
				return getElaborateAllPragma();
			case AdaPackage.ASSOCIATION_CLASS__ELABORATE_BODY_PRAGMA:
				return getElaborateBodyPragma();
			case AdaPackage.ASSOCIATION_CLASS__EXPORT_PRAGMA:
				return getExportPragma();
			case AdaPackage.ASSOCIATION_CLASS__IMPORT_PRAGMA:
				return getImportPragma();
			case AdaPackage.ASSOCIATION_CLASS__INLINE_PRAGMA:
				return getInlinePragma();
			case AdaPackage.ASSOCIATION_CLASS__INSPECTION_POINT_PRAGMA:
				return getInspectionPointPragma();
			case AdaPackage.ASSOCIATION_CLASS__INTERRUPT_HANDLER_PRAGMA:
				return getInterruptHandlerPragma();
			case AdaPackage.ASSOCIATION_CLASS__INTERRUPT_PRIORITY_PRAGMA:
				return getInterruptPriorityPragma();
			case AdaPackage.ASSOCIATION_CLASS__LINKER_OPTIONS_PRAGMA:
				return getLinkerOptionsPragma();
			case AdaPackage.ASSOCIATION_CLASS__LIST_PRAGMA:
				return getListPragma();
			case AdaPackage.ASSOCIATION_CLASS__LOCKING_POLICY_PRAGMA:
				return getLockingPolicyPragma();
			case AdaPackage.ASSOCIATION_CLASS__NORMALIZE_SCALARS_PRAGMA:
				return getNormalizeScalarsPragma();
			case AdaPackage.ASSOCIATION_CLASS__OPTIMIZE_PRAGMA:
				return getOptimizePragma();
			case AdaPackage.ASSOCIATION_CLASS__PACK_PRAGMA:
				return getPackPragma();
			case AdaPackage.ASSOCIATION_CLASS__PAGE_PRAGMA:
				return getPagePragma();
			case AdaPackage.ASSOCIATION_CLASS__PREELABORATE_PRAGMA:
				return getPreelaboratePragma();
			case AdaPackage.ASSOCIATION_CLASS__PRIORITY_PRAGMA:
				return getPriorityPragma();
			case AdaPackage.ASSOCIATION_CLASS__PURE_PRAGMA:
				return getPurePragma();
			case AdaPackage.ASSOCIATION_CLASS__QUEUING_POLICY_PRAGMA:
				return getQueuingPolicyPragma();
			case AdaPackage.ASSOCIATION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA:
				return getRemoteCallInterfacePragma();
			case AdaPackage.ASSOCIATION_CLASS__REMOTE_TYPES_PRAGMA:
				return getRemoteTypesPragma();
			case AdaPackage.ASSOCIATION_CLASS__RESTRICTIONS_PRAGMA:
				return getRestrictionsPragma();
			case AdaPackage.ASSOCIATION_CLASS__REVIEWABLE_PRAGMA:
				return getReviewablePragma();
			case AdaPackage.ASSOCIATION_CLASS__SHARED_PASSIVE_PRAGMA:
				return getSharedPassivePragma();
			case AdaPackage.ASSOCIATION_CLASS__STORAGE_SIZE_PRAGMA:
				return getStorageSizePragma();
			case AdaPackage.ASSOCIATION_CLASS__SUPPRESS_PRAGMA:
				return getSuppressPragma();
			case AdaPackage.ASSOCIATION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA:
				return getTaskDispatchingPolicyPragma();
			case AdaPackage.ASSOCIATION_CLASS__VOLATILE_PRAGMA:
				return getVolatilePragma();
			case AdaPackage.ASSOCIATION_CLASS__VOLATILE_COMPONENTS_PRAGMA:
				return getVolatileComponentsPragma();
			case AdaPackage.ASSOCIATION_CLASS__ASSERT_PRAGMA:
				return getAssertPragma();
			case AdaPackage.ASSOCIATION_CLASS__ASSERTION_POLICY_PRAGMA:
				return getAssertionPolicyPragma();
			case AdaPackage.ASSOCIATION_CLASS__DETECT_BLOCKING_PRAGMA:
				return getDetectBlockingPragma();
			case AdaPackage.ASSOCIATION_CLASS__NO_RETURN_PRAGMA:
				return getNoReturnPragma();
			case AdaPackage.ASSOCIATION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA:
				return getPartitionElaborationPolicyPragma();
			case AdaPackage.ASSOCIATION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA:
				return getPreelaborableInitializationPragma();
			case AdaPackage.ASSOCIATION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return getPrioritySpecificDispatchingPragma();
			case AdaPackage.ASSOCIATION_CLASS__PROFILE_PRAGMA:
				return getProfilePragma();
			case AdaPackage.ASSOCIATION_CLASS__RELATIVE_DEADLINE_PRAGMA:
				return getRelativeDeadlinePragma();
			case AdaPackage.ASSOCIATION_CLASS__UNCHECKED_UNION_PRAGMA:
				return getUncheckedUnionPragma();
			case AdaPackage.ASSOCIATION_CLASS__UNSUPPRESS_PRAGMA:
				return getUnsuppressPragma();
			case AdaPackage.ASSOCIATION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA:
				return getDefaultStoragePoolPragma();
			case AdaPackage.ASSOCIATION_CLASS__DISPATCHING_DOMAIN_PRAGMA:
				return getDispatchingDomainPragma();
			case AdaPackage.ASSOCIATION_CLASS__CPU_PRAGMA:
				return getCpuPragma();
			case AdaPackage.ASSOCIATION_CLASS__INDEPENDENT_PRAGMA:
				return getIndependentPragma();
			case AdaPackage.ASSOCIATION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA:
				return getIndependentComponentsPragma();
			case AdaPackage.ASSOCIATION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA:
				return getImplementationDefinedPragma();
			case AdaPackage.ASSOCIATION_CLASS__UNKNOWN_PRAGMA:
				return getUnknownPragma();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.ASSOCIATION_CLASS__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__PRAGMA_ARGUMENT_ASSOCIATION:
				setPragmaArgumentAssociation((PragmaArgumentAssociation)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__DISCRIMINANT_ASSOCIATION:
				setDiscriminantAssociation((DiscriminantAssociation)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__RECORD_COMPONENT_ASSOCIATION:
				setRecordComponentAssociation((RecordComponentAssociation)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__ARRAY_COMPONENT_ASSOCIATION:
				setArrayComponentAssociation((ArrayComponentAssociation)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__PARAMETER_ASSOCIATION:
				setParameterAssociation((ParameterAssociation)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__GENERIC_ASSOCIATION:
				setGenericAssociation((GenericAssociation)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__COMMENT:
				setComment((Comment)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__ALL_CALLS_REMOTE_PRAGMA:
				setAllCallsRemotePragma((AllCallsRemotePragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__ASYNCHRONOUS_PRAGMA:
				setAsynchronousPragma((AsynchronousPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__ATOMIC_PRAGMA:
				setAtomicPragma((AtomicPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__ATOMIC_COMPONENTS_PRAGMA:
				setAtomicComponentsPragma((AtomicComponentsPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__ATTACH_HANDLER_PRAGMA:
				setAttachHandlerPragma((AttachHandlerPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__CONTROLLED_PRAGMA:
				setControlledPragma((ControlledPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__CONVENTION_PRAGMA:
				setConventionPragma((ConventionPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__DISCARD_NAMES_PRAGMA:
				setDiscardNamesPragma((DiscardNamesPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__ELABORATE_PRAGMA:
				setElaboratePragma((ElaboratePragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__ELABORATE_ALL_PRAGMA:
				setElaborateAllPragma((ElaborateAllPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__ELABORATE_BODY_PRAGMA:
				setElaborateBodyPragma((ElaborateBodyPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__EXPORT_PRAGMA:
				setExportPragma((ExportPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__IMPORT_PRAGMA:
				setImportPragma((ImportPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__INLINE_PRAGMA:
				setInlinePragma((InlinePragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__INSPECTION_POINT_PRAGMA:
				setInspectionPointPragma((InspectionPointPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__INTERRUPT_HANDLER_PRAGMA:
				setInterruptHandlerPragma((InterruptHandlerPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__INTERRUPT_PRIORITY_PRAGMA:
				setInterruptPriorityPragma((InterruptPriorityPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__LINKER_OPTIONS_PRAGMA:
				setLinkerOptionsPragma((LinkerOptionsPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__LIST_PRAGMA:
				setListPragma((ListPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__LOCKING_POLICY_PRAGMA:
				setLockingPolicyPragma((LockingPolicyPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__NORMALIZE_SCALARS_PRAGMA:
				setNormalizeScalarsPragma((NormalizeScalarsPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__OPTIMIZE_PRAGMA:
				setOptimizePragma((OptimizePragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__PACK_PRAGMA:
				setPackPragma((PackPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__PAGE_PRAGMA:
				setPagePragma((PagePragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__PREELABORATE_PRAGMA:
				setPreelaboratePragma((PreelaboratePragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__PRIORITY_PRAGMA:
				setPriorityPragma((PriorityPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__PURE_PRAGMA:
				setPurePragma((PurePragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__QUEUING_POLICY_PRAGMA:
				setQueuingPolicyPragma((QueuingPolicyPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA:
				setRemoteCallInterfacePragma((RemoteCallInterfacePragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__REMOTE_TYPES_PRAGMA:
				setRemoteTypesPragma((RemoteTypesPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__RESTRICTIONS_PRAGMA:
				setRestrictionsPragma((RestrictionsPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__REVIEWABLE_PRAGMA:
				setReviewablePragma((ReviewablePragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__SHARED_PASSIVE_PRAGMA:
				setSharedPassivePragma((SharedPassivePragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__STORAGE_SIZE_PRAGMA:
				setStorageSizePragma((StorageSizePragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__SUPPRESS_PRAGMA:
				setSuppressPragma((SuppressPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA:
				setTaskDispatchingPolicyPragma((TaskDispatchingPolicyPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__VOLATILE_PRAGMA:
				setVolatilePragma((VolatilePragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__VOLATILE_COMPONENTS_PRAGMA:
				setVolatileComponentsPragma((VolatileComponentsPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__ASSERT_PRAGMA:
				setAssertPragma((AssertPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__ASSERTION_POLICY_PRAGMA:
				setAssertionPolicyPragma((AssertionPolicyPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__DETECT_BLOCKING_PRAGMA:
				setDetectBlockingPragma((DetectBlockingPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__NO_RETURN_PRAGMA:
				setNoReturnPragma((NoReturnPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA:
				setPartitionElaborationPolicyPragma((PartitionElaborationPolicyPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA:
				setPreelaborableInitializationPragma((PreelaborableInitializationPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				setPrioritySpecificDispatchingPragma((PrioritySpecificDispatchingPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__PROFILE_PRAGMA:
				setProfilePragma((ProfilePragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__RELATIVE_DEADLINE_PRAGMA:
				setRelativeDeadlinePragma((RelativeDeadlinePragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__UNCHECKED_UNION_PRAGMA:
				setUncheckedUnionPragma((UncheckedUnionPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__UNSUPPRESS_PRAGMA:
				setUnsuppressPragma((UnsuppressPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA:
				setDefaultStoragePoolPragma((DefaultStoragePoolPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__DISPATCHING_DOMAIN_PRAGMA:
				setDispatchingDomainPragma((DispatchingDomainPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__CPU_PRAGMA:
				setCpuPragma((CpuPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__INDEPENDENT_PRAGMA:
				setIndependentPragma((IndependentPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA:
				setIndependentComponentsPragma((IndependentComponentsPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA:
				setImplementationDefinedPragma((ImplementationDefinedPragma)newValue);
				return;
			case AdaPackage.ASSOCIATION_CLASS__UNKNOWN_PRAGMA:
				setUnknownPragma((UnknownPragma)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.ASSOCIATION_CLASS__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__PRAGMA_ARGUMENT_ASSOCIATION:
				setPragmaArgumentAssociation((PragmaArgumentAssociation)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__DISCRIMINANT_ASSOCIATION:
				setDiscriminantAssociation((DiscriminantAssociation)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__RECORD_COMPONENT_ASSOCIATION:
				setRecordComponentAssociation((RecordComponentAssociation)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__ARRAY_COMPONENT_ASSOCIATION:
				setArrayComponentAssociation((ArrayComponentAssociation)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__PARAMETER_ASSOCIATION:
				setParameterAssociation((ParameterAssociation)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__GENERIC_ASSOCIATION:
				setGenericAssociation((GenericAssociation)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__COMMENT:
				setComment((Comment)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__ALL_CALLS_REMOTE_PRAGMA:
				setAllCallsRemotePragma((AllCallsRemotePragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__ASYNCHRONOUS_PRAGMA:
				setAsynchronousPragma((AsynchronousPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__ATOMIC_PRAGMA:
				setAtomicPragma((AtomicPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__ATOMIC_COMPONENTS_PRAGMA:
				setAtomicComponentsPragma((AtomicComponentsPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__ATTACH_HANDLER_PRAGMA:
				setAttachHandlerPragma((AttachHandlerPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__CONTROLLED_PRAGMA:
				setControlledPragma((ControlledPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__CONVENTION_PRAGMA:
				setConventionPragma((ConventionPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__DISCARD_NAMES_PRAGMA:
				setDiscardNamesPragma((DiscardNamesPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__ELABORATE_PRAGMA:
				setElaboratePragma((ElaboratePragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__ELABORATE_ALL_PRAGMA:
				setElaborateAllPragma((ElaborateAllPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__ELABORATE_BODY_PRAGMA:
				setElaborateBodyPragma((ElaborateBodyPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__EXPORT_PRAGMA:
				setExportPragma((ExportPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__IMPORT_PRAGMA:
				setImportPragma((ImportPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__INLINE_PRAGMA:
				setInlinePragma((InlinePragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__INSPECTION_POINT_PRAGMA:
				setInspectionPointPragma((InspectionPointPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__INTERRUPT_HANDLER_PRAGMA:
				setInterruptHandlerPragma((InterruptHandlerPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__INTERRUPT_PRIORITY_PRAGMA:
				setInterruptPriorityPragma((InterruptPriorityPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__LINKER_OPTIONS_PRAGMA:
				setLinkerOptionsPragma((LinkerOptionsPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__LIST_PRAGMA:
				setListPragma((ListPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__LOCKING_POLICY_PRAGMA:
				setLockingPolicyPragma((LockingPolicyPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__NORMALIZE_SCALARS_PRAGMA:
				setNormalizeScalarsPragma((NormalizeScalarsPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__OPTIMIZE_PRAGMA:
				setOptimizePragma((OptimizePragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__PACK_PRAGMA:
				setPackPragma((PackPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__PAGE_PRAGMA:
				setPagePragma((PagePragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__PREELABORATE_PRAGMA:
				setPreelaboratePragma((PreelaboratePragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__PRIORITY_PRAGMA:
				setPriorityPragma((PriorityPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__PURE_PRAGMA:
				setPurePragma((PurePragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__QUEUING_POLICY_PRAGMA:
				setQueuingPolicyPragma((QueuingPolicyPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA:
				setRemoteCallInterfacePragma((RemoteCallInterfacePragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__REMOTE_TYPES_PRAGMA:
				setRemoteTypesPragma((RemoteTypesPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__RESTRICTIONS_PRAGMA:
				setRestrictionsPragma((RestrictionsPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__REVIEWABLE_PRAGMA:
				setReviewablePragma((ReviewablePragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__SHARED_PASSIVE_PRAGMA:
				setSharedPassivePragma((SharedPassivePragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__STORAGE_SIZE_PRAGMA:
				setStorageSizePragma((StorageSizePragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__SUPPRESS_PRAGMA:
				setSuppressPragma((SuppressPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA:
				setTaskDispatchingPolicyPragma((TaskDispatchingPolicyPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__VOLATILE_PRAGMA:
				setVolatilePragma((VolatilePragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__VOLATILE_COMPONENTS_PRAGMA:
				setVolatileComponentsPragma((VolatileComponentsPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__ASSERT_PRAGMA:
				setAssertPragma((AssertPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__ASSERTION_POLICY_PRAGMA:
				setAssertionPolicyPragma((AssertionPolicyPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__DETECT_BLOCKING_PRAGMA:
				setDetectBlockingPragma((DetectBlockingPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__NO_RETURN_PRAGMA:
				setNoReturnPragma((NoReturnPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA:
				setPartitionElaborationPolicyPragma((PartitionElaborationPolicyPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA:
				setPreelaborableInitializationPragma((PreelaborableInitializationPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				setPrioritySpecificDispatchingPragma((PrioritySpecificDispatchingPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__PROFILE_PRAGMA:
				setProfilePragma((ProfilePragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__RELATIVE_DEADLINE_PRAGMA:
				setRelativeDeadlinePragma((RelativeDeadlinePragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__UNCHECKED_UNION_PRAGMA:
				setUncheckedUnionPragma((UncheckedUnionPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__UNSUPPRESS_PRAGMA:
				setUnsuppressPragma((UnsuppressPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA:
				setDefaultStoragePoolPragma((DefaultStoragePoolPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__DISPATCHING_DOMAIN_PRAGMA:
				setDispatchingDomainPragma((DispatchingDomainPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__CPU_PRAGMA:
				setCpuPragma((CpuPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__INDEPENDENT_PRAGMA:
				setIndependentPragma((IndependentPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA:
				setIndependentComponentsPragma((IndependentComponentsPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA:
				setImplementationDefinedPragma((ImplementationDefinedPragma)null);
				return;
			case AdaPackage.ASSOCIATION_CLASS__UNKNOWN_PRAGMA:
				setUnknownPragma((UnknownPragma)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.ASSOCIATION_CLASS__NOT_AN_ELEMENT:
				return notAnElement != null;
			case AdaPackage.ASSOCIATION_CLASS__PRAGMA_ARGUMENT_ASSOCIATION:
				return pragmaArgumentAssociation != null;
			case AdaPackage.ASSOCIATION_CLASS__DISCRIMINANT_ASSOCIATION:
				return discriminantAssociation != null;
			case AdaPackage.ASSOCIATION_CLASS__RECORD_COMPONENT_ASSOCIATION:
				return recordComponentAssociation != null;
			case AdaPackage.ASSOCIATION_CLASS__ARRAY_COMPONENT_ASSOCIATION:
				return arrayComponentAssociation != null;
			case AdaPackage.ASSOCIATION_CLASS__PARAMETER_ASSOCIATION:
				return parameterAssociation != null;
			case AdaPackage.ASSOCIATION_CLASS__GENERIC_ASSOCIATION:
				return genericAssociation != null;
			case AdaPackage.ASSOCIATION_CLASS__COMMENT:
				return comment != null;
			case AdaPackage.ASSOCIATION_CLASS__ALL_CALLS_REMOTE_PRAGMA:
				return allCallsRemotePragma != null;
			case AdaPackage.ASSOCIATION_CLASS__ASYNCHRONOUS_PRAGMA:
				return asynchronousPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__ATOMIC_PRAGMA:
				return atomicPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__ATOMIC_COMPONENTS_PRAGMA:
				return atomicComponentsPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__ATTACH_HANDLER_PRAGMA:
				return attachHandlerPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__CONTROLLED_PRAGMA:
				return controlledPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__CONVENTION_PRAGMA:
				return conventionPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__DISCARD_NAMES_PRAGMA:
				return discardNamesPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__ELABORATE_PRAGMA:
				return elaboratePragma != null;
			case AdaPackage.ASSOCIATION_CLASS__ELABORATE_ALL_PRAGMA:
				return elaborateAllPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__ELABORATE_BODY_PRAGMA:
				return elaborateBodyPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__EXPORT_PRAGMA:
				return exportPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__IMPORT_PRAGMA:
				return importPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__INLINE_PRAGMA:
				return inlinePragma != null;
			case AdaPackage.ASSOCIATION_CLASS__INSPECTION_POINT_PRAGMA:
				return inspectionPointPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__INTERRUPT_HANDLER_PRAGMA:
				return interruptHandlerPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__INTERRUPT_PRIORITY_PRAGMA:
				return interruptPriorityPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__LINKER_OPTIONS_PRAGMA:
				return linkerOptionsPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__LIST_PRAGMA:
				return listPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__LOCKING_POLICY_PRAGMA:
				return lockingPolicyPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__NORMALIZE_SCALARS_PRAGMA:
				return normalizeScalarsPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__OPTIMIZE_PRAGMA:
				return optimizePragma != null;
			case AdaPackage.ASSOCIATION_CLASS__PACK_PRAGMA:
				return packPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__PAGE_PRAGMA:
				return pagePragma != null;
			case AdaPackage.ASSOCIATION_CLASS__PREELABORATE_PRAGMA:
				return preelaboratePragma != null;
			case AdaPackage.ASSOCIATION_CLASS__PRIORITY_PRAGMA:
				return priorityPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__PURE_PRAGMA:
				return purePragma != null;
			case AdaPackage.ASSOCIATION_CLASS__QUEUING_POLICY_PRAGMA:
				return queuingPolicyPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA:
				return remoteCallInterfacePragma != null;
			case AdaPackage.ASSOCIATION_CLASS__REMOTE_TYPES_PRAGMA:
				return remoteTypesPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__RESTRICTIONS_PRAGMA:
				return restrictionsPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__REVIEWABLE_PRAGMA:
				return reviewablePragma != null;
			case AdaPackage.ASSOCIATION_CLASS__SHARED_PASSIVE_PRAGMA:
				return sharedPassivePragma != null;
			case AdaPackage.ASSOCIATION_CLASS__STORAGE_SIZE_PRAGMA:
				return storageSizePragma != null;
			case AdaPackage.ASSOCIATION_CLASS__SUPPRESS_PRAGMA:
				return suppressPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA:
				return taskDispatchingPolicyPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__VOLATILE_PRAGMA:
				return volatilePragma != null;
			case AdaPackage.ASSOCIATION_CLASS__VOLATILE_COMPONENTS_PRAGMA:
				return volatileComponentsPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__ASSERT_PRAGMA:
				return assertPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__ASSERTION_POLICY_PRAGMA:
				return assertionPolicyPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__DETECT_BLOCKING_PRAGMA:
				return detectBlockingPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__NO_RETURN_PRAGMA:
				return noReturnPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA:
				return partitionElaborationPolicyPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA:
				return preelaborableInitializationPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return prioritySpecificDispatchingPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__PROFILE_PRAGMA:
				return profilePragma != null;
			case AdaPackage.ASSOCIATION_CLASS__RELATIVE_DEADLINE_PRAGMA:
				return relativeDeadlinePragma != null;
			case AdaPackage.ASSOCIATION_CLASS__UNCHECKED_UNION_PRAGMA:
				return uncheckedUnionPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__UNSUPPRESS_PRAGMA:
				return unsuppressPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA:
				return defaultStoragePoolPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__DISPATCHING_DOMAIN_PRAGMA:
				return dispatchingDomainPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__CPU_PRAGMA:
				return cpuPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__INDEPENDENT_PRAGMA:
				return independentPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA:
				return independentComponentsPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA:
				return implementationDefinedPragma != null;
			case AdaPackage.ASSOCIATION_CLASS__UNKNOWN_PRAGMA:
				return unknownPragma != null;
		}
		return super.eIsSet(featureID);
	}

} //AssociationClassImpl
