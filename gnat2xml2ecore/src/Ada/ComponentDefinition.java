/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.ComponentDefinition#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.ComponentDefinition#getHasAliasedQ <em>Has Aliased Q</em>}</li>
 *   <li>{@link Ada.ComponentDefinition#getComponentDefinitionViewQ <em>Component Definition View Q</em>}</li>
 *   <li>{@link Ada.ComponentDefinition#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getComponentDefinition()
 * @model extendedMetaData="name='Component_Definition' kind='elementOnly'"
 * @generated
 */
public interface ComponentDefinition extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getComponentDefinition_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.ComponentDefinition#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Has Aliased Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Aliased Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Aliased Q</em>' containment reference.
	 * @see #setHasAliasedQ(HasAliasedQType6)
	 * @see Ada.AdaPackage#getComponentDefinition_HasAliasedQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='has_aliased_q' namespace='##targetNamespace'"
	 * @generated
	 */
	HasAliasedQType6 getHasAliasedQ();

	/**
	 * Sets the value of the '{@link Ada.ComponentDefinition#getHasAliasedQ <em>Has Aliased Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Aliased Q</em>' containment reference.
	 * @see #getHasAliasedQ()
	 * @generated
	 */
	void setHasAliasedQ(HasAliasedQType6 value);

	/**
	 * Returns the value of the '<em><b>Component Definition View Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Definition View Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Definition View Q</em>' containment reference.
	 * @see #setComponentDefinitionViewQ(DefinitionClass)
	 * @see Ada.AdaPackage#getComponentDefinition_ComponentDefinitionViewQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='component_definition_view_q' namespace='##targetNamespace'"
	 * @generated
	 */
	DefinitionClass getComponentDefinitionViewQ();

	/**
	 * Sets the value of the '{@link Ada.ComponentDefinition#getComponentDefinitionViewQ <em>Component Definition View Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component Definition View Q</em>' containment reference.
	 * @see #getComponentDefinitionViewQ()
	 * @generated
	 */
	void setComponentDefinitionViewQ(DefinitionClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getComponentDefinition_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.ComponentDefinition#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // ComponentDefinition
