/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.DiscreteRangeClass;
import Ada.ExpressionClass;
import Ada.Slice;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Slice</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.SliceImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.SliceImpl#getPrefixQ <em>Prefix Q</em>}</li>
 *   <li>{@link Ada.impl.SliceImpl#getSliceRangeQ <em>Slice Range Q</em>}</li>
 *   <li>{@link Ada.impl.SliceImpl#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.impl.SliceImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SliceImpl extends MinimalEObjectImpl.Container implements Slice {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getPrefixQ() <em>Prefix Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrefixQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass prefixQ;

	/**
	 * The cached value of the '{@link #getSliceRangeQ() <em>Slice Range Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSliceRangeQ()
	 * @generated
	 * @ordered
	 */
	protected DiscreteRangeClass sliceRangeQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SliceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getSlice();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.SLICE__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.SLICE__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.SLICE__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.SLICE__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getPrefixQ() {
		return prefixQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrefixQ(ExpressionClass newPrefixQ, NotificationChain msgs) {
		ExpressionClass oldPrefixQ = prefixQ;
		prefixQ = newPrefixQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.SLICE__PREFIX_Q, oldPrefixQ, newPrefixQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrefixQ(ExpressionClass newPrefixQ) {
		if (newPrefixQ != prefixQ) {
			NotificationChain msgs = null;
			if (prefixQ != null)
				msgs = ((InternalEObject)prefixQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.SLICE__PREFIX_Q, null, msgs);
			if (newPrefixQ != null)
				msgs = ((InternalEObject)newPrefixQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.SLICE__PREFIX_Q, null, msgs);
			msgs = basicSetPrefixQ(newPrefixQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.SLICE__PREFIX_Q, newPrefixQ, newPrefixQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscreteRangeClass getSliceRangeQ() {
		return sliceRangeQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSliceRangeQ(DiscreteRangeClass newSliceRangeQ, NotificationChain msgs) {
		DiscreteRangeClass oldSliceRangeQ = sliceRangeQ;
		sliceRangeQ = newSliceRangeQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.SLICE__SLICE_RANGE_Q, oldSliceRangeQ, newSliceRangeQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSliceRangeQ(DiscreteRangeClass newSliceRangeQ) {
		if (newSliceRangeQ != sliceRangeQ) {
			NotificationChain msgs = null;
			if (sliceRangeQ != null)
				msgs = ((InternalEObject)sliceRangeQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.SLICE__SLICE_RANGE_Q, null, msgs);
			if (newSliceRangeQ != null)
				msgs = ((InternalEObject)newSliceRangeQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.SLICE__SLICE_RANGE_Q, null, msgs);
			msgs = basicSetSliceRangeQ(newSliceRangeQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.SLICE__SLICE_RANGE_Q, newSliceRangeQ, newSliceRangeQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.SLICE__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.SLICE__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.SLICE__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.SLICE__PREFIX_Q:
				return basicSetPrefixQ(null, msgs);
			case AdaPackage.SLICE__SLICE_RANGE_Q:
				return basicSetSliceRangeQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.SLICE__SLOC:
				return getSloc();
			case AdaPackage.SLICE__PREFIX_Q:
				return getPrefixQ();
			case AdaPackage.SLICE__SLICE_RANGE_Q:
				return getSliceRangeQ();
			case AdaPackage.SLICE__CHECKS:
				return getChecks();
			case AdaPackage.SLICE__TYPE:
				return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.SLICE__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.SLICE__PREFIX_Q:
				setPrefixQ((ExpressionClass)newValue);
				return;
			case AdaPackage.SLICE__SLICE_RANGE_Q:
				setSliceRangeQ((DiscreteRangeClass)newValue);
				return;
			case AdaPackage.SLICE__CHECKS:
				setChecks((String)newValue);
				return;
			case AdaPackage.SLICE__TYPE:
				setType((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.SLICE__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.SLICE__PREFIX_Q:
				setPrefixQ((ExpressionClass)null);
				return;
			case AdaPackage.SLICE__SLICE_RANGE_Q:
				setSliceRangeQ((DiscreteRangeClass)null);
				return;
			case AdaPackage.SLICE__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
			case AdaPackage.SLICE__TYPE:
				setType(TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.SLICE__SLOC:
				return sloc != null;
			case AdaPackage.SLICE__PREFIX_Q:
				return prefixQ != null;
			case AdaPackage.SLICE__SLICE_RANGE_Q:
				return sliceRangeQ != null;
			case AdaPackage.SLICE__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
			case AdaPackage.SLICE__TYPE:
				return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(", type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //SliceImpl
