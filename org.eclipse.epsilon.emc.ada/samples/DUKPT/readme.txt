--------------------------------------------------------------------------------
--
-- DUKPT
--
-- Readme - Summary and user guide.
--
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
Dependencies

   libadacrypt  - https://github.com/cforler/Ada-Crypto-Library
   GNAT Runtime - platform dependent?

--------------------------------------------------------------------------------
Summary

   This SPARK implementation of the Derived Unique Key Per Transaction key
   handling routine is loosely based on the ANSI standard - ANS X9.24-1:2009. It
   has been modified to use AES128 rather than 3DES for the encryption method
   for generating the keys. The crypto library used is not itself written in
   SPARK.

   The sizes of many of the registers defined in the example specification in
   ANS X9.24-1:2009 Section A have been changed to simplify the implementation
   (without) changing the function.

   The Key Serial Number (KSN) is now 128 bits long with the right 32 bits
   (least significant) reserved for the Encryption Counter. Currently there is
   no additional functionality for using keys for a session of back and forth
   communication. This will likely need to need an additional flag in the KSN if
   the DUKPT 'library' is to handle sessions - or the user can handle the keys
   themselves - which is cleaner.

   The ANSI standard ANS X9.24-1 talks about a 'short-cut' method of the server
   arriving at the future key from the KSN. There is insufficient detail on the
   method in the standard and attempts to reproduce the method have failed so
   the decryption keys are found by cycling through the same method used to
   generate the key in the first place.

--------------------------------------------------------------------------------
User Guide

   The input to DUKPT is the Base Derivation Key (BDK) and the Initial Key
   Serial Number (IKSN).

   Terminal side:

      -- Generate the Initial PIN Encryption Key (IPEK)
      DUKPT.Derive_IPEK(BDK, IKSN, IPEK);
      -- Load the first 21 keys into the Future Keys Register (FKR)
      DUKPT.Load_Initial_Keys(IPEK, IKSN);
      -- Get the derived Encryption and MAC keys based on the next key from
      -- the Future Keys Register (new keys are generated automatically when
      -- needed)
      DUKPT.Get_Sender_Transaction_Keys(KSN, Encrypt_Key_Sender, MAC_Key_Sender);
   
      -- The keys are then used to encrypt/authenticate a message which is sent
      -- to the server. Any 128-bit key schemes can be used but the first 128
      -- bits must be the plain text KSN returned from "Get_Sender_Data(...)"

   Server side:

      -- First the plain text KSN must be extracted from the the received
      -- message. This KSN and the BDK are used to derive the IPEK for this
      -- device.
      DUKPT.Derive_IPEK(BDK, KSN, IPEK);
      -- The IPEK and KSN are then used to retrieve the keys used to decrypt and
      -- authenticate the message.
      DUKPT.Retrieve_Transaction_Keys(IPEK, KSN, 
                                      Encrypt_Key_Receiver, MAC_Key_Receiver);

--------------------------------------------------------------------------------
SPARK

   There are two functions in dukpt-utilities.ad* that are unused by the 
   DUKPT implementation and so have pragma SPARK_Mode(Off). These functions
   have been useful for debugging so I have left them here. Any parent 
   project must have a global configuration parameter to set SPARK_Mode
   or there will be problems with compilation.

--------------------------------------------------------------------------------
