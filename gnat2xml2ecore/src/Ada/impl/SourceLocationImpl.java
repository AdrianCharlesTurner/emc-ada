/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.SourceLocation;

import java.math.BigInteger;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Source Location</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.SourceLocationImpl#getCol <em>Col</em>}</li>
 *   <li>{@link Ada.impl.SourceLocationImpl#getEndcol <em>Endcol</em>}</li>
 *   <li>{@link Ada.impl.SourceLocationImpl#getEndline <em>Endline</em>}</li>
 *   <li>{@link Ada.impl.SourceLocationImpl#getLine <em>Line</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SourceLocationImpl extends MinimalEObjectImpl.Container implements SourceLocation {
	/**
	 * The default value of the '{@link #getCol() <em>Col</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCol()
	 * @generated
	 * @ordered
	 */
	protected static final BigInteger COL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCol() <em>Col</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCol()
	 * @generated
	 * @ordered
	 */
	protected BigInteger col = COL_EDEFAULT;

	/**
	 * The default value of the '{@link #getEndcol() <em>Endcol</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndcol()
	 * @generated
	 * @ordered
	 */
	protected static final BigInteger ENDCOL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEndcol() <em>Endcol</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndcol()
	 * @generated
	 * @ordered
	 */
	protected BigInteger endcol = ENDCOL_EDEFAULT;

	/**
	 * The default value of the '{@link #getEndline() <em>Endline</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndline()
	 * @generated
	 * @ordered
	 */
	protected static final BigInteger ENDLINE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEndline() <em>Endline</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndline()
	 * @generated
	 * @ordered
	 */
	protected BigInteger endline = ENDLINE_EDEFAULT;

	/**
	 * The default value of the '{@link #getLine() <em>Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLine()
	 * @generated
	 * @ordered
	 */
	protected static final BigInteger LINE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLine() <em>Line</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLine()
	 * @generated
	 * @ordered
	 */
	protected BigInteger line = LINE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SourceLocationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getSourceLocation();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigInteger getCol() {
		return col;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCol(BigInteger newCol) {
		BigInteger oldCol = col;
		col = newCol;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.SOURCE_LOCATION__COL, oldCol, col));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigInteger getEndcol() {
		return endcol;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndcol(BigInteger newEndcol) {
		BigInteger oldEndcol = endcol;
		endcol = newEndcol;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.SOURCE_LOCATION__ENDCOL, oldEndcol, endcol));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigInteger getEndline() {
		return endline;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndline(BigInteger newEndline) {
		BigInteger oldEndline = endline;
		endline = newEndline;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.SOURCE_LOCATION__ENDLINE, oldEndline, endline));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigInteger getLine() {
		return line;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLine(BigInteger newLine) {
		BigInteger oldLine = line;
		line = newLine;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.SOURCE_LOCATION__LINE, oldLine, line));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.SOURCE_LOCATION__COL:
				return getCol();
			case AdaPackage.SOURCE_LOCATION__ENDCOL:
				return getEndcol();
			case AdaPackage.SOURCE_LOCATION__ENDLINE:
				return getEndline();
			case AdaPackage.SOURCE_LOCATION__LINE:
				return getLine();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.SOURCE_LOCATION__COL:
				setCol((BigInteger)newValue);
				return;
			case AdaPackage.SOURCE_LOCATION__ENDCOL:
				setEndcol((BigInteger)newValue);
				return;
			case AdaPackage.SOURCE_LOCATION__ENDLINE:
				setEndline((BigInteger)newValue);
				return;
			case AdaPackage.SOURCE_LOCATION__LINE:
				setLine((BigInteger)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.SOURCE_LOCATION__COL:
				setCol(COL_EDEFAULT);
				return;
			case AdaPackage.SOURCE_LOCATION__ENDCOL:
				setEndcol(ENDCOL_EDEFAULT);
				return;
			case AdaPackage.SOURCE_LOCATION__ENDLINE:
				setEndline(ENDLINE_EDEFAULT);
				return;
			case AdaPackage.SOURCE_LOCATION__LINE:
				setLine(LINE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.SOURCE_LOCATION__COL:
				return COL_EDEFAULT == null ? col != null : !COL_EDEFAULT.equals(col);
			case AdaPackage.SOURCE_LOCATION__ENDCOL:
				return ENDCOL_EDEFAULT == null ? endcol != null : !ENDCOL_EDEFAULT.equals(endcol);
			case AdaPackage.SOURCE_LOCATION__ENDLINE:
				return ENDLINE_EDEFAULT == null ? endline != null : !ENDLINE_EDEFAULT.equals(endline);
			case AdaPackage.SOURCE_LOCATION__LINE:
				return LINE_EDEFAULT == null ? line != null : !LINE_EDEFAULT.equals(line);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (col: ");
		result.append(col);
		result.append(", endcol: ");
		result.append(endcol);
		result.append(", endline: ");
		result.append(endline);
		result.append(", line: ");
		result.append(line);
		result.append(')');
		return result.toString();
	}

} //SourceLocationImpl
