/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.DefiningNameList;
import Ada.DefinitionClass;
import Ada.ElementList;
import Ada.PrivateExtensionDeclaration;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Private Extension Declaration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.PrivateExtensionDeclarationImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.PrivateExtensionDeclarationImpl#getNamesQl <em>Names Ql</em>}</li>
 *   <li>{@link Ada.impl.PrivateExtensionDeclarationImpl#getDiscriminantPartQ <em>Discriminant Part Q</em>}</li>
 *   <li>{@link Ada.impl.PrivateExtensionDeclarationImpl#getTypeDeclarationViewQ <em>Type Declaration View Q</em>}</li>
 *   <li>{@link Ada.impl.PrivateExtensionDeclarationImpl#getAspectSpecificationsQl <em>Aspect Specifications Ql</em>}</li>
 *   <li>{@link Ada.impl.PrivateExtensionDeclarationImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PrivateExtensionDeclarationImpl extends MinimalEObjectImpl.Container implements PrivateExtensionDeclaration {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getNamesQl() <em>Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamesQl()
	 * @generated
	 * @ordered
	 */
	protected DefiningNameList namesQl;

	/**
	 * The cached value of the '{@link #getDiscriminantPartQ() <em>Discriminant Part Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiscriminantPartQ()
	 * @generated
	 * @ordered
	 */
	protected DefinitionClass discriminantPartQ;

	/**
	 * The cached value of the '{@link #getTypeDeclarationViewQ() <em>Type Declaration View Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeDeclarationViewQ()
	 * @generated
	 * @ordered
	 */
	protected DefinitionClass typeDeclarationViewQ;

	/**
	 * The cached value of the '{@link #getAspectSpecificationsQl() <em>Aspect Specifications Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAspectSpecificationsQl()
	 * @generated
	 * @ordered
	 */
	protected ElementList aspectSpecificationsQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PrivateExtensionDeclarationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getPrivateExtensionDeclaration();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.PRIVATE_EXTENSION_DECLARATION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIVATE_EXTENSION_DECLARATION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIVATE_EXTENSION_DECLARATION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PRIVATE_EXTENSION_DECLARATION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNameList getNamesQl() {
		return namesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNamesQl(DefiningNameList newNamesQl, NotificationChain msgs) {
		DefiningNameList oldNamesQl = namesQl;
		namesQl = newNamesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.PRIVATE_EXTENSION_DECLARATION__NAMES_QL, oldNamesQl, newNamesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNamesQl(DefiningNameList newNamesQl) {
		if (newNamesQl != namesQl) {
			NotificationChain msgs = null;
			if (namesQl != null)
				msgs = ((InternalEObject)namesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIVATE_EXTENSION_DECLARATION__NAMES_QL, null, msgs);
			if (newNamesQl != null)
				msgs = ((InternalEObject)newNamesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIVATE_EXTENSION_DECLARATION__NAMES_QL, null, msgs);
			msgs = basicSetNamesQl(newNamesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PRIVATE_EXTENSION_DECLARATION__NAMES_QL, newNamesQl, newNamesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefinitionClass getDiscriminantPartQ() {
		return discriminantPartQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscriminantPartQ(DefinitionClass newDiscriminantPartQ, NotificationChain msgs) {
		DefinitionClass oldDiscriminantPartQ = discriminantPartQ;
		discriminantPartQ = newDiscriminantPartQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.PRIVATE_EXTENSION_DECLARATION__DISCRIMINANT_PART_Q, oldDiscriminantPartQ, newDiscriminantPartQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscriminantPartQ(DefinitionClass newDiscriminantPartQ) {
		if (newDiscriminantPartQ != discriminantPartQ) {
			NotificationChain msgs = null;
			if (discriminantPartQ != null)
				msgs = ((InternalEObject)discriminantPartQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIVATE_EXTENSION_DECLARATION__DISCRIMINANT_PART_Q, null, msgs);
			if (newDiscriminantPartQ != null)
				msgs = ((InternalEObject)newDiscriminantPartQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIVATE_EXTENSION_DECLARATION__DISCRIMINANT_PART_Q, null, msgs);
			msgs = basicSetDiscriminantPartQ(newDiscriminantPartQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PRIVATE_EXTENSION_DECLARATION__DISCRIMINANT_PART_Q, newDiscriminantPartQ, newDiscriminantPartQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefinitionClass getTypeDeclarationViewQ() {
		return typeDeclarationViewQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTypeDeclarationViewQ(DefinitionClass newTypeDeclarationViewQ, NotificationChain msgs) {
		DefinitionClass oldTypeDeclarationViewQ = typeDeclarationViewQ;
		typeDeclarationViewQ = newTypeDeclarationViewQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.PRIVATE_EXTENSION_DECLARATION__TYPE_DECLARATION_VIEW_Q, oldTypeDeclarationViewQ, newTypeDeclarationViewQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypeDeclarationViewQ(DefinitionClass newTypeDeclarationViewQ) {
		if (newTypeDeclarationViewQ != typeDeclarationViewQ) {
			NotificationChain msgs = null;
			if (typeDeclarationViewQ != null)
				msgs = ((InternalEObject)typeDeclarationViewQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIVATE_EXTENSION_DECLARATION__TYPE_DECLARATION_VIEW_Q, null, msgs);
			if (newTypeDeclarationViewQ != null)
				msgs = ((InternalEObject)newTypeDeclarationViewQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIVATE_EXTENSION_DECLARATION__TYPE_DECLARATION_VIEW_Q, null, msgs);
			msgs = basicSetTypeDeclarationViewQ(newTypeDeclarationViewQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PRIVATE_EXTENSION_DECLARATION__TYPE_DECLARATION_VIEW_Q, newTypeDeclarationViewQ, newTypeDeclarationViewQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementList getAspectSpecificationsQl() {
		return aspectSpecificationsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAspectSpecificationsQl(ElementList newAspectSpecificationsQl, NotificationChain msgs) {
		ElementList oldAspectSpecificationsQl = aspectSpecificationsQl;
		aspectSpecificationsQl = newAspectSpecificationsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.PRIVATE_EXTENSION_DECLARATION__ASPECT_SPECIFICATIONS_QL, oldAspectSpecificationsQl, newAspectSpecificationsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAspectSpecificationsQl(ElementList newAspectSpecificationsQl) {
		if (newAspectSpecificationsQl != aspectSpecificationsQl) {
			NotificationChain msgs = null;
			if (aspectSpecificationsQl != null)
				msgs = ((InternalEObject)aspectSpecificationsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIVATE_EXTENSION_DECLARATION__ASPECT_SPECIFICATIONS_QL, null, msgs);
			if (newAspectSpecificationsQl != null)
				msgs = ((InternalEObject)newAspectSpecificationsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIVATE_EXTENSION_DECLARATION__ASPECT_SPECIFICATIONS_QL, null, msgs);
			msgs = basicSetAspectSpecificationsQl(newAspectSpecificationsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PRIVATE_EXTENSION_DECLARATION__ASPECT_SPECIFICATIONS_QL, newAspectSpecificationsQl, newAspectSpecificationsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PRIVATE_EXTENSION_DECLARATION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__NAMES_QL:
				return basicSetNamesQl(null, msgs);
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__DISCRIMINANT_PART_Q:
				return basicSetDiscriminantPartQ(null, msgs);
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__TYPE_DECLARATION_VIEW_Q:
				return basicSetTypeDeclarationViewQ(null, msgs);
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__ASPECT_SPECIFICATIONS_QL:
				return basicSetAspectSpecificationsQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__SLOC:
				return getSloc();
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__NAMES_QL:
				return getNamesQl();
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__DISCRIMINANT_PART_Q:
				return getDiscriminantPartQ();
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__TYPE_DECLARATION_VIEW_Q:
				return getTypeDeclarationViewQ();
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__ASPECT_SPECIFICATIONS_QL:
				return getAspectSpecificationsQl();
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__NAMES_QL:
				setNamesQl((DefiningNameList)newValue);
				return;
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__DISCRIMINANT_PART_Q:
				setDiscriminantPartQ((DefinitionClass)newValue);
				return;
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__TYPE_DECLARATION_VIEW_Q:
				setTypeDeclarationViewQ((DefinitionClass)newValue);
				return;
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__ASPECT_SPECIFICATIONS_QL:
				setAspectSpecificationsQl((ElementList)newValue);
				return;
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__NAMES_QL:
				setNamesQl((DefiningNameList)null);
				return;
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__DISCRIMINANT_PART_Q:
				setDiscriminantPartQ((DefinitionClass)null);
				return;
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__TYPE_DECLARATION_VIEW_Q:
				setTypeDeclarationViewQ((DefinitionClass)null);
				return;
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__ASPECT_SPECIFICATIONS_QL:
				setAspectSpecificationsQl((ElementList)null);
				return;
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__SLOC:
				return sloc != null;
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__NAMES_QL:
				return namesQl != null;
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__DISCRIMINANT_PART_Q:
				return discriminantPartQ != null;
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__TYPE_DECLARATION_VIEW_Q:
				return typeDeclarationViewQ != null;
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__ASPECT_SPECIFICATIONS_QL:
				return aspectSpecificationsQl != null;
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //PrivateExtensionDeclarationImpl
