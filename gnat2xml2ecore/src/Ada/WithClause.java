/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>With Clause</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.WithClause#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.WithClause#getHasLimitedQ <em>Has Limited Q</em>}</li>
 *   <li>{@link Ada.WithClause#getHasPrivateQ <em>Has Private Q</em>}</li>
 *   <li>{@link Ada.WithClause#getClauseNamesQl <em>Clause Names Ql</em>}</li>
 *   <li>{@link Ada.WithClause#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getWithClause()
 * @model extendedMetaData="name='With_Clause' kind='elementOnly'"
 * @generated
 */
public interface WithClause extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getWithClause_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.WithClause#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Has Limited Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Limited Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Limited Q</em>' containment reference.
	 * @see #setHasLimitedQ(HasLimitedQType)
	 * @see Ada.AdaPackage#getWithClause_HasLimitedQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='has_limited_q' namespace='##targetNamespace'"
	 * @generated
	 */
	HasLimitedQType getHasLimitedQ();

	/**
	 * Sets the value of the '{@link Ada.WithClause#getHasLimitedQ <em>Has Limited Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Limited Q</em>' containment reference.
	 * @see #getHasLimitedQ()
	 * @generated
	 */
	void setHasLimitedQ(HasLimitedQType value);

	/**
	 * Returns the value of the '<em><b>Has Private Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Private Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Private Q</em>' containment reference.
	 * @see #setHasPrivateQ(HasPrivateQType1)
	 * @see Ada.AdaPackage#getWithClause_HasPrivateQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='has_private_q' namespace='##targetNamespace'"
	 * @generated
	 */
	HasPrivateQType1 getHasPrivateQ();

	/**
	 * Sets the value of the '{@link Ada.WithClause#getHasPrivateQ <em>Has Private Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Private Q</em>' containment reference.
	 * @see #getHasPrivateQ()
	 * @generated
	 */
	void setHasPrivateQ(HasPrivateQType1 value);

	/**
	 * Returns the value of the '<em><b>Clause Names Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Clause Names Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Clause Names Ql</em>' containment reference.
	 * @see #setClauseNamesQl(NameList)
	 * @see Ada.AdaPackage#getWithClause_ClauseNamesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='clause_names_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	NameList getClauseNamesQl();

	/**
	 * Sets the value of the '{@link Ada.WithClause#getClauseNamesQl <em>Clause Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Clause Names Ql</em>' containment reference.
	 * @see #getClauseNamesQl()
	 * @generated
	 */
	void setClauseNamesQl(NameList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getWithClause_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.WithClause#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // WithClause
