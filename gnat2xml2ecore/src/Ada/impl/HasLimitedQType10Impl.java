/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.HasLimitedQType10;
import Ada.Limited;
import Ada.NotAnElement;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Has Limited QType10</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.HasLimitedQType10Impl#getLimited <em>Limited</em>}</li>
 *   <li>{@link Ada.impl.HasLimitedQType10Impl#getNotAnElement <em>Not An Element</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HasLimitedQType10Impl extends MinimalEObjectImpl.Container implements HasLimitedQType10 {
	/**
	 * The cached value of the '{@link #getLimited() <em>Limited</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLimited()
	 * @generated
	 * @ordered
	 */
	protected Limited limited;

	/**
	 * The cached value of the '{@link #getNotAnElement() <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotAnElement()
	 * @generated
	 * @ordered
	 */
	protected NotAnElement notAnElement;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HasLimitedQType10Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getHasLimitedQType10();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Limited getLimited() {
		return limited;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLimited(Limited newLimited, NotificationChain msgs) {
		Limited oldLimited = limited;
		limited = newLimited;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.HAS_LIMITED_QTYPE10__LIMITED, oldLimited, newLimited);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLimited(Limited newLimited) {
		if (newLimited != limited) {
			NotificationChain msgs = null;
			if (limited != null)
				msgs = ((InternalEObject)limited).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.HAS_LIMITED_QTYPE10__LIMITED, null, msgs);
			if (newLimited != null)
				msgs = ((InternalEObject)newLimited).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.HAS_LIMITED_QTYPE10__LIMITED, null, msgs);
			msgs = basicSetLimited(newLimited, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.HAS_LIMITED_QTYPE10__LIMITED, newLimited, newLimited));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotAnElement getNotAnElement() {
		return notAnElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNotAnElement(NotAnElement newNotAnElement, NotificationChain msgs) {
		NotAnElement oldNotAnElement = notAnElement;
		notAnElement = newNotAnElement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.HAS_LIMITED_QTYPE10__NOT_AN_ELEMENT, oldNotAnElement, newNotAnElement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotAnElement(NotAnElement newNotAnElement) {
		if (newNotAnElement != notAnElement) {
			NotificationChain msgs = null;
			if (notAnElement != null)
				msgs = ((InternalEObject)notAnElement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.HAS_LIMITED_QTYPE10__NOT_AN_ELEMENT, null, msgs);
			if (newNotAnElement != null)
				msgs = ((InternalEObject)newNotAnElement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.HAS_LIMITED_QTYPE10__NOT_AN_ELEMENT, null, msgs);
			msgs = basicSetNotAnElement(newNotAnElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.HAS_LIMITED_QTYPE10__NOT_AN_ELEMENT, newNotAnElement, newNotAnElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.HAS_LIMITED_QTYPE10__LIMITED:
				return basicSetLimited(null, msgs);
			case AdaPackage.HAS_LIMITED_QTYPE10__NOT_AN_ELEMENT:
				return basicSetNotAnElement(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.HAS_LIMITED_QTYPE10__LIMITED:
				return getLimited();
			case AdaPackage.HAS_LIMITED_QTYPE10__NOT_AN_ELEMENT:
				return getNotAnElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.HAS_LIMITED_QTYPE10__LIMITED:
				setLimited((Limited)newValue);
				return;
			case AdaPackage.HAS_LIMITED_QTYPE10__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.HAS_LIMITED_QTYPE10__LIMITED:
				setLimited((Limited)null);
				return;
			case AdaPackage.HAS_LIMITED_QTYPE10__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.HAS_LIMITED_QTYPE10__LIMITED:
				return limited != null;
			case AdaPackage.HAS_LIMITED_QTYPE10__NOT_AN_ELEMENT:
				return notAnElement != null;
		}
		return super.eIsSet(featureID);
	}

} //HasLimitedQType10Impl
