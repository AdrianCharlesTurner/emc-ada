/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Formal Limited Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.FormalLimitedInterface#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.FormalLimitedInterface#getDefinitionInterfaceListQl <em>Definition Interface List Ql</em>}</li>
 *   <li>{@link Ada.FormalLimitedInterface#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getFormalLimitedInterface()
 * @model extendedMetaData="name='Formal_Limited_Interface' kind='elementOnly'"
 * @generated
 */
public interface FormalLimitedInterface extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getFormalLimitedInterface_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.FormalLimitedInterface#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Definition Interface List Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Definition Interface List Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definition Interface List Ql</em>' containment reference.
	 * @see #setDefinitionInterfaceListQl(ExpressionList)
	 * @see Ada.AdaPackage#getFormalLimitedInterface_DefinitionInterfaceListQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='definition_interface_list_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionList getDefinitionInterfaceListQl();

	/**
	 * Sets the value of the '{@link Ada.FormalLimitedInterface#getDefinitionInterfaceListQl <em>Definition Interface List Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Definition Interface List Ql</em>' containment reference.
	 * @see #getDefinitionInterfaceListQl()
	 * @generated
	 */
	void setDefinitionInterfaceListQl(ExpressionList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getFormalLimitedInterface_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.FormalLimitedInterface#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // FormalLimitedInterface
