/**
 */
package Ada;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see Ada.AdaPackage
 * @generated
 */
public interface AdaFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AdaFactory eINSTANCE = Ada.impl.AdaFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Abort Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Abort Statement</em>'.
	 * @generated
	 */
	AbortStatement createAbortStatement();

	/**
	 * Returns a new object of class '<em>Abs Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Abs Operator</em>'.
	 * @generated
	 */
	AbsOperator createAbsOperator();

	/**
	 * Returns a new object of class '<em>Abstract</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Abstract</em>'.
	 * @generated
	 */
	Abstract createAbstract();

	/**
	 * Returns a new object of class '<em>Accept Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Accept Statement</em>'.
	 * @generated
	 */
	AcceptStatement createAcceptStatement();

	/**
	 * Returns a new object of class '<em>Access Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Access Attribute</em>'.
	 * @generated
	 */
	AccessAttribute createAccessAttribute();

	/**
	 * Returns a new object of class '<em>Access To Constant</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Access To Constant</em>'.
	 * @generated
	 */
	AccessToConstant createAccessToConstant();

	/**
	 * Returns a new object of class '<em>Access To Function</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Access To Function</em>'.
	 * @generated
	 */
	AccessToFunction createAccessToFunction();

	/**
	 * Returns a new object of class '<em>Access To Procedure</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Access To Procedure</em>'.
	 * @generated
	 */
	AccessToProcedure createAccessToProcedure();

	/**
	 * Returns a new object of class '<em>Access To Protected Function</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Access To Protected Function</em>'.
	 * @generated
	 */
	AccessToProtectedFunction createAccessToProtectedFunction();

	/**
	 * Returns a new object of class '<em>Access To Protected Procedure</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Access To Protected Procedure</em>'.
	 * @generated
	 */
	AccessToProtectedProcedure createAccessToProtectedProcedure();

	/**
	 * Returns a new object of class '<em>Access To Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Access To Variable</em>'.
	 * @generated
	 */
	AccessToVariable createAccessToVariable();

	/**
	 * Returns a new object of class '<em>Address Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Address Attribute</em>'.
	 * @generated
	 */
	AddressAttribute createAddressAttribute();

	/**
	 * Returns a new object of class '<em>Adjacent Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Adjacent Attribute</em>'.
	 * @generated
	 */
	AdjacentAttribute createAdjacentAttribute();

	/**
	 * Returns a new object of class '<em>Aft Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aft Attribute</em>'.
	 * @generated
	 */
	AftAttribute createAftAttribute();

	/**
	 * Returns a new object of class '<em>Aliased</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aliased</em>'.
	 * @generated
	 */
	Aliased createAliased();

	/**
	 * Returns a new object of class '<em>Alignment Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Alignment Attribute</em>'.
	 * @generated
	 */
	AlignmentAttribute createAlignmentAttribute();

	/**
	 * Returns a new object of class '<em>All Calls Remote Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>All Calls Remote Pragma</em>'.
	 * @generated
	 */
	AllCallsRemotePragma createAllCallsRemotePragma();

	/**
	 * Returns a new object of class '<em>Allocation From Qualified Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Allocation From Qualified Expression</em>'.
	 * @generated
	 */
	AllocationFromQualifiedExpression createAllocationFromQualifiedExpression();

	/**
	 * Returns a new object of class '<em>Allocation From Subtype</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Allocation From Subtype</em>'.
	 * @generated
	 */
	AllocationFromSubtype createAllocationFromSubtype();

	/**
	 * Returns a new object of class '<em>And Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>And Operator</em>'.
	 * @generated
	 */
	AndOperator createAndOperator();

	/**
	 * Returns a new object of class '<em>And Then Short Circuit</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>And Then Short Circuit</em>'.
	 * @generated
	 */
	AndThenShortCircuit createAndThenShortCircuit();

	/**
	 * Returns a new object of class '<em>Anonymous Access To Constant</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Anonymous Access To Constant</em>'.
	 * @generated
	 */
	AnonymousAccessToConstant createAnonymousAccessToConstant();

	/**
	 * Returns a new object of class '<em>Anonymous Access To Function</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Anonymous Access To Function</em>'.
	 * @generated
	 */
	AnonymousAccessToFunction createAnonymousAccessToFunction();

	/**
	 * Returns a new object of class '<em>Anonymous Access To Procedure</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Anonymous Access To Procedure</em>'.
	 * @generated
	 */
	AnonymousAccessToProcedure createAnonymousAccessToProcedure();

	/**
	 * Returns a new object of class '<em>Anonymous Access To Protected Function</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Anonymous Access To Protected Function</em>'.
	 * @generated
	 */
	AnonymousAccessToProtectedFunction createAnonymousAccessToProtectedFunction();

	/**
	 * Returns a new object of class '<em>Anonymous Access To Protected Procedure</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Anonymous Access To Protected Procedure</em>'.
	 * @generated
	 */
	AnonymousAccessToProtectedProcedure createAnonymousAccessToProtectedProcedure();

	/**
	 * Returns a new object of class '<em>Anonymous Access To Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Anonymous Access To Variable</em>'.
	 * @generated
	 */
	AnonymousAccessToVariable createAnonymousAccessToVariable();

	/**
	 * Returns a new object of class '<em>Array Component Association</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Array Component Association</em>'.
	 * @generated
	 */
	ArrayComponentAssociation createArrayComponentAssociation();

	/**
	 * Returns a new object of class '<em>Aspect Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aspect Specification</em>'.
	 * @generated
	 */
	AspectSpecification createAspectSpecification();

	/**
	 * Returns a new object of class '<em>Assertion Policy Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Assertion Policy Pragma</em>'.
	 * @generated
	 */
	AssertionPolicyPragma createAssertionPolicyPragma();

	/**
	 * Returns a new object of class '<em>Assert Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Assert Pragma</em>'.
	 * @generated
	 */
	AssertPragma createAssertPragma();

	/**
	 * Returns a new object of class '<em>Assignment Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Assignment Statement</em>'.
	 * @generated
	 */
	AssignmentStatement createAssignmentStatement();

	/**
	 * Returns a new object of class '<em>Association Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Association Class</em>'.
	 * @generated
	 */
	AssociationClass createAssociationClass();

	/**
	 * Returns a new object of class '<em>Association List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Association List</em>'.
	 * @generated
	 */
	AssociationList createAssociationList();

	/**
	 * Returns a new object of class '<em>Asynchronous Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Asynchronous Pragma</em>'.
	 * @generated
	 */
	AsynchronousPragma createAsynchronousPragma();

	/**
	 * Returns a new object of class '<em>Asynchronous Select Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Asynchronous Select Statement</em>'.
	 * @generated
	 */
	AsynchronousSelectStatement createAsynchronousSelectStatement();

	/**
	 * Returns a new object of class '<em>At Clause</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>At Clause</em>'.
	 * @generated
	 */
	AtClause createAtClause();

	/**
	 * Returns a new object of class '<em>Atomic Components Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Atomic Components Pragma</em>'.
	 * @generated
	 */
	AtomicComponentsPragma createAtomicComponentsPragma();

	/**
	 * Returns a new object of class '<em>Atomic Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Atomic Pragma</em>'.
	 * @generated
	 */
	AtomicPragma createAtomicPragma();

	/**
	 * Returns a new object of class '<em>Attach Handler Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attach Handler Pragma</em>'.
	 * @generated
	 */
	AttachHandlerPragma createAttachHandlerPragma();

	/**
	 * Returns a new object of class '<em>Attribute Definition Clause</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attribute Definition Clause</em>'.
	 * @generated
	 */
	AttributeDefinitionClause createAttributeDefinitionClause();

	/**
	 * Returns a new object of class '<em>Base Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Base Attribute</em>'.
	 * @generated
	 */
	BaseAttribute createBaseAttribute();

	/**
	 * Returns a new object of class '<em>Bit Order Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Bit Order Attribute</em>'.
	 * @generated
	 */
	BitOrderAttribute createBitOrderAttribute();

	/**
	 * Returns a new object of class '<em>Block Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Block Statement</em>'.
	 * @generated
	 */
	BlockStatement createBlockStatement();

	/**
	 * Returns a new object of class '<em>Body Version Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Body Version Attribute</em>'.
	 * @generated
	 */
	BodyVersionAttribute createBodyVersionAttribute();

	/**
	 * Returns a new object of class '<em>Box Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Box Expression</em>'.
	 * @generated
	 */
	BoxExpression createBoxExpression();

	/**
	 * Returns a new object of class '<em>Callable Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Callable Attribute</em>'.
	 * @generated
	 */
	CallableAttribute createCallableAttribute();

	/**
	 * Returns a new object of class '<em>Caller Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Caller Attribute</em>'.
	 * @generated
	 */
	CallerAttribute createCallerAttribute();

	/**
	 * Returns a new object of class '<em>Case Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Case Expression</em>'.
	 * @generated
	 */
	CaseExpression createCaseExpression();

	/**
	 * Returns a new object of class '<em>Case Expression Path</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Case Expression Path</em>'.
	 * @generated
	 */
	CaseExpressionPath createCaseExpressionPath();

	/**
	 * Returns a new object of class '<em>Case Path</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Case Path</em>'.
	 * @generated
	 */
	CasePath createCasePath();

	/**
	 * Returns a new object of class '<em>Case Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Case Statement</em>'.
	 * @generated
	 */
	CaseStatement createCaseStatement();

	/**
	 * Returns a new object of class '<em>Ceiling Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ceiling Attribute</em>'.
	 * @generated
	 */
	CeilingAttribute createCeilingAttribute();

	/**
	 * Returns a new object of class '<em>Character Literal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Character Literal</em>'.
	 * @generated
	 */
	CharacterLiteral createCharacterLiteral();

	/**
	 * Returns a new object of class '<em>Choice Parameter Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Choice Parameter Specification</em>'.
	 * @generated
	 */
	ChoiceParameterSpecification createChoiceParameterSpecification();

	/**
	 * Returns a new object of class '<em>Class Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Class Attribute</em>'.
	 * @generated
	 */
	ClassAttribute createClassAttribute();

	/**
	 * Returns a new object of class '<em>Code Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Code Statement</em>'.
	 * @generated
	 */
	CodeStatement createCodeStatement();

	/**
	 * Returns a new object of class '<em>Comment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Comment</em>'.
	 * @generated
	 */
	Comment createComment();

	/**
	 * Returns a new object of class '<em>Compilation Unit</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Compilation Unit</em>'.
	 * @generated
	 */
	CompilationUnit createCompilationUnit();

	/**
	 * Returns a new object of class '<em>Component Clause</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Component Clause</em>'.
	 * @generated
	 */
	ComponentClause createComponentClause();

	/**
	 * Returns a new object of class '<em>Component Clause List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Component Clause List</em>'.
	 * @generated
	 */
	ComponentClauseList createComponentClauseList();

	/**
	 * Returns a new object of class '<em>Component Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Component Declaration</em>'.
	 * @generated
	 */
	ComponentDeclaration createComponentDeclaration();

	/**
	 * Returns a new object of class '<em>Component Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Component Definition</em>'.
	 * @generated
	 */
	ComponentDefinition createComponentDefinition();

	/**
	 * Returns a new object of class '<em>Component Size Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Component Size Attribute</em>'.
	 * @generated
	 */
	ComponentSizeAttribute createComponentSizeAttribute();

	/**
	 * Returns a new object of class '<em>Compose Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Compose Attribute</em>'.
	 * @generated
	 */
	ComposeAttribute createComposeAttribute();

	/**
	 * Returns a new object of class '<em>Concatenate Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Concatenate Operator</em>'.
	 * @generated
	 */
	ConcatenateOperator createConcatenateOperator();

	/**
	 * Returns a new object of class '<em>Conditional Entry Call Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Conditional Entry Call Statement</em>'.
	 * @generated
	 */
	ConditionalEntryCallStatement createConditionalEntryCallStatement();

	/**
	 * Returns a new object of class '<em>Constant Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Constant Declaration</em>'.
	 * @generated
	 */
	ConstantDeclaration createConstantDeclaration();

	/**
	 * Returns a new object of class '<em>Constrained Array Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Constrained Array Definition</em>'.
	 * @generated
	 */
	ConstrainedArrayDefinition createConstrainedArrayDefinition();

	/**
	 * Returns a new object of class '<em>Constrained Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Constrained Attribute</em>'.
	 * @generated
	 */
	ConstrainedAttribute createConstrainedAttribute();

	/**
	 * Returns a new object of class '<em>Constraint Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Constraint Class</em>'.
	 * @generated
	 */
	ConstraintClass createConstraintClass();

	/**
	 * Returns a new object of class '<em>Context Clause Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Context Clause Class</em>'.
	 * @generated
	 */
	ContextClauseClass createContextClauseClass();

	/**
	 * Returns a new object of class '<em>Context Clause List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Context Clause List</em>'.
	 * @generated
	 */
	ContextClauseList createContextClauseList();

	/**
	 * Returns a new object of class '<em>Controlled Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Controlled Pragma</em>'.
	 * @generated
	 */
	ControlledPragma createControlledPragma();

	/**
	 * Returns a new object of class '<em>Convention Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Convention Pragma</em>'.
	 * @generated
	 */
	ConventionPragma createConventionPragma();

	/**
	 * Returns a new object of class '<em>Copy Sign Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Copy Sign Attribute</em>'.
	 * @generated
	 */
	CopySignAttribute createCopySignAttribute();

	/**
	 * Returns a new object of class '<em>Count Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Count Attribute</em>'.
	 * @generated
	 */
	CountAttribute createCountAttribute();

	/**
	 * Returns a new object of class '<em>Cpu Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cpu Pragma</em>'.
	 * @generated
	 */
	CpuPragma createCpuPragma();

	/**
	 * Returns a new object of class '<em>Decimal Fixed Point Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Decimal Fixed Point Definition</em>'.
	 * @generated
	 */
	DecimalFixedPointDefinition createDecimalFixedPointDefinition();

	/**
	 * Returns a new object of class '<em>Declaration Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Declaration Class</em>'.
	 * @generated
	 */
	DeclarationClass createDeclarationClass();

	/**
	 * Returns a new object of class '<em>Declaration List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Declaration List</em>'.
	 * @generated
	 */
	DeclarationList createDeclarationList();

	/**
	 * Returns a new object of class '<em>Declarative Item Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Declarative Item Class</em>'.
	 * @generated
	 */
	DeclarativeItemClass createDeclarativeItemClass();

	/**
	 * Returns a new object of class '<em>Declarative Item List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Declarative Item List</em>'.
	 * @generated
	 */
	DeclarativeItemList createDeclarativeItemList();

	/**
	 * Returns a new object of class '<em>Default Storage Pool Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Default Storage Pool Pragma</em>'.
	 * @generated
	 */
	DefaultStoragePoolPragma createDefaultStoragePoolPragma();

	/**
	 * Returns a new object of class '<em>Deferred Constant Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Deferred Constant Declaration</em>'.
	 * @generated
	 */
	DeferredConstantDeclaration createDeferredConstantDeclaration();

	/**
	 * Returns a new object of class '<em>Defining Abs Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Abs Operator</em>'.
	 * @generated
	 */
	DefiningAbsOperator createDefiningAbsOperator();

	/**
	 * Returns a new object of class '<em>Defining And Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining And Operator</em>'.
	 * @generated
	 */
	DefiningAndOperator createDefiningAndOperator();

	/**
	 * Returns a new object of class '<em>Defining Character Literal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Character Literal</em>'.
	 * @generated
	 */
	DefiningCharacterLiteral createDefiningCharacterLiteral();

	/**
	 * Returns a new object of class '<em>Defining Concatenate Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Concatenate Operator</em>'.
	 * @generated
	 */
	DefiningConcatenateOperator createDefiningConcatenateOperator();

	/**
	 * Returns a new object of class '<em>Defining Divide Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Divide Operator</em>'.
	 * @generated
	 */
	DefiningDivideOperator createDefiningDivideOperator();

	/**
	 * Returns a new object of class '<em>Defining Enumeration Literal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Enumeration Literal</em>'.
	 * @generated
	 */
	DefiningEnumerationLiteral createDefiningEnumerationLiteral();

	/**
	 * Returns a new object of class '<em>Defining Equal Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Equal Operator</em>'.
	 * @generated
	 */
	DefiningEqualOperator createDefiningEqualOperator();

	/**
	 * Returns a new object of class '<em>Defining Expanded Name</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Expanded Name</em>'.
	 * @generated
	 */
	DefiningExpandedName createDefiningExpandedName();

	/**
	 * Returns a new object of class '<em>Defining Exponentiate Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Exponentiate Operator</em>'.
	 * @generated
	 */
	DefiningExponentiateOperator createDefiningExponentiateOperator();

	/**
	 * Returns a new object of class '<em>Defining Greater Than Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Greater Than Operator</em>'.
	 * @generated
	 */
	DefiningGreaterThanOperator createDefiningGreaterThanOperator();

	/**
	 * Returns a new object of class '<em>Defining Greater Than Or Equal Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Greater Than Or Equal Operator</em>'.
	 * @generated
	 */
	DefiningGreaterThanOrEqualOperator createDefiningGreaterThanOrEqualOperator();

	/**
	 * Returns a new object of class '<em>Defining Identifier</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Identifier</em>'.
	 * @generated
	 */
	DefiningIdentifier createDefiningIdentifier();

	/**
	 * Returns a new object of class '<em>Defining Less Than Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Less Than Operator</em>'.
	 * @generated
	 */
	DefiningLessThanOperator createDefiningLessThanOperator();

	/**
	 * Returns a new object of class '<em>Defining Less Than Or Equal Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Less Than Or Equal Operator</em>'.
	 * @generated
	 */
	DefiningLessThanOrEqualOperator createDefiningLessThanOrEqualOperator();

	/**
	 * Returns a new object of class '<em>Defining Minus Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Minus Operator</em>'.
	 * @generated
	 */
	DefiningMinusOperator createDefiningMinusOperator();

	/**
	 * Returns a new object of class '<em>Defining Mod Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Mod Operator</em>'.
	 * @generated
	 */
	DefiningModOperator createDefiningModOperator();

	/**
	 * Returns a new object of class '<em>Defining Multiply Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Multiply Operator</em>'.
	 * @generated
	 */
	DefiningMultiplyOperator createDefiningMultiplyOperator();

	/**
	 * Returns a new object of class '<em>Defining Name Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Name Class</em>'.
	 * @generated
	 */
	DefiningNameClass createDefiningNameClass();

	/**
	 * Returns a new object of class '<em>Defining Name List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Name List</em>'.
	 * @generated
	 */
	DefiningNameList createDefiningNameList();

	/**
	 * Returns a new object of class '<em>Defining Not Equal Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Not Equal Operator</em>'.
	 * @generated
	 */
	DefiningNotEqualOperator createDefiningNotEqualOperator();

	/**
	 * Returns a new object of class '<em>Defining Not Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Not Operator</em>'.
	 * @generated
	 */
	DefiningNotOperator createDefiningNotOperator();

	/**
	 * Returns a new object of class '<em>Defining Or Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Or Operator</em>'.
	 * @generated
	 */
	DefiningOrOperator createDefiningOrOperator();

	/**
	 * Returns a new object of class '<em>Defining Plus Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Plus Operator</em>'.
	 * @generated
	 */
	DefiningPlusOperator createDefiningPlusOperator();

	/**
	 * Returns a new object of class '<em>Defining Rem Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Rem Operator</em>'.
	 * @generated
	 */
	DefiningRemOperator createDefiningRemOperator();

	/**
	 * Returns a new object of class '<em>Defining Unary Minus Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Unary Minus Operator</em>'.
	 * @generated
	 */
	DefiningUnaryMinusOperator createDefiningUnaryMinusOperator();

	/**
	 * Returns a new object of class '<em>Defining Unary Plus Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Unary Plus Operator</em>'.
	 * @generated
	 */
	DefiningUnaryPlusOperator createDefiningUnaryPlusOperator();

	/**
	 * Returns a new object of class '<em>Defining Xor Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Defining Xor Operator</em>'.
	 * @generated
	 */
	DefiningXorOperator createDefiningXorOperator();

	/**
	 * Returns a new object of class '<em>Definite Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Definite Attribute</em>'.
	 * @generated
	 */
	DefiniteAttribute createDefiniteAttribute();

	/**
	 * Returns a new object of class '<em>Definition Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Definition Class</em>'.
	 * @generated
	 */
	DefinitionClass createDefinitionClass();

	/**
	 * Returns a new object of class '<em>Definition List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Definition List</em>'.
	 * @generated
	 */
	DefinitionList createDefinitionList();

	/**
	 * Returns a new object of class '<em>Delay Relative Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Delay Relative Statement</em>'.
	 * @generated
	 */
	DelayRelativeStatement createDelayRelativeStatement();

	/**
	 * Returns a new object of class '<em>Delay Until Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Delay Until Statement</em>'.
	 * @generated
	 */
	DelayUntilStatement createDelayUntilStatement();

	/**
	 * Returns a new object of class '<em>Delta Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Delta Attribute</em>'.
	 * @generated
	 */
	DeltaAttribute createDeltaAttribute();

	/**
	 * Returns a new object of class '<em>Delta Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Delta Constraint</em>'.
	 * @generated
	 */
	DeltaConstraint createDeltaConstraint();

	/**
	 * Returns a new object of class '<em>Denorm Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Denorm Attribute</em>'.
	 * @generated
	 */
	DenormAttribute createDenormAttribute();

	/**
	 * Returns a new object of class '<em>Derived Record Extension Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Derived Record Extension Definition</em>'.
	 * @generated
	 */
	DerivedRecordExtensionDefinition createDerivedRecordExtensionDefinition();

	/**
	 * Returns a new object of class '<em>Derived Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Derived Type Definition</em>'.
	 * @generated
	 */
	DerivedTypeDefinition createDerivedTypeDefinition();

	/**
	 * Returns a new object of class '<em>Detect Blocking Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Detect Blocking Pragma</em>'.
	 * @generated
	 */
	DetectBlockingPragma createDetectBlockingPragma();

	/**
	 * Returns a new object of class '<em>Digits Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Digits Attribute</em>'.
	 * @generated
	 */
	DigitsAttribute createDigitsAttribute();

	/**
	 * Returns a new object of class '<em>Digits Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Digits Constraint</em>'.
	 * @generated
	 */
	DigitsConstraint createDigitsConstraint();

	/**
	 * Returns a new object of class '<em>Discard Names Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Discard Names Pragma</em>'.
	 * @generated
	 */
	DiscardNamesPragma createDiscardNamesPragma();

	/**
	 * Returns a new object of class '<em>Discrete Range Attribute Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Discrete Range Attribute Reference</em>'.
	 * @generated
	 */
	DiscreteRangeAttributeReference createDiscreteRangeAttributeReference();

	/**
	 * Returns a new object of class '<em>Discrete Range Attribute Reference As Subtype Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Discrete Range Attribute Reference As Subtype Definition</em>'.
	 * @generated
	 */
	DiscreteRangeAttributeReferenceAsSubtypeDefinition createDiscreteRangeAttributeReferenceAsSubtypeDefinition();

	/**
	 * Returns a new object of class '<em>Discrete Range Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Discrete Range Class</em>'.
	 * @generated
	 */
	DiscreteRangeClass createDiscreteRangeClass();

	/**
	 * Returns a new object of class '<em>Discrete Range List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Discrete Range List</em>'.
	 * @generated
	 */
	DiscreteRangeList createDiscreteRangeList();

	/**
	 * Returns a new object of class '<em>Discrete Simple Expression Range</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Discrete Simple Expression Range</em>'.
	 * @generated
	 */
	DiscreteSimpleExpressionRange createDiscreteSimpleExpressionRange();

	/**
	 * Returns a new object of class '<em>Discrete Simple Expression Range As Subtype Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Discrete Simple Expression Range As Subtype Definition</em>'.
	 * @generated
	 */
	DiscreteSimpleExpressionRangeAsSubtypeDefinition createDiscreteSimpleExpressionRangeAsSubtypeDefinition();

	/**
	 * Returns a new object of class '<em>Discrete Subtype Definition Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Discrete Subtype Definition Class</em>'.
	 * @generated
	 */
	DiscreteSubtypeDefinitionClass createDiscreteSubtypeDefinitionClass();

	/**
	 * Returns a new object of class '<em>Discrete Subtype Indication</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Discrete Subtype Indication</em>'.
	 * @generated
	 */
	DiscreteSubtypeIndication createDiscreteSubtypeIndication();

	/**
	 * Returns a new object of class '<em>Discrete Subtype Indication As Subtype Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Discrete Subtype Indication As Subtype Definition</em>'.
	 * @generated
	 */
	DiscreteSubtypeIndicationAsSubtypeDefinition createDiscreteSubtypeIndicationAsSubtypeDefinition();

	/**
	 * Returns a new object of class '<em>Discriminant Association</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Discriminant Association</em>'.
	 * @generated
	 */
	DiscriminantAssociation createDiscriminantAssociation();

	/**
	 * Returns a new object of class '<em>Discriminant Association List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Discriminant Association List</em>'.
	 * @generated
	 */
	DiscriminantAssociationList createDiscriminantAssociationList();

	/**
	 * Returns a new object of class '<em>Discriminant Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Discriminant Constraint</em>'.
	 * @generated
	 */
	DiscriminantConstraint createDiscriminantConstraint();

	/**
	 * Returns a new object of class '<em>Discriminant Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Discriminant Specification</em>'.
	 * @generated
	 */
	DiscriminantSpecification createDiscriminantSpecification();

	/**
	 * Returns a new object of class '<em>Discriminant Specification List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Discriminant Specification List</em>'.
	 * @generated
	 */
	DiscriminantSpecificationList createDiscriminantSpecificationList();

	/**
	 * Returns a new object of class '<em>Dispatching Domain Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dispatching Domain Pragma</em>'.
	 * @generated
	 */
	DispatchingDomainPragma createDispatchingDomainPragma();

	/**
	 * Returns a new object of class '<em>Divide Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Divide Operator</em>'.
	 * @generated
	 */
	DivideOperator createDivideOperator();

	/**
	 * Returns a new object of class '<em>Document Root</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Document Root</em>'.
	 * @generated
	 */
	DocumentRoot createDocumentRoot();

	/**
	 * Returns a new object of class '<em>Elaborate All Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Elaborate All Pragma</em>'.
	 * @generated
	 */
	ElaborateAllPragma createElaborateAllPragma();

	/**
	 * Returns a new object of class '<em>Elaborate Body Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Elaborate Body Pragma</em>'.
	 * @generated
	 */
	ElaborateBodyPragma createElaborateBodyPragma();

	/**
	 * Returns a new object of class '<em>Elaborate Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Elaborate Pragma</em>'.
	 * @generated
	 */
	ElaboratePragma createElaboratePragma();

	/**
	 * Returns a new object of class '<em>Element Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Element Class</em>'.
	 * @generated
	 */
	ElementClass createElementClass();

	/**
	 * Returns a new object of class '<em>Element Iterator Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Element Iterator Specification</em>'.
	 * @generated
	 */
	ElementIteratorSpecification createElementIteratorSpecification();

	/**
	 * Returns a new object of class '<em>Element List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Element List</em>'.
	 * @generated
	 */
	ElementList createElementList();

	/**
	 * Returns a new object of class '<em>Else Expression Path</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Else Expression Path</em>'.
	 * @generated
	 */
	ElseExpressionPath createElseExpressionPath();

	/**
	 * Returns a new object of class '<em>Else Path</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Else Path</em>'.
	 * @generated
	 */
	ElsePath createElsePath();

	/**
	 * Returns a new object of class '<em>Elsif Expression Path</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Elsif Expression Path</em>'.
	 * @generated
	 */
	ElsifExpressionPath createElsifExpressionPath();

	/**
	 * Returns a new object of class '<em>Elsif Path</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Elsif Path</em>'.
	 * @generated
	 */
	ElsifPath createElsifPath();

	/**
	 * Returns a new object of class '<em>Entry Body Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Entry Body Declaration</em>'.
	 * @generated
	 */
	EntryBodyDeclaration createEntryBodyDeclaration();

	/**
	 * Returns a new object of class '<em>Entry Call Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Entry Call Statement</em>'.
	 * @generated
	 */
	EntryCallStatement createEntryCallStatement();

	/**
	 * Returns a new object of class '<em>Entry Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Entry Declaration</em>'.
	 * @generated
	 */
	EntryDeclaration createEntryDeclaration();

	/**
	 * Returns a new object of class '<em>Entry Index Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Entry Index Specification</em>'.
	 * @generated
	 */
	EntryIndexSpecification createEntryIndexSpecification();

	/**
	 * Returns a new object of class '<em>Enumeration Literal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Enumeration Literal</em>'.
	 * @generated
	 */
	EnumerationLiteral createEnumerationLiteral();

	/**
	 * Returns a new object of class '<em>Enumeration Literal Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Enumeration Literal Specification</em>'.
	 * @generated
	 */
	EnumerationLiteralSpecification createEnumerationLiteralSpecification();

	/**
	 * Returns a new object of class '<em>Enumeration Representation Clause</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Enumeration Representation Clause</em>'.
	 * @generated
	 */
	EnumerationRepresentationClause createEnumerationRepresentationClause();

	/**
	 * Returns a new object of class '<em>Enumeration Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Enumeration Type Definition</em>'.
	 * @generated
	 */
	EnumerationTypeDefinition createEnumerationTypeDefinition();

	/**
	 * Returns a new object of class '<em>Equal Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Equal Operator</em>'.
	 * @generated
	 */
	EqualOperator createEqualOperator();

	/**
	 * Returns a new object of class '<em>Exception Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Exception Declaration</em>'.
	 * @generated
	 */
	ExceptionDeclaration createExceptionDeclaration();

	/**
	 * Returns a new object of class '<em>Exception Handler</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Exception Handler</em>'.
	 * @generated
	 */
	ExceptionHandler createExceptionHandler();

	/**
	 * Returns a new object of class '<em>Exception Handler List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Exception Handler List</em>'.
	 * @generated
	 */
	ExceptionHandlerList createExceptionHandlerList();

	/**
	 * Returns a new object of class '<em>Exception Renaming Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Exception Renaming Declaration</em>'.
	 * @generated
	 */
	ExceptionRenamingDeclaration createExceptionRenamingDeclaration();

	/**
	 * Returns a new object of class '<em>Exit Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Exit Statement</em>'.
	 * @generated
	 */
	ExitStatement createExitStatement();

	/**
	 * Returns a new object of class '<em>Explicit Dereference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Explicit Dereference</em>'.
	 * @generated
	 */
	ExplicitDereference createExplicitDereference();

	/**
	 * Returns a new object of class '<em>Exponent Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Exponent Attribute</em>'.
	 * @generated
	 */
	ExponentAttribute createExponentAttribute();

	/**
	 * Returns a new object of class '<em>Exponentiate Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Exponentiate Operator</em>'.
	 * @generated
	 */
	ExponentiateOperator createExponentiateOperator();

	/**
	 * Returns a new object of class '<em>Export Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Export Pragma</em>'.
	 * @generated
	 */
	ExportPragma createExportPragma();

	/**
	 * Returns a new object of class '<em>Expression Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Expression Class</em>'.
	 * @generated
	 */
	ExpressionClass createExpressionClass();

	/**
	 * Returns a new object of class '<em>Expression Function Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Expression Function Declaration</em>'.
	 * @generated
	 */
	ExpressionFunctionDeclaration createExpressionFunctionDeclaration();

	/**
	 * Returns a new object of class '<em>Expression List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Expression List</em>'.
	 * @generated
	 */
	ExpressionList createExpressionList();

	/**
	 * Returns a new object of class '<em>Extended Return Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Extended Return Statement</em>'.
	 * @generated
	 */
	ExtendedReturnStatement createExtendedReturnStatement();

	/**
	 * Returns a new object of class '<em>Extension Aggregate</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Extension Aggregate</em>'.
	 * @generated
	 */
	ExtensionAggregate createExtensionAggregate();

	/**
	 * Returns a new object of class '<em>External Tag Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>External Tag Attribute</em>'.
	 * @generated
	 */
	ExternalTagAttribute createExternalTagAttribute();

	/**
	 * Returns a new object of class '<em>First Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>First Attribute</em>'.
	 * @generated
	 */
	FirstAttribute createFirstAttribute();

	/**
	 * Returns a new object of class '<em>First Bit Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>First Bit Attribute</em>'.
	 * @generated
	 */
	FirstBitAttribute createFirstBitAttribute();

	/**
	 * Returns a new object of class '<em>Floating Point Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Floating Point Definition</em>'.
	 * @generated
	 */
	FloatingPointDefinition createFloatingPointDefinition();

	/**
	 * Returns a new object of class '<em>Floor Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Floor Attribute</em>'.
	 * @generated
	 */
	FloorAttribute createFloorAttribute();

	/**
	 * Returns a new object of class '<em>For All Quantified Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>For All Quantified Expression</em>'.
	 * @generated
	 */
	ForAllQuantifiedExpression createForAllQuantifiedExpression();

	/**
	 * Returns a new object of class '<em>Fore Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fore Attribute</em>'.
	 * @generated
	 */
	ForeAttribute createForeAttribute();

	/**
	 * Returns a new object of class '<em>For Loop Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>For Loop Statement</em>'.
	 * @generated
	 */
	ForLoopStatement createForLoopStatement();

	/**
	 * Returns a new object of class '<em>Formal Access To Constant</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Access To Constant</em>'.
	 * @generated
	 */
	FormalAccessToConstant createFormalAccessToConstant();

	/**
	 * Returns a new object of class '<em>Formal Access To Function</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Access To Function</em>'.
	 * @generated
	 */
	FormalAccessToFunction createFormalAccessToFunction();

	/**
	 * Returns a new object of class '<em>Formal Access To Procedure</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Access To Procedure</em>'.
	 * @generated
	 */
	FormalAccessToProcedure createFormalAccessToProcedure();

	/**
	 * Returns a new object of class '<em>Formal Access To Protected Function</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Access To Protected Function</em>'.
	 * @generated
	 */
	FormalAccessToProtectedFunction createFormalAccessToProtectedFunction();

	/**
	 * Returns a new object of class '<em>Formal Access To Protected Procedure</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Access To Protected Procedure</em>'.
	 * @generated
	 */
	FormalAccessToProtectedProcedure createFormalAccessToProtectedProcedure();

	/**
	 * Returns a new object of class '<em>Formal Access To Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Access To Variable</em>'.
	 * @generated
	 */
	FormalAccessToVariable createFormalAccessToVariable();

	/**
	 * Returns a new object of class '<em>Formal Constrained Array Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Constrained Array Definition</em>'.
	 * @generated
	 */
	FormalConstrainedArrayDefinition createFormalConstrainedArrayDefinition();

	/**
	 * Returns a new object of class '<em>Formal Decimal Fixed Point Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Decimal Fixed Point Definition</em>'.
	 * @generated
	 */
	FormalDecimalFixedPointDefinition createFormalDecimalFixedPointDefinition();

	/**
	 * Returns a new object of class '<em>Formal Derived Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Derived Type Definition</em>'.
	 * @generated
	 */
	FormalDerivedTypeDefinition createFormalDerivedTypeDefinition();

	/**
	 * Returns a new object of class '<em>Formal Discrete Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Discrete Type Definition</em>'.
	 * @generated
	 */
	FormalDiscreteTypeDefinition createFormalDiscreteTypeDefinition();

	/**
	 * Returns a new object of class '<em>Formal Floating Point Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Floating Point Definition</em>'.
	 * @generated
	 */
	FormalFloatingPointDefinition createFormalFloatingPointDefinition();

	/**
	 * Returns a new object of class '<em>Formal Function Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Function Declaration</em>'.
	 * @generated
	 */
	FormalFunctionDeclaration createFormalFunctionDeclaration();

	/**
	 * Returns a new object of class '<em>Formal Incomplete Type Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Incomplete Type Declaration</em>'.
	 * @generated
	 */
	FormalIncompleteTypeDeclaration createFormalIncompleteTypeDeclaration();

	/**
	 * Returns a new object of class '<em>Formal Limited Interface</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Limited Interface</em>'.
	 * @generated
	 */
	FormalLimitedInterface createFormalLimitedInterface();

	/**
	 * Returns a new object of class '<em>Formal Modular Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Modular Type Definition</em>'.
	 * @generated
	 */
	FormalModularTypeDefinition createFormalModularTypeDefinition();

	/**
	 * Returns a new object of class '<em>Formal Object Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Object Declaration</em>'.
	 * @generated
	 */
	FormalObjectDeclaration createFormalObjectDeclaration();

	/**
	 * Returns a new object of class '<em>Formal Ordinary Fixed Point Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Ordinary Fixed Point Definition</em>'.
	 * @generated
	 */
	FormalOrdinaryFixedPointDefinition createFormalOrdinaryFixedPointDefinition();

	/**
	 * Returns a new object of class '<em>Formal Ordinary Interface</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Ordinary Interface</em>'.
	 * @generated
	 */
	FormalOrdinaryInterface createFormalOrdinaryInterface();

	/**
	 * Returns a new object of class '<em>Formal Package Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Package Declaration</em>'.
	 * @generated
	 */
	FormalPackageDeclaration createFormalPackageDeclaration();

	/**
	 * Returns a new object of class '<em>Formal Package Declaration With Box</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Package Declaration With Box</em>'.
	 * @generated
	 */
	FormalPackageDeclarationWithBox createFormalPackageDeclarationWithBox();

	/**
	 * Returns a new object of class '<em>Formal Pool Specific Access To Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Pool Specific Access To Variable</em>'.
	 * @generated
	 */
	FormalPoolSpecificAccessToVariable createFormalPoolSpecificAccessToVariable();

	/**
	 * Returns a new object of class '<em>Formal Private Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Private Type Definition</em>'.
	 * @generated
	 */
	FormalPrivateTypeDefinition createFormalPrivateTypeDefinition();

	/**
	 * Returns a new object of class '<em>Formal Procedure Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Procedure Declaration</em>'.
	 * @generated
	 */
	FormalProcedureDeclaration createFormalProcedureDeclaration();

	/**
	 * Returns a new object of class '<em>Formal Protected Interface</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Protected Interface</em>'.
	 * @generated
	 */
	FormalProtectedInterface createFormalProtectedInterface();

	/**
	 * Returns a new object of class '<em>Formal Signed Integer Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Signed Integer Type Definition</em>'.
	 * @generated
	 */
	FormalSignedIntegerTypeDefinition createFormalSignedIntegerTypeDefinition();

	/**
	 * Returns a new object of class '<em>Formal Synchronized Interface</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Synchronized Interface</em>'.
	 * @generated
	 */
	FormalSynchronizedInterface createFormalSynchronizedInterface();

	/**
	 * Returns a new object of class '<em>Formal Tagged Private Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Tagged Private Type Definition</em>'.
	 * @generated
	 */
	FormalTaggedPrivateTypeDefinition createFormalTaggedPrivateTypeDefinition();

	/**
	 * Returns a new object of class '<em>Formal Task Interface</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Task Interface</em>'.
	 * @generated
	 */
	FormalTaskInterface createFormalTaskInterface();

	/**
	 * Returns a new object of class '<em>Formal Type Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Type Declaration</em>'.
	 * @generated
	 */
	FormalTypeDeclaration createFormalTypeDeclaration();

	/**
	 * Returns a new object of class '<em>Formal Unconstrained Array Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Formal Unconstrained Array Definition</em>'.
	 * @generated
	 */
	FormalUnconstrainedArrayDefinition createFormalUnconstrainedArrayDefinition();

	/**
	 * Returns a new object of class '<em>For Some Quantified Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>For Some Quantified Expression</em>'.
	 * @generated
	 */
	ForSomeQuantifiedExpression createForSomeQuantifiedExpression();

	/**
	 * Returns a new object of class '<em>Fraction Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fraction Attribute</em>'.
	 * @generated
	 */
	FractionAttribute createFractionAttribute();

	/**
	 * Returns a new object of class '<em>Function Body Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Function Body Declaration</em>'.
	 * @generated
	 */
	FunctionBodyDeclaration createFunctionBodyDeclaration();

	/**
	 * Returns a new object of class '<em>Function Body Stub</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Function Body Stub</em>'.
	 * @generated
	 */
	FunctionBodyStub createFunctionBodyStub();

	/**
	 * Returns a new object of class '<em>Function Call</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Function Call</em>'.
	 * @generated
	 */
	FunctionCall createFunctionCall();

	/**
	 * Returns a new object of class '<em>Function Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Function Declaration</em>'.
	 * @generated
	 */
	FunctionDeclaration createFunctionDeclaration();

	/**
	 * Returns a new object of class '<em>Function Instantiation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Function Instantiation</em>'.
	 * @generated
	 */
	FunctionInstantiation createFunctionInstantiation();

	/**
	 * Returns a new object of class '<em>Function Renaming Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Function Renaming Declaration</em>'.
	 * @generated
	 */
	FunctionRenamingDeclaration createFunctionRenamingDeclaration();

	/**
	 * Returns a new object of class '<em>Generalized Iterator Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Generalized Iterator Specification</em>'.
	 * @generated
	 */
	GeneralizedIteratorSpecification createGeneralizedIteratorSpecification();

	/**
	 * Returns a new object of class '<em>Generic Association</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Generic Association</em>'.
	 * @generated
	 */
	GenericAssociation createGenericAssociation();

	/**
	 * Returns a new object of class '<em>Generic Function Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Generic Function Declaration</em>'.
	 * @generated
	 */
	GenericFunctionDeclaration createGenericFunctionDeclaration();

	/**
	 * Returns a new object of class '<em>Generic Function Renaming Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Generic Function Renaming Declaration</em>'.
	 * @generated
	 */
	GenericFunctionRenamingDeclaration createGenericFunctionRenamingDeclaration();

	/**
	 * Returns a new object of class '<em>Generic Package Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Generic Package Declaration</em>'.
	 * @generated
	 */
	GenericPackageDeclaration createGenericPackageDeclaration();

	/**
	 * Returns a new object of class '<em>Generic Package Renaming Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Generic Package Renaming Declaration</em>'.
	 * @generated
	 */
	GenericPackageRenamingDeclaration createGenericPackageRenamingDeclaration();

	/**
	 * Returns a new object of class '<em>Generic Procedure Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Generic Procedure Declaration</em>'.
	 * @generated
	 */
	GenericProcedureDeclaration createGenericProcedureDeclaration();

	/**
	 * Returns a new object of class '<em>Generic Procedure Renaming Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Generic Procedure Renaming Declaration</em>'.
	 * @generated
	 */
	GenericProcedureRenamingDeclaration createGenericProcedureRenamingDeclaration();

	/**
	 * Returns a new object of class '<em>Goto Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Goto Statement</em>'.
	 * @generated
	 */
	GotoStatement createGotoStatement();

	/**
	 * Returns a new object of class '<em>Greater Than Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Greater Than Operator</em>'.
	 * @generated
	 */
	GreaterThanOperator createGreaterThanOperator();

	/**
	 * Returns a new object of class '<em>Greater Than Or Equal Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Greater Than Or Equal Operator</em>'.
	 * @generated
	 */
	GreaterThanOrEqualOperator createGreaterThanOrEqualOperator();

	/**
	 * Returns a new object of class '<em>Has Abstract QType</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Abstract QType</em>'.
	 * @generated
	 */
	HasAbstractQType createHasAbstractQType();

	/**
	 * Returns a new object of class '<em>Has Abstract QType1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Abstract QType1</em>'.
	 * @generated
	 */
	HasAbstractQType1 createHasAbstractQType1();

	/**
	 * Returns a new object of class '<em>Has Abstract QType2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Abstract QType2</em>'.
	 * @generated
	 */
	HasAbstractQType2 createHasAbstractQType2();

	/**
	 * Returns a new object of class '<em>Has Abstract QType3</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Abstract QType3</em>'.
	 * @generated
	 */
	HasAbstractQType3 createHasAbstractQType3();

	/**
	 * Returns a new object of class '<em>Has Abstract QType4</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Abstract QType4</em>'.
	 * @generated
	 */
	HasAbstractQType4 createHasAbstractQType4();

	/**
	 * Returns a new object of class '<em>Has Abstract QType5</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Abstract QType5</em>'.
	 * @generated
	 */
	HasAbstractQType5 createHasAbstractQType5();

	/**
	 * Returns a new object of class '<em>Has Abstract QType6</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Abstract QType6</em>'.
	 * @generated
	 */
	HasAbstractQType6 createHasAbstractQType6();

	/**
	 * Returns a new object of class '<em>Has Abstract QType7</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Abstract QType7</em>'.
	 * @generated
	 */
	HasAbstractQType7 createHasAbstractQType7();

	/**
	 * Returns a new object of class '<em>Has Abstract QType8</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Abstract QType8</em>'.
	 * @generated
	 */
	HasAbstractQType8 createHasAbstractQType8();

	/**
	 * Returns a new object of class '<em>Has Abstract QType9</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Abstract QType9</em>'.
	 * @generated
	 */
	HasAbstractQType9 createHasAbstractQType9();

	/**
	 * Returns a new object of class '<em>Has Abstract QType10</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Abstract QType10</em>'.
	 * @generated
	 */
	HasAbstractQType10 createHasAbstractQType10();

	/**
	 * Returns a new object of class '<em>Has Abstract QType11</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Abstract QType11</em>'.
	 * @generated
	 */
	HasAbstractQType11 createHasAbstractQType11();

	/**
	 * Returns a new object of class '<em>Has Abstract QType12</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Abstract QType12</em>'.
	 * @generated
	 */
	HasAbstractQType12 createHasAbstractQType12();

	/**
	 * Returns a new object of class '<em>Has Abstract QType13</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Abstract QType13</em>'.
	 * @generated
	 */
	HasAbstractQType13 createHasAbstractQType13();

	/**
	 * Returns a new object of class '<em>Has Aliased QType</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Aliased QType</em>'.
	 * @generated
	 */
	HasAliasedQType createHasAliasedQType();

	/**
	 * Returns a new object of class '<em>Has Aliased QType1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Aliased QType1</em>'.
	 * @generated
	 */
	HasAliasedQType1 createHasAliasedQType1();

	/**
	 * Returns a new object of class '<em>Has Aliased QType2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Aliased QType2</em>'.
	 * @generated
	 */
	HasAliasedQType2 createHasAliasedQType2();

	/**
	 * Returns a new object of class '<em>Has Aliased QType3</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Aliased QType3</em>'.
	 * @generated
	 */
	HasAliasedQType3 createHasAliasedQType3();

	/**
	 * Returns a new object of class '<em>Has Aliased QType4</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Aliased QType4</em>'.
	 * @generated
	 */
	HasAliasedQType4 createHasAliasedQType4();

	/**
	 * Returns a new object of class '<em>Has Aliased QType5</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Aliased QType5</em>'.
	 * @generated
	 */
	HasAliasedQType5 createHasAliasedQType5();

	/**
	 * Returns a new object of class '<em>Has Aliased QType6</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Aliased QType6</em>'.
	 * @generated
	 */
	HasAliasedQType6 createHasAliasedQType6();

	/**
	 * Returns a new object of class '<em>Has Aliased QType7</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Aliased QType7</em>'.
	 * @generated
	 */
	HasAliasedQType7 createHasAliasedQType7();

	/**
	 * Returns a new object of class '<em>Has Aliased QType8</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Aliased QType8</em>'.
	 * @generated
	 */
	HasAliasedQType8 createHasAliasedQType8();

	/**
	 * Returns a new object of class '<em>Has Limited QType</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Limited QType</em>'.
	 * @generated
	 */
	HasLimitedQType createHasLimitedQType();

	/**
	 * Returns a new object of class '<em>Has Limited QType1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Limited QType1</em>'.
	 * @generated
	 */
	HasLimitedQType1 createHasLimitedQType1();

	/**
	 * Returns a new object of class '<em>Has Limited QType2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Limited QType2</em>'.
	 * @generated
	 */
	HasLimitedQType2 createHasLimitedQType2();

	/**
	 * Returns a new object of class '<em>Has Limited QType3</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Limited QType3</em>'.
	 * @generated
	 */
	HasLimitedQType3 createHasLimitedQType3();

	/**
	 * Returns a new object of class '<em>Has Limited QType4</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Limited QType4</em>'.
	 * @generated
	 */
	HasLimitedQType4 createHasLimitedQType4();

	/**
	 * Returns a new object of class '<em>Has Limited QType5</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Limited QType5</em>'.
	 * @generated
	 */
	HasLimitedQType5 createHasLimitedQType5();

	/**
	 * Returns a new object of class '<em>Has Limited QType6</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Limited QType6</em>'.
	 * @generated
	 */
	HasLimitedQType6 createHasLimitedQType6();

	/**
	 * Returns a new object of class '<em>Has Limited QType7</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Limited QType7</em>'.
	 * @generated
	 */
	HasLimitedQType7 createHasLimitedQType7();

	/**
	 * Returns a new object of class '<em>Has Limited QType8</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Limited QType8</em>'.
	 * @generated
	 */
	HasLimitedQType8 createHasLimitedQType8();

	/**
	 * Returns a new object of class '<em>Has Limited QType9</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Limited QType9</em>'.
	 * @generated
	 */
	HasLimitedQType9 createHasLimitedQType9();

	/**
	 * Returns a new object of class '<em>Has Limited QType10</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Limited QType10</em>'.
	 * @generated
	 */
	HasLimitedQType10 createHasLimitedQType10();

	/**
	 * Returns a new object of class '<em>Has Limited QType11</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Limited QType11</em>'.
	 * @generated
	 */
	HasLimitedQType11 createHasLimitedQType11();

	/**
	 * Returns a new object of class '<em>Has Null Exclusion QType</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Null Exclusion QType</em>'.
	 * @generated
	 */
	HasNullExclusionQType createHasNullExclusionQType();

	/**
	 * Returns a new object of class '<em>Has Null Exclusion QType1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Null Exclusion QType1</em>'.
	 * @generated
	 */
	HasNullExclusionQType1 createHasNullExclusionQType1();

	/**
	 * Returns a new object of class '<em>Has Null Exclusion QType2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Null Exclusion QType2</em>'.
	 * @generated
	 */
	HasNullExclusionQType2 createHasNullExclusionQType2();

	/**
	 * Returns a new object of class '<em>Has Null Exclusion QType3</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Null Exclusion QType3</em>'.
	 * @generated
	 */
	HasNullExclusionQType3 createHasNullExclusionQType3();

	/**
	 * Returns a new object of class '<em>Has Null Exclusion QType4</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Null Exclusion QType4</em>'.
	 * @generated
	 */
	HasNullExclusionQType4 createHasNullExclusionQType4();

	/**
	 * Returns a new object of class '<em>Has Null Exclusion QType5</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Null Exclusion QType5</em>'.
	 * @generated
	 */
	HasNullExclusionQType5 createHasNullExclusionQType5();

	/**
	 * Returns a new object of class '<em>Has Null Exclusion QType6</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Null Exclusion QType6</em>'.
	 * @generated
	 */
	HasNullExclusionQType6 createHasNullExclusionQType6();

	/**
	 * Returns a new object of class '<em>Has Null Exclusion QType7</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Null Exclusion QType7</em>'.
	 * @generated
	 */
	HasNullExclusionQType7 createHasNullExclusionQType7();

	/**
	 * Returns a new object of class '<em>Has Null Exclusion QType8</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Null Exclusion QType8</em>'.
	 * @generated
	 */
	HasNullExclusionQType8 createHasNullExclusionQType8();

	/**
	 * Returns a new object of class '<em>Has Null Exclusion QType9</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Null Exclusion QType9</em>'.
	 * @generated
	 */
	HasNullExclusionQType9 createHasNullExclusionQType9();

	/**
	 * Returns a new object of class '<em>Has Null Exclusion QType10</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Null Exclusion QType10</em>'.
	 * @generated
	 */
	HasNullExclusionQType10 createHasNullExclusionQType10();

	/**
	 * Returns a new object of class '<em>Has Null Exclusion QType11</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Null Exclusion QType11</em>'.
	 * @generated
	 */
	HasNullExclusionQType11 createHasNullExclusionQType11();

	/**
	 * Returns a new object of class '<em>Has Null Exclusion QType12</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Null Exclusion QType12</em>'.
	 * @generated
	 */
	HasNullExclusionQType12 createHasNullExclusionQType12();

	/**
	 * Returns a new object of class '<em>Has Null Exclusion QType13</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Null Exclusion QType13</em>'.
	 * @generated
	 */
	HasNullExclusionQType13 createHasNullExclusionQType13();

	/**
	 * Returns a new object of class '<em>Has Null Exclusion QType14</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Null Exclusion QType14</em>'.
	 * @generated
	 */
	HasNullExclusionQType14 createHasNullExclusionQType14();

	/**
	 * Returns a new object of class '<em>Has Null Exclusion QType15</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Null Exclusion QType15</em>'.
	 * @generated
	 */
	HasNullExclusionQType15 createHasNullExclusionQType15();

	/**
	 * Returns a new object of class '<em>Has Null Exclusion QType16</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Null Exclusion QType16</em>'.
	 * @generated
	 */
	HasNullExclusionQType16 createHasNullExclusionQType16();

	/**
	 * Returns a new object of class '<em>Has Null Exclusion QType17</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Null Exclusion QType17</em>'.
	 * @generated
	 */
	HasNullExclusionQType17 createHasNullExclusionQType17();

	/**
	 * Returns a new object of class '<em>Has Null Exclusion QType18</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Null Exclusion QType18</em>'.
	 * @generated
	 */
	HasNullExclusionQType18 createHasNullExclusionQType18();

	/**
	 * Returns a new object of class '<em>Has Null Exclusion QType19</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Null Exclusion QType19</em>'.
	 * @generated
	 */
	HasNullExclusionQType19 createHasNullExclusionQType19();

	/**
	 * Returns a new object of class '<em>Has Null Exclusion QType20</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Null Exclusion QType20</em>'.
	 * @generated
	 */
	HasNullExclusionQType20 createHasNullExclusionQType20();

	/**
	 * Returns a new object of class '<em>Has Null Exclusion QType21</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Null Exclusion QType21</em>'.
	 * @generated
	 */
	HasNullExclusionQType21 createHasNullExclusionQType21();

	/**
	 * Returns a new object of class '<em>Has Null Exclusion QType22</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Null Exclusion QType22</em>'.
	 * @generated
	 */
	HasNullExclusionQType22 createHasNullExclusionQType22();

	/**
	 * Returns a new object of class '<em>Has Null Exclusion QType23</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Null Exclusion QType23</em>'.
	 * @generated
	 */
	HasNullExclusionQType23 createHasNullExclusionQType23();

	/**
	 * Returns a new object of class '<em>Has Null Exclusion QType24</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Null Exclusion QType24</em>'.
	 * @generated
	 */
	HasNullExclusionQType24 createHasNullExclusionQType24();

	/**
	 * Returns a new object of class '<em>Has Private QType</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Private QType</em>'.
	 * @generated
	 */
	HasPrivateQType createHasPrivateQType();

	/**
	 * Returns a new object of class '<em>Has Private QType1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Private QType1</em>'.
	 * @generated
	 */
	HasPrivateQType1 createHasPrivateQType1();

	/**
	 * Returns a new object of class '<em>Has Reverse QType</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Reverse QType</em>'.
	 * @generated
	 */
	HasReverseQType createHasReverseQType();

	/**
	 * Returns a new object of class '<em>Has Reverse QType1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Reverse QType1</em>'.
	 * @generated
	 */
	HasReverseQType1 createHasReverseQType1();

	/**
	 * Returns a new object of class '<em>Has Reverse QType2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Reverse QType2</em>'.
	 * @generated
	 */
	HasReverseQType2 createHasReverseQType2();

	/**
	 * Returns a new object of class '<em>Has Synchronized QType</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Synchronized QType</em>'.
	 * @generated
	 */
	HasSynchronizedQType createHasSynchronizedQType();

	/**
	 * Returns a new object of class '<em>Has Synchronized QType1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Synchronized QType1</em>'.
	 * @generated
	 */
	HasSynchronizedQType1 createHasSynchronizedQType1();

	/**
	 * Returns a new object of class '<em>Has Tagged QType</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Has Tagged QType</em>'.
	 * @generated
	 */
	HasTaggedQType createHasTaggedQType();

	/**
	 * Returns a new object of class '<em>Identifier</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Identifier</em>'.
	 * @generated
	 */
	Identifier createIdentifier();

	/**
	 * Returns a new object of class '<em>Identity Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Identity Attribute</em>'.
	 * @generated
	 */
	IdentityAttribute createIdentityAttribute();

	/**
	 * Returns a new object of class '<em>If Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>If Expression</em>'.
	 * @generated
	 */
	IfExpression createIfExpression();

	/**
	 * Returns a new object of class '<em>If Expression Path</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>If Expression Path</em>'.
	 * @generated
	 */
	IfExpressionPath createIfExpressionPath();

	/**
	 * Returns a new object of class '<em>If Path</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>If Path</em>'.
	 * @generated
	 */
	IfPath createIfPath();

	/**
	 * Returns a new object of class '<em>If Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>If Statement</em>'.
	 * @generated
	 */
	IfStatement createIfStatement();

	/**
	 * Returns a new object of class '<em>Image Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Image Attribute</em>'.
	 * @generated
	 */
	ImageAttribute createImageAttribute();

	/**
	 * Returns a new object of class '<em>Implementation Defined Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Implementation Defined Attribute</em>'.
	 * @generated
	 */
	ImplementationDefinedAttribute createImplementationDefinedAttribute();

	/**
	 * Returns a new object of class '<em>Implementation Defined Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Implementation Defined Pragma</em>'.
	 * @generated
	 */
	ImplementationDefinedPragma createImplementationDefinedPragma();

	/**
	 * Returns a new object of class '<em>Import Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Import Pragma</em>'.
	 * @generated
	 */
	ImportPragma createImportPragma();

	/**
	 * Returns a new object of class '<em>Incomplete Type Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Incomplete Type Declaration</em>'.
	 * @generated
	 */
	IncompleteTypeDeclaration createIncompleteTypeDeclaration();

	/**
	 * Returns a new object of class '<em>Independent Components Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Independent Components Pragma</em>'.
	 * @generated
	 */
	IndependentComponentsPragma createIndependentComponentsPragma();

	/**
	 * Returns a new object of class '<em>Independent Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Independent Pragma</em>'.
	 * @generated
	 */
	IndependentPragma createIndependentPragma();

	/**
	 * Returns a new object of class '<em>Index Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Index Constraint</em>'.
	 * @generated
	 */
	IndexConstraint createIndexConstraint();

	/**
	 * Returns a new object of class '<em>Indexed Component</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Indexed Component</em>'.
	 * @generated
	 */
	IndexedComponent createIndexedComponent();

	/**
	 * Returns a new object of class '<em>Inline Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Inline Pragma</em>'.
	 * @generated
	 */
	InlinePragma createInlinePragma();

	/**
	 * Returns a new object of class '<em>In Membership Test</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>In Membership Test</em>'.
	 * @generated
	 */
	InMembershipTest createInMembershipTest();

	/**
	 * Returns a new object of class '<em>Input Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Input Attribute</em>'.
	 * @generated
	 */
	InputAttribute createInputAttribute();

	/**
	 * Returns a new object of class '<em>Inspection Point Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Inspection Point Pragma</em>'.
	 * @generated
	 */
	InspectionPointPragma createInspectionPointPragma();

	/**
	 * Returns a new object of class '<em>Integer Literal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Integer Literal</em>'.
	 * @generated
	 */
	IntegerLiteral createIntegerLiteral();

	/**
	 * Returns a new object of class '<em>Integer Number Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Integer Number Declaration</em>'.
	 * @generated
	 */
	IntegerNumberDeclaration createIntegerNumberDeclaration();

	/**
	 * Returns a new object of class '<em>Interrupt Handler Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Interrupt Handler Pragma</em>'.
	 * @generated
	 */
	InterruptHandlerPragma createInterruptHandlerPragma();

	/**
	 * Returns a new object of class '<em>Interrupt Priority Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Interrupt Priority Pragma</em>'.
	 * @generated
	 */
	InterruptPriorityPragma createInterruptPriorityPragma();

	/**
	 * Returns a new object of class '<em>Is Not Null Return QType</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Not Null Return QType</em>'.
	 * @generated
	 */
	IsNotNullReturnQType createIsNotNullReturnQType();

	/**
	 * Returns a new object of class '<em>Is Not Null Return QType1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Not Null Return QType1</em>'.
	 * @generated
	 */
	IsNotNullReturnQType1 createIsNotNullReturnQType1();

	/**
	 * Returns a new object of class '<em>Is Not Null Return QType2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Not Null Return QType2</em>'.
	 * @generated
	 */
	IsNotNullReturnQType2 createIsNotNullReturnQType2();

	/**
	 * Returns a new object of class '<em>Is Not Null Return QType3</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Not Null Return QType3</em>'.
	 * @generated
	 */
	IsNotNullReturnQType3 createIsNotNullReturnQType3();

	/**
	 * Returns a new object of class '<em>Is Not Null Return QType4</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Not Null Return QType4</em>'.
	 * @generated
	 */
	IsNotNullReturnQType4 createIsNotNullReturnQType4();

	/**
	 * Returns a new object of class '<em>Is Not Null Return QType5</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Not Null Return QType5</em>'.
	 * @generated
	 */
	IsNotNullReturnQType5 createIsNotNullReturnQType5();

	/**
	 * Returns a new object of class '<em>Is Not Null Return QType6</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Not Null Return QType6</em>'.
	 * @generated
	 */
	IsNotNullReturnQType6 createIsNotNullReturnQType6();

	/**
	 * Returns a new object of class '<em>Is Not Null Return QType7</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Not Null Return QType7</em>'.
	 * @generated
	 */
	IsNotNullReturnQType7 createIsNotNullReturnQType7();

	/**
	 * Returns a new object of class '<em>Is Not Null Return QType8</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Not Null Return QType8</em>'.
	 * @generated
	 */
	IsNotNullReturnQType8 createIsNotNullReturnQType8();

	/**
	 * Returns a new object of class '<em>Is Not Null Return QType9</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Not Null Return QType9</em>'.
	 * @generated
	 */
	IsNotNullReturnQType9 createIsNotNullReturnQType9();

	/**
	 * Returns a new object of class '<em>Is Not Null Return QType10</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Not Null Return QType10</em>'.
	 * @generated
	 */
	IsNotNullReturnQType10 createIsNotNullReturnQType10();

	/**
	 * Returns a new object of class '<em>Is Not Null Return QType11</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Not Null Return QType11</em>'.
	 * @generated
	 */
	IsNotNullReturnQType11 createIsNotNullReturnQType11();

	/**
	 * Returns a new object of class '<em>Is Not Overriding Declaration QType</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Not Overriding Declaration QType</em>'.
	 * @generated
	 */
	IsNotOverridingDeclarationQType createIsNotOverridingDeclarationQType();

	/**
	 * Returns a new object of class '<em>Is Not Overriding Declaration QType1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Not Overriding Declaration QType1</em>'.
	 * @generated
	 */
	IsNotOverridingDeclarationQType1 createIsNotOverridingDeclarationQType1();

	/**
	 * Returns a new object of class '<em>Is Not Overriding Declaration QType2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Not Overriding Declaration QType2</em>'.
	 * @generated
	 */
	IsNotOverridingDeclarationQType2 createIsNotOverridingDeclarationQType2();

	/**
	 * Returns a new object of class '<em>Is Not Overriding Declaration QType3</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Not Overriding Declaration QType3</em>'.
	 * @generated
	 */
	IsNotOverridingDeclarationQType3 createIsNotOverridingDeclarationQType3();

	/**
	 * Returns a new object of class '<em>Is Not Overriding Declaration QType4</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Not Overriding Declaration QType4</em>'.
	 * @generated
	 */
	IsNotOverridingDeclarationQType4 createIsNotOverridingDeclarationQType4();

	/**
	 * Returns a new object of class '<em>Is Not Overriding Declaration QType5</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Not Overriding Declaration QType5</em>'.
	 * @generated
	 */
	IsNotOverridingDeclarationQType5 createIsNotOverridingDeclarationQType5();

	/**
	 * Returns a new object of class '<em>Is Not Overriding Declaration QType6</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Not Overriding Declaration QType6</em>'.
	 * @generated
	 */
	IsNotOverridingDeclarationQType6 createIsNotOverridingDeclarationQType6();

	/**
	 * Returns a new object of class '<em>Is Not Overriding Declaration QType7</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Not Overriding Declaration QType7</em>'.
	 * @generated
	 */
	IsNotOverridingDeclarationQType7 createIsNotOverridingDeclarationQType7();

	/**
	 * Returns a new object of class '<em>Is Not Overriding Declaration QType8</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Not Overriding Declaration QType8</em>'.
	 * @generated
	 */
	IsNotOverridingDeclarationQType8 createIsNotOverridingDeclarationQType8();

	/**
	 * Returns a new object of class '<em>Is Not Overriding Declaration QType9</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Not Overriding Declaration QType9</em>'.
	 * @generated
	 */
	IsNotOverridingDeclarationQType9 createIsNotOverridingDeclarationQType9();

	/**
	 * Returns a new object of class '<em>Is Not Overriding Declaration QType10</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Not Overriding Declaration QType10</em>'.
	 * @generated
	 */
	IsNotOverridingDeclarationQType10 createIsNotOverridingDeclarationQType10();

	/**
	 * Returns a new object of class '<em>Is Not Overriding Declaration QType11</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Not Overriding Declaration QType11</em>'.
	 * @generated
	 */
	IsNotOverridingDeclarationQType11 createIsNotOverridingDeclarationQType11();

	/**
	 * Returns a new object of class '<em>Is Overriding Declaration QType</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Overriding Declaration QType</em>'.
	 * @generated
	 */
	IsOverridingDeclarationQType createIsOverridingDeclarationQType();

	/**
	 * Returns a new object of class '<em>Is Overriding Declaration QType1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Overriding Declaration QType1</em>'.
	 * @generated
	 */
	IsOverridingDeclarationQType1 createIsOverridingDeclarationQType1();

	/**
	 * Returns a new object of class '<em>Is Overriding Declaration QType2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Overriding Declaration QType2</em>'.
	 * @generated
	 */
	IsOverridingDeclarationQType2 createIsOverridingDeclarationQType2();

	/**
	 * Returns a new object of class '<em>Is Overriding Declaration QType3</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Overriding Declaration QType3</em>'.
	 * @generated
	 */
	IsOverridingDeclarationQType3 createIsOverridingDeclarationQType3();

	/**
	 * Returns a new object of class '<em>Is Overriding Declaration QType4</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Overriding Declaration QType4</em>'.
	 * @generated
	 */
	IsOverridingDeclarationQType4 createIsOverridingDeclarationQType4();

	/**
	 * Returns a new object of class '<em>Is Overriding Declaration QType5</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Overriding Declaration QType5</em>'.
	 * @generated
	 */
	IsOverridingDeclarationQType5 createIsOverridingDeclarationQType5();

	/**
	 * Returns a new object of class '<em>Is Overriding Declaration QType6</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Overriding Declaration QType6</em>'.
	 * @generated
	 */
	IsOverridingDeclarationQType6 createIsOverridingDeclarationQType6();

	/**
	 * Returns a new object of class '<em>Is Overriding Declaration QType7</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Overriding Declaration QType7</em>'.
	 * @generated
	 */
	IsOverridingDeclarationQType7 createIsOverridingDeclarationQType7();

	/**
	 * Returns a new object of class '<em>Is Overriding Declaration QType8</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Overriding Declaration QType8</em>'.
	 * @generated
	 */
	IsOverridingDeclarationQType8 createIsOverridingDeclarationQType8();

	/**
	 * Returns a new object of class '<em>Is Overriding Declaration QType9</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Overriding Declaration QType9</em>'.
	 * @generated
	 */
	IsOverridingDeclarationQType9 createIsOverridingDeclarationQType9();

	/**
	 * Returns a new object of class '<em>Is Overriding Declaration QType10</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Overriding Declaration QType10</em>'.
	 * @generated
	 */
	IsOverridingDeclarationQType10 createIsOverridingDeclarationQType10();

	/**
	 * Returns a new object of class '<em>Is Overriding Declaration QType11</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Overriding Declaration QType11</em>'.
	 * @generated
	 */
	IsOverridingDeclarationQType11 createIsOverridingDeclarationQType11();

	/**
	 * Returns a new object of class '<em>Is Prefix Call</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Prefix Call</em>'.
	 * @generated
	 */
	IsPrefixCall createIsPrefixCall();

	/**
	 * Returns a new object of class '<em>Is Prefix Call QType</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Prefix Call QType</em>'.
	 * @generated
	 */
	IsPrefixCallQType createIsPrefixCallQType();

	/**
	 * Returns a new object of class '<em>Is Prefix Notation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Prefix Notation</em>'.
	 * @generated
	 */
	IsPrefixNotation createIsPrefixNotation();

	/**
	 * Returns a new object of class '<em>Is Prefix Notation QType</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Prefix Notation QType</em>'.
	 * @generated
	 */
	IsPrefixNotationQType createIsPrefixNotationQType();

	/**
	 * Returns a new object of class '<em>Is Prefix Notation QType1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Prefix Notation QType1</em>'.
	 * @generated
	 */
	IsPrefixNotationQType1 createIsPrefixNotationQType1();

	/**
	 * Returns a new object of class '<em>Known Discriminant Part</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Known Discriminant Part</em>'.
	 * @generated
	 */
	KnownDiscriminantPart createKnownDiscriminantPart();

	/**
	 * Returns a new object of class '<em>Last Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Last Attribute</em>'.
	 * @generated
	 */
	LastAttribute createLastAttribute();

	/**
	 * Returns a new object of class '<em>Last Bit Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Last Bit Attribute</em>'.
	 * @generated
	 */
	LastBitAttribute createLastBitAttribute();

	/**
	 * Returns a new object of class '<em>Leading Part Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Leading Part Attribute</em>'.
	 * @generated
	 */
	LeadingPartAttribute createLeadingPartAttribute();

	/**
	 * Returns a new object of class '<em>Length Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Length Attribute</em>'.
	 * @generated
	 */
	LengthAttribute createLengthAttribute();

	/**
	 * Returns a new object of class '<em>Less Than Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Less Than Operator</em>'.
	 * @generated
	 */
	LessThanOperator createLessThanOperator();

	/**
	 * Returns a new object of class '<em>Less Than Or Equal Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Less Than Or Equal Operator</em>'.
	 * @generated
	 */
	LessThanOrEqualOperator createLessThanOrEqualOperator();

	/**
	 * Returns a new object of class '<em>Limited</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Limited</em>'.
	 * @generated
	 */
	Limited createLimited();

	/**
	 * Returns a new object of class '<em>Limited Interface</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Limited Interface</em>'.
	 * @generated
	 */
	LimitedInterface createLimitedInterface();

	/**
	 * Returns a new object of class '<em>Linker Options Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Linker Options Pragma</em>'.
	 * @generated
	 */
	LinkerOptionsPragma createLinkerOptionsPragma();

	/**
	 * Returns a new object of class '<em>List Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>List Pragma</em>'.
	 * @generated
	 */
	ListPragma createListPragma();

	/**
	 * Returns a new object of class '<em>Locking Policy Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Locking Policy Pragma</em>'.
	 * @generated
	 */
	LockingPolicyPragma createLockingPolicyPragma();

	/**
	 * Returns a new object of class '<em>Loop Parameter Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Loop Parameter Specification</em>'.
	 * @generated
	 */
	LoopParameterSpecification createLoopParameterSpecification();

	/**
	 * Returns a new object of class '<em>Loop Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Loop Statement</em>'.
	 * @generated
	 */
	LoopStatement createLoopStatement();

	/**
	 * Returns a new object of class '<em>Machine Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Machine Attribute</em>'.
	 * @generated
	 */
	MachineAttribute createMachineAttribute();

	/**
	 * Returns a new object of class '<em>Machine Emax Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Machine Emax Attribute</em>'.
	 * @generated
	 */
	MachineEmaxAttribute createMachineEmaxAttribute();

	/**
	 * Returns a new object of class '<em>Machine Emin Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Machine Emin Attribute</em>'.
	 * @generated
	 */
	MachineEminAttribute createMachineEminAttribute();

	/**
	 * Returns a new object of class '<em>Machine Mantissa Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Machine Mantissa Attribute</em>'.
	 * @generated
	 */
	MachineMantissaAttribute createMachineMantissaAttribute();

	/**
	 * Returns a new object of class '<em>Machine Overflows Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Machine Overflows Attribute</em>'.
	 * @generated
	 */
	MachineOverflowsAttribute createMachineOverflowsAttribute();

	/**
	 * Returns a new object of class '<em>Machine Radix Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Machine Radix Attribute</em>'.
	 * @generated
	 */
	MachineRadixAttribute createMachineRadixAttribute();

	/**
	 * Returns a new object of class '<em>Machine Rounding Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Machine Rounding Attribute</em>'.
	 * @generated
	 */
	MachineRoundingAttribute createMachineRoundingAttribute();

	/**
	 * Returns a new object of class '<em>Machine Rounds Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Machine Rounds Attribute</em>'.
	 * @generated
	 */
	MachineRoundsAttribute createMachineRoundsAttribute();

	/**
	 * Returns a new object of class '<em>Max Alignment For Allocation Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Max Alignment For Allocation Attribute</em>'.
	 * @generated
	 */
	MaxAlignmentForAllocationAttribute createMaxAlignmentForAllocationAttribute();

	/**
	 * Returns a new object of class '<em>Max Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Max Attribute</em>'.
	 * @generated
	 */
	MaxAttribute createMaxAttribute();

	/**
	 * Returns a new object of class '<em>Max Size In Storage Elements Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Max Size In Storage Elements Attribute</em>'.
	 * @generated
	 */
	MaxSizeInStorageElementsAttribute createMaxSizeInStorageElementsAttribute();

	/**
	 * Returns a new object of class '<em>Min Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Min Attribute</em>'.
	 * @generated
	 */
	MinAttribute createMinAttribute();

	/**
	 * Returns a new object of class '<em>Minus Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Minus Operator</em>'.
	 * @generated
	 */
	MinusOperator createMinusOperator();

	/**
	 * Returns a new object of class '<em>Mod Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Mod Attribute</em>'.
	 * @generated
	 */
	ModAttribute createModAttribute();

	/**
	 * Returns a new object of class '<em>Model Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Model Attribute</em>'.
	 * @generated
	 */
	ModelAttribute createModelAttribute();

	/**
	 * Returns a new object of class '<em>Model Emin Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Model Emin Attribute</em>'.
	 * @generated
	 */
	ModelEminAttribute createModelEminAttribute();

	/**
	 * Returns a new object of class '<em>Model Epsilon Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Model Epsilon Attribute</em>'.
	 * @generated
	 */
	ModelEpsilonAttribute createModelEpsilonAttribute();

	/**
	 * Returns a new object of class '<em>Model Mantissa Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Model Mantissa Attribute</em>'.
	 * @generated
	 */
	ModelMantissaAttribute createModelMantissaAttribute();

	/**
	 * Returns a new object of class '<em>Model Small Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Model Small Attribute</em>'.
	 * @generated
	 */
	ModelSmallAttribute createModelSmallAttribute();

	/**
	 * Returns a new object of class '<em>Mod Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Mod Operator</em>'.
	 * @generated
	 */
	ModOperator createModOperator();

	/**
	 * Returns a new object of class '<em>Modular Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Modular Type Definition</em>'.
	 * @generated
	 */
	ModularTypeDefinition createModularTypeDefinition();

	/**
	 * Returns a new object of class '<em>Modulus Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Modulus Attribute</em>'.
	 * @generated
	 */
	ModulusAttribute createModulusAttribute();

	/**
	 * Returns a new object of class '<em>Multiply Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Multiply Operator</em>'.
	 * @generated
	 */
	MultiplyOperator createMultiplyOperator();

	/**
	 * Returns a new object of class '<em>Name Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Name Class</em>'.
	 * @generated
	 */
	NameClass createNameClass();

	/**
	 * Returns a new object of class '<em>Named Array Aggregate</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Named Array Aggregate</em>'.
	 * @generated
	 */
	NamedArrayAggregate createNamedArrayAggregate();

	/**
	 * Returns a new object of class '<em>Name List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Name List</em>'.
	 * @generated
	 */
	NameList createNameList();

	/**
	 * Returns a new object of class '<em>No Return Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>No Return Pragma</em>'.
	 * @generated
	 */
	NoReturnPragma createNoReturnPragma();

	/**
	 * Returns a new object of class '<em>Normalize Scalars Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Normalize Scalars Pragma</em>'.
	 * @generated
	 */
	NormalizeScalarsPragma createNormalizeScalarsPragma();

	/**
	 * Returns a new object of class '<em>Not An Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Not An Element</em>'.
	 * @generated
	 */
	NotAnElement createNotAnElement();

	/**
	 * Returns a new object of class '<em>Not Equal Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Not Equal Operator</em>'.
	 * @generated
	 */
	NotEqualOperator createNotEqualOperator();

	/**
	 * Returns a new object of class '<em>Not In Membership Test</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Not In Membership Test</em>'.
	 * @generated
	 */
	NotInMembershipTest createNotInMembershipTest();

	/**
	 * Returns a new object of class '<em>Not Null Return</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Not Null Return</em>'.
	 * @generated
	 */
	NotNullReturn createNotNullReturn();

	/**
	 * Returns a new object of class '<em>Not Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Not Operator</em>'.
	 * @generated
	 */
	NotOperator createNotOperator();

	/**
	 * Returns a new object of class '<em>Not Overriding</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Not Overriding</em>'.
	 * @generated
	 */
	NotOverriding createNotOverriding();

	/**
	 * Returns a new object of class '<em>Null Component</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Null Component</em>'.
	 * @generated
	 */
	NullComponent createNullComponent();

	/**
	 * Returns a new object of class '<em>Null Exclusion</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Null Exclusion</em>'.
	 * @generated
	 */
	NullExclusion createNullExclusion();

	/**
	 * Returns a new object of class '<em>Null Literal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Null Literal</em>'.
	 * @generated
	 */
	NullLiteral createNullLiteral();

	/**
	 * Returns a new object of class '<em>Null Procedure Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Null Procedure Declaration</em>'.
	 * @generated
	 */
	NullProcedureDeclaration createNullProcedureDeclaration();

	/**
	 * Returns a new object of class '<em>Null Record Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Null Record Definition</em>'.
	 * @generated
	 */
	NullRecordDefinition createNullRecordDefinition();

	/**
	 * Returns a new object of class '<em>Null Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Null Statement</em>'.
	 * @generated
	 */
	NullStatement createNullStatement();

	/**
	 * Returns a new object of class '<em>Object Renaming Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Object Renaming Declaration</em>'.
	 * @generated
	 */
	ObjectRenamingDeclaration createObjectRenamingDeclaration();

	/**
	 * Returns a new object of class '<em>Optimize Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Optimize Pragma</em>'.
	 * @generated
	 */
	OptimizePragma createOptimizePragma();

	/**
	 * Returns a new object of class '<em>Ordinary Fixed Point Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ordinary Fixed Point Definition</em>'.
	 * @generated
	 */
	OrdinaryFixedPointDefinition createOrdinaryFixedPointDefinition();

	/**
	 * Returns a new object of class '<em>Ordinary Interface</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ordinary Interface</em>'.
	 * @generated
	 */
	OrdinaryInterface createOrdinaryInterface();

	/**
	 * Returns a new object of class '<em>Ordinary Type Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ordinary Type Declaration</em>'.
	 * @generated
	 */
	OrdinaryTypeDeclaration createOrdinaryTypeDeclaration();

	/**
	 * Returns a new object of class '<em>Or Else Short Circuit</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Or Else Short Circuit</em>'.
	 * @generated
	 */
	OrElseShortCircuit createOrElseShortCircuit();

	/**
	 * Returns a new object of class '<em>Or Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Or Operator</em>'.
	 * @generated
	 */
	OrOperator createOrOperator();

	/**
	 * Returns a new object of class '<em>Or Path</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Or Path</em>'.
	 * @generated
	 */
	OrPath createOrPath();

	/**
	 * Returns a new object of class '<em>Others Choice</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Others Choice</em>'.
	 * @generated
	 */
	OthersChoice createOthersChoice();

	/**
	 * Returns a new object of class '<em>Output Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Output Attribute</em>'.
	 * @generated
	 */
	OutputAttribute createOutputAttribute();

	/**
	 * Returns a new object of class '<em>Overlaps Storage Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Overlaps Storage Attribute</em>'.
	 * @generated
	 */
	OverlapsStorageAttribute createOverlapsStorageAttribute();

	/**
	 * Returns a new object of class '<em>Overriding</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Overriding</em>'.
	 * @generated
	 */
	Overriding createOverriding();

	/**
	 * Returns a new object of class '<em>Package Body Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Package Body Declaration</em>'.
	 * @generated
	 */
	PackageBodyDeclaration createPackageBodyDeclaration();

	/**
	 * Returns a new object of class '<em>Package Body Stub</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Package Body Stub</em>'.
	 * @generated
	 */
	PackageBodyStub createPackageBodyStub();

	/**
	 * Returns a new object of class '<em>Package Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Package Declaration</em>'.
	 * @generated
	 */
	PackageDeclaration createPackageDeclaration();

	/**
	 * Returns a new object of class '<em>Package Instantiation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Package Instantiation</em>'.
	 * @generated
	 */
	PackageInstantiation createPackageInstantiation();

	/**
	 * Returns a new object of class '<em>Package Renaming Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Package Renaming Declaration</em>'.
	 * @generated
	 */
	PackageRenamingDeclaration createPackageRenamingDeclaration();

	/**
	 * Returns a new object of class '<em>Pack Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pack Pragma</em>'.
	 * @generated
	 */
	PackPragma createPackPragma();

	/**
	 * Returns a new object of class '<em>Page Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Page Pragma</em>'.
	 * @generated
	 */
	PagePragma createPagePragma();

	/**
	 * Returns a new object of class '<em>Parameter Association</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parameter Association</em>'.
	 * @generated
	 */
	ParameterAssociation createParameterAssociation();

	/**
	 * Returns a new object of class '<em>Parameter Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parameter Specification</em>'.
	 * @generated
	 */
	ParameterSpecification createParameterSpecification();

	/**
	 * Returns a new object of class '<em>Parameter Specification List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parameter Specification List</em>'.
	 * @generated
	 */
	ParameterSpecificationList createParameterSpecificationList();

	/**
	 * Returns a new object of class '<em>Parenthesized Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parenthesized Expression</em>'.
	 * @generated
	 */
	ParenthesizedExpression createParenthesizedExpression();

	/**
	 * Returns a new object of class '<em>Partition Elaboration Policy Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Partition Elaboration Policy Pragma</em>'.
	 * @generated
	 */
	PartitionElaborationPolicyPragma createPartitionElaborationPolicyPragma();

	/**
	 * Returns a new object of class '<em>Partition Id Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Partition Id Attribute</em>'.
	 * @generated
	 */
	PartitionIdAttribute createPartitionIdAttribute();

	/**
	 * Returns a new object of class '<em>Path Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Path Class</em>'.
	 * @generated
	 */
	PathClass createPathClass();

	/**
	 * Returns a new object of class '<em>Path List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Path List</em>'.
	 * @generated
	 */
	PathList createPathList();

	/**
	 * Returns a new object of class '<em>Plus Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Plus Operator</em>'.
	 * @generated
	 */
	PlusOperator createPlusOperator();

	/**
	 * Returns a new object of class '<em>Pool Specific Access To Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pool Specific Access To Variable</em>'.
	 * @generated
	 */
	PoolSpecificAccessToVariable createPoolSpecificAccessToVariable();

	/**
	 * Returns a new object of class '<em>Pos Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pos Attribute</em>'.
	 * @generated
	 */
	PosAttribute createPosAttribute();

	/**
	 * Returns a new object of class '<em>Positional Array Aggregate</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Positional Array Aggregate</em>'.
	 * @generated
	 */
	PositionalArrayAggregate createPositionalArrayAggregate();

	/**
	 * Returns a new object of class '<em>Position Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Position Attribute</em>'.
	 * @generated
	 */
	PositionAttribute createPositionAttribute();

	/**
	 * Returns a new object of class '<em>Pragma Argument Association</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pragma Argument Association</em>'.
	 * @generated
	 */
	PragmaArgumentAssociation createPragmaArgumentAssociation();

	/**
	 * Returns a new object of class '<em>Pragma Element Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pragma Element Class</em>'.
	 * @generated
	 */
	PragmaElementClass createPragmaElementClass();

	/**
	 * Returns a new object of class '<em>Pred Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pred Attribute</em>'.
	 * @generated
	 */
	PredAttribute createPredAttribute();

	/**
	 * Returns a new object of class '<em>Preelaborable Initialization Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Preelaborable Initialization Pragma</em>'.
	 * @generated
	 */
	PreelaborableInitializationPragma createPreelaborableInitializationPragma();

	/**
	 * Returns a new object of class '<em>Preelaborate Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Preelaborate Pragma</em>'.
	 * @generated
	 */
	PreelaboratePragma createPreelaboratePragma();

	/**
	 * Returns a new object of class '<em>Priority Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Priority Attribute</em>'.
	 * @generated
	 */
	PriorityAttribute createPriorityAttribute();

	/**
	 * Returns a new object of class '<em>Priority Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Priority Pragma</em>'.
	 * @generated
	 */
	PriorityPragma createPriorityPragma();

	/**
	 * Returns a new object of class '<em>Priority Specific Dispatching Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Priority Specific Dispatching Pragma</em>'.
	 * @generated
	 */
	PrioritySpecificDispatchingPragma createPrioritySpecificDispatchingPragma();

	/**
	 * Returns a new object of class '<em>Private</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Private</em>'.
	 * @generated
	 */
	Private createPrivate();

	/**
	 * Returns a new object of class '<em>Private Extension Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Private Extension Declaration</em>'.
	 * @generated
	 */
	PrivateExtensionDeclaration createPrivateExtensionDeclaration();

	/**
	 * Returns a new object of class '<em>Private Extension Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Private Extension Definition</em>'.
	 * @generated
	 */
	PrivateExtensionDefinition createPrivateExtensionDefinition();

	/**
	 * Returns a new object of class '<em>Private Type Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Private Type Declaration</em>'.
	 * @generated
	 */
	PrivateTypeDeclaration createPrivateTypeDeclaration();

	/**
	 * Returns a new object of class '<em>Private Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Private Type Definition</em>'.
	 * @generated
	 */
	PrivateTypeDefinition createPrivateTypeDefinition();

	/**
	 * Returns a new object of class '<em>Procedure Body Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Procedure Body Declaration</em>'.
	 * @generated
	 */
	ProcedureBodyDeclaration createProcedureBodyDeclaration();

	/**
	 * Returns a new object of class '<em>Procedure Body Stub</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Procedure Body Stub</em>'.
	 * @generated
	 */
	ProcedureBodyStub createProcedureBodyStub();

	/**
	 * Returns a new object of class '<em>Procedure Call Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Procedure Call Statement</em>'.
	 * @generated
	 */
	ProcedureCallStatement createProcedureCallStatement();

	/**
	 * Returns a new object of class '<em>Procedure Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Procedure Declaration</em>'.
	 * @generated
	 */
	ProcedureDeclaration createProcedureDeclaration();

	/**
	 * Returns a new object of class '<em>Procedure Instantiation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Procedure Instantiation</em>'.
	 * @generated
	 */
	ProcedureInstantiation createProcedureInstantiation();

	/**
	 * Returns a new object of class '<em>Procedure Renaming Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Procedure Renaming Declaration</em>'.
	 * @generated
	 */
	ProcedureRenamingDeclaration createProcedureRenamingDeclaration();

	/**
	 * Returns a new object of class '<em>Profile Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Profile Pragma</em>'.
	 * @generated
	 */
	ProfilePragma createProfilePragma();

	/**
	 * Returns a new object of class '<em>Protected Body Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Protected Body Declaration</em>'.
	 * @generated
	 */
	ProtectedBodyDeclaration createProtectedBodyDeclaration();

	/**
	 * Returns a new object of class '<em>Protected Body Stub</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Protected Body Stub</em>'.
	 * @generated
	 */
	ProtectedBodyStub createProtectedBodyStub();

	/**
	 * Returns a new object of class '<em>Protected Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Protected Definition</em>'.
	 * @generated
	 */
	ProtectedDefinition createProtectedDefinition();

	/**
	 * Returns a new object of class '<em>Protected Interface</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Protected Interface</em>'.
	 * @generated
	 */
	ProtectedInterface createProtectedInterface();

	/**
	 * Returns a new object of class '<em>Protected Type Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Protected Type Declaration</em>'.
	 * @generated
	 */
	ProtectedTypeDeclaration createProtectedTypeDeclaration();

	/**
	 * Returns a new object of class '<em>Pure Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pure Pragma</em>'.
	 * @generated
	 */
	PurePragma createPurePragma();

	/**
	 * Returns a new object of class '<em>Qualified Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Qualified Expression</em>'.
	 * @generated
	 */
	QualifiedExpression createQualifiedExpression();

	/**
	 * Returns a new object of class '<em>Queuing Policy Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Queuing Policy Pragma</em>'.
	 * @generated
	 */
	QueuingPolicyPragma createQueuingPolicyPragma();

	/**
	 * Returns a new object of class '<em>Raise Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Raise Expression</em>'.
	 * @generated
	 */
	RaiseExpression createRaiseExpression();

	/**
	 * Returns a new object of class '<em>Raise Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Raise Statement</em>'.
	 * @generated
	 */
	RaiseStatement createRaiseStatement();

	/**
	 * Returns a new object of class '<em>Range Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Range Attribute</em>'.
	 * @generated
	 */
	RangeAttribute createRangeAttribute();

	/**
	 * Returns a new object of class '<em>Range Attribute Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Range Attribute Reference</em>'.
	 * @generated
	 */
	RangeAttributeReference createRangeAttributeReference();

	/**
	 * Returns a new object of class '<em>Range Constraint Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Range Constraint Class</em>'.
	 * @generated
	 */
	RangeConstraintClass createRangeConstraintClass();

	/**
	 * Returns a new object of class '<em>Read Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Read Attribute</em>'.
	 * @generated
	 */
	ReadAttribute createReadAttribute();

	/**
	 * Returns a new object of class '<em>Real Literal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Real Literal</em>'.
	 * @generated
	 */
	RealLiteral createRealLiteral();

	/**
	 * Returns a new object of class '<em>Real Number Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Real Number Declaration</em>'.
	 * @generated
	 */
	RealNumberDeclaration createRealNumberDeclaration();

	/**
	 * Returns a new object of class '<em>Record Aggregate</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Record Aggregate</em>'.
	 * @generated
	 */
	RecordAggregate createRecordAggregate();

	/**
	 * Returns a new object of class '<em>Record Component Association</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Record Component Association</em>'.
	 * @generated
	 */
	RecordComponentAssociation createRecordComponentAssociation();

	/**
	 * Returns a new object of class '<em>Record Component Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Record Component Class</em>'.
	 * @generated
	 */
	RecordComponentClass createRecordComponentClass();

	/**
	 * Returns a new object of class '<em>Record Component List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Record Component List</em>'.
	 * @generated
	 */
	RecordComponentList createRecordComponentList();

	/**
	 * Returns a new object of class '<em>Record Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Record Definition</em>'.
	 * @generated
	 */
	RecordDefinition createRecordDefinition();

	/**
	 * Returns a new object of class '<em>Record Representation Clause</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Record Representation Clause</em>'.
	 * @generated
	 */
	RecordRepresentationClause createRecordRepresentationClause();

	/**
	 * Returns a new object of class '<em>Record Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Record Type Definition</em>'.
	 * @generated
	 */
	RecordTypeDefinition createRecordTypeDefinition();

	/**
	 * Returns a new object of class '<em>Relative Deadline Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Relative Deadline Pragma</em>'.
	 * @generated
	 */
	RelativeDeadlinePragma createRelativeDeadlinePragma();

	/**
	 * Returns a new object of class '<em>Remainder Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Remainder Attribute</em>'.
	 * @generated
	 */
	RemainderAttribute createRemainderAttribute();

	/**
	 * Returns a new object of class '<em>Rem Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rem Operator</em>'.
	 * @generated
	 */
	RemOperator createRemOperator();

	/**
	 * Returns a new object of class '<em>Remote Call Interface Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Remote Call Interface Pragma</em>'.
	 * @generated
	 */
	RemoteCallInterfacePragma createRemoteCallInterfacePragma();

	/**
	 * Returns a new object of class '<em>Remote Types Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Remote Types Pragma</em>'.
	 * @generated
	 */
	RemoteTypesPragma createRemoteTypesPragma();

	/**
	 * Returns a new object of class '<em>Requeue Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Requeue Statement</em>'.
	 * @generated
	 */
	RequeueStatement createRequeueStatement();

	/**
	 * Returns a new object of class '<em>Requeue Statement With Abort</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Requeue Statement With Abort</em>'.
	 * @generated
	 */
	RequeueStatementWithAbort createRequeueStatementWithAbort();

	/**
	 * Returns a new object of class '<em>Restrictions Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Restrictions Pragma</em>'.
	 * @generated
	 */
	RestrictionsPragma createRestrictionsPragma();

	/**
	 * Returns a new object of class '<em>Return Constant Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Return Constant Specification</em>'.
	 * @generated
	 */
	ReturnConstantSpecification createReturnConstantSpecification();

	/**
	 * Returns a new object of class '<em>Return Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Return Statement</em>'.
	 * @generated
	 */
	ReturnStatement createReturnStatement();

	/**
	 * Returns a new object of class '<em>Return Variable Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Return Variable Specification</em>'.
	 * @generated
	 */
	ReturnVariableSpecification createReturnVariableSpecification();

	/**
	 * Returns a new object of class '<em>Reverse</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reverse</em>'.
	 * @generated
	 */
	Reverse createReverse();

	/**
	 * Returns a new object of class '<em>Reviewable Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reviewable Pragma</em>'.
	 * @generated
	 */
	ReviewablePragma createReviewablePragma();

	/**
	 * Returns a new object of class '<em>Root Integer Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Root Integer Definition</em>'.
	 * @generated
	 */
	RootIntegerDefinition createRootIntegerDefinition();

	/**
	 * Returns a new object of class '<em>Root Real Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Root Real Definition</em>'.
	 * @generated
	 */
	RootRealDefinition createRootRealDefinition();

	/**
	 * Returns a new object of class '<em>Round Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Round Attribute</em>'.
	 * @generated
	 */
	RoundAttribute createRoundAttribute();

	/**
	 * Returns a new object of class '<em>Rounding Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rounding Attribute</em>'.
	 * @generated
	 */
	RoundingAttribute createRoundingAttribute();

	/**
	 * Returns a new object of class '<em>Safe First Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Safe First Attribute</em>'.
	 * @generated
	 */
	SafeFirstAttribute createSafeFirstAttribute();

	/**
	 * Returns a new object of class '<em>Safe Last Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Safe Last Attribute</em>'.
	 * @generated
	 */
	SafeLastAttribute createSafeLastAttribute();

	/**
	 * Returns a new object of class '<em>Scale Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Scale Attribute</em>'.
	 * @generated
	 */
	ScaleAttribute createScaleAttribute();

	/**
	 * Returns a new object of class '<em>Scaling Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Scaling Attribute</em>'.
	 * @generated
	 */
	ScalingAttribute createScalingAttribute();

	/**
	 * Returns a new object of class '<em>Selected Component</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Selected Component</em>'.
	 * @generated
	 */
	SelectedComponent createSelectedComponent();

	/**
	 * Returns a new object of class '<em>Selective Accept Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Selective Accept Statement</em>'.
	 * @generated
	 */
	SelectiveAcceptStatement createSelectiveAcceptStatement();

	/**
	 * Returns a new object of class '<em>Select Path</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Select Path</em>'.
	 * @generated
	 */
	SelectPath createSelectPath();

	/**
	 * Returns a new object of class '<em>Shared Passive Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Shared Passive Pragma</em>'.
	 * @generated
	 */
	SharedPassivePragma createSharedPassivePragma();

	/**
	 * Returns a new object of class '<em>Signed Integer Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Signed Integer Type Definition</em>'.
	 * @generated
	 */
	SignedIntegerTypeDefinition createSignedIntegerTypeDefinition();

	/**
	 * Returns a new object of class '<em>Signed Zeros Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Signed Zeros Attribute</em>'.
	 * @generated
	 */
	SignedZerosAttribute createSignedZerosAttribute();

	/**
	 * Returns a new object of class '<em>Simple Expression Range</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simple Expression Range</em>'.
	 * @generated
	 */
	SimpleExpressionRange createSimpleExpressionRange();

	/**
	 * Returns a new object of class '<em>Single Protected Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Single Protected Declaration</em>'.
	 * @generated
	 */
	SingleProtectedDeclaration createSingleProtectedDeclaration();

	/**
	 * Returns a new object of class '<em>Single Task Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Single Task Declaration</em>'.
	 * @generated
	 */
	SingleTaskDeclaration createSingleTaskDeclaration();

	/**
	 * Returns a new object of class '<em>Size Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Size Attribute</em>'.
	 * @generated
	 */
	SizeAttribute createSizeAttribute();

	/**
	 * Returns a new object of class '<em>Slice</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Slice</em>'.
	 * @generated
	 */
	Slice createSlice();

	/**
	 * Returns a new object of class '<em>Small Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Small Attribute</em>'.
	 * @generated
	 */
	SmallAttribute createSmallAttribute();

	/**
	 * Returns a new object of class '<em>Source Location</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Source Location</em>'.
	 * @generated
	 */
	SourceLocation createSourceLocation();

	/**
	 * Returns a new object of class '<em>Statement Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Statement Class</em>'.
	 * @generated
	 */
	StatementClass createStatementClass();

	/**
	 * Returns a new object of class '<em>Statement List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Statement List</em>'.
	 * @generated
	 */
	StatementList createStatementList();

	/**
	 * Returns a new object of class '<em>Storage Pool Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Storage Pool Attribute</em>'.
	 * @generated
	 */
	StoragePoolAttribute createStoragePoolAttribute();

	/**
	 * Returns a new object of class '<em>Storage Size Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Storage Size Attribute</em>'.
	 * @generated
	 */
	StorageSizeAttribute createStorageSizeAttribute();

	/**
	 * Returns a new object of class '<em>Storage Size Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Storage Size Pragma</em>'.
	 * @generated
	 */
	StorageSizePragma createStorageSizePragma();

	/**
	 * Returns a new object of class '<em>Stream Size Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Stream Size Attribute</em>'.
	 * @generated
	 */
	StreamSizeAttribute createStreamSizeAttribute();

	/**
	 * Returns a new object of class '<em>String Literal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>String Literal</em>'.
	 * @generated
	 */
	StringLiteral createStringLiteral();

	/**
	 * Returns a new object of class '<em>Subtype Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Subtype Declaration</em>'.
	 * @generated
	 */
	SubtypeDeclaration createSubtypeDeclaration();

	/**
	 * Returns a new object of class '<em>Subtype Indication</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Subtype Indication</em>'.
	 * @generated
	 */
	SubtypeIndication createSubtypeIndication();

	/**
	 * Returns a new object of class '<em>Succ Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Succ Attribute</em>'.
	 * @generated
	 */
	SuccAttribute createSuccAttribute();

	/**
	 * Returns a new object of class '<em>Suppress Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Suppress Pragma</em>'.
	 * @generated
	 */
	SuppressPragma createSuppressPragma();

	/**
	 * Returns a new object of class '<em>Synchronized</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Synchronized</em>'.
	 * @generated
	 */
	Synchronized createSynchronized();

	/**
	 * Returns a new object of class '<em>Synchronized Interface</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Synchronized Interface</em>'.
	 * @generated
	 */
	SynchronizedInterface createSynchronizedInterface();

	/**
	 * Returns a new object of class '<em>Tag Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tag Attribute</em>'.
	 * @generated
	 */
	TagAttribute createTagAttribute();

	/**
	 * Returns a new object of class '<em>Tagged</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tagged</em>'.
	 * @generated
	 */
	Tagged createTagged();

	/**
	 * Returns a new object of class '<em>Tagged Incomplete Type Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tagged Incomplete Type Declaration</em>'.
	 * @generated
	 */
	TaggedIncompleteTypeDeclaration createTaggedIncompleteTypeDeclaration();

	/**
	 * Returns a new object of class '<em>Tagged Private Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tagged Private Type Definition</em>'.
	 * @generated
	 */
	TaggedPrivateTypeDefinition createTaggedPrivateTypeDefinition();

	/**
	 * Returns a new object of class '<em>Tagged Record Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tagged Record Type Definition</em>'.
	 * @generated
	 */
	TaggedRecordTypeDefinition createTaggedRecordTypeDefinition();

	/**
	 * Returns a new object of class '<em>Task Body Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Task Body Declaration</em>'.
	 * @generated
	 */
	TaskBodyDeclaration createTaskBodyDeclaration();

	/**
	 * Returns a new object of class '<em>Task Body Stub</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Task Body Stub</em>'.
	 * @generated
	 */
	TaskBodyStub createTaskBodyStub();

	/**
	 * Returns a new object of class '<em>Task Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Task Definition</em>'.
	 * @generated
	 */
	TaskDefinition createTaskDefinition();

	/**
	 * Returns a new object of class '<em>Task Dispatching Policy Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Task Dispatching Policy Pragma</em>'.
	 * @generated
	 */
	TaskDispatchingPolicyPragma createTaskDispatchingPolicyPragma();

	/**
	 * Returns a new object of class '<em>Task Interface</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Task Interface</em>'.
	 * @generated
	 */
	TaskInterface createTaskInterface();

	/**
	 * Returns a new object of class '<em>Task Type Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Task Type Declaration</em>'.
	 * @generated
	 */
	TaskTypeDeclaration createTaskTypeDeclaration();

	/**
	 * Returns a new object of class '<em>Terminate Alternative Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Terminate Alternative Statement</em>'.
	 * @generated
	 */
	TerminateAlternativeStatement createTerminateAlternativeStatement();

	/**
	 * Returns a new object of class '<em>Terminated Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Terminated Attribute</em>'.
	 * @generated
	 */
	TerminatedAttribute createTerminatedAttribute();

	/**
	 * Returns a new object of class '<em>Then Abort Path</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Then Abort Path</em>'.
	 * @generated
	 */
	ThenAbortPath createThenAbortPath();

	/**
	 * Returns a new object of class '<em>Timed Entry Call Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Timed Entry Call Statement</em>'.
	 * @generated
	 */
	TimedEntryCallStatement createTimedEntryCallStatement();

	/**
	 * Returns a new object of class '<em>Truncation Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Truncation Attribute</em>'.
	 * @generated
	 */
	TruncationAttribute createTruncationAttribute();

	/**
	 * Returns a new object of class '<em>Type Conversion</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Type Conversion</em>'.
	 * @generated
	 */
	TypeConversion createTypeConversion();

	/**
	 * Returns a new object of class '<em>Unary Minus Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unary Minus Operator</em>'.
	 * @generated
	 */
	UnaryMinusOperator createUnaryMinusOperator();

	/**
	 * Returns a new object of class '<em>Unary Plus Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unary Plus Operator</em>'.
	 * @generated
	 */
	UnaryPlusOperator createUnaryPlusOperator();

	/**
	 * Returns a new object of class '<em>Unbiased Rounding Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unbiased Rounding Attribute</em>'.
	 * @generated
	 */
	UnbiasedRoundingAttribute createUnbiasedRoundingAttribute();

	/**
	 * Returns a new object of class '<em>Unchecked Access Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unchecked Access Attribute</em>'.
	 * @generated
	 */
	UncheckedAccessAttribute createUncheckedAccessAttribute();

	/**
	 * Returns a new object of class '<em>Unchecked Union Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unchecked Union Pragma</em>'.
	 * @generated
	 */
	UncheckedUnionPragma createUncheckedUnionPragma();

	/**
	 * Returns a new object of class '<em>Unconstrained Array Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unconstrained Array Definition</em>'.
	 * @generated
	 */
	UnconstrainedArrayDefinition createUnconstrainedArrayDefinition();

	/**
	 * Returns a new object of class '<em>Universal Fixed Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Universal Fixed Definition</em>'.
	 * @generated
	 */
	UniversalFixedDefinition createUniversalFixedDefinition();

	/**
	 * Returns a new object of class '<em>Universal Integer Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Universal Integer Definition</em>'.
	 * @generated
	 */
	UniversalIntegerDefinition createUniversalIntegerDefinition();

	/**
	 * Returns a new object of class '<em>Universal Real Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Universal Real Definition</em>'.
	 * @generated
	 */
	UniversalRealDefinition createUniversalRealDefinition();

	/**
	 * Returns a new object of class '<em>Unknown Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unknown Attribute</em>'.
	 * @generated
	 */
	UnknownAttribute createUnknownAttribute();

	/**
	 * Returns a new object of class '<em>Unknown Discriminant Part</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unknown Discriminant Part</em>'.
	 * @generated
	 */
	UnknownDiscriminantPart createUnknownDiscriminantPart();

	/**
	 * Returns a new object of class '<em>Unknown Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unknown Pragma</em>'.
	 * @generated
	 */
	UnknownPragma createUnknownPragma();

	/**
	 * Returns a new object of class '<em>Unsuppress Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unsuppress Pragma</em>'.
	 * @generated
	 */
	UnsuppressPragma createUnsuppressPragma();

	/**
	 * Returns a new object of class '<em>Use All Type Clause</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Use All Type Clause</em>'.
	 * @generated
	 */
	UseAllTypeClause createUseAllTypeClause();

	/**
	 * Returns a new object of class '<em>Use Package Clause</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Use Package Clause</em>'.
	 * @generated
	 */
	UsePackageClause createUsePackageClause();

	/**
	 * Returns a new object of class '<em>Use Type Clause</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Use Type Clause</em>'.
	 * @generated
	 */
	UseTypeClause createUseTypeClause();

	/**
	 * Returns a new object of class '<em>Val Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Val Attribute</em>'.
	 * @generated
	 */
	ValAttribute createValAttribute();

	/**
	 * Returns a new object of class '<em>Valid Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Valid Attribute</em>'.
	 * @generated
	 */
	ValidAttribute createValidAttribute();

	/**
	 * Returns a new object of class '<em>Value Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Value Attribute</em>'.
	 * @generated
	 */
	ValueAttribute createValueAttribute();

	/**
	 * Returns a new object of class '<em>Variable Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Variable Declaration</em>'.
	 * @generated
	 */
	VariableDeclaration createVariableDeclaration();

	/**
	 * Returns a new object of class '<em>Variant</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Variant</em>'.
	 * @generated
	 */
	Variant createVariant();

	/**
	 * Returns a new object of class '<em>Variant List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Variant List</em>'.
	 * @generated
	 */
	VariantList createVariantList();

	/**
	 * Returns a new object of class '<em>Variant Part</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Variant Part</em>'.
	 * @generated
	 */
	VariantPart createVariantPart();

	/**
	 * Returns a new object of class '<em>Version Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Version Attribute</em>'.
	 * @generated
	 */
	VersionAttribute createVersionAttribute();

	/**
	 * Returns a new object of class '<em>Volatile Components Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Volatile Components Pragma</em>'.
	 * @generated
	 */
	VolatileComponentsPragma createVolatileComponentsPragma();

	/**
	 * Returns a new object of class '<em>Volatile Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Volatile Pragma</em>'.
	 * @generated
	 */
	VolatilePragma createVolatilePragma();

	/**
	 * Returns a new object of class '<em>While Loop Statement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>While Loop Statement</em>'.
	 * @generated
	 */
	WhileLoopStatement createWhileLoopStatement();

	/**
	 * Returns a new object of class '<em>Wide Image Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Wide Image Attribute</em>'.
	 * @generated
	 */
	WideImageAttribute createWideImageAttribute();

	/**
	 * Returns a new object of class '<em>Wide Value Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Wide Value Attribute</em>'.
	 * @generated
	 */
	WideValueAttribute createWideValueAttribute();

	/**
	 * Returns a new object of class '<em>Wide Wide Image Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Wide Wide Image Attribute</em>'.
	 * @generated
	 */
	WideWideImageAttribute createWideWideImageAttribute();

	/**
	 * Returns a new object of class '<em>Wide Wide Value Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Wide Wide Value Attribute</em>'.
	 * @generated
	 */
	WideWideValueAttribute createWideWideValueAttribute();

	/**
	 * Returns a new object of class '<em>Wide Wide Width Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Wide Wide Width Attribute</em>'.
	 * @generated
	 */
	WideWideWidthAttribute createWideWideWidthAttribute();

	/**
	 * Returns a new object of class '<em>Wide Width Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Wide Width Attribute</em>'.
	 * @generated
	 */
	WideWidthAttribute createWideWidthAttribute();

	/**
	 * Returns a new object of class '<em>Width Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Width Attribute</em>'.
	 * @generated
	 */
	WidthAttribute createWidthAttribute();

	/**
	 * Returns a new object of class '<em>With Clause</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>With Clause</em>'.
	 * @generated
	 */
	WithClause createWithClause();

	/**
	 * Returns a new object of class '<em>Write Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Write Attribute</em>'.
	 * @generated
	 */
	WriteAttribute createWriteAttribute();

	/**
	 * Returns a new object of class '<em>Xor Operator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Xor Operator</em>'.
	 * @generated
	 */
	XorOperator createXorOperator();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	AdaPackage getAdaPackage();

} //AdaFactory
