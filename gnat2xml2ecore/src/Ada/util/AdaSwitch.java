/**
 */
package Ada.util;

import Ada.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see Ada.AdaPackage
 * @generated
 */
public class AdaSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static AdaPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdaSwitch() {
		if (modelPackage == null) {
			modelPackage = AdaPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case AdaPackage.ABORT_STATEMENT: {
				AbortStatement abortStatement = (AbortStatement)theEObject;
				T result = caseAbortStatement(abortStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ABS_OPERATOR: {
				AbsOperator absOperator = (AbsOperator)theEObject;
				T result = caseAbsOperator(absOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ABSTRACT: {
				Abstract abstract_ = (Abstract)theEObject;
				T result = caseAbstract(abstract_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ACCEPT_STATEMENT: {
				AcceptStatement acceptStatement = (AcceptStatement)theEObject;
				T result = caseAcceptStatement(acceptStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ACCESS_ATTRIBUTE: {
				AccessAttribute accessAttribute = (AccessAttribute)theEObject;
				T result = caseAccessAttribute(accessAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ACCESS_TO_CONSTANT: {
				AccessToConstant accessToConstant = (AccessToConstant)theEObject;
				T result = caseAccessToConstant(accessToConstant);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ACCESS_TO_FUNCTION: {
				AccessToFunction accessToFunction = (AccessToFunction)theEObject;
				T result = caseAccessToFunction(accessToFunction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ACCESS_TO_PROCEDURE: {
				AccessToProcedure accessToProcedure = (AccessToProcedure)theEObject;
				T result = caseAccessToProcedure(accessToProcedure);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ACCESS_TO_PROTECTED_FUNCTION: {
				AccessToProtectedFunction accessToProtectedFunction = (AccessToProtectedFunction)theEObject;
				T result = caseAccessToProtectedFunction(accessToProtectedFunction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ACCESS_TO_PROTECTED_PROCEDURE: {
				AccessToProtectedProcedure accessToProtectedProcedure = (AccessToProtectedProcedure)theEObject;
				T result = caseAccessToProtectedProcedure(accessToProtectedProcedure);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ACCESS_TO_VARIABLE: {
				AccessToVariable accessToVariable = (AccessToVariable)theEObject;
				T result = caseAccessToVariable(accessToVariable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ADDRESS_ATTRIBUTE: {
				AddressAttribute addressAttribute = (AddressAttribute)theEObject;
				T result = caseAddressAttribute(addressAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ADJACENT_ATTRIBUTE: {
				AdjacentAttribute adjacentAttribute = (AdjacentAttribute)theEObject;
				T result = caseAdjacentAttribute(adjacentAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.AFT_ATTRIBUTE: {
				AftAttribute aftAttribute = (AftAttribute)theEObject;
				T result = caseAftAttribute(aftAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ALIASED: {
				Aliased aliased = (Aliased)theEObject;
				T result = caseAliased(aliased);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ALIGNMENT_ATTRIBUTE: {
				AlignmentAttribute alignmentAttribute = (AlignmentAttribute)theEObject;
				T result = caseAlignmentAttribute(alignmentAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ALL_CALLS_REMOTE_PRAGMA: {
				AllCallsRemotePragma allCallsRemotePragma = (AllCallsRemotePragma)theEObject;
				T result = caseAllCallsRemotePragma(allCallsRemotePragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ALLOCATION_FROM_QUALIFIED_EXPRESSION: {
				AllocationFromQualifiedExpression allocationFromQualifiedExpression = (AllocationFromQualifiedExpression)theEObject;
				T result = caseAllocationFromQualifiedExpression(allocationFromQualifiedExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ALLOCATION_FROM_SUBTYPE: {
				AllocationFromSubtype allocationFromSubtype = (AllocationFromSubtype)theEObject;
				T result = caseAllocationFromSubtype(allocationFromSubtype);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.AND_OPERATOR: {
				AndOperator andOperator = (AndOperator)theEObject;
				T result = caseAndOperator(andOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.AND_THEN_SHORT_CIRCUIT: {
				AndThenShortCircuit andThenShortCircuit = (AndThenShortCircuit)theEObject;
				T result = caseAndThenShortCircuit(andThenShortCircuit);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ANONYMOUS_ACCESS_TO_CONSTANT: {
				AnonymousAccessToConstant anonymousAccessToConstant = (AnonymousAccessToConstant)theEObject;
				T result = caseAnonymousAccessToConstant(anonymousAccessToConstant);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ANONYMOUS_ACCESS_TO_FUNCTION: {
				AnonymousAccessToFunction anonymousAccessToFunction = (AnonymousAccessToFunction)theEObject;
				T result = caseAnonymousAccessToFunction(anonymousAccessToFunction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROCEDURE: {
				AnonymousAccessToProcedure anonymousAccessToProcedure = (AnonymousAccessToProcedure)theEObject;
				T result = caseAnonymousAccessToProcedure(anonymousAccessToProcedure);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION: {
				AnonymousAccessToProtectedFunction anonymousAccessToProtectedFunction = (AnonymousAccessToProtectedFunction)theEObject;
				T result = caseAnonymousAccessToProtectedFunction(anonymousAccessToProtectedFunction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE: {
				AnonymousAccessToProtectedProcedure anonymousAccessToProtectedProcedure = (AnonymousAccessToProtectedProcedure)theEObject;
				T result = caseAnonymousAccessToProtectedProcedure(anonymousAccessToProtectedProcedure);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE: {
				AnonymousAccessToVariable anonymousAccessToVariable = (AnonymousAccessToVariable)theEObject;
				T result = caseAnonymousAccessToVariable(anonymousAccessToVariable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ARRAY_COMPONENT_ASSOCIATION: {
				ArrayComponentAssociation arrayComponentAssociation = (ArrayComponentAssociation)theEObject;
				T result = caseArrayComponentAssociation(arrayComponentAssociation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ASPECT_SPECIFICATION: {
				AspectSpecification aspectSpecification = (AspectSpecification)theEObject;
				T result = caseAspectSpecification(aspectSpecification);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ASSERTION_POLICY_PRAGMA: {
				AssertionPolicyPragma assertionPolicyPragma = (AssertionPolicyPragma)theEObject;
				T result = caseAssertionPolicyPragma(assertionPolicyPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ASSERT_PRAGMA: {
				AssertPragma assertPragma = (AssertPragma)theEObject;
				T result = caseAssertPragma(assertPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ASSIGNMENT_STATEMENT: {
				AssignmentStatement assignmentStatement = (AssignmentStatement)theEObject;
				T result = caseAssignmentStatement(assignmentStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ASSOCIATION_CLASS: {
				AssociationClass associationClass = (AssociationClass)theEObject;
				T result = caseAssociationClass(associationClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ASSOCIATION_LIST: {
				AssociationList associationList = (AssociationList)theEObject;
				T result = caseAssociationList(associationList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ASYNCHRONOUS_PRAGMA: {
				AsynchronousPragma asynchronousPragma = (AsynchronousPragma)theEObject;
				T result = caseAsynchronousPragma(asynchronousPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ASYNCHRONOUS_SELECT_STATEMENT: {
				AsynchronousSelectStatement asynchronousSelectStatement = (AsynchronousSelectStatement)theEObject;
				T result = caseAsynchronousSelectStatement(asynchronousSelectStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.AT_CLAUSE: {
				AtClause atClause = (AtClause)theEObject;
				T result = caseAtClause(atClause);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ATOMIC_COMPONENTS_PRAGMA: {
				AtomicComponentsPragma atomicComponentsPragma = (AtomicComponentsPragma)theEObject;
				T result = caseAtomicComponentsPragma(atomicComponentsPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ATOMIC_PRAGMA: {
				AtomicPragma atomicPragma = (AtomicPragma)theEObject;
				T result = caseAtomicPragma(atomicPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ATTACH_HANDLER_PRAGMA: {
				AttachHandlerPragma attachHandlerPragma = (AttachHandlerPragma)theEObject;
				T result = caseAttachHandlerPragma(attachHandlerPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE: {
				AttributeDefinitionClause attributeDefinitionClause = (AttributeDefinitionClause)theEObject;
				T result = caseAttributeDefinitionClause(attributeDefinitionClause);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.BASE_ATTRIBUTE: {
				BaseAttribute baseAttribute = (BaseAttribute)theEObject;
				T result = caseBaseAttribute(baseAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.BIT_ORDER_ATTRIBUTE: {
				BitOrderAttribute bitOrderAttribute = (BitOrderAttribute)theEObject;
				T result = caseBitOrderAttribute(bitOrderAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.BLOCK_STATEMENT: {
				BlockStatement blockStatement = (BlockStatement)theEObject;
				T result = caseBlockStatement(blockStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.BODY_VERSION_ATTRIBUTE: {
				BodyVersionAttribute bodyVersionAttribute = (BodyVersionAttribute)theEObject;
				T result = caseBodyVersionAttribute(bodyVersionAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.BOX_EXPRESSION: {
				BoxExpression boxExpression = (BoxExpression)theEObject;
				T result = caseBoxExpression(boxExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.CALLABLE_ATTRIBUTE: {
				CallableAttribute callableAttribute = (CallableAttribute)theEObject;
				T result = caseCallableAttribute(callableAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.CALLER_ATTRIBUTE: {
				CallerAttribute callerAttribute = (CallerAttribute)theEObject;
				T result = caseCallerAttribute(callerAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.CASE_EXPRESSION: {
				CaseExpression caseExpression = (CaseExpression)theEObject;
				T result = caseCaseExpression(caseExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.CASE_EXPRESSION_PATH: {
				CaseExpressionPath caseExpressionPath = (CaseExpressionPath)theEObject;
				T result = caseCaseExpressionPath(caseExpressionPath);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.CASE_PATH: {
				CasePath casePath = (CasePath)theEObject;
				T result = caseCasePath(casePath);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.CASE_STATEMENT: {
				CaseStatement caseStatement = (CaseStatement)theEObject;
				T result = caseCaseStatement(caseStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.CEILING_ATTRIBUTE: {
				CeilingAttribute ceilingAttribute = (CeilingAttribute)theEObject;
				T result = caseCeilingAttribute(ceilingAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.CHARACTER_LITERAL: {
				CharacterLiteral characterLiteral = (CharacterLiteral)theEObject;
				T result = caseCharacterLiteral(characterLiteral);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.CHOICE_PARAMETER_SPECIFICATION: {
				ChoiceParameterSpecification choiceParameterSpecification = (ChoiceParameterSpecification)theEObject;
				T result = caseChoiceParameterSpecification(choiceParameterSpecification);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.CLASS_ATTRIBUTE: {
				ClassAttribute classAttribute = (ClassAttribute)theEObject;
				T result = caseClassAttribute(classAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.CODE_STATEMENT: {
				CodeStatement codeStatement = (CodeStatement)theEObject;
				T result = caseCodeStatement(codeStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.COMMENT: {
				Comment comment = (Comment)theEObject;
				T result = caseComment(comment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.COMPILATION_UNIT: {
				CompilationUnit compilationUnit = (CompilationUnit)theEObject;
				T result = caseCompilationUnit(compilationUnit);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.COMPONENT_CLAUSE: {
				ComponentClause componentClause = (ComponentClause)theEObject;
				T result = caseComponentClause(componentClause);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.COMPONENT_CLAUSE_LIST: {
				ComponentClauseList componentClauseList = (ComponentClauseList)theEObject;
				T result = caseComponentClauseList(componentClauseList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.COMPONENT_DECLARATION: {
				ComponentDeclaration componentDeclaration = (ComponentDeclaration)theEObject;
				T result = caseComponentDeclaration(componentDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.COMPONENT_DEFINITION: {
				ComponentDefinition componentDefinition = (ComponentDefinition)theEObject;
				T result = caseComponentDefinition(componentDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.COMPONENT_SIZE_ATTRIBUTE: {
				ComponentSizeAttribute componentSizeAttribute = (ComponentSizeAttribute)theEObject;
				T result = caseComponentSizeAttribute(componentSizeAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.COMPOSE_ATTRIBUTE: {
				ComposeAttribute composeAttribute = (ComposeAttribute)theEObject;
				T result = caseComposeAttribute(composeAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.CONCATENATE_OPERATOR: {
				ConcatenateOperator concatenateOperator = (ConcatenateOperator)theEObject;
				T result = caseConcatenateOperator(concatenateOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.CONDITIONAL_ENTRY_CALL_STATEMENT: {
				ConditionalEntryCallStatement conditionalEntryCallStatement = (ConditionalEntryCallStatement)theEObject;
				T result = caseConditionalEntryCallStatement(conditionalEntryCallStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.CONSTANT_DECLARATION: {
				ConstantDeclaration constantDeclaration = (ConstantDeclaration)theEObject;
				T result = caseConstantDeclaration(constantDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.CONSTRAINED_ARRAY_DEFINITION: {
				ConstrainedArrayDefinition constrainedArrayDefinition = (ConstrainedArrayDefinition)theEObject;
				T result = caseConstrainedArrayDefinition(constrainedArrayDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.CONSTRAINED_ATTRIBUTE: {
				ConstrainedAttribute constrainedAttribute = (ConstrainedAttribute)theEObject;
				T result = caseConstrainedAttribute(constrainedAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.CONSTRAINT_CLASS: {
				ConstraintClass constraintClass = (ConstraintClass)theEObject;
				T result = caseConstraintClass(constraintClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.CONTEXT_CLAUSE_CLASS: {
				ContextClauseClass contextClauseClass = (ContextClauseClass)theEObject;
				T result = caseContextClauseClass(contextClauseClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.CONTEXT_CLAUSE_LIST: {
				ContextClauseList contextClauseList = (ContextClauseList)theEObject;
				T result = caseContextClauseList(contextClauseList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.CONTROLLED_PRAGMA: {
				ControlledPragma controlledPragma = (ControlledPragma)theEObject;
				T result = caseControlledPragma(controlledPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.CONVENTION_PRAGMA: {
				ConventionPragma conventionPragma = (ConventionPragma)theEObject;
				T result = caseConventionPragma(conventionPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.COPY_SIGN_ATTRIBUTE: {
				CopySignAttribute copySignAttribute = (CopySignAttribute)theEObject;
				T result = caseCopySignAttribute(copySignAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.COUNT_ATTRIBUTE: {
				CountAttribute countAttribute = (CountAttribute)theEObject;
				T result = caseCountAttribute(countAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.CPU_PRAGMA: {
				CpuPragma cpuPragma = (CpuPragma)theEObject;
				T result = caseCpuPragma(cpuPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DECIMAL_FIXED_POINT_DEFINITION: {
				DecimalFixedPointDefinition decimalFixedPointDefinition = (DecimalFixedPointDefinition)theEObject;
				T result = caseDecimalFixedPointDefinition(decimalFixedPointDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DECLARATION_CLASS: {
				DeclarationClass declarationClass = (DeclarationClass)theEObject;
				T result = caseDeclarationClass(declarationClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DECLARATION_LIST: {
				DeclarationList declarationList = (DeclarationList)theEObject;
				T result = caseDeclarationList(declarationList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DECLARATIVE_ITEM_CLASS: {
				DeclarativeItemClass declarativeItemClass = (DeclarativeItemClass)theEObject;
				T result = caseDeclarativeItemClass(declarativeItemClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DECLARATIVE_ITEM_LIST: {
				DeclarativeItemList declarativeItemList = (DeclarativeItemList)theEObject;
				T result = caseDeclarativeItemList(declarativeItemList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFAULT_STORAGE_POOL_PRAGMA: {
				DefaultStoragePoolPragma defaultStoragePoolPragma = (DefaultStoragePoolPragma)theEObject;
				T result = caseDefaultStoragePoolPragma(defaultStoragePoolPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION: {
				DeferredConstantDeclaration deferredConstantDeclaration = (DeferredConstantDeclaration)theEObject;
				T result = caseDeferredConstantDeclaration(deferredConstantDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_ABS_OPERATOR: {
				DefiningAbsOperator definingAbsOperator = (DefiningAbsOperator)theEObject;
				T result = caseDefiningAbsOperator(definingAbsOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_AND_OPERATOR: {
				DefiningAndOperator definingAndOperator = (DefiningAndOperator)theEObject;
				T result = caseDefiningAndOperator(definingAndOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_CHARACTER_LITERAL: {
				DefiningCharacterLiteral definingCharacterLiteral = (DefiningCharacterLiteral)theEObject;
				T result = caseDefiningCharacterLiteral(definingCharacterLiteral);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_CONCATENATE_OPERATOR: {
				DefiningConcatenateOperator definingConcatenateOperator = (DefiningConcatenateOperator)theEObject;
				T result = caseDefiningConcatenateOperator(definingConcatenateOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_DIVIDE_OPERATOR: {
				DefiningDivideOperator definingDivideOperator = (DefiningDivideOperator)theEObject;
				T result = caseDefiningDivideOperator(definingDivideOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_ENUMERATION_LITERAL: {
				DefiningEnumerationLiteral definingEnumerationLiteral = (DefiningEnumerationLiteral)theEObject;
				T result = caseDefiningEnumerationLiteral(definingEnumerationLiteral);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_EQUAL_OPERATOR: {
				DefiningEqualOperator definingEqualOperator = (DefiningEqualOperator)theEObject;
				T result = caseDefiningEqualOperator(definingEqualOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_EXPANDED_NAME: {
				DefiningExpandedName definingExpandedName = (DefiningExpandedName)theEObject;
				T result = caseDefiningExpandedName(definingExpandedName);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_EXPONENTIATE_OPERATOR: {
				DefiningExponentiateOperator definingExponentiateOperator = (DefiningExponentiateOperator)theEObject;
				T result = caseDefiningExponentiateOperator(definingExponentiateOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_GREATER_THAN_OPERATOR: {
				DefiningGreaterThanOperator definingGreaterThanOperator = (DefiningGreaterThanOperator)theEObject;
				T result = caseDefiningGreaterThanOperator(definingGreaterThanOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_GREATER_THAN_OR_EQUAL_OPERATOR: {
				DefiningGreaterThanOrEqualOperator definingGreaterThanOrEqualOperator = (DefiningGreaterThanOrEqualOperator)theEObject;
				T result = caseDefiningGreaterThanOrEqualOperator(definingGreaterThanOrEqualOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_IDENTIFIER: {
				DefiningIdentifier definingIdentifier = (DefiningIdentifier)theEObject;
				T result = caseDefiningIdentifier(definingIdentifier);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_LESS_THAN_OPERATOR: {
				DefiningLessThanOperator definingLessThanOperator = (DefiningLessThanOperator)theEObject;
				T result = caseDefiningLessThanOperator(definingLessThanOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_LESS_THAN_OR_EQUAL_OPERATOR: {
				DefiningLessThanOrEqualOperator definingLessThanOrEqualOperator = (DefiningLessThanOrEqualOperator)theEObject;
				T result = caseDefiningLessThanOrEqualOperator(definingLessThanOrEqualOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_MINUS_OPERATOR: {
				DefiningMinusOperator definingMinusOperator = (DefiningMinusOperator)theEObject;
				T result = caseDefiningMinusOperator(definingMinusOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_MOD_OPERATOR: {
				DefiningModOperator definingModOperator = (DefiningModOperator)theEObject;
				T result = caseDefiningModOperator(definingModOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_MULTIPLY_OPERATOR: {
				DefiningMultiplyOperator definingMultiplyOperator = (DefiningMultiplyOperator)theEObject;
				T result = caseDefiningMultiplyOperator(definingMultiplyOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_NAME_CLASS: {
				DefiningNameClass definingNameClass = (DefiningNameClass)theEObject;
				T result = caseDefiningNameClass(definingNameClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_NAME_LIST: {
				DefiningNameList definingNameList = (DefiningNameList)theEObject;
				T result = caseDefiningNameList(definingNameList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_NOT_EQUAL_OPERATOR: {
				DefiningNotEqualOperator definingNotEqualOperator = (DefiningNotEqualOperator)theEObject;
				T result = caseDefiningNotEqualOperator(definingNotEqualOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_NOT_OPERATOR: {
				DefiningNotOperator definingNotOperator = (DefiningNotOperator)theEObject;
				T result = caseDefiningNotOperator(definingNotOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_OR_OPERATOR: {
				DefiningOrOperator definingOrOperator = (DefiningOrOperator)theEObject;
				T result = caseDefiningOrOperator(definingOrOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_PLUS_OPERATOR: {
				DefiningPlusOperator definingPlusOperator = (DefiningPlusOperator)theEObject;
				T result = caseDefiningPlusOperator(definingPlusOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_REM_OPERATOR: {
				DefiningRemOperator definingRemOperator = (DefiningRemOperator)theEObject;
				T result = caseDefiningRemOperator(definingRemOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_UNARY_MINUS_OPERATOR: {
				DefiningUnaryMinusOperator definingUnaryMinusOperator = (DefiningUnaryMinusOperator)theEObject;
				T result = caseDefiningUnaryMinusOperator(definingUnaryMinusOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_UNARY_PLUS_OPERATOR: {
				DefiningUnaryPlusOperator definingUnaryPlusOperator = (DefiningUnaryPlusOperator)theEObject;
				T result = caseDefiningUnaryPlusOperator(definingUnaryPlusOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINING_XOR_OPERATOR: {
				DefiningXorOperator definingXorOperator = (DefiningXorOperator)theEObject;
				T result = caseDefiningXorOperator(definingXorOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINITE_ATTRIBUTE: {
				DefiniteAttribute definiteAttribute = (DefiniteAttribute)theEObject;
				T result = caseDefiniteAttribute(definiteAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINITION_CLASS: {
				DefinitionClass definitionClass = (DefinitionClass)theEObject;
				T result = caseDefinitionClass(definitionClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DEFINITION_LIST: {
				DefinitionList definitionList = (DefinitionList)theEObject;
				T result = caseDefinitionList(definitionList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DELAY_RELATIVE_STATEMENT: {
				DelayRelativeStatement delayRelativeStatement = (DelayRelativeStatement)theEObject;
				T result = caseDelayRelativeStatement(delayRelativeStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DELAY_UNTIL_STATEMENT: {
				DelayUntilStatement delayUntilStatement = (DelayUntilStatement)theEObject;
				T result = caseDelayUntilStatement(delayUntilStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DELTA_ATTRIBUTE: {
				DeltaAttribute deltaAttribute = (DeltaAttribute)theEObject;
				T result = caseDeltaAttribute(deltaAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DELTA_CONSTRAINT: {
				DeltaConstraint deltaConstraint = (DeltaConstraint)theEObject;
				T result = caseDeltaConstraint(deltaConstraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DENORM_ATTRIBUTE: {
				DenormAttribute denormAttribute = (DenormAttribute)theEObject;
				T result = caseDenormAttribute(denormAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION: {
				DerivedRecordExtensionDefinition derivedRecordExtensionDefinition = (DerivedRecordExtensionDefinition)theEObject;
				T result = caseDerivedRecordExtensionDefinition(derivedRecordExtensionDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DERIVED_TYPE_DEFINITION: {
				DerivedTypeDefinition derivedTypeDefinition = (DerivedTypeDefinition)theEObject;
				T result = caseDerivedTypeDefinition(derivedTypeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DETECT_BLOCKING_PRAGMA: {
				DetectBlockingPragma detectBlockingPragma = (DetectBlockingPragma)theEObject;
				T result = caseDetectBlockingPragma(detectBlockingPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DIGITS_ATTRIBUTE: {
				DigitsAttribute digitsAttribute = (DigitsAttribute)theEObject;
				T result = caseDigitsAttribute(digitsAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DIGITS_CONSTRAINT: {
				DigitsConstraint digitsConstraint = (DigitsConstraint)theEObject;
				T result = caseDigitsConstraint(digitsConstraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DISCARD_NAMES_PRAGMA: {
				DiscardNamesPragma discardNamesPragma = (DiscardNamesPragma)theEObject;
				T result = caseDiscardNamesPragma(discardNamesPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DISCRETE_RANGE_ATTRIBUTE_REFERENCE: {
				DiscreteRangeAttributeReference discreteRangeAttributeReference = (DiscreteRangeAttributeReference)theEObject;
				T result = caseDiscreteRangeAttributeReference(discreteRangeAttributeReference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION: {
				DiscreteRangeAttributeReferenceAsSubtypeDefinition discreteRangeAttributeReferenceAsSubtypeDefinition = (DiscreteRangeAttributeReferenceAsSubtypeDefinition)theEObject;
				T result = caseDiscreteRangeAttributeReferenceAsSubtypeDefinition(discreteRangeAttributeReferenceAsSubtypeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DISCRETE_RANGE_CLASS: {
				DiscreteRangeClass discreteRangeClass = (DiscreteRangeClass)theEObject;
				T result = caseDiscreteRangeClass(discreteRangeClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DISCRETE_RANGE_LIST: {
				DiscreteRangeList discreteRangeList = (DiscreteRangeList)theEObject;
				T result = caseDiscreteRangeList(discreteRangeList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE: {
				DiscreteSimpleExpressionRange discreteSimpleExpressionRange = (DiscreteSimpleExpressionRange)theEObject;
				T result = caseDiscreteSimpleExpressionRange(discreteSimpleExpressionRange);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION: {
				DiscreteSimpleExpressionRangeAsSubtypeDefinition discreteSimpleExpressionRangeAsSubtypeDefinition = (DiscreteSimpleExpressionRangeAsSubtypeDefinition)theEObject;
				T result = caseDiscreteSimpleExpressionRangeAsSubtypeDefinition(discreteSimpleExpressionRangeAsSubtypeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DISCRETE_SUBTYPE_DEFINITION_CLASS: {
				DiscreteSubtypeDefinitionClass discreteSubtypeDefinitionClass = (DiscreteSubtypeDefinitionClass)theEObject;
				T result = caseDiscreteSubtypeDefinitionClass(discreteSubtypeDefinitionClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DISCRETE_SUBTYPE_INDICATION: {
				DiscreteSubtypeIndication discreteSubtypeIndication = (DiscreteSubtypeIndication)theEObject;
				T result = caseDiscreteSubtypeIndication(discreteSubtypeIndication);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION: {
				DiscreteSubtypeIndicationAsSubtypeDefinition discreteSubtypeIndicationAsSubtypeDefinition = (DiscreteSubtypeIndicationAsSubtypeDefinition)theEObject;
				T result = caseDiscreteSubtypeIndicationAsSubtypeDefinition(discreteSubtypeIndicationAsSubtypeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DISCRIMINANT_ASSOCIATION: {
				DiscriminantAssociation discriminantAssociation = (DiscriminantAssociation)theEObject;
				T result = caseDiscriminantAssociation(discriminantAssociation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DISCRIMINANT_ASSOCIATION_LIST: {
				DiscriminantAssociationList discriminantAssociationList = (DiscriminantAssociationList)theEObject;
				T result = caseDiscriminantAssociationList(discriminantAssociationList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DISCRIMINANT_CONSTRAINT: {
				DiscriminantConstraint discriminantConstraint = (DiscriminantConstraint)theEObject;
				T result = caseDiscriminantConstraint(discriminantConstraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DISCRIMINANT_SPECIFICATION: {
				DiscriminantSpecification discriminantSpecification = (DiscriminantSpecification)theEObject;
				T result = caseDiscriminantSpecification(discriminantSpecification);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST: {
				DiscriminantSpecificationList discriminantSpecificationList = (DiscriminantSpecificationList)theEObject;
				T result = caseDiscriminantSpecificationList(discriminantSpecificationList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DISPATCHING_DOMAIN_PRAGMA: {
				DispatchingDomainPragma dispatchingDomainPragma = (DispatchingDomainPragma)theEObject;
				T result = caseDispatchingDomainPragma(dispatchingDomainPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DIVIDE_OPERATOR: {
				DivideOperator divideOperator = (DivideOperator)theEObject;
				T result = caseDivideOperator(divideOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.DOCUMENT_ROOT: {
				DocumentRoot documentRoot = (DocumentRoot)theEObject;
				T result = caseDocumentRoot(documentRoot);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ELABORATE_ALL_PRAGMA: {
				ElaborateAllPragma elaborateAllPragma = (ElaborateAllPragma)theEObject;
				T result = caseElaborateAllPragma(elaborateAllPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ELABORATE_BODY_PRAGMA: {
				ElaborateBodyPragma elaborateBodyPragma = (ElaborateBodyPragma)theEObject;
				T result = caseElaborateBodyPragma(elaborateBodyPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ELABORATE_PRAGMA: {
				ElaboratePragma elaboratePragma = (ElaboratePragma)theEObject;
				T result = caseElaboratePragma(elaboratePragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ELEMENT_CLASS: {
				ElementClass elementClass = (ElementClass)theEObject;
				T result = caseElementClass(elementClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION: {
				ElementIteratorSpecification elementIteratorSpecification = (ElementIteratorSpecification)theEObject;
				T result = caseElementIteratorSpecification(elementIteratorSpecification);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ELEMENT_LIST: {
				ElementList elementList = (ElementList)theEObject;
				T result = caseElementList(elementList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ELSE_EXPRESSION_PATH: {
				ElseExpressionPath elseExpressionPath = (ElseExpressionPath)theEObject;
				T result = caseElseExpressionPath(elseExpressionPath);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ELSE_PATH: {
				ElsePath elsePath = (ElsePath)theEObject;
				T result = caseElsePath(elsePath);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ELSIF_EXPRESSION_PATH: {
				ElsifExpressionPath elsifExpressionPath = (ElsifExpressionPath)theEObject;
				T result = caseElsifExpressionPath(elsifExpressionPath);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ELSIF_PATH: {
				ElsifPath elsifPath = (ElsifPath)theEObject;
				T result = caseElsifPath(elsifPath);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ENTRY_BODY_DECLARATION: {
				EntryBodyDeclaration entryBodyDeclaration = (EntryBodyDeclaration)theEObject;
				T result = caseEntryBodyDeclaration(entryBodyDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ENTRY_CALL_STATEMENT: {
				EntryCallStatement entryCallStatement = (EntryCallStatement)theEObject;
				T result = caseEntryCallStatement(entryCallStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ENTRY_DECLARATION: {
				EntryDeclaration entryDeclaration = (EntryDeclaration)theEObject;
				T result = caseEntryDeclaration(entryDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ENTRY_INDEX_SPECIFICATION: {
				EntryIndexSpecification entryIndexSpecification = (EntryIndexSpecification)theEObject;
				T result = caseEntryIndexSpecification(entryIndexSpecification);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ENUMERATION_LITERAL: {
				EnumerationLiteral enumerationLiteral = (EnumerationLiteral)theEObject;
				T result = caseEnumerationLiteral(enumerationLiteral);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ENUMERATION_LITERAL_SPECIFICATION: {
				EnumerationLiteralSpecification enumerationLiteralSpecification = (EnumerationLiteralSpecification)theEObject;
				T result = caseEnumerationLiteralSpecification(enumerationLiteralSpecification);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ENUMERATION_REPRESENTATION_CLAUSE: {
				EnumerationRepresentationClause enumerationRepresentationClause = (EnumerationRepresentationClause)theEObject;
				T result = caseEnumerationRepresentationClause(enumerationRepresentationClause);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ENUMERATION_TYPE_DEFINITION: {
				EnumerationTypeDefinition enumerationTypeDefinition = (EnumerationTypeDefinition)theEObject;
				T result = caseEnumerationTypeDefinition(enumerationTypeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.EQUAL_OPERATOR: {
				EqualOperator equalOperator = (EqualOperator)theEObject;
				T result = caseEqualOperator(equalOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.EXCEPTION_DECLARATION: {
				ExceptionDeclaration exceptionDeclaration = (ExceptionDeclaration)theEObject;
				T result = caseExceptionDeclaration(exceptionDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.EXCEPTION_HANDLER: {
				ExceptionHandler exceptionHandler = (ExceptionHandler)theEObject;
				T result = caseExceptionHandler(exceptionHandler);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.EXCEPTION_HANDLER_LIST: {
				ExceptionHandlerList exceptionHandlerList = (ExceptionHandlerList)theEObject;
				T result = caseExceptionHandlerList(exceptionHandlerList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.EXCEPTION_RENAMING_DECLARATION: {
				ExceptionRenamingDeclaration exceptionRenamingDeclaration = (ExceptionRenamingDeclaration)theEObject;
				T result = caseExceptionRenamingDeclaration(exceptionRenamingDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.EXIT_STATEMENT: {
				ExitStatement exitStatement = (ExitStatement)theEObject;
				T result = caseExitStatement(exitStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.EXPLICIT_DEREFERENCE: {
				ExplicitDereference explicitDereference = (ExplicitDereference)theEObject;
				T result = caseExplicitDereference(explicitDereference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.EXPONENT_ATTRIBUTE: {
				ExponentAttribute exponentAttribute = (ExponentAttribute)theEObject;
				T result = caseExponentAttribute(exponentAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.EXPONENTIATE_OPERATOR: {
				ExponentiateOperator exponentiateOperator = (ExponentiateOperator)theEObject;
				T result = caseExponentiateOperator(exponentiateOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.EXPORT_PRAGMA: {
				ExportPragma exportPragma = (ExportPragma)theEObject;
				T result = caseExportPragma(exportPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.EXPRESSION_CLASS: {
				ExpressionClass expressionClass = (ExpressionClass)theEObject;
				T result = caseExpressionClass(expressionClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.EXPRESSION_FUNCTION_DECLARATION: {
				ExpressionFunctionDeclaration expressionFunctionDeclaration = (ExpressionFunctionDeclaration)theEObject;
				T result = caseExpressionFunctionDeclaration(expressionFunctionDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.EXPRESSION_LIST: {
				ExpressionList expressionList = (ExpressionList)theEObject;
				T result = caseExpressionList(expressionList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.EXTENDED_RETURN_STATEMENT: {
				ExtendedReturnStatement extendedReturnStatement = (ExtendedReturnStatement)theEObject;
				T result = caseExtendedReturnStatement(extendedReturnStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.EXTENSION_AGGREGATE: {
				ExtensionAggregate extensionAggregate = (ExtensionAggregate)theEObject;
				T result = caseExtensionAggregate(extensionAggregate);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.EXTERNAL_TAG_ATTRIBUTE: {
				ExternalTagAttribute externalTagAttribute = (ExternalTagAttribute)theEObject;
				T result = caseExternalTagAttribute(externalTagAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FIRST_ATTRIBUTE: {
				FirstAttribute firstAttribute = (FirstAttribute)theEObject;
				T result = caseFirstAttribute(firstAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FIRST_BIT_ATTRIBUTE: {
				FirstBitAttribute firstBitAttribute = (FirstBitAttribute)theEObject;
				T result = caseFirstBitAttribute(firstBitAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FLOATING_POINT_DEFINITION: {
				FloatingPointDefinition floatingPointDefinition = (FloatingPointDefinition)theEObject;
				T result = caseFloatingPointDefinition(floatingPointDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FLOOR_ATTRIBUTE: {
				FloorAttribute floorAttribute = (FloorAttribute)theEObject;
				T result = caseFloorAttribute(floorAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FOR_ALL_QUANTIFIED_EXPRESSION: {
				ForAllQuantifiedExpression forAllQuantifiedExpression = (ForAllQuantifiedExpression)theEObject;
				T result = caseForAllQuantifiedExpression(forAllQuantifiedExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORE_ATTRIBUTE: {
				ForeAttribute foreAttribute = (ForeAttribute)theEObject;
				T result = caseForeAttribute(foreAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FOR_LOOP_STATEMENT: {
				ForLoopStatement forLoopStatement = (ForLoopStatement)theEObject;
				T result = caseForLoopStatement(forLoopStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_ACCESS_TO_CONSTANT: {
				FormalAccessToConstant formalAccessToConstant = (FormalAccessToConstant)theEObject;
				T result = caseFormalAccessToConstant(formalAccessToConstant);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_ACCESS_TO_FUNCTION: {
				FormalAccessToFunction formalAccessToFunction = (FormalAccessToFunction)theEObject;
				T result = caseFormalAccessToFunction(formalAccessToFunction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_ACCESS_TO_PROCEDURE: {
				FormalAccessToProcedure formalAccessToProcedure = (FormalAccessToProcedure)theEObject;
				T result = caseFormalAccessToProcedure(formalAccessToProcedure);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_ACCESS_TO_PROTECTED_FUNCTION: {
				FormalAccessToProtectedFunction formalAccessToProtectedFunction = (FormalAccessToProtectedFunction)theEObject;
				T result = caseFormalAccessToProtectedFunction(formalAccessToProtectedFunction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_ACCESS_TO_PROTECTED_PROCEDURE: {
				FormalAccessToProtectedProcedure formalAccessToProtectedProcedure = (FormalAccessToProtectedProcedure)theEObject;
				T result = caseFormalAccessToProtectedProcedure(formalAccessToProtectedProcedure);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_ACCESS_TO_VARIABLE: {
				FormalAccessToVariable formalAccessToVariable = (FormalAccessToVariable)theEObject;
				T result = caseFormalAccessToVariable(formalAccessToVariable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_CONSTRAINED_ARRAY_DEFINITION: {
				FormalConstrainedArrayDefinition formalConstrainedArrayDefinition = (FormalConstrainedArrayDefinition)theEObject;
				T result = caseFormalConstrainedArrayDefinition(formalConstrainedArrayDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_DECIMAL_FIXED_POINT_DEFINITION: {
				FormalDecimalFixedPointDefinition formalDecimalFixedPointDefinition = (FormalDecimalFixedPointDefinition)theEObject;
				T result = caseFormalDecimalFixedPointDefinition(formalDecimalFixedPointDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION: {
				FormalDerivedTypeDefinition formalDerivedTypeDefinition = (FormalDerivedTypeDefinition)theEObject;
				T result = caseFormalDerivedTypeDefinition(formalDerivedTypeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_DISCRETE_TYPE_DEFINITION: {
				FormalDiscreteTypeDefinition formalDiscreteTypeDefinition = (FormalDiscreteTypeDefinition)theEObject;
				T result = caseFormalDiscreteTypeDefinition(formalDiscreteTypeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_FLOATING_POINT_DEFINITION: {
				FormalFloatingPointDefinition formalFloatingPointDefinition = (FormalFloatingPointDefinition)theEObject;
				T result = caseFormalFloatingPointDefinition(formalFloatingPointDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_FUNCTION_DECLARATION: {
				FormalFunctionDeclaration formalFunctionDeclaration = (FormalFunctionDeclaration)theEObject;
				T result = caseFormalFunctionDeclaration(formalFunctionDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_INCOMPLETE_TYPE_DECLARATION: {
				FormalIncompleteTypeDeclaration formalIncompleteTypeDeclaration = (FormalIncompleteTypeDeclaration)theEObject;
				T result = caseFormalIncompleteTypeDeclaration(formalIncompleteTypeDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_LIMITED_INTERFACE: {
				FormalLimitedInterface formalLimitedInterface = (FormalLimitedInterface)theEObject;
				T result = caseFormalLimitedInterface(formalLimitedInterface);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_MODULAR_TYPE_DEFINITION: {
				FormalModularTypeDefinition formalModularTypeDefinition = (FormalModularTypeDefinition)theEObject;
				T result = caseFormalModularTypeDefinition(formalModularTypeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_OBJECT_DECLARATION: {
				FormalObjectDeclaration formalObjectDeclaration = (FormalObjectDeclaration)theEObject;
				T result = caseFormalObjectDeclaration(formalObjectDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_ORDINARY_FIXED_POINT_DEFINITION: {
				FormalOrdinaryFixedPointDefinition formalOrdinaryFixedPointDefinition = (FormalOrdinaryFixedPointDefinition)theEObject;
				T result = caseFormalOrdinaryFixedPointDefinition(formalOrdinaryFixedPointDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_ORDINARY_INTERFACE: {
				FormalOrdinaryInterface formalOrdinaryInterface = (FormalOrdinaryInterface)theEObject;
				T result = caseFormalOrdinaryInterface(formalOrdinaryInterface);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_PACKAGE_DECLARATION: {
				FormalPackageDeclaration formalPackageDeclaration = (FormalPackageDeclaration)theEObject;
				T result = caseFormalPackageDeclaration(formalPackageDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_PACKAGE_DECLARATION_WITH_BOX: {
				FormalPackageDeclarationWithBox formalPackageDeclarationWithBox = (FormalPackageDeclarationWithBox)theEObject;
				T result = caseFormalPackageDeclarationWithBox(formalPackageDeclarationWithBox);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE: {
				FormalPoolSpecificAccessToVariable formalPoolSpecificAccessToVariable = (FormalPoolSpecificAccessToVariable)theEObject;
				T result = caseFormalPoolSpecificAccessToVariable(formalPoolSpecificAccessToVariable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_PRIVATE_TYPE_DEFINITION: {
				FormalPrivateTypeDefinition formalPrivateTypeDefinition = (FormalPrivateTypeDefinition)theEObject;
				T result = caseFormalPrivateTypeDefinition(formalPrivateTypeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_PROCEDURE_DECLARATION: {
				FormalProcedureDeclaration formalProcedureDeclaration = (FormalProcedureDeclaration)theEObject;
				T result = caseFormalProcedureDeclaration(formalProcedureDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_PROTECTED_INTERFACE: {
				FormalProtectedInterface formalProtectedInterface = (FormalProtectedInterface)theEObject;
				T result = caseFormalProtectedInterface(formalProtectedInterface);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_SIGNED_INTEGER_TYPE_DEFINITION: {
				FormalSignedIntegerTypeDefinition formalSignedIntegerTypeDefinition = (FormalSignedIntegerTypeDefinition)theEObject;
				T result = caseFormalSignedIntegerTypeDefinition(formalSignedIntegerTypeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_SYNCHRONIZED_INTERFACE: {
				FormalSynchronizedInterface formalSynchronizedInterface = (FormalSynchronizedInterface)theEObject;
				T result = caseFormalSynchronizedInterface(formalSynchronizedInterface);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION: {
				FormalTaggedPrivateTypeDefinition formalTaggedPrivateTypeDefinition = (FormalTaggedPrivateTypeDefinition)theEObject;
				T result = caseFormalTaggedPrivateTypeDefinition(formalTaggedPrivateTypeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_TASK_INTERFACE: {
				FormalTaskInterface formalTaskInterface = (FormalTaskInterface)theEObject;
				T result = caseFormalTaskInterface(formalTaskInterface);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_TYPE_DECLARATION: {
				FormalTypeDeclaration formalTypeDeclaration = (FormalTypeDeclaration)theEObject;
				T result = caseFormalTypeDeclaration(formalTypeDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FORMAL_UNCONSTRAINED_ARRAY_DEFINITION: {
				FormalUnconstrainedArrayDefinition formalUnconstrainedArrayDefinition = (FormalUnconstrainedArrayDefinition)theEObject;
				T result = caseFormalUnconstrainedArrayDefinition(formalUnconstrainedArrayDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION: {
				ForSomeQuantifiedExpression forSomeQuantifiedExpression = (ForSomeQuantifiedExpression)theEObject;
				T result = caseForSomeQuantifiedExpression(forSomeQuantifiedExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FRACTION_ATTRIBUTE: {
				FractionAttribute fractionAttribute = (FractionAttribute)theEObject;
				T result = caseFractionAttribute(fractionAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FUNCTION_BODY_DECLARATION: {
				FunctionBodyDeclaration functionBodyDeclaration = (FunctionBodyDeclaration)theEObject;
				T result = caseFunctionBodyDeclaration(functionBodyDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FUNCTION_BODY_STUB: {
				FunctionBodyStub functionBodyStub = (FunctionBodyStub)theEObject;
				T result = caseFunctionBodyStub(functionBodyStub);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FUNCTION_CALL: {
				FunctionCall functionCall = (FunctionCall)theEObject;
				T result = caseFunctionCall(functionCall);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FUNCTION_DECLARATION: {
				FunctionDeclaration functionDeclaration = (FunctionDeclaration)theEObject;
				T result = caseFunctionDeclaration(functionDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FUNCTION_INSTANTIATION: {
				FunctionInstantiation functionInstantiation = (FunctionInstantiation)theEObject;
				T result = caseFunctionInstantiation(functionInstantiation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.FUNCTION_RENAMING_DECLARATION: {
				FunctionRenamingDeclaration functionRenamingDeclaration = (FunctionRenamingDeclaration)theEObject;
				T result = caseFunctionRenamingDeclaration(functionRenamingDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.GENERALIZED_ITERATOR_SPECIFICATION: {
				GeneralizedIteratorSpecification generalizedIteratorSpecification = (GeneralizedIteratorSpecification)theEObject;
				T result = caseGeneralizedIteratorSpecification(generalizedIteratorSpecification);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.GENERIC_ASSOCIATION: {
				GenericAssociation genericAssociation = (GenericAssociation)theEObject;
				T result = caseGenericAssociation(genericAssociation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.GENERIC_FUNCTION_DECLARATION: {
				GenericFunctionDeclaration genericFunctionDeclaration = (GenericFunctionDeclaration)theEObject;
				T result = caseGenericFunctionDeclaration(genericFunctionDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.GENERIC_FUNCTION_RENAMING_DECLARATION: {
				GenericFunctionRenamingDeclaration genericFunctionRenamingDeclaration = (GenericFunctionRenamingDeclaration)theEObject;
				T result = caseGenericFunctionRenamingDeclaration(genericFunctionRenamingDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.GENERIC_PACKAGE_DECLARATION: {
				GenericPackageDeclaration genericPackageDeclaration = (GenericPackageDeclaration)theEObject;
				T result = caseGenericPackageDeclaration(genericPackageDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.GENERIC_PACKAGE_RENAMING_DECLARATION: {
				GenericPackageRenamingDeclaration genericPackageRenamingDeclaration = (GenericPackageRenamingDeclaration)theEObject;
				T result = caseGenericPackageRenamingDeclaration(genericPackageRenamingDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.GENERIC_PROCEDURE_DECLARATION: {
				GenericProcedureDeclaration genericProcedureDeclaration = (GenericProcedureDeclaration)theEObject;
				T result = caseGenericProcedureDeclaration(genericProcedureDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.GENERIC_PROCEDURE_RENAMING_DECLARATION: {
				GenericProcedureRenamingDeclaration genericProcedureRenamingDeclaration = (GenericProcedureRenamingDeclaration)theEObject;
				T result = caseGenericProcedureRenamingDeclaration(genericProcedureRenamingDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.GOTO_STATEMENT: {
				GotoStatement gotoStatement = (GotoStatement)theEObject;
				T result = caseGotoStatement(gotoStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.GREATER_THAN_OPERATOR: {
				GreaterThanOperator greaterThanOperator = (GreaterThanOperator)theEObject;
				T result = caseGreaterThanOperator(greaterThanOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.GREATER_THAN_OR_EQUAL_OPERATOR: {
				GreaterThanOrEqualOperator greaterThanOrEqualOperator = (GreaterThanOrEqualOperator)theEObject;
				T result = caseGreaterThanOrEqualOperator(greaterThanOrEqualOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_ABSTRACT_QTYPE: {
				HasAbstractQType hasAbstractQType = (HasAbstractQType)theEObject;
				T result = caseHasAbstractQType(hasAbstractQType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_ABSTRACT_QTYPE1: {
				HasAbstractQType1 hasAbstractQType1 = (HasAbstractQType1)theEObject;
				T result = caseHasAbstractQType1(hasAbstractQType1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_ABSTRACT_QTYPE2: {
				HasAbstractQType2 hasAbstractQType2 = (HasAbstractQType2)theEObject;
				T result = caseHasAbstractQType2(hasAbstractQType2);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_ABSTRACT_QTYPE3: {
				HasAbstractQType3 hasAbstractQType3 = (HasAbstractQType3)theEObject;
				T result = caseHasAbstractQType3(hasAbstractQType3);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_ABSTRACT_QTYPE4: {
				HasAbstractQType4 hasAbstractQType4 = (HasAbstractQType4)theEObject;
				T result = caseHasAbstractQType4(hasAbstractQType4);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_ABSTRACT_QTYPE5: {
				HasAbstractQType5 hasAbstractQType5 = (HasAbstractQType5)theEObject;
				T result = caseHasAbstractQType5(hasAbstractQType5);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_ABSTRACT_QTYPE6: {
				HasAbstractQType6 hasAbstractQType6 = (HasAbstractQType6)theEObject;
				T result = caseHasAbstractQType6(hasAbstractQType6);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_ABSTRACT_QTYPE7: {
				HasAbstractQType7 hasAbstractQType7 = (HasAbstractQType7)theEObject;
				T result = caseHasAbstractQType7(hasAbstractQType7);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_ABSTRACT_QTYPE8: {
				HasAbstractQType8 hasAbstractQType8 = (HasAbstractQType8)theEObject;
				T result = caseHasAbstractQType8(hasAbstractQType8);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_ABSTRACT_QTYPE9: {
				HasAbstractQType9 hasAbstractQType9 = (HasAbstractQType9)theEObject;
				T result = caseHasAbstractQType9(hasAbstractQType9);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_ABSTRACT_QTYPE10: {
				HasAbstractQType10 hasAbstractQType10 = (HasAbstractQType10)theEObject;
				T result = caseHasAbstractQType10(hasAbstractQType10);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_ABSTRACT_QTYPE11: {
				HasAbstractQType11 hasAbstractQType11 = (HasAbstractQType11)theEObject;
				T result = caseHasAbstractQType11(hasAbstractQType11);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_ABSTRACT_QTYPE12: {
				HasAbstractQType12 hasAbstractQType12 = (HasAbstractQType12)theEObject;
				T result = caseHasAbstractQType12(hasAbstractQType12);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_ABSTRACT_QTYPE13: {
				HasAbstractQType13 hasAbstractQType13 = (HasAbstractQType13)theEObject;
				T result = caseHasAbstractQType13(hasAbstractQType13);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_ALIASED_QTYPE: {
				HasAliasedQType hasAliasedQType = (HasAliasedQType)theEObject;
				T result = caseHasAliasedQType(hasAliasedQType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_ALIASED_QTYPE1: {
				HasAliasedQType1 hasAliasedQType1 = (HasAliasedQType1)theEObject;
				T result = caseHasAliasedQType1(hasAliasedQType1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_ALIASED_QTYPE2: {
				HasAliasedQType2 hasAliasedQType2 = (HasAliasedQType2)theEObject;
				T result = caseHasAliasedQType2(hasAliasedQType2);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_ALIASED_QTYPE3: {
				HasAliasedQType3 hasAliasedQType3 = (HasAliasedQType3)theEObject;
				T result = caseHasAliasedQType3(hasAliasedQType3);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_ALIASED_QTYPE4: {
				HasAliasedQType4 hasAliasedQType4 = (HasAliasedQType4)theEObject;
				T result = caseHasAliasedQType4(hasAliasedQType4);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_ALIASED_QTYPE5: {
				HasAliasedQType5 hasAliasedQType5 = (HasAliasedQType5)theEObject;
				T result = caseHasAliasedQType5(hasAliasedQType5);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_ALIASED_QTYPE6: {
				HasAliasedQType6 hasAliasedQType6 = (HasAliasedQType6)theEObject;
				T result = caseHasAliasedQType6(hasAliasedQType6);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_ALIASED_QTYPE7: {
				HasAliasedQType7 hasAliasedQType7 = (HasAliasedQType7)theEObject;
				T result = caseHasAliasedQType7(hasAliasedQType7);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_ALIASED_QTYPE8: {
				HasAliasedQType8 hasAliasedQType8 = (HasAliasedQType8)theEObject;
				T result = caseHasAliasedQType8(hasAliasedQType8);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_LIMITED_QTYPE: {
				HasLimitedQType hasLimitedQType = (HasLimitedQType)theEObject;
				T result = caseHasLimitedQType(hasLimitedQType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_LIMITED_QTYPE1: {
				HasLimitedQType1 hasLimitedQType1 = (HasLimitedQType1)theEObject;
				T result = caseHasLimitedQType1(hasLimitedQType1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_LIMITED_QTYPE2: {
				HasLimitedQType2 hasLimitedQType2 = (HasLimitedQType2)theEObject;
				T result = caseHasLimitedQType2(hasLimitedQType2);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_LIMITED_QTYPE3: {
				HasLimitedQType3 hasLimitedQType3 = (HasLimitedQType3)theEObject;
				T result = caseHasLimitedQType3(hasLimitedQType3);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_LIMITED_QTYPE4: {
				HasLimitedQType4 hasLimitedQType4 = (HasLimitedQType4)theEObject;
				T result = caseHasLimitedQType4(hasLimitedQType4);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_LIMITED_QTYPE5: {
				HasLimitedQType5 hasLimitedQType5 = (HasLimitedQType5)theEObject;
				T result = caseHasLimitedQType5(hasLimitedQType5);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_LIMITED_QTYPE6: {
				HasLimitedQType6 hasLimitedQType6 = (HasLimitedQType6)theEObject;
				T result = caseHasLimitedQType6(hasLimitedQType6);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_LIMITED_QTYPE7: {
				HasLimitedQType7 hasLimitedQType7 = (HasLimitedQType7)theEObject;
				T result = caseHasLimitedQType7(hasLimitedQType7);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_LIMITED_QTYPE8: {
				HasLimitedQType8 hasLimitedQType8 = (HasLimitedQType8)theEObject;
				T result = caseHasLimitedQType8(hasLimitedQType8);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_LIMITED_QTYPE9: {
				HasLimitedQType9 hasLimitedQType9 = (HasLimitedQType9)theEObject;
				T result = caseHasLimitedQType9(hasLimitedQType9);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_LIMITED_QTYPE10: {
				HasLimitedQType10 hasLimitedQType10 = (HasLimitedQType10)theEObject;
				T result = caseHasLimitedQType10(hasLimitedQType10);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_LIMITED_QTYPE11: {
				HasLimitedQType11 hasLimitedQType11 = (HasLimitedQType11)theEObject;
				T result = caseHasLimitedQType11(hasLimitedQType11);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE: {
				HasNullExclusionQType hasNullExclusionQType = (HasNullExclusionQType)theEObject;
				T result = caseHasNullExclusionQType(hasNullExclusionQType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE1: {
				HasNullExclusionQType1 hasNullExclusionQType1 = (HasNullExclusionQType1)theEObject;
				T result = caseHasNullExclusionQType1(hasNullExclusionQType1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE2: {
				HasNullExclusionQType2 hasNullExclusionQType2 = (HasNullExclusionQType2)theEObject;
				T result = caseHasNullExclusionQType2(hasNullExclusionQType2);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE3: {
				HasNullExclusionQType3 hasNullExclusionQType3 = (HasNullExclusionQType3)theEObject;
				T result = caseHasNullExclusionQType3(hasNullExclusionQType3);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE4: {
				HasNullExclusionQType4 hasNullExclusionQType4 = (HasNullExclusionQType4)theEObject;
				T result = caseHasNullExclusionQType4(hasNullExclusionQType4);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE5: {
				HasNullExclusionQType5 hasNullExclusionQType5 = (HasNullExclusionQType5)theEObject;
				T result = caseHasNullExclusionQType5(hasNullExclusionQType5);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE6: {
				HasNullExclusionQType6 hasNullExclusionQType6 = (HasNullExclusionQType6)theEObject;
				T result = caseHasNullExclusionQType6(hasNullExclusionQType6);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE7: {
				HasNullExclusionQType7 hasNullExclusionQType7 = (HasNullExclusionQType7)theEObject;
				T result = caseHasNullExclusionQType7(hasNullExclusionQType7);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE8: {
				HasNullExclusionQType8 hasNullExclusionQType8 = (HasNullExclusionQType8)theEObject;
				T result = caseHasNullExclusionQType8(hasNullExclusionQType8);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE9: {
				HasNullExclusionQType9 hasNullExclusionQType9 = (HasNullExclusionQType9)theEObject;
				T result = caseHasNullExclusionQType9(hasNullExclusionQType9);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE10: {
				HasNullExclusionQType10 hasNullExclusionQType10 = (HasNullExclusionQType10)theEObject;
				T result = caseHasNullExclusionQType10(hasNullExclusionQType10);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE11: {
				HasNullExclusionQType11 hasNullExclusionQType11 = (HasNullExclusionQType11)theEObject;
				T result = caseHasNullExclusionQType11(hasNullExclusionQType11);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE12: {
				HasNullExclusionQType12 hasNullExclusionQType12 = (HasNullExclusionQType12)theEObject;
				T result = caseHasNullExclusionQType12(hasNullExclusionQType12);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE13: {
				HasNullExclusionQType13 hasNullExclusionQType13 = (HasNullExclusionQType13)theEObject;
				T result = caseHasNullExclusionQType13(hasNullExclusionQType13);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE14: {
				HasNullExclusionQType14 hasNullExclusionQType14 = (HasNullExclusionQType14)theEObject;
				T result = caseHasNullExclusionQType14(hasNullExclusionQType14);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE15: {
				HasNullExclusionQType15 hasNullExclusionQType15 = (HasNullExclusionQType15)theEObject;
				T result = caseHasNullExclusionQType15(hasNullExclusionQType15);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE16: {
				HasNullExclusionQType16 hasNullExclusionQType16 = (HasNullExclusionQType16)theEObject;
				T result = caseHasNullExclusionQType16(hasNullExclusionQType16);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE17: {
				HasNullExclusionQType17 hasNullExclusionQType17 = (HasNullExclusionQType17)theEObject;
				T result = caseHasNullExclusionQType17(hasNullExclusionQType17);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE18: {
				HasNullExclusionQType18 hasNullExclusionQType18 = (HasNullExclusionQType18)theEObject;
				T result = caseHasNullExclusionQType18(hasNullExclusionQType18);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE19: {
				HasNullExclusionQType19 hasNullExclusionQType19 = (HasNullExclusionQType19)theEObject;
				T result = caseHasNullExclusionQType19(hasNullExclusionQType19);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE20: {
				HasNullExclusionQType20 hasNullExclusionQType20 = (HasNullExclusionQType20)theEObject;
				T result = caseHasNullExclusionQType20(hasNullExclusionQType20);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE21: {
				HasNullExclusionQType21 hasNullExclusionQType21 = (HasNullExclusionQType21)theEObject;
				T result = caseHasNullExclusionQType21(hasNullExclusionQType21);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE22: {
				HasNullExclusionQType22 hasNullExclusionQType22 = (HasNullExclusionQType22)theEObject;
				T result = caseHasNullExclusionQType22(hasNullExclusionQType22);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE23: {
				HasNullExclusionQType23 hasNullExclusionQType23 = (HasNullExclusionQType23)theEObject;
				T result = caseHasNullExclusionQType23(hasNullExclusionQType23);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE24: {
				HasNullExclusionQType24 hasNullExclusionQType24 = (HasNullExclusionQType24)theEObject;
				T result = caseHasNullExclusionQType24(hasNullExclusionQType24);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_PRIVATE_QTYPE: {
				HasPrivateQType hasPrivateQType = (HasPrivateQType)theEObject;
				T result = caseHasPrivateQType(hasPrivateQType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_PRIVATE_QTYPE1: {
				HasPrivateQType1 hasPrivateQType1 = (HasPrivateQType1)theEObject;
				T result = caseHasPrivateQType1(hasPrivateQType1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_REVERSE_QTYPE: {
				HasReverseQType hasReverseQType = (HasReverseQType)theEObject;
				T result = caseHasReverseQType(hasReverseQType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_REVERSE_QTYPE1: {
				HasReverseQType1 hasReverseQType1 = (HasReverseQType1)theEObject;
				T result = caseHasReverseQType1(hasReverseQType1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_REVERSE_QTYPE2: {
				HasReverseQType2 hasReverseQType2 = (HasReverseQType2)theEObject;
				T result = caseHasReverseQType2(hasReverseQType2);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_SYNCHRONIZED_QTYPE: {
				HasSynchronizedQType hasSynchronizedQType = (HasSynchronizedQType)theEObject;
				T result = caseHasSynchronizedQType(hasSynchronizedQType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_SYNCHRONIZED_QTYPE1: {
				HasSynchronizedQType1 hasSynchronizedQType1 = (HasSynchronizedQType1)theEObject;
				T result = caseHasSynchronizedQType1(hasSynchronizedQType1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.HAS_TAGGED_QTYPE: {
				HasTaggedQType hasTaggedQType = (HasTaggedQType)theEObject;
				T result = caseHasTaggedQType(hasTaggedQType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IDENTIFIER: {
				Identifier identifier = (Identifier)theEObject;
				T result = caseIdentifier(identifier);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IDENTITY_ATTRIBUTE: {
				IdentityAttribute identityAttribute = (IdentityAttribute)theEObject;
				T result = caseIdentityAttribute(identityAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IF_EXPRESSION: {
				IfExpression ifExpression = (IfExpression)theEObject;
				T result = caseIfExpression(ifExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IF_EXPRESSION_PATH: {
				IfExpressionPath ifExpressionPath = (IfExpressionPath)theEObject;
				T result = caseIfExpressionPath(ifExpressionPath);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IF_PATH: {
				IfPath ifPath = (IfPath)theEObject;
				T result = caseIfPath(ifPath);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IF_STATEMENT: {
				IfStatement ifStatement = (IfStatement)theEObject;
				T result = caseIfStatement(ifStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IMAGE_ATTRIBUTE: {
				ImageAttribute imageAttribute = (ImageAttribute)theEObject;
				T result = caseImageAttribute(imageAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IMPLEMENTATION_DEFINED_ATTRIBUTE: {
				ImplementationDefinedAttribute implementationDefinedAttribute = (ImplementationDefinedAttribute)theEObject;
				T result = caseImplementationDefinedAttribute(implementationDefinedAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IMPLEMENTATION_DEFINED_PRAGMA: {
				ImplementationDefinedPragma implementationDefinedPragma = (ImplementationDefinedPragma)theEObject;
				T result = caseImplementationDefinedPragma(implementationDefinedPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IMPORT_PRAGMA: {
				ImportPragma importPragma = (ImportPragma)theEObject;
				T result = caseImportPragma(importPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.INCOMPLETE_TYPE_DECLARATION: {
				IncompleteTypeDeclaration incompleteTypeDeclaration = (IncompleteTypeDeclaration)theEObject;
				T result = caseIncompleteTypeDeclaration(incompleteTypeDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.INDEPENDENT_COMPONENTS_PRAGMA: {
				IndependentComponentsPragma independentComponentsPragma = (IndependentComponentsPragma)theEObject;
				T result = caseIndependentComponentsPragma(independentComponentsPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.INDEPENDENT_PRAGMA: {
				IndependentPragma independentPragma = (IndependentPragma)theEObject;
				T result = caseIndependentPragma(independentPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.INDEX_CONSTRAINT: {
				IndexConstraint indexConstraint = (IndexConstraint)theEObject;
				T result = caseIndexConstraint(indexConstraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.INDEXED_COMPONENT: {
				IndexedComponent indexedComponent = (IndexedComponent)theEObject;
				T result = caseIndexedComponent(indexedComponent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.INLINE_PRAGMA: {
				InlinePragma inlinePragma = (InlinePragma)theEObject;
				T result = caseInlinePragma(inlinePragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IN_MEMBERSHIP_TEST: {
				InMembershipTest inMembershipTest = (InMembershipTest)theEObject;
				T result = caseInMembershipTest(inMembershipTest);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.INPUT_ATTRIBUTE: {
				InputAttribute inputAttribute = (InputAttribute)theEObject;
				T result = caseInputAttribute(inputAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.INSPECTION_POINT_PRAGMA: {
				InspectionPointPragma inspectionPointPragma = (InspectionPointPragma)theEObject;
				T result = caseInspectionPointPragma(inspectionPointPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.INTEGER_LITERAL: {
				IntegerLiteral integerLiteral = (IntegerLiteral)theEObject;
				T result = caseIntegerLiteral(integerLiteral);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.INTEGER_NUMBER_DECLARATION: {
				IntegerNumberDeclaration integerNumberDeclaration = (IntegerNumberDeclaration)theEObject;
				T result = caseIntegerNumberDeclaration(integerNumberDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.INTERRUPT_HANDLER_PRAGMA: {
				InterruptHandlerPragma interruptHandlerPragma = (InterruptHandlerPragma)theEObject;
				T result = caseInterruptHandlerPragma(interruptHandlerPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.INTERRUPT_PRIORITY_PRAGMA: {
				InterruptPriorityPragma interruptPriorityPragma = (InterruptPriorityPragma)theEObject;
				T result = caseInterruptPriorityPragma(interruptPriorityPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE: {
				IsNotNullReturnQType isNotNullReturnQType = (IsNotNullReturnQType)theEObject;
				T result = caseIsNotNullReturnQType(isNotNullReturnQType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE1: {
				IsNotNullReturnQType1 isNotNullReturnQType1 = (IsNotNullReturnQType1)theEObject;
				T result = caseIsNotNullReturnQType1(isNotNullReturnQType1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE2: {
				IsNotNullReturnQType2 isNotNullReturnQType2 = (IsNotNullReturnQType2)theEObject;
				T result = caseIsNotNullReturnQType2(isNotNullReturnQType2);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE3: {
				IsNotNullReturnQType3 isNotNullReturnQType3 = (IsNotNullReturnQType3)theEObject;
				T result = caseIsNotNullReturnQType3(isNotNullReturnQType3);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE4: {
				IsNotNullReturnQType4 isNotNullReturnQType4 = (IsNotNullReturnQType4)theEObject;
				T result = caseIsNotNullReturnQType4(isNotNullReturnQType4);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE5: {
				IsNotNullReturnQType5 isNotNullReturnQType5 = (IsNotNullReturnQType5)theEObject;
				T result = caseIsNotNullReturnQType5(isNotNullReturnQType5);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE6: {
				IsNotNullReturnQType6 isNotNullReturnQType6 = (IsNotNullReturnQType6)theEObject;
				T result = caseIsNotNullReturnQType6(isNotNullReturnQType6);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE7: {
				IsNotNullReturnQType7 isNotNullReturnQType7 = (IsNotNullReturnQType7)theEObject;
				T result = caseIsNotNullReturnQType7(isNotNullReturnQType7);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE8: {
				IsNotNullReturnQType8 isNotNullReturnQType8 = (IsNotNullReturnQType8)theEObject;
				T result = caseIsNotNullReturnQType8(isNotNullReturnQType8);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE9: {
				IsNotNullReturnQType9 isNotNullReturnQType9 = (IsNotNullReturnQType9)theEObject;
				T result = caseIsNotNullReturnQType9(isNotNullReturnQType9);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE10: {
				IsNotNullReturnQType10 isNotNullReturnQType10 = (IsNotNullReturnQType10)theEObject;
				T result = caseIsNotNullReturnQType10(isNotNullReturnQType10);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE11: {
				IsNotNullReturnQType11 isNotNullReturnQType11 = (IsNotNullReturnQType11)theEObject;
				T result = caseIsNotNullReturnQType11(isNotNullReturnQType11);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE: {
				IsNotOverridingDeclarationQType isNotOverridingDeclarationQType = (IsNotOverridingDeclarationQType)theEObject;
				T result = caseIsNotOverridingDeclarationQType(isNotOverridingDeclarationQType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE1: {
				IsNotOverridingDeclarationQType1 isNotOverridingDeclarationQType1 = (IsNotOverridingDeclarationQType1)theEObject;
				T result = caseIsNotOverridingDeclarationQType1(isNotOverridingDeclarationQType1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE2: {
				IsNotOverridingDeclarationQType2 isNotOverridingDeclarationQType2 = (IsNotOverridingDeclarationQType2)theEObject;
				T result = caseIsNotOverridingDeclarationQType2(isNotOverridingDeclarationQType2);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE3: {
				IsNotOverridingDeclarationQType3 isNotOverridingDeclarationQType3 = (IsNotOverridingDeclarationQType3)theEObject;
				T result = caseIsNotOverridingDeclarationQType3(isNotOverridingDeclarationQType3);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE4: {
				IsNotOverridingDeclarationQType4 isNotOverridingDeclarationQType4 = (IsNotOverridingDeclarationQType4)theEObject;
				T result = caseIsNotOverridingDeclarationQType4(isNotOverridingDeclarationQType4);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE5: {
				IsNotOverridingDeclarationQType5 isNotOverridingDeclarationQType5 = (IsNotOverridingDeclarationQType5)theEObject;
				T result = caseIsNotOverridingDeclarationQType5(isNotOverridingDeclarationQType5);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE6: {
				IsNotOverridingDeclarationQType6 isNotOverridingDeclarationQType6 = (IsNotOverridingDeclarationQType6)theEObject;
				T result = caseIsNotOverridingDeclarationQType6(isNotOverridingDeclarationQType6);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE7: {
				IsNotOverridingDeclarationQType7 isNotOverridingDeclarationQType7 = (IsNotOverridingDeclarationQType7)theEObject;
				T result = caseIsNotOverridingDeclarationQType7(isNotOverridingDeclarationQType7);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE8: {
				IsNotOverridingDeclarationQType8 isNotOverridingDeclarationQType8 = (IsNotOverridingDeclarationQType8)theEObject;
				T result = caseIsNotOverridingDeclarationQType8(isNotOverridingDeclarationQType8);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE9: {
				IsNotOverridingDeclarationQType9 isNotOverridingDeclarationQType9 = (IsNotOverridingDeclarationQType9)theEObject;
				T result = caseIsNotOverridingDeclarationQType9(isNotOverridingDeclarationQType9);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE10: {
				IsNotOverridingDeclarationQType10 isNotOverridingDeclarationQType10 = (IsNotOverridingDeclarationQType10)theEObject;
				T result = caseIsNotOverridingDeclarationQType10(isNotOverridingDeclarationQType10);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE11: {
				IsNotOverridingDeclarationQType11 isNotOverridingDeclarationQType11 = (IsNotOverridingDeclarationQType11)theEObject;
				T result = caseIsNotOverridingDeclarationQType11(isNotOverridingDeclarationQType11);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_OVERRIDING_DECLARATION_QTYPE: {
				IsOverridingDeclarationQType isOverridingDeclarationQType = (IsOverridingDeclarationQType)theEObject;
				T result = caseIsOverridingDeclarationQType(isOverridingDeclarationQType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_OVERRIDING_DECLARATION_QTYPE1: {
				IsOverridingDeclarationQType1 isOverridingDeclarationQType1 = (IsOverridingDeclarationQType1)theEObject;
				T result = caseIsOverridingDeclarationQType1(isOverridingDeclarationQType1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_OVERRIDING_DECLARATION_QTYPE2: {
				IsOverridingDeclarationQType2 isOverridingDeclarationQType2 = (IsOverridingDeclarationQType2)theEObject;
				T result = caseIsOverridingDeclarationQType2(isOverridingDeclarationQType2);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_OVERRIDING_DECLARATION_QTYPE3: {
				IsOverridingDeclarationQType3 isOverridingDeclarationQType3 = (IsOverridingDeclarationQType3)theEObject;
				T result = caseIsOverridingDeclarationQType3(isOverridingDeclarationQType3);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_OVERRIDING_DECLARATION_QTYPE4: {
				IsOverridingDeclarationQType4 isOverridingDeclarationQType4 = (IsOverridingDeclarationQType4)theEObject;
				T result = caseIsOverridingDeclarationQType4(isOverridingDeclarationQType4);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_OVERRIDING_DECLARATION_QTYPE5: {
				IsOverridingDeclarationQType5 isOverridingDeclarationQType5 = (IsOverridingDeclarationQType5)theEObject;
				T result = caseIsOverridingDeclarationQType5(isOverridingDeclarationQType5);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_OVERRIDING_DECLARATION_QTYPE6: {
				IsOverridingDeclarationQType6 isOverridingDeclarationQType6 = (IsOverridingDeclarationQType6)theEObject;
				T result = caseIsOverridingDeclarationQType6(isOverridingDeclarationQType6);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_OVERRIDING_DECLARATION_QTYPE7: {
				IsOverridingDeclarationQType7 isOverridingDeclarationQType7 = (IsOverridingDeclarationQType7)theEObject;
				T result = caseIsOverridingDeclarationQType7(isOverridingDeclarationQType7);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_OVERRIDING_DECLARATION_QTYPE8: {
				IsOverridingDeclarationQType8 isOverridingDeclarationQType8 = (IsOverridingDeclarationQType8)theEObject;
				T result = caseIsOverridingDeclarationQType8(isOverridingDeclarationQType8);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_OVERRIDING_DECLARATION_QTYPE9: {
				IsOverridingDeclarationQType9 isOverridingDeclarationQType9 = (IsOverridingDeclarationQType9)theEObject;
				T result = caseIsOverridingDeclarationQType9(isOverridingDeclarationQType9);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_OVERRIDING_DECLARATION_QTYPE10: {
				IsOverridingDeclarationQType10 isOverridingDeclarationQType10 = (IsOverridingDeclarationQType10)theEObject;
				T result = caseIsOverridingDeclarationQType10(isOverridingDeclarationQType10);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_OVERRIDING_DECLARATION_QTYPE11: {
				IsOverridingDeclarationQType11 isOverridingDeclarationQType11 = (IsOverridingDeclarationQType11)theEObject;
				T result = caseIsOverridingDeclarationQType11(isOverridingDeclarationQType11);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_PREFIX_CALL: {
				IsPrefixCall isPrefixCall = (IsPrefixCall)theEObject;
				T result = caseIsPrefixCall(isPrefixCall);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_PREFIX_CALL_QTYPE: {
				IsPrefixCallQType isPrefixCallQType = (IsPrefixCallQType)theEObject;
				T result = caseIsPrefixCallQType(isPrefixCallQType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_PREFIX_NOTATION: {
				IsPrefixNotation isPrefixNotation = (IsPrefixNotation)theEObject;
				T result = caseIsPrefixNotation(isPrefixNotation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_PREFIX_NOTATION_QTYPE: {
				IsPrefixNotationQType isPrefixNotationQType = (IsPrefixNotationQType)theEObject;
				T result = caseIsPrefixNotationQType(isPrefixNotationQType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.IS_PREFIX_NOTATION_QTYPE1: {
				IsPrefixNotationQType1 isPrefixNotationQType1 = (IsPrefixNotationQType1)theEObject;
				T result = caseIsPrefixNotationQType1(isPrefixNotationQType1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.KNOWN_DISCRIMINANT_PART: {
				KnownDiscriminantPart knownDiscriminantPart = (KnownDiscriminantPart)theEObject;
				T result = caseKnownDiscriminantPart(knownDiscriminantPart);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.LAST_ATTRIBUTE: {
				LastAttribute lastAttribute = (LastAttribute)theEObject;
				T result = caseLastAttribute(lastAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.LAST_BIT_ATTRIBUTE: {
				LastBitAttribute lastBitAttribute = (LastBitAttribute)theEObject;
				T result = caseLastBitAttribute(lastBitAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.LEADING_PART_ATTRIBUTE: {
				LeadingPartAttribute leadingPartAttribute = (LeadingPartAttribute)theEObject;
				T result = caseLeadingPartAttribute(leadingPartAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.LENGTH_ATTRIBUTE: {
				LengthAttribute lengthAttribute = (LengthAttribute)theEObject;
				T result = caseLengthAttribute(lengthAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.LESS_THAN_OPERATOR: {
				LessThanOperator lessThanOperator = (LessThanOperator)theEObject;
				T result = caseLessThanOperator(lessThanOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.LESS_THAN_OR_EQUAL_OPERATOR: {
				LessThanOrEqualOperator lessThanOrEqualOperator = (LessThanOrEqualOperator)theEObject;
				T result = caseLessThanOrEqualOperator(lessThanOrEqualOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.LIMITED: {
				Limited limited = (Limited)theEObject;
				T result = caseLimited(limited);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.LIMITED_INTERFACE: {
				LimitedInterface limitedInterface = (LimitedInterface)theEObject;
				T result = caseLimitedInterface(limitedInterface);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.LINKER_OPTIONS_PRAGMA: {
				LinkerOptionsPragma linkerOptionsPragma = (LinkerOptionsPragma)theEObject;
				T result = caseLinkerOptionsPragma(linkerOptionsPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.LIST_PRAGMA: {
				ListPragma listPragma = (ListPragma)theEObject;
				T result = caseListPragma(listPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.LOCKING_POLICY_PRAGMA: {
				LockingPolicyPragma lockingPolicyPragma = (LockingPolicyPragma)theEObject;
				T result = caseLockingPolicyPragma(lockingPolicyPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.LOOP_PARAMETER_SPECIFICATION: {
				LoopParameterSpecification loopParameterSpecification = (LoopParameterSpecification)theEObject;
				T result = caseLoopParameterSpecification(loopParameterSpecification);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.LOOP_STATEMENT: {
				LoopStatement loopStatement = (LoopStatement)theEObject;
				T result = caseLoopStatement(loopStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.MACHINE_ATTRIBUTE: {
				MachineAttribute machineAttribute = (MachineAttribute)theEObject;
				T result = caseMachineAttribute(machineAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.MACHINE_EMAX_ATTRIBUTE: {
				MachineEmaxAttribute machineEmaxAttribute = (MachineEmaxAttribute)theEObject;
				T result = caseMachineEmaxAttribute(machineEmaxAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.MACHINE_EMIN_ATTRIBUTE: {
				MachineEminAttribute machineEminAttribute = (MachineEminAttribute)theEObject;
				T result = caseMachineEminAttribute(machineEminAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.MACHINE_MANTISSA_ATTRIBUTE: {
				MachineMantissaAttribute machineMantissaAttribute = (MachineMantissaAttribute)theEObject;
				T result = caseMachineMantissaAttribute(machineMantissaAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.MACHINE_OVERFLOWS_ATTRIBUTE: {
				MachineOverflowsAttribute machineOverflowsAttribute = (MachineOverflowsAttribute)theEObject;
				T result = caseMachineOverflowsAttribute(machineOverflowsAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.MACHINE_RADIX_ATTRIBUTE: {
				MachineRadixAttribute machineRadixAttribute = (MachineRadixAttribute)theEObject;
				T result = caseMachineRadixAttribute(machineRadixAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.MACHINE_ROUNDING_ATTRIBUTE: {
				MachineRoundingAttribute machineRoundingAttribute = (MachineRoundingAttribute)theEObject;
				T result = caseMachineRoundingAttribute(machineRoundingAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.MACHINE_ROUNDS_ATTRIBUTE: {
				MachineRoundsAttribute machineRoundsAttribute = (MachineRoundsAttribute)theEObject;
				T result = caseMachineRoundsAttribute(machineRoundsAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE: {
				MaxAlignmentForAllocationAttribute maxAlignmentForAllocationAttribute = (MaxAlignmentForAllocationAttribute)theEObject;
				T result = caseMaxAlignmentForAllocationAttribute(maxAlignmentForAllocationAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.MAX_ATTRIBUTE: {
				MaxAttribute maxAttribute = (MaxAttribute)theEObject;
				T result = caseMaxAttribute(maxAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE: {
				MaxSizeInStorageElementsAttribute maxSizeInStorageElementsAttribute = (MaxSizeInStorageElementsAttribute)theEObject;
				T result = caseMaxSizeInStorageElementsAttribute(maxSizeInStorageElementsAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.MIN_ATTRIBUTE: {
				MinAttribute minAttribute = (MinAttribute)theEObject;
				T result = caseMinAttribute(minAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.MINUS_OPERATOR: {
				MinusOperator minusOperator = (MinusOperator)theEObject;
				T result = caseMinusOperator(minusOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.MOD_ATTRIBUTE: {
				ModAttribute modAttribute = (ModAttribute)theEObject;
				T result = caseModAttribute(modAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.MODEL_ATTRIBUTE: {
				ModelAttribute modelAttribute = (ModelAttribute)theEObject;
				T result = caseModelAttribute(modelAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.MODEL_EMIN_ATTRIBUTE: {
				ModelEminAttribute modelEminAttribute = (ModelEminAttribute)theEObject;
				T result = caseModelEminAttribute(modelEminAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.MODEL_EPSILON_ATTRIBUTE: {
				ModelEpsilonAttribute modelEpsilonAttribute = (ModelEpsilonAttribute)theEObject;
				T result = caseModelEpsilonAttribute(modelEpsilonAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.MODEL_MANTISSA_ATTRIBUTE: {
				ModelMantissaAttribute modelMantissaAttribute = (ModelMantissaAttribute)theEObject;
				T result = caseModelMantissaAttribute(modelMantissaAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.MODEL_SMALL_ATTRIBUTE: {
				ModelSmallAttribute modelSmallAttribute = (ModelSmallAttribute)theEObject;
				T result = caseModelSmallAttribute(modelSmallAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.MOD_OPERATOR: {
				ModOperator modOperator = (ModOperator)theEObject;
				T result = caseModOperator(modOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.MODULAR_TYPE_DEFINITION: {
				ModularTypeDefinition modularTypeDefinition = (ModularTypeDefinition)theEObject;
				T result = caseModularTypeDefinition(modularTypeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.MODULUS_ATTRIBUTE: {
				ModulusAttribute modulusAttribute = (ModulusAttribute)theEObject;
				T result = caseModulusAttribute(modulusAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.MULTIPLY_OPERATOR: {
				MultiplyOperator multiplyOperator = (MultiplyOperator)theEObject;
				T result = caseMultiplyOperator(multiplyOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.NAME_CLASS: {
				NameClass nameClass = (NameClass)theEObject;
				T result = caseNameClass(nameClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.NAMED_ARRAY_AGGREGATE: {
				NamedArrayAggregate namedArrayAggregate = (NamedArrayAggregate)theEObject;
				T result = caseNamedArrayAggregate(namedArrayAggregate);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.NAME_LIST: {
				NameList nameList = (NameList)theEObject;
				T result = caseNameList(nameList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.NO_RETURN_PRAGMA: {
				NoReturnPragma noReturnPragma = (NoReturnPragma)theEObject;
				T result = caseNoReturnPragma(noReturnPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.NORMALIZE_SCALARS_PRAGMA: {
				NormalizeScalarsPragma normalizeScalarsPragma = (NormalizeScalarsPragma)theEObject;
				T result = caseNormalizeScalarsPragma(normalizeScalarsPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.NOT_AN_ELEMENT: {
				NotAnElement notAnElement = (NotAnElement)theEObject;
				T result = caseNotAnElement(notAnElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.NOT_EQUAL_OPERATOR: {
				NotEqualOperator notEqualOperator = (NotEqualOperator)theEObject;
				T result = caseNotEqualOperator(notEqualOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.NOT_IN_MEMBERSHIP_TEST: {
				NotInMembershipTest notInMembershipTest = (NotInMembershipTest)theEObject;
				T result = caseNotInMembershipTest(notInMembershipTest);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.NOT_NULL_RETURN: {
				NotNullReturn notNullReturn = (NotNullReturn)theEObject;
				T result = caseNotNullReturn(notNullReturn);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.NOT_OPERATOR: {
				NotOperator notOperator = (NotOperator)theEObject;
				T result = caseNotOperator(notOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.NOT_OVERRIDING: {
				NotOverriding notOverriding = (NotOverriding)theEObject;
				T result = caseNotOverriding(notOverriding);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.NULL_COMPONENT: {
				NullComponent nullComponent = (NullComponent)theEObject;
				T result = caseNullComponent(nullComponent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.NULL_EXCLUSION: {
				NullExclusion nullExclusion = (NullExclusion)theEObject;
				T result = caseNullExclusion(nullExclusion);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.NULL_LITERAL: {
				NullLiteral nullLiteral = (NullLiteral)theEObject;
				T result = caseNullLiteral(nullLiteral);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.NULL_PROCEDURE_DECLARATION: {
				NullProcedureDeclaration nullProcedureDeclaration = (NullProcedureDeclaration)theEObject;
				T result = caseNullProcedureDeclaration(nullProcedureDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.NULL_RECORD_DEFINITION: {
				NullRecordDefinition nullRecordDefinition = (NullRecordDefinition)theEObject;
				T result = caseNullRecordDefinition(nullRecordDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.NULL_STATEMENT: {
				NullStatement nullStatement = (NullStatement)theEObject;
				T result = caseNullStatement(nullStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.OBJECT_RENAMING_DECLARATION: {
				ObjectRenamingDeclaration objectRenamingDeclaration = (ObjectRenamingDeclaration)theEObject;
				T result = caseObjectRenamingDeclaration(objectRenamingDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.OPTIMIZE_PRAGMA: {
				OptimizePragma optimizePragma = (OptimizePragma)theEObject;
				T result = caseOptimizePragma(optimizePragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ORDINARY_FIXED_POINT_DEFINITION: {
				OrdinaryFixedPointDefinition ordinaryFixedPointDefinition = (OrdinaryFixedPointDefinition)theEObject;
				T result = caseOrdinaryFixedPointDefinition(ordinaryFixedPointDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ORDINARY_INTERFACE: {
				OrdinaryInterface ordinaryInterface = (OrdinaryInterface)theEObject;
				T result = caseOrdinaryInterface(ordinaryInterface);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ORDINARY_TYPE_DECLARATION: {
				OrdinaryTypeDeclaration ordinaryTypeDeclaration = (OrdinaryTypeDeclaration)theEObject;
				T result = caseOrdinaryTypeDeclaration(ordinaryTypeDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.OR_ELSE_SHORT_CIRCUIT: {
				OrElseShortCircuit orElseShortCircuit = (OrElseShortCircuit)theEObject;
				T result = caseOrElseShortCircuit(orElseShortCircuit);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.OR_OPERATOR: {
				OrOperator orOperator = (OrOperator)theEObject;
				T result = caseOrOperator(orOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.OR_PATH: {
				OrPath orPath = (OrPath)theEObject;
				T result = caseOrPath(orPath);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.OTHERS_CHOICE: {
				OthersChoice othersChoice = (OthersChoice)theEObject;
				T result = caseOthersChoice(othersChoice);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.OUTPUT_ATTRIBUTE: {
				OutputAttribute outputAttribute = (OutputAttribute)theEObject;
				T result = caseOutputAttribute(outputAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.OVERLAPS_STORAGE_ATTRIBUTE: {
				OverlapsStorageAttribute overlapsStorageAttribute = (OverlapsStorageAttribute)theEObject;
				T result = caseOverlapsStorageAttribute(overlapsStorageAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.OVERRIDING: {
				Overriding overriding = (Overriding)theEObject;
				T result = caseOverriding(overriding);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PACKAGE_BODY_DECLARATION: {
				PackageBodyDeclaration packageBodyDeclaration = (PackageBodyDeclaration)theEObject;
				T result = casePackageBodyDeclaration(packageBodyDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PACKAGE_BODY_STUB: {
				PackageBodyStub packageBodyStub = (PackageBodyStub)theEObject;
				T result = casePackageBodyStub(packageBodyStub);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PACKAGE_DECLARATION: {
				PackageDeclaration packageDeclaration = (PackageDeclaration)theEObject;
				T result = casePackageDeclaration(packageDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PACKAGE_INSTANTIATION: {
				PackageInstantiation packageInstantiation = (PackageInstantiation)theEObject;
				T result = casePackageInstantiation(packageInstantiation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PACKAGE_RENAMING_DECLARATION: {
				PackageRenamingDeclaration packageRenamingDeclaration = (PackageRenamingDeclaration)theEObject;
				T result = casePackageRenamingDeclaration(packageRenamingDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PACK_PRAGMA: {
				PackPragma packPragma = (PackPragma)theEObject;
				T result = casePackPragma(packPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PAGE_PRAGMA: {
				PagePragma pagePragma = (PagePragma)theEObject;
				T result = casePagePragma(pagePragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PARAMETER_ASSOCIATION: {
				ParameterAssociation parameterAssociation = (ParameterAssociation)theEObject;
				T result = caseParameterAssociation(parameterAssociation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PARAMETER_SPECIFICATION: {
				ParameterSpecification parameterSpecification = (ParameterSpecification)theEObject;
				T result = caseParameterSpecification(parameterSpecification);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PARAMETER_SPECIFICATION_LIST: {
				ParameterSpecificationList parameterSpecificationList = (ParameterSpecificationList)theEObject;
				T result = caseParameterSpecificationList(parameterSpecificationList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PARENTHESIZED_EXPRESSION: {
				ParenthesizedExpression parenthesizedExpression = (ParenthesizedExpression)theEObject;
				T result = caseParenthesizedExpression(parenthesizedExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PARTITION_ELABORATION_POLICY_PRAGMA: {
				PartitionElaborationPolicyPragma partitionElaborationPolicyPragma = (PartitionElaborationPolicyPragma)theEObject;
				T result = casePartitionElaborationPolicyPragma(partitionElaborationPolicyPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PARTITION_ID_ATTRIBUTE: {
				PartitionIdAttribute partitionIdAttribute = (PartitionIdAttribute)theEObject;
				T result = casePartitionIdAttribute(partitionIdAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PATH_CLASS: {
				PathClass pathClass = (PathClass)theEObject;
				T result = casePathClass(pathClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PATH_LIST: {
				PathList pathList = (PathList)theEObject;
				T result = casePathList(pathList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PLUS_OPERATOR: {
				PlusOperator plusOperator = (PlusOperator)theEObject;
				T result = casePlusOperator(plusOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.POOL_SPECIFIC_ACCESS_TO_VARIABLE: {
				PoolSpecificAccessToVariable poolSpecificAccessToVariable = (PoolSpecificAccessToVariable)theEObject;
				T result = casePoolSpecificAccessToVariable(poolSpecificAccessToVariable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.POS_ATTRIBUTE: {
				PosAttribute posAttribute = (PosAttribute)theEObject;
				T result = casePosAttribute(posAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.POSITIONAL_ARRAY_AGGREGATE: {
				PositionalArrayAggregate positionalArrayAggregate = (PositionalArrayAggregate)theEObject;
				T result = casePositionalArrayAggregate(positionalArrayAggregate);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.POSITION_ATTRIBUTE: {
				PositionAttribute positionAttribute = (PositionAttribute)theEObject;
				T result = casePositionAttribute(positionAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION: {
				PragmaArgumentAssociation pragmaArgumentAssociation = (PragmaArgumentAssociation)theEObject;
				T result = casePragmaArgumentAssociation(pragmaArgumentAssociation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PRAGMA_ELEMENT_CLASS: {
				PragmaElementClass pragmaElementClass = (PragmaElementClass)theEObject;
				T result = casePragmaElementClass(pragmaElementClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PRED_ATTRIBUTE: {
				PredAttribute predAttribute = (PredAttribute)theEObject;
				T result = casePredAttribute(predAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PREELABORABLE_INITIALIZATION_PRAGMA: {
				PreelaborableInitializationPragma preelaborableInitializationPragma = (PreelaborableInitializationPragma)theEObject;
				T result = casePreelaborableInitializationPragma(preelaborableInitializationPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PREELABORATE_PRAGMA: {
				PreelaboratePragma preelaboratePragma = (PreelaboratePragma)theEObject;
				T result = casePreelaboratePragma(preelaboratePragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PRIORITY_ATTRIBUTE: {
				PriorityAttribute priorityAttribute = (PriorityAttribute)theEObject;
				T result = casePriorityAttribute(priorityAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PRIORITY_PRAGMA: {
				PriorityPragma priorityPragma = (PriorityPragma)theEObject;
				T result = casePriorityPragma(priorityPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PRIORITY_SPECIFIC_DISPATCHING_PRAGMA: {
				PrioritySpecificDispatchingPragma prioritySpecificDispatchingPragma = (PrioritySpecificDispatchingPragma)theEObject;
				T result = casePrioritySpecificDispatchingPragma(prioritySpecificDispatchingPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PRIVATE: {
				Private private_ = (Private)theEObject;
				T result = casePrivate(private_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION: {
				PrivateExtensionDeclaration privateExtensionDeclaration = (PrivateExtensionDeclaration)theEObject;
				T result = casePrivateExtensionDeclaration(privateExtensionDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION: {
				PrivateExtensionDefinition privateExtensionDefinition = (PrivateExtensionDefinition)theEObject;
				T result = casePrivateExtensionDefinition(privateExtensionDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PRIVATE_TYPE_DECLARATION: {
				PrivateTypeDeclaration privateTypeDeclaration = (PrivateTypeDeclaration)theEObject;
				T result = casePrivateTypeDeclaration(privateTypeDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PRIVATE_TYPE_DEFINITION: {
				PrivateTypeDefinition privateTypeDefinition = (PrivateTypeDefinition)theEObject;
				T result = casePrivateTypeDefinition(privateTypeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PROCEDURE_BODY_DECLARATION: {
				ProcedureBodyDeclaration procedureBodyDeclaration = (ProcedureBodyDeclaration)theEObject;
				T result = caseProcedureBodyDeclaration(procedureBodyDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PROCEDURE_BODY_STUB: {
				ProcedureBodyStub procedureBodyStub = (ProcedureBodyStub)theEObject;
				T result = caseProcedureBodyStub(procedureBodyStub);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PROCEDURE_CALL_STATEMENT: {
				ProcedureCallStatement procedureCallStatement = (ProcedureCallStatement)theEObject;
				T result = caseProcedureCallStatement(procedureCallStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PROCEDURE_DECLARATION: {
				ProcedureDeclaration procedureDeclaration = (ProcedureDeclaration)theEObject;
				T result = caseProcedureDeclaration(procedureDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PROCEDURE_INSTANTIATION: {
				ProcedureInstantiation procedureInstantiation = (ProcedureInstantiation)theEObject;
				T result = caseProcedureInstantiation(procedureInstantiation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PROCEDURE_RENAMING_DECLARATION: {
				ProcedureRenamingDeclaration procedureRenamingDeclaration = (ProcedureRenamingDeclaration)theEObject;
				T result = caseProcedureRenamingDeclaration(procedureRenamingDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PROFILE_PRAGMA: {
				ProfilePragma profilePragma = (ProfilePragma)theEObject;
				T result = caseProfilePragma(profilePragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PROTECTED_BODY_DECLARATION: {
				ProtectedBodyDeclaration protectedBodyDeclaration = (ProtectedBodyDeclaration)theEObject;
				T result = caseProtectedBodyDeclaration(protectedBodyDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PROTECTED_BODY_STUB: {
				ProtectedBodyStub protectedBodyStub = (ProtectedBodyStub)theEObject;
				T result = caseProtectedBodyStub(protectedBodyStub);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PROTECTED_DEFINITION: {
				ProtectedDefinition protectedDefinition = (ProtectedDefinition)theEObject;
				T result = caseProtectedDefinition(protectedDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PROTECTED_INTERFACE: {
				ProtectedInterface protectedInterface = (ProtectedInterface)theEObject;
				T result = caseProtectedInterface(protectedInterface);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PROTECTED_TYPE_DECLARATION: {
				ProtectedTypeDeclaration protectedTypeDeclaration = (ProtectedTypeDeclaration)theEObject;
				T result = caseProtectedTypeDeclaration(protectedTypeDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.PURE_PRAGMA: {
				PurePragma purePragma = (PurePragma)theEObject;
				T result = casePurePragma(purePragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.QUALIFIED_EXPRESSION: {
				QualifiedExpression qualifiedExpression = (QualifiedExpression)theEObject;
				T result = caseQualifiedExpression(qualifiedExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.QUEUING_POLICY_PRAGMA: {
				QueuingPolicyPragma queuingPolicyPragma = (QueuingPolicyPragma)theEObject;
				T result = caseQueuingPolicyPragma(queuingPolicyPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.RAISE_EXPRESSION: {
				RaiseExpression raiseExpression = (RaiseExpression)theEObject;
				T result = caseRaiseExpression(raiseExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.RAISE_STATEMENT: {
				RaiseStatement raiseStatement = (RaiseStatement)theEObject;
				T result = caseRaiseStatement(raiseStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.RANGE_ATTRIBUTE: {
				RangeAttribute rangeAttribute = (RangeAttribute)theEObject;
				T result = caseRangeAttribute(rangeAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.RANGE_ATTRIBUTE_REFERENCE: {
				RangeAttributeReference rangeAttributeReference = (RangeAttributeReference)theEObject;
				T result = caseRangeAttributeReference(rangeAttributeReference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.RANGE_CONSTRAINT_CLASS: {
				RangeConstraintClass rangeConstraintClass = (RangeConstraintClass)theEObject;
				T result = caseRangeConstraintClass(rangeConstraintClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.READ_ATTRIBUTE: {
				ReadAttribute readAttribute = (ReadAttribute)theEObject;
				T result = caseReadAttribute(readAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.REAL_LITERAL: {
				RealLiteral realLiteral = (RealLiteral)theEObject;
				T result = caseRealLiteral(realLiteral);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.REAL_NUMBER_DECLARATION: {
				RealNumberDeclaration realNumberDeclaration = (RealNumberDeclaration)theEObject;
				T result = caseRealNumberDeclaration(realNumberDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.RECORD_AGGREGATE: {
				RecordAggregate recordAggregate = (RecordAggregate)theEObject;
				T result = caseRecordAggregate(recordAggregate);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.RECORD_COMPONENT_ASSOCIATION: {
				RecordComponentAssociation recordComponentAssociation = (RecordComponentAssociation)theEObject;
				T result = caseRecordComponentAssociation(recordComponentAssociation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.RECORD_COMPONENT_CLASS: {
				RecordComponentClass recordComponentClass = (RecordComponentClass)theEObject;
				T result = caseRecordComponentClass(recordComponentClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.RECORD_COMPONENT_LIST: {
				RecordComponentList recordComponentList = (RecordComponentList)theEObject;
				T result = caseRecordComponentList(recordComponentList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.RECORD_DEFINITION: {
				RecordDefinition recordDefinition = (RecordDefinition)theEObject;
				T result = caseRecordDefinition(recordDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.RECORD_REPRESENTATION_CLAUSE: {
				RecordRepresentationClause recordRepresentationClause = (RecordRepresentationClause)theEObject;
				T result = caseRecordRepresentationClause(recordRepresentationClause);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.RECORD_TYPE_DEFINITION: {
				RecordTypeDefinition recordTypeDefinition = (RecordTypeDefinition)theEObject;
				T result = caseRecordTypeDefinition(recordTypeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.RELATIVE_DEADLINE_PRAGMA: {
				RelativeDeadlinePragma relativeDeadlinePragma = (RelativeDeadlinePragma)theEObject;
				T result = caseRelativeDeadlinePragma(relativeDeadlinePragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.REMAINDER_ATTRIBUTE: {
				RemainderAttribute remainderAttribute = (RemainderAttribute)theEObject;
				T result = caseRemainderAttribute(remainderAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.REM_OPERATOR: {
				RemOperator remOperator = (RemOperator)theEObject;
				T result = caseRemOperator(remOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA: {
				RemoteCallInterfacePragma remoteCallInterfacePragma = (RemoteCallInterfacePragma)theEObject;
				T result = caseRemoteCallInterfacePragma(remoteCallInterfacePragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.REMOTE_TYPES_PRAGMA: {
				RemoteTypesPragma remoteTypesPragma = (RemoteTypesPragma)theEObject;
				T result = caseRemoteTypesPragma(remoteTypesPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.REQUEUE_STATEMENT: {
				RequeueStatement requeueStatement = (RequeueStatement)theEObject;
				T result = caseRequeueStatement(requeueStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.REQUEUE_STATEMENT_WITH_ABORT: {
				RequeueStatementWithAbort requeueStatementWithAbort = (RequeueStatementWithAbort)theEObject;
				T result = caseRequeueStatementWithAbort(requeueStatementWithAbort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.RESTRICTIONS_PRAGMA: {
				RestrictionsPragma restrictionsPragma = (RestrictionsPragma)theEObject;
				T result = caseRestrictionsPragma(restrictionsPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.RETURN_CONSTANT_SPECIFICATION: {
				ReturnConstantSpecification returnConstantSpecification = (ReturnConstantSpecification)theEObject;
				T result = caseReturnConstantSpecification(returnConstantSpecification);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.RETURN_STATEMENT: {
				ReturnStatement returnStatement = (ReturnStatement)theEObject;
				T result = caseReturnStatement(returnStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.RETURN_VARIABLE_SPECIFICATION: {
				ReturnVariableSpecification returnVariableSpecification = (ReturnVariableSpecification)theEObject;
				T result = caseReturnVariableSpecification(returnVariableSpecification);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.REVERSE: {
				Reverse reverse = (Reverse)theEObject;
				T result = caseReverse(reverse);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.REVIEWABLE_PRAGMA: {
				ReviewablePragma reviewablePragma = (ReviewablePragma)theEObject;
				T result = caseReviewablePragma(reviewablePragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ROOT_INTEGER_DEFINITION: {
				RootIntegerDefinition rootIntegerDefinition = (RootIntegerDefinition)theEObject;
				T result = caseRootIntegerDefinition(rootIntegerDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ROOT_REAL_DEFINITION: {
				RootRealDefinition rootRealDefinition = (RootRealDefinition)theEObject;
				T result = caseRootRealDefinition(rootRealDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ROUND_ATTRIBUTE: {
				RoundAttribute roundAttribute = (RoundAttribute)theEObject;
				T result = caseRoundAttribute(roundAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.ROUNDING_ATTRIBUTE: {
				RoundingAttribute roundingAttribute = (RoundingAttribute)theEObject;
				T result = caseRoundingAttribute(roundingAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.SAFE_FIRST_ATTRIBUTE: {
				SafeFirstAttribute safeFirstAttribute = (SafeFirstAttribute)theEObject;
				T result = caseSafeFirstAttribute(safeFirstAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.SAFE_LAST_ATTRIBUTE: {
				SafeLastAttribute safeLastAttribute = (SafeLastAttribute)theEObject;
				T result = caseSafeLastAttribute(safeLastAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.SCALE_ATTRIBUTE: {
				ScaleAttribute scaleAttribute = (ScaleAttribute)theEObject;
				T result = caseScaleAttribute(scaleAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.SCALING_ATTRIBUTE: {
				ScalingAttribute scalingAttribute = (ScalingAttribute)theEObject;
				T result = caseScalingAttribute(scalingAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.SELECTED_COMPONENT: {
				SelectedComponent selectedComponent = (SelectedComponent)theEObject;
				T result = caseSelectedComponent(selectedComponent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.SELECTIVE_ACCEPT_STATEMENT: {
				SelectiveAcceptStatement selectiveAcceptStatement = (SelectiveAcceptStatement)theEObject;
				T result = caseSelectiveAcceptStatement(selectiveAcceptStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.SELECT_PATH: {
				SelectPath selectPath = (SelectPath)theEObject;
				T result = caseSelectPath(selectPath);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.SHARED_PASSIVE_PRAGMA: {
				SharedPassivePragma sharedPassivePragma = (SharedPassivePragma)theEObject;
				T result = caseSharedPassivePragma(sharedPassivePragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.SIGNED_INTEGER_TYPE_DEFINITION: {
				SignedIntegerTypeDefinition signedIntegerTypeDefinition = (SignedIntegerTypeDefinition)theEObject;
				T result = caseSignedIntegerTypeDefinition(signedIntegerTypeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.SIGNED_ZEROS_ATTRIBUTE: {
				SignedZerosAttribute signedZerosAttribute = (SignedZerosAttribute)theEObject;
				T result = caseSignedZerosAttribute(signedZerosAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.SIMPLE_EXPRESSION_RANGE: {
				SimpleExpressionRange simpleExpressionRange = (SimpleExpressionRange)theEObject;
				T result = caseSimpleExpressionRange(simpleExpressionRange);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.SINGLE_PROTECTED_DECLARATION: {
				SingleProtectedDeclaration singleProtectedDeclaration = (SingleProtectedDeclaration)theEObject;
				T result = caseSingleProtectedDeclaration(singleProtectedDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.SINGLE_TASK_DECLARATION: {
				SingleTaskDeclaration singleTaskDeclaration = (SingleTaskDeclaration)theEObject;
				T result = caseSingleTaskDeclaration(singleTaskDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.SIZE_ATTRIBUTE: {
				SizeAttribute sizeAttribute = (SizeAttribute)theEObject;
				T result = caseSizeAttribute(sizeAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.SLICE: {
				Slice slice = (Slice)theEObject;
				T result = caseSlice(slice);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.SMALL_ATTRIBUTE: {
				SmallAttribute smallAttribute = (SmallAttribute)theEObject;
				T result = caseSmallAttribute(smallAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.SOURCE_LOCATION: {
				SourceLocation sourceLocation = (SourceLocation)theEObject;
				T result = caseSourceLocation(sourceLocation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.STATEMENT_CLASS: {
				StatementClass statementClass = (StatementClass)theEObject;
				T result = caseStatementClass(statementClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.STATEMENT_LIST: {
				StatementList statementList = (StatementList)theEObject;
				T result = caseStatementList(statementList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.STORAGE_POOL_ATTRIBUTE: {
				StoragePoolAttribute storagePoolAttribute = (StoragePoolAttribute)theEObject;
				T result = caseStoragePoolAttribute(storagePoolAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.STORAGE_SIZE_ATTRIBUTE: {
				StorageSizeAttribute storageSizeAttribute = (StorageSizeAttribute)theEObject;
				T result = caseStorageSizeAttribute(storageSizeAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.STORAGE_SIZE_PRAGMA: {
				StorageSizePragma storageSizePragma = (StorageSizePragma)theEObject;
				T result = caseStorageSizePragma(storageSizePragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.STREAM_SIZE_ATTRIBUTE: {
				StreamSizeAttribute streamSizeAttribute = (StreamSizeAttribute)theEObject;
				T result = caseStreamSizeAttribute(streamSizeAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.STRING_LITERAL: {
				StringLiteral stringLiteral = (StringLiteral)theEObject;
				T result = caseStringLiteral(stringLiteral);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.SUBTYPE_DECLARATION: {
				SubtypeDeclaration subtypeDeclaration = (SubtypeDeclaration)theEObject;
				T result = caseSubtypeDeclaration(subtypeDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.SUBTYPE_INDICATION: {
				SubtypeIndication subtypeIndication = (SubtypeIndication)theEObject;
				T result = caseSubtypeIndication(subtypeIndication);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.SUCC_ATTRIBUTE: {
				SuccAttribute succAttribute = (SuccAttribute)theEObject;
				T result = caseSuccAttribute(succAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.SUPPRESS_PRAGMA: {
				SuppressPragma suppressPragma = (SuppressPragma)theEObject;
				T result = caseSuppressPragma(suppressPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.SYNCHRONIZED: {
				Synchronized synchronized_ = (Synchronized)theEObject;
				T result = caseSynchronized(synchronized_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.SYNCHRONIZED_INTERFACE: {
				SynchronizedInterface synchronizedInterface = (SynchronizedInterface)theEObject;
				T result = caseSynchronizedInterface(synchronizedInterface);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.TAG_ATTRIBUTE: {
				TagAttribute tagAttribute = (TagAttribute)theEObject;
				T result = caseTagAttribute(tagAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.TAGGED: {
				Tagged tagged = (Tagged)theEObject;
				T result = caseTagged(tagged);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.TAGGED_INCOMPLETE_TYPE_DECLARATION: {
				TaggedIncompleteTypeDeclaration taggedIncompleteTypeDeclaration = (TaggedIncompleteTypeDeclaration)theEObject;
				T result = caseTaggedIncompleteTypeDeclaration(taggedIncompleteTypeDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.TAGGED_PRIVATE_TYPE_DEFINITION: {
				TaggedPrivateTypeDefinition taggedPrivateTypeDefinition = (TaggedPrivateTypeDefinition)theEObject;
				T result = caseTaggedPrivateTypeDefinition(taggedPrivateTypeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.TAGGED_RECORD_TYPE_DEFINITION: {
				TaggedRecordTypeDefinition taggedRecordTypeDefinition = (TaggedRecordTypeDefinition)theEObject;
				T result = caseTaggedRecordTypeDefinition(taggedRecordTypeDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.TASK_BODY_DECLARATION: {
				TaskBodyDeclaration taskBodyDeclaration = (TaskBodyDeclaration)theEObject;
				T result = caseTaskBodyDeclaration(taskBodyDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.TASK_BODY_STUB: {
				TaskBodyStub taskBodyStub = (TaskBodyStub)theEObject;
				T result = caseTaskBodyStub(taskBodyStub);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.TASK_DEFINITION: {
				TaskDefinition taskDefinition = (TaskDefinition)theEObject;
				T result = caseTaskDefinition(taskDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.TASK_DISPATCHING_POLICY_PRAGMA: {
				TaskDispatchingPolicyPragma taskDispatchingPolicyPragma = (TaskDispatchingPolicyPragma)theEObject;
				T result = caseTaskDispatchingPolicyPragma(taskDispatchingPolicyPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.TASK_INTERFACE: {
				TaskInterface taskInterface = (TaskInterface)theEObject;
				T result = caseTaskInterface(taskInterface);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.TASK_TYPE_DECLARATION: {
				TaskTypeDeclaration taskTypeDeclaration = (TaskTypeDeclaration)theEObject;
				T result = caseTaskTypeDeclaration(taskTypeDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.TERMINATE_ALTERNATIVE_STATEMENT: {
				TerminateAlternativeStatement terminateAlternativeStatement = (TerminateAlternativeStatement)theEObject;
				T result = caseTerminateAlternativeStatement(terminateAlternativeStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.TERMINATED_ATTRIBUTE: {
				TerminatedAttribute terminatedAttribute = (TerminatedAttribute)theEObject;
				T result = caseTerminatedAttribute(terminatedAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.THEN_ABORT_PATH: {
				ThenAbortPath thenAbortPath = (ThenAbortPath)theEObject;
				T result = caseThenAbortPath(thenAbortPath);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.TIMED_ENTRY_CALL_STATEMENT: {
				TimedEntryCallStatement timedEntryCallStatement = (TimedEntryCallStatement)theEObject;
				T result = caseTimedEntryCallStatement(timedEntryCallStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.TRUNCATION_ATTRIBUTE: {
				TruncationAttribute truncationAttribute = (TruncationAttribute)theEObject;
				T result = caseTruncationAttribute(truncationAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.TYPE_CONVERSION: {
				TypeConversion typeConversion = (TypeConversion)theEObject;
				T result = caseTypeConversion(typeConversion);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.UNARY_MINUS_OPERATOR: {
				UnaryMinusOperator unaryMinusOperator = (UnaryMinusOperator)theEObject;
				T result = caseUnaryMinusOperator(unaryMinusOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.UNARY_PLUS_OPERATOR: {
				UnaryPlusOperator unaryPlusOperator = (UnaryPlusOperator)theEObject;
				T result = caseUnaryPlusOperator(unaryPlusOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.UNBIASED_ROUNDING_ATTRIBUTE: {
				UnbiasedRoundingAttribute unbiasedRoundingAttribute = (UnbiasedRoundingAttribute)theEObject;
				T result = caseUnbiasedRoundingAttribute(unbiasedRoundingAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.UNCHECKED_ACCESS_ATTRIBUTE: {
				UncheckedAccessAttribute uncheckedAccessAttribute = (UncheckedAccessAttribute)theEObject;
				T result = caseUncheckedAccessAttribute(uncheckedAccessAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.UNCHECKED_UNION_PRAGMA: {
				UncheckedUnionPragma uncheckedUnionPragma = (UncheckedUnionPragma)theEObject;
				T result = caseUncheckedUnionPragma(uncheckedUnionPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION: {
				UnconstrainedArrayDefinition unconstrainedArrayDefinition = (UnconstrainedArrayDefinition)theEObject;
				T result = caseUnconstrainedArrayDefinition(unconstrainedArrayDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.UNIVERSAL_FIXED_DEFINITION: {
				UniversalFixedDefinition universalFixedDefinition = (UniversalFixedDefinition)theEObject;
				T result = caseUniversalFixedDefinition(universalFixedDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.UNIVERSAL_INTEGER_DEFINITION: {
				UniversalIntegerDefinition universalIntegerDefinition = (UniversalIntegerDefinition)theEObject;
				T result = caseUniversalIntegerDefinition(universalIntegerDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.UNIVERSAL_REAL_DEFINITION: {
				UniversalRealDefinition universalRealDefinition = (UniversalRealDefinition)theEObject;
				T result = caseUniversalRealDefinition(universalRealDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.UNKNOWN_ATTRIBUTE: {
				UnknownAttribute unknownAttribute = (UnknownAttribute)theEObject;
				T result = caseUnknownAttribute(unknownAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.UNKNOWN_DISCRIMINANT_PART: {
				UnknownDiscriminantPart unknownDiscriminantPart = (UnknownDiscriminantPart)theEObject;
				T result = caseUnknownDiscriminantPart(unknownDiscriminantPart);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.UNKNOWN_PRAGMA: {
				UnknownPragma unknownPragma = (UnknownPragma)theEObject;
				T result = caseUnknownPragma(unknownPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.UNSUPPRESS_PRAGMA: {
				UnsuppressPragma unsuppressPragma = (UnsuppressPragma)theEObject;
				T result = caseUnsuppressPragma(unsuppressPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.USE_ALL_TYPE_CLAUSE: {
				UseAllTypeClause useAllTypeClause = (UseAllTypeClause)theEObject;
				T result = caseUseAllTypeClause(useAllTypeClause);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.USE_PACKAGE_CLAUSE: {
				UsePackageClause usePackageClause = (UsePackageClause)theEObject;
				T result = caseUsePackageClause(usePackageClause);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.USE_TYPE_CLAUSE: {
				UseTypeClause useTypeClause = (UseTypeClause)theEObject;
				T result = caseUseTypeClause(useTypeClause);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.VAL_ATTRIBUTE: {
				ValAttribute valAttribute = (ValAttribute)theEObject;
				T result = caseValAttribute(valAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.VALID_ATTRIBUTE: {
				ValidAttribute validAttribute = (ValidAttribute)theEObject;
				T result = caseValidAttribute(validAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.VALUE_ATTRIBUTE: {
				ValueAttribute valueAttribute = (ValueAttribute)theEObject;
				T result = caseValueAttribute(valueAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.VARIABLE_DECLARATION: {
				VariableDeclaration variableDeclaration = (VariableDeclaration)theEObject;
				T result = caseVariableDeclaration(variableDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.VARIANT: {
				Variant variant = (Variant)theEObject;
				T result = caseVariant(variant);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.VARIANT_LIST: {
				VariantList variantList = (VariantList)theEObject;
				T result = caseVariantList(variantList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.VARIANT_PART: {
				VariantPart variantPart = (VariantPart)theEObject;
				T result = caseVariantPart(variantPart);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.VERSION_ATTRIBUTE: {
				VersionAttribute versionAttribute = (VersionAttribute)theEObject;
				T result = caseVersionAttribute(versionAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.VOLATILE_COMPONENTS_PRAGMA: {
				VolatileComponentsPragma volatileComponentsPragma = (VolatileComponentsPragma)theEObject;
				T result = caseVolatileComponentsPragma(volatileComponentsPragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.VOLATILE_PRAGMA: {
				VolatilePragma volatilePragma = (VolatilePragma)theEObject;
				T result = caseVolatilePragma(volatilePragma);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.WHILE_LOOP_STATEMENT: {
				WhileLoopStatement whileLoopStatement = (WhileLoopStatement)theEObject;
				T result = caseWhileLoopStatement(whileLoopStatement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.WIDE_IMAGE_ATTRIBUTE: {
				WideImageAttribute wideImageAttribute = (WideImageAttribute)theEObject;
				T result = caseWideImageAttribute(wideImageAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.WIDE_VALUE_ATTRIBUTE: {
				WideValueAttribute wideValueAttribute = (WideValueAttribute)theEObject;
				T result = caseWideValueAttribute(wideValueAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.WIDE_WIDE_IMAGE_ATTRIBUTE: {
				WideWideImageAttribute wideWideImageAttribute = (WideWideImageAttribute)theEObject;
				T result = caseWideWideImageAttribute(wideWideImageAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.WIDE_WIDE_VALUE_ATTRIBUTE: {
				WideWideValueAttribute wideWideValueAttribute = (WideWideValueAttribute)theEObject;
				T result = caseWideWideValueAttribute(wideWideValueAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.WIDE_WIDE_WIDTH_ATTRIBUTE: {
				WideWideWidthAttribute wideWideWidthAttribute = (WideWideWidthAttribute)theEObject;
				T result = caseWideWideWidthAttribute(wideWideWidthAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.WIDE_WIDTH_ATTRIBUTE: {
				WideWidthAttribute wideWidthAttribute = (WideWidthAttribute)theEObject;
				T result = caseWideWidthAttribute(wideWidthAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.WIDTH_ATTRIBUTE: {
				WidthAttribute widthAttribute = (WidthAttribute)theEObject;
				T result = caseWidthAttribute(widthAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.WITH_CLAUSE: {
				WithClause withClause = (WithClause)theEObject;
				T result = caseWithClause(withClause);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.WRITE_ATTRIBUTE: {
				WriteAttribute writeAttribute = (WriteAttribute)theEObject;
				T result = caseWriteAttribute(writeAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AdaPackage.XOR_OPERATOR: {
				XorOperator xorOperator = (XorOperator)theEObject;
				T result = caseXorOperator(xorOperator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abort Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abort Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbortStatement(AbortStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abs Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abs Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbsOperator(AbsOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstract(Abstract object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Accept Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Accept Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAcceptStatement(AcceptStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Access Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Access Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAccessAttribute(AccessAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Access To Constant</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Access To Constant</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAccessToConstant(AccessToConstant object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Access To Function</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Access To Function</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAccessToFunction(AccessToFunction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Access To Procedure</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Access To Procedure</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAccessToProcedure(AccessToProcedure object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Access To Protected Function</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Access To Protected Function</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAccessToProtectedFunction(AccessToProtectedFunction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Access To Protected Procedure</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Access To Protected Procedure</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAccessToProtectedProcedure(AccessToProtectedProcedure object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Access To Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Access To Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAccessToVariable(AccessToVariable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Address Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Address Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAddressAttribute(AddressAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Adjacent Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Adjacent Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAdjacentAttribute(AdjacentAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aft Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aft Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAftAttribute(AftAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aliased</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aliased</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAliased(Aliased object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Alignment Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Alignment Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAlignmentAttribute(AlignmentAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>All Calls Remote Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>All Calls Remote Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAllCallsRemotePragma(AllCallsRemotePragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Allocation From Qualified Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Allocation From Qualified Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAllocationFromQualifiedExpression(AllocationFromQualifiedExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Allocation From Subtype</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Allocation From Subtype</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAllocationFromSubtype(AllocationFromSubtype object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>And Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>And Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAndOperator(AndOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>And Then Short Circuit</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>And Then Short Circuit</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAndThenShortCircuit(AndThenShortCircuit object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Anonymous Access To Constant</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Anonymous Access To Constant</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnonymousAccessToConstant(AnonymousAccessToConstant object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Anonymous Access To Function</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Anonymous Access To Function</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnonymousAccessToFunction(AnonymousAccessToFunction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Anonymous Access To Procedure</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Anonymous Access To Procedure</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnonymousAccessToProcedure(AnonymousAccessToProcedure object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Anonymous Access To Protected Function</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Anonymous Access To Protected Function</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnonymousAccessToProtectedFunction(AnonymousAccessToProtectedFunction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Anonymous Access To Protected Procedure</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Anonymous Access To Protected Procedure</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnonymousAccessToProtectedProcedure(AnonymousAccessToProtectedProcedure object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Anonymous Access To Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Anonymous Access To Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnonymousAccessToVariable(AnonymousAccessToVariable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Array Component Association</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Array Component Association</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseArrayComponentAssociation(ArrayComponentAssociation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aspect Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aspect Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAspectSpecification(AspectSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Assertion Policy Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Assertion Policy Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssertionPolicyPragma(AssertionPolicyPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Assert Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Assert Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssertPragma(AssertPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Assignment Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Assignment Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssignmentStatement(AssignmentStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Association Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Association Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssociationClass(AssociationClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Association List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Association List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssociationList(AssociationList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Asynchronous Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Asynchronous Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAsynchronousPragma(AsynchronousPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Asynchronous Select Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Asynchronous Select Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAsynchronousSelectStatement(AsynchronousSelectStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>At Clause</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>At Clause</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAtClause(AtClause object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Atomic Components Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Atomic Components Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAtomicComponentsPragma(AtomicComponentsPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Atomic Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Atomic Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAtomicPragma(AtomicPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attach Handler Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attach Handler Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAttachHandlerPragma(AttachHandlerPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attribute Definition Clause</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attribute Definition Clause</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAttributeDefinitionClause(AttributeDefinitionClause object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Base Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Base Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBaseAttribute(BaseAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Bit Order Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Bit Order Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBitOrderAttribute(BitOrderAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Block Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Block Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlockStatement(BlockStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Body Version Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Body Version Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBodyVersionAttribute(BodyVersionAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Box Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Box Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBoxExpression(BoxExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Callable Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Callable Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCallableAttribute(CallableAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Caller Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Caller Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCallerAttribute(CallerAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Case Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Case Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCaseExpression(CaseExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Case Expression Path</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Case Expression Path</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCaseExpressionPath(CaseExpressionPath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Case Path</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Case Path</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCasePath(CasePath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Case Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Case Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCaseStatement(CaseStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ceiling Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ceiling Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCeilingAttribute(CeilingAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Character Literal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Character Literal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCharacterLiteral(CharacterLiteral object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Choice Parameter Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Choice Parameter Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChoiceParameterSpecification(ChoiceParameterSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Class Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Class Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClassAttribute(ClassAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Code Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Code Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCodeStatement(CodeStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Comment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Comment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComment(Comment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Compilation Unit</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Compilation Unit</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCompilationUnit(CompilationUnit object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Clause</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Clause</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentClause(ComponentClause object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Clause List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Clause List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentClauseList(ComponentClauseList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentDeclaration(ComponentDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentDefinition(ComponentDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Size Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Size Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentSizeAttribute(ComponentSizeAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Compose Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Compose Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComposeAttribute(ComposeAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Concatenate Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Concatenate Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConcatenateOperator(ConcatenateOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Conditional Entry Call Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Conditional Entry Call Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConditionalEntryCallStatement(ConditionalEntryCallStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constant Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constant Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstantDeclaration(ConstantDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constrained Array Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constrained Array Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstrainedArrayDefinition(ConstrainedArrayDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constrained Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constrained Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstrainedAttribute(ConstrainedAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constraint Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constraint Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstraintClass(ConstraintClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Context Clause Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Context Clause Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContextClauseClass(ContextClauseClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Context Clause List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Context Clause List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContextClauseList(ContextClauseList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Controlled Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Controlled Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseControlledPragma(ControlledPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Convention Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Convention Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConventionPragma(ConventionPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Copy Sign Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Copy Sign Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCopySignAttribute(CopySignAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Count Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Count Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCountAttribute(CountAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cpu Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cpu Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCpuPragma(CpuPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Decimal Fixed Point Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Decimal Fixed Point Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDecimalFixedPointDefinition(DecimalFixedPointDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Declaration Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Declaration Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeclarationClass(DeclarationClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Declaration List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Declaration List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeclarationList(DeclarationList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Declarative Item Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Declarative Item Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeclarativeItemClass(DeclarativeItemClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Declarative Item List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Declarative Item List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeclarativeItemList(DeclarativeItemList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Default Storage Pool Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Default Storage Pool Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefaultStoragePoolPragma(DefaultStoragePoolPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deferred Constant Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deferred Constant Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeferredConstantDeclaration(DeferredConstantDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Abs Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Abs Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningAbsOperator(DefiningAbsOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining And Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining And Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningAndOperator(DefiningAndOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Character Literal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Character Literal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningCharacterLiteral(DefiningCharacterLiteral object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Concatenate Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Concatenate Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningConcatenateOperator(DefiningConcatenateOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Divide Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Divide Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningDivideOperator(DefiningDivideOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Enumeration Literal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Enumeration Literal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningEnumerationLiteral(DefiningEnumerationLiteral object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Equal Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Equal Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningEqualOperator(DefiningEqualOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Expanded Name</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Expanded Name</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningExpandedName(DefiningExpandedName object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Exponentiate Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Exponentiate Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningExponentiateOperator(DefiningExponentiateOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Greater Than Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Greater Than Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningGreaterThanOperator(DefiningGreaterThanOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Greater Than Or Equal Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Greater Than Or Equal Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningGreaterThanOrEqualOperator(DefiningGreaterThanOrEqualOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Identifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Identifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningIdentifier(DefiningIdentifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Less Than Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Less Than Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningLessThanOperator(DefiningLessThanOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Less Than Or Equal Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Less Than Or Equal Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningLessThanOrEqualOperator(DefiningLessThanOrEqualOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Minus Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Minus Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningMinusOperator(DefiningMinusOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Mod Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Mod Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningModOperator(DefiningModOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Multiply Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Multiply Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningMultiplyOperator(DefiningMultiplyOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Name Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Name Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningNameClass(DefiningNameClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Name List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Name List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningNameList(DefiningNameList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Not Equal Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Not Equal Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningNotEqualOperator(DefiningNotEqualOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Not Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Not Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningNotOperator(DefiningNotOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Or Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Or Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningOrOperator(DefiningOrOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Plus Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Plus Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningPlusOperator(DefiningPlusOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Rem Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Rem Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningRemOperator(DefiningRemOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Unary Minus Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Unary Minus Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningUnaryMinusOperator(DefiningUnaryMinusOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Unary Plus Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Unary Plus Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningUnaryPlusOperator(DefiningUnaryPlusOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Defining Xor Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Defining Xor Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiningXorOperator(DefiningXorOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Definite Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Definite Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefiniteAttribute(DefiniteAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Definition Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Definition Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefinitionClass(DefinitionClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Definition List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Definition List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDefinitionList(DefinitionList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Delay Relative Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Delay Relative Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDelayRelativeStatement(DelayRelativeStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Delay Until Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Delay Until Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDelayUntilStatement(DelayUntilStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Delta Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Delta Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeltaAttribute(DeltaAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Delta Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Delta Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeltaConstraint(DeltaConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Denorm Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Denorm Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDenormAttribute(DenormAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Derived Record Extension Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Derived Record Extension Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDerivedRecordExtensionDefinition(DerivedRecordExtensionDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Derived Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Derived Type Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDerivedTypeDefinition(DerivedTypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Detect Blocking Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Detect Blocking Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDetectBlockingPragma(DetectBlockingPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Digits Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Digits Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDigitsAttribute(DigitsAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Digits Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Digits Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDigitsConstraint(DigitsConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Discard Names Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Discard Names Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDiscardNamesPragma(DiscardNamesPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Discrete Range Attribute Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Discrete Range Attribute Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDiscreteRangeAttributeReference(DiscreteRangeAttributeReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Discrete Range Attribute Reference As Subtype Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Discrete Range Attribute Reference As Subtype Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDiscreteRangeAttributeReferenceAsSubtypeDefinition(DiscreteRangeAttributeReferenceAsSubtypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Discrete Range Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Discrete Range Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDiscreteRangeClass(DiscreteRangeClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Discrete Range List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Discrete Range List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDiscreteRangeList(DiscreteRangeList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Discrete Simple Expression Range</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Discrete Simple Expression Range</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDiscreteSimpleExpressionRange(DiscreteSimpleExpressionRange object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Discrete Simple Expression Range As Subtype Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Discrete Simple Expression Range As Subtype Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDiscreteSimpleExpressionRangeAsSubtypeDefinition(DiscreteSimpleExpressionRangeAsSubtypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Discrete Subtype Definition Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Discrete Subtype Definition Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDiscreteSubtypeDefinitionClass(DiscreteSubtypeDefinitionClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Discrete Subtype Indication</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Discrete Subtype Indication</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDiscreteSubtypeIndication(DiscreteSubtypeIndication object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Discrete Subtype Indication As Subtype Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Discrete Subtype Indication As Subtype Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDiscreteSubtypeIndicationAsSubtypeDefinition(DiscreteSubtypeIndicationAsSubtypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Discriminant Association</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Discriminant Association</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDiscriminantAssociation(DiscriminantAssociation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Discriminant Association List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Discriminant Association List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDiscriminantAssociationList(DiscriminantAssociationList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Discriminant Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Discriminant Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDiscriminantConstraint(DiscriminantConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Discriminant Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Discriminant Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDiscriminantSpecification(DiscriminantSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Discriminant Specification List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Discriminant Specification List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDiscriminantSpecificationList(DiscriminantSpecificationList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dispatching Domain Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dispatching Domain Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDispatchingDomainPragma(DispatchingDomainPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Divide Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Divide Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDivideOperator(DivideOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Document Root</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Document Root</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDocumentRoot(DocumentRoot object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Elaborate All Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Elaborate All Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElaborateAllPragma(ElaborateAllPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Elaborate Body Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Elaborate Body Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElaborateBodyPragma(ElaborateBodyPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Elaborate Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Elaborate Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElaboratePragma(ElaboratePragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElementClass(ElementClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element Iterator Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element Iterator Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElementIteratorSpecification(ElementIteratorSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElementList(ElementList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Else Expression Path</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Else Expression Path</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElseExpressionPath(ElseExpressionPath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Else Path</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Else Path</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElsePath(ElsePath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Elsif Expression Path</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Elsif Expression Path</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElsifExpressionPath(ElsifExpressionPath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Elsif Path</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Elsif Path</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElsifPath(ElsifPath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Entry Body Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Entry Body Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEntryBodyDeclaration(EntryBodyDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Entry Call Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Entry Call Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEntryCallStatement(EntryCallStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Entry Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Entry Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEntryDeclaration(EntryDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Entry Index Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Entry Index Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEntryIndexSpecification(EntryIndexSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enumeration Literal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enumeration Literal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnumerationLiteral(EnumerationLiteral object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enumeration Literal Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enumeration Literal Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnumerationLiteralSpecification(EnumerationLiteralSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enumeration Representation Clause</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enumeration Representation Clause</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnumerationRepresentationClause(EnumerationRepresentationClause object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enumeration Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enumeration Type Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnumerationTypeDefinition(EnumerationTypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Equal Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Equal Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEqualOperator(EqualOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Exception Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Exception Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExceptionDeclaration(ExceptionDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Exception Handler</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Exception Handler</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExceptionHandler(ExceptionHandler object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Exception Handler List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Exception Handler List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExceptionHandlerList(ExceptionHandlerList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Exception Renaming Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Exception Renaming Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExceptionRenamingDeclaration(ExceptionRenamingDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Exit Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Exit Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExitStatement(ExitStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Explicit Dereference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Explicit Dereference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExplicitDereference(ExplicitDereference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Exponent Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Exponent Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExponentAttribute(ExponentAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Exponentiate Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Exponentiate Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExponentiateOperator(ExponentiateOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Export Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Export Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExportPragma(ExportPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Expression Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expression Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExpressionClass(ExpressionClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Expression Function Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expression Function Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExpressionFunctionDeclaration(ExpressionFunctionDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Expression List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expression List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExpressionList(ExpressionList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Extended Return Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Extended Return Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExtendedReturnStatement(ExtendedReturnStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Extension Aggregate</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Extension Aggregate</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExtensionAggregate(ExtensionAggregate object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>External Tag Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>External Tag Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExternalTagAttribute(ExternalTagAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>First Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>First Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFirstAttribute(FirstAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>First Bit Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>First Bit Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFirstBitAttribute(FirstBitAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Floating Point Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Floating Point Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFloatingPointDefinition(FloatingPointDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Floor Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Floor Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFloorAttribute(FloorAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>For All Quantified Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>For All Quantified Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseForAllQuantifiedExpression(ForAllQuantifiedExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fore Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fore Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseForeAttribute(ForeAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>For Loop Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>For Loop Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseForLoopStatement(ForLoopStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Access To Constant</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Access To Constant</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalAccessToConstant(FormalAccessToConstant object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Access To Function</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Access To Function</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalAccessToFunction(FormalAccessToFunction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Access To Procedure</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Access To Procedure</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalAccessToProcedure(FormalAccessToProcedure object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Access To Protected Function</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Access To Protected Function</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalAccessToProtectedFunction(FormalAccessToProtectedFunction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Access To Protected Procedure</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Access To Protected Procedure</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalAccessToProtectedProcedure(FormalAccessToProtectedProcedure object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Access To Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Access To Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalAccessToVariable(FormalAccessToVariable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Constrained Array Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Constrained Array Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalConstrainedArrayDefinition(FormalConstrainedArrayDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Decimal Fixed Point Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Decimal Fixed Point Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalDecimalFixedPointDefinition(FormalDecimalFixedPointDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Derived Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Derived Type Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalDerivedTypeDefinition(FormalDerivedTypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Discrete Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Discrete Type Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalDiscreteTypeDefinition(FormalDiscreteTypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Floating Point Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Floating Point Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalFloatingPointDefinition(FormalFloatingPointDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Function Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Function Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalFunctionDeclaration(FormalFunctionDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Incomplete Type Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Incomplete Type Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalIncompleteTypeDeclaration(FormalIncompleteTypeDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Limited Interface</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Limited Interface</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalLimitedInterface(FormalLimitedInterface object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Modular Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Modular Type Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalModularTypeDefinition(FormalModularTypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Object Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Object Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalObjectDeclaration(FormalObjectDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Ordinary Fixed Point Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Ordinary Fixed Point Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalOrdinaryFixedPointDefinition(FormalOrdinaryFixedPointDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Ordinary Interface</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Ordinary Interface</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalOrdinaryInterface(FormalOrdinaryInterface object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Package Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Package Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalPackageDeclaration(FormalPackageDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Package Declaration With Box</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Package Declaration With Box</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalPackageDeclarationWithBox(FormalPackageDeclarationWithBox object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Pool Specific Access To Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Pool Specific Access To Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalPoolSpecificAccessToVariable(FormalPoolSpecificAccessToVariable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Private Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Private Type Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalPrivateTypeDefinition(FormalPrivateTypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Procedure Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Procedure Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalProcedureDeclaration(FormalProcedureDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Protected Interface</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Protected Interface</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalProtectedInterface(FormalProtectedInterface object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Signed Integer Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Signed Integer Type Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalSignedIntegerTypeDefinition(FormalSignedIntegerTypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Synchronized Interface</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Synchronized Interface</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalSynchronizedInterface(FormalSynchronizedInterface object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Tagged Private Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Tagged Private Type Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalTaggedPrivateTypeDefinition(FormalTaggedPrivateTypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Task Interface</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Task Interface</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalTaskInterface(FormalTaskInterface object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Type Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Type Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalTypeDeclaration(FormalTypeDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Formal Unconstrained Array Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Formal Unconstrained Array Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormalUnconstrainedArrayDefinition(FormalUnconstrainedArrayDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>For Some Quantified Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>For Some Quantified Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseForSomeQuantifiedExpression(ForSomeQuantifiedExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fraction Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fraction Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFractionAttribute(FractionAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Function Body Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Function Body Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFunctionBodyDeclaration(FunctionBodyDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Function Body Stub</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Function Body Stub</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFunctionBodyStub(FunctionBodyStub object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Function Call</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Function Call</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFunctionCall(FunctionCall object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Function Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Function Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFunctionDeclaration(FunctionDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Function Instantiation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Function Instantiation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFunctionInstantiation(FunctionInstantiation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Function Renaming Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Function Renaming Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFunctionRenamingDeclaration(FunctionRenamingDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Generalized Iterator Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Generalized Iterator Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGeneralizedIteratorSpecification(GeneralizedIteratorSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Generic Association</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Generic Association</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGenericAssociation(GenericAssociation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Generic Function Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Generic Function Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGenericFunctionDeclaration(GenericFunctionDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Generic Function Renaming Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Generic Function Renaming Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGenericFunctionRenamingDeclaration(GenericFunctionRenamingDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Generic Package Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Generic Package Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGenericPackageDeclaration(GenericPackageDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Generic Package Renaming Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Generic Package Renaming Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGenericPackageRenamingDeclaration(GenericPackageRenamingDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Generic Procedure Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Generic Procedure Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGenericProcedureDeclaration(GenericProcedureDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Generic Procedure Renaming Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Generic Procedure Renaming Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGenericProcedureRenamingDeclaration(GenericProcedureRenamingDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Goto Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Goto Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGotoStatement(GotoStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Greater Than Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Greater Than Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGreaterThanOperator(GreaterThanOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Greater Than Or Equal Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Greater Than Or Equal Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGreaterThanOrEqualOperator(GreaterThanOrEqualOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Abstract QType</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Abstract QType</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasAbstractQType(HasAbstractQType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Abstract QType1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Abstract QType1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasAbstractQType1(HasAbstractQType1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Abstract QType2</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Abstract QType2</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasAbstractQType2(HasAbstractQType2 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Abstract QType3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Abstract QType3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasAbstractQType3(HasAbstractQType3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Abstract QType4</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Abstract QType4</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasAbstractQType4(HasAbstractQType4 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Abstract QType5</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Abstract QType5</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasAbstractQType5(HasAbstractQType5 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Abstract QType6</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Abstract QType6</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasAbstractQType6(HasAbstractQType6 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Abstract QType7</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Abstract QType7</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasAbstractQType7(HasAbstractQType7 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Abstract QType8</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Abstract QType8</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasAbstractQType8(HasAbstractQType8 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Abstract QType9</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Abstract QType9</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasAbstractQType9(HasAbstractQType9 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Abstract QType10</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Abstract QType10</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasAbstractQType10(HasAbstractQType10 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Abstract QType11</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Abstract QType11</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasAbstractQType11(HasAbstractQType11 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Abstract QType12</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Abstract QType12</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasAbstractQType12(HasAbstractQType12 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Abstract QType13</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Abstract QType13</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasAbstractQType13(HasAbstractQType13 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Aliased QType</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Aliased QType</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasAliasedQType(HasAliasedQType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Aliased QType1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Aliased QType1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasAliasedQType1(HasAliasedQType1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Aliased QType2</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Aliased QType2</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasAliasedQType2(HasAliasedQType2 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Aliased QType3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Aliased QType3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasAliasedQType3(HasAliasedQType3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Aliased QType4</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Aliased QType4</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasAliasedQType4(HasAliasedQType4 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Aliased QType5</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Aliased QType5</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasAliasedQType5(HasAliasedQType5 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Aliased QType6</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Aliased QType6</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasAliasedQType6(HasAliasedQType6 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Aliased QType7</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Aliased QType7</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasAliasedQType7(HasAliasedQType7 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Aliased QType8</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Aliased QType8</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasAliasedQType8(HasAliasedQType8 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Limited QType</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Limited QType</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasLimitedQType(HasLimitedQType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Limited QType1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Limited QType1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasLimitedQType1(HasLimitedQType1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Limited QType2</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Limited QType2</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasLimitedQType2(HasLimitedQType2 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Limited QType3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Limited QType3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasLimitedQType3(HasLimitedQType3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Limited QType4</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Limited QType4</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasLimitedQType4(HasLimitedQType4 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Limited QType5</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Limited QType5</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasLimitedQType5(HasLimitedQType5 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Limited QType6</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Limited QType6</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasLimitedQType6(HasLimitedQType6 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Limited QType7</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Limited QType7</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasLimitedQType7(HasLimitedQType7 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Limited QType8</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Limited QType8</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasLimitedQType8(HasLimitedQType8 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Limited QType9</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Limited QType9</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasLimitedQType9(HasLimitedQType9 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Limited QType10</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Limited QType10</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasLimitedQType10(HasLimitedQType10 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Limited QType11</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Limited QType11</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasLimitedQType11(HasLimitedQType11 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Null Exclusion QType</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Null Exclusion QType</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNullExclusionQType(HasNullExclusionQType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Null Exclusion QType1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Null Exclusion QType1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNullExclusionQType1(HasNullExclusionQType1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Null Exclusion QType2</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Null Exclusion QType2</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNullExclusionQType2(HasNullExclusionQType2 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Null Exclusion QType3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Null Exclusion QType3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNullExclusionQType3(HasNullExclusionQType3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Null Exclusion QType4</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Null Exclusion QType4</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNullExclusionQType4(HasNullExclusionQType4 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Null Exclusion QType5</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Null Exclusion QType5</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNullExclusionQType5(HasNullExclusionQType5 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Null Exclusion QType6</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Null Exclusion QType6</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNullExclusionQType6(HasNullExclusionQType6 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Null Exclusion QType7</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Null Exclusion QType7</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNullExclusionQType7(HasNullExclusionQType7 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Null Exclusion QType8</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Null Exclusion QType8</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNullExclusionQType8(HasNullExclusionQType8 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Null Exclusion QType9</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Null Exclusion QType9</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNullExclusionQType9(HasNullExclusionQType9 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Null Exclusion QType10</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Null Exclusion QType10</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNullExclusionQType10(HasNullExclusionQType10 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Null Exclusion QType11</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Null Exclusion QType11</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNullExclusionQType11(HasNullExclusionQType11 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Null Exclusion QType12</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Null Exclusion QType12</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNullExclusionQType12(HasNullExclusionQType12 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Null Exclusion QType13</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Null Exclusion QType13</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNullExclusionQType13(HasNullExclusionQType13 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Null Exclusion QType14</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Null Exclusion QType14</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNullExclusionQType14(HasNullExclusionQType14 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Null Exclusion QType15</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Null Exclusion QType15</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNullExclusionQType15(HasNullExclusionQType15 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Null Exclusion QType16</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Null Exclusion QType16</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNullExclusionQType16(HasNullExclusionQType16 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Null Exclusion QType17</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Null Exclusion QType17</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNullExclusionQType17(HasNullExclusionQType17 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Null Exclusion QType18</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Null Exclusion QType18</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNullExclusionQType18(HasNullExclusionQType18 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Null Exclusion QType19</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Null Exclusion QType19</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNullExclusionQType19(HasNullExclusionQType19 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Null Exclusion QType20</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Null Exclusion QType20</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNullExclusionQType20(HasNullExclusionQType20 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Null Exclusion QType21</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Null Exclusion QType21</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNullExclusionQType21(HasNullExclusionQType21 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Null Exclusion QType22</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Null Exclusion QType22</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNullExclusionQType22(HasNullExclusionQType22 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Null Exclusion QType23</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Null Exclusion QType23</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNullExclusionQType23(HasNullExclusionQType23 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Null Exclusion QType24</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Null Exclusion QType24</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasNullExclusionQType24(HasNullExclusionQType24 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Private QType</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Private QType</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasPrivateQType(HasPrivateQType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Private QType1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Private QType1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasPrivateQType1(HasPrivateQType1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Reverse QType</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Reverse QType</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasReverseQType(HasReverseQType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Reverse QType1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Reverse QType1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasReverseQType1(HasReverseQType1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Reverse QType2</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Reverse QType2</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasReverseQType2(HasReverseQType2 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Synchronized QType</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Synchronized QType</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasSynchronizedQType(HasSynchronizedQType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Synchronized QType1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Synchronized QType1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasSynchronizedQType1(HasSynchronizedQType1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Has Tagged QType</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Has Tagged QType</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHasTaggedQType(HasTaggedQType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Identifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Identifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIdentifier(Identifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Identity Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Identity Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIdentityAttribute(IdentityAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>If Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>If Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIfExpression(IfExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>If Expression Path</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>If Expression Path</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIfExpressionPath(IfExpressionPath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>If Path</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>If Path</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIfPath(IfPath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>If Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>If Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIfStatement(IfStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Image Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Image Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImageAttribute(ImageAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Implementation Defined Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Implementation Defined Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImplementationDefinedAttribute(ImplementationDefinedAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Implementation Defined Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Implementation Defined Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImplementationDefinedPragma(ImplementationDefinedPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Import Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Import Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImportPragma(ImportPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Incomplete Type Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Incomplete Type Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIncompleteTypeDeclaration(IncompleteTypeDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Independent Components Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Independent Components Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIndependentComponentsPragma(IndependentComponentsPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Independent Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Independent Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIndependentPragma(IndependentPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Index Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Index Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIndexConstraint(IndexConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Indexed Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Indexed Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIndexedComponent(IndexedComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Inline Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Inline Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInlinePragma(InlinePragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>In Membership Test</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>In Membership Test</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInMembershipTest(InMembershipTest object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Input Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Input Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInputAttribute(InputAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Inspection Point Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Inspection Point Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInspectionPointPragma(InspectionPointPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Integer Literal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Integer Literal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIntegerLiteral(IntegerLiteral object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Integer Number Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Integer Number Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIntegerNumberDeclaration(IntegerNumberDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interrupt Handler Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interrupt Handler Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInterruptHandlerPragma(InterruptHandlerPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interrupt Priority Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interrupt Priority Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInterruptPriorityPragma(InterruptPriorityPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Not Null Return QType</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Not Null Return QType</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsNotNullReturnQType(IsNotNullReturnQType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Not Null Return QType1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Not Null Return QType1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsNotNullReturnQType1(IsNotNullReturnQType1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Not Null Return QType2</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Not Null Return QType2</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsNotNullReturnQType2(IsNotNullReturnQType2 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Not Null Return QType3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Not Null Return QType3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsNotNullReturnQType3(IsNotNullReturnQType3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Not Null Return QType4</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Not Null Return QType4</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsNotNullReturnQType4(IsNotNullReturnQType4 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Not Null Return QType5</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Not Null Return QType5</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsNotNullReturnQType5(IsNotNullReturnQType5 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Not Null Return QType6</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Not Null Return QType6</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsNotNullReturnQType6(IsNotNullReturnQType6 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Not Null Return QType7</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Not Null Return QType7</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsNotNullReturnQType7(IsNotNullReturnQType7 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Not Null Return QType8</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Not Null Return QType8</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsNotNullReturnQType8(IsNotNullReturnQType8 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Not Null Return QType9</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Not Null Return QType9</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsNotNullReturnQType9(IsNotNullReturnQType9 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Not Null Return QType10</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Not Null Return QType10</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsNotNullReturnQType10(IsNotNullReturnQType10 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Not Null Return QType11</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Not Null Return QType11</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsNotNullReturnQType11(IsNotNullReturnQType11 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Not Overriding Declaration QType</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Not Overriding Declaration QType</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsNotOverridingDeclarationQType(IsNotOverridingDeclarationQType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Not Overriding Declaration QType1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Not Overriding Declaration QType1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsNotOverridingDeclarationQType1(IsNotOverridingDeclarationQType1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Not Overriding Declaration QType2</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Not Overriding Declaration QType2</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsNotOverridingDeclarationQType2(IsNotOverridingDeclarationQType2 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Not Overriding Declaration QType3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Not Overriding Declaration QType3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsNotOverridingDeclarationQType3(IsNotOverridingDeclarationQType3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Not Overriding Declaration QType4</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Not Overriding Declaration QType4</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsNotOverridingDeclarationQType4(IsNotOverridingDeclarationQType4 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Not Overriding Declaration QType5</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Not Overriding Declaration QType5</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsNotOverridingDeclarationQType5(IsNotOverridingDeclarationQType5 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Not Overriding Declaration QType6</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Not Overriding Declaration QType6</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsNotOverridingDeclarationQType6(IsNotOverridingDeclarationQType6 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Not Overriding Declaration QType7</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Not Overriding Declaration QType7</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsNotOverridingDeclarationQType7(IsNotOverridingDeclarationQType7 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Not Overriding Declaration QType8</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Not Overriding Declaration QType8</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsNotOverridingDeclarationQType8(IsNotOverridingDeclarationQType8 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Not Overriding Declaration QType9</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Not Overriding Declaration QType9</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsNotOverridingDeclarationQType9(IsNotOverridingDeclarationQType9 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Not Overriding Declaration QType10</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Not Overriding Declaration QType10</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsNotOverridingDeclarationQType10(IsNotOverridingDeclarationQType10 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Not Overriding Declaration QType11</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Not Overriding Declaration QType11</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsNotOverridingDeclarationQType11(IsNotOverridingDeclarationQType11 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Overriding Declaration QType</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Overriding Declaration QType</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsOverridingDeclarationQType(IsOverridingDeclarationQType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Overriding Declaration QType1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Overriding Declaration QType1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsOverridingDeclarationQType1(IsOverridingDeclarationQType1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Overriding Declaration QType2</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Overriding Declaration QType2</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsOverridingDeclarationQType2(IsOverridingDeclarationQType2 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Overriding Declaration QType3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Overriding Declaration QType3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsOverridingDeclarationQType3(IsOverridingDeclarationQType3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Overriding Declaration QType4</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Overriding Declaration QType4</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsOverridingDeclarationQType4(IsOverridingDeclarationQType4 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Overriding Declaration QType5</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Overriding Declaration QType5</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsOverridingDeclarationQType5(IsOverridingDeclarationQType5 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Overriding Declaration QType6</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Overriding Declaration QType6</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsOverridingDeclarationQType6(IsOverridingDeclarationQType6 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Overriding Declaration QType7</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Overriding Declaration QType7</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsOverridingDeclarationQType7(IsOverridingDeclarationQType7 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Overriding Declaration QType8</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Overriding Declaration QType8</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsOverridingDeclarationQType8(IsOverridingDeclarationQType8 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Overriding Declaration QType9</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Overriding Declaration QType9</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsOverridingDeclarationQType9(IsOverridingDeclarationQType9 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Overriding Declaration QType10</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Overriding Declaration QType10</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsOverridingDeclarationQType10(IsOverridingDeclarationQType10 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Overriding Declaration QType11</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Overriding Declaration QType11</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsOverridingDeclarationQType11(IsOverridingDeclarationQType11 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Prefix Call</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Prefix Call</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsPrefixCall(IsPrefixCall object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Prefix Call QType</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Prefix Call QType</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsPrefixCallQType(IsPrefixCallQType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Prefix Notation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Prefix Notation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsPrefixNotation(IsPrefixNotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Prefix Notation QType</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Prefix Notation QType</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsPrefixNotationQType(IsPrefixNotationQType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Prefix Notation QType1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Prefix Notation QType1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsPrefixNotationQType1(IsPrefixNotationQType1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Known Discriminant Part</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Known Discriminant Part</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseKnownDiscriminantPart(KnownDiscriminantPart object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Last Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Last Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLastAttribute(LastAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Last Bit Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Last Bit Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLastBitAttribute(LastBitAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Leading Part Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Leading Part Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLeadingPartAttribute(LeadingPartAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Length Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Length Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLengthAttribute(LengthAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Less Than Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Less Than Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLessThanOperator(LessThanOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Less Than Or Equal Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Less Than Or Equal Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLessThanOrEqualOperator(LessThanOrEqualOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Limited</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Limited</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLimited(Limited object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Limited Interface</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Limited Interface</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLimitedInterface(LimitedInterface object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Linker Options Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Linker Options Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLinkerOptionsPragma(LinkerOptionsPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>List Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>List Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseListPragma(ListPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Locking Policy Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Locking Policy Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLockingPolicyPragma(LockingPolicyPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Loop Parameter Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Loop Parameter Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLoopParameterSpecification(LoopParameterSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Loop Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Loop Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLoopStatement(LoopStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Machine Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Machine Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMachineAttribute(MachineAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Machine Emax Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Machine Emax Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMachineEmaxAttribute(MachineEmaxAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Machine Emin Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Machine Emin Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMachineEminAttribute(MachineEminAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Machine Mantissa Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Machine Mantissa Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMachineMantissaAttribute(MachineMantissaAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Machine Overflows Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Machine Overflows Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMachineOverflowsAttribute(MachineOverflowsAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Machine Radix Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Machine Radix Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMachineRadixAttribute(MachineRadixAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Machine Rounding Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Machine Rounding Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMachineRoundingAttribute(MachineRoundingAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Machine Rounds Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Machine Rounds Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMachineRoundsAttribute(MachineRoundsAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Max Alignment For Allocation Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Max Alignment For Allocation Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaxAlignmentForAllocationAttribute(MaxAlignmentForAllocationAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Max Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Max Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaxAttribute(MaxAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Max Size In Storage Elements Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Max Size In Storage Elements Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaxSizeInStorageElementsAttribute(MaxSizeInStorageElementsAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Min Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Min Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMinAttribute(MinAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Minus Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Minus Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMinusOperator(MinusOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Mod Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mod Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModAttribute(ModAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModelAttribute(ModelAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model Emin Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model Emin Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModelEminAttribute(ModelEminAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model Epsilon Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model Epsilon Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModelEpsilonAttribute(ModelEpsilonAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model Mantissa Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model Mantissa Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModelMantissaAttribute(ModelMantissaAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model Small Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model Small Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModelSmallAttribute(ModelSmallAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Mod Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mod Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModOperator(ModOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Modular Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Modular Type Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModularTypeDefinition(ModularTypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Modulus Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Modulus Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModulusAttribute(ModulusAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Multiply Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Multiply Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMultiplyOperator(MultiplyOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Name Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Name Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNameClass(NameClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Array Aggregate</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Array Aggregate</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedArrayAggregate(NamedArrayAggregate object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Name List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Name List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNameList(NameList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>No Return Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>No Return Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNoReturnPragma(NoReturnPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Normalize Scalars Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Normalize Scalars Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNormalizeScalarsPragma(NormalizeScalarsPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Not An Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Not An Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNotAnElement(NotAnElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Not Equal Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Not Equal Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNotEqualOperator(NotEqualOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Not In Membership Test</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Not In Membership Test</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNotInMembershipTest(NotInMembershipTest object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Not Null Return</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Not Null Return</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNotNullReturn(NotNullReturn object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Not Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Not Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNotOperator(NotOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Not Overriding</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Not Overriding</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNotOverriding(NotOverriding object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Null Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Null Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNullComponent(NullComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Null Exclusion</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Null Exclusion</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNullExclusion(NullExclusion object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Null Literal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Null Literal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNullLiteral(NullLiteral object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Null Procedure Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Null Procedure Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNullProcedureDeclaration(NullProcedureDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Null Record Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Null Record Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNullRecordDefinition(NullRecordDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Null Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Null Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNullStatement(NullStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Object Renaming Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Object Renaming Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseObjectRenamingDeclaration(ObjectRenamingDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Optimize Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Optimize Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOptimizePragma(OptimizePragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ordinary Fixed Point Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ordinary Fixed Point Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOrdinaryFixedPointDefinition(OrdinaryFixedPointDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ordinary Interface</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ordinary Interface</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOrdinaryInterface(OrdinaryInterface object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ordinary Type Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ordinary Type Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOrdinaryTypeDeclaration(OrdinaryTypeDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Or Else Short Circuit</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Or Else Short Circuit</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOrElseShortCircuit(OrElseShortCircuit object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Or Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Or Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOrOperator(OrOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Or Path</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Or Path</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOrPath(OrPath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Others Choice</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Others Choice</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOthersChoice(OthersChoice object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Output Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Output Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOutputAttribute(OutputAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Overlaps Storage Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Overlaps Storage Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOverlapsStorageAttribute(OverlapsStorageAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Overriding</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Overriding</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOverriding(Overriding object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Package Body Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Package Body Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePackageBodyDeclaration(PackageBodyDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Package Body Stub</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Package Body Stub</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePackageBodyStub(PackageBodyStub object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Package Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Package Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePackageDeclaration(PackageDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Package Instantiation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Package Instantiation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePackageInstantiation(PackageInstantiation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Package Renaming Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Package Renaming Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePackageRenamingDeclaration(PackageRenamingDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pack Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pack Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePackPragma(PackPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Page Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Page Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePagePragma(PagePragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter Association</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter Association</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameterAssociation(ParameterAssociation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameterSpecification(ParameterSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter Specification List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter Specification List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameterSpecificationList(ParameterSpecificationList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parenthesized Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parenthesized Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParenthesizedExpression(ParenthesizedExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Partition Elaboration Policy Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Partition Elaboration Policy Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePartitionElaborationPolicyPragma(PartitionElaborationPolicyPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Partition Id Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Partition Id Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePartitionIdAttribute(PartitionIdAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Path Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Path Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePathClass(PathClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Path List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Path List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePathList(PathList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Plus Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Plus Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePlusOperator(PlusOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pool Specific Access To Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pool Specific Access To Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePoolSpecificAccessToVariable(PoolSpecificAccessToVariable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pos Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pos Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePosAttribute(PosAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Positional Array Aggregate</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Positional Array Aggregate</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePositionalArrayAggregate(PositionalArrayAggregate object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Position Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Position Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePositionAttribute(PositionAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pragma Argument Association</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pragma Argument Association</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePragmaArgumentAssociation(PragmaArgumentAssociation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pragma Element Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pragma Element Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePragmaElementClass(PragmaElementClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pred Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pred Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePredAttribute(PredAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Preelaborable Initialization Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Preelaborable Initialization Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePreelaborableInitializationPragma(PreelaborableInitializationPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Preelaborate Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Preelaborate Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePreelaboratePragma(PreelaboratePragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Priority Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Priority Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePriorityAttribute(PriorityAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Priority Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Priority Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePriorityPragma(PriorityPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Priority Specific Dispatching Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Priority Specific Dispatching Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePrioritySpecificDispatchingPragma(PrioritySpecificDispatchingPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Private</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Private</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePrivate(Private object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Private Extension Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Private Extension Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePrivateExtensionDeclaration(PrivateExtensionDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Private Extension Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Private Extension Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePrivateExtensionDefinition(PrivateExtensionDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Private Type Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Private Type Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePrivateTypeDeclaration(PrivateTypeDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Private Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Private Type Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePrivateTypeDefinition(PrivateTypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Procedure Body Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Procedure Body Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcedureBodyDeclaration(ProcedureBodyDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Procedure Body Stub</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Procedure Body Stub</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcedureBodyStub(ProcedureBodyStub object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Procedure Call Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Procedure Call Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcedureCallStatement(ProcedureCallStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Procedure Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Procedure Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcedureDeclaration(ProcedureDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Procedure Instantiation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Procedure Instantiation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcedureInstantiation(ProcedureInstantiation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Procedure Renaming Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Procedure Renaming Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcedureRenamingDeclaration(ProcedureRenamingDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Profile Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Profile Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProfilePragma(ProfilePragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Protected Body Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Protected Body Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProtectedBodyDeclaration(ProtectedBodyDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Protected Body Stub</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Protected Body Stub</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProtectedBodyStub(ProtectedBodyStub object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Protected Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Protected Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProtectedDefinition(ProtectedDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Protected Interface</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Protected Interface</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProtectedInterface(ProtectedInterface object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Protected Type Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Protected Type Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProtectedTypeDeclaration(ProtectedTypeDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pure Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pure Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePurePragma(PurePragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Qualified Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Qualified Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseQualifiedExpression(QualifiedExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Queuing Policy Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Queuing Policy Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseQueuingPolicyPragma(QueuingPolicyPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Raise Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Raise Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRaiseExpression(RaiseExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Raise Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Raise Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRaiseStatement(RaiseStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Range Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Range Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRangeAttribute(RangeAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Range Attribute Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Range Attribute Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRangeAttributeReference(RangeAttributeReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Range Constraint Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Range Constraint Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRangeConstraintClass(RangeConstraintClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Read Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Read Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReadAttribute(ReadAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Real Literal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Real Literal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRealLiteral(RealLiteral object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Real Number Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Real Number Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRealNumberDeclaration(RealNumberDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Record Aggregate</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Record Aggregate</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRecordAggregate(RecordAggregate object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Record Component Association</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Record Component Association</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRecordComponentAssociation(RecordComponentAssociation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Record Component Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Record Component Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRecordComponentClass(RecordComponentClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Record Component List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Record Component List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRecordComponentList(RecordComponentList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Record Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Record Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRecordDefinition(RecordDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Record Representation Clause</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Record Representation Clause</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRecordRepresentationClause(RecordRepresentationClause object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Record Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Record Type Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRecordTypeDefinition(RecordTypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Relative Deadline Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Relative Deadline Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRelativeDeadlinePragma(RelativeDeadlinePragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Remainder Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Remainder Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRemainderAttribute(RemainderAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rem Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rem Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRemOperator(RemOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Remote Call Interface Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Remote Call Interface Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRemoteCallInterfacePragma(RemoteCallInterfacePragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Remote Types Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Remote Types Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRemoteTypesPragma(RemoteTypesPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Requeue Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Requeue Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRequeueStatement(RequeueStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Requeue Statement With Abort</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Requeue Statement With Abort</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRequeueStatementWithAbort(RequeueStatementWithAbort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Restrictions Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Restrictions Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRestrictionsPragma(RestrictionsPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Return Constant Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Return Constant Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReturnConstantSpecification(ReturnConstantSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Return Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Return Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReturnStatement(ReturnStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Return Variable Specification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Return Variable Specification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReturnVariableSpecification(ReturnVariableSpecification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reverse</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reverse</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReverse(Reverse object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reviewable Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reviewable Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReviewablePragma(ReviewablePragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Root Integer Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Root Integer Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRootIntegerDefinition(RootIntegerDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Root Real Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Root Real Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRootRealDefinition(RootRealDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Round Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Round Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRoundAttribute(RoundAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rounding Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rounding Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRoundingAttribute(RoundingAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Safe First Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Safe First Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSafeFirstAttribute(SafeFirstAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Safe Last Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Safe Last Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSafeLastAttribute(SafeLastAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Scale Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Scale Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScaleAttribute(ScaleAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Scaling Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Scaling Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScalingAttribute(ScalingAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Selected Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Selected Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSelectedComponent(SelectedComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Selective Accept Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Selective Accept Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSelectiveAcceptStatement(SelectiveAcceptStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Select Path</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Select Path</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSelectPath(SelectPath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Shared Passive Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Shared Passive Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSharedPassivePragma(SharedPassivePragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Signed Integer Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Signed Integer Type Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSignedIntegerTypeDefinition(SignedIntegerTypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Signed Zeros Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Signed Zeros Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSignedZerosAttribute(SignedZerosAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple Expression Range</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple Expression Range</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimpleExpressionRange(SimpleExpressionRange object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Single Protected Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Single Protected Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSingleProtectedDeclaration(SingleProtectedDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Single Task Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Single Task Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSingleTaskDeclaration(SingleTaskDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Size Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Size Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSizeAttribute(SizeAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Slice</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Slice</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSlice(Slice object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Small Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Small Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSmallAttribute(SmallAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Source Location</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Source Location</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSourceLocation(SourceLocation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Statement Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Statement Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStatementClass(StatementClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Statement List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Statement List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStatementList(StatementList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Storage Pool Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Storage Pool Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStoragePoolAttribute(StoragePoolAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Storage Size Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Storage Size Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStorageSizeAttribute(StorageSizeAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Storage Size Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Storage Size Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStorageSizePragma(StorageSizePragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Stream Size Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Stream Size Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStreamSizeAttribute(StreamSizeAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String Literal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String Literal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStringLiteral(StringLiteral object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Subtype Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Subtype Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubtypeDeclaration(SubtypeDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Subtype Indication</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Subtype Indication</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubtypeIndication(SubtypeIndication object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Succ Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Succ Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSuccAttribute(SuccAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Suppress Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Suppress Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSuppressPragma(SuppressPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Synchronized</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Synchronized</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSynchronized(Synchronized object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Synchronized Interface</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Synchronized Interface</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSynchronizedInterface(SynchronizedInterface object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tag Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tag Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTagAttribute(TagAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tagged</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tagged</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTagged(Tagged object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tagged Incomplete Type Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tagged Incomplete Type Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTaggedIncompleteTypeDeclaration(TaggedIncompleteTypeDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tagged Private Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tagged Private Type Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTaggedPrivateTypeDefinition(TaggedPrivateTypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tagged Record Type Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tagged Record Type Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTaggedRecordTypeDefinition(TaggedRecordTypeDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Task Body Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Task Body Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTaskBodyDeclaration(TaskBodyDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Task Body Stub</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Task Body Stub</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTaskBodyStub(TaskBodyStub object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Task Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Task Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTaskDefinition(TaskDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Task Dispatching Policy Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Task Dispatching Policy Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTaskDispatchingPolicyPragma(TaskDispatchingPolicyPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Task Interface</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Task Interface</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTaskInterface(TaskInterface object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Task Type Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Task Type Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTaskTypeDeclaration(TaskTypeDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Terminate Alternative Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Terminate Alternative Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTerminateAlternativeStatement(TerminateAlternativeStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Terminated Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Terminated Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTerminatedAttribute(TerminatedAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Then Abort Path</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Then Abort Path</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseThenAbortPath(ThenAbortPath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Timed Entry Call Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Timed Entry Call Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTimedEntryCallStatement(TimedEntryCallStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Truncation Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Truncation Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTruncationAttribute(TruncationAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Type Conversion</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type Conversion</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTypeConversion(TypeConversion object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unary Minus Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unary Minus Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnaryMinusOperator(UnaryMinusOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unary Plus Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unary Plus Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnaryPlusOperator(UnaryPlusOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unbiased Rounding Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unbiased Rounding Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnbiasedRoundingAttribute(UnbiasedRoundingAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unchecked Access Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unchecked Access Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUncheckedAccessAttribute(UncheckedAccessAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unchecked Union Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unchecked Union Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUncheckedUnionPragma(UncheckedUnionPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unconstrained Array Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unconstrained Array Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnconstrainedArrayDefinition(UnconstrainedArrayDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Universal Fixed Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Universal Fixed Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUniversalFixedDefinition(UniversalFixedDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Universal Integer Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Universal Integer Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUniversalIntegerDefinition(UniversalIntegerDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Universal Real Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Universal Real Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUniversalRealDefinition(UniversalRealDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unknown Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unknown Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnknownAttribute(UnknownAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unknown Discriminant Part</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unknown Discriminant Part</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnknownDiscriminantPart(UnknownDiscriminantPart object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unknown Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unknown Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnknownPragma(UnknownPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unsuppress Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unsuppress Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnsuppressPragma(UnsuppressPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Use All Type Clause</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Use All Type Clause</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUseAllTypeClause(UseAllTypeClause object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Use Package Clause</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Use Package Clause</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUsePackageClause(UsePackageClause object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Use Type Clause</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Use Type Clause</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUseTypeClause(UseTypeClause object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Val Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Val Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseValAttribute(ValAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Valid Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Valid Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseValidAttribute(ValidAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Value Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Value Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseValueAttribute(ValueAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariableDeclaration(VariableDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variant</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variant</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariant(Variant object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variant List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variant List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariantList(VariantList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variant Part</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variant Part</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariantPart(VariantPart object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Version Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Version Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVersionAttribute(VersionAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Volatile Components Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Volatile Components Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVolatileComponentsPragma(VolatileComponentsPragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Volatile Pragma</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Volatile Pragma</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVolatilePragma(VolatilePragma object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>While Loop Statement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>While Loop Statement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWhileLoopStatement(WhileLoopStatement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Wide Image Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wide Image Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWideImageAttribute(WideImageAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Wide Value Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wide Value Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWideValueAttribute(WideValueAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Wide Wide Image Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wide Wide Image Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWideWideImageAttribute(WideWideImageAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Wide Wide Value Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wide Wide Value Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWideWideValueAttribute(WideWideValueAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Wide Wide Width Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wide Wide Width Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWideWideWidthAttribute(WideWideWidthAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Wide Width Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wide Width Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWideWidthAttribute(WideWidthAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Width Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Width Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWidthAttribute(WidthAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>With Clause</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>With Clause</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWithClause(WithClause object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Write Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Write Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWriteAttribute(WriteAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Xor Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Xor Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXorOperator(XorOperator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //AdaSwitch
