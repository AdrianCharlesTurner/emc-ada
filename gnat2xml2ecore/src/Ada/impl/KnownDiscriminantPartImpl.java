/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.DiscriminantSpecificationList;
import Ada.KnownDiscriminantPart;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Known Discriminant Part</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.KnownDiscriminantPartImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.KnownDiscriminantPartImpl#getDiscriminantsQl <em>Discriminants Ql</em>}</li>
 *   <li>{@link Ada.impl.KnownDiscriminantPartImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class KnownDiscriminantPartImpl extends MinimalEObjectImpl.Container implements KnownDiscriminantPart {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getDiscriminantsQl() <em>Discriminants Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiscriminantsQl()
	 * @generated
	 * @ordered
	 */
	protected DiscriminantSpecificationList discriminantsQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected KnownDiscriminantPartImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getKnownDiscriminantPart();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.KNOWN_DISCRIMINANT_PART__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.KNOWN_DISCRIMINANT_PART__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.KNOWN_DISCRIMINANT_PART__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.KNOWN_DISCRIMINANT_PART__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscriminantSpecificationList getDiscriminantsQl() {
		return discriminantsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscriminantsQl(DiscriminantSpecificationList newDiscriminantsQl, NotificationChain msgs) {
		DiscriminantSpecificationList oldDiscriminantsQl = discriminantsQl;
		discriminantsQl = newDiscriminantsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.KNOWN_DISCRIMINANT_PART__DISCRIMINANTS_QL, oldDiscriminantsQl, newDiscriminantsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscriminantsQl(DiscriminantSpecificationList newDiscriminantsQl) {
		if (newDiscriminantsQl != discriminantsQl) {
			NotificationChain msgs = null;
			if (discriminantsQl != null)
				msgs = ((InternalEObject)discriminantsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.KNOWN_DISCRIMINANT_PART__DISCRIMINANTS_QL, null, msgs);
			if (newDiscriminantsQl != null)
				msgs = ((InternalEObject)newDiscriminantsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.KNOWN_DISCRIMINANT_PART__DISCRIMINANTS_QL, null, msgs);
			msgs = basicSetDiscriminantsQl(newDiscriminantsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.KNOWN_DISCRIMINANT_PART__DISCRIMINANTS_QL, newDiscriminantsQl, newDiscriminantsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.KNOWN_DISCRIMINANT_PART__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.KNOWN_DISCRIMINANT_PART__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.KNOWN_DISCRIMINANT_PART__DISCRIMINANTS_QL:
				return basicSetDiscriminantsQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.KNOWN_DISCRIMINANT_PART__SLOC:
				return getSloc();
			case AdaPackage.KNOWN_DISCRIMINANT_PART__DISCRIMINANTS_QL:
				return getDiscriminantsQl();
			case AdaPackage.KNOWN_DISCRIMINANT_PART__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.KNOWN_DISCRIMINANT_PART__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.KNOWN_DISCRIMINANT_PART__DISCRIMINANTS_QL:
				setDiscriminantsQl((DiscriminantSpecificationList)newValue);
				return;
			case AdaPackage.KNOWN_DISCRIMINANT_PART__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.KNOWN_DISCRIMINANT_PART__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.KNOWN_DISCRIMINANT_PART__DISCRIMINANTS_QL:
				setDiscriminantsQl((DiscriminantSpecificationList)null);
				return;
			case AdaPackage.KNOWN_DISCRIMINANT_PART__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.KNOWN_DISCRIMINANT_PART__SLOC:
				return sloc != null;
			case AdaPackage.KNOWN_DISCRIMINANT_PART__DISCRIMINANTS_QL:
				return discriminantsQl != null;
			case AdaPackage.KNOWN_DISCRIMINANT_PART__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //KnownDiscriminantPartImpl
