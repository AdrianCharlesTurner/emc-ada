/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Accept Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.AcceptStatement#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.AcceptStatement#getLabelNamesQl <em>Label Names Ql</em>}</li>
 *   <li>{@link Ada.AcceptStatement#getAcceptEntryDirectNameQ <em>Accept Entry Direct Name Q</em>}</li>
 *   <li>{@link Ada.AcceptStatement#getAcceptEntryIndexQ <em>Accept Entry Index Q</em>}</li>
 *   <li>{@link Ada.AcceptStatement#getAcceptParametersQl <em>Accept Parameters Ql</em>}</li>
 *   <li>{@link Ada.AcceptStatement#getAcceptBodyStatementsQl <em>Accept Body Statements Ql</em>}</li>
 *   <li>{@link Ada.AcceptStatement#getAcceptBodyExceptionHandlersQl <em>Accept Body Exception Handlers Ql</em>}</li>
 *   <li>{@link Ada.AcceptStatement#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getAcceptStatement()
 * @model extendedMetaData="name='Accept_Statement' kind='elementOnly'"
 * @generated
 */
public interface AcceptStatement extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getAcceptStatement_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.AcceptStatement#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Label Names Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label Names Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label Names Ql</em>' containment reference.
	 * @see #setLabelNamesQl(DefiningNameList)
	 * @see Ada.AdaPackage#getAcceptStatement_LabelNamesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='label_names_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameList getLabelNamesQl();

	/**
	 * Sets the value of the '{@link Ada.AcceptStatement#getLabelNamesQl <em>Label Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label Names Ql</em>' containment reference.
	 * @see #getLabelNamesQl()
	 * @generated
	 */
	void setLabelNamesQl(DefiningNameList value);

	/**
	 * Returns the value of the '<em><b>Accept Entry Direct Name Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Accept Entry Direct Name Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Accept Entry Direct Name Q</em>' containment reference.
	 * @see #setAcceptEntryDirectNameQ(NameClass)
	 * @see Ada.AdaPackage#getAcceptStatement_AcceptEntryDirectNameQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='accept_entry_direct_name_q' namespace='##targetNamespace'"
	 * @generated
	 */
	NameClass getAcceptEntryDirectNameQ();

	/**
	 * Sets the value of the '{@link Ada.AcceptStatement#getAcceptEntryDirectNameQ <em>Accept Entry Direct Name Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Accept Entry Direct Name Q</em>' containment reference.
	 * @see #getAcceptEntryDirectNameQ()
	 * @generated
	 */
	void setAcceptEntryDirectNameQ(NameClass value);

	/**
	 * Returns the value of the '<em><b>Accept Entry Index Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Accept Entry Index Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Accept Entry Index Q</em>' containment reference.
	 * @see #setAcceptEntryIndexQ(ExpressionClass)
	 * @see Ada.AdaPackage#getAcceptStatement_AcceptEntryIndexQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='accept_entry_index_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getAcceptEntryIndexQ();

	/**
	 * Sets the value of the '{@link Ada.AcceptStatement#getAcceptEntryIndexQ <em>Accept Entry Index Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Accept Entry Index Q</em>' containment reference.
	 * @see #getAcceptEntryIndexQ()
	 * @generated
	 */
	void setAcceptEntryIndexQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Accept Parameters Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Accept Parameters Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Accept Parameters Ql</em>' containment reference.
	 * @see #setAcceptParametersQl(ParameterSpecificationList)
	 * @see Ada.AdaPackage#getAcceptStatement_AcceptParametersQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='accept_parameters_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ParameterSpecificationList getAcceptParametersQl();

	/**
	 * Sets the value of the '{@link Ada.AcceptStatement#getAcceptParametersQl <em>Accept Parameters Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Accept Parameters Ql</em>' containment reference.
	 * @see #getAcceptParametersQl()
	 * @generated
	 */
	void setAcceptParametersQl(ParameterSpecificationList value);

	/**
	 * Returns the value of the '<em><b>Accept Body Statements Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Accept Body Statements Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Accept Body Statements Ql</em>' containment reference.
	 * @see #setAcceptBodyStatementsQl(StatementList)
	 * @see Ada.AdaPackage#getAcceptStatement_AcceptBodyStatementsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='accept_body_statements_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	StatementList getAcceptBodyStatementsQl();

	/**
	 * Sets the value of the '{@link Ada.AcceptStatement#getAcceptBodyStatementsQl <em>Accept Body Statements Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Accept Body Statements Ql</em>' containment reference.
	 * @see #getAcceptBodyStatementsQl()
	 * @generated
	 */
	void setAcceptBodyStatementsQl(StatementList value);

	/**
	 * Returns the value of the '<em><b>Accept Body Exception Handlers Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Accept Body Exception Handlers Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Accept Body Exception Handlers Ql</em>' containment reference.
	 * @see #setAcceptBodyExceptionHandlersQl(StatementList)
	 * @see Ada.AdaPackage#getAcceptStatement_AcceptBodyExceptionHandlersQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='accept_body_exception_handlers_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	StatementList getAcceptBodyExceptionHandlersQl();

	/**
	 * Sets the value of the '{@link Ada.AcceptStatement#getAcceptBodyExceptionHandlersQl <em>Accept Body Exception Handlers Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Accept Body Exception Handlers Ql</em>' containment reference.
	 * @see #getAcceptBodyExceptionHandlersQl()
	 * @generated
	 */
	void setAcceptBodyExceptionHandlersQl(StatementList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getAcceptStatement_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.AcceptStatement#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // AcceptStatement
