with Crypto.Types; use Crypto.Types;
with DUKPT; use DUKPT;
with DUKPT.Utilities; use DUKPT.Utilities;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Containers.Ordered_Sets;

procedure DUKPT_Test_Collisions
is
   pragma SPARK_Mode (Off);

   -- If this is larger than the self imposed limit of 21 bit counter in
   -- DUKPT.Utilities then DUKPT will kill itself and keys will match falsely
   Iterations : constant Integer := 2097151;

   type String_32 is new String(1 .. 32);
   type String_32_Array is array(Natural range 0..Iterations - 1) of String_32;
   type String_32_Array_Access is access String_32_Array;

   package String_Sets is new Ada.Containers.Ordered_Sets(String_32);
   use String_Sets;

   TestData_BDK  : B_Block128 := (16#01#, 16#23#, 16#45#, 16#67#,
                                  16#89#, 16#AB#, 16#CD#, 16#EF#,
                                  16#FE#, 16#DC#, 16#BA#, 16#98#,
                                  16#76#, 16#54#, 16#32#, 16#10#);
   TestData_IKSN : B_Block128 := (16#00#, 16#00#, 16#00#, 16#00#,
                                  16#00#, 16#00#, 16#00#, 16#00#,
                                  16#00#, 16#00#, 16#00#, 16#00#,
                                  16#00#, 16#00#, 16#00#, 16#00#);
   IPEK          : B_Block128;
   KSN           : B_Block128;
   Encrypt_Key_S : B_Block128;
   MAC_Key_S     : B_Block128;
   EK_Strings : String_32_Array_Access := new String_32_Array;
   MAC_Strings : String_32_Array_Access := new String_32_Array;
   Unique_EK : Set;
   Unique_MAC : Set;
   --Set_Cur : Cursor;

   -- How to convert the blocks of bytes into Strings.
   function Block128_To_String(Block_Data : Crypto.Types.B_Block128) return String
   is
      Return_String : String(1 .. 32);
      Count : Integer := 1;
   begin
      for B in 0 .. 15 loop
         for A of DUKPT.Utilities.To_Hex(Natural(Block_Data(B))) loop
            Return_String(Count) := A;
            Count := Count + 1;
         end loop;
      end loop;
      return Return_String;
   end Block128_To_String;

begin
   DUKPT.Derive_IPEK(TestData_BDK, TestData_IKSN, IPEK);
   DUKPT.Load_Initial_Keys(IPEK, TestData_IKSN);
   for A in 0 .. Iterations - 1 loop
      DUKPT.Get_Sender_Transaction_Keys(KSN, Encrypt_Key_S, MAC_Key_S);
      EK_Strings(A) := String_32(Block128_To_String(Encrypt_Key_S));
      MAC_Strings(A) := String_32(Block128_To_String(MAC_Key_S));
   end loop;

   -- Fill Encryption Key Set
   for I in EK_Strings'Range loop
      begin
         Unique_EK.Insert(EK_Strings(I));
      exception
         when Constraint_Error => Put_Line("EK Nope : " & String(EK_Strings(I)));
      end;
   end loop;

   -- Fill MAC Key Set
   for I in MAC_Strings'Range loop
      begin
         Unique_MAC.Insert(MAC_Strings(I));
      exception
         when Constraint_Error => Put_Line("MAC Nope: " & String(MAC_Strings(I)));
      end;
   end loop;

   -- To print all the unique keys - DO NOT enable when many iterations.
--     Set_Cur := Unique_EK.First;
--     loop
--        Put_Line(Item => String(Element(Set_Cur)));
--        exit when Set_Cur = Unique_EK.Last;
--        Set_Cur := Next(Set_Cur);
--     end loop;

end DUKPT_Test_Collisions;
