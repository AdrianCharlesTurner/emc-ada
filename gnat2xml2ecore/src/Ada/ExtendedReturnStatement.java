/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Extended Return Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.ExtendedReturnStatement#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.ExtendedReturnStatement#getLabelNamesQl <em>Label Names Ql</em>}</li>
 *   <li>{@link Ada.ExtendedReturnStatement#getReturnObjectDeclarationQ <em>Return Object Declaration Q</em>}</li>
 *   <li>{@link Ada.ExtendedReturnStatement#getExtendedReturnStatementsQl <em>Extended Return Statements Ql</em>}</li>
 *   <li>{@link Ada.ExtendedReturnStatement#getExtendedReturnExceptionHandlersQl <em>Extended Return Exception Handlers Ql</em>}</li>
 *   <li>{@link Ada.ExtendedReturnStatement#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getExtendedReturnStatement()
 * @model extendedMetaData="name='Extended_Return_Statement' kind='elementOnly'"
 * @generated
 */
public interface ExtendedReturnStatement extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getExtendedReturnStatement_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.ExtendedReturnStatement#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Label Names Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label Names Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label Names Ql</em>' containment reference.
	 * @see #setLabelNamesQl(DefiningNameList)
	 * @see Ada.AdaPackage#getExtendedReturnStatement_LabelNamesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='label_names_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameList getLabelNamesQl();

	/**
	 * Sets the value of the '{@link Ada.ExtendedReturnStatement#getLabelNamesQl <em>Label Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label Names Ql</em>' containment reference.
	 * @see #getLabelNamesQl()
	 * @generated
	 */
	void setLabelNamesQl(DefiningNameList value);

	/**
	 * Returns the value of the '<em><b>Return Object Declaration Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Return Object Declaration Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Return Object Declaration Q</em>' containment reference.
	 * @see #setReturnObjectDeclarationQ(DeclarationClass)
	 * @see Ada.AdaPackage#getExtendedReturnStatement_ReturnObjectDeclarationQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='return_object_declaration_q' namespace='##targetNamespace'"
	 * @generated
	 */
	DeclarationClass getReturnObjectDeclarationQ();

	/**
	 * Sets the value of the '{@link Ada.ExtendedReturnStatement#getReturnObjectDeclarationQ <em>Return Object Declaration Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Return Object Declaration Q</em>' containment reference.
	 * @see #getReturnObjectDeclarationQ()
	 * @generated
	 */
	void setReturnObjectDeclarationQ(DeclarationClass value);

	/**
	 * Returns the value of the '<em><b>Extended Return Statements Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extended Return Statements Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extended Return Statements Ql</em>' containment reference.
	 * @see #setExtendedReturnStatementsQl(StatementList)
	 * @see Ada.AdaPackage#getExtendedReturnStatement_ExtendedReturnStatementsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='extended_return_statements_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	StatementList getExtendedReturnStatementsQl();

	/**
	 * Sets the value of the '{@link Ada.ExtendedReturnStatement#getExtendedReturnStatementsQl <em>Extended Return Statements Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extended Return Statements Ql</em>' containment reference.
	 * @see #getExtendedReturnStatementsQl()
	 * @generated
	 */
	void setExtendedReturnStatementsQl(StatementList value);

	/**
	 * Returns the value of the '<em><b>Extended Return Exception Handlers Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extended Return Exception Handlers Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extended Return Exception Handlers Ql</em>' containment reference.
	 * @see #setExtendedReturnExceptionHandlersQl(ExceptionHandlerList)
	 * @see Ada.AdaPackage#getExtendedReturnStatement_ExtendedReturnExceptionHandlersQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='extended_return_exception_handlers_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ExceptionHandlerList getExtendedReturnExceptionHandlersQl();

	/**
	 * Sets the value of the '{@link Ada.ExtendedReturnStatement#getExtendedReturnExceptionHandlersQl <em>Extended Return Exception Handlers Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extended Return Exception Handlers Ql</em>' containment reference.
	 * @see #getExtendedReturnExceptionHandlersQl()
	 * @generated
	 */
	void setExtendedReturnExceptionHandlersQl(ExceptionHandlerList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getExtendedReturnStatement_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.ExtendedReturnStatement#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // ExtendedReturnStatement
