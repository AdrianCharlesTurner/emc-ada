/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.DefiningExpandedName;
import Ada.DefiningNameClass;
import Ada.NameClass;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Defining Expanded Name</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.DefiningExpandedNameImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.DefiningExpandedNameImpl#getDefiningPrefixQ <em>Defining Prefix Q</em>}</li>
 *   <li>{@link Ada.impl.DefiningExpandedNameImpl#getDefiningSelectorQ <em>Defining Selector Q</em>}</li>
 *   <li>{@link Ada.impl.DefiningExpandedNameImpl#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.impl.DefiningExpandedNameImpl#getDef <em>Def</em>}</li>
 *   <li>{@link Ada.impl.DefiningExpandedNameImpl#getDefName <em>Def Name</em>}</li>
 *   <li>{@link Ada.impl.DefiningExpandedNameImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DefiningExpandedNameImpl extends MinimalEObjectImpl.Container implements DefiningExpandedName {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getDefiningPrefixQ() <em>Defining Prefix Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefiningPrefixQ()
	 * @generated
	 * @ordered
	 */
	protected NameClass definingPrefixQ;

	/**
	 * The cached value of the '{@link #getDefiningSelectorQ() <em>Defining Selector Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefiningSelectorQ()
	 * @generated
	 * @ordered
	 */
	protected DefiningNameClass definingSelectorQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * The default value of the '{@link #getDef() <em>Def</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDef()
	 * @generated
	 * @ordered
	 */
	protected static final String DEF_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDef() <em>Def</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDef()
	 * @generated
	 * @ordered
	 */
	protected String def = DEF_EDEFAULT;

	/**
	 * The default value of the '{@link #getDefName() <em>Def Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefName()
	 * @generated
	 * @ordered
	 */
	protected static final String DEF_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDefName() <em>Def Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefName()
	 * @generated
	 * @ordered
	 */
	protected String defName = DEF_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DefiningExpandedNameImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getDefiningExpandedName();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINING_EXPANDED_NAME__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINING_EXPANDED_NAME__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINING_EXPANDED_NAME__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINING_EXPANDED_NAME__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NameClass getDefiningPrefixQ() {
		return definingPrefixQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningPrefixQ(NameClass newDefiningPrefixQ, NotificationChain msgs) {
		NameClass oldDefiningPrefixQ = definingPrefixQ;
		definingPrefixQ = newDefiningPrefixQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINING_EXPANDED_NAME__DEFINING_PREFIX_Q, oldDefiningPrefixQ, newDefiningPrefixQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningPrefixQ(NameClass newDefiningPrefixQ) {
		if (newDefiningPrefixQ != definingPrefixQ) {
			NotificationChain msgs = null;
			if (definingPrefixQ != null)
				msgs = ((InternalEObject)definingPrefixQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINING_EXPANDED_NAME__DEFINING_PREFIX_Q, null, msgs);
			if (newDefiningPrefixQ != null)
				msgs = ((InternalEObject)newDefiningPrefixQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINING_EXPANDED_NAME__DEFINING_PREFIX_Q, null, msgs);
			msgs = basicSetDefiningPrefixQ(newDefiningPrefixQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINING_EXPANDED_NAME__DEFINING_PREFIX_Q, newDefiningPrefixQ, newDefiningPrefixQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNameClass getDefiningSelectorQ() {
		return definingSelectorQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefiningSelectorQ(DefiningNameClass newDefiningSelectorQ, NotificationChain msgs) {
		DefiningNameClass oldDefiningSelectorQ = definingSelectorQ;
		definingSelectorQ = newDefiningSelectorQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINING_EXPANDED_NAME__DEFINING_SELECTOR_Q, oldDefiningSelectorQ, newDefiningSelectorQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefiningSelectorQ(DefiningNameClass newDefiningSelectorQ) {
		if (newDefiningSelectorQ != definingSelectorQ) {
			NotificationChain msgs = null;
			if (definingSelectorQ != null)
				msgs = ((InternalEObject)definingSelectorQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINING_EXPANDED_NAME__DEFINING_SELECTOR_Q, null, msgs);
			if (newDefiningSelectorQ != null)
				msgs = ((InternalEObject)newDefiningSelectorQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINING_EXPANDED_NAME__DEFINING_SELECTOR_Q, null, msgs);
			msgs = basicSetDefiningSelectorQ(newDefiningSelectorQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINING_EXPANDED_NAME__DEFINING_SELECTOR_Q, newDefiningSelectorQ, newDefiningSelectorQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINING_EXPANDED_NAME__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDef() {
		return def;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDef(String newDef) {
		String oldDef = def;
		def = newDef;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINING_EXPANDED_NAME__DEF, oldDef, def));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDefName() {
		return defName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefName(String newDefName) {
		String oldDefName = defName;
		defName = newDefName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINING_EXPANDED_NAME__DEF_NAME, oldDefName, defName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINING_EXPANDED_NAME__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.DEFINING_EXPANDED_NAME__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.DEFINING_EXPANDED_NAME__DEFINING_PREFIX_Q:
				return basicSetDefiningPrefixQ(null, msgs);
			case AdaPackage.DEFINING_EXPANDED_NAME__DEFINING_SELECTOR_Q:
				return basicSetDefiningSelectorQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.DEFINING_EXPANDED_NAME__SLOC:
				return getSloc();
			case AdaPackage.DEFINING_EXPANDED_NAME__DEFINING_PREFIX_Q:
				return getDefiningPrefixQ();
			case AdaPackage.DEFINING_EXPANDED_NAME__DEFINING_SELECTOR_Q:
				return getDefiningSelectorQ();
			case AdaPackage.DEFINING_EXPANDED_NAME__CHECKS:
				return getChecks();
			case AdaPackage.DEFINING_EXPANDED_NAME__DEF:
				return getDef();
			case AdaPackage.DEFINING_EXPANDED_NAME__DEF_NAME:
				return getDefName();
			case AdaPackage.DEFINING_EXPANDED_NAME__TYPE:
				return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.DEFINING_EXPANDED_NAME__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.DEFINING_EXPANDED_NAME__DEFINING_PREFIX_Q:
				setDefiningPrefixQ((NameClass)newValue);
				return;
			case AdaPackage.DEFINING_EXPANDED_NAME__DEFINING_SELECTOR_Q:
				setDefiningSelectorQ((DefiningNameClass)newValue);
				return;
			case AdaPackage.DEFINING_EXPANDED_NAME__CHECKS:
				setChecks((String)newValue);
				return;
			case AdaPackage.DEFINING_EXPANDED_NAME__DEF:
				setDef((String)newValue);
				return;
			case AdaPackage.DEFINING_EXPANDED_NAME__DEF_NAME:
				setDefName((String)newValue);
				return;
			case AdaPackage.DEFINING_EXPANDED_NAME__TYPE:
				setType((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.DEFINING_EXPANDED_NAME__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.DEFINING_EXPANDED_NAME__DEFINING_PREFIX_Q:
				setDefiningPrefixQ((NameClass)null);
				return;
			case AdaPackage.DEFINING_EXPANDED_NAME__DEFINING_SELECTOR_Q:
				setDefiningSelectorQ((DefiningNameClass)null);
				return;
			case AdaPackage.DEFINING_EXPANDED_NAME__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
			case AdaPackage.DEFINING_EXPANDED_NAME__DEF:
				setDef(DEF_EDEFAULT);
				return;
			case AdaPackage.DEFINING_EXPANDED_NAME__DEF_NAME:
				setDefName(DEF_NAME_EDEFAULT);
				return;
			case AdaPackage.DEFINING_EXPANDED_NAME__TYPE:
				setType(TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.DEFINING_EXPANDED_NAME__SLOC:
				return sloc != null;
			case AdaPackage.DEFINING_EXPANDED_NAME__DEFINING_PREFIX_Q:
				return definingPrefixQ != null;
			case AdaPackage.DEFINING_EXPANDED_NAME__DEFINING_SELECTOR_Q:
				return definingSelectorQ != null;
			case AdaPackage.DEFINING_EXPANDED_NAME__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
			case AdaPackage.DEFINING_EXPANDED_NAME__DEF:
				return DEF_EDEFAULT == null ? def != null : !DEF_EDEFAULT.equals(def);
			case AdaPackage.DEFINING_EXPANDED_NAME__DEF_NAME:
				return DEF_NAME_EDEFAULT == null ? defName != null : !DEF_NAME_EDEFAULT.equals(defName);
			case AdaPackage.DEFINING_EXPANDED_NAME__TYPE:
				return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(", def: ");
		result.append(def);
		result.append(", defName: ");
		result.append(defName);
		result.append(", type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //DefiningExpandedNameImpl
