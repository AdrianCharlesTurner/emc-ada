# emc-ada

Similar to emc-czt these eclipse projects will allow manipulation of textual ada source files as Ecore models.

---

# Dependancies

  * org.eclipse.epsilon.eol.engine,
  * org.eclipse.emf,
  * org.eclipse.emf.ecore,
  * org.eclipse.emf.ecore.xmi,
  * org.eclipse.emf.ecore.change,
  * org.eclipse.epsilon.emc.emf;visibility:=reexport,
  * org.eclipse.xsd,
  * org.eclipse.core.runtime,
  * org.eclipse.core.resources
  * org.eclipse.epsilon.common.dt,
  * org.eclipse.jface,
  * org.eclipse.emf,
  * org.eclipse.emf.ecore,
  * org.eclipse.epsilon.emc.ada