/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Extension Aggregate</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.ExtensionAggregate#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.ExtensionAggregate#getExtensionAggregateExpressionQ <em>Extension Aggregate Expression Q</em>}</li>
 *   <li>{@link Ada.ExtensionAggregate#getRecordComponentAssociationsQl <em>Record Component Associations Ql</em>}</li>
 *   <li>{@link Ada.ExtensionAggregate#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.ExtensionAggregate#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getExtensionAggregate()
 * @model extendedMetaData="name='Extension_Aggregate' kind='elementOnly'"
 * @generated
 */
public interface ExtensionAggregate extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getExtensionAggregate_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.ExtensionAggregate#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Extension Aggregate Expression Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extension Aggregate Expression Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extension Aggregate Expression Q</em>' containment reference.
	 * @see #setExtensionAggregateExpressionQ(ExpressionClass)
	 * @see Ada.AdaPackage#getExtensionAggregate_ExtensionAggregateExpressionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='extension_aggregate_expression_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getExtensionAggregateExpressionQ();

	/**
	 * Sets the value of the '{@link Ada.ExtensionAggregate#getExtensionAggregateExpressionQ <em>Extension Aggregate Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extension Aggregate Expression Q</em>' containment reference.
	 * @see #getExtensionAggregateExpressionQ()
	 * @generated
	 */
	void setExtensionAggregateExpressionQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Record Component Associations Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Component Associations Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Component Associations Ql</em>' containment reference.
	 * @see #setRecordComponentAssociationsQl(AssociationList)
	 * @see Ada.AdaPackage#getExtensionAggregate_RecordComponentAssociationsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='record_component_associations_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	AssociationList getRecordComponentAssociationsQl();

	/**
	 * Sets the value of the '{@link Ada.ExtensionAggregate#getRecordComponentAssociationsQl <em>Record Component Associations Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Record Component Associations Ql</em>' containment reference.
	 * @see #getRecordComponentAssociationsQl()
	 * @generated
	 */
	void setRecordComponentAssociationsQl(AssociationList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getExtensionAggregate_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.ExtensionAggregate#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see Ada.AdaPackage#getExtensionAggregate_Type()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='type' namespace='##targetNamespace'"
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link Ada.ExtensionAggregate#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

} // ExtensionAggregate
