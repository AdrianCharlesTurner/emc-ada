/**
 */
package Ada.util;

import Ada.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see Ada.AdaPackage
 * @generated
 */
public class AdaAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static AdaPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdaAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = AdaPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AdaSwitch<Adapter> modelSwitch =
		new AdaSwitch<Adapter>() {
			@Override
			public Adapter caseAbortStatement(AbortStatement object) {
				return createAbortStatementAdapter();
			}
			@Override
			public Adapter caseAbsOperator(AbsOperator object) {
				return createAbsOperatorAdapter();
			}
			@Override
			public Adapter caseAbstract(Abstract object) {
				return createAbstractAdapter();
			}
			@Override
			public Adapter caseAcceptStatement(AcceptStatement object) {
				return createAcceptStatementAdapter();
			}
			@Override
			public Adapter caseAccessAttribute(AccessAttribute object) {
				return createAccessAttributeAdapter();
			}
			@Override
			public Adapter caseAccessToConstant(AccessToConstant object) {
				return createAccessToConstantAdapter();
			}
			@Override
			public Adapter caseAccessToFunction(AccessToFunction object) {
				return createAccessToFunctionAdapter();
			}
			@Override
			public Adapter caseAccessToProcedure(AccessToProcedure object) {
				return createAccessToProcedureAdapter();
			}
			@Override
			public Adapter caseAccessToProtectedFunction(AccessToProtectedFunction object) {
				return createAccessToProtectedFunctionAdapter();
			}
			@Override
			public Adapter caseAccessToProtectedProcedure(AccessToProtectedProcedure object) {
				return createAccessToProtectedProcedureAdapter();
			}
			@Override
			public Adapter caseAccessToVariable(AccessToVariable object) {
				return createAccessToVariableAdapter();
			}
			@Override
			public Adapter caseAddressAttribute(AddressAttribute object) {
				return createAddressAttributeAdapter();
			}
			@Override
			public Adapter caseAdjacentAttribute(AdjacentAttribute object) {
				return createAdjacentAttributeAdapter();
			}
			@Override
			public Adapter caseAftAttribute(AftAttribute object) {
				return createAftAttributeAdapter();
			}
			@Override
			public Adapter caseAliased(Aliased object) {
				return createAliasedAdapter();
			}
			@Override
			public Adapter caseAlignmentAttribute(AlignmentAttribute object) {
				return createAlignmentAttributeAdapter();
			}
			@Override
			public Adapter caseAllCallsRemotePragma(AllCallsRemotePragma object) {
				return createAllCallsRemotePragmaAdapter();
			}
			@Override
			public Adapter caseAllocationFromQualifiedExpression(AllocationFromQualifiedExpression object) {
				return createAllocationFromQualifiedExpressionAdapter();
			}
			@Override
			public Adapter caseAllocationFromSubtype(AllocationFromSubtype object) {
				return createAllocationFromSubtypeAdapter();
			}
			@Override
			public Adapter caseAndOperator(AndOperator object) {
				return createAndOperatorAdapter();
			}
			@Override
			public Adapter caseAndThenShortCircuit(AndThenShortCircuit object) {
				return createAndThenShortCircuitAdapter();
			}
			@Override
			public Adapter caseAnonymousAccessToConstant(AnonymousAccessToConstant object) {
				return createAnonymousAccessToConstantAdapter();
			}
			@Override
			public Adapter caseAnonymousAccessToFunction(AnonymousAccessToFunction object) {
				return createAnonymousAccessToFunctionAdapter();
			}
			@Override
			public Adapter caseAnonymousAccessToProcedure(AnonymousAccessToProcedure object) {
				return createAnonymousAccessToProcedureAdapter();
			}
			@Override
			public Adapter caseAnonymousAccessToProtectedFunction(AnonymousAccessToProtectedFunction object) {
				return createAnonymousAccessToProtectedFunctionAdapter();
			}
			@Override
			public Adapter caseAnonymousAccessToProtectedProcedure(AnonymousAccessToProtectedProcedure object) {
				return createAnonymousAccessToProtectedProcedureAdapter();
			}
			@Override
			public Adapter caseAnonymousAccessToVariable(AnonymousAccessToVariable object) {
				return createAnonymousAccessToVariableAdapter();
			}
			@Override
			public Adapter caseArrayComponentAssociation(ArrayComponentAssociation object) {
				return createArrayComponentAssociationAdapter();
			}
			@Override
			public Adapter caseAspectSpecification(AspectSpecification object) {
				return createAspectSpecificationAdapter();
			}
			@Override
			public Adapter caseAssertionPolicyPragma(AssertionPolicyPragma object) {
				return createAssertionPolicyPragmaAdapter();
			}
			@Override
			public Adapter caseAssertPragma(AssertPragma object) {
				return createAssertPragmaAdapter();
			}
			@Override
			public Adapter caseAssignmentStatement(AssignmentStatement object) {
				return createAssignmentStatementAdapter();
			}
			@Override
			public Adapter caseAssociationClass(AssociationClass object) {
				return createAssociationClassAdapter();
			}
			@Override
			public Adapter caseAssociationList(AssociationList object) {
				return createAssociationListAdapter();
			}
			@Override
			public Adapter caseAsynchronousPragma(AsynchronousPragma object) {
				return createAsynchronousPragmaAdapter();
			}
			@Override
			public Adapter caseAsynchronousSelectStatement(AsynchronousSelectStatement object) {
				return createAsynchronousSelectStatementAdapter();
			}
			@Override
			public Adapter caseAtClause(AtClause object) {
				return createAtClauseAdapter();
			}
			@Override
			public Adapter caseAtomicComponentsPragma(AtomicComponentsPragma object) {
				return createAtomicComponentsPragmaAdapter();
			}
			@Override
			public Adapter caseAtomicPragma(AtomicPragma object) {
				return createAtomicPragmaAdapter();
			}
			@Override
			public Adapter caseAttachHandlerPragma(AttachHandlerPragma object) {
				return createAttachHandlerPragmaAdapter();
			}
			@Override
			public Adapter caseAttributeDefinitionClause(AttributeDefinitionClause object) {
				return createAttributeDefinitionClauseAdapter();
			}
			@Override
			public Adapter caseBaseAttribute(BaseAttribute object) {
				return createBaseAttributeAdapter();
			}
			@Override
			public Adapter caseBitOrderAttribute(BitOrderAttribute object) {
				return createBitOrderAttributeAdapter();
			}
			@Override
			public Adapter caseBlockStatement(BlockStatement object) {
				return createBlockStatementAdapter();
			}
			@Override
			public Adapter caseBodyVersionAttribute(BodyVersionAttribute object) {
				return createBodyVersionAttributeAdapter();
			}
			@Override
			public Adapter caseBoxExpression(BoxExpression object) {
				return createBoxExpressionAdapter();
			}
			@Override
			public Adapter caseCallableAttribute(CallableAttribute object) {
				return createCallableAttributeAdapter();
			}
			@Override
			public Adapter caseCallerAttribute(CallerAttribute object) {
				return createCallerAttributeAdapter();
			}
			@Override
			public Adapter caseCaseExpression(CaseExpression object) {
				return createCaseExpressionAdapter();
			}
			@Override
			public Adapter caseCaseExpressionPath(CaseExpressionPath object) {
				return createCaseExpressionPathAdapter();
			}
			@Override
			public Adapter caseCasePath(CasePath object) {
				return createCasePathAdapter();
			}
			@Override
			public Adapter caseCaseStatement(CaseStatement object) {
				return createCaseStatementAdapter();
			}
			@Override
			public Adapter caseCeilingAttribute(CeilingAttribute object) {
				return createCeilingAttributeAdapter();
			}
			@Override
			public Adapter caseCharacterLiteral(CharacterLiteral object) {
				return createCharacterLiteralAdapter();
			}
			@Override
			public Adapter caseChoiceParameterSpecification(ChoiceParameterSpecification object) {
				return createChoiceParameterSpecificationAdapter();
			}
			@Override
			public Adapter caseClassAttribute(ClassAttribute object) {
				return createClassAttributeAdapter();
			}
			@Override
			public Adapter caseCodeStatement(CodeStatement object) {
				return createCodeStatementAdapter();
			}
			@Override
			public Adapter caseComment(Comment object) {
				return createCommentAdapter();
			}
			@Override
			public Adapter caseCompilationUnit(CompilationUnit object) {
				return createCompilationUnitAdapter();
			}
			@Override
			public Adapter caseComponentClause(ComponentClause object) {
				return createComponentClauseAdapter();
			}
			@Override
			public Adapter caseComponentClauseList(ComponentClauseList object) {
				return createComponentClauseListAdapter();
			}
			@Override
			public Adapter caseComponentDeclaration(ComponentDeclaration object) {
				return createComponentDeclarationAdapter();
			}
			@Override
			public Adapter caseComponentDefinition(ComponentDefinition object) {
				return createComponentDefinitionAdapter();
			}
			@Override
			public Adapter caseComponentSizeAttribute(ComponentSizeAttribute object) {
				return createComponentSizeAttributeAdapter();
			}
			@Override
			public Adapter caseComposeAttribute(ComposeAttribute object) {
				return createComposeAttributeAdapter();
			}
			@Override
			public Adapter caseConcatenateOperator(ConcatenateOperator object) {
				return createConcatenateOperatorAdapter();
			}
			@Override
			public Adapter caseConditionalEntryCallStatement(ConditionalEntryCallStatement object) {
				return createConditionalEntryCallStatementAdapter();
			}
			@Override
			public Adapter caseConstantDeclaration(ConstantDeclaration object) {
				return createConstantDeclarationAdapter();
			}
			@Override
			public Adapter caseConstrainedArrayDefinition(ConstrainedArrayDefinition object) {
				return createConstrainedArrayDefinitionAdapter();
			}
			@Override
			public Adapter caseConstrainedAttribute(ConstrainedAttribute object) {
				return createConstrainedAttributeAdapter();
			}
			@Override
			public Adapter caseConstraintClass(ConstraintClass object) {
				return createConstraintClassAdapter();
			}
			@Override
			public Adapter caseContextClauseClass(ContextClauseClass object) {
				return createContextClauseClassAdapter();
			}
			@Override
			public Adapter caseContextClauseList(ContextClauseList object) {
				return createContextClauseListAdapter();
			}
			@Override
			public Adapter caseControlledPragma(ControlledPragma object) {
				return createControlledPragmaAdapter();
			}
			@Override
			public Adapter caseConventionPragma(ConventionPragma object) {
				return createConventionPragmaAdapter();
			}
			@Override
			public Adapter caseCopySignAttribute(CopySignAttribute object) {
				return createCopySignAttributeAdapter();
			}
			@Override
			public Adapter caseCountAttribute(CountAttribute object) {
				return createCountAttributeAdapter();
			}
			@Override
			public Adapter caseCpuPragma(CpuPragma object) {
				return createCpuPragmaAdapter();
			}
			@Override
			public Adapter caseDecimalFixedPointDefinition(DecimalFixedPointDefinition object) {
				return createDecimalFixedPointDefinitionAdapter();
			}
			@Override
			public Adapter caseDeclarationClass(DeclarationClass object) {
				return createDeclarationClassAdapter();
			}
			@Override
			public Adapter caseDeclarationList(DeclarationList object) {
				return createDeclarationListAdapter();
			}
			@Override
			public Adapter caseDeclarativeItemClass(DeclarativeItemClass object) {
				return createDeclarativeItemClassAdapter();
			}
			@Override
			public Adapter caseDeclarativeItemList(DeclarativeItemList object) {
				return createDeclarativeItemListAdapter();
			}
			@Override
			public Adapter caseDefaultStoragePoolPragma(DefaultStoragePoolPragma object) {
				return createDefaultStoragePoolPragmaAdapter();
			}
			@Override
			public Adapter caseDeferredConstantDeclaration(DeferredConstantDeclaration object) {
				return createDeferredConstantDeclarationAdapter();
			}
			@Override
			public Adapter caseDefiningAbsOperator(DefiningAbsOperator object) {
				return createDefiningAbsOperatorAdapter();
			}
			@Override
			public Adapter caseDefiningAndOperator(DefiningAndOperator object) {
				return createDefiningAndOperatorAdapter();
			}
			@Override
			public Adapter caseDefiningCharacterLiteral(DefiningCharacterLiteral object) {
				return createDefiningCharacterLiteralAdapter();
			}
			@Override
			public Adapter caseDefiningConcatenateOperator(DefiningConcatenateOperator object) {
				return createDefiningConcatenateOperatorAdapter();
			}
			@Override
			public Adapter caseDefiningDivideOperator(DefiningDivideOperator object) {
				return createDefiningDivideOperatorAdapter();
			}
			@Override
			public Adapter caseDefiningEnumerationLiteral(DefiningEnumerationLiteral object) {
				return createDefiningEnumerationLiteralAdapter();
			}
			@Override
			public Adapter caseDefiningEqualOperator(DefiningEqualOperator object) {
				return createDefiningEqualOperatorAdapter();
			}
			@Override
			public Adapter caseDefiningExpandedName(DefiningExpandedName object) {
				return createDefiningExpandedNameAdapter();
			}
			@Override
			public Adapter caseDefiningExponentiateOperator(DefiningExponentiateOperator object) {
				return createDefiningExponentiateOperatorAdapter();
			}
			@Override
			public Adapter caseDefiningGreaterThanOperator(DefiningGreaterThanOperator object) {
				return createDefiningGreaterThanOperatorAdapter();
			}
			@Override
			public Adapter caseDefiningGreaterThanOrEqualOperator(DefiningGreaterThanOrEqualOperator object) {
				return createDefiningGreaterThanOrEqualOperatorAdapter();
			}
			@Override
			public Adapter caseDefiningIdentifier(DefiningIdentifier object) {
				return createDefiningIdentifierAdapter();
			}
			@Override
			public Adapter caseDefiningLessThanOperator(DefiningLessThanOperator object) {
				return createDefiningLessThanOperatorAdapter();
			}
			@Override
			public Adapter caseDefiningLessThanOrEqualOperator(DefiningLessThanOrEqualOperator object) {
				return createDefiningLessThanOrEqualOperatorAdapter();
			}
			@Override
			public Adapter caseDefiningMinusOperator(DefiningMinusOperator object) {
				return createDefiningMinusOperatorAdapter();
			}
			@Override
			public Adapter caseDefiningModOperator(DefiningModOperator object) {
				return createDefiningModOperatorAdapter();
			}
			@Override
			public Adapter caseDefiningMultiplyOperator(DefiningMultiplyOperator object) {
				return createDefiningMultiplyOperatorAdapter();
			}
			@Override
			public Adapter caseDefiningNameClass(DefiningNameClass object) {
				return createDefiningNameClassAdapter();
			}
			@Override
			public Adapter caseDefiningNameList(DefiningNameList object) {
				return createDefiningNameListAdapter();
			}
			@Override
			public Adapter caseDefiningNotEqualOperator(DefiningNotEqualOperator object) {
				return createDefiningNotEqualOperatorAdapter();
			}
			@Override
			public Adapter caseDefiningNotOperator(DefiningNotOperator object) {
				return createDefiningNotOperatorAdapter();
			}
			@Override
			public Adapter caseDefiningOrOperator(DefiningOrOperator object) {
				return createDefiningOrOperatorAdapter();
			}
			@Override
			public Adapter caseDefiningPlusOperator(DefiningPlusOperator object) {
				return createDefiningPlusOperatorAdapter();
			}
			@Override
			public Adapter caseDefiningRemOperator(DefiningRemOperator object) {
				return createDefiningRemOperatorAdapter();
			}
			@Override
			public Adapter caseDefiningUnaryMinusOperator(DefiningUnaryMinusOperator object) {
				return createDefiningUnaryMinusOperatorAdapter();
			}
			@Override
			public Adapter caseDefiningUnaryPlusOperator(DefiningUnaryPlusOperator object) {
				return createDefiningUnaryPlusOperatorAdapter();
			}
			@Override
			public Adapter caseDefiningXorOperator(DefiningXorOperator object) {
				return createDefiningXorOperatorAdapter();
			}
			@Override
			public Adapter caseDefiniteAttribute(DefiniteAttribute object) {
				return createDefiniteAttributeAdapter();
			}
			@Override
			public Adapter caseDefinitionClass(DefinitionClass object) {
				return createDefinitionClassAdapter();
			}
			@Override
			public Adapter caseDefinitionList(DefinitionList object) {
				return createDefinitionListAdapter();
			}
			@Override
			public Adapter caseDelayRelativeStatement(DelayRelativeStatement object) {
				return createDelayRelativeStatementAdapter();
			}
			@Override
			public Adapter caseDelayUntilStatement(DelayUntilStatement object) {
				return createDelayUntilStatementAdapter();
			}
			@Override
			public Adapter caseDeltaAttribute(DeltaAttribute object) {
				return createDeltaAttributeAdapter();
			}
			@Override
			public Adapter caseDeltaConstraint(DeltaConstraint object) {
				return createDeltaConstraintAdapter();
			}
			@Override
			public Adapter caseDenormAttribute(DenormAttribute object) {
				return createDenormAttributeAdapter();
			}
			@Override
			public Adapter caseDerivedRecordExtensionDefinition(DerivedRecordExtensionDefinition object) {
				return createDerivedRecordExtensionDefinitionAdapter();
			}
			@Override
			public Adapter caseDerivedTypeDefinition(DerivedTypeDefinition object) {
				return createDerivedTypeDefinitionAdapter();
			}
			@Override
			public Adapter caseDetectBlockingPragma(DetectBlockingPragma object) {
				return createDetectBlockingPragmaAdapter();
			}
			@Override
			public Adapter caseDigitsAttribute(DigitsAttribute object) {
				return createDigitsAttributeAdapter();
			}
			@Override
			public Adapter caseDigitsConstraint(DigitsConstraint object) {
				return createDigitsConstraintAdapter();
			}
			@Override
			public Adapter caseDiscardNamesPragma(DiscardNamesPragma object) {
				return createDiscardNamesPragmaAdapter();
			}
			@Override
			public Adapter caseDiscreteRangeAttributeReference(DiscreteRangeAttributeReference object) {
				return createDiscreteRangeAttributeReferenceAdapter();
			}
			@Override
			public Adapter caseDiscreteRangeAttributeReferenceAsSubtypeDefinition(DiscreteRangeAttributeReferenceAsSubtypeDefinition object) {
				return createDiscreteRangeAttributeReferenceAsSubtypeDefinitionAdapter();
			}
			@Override
			public Adapter caseDiscreteRangeClass(DiscreteRangeClass object) {
				return createDiscreteRangeClassAdapter();
			}
			@Override
			public Adapter caseDiscreteRangeList(DiscreteRangeList object) {
				return createDiscreteRangeListAdapter();
			}
			@Override
			public Adapter caseDiscreteSimpleExpressionRange(DiscreteSimpleExpressionRange object) {
				return createDiscreteSimpleExpressionRangeAdapter();
			}
			@Override
			public Adapter caseDiscreteSimpleExpressionRangeAsSubtypeDefinition(DiscreteSimpleExpressionRangeAsSubtypeDefinition object) {
				return createDiscreteSimpleExpressionRangeAsSubtypeDefinitionAdapter();
			}
			@Override
			public Adapter caseDiscreteSubtypeDefinitionClass(DiscreteSubtypeDefinitionClass object) {
				return createDiscreteSubtypeDefinitionClassAdapter();
			}
			@Override
			public Adapter caseDiscreteSubtypeIndication(DiscreteSubtypeIndication object) {
				return createDiscreteSubtypeIndicationAdapter();
			}
			@Override
			public Adapter caseDiscreteSubtypeIndicationAsSubtypeDefinition(DiscreteSubtypeIndicationAsSubtypeDefinition object) {
				return createDiscreteSubtypeIndicationAsSubtypeDefinitionAdapter();
			}
			@Override
			public Adapter caseDiscriminantAssociation(DiscriminantAssociation object) {
				return createDiscriminantAssociationAdapter();
			}
			@Override
			public Adapter caseDiscriminantAssociationList(DiscriminantAssociationList object) {
				return createDiscriminantAssociationListAdapter();
			}
			@Override
			public Adapter caseDiscriminantConstraint(DiscriminantConstraint object) {
				return createDiscriminantConstraintAdapter();
			}
			@Override
			public Adapter caseDiscriminantSpecification(DiscriminantSpecification object) {
				return createDiscriminantSpecificationAdapter();
			}
			@Override
			public Adapter caseDiscriminantSpecificationList(DiscriminantSpecificationList object) {
				return createDiscriminantSpecificationListAdapter();
			}
			@Override
			public Adapter caseDispatchingDomainPragma(DispatchingDomainPragma object) {
				return createDispatchingDomainPragmaAdapter();
			}
			@Override
			public Adapter caseDivideOperator(DivideOperator object) {
				return createDivideOperatorAdapter();
			}
			@Override
			public Adapter caseDocumentRoot(DocumentRoot object) {
				return createDocumentRootAdapter();
			}
			@Override
			public Adapter caseElaborateAllPragma(ElaborateAllPragma object) {
				return createElaborateAllPragmaAdapter();
			}
			@Override
			public Adapter caseElaborateBodyPragma(ElaborateBodyPragma object) {
				return createElaborateBodyPragmaAdapter();
			}
			@Override
			public Adapter caseElaboratePragma(ElaboratePragma object) {
				return createElaboratePragmaAdapter();
			}
			@Override
			public Adapter caseElementClass(ElementClass object) {
				return createElementClassAdapter();
			}
			@Override
			public Adapter caseElementIteratorSpecification(ElementIteratorSpecification object) {
				return createElementIteratorSpecificationAdapter();
			}
			@Override
			public Adapter caseElementList(ElementList object) {
				return createElementListAdapter();
			}
			@Override
			public Adapter caseElseExpressionPath(ElseExpressionPath object) {
				return createElseExpressionPathAdapter();
			}
			@Override
			public Adapter caseElsePath(ElsePath object) {
				return createElsePathAdapter();
			}
			@Override
			public Adapter caseElsifExpressionPath(ElsifExpressionPath object) {
				return createElsifExpressionPathAdapter();
			}
			@Override
			public Adapter caseElsifPath(ElsifPath object) {
				return createElsifPathAdapter();
			}
			@Override
			public Adapter caseEntryBodyDeclaration(EntryBodyDeclaration object) {
				return createEntryBodyDeclarationAdapter();
			}
			@Override
			public Adapter caseEntryCallStatement(EntryCallStatement object) {
				return createEntryCallStatementAdapter();
			}
			@Override
			public Adapter caseEntryDeclaration(EntryDeclaration object) {
				return createEntryDeclarationAdapter();
			}
			@Override
			public Adapter caseEntryIndexSpecification(EntryIndexSpecification object) {
				return createEntryIndexSpecificationAdapter();
			}
			@Override
			public Adapter caseEnumerationLiteral(EnumerationLiteral object) {
				return createEnumerationLiteralAdapter();
			}
			@Override
			public Adapter caseEnumerationLiteralSpecification(EnumerationLiteralSpecification object) {
				return createEnumerationLiteralSpecificationAdapter();
			}
			@Override
			public Adapter caseEnumerationRepresentationClause(EnumerationRepresentationClause object) {
				return createEnumerationRepresentationClauseAdapter();
			}
			@Override
			public Adapter caseEnumerationTypeDefinition(EnumerationTypeDefinition object) {
				return createEnumerationTypeDefinitionAdapter();
			}
			@Override
			public Adapter caseEqualOperator(EqualOperator object) {
				return createEqualOperatorAdapter();
			}
			@Override
			public Adapter caseExceptionDeclaration(ExceptionDeclaration object) {
				return createExceptionDeclarationAdapter();
			}
			@Override
			public Adapter caseExceptionHandler(ExceptionHandler object) {
				return createExceptionHandlerAdapter();
			}
			@Override
			public Adapter caseExceptionHandlerList(ExceptionHandlerList object) {
				return createExceptionHandlerListAdapter();
			}
			@Override
			public Adapter caseExceptionRenamingDeclaration(ExceptionRenamingDeclaration object) {
				return createExceptionRenamingDeclarationAdapter();
			}
			@Override
			public Adapter caseExitStatement(ExitStatement object) {
				return createExitStatementAdapter();
			}
			@Override
			public Adapter caseExplicitDereference(ExplicitDereference object) {
				return createExplicitDereferenceAdapter();
			}
			@Override
			public Adapter caseExponentAttribute(ExponentAttribute object) {
				return createExponentAttributeAdapter();
			}
			@Override
			public Adapter caseExponentiateOperator(ExponentiateOperator object) {
				return createExponentiateOperatorAdapter();
			}
			@Override
			public Adapter caseExportPragma(ExportPragma object) {
				return createExportPragmaAdapter();
			}
			@Override
			public Adapter caseExpressionClass(ExpressionClass object) {
				return createExpressionClassAdapter();
			}
			@Override
			public Adapter caseExpressionFunctionDeclaration(ExpressionFunctionDeclaration object) {
				return createExpressionFunctionDeclarationAdapter();
			}
			@Override
			public Adapter caseExpressionList(ExpressionList object) {
				return createExpressionListAdapter();
			}
			@Override
			public Adapter caseExtendedReturnStatement(ExtendedReturnStatement object) {
				return createExtendedReturnStatementAdapter();
			}
			@Override
			public Adapter caseExtensionAggregate(ExtensionAggregate object) {
				return createExtensionAggregateAdapter();
			}
			@Override
			public Adapter caseExternalTagAttribute(ExternalTagAttribute object) {
				return createExternalTagAttributeAdapter();
			}
			@Override
			public Adapter caseFirstAttribute(FirstAttribute object) {
				return createFirstAttributeAdapter();
			}
			@Override
			public Adapter caseFirstBitAttribute(FirstBitAttribute object) {
				return createFirstBitAttributeAdapter();
			}
			@Override
			public Adapter caseFloatingPointDefinition(FloatingPointDefinition object) {
				return createFloatingPointDefinitionAdapter();
			}
			@Override
			public Adapter caseFloorAttribute(FloorAttribute object) {
				return createFloorAttributeAdapter();
			}
			@Override
			public Adapter caseForAllQuantifiedExpression(ForAllQuantifiedExpression object) {
				return createForAllQuantifiedExpressionAdapter();
			}
			@Override
			public Adapter caseForeAttribute(ForeAttribute object) {
				return createForeAttributeAdapter();
			}
			@Override
			public Adapter caseForLoopStatement(ForLoopStatement object) {
				return createForLoopStatementAdapter();
			}
			@Override
			public Adapter caseFormalAccessToConstant(FormalAccessToConstant object) {
				return createFormalAccessToConstantAdapter();
			}
			@Override
			public Adapter caseFormalAccessToFunction(FormalAccessToFunction object) {
				return createFormalAccessToFunctionAdapter();
			}
			@Override
			public Adapter caseFormalAccessToProcedure(FormalAccessToProcedure object) {
				return createFormalAccessToProcedureAdapter();
			}
			@Override
			public Adapter caseFormalAccessToProtectedFunction(FormalAccessToProtectedFunction object) {
				return createFormalAccessToProtectedFunctionAdapter();
			}
			@Override
			public Adapter caseFormalAccessToProtectedProcedure(FormalAccessToProtectedProcedure object) {
				return createFormalAccessToProtectedProcedureAdapter();
			}
			@Override
			public Adapter caseFormalAccessToVariable(FormalAccessToVariable object) {
				return createFormalAccessToVariableAdapter();
			}
			@Override
			public Adapter caseFormalConstrainedArrayDefinition(FormalConstrainedArrayDefinition object) {
				return createFormalConstrainedArrayDefinitionAdapter();
			}
			@Override
			public Adapter caseFormalDecimalFixedPointDefinition(FormalDecimalFixedPointDefinition object) {
				return createFormalDecimalFixedPointDefinitionAdapter();
			}
			@Override
			public Adapter caseFormalDerivedTypeDefinition(FormalDerivedTypeDefinition object) {
				return createFormalDerivedTypeDefinitionAdapter();
			}
			@Override
			public Adapter caseFormalDiscreteTypeDefinition(FormalDiscreteTypeDefinition object) {
				return createFormalDiscreteTypeDefinitionAdapter();
			}
			@Override
			public Adapter caseFormalFloatingPointDefinition(FormalFloatingPointDefinition object) {
				return createFormalFloatingPointDefinitionAdapter();
			}
			@Override
			public Adapter caseFormalFunctionDeclaration(FormalFunctionDeclaration object) {
				return createFormalFunctionDeclarationAdapter();
			}
			@Override
			public Adapter caseFormalIncompleteTypeDeclaration(FormalIncompleteTypeDeclaration object) {
				return createFormalIncompleteTypeDeclarationAdapter();
			}
			@Override
			public Adapter caseFormalLimitedInterface(FormalLimitedInterface object) {
				return createFormalLimitedInterfaceAdapter();
			}
			@Override
			public Adapter caseFormalModularTypeDefinition(FormalModularTypeDefinition object) {
				return createFormalModularTypeDefinitionAdapter();
			}
			@Override
			public Adapter caseFormalObjectDeclaration(FormalObjectDeclaration object) {
				return createFormalObjectDeclarationAdapter();
			}
			@Override
			public Adapter caseFormalOrdinaryFixedPointDefinition(FormalOrdinaryFixedPointDefinition object) {
				return createFormalOrdinaryFixedPointDefinitionAdapter();
			}
			@Override
			public Adapter caseFormalOrdinaryInterface(FormalOrdinaryInterface object) {
				return createFormalOrdinaryInterfaceAdapter();
			}
			@Override
			public Adapter caseFormalPackageDeclaration(FormalPackageDeclaration object) {
				return createFormalPackageDeclarationAdapter();
			}
			@Override
			public Adapter caseFormalPackageDeclarationWithBox(FormalPackageDeclarationWithBox object) {
				return createFormalPackageDeclarationWithBoxAdapter();
			}
			@Override
			public Adapter caseFormalPoolSpecificAccessToVariable(FormalPoolSpecificAccessToVariable object) {
				return createFormalPoolSpecificAccessToVariableAdapter();
			}
			@Override
			public Adapter caseFormalPrivateTypeDefinition(FormalPrivateTypeDefinition object) {
				return createFormalPrivateTypeDefinitionAdapter();
			}
			@Override
			public Adapter caseFormalProcedureDeclaration(FormalProcedureDeclaration object) {
				return createFormalProcedureDeclarationAdapter();
			}
			@Override
			public Adapter caseFormalProtectedInterface(FormalProtectedInterface object) {
				return createFormalProtectedInterfaceAdapter();
			}
			@Override
			public Adapter caseFormalSignedIntegerTypeDefinition(FormalSignedIntegerTypeDefinition object) {
				return createFormalSignedIntegerTypeDefinitionAdapter();
			}
			@Override
			public Adapter caseFormalSynchronizedInterface(FormalSynchronizedInterface object) {
				return createFormalSynchronizedInterfaceAdapter();
			}
			@Override
			public Adapter caseFormalTaggedPrivateTypeDefinition(FormalTaggedPrivateTypeDefinition object) {
				return createFormalTaggedPrivateTypeDefinitionAdapter();
			}
			@Override
			public Adapter caseFormalTaskInterface(FormalTaskInterface object) {
				return createFormalTaskInterfaceAdapter();
			}
			@Override
			public Adapter caseFormalTypeDeclaration(FormalTypeDeclaration object) {
				return createFormalTypeDeclarationAdapter();
			}
			@Override
			public Adapter caseFormalUnconstrainedArrayDefinition(FormalUnconstrainedArrayDefinition object) {
				return createFormalUnconstrainedArrayDefinitionAdapter();
			}
			@Override
			public Adapter caseForSomeQuantifiedExpression(ForSomeQuantifiedExpression object) {
				return createForSomeQuantifiedExpressionAdapter();
			}
			@Override
			public Adapter caseFractionAttribute(FractionAttribute object) {
				return createFractionAttributeAdapter();
			}
			@Override
			public Adapter caseFunctionBodyDeclaration(FunctionBodyDeclaration object) {
				return createFunctionBodyDeclarationAdapter();
			}
			@Override
			public Adapter caseFunctionBodyStub(FunctionBodyStub object) {
				return createFunctionBodyStubAdapter();
			}
			@Override
			public Adapter caseFunctionCall(FunctionCall object) {
				return createFunctionCallAdapter();
			}
			@Override
			public Adapter caseFunctionDeclaration(FunctionDeclaration object) {
				return createFunctionDeclarationAdapter();
			}
			@Override
			public Adapter caseFunctionInstantiation(FunctionInstantiation object) {
				return createFunctionInstantiationAdapter();
			}
			@Override
			public Adapter caseFunctionRenamingDeclaration(FunctionRenamingDeclaration object) {
				return createFunctionRenamingDeclarationAdapter();
			}
			@Override
			public Adapter caseGeneralizedIteratorSpecification(GeneralizedIteratorSpecification object) {
				return createGeneralizedIteratorSpecificationAdapter();
			}
			@Override
			public Adapter caseGenericAssociation(GenericAssociation object) {
				return createGenericAssociationAdapter();
			}
			@Override
			public Adapter caseGenericFunctionDeclaration(GenericFunctionDeclaration object) {
				return createGenericFunctionDeclarationAdapter();
			}
			@Override
			public Adapter caseGenericFunctionRenamingDeclaration(GenericFunctionRenamingDeclaration object) {
				return createGenericFunctionRenamingDeclarationAdapter();
			}
			@Override
			public Adapter caseGenericPackageDeclaration(GenericPackageDeclaration object) {
				return createGenericPackageDeclarationAdapter();
			}
			@Override
			public Adapter caseGenericPackageRenamingDeclaration(GenericPackageRenamingDeclaration object) {
				return createGenericPackageRenamingDeclarationAdapter();
			}
			@Override
			public Adapter caseGenericProcedureDeclaration(GenericProcedureDeclaration object) {
				return createGenericProcedureDeclarationAdapter();
			}
			@Override
			public Adapter caseGenericProcedureRenamingDeclaration(GenericProcedureRenamingDeclaration object) {
				return createGenericProcedureRenamingDeclarationAdapter();
			}
			@Override
			public Adapter caseGotoStatement(GotoStatement object) {
				return createGotoStatementAdapter();
			}
			@Override
			public Adapter caseGreaterThanOperator(GreaterThanOperator object) {
				return createGreaterThanOperatorAdapter();
			}
			@Override
			public Adapter caseGreaterThanOrEqualOperator(GreaterThanOrEqualOperator object) {
				return createGreaterThanOrEqualOperatorAdapter();
			}
			@Override
			public Adapter caseHasAbstractQType(HasAbstractQType object) {
				return createHasAbstractQTypeAdapter();
			}
			@Override
			public Adapter caseHasAbstractQType1(HasAbstractQType1 object) {
				return createHasAbstractQType1Adapter();
			}
			@Override
			public Adapter caseHasAbstractQType2(HasAbstractQType2 object) {
				return createHasAbstractQType2Adapter();
			}
			@Override
			public Adapter caseHasAbstractQType3(HasAbstractQType3 object) {
				return createHasAbstractQType3Adapter();
			}
			@Override
			public Adapter caseHasAbstractQType4(HasAbstractQType4 object) {
				return createHasAbstractQType4Adapter();
			}
			@Override
			public Adapter caseHasAbstractQType5(HasAbstractQType5 object) {
				return createHasAbstractQType5Adapter();
			}
			@Override
			public Adapter caseHasAbstractQType6(HasAbstractQType6 object) {
				return createHasAbstractQType6Adapter();
			}
			@Override
			public Adapter caseHasAbstractQType7(HasAbstractQType7 object) {
				return createHasAbstractQType7Adapter();
			}
			@Override
			public Adapter caseHasAbstractQType8(HasAbstractQType8 object) {
				return createHasAbstractQType8Adapter();
			}
			@Override
			public Adapter caseHasAbstractQType9(HasAbstractQType9 object) {
				return createHasAbstractQType9Adapter();
			}
			@Override
			public Adapter caseHasAbstractQType10(HasAbstractQType10 object) {
				return createHasAbstractQType10Adapter();
			}
			@Override
			public Adapter caseHasAbstractQType11(HasAbstractQType11 object) {
				return createHasAbstractQType11Adapter();
			}
			@Override
			public Adapter caseHasAbstractQType12(HasAbstractQType12 object) {
				return createHasAbstractQType12Adapter();
			}
			@Override
			public Adapter caseHasAbstractQType13(HasAbstractQType13 object) {
				return createHasAbstractQType13Adapter();
			}
			@Override
			public Adapter caseHasAliasedQType(HasAliasedQType object) {
				return createHasAliasedQTypeAdapter();
			}
			@Override
			public Adapter caseHasAliasedQType1(HasAliasedQType1 object) {
				return createHasAliasedQType1Adapter();
			}
			@Override
			public Adapter caseHasAliasedQType2(HasAliasedQType2 object) {
				return createHasAliasedQType2Adapter();
			}
			@Override
			public Adapter caseHasAliasedQType3(HasAliasedQType3 object) {
				return createHasAliasedQType3Adapter();
			}
			@Override
			public Adapter caseHasAliasedQType4(HasAliasedQType4 object) {
				return createHasAliasedQType4Adapter();
			}
			@Override
			public Adapter caseHasAliasedQType5(HasAliasedQType5 object) {
				return createHasAliasedQType5Adapter();
			}
			@Override
			public Adapter caseHasAliasedQType6(HasAliasedQType6 object) {
				return createHasAliasedQType6Adapter();
			}
			@Override
			public Adapter caseHasAliasedQType7(HasAliasedQType7 object) {
				return createHasAliasedQType7Adapter();
			}
			@Override
			public Adapter caseHasAliasedQType8(HasAliasedQType8 object) {
				return createHasAliasedQType8Adapter();
			}
			@Override
			public Adapter caseHasLimitedQType(HasLimitedQType object) {
				return createHasLimitedQTypeAdapter();
			}
			@Override
			public Adapter caseHasLimitedQType1(HasLimitedQType1 object) {
				return createHasLimitedQType1Adapter();
			}
			@Override
			public Adapter caseHasLimitedQType2(HasLimitedQType2 object) {
				return createHasLimitedQType2Adapter();
			}
			@Override
			public Adapter caseHasLimitedQType3(HasLimitedQType3 object) {
				return createHasLimitedQType3Adapter();
			}
			@Override
			public Adapter caseHasLimitedQType4(HasLimitedQType4 object) {
				return createHasLimitedQType4Adapter();
			}
			@Override
			public Adapter caseHasLimitedQType5(HasLimitedQType5 object) {
				return createHasLimitedQType5Adapter();
			}
			@Override
			public Adapter caseHasLimitedQType6(HasLimitedQType6 object) {
				return createHasLimitedQType6Adapter();
			}
			@Override
			public Adapter caseHasLimitedQType7(HasLimitedQType7 object) {
				return createHasLimitedQType7Adapter();
			}
			@Override
			public Adapter caseHasLimitedQType8(HasLimitedQType8 object) {
				return createHasLimitedQType8Adapter();
			}
			@Override
			public Adapter caseHasLimitedQType9(HasLimitedQType9 object) {
				return createHasLimitedQType9Adapter();
			}
			@Override
			public Adapter caseHasLimitedQType10(HasLimitedQType10 object) {
				return createHasLimitedQType10Adapter();
			}
			@Override
			public Adapter caseHasLimitedQType11(HasLimitedQType11 object) {
				return createHasLimitedQType11Adapter();
			}
			@Override
			public Adapter caseHasNullExclusionQType(HasNullExclusionQType object) {
				return createHasNullExclusionQTypeAdapter();
			}
			@Override
			public Adapter caseHasNullExclusionQType1(HasNullExclusionQType1 object) {
				return createHasNullExclusionQType1Adapter();
			}
			@Override
			public Adapter caseHasNullExclusionQType2(HasNullExclusionQType2 object) {
				return createHasNullExclusionQType2Adapter();
			}
			@Override
			public Adapter caseHasNullExclusionQType3(HasNullExclusionQType3 object) {
				return createHasNullExclusionQType3Adapter();
			}
			@Override
			public Adapter caseHasNullExclusionQType4(HasNullExclusionQType4 object) {
				return createHasNullExclusionQType4Adapter();
			}
			@Override
			public Adapter caseHasNullExclusionQType5(HasNullExclusionQType5 object) {
				return createHasNullExclusionQType5Adapter();
			}
			@Override
			public Adapter caseHasNullExclusionQType6(HasNullExclusionQType6 object) {
				return createHasNullExclusionQType6Adapter();
			}
			@Override
			public Adapter caseHasNullExclusionQType7(HasNullExclusionQType7 object) {
				return createHasNullExclusionQType7Adapter();
			}
			@Override
			public Adapter caseHasNullExclusionQType8(HasNullExclusionQType8 object) {
				return createHasNullExclusionQType8Adapter();
			}
			@Override
			public Adapter caseHasNullExclusionQType9(HasNullExclusionQType9 object) {
				return createHasNullExclusionQType9Adapter();
			}
			@Override
			public Adapter caseHasNullExclusionQType10(HasNullExclusionQType10 object) {
				return createHasNullExclusionQType10Adapter();
			}
			@Override
			public Adapter caseHasNullExclusionQType11(HasNullExclusionQType11 object) {
				return createHasNullExclusionQType11Adapter();
			}
			@Override
			public Adapter caseHasNullExclusionQType12(HasNullExclusionQType12 object) {
				return createHasNullExclusionQType12Adapter();
			}
			@Override
			public Adapter caseHasNullExclusionQType13(HasNullExclusionQType13 object) {
				return createHasNullExclusionQType13Adapter();
			}
			@Override
			public Adapter caseHasNullExclusionQType14(HasNullExclusionQType14 object) {
				return createHasNullExclusionQType14Adapter();
			}
			@Override
			public Adapter caseHasNullExclusionQType15(HasNullExclusionQType15 object) {
				return createHasNullExclusionQType15Adapter();
			}
			@Override
			public Adapter caseHasNullExclusionQType16(HasNullExclusionQType16 object) {
				return createHasNullExclusionQType16Adapter();
			}
			@Override
			public Adapter caseHasNullExclusionQType17(HasNullExclusionQType17 object) {
				return createHasNullExclusionQType17Adapter();
			}
			@Override
			public Adapter caseHasNullExclusionQType18(HasNullExclusionQType18 object) {
				return createHasNullExclusionQType18Adapter();
			}
			@Override
			public Adapter caseHasNullExclusionQType19(HasNullExclusionQType19 object) {
				return createHasNullExclusionQType19Adapter();
			}
			@Override
			public Adapter caseHasNullExclusionQType20(HasNullExclusionQType20 object) {
				return createHasNullExclusionQType20Adapter();
			}
			@Override
			public Adapter caseHasNullExclusionQType21(HasNullExclusionQType21 object) {
				return createHasNullExclusionQType21Adapter();
			}
			@Override
			public Adapter caseHasNullExclusionQType22(HasNullExclusionQType22 object) {
				return createHasNullExclusionQType22Adapter();
			}
			@Override
			public Adapter caseHasNullExclusionQType23(HasNullExclusionQType23 object) {
				return createHasNullExclusionQType23Adapter();
			}
			@Override
			public Adapter caseHasNullExclusionQType24(HasNullExclusionQType24 object) {
				return createHasNullExclusionQType24Adapter();
			}
			@Override
			public Adapter caseHasPrivateQType(HasPrivateQType object) {
				return createHasPrivateQTypeAdapter();
			}
			@Override
			public Adapter caseHasPrivateQType1(HasPrivateQType1 object) {
				return createHasPrivateQType1Adapter();
			}
			@Override
			public Adapter caseHasReverseQType(HasReverseQType object) {
				return createHasReverseQTypeAdapter();
			}
			@Override
			public Adapter caseHasReverseQType1(HasReverseQType1 object) {
				return createHasReverseQType1Adapter();
			}
			@Override
			public Adapter caseHasReverseQType2(HasReverseQType2 object) {
				return createHasReverseQType2Adapter();
			}
			@Override
			public Adapter caseHasSynchronizedQType(HasSynchronizedQType object) {
				return createHasSynchronizedQTypeAdapter();
			}
			@Override
			public Adapter caseHasSynchronizedQType1(HasSynchronizedQType1 object) {
				return createHasSynchronizedQType1Adapter();
			}
			@Override
			public Adapter caseHasTaggedQType(HasTaggedQType object) {
				return createHasTaggedQTypeAdapter();
			}
			@Override
			public Adapter caseIdentifier(Identifier object) {
				return createIdentifierAdapter();
			}
			@Override
			public Adapter caseIdentityAttribute(IdentityAttribute object) {
				return createIdentityAttributeAdapter();
			}
			@Override
			public Adapter caseIfExpression(IfExpression object) {
				return createIfExpressionAdapter();
			}
			@Override
			public Adapter caseIfExpressionPath(IfExpressionPath object) {
				return createIfExpressionPathAdapter();
			}
			@Override
			public Adapter caseIfPath(IfPath object) {
				return createIfPathAdapter();
			}
			@Override
			public Adapter caseIfStatement(IfStatement object) {
				return createIfStatementAdapter();
			}
			@Override
			public Adapter caseImageAttribute(ImageAttribute object) {
				return createImageAttributeAdapter();
			}
			@Override
			public Adapter caseImplementationDefinedAttribute(ImplementationDefinedAttribute object) {
				return createImplementationDefinedAttributeAdapter();
			}
			@Override
			public Adapter caseImplementationDefinedPragma(ImplementationDefinedPragma object) {
				return createImplementationDefinedPragmaAdapter();
			}
			@Override
			public Adapter caseImportPragma(ImportPragma object) {
				return createImportPragmaAdapter();
			}
			@Override
			public Adapter caseIncompleteTypeDeclaration(IncompleteTypeDeclaration object) {
				return createIncompleteTypeDeclarationAdapter();
			}
			@Override
			public Adapter caseIndependentComponentsPragma(IndependentComponentsPragma object) {
				return createIndependentComponentsPragmaAdapter();
			}
			@Override
			public Adapter caseIndependentPragma(IndependentPragma object) {
				return createIndependentPragmaAdapter();
			}
			@Override
			public Adapter caseIndexConstraint(IndexConstraint object) {
				return createIndexConstraintAdapter();
			}
			@Override
			public Adapter caseIndexedComponent(IndexedComponent object) {
				return createIndexedComponentAdapter();
			}
			@Override
			public Adapter caseInlinePragma(InlinePragma object) {
				return createInlinePragmaAdapter();
			}
			@Override
			public Adapter caseInMembershipTest(InMembershipTest object) {
				return createInMembershipTestAdapter();
			}
			@Override
			public Adapter caseInputAttribute(InputAttribute object) {
				return createInputAttributeAdapter();
			}
			@Override
			public Adapter caseInspectionPointPragma(InspectionPointPragma object) {
				return createInspectionPointPragmaAdapter();
			}
			@Override
			public Adapter caseIntegerLiteral(IntegerLiteral object) {
				return createIntegerLiteralAdapter();
			}
			@Override
			public Adapter caseIntegerNumberDeclaration(IntegerNumberDeclaration object) {
				return createIntegerNumberDeclarationAdapter();
			}
			@Override
			public Adapter caseInterruptHandlerPragma(InterruptHandlerPragma object) {
				return createInterruptHandlerPragmaAdapter();
			}
			@Override
			public Adapter caseInterruptPriorityPragma(InterruptPriorityPragma object) {
				return createInterruptPriorityPragmaAdapter();
			}
			@Override
			public Adapter caseIsNotNullReturnQType(IsNotNullReturnQType object) {
				return createIsNotNullReturnQTypeAdapter();
			}
			@Override
			public Adapter caseIsNotNullReturnQType1(IsNotNullReturnQType1 object) {
				return createIsNotNullReturnQType1Adapter();
			}
			@Override
			public Adapter caseIsNotNullReturnQType2(IsNotNullReturnQType2 object) {
				return createIsNotNullReturnQType2Adapter();
			}
			@Override
			public Adapter caseIsNotNullReturnQType3(IsNotNullReturnQType3 object) {
				return createIsNotNullReturnQType3Adapter();
			}
			@Override
			public Adapter caseIsNotNullReturnQType4(IsNotNullReturnQType4 object) {
				return createIsNotNullReturnQType4Adapter();
			}
			@Override
			public Adapter caseIsNotNullReturnQType5(IsNotNullReturnQType5 object) {
				return createIsNotNullReturnQType5Adapter();
			}
			@Override
			public Adapter caseIsNotNullReturnQType6(IsNotNullReturnQType6 object) {
				return createIsNotNullReturnQType6Adapter();
			}
			@Override
			public Adapter caseIsNotNullReturnQType7(IsNotNullReturnQType7 object) {
				return createIsNotNullReturnQType7Adapter();
			}
			@Override
			public Adapter caseIsNotNullReturnQType8(IsNotNullReturnQType8 object) {
				return createIsNotNullReturnQType8Adapter();
			}
			@Override
			public Adapter caseIsNotNullReturnQType9(IsNotNullReturnQType9 object) {
				return createIsNotNullReturnQType9Adapter();
			}
			@Override
			public Adapter caseIsNotNullReturnQType10(IsNotNullReturnQType10 object) {
				return createIsNotNullReturnQType10Adapter();
			}
			@Override
			public Adapter caseIsNotNullReturnQType11(IsNotNullReturnQType11 object) {
				return createIsNotNullReturnQType11Adapter();
			}
			@Override
			public Adapter caseIsNotOverridingDeclarationQType(IsNotOverridingDeclarationQType object) {
				return createIsNotOverridingDeclarationQTypeAdapter();
			}
			@Override
			public Adapter caseIsNotOverridingDeclarationQType1(IsNotOverridingDeclarationQType1 object) {
				return createIsNotOverridingDeclarationQType1Adapter();
			}
			@Override
			public Adapter caseIsNotOverridingDeclarationQType2(IsNotOverridingDeclarationQType2 object) {
				return createIsNotOverridingDeclarationQType2Adapter();
			}
			@Override
			public Adapter caseIsNotOverridingDeclarationQType3(IsNotOverridingDeclarationQType3 object) {
				return createIsNotOverridingDeclarationQType3Adapter();
			}
			@Override
			public Adapter caseIsNotOverridingDeclarationQType4(IsNotOverridingDeclarationQType4 object) {
				return createIsNotOverridingDeclarationQType4Adapter();
			}
			@Override
			public Adapter caseIsNotOverridingDeclarationQType5(IsNotOverridingDeclarationQType5 object) {
				return createIsNotOverridingDeclarationQType5Adapter();
			}
			@Override
			public Adapter caseIsNotOverridingDeclarationQType6(IsNotOverridingDeclarationQType6 object) {
				return createIsNotOverridingDeclarationQType6Adapter();
			}
			@Override
			public Adapter caseIsNotOverridingDeclarationQType7(IsNotOverridingDeclarationQType7 object) {
				return createIsNotOverridingDeclarationQType7Adapter();
			}
			@Override
			public Adapter caseIsNotOverridingDeclarationQType8(IsNotOverridingDeclarationQType8 object) {
				return createIsNotOverridingDeclarationQType8Adapter();
			}
			@Override
			public Adapter caseIsNotOverridingDeclarationQType9(IsNotOverridingDeclarationQType9 object) {
				return createIsNotOverridingDeclarationQType9Adapter();
			}
			@Override
			public Adapter caseIsNotOverridingDeclarationQType10(IsNotOverridingDeclarationQType10 object) {
				return createIsNotOverridingDeclarationQType10Adapter();
			}
			@Override
			public Adapter caseIsNotOverridingDeclarationQType11(IsNotOverridingDeclarationQType11 object) {
				return createIsNotOverridingDeclarationQType11Adapter();
			}
			@Override
			public Adapter caseIsOverridingDeclarationQType(IsOverridingDeclarationQType object) {
				return createIsOverridingDeclarationQTypeAdapter();
			}
			@Override
			public Adapter caseIsOverridingDeclarationQType1(IsOverridingDeclarationQType1 object) {
				return createIsOverridingDeclarationQType1Adapter();
			}
			@Override
			public Adapter caseIsOverridingDeclarationQType2(IsOverridingDeclarationQType2 object) {
				return createIsOverridingDeclarationQType2Adapter();
			}
			@Override
			public Adapter caseIsOverridingDeclarationQType3(IsOverridingDeclarationQType3 object) {
				return createIsOverridingDeclarationQType3Adapter();
			}
			@Override
			public Adapter caseIsOverridingDeclarationQType4(IsOverridingDeclarationQType4 object) {
				return createIsOverridingDeclarationQType4Adapter();
			}
			@Override
			public Adapter caseIsOverridingDeclarationQType5(IsOverridingDeclarationQType5 object) {
				return createIsOverridingDeclarationQType5Adapter();
			}
			@Override
			public Adapter caseIsOverridingDeclarationQType6(IsOverridingDeclarationQType6 object) {
				return createIsOverridingDeclarationQType6Adapter();
			}
			@Override
			public Adapter caseIsOverridingDeclarationQType7(IsOverridingDeclarationQType7 object) {
				return createIsOverridingDeclarationQType7Adapter();
			}
			@Override
			public Adapter caseIsOverridingDeclarationQType8(IsOverridingDeclarationQType8 object) {
				return createIsOverridingDeclarationQType8Adapter();
			}
			@Override
			public Adapter caseIsOverridingDeclarationQType9(IsOverridingDeclarationQType9 object) {
				return createIsOverridingDeclarationQType9Adapter();
			}
			@Override
			public Adapter caseIsOverridingDeclarationQType10(IsOverridingDeclarationQType10 object) {
				return createIsOverridingDeclarationQType10Adapter();
			}
			@Override
			public Adapter caseIsOverridingDeclarationQType11(IsOverridingDeclarationQType11 object) {
				return createIsOverridingDeclarationQType11Adapter();
			}
			@Override
			public Adapter caseIsPrefixCall(IsPrefixCall object) {
				return createIsPrefixCallAdapter();
			}
			@Override
			public Adapter caseIsPrefixCallQType(IsPrefixCallQType object) {
				return createIsPrefixCallQTypeAdapter();
			}
			@Override
			public Adapter caseIsPrefixNotation(IsPrefixNotation object) {
				return createIsPrefixNotationAdapter();
			}
			@Override
			public Adapter caseIsPrefixNotationQType(IsPrefixNotationQType object) {
				return createIsPrefixNotationQTypeAdapter();
			}
			@Override
			public Adapter caseIsPrefixNotationQType1(IsPrefixNotationQType1 object) {
				return createIsPrefixNotationQType1Adapter();
			}
			@Override
			public Adapter caseKnownDiscriminantPart(KnownDiscriminantPart object) {
				return createKnownDiscriminantPartAdapter();
			}
			@Override
			public Adapter caseLastAttribute(LastAttribute object) {
				return createLastAttributeAdapter();
			}
			@Override
			public Adapter caseLastBitAttribute(LastBitAttribute object) {
				return createLastBitAttributeAdapter();
			}
			@Override
			public Adapter caseLeadingPartAttribute(LeadingPartAttribute object) {
				return createLeadingPartAttributeAdapter();
			}
			@Override
			public Adapter caseLengthAttribute(LengthAttribute object) {
				return createLengthAttributeAdapter();
			}
			@Override
			public Adapter caseLessThanOperator(LessThanOperator object) {
				return createLessThanOperatorAdapter();
			}
			@Override
			public Adapter caseLessThanOrEqualOperator(LessThanOrEqualOperator object) {
				return createLessThanOrEqualOperatorAdapter();
			}
			@Override
			public Adapter caseLimited(Limited object) {
				return createLimitedAdapter();
			}
			@Override
			public Adapter caseLimitedInterface(LimitedInterface object) {
				return createLimitedInterfaceAdapter();
			}
			@Override
			public Adapter caseLinkerOptionsPragma(LinkerOptionsPragma object) {
				return createLinkerOptionsPragmaAdapter();
			}
			@Override
			public Adapter caseListPragma(ListPragma object) {
				return createListPragmaAdapter();
			}
			@Override
			public Adapter caseLockingPolicyPragma(LockingPolicyPragma object) {
				return createLockingPolicyPragmaAdapter();
			}
			@Override
			public Adapter caseLoopParameterSpecification(LoopParameterSpecification object) {
				return createLoopParameterSpecificationAdapter();
			}
			@Override
			public Adapter caseLoopStatement(LoopStatement object) {
				return createLoopStatementAdapter();
			}
			@Override
			public Adapter caseMachineAttribute(MachineAttribute object) {
				return createMachineAttributeAdapter();
			}
			@Override
			public Adapter caseMachineEmaxAttribute(MachineEmaxAttribute object) {
				return createMachineEmaxAttributeAdapter();
			}
			@Override
			public Adapter caseMachineEminAttribute(MachineEminAttribute object) {
				return createMachineEminAttributeAdapter();
			}
			@Override
			public Adapter caseMachineMantissaAttribute(MachineMantissaAttribute object) {
				return createMachineMantissaAttributeAdapter();
			}
			@Override
			public Adapter caseMachineOverflowsAttribute(MachineOverflowsAttribute object) {
				return createMachineOverflowsAttributeAdapter();
			}
			@Override
			public Adapter caseMachineRadixAttribute(MachineRadixAttribute object) {
				return createMachineRadixAttributeAdapter();
			}
			@Override
			public Adapter caseMachineRoundingAttribute(MachineRoundingAttribute object) {
				return createMachineRoundingAttributeAdapter();
			}
			@Override
			public Adapter caseMachineRoundsAttribute(MachineRoundsAttribute object) {
				return createMachineRoundsAttributeAdapter();
			}
			@Override
			public Adapter caseMaxAlignmentForAllocationAttribute(MaxAlignmentForAllocationAttribute object) {
				return createMaxAlignmentForAllocationAttributeAdapter();
			}
			@Override
			public Adapter caseMaxAttribute(MaxAttribute object) {
				return createMaxAttributeAdapter();
			}
			@Override
			public Adapter caseMaxSizeInStorageElementsAttribute(MaxSizeInStorageElementsAttribute object) {
				return createMaxSizeInStorageElementsAttributeAdapter();
			}
			@Override
			public Adapter caseMinAttribute(MinAttribute object) {
				return createMinAttributeAdapter();
			}
			@Override
			public Adapter caseMinusOperator(MinusOperator object) {
				return createMinusOperatorAdapter();
			}
			@Override
			public Adapter caseModAttribute(ModAttribute object) {
				return createModAttributeAdapter();
			}
			@Override
			public Adapter caseModelAttribute(ModelAttribute object) {
				return createModelAttributeAdapter();
			}
			@Override
			public Adapter caseModelEminAttribute(ModelEminAttribute object) {
				return createModelEminAttributeAdapter();
			}
			@Override
			public Adapter caseModelEpsilonAttribute(ModelEpsilonAttribute object) {
				return createModelEpsilonAttributeAdapter();
			}
			@Override
			public Adapter caseModelMantissaAttribute(ModelMantissaAttribute object) {
				return createModelMantissaAttributeAdapter();
			}
			@Override
			public Adapter caseModelSmallAttribute(ModelSmallAttribute object) {
				return createModelSmallAttributeAdapter();
			}
			@Override
			public Adapter caseModOperator(ModOperator object) {
				return createModOperatorAdapter();
			}
			@Override
			public Adapter caseModularTypeDefinition(ModularTypeDefinition object) {
				return createModularTypeDefinitionAdapter();
			}
			@Override
			public Adapter caseModulusAttribute(ModulusAttribute object) {
				return createModulusAttributeAdapter();
			}
			@Override
			public Adapter caseMultiplyOperator(MultiplyOperator object) {
				return createMultiplyOperatorAdapter();
			}
			@Override
			public Adapter caseNameClass(NameClass object) {
				return createNameClassAdapter();
			}
			@Override
			public Adapter caseNamedArrayAggregate(NamedArrayAggregate object) {
				return createNamedArrayAggregateAdapter();
			}
			@Override
			public Adapter caseNameList(NameList object) {
				return createNameListAdapter();
			}
			@Override
			public Adapter caseNoReturnPragma(NoReturnPragma object) {
				return createNoReturnPragmaAdapter();
			}
			@Override
			public Adapter caseNormalizeScalarsPragma(NormalizeScalarsPragma object) {
				return createNormalizeScalarsPragmaAdapter();
			}
			@Override
			public Adapter caseNotAnElement(NotAnElement object) {
				return createNotAnElementAdapter();
			}
			@Override
			public Adapter caseNotEqualOperator(NotEqualOperator object) {
				return createNotEqualOperatorAdapter();
			}
			@Override
			public Adapter caseNotInMembershipTest(NotInMembershipTest object) {
				return createNotInMembershipTestAdapter();
			}
			@Override
			public Adapter caseNotNullReturn(NotNullReturn object) {
				return createNotNullReturnAdapter();
			}
			@Override
			public Adapter caseNotOperator(NotOperator object) {
				return createNotOperatorAdapter();
			}
			@Override
			public Adapter caseNotOverriding(NotOverriding object) {
				return createNotOverridingAdapter();
			}
			@Override
			public Adapter caseNullComponent(NullComponent object) {
				return createNullComponentAdapter();
			}
			@Override
			public Adapter caseNullExclusion(NullExclusion object) {
				return createNullExclusionAdapter();
			}
			@Override
			public Adapter caseNullLiteral(NullLiteral object) {
				return createNullLiteralAdapter();
			}
			@Override
			public Adapter caseNullProcedureDeclaration(NullProcedureDeclaration object) {
				return createNullProcedureDeclarationAdapter();
			}
			@Override
			public Adapter caseNullRecordDefinition(NullRecordDefinition object) {
				return createNullRecordDefinitionAdapter();
			}
			@Override
			public Adapter caseNullStatement(NullStatement object) {
				return createNullStatementAdapter();
			}
			@Override
			public Adapter caseObjectRenamingDeclaration(ObjectRenamingDeclaration object) {
				return createObjectRenamingDeclarationAdapter();
			}
			@Override
			public Adapter caseOptimizePragma(OptimizePragma object) {
				return createOptimizePragmaAdapter();
			}
			@Override
			public Adapter caseOrdinaryFixedPointDefinition(OrdinaryFixedPointDefinition object) {
				return createOrdinaryFixedPointDefinitionAdapter();
			}
			@Override
			public Adapter caseOrdinaryInterface(OrdinaryInterface object) {
				return createOrdinaryInterfaceAdapter();
			}
			@Override
			public Adapter caseOrdinaryTypeDeclaration(OrdinaryTypeDeclaration object) {
				return createOrdinaryTypeDeclarationAdapter();
			}
			@Override
			public Adapter caseOrElseShortCircuit(OrElseShortCircuit object) {
				return createOrElseShortCircuitAdapter();
			}
			@Override
			public Adapter caseOrOperator(OrOperator object) {
				return createOrOperatorAdapter();
			}
			@Override
			public Adapter caseOrPath(OrPath object) {
				return createOrPathAdapter();
			}
			@Override
			public Adapter caseOthersChoice(OthersChoice object) {
				return createOthersChoiceAdapter();
			}
			@Override
			public Adapter caseOutputAttribute(OutputAttribute object) {
				return createOutputAttributeAdapter();
			}
			@Override
			public Adapter caseOverlapsStorageAttribute(OverlapsStorageAttribute object) {
				return createOverlapsStorageAttributeAdapter();
			}
			@Override
			public Adapter caseOverriding(Overriding object) {
				return createOverridingAdapter();
			}
			@Override
			public Adapter casePackageBodyDeclaration(PackageBodyDeclaration object) {
				return createPackageBodyDeclarationAdapter();
			}
			@Override
			public Adapter casePackageBodyStub(PackageBodyStub object) {
				return createPackageBodyStubAdapter();
			}
			@Override
			public Adapter casePackageDeclaration(PackageDeclaration object) {
				return createPackageDeclarationAdapter();
			}
			@Override
			public Adapter casePackageInstantiation(PackageInstantiation object) {
				return createPackageInstantiationAdapter();
			}
			@Override
			public Adapter casePackageRenamingDeclaration(PackageRenamingDeclaration object) {
				return createPackageRenamingDeclarationAdapter();
			}
			@Override
			public Adapter casePackPragma(PackPragma object) {
				return createPackPragmaAdapter();
			}
			@Override
			public Adapter casePagePragma(PagePragma object) {
				return createPagePragmaAdapter();
			}
			@Override
			public Adapter caseParameterAssociation(ParameterAssociation object) {
				return createParameterAssociationAdapter();
			}
			@Override
			public Adapter caseParameterSpecification(ParameterSpecification object) {
				return createParameterSpecificationAdapter();
			}
			@Override
			public Adapter caseParameterSpecificationList(ParameterSpecificationList object) {
				return createParameterSpecificationListAdapter();
			}
			@Override
			public Adapter caseParenthesizedExpression(ParenthesizedExpression object) {
				return createParenthesizedExpressionAdapter();
			}
			@Override
			public Adapter casePartitionElaborationPolicyPragma(PartitionElaborationPolicyPragma object) {
				return createPartitionElaborationPolicyPragmaAdapter();
			}
			@Override
			public Adapter casePartitionIdAttribute(PartitionIdAttribute object) {
				return createPartitionIdAttributeAdapter();
			}
			@Override
			public Adapter casePathClass(PathClass object) {
				return createPathClassAdapter();
			}
			@Override
			public Adapter casePathList(PathList object) {
				return createPathListAdapter();
			}
			@Override
			public Adapter casePlusOperator(PlusOperator object) {
				return createPlusOperatorAdapter();
			}
			@Override
			public Adapter casePoolSpecificAccessToVariable(PoolSpecificAccessToVariable object) {
				return createPoolSpecificAccessToVariableAdapter();
			}
			@Override
			public Adapter casePosAttribute(PosAttribute object) {
				return createPosAttributeAdapter();
			}
			@Override
			public Adapter casePositionalArrayAggregate(PositionalArrayAggregate object) {
				return createPositionalArrayAggregateAdapter();
			}
			@Override
			public Adapter casePositionAttribute(PositionAttribute object) {
				return createPositionAttributeAdapter();
			}
			@Override
			public Adapter casePragmaArgumentAssociation(PragmaArgumentAssociation object) {
				return createPragmaArgumentAssociationAdapter();
			}
			@Override
			public Adapter casePragmaElementClass(PragmaElementClass object) {
				return createPragmaElementClassAdapter();
			}
			@Override
			public Adapter casePredAttribute(PredAttribute object) {
				return createPredAttributeAdapter();
			}
			@Override
			public Adapter casePreelaborableInitializationPragma(PreelaborableInitializationPragma object) {
				return createPreelaborableInitializationPragmaAdapter();
			}
			@Override
			public Adapter casePreelaboratePragma(PreelaboratePragma object) {
				return createPreelaboratePragmaAdapter();
			}
			@Override
			public Adapter casePriorityAttribute(PriorityAttribute object) {
				return createPriorityAttributeAdapter();
			}
			@Override
			public Adapter casePriorityPragma(PriorityPragma object) {
				return createPriorityPragmaAdapter();
			}
			@Override
			public Adapter casePrioritySpecificDispatchingPragma(PrioritySpecificDispatchingPragma object) {
				return createPrioritySpecificDispatchingPragmaAdapter();
			}
			@Override
			public Adapter casePrivate(Private object) {
				return createPrivateAdapter();
			}
			@Override
			public Adapter casePrivateExtensionDeclaration(PrivateExtensionDeclaration object) {
				return createPrivateExtensionDeclarationAdapter();
			}
			@Override
			public Adapter casePrivateExtensionDefinition(PrivateExtensionDefinition object) {
				return createPrivateExtensionDefinitionAdapter();
			}
			@Override
			public Adapter casePrivateTypeDeclaration(PrivateTypeDeclaration object) {
				return createPrivateTypeDeclarationAdapter();
			}
			@Override
			public Adapter casePrivateTypeDefinition(PrivateTypeDefinition object) {
				return createPrivateTypeDefinitionAdapter();
			}
			@Override
			public Adapter caseProcedureBodyDeclaration(ProcedureBodyDeclaration object) {
				return createProcedureBodyDeclarationAdapter();
			}
			@Override
			public Adapter caseProcedureBodyStub(ProcedureBodyStub object) {
				return createProcedureBodyStubAdapter();
			}
			@Override
			public Adapter caseProcedureCallStatement(ProcedureCallStatement object) {
				return createProcedureCallStatementAdapter();
			}
			@Override
			public Adapter caseProcedureDeclaration(ProcedureDeclaration object) {
				return createProcedureDeclarationAdapter();
			}
			@Override
			public Adapter caseProcedureInstantiation(ProcedureInstantiation object) {
				return createProcedureInstantiationAdapter();
			}
			@Override
			public Adapter caseProcedureRenamingDeclaration(ProcedureRenamingDeclaration object) {
				return createProcedureRenamingDeclarationAdapter();
			}
			@Override
			public Adapter caseProfilePragma(ProfilePragma object) {
				return createProfilePragmaAdapter();
			}
			@Override
			public Adapter caseProtectedBodyDeclaration(ProtectedBodyDeclaration object) {
				return createProtectedBodyDeclarationAdapter();
			}
			@Override
			public Adapter caseProtectedBodyStub(ProtectedBodyStub object) {
				return createProtectedBodyStubAdapter();
			}
			@Override
			public Adapter caseProtectedDefinition(ProtectedDefinition object) {
				return createProtectedDefinitionAdapter();
			}
			@Override
			public Adapter caseProtectedInterface(ProtectedInterface object) {
				return createProtectedInterfaceAdapter();
			}
			@Override
			public Adapter caseProtectedTypeDeclaration(ProtectedTypeDeclaration object) {
				return createProtectedTypeDeclarationAdapter();
			}
			@Override
			public Adapter casePurePragma(PurePragma object) {
				return createPurePragmaAdapter();
			}
			@Override
			public Adapter caseQualifiedExpression(QualifiedExpression object) {
				return createQualifiedExpressionAdapter();
			}
			@Override
			public Adapter caseQueuingPolicyPragma(QueuingPolicyPragma object) {
				return createQueuingPolicyPragmaAdapter();
			}
			@Override
			public Adapter caseRaiseExpression(RaiseExpression object) {
				return createRaiseExpressionAdapter();
			}
			@Override
			public Adapter caseRaiseStatement(RaiseStatement object) {
				return createRaiseStatementAdapter();
			}
			@Override
			public Adapter caseRangeAttribute(RangeAttribute object) {
				return createRangeAttributeAdapter();
			}
			@Override
			public Adapter caseRangeAttributeReference(RangeAttributeReference object) {
				return createRangeAttributeReferenceAdapter();
			}
			@Override
			public Adapter caseRangeConstraintClass(RangeConstraintClass object) {
				return createRangeConstraintClassAdapter();
			}
			@Override
			public Adapter caseReadAttribute(ReadAttribute object) {
				return createReadAttributeAdapter();
			}
			@Override
			public Adapter caseRealLiteral(RealLiteral object) {
				return createRealLiteralAdapter();
			}
			@Override
			public Adapter caseRealNumberDeclaration(RealNumberDeclaration object) {
				return createRealNumberDeclarationAdapter();
			}
			@Override
			public Adapter caseRecordAggregate(RecordAggregate object) {
				return createRecordAggregateAdapter();
			}
			@Override
			public Adapter caseRecordComponentAssociation(RecordComponentAssociation object) {
				return createRecordComponentAssociationAdapter();
			}
			@Override
			public Adapter caseRecordComponentClass(RecordComponentClass object) {
				return createRecordComponentClassAdapter();
			}
			@Override
			public Adapter caseRecordComponentList(RecordComponentList object) {
				return createRecordComponentListAdapter();
			}
			@Override
			public Adapter caseRecordDefinition(RecordDefinition object) {
				return createRecordDefinitionAdapter();
			}
			@Override
			public Adapter caseRecordRepresentationClause(RecordRepresentationClause object) {
				return createRecordRepresentationClauseAdapter();
			}
			@Override
			public Adapter caseRecordTypeDefinition(RecordTypeDefinition object) {
				return createRecordTypeDefinitionAdapter();
			}
			@Override
			public Adapter caseRelativeDeadlinePragma(RelativeDeadlinePragma object) {
				return createRelativeDeadlinePragmaAdapter();
			}
			@Override
			public Adapter caseRemainderAttribute(RemainderAttribute object) {
				return createRemainderAttributeAdapter();
			}
			@Override
			public Adapter caseRemOperator(RemOperator object) {
				return createRemOperatorAdapter();
			}
			@Override
			public Adapter caseRemoteCallInterfacePragma(RemoteCallInterfacePragma object) {
				return createRemoteCallInterfacePragmaAdapter();
			}
			@Override
			public Adapter caseRemoteTypesPragma(RemoteTypesPragma object) {
				return createRemoteTypesPragmaAdapter();
			}
			@Override
			public Adapter caseRequeueStatement(RequeueStatement object) {
				return createRequeueStatementAdapter();
			}
			@Override
			public Adapter caseRequeueStatementWithAbort(RequeueStatementWithAbort object) {
				return createRequeueStatementWithAbortAdapter();
			}
			@Override
			public Adapter caseRestrictionsPragma(RestrictionsPragma object) {
				return createRestrictionsPragmaAdapter();
			}
			@Override
			public Adapter caseReturnConstantSpecification(ReturnConstantSpecification object) {
				return createReturnConstantSpecificationAdapter();
			}
			@Override
			public Adapter caseReturnStatement(ReturnStatement object) {
				return createReturnStatementAdapter();
			}
			@Override
			public Adapter caseReturnVariableSpecification(ReturnVariableSpecification object) {
				return createReturnVariableSpecificationAdapter();
			}
			@Override
			public Adapter caseReverse(Reverse object) {
				return createReverseAdapter();
			}
			@Override
			public Adapter caseReviewablePragma(ReviewablePragma object) {
				return createReviewablePragmaAdapter();
			}
			@Override
			public Adapter caseRootIntegerDefinition(RootIntegerDefinition object) {
				return createRootIntegerDefinitionAdapter();
			}
			@Override
			public Adapter caseRootRealDefinition(RootRealDefinition object) {
				return createRootRealDefinitionAdapter();
			}
			@Override
			public Adapter caseRoundAttribute(RoundAttribute object) {
				return createRoundAttributeAdapter();
			}
			@Override
			public Adapter caseRoundingAttribute(RoundingAttribute object) {
				return createRoundingAttributeAdapter();
			}
			@Override
			public Adapter caseSafeFirstAttribute(SafeFirstAttribute object) {
				return createSafeFirstAttributeAdapter();
			}
			@Override
			public Adapter caseSafeLastAttribute(SafeLastAttribute object) {
				return createSafeLastAttributeAdapter();
			}
			@Override
			public Adapter caseScaleAttribute(ScaleAttribute object) {
				return createScaleAttributeAdapter();
			}
			@Override
			public Adapter caseScalingAttribute(ScalingAttribute object) {
				return createScalingAttributeAdapter();
			}
			@Override
			public Adapter caseSelectedComponent(SelectedComponent object) {
				return createSelectedComponentAdapter();
			}
			@Override
			public Adapter caseSelectiveAcceptStatement(SelectiveAcceptStatement object) {
				return createSelectiveAcceptStatementAdapter();
			}
			@Override
			public Adapter caseSelectPath(SelectPath object) {
				return createSelectPathAdapter();
			}
			@Override
			public Adapter caseSharedPassivePragma(SharedPassivePragma object) {
				return createSharedPassivePragmaAdapter();
			}
			@Override
			public Adapter caseSignedIntegerTypeDefinition(SignedIntegerTypeDefinition object) {
				return createSignedIntegerTypeDefinitionAdapter();
			}
			@Override
			public Adapter caseSignedZerosAttribute(SignedZerosAttribute object) {
				return createSignedZerosAttributeAdapter();
			}
			@Override
			public Adapter caseSimpleExpressionRange(SimpleExpressionRange object) {
				return createSimpleExpressionRangeAdapter();
			}
			@Override
			public Adapter caseSingleProtectedDeclaration(SingleProtectedDeclaration object) {
				return createSingleProtectedDeclarationAdapter();
			}
			@Override
			public Adapter caseSingleTaskDeclaration(SingleTaskDeclaration object) {
				return createSingleTaskDeclarationAdapter();
			}
			@Override
			public Adapter caseSizeAttribute(SizeAttribute object) {
				return createSizeAttributeAdapter();
			}
			@Override
			public Adapter caseSlice(Slice object) {
				return createSliceAdapter();
			}
			@Override
			public Adapter caseSmallAttribute(SmallAttribute object) {
				return createSmallAttributeAdapter();
			}
			@Override
			public Adapter caseSourceLocation(SourceLocation object) {
				return createSourceLocationAdapter();
			}
			@Override
			public Adapter caseStatementClass(StatementClass object) {
				return createStatementClassAdapter();
			}
			@Override
			public Adapter caseStatementList(StatementList object) {
				return createStatementListAdapter();
			}
			@Override
			public Adapter caseStoragePoolAttribute(StoragePoolAttribute object) {
				return createStoragePoolAttributeAdapter();
			}
			@Override
			public Adapter caseStorageSizeAttribute(StorageSizeAttribute object) {
				return createStorageSizeAttributeAdapter();
			}
			@Override
			public Adapter caseStorageSizePragma(StorageSizePragma object) {
				return createStorageSizePragmaAdapter();
			}
			@Override
			public Adapter caseStreamSizeAttribute(StreamSizeAttribute object) {
				return createStreamSizeAttributeAdapter();
			}
			@Override
			public Adapter caseStringLiteral(StringLiteral object) {
				return createStringLiteralAdapter();
			}
			@Override
			public Adapter caseSubtypeDeclaration(SubtypeDeclaration object) {
				return createSubtypeDeclarationAdapter();
			}
			@Override
			public Adapter caseSubtypeIndication(SubtypeIndication object) {
				return createSubtypeIndicationAdapter();
			}
			@Override
			public Adapter caseSuccAttribute(SuccAttribute object) {
				return createSuccAttributeAdapter();
			}
			@Override
			public Adapter caseSuppressPragma(SuppressPragma object) {
				return createSuppressPragmaAdapter();
			}
			@Override
			public Adapter caseSynchronized(Synchronized object) {
				return createSynchronizedAdapter();
			}
			@Override
			public Adapter caseSynchronizedInterface(SynchronizedInterface object) {
				return createSynchronizedInterfaceAdapter();
			}
			@Override
			public Adapter caseTagAttribute(TagAttribute object) {
				return createTagAttributeAdapter();
			}
			@Override
			public Adapter caseTagged(Tagged object) {
				return createTaggedAdapter();
			}
			@Override
			public Adapter caseTaggedIncompleteTypeDeclaration(TaggedIncompleteTypeDeclaration object) {
				return createTaggedIncompleteTypeDeclarationAdapter();
			}
			@Override
			public Adapter caseTaggedPrivateTypeDefinition(TaggedPrivateTypeDefinition object) {
				return createTaggedPrivateTypeDefinitionAdapter();
			}
			@Override
			public Adapter caseTaggedRecordTypeDefinition(TaggedRecordTypeDefinition object) {
				return createTaggedRecordTypeDefinitionAdapter();
			}
			@Override
			public Adapter caseTaskBodyDeclaration(TaskBodyDeclaration object) {
				return createTaskBodyDeclarationAdapter();
			}
			@Override
			public Adapter caseTaskBodyStub(TaskBodyStub object) {
				return createTaskBodyStubAdapter();
			}
			@Override
			public Adapter caseTaskDefinition(TaskDefinition object) {
				return createTaskDefinitionAdapter();
			}
			@Override
			public Adapter caseTaskDispatchingPolicyPragma(TaskDispatchingPolicyPragma object) {
				return createTaskDispatchingPolicyPragmaAdapter();
			}
			@Override
			public Adapter caseTaskInterface(TaskInterface object) {
				return createTaskInterfaceAdapter();
			}
			@Override
			public Adapter caseTaskTypeDeclaration(TaskTypeDeclaration object) {
				return createTaskTypeDeclarationAdapter();
			}
			@Override
			public Adapter caseTerminateAlternativeStatement(TerminateAlternativeStatement object) {
				return createTerminateAlternativeStatementAdapter();
			}
			@Override
			public Adapter caseTerminatedAttribute(TerminatedAttribute object) {
				return createTerminatedAttributeAdapter();
			}
			@Override
			public Adapter caseThenAbortPath(ThenAbortPath object) {
				return createThenAbortPathAdapter();
			}
			@Override
			public Adapter caseTimedEntryCallStatement(TimedEntryCallStatement object) {
				return createTimedEntryCallStatementAdapter();
			}
			@Override
			public Adapter caseTruncationAttribute(TruncationAttribute object) {
				return createTruncationAttributeAdapter();
			}
			@Override
			public Adapter caseTypeConversion(TypeConversion object) {
				return createTypeConversionAdapter();
			}
			@Override
			public Adapter caseUnaryMinusOperator(UnaryMinusOperator object) {
				return createUnaryMinusOperatorAdapter();
			}
			@Override
			public Adapter caseUnaryPlusOperator(UnaryPlusOperator object) {
				return createUnaryPlusOperatorAdapter();
			}
			@Override
			public Adapter caseUnbiasedRoundingAttribute(UnbiasedRoundingAttribute object) {
				return createUnbiasedRoundingAttributeAdapter();
			}
			@Override
			public Adapter caseUncheckedAccessAttribute(UncheckedAccessAttribute object) {
				return createUncheckedAccessAttributeAdapter();
			}
			@Override
			public Adapter caseUncheckedUnionPragma(UncheckedUnionPragma object) {
				return createUncheckedUnionPragmaAdapter();
			}
			@Override
			public Adapter caseUnconstrainedArrayDefinition(UnconstrainedArrayDefinition object) {
				return createUnconstrainedArrayDefinitionAdapter();
			}
			@Override
			public Adapter caseUniversalFixedDefinition(UniversalFixedDefinition object) {
				return createUniversalFixedDefinitionAdapter();
			}
			@Override
			public Adapter caseUniversalIntegerDefinition(UniversalIntegerDefinition object) {
				return createUniversalIntegerDefinitionAdapter();
			}
			@Override
			public Adapter caseUniversalRealDefinition(UniversalRealDefinition object) {
				return createUniversalRealDefinitionAdapter();
			}
			@Override
			public Adapter caseUnknownAttribute(UnknownAttribute object) {
				return createUnknownAttributeAdapter();
			}
			@Override
			public Adapter caseUnknownDiscriminantPart(UnknownDiscriminantPart object) {
				return createUnknownDiscriminantPartAdapter();
			}
			@Override
			public Adapter caseUnknownPragma(UnknownPragma object) {
				return createUnknownPragmaAdapter();
			}
			@Override
			public Adapter caseUnsuppressPragma(UnsuppressPragma object) {
				return createUnsuppressPragmaAdapter();
			}
			@Override
			public Adapter caseUseAllTypeClause(UseAllTypeClause object) {
				return createUseAllTypeClauseAdapter();
			}
			@Override
			public Adapter caseUsePackageClause(UsePackageClause object) {
				return createUsePackageClauseAdapter();
			}
			@Override
			public Adapter caseUseTypeClause(UseTypeClause object) {
				return createUseTypeClauseAdapter();
			}
			@Override
			public Adapter caseValAttribute(ValAttribute object) {
				return createValAttributeAdapter();
			}
			@Override
			public Adapter caseValidAttribute(ValidAttribute object) {
				return createValidAttributeAdapter();
			}
			@Override
			public Adapter caseValueAttribute(ValueAttribute object) {
				return createValueAttributeAdapter();
			}
			@Override
			public Adapter caseVariableDeclaration(VariableDeclaration object) {
				return createVariableDeclarationAdapter();
			}
			@Override
			public Adapter caseVariant(Variant object) {
				return createVariantAdapter();
			}
			@Override
			public Adapter caseVariantList(VariantList object) {
				return createVariantListAdapter();
			}
			@Override
			public Adapter caseVariantPart(VariantPart object) {
				return createVariantPartAdapter();
			}
			@Override
			public Adapter caseVersionAttribute(VersionAttribute object) {
				return createVersionAttributeAdapter();
			}
			@Override
			public Adapter caseVolatileComponentsPragma(VolatileComponentsPragma object) {
				return createVolatileComponentsPragmaAdapter();
			}
			@Override
			public Adapter caseVolatilePragma(VolatilePragma object) {
				return createVolatilePragmaAdapter();
			}
			@Override
			public Adapter caseWhileLoopStatement(WhileLoopStatement object) {
				return createWhileLoopStatementAdapter();
			}
			@Override
			public Adapter caseWideImageAttribute(WideImageAttribute object) {
				return createWideImageAttributeAdapter();
			}
			@Override
			public Adapter caseWideValueAttribute(WideValueAttribute object) {
				return createWideValueAttributeAdapter();
			}
			@Override
			public Adapter caseWideWideImageAttribute(WideWideImageAttribute object) {
				return createWideWideImageAttributeAdapter();
			}
			@Override
			public Adapter caseWideWideValueAttribute(WideWideValueAttribute object) {
				return createWideWideValueAttributeAdapter();
			}
			@Override
			public Adapter caseWideWideWidthAttribute(WideWideWidthAttribute object) {
				return createWideWideWidthAttributeAdapter();
			}
			@Override
			public Adapter caseWideWidthAttribute(WideWidthAttribute object) {
				return createWideWidthAttributeAdapter();
			}
			@Override
			public Adapter caseWidthAttribute(WidthAttribute object) {
				return createWidthAttributeAdapter();
			}
			@Override
			public Adapter caseWithClause(WithClause object) {
				return createWithClauseAdapter();
			}
			@Override
			public Adapter caseWriteAttribute(WriteAttribute object) {
				return createWriteAttributeAdapter();
			}
			@Override
			public Adapter caseXorOperator(XorOperator object) {
				return createXorOperatorAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link Ada.AbortStatement <em>Abort Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AbortStatement
	 * @generated
	 */
	public Adapter createAbortStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AbsOperator <em>Abs Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AbsOperator
	 * @generated
	 */
	public Adapter createAbsOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.Abstract <em>Abstract</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.Abstract
	 * @generated
	 */
	public Adapter createAbstractAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AcceptStatement <em>Accept Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AcceptStatement
	 * @generated
	 */
	public Adapter createAcceptStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AccessAttribute <em>Access Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AccessAttribute
	 * @generated
	 */
	public Adapter createAccessAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AccessToConstant <em>Access To Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AccessToConstant
	 * @generated
	 */
	public Adapter createAccessToConstantAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AccessToFunction <em>Access To Function</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AccessToFunction
	 * @generated
	 */
	public Adapter createAccessToFunctionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AccessToProcedure <em>Access To Procedure</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AccessToProcedure
	 * @generated
	 */
	public Adapter createAccessToProcedureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AccessToProtectedFunction <em>Access To Protected Function</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AccessToProtectedFunction
	 * @generated
	 */
	public Adapter createAccessToProtectedFunctionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AccessToProtectedProcedure <em>Access To Protected Procedure</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AccessToProtectedProcedure
	 * @generated
	 */
	public Adapter createAccessToProtectedProcedureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AccessToVariable <em>Access To Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AccessToVariable
	 * @generated
	 */
	public Adapter createAccessToVariableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AddressAttribute <em>Address Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AddressAttribute
	 * @generated
	 */
	public Adapter createAddressAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AdjacentAttribute <em>Adjacent Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AdjacentAttribute
	 * @generated
	 */
	public Adapter createAdjacentAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AftAttribute <em>Aft Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AftAttribute
	 * @generated
	 */
	public Adapter createAftAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.Aliased <em>Aliased</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.Aliased
	 * @generated
	 */
	public Adapter createAliasedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AlignmentAttribute <em>Alignment Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AlignmentAttribute
	 * @generated
	 */
	public Adapter createAlignmentAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AllCallsRemotePragma <em>All Calls Remote Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AllCallsRemotePragma
	 * @generated
	 */
	public Adapter createAllCallsRemotePragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AllocationFromQualifiedExpression <em>Allocation From Qualified Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AllocationFromQualifiedExpression
	 * @generated
	 */
	public Adapter createAllocationFromQualifiedExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AllocationFromSubtype <em>Allocation From Subtype</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AllocationFromSubtype
	 * @generated
	 */
	public Adapter createAllocationFromSubtypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AndOperator <em>And Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AndOperator
	 * @generated
	 */
	public Adapter createAndOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AndThenShortCircuit <em>And Then Short Circuit</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AndThenShortCircuit
	 * @generated
	 */
	public Adapter createAndThenShortCircuitAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AnonymousAccessToConstant <em>Anonymous Access To Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AnonymousAccessToConstant
	 * @generated
	 */
	public Adapter createAnonymousAccessToConstantAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AnonymousAccessToFunction <em>Anonymous Access To Function</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AnonymousAccessToFunction
	 * @generated
	 */
	public Adapter createAnonymousAccessToFunctionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AnonymousAccessToProcedure <em>Anonymous Access To Procedure</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AnonymousAccessToProcedure
	 * @generated
	 */
	public Adapter createAnonymousAccessToProcedureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AnonymousAccessToProtectedFunction <em>Anonymous Access To Protected Function</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AnonymousAccessToProtectedFunction
	 * @generated
	 */
	public Adapter createAnonymousAccessToProtectedFunctionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AnonymousAccessToProtectedProcedure <em>Anonymous Access To Protected Procedure</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AnonymousAccessToProtectedProcedure
	 * @generated
	 */
	public Adapter createAnonymousAccessToProtectedProcedureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AnonymousAccessToVariable <em>Anonymous Access To Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AnonymousAccessToVariable
	 * @generated
	 */
	public Adapter createAnonymousAccessToVariableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ArrayComponentAssociation <em>Array Component Association</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ArrayComponentAssociation
	 * @generated
	 */
	public Adapter createArrayComponentAssociationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AspectSpecification <em>Aspect Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AspectSpecification
	 * @generated
	 */
	public Adapter createAspectSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AssertionPolicyPragma <em>Assertion Policy Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AssertionPolicyPragma
	 * @generated
	 */
	public Adapter createAssertionPolicyPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AssertPragma <em>Assert Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AssertPragma
	 * @generated
	 */
	public Adapter createAssertPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AssignmentStatement <em>Assignment Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AssignmentStatement
	 * @generated
	 */
	public Adapter createAssignmentStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AssociationClass <em>Association Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AssociationClass
	 * @generated
	 */
	public Adapter createAssociationClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AssociationList <em>Association List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AssociationList
	 * @generated
	 */
	public Adapter createAssociationListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AsynchronousPragma <em>Asynchronous Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AsynchronousPragma
	 * @generated
	 */
	public Adapter createAsynchronousPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AsynchronousSelectStatement <em>Asynchronous Select Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AsynchronousSelectStatement
	 * @generated
	 */
	public Adapter createAsynchronousSelectStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AtClause <em>At Clause</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AtClause
	 * @generated
	 */
	public Adapter createAtClauseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AtomicComponentsPragma <em>Atomic Components Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AtomicComponentsPragma
	 * @generated
	 */
	public Adapter createAtomicComponentsPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AtomicPragma <em>Atomic Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AtomicPragma
	 * @generated
	 */
	public Adapter createAtomicPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AttachHandlerPragma <em>Attach Handler Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AttachHandlerPragma
	 * @generated
	 */
	public Adapter createAttachHandlerPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.AttributeDefinitionClause <em>Attribute Definition Clause</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.AttributeDefinitionClause
	 * @generated
	 */
	public Adapter createAttributeDefinitionClauseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.BaseAttribute <em>Base Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.BaseAttribute
	 * @generated
	 */
	public Adapter createBaseAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.BitOrderAttribute <em>Bit Order Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.BitOrderAttribute
	 * @generated
	 */
	public Adapter createBitOrderAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.BlockStatement <em>Block Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.BlockStatement
	 * @generated
	 */
	public Adapter createBlockStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.BodyVersionAttribute <em>Body Version Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.BodyVersionAttribute
	 * @generated
	 */
	public Adapter createBodyVersionAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.BoxExpression <em>Box Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.BoxExpression
	 * @generated
	 */
	public Adapter createBoxExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.CallableAttribute <em>Callable Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.CallableAttribute
	 * @generated
	 */
	public Adapter createCallableAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.CallerAttribute <em>Caller Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.CallerAttribute
	 * @generated
	 */
	public Adapter createCallerAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.CaseExpression <em>Case Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.CaseExpression
	 * @generated
	 */
	public Adapter createCaseExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.CaseExpressionPath <em>Case Expression Path</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.CaseExpressionPath
	 * @generated
	 */
	public Adapter createCaseExpressionPathAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.CasePath <em>Case Path</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.CasePath
	 * @generated
	 */
	public Adapter createCasePathAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.CaseStatement <em>Case Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.CaseStatement
	 * @generated
	 */
	public Adapter createCaseStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.CeilingAttribute <em>Ceiling Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.CeilingAttribute
	 * @generated
	 */
	public Adapter createCeilingAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.CharacterLiteral <em>Character Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.CharacterLiteral
	 * @generated
	 */
	public Adapter createCharacterLiteralAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ChoiceParameterSpecification <em>Choice Parameter Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ChoiceParameterSpecification
	 * @generated
	 */
	public Adapter createChoiceParameterSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ClassAttribute <em>Class Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ClassAttribute
	 * @generated
	 */
	public Adapter createClassAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.CodeStatement <em>Code Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.CodeStatement
	 * @generated
	 */
	public Adapter createCodeStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.Comment <em>Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.Comment
	 * @generated
	 */
	public Adapter createCommentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.CompilationUnit <em>Compilation Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.CompilationUnit
	 * @generated
	 */
	public Adapter createCompilationUnitAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ComponentClause <em>Component Clause</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ComponentClause
	 * @generated
	 */
	public Adapter createComponentClauseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ComponentClauseList <em>Component Clause List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ComponentClauseList
	 * @generated
	 */
	public Adapter createComponentClauseListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ComponentDeclaration <em>Component Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ComponentDeclaration
	 * @generated
	 */
	public Adapter createComponentDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ComponentDefinition <em>Component Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ComponentDefinition
	 * @generated
	 */
	public Adapter createComponentDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ComponentSizeAttribute <em>Component Size Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ComponentSizeAttribute
	 * @generated
	 */
	public Adapter createComponentSizeAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ComposeAttribute <em>Compose Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ComposeAttribute
	 * @generated
	 */
	public Adapter createComposeAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ConcatenateOperator <em>Concatenate Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ConcatenateOperator
	 * @generated
	 */
	public Adapter createConcatenateOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ConditionalEntryCallStatement <em>Conditional Entry Call Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ConditionalEntryCallStatement
	 * @generated
	 */
	public Adapter createConditionalEntryCallStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ConstantDeclaration <em>Constant Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ConstantDeclaration
	 * @generated
	 */
	public Adapter createConstantDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ConstrainedArrayDefinition <em>Constrained Array Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ConstrainedArrayDefinition
	 * @generated
	 */
	public Adapter createConstrainedArrayDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ConstrainedAttribute <em>Constrained Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ConstrainedAttribute
	 * @generated
	 */
	public Adapter createConstrainedAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ConstraintClass <em>Constraint Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ConstraintClass
	 * @generated
	 */
	public Adapter createConstraintClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ContextClauseClass <em>Context Clause Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ContextClauseClass
	 * @generated
	 */
	public Adapter createContextClauseClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ContextClauseList <em>Context Clause List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ContextClauseList
	 * @generated
	 */
	public Adapter createContextClauseListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ControlledPragma <em>Controlled Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ControlledPragma
	 * @generated
	 */
	public Adapter createControlledPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ConventionPragma <em>Convention Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ConventionPragma
	 * @generated
	 */
	public Adapter createConventionPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.CopySignAttribute <em>Copy Sign Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.CopySignAttribute
	 * @generated
	 */
	public Adapter createCopySignAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.CountAttribute <em>Count Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.CountAttribute
	 * @generated
	 */
	public Adapter createCountAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.CpuPragma <em>Cpu Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.CpuPragma
	 * @generated
	 */
	public Adapter createCpuPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DecimalFixedPointDefinition <em>Decimal Fixed Point Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DecimalFixedPointDefinition
	 * @generated
	 */
	public Adapter createDecimalFixedPointDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DeclarationClass <em>Declaration Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DeclarationClass
	 * @generated
	 */
	public Adapter createDeclarationClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DeclarationList <em>Declaration List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DeclarationList
	 * @generated
	 */
	public Adapter createDeclarationListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DeclarativeItemClass <em>Declarative Item Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DeclarativeItemClass
	 * @generated
	 */
	public Adapter createDeclarativeItemClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DeclarativeItemList <em>Declarative Item List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DeclarativeItemList
	 * @generated
	 */
	public Adapter createDeclarativeItemListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefaultStoragePoolPragma
	 * @generated
	 */
	public Adapter createDefaultStoragePoolPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DeferredConstantDeclaration <em>Deferred Constant Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DeferredConstantDeclaration
	 * @generated
	 */
	public Adapter createDeferredConstantDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningAbsOperator <em>Defining Abs Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningAbsOperator
	 * @generated
	 */
	public Adapter createDefiningAbsOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningAndOperator <em>Defining And Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningAndOperator
	 * @generated
	 */
	public Adapter createDefiningAndOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningCharacterLiteral <em>Defining Character Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningCharacterLiteral
	 * @generated
	 */
	public Adapter createDefiningCharacterLiteralAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningConcatenateOperator <em>Defining Concatenate Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningConcatenateOperator
	 * @generated
	 */
	public Adapter createDefiningConcatenateOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningDivideOperator <em>Defining Divide Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningDivideOperator
	 * @generated
	 */
	public Adapter createDefiningDivideOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningEnumerationLiteral <em>Defining Enumeration Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningEnumerationLiteral
	 * @generated
	 */
	public Adapter createDefiningEnumerationLiteralAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningEqualOperator <em>Defining Equal Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningEqualOperator
	 * @generated
	 */
	public Adapter createDefiningEqualOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningExpandedName <em>Defining Expanded Name</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningExpandedName
	 * @generated
	 */
	public Adapter createDefiningExpandedNameAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningExponentiateOperator <em>Defining Exponentiate Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningExponentiateOperator
	 * @generated
	 */
	public Adapter createDefiningExponentiateOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningGreaterThanOperator <em>Defining Greater Than Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningGreaterThanOperator
	 * @generated
	 */
	public Adapter createDefiningGreaterThanOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningGreaterThanOrEqualOperator <em>Defining Greater Than Or Equal Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningGreaterThanOrEqualOperator
	 * @generated
	 */
	public Adapter createDefiningGreaterThanOrEqualOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningIdentifier <em>Defining Identifier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningIdentifier
	 * @generated
	 */
	public Adapter createDefiningIdentifierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningLessThanOperator <em>Defining Less Than Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningLessThanOperator
	 * @generated
	 */
	public Adapter createDefiningLessThanOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningLessThanOrEqualOperator <em>Defining Less Than Or Equal Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningLessThanOrEqualOperator
	 * @generated
	 */
	public Adapter createDefiningLessThanOrEqualOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningMinusOperator <em>Defining Minus Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningMinusOperator
	 * @generated
	 */
	public Adapter createDefiningMinusOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningModOperator <em>Defining Mod Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningModOperator
	 * @generated
	 */
	public Adapter createDefiningModOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningMultiplyOperator <em>Defining Multiply Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningMultiplyOperator
	 * @generated
	 */
	public Adapter createDefiningMultiplyOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningNameClass <em>Defining Name Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningNameClass
	 * @generated
	 */
	public Adapter createDefiningNameClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningNameList <em>Defining Name List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningNameList
	 * @generated
	 */
	public Adapter createDefiningNameListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningNotEqualOperator <em>Defining Not Equal Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningNotEqualOperator
	 * @generated
	 */
	public Adapter createDefiningNotEqualOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningNotOperator <em>Defining Not Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningNotOperator
	 * @generated
	 */
	public Adapter createDefiningNotOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningOrOperator <em>Defining Or Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningOrOperator
	 * @generated
	 */
	public Adapter createDefiningOrOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningPlusOperator <em>Defining Plus Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningPlusOperator
	 * @generated
	 */
	public Adapter createDefiningPlusOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningRemOperator <em>Defining Rem Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningRemOperator
	 * @generated
	 */
	public Adapter createDefiningRemOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningUnaryMinusOperator <em>Defining Unary Minus Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningUnaryMinusOperator
	 * @generated
	 */
	public Adapter createDefiningUnaryMinusOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningUnaryPlusOperator <em>Defining Unary Plus Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningUnaryPlusOperator
	 * @generated
	 */
	public Adapter createDefiningUnaryPlusOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiningXorOperator <em>Defining Xor Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiningXorOperator
	 * @generated
	 */
	public Adapter createDefiningXorOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefiniteAttribute <em>Definite Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefiniteAttribute
	 * @generated
	 */
	public Adapter createDefiniteAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefinitionClass <em>Definition Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefinitionClass
	 * @generated
	 */
	public Adapter createDefinitionClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DefinitionList <em>Definition List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DefinitionList
	 * @generated
	 */
	public Adapter createDefinitionListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DelayRelativeStatement <em>Delay Relative Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DelayRelativeStatement
	 * @generated
	 */
	public Adapter createDelayRelativeStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DelayUntilStatement <em>Delay Until Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DelayUntilStatement
	 * @generated
	 */
	public Adapter createDelayUntilStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DeltaAttribute <em>Delta Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DeltaAttribute
	 * @generated
	 */
	public Adapter createDeltaAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DeltaConstraint <em>Delta Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DeltaConstraint
	 * @generated
	 */
	public Adapter createDeltaConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DenormAttribute <em>Denorm Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DenormAttribute
	 * @generated
	 */
	public Adapter createDenormAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DerivedRecordExtensionDefinition <em>Derived Record Extension Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DerivedRecordExtensionDefinition
	 * @generated
	 */
	public Adapter createDerivedRecordExtensionDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DerivedTypeDefinition <em>Derived Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DerivedTypeDefinition
	 * @generated
	 */
	public Adapter createDerivedTypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DetectBlockingPragma <em>Detect Blocking Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DetectBlockingPragma
	 * @generated
	 */
	public Adapter createDetectBlockingPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DigitsAttribute <em>Digits Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DigitsAttribute
	 * @generated
	 */
	public Adapter createDigitsAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DigitsConstraint <em>Digits Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DigitsConstraint
	 * @generated
	 */
	public Adapter createDigitsConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DiscardNamesPragma <em>Discard Names Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DiscardNamesPragma
	 * @generated
	 */
	public Adapter createDiscardNamesPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DiscreteRangeAttributeReference <em>Discrete Range Attribute Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DiscreteRangeAttributeReference
	 * @generated
	 */
	public Adapter createDiscreteRangeAttributeReferenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DiscreteRangeAttributeReferenceAsSubtypeDefinition <em>Discrete Range Attribute Reference As Subtype Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DiscreteRangeAttributeReferenceAsSubtypeDefinition
	 * @generated
	 */
	public Adapter createDiscreteRangeAttributeReferenceAsSubtypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DiscreteRangeClass <em>Discrete Range Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DiscreteRangeClass
	 * @generated
	 */
	public Adapter createDiscreteRangeClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DiscreteRangeList <em>Discrete Range List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DiscreteRangeList
	 * @generated
	 */
	public Adapter createDiscreteRangeListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DiscreteSimpleExpressionRange <em>Discrete Simple Expression Range</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DiscreteSimpleExpressionRange
	 * @generated
	 */
	public Adapter createDiscreteSimpleExpressionRangeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DiscreteSimpleExpressionRangeAsSubtypeDefinition <em>Discrete Simple Expression Range As Subtype Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DiscreteSimpleExpressionRangeAsSubtypeDefinition
	 * @generated
	 */
	public Adapter createDiscreteSimpleExpressionRangeAsSubtypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DiscreteSubtypeDefinitionClass <em>Discrete Subtype Definition Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DiscreteSubtypeDefinitionClass
	 * @generated
	 */
	public Adapter createDiscreteSubtypeDefinitionClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DiscreteSubtypeIndication <em>Discrete Subtype Indication</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DiscreteSubtypeIndication
	 * @generated
	 */
	public Adapter createDiscreteSubtypeIndicationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DiscreteSubtypeIndicationAsSubtypeDefinition <em>Discrete Subtype Indication As Subtype Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DiscreteSubtypeIndicationAsSubtypeDefinition
	 * @generated
	 */
	public Adapter createDiscreteSubtypeIndicationAsSubtypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DiscriminantAssociation <em>Discriminant Association</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DiscriminantAssociation
	 * @generated
	 */
	public Adapter createDiscriminantAssociationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DiscriminantAssociationList <em>Discriminant Association List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DiscriminantAssociationList
	 * @generated
	 */
	public Adapter createDiscriminantAssociationListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DiscriminantConstraint <em>Discriminant Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DiscriminantConstraint
	 * @generated
	 */
	public Adapter createDiscriminantConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DiscriminantSpecification <em>Discriminant Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DiscriminantSpecification
	 * @generated
	 */
	public Adapter createDiscriminantSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DiscriminantSpecificationList <em>Discriminant Specification List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DiscriminantSpecificationList
	 * @generated
	 */
	public Adapter createDiscriminantSpecificationListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DispatchingDomainPragma <em>Dispatching Domain Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DispatchingDomainPragma
	 * @generated
	 */
	public Adapter createDispatchingDomainPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DivideOperator <em>Divide Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DivideOperator
	 * @generated
	 */
	public Adapter createDivideOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.DocumentRoot
	 * @generated
	 */
	public Adapter createDocumentRootAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ElaborateAllPragma <em>Elaborate All Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ElaborateAllPragma
	 * @generated
	 */
	public Adapter createElaborateAllPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ElaborateBodyPragma <em>Elaborate Body Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ElaborateBodyPragma
	 * @generated
	 */
	public Adapter createElaborateBodyPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ElaboratePragma <em>Elaborate Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ElaboratePragma
	 * @generated
	 */
	public Adapter createElaboratePragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ElementClass <em>Element Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ElementClass
	 * @generated
	 */
	public Adapter createElementClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ElementIteratorSpecification <em>Element Iterator Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ElementIteratorSpecification
	 * @generated
	 */
	public Adapter createElementIteratorSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ElementList <em>Element List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ElementList
	 * @generated
	 */
	public Adapter createElementListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ElseExpressionPath <em>Else Expression Path</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ElseExpressionPath
	 * @generated
	 */
	public Adapter createElseExpressionPathAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ElsePath <em>Else Path</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ElsePath
	 * @generated
	 */
	public Adapter createElsePathAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ElsifExpressionPath <em>Elsif Expression Path</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ElsifExpressionPath
	 * @generated
	 */
	public Adapter createElsifExpressionPathAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ElsifPath <em>Elsif Path</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ElsifPath
	 * @generated
	 */
	public Adapter createElsifPathAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.EntryBodyDeclaration <em>Entry Body Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.EntryBodyDeclaration
	 * @generated
	 */
	public Adapter createEntryBodyDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.EntryCallStatement <em>Entry Call Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.EntryCallStatement
	 * @generated
	 */
	public Adapter createEntryCallStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.EntryDeclaration <em>Entry Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.EntryDeclaration
	 * @generated
	 */
	public Adapter createEntryDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.EntryIndexSpecification <em>Entry Index Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.EntryIndexSpecification
	 * @generated
	 */
	public Adapter createEntryIndexSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.EnumerationLiteral <em>Enumeration Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.EnumerationLiteral
	 * @generated
	 */
	public Adapter createEnumerationLiteralAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.EnumerationLiteralSpecification <em>Enumeration Literal Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.EnumerationLiteralSpecification
	 * @generated
	 */
	public Adapter createEnumerationLiteralSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.EnumerationRepresentationClause <em>Enumeration Representation Clause</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.EnumerationRepresentationClause
	 * @generated
	 */
	public Adapter createEnumerationRepresentationClauseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.EnumerationTypeDefinition <em>Enumeration Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.EnumerationTypeDefinition
	 * @generated
	 */
	public Adapter createEnumerationTypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.EqualOperator <em>Equal Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.EqualOperator
	 * @generated
	 */
	public Adapter createEqualOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ExceptionDeclaration <em>Exception Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ExceptionDeclaration
	 * @generated
	 */
	public Adapter createExceptionDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ExceptionHandler <em>Exception Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ExceptionHandler
	 * @generated
	 */
	public Adapter createExceptionHandlerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ExceptionHandlerList <em>Exception Handler List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ExceptionHandlerList
	 * @generated
	 */
	public Adapter createExceptionHandlerListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ExceptionRenamingDeclaration <em>Exception Renaming Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ExceptionRenamingDeclaration
	 * @generated
	 */
	public Adapter createExceptionRenamingDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ExitStatement <em>Exit Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ExitStatement
	 * @generated
	 */
	public Adapter createExitStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ExplicitDereference <em>Explicit Dereference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ExplicitDereference
	 * @generated
	 */
	public Adapter createExplicitDereferenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ExponentAttribute <em>Exponent Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ExponentAttribute
	 * @generated
	 */
	public Adapter createExponentAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ExponentiateOperator <em>Exponentiate Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ExponentiateOperator
	 * @generated
	 */
	public Adapter createExponentiateOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ExportPragma <em>Export Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ExportPragma
	 * @generated
	 */
	public Adapter createExportPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ExpressionClass <em>Expression Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ExpressionClass
	 * @generated
	 */
	public Adapter createExpressionClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ExpressionFunctionDeclaration <em>Expression Function Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ExpressionFunctionDeclaration
	 * @generated
	 */
	public Adapter createExpressionFunctionDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ExpressionList <em>Expression List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ExpressionList
	 * @generated
	 */
	public Adapter createExpressionListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ExtendedReturnStatement <em>Extended Return Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ExtendedReturnStatement
	 * @generated
	 */
	public Adapter createExtendedReturnStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ExtensionAggregate <em>Extension Aggregate</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ExtensionAggregate
	 * @generated
	 */
	public Adapter createExtensionAggregateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ExternalTagAttribute <em>External Tag Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ExternalTagAttribute
	 * @generated
	 */
	public Adapter createExternalTagAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FirstAttribute <em>First Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FirstAttribute
	 * @generated
	 */
	public Adapter createFirstAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FirstBitAttribute <em>First Bit Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FirstBitAttribute
	 * @generated
	 */
	public Adapter createFirstBitAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FloatingPointDefinition <em>Floating Point Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FloatingPointDefinition
	 * @generated
	 */
	public Adapter createFloatingPointDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FloorAttribute <em>Floor Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FloorAttribute
	 * @generated
	 */
	public Adapter createFloorAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ForAllQuantifiedExpression <em>For All Quantified Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ForAllQuantifiedExpression
	 * @generated
	 */
	public Adapter createForAllQuantifiedExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ForeAttribute <em>Fore Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ForeAttribute
	 * @generated
	 */
	public Adapter createForeAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ForLoopStatement <em>For Loop Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ForLoopStatement
	 * @generated
	 */
	public Adapter createForLoopStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalAccessToConstant <em>Formal Access To Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalAccessToConstant
	 * @generated
	 */
	public Adapter createFormalAccessToConstantAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalAccessToFunction <em>Formal Access To Function</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalAccessToFunction
	 * @generated
	 */
	public Adapter createFormalAccessToFunctionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalAccessToProcedure <em>Formal Access To Procedure</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalAccessToProcedure
	 * @generated
	 */
	public Adapter createFormalAccessToProcedureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalAccessToProtectedFunction <em>Formal Access To Protected Function</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalAccessToProtectedFunction
	 * @generated
	 */
	public Adapter createFormalAccessToProtectedFunctionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalAccessToProtectedProcedure <em>Formal Access To Protected Procedure</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalAccessToProtectedProcedure
	 * @generated
	 */
	public Adapter createFormalAccessToProtectedProcedureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalAccessToVariable <em>Formal Access To Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalAccessToVariable
	 * @generated
	 */
	public Adapter createFormalAccessToVariableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalConstrainedArrayDefinition <em>Formal Constrained Array Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalConstrainedArrayDefinition
	 * @generated
	 */
	public Adapter createFormalConstrainedArrayDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalDecimalFixedPointDefinition <em>Formal Decimal Fixed Point Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalDecimalFixedPointDefinition
	 * @generated
	 */
	public Adapter createFormalDecimalFixedPointDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalDerivedTypeDefinition <em>Formal Derived Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalDerivedTypeDefinition
	 * @generated
	 */
	public Adapter createFormalDerivedTypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalDiscreteTypeDefinition <em>Formal Discrete Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalDiscreteTypeDefinition
	 * @generated
	 */
	public Adapter createFormalDiscreteTypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalFloatingPointDefinition <em>Formal Floating Point Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalFloatingPointDefinition
	 * @generated
	 */
	public Adapter createFormalFloatingPointDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalFunctionDeclaration <em>Formal Function Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalFunctionDeclaration
	 * @generated
	 */
	public Adapter createFormalFunctionDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalIncompleteTypeDeclaration <em>Formal Incomplete Type Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalIncompleteTypeDeclaration
	 * @generated
	 */
	public Adapter createFormalIncompleteTypeDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalLimitedInterface <em>Formal Limited Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalLimitedInterface
	 * @generated
	 */
	public Adapter createFormalLimitedInterfaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalModularTypeDefinition <em>Formal Modular Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalModularTypeDefinition
	 * @generated
	 */
	public Adapter createFormalModularTypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalObjectDeclaration <em>Formal Object Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalObjectDeclaration
	 * @generated
	 */
	public Adapter createFormalObjectDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalOrdinaryFixedPointDefinition <em>Formal Ordinary Fixed Point Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalOrdinaryFixedPointDefinition
	 * @generated
	 */
	public Adapter createFormalOrdinaryFixedPointDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalOrdinaryInterface <em>Formal Ordinary Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalOrdinaryInterface
	 * @generated
	 */
	public Adapter createFormalOrdinaryInterfaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalPackageDeclaration <em>Formal Package Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalPackageDeclaration
	 * @generated
	 */
	public Adapter createFormalPackageDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalPackageDeclarationWithBox <em>Formal Package Declaration With Box</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalPackageDeclarationWithBox
	 * @generated
	 */
	public Adapter createFormalPackageDeclarationWithBoxAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalPoolSpecificAccessToVariable <em>Formal Pool Specific Access To Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalPoolSpecificAccessToVariable
	 * @generated
	 */
	public Adapter createFormalPoolSpecificAccessToVariableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalPrivateTypeDefinition <em>Formal Private Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalPrivateTypeDefinition
	 * @generated
	 */
	public Adapter createFormalPrivateTypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalProcedureDeclaration <em>Formal Procedure Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalProcedureDeclaration
	 * @generated
	 */
	public Adapter createFormalProcedureDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalProtectedInterface <em>Formal Protected Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalProtectedInterface
	 * @generated
	 */
	public Adapter createFormalProtectedInterfaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalSignedIntegerTypeDefinition <em>Formal Signed Integer Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalSignedIntegerTypeDefinition
	 * @generated
	 */
	public Adapter createFormalSignedIntegerTypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalSynchronizedInterface <em>Formal Synchronized Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalSynchronizedInterface
	 * @generated
	 */
	public Adapter createFormalSynchronizedInterfaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalTaggedPrivateTypeDefinition <em>Formal Tagged Private Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalTaggedPrivateTypeDefinition
	 * @generated
	 */
	public Adapter createFormalTaggedPrivateTypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalTaskInterface <em>Formal Task Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalTaskInterface
	 * @generated
	 */
	public Adapter createFormalTaskInterfaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalTypeDeclaration <em>Formal Type Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalTypeDeclaration
	 * @generated
	 */
	public Adapter createFormalTypeDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FormalUnconstrainedArrayDefinition <em>Formal Unconstrained Array Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FormalUnconstrainedArrayDefinition
	 * @generated
	 */
	public Adapter createFormalUnconstrainedArrayDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ForSomeQuantifiedExpression <em>For Some Quantified Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ForSomeQuantifiedExpression
	 * @generated
	 */
	public Adapter createForSomeQuantifiedExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FractionAttribute <em>Fraction Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FractionAttribute
	 * @generated
	 */
	public Adapter createFractionAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FunctionBodyDeclaration <em>Function Body Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FunctionBodyDeclaration
	 * @generated
	 */
	public Adapter createFunctionBodyDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FunctionBodyStub <em>Function Body Stub</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FunctionBodyStub
	 * @generated
	 */
	public Adapter createFunctionBodyStubAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FunctionCall <em>Function Call</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FunctionCall
	 * @generated
	 */
	public Adapter createFunctionCallAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FunctionDeclaration <em>Function Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FunctionDeclaration
	 * @generated
	 */
	public Adapter createFunctionDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FunctionInstantiation <em>Function Instantiation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FunctionInstantiation
	 * @generated
	 */
	public Adapter createFunctionInstantiationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.FunctionRenamingDeclaration <em>Function Renaming Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.FunctionRenamingDeclaration
	 * @generated
	 */
	public Adapter createFunctionRenamingDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.GeneralizedIteratorSpecification <em>Generalized Iterator Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.GeneralizedIteratorSpecification
	 * @generated
	 */
	public Adapter createGeneralizedIteratorSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.GenericAssociation <em>Generic Association</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.GenericAssociation
	 * @generated
	 */
	public Adapter createGenericAssociationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.GenericFunctionDeclaration <em>Generic Function Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.GenericFunctionDeclaration
	 * @generated
	 */
	public Adapter createGenericFunctionDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.GenericFunctionRenamingDeclaration <em>Generic Function Renaming Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.GenericFunctionRenamingDeclaration
	 * @generated
	 */
	public Adapter createGenericFunctionRenamingDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.GenericPackageDeclaration <em>Generic Package Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.GenericPackageDeclaration
	 * @generated
	 */
	public Adapter createGenericPackageDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.GenericPackageRenamingDeclaration <em>Generic Package Renaming Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.GenericPackageRenamingDeclaration
	 * @generated
	 */
	public Adapter createGenericPackageRenamingDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.GenericProcedureDeclaration <em>Generic Procedure Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.GenericProcedureDeclaration
	 * @generated
	 */
	public Adapter createGenericProcedureDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.GenericProcedureRenamingDeclaration <em>Generic Procedure Renaming Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.GenericProcedureRenamingDeclaration
	 * @generated
	 */
	public Adapter createGenericProcedureRenamingDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.GotoStatement <em>Goto Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.GotoStatement
	 * @generated
	 */
	public Adapter createGotoStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.GreaterThanOperator <em>Greater Than Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.GreaterThanOperator
	 * @generated
	 */
	public Adapter createGreaterThanOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.GreaterThanOrEqualOperator <em>Greater Than Or Equal Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.GreaterThanOrEqualOperator
	 * @generated
	 */
	public Adapter createGreaterThanOrEqualOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasAbstractQType <em>Has Abstract QType</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasAbstractQType
	 * @generated
	 */
	public Adapter createHasAbstractQTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasAbstractQType1 <em>Has Abstract QType1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasAbstractQType1
	 * @generated
	 */
	public Adapter createHasAbstractQType1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasAbstractQType2 <em>Has Abstract QType2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasAbstractQType2
	 * @generated
	 */
	public Adapter createHasAbstractQType2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasAbstractQType3 <em>Has Abstract QType3</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasAbstractQType3
	 * @generated
	 */
	public Adapter createHasAbstractQType3Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasAbstractQType4 <em>Has Abstract QType4</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasAbstractQType4
	 * @generated
	 */
	public Adapter createHasAbstractQType4Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasAbstractQType5 <em>Has Abstract QType5</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasAbstractQType5
	 * @generated
	 */
	public Adapter createHasAbstractQType5Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasAbstractQType6 <em>Has Abstract QType6</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasAbstractQType6
	 * @generated
	 */
	public Adapter createHasAbstractQType6Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasAbstractQType7 <em>Has Abstract QType7</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasAbstractQType7
	 * @generated
	 */
	public Adapter createHasAbstractQType7Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasAbstractQType8 <em>Has Abstract QType8</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasAbstractQType8
	 * @generated
	 */
	public Adapter createHasAbstractQType8Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasAbstractQType9 <em>Has Abstract QType9</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasAbstractQType9
	 * @generated
	 */
	public Adapter createHasAbstractQType9Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasAbstractQType10 <em>Has Abstract QType10</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasAbstractQType10
	 * @generated
	 */
	public Adapter createHasAbstractQType10Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasAbstractQType11 <em>Has Abstract QType11</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasAbstractQType11
	 * @generated
	 */
	public Adapter createHasAbstractQType11Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasAbstractQType12 <em>Has Abstract QType12</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasAbstractQType12
	 * @generated
	 */
	public Adapter createHasAbstractQType12Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasAbstractQType13 <em>Has Abstract QType13</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasAbstractQType13
	 * @generated
	 */
	public Adapter createHasAbstractQType13Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasAliasedQType <em>Has Aliased QType</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasAliasedQType
	 * @generated
	 */
	public Adapter createHasAliasedQTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasAliasedQType1 <em>Has Aliased QType1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasAliasedQType1
	 * @generated
	 */
	public Adapter createHasAliasedQType1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasAliasedQType2 <em>Has Aliased QType2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasAliasedQType2
	 * @generated
	 */
	public Adapter createHasAliasedQType2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasAliasedQType3 <em>Has Aliased QType3</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasAliasedQType3
	 * @generated
	 */
	public Adapter createHasAliasedQType3Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasAliasedQType4 <em>Has Aliased QType4</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasAliasedQType4
	 * @generated
	 */
	public Adapter createHasAliasedQType4Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasAliasedQType5 <em>Has Aliased QType5</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasAliasedQType5
	 * @generated
	 */
	public Adapter createHasAliasedQType5Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasAliasedQType6 <em>Has Aliased QType6</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasAliasedQType6
	 * @generated
	 */
	public Adapter createHasAliasedQType6Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasAliasedQType7 <em>Has Aliased QType7</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasAliasedQType7
	 * @generated
	 */
	public Adapter createHasAliasedQType7Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasAliasedQType8 <em>Has Aliased QType8</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasAliasedQType8
	 * @generated
	 */
	public Adapter createHasAliasedQType8Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasLimitedQType <em>Has Limited QType</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasLimitedQType
	 * @generated
	 */
	public Adapter createHasLimitedQTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasLimitedQType1 <em>Has Limited QType1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasLimitedQType1
	 * @generated
	 */
	public Adapter createHasLimitedQType1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasLimitedQType2 <em>Has Limited QType2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasLimitedQType2
	 * @generated
	 */
	public Adapter createHasLimitedQType2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasLimitedQType3 <em>Has Limited QType3</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasLimitedQType3
	 * @generated
	 */
	public Adapter createHasLimitedQType3Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasLimitedQType4 <em>Has Limited QType4</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasLimitedQType4
	 * @generated
	 */
	public Adapter createHasLimitedQType4Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasLimitedQType5 <em>Has Limited QType5</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasLimitedQType5
	 * @generated
	 */
	public Adapter createHasLimitedQType5Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasLimitedQType6 <em>Has Limited QType6</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasLimitedQType6
	 * @generated
	 */
	public Adapter createHasLimitedQType6Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasLimitedQType7 <em>Has Limited QType7</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasLimitedQType7
	 * @generated
	 */
	public Adapter createHasLimitedQType7Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasLimitedQType8 <em>Has Limited QType8</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasLimitedQType8
	 * @generated
	 */
	public Adapter createHasLimitedQType8Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasLimitedQType9 <em>Has Limited QType9</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasLimitedQType9
	 * @generated
	 */
	public Adapter createHasLimitedQType9Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasLimitedQType10 <em>Has Limited QType10</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasLimitedQType10
	 * @generated
	 */
	public Adapter createHasLimitedQType10Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasLimitedQType11 <em>Has Limited QType11</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasLimitedQType11
	 * @generated
	 */
	public Adapter createHasLimitedQType11Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasNullExclusionQType <em>Has Null Exclusion QType</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasNullExclusionQType
	 * @generated
	 */
	public Adapter createHasNullExclusionQTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasNullExclusionQType1 <em>Has Null Exclusion QType1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasNullExclusionQType1
	 * @generated
	 */
	public Adapter createHasNullExclusionQType1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasNullExclusionQType2 <em>Has Null Exclusion QType2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasNullExclusionQType2
	 * @generated
	 */
	public Adapter createHasNullExclusionQType2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasNullExclusionQType3 <em>Has Null Exclusion QType3</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasNullExclusionQType3
	 * @generated
	 */
	public Adapter createHasNullExclusionQType3Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasNullExclusionQType4 <em>Has Null Exclusion QType4</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasNullExclusionQType4
	 * @generated
	 */
	public Adapter createHasNullExclusionQType4Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasNullExclusionQType5 <em>Has Null Exclusion QType5</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasNullExclusionQType5
	 * @generated
	 */
	public Adapter createHasNullExclusionQType5Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasNullExclusionQType6 <em>Has Null Exclusion QType6</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasNullExclusionQType6
	 * @generated
	 */
	public Adapter createHasNullExclusionQType6Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasNullExclusionQType7 <em>Has Null Exclusion QType7</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasNullExclusionQType7
	 * @generated
	 */
	public Adapter createHasNullExclusionQType7Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasNullExclusionQType8 <em>Has Null Exclusion QType8</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasNullExclusionQType8
	 * @generated
	 */
	public Adapter createHasNullExclusionQType8Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasNullExclusionQType9 <em>Has Null Exclusion QType9</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasNullExclusionQType9
	 * @generated
	 */
	public Adapter createHasNullExclusionQType9Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasNullExclusionQType10 <em>Has Null Exclusion QType10</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasNullExclusionQType10
	 * @generated
	 */
	public Adapter createHasNullExclusionQType10Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasNullExclusionQType11 <em>Has Null Exclusion QType11</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasNullExclusionQType11
	 * @generated
	 */
	public Adapter createHasNullExclusionQType11Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasNullExclusionQType12 <em>Has Null Exclusion QType12</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasNullExclusionQType12
	 * @generated
	 */
	public Adapter createHasNullExclusionQType12Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasNullExclusionQType13 <em>Has Null Exclusion QType13</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasNullExclusionQType13
	 * @generated
	 */
	public Adapter createHasNullExclusionQType13Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasNullExclusionQType14 <em>Has Null Exclusion QType14</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasNullExclusionQType14
	 * @generated
	 */
	public Adapter createHasNullExclusionQType14Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasNullExclusionQType15 <em>Has Null Exclusion QType15</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasNullExclusionQType15
	 * @generated
	 */
	public Adapter createHasNullExclusionQType15Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasNullExclusionQType16 <em>Has Null Exclusion QType16</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasNullExclusionQType16
	 * @generated
	 */
	public Adapter createHasNullExclusionQType16Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasNullExclusionQType17 <em>Has Null Exclusion QType17</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasNullExclusionQType17
	 * @generated
	 */
	public Adapter createHasNullExclusionQType17Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasNullExclusionQType18 <em>Has Null Exclusion QType18</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasNullExclusionQType18
	 * @generated
	 */
	public Adapter createHasNullExclusionQType18Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasNullExclusionQType19 <em>Has Null Exclusion QType19</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasNullExclusionQType19
	 * @generated
	 */
	public Adapter createHasNullExclusionQType19Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasNullExclusionQType20 <em>Has Null Exclusion QType20</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasNullExclusionQType20
	 * @generated
	 */
	public Adapter createHasNullExclusionQType20Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasNullExclusionQType21 <em>Has Null Exclusion QType21</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasNullExclusionQType21
	 * @generated
	 */
	public Adapter createHasNullExclusionQType21Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasNullExclusionQType22 <em>Has Null Exclusion QType22</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasNullExclusionQType22
	 * @generated
	 */
	public Adapter createHasNullExclusionQType22Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasNullExclusionQType23 <em>Has Null Exclusion QType23</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasNullExclusionQType23
	 * @generated
	 */
	public Adapter createHasNullExclusionQType23Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasNullExclusionQType24 <em>Has Null Exclusion QType24</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasNullExclusionQType24
	 * @generated
	 */
	public Adapter createHasNullExclusionQType24Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasPrivateQType <em>Has Private QType</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasPrivateQType
	 * @generated
	 */
	public Adapter createHasPrivateQTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasPrivateQType1 <em>Has Private QType1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasPrivateQType1
	 * @generated
	 */
	public Adapter createHasPrivateQType1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasReverseQType <em>Has Reverse QType</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasReverseQType
	 * @generated
	 */
	public Adapter createHasReverseQTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasReverseQType1 <em>Has Reverse QType1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasReverseQType1
	 * @generated
	 */
	public Adapter createHasReverseQType1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasReverseQType2 <em>Has Reverse QType2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasReverseQType2
	 * @generated
	 */
	public Adapter createHasReverseQType2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasSynchronizedQType <em>Has Synchronized QType</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasSynchronizedQType
	 * @generated
	 */
	public Adapter createHasSynchronizedQTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasSynchronizedQType1 <em>Has Synchronized QType1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasSynchronizedQType1
	 * @generated
	 */
	public Adapter createHasSynchronizedQType1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.HasTaggedQType <em>Has Tagged QType</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.HasTaggedQType
	 * @generated
	 */
	public Adapter createHasTaggedQTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.Identifier <em>Identifier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.Identifier
	 * @generated
	 */
	public Adapter createIdentifierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IdentityAttribute <em>Identity Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IdentityAttribute
	 * @generated
	 */
	public Adapter createIdentityAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IfExpression <em>If Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IfExpression
	 * @generated
	 */
	public Adapter createIfExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IfExpressionPath <em>If Expression Path</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IfExpressionPath
	 * @generated
	 */
	public Adapter createIfExpressionPathAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IfPath <em>If Path</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IfPath
	 * @generated
	 */
	public Adapter createIfPathAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IfStatement <em>If Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IfStatement
	 * @generated
	 */
	public Adapter createIfStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ImageAttribute <em>Image Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ImageAttribute
	 * @generated
	 */
	public Adapter createImageAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ImplementationDefinedAttribute <em>Implementation Defined Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ImplementationDefinedAttribute
	 * @generated
	 */
	public Adapter createImplementationDefinedAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ImplementationDefinedPragma <em>Implementation Defined Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ImplementationDefinedPragma
	 * @generated
	 */
	public Adapter createImplementationDefinedPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ImportPragma <em>Import Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ImportPragma
	 * @generated
	 */
	public Adapter createImportPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IncompleteTypeDeclaration <em>Incomplete Type Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IncompleteTypeDeclaration
	 * @generated
	 */
	public Adapter createIncompleteTypeDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IndependentComponentsPragma <em>Independent Components Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IndependentComponentsPragma
	 * @generated
	 */
	public Adapter createIndependentComponentsPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IndependentPragma <em>Independent Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IndependentPragma
	 * @generated
	 */
	public Adapter createIndependentPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IndexConstraint <em>Index Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IndexConstraint
	 * @generated
	 */
	public Adapter createIndexConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IndexedComponent <em>Indexed Component</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IndexedComponent
	 * @generated
	 */
	public Adapter createIndexedComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.InlinePragma <em>Inline Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.InlinePragma
	 * @generated
	 */
	public Adapter createInlinePragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.InMembershipTest <em>In Membership Test</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.InMembershipTest
	 * @generated
	 */
	public Adapter createInMembershipTestAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.InputAttribute <em>Input Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.InputAttribute
	 * @generated
	 */
	public Adapter createInputAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.InspectionPointPragma <em>Inspection Point Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.InspectionPointPragma
	 * @generated
	 */
	public Adapter createInspectionPointPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IntegerLiteral <em>Integer Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IntegerLiteral
	 * @generated
	 */
	public Adapter createIntegerLiteralAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IntegerNumberDeclaration <em>Integer Number Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IntegerNumberDeclaration
	 * @generated
	 */
	public Adapter createIntegerNumberDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.InterruptHandlerPragma <em>Interrupt Handler Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.InterruptHandlerPragma
	 * @generated
	 */
	public Adapter createInterruptHandlerPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.InterruptPriorityPragma <em>Interrupt Priority Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.InterruptPriorityPragma
	 * @generated
	 */
	public Adapter createInterruptPriorityPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsNotNullReturnQType <em>Is Not Null Return QType</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsNotNullReturnQType
	 * @generated
	 */
	public Adapter createIsNotNullReturnQTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsNotNullReturnQType1 <em>Is Not Null Return QType1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsNotNullReturnQType1
	 * @generated
	 */
	public Adapter createIsNotNullReturnQType1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsNotNullReturnQType2 <em>Is Not Null Return QType2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsNotNullReturnQType2
	 * @generated
	 */
	public Adapter createIsNotNullReturnQType2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsNotNullReturnQType3 <em>Is Not Null Return QType3</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsNotNullReturnQType3
	 * @generated
	 */
	public Adapter createIsNotNullReturnQType3Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsNotNullReturnQType4 <em>Is Not Null Return QType4</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsNotNullReturnQType4
	 * @generated
	 */
	public Adapter createIsNotNullReturnQType4Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsNotNullReturnQType5 <em>Is Not Null Return QType5</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsNotNullReturnQType5
	 * @generated
	 */
	public Adapter createIsNotNullReturnQType5Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsNotNullReturnQType6 <em>Is Not Null Return QType6</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsNotNullReturnQType6
	 * @generated
	 */
	public Adapter createIsNotNullReturnQType6Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsNotNullReturnQType7 <em>Is Not Null Return QType7</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsNotNullReturnQType7
	 * @generated
	 */
	public Adapter createIsNotNullReturnQType7Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsNotNullReturnQType8 <em>Is Not Null Return QType8</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsNotNullReturnQType8
	 * @generated
	 */
	public Adapter createIsNotNullReturnQType8Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsNotNullReturnQType9 <em>Is Not Null Return QType9</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsNotNullReturnQType9
	 * @generated
	 */
	public Adapter createIsNotNullReturnQType9Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsNotNullReturnQType10 <em>Is Not Null Return QType10</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsNotNullReturnQType10
	 * @generated
	 */
	public Adapter createIsNotNullReturnQType10Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsNotNullReturnQType11 <em>Is Not Null Return QType11</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsNotNullReturnQType11
	 * @generated
	 */
	public Adapter createIsNotNullReturnQType11Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsNotOverridingDeclarationQType <em>Is Not Overriding Declaration QType</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsNotOverridingDeclarationQType
	 * @generated
	 */
	public Adapter createIsNotOverridingDeclarationQTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsNotOverridingDeclarationQType1 <em>Is Not Overriding Declaration QType1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsNotOverridingDeclarationQType1
	 * @generated
	 */
	public Adapter createIsNotOverridingDeclarationQType1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsNotOverridingDeclarationQType2 <em>Is Not Overriding Declaration QType2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsNotOverridingDeclarationQType2
	 * @generated
	 */
	public Adapter createIsNotOverridingDeclarationQType2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsNotOverridingDeclarationQType3 <em>Is Not Overriding Declaration QType3</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsNotOverridingDeclarationQType3
	 * @generated
	 */
	public Adapter createIsNotOverridingDeclarationQType3Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsNotOverridingDeclarationQType4 <em>Is Not Overriding Declaration QType4</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsNotOverridingDeclarationQType4
	 * @generated
	 */
	public Adapter createIsNotOverridingDeclarationQType4Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsNotOverridingDeclarationQType5 <em>Is Not Overriding Declaration QType5</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsNotOverridingDeclarationQType5
	 * @generated
	 */
	public Adapter createIsNotOverridingDeclarationQType5Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsNotOverridingDeclarationQType6 <em>Is Not Overriding Declaration QType6</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsNotOverridingDeclarationQType6
	 * @generated
	 */
	public Adapter createIsNotOverridingDeclarationQType6Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsNotOverridingDeclarationQType7 <em>Is Not Overriding Declaration QType7</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsNotOverridingDeclarationQType7
	 * @generated
	 */
	public Adapter createIsNotOverridingDeclarationQType7Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsNotOverridingDeclarationQType8 <em>Is Not Overriding Declaration QType8</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsNotOverridingDeclarationQType8
	 * @generated
	 */
	public Adapter createIsNotOverridingDeclarationQType8Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsNotOverridingDeclarationQType9 <em>Is Not Overriding Declaration QType9</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsNotOverridingDeclarationQType9
	 * @generated
	 */
	public Adapter createIsNotOverridingDeclarationQType9Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsNotOverridingDeclarationQType10 <em>Is Not Overriding Declaration QType10</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsNotOverridingDeclarationQType10
	 * @generated
	 */
	public Adapter createIsNotOverridingDeclarationQType10Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsNotOverridingDeclarationQType11 <em>Is Not Overriding Declaration QType11</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsNotOverridingDeclarationQType11
	 * @generated
	 */
	public Adapter createIsNotOverridingDeclarationQType11Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsOverridingDeclarationQType <em>Is Overriding Declaration QType</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsOverridingDeclarationQType
	 * @generated
	 */
	public Adapter createIsOverridingDeclarationQTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsOverridingDeclarationQType1 <em>Is Overriding Declaration QType1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsOverridingDeclarationQType1
	 * @generated
	 */
	public Adapter createIsOverridingDeclarationQType1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsOverridingDeclarationQType2 <em>Is Overriding Declaration QType2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsOverridingDeclarationQType2
	 * @generated
	 */
	public Adapter createIsOverridingDeclarationQType2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsOverridingDeclarationQType3 <em>Is Overriding Declaration QType3</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsOverridingDeclarationQType3
	 * @generated
	 */
	public Adapter createIsOverridingDeclarationQType3Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsOverridingDeclarationQType4 <em>Is Overriding Declaration QType4</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsOverridingDeclarationQType4
	 * @generated
	 */
	public Adapter createIsOverridingDeclarationQType4Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsOverridingDeclarationQType5 <em>Is Overriding Declaration QType5</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsOverridingDeclarationQType5
	 * @generated
	 */
	public Adapter createIsOverridingDeclarationQType5Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsOverridingDeclarationQType6 <em>Is Overriding Declaration QType6</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsOverridingDeclarationQType6
	 * @generated
	 */
	public Adapter createIsOverridingDeclarationQType6Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsOverridingDeclarationQType7 <em>Is Overriding Declaration QType7</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsOverridingDeclarationQType7
	 * @generated
	 */
	public Adapter createIsOverridingDeclarationQType7Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsOverridingDeclarationQType8 <em>Is Overriding Declaration QType8</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsOverridingDeclarationQType8
	 * @generated
	 */
	public Adapter createIsOverridingDeclarationQType8Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsOverridingDeclarationQType9 <em>Is Overriding Declaration QType9</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsOverridingDeclarationQType9
	 * @generated
	 */
	public Adapter createIsOverridingDeclarationQType9Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsOverridingDeclarationQType10 <em>Is Overriding Declaration QType10</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsOverridingDeclarationQType10
	 * @generated
	 */
	public Adapter createIsOverridingDeclarationQType10Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsOverridingDeclarationQType11 <em>Is Overriding Declaration QType11</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsOverridingDeclarationQType11
	 * @generated
	 */
	public Adapter createIsOverridingDeclarationQType11Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsPrefixCall <em>Is Prefix Call</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsPrefixCall
	 * @generated
	 */
	public Adapter createIsPrefixCallAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsPrefixCallQType <em>Is Prefix Call QType</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsPrefixCallQType
	 * @generated
	 */
	public Adapter createIsPrefixCallQTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsPrefixNotation <em>Is Prefix Notation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsPrefixNotation
	 * @generated
	 */
	public Adapter createIsPrefixNotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsPrefixNotationQType <em>Is Prefix Notation QType</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsPrefixNotationQType
	 * @generated
	 */
	public Adapter createIsPrefixNotationQTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.IsPrefixNotationQType1 <em>Is Prefix Notation QType1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.IsPrefixNotationQType1
	 * @generated
	 */
	public Adapter createIsPrefixNotationQType1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.KnownDiscriminantPart <em>Known Discriminant Part</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.KnownDiscriminantPart
	 * @generated
	 */
	public Adapter createKnownDiscriminantPartAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.LastAttribute <em>Last Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.LastAttribute
	 * @generated
	 */
	public Adapter createLastAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.LastBitAttribute <em>Last Bit Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.LastBitAttribute
	 * @generated
	 */
	public Adapter createLastBitAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.LeadingPartAttribute <em>Leading Part Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.LeadingPartAttribute
	 * @generated
	 */
	public Adapter createLeadingPartAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.LengthAttribute <em>Length Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.LengthAttribute
	 * @generated
	 */
	public Adapter createLengthAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.LessThanOperator <em>Less Than Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.LessThanOperator
	 * @generated
	 */
	public Adapter createLessThanOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.LessThanOrEqualOperator <em>Less Than Or Equal Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.LessThanOrEqualOperator
	 * @generated
	 */
	public Adapter createLessThanOrEqualOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.Limited <em>Limited</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.Limited
	 * @generated
	 */
	public Adapter createLimitedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.LimitedInterface <em>Limited Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.LimitedInterface
	 * @generated
	 */
	public Adapter createLimitedInterfaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.LinkerOptionsPragma <em>Linker Options Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.LinkerOptionsPragma
	 * @generated
	 */
	public Adapter createLinkerOptionsPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ListPragma <em>List Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ListPragma
	 * @generated
	 */
	public Adapter createListPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.LockingPolicyPragma <em>Locking Policy Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.LockingPolicyPragma
	 * @generated
	 */
	public Adapter createLockingPolicyPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.LoopParameterSpecification <em>Loop Parameter Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.LoopParameterSpecification
	 * @generated
	 */
	public Adapter createLoopParameterSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.LoopStatement <em>Loop Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.LoopStatement
	 * @generated
	 */
	public Adapter createLoopStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.MachineAttribute <em>Machine Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.MachineAttribute
	 * @generated
	 */
	public Adapter createMachineAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.MachineEmaxAttribute <em>Machine Emax Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.MachineEmaxAttribute
	 * @generated
	 */
	public Adapter createMachineEmaxAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.MachineEminAttribute <em>Machine Emin Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.MachineEminAttribute
	 * @generated
	 */
	public Adapter createMachineEminAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.MachineMantissaAttribute <em>Machine Mantissa Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.MachineMantissaAttribute
	 * @generated
	 */
	public Adapter createMachineMantissaAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.MachineOverflowsAttribute <em>Machine Overflows Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.MachineOverflowsAttribute
	 * @generated
	 */
	public Adapter createMachineOverflowsAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.MachineRadixAttribute <em>Machine Radix Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.MachineRadixAttribute
	 * @generated
	 */
	public Adapter createMachineRadixAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.MachineRoundingAttribute <em>Machine Rounding Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.MachineRoundingAttribute
	 * @generated
	 */
	public Adapter createMachineRoundingAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.MachineRoundsAttribute <em>Machine Rounds Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.MachineRoundsAttribute
	 * @generated
	 */
	public Adapter createMachineRoundsAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.MaxAlignmentForAllocationAttribute <em>Max Alignment For Allocation Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.MaxAlignmentForAllocationAttribute
	 * @generated
	 */
	public Adapter createMaxAlignmentForAllocationAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.MaxAttribute <em>Max Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.MaxAttribute
	 * @generated
	 */
	public Adapter createMaxAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.MaxSizeInStorageElementsAttribute <em>Max Size In Storage Elements Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.MaxSizeInStorageElementsAttribute
	 * @generated
	 */
	public Adapter createMaxSizeInStorageElementsAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.MinAttribute <em>Min Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.MinAttribute
	 * @generated
	 */
	public Adapter createMinAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.MinusOperator <em>Minus Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.MinusOperator
	 * @generated
	 */
	public Adapter createMinusOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ModAttribute <em>Mod Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ModAttribute
	 * @generated
	 */
	public Adapter createModAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ModelAttribute <em>Model Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ModelAttribute
	 * @generated
	 */
	public Adapter createModelAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ModelEminAttribute <em>Model Emin Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ModelEminAttribute
	 * @generated
	 */
	public Adapter createModelEminAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ModelEpsilonAttribute <em>Model Epsilon Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ModelEpsilonAttribute
	 * @generated
	 */
	public Adapter createModelEpsilonAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ModelMantissaAttribute <em>Model Mantissa Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ModelMantissaAttribute
	 * @generated
	 */
	public Adapter createModelMantissaAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ModelSmallAttribute <em>Model Small Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ModelSmallAttribute
	 * @generated
	 */
	public Adapter createModelSmallAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ModOperator <em>Mod Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ModOperator
	 * @generated
	 */
	public Adapter createModOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ModularTypeDefinition <em>Modular Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ModularTypeDefinition
	 * @generated
	 */
	public Adapter createModularTypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ModulusAttribute <em>Modulus Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ModulusAttribute
	 * @generated
	 */
	public Adapter createModulusAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.MultiplyOperator <em>Multiply Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.MultiplyOperator
	 * @generated
	 */
	public Adapter createMultiplyOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.NameClass <em>Name Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.NameClass
	 * @generated
	 */
	public Adapter createNameClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.NamedArrayAggregate <em>Named Array Aggregate</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.NamedArrayAggregate
	 * @generated
	 */
	public Adapter createNamedArrayAggregateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.NameList <em>Name List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.NameList
	 * @generated
	 */
	public Adapter createNameListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.NoReturnPragma <em>No Return Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.NoReturnPragma
	 * @generated
	 */
	public Adapter createNoReturnPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.NormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.NormalizeScalarsPragma
	 * @generated
	 */
	public Adapter createNormalizeScalarsPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.NotAnElement <em>Not An Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.NotAnElement
	 * @generated
	 */
	public Adapter createNotAnElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.NotEqualOperator <em>Not Equal Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.NotEqualOperator
	 * @generated
	 */
	public Adapter createNotEqualOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.NotInMembershipTest <em>Not In Membership Test</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.NotInMembershipTest
	 * @generated
	 */
	public Adapter createNotInMembershipTestAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.NotNullReturn <em>Not Null Return</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.NotNullReturn
	 * @generated
	 */
	public Adapter createNotNullReturnAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.NotOperator <em>Not Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.NotOperator
	 * @generated
	 */
	public Adapter createNotOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.NotOverriding <em>Not Overriding</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.NotOverriding
	 * @generated
	 */
	public Adapter createNotOverridingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.NullComponent <em>Null Component</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.NullComponent
	 * @generated
	 */
	public Adapter createNullComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.NullExclusion <em>Null Exclusion</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.NullExclusion
	 * @generated
	 */
	public Adapter createNullExclusionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.NullLiteral <em>Null Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.NullLiteral
	 * @generated
	 */
	public Adapter createNullLiteralAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.NullProcedureDeclaration <em>Null Procedure Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.NullProcedureDeclaration
	 * @generated
	 */
	public Adapter createNullProcedureDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.NullRecordDefinition <em>Null Record Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.NullRecordDefinition
	 * @generated
	 */
	public Adapter createNullRecordDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.NullStatement <em>Null Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.NullStatement
	 * @generated
	 */
	public Adapter createNullStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ObjectRenamingDeclaration <em>Object Renaming Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ObjectRenamingDeclaration
	 * @generated
	 */
	public Adapter createObjectRenamingDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.OptimizePragma <em>Optimize Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.OptimizePragma
	 * @generated
	 */
	public Adapter createOptimizePragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.OrdinaryFixedPointDefinition <em>Ordinary Fixed Point Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.OrdinaryFixedPointDefinition
	 * @generated
	 */
	public Adapter createOrdinaryFixedPointDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.OrdinaryInterface <em>Ordinary Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.OrdinaryInterface
	 * @generated
	 */
	public Adapter createOrdinaryInterfaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.OrdinaryTypeDeclaration <em>Ordinary Type Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.OrdinaryTypeDeclaration
	 * @generated
	 */
	public Adapter createOrdinaryTypeDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.OrElseShortCircuit <em>Or Else Short Circuit</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.OrElseShortCircuit
	 * @generated
	 */
	public Adapter createOrElseShortCircuitAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.OrOperator <em>Or Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.OrOperator
	 * @generated
	 */
	public Adapter createOrOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.OrPath <em>Or Path</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.OrPath
	 * @generated
	 */
	public Adapter createOrPathAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.OthersChoice <em>Others Choice</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.OthersChoice
	 * @generated
	 */
	public Adapter createOthersChoiceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.OutputAttribute <em>Output Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.OutputAttribute
	 * @generated
	 */
	public Adapter createOutputAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.OverlapsStorageAttribute <em>Overlaps Storage Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.OverlapsStorageAttribute
	 * @generated
	 */
	public Adapter createOverlapsStorageAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.Overriding <em>Overriding</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.Overriding
	 * @generated
	 */
	public Adapter createOverridingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PackageBodyDeclaration <em>Package Body Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PackageBodyDeclaration
	 * @generated
	 */
	public Adapter createPackageBodyDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PackageBodyStub <em>Package Body Stub</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PackageBodyStub
	 * @generated
	 */
	public Adapter createPackageBodyStubAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PackageDeclaration <em>Package Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PackageDeclaration
	 * @generated
	 */
	public Adapter createPackageDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PackageInstantiation <em>Package Instantiation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PackageInstantiation
	 * @generated
	 */
	public Adapter createPackageInstantiationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PackageRenamingDeclaration <em>Package Renaming Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PackageRenamingDeclaration
	 * @generated
	 */
	public Adapter createPackageRenamingDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PackPragma <em>Pack Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PackPragma
	 * @generated
	 */
	public Adapter createPackPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PagePragma <em>Page Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PagePragma
	 * @generated
	 */
	public Adapter createPagePragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ParameterAssociation <em>Parameter Association</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ParameterAssociation
	 * @generated
	 */
	public Adapter createParameterAssociationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ParameterSpecification <em>Parameter Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ParameterSpecification
	 * @generated
	 */
	public Adapter createParameterSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ParameterSpecificationList <em>Parameter Specification List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ParameterSpecificationList
	 * @generated
	 */
	public Adapter createParameterSpecificationListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ParenthesizedExpression <em>Parenthesized Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ParenthesizedExpression
	 * @generated
	 */
	public Adapter createParenthesizedExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PartitionElaborationPolicyPragma
	 * @generated
	 */
	public Adapter createPartitionElaborationPolicyPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PartitionIdAttribute <em>Partition Id Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PartitionIdAttribute
	 * @generated
	 */
	public Adapter createPartitionIdAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PathClass <em>Path Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PathClass
	 * @generated
	 */
	public Adapter createPathClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PathList <em>Path List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PathList
	 * @generated
	 */
	public Adapter createPathListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PlusOperator <em>Plus Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PlusOperator
	 * @generated
	 */
	public Adapter createPlusOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PoolSpecificAccessToVariable <em>Pool Specific Access To Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PoolSpecificAccessToVariable
	 * @generated
	 */
	public Adapter createPoolSpecificAccessToVariableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PosAttribute <em>Pos Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PosAttribute
	 * @generated
	 */
	public Adapter createPosAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PositionalArrayAggregate <em>Positional Array Aggregate</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PositionalArrayAggregate
	 * @generated
	 */
	public Adapter createPositionalArrayAggregateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PositionAttribute <em>Position Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PositionAttribute
	 * @generated
	 */
	public Adapter createPositionAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PragmaArgumentAssociation <em>Pragma Argument Association</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PragmaArgumentAssociation
	 * @generated
	 */
	public Adapter createPragmaArgumentAssociationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PragmaElementClass <em>Pragma Element Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PragmaElementClass
	 * @generated
	 */
	public Adapter createPragmaElementClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PredAttribute <em>Pred Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PredAttribute
	 * @generated
	 */
	public Adapter createPredAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PreelaborableInitializationPragma
	 * @generated
	 */
	public Adapter createPreelaborableInitializationPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PreelaboratePragma <em>Preelaborate Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PreelaboratePragma
	 * @generated
	 */
	public Adapter createPreelaboratePragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PriorityAttribute <em>Priority Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PriorityAttribute
	 * @generated
	 */
	public Adapter createPriorityAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PriorityPragma <em>Priority Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PriorityPragma
	 * @generated
	 */
	public Adapter createPriorityPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PrioritySpecificDispatchingPragma
	 * @generated
	 */
	public Adapter createPrioritySpecificDispatchingPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.Private <em>Private</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.Private
	 * @generated
	 */
	public Adapter createPrivateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PrivateExtensionDeclaration <em>Private Extension Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PrivateExtensionDeclaration
	 * @generated
	 */
	public Adapter createPrivateExtensionDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PrivateExtensionDefinition <em>Private Extension Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PrivateExtensionDefinition
	 * @generated
	 */
	public Adapter createPrivateExtensionDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PrivateTypeDeclaration <em>Private Type Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PrivateTypeDeclaration
	 * @generated
	 */
	public Adapter createPrivateTypeDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PrivateTypeDefinition <em>Private Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PrivateTypeDefinition
	 * @generated
	 */
	public Adapter createPrivateTypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ProcedureBodyDeclaration <em>Procedure Body Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ProcedureBodyDeclaration
	 * @generated
	 */
	public Adapter createProcedureBodyDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ProcedureBodyStub <em>Procedure Body Stub</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ProcedureBodyStub
	 * @generated
	 */
	public Adapter createProcedureBodyStubAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ProcedureCallStatement <em>Procedure Call Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ProcedureCallStatement
	 * @generated
	 */
	public Adapter createProcedureCallStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ProcedureDeclaration <em>Procedure Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ProcedureDeclaration
	 * @generated
	 */
	public Adapter createProcedureDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ProcedureInstantiation <em>Procedure Instantiation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ProcedureInstantiation
	 * @generated
	 */
	public Adapter createProcedureInstantiationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ProcedureRenamingDeclaration <em>Procedure Renaming Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ProcedureRenamingDeclaration
	 * @generated
	 */
	public Adapter createProcedureRenamingDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ProfilePragma <em>Profile Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ProfilePragma
	 * @generated
	 */
	public Adapter createProfilePragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ProtectedBodyDeclaration <em>Protected Body Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ProtectedBodyDeclaration
	 * @generated
	 */
	public Adapter createProtectedBodyDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ProtectedBodyStub <em>Protected Body Stub</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ProtectedBodyStub
	 * @generated
	 */
	public Adapter createProtectedBodyStubAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ProtectedDefinition <em>Protected Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ProtectedDefinition
	 * @generated
	 */
	public Adapter createProtectedDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ProtectedInterface <em>Protected Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ProtectedInterface
	 * @generated
	 */
	public Adapter createProtectedInterfaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ProtectedTypeDeclaration <em>Protected Type Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ProtectedTypeDeclaration
	 * @generated
	 */
	public Adapter createProtectedTypeDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.PurePragma <em>Pure Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.PurePragma
	 * @generated
	 */
	public Adapter createPurePragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.QualifiedExpression <em>Qualified Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.QualifiedExpression
	 * @generated
	 */
	public Adapter createQualifiedExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.QueuingPolicyPragma <em>Queuing Policy Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.QueuingPolicyPragma
	 * @generated
	 */
	public Adapter createQueuingPolicyPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RaiseExpression <em>Raise Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RaiseExpression
	 * @generated
	 */
	public Adapter createRaiseExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RaiseStatement <em>Raise Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RaiseStatement
	 * @generated
	 */
	public Adapter createRaiseStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RangeAttribute <em>Range Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RangeAttribute
	 * @generated
	 */
	public Adapter createRangeAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RangeAttributeReference <em>Range Attribute Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RangeAttributeReference
	 * @generated
	 */
	public Adapter createRangeAttributeReferenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RangeConstraintClass <em>Range Constraint Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RangeConstraintClass
	 * @generated
	 */
	public Adapter createRangeConstraintClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ReadAttribute <em>Read Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ReadAttribute
	 * @generated
	 */
	public Adapter createReadAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RealLiteral <em>Real Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RealLiteral
	 * @generated
	 */
	public Adapter createRealLiteralAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RealNumberDeclaration <em>Real Number Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RealNumberDeclaration
	 * @generated
	 */
	public Adapter createRealNumberDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RecordAggregate <em>Record Aggregate</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RecordAggregate
	 * @generated
	 */
	public Adapter createRecordAggregateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RecordComponentAssociation <em>Record Component Association</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RecordComponentAssociation
	 * @generated
	 */
	public Adapter createRecordComponentAssociationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RecordComponentClass <em>Record Component Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RecordComponentClass
	 * @generated
	 */
	public Adapter createRecordComponentClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RecordComponentList <em>Record Component List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RecordComponentList
	 * @generated
	 */
	public Adapter createRecordComponentListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RecordDefinition <em>Record Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RecordDefinition
	 * @generated
	 */
	public Adapter createRecordDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RecordRepresentationClause <em>Record Representation Clause</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RecordRepresentationClause
	 * @generated
	 */
	public Adapter createRecordRepresentationClauseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RecordTypeDefinition <em>Record Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RecordTypeDefinition
	 * @generated
	 */
	public Adapter createRecordTypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RelativeDeadlinePragma <em>Relative Deadline Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RelativeDeadlinePragma
	 * @generated
	 */
	public Adapter createRelativeDeadlinePragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RemainderAttribute <em>Remainder Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RemainderAttribute
	 * @generated
	 */
	public Adapter createRemainderAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RemOperator <em>Rem Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RemOperator
	 * @generated
	 */
	public Adapter createRemOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RemoteCallInterfacePragma
	 * @generated
	 */
	public Adapter createRemoteCallInterfacePragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RemoteTypesPragma <em>Remote Types Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RemoteTypesPragma
	 * @generated
	 */
	public Adapter createRemoteTypesPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RequeueStatement <em>Requeue Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RequeueStatement
	 * @generated
	 */
	public Adapter createRequeueStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RequeueStatementWithAbort <em>Requeue Statement With Abort</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RequeueStatementWithAbort
	 * @generated
	 */
	public Adapter createRequeueStatementWithAbortAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RestrictionsPragma <em>Restrictions Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RestrictionsPragma
	 * @generated
	 */
	public Adapter createRestrictionsPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ReturnConstantSpecification <em>Return Constant Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ReturnConstantSpecification
	 * @generated
	 */
	public Adapter createReturnConstantSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ReturnStatement <em>Return Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ReturnStatement
	 * @generated
	 */
	public Adapter createReturnStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ReturnVariableSpecification <em>Return Variable Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ReturnVariableSpecification
	 * @generated
	 */
	public Adapter createReturnVariableSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.Reverse <em>Reverse</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.Reverse
	 * @generated
	 */
	public Adapter createReverseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ReviewablePragma <em>Reviewable Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ReviewablePragma
	 * @generated
	 */
	public Adapter createReviewablePragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RootIntegerDefinition <em>Root Integer Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RootIntegerDefinition
	 * @generated
	 */
	public Adapter createRootIntegerDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RootRealDefinition <em>Root Real Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RootRealDefinition
	 * @generated
	 */
	public Adapter createRootRealDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RoundAttribute <em>Round Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RoundAttribute
	 * @generated
	 */
	public Adapter createRoundAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.RoundingAttribute <em>Rounding Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.RoundingAttribute
	 * @generated
	 */
	public Adapter createRoundingAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.SafeFirstAttribute <em>Safe First Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.SafeFirstAttribute
	 * @generated
	 */
	public Adapter createSafeFirstAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.SafeLastAttribute <em>Safe Last Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.SafeLastAttribute
	 * @generated
	 */
	public Adapter createSafeLastAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ScaleAttribute <em>Scale Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ScaleAttribute
	 * @generated
	 */
	public Adapter createScaleAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ScalingAttribute <em>Scaling Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ScalingAttribute
	 * @generated
	 */
	public Adapter createScalingAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.SelectedComponent <em>Selected Component</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.SelectedComponent
	 * @generated
	 */
	public Adapter createSelectedComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.SelectiveAcceptStatement <em>Selective Accept Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.SelectiveAcceptStatement
	 * @generated
	 */
	public Adapter createSelectiveAcceptStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.SelectPath <em>Select Path</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.SelectPath
	 * @generated
	 */
	public Adapter createSelectPathAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.SharedPassivePragma <em>Shared Passive Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.SharedPassivePragma
	 * @generated
	 */
	public Adapter createSharedPassivePragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.SignedIntegerTypeDefinition <em>Signed Integer Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.SignedIntegerTypeDefinition
	 * @generated
	 */
	public Adapter createSignedIntegerTypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.SignedZerosAttribute <em>Signed Zeros Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.SignedZerosAttribute
	 * @generated
	 */
	public Adapter createSignedZerosAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.SimpleExpressionRange <em>Simple Expression Range</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.SimpleExpressionRange
	 * @generated
	 */
	public Adapter createSimpleExpressionRangeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.SingleProtectedDeclaration <em>Single Protected Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.SingleProtectedDeclaration
	 * @generated
	 */
	public Adapter createSingleProtectedDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.SingleTaskDeclaration <em>Single Task Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.SingleTaskDeclaration
	 * @generated
	 */
	public Adapter createSingleTaskDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.SizeAttribute <em>Size Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.SizeAttribute
	 * @generated
	 */
	public Adapter createSizeAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.Slice <em>Slice</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.Slice
	 * @generated
	 */
	public Adapter createSliceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.SmallAttribute <em>Small Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.SmallAttribute
	 * @generated
	 */
	public Adapter createSmallAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.SourceLocation <em>Source Location</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.SourceLocation
	 * @generated
	 */
	public Adapter createSourceLocationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.StatementClass <em>Statement Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.StatementClass
	 * @generated
	 */
	public Adapter createStatementClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.StatementList <em>Statement List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.StatementList
	 * @generated
	 */
	public Adapter createStatementListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.StoragePoolAttribute <em>Storage Pool Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.StoragePoolAttribute
	 * @generated
	 */
	public Adapter createStoragePoolAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.StorageSizeAttribute <em>Storage Size Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.StorageSizeAttribute
	 * @generated
	 */
	public Adapter createStorageSizeAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.StorageSizePragma <em>Storage Size Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.StorageSizePragma
	 * @generated
	 */
	public Adapter createStorageSizePragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.StreamSizeAttribute <em>Stream Size Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.StreamSizeAttribute
	 * @generated
	 */
	public Adapter createStreamSizeAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.StringLiteral <em>String Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.StringLiteral
	 * @generated
	 */
	public Adapter createStringLiteralAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.SubtypeDeclaration <em>Subtype Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.SubtypeDeclaration
	 * @generated
	 */
	public Adapter createSubtypeDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.SubtypeIndication <em>Subtype Indication</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.SubtypeIndication
	 * @generated
	 */
	public Adapter createSubtypeIndicationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.SuccAttribute <em>Succ Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.SuccAttribute
	 * @generated
	 */
	public Adapter createSuccAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.SuppressPragma <em>Suppress Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.SuppressPragma
	 * @generated
	 */
	public Adapter createSuppressPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.Synchronized <em>Synchronized</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.Synchronized
	 * @generated
	 */
	public Adapter createSynchronizedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.SynchronizedInterface <em>Synchronized Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.SynchronizedInterface
	 * @generated
	 */
	public Adapter createSynchronizedInterfaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.TagAttribute <em>Tag Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.TagAttribute
	 * @generated
	 */
	public Adapter createTagAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.Tagged <em>Tagged</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.Tagged
	 * @generated
	 */
	public Adapter createTaggedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.TaggedIncompleteTypeDeclaration <em>Tagged Incomplete Type Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.TaggedIncompleteTypeDeclaration
	 * @generated
	 */
	public Adapter createTaggedIncompleteTypeDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.TaggedPrivateTypeDefinition <em>Tagged Private Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.TaggedPrivateTypeDefinition
	 * @generated
	 */
	public Adapter createTaggedPrivateTypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.TaggedRecordTypeDefinition <em>Tagged Record Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.TaggedRecordTypeDefinition
	 * @generated
	 */
	public Adapter createTaggedRecordTypeDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.TaskBodyDeclaration <em>Task Body Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.TaskBodyDeclaration
	 * @generated
	 */
	public Adapter createTaskBodyDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.TaskBodyStub <em>Task Body Stub</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.TaskBodyStub
	 * @generated
	 */
	public Adapter createTaskBodyStubAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.TaskDefinition <em>Task Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.TaskDefinition
	 * @generated
	 */
	public Adapter createTaskDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.TaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.TaskDispatchingPolicyPragma
	 * @generated
	 */
	public Adapter createTaskDispatchingPolicyPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.TaskInterface <em>Task Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.TaskInterface
	 * @generated
	 */
	public Adapter createTaskInterfaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.TaskTypeDeclaration <em>Task Type Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.TaskTypeDeclaration
	 * @generated
	 */
	public Adapter createTaskTypeDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.TerminateAlternativeStatement <em>Terminate Alternative Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.TerminateAlternativeStatement
	 * @generated
	 */
	public Adapter createTerminateAlternativeStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.TerminatedAttribute <em>Terminated Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.TerminatedAttribute
	 * @generated
	 */
	public Adapter createTerminatedAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ThenAbortPath <em>Then Abort Path</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ThenAbortPath
	 * @generated
	 */
	public Adapter createThenAbortPathAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.TimedEntryCallStatement <em>Timed Entry Call Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.TimedEntryCallStatement
	 * @generated
	 */
	public Adapter createTimedEntryCallStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.TruncationAttribute <em>Truncation Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.TruncationAttribute
	 * @generated
	 */
	public Adapter createTruncationAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.TypeConversion <em>Type Conversion</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.TypeConversion
	 * @generated
	 */
	public Adapter createTypeConversionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.UnaryMinusOperator <em>Unary Minus Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.UnaryMinusOperator
	 * @generated
	 */
	public Adapter createUnaryMinusOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.UnaryPlusOperator <em>Unary Plus Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.UnaryPlusOperator
	 * @generated
	 */
	public Adapter createUnaryPlusOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.UnbiasedRoundingAttribute <em>Unbiased Rounding Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.UnbiasedRoundingAttribute
	 * @generated
	 */
	public Adapter createUnbiasedRoundingAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.UncheckedAccessAttribute <em>Unchecked Access Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.UncheckedAccessAttribute
	 * @generated
	 */
	public Adapter createUncheckedAccessAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.UncheckedUnionPragma <em>Unchecked Union Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.UncheckedUnionPragma
	 * @generated
	 */
	public Adapter createUncheckedUnionPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.UnconstrainedArrayDefinition <em>Unconstrained Array Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.UnconstrainedArrayDefinition
	 * @generated
	 */
	public Adapter createUnconstrainedArrayDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.UniversalFixedDefinition <em>Universal Fixed Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.UniversalFixedDefinition
	 * @generated
	 */
	public Adapter createUniversalFixedDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.UniversalIntegerDefinition <em>Universal Integer Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.UniversalIntegerDefinition
	 * @generated
	 */
	public Adapter createUniversalIntegerDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.UniversalRealDefinition <em>Universal Real Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.UniversalRealDefinition
	 * @generated
	 */
	public Adapter createUniversalRealDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.UnknownAttribute <em>Unknown Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.UnknownAttribute
	 * @generated
	 */
	public Adapter createUnknownAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.UnknownDiscriminantPart <em>Unknown Discriminant Part</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.UnknownDiscriminantPart
	 * @generated
	 */
	public Adapter createUnknownDiscriminantPartAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.UnknownPragma <em>Unknown Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.UnknownPragma
	 * @generated
	 */
	public Adapter createUnknownPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.UnsuppressPragma <em>Unsuppress Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.UnsuppressPragma
	 * @generated
	 */
	public Adapter createUnsuppressPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.UseAllTypeClause <em>Use All Type Clause</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.UseAllTypeClause
	 * @generated
	 */
	public Adapter createUseAllTypeClauseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.UsePackageClause <em>Use Package Clause</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.UsePackageClause
	 * @generated
	 */
	public Adapter createUsePackageClauseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.UseTypeClause <em>Use Type Clause</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.UseTypeClause
	 * @generated
	 */
	public Adapter createUseTypeClauseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ValAttribute <em>Val Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ValAttribute
	 * @generated
	 */
	public Adapter createValAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ValidAttribute <em>Valid Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ValidAttribute
	 * @generated
	 */
	public Adapter createValidAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.ValueAttribute <em>Value Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.ValueAttribute
	 * @generated
	 */
	public Adapter createValueAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.VariableDeclaration <em>Variable Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.VariableDeclaration
	 * @generated
	 */
	public Adapter createVariableDeclarationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.Variant <em>Variant</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.Variant
	 * @generated
	 */
	public Adapter createVariantAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.VariantList <em>Variant List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.VariantList
	 * @generated
	 */
	public Adapter createVariantListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.VariantPart <em>Variant Part</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.VariantPart
	 * @generated
	 */
	public Adapter createVariantPartAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.VersionAttribute <em>Version Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.VersionAttribute
	 * @generated
	 */
	public Adapter createVersionAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.VolatileComponentsPragma <em>Volatile Components Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.VolatileComponentsPragma
	 * @generated
	 */
	public Adapter createVolatileComponentsPragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.VolatilePragma <em>Volatile Pragma</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.VolatilePragma
	 * @generated
	 */
	public Adapter createVolatilePragmaAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.WhileLoopStatement <em>While Loop Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.WhileLoopStatement
	 * @generated
	 */
	public Adapter createWhileLoopStatementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.WideImageAttribute <em>Wide Image Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.WideImageAttribute
	 * @generated
	 */
	public Adapter createWideImageAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.WideValueAttribute <em>Wide Value Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.WideValueAttribute
	 * @generated
	 */
	public Adapter createWideValueAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.WideWideImageAttribute <em>Wide Wide Image Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.WideWideImageAttribute
	 * @generated
	 */
	public Adapter createWideWideImageAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.WideWideValueAttribute <em>Wide Wide Value Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.WideWideValueAttribute
	 * @generated
	 */
	public Adapter createWideWideValueAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.WideWideWidthAttribute <em>Wide Wide Width Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.WideWideWidthAttribute
	 * @generated
	 */
	public Adapter createWideWideWidthAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.WideWidthAttribute <em>Wide Width Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.WideWidthAttribute
	 * @generated
	 */
	public Adapter createWideWidthAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.WidthAttribute <em>Width Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.WidthAttribute
	 * @generated
	 */
	public Adapter createWidthAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.WithClause <em>With Clause</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.WithClause
	 * @generated
	 */
	public Adapter createWithClauseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.WriteAttribute <em>Write Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.WriteAttribute
	 * @generated
	 */
	public Adapter createWriteAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link Ada.XorOperator <em>Xor Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see Ada.XorOperator
	 * @generated
	 */
	public Adapter createXorOperatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //AdaAdapterFactory
