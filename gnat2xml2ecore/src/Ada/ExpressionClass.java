/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.ExpressionClass#getNotAnElement <em>Not An Element</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getDiscreteRangeAttributeReference <em>Discrete Range Attribute Reference</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getDiscreteSimpleExpressionRange <em>Discrete Simple Expression Range</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getOthersChoice <em>Others Choice</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getBoxExpression <em>Box Expression</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getIntegerLiteral <em>Integer Literal</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getRealLiteral <em>Real Literal</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getStringLiteral <em>String Literal</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getAndOperator <em>And Operator</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getOrOperator <em>Or Operator</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getXorOperator <em>Xor Operator</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getEqualOperator <em>Equal Operator</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getNotEqualOperator <em>Not Equal Operator</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getLessThanOperator <em>Less Than Operator</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getLessThanOrEqualOperator <em>Less Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getGreaterThanOperator <em>Greater Than Operator</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getGreaterThanOrEqualOperator <em>Greater Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getPlusOperator <em>Plus Operator</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getMinusOperator <em>Minus Operator</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getConcatenateOperator <em>Concatenate Operator</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getUnaryPlusOperator <em>Unary Plus Operator</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getUnaryMinusOperator <em>Unary Minus Operator</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getMultiplyOperator <em>Multiply Operator</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getDivideOperator <em>Divide Operator</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getModOperator <em>Mod Operator</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getRemOperator <em>Rem Operator</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getExponentiateOperator <em>Exponentiate Operator</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getAbsOperator <em>Abs Operator</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getNotOperator <em>Not Operator</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getCharacterLiteral <em>Character Literal</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getEnumerationLiteral <em>Enumeration Literal</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getExplicitDereference <em>Explicit Dereference</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getFunctionCall <em>Function Call</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getIndexedComponent <em>Indexed Component</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getSlice <em>Slice</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getSelectedComponent <em>Selected Component</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getAccessAttribute <em>Access Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getAddressAttribute <em>Address Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getAdjacentAttribute <em>Adjacent Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getAftAttribute <em>Aft Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getAlignmentAttribute <em>Alignment Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getBaseAttribute <em>Base Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getBitOrderAttribute <em>Bit Order Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getBodyVersionAttribute <em>Body Version Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getCallableAttribute <em>Callable Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getCallerAttribute <em>Caller Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getCeilingAttribute <em>Ceiling Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getClassAttribute <em>Class Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getComponentSizeAttribute <em>Component Size Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getComposeAttribute <em>Compose Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getConstrainedAttribute <em>Constrained Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getCopySignAttribute <em>Copy Sign Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getCountAttribute <em>Count Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getDefiniteAttribute <em>Definite Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getDeltaAttribute <em>Delta Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getDenormAttribute <em>Denorm Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getDigitsAttribute <em>Digits Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getExponentAttribute <em>Exponent Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getExternalTagAttribute <em>External Tag Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getFirstAttribute <em>First Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getFirstBitAttribute <em>First Bit Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getFloorAttribute <em>Floor Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getForeAttribute <em>Fore Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getFractionAttribute <em>Fraction Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getIdentityAttribute <em>Identity Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getImageAttribute <em>Image Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getInputAttribute <em>Input Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getLastAttribute <em>Last Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getLastBitAttribute <em>Last Bit Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getLeadingPartAttribute <em>Leading Part Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getLengthAttribute <em>Length Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getMachineAttribute <em>Machine Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getMachineEmaxAttribute <em>Machine Emax Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getMachineEminAttribute <em>Machine Emin Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getMachineMantissaAttribute <em>Machine Mantissa Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getMachineOverflowsAttribute <em>Machine Overflows Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getMachineRadixAttribute <em>Machine Radix Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getMachineRoundsAttribute <em>Machine Rounds Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getMaxAttribute <em>Max Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getMaxSizeInStorageElementsAttribute <em>Max Size In Storage Elements Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getMinAttribute <em>Min Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getModelAttribute <em>Model Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getModelEminAttribute <em>Model Emin Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getModelEpsilonAttribute <em>Model Epsilon Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getModelMantissaAttribute <em>Model Mantissa Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getModelSmallAttribute <em>Model Small Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getModulusAttribute <em>Modulus Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getOutputAttribute <em>Output Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getPartitionIdAttribute <em>Partition Id Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getPosAttribute <em>Pos Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getPositionAttribute <em>Position Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getPredAttribute <em>Pred Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getRangeAttribute <em>Range Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getReadAttribute <em>Read Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getRemainderAttribute <em>Remainder Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getRoundAttribute <em>Round Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getRoundingAttribute <em>Rounding Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getSafeFirstAttribute <em>Safe First Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getSafeLastAttribute <em>Safe Last Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getScaleAttribute <em>Scale Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getScalingAttribute <em>Scaling Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getSignedZerosAttribute <em>Signed Zeros Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getSizeAttribute <em>Size Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getSmallAttribute <em>Small Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getStoragePoolAttribute <em>Storage Pool Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getStorageSizeAttribute <em>Storage Size Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getSuccAttribute <em>Succ Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getTagAttribute <em>Tag Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getTerminatedAttribute <em>Terminated Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getTruncationAttribute <em>Truncation Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getUnbiasedRoundingAttribute <em>Unbiased Rounding Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getUncheckedAccessAttribute <em>Unchecked Access Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getValAttribute <em>Val Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getValidAttribute <em>Valid Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getValueAttribute <em>Value Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getVersionAttribute <em>Version Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getWideImageAttribute <em>Wide Image Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getWideValueAttribute <em>Wide Value Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getWideWidthAttribute <em>Wide Width Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getWidthAttribute <em>Width Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getWriteAttribute <em>Write Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getMachineRoundingAttribute <em>Machine Rounding Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getModAttribute <em>Mod Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getPriorityAttribute <em>Priority Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getStreamSizeAttribute <em>Stream Size Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getWideWideImageAttribute <em>Wide Wide Image Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getWideWideValueAttribute <em>Wide Wide Value Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getWideWideWidthAttribute <em>Wide Wide Width Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getMaxAlignmentForAllocationAttribute <em>Max Alignment For Allocation Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getOverlapsStorageAttribute <em>Overlaps Storage Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getImplementationDefinedAttribute <em>Implementation Defined Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getUnknownAttribute <em>Unknown Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getRecordAggregate <em>Record Aggregate</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getExtensionAggregate <em>Extension Aggregate</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getPositionalArrayAggregate <em>Positional Array Aggregate</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getNamedArrayAggregate <em>Named Array Aggregate</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getAndThenShortCircuit <em>And Then Short Circuit</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getOrElseShortCircuit <em>Or Else Short Circuit</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getInMembershipTest <em>In Membership Test</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getNotInMembershipTest <em>Not In Membership Test</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getNullLiteral <em>Null Literal</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getParenthesizedExpression <em>Parenthesized Expression</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getRaiseExpression <em>Raise Expression</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getTypeConversion <em>Type Conversion</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getQualifiedExpression <em>Qualified Expression</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getAllocationFromSubtype <em>Allocation From Subtype</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getAllocationFromQualifiedExpression <em>Allocation From Qualified Expression</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getCaseExpression <em>Case Expression</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getIfExpression <em>If Expression</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getForAllQuantifiedExpression <em>For All Quantified Expression</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getForSomeQuantifiedExpression <em>For Some Quantified Expression</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getComment <em>Comment</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getAsynchronousPragma <em>Asynchronous Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getAtomicPragma <em>Atomic Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getAttachHandlerPragma <em>Attach Handler Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getControlledPragma <em>Controlled Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getConventionPragma <em>Convention Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getDiscardNamesPragma <em>Discard Names Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getElaboratePragma <em>Elaborate Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getElaborateAllPragma <em>Elaborate All Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getExportPragma <em>Export Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getImportPragma <em>Import Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getInlinePragma <em>Inline Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getInspectionPointPragma <em>Inspection Point Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getLinkerOptionsPragma <em>Linker Options Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getListPragma <em>List Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getLockingPolicyPragma <em>Locking Policy Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getOptimizePragma <em>Optimize Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getPackPragma <em>Pack Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getPagePragma <em>Page Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getPreelaboratePragma <em>Preelaborate Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getPriorityPragma <em>Priority Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getPurePragma <em>Pure Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getRemoteTypesPragma <em>Remote Types Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getRestrictionsPragma <em>Restrictions Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getReviewablePragma <em>Reviewable Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getSharedPassivePragma <em>Shared Passive Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getStorageSizePragma <em>Storage Size Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getSuppressPragma <em>Suppress Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getVolatilePragma <em>Volatile Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getAssertPragma <em>Assert Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getNoReturnPragma <em>No Return Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getProfilePragma <em>Profile Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getUnsuppressPragma <em>Unsuppress Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getCpuPragma <em>Cpu Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getIndependentPragma <em>Independent Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getIndependentComponentsPragma <em>Independent Components Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionClass#getUnknownPragma <em>Unknown Pragma</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getExpressionClass()
 * @model extendedMetaData="name='Expression_Class' kind='elementOnly'"
 * @generated
 */
public interface ExpressionClass extends EObject {
	/**
	 * Returns the value of the '<em><b>Not An Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not An Element</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not An Element</em>' containment reference.
	 * @see #setNotAnElement(NotAnElement)
	 * @see Ada.AdaPackage#getExpressionClass_NotAnElement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not_an_element' namespace='##targetNamespace'"
	 * @generated
	 */
	NotAnElement getNotAnElement();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getNotAnElement <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not An Element</em>' containment reference.
	 * @see #getNotAnElement()
	 * @generated
	 */
	void setNotAnElement(NotAnElement value);

	/**
	 * Returns the value of the '<em><b>Discrete Range Attribute Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Range Attribute Reference</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Range Attribute Reference</em>' containment reference.
	 * @see #setDiscreteRangeAttributeReference(DiscreteRangeAttributeReference)
	 * @see Ada.AdaPackage#getExpressionClass_DiscreteRangeAttributeReference()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='discrete_range_attribute_reference' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscreteRangeAttributeReference getDiscreteRangeAttributeReference();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getDiscreteRangeAttributeReference <em>Discrete Range Attribute Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discrete Range Attribute Reference</em>' containment reference.
	 * @see #getDiscreteRangeAttributeReference()
	 * @generated
	 */
	void setDiscreteRangeAttributeReference(DiscreteRangeAttributeReference value);

	/**
	 * Returns the value of the '<em><b>Discrete Simple Expression Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Simple Expression Range</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Simple Expression Range</em>' containment reference.
	 * @see #setDiscreteSimpleExpressionRange(DiscreteSimpleExpressionRange)
	 * @see Ada.AdaPackage#getExpressionClass_DiscreteSimpleExpressionRange()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='discrete_simple_expression_range' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscreteSimpleExpressionRange getDiscreteSimpleExpressionRange();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getDiscreteSimpleExpressionRange <em>Discrete Simple Expression Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discrete Simple Expression Range</em>' containment reference.
	 * @see #getDiscreteSimpleExpressionRange()
	 * @generated
	 */
	void setDiscreteSimpleExpressionRange(DiscreteSimpleExpressionRange value);

	/**
	 * Returns the value of the '<em><b>Others Choice</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Others Choice</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Others Choice</em>' containment reference.
	 * @see #setOthersChoice(OthersChoice)
	 * @see Ada.AdaPackage#getExpressionClass_OthersChoice()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='others_choice' namespace='##targetNamespace'"
	 * @generated
	 */
	OthersChoice getOthersChoice();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getOthersChoice <em>Others Choice</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Others Choice</em>' containment reference.
	 * @see #getOthersChoice()
	 * @generated
	 */
	void setOthersChoice(OthersChoice value);

	/**
	 * Returns the value of the '<em><b>Box Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Box Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Box Expression</em>' containment reference.
	 * @see #setBoxExpression(BoxExpression)
	 * @see Ada.AdaPackage#getExpressionClass_BoxExpression()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='box_expression' namespace='##targetNamespace'"
	 * @generated
	 */
	BoxExpression getBoxExpression();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getBoxExpression <em>Box Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Box Expression</em>' containment reference.
	 * @see #getBoxExpression()
	 * @generated
	 */
	void setBoxExpression(BoxExpression value);

	/**
	 * Returns the value of the '<em><b>Integer Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integer Literal</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integer Literal</em>' containment reference.
	 * @see #setIntegerLiteral(IntegerLiteral)
	 * @see Ada.AdaPackage#getExpressionClass_IntegerLiteral()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='integer_literal' namespace='##targetNamespace'"
	 * @generated
	 */
	IntegerLiteral getIntegerLiteral();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getIntegerLiteral <em>Integer Literal</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Integer Literal</em>' containment reference.
	 * @see #getIntegerLiteral()
	 * @generated
	 */
	void setIntegerLiteral(IntegerLiteral value);

	/**
	 * Returns the value of the '<em><b>Real Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Real Literal</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Real Literal</em>' containment reference.
	 * @see #setRealLiteral(RealLiteral)
	 * @see Ada.AdaPackage#getExpressionClass_RealLiteral()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='real_literal' namespace='##targetNamespace'"
	 * @generated
	 */
	RealLiteral getRealLiteral();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getRealLiteral <em>Real Literal</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Real Literal</em>' containment reference.
	 * @see #getRealLiteral()
	 * @generated
	 */
	void setRealLiteral(RealLiteral value);

	/**
	 * Returns the value of the '<em><b>String Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>String Literal</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String Literal</em>' containment reference.
	 * @see #setStringLiteral(StringLiteral)
	 * @see Ada.AdaPackage#getExpressionClass_StringLiteral()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='string_literal' namespace='##targetNamespace'"
	 * @generated
	 */
	StringLiteral getStringLiteral();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getStringLiteral <em>String Literal</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>String Literal</em>' containment reference.
	 * @see #getStringLiteral()
	 * @generated
	 */
	void setStringLiteral(StringLiteral value);

	/**
	 * Returns the value of the '<em><b>Identifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Identifier</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identifier</em>' containment reference.
	 * @see #setIdentifier(Identifier)
	 * @see Ada.AdaPackage#getExpressionClass_Identifier()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='identifier' namespace='##targetNamespace'"
	 * @generated
	 */
	Identifier getIdentifier();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getIdentifier <em>Identifier</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Identifier</em>' containment reference.
	 * @see #getIdentifier()
	 * @generated
	 */
	void setIdentifier(Identifier value);

	/**
	 * Returns the value of the '<em><b>And Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>And Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>And Operator</em>' containment reference.
	 * @see #setAndOperator(AndOperator)
	 * @see Ada.AdaPackage#getExpressionClass_AndOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='and_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	AndOperator getAndOperator();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getAndOperator <em>And Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>And Operator</em>' containment reference.
	 * @see #getAndOperator()
	 * @generated
	 */
	void setAndOperator(AndOperator value);

	/**
	 * Returns the value of the '<em><b>Or Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Or Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Or Operator</em>' containment reference.
	 * @see #setOrOperator(OrOperator)
	 * @see Ada.AdaPackage#getExpressionClass_OrOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='or_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	OrOperator getOrOperator();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getOrOperator <em>Or Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Or Operator</em>' containment reference.
	 * @see #getOrOperator()
	 * @generated
	 */
	void setOrOperator(OrOperator value);

	/**
	 * Returns the value of the '<em><b>Xor Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Xor Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Xor Operator</em>' containment reference.
	 * @see #setXorOperator(XorOperator)
	 * @see Ada.AdaPackage#getExpressionClass_XorOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='xor_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	XorOperator getXorOperator();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getXorOperator <em>Xor Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Xor Operator</em>' containment reference.
	 * @see #getXorOperator()
	 * @generated
	 */
	void setXorOperator(XorOperator value);

	/**
	 * Returns the value of the '<em><b>Equal Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equal Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equal Operator</em>' containment reference.
	 * @see #setEqualOperator(EqualOperator)
	 * @see Ada.AdaPackage#getExpressionClass_EqualOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='equal_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	EqualOperator getEqualOperator();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getEqualOperator <em>Equal Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Equal Operator</em>' containment reference.
	 * @see #getEqualOperator()
	 * @generated
	 */
	void setEqualOperator(EqualOperator value);

	/**
	 * Returns the value of the '<em><b>Not Equal Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not Equal Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not Equal Operator</em>' containment reference.
	 * @see #setNotEqualOperator(NotEqualOperator)
	 * @see Ada.AdaPackage#getExpressionClass_NotEqualOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not_equal_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	NotEqualOperator getNotEqualOperator();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getNotEqualOperator <em>Not Equal Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not Equal Operator</em>' containment reference.
	 * @see #getNotEqualOperator()
	 * @generated
	 */
	void setNotEqualOperator(NotEqualOperator value);

	/**
	 * Returns the value of the '<em><b>Less Than Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Less Than Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Less Than Operator</em>' containment reference.
	 * @see #setLessThanOperator(LessThanOperator)
	 * @see Ada.AdaPackage#getExpressionClass_LessThanOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='less_than_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	LessThanOperator getLessThanOperator();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getLessThanOperator <em>Less Than Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Less Than Operator</em>' containment reference.
	 * @see #getLessThanOperator()
	 * @generated
	 */
	void setLessThanOperator(LessThanOperator value);

	/**
	 * Returns the value of the '<em><b>Less Than Or Equal Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Less Than Or Equal Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Less Than Or Equal Operator</em>' containment reference.
	 * @see #setLessThanOrEqualOperator(LessThanOrEqualOperator)
	 * @see Ada.AdaPackage#getExpressionClass_LessThanOrEqualOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='less_than_or_equal_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	LessThanOrEqualOperator getLessThanOrEqualOperator();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getLessThanOrEqualOperator <em>Less Than Or Equal Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Less Than Or Equal Operator</em>' containment reference.
	 * @see #getLessThanOrEqualOperator()
	 * @generated
	 */
	void setLessThanOrEqualOperator(LessThanOrEqualOperator value);

	/**
	 * Returns the value of the '<em><b>Greater Than Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Greater Than Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Greater Than Operator</em>' containment reference.
	 * @see #setGreaterThanOperator(GreaterThanOperator)
	 * @see Ada.AdaPackage#getExpressionClass_GreaterThanOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='greater_than_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	GreaterThanOperator getGreaterThanOperator();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getGreaterThanOperator <em>Greater Than Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Greater Than Operator</em>' containment reference.
	 * @see #getGreaterThanOperator()
	 * @generated
	 */
	void setGreaterThanOperator(GreaterThanOperator value);

	/**
	 * Returns the value of the '<em><b>Greater Than Or Equal Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Greater Than Or Equal Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Greater Than Or Equal Operator</em>' containment reference.
	 * @see #setGreaterThanOrEqualOperator(GreaterThanOrEqualOperator)
	 * @see Ada.AdaPackage#getExpressionClass_GreaterThanOrEqualOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='greater_than_or_equal_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	GreaterThanOrEqualOperator getGreaterThanOrEqualOperator();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getGreaterThanOrEqualOperator <em>Greater Than Or Equal Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Greater Than Or Equal Operator</em>' containment reference.
	 * @see #getGreaterThanOrEqualOperator()
	 * @generated
	 */
	void setGreaterThanOrEqualOperator(GreaterThanOrEqualOperator value);

	/**
	 * Returns the value of the '<em><b>Plus Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Plus Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Plus Operator</em>' containment reference.
	 * @see #setPlusOperator(PlusOperator)
	 * @see Ada.AdaPackage#getExpressionClass_PlusOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='plus_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	PlusOperator getPlusOperator();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getPlusOperator <em>Plus Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Plus Operator</em>' containment reference.
	 * @see #getPlusOperator()
	 * @generated
	 */
	void setPlusOperator(PlusOperator value);

	/**
	 * Returns the value of the '<em><b>Minus Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Minus Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Minus Operator</em>' containment reference.
	 * @see #setMinusOperator(MinusOperator)
	 * @see Ada.AdaPackage#getExpressionClass_MinusOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='minus_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	MinusOperator getMinusOperator();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getMinusOperator <em>Minus Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Minus Operator</em>' containment reference.
	 * @see #getMinusOperator()
	 * @generated
	 */
	void setMinusOperator(MinusOperator value);

	/**
	 * Returns the value of the '<em><b>Concatenate Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Concatenate Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Concatenate Operator</em>' containment reference.
	 * @see #setConcatenateOperator(ConcatenateOperator)
	 * @see Ada.AdaPackage#getExpressionClass_ConcatenateOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='concatenate_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	ConcatenateOperator getConcatenateOperator();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getConcatenateOperator <em>Concatenate Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Concatenate Operator</em>' containment reference.
	 * @see #getConcatenateOperator()
	 * @generated
	 */
	void setConcatenateOperator(ConcatenateOperator value);

	/**
	 * Returns the value of the '<em><b>Unary Plus Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unary Plus Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unary Plus Operator</em>' containment reference.
	 * @see #setUnaryPlusOperator(UnaryPlusOperator)
	 * @see Ada.AdaPackage#getExpressionClass_UnaryPlusOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unary_plus_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	UnaryPlusOperator getUnaryPlusOperator();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getUnaryPlusOperator <em>Unary Plus Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unary Plus Operator</em>' containment reference.
	 * @see #getUnaryPlusOperator()
	 * @generated
	 */
	void setUnaryPlusOperator(UnaryPlusOperator value);

	/**
	 * Returns the value of the '<em><b>Unary Minus Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unary Minus Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unary Minus Operator</em>' containment reference.
	 * @see #setUnaryMinusOperator(UnaryMinusOperator)
	 * @see Ada.AdaPackage#getExpressionClass_UnaryMinusOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unary_minus_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	UnaryMinusOperator getUnaryMinusOperator();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getUnaryMinusOperator <em>Unary Minus Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unary Minus Operator</em>' containment reference.
	 * @see #getUnaryMinusOperator()
	 * @generated
	 */
	void setUnaryMinusOperator(UnaryMinusOperator value);

	/**
	 * Returns the value of the '<em><b>Multiply Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Multiply Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multiply Operator</em>' containment reference.
	 * @see #setMultiplyOperator(MultiplyOperator)
	 * @see Ada.AdaPackage#getExpressionClass_MultiplyOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='multiply_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	MultiplyOperator getMultiplyOperator();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getMultiplyOperator <em>Multiply Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Multiply Operator</em>' containment reference.
	 * @see #getMultiplyOperator()
	 * @generated
	 */
	void setMultiplyOperator(MultiplyOperator value);

	/**
	 * Returns the value of the '<em><b>Divide Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Divide Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Divide Operator</em>' containment reference.
	 * @see #setDivideOperator(DivideOperator)
	 * @see Ada.AdaPackage#getExpressionClass_DivideOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='divide_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DivideOperator getDivideOperator();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getDivideOperator <em>Divide Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Divide Operator</em>' containment reference.
	 * @see #getDivideOperator()
	 * @generated
	 */
	void setDivideOperator(DivideOperator value);

	/**
	 * Returns the value of the '<em><b>Mod Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mod Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mod Operator</em>' containment reference.
	 * @see #setModOperator(ModOperator)
	 * @see Ada.AdaPackage#getExpressionClass_ModOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='mod_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	ModOperator getModOperator();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getModOperator <em>Mod Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mod Operator</em>' containment reference.
	 * @see #getModOperator()
	 * @generated
	 */
	void setModOperator(ModOperator value);

	/**
	 * Returns the value of the '<em><b>Rem Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rem Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rem Operator</em>' containment reference.
	 * @see #setRemOperator(RemOperator)
	 * @see Ada.AdaPackage#getExpressionClass_RemOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='rem_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	RemOperator getRemOperator();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getRemOperator <em>Rem Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rem Operator</em>' containment reference.
	 * @see #getRemOperator()
	 * @generated
	 */
	void setRemOperator(RemOperator value);

	/**
	 * Returns the value of the '<em><b>Exponentiate Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exponentiate Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exponentiate Operator</em>' containment reference.
	 * @see #setExponentiateOperator(ExponentiateOperator)
	 * @see Ada.AdaPackage#getExpressionClass_ExponentiateOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='exponentiate_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	ExponentiateOperator getExponentiateOperator();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getExponentiateOperator <em>Exponentiate Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exponentiate Operator</em>' containment reference.
	 * @see #getExponentiateOperator()
	 * @generated
	 */
	void setExponentiateOperator(ExponentiateOperator value);

	/**
	 * Returns the value of the '<em><b>Abs Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abs Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abs Operator</em>' containment reference.
	 * @see #setAbsOperator(AbsOperator)
	 * @see Ada.AdaPackage#getExpressionClass_AbsOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='abs_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	AbsOperator getAbsOperator();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getAbsOperator <em>Abs Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abs Operator</em>' containment reference.
	 * @see #getAbsOperator()
	 * @generated
	 */
	void setAbsOperator(AbsOperator value);

	/**
	 * Returns the value of the '<em><b>Not Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not Operator</em>' containment reference.
	 * @see #setNotOperator(NotOperator)
	 * @see Ada.AdaPackage#getExpressionClass_NotOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	NotOperator getNotOperator();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getNotOperator <em>Not Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not Operator</em>' containment reference.
	 * @see #getNotOperator()
	 * @generated
	 */
	void setNotOperator(NotOperator value);

	/**
	 * Returns the value of the '<em><b>Character Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Character Literal</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Character Literal</em>' containment reference.
	 * @see #setCharacterLiteral(CharacterLiteral)
	 * @see Ada.AdaPackage#getExpressionClass_CharacterLiteral()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='character_literal' namespace='##targetNamespace'"
	 * @generated
	 */
	CharacterLiteral getCharacterLiteral();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getCharacterLiteral <em>Character Literal</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Character Literal</em>' containment reference.
	 * @see #getCharacterLiteral()
	 * @generated
	 */
	void setCharacterLiteral(CharacterLiteral value);

	/**
	 * Returns the value of the '<em><b>Enumeration Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumeration Literal</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumeration Literal</em>' containment reference.
	 * @see #setEnumerationLiteral(EnumerationLiteral)
	 * @see Ada.AdaPackage#getExpressionClass_EnumerationLiteral()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='enumeration_literal' namespace='##targetNamespace'"
	 * @generated
	 */
	EnumerationLiteral getEnumerationLiteral();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getEnumerationLiteral <em>Enumeration Literal</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enumeration Literal</em>' containment reference.
	 * @see #getEnumerationLiteral()
	 * @generated
	 */
	void setEnumerationLiteral(EnumerationLiteral value);

	/**
	 * Returns the value of the '<em><b>Explicit Dereference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Explicit Dereference</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Explicit Dereference</em>' containment reference.
	 * @see #setExplicitDereference(ExplicitDereference)
	 * @see Ada.AdaPackage#getExpressionClass_ExplicitDereference()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='explicit_dereference' namespace='##targetNamespace'"
	 * @generated
	 */
	ExplicitDereference getExplicitDereference();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getExplicitDereference <em>Explicit Dereference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Explicit Dereference</em>' containment reference.
	 * @see #getExplicitDereference()
	 * @generated
	 */
	void setExplicitDereference(ExplicitDereference value);

	/**
	 * Returns the value of the '<em><b>Function Call</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Call</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Call</em>' containment reference.
	 * @see #setFunctionCall(FunctionCall)
	 * @see Ada.AdaPackage#getExpressionClass_FunctionCall()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='function_call' namespace='##targetNamespace'"
	 * @generated
	 */
	FunctionCall getFunctionCall();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getFunctionCall <em>Function Call</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function Call</em>' containment reference.
	 * @see #getFunctionCall()
	 * @generated
	 */
	void setFunctionCall(FunctionCall value);

	/**
	 * Returns the value of the '<em><b>Indexed Component</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Indexed Component</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Indexed Component</em>' containment reference.
	 * @see #setIndexedComponent(IndexedComponent)
	 * @see Ada.AdaPackage#getExpressionClass_IndexedComponent()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='indexed_component' namespace='##targetNamespace'"
	 * @generated
	 */
	IndexedComponent getIndexedComponent();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getIndexedComponent <em>Indexed Component</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Indexed Component</em>' containment reference.
	 * @see #getIndexedComponent()
	 * @generated
	 */
	void setIndexedComponent(IndexedComponent value);

	/**
	 * Returns the value of the '<em><b>Slice</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Slice</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Slice</em>' containment reference.
	 * @see #setSlice(Slice)
	 * @see Ada.AdaPackage#getExpressionClass_Slice()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='slice' namespace='##targetNamespace'"
	 * @generated
	 */
	Slice getSlice();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getSlice <em>Slice</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Slice</em>' containment reference.
	 * @see #getSlice()
	 * @generated
	 */
	void setSlice(Slice value);

	/**
	 * Returns the value of the '<em><b>Selected Component</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selected Component</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selected Component</em>' containment reference.
	 * @see #setSelectedComponent(SelectedComponent)
	 * @see Ada.AdaPackage#getExpressionClass_SelectedComponent()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='selected_component' namespace='##targetNamespace'"
	 * @generated
	 */
	SelectedComponent getSelectedComponent();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getSelectedComponent <em>Selected Component</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selected Component</em>' containment reference.
	 * @see #getSelectedComponent()
	 * @generated
	 */
	void setSelectedComponent(SelectedComponent value);

	/**
	 * Returns the value of the '<em><b>Access Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access Attribute</em>' containment reference.
	 * @see #setAccessAttribute(AccessAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_AccessAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='access_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	AccessAttribute getAccessAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getAccessAttribute <em>Access Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access Attribute</em>' containment reference.
	 * @see #getAccessAttribute()
	 * @generated
	 */
	void setAccessAttribute(AccessAttribute value);

	/**
	 * Returns the value of the '<em><b>Address Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Address Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Address Attribute</em>' containment reference.
	 * @see #setAddressAttribute(AddressAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_AddressAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='address_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	AddressAttribute getAddressAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getAddressAttribute <em>Address Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Address Attribute</em>' containment reference.
	 * @see #getAddressAttribute()
	 * @generated
	 */
	void setAddressAttribute(AddressAttribute value);

	/**
	 * Returns the value of the '<em><b>Adjacent Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Adjacent Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Adjacent Attribute</em>' containment reference.
	 * @see #setAdjacentAttribute(AdjacentAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_AdjacentAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='adjacent_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	AdjacentAttribute getAdjacentAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getAdjacentAttribute <em>Adjacent Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Adjacent Attribute</em>' containment reference.
	 * @see #getAdjacentAttribute()
	 * @generated
	 */
	void setAdjacentAttribute(AdjacentAttribute value);

	/**
	 * Returns the value of the '<em><b>Aft Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aft Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aft Attribute</em>' containment reference.
	 * @see #setAftAttribute(AftAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_AftAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='aft_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	AftAttribute getAftAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getAftAttribute <em>Aft Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aft Attribute</em>' containment reference.
	 * @see #getAftAttribute()
	 * @generated
	 */
	void setAftAttribute(AftAttribute value);

	/**
	 * Returns the value of the '<em><b>Alignment Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alignment Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alignment Attribute</em>' containment reference.
	 * @see #setAlignmentAttribute(AlignmentAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_AlignmentAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='alignment_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	AlignmentAttribute getAlignmentAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getAlignmentAttribute <em>Alignment Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alignment Attribute</em>' containment reference.
	 * @see #getAlignmentAttribute()
	 * @generated
	 */
	void setAlignmentAttribute(AlignmentAttribute value);

	/**
	 * Returns the value of the '<em><b>Base Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Attribute</em>' containment reference.
	 * @see #setBaseAttribute(BaseAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_BaseAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='base_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	BaseAttribute getBaseAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getBaseAttribute <em>Base Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Attribute</em>' containment reference.
	 * @see #getBaseAttribute()
	 * @generated
	 */
	void setBaseAttribute(BaseAttribute value);

	/**
	 * Returns the value of the '<em><b>Bit Order Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bit Order Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bit Order Attribute</em>' containment reference.
	 * @see #setBitOrderAttribute(BitOrderAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_BitOrderAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='bit_order_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	BitOrderAttribute getBitOrderAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getBitOrderAttribute <em>Bit Order Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bit Order Attribute</em>' containment reference.
	 * @see #getBitOrderAttribute()
	 * @generated
	 */
	void setBitOrderAttribute(BitOrderAttribute value);

	/**
	 * Returns the value of the '<em><b>Body Version Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body Version Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body Version Attribute</em>' containment reference.
	 * @see #setBodyVersionAttribute(BodyVersionAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_BodyVersionAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='body_version_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	BodyVersionAttribute getBodyVersionAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getBodyVersionAttribute <em>Body Version Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body Version Attribute</em>' containment reference.
	 * @see #getBodyVersionAttribute()
	 * @generated
	 */
	void setBodyVersionAttribute(BodyVersionAttribute value);

	/**
	 * Returns the value of the '<em><b>Callable Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Callable Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Callable Attribute</em>' containment reference.
	 * @see #setCallableAttribute(CallableAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_CallableAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='callable_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	CallableAttribute getCallableAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getCallableAttribute <em>Callable Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Callable Attribute</em>' containment reference.
	 * @see #getCallableAttribute()
	 * @generated
	 */
	void setCallableAttribute(CallableAttribute value);

	/**
	 * Returns the value of the '<em><b>Caller Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Caller Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Caller Attribute</em>' containment reference.
	 * @see #setCallerAttribute(CallerAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_CallerAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='caller_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	CallerAttribute getCallerAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getCallerAttribute <em>Caller Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Caller Attribute</em>' containment reference.
	 * @see #getCallerAttribute()
	 * @generated
	 */
	void setCallerAttribute(CallerAttribute value);

	/**
	 * Returns the value of the '<em><b>Ceiling Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ceiling Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ceiling Attribute</em>' containment reference.
	 * @see #setCeilingAttribute(CeilingAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_CeilingAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ceiling_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	CeilingAttribute getCeilingAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getCeilingAttribute <em>Ceiling Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ceiling Attribute</em>' containment reference.
	 * @see #getCeilingAttribute()
	 * @generated
	 */
	void setCeilingAttribute(CeilingAttribute value);

	/**
	 * Returns the value of the '<em><b>Class Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Attribute</em>' containment reference.
	 * @see #setClassAttribute(ClassAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_ClassAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='class_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ClassAttribute getClassAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getClassAttribute <em>Class Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class Attribute</em>' containment reference.
	 * @see #getClassAttribute()
	 * @generated
	 */
	void setClassAttribute(ClassAttribute value);

	/**
	 * Returns the value of the '<em><b>Component Size Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Size Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Size Attribute</em>' containment reference.
	 * @see #setComponentSizeAttribute(ComponentSizeAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_ComponentSizeAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='component_size_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ComponentSizeAttribute getComponentSizeAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getComponentSizeAttribute <em>Component Size Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component Size Attribute</em>' containment reference.
	 * @see #getComponentSizeAttribute()
	 * @generated
	 */
	void setComponentSizeAttribute(ComponentSizeAttribute value);

	/**
	 * Returns the value of the '<em><b>Compose Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Compose Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Compose Attribute</em>' containment reference.
	 * @see #setComposeAttribute(ComposeAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_ComposeAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='compose_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ComposeAttribute getComposeAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getComposeAttribute <em>Compose Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Compose Attribute</em>' containment reference.
	 * @see #getComposeAttribute()
	 * @generated
	 */
	void setComposeAttribute(ComposeAttribute value);

	/**
	 * Returns the value of the '<em><b>Constrained Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constrained Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constrained Attribute</em>' containment reference.
	 * @see #setConstrainedAttribute(ConstrainedAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_ConstrainedAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='constrained_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ConstrainedAttribute getConstrainedAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getConstrainedAttribute <em>Constrained Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constrained Attribute</em>' containment reference.
	 * @see #getConstrainedAttribute()
	 * @generated
	 */
	void setConstrainedAttribute(ConstrainedAttribute value);

	/**
	 * Returns the value of the '<em><b>Copy Sign Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Copy Sign Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Copy Sign Attribute</em>' containment reference.
	 * @see #setCopySignAttribute(CopySignAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_CopySignAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='copy_sign_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	CopySignAttribute getCopySignAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getCopySignAttribute <em>Copy Sign Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Copy Sign Attribute</em>' containment reference.
	 * @see #getCopySignAttribute()
	 * @generated
	 */
	void setCopySignAttribute(CopySignAttribute value);

	/**
	 * Returns the value of the '<em><b>Count Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Count Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Count Attribute</em>' containment reference.
	 * @see #setCountAttribute(CountAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_CountAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='count_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	CountAttribute getCountAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getCountAttribute <em>Count Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Count Attribute</em>' containment reference.
	 * @see #getCountAttribute()
	 * @generated
	 */
	void setCountAttribute(CountAttribute value);

	/**
	 * Returns the value of the '<em><b>Definite Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Definite Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definite Attribute</em>' containment reference.
	 * @see #setDefiniteAttribute(DefiniteAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_DefiniteAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='definite_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiniteAttribute getDefiniteAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getDefiniteAttribute <em>Definite Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Definite Attribute</em>' containment reference.
	 * @see #getDefiniteAttribute()
	 * @generated
	 */
	void setDefiniteAttribute(DefiniteAttribute value);

	/**
	 * Returns the value of the '<em><b>Delta Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delta Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delta Attribute</em>' containment reference.
	 * @see #setDeltaAttribute(DeltaAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_DeltaAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='delta_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	DeltaAttribute getDeltaAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getDeltaAttribute <em>Delta Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delta Attribute</em>' containment reference.
	 * @see #getDeltaAttribute()
	 * @generated
	 */
	void setDeltaAttribute(DeltaAttribute value);

	/**
	 * Returns the value of the '<em><b>Denorm Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Denorm Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Denorm Attribute</em>' containment reference.
	 * @see #setDenormAttribute(DenormAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_DenormAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='denorm_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	DenormAttribute getDenormAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getDenormAttribute <em>Denorm Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Denorm Attribute</em>' containment reference.
	 * @see #getDenormAttribute()
	 * @generated
	 */
	void setDenormAttribute(DenormAttribute value);

	/**
	 * Returns the value of the '<em><b>Digits Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Digits Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Digits Attribute</em>' containment reference.
	 * @see #setDigitsAttribute(DigitsAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_DigitsAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='digits_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	DigitsAttribute getDigitsAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getDigitsAttribute <em>Digits Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Digits Attribute</em>' containment reference.
	 * @see #getDigitsAttribute()
	 * @generated
	 */
	void setDigitsAttribute(DigitsAttribute value);

	/**
	 * Returns the value of the '<em><b>Exponent Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exponent Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exponent Attribute</em>' containment reference.
	 * @see #setExponentAttribute(ExponentAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_ExponentAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='exponent_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ExponentAttribute getExponentAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getExponentAttribute <em>Exponent Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exponent Attribute</em>' containment reference.
	 * @see #getExponentAttribute()
	 * @generated
	 */
	void setExponentAttribute(ExponentAttribute value);

	/**
	 * Returns the value of the '<em><b>External Tag Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External Tag Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Tag Attribute</em>' containment reference.
	 * @see #setExternalTagAttribute(ExternalTagAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_ExternalTagAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='external_tag_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ExternalTagAttribute getExternalTagAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getExternalTagAttribute <em>External Tag Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>External Tag Attribute</em>' containment reference.
	 * @see #getExternalTagAttribute()
	 * @generated
	 */
	void setExternalTagAttribute(ExternalTagAttribute value);

	/**
	 * Returns the value of the '<em><b>First Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Attribute</em>' containment reference.
	 * @see #setFirstAttribute(FirstAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_FirstAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='first_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	FirstAttribute getFirstAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getFirstAttribute <em>First Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First Attribute</em>' containment reference.
	 * @see #getFirstAttribute()
	 * @generated
	 */
	void setFirstAttribute(FirstAttribute value);

	/**
	 * Returns the value of the '<em><b>First Bit Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Bit Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Bit Attribute</em>' containment reference.
	 * @see #setFirstBitAttribute(FirstBitAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_FirstBitAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='first_bit_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	FirstBitAttribute getFirstBitAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getFirstBitAttribute <em>First Bit Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First Bit Attribute</em>' containment reference.
	 * @see #getFirstBitAttribute()
	 * @generated
	 */
	void setFirstBitAttribute(FirstBitAttribute value);

	/**
	 * Returns the value of the '<em><b>Floor Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Floor Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Floor Attribute</em>' containment reference.
	 * @see #setFloorAttribute(FloorAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_FloorAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='floor_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	FloorAttribute getFloorAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getFloorAttribute <em>Floor Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Floor Attribute</em>' containment reference.
	 * @see #getFloorAttribute()
	 * @generated
	 */
	void setFloorAttribute(FloorAttribute value);

	/**
	 * Returns the value of the '<em><b>Fore Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fore Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fore Attribute</em>' containment reference.
	 * @see #setForeAttribute(ForeAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_ForeAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='fore_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ForeAttribute getForeAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getForeAttribute <em>Fore Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fore Attribute</em>' containment reference.
	 * @see #getForeAttribute()
	 * @generated
	 */
	void setForeAttribute(ForeAttribute value);

	/**
	 * Returns the value of the '<em><b>Fraction Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fraction Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fraction Attribute</em>' containment reference.
	 * @see #setFractionAttribute(FractionAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_FractionAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='fraction_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	FractionAttribute getFractionAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getFractionAttribute <em>Fraction Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fraction Attribute</em>' containment reference.
	 * @see #getFractionAttribute()
	 * @generated
	 */
	void setFractionAttribute(FractionAttribute value);

	/**
	 * Returns the value of the '<em><b>Identity Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Identity Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identity Attribute</em>' containment reference.
	 * @see #setIdentityAttribute(IdentityAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_IdentityAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='identity_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	IdentityAttribute getIdentityAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getIdentityAttribute <em>Identity Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Identity Attribute</em>' containment reference.
	 * @see #getIdentityAttribute()
	 * @generated
	 */
	void setIdentityAttribute(IdentityAttribute value);

	/**
	 * Returns the value of the '<em><b>Image Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Image Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Image Attribute</em>' containment reference.
	 * @see #setImageAttribute(ImageAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_ImageAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='image_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ImageAttribute getImageAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getImageAttribute <em>Image Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Image Attribute</em>' containment reference.
	 * @see #getImageAttribute()
	 * @generated
	 */
	void setImageAttribute(ImageAttribute value);

	/**
	 * Returns the value of the '<em><b>Input Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Attribute</em>' containment reference.
	 * @see #setInputAttribute(InputAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_InputAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='input_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	InputAttribute getInputAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getInputAttribute <em>Input Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input Attribute</em>' containment reference.
	 * @see #getInputAttribute()
	 * @generated
	 */
	void setInputAttribute(InputAttribute value);

	/**
	 * Returns the value of the '<em><b>Last Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Attribute</em>' containment reference.
	 * @see #setLastAttribute(LastAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_LastAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='last_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	LastAttribute getLastAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getLastAttribute <em>Last Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Attribute</em>' containment reference.
	 * @see #getLastAttribute()
	 * @generated
	 */
	void setLastAttribute(LastAttribute value);

	/**
	 * Returns the value of the '<em><b>Last Bit Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Bit Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Bit Attribute</em>' containment reference.
	 * @see #setLastBitAttribute(LastBitAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_LastBitAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='last_bit_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	LastBitAttribute getLastBitAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getLastBitAttribute <em>Last Bit Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Bit Attribute</em>' containment reference.
	 * @see #getLastBitAttribute()
	 * @generated
	 */
	void setLastBitAttribute(LastBitAttribute value);

	/**
	 * Returns the value of the '<em><b>Leading Part Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Leading Part Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Leading Part Attribute</em>' containment reference.
	 * @see #setLeadingPartAttribute(LeadingPartAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_LeadingPartAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='leading_part_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	LeadingPartAttribute getLeadingPartAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getLeadingPartAttribute <em>Leading Part Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Leading Part Attribute</em>' containment reference.
	 * @see #getLeadingPartAttribute()
	 * @generated
	 */
	void setLeadingPartAttribute(LeadingPartAttribute value);

	/**
	 * Returns the value of the '<em><b>Length Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Length Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Length Attribute</em>' containment reference.
	 * @see #setLengthAttribute(LengthAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_LengthAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='length_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	LengthAttribute getLengthAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getLengthAttribute <em>Length Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Length Attribute</em>' containment reference.
	 * @see #getLengthAttribute()
	 * @generated
	 */
	void setLengthAttribute(LengthAttribute value);

	/**
	 * Returns the value of the '<em><b>Machine Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Attribute</em>' containment reference.
	 * @see #setMachineAttribute(MachineAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_MachineAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='machine_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	MachineAttribute getMachineAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getMachineAttribute <em>Machine Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Machine Attribute</em>' containment reference.
	 * @see #getMachineAttribute()
	 * @generated
	 */
	void setMachineAttribute(MachineAttribute value);

	/**
	 * Returns the value of the '<em><b>Machine Emax Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Emax Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Emax Attribute</em>' containment reference.
	 * @see #setMachineEmaxAttribute(MachineEmaxAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_MachineEmaxAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='machine_emax_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	MachineEmaxAttribute getMachineEmaxAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getMachineEmaxAttribute <em>Machine Emax Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Machine Emax Attribute</em>' containment reference.
	 * @see #getMachineEmaxAttribute()
	 * @generated
	 */
	void setMachineEmaxAttribute(MachineEmaxAttribute value);

	/**
	 * Returns the value of the '<em><b>Machine Emin Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Emin Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Emin Attribute</em>' containment reference.
	 * @see #setMachineEminAttribute(MachineEminAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_MachineEminAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='machine_emin_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	MachineEminAttribute getMachineEminAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getMachineEminAttribute <em>Machine Emin Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Machine Emin Attribute</em>' containment reference.
	 * @see #getMachineEminAttribute()
	 * @generated
	 */
	void setMachineEminAttribute(MachineEminAttribute value);

	/**
	 * Returns the value of the '<em><b>Machine Mantissa Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Mantissa Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Mantissa Attribute</em>' containment reference.
	 * @see #setMachineMantissaAttribute(MachineMantissaAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_MachineMantissaAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='machine_mantissa_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	MachineMantissaAttribute getMachineMantissaAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getMachineMantissaAttribute <em>Machine Mantissa Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Machine Mantissa Attribute</em>' containment reference.
	 * @see #getMachineMantissaAttribute()
	 * @generated
	 */
	void setMachineMantissaAttribute(MachineMantissaAttribute value);

	/**
	 * Returns the value of the '<em><b>Machine Overflows Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Overflows Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Overflows Attribute</em>' containment reference.
	 * @see #setMachineOverflowsAttribute(MachineOverflowsAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_MachineOverflowsAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='machine_overflows_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	MachineOverflowsAttribute getMachineOverflowsAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getMachineOverflowsAttribute <em>Machine Overflows Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Machine Overflows Attribute</em>' containment reference.
	 * @see #getMachineOverflowsAttribute()
	 * @generated
	 */
	void setMachineOverflowsAttribute(MachineOverflowsAttribute value);

	/**
	 * Returns the value of the '<em><b>Machine Radix Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Radix Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Radix Attribute</em>' containment reference.
	 * @see #setMachineRadixAttribute(MachineRadixAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_MachineRadixAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='machine_radix_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	MachineRadixAttribute getMachineRadixAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getMachineRadixAttribute <em>Machine Radix Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Machine Radix Attribute</em>' containment reference.
	 * @see #getMachineRadixAttribute()
	 * @generated
	 */
	void setMachineRadixAttribute(MachineRadixAttribute value);

	/**
	 * Returns the value of the '<em><b>Machine Rounds Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Rounds Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Rounds Attribute</em>' containment reference.
	 * @see #setMachineRoundsAttribute(MachineRoundsAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_MachineRoundsAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='machine_rounds_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	MachineRoundsAttribute getMachineRoundsAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getMachineRoundsAttribute <em>Machine Rounds Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Machine Rounds Attribute</em>' containment reference.
	 * @see #getMachineRoundsAttribute()
	 * @generated
	 */
	void setMachineRoundsAttribute(MachineRoundsAttribute value);

	/**
	 * Returns the value of the '<em><b>Max Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Attribute</em>' containment reference.
	 * @see #setMaxAttribute(MaxAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_MaxAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='max_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	MaxAttribute getMaxAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getMaxAttribute <em>Max Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Attribute</em>' containment reference.
	 * @see #getMaxAttribute()
	 * @generated
	 */
	void setMaxAttribute(MaxAttribute value);

	/**
	 * Returns the value of the '<em><b>Max Size In Storage Elements Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Size In Storage Elements Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Size In Storage Elements Attribute</em>' containment reference.
	 * @see #setMaxSizeInStorageElementsAttribute(MaxSizeInStorageElementsAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_MaxSizeInStorageElementsAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='max_size_in_storage_elements_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	MaxSizeInStorageElementsAttribute getMaxSizeInStorageElementsAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getMaxSizeInStorageElementsAttribute <em>Max Size In Storage Elements Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Size In Storage Elements Attribute</em>' containment reference.
	 * @see #getMaxSizeInStorageElementsAttribute()
	 * @generated
	 */
	void setMaxSizeInStorageElementsAttribute(MaxSizeInStorageElementsAttribute value);

	/**
	 * Returns the value of the '<em><b>Min Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Attribute</em>' containment reference.
	 * @see #setMinAttribute(MinAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_MinAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='min_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	MinAttribute getMinAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getMinAttribute <em>Min Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Attribute</em>' containment reference.
	 * @see #getMinAttribute()
	 * @generated
	 */
	void setMinAttribute(MinAttribute value);

	/**
	 * Returns the value of the '<em><b>Model Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Attribute</em>' containment reference.
	 * @see #setModelAttribute(ModelAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_ModelAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='model_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ModelAttribute getModelAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getModelAttribute <em>Model Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Attribute</em>' containment reference.
	 * @see #getModelAttribute()
	 * @generated
	 */
	void setModelAttribute(ModelAttribute value);

	/**
	 * Returns the value of the '<em><b>Model Emin Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Emin Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Emin Attribute</em>' containment reference.
	 * @see #setModelEminAttribute(ModelEminAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_ModelEminAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='model_emin_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ModelEminAttribute getModelEminAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getModelEminAttribute <em>Model Emin Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Emin Attribute</em>' containment reference.
	 * @see #getModelEminAttribute()
	 * @generated
	 */
	void setModelEminAttribute(ModelEminAttribute value);

	/**
	 * Returns the value of the '<em><b>Model Epsilon Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Epsilon Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Epsilon Attribute</em>' containment reference.
	 * @see #setModelEpsilonAttribute(ModelEpsilonAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_ModelEpsilonAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='model_epsilon_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ModelEpsilonAttribute getModelEpsilonAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getModelEpsilonAttribute <em>Model Epsilon Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Epsilon Attribute</em>' containment reference.
	 * @see #getModelEpsilonAttribute()
	 * @generated
	 */
	void setModelEpsilonAttribute(ModelEpsilonAttribute value);

	/**
	 * Returns the value of the '<em><b>Model Mantissa Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Mantissa Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Mantissa Attribute</em>' containment reference.
	 * @see #setModelMantissaAttribute(ModelMantissaAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_ModelMantissaAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='model_mantissa_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ModelMantissaAttribute getModelMantissaAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getModelMantissaAttribute <em>Model Mantissa Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Mantissa Attribute</em>' containment reference.
	 * @see #getModelMantissaAttribute()
	 * @generated
	 */
	void setModelMantissaAttribute(ModelMantissaAttribute value);

	/**
	 * Returns the value of the '<em><b>Model Small Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Small Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Small Attribute</em>' containment reference.
	 * @see #setModelSmallAttribute(ModelSmallAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_ModelSmallAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='model_small_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ModelSmallAttribute getModelSmallAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getModelSmallAttribute <em>Model Small Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Small Attribute</em>' containment reference.
	 * @see #getModelSmallAttribute()
	 * @generated
	 */
	void setModelSmallAttribute(ModelSmallAttribute value);

	/**
	 * Returns the value of the '<em><b>Modulus Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Modulus Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Modulus Attribute</em>' containment reference.
	 * @see #setModulusAttribute(ModulusAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_ModulusAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='modulus_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ModulusAttribute getModulusAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getModulusAttribute <em>Modulus Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Modulus Attribute</em>' containment reference.
	 * @see #getModulusAttribute()
	 * @generated
	 */
	void setModulusAttribute(ModulusAttribute value);

	/**
	 * Returns the value of the '<em><b>Output Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Attribute</em>' containment reference.
	 * @see #setOutputAttribute(OutputAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_OutputAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='output_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	OutputAttribute getOutputAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getOutputAttribute <em>Output Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Attribute</em>' containment reference.
	 * @see #getOutputAttribute()
	 * @generated
	 */
	void setOutputAttribute(OutputAttribute value);

	/**
	 * Returns the value of the '<em><b>Partition Id Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partition Id Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partition Id Attribute</em>' containment reference.
	 * @see #setPartitionIdAttribute(PartitionIdAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_PartitionIdAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='partition_id_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	PartitionIdAttribute getPartitionIdAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getPartitionIdAttribute <em>Partition Id Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Partition Id Attribute</em>' containment reference.
	 * @see #getPartitionIdAttribute()
	 * @generated
	 */
	void setPartitionIdAttribute(PartitionIdAttribute value);

	/**
	 * Returns the value of the '<em><b>Pos Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pos Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pos Attribute</em>' containment reference.
	 * @see #setPosAttribute(PosAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_PosAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='pos_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	PosAttribute getPosAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getPosAttribute <em>Pos Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pos Attribute</em>' containment reference.
	 * @see #getPosAttribute()
	 * @generated
	 */
	void setPosAttribute(PosAttribute value);

	/**
	 * Returns the value of the '<em><b>Position Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Position Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position Attribute</em>' containment reference.
	 * @see #setPositionAttribute(PositionAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_PositionAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='position_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	PositionAttribute getPositionAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getPositionAttribute <em>Position Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Position Attribute</em>' containment reference.
	 * @see #getPositionAttribute()
	 * @generated
	 */
	void setPositionAttribute(PositionAttribute value);

	/**
	 * Returns the value of the '<em><b>Pred Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pred Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pred Attribute</em>' containment reference.
	 * @see #setPredAttribute(PredAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_PredAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='pred_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	PredAttribute getPredAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getPredAttribute <em>Pred Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pred Attribute</em>' containment reference.
	 * @see #getPredAttribute()
	 * @generated
	 */
	void setPredAttribute(PredAttribute value);

	/**
	 * Returns the value of the '<em><b>Range Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Range Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range Attribute</em>' containment reference.
	 * @see #setRangeAttribute(RangeAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_RangeAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='range_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	RangeAttribute getRangeAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getRangeAttribute <em>Range Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Range Attribute</em>' containment reference.
	 * @see #getRangeAttribute()
	 * @generated
	 */
	void setRangeAttribute(RangeAttribute value);

	/**
	 * Returns the value of the '<em><b>Read Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Read Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Read Attribute</em>' containment reference.
	 * @see #setReadAttribute(ReadAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_ReadAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='read_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ReadAttribute getReadAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getReadAttribute <em>Read Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Read Attribute</em>' containment reference.
	 * @see #getReadAttribute()
	 * @generated
	 */
	void setReadAttribute(ReadAttribute value);

	/**
	 * Returns the value of the '<em><b>Remainder Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remainder Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remainder Attribute</em>' containment reference.
	 * @see #setRemainderAttribute(RemainderAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_RemainderAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='remainder_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	RemainderAttribute getRemainderAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getRemainderAttribute <em>Remainder Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Remainder Attribute</em>' containment reference.
	 * @see #getRemainderAttribute()
	 * @generated
	 */
	void setRemainderAttribute(RemainderAttribute value);

	/**
	 * Returns the value of the '<em><b>Round Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Round Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Round Attribute</em>' containment reference.
	 * @see #setRoundAttribute(RoundAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_RoundAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='round_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	RoundAttribute getRoundAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getRoundAttribute <em>Round Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Round Attribute</em>' containment reference.
	 * @see #getRoundAttribute()
	 * @generated
	 */
	void setRoundAttribute(RoundAttribute value);

	/**
	 * Returns the value of the '<em><b>Rounding Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rounding Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rounding Attribute</em>' containment reference.
	 * @see #setRoundingAttribute(RoundingAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_RoundingAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='rounding_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	RoundingAttribute getRoundingAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getRoundingAttribute <em>Rounding Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rounding Attribute</em>' containment reference.
	 * @see #getRoundingAttribute()
	 * @generated
	 */
	void setRoundingAttribute(RoundingAttribute value);

	/**
	 * Returns the value of the '<em><b>Safe First Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Safe First Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Safe First Attribute</em>' containment reference.
	 * @see #setSafeFirstAttribute(SafeFirstAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_SafeFirstAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='safe_first_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	SafeFirstAttribute getSafeFirstAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getSafeFirstAttribute <em>Safe First Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Safe First Attribute</em>' containment reference.
	 * @see #getSafeFirstAttribute()
	 * @generated
	 */
	void setSafeFirstAttribute(SafeFirstAttribute value);

	/**
	 * Returns the value of the '<em><b>Safe Last Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Safe Last Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Safe Last Attribute</em>' containment reference.
	 * @see #setSafeLastAttribute(SafeLastAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_SafeLastAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='safe_last_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	SafeLastAttribute getSafeLastAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getSafeLastAttribute <em>Safe Last Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Safe Last Attribute</em>' containment reference.
	 * @see #getSafeLastAttribute()
	 * @generated
	 */
	void setSafeLastAttribute(SafeLastAttribute value);

	/**
	 * Returns the value of the '<em><b>Scale Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scale Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scale Attribute</em>' containment reference.
	 * @see #setScaleAttribute(ScaleAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_ScaleAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='scale_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ScaleAttribute getScaleAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getScaleAttribute <em>Scale Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scale Attribute</em>' containment reference.
	 * @see #getScaleAttribute()
	 * @generated
	 */
	void setScaleAttribute(ScaleAttribute value);

	/**
	 * Returns the value of the '<em><b>Scaling Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scaling Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scaling Attribute</em>' containment reference.
	 * @see #setScalingAttribute(ScalingAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_ScalingAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='scaling_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ScalingAttribute getScalingAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getScalingAttribute <em>Scaling Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scaling Attribute</em>' containment reference.
	 * @see #getScalingAttribute()
	 * @generated
	 */
	void setScalingAttribute(ScalingAttribute value);

	/**
	 * Returns the value of the '<em><b>Signed Zeros Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signed Zeros Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signed Zeros Attribute</em>' containment reference.
	 * @see #setSignedZerosAttribute(SignedZerosAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_SignedZerosAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='signed_zeros_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	SignedZerosAttribute getSignedZerosAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getSignedZerosAttribute <em>Signed Zeros Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Signed Zeros Attribute</em>' containment reference.
	 * @see #getSignedZerosAttribute()
	 * @generated
	 */
	void setSignedZerosAttribute(SignedZerosAttribute value);

	/**
	 * Returns the value of the '<em><b>Size Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Size Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Size Attribute</em>' containment reference.
	 * @see #setSizeAttribute(SizeAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_SizeAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='size_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	SizeAttribute getSizeAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getSizeAttribute <em>Size Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Size Attribute</em>' containment reference.
	 * @see #getSizeAttribute()
	 * @generated
	 */
	void setSizeAttribute(SizeAttribute value);

	/**
	 * Returns the value of the '<em><b>Small Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Small Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Small Attribute</em>' containment reference.
	 * @see #setSmallAttribute(SmallAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_SmallAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='small_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	SmallAttribute getSmallAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getSmallAttribute <em>Small Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Small Attribute</em>' containment reference.
	 * @see #getSmallAttribute()
	 * @generated
	 */
	void setSmallAttribute(SmallAttribute value);

	/**
	 * Returns the value of the '<em><b>Storage Pool Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Pool Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Pool Attribute</em>' containment reference.
	 * @see #setStoragePoolAttribute(StoragePoolAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_StoragePoolAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='storage_pool_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	StoragePoolAttribute getStoragePoolAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getStoragePoolAttribute <em>Storage Pool Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Storage Pool Attribute</em>' containment reference.
	 * @see #getStoragePoolAttribute()
	 * @generated
	 */
	void setStoragePoolAttribute(StoragePoolAttribute value);

	/**
	 * Returns the value of the '<em><b>Storage Size Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Size Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Size Attribute</em>' containment reference.
	 * @see #setStorageSizeAttribute(StorageSizeAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_StorageSizeAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='storage_size_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	StorageSizeAttribute getStorageSizeAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getStorageSizeAttribute <em>Storage Size Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Storage Size Attribute</em>' containment reference.
	 * @see #getStorageSizeAttribute()
	 * @generated
	 */
	void setStorageSizeAttribute(StorageSizeAttribute value);

	/**
	 * Returns the value of the '<em><b>Succ Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Succ Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Succ Attribute</em>' containment reference.
	 * @see #setSuccAttribute(SuccAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_SuccAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='succ_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	SuccAttribute getSuccAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getSuccAttribute <em>Succ Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Succ Attribute</em>' containment reference.
	 * @see #getSuccAttribute()
	 * @generated
	 */
	void setSuccAttribute(SuccAttribute value);

	/**
	 * Returns the value of the '<em><b>Tag Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tag Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tag Attribute</em>' containment reference.
	 * @see #setTagAttribute(TagAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_TagAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='tag_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	TagAttribute getTagAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getTagAttribute <em>Tag Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tag Attribute</em>' containment reference.
	 * @see #getTagAttribute()
	 * @generated
	 */
	void setTagAttribute(TagAttribute value);

	/**
	 * Returns the value of the '<em><b>Terminated Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Terminated Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Terminated Attribute</em>' containment reference.
	 * @see #setTerminatedAttribute(TerminatedAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_TerminatedAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='terminated_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	TerminatedAttribute getTerminatedAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getTerminatedAttribute <em>Terminated Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Terminated Attribute</em>' containment reference.
	 * @see #getTerminatedAttribute()
	 * @generated
	 */
	void setTerminatedAttribute(TerminatedAttribute value);

	/**
	 * Returns the value of the '<em><b>Truncation Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Truncation Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Truncation Attribute</em>' containment reference.
	 * @see #setTruncationAttribute(TruncationAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_TruncationAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='truncation_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	TruncationAttribute getTruncationAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getTruncationAttribute <em>Truncation Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Truncation Attribute</em>' containment reference.
	 * @see #getTruncationAttribute()
	 * @generated
	 */
	void setTruncationAttribute(TruncationAttribute value);

	/**
	 * Returns the value of the '<em><b>Unbiased Rounding Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unbiased Rounding Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unbiased Rounding Attribute</em>' containment reference.
	 * @see #setUnbiasedRoundingAttribute(UnbiasedRoundingAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_UnbiasedRoundingAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unbiased_rounding_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	UnbiasedRoundingAttribute getUnbiasedRoundingAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getUnbiasedRoundingAttribute <em>Unbiased Rounding Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unbiased Rounding Attribute</em>' containment reference.
	 * @see #getUnbiasedRoundingAttribute()
	 * @generated
	 */
	void setUnbiasedRoundingAttribute(UnbiasedRoundingAttribute value);

	/**
	 * Returns the value of the '<em><b>Unchecked Access Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unchecked Access Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unchecked Access Attribute</em>' containment reference.
	 * @see #setUncheckedAccessAttribute(UncheckedAccessAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_UncheckedAccessAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unchecked_access_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	UncheckedAccessAttribute getUncheckedAccessAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getUncheckedAccessAttribute <em>Unchecked Access Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unchecked Access Attribute</em>' containment reference.
	 * @see #getUncheckedAccessAttribute()
	 * @generated
	 */
	void setUncheckedAccessAttribute(UncheckedAccessAttribute value);

	/**
	 * Returns the value of the '<em><b>Val Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Val Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Val Attribute</em>' containment reference.
	 * @see #setValAttribute(ValAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_ValAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='val_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ValAttribute getValAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getValAttribute <em>Val Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Val Attribute</em>' containment reference.
	 * @see #getValAttribute()
	 * @generated
	 */
	void setValAttribute(ValAttribute value);

	/**
	 * Returns the value of the '<em><b>Valid Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Valid Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Valid Attribute</em>' containment reference.
	 * @see #setValidAttribute(ValidAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_ValidAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='valid_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ValidAttribute getValidAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getValidAttribute <em>Valid Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Valid Attribute</em>' containment reference.
	 * @see #getValidAttribute()
	 * @generated
	 */
	void setValidAttribute(ValidAttribute value);

	/**
	 * Returns the value of the '<em><b>Value Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Attribute</em>' containment reference.
	 * @see #setValueAttribute(ValueAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_ValueAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='value_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ValueAttribute getValueAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getValueAttribute <em>Value Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Attribute</em>' containment reference.
	 * @see #getValueAttribute()
	 * @generated
	 */
	void setValueAttribute(ValueAttribute value);

	/**
	 * Returns the value of the '<em><b>Version Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version Attribute</em>' containment reference.
	 * @see #setVersionAttribute(VersionAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_VersionAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='version_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	VersionAttribute getVersionAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getVersionAttribute <em>Version Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version Attribute</em>' containment reference.
	 * @see #getVersionAttribute()
	 * @generated
	 */
	void setVersionAttribute(VersionAttribute value);

	/**
	 * Returns the value of the '<em><b>Wide Image Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Image Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Image Attribute</em>' containment reference.
	 * @see #setWideImageAttribute(WideImageAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_WideImageAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='wide_image_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	WideImageAttribute getWideImageAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getWideImageAttribute <em>Wide Image Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wide Image Attribute</em>' containment reference.
	 * @see #getWideImageAttribute()
	 * @generated
	 */
	void setWideImageAttribute(WideImageAttribute value);

	/**
	 * Returns the value of the '<em><b>Wide Value Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Value Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Value Attribute</em>' containment reference.
	 * @see #setWideValueAttribute(WideValueAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_WideValueAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='wide_value_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	WideValueAttribute getWideValueAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getWideValueAttribute <em>Wide Value Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wide Value Attribute</em>' containment reference.
	 * @see #getWideValueAttribute()
	 * @generated
	 */
	void setWideValueAttribute(WideValueAttribute value);

	/**
	 * Returns the value of the '<em><b>Wide Width Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Width Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Width Attribute</em>' containment reference.
	 * @see #setWideWidthAttribute(WideWidthAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_WideWidthAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='wide_width_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	WideWidthAttribute getWideWidthAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getWideWidthAttribute <em>Wide Width Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wide Width Attribute</em>' containment reference.
	 * @see #getWideWidthAttribute()
	 * @generated
	 */
	void setWideWidthAttribute(WideWidthAttribute value);

	/**
	 * Returns the value of the '<em><b>Width Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Width Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Width Attribute</em>' containment reference.
	 * @see #setWidthAttribute(WidthAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_WidthAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='width_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	WidthAttribute getWidthAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getWidthAttribute <em>Width Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Width Attribute</em>' containment reference.
	 * @see #getWidthAttribute()
	 * @generated
	 */
	void setWidthAttribute(WidthAttribute value);

	/**
	 * Returns the value of the '<em><b>Write Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Write Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Write Attribute</em>' containment reference.
	 * @see #setWriteAttribute(WriteAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_WriteAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='write_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	WriteAttribute getWriteAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getWriteAttribute <em>Write Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Write Attribute</em>' containment reference.
	 * @see #getWriteAttribute()
	 * @generated
	 */
	void setWriteAttribute(WriteAttribute value);

	/**
	 * Returns the value of the '<em><b>Machine Rounding Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Rounding Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Rounding Attribute</em>' containment reference.
	 * @see #setMachineRoundingAttribute(MachineRoundingAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_MachineRoundingAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='machine_rounding_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	MachineRoundingAttribute getMachineRoundingAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getMachineRoundingAttribute <em>Machine Rounding Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Machine Rounding Attribute</em>' containment reference.
	 * @see #getMachineRoundingAttribute()
	 * @generated
	 */
	void setMachineRoundingAttribute(MachineRoundingAttribute value);

	/**
	 * Returns the value of the '<em><b>Mod Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mod Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mod Attribute</em>' containment reference.
	 * @see #setModAttribute(ModAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_ModAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='mod_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ModAttribute getModAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getModAttribute <em>Mod Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mod Attribute</em>' containment reference.
	 * @see #getModAttribute()
	 * @generated
	 */
	void setModAttribute(ModAttribute value);

	/**
	 * Returns the value of the '<em><b>Priority Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Attribute</em>' containment reference.
	 * @see #setPriorityAttribute(PriorityAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_PriorityAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='priority_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	PriorityAttribute getPriorityAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getPriorityAttribute <em>Priority Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Priority Attribute</em>' containment reference.
	 * @see #getPriorityAttribute()
	 * @generated
	 */
	void setPriorityAttribute(PriorityAttribute value);

	/**
	 * Returns the value of the '<em><b>Stream Size Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stream Size Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stream Size Attribute</em>' containment reference.
	 * @see #setStreamSizeAttribute(StreamSizeAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_StreamSizeAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='stream_size_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	StreamSizeAttribute getStreamSizeAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getStreamSizeAttribute <em>Stream Size Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Stream Size Attribute</em>' containment reference.
	 * @see #getStreamSizeAttribute()
	 * @generated
	 */
	void setStreamSizeAttribute(StreamSizeAttribute value);

	/**
	 * Returns the value of the '<em><b>Wide Wide Image Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Wide Image Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Wide Image Attribute</em>' containment reference.
	 * @see #setWideWideImageAttribute(WideWideImageAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_WideWideImageAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='wide_wide_image_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	WideWideImageAttribute getWideWideImageAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getWideWideImageAttribute <em>Wide Wide Image Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wide Wide Image Attribute</em>' containment reference.
	 * @see #getWideWideImageAttribute()
	 * @generated
	 */
	void setWideWideImageAttribute(WideWideImageAttribute value);

	/**
	 * Returns the value of the '<em><b>Wide Wide Value Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Wide Value Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Wide Value Attribute</em>' containment reference.
	 * @see #setWideWideValueAttribute(WideWideValueAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_WideWideValueAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='wide_wide_value_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	WideWideValueAttribute getWideWideValueAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getWideWideValueAttribute <em>Wide Wide Value Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wide Wide Value Attribute</em>' containment reference.
	 * @see #getWideWideValueAttribute()
	 * @generated
	 */
	void setWideWideValueAttribute(WideWideValueAttribute value);

	/**
	 * Returns the value of the '<em><b>Wide Wide Width Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Wide Width Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Wide Width Attribute</em>' containment reference.
	 * @see #setWideWideWidthAttribute(WideWideWidthAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_WideWideWidthAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='wide_wide_width_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	WideWideWidthAttribute getWideWideWidthAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getWideWideWidthAttribute <em>Wide Wide Width Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wide Wide Width Attribute</em>' containment reference.
	 * @see #getWideWideWidthAttribute()
	 * @generated
	 */
	void setWideWideWidthAttribute(WideWideWidthAttribute value);

	/**
	 * Returns the value of the '<em><b>Max Alignment For Allocation Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Alignment For Allocation Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Alignment For Allocation Attribute</em>' containment reference.
	 * @see #setMaxAlignmentForAllocationAttribute(MaxAlignmentForAllocationAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_MaxAlignmentForAllocationAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='max_alignment_for_allocation_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	MaxAlignmentForAllocationAttribute getMaxAlignmentForAllocationAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getMaxAlignmentForAllocationAttribute <em>Max Alignment For Allocation Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Alignment For Allocation Attribute</em>' containment reference.
	 * @see #getMaxAlignmentForAllocationAttribute()
	 * @generated
	 */
	void setMaxAlignmentForAllocationAttribute(MaxAlignmentForAllocationAttribute value);

	/**
	 * Returns the value of the '<em><b>Overlaps Storage Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overlaps Storage Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overlaps Storage Attribute</em>' containment reference.
	 * @see #setOverlapsStorageAttribute(OverlapsStorageAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_OverlapsStorageAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='overlaps_storage_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	OverlapsStorageAttribute getOverlapsStorageAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getOverlapsStorageAttribute <em>Overlaps Storage Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Overlaps Storage Attribute</em>' containment reference.
	 * @see #getOverlapsStorageAttribute()
	 * @generated
	 */
	void setOverlapsStorageAttribute(OverlapsStorageAttribute value);

	/**
	 * Returns the value of the '<em><b>Implementation Defined Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implementation Defined Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implementation Defined Attribute</em>' containment reference.
	 * @see #setImplementationDefinedAttribute(ImplementationDefinedAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_ImplementationDefinedAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='implementation_defined_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ImplementationDefinedAttribute getImplementationDefinedAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getImplementationDefinedAttribute <em>Implementation Defined Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Implementation Defined Attribute</em>' containment reference.
	 * @see #getImplementationDefinedAttribute()
	 * @generated
	 */
	void setImplementationDefinedAttribute(ImplementationDefinedAttribute value);

	/**
	 * Returns the value of the '<em><b>Unknown Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unknown Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unknown Attribute</em>' containment reference.
	 * @see #setUnknownAttribute(UnknownAttribute)
	 * @see Ada.AdaPackage#getExpressionClass_UnknownAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unknown_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	UnknownAttribute getUnknownAttribute();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getUnknownAttribute <em>Unknown Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unknown Attribute</em>' containment reference.
	 * @see #getUnknownAttribute()
	 * @generated
	 */
	void setUnknownAttribute(UnknownAttribute value);

	/**
	 * Returns the value of the '<em><b>Record Aggregate</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Aggregate</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Aggregate</em>' containment reference.
	 * @see #setRecordAggregate(RecordAggregate)
	 * @see Ada.AdaPackage#getExpressionClass_RecordAggregate()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='record_aggregate' namespace='##targetNamespace'"
	 * @generated
	 */
	RecordAggregate getRecordAggregate();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getRecordAggregate <em>Record Aggregate</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Record Aggregate</em>' containment reference.
	 * @see #getRecordAggregate()
	 * @generated
	 */
	void setRecordAggregate(RecordAggregate value);

	/**
	 * Returns the value of the '<em><b>Extension Aggregate</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extension Aggregate</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extension Aggregate</em>' containment reference.
	 * @see #setExtensionAggregate(ExtensionAggregate)
	 * @see Ada.AdaPackage#getExpressionClass_ExtensionAggregate()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='extension_aggregate' namespace='##targetNamespace'"
	 * @generated
	 */
	ExtensionAggregate getExtensionAggregate();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getExtensionAggregate <em>Extension Aggregate</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extension Aggregate</em>' containment reference.
	 * @see #getExtensionAggregate()
	 * @generated
	 */
	void setExtensionAggregate(ExtensionAggregate value);

	/**
	 * Returns the value of the '<em><b>Positional Array Aggregate</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Positional Array Aggregate</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Positional Array Aggregate</em>' containment reference.
	 * @see #setPositionalArrayAggregate(PositionalArrayAggregate)
	 * @see Ada.AdaPackage#getExpressionClass_PositionalArrayAggregate()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='positional_array_aggregate' namespace='##targetNamespace'"
	 * @generated
	 */
	PositionalArrayAggregate getPositionalArrayAggregate();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getPositionalArrayAggregate <em>Positional Array Aggregate</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Positional Array Aggregate</em>' containment reference.
	 * @see #getPositionalArrayAggregate()
	 * @generated
	 */
	void setPositionalArrayAggregate(PositionalArrayAggregate value);

	/**
	 * Returns the value of the '<em><b>Named Array Aggregate</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Named Array Aggregate</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Named Array Aggregate</em>' containment reference.
	 * @see #setNamedArrayAggregate(NamedArrayAggregate)
	 * @see Ada.AdaPackage#getExpressionClass_NamedArrayAggregate()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='named_array_aggregate' namespace='##targetNamespace'"
	 * @generated
	 */
	NamedArrayAggregate getNamedArrayAggregate();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getNamedArrayAggregate <em>Named Array Aggregate</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Named Array Aggregate</em>' containment reference.
	 * @see #getNamedArrayAggregate()
	 * @generated
	 */
	void setNamedArrayAggregate(NamedArrayAggregate value);

	/**
	 * Returns the value of the '<em><b>And Then Short Circuit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>And Then Short Circuit</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>And Then Short Circuit</em>' containment reference.
	 * @see #setAndThenShortCircuit(AndThenShortCircuit)
	 * @see Ada.AdaPackage#getExpressionClass_AndThenShortCircuit()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='and_then_short_circuit' namespace='##targetNamespace'"
	 * @generated
	 */
	AndThenShortCircuit getAndThenShortCircuit();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getAndThenShortCircuit <em>And Then Short Circuit</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>And Then Short Circuit</em>' containment reference.
	 * @see #getAndThenShortCircuit()
	 * @generated
	 */
	void setAndThenShortCircuit(AndThenShortCircuit value);

	/**
	 * Returns the value of the '<em><b>Or Else Short Circuit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Or Else Short Circuit</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Or Else Short Circuit</em>' containment reference.
	 * @see #setOrElseShortCircuit(OrElseShortCircuit)
	 * @see Ada.AdaPackage#getExpressionClass_OrElseShortCircuit()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='or_else_short_circuit' namespace='##targetNamespace'"
	 * @generated
	 */
	OrElseShortCircuit getOrElseShortCircuit();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getOrElseShortCircuit <em>Or Else Short Circuit</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Or Else Short Circuit</em>' containment reference.
	 * @see #getOrElseShortCircuit()
	 * @generated
	 */
	void setOrElseShortCircuit(OrElseShortCircuit value);

	/**
	 * Returns the value of the '<em><b>In Membership Test</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In Membership Test</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In Membership Test</em>' containment reference.
	 * @see #setInMembershipTest(InMembershipTest)
	 * @see Ada.AdaPackage#getExpressionClass_InMembershipTest()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='in_membership_test' namespace='##targetNamespace'"
	 * @generated
	 */
	InMembershipTest getInMembershipTest();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getInMembershipTest <em>In Membership Test</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>In Membership Test</em>' containment reference.
	 * @see #getInMembershipTest()
	 * @generated
	 */
	void setInMembershipTest(InMembershipTest value);

	/**
	 * Returns the value of the '<em><b>Not In Membership Test</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not In Membership Test</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not In Membership Test</em>' containment reference.
	 * @see #setNotInMembershipTest(NotInMembershipTest)
	 * @see Ada.AdaPackage#getExpressionClass_NotInMembershipTest()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not_in_membership_test' namespace='##targetNamespace'"
	 * @generated
	 */
	NotInMembershipTest getNotInMembershipTest();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getNotInMembershipTest <em>Not In Membership Test</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not In Membership Test</em>' containment reference.
	 * @see #getNotInMembershipTest()
	 * @generated
	 */
	void setNotInMembershipTest(NotInMembershipTest value);

	/**
	 * Returns the value of the '<em><b>Null Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Null Literal</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Null Literal</em>' containment reference.
	 * @see #setNullLiteral(NullLiteral)
	 * @see Ada.AdaPackage#getExpressionClass_NullLiteral()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='null_literal' namespace='##targetNamespace'"
	 * @generated
	 */
	NullLiteral getNullLiteral();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getNullLiteral <em>Null Literal</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Null Literal</em>' containment reference.
	 * @see #getNullLiteral()
	 * @generated
	 */
	void setNullLiteral(NullLiteral value);

	/**
	 * Returns the value of the '<em><b>Parenthesized Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parenthesized Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parenthesized Expression</em>' containment reference.
	 * @see #setParenthesizedExpression(ParenthesizedExpression)
	 * @see Ada.AdaPackage#getExpressionClass_ParenthesizedExpression()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='parenthesized_expression' namespace='##targetNamespace'"
	 * @generated
	 */
	ParenthesizedExpression getParenthesizedExpression();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getParenthesizedExpression <em>Parenthesized Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parenthesized Expression</em>' containment reference.
	 * @see #getParenthesizedExpression()
	 * @generated
	 */
	void setParenthesizedExpression(ParenthesizedExpression value);

	/**
	 * Returns the value of the '<em><b>Raise Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Raise Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Raise Expression</em>' containment reference.
	 * @see #setRaiseExpression(RaiseExpression)
	 * @see Ada.AdaPackage#getExpressionClass_RaiseExpression()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='raise_expression' namespace='##targetNamespace'"
	 * @generated
	 */
	RaiseExpression getRaiseExpression();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getRaiseExpression <em>Raise Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Raise Expression</em>' containment reference.
	 * @see #getRaiseExpression()
	 * @generated
	 */
	void setRaiseExpression(RaiseExpression value);

	/**
	 * Returns the value of the '<em><b>Type Conversion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Conversion</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Conversion</em>' containment reference.
	 * @see #setTypeConversion(TypeConversion)
	 * @see Ada.AdaPackage#getExpressionClass_TypeConversion()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='type_conversion' namespace='##targetNamespace'"
	 * @generated
	 */
	TypeConversion getTypeConversion();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getTypeConversion <em>Type Conversion</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type Conversion</em>' containment reference.
	 * @see #getTypeConversion()
	 * @generated
	 */
	void setTypeConversion(TypeConversion value);

	/**
	 * Returns the value of the '<em><b>Qualified Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Qualified Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Qualified Expression</em>' containment reference.
	 * @see #setQualifiedExpression(QualifiedExpression)
	 * @see Ada.AdaPackage#getExpressionClass_QualifiedExpression()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='qualified_expression' namespace='##targetNamespace'"
	 * @generated
	 */
	QualifiedExpression getQualifiedExpression();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getQualifiedExpression <em>Qualified Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Qualified Expression</em>' containment reference.
	 * @see #getQualifiedExpression()
	 * @generated
	 */
	void setQualifiedExpression(QualifiedExpression value);

	/**
	 * Returns the value of the '<em><b>Allocation From Subtype</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Allocation From Subtype</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Allocation From Subtype</em>' containment reference.
	 * @see #setAllocationFromSubtype(AllocationFromSubtype)
	 * @see Ada.AdaPackage#getExpressionClass_AllocationFromSubtype()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='allocation_from_subtype' namespace='##targetNamespace'"
	 * @generated
	 */
	AllocationFromSubtype getAllocationFromSubtype();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getAllocationFromSubtype <em>Allocation From Subtype</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Allocation From Subtype</em>' containment reference.
	 * @see #getAllocationFromSubtype()
	 * @generated
	 */
	void setAllocationFromSubtype(AllocationFromSubtype value);

	/**
	 * Returns the value of the '<em><b>Allocation From Qualified Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Allocation From Qualified Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Allocation From Qualified Expression</em>' containment reference.
	 * @see #setAllocationFromQualifiedExpression(AllocationFromQualifiedExpression)
	 * @see Ada.AdaPackage#getExpressionClass_AllocationFromQualifiedExpression()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='allocation_from_qualified_expression' namespace='##targetNamespace'"
	 * @generated
	 */
	AllocationFromQualifiedExpression getAllocationFromQualifiedExpression();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getAllocationFromQualifiedExpression <em>Allocation From Qualified Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Allocation From Qualified Expression</em>' containment reference.
	 * @see #getAllocationFromQualifiedExpression()
	 * @generated
	 */
	void setAllocationFromQualifiedExpression(AllocationFromQualifiedExpression value);

	/**
	 * Returns the value of the '<em><b>Case Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Case Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Case Expression</em>' containment reference.
	 * @see #setCaseExpression(CaseExpression)
	 * @see Ada.AdaPackage#getExpressionClass_CaseExpression()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='case_expression' namespace='##targetNamespace'"
	 * @generated
	 */
	CaseExpression getCaseExpression();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getCaseExpression <em>Case Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Case Expression</em>' containment reference.
	 * @see #getCaseExpression()
	 * @generated
	 */
	void setCaseExpression(CaseExpression value);

	/**
	 * Returns the value of the '<em><b>If Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>If Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>If Expression</em>' containment reference.
	 * @see #setIfExpression(IfExpression)
	 * @see Ada.AdaPackage#getExpressionClass_IfExpression()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='if_expression' namespace='##targetNamespace'"
	 * @generated
	 */
	IfExpression getIfExpression();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getIfExpression <em>If Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>If Expression</em>' containment reference.
	 * @see #getIfExpression()
	 * @generated
	 */
	void setIfExpression(IfExpression value);

	/**
	 * Returns the value of the '<em><b>For All Quantified Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>For All Quantified Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>For All Quantified Expression</em>' containment reference.
	 * @see #setForAllQuantifiedExpression(ForAllQuantifiedExpression)
	 * @see Ada.AdaPackage#getExpressionClass_ForAllQuantifiedExpression()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='for_all_quantified_expression' namespace='##targetNamespace'"
	 * @generated
	 */
	ForAllQuantifiedExpression getForAllQuantifiedExpression();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getForAllQuantifiedExpression <em>For All Quantified Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>For All Quantified Expression</em>' containment reference.
	 * @see #getForAllQuantifiedExpression()
	 * @generated
	 */
	void setForAllQuantifiedExpression(ForAllQuantifiedExpression value);

	/**
	 * Returns the value of the '<em><b>For Some Quantified Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>For Some Quantified Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>For Some Quantified Expression</em>' containment reference.
	 * @see #setForSomeQuantifiedExpression(ForSomeQuantifiedExpression)
	 * @see Ada.AdaPackage#getExpressionClass_ForSomeQuantifiedExpression()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='for_some_quantified_expression' namespace='##targetNamespace'"
	 * @generated
	 */
	ForSomeQuantifiedExpression getForSomeQuantifiedExpression();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getForSomeQuantifiedExpression <em>For Some Quantified Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>For Some Quantified Expression</em>' containment reference.
	 * @see #getForSomeQuantifiedExpression()
	 * @generated
	 */
	void setForSomeQuantifiedExpression(ForSomeQuantifiedExpression value);

	/**
	 * Returns the value of the '<em><b>Comment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comment</em>' containment reference.
	 * @see #setComment(Comment)
	 * @see Ada.AdaPackage#getExpressionClass_Comment()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='comment' namespace='##targetNamespace'"
	 * @generated
	 */
	Comment getComment();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getComment <em>Comment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comment</em>' containment reference.
	 * @see #getComment()
	 * @generated
	 */
	void setComment(Comment value);

	/**
	 * Returns the value of the '<em><b>All Calls Remote Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Calls Remote Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Calls Remote Pragma</em>' containment reference.
	 * @see #setAllCallsRemotePragma(AllCallsRemotePragma)
	 * @see Ada.AdaPackage#getExpressionClass_AllCallsRemotePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='all_calls_remote_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AllCallsRemotePragma getAllCallsRemotePragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>All Calls Remote Pragma</em>' containment reference.
	 * @see #getAllCallsRemotePragma()
	 * @generated
	 */
	void setAllCallsRemotePragma(AllCallsRemotePragma value);

	/**
	 * Returns the value of the '<em><b>Asynchronous Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asynchronous Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asynchronous Pragma</em>' containment reference.
	 * @see #setAsynchronousPragma(AsynchronousPragma)
	 * @see Ada.AdaPackage#getExpressionClass_AsynchronousPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='asynchronous_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AsynchronousPragma getAsynchronousPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getAsynchronousPragma <em>Asynchronous Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Asynchronous Pragma</em>' containment reference.
	 * @see #getAsynchronousPragma()
	 * @generated
	 */
	void setAsynchronousPragma(AsynchronousPragma value);

	/**
	 * Returns the value of the '<em><b>Atomic Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Pragma</em>' containment reference.
	 * @see #setAtomicPragma(AtomicPragma)
	 * @see Ada.AdaPackage#getExpressionClass_AtomicPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='atomic_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AtomicPragma getAtomicPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getAtomicPragma <em>Atomic Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Atomic Pragma</em>' containment reference.
	 * @see #getAtomicPragma()
	 * @generated
	 */
	void setAtomicPragma(AtomicPragma value);

	/**
	 * Returns the value of the '<em><b>Atomic Components Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Components Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Components Pragma</em>' containment reference.
	 * @see #setAtomicComponentsPragma(AtomicComponentsPragma)
	 * @see Ada.AdaPackage#getExpressionClass_AtomicComponentsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='atomic_components_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AtomicComponentsPragma getAtomicComponentsPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Atomic Components Pragma</em>' containment reference.
	 * @see #getAtomicComponentsPragma()
	 * @generated
	 */
	void setAtomicComponentsPragma(AtomicComponentsPragma value);

	/**
	 * Returns the value of the '<em><b>Attach Handler Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attach Handler Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attach Handler Pragma</em>' containment reference.
	 * @see #setAttachHandlerPragma(AttachHandlerPragma)
	 * @see Ada.AdaPackage#getExpressionClass_AttachHandlerPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='attach_handler_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AttachHandlerPragma getAttachHandlerPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getAttachHandlerPragma <em>Attach Handler Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attach Handler Pragma</em>' containment reference.
	 * @see #getAttachHandlerPragma()
	 * @generated
	 */
	void setAttachHandlerPragma(AttachHandlerPragma value);

	/**
	 * Returns the value of the '<em><b>Controlled Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controlled Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controlled Pragma</em>' containment reference.
	 * @see #setControlledPragma(ControlledPragma)
	 * @see Ada.AdaPackage#getExpressionClass_ControlledPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='controlled_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ControlledPragma getControlledPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getControlledPragma <em>Controlled Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Controlled Pragma</em>' containment reference.
	 * @see #getControlledPragma()
	 * @generated
	 */
	void setControlledPragma(ControlledPragma value);

	/**
	 * Returns the value of the '<em><b>Convention Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Convention Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Convention Pragma</em>' containment reference.
	 * @see #setConventionPragma(ConventionPragma)
	 * @see Ada.AdaPackage#getExpressionClass_ConventionPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='convention_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ConventionPragma getConventionPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getConventionPragma <em>Convention Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Convention Pragma</em>' containment reference.
	 * @see #getConventionPragma()
	 * @generated
	 */
	void setConventionPragma(ConventionPragma value);

	/**
	 * Returns the value of the '<em><b>Discard Names Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discard Names Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discard Names Pragma</em>' containment reference.
	 * @see #setDiscardNamesPragma(DiscardNamesPragma)
	 * @see Ada.AdaPackage#getExpressionClass_DiscardNamesPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='discard_names_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscardNamesPragma getDiscardNamesPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getDiscardNamesPragma <em>Discard Names Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discard Names Pragma</em>' containment reference.
	 * @see #getDiscardNamesPragma()
	 * @generated
	 */
	void setDiscardNamesPragma(DiscardNamesPragma value);

	/**
	 * Returns the value of the '<em><b>Elaborate Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Pragma</em>' containment reference.
	 * @see #setElaboratePragma(ElaboratePragma)
	 * @see Ada.AdaPackage#getExpressionClass_ElaboratePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='elaborate_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ElaboratePragma getElaboratePragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getElaboratePragma <em>Elaborate Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Elaborate Pragma</em>' containment reference.
	 * @see #getElaboratePragma()
	 * @generated
	 */
	void setElaboratePragma(ElaboratePragma value);

	/**
	 * Returns the value of the '<em><b>Elaborate All Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate All Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate All Pragma</em>' containment reference.
	 * @see #setElaborateAllPragma(ElaborateAllPragma)
	 * @see Ada.AdaPackage#getExpressionClass_ElaborateAllPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='elaborate_all_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ElaborateAllPragma getElaborateAllPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getElaborateAllPragma <em>Elaborate All Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Elaborate All Pragma</em>' containment reference.
	 * @see #getElaborateAllPragma()
	 * @generated
	 */
	void setElaborateAllPragma(ElaborateAllPragma value);

	/**
	 * Returns the value of the '<em><b>Elaborate Body Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Body Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Body Pragma</em>' containment reference.
	 * @see #setElaborateBodyPragma(ElaborateBodyPragma)
	 * @see Ada.AdaPackage#getExpressionClass_ElaborateBodyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='elaborate_body_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ElaborateBodyPragma getElaborateBodyPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Elaborate Body Pragma</em>' containment reference.
	 * @see #getElaborateBodyPragma()
	 * @generated
	 */
	void setElaborateBodyPragma(ElaborateBodyPragma value);

	/**
	 * Returns the value of the '<em><b>Export Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Export Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Export Pragma</em>' containment reference.
	 * @see #setExportPragma(ExportPragma)
	 * @see Ada.AdaPackage#getExpressionClass_ExportPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='export_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ExportPragma getExportPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getExportPragma <em>Export Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Export Pragma</em>' containment reference.
	 * @see #getExportPragma()
	 * @generated
	 */
	void setExportPragma(ExportPragma value);

	/**
	 * Returns the value of the '<em><b>Import Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Import Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Import Pragma</em>' containment reference.
	 * @see #setImportPragma(ImportPragma)
	 * @see Ada.AdaPackage#getExpressionClass_ImportPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='import_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ImportPragma getImportPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getImportPragma <em>Import Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Import Pragma</em>' containment reference.
	 * @see #getImportPragma()
	 * @generated
	 */
	void setImportPragma(ImportPragma value);

	/**
	 * Returns the value of the '<em><b>Inline Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inline Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inline Pragma</em>' containment reference.
	 * @see #setInlinePragma(InlinePragma)
	 * @see Ada.AdaPackage#getExpressionClass_InlinePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='inline_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	InlinePragma getInlinePragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getInlinePragma <em>Inline Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inline Pragma</em>' containment reference.
	 * @see #getInlinePragma()
	 * @generated
	 */
	void setInlinePragma(InlinePragma value);

	/**
	 * Returns the value of the '<em><b>Inspection Point Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inspection Point Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inspection Point Pragma</em>' containment reference.
	 * @see #setInspectionPointPragma(InspectionPointPragma)
	 * @see Ada.AdaPackage#getExpressionClass_InspectionPointPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='inspection_point_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	InspectionPointPragma getInspectionPointPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getInspectionPointPragma <em>Inspection Point Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inspection Point Pragma</em>' containment reference.
	 * @see #getInspectionPointPragma()
	 * @generated
	 */
	void setInspectionPointPragma(InspectionPointPragma value);

	/**
	 * Returns the value of the '<em><b>Interrupt Handler Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Handler Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Handler Pragma</em>' containment reference.
	 * @see #setInterruptHandlerPragma(InterruptHandlerPragma)
	 * @see Ada.AdaPackage#getExpressionClass_InterruptHandlerPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='interrupt_handler_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	InterruptHandlerPragma getInterruptHandlerPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interrupt Handler Pragma</em>' containment reference.
	 * @see #getInterruptHandlerPragma()
	 * @generated
	 */
	void setInterruptHandlerPragma(InterruptHandlerPragma value);

	/**
	 * Returns the value of the '<em><b>Interrupt Priority Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Priority Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Priority Pragma</em>' containment reference.
	 * @see #setInterruptPriorityPragma(InterruptPriorityPragma)
	 * @see Ada.AdaPackage#getExpressionClass_InterruptPriorityPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='interrupt_priority_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	InterruptPriorityPragma getInterruptPriorityPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interrupt Priority Pragma</em>' containment reference.
	 * @see #getInterruptPriorityPragma()
	 * @generated
	 */
	void setInterruptPriorityPragma(InterruptPriorityPragma value);

	/**
	 * Returns the value of the '<em><b>Linker Options Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Linker Options Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Linker Options Pragma</em>' containment reference.
	 * @see #setLinkerOptionsPragma(LinkerOptionsPragma)
	 * @see Ada.AdaPackage#getExpressionClass_LinkerOptionsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='linker_options_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	LinkerOptionsPragma getLinkerOptionsPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getLinkerOptionsPragma <em>Linker Options Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Linker Options Pragma</em>' containment reference.
	 * @see #getLinkerOptionsPragma()
	 * @generated
	 */
	void setLinkerOptionsPragma(LinkerOptionsPragma value);

	/**
	 * Returns the value of the '<em><b>List Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Pragma</em>' containment reference.
	 * @see #setListPragma(ListPragma)
	 * @see Ada.AdaPackage#getExpressionClass_ListPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='list_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ListPragma getListPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getListPragma <em>List Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>List Pragma</em>' containment reference.
	 * @see #getListPragma()
	 * @generated
	 */
	void setListPragma(ListPragma value);

	/**
	 * Returns the value of the '<em><b>Locking Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Locking Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Locking Policy Pragma</em>' containment reference.
	 * @see #setLockingPolicyPragma(LockingPolicyPragma)
	 * @see Ada.AdaPackage#getExpressionClass_LockingPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='locking_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	LockingPolicyPragma getLockingPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getLockingPolicyPragma <em>Locking Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Locking Policy Pragma</em>' containment reference.
	 * @see #getLockingPolicyPragma()
	 * @generated
	 */
	void setLockingPolicyPragma(LockingPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Normalize Scalars Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Normalize Scalars Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Normalize Scalars Pragma</em>' containment reference.
	 * @see #setNormalizeScalarsPragma(NormalizeScalarsPragma)
	 * @see Ada.AdaPackage#getExpressionClass_NormalizeScalarsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='normalize_scalars_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	NormalizeScalarsPragma getNormalizeScalarsPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Normalize Scalars Pragma</em>' containment reference.
	 * @see #getNormalizeScalarsPragma()
	 * @generated
	 */
	void setNormalizeScalarsPragma(NormalizeScalarsPragma value);

	/**
	 * Returns the value of the '<em><b>Optimize Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Optimize Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Optimize Pragma</em>' containment reference.
	 * @see #setOptimizePragma(OptimizePragma)
	 * @see Ada.AdaPackage#getExpressionClass_OptimizePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='optimize_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	OptimizePragma getOptimizePragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getOptimizePragma <em>Optimize Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Optimize Pragma</em>' containment reference.
	 * @see #getOptimizePragma()
	 * @generated
	 */
	void setOptimizePragma(OptimizePragma value);

	/**
	 * Returns the value of the '<em><b>Pack Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pack Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pack Pragma</em>' containment reference.
	 * @see #setPackPragma(PackPragma)
	 * @see Ada.AdaPackage#getExpressionClass_PackPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='pack_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PackPragma getPackPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getPackPragma <em>Pack Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pack Pragma</em>' containment reference.
	 * @see #getPackPragma()
	 * @generated
	 */
	void setPackPragma(PackPragma value);

	/**
	 * Returns the value of the '<em><b>Page Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Page Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Page Pragma</em>' containment reference.
	 * @see #setPagePragma(PagePragma)
	 * @see Ada.AdaPackage#getExpressionClass_PagePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='page_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PagePragma getPagePragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getPagePragma <em>Page Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Page Pragma</em>' containment reference.
	 * @see #getPagePragma()
	 * @generated
	 */
	void setPagePragma(PagePragma value);

	/**
	 * Returns the value of the '<em><b>Preelaborate Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborate Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborate Pragma</em>' containment reference.
	 * @see #setPreelaboratePragma(PreelaboratePragma)
	 * @see Ada.AdaPackage#getExpressionClass_PreelaboratePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='preelaborate_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PreelaboratePragma getPreelaboratePragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getPreelaboratePragma <em>Preelaborate Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preelaborate Pragma</em>' containment reference.
	 * @see #getPreelaboratePragma()
	 * @generated
	 */
	void setPreelaboratePragma(PreelaboratePragma value);

	/**
	 * Returns the value of the '<em><b>Priority Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Pragma</em>' containment reference.
	 * @see #setPriorityPragma(PriorityPragma)
	 * @see Ada.AdaPackage#getExpressionClass_PriorityPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='priority_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PriorityPragma getPriorityPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getPriorityPragma <em>Priority Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Priority Pragma</em>' containment reference.
	 * @see #getPriorityPragma()
	 * @generated
	 */
	void setPriorityPragma(PriorityPragma value);

	/**
	 * Returns the value of the '<em><b>Pure Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pure Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pure Pragma</em>' containment reference.
	 * @see #setPurePragma(PurePragma)
	 * @see Ada.AdaPackage#getExpressionClass_PurePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='pure_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PurePragma getPurePragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getPurePragma <em>Pure Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pure Pragma</em>' containment reference.
	 * @see #getPurePragma()
	 * @generated
	 */
	void setPurePragma(PurePragma value);

	/**
	 * Returns the value of the '<em><b>Queuing Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Queuing Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Queuing Policy Pragma</em>' containment reference.
	 * @see #setQueuingPolicyPragma(QueuingPolicyPragma)
	 * @see Ada.AdaPackage#getExpressionClass_QueuingPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='queuing_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	QueuingPolicyPragma getQueuingPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Queuing Policy Pragma</em>' containment reference.
	 * @see #getQueuingPolicyPragma()
	 * @generated
	 */
	void setQueuingPolicyPragma(QueuingPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Remote Call Interface Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Call Interface Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Call Interface Pragma</em>' containment reference.
	 * @see #setRemoteCallInterfacePragma(RemoteCallInterfacePragma)
	 * @see Ada.AdaPackage#getExpressionClass_RemoteCallInterfacePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='remote_call_interface_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	RemoteCallInterfacePragma getRemoteCallInterfacePragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Remote Call Interface Pragma</em>' containment reference.
	 * @see #getRemoteCallInterfacePragma()
	 * @generated
	 */
	void setRemoteCallInterfacePragma(RemoteCallInterfacePragma value);

	/**
	 * Returns the value of the '<em><b>Remote Types Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Types Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Types Pragma</em>' containment reference.
	 * @see #setRemoteTypesPragma(RemoteTypesPragma)
	 * @see Ada.AdaPackage#getExpressionClass_RemoteTypesPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='remote_types_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	RemoteTypesPragma getRemoteTypesPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getRemoteTypesPragma <em>Remote Types Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Remote Types Pragma</em>' containment reference.
	 * @see #getRemoteTypesPragma()
	 * @generated
	 */
	void setRemoteTypesPragma(RemoteTypesPragma value);

	/**
	 * Returns the value of the '<em><b>Restrictions Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Restrictions Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Restrictions Pragma</em>' containment reference.
	 * @see #setRestrictionsPragma(RestrictionsPragma)
	 * @see Ada.AdaPackage#getExpressionClass_RestrictionsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='restrictions_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	RestrictionsPragma getRestrictionsPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getRestrictionsPragma <em>Restrictions Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Restrictions Pragma</em>' containment reference.
	 * @see #getRestrictionsPragma()
	 * @generated
	 */
	void setRestrictionsPragma(RestrictionsPragma value);

	/**
	 * Returns the value of the '<em><b>Reviewable Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reviewable Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reviewable Pragma</em>' containment reference.
	 * @see #setReviewablePragma(ReviewablePragma)
	 * @see Ada.AdaPackage#getExpressionClass_ReviewablePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='reviewable_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ReviewablePragma getReviewablePragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getReviewablePragma <em>Reviewable Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reviewable Pragma</em>' containment reference.
	 * @see #getReviewablePragma()
	 * @generated
	 */
	void setReviewablePragma(ReviewablePragma value);

	/**
	 * Returns the value of the '<em><b>Shared Passive Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shared Passive Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shared Passive Pragma</em>' containment reference.
	 * @see #setSharedPassivePragma(SharedPassivePragma)
	 * @see Ada.AdaPackage#getExpressionClass_SharedPassivePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='shared_passive_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	SharedPassivePragma getSharedPassivePragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getSharedPassivePragma <em>Shared Passive Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Shared Passive Pragma</em>' containment reference.
	 * @see #getSharedPassivePragma()
	 * @generated
	 */
	void setSharedPassivePragma(SharedPassivePragma value);

	/**
	 * Returns the value of the '<em><b>Storage Size Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Size Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Size Pragma</em>' containment reference.
	 * @see #setStorageSizePragma(StorageSizePragma)
	 * @see Ada.AdaPackage#getExpressionClass_StorageSizePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='storage_size_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	StorageSizePragma getStorageSizePragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getStorageSizePragma <em>Storage Size Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Storage Size Pragma</em>' containment reference.
	 * @see #getStorageSizePragma()
	 * @generated
	 */
	void setStorageSizePragma(StorageSizePragma value);

	/**
	 * Returns the value of the '<em><b>Suppress Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Suppress Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Suppress Pragma</em>' containment reference.
	 * @see #setSuppressPragma(SuppressPragma)
	 * @see Ada.AdaPackage#getExpressionClass_SuppressPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='suppress_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	SuppressPragma getSuppressPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getSuppressPragma <em>Suppress Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Suppress Pragma</em>' containment reference.
	 * @see #getSuppressPragma()
	 * @generated
	 */
	void setSuppressPragma(SuppressPragma value);

	/**
	 * Returns the value of the '<em><b>Task Dispatching Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Dispatching Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Dispatching Policy Pragma</em>' containment reference.
	 * @see #setTaskDispatchingPolicyPragma(TaskDispatchingPolicyPragma)
	 * @see Ada.AdaPackage#getExpressionClass_TaskDispatchingPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='task_dispatching_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	TaskDispatchingPolicyPragma getTaskDispatchingPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Dispatching Policy Pragma</em>' containment reference.
	 * @see #getTaskDispatchingPolicyPragma()
	 * @generated
	 */
	void setTaskDispatchingPolicyPragma(TaskDispatchingPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Volatile Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Pragma</em>' containment reference.
	 * @see #setVolatilePragma(VolatilePragma)
	 * @see Ada.AdaPackage#getExpressionClass_VolatilePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='volatile_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	VolatilePragma getVolatilePragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getVolatilePragma <em>Volatile Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Volatile Pragma</em>' containment reference.
	 * @see #getVolatilePragma()
	 * @generated
	 */
	void setVolatilePragma(VolatilePragma value);

	/**
	 * Returns the value of the '<em><b>Volatile Components Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Components Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Components Pragma</em>' containment reference.
	 * @see #setVolatileComponentsPragma(VolatileComponentsPragma)
	 * @see Ada.AdaPackage#getExpressionClass_VolatileComponentsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='volatile_components_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	VolatileComponentsPragma getVolatileComponentsPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Volatile Components Pragma</em>' containment reference.
	 * @see #getVolatileComponentsPragma()
	 * @generated
	 */
	void setVolatileComponentsPragma(VolatileComponentsPragma value);

	/**
	 * Returns the value of the '<em><b>Assert Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assert Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assert Pragma</em>' containment reference.
	 * @see #setAssertPragma(AssertPragma)
	 * @see Ada.AdaPackage#getExpressionClass_AssertPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='assert_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AssertPragma getAssertPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getAssertPragma <em>Assert Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assert Pragma</em>' containment reference.
	 * @see #getAssertPragma()
	 * @generated
	 */
	void setAssertPragma(AssertPragma value);

	/**
	 * Returns the value of the '<em><b>Assertion Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assertion Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assertion Policy Pragma</em>' containment reference.
	 * @see #setAssertionPolicyPragma(AssertionPolicyPragma)
	 * @see Ada.AdaPackage#getExpressionClass_AssertionPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='assertion_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AssertionPolicyPragma getAssertionPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assertion Policy Pragma</em>' containment reference.
	 * @see #getAssertionPolicyPragma()
	 * @generated
	 */
	void setAssertionPolicyPragma(AssertionPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Detect Blocking Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Detect Blocking Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Detect Blocking Pragma</em>' containment reference.
	 * @see #setDetectBlockingPragma(DetectBlockingPragma)
	 * @see Ada.AdaPackage#getExpressionClass_DetectBlockingPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='detect_blocking_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	DetectBlockingPragma getDetectBlockingPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Detect Blocking Pragma</em>' containment reference.
	 * @see #getDetectBlockingPragma()
	 * @generated
	 */
	void setDetectBlockingPragma(DetectBlockingPragma value);

	/**
	 * Returns the value of the '<em><b>No Return Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>No Return Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>No Return Pragma</em>' containment reference.
	 * @see #setNoReturnPragma(NoReturnPragma)
	 * @see Ada.AdaPackage#getExpressionClass_NoReturnPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='no_return_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	NoReturnPragma getNoReturnPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getNoReturnPragma <em>No Return Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>No Return Pragma</em>' containment reference.
	 * @see #getNoReturnPragma()
	 * @generated
	 */
	void setNoReturnPragma(NoReturnPragma value);

	/**
	 * Returns the value of the '<em><b>Partition Elaboration Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partition Elaboration Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partition Elaboration Policy Pragma</em>' containment reference.
	 * @see #setPartitionElaborationPolicyPragma(PartitionElaborationPolicyPragma)
	 * @see Ada.AdaPackage#getExpressionClass_PartitionElaborationPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='partition_elaboration_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PartitionElaborationPolicyPragma getPartitionElaborationPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Partition Elaboration Policy Pragma</em>' containment reference.
	 * @see #getPartitionElaborationPolicyPragma()
	 * @generated
	 */
	void setPartitionElaborationPolicyPragma(PartitionElaborationPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Preelaborable Initialization Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborable Initialization Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborable Initialization Pragma</em>' containment reference.
	 * @see #setPreelaborableInitializationPragma(PreelaborableInitializationPragma)
	 * @see Ada.AdaPackage#getExpressionClass_PreelaborableInitializationPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='preelaborable_initialization_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PreelaborableInitializationPragma getPreelaborableInitializationPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preelaborable Initialization Pragma</em>' containment reference.
	 * @see #getPreelaborableInitializationPragma()
	 * @generated
	 */
	void setPreelaborableInitializationPragma(PreelaborableInitializationPragma value);

	/**
	 * Returns the value of the '<em><b>Priority Specific Dispatching Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Specific Dispatching Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Specific Dispatching Pragma</em>' containment reference.
	 * @see #setPrioritySpecificDispatchingPragma(PrioritySpecificDispatchingPragma)
	 * @see Ada.AdaPackage#getExpressionClass_PrioritySpecificDispatchingPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='priority_specific_dispatching_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PrioritySpecificDispatchingPragma getPrioritySpecificDispatchingPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Priority Specific Dispatching Pragma</em>' containment reference.
	 * @see #getPrioritySpecificDispatchingPragma()
	 * @generated
	 */
	void setPrioritySpecificDispatchingPragma(PrioritySpecificDispatchingPragma value);

	/**
	 * Returns the value of the '<em><b>Profile Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Profile Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Profile Pragma</em>' containment reference.
	 * @see #setProfilePragma(ProfilePragma)
	 * @see Ada.AdaPackage#getExpressionClass_ProfilePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='profile_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ProfilePragma getProfilePragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getProfilePragma <em>Profile Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Profile Pragma</em>' containment reference.
	 * @see #getProfilePragma()
	 * @generated
	 */
	void setProfilePragma(ProfilePragma value);

	/**
	 * Returns the value of the '<em><b>Relative Deadline Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relative Deadline Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relative Deadline Pragma</em>' containment reference.
	 * @see #setRelativeDeadlinePragma(RelativeDeadlinePragma)
	 * @see Ada.AdaPackage#getExpressionClass_RelativeDeadlinePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='relative_deadline_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	RelativeDeadlinePragma getRelativeDeadlinePragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relative Deadline Pragma</em>' containment reference.
	 * @see #getRelativeDeadlinePragma()
	 * @generated
	 */
	void setRelativeDeadlinePragma(RelativeDeadlinePragma value);

	/**
	 * Returns the value of the '<em><b>Unchecked Union Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unchecked Union Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unchecked Union Pragma</em>' containment reference.
	 * @see #setUncheckedUnionPragma(UncheckedUnionPragma)
	 * @see Ada.AdaPackage#getExpressionClass_UncheckedUnionPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unchecked_union_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	UncheckedUnionPragma getUncheckedUnionPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unchecked Union Pragma</em>' containment reference.
	 * @see #getUncheckedUnionPragma()
	 * @generated
	 */
	void setUncheckedUnionPragma(UncheckedUnionPragma value);

	/**
	 * Returns the value of the '<em><b>Unsuppress Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unsuppress Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unsuppress Pragma</em>' containment reference.
	 * @see #setUnsuppressPragma(UnsuppressPragma)
	 * @see Ada.AdaPackage#getExpressionClass_UnsuppressPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unsuppress_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	UnsuppressPragma getUnsuppressPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getUnsuppressPragma <em>Unsuppress Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unsuppress Pragma</em>' containment reference.
	 * @see #getUnsuppressPragma()
	 * @generated
	 */
	void setUnsuppressPragma(UnsuppressPragma value);

	/**
	 * Returns the value of the '<em><b>Default Storage Pool Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Storage Pool Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Storage Pool Pragma</em>' containment reference.
	 * @see #setDefaultStoragePoolPragma(DefaultStoragePoolPragma)
	 * @see Ada.AdaPackage#getExpressionClass_DefaultStoragePoolPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='default_storage_pool_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	DefaultStoragePoolPragma getDefaultStoragePoolPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Storage Pool Pragma</em>' containment reference.
	 * @see #getDefaultStoragePoolPragma()
	 * @generated
	 */
	void setDefaultStoragePoolPragma(DefaultStoragePoolPragma value);

	/**
	 * Returns the value of the '<em><b>Dispatching Domain Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dispatching Domain Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dispatching Domain Pragma</em>' containment reference.
	 * @see #setDispatchingDomainPragma(DispatchingDomainPragma)
	 * @see Ada.AdaPackage#getExpressionClass_DispatchingDomainPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='dispatching_domain_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	DispatchingDomainPragma getDispatchingDomainPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dispatching Domain Pragma</em>' containment reference.
	 * @see #getDispatchingDomainPragma()
	 * @generated
	 */
	void setDispatchingDomainPragma(DispatchingDomainPragma value);

	/**
	 * Returns the value of the '<em><b>Cpu Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cpu Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cpu Pragma</em>' containment reference.
	 * @see #setCpuPragma(CpuPragma)
	 * @see Ada.AdaPackage#getExpressionClass_CpuPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='cpu_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	CpuPragma getCpuPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getCpuPragma <em>Cpu Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cpu Pragma</em>' containment reference.
	 * @see #getCpuPragma()
	 * @generated
	 */
	void setCpuPragma(CpuPragma value);

	/**
	 * Returns the value of the '<em><b>Independent Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Pragma</em>' containment reference.
	 * @see #setIndependentPragma(IndependentPragma)
	 * @see Ada.AdaPackage#getExpressionClass_IndependentPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='independent_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	IndependentPragma getIndependentPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getIndependentPragma <em>Independent Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Independent Pragma</em>' containment reference.
	 * @see #getIndependentPragma()
	 * @generated
	 */
	void setIndependentPragma(IndependentPragma value);

	/**
	 * Returns the value of the '<em><b>Independent Components Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Components Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Components Pragma</em>' containment reference.
	 * @see #setIndependentComponentsPragma(IndependentComponentsPragma)
	 * @see Ada.AdaPackage#getExpressionClass_IndependentComponentsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='independent_components_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	IndependentComponentsPragma getIndependentComponentsPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getIndependentComponentsPragma <em>Independent Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Independent Components Pragma</em>' containment reference.
	 * @see #getIndependentComponentsPragma()
	 * @generated
	 */
	void setIndependentComponentsPragma(IndependentComponentsPragma value);

	/**
	 * Returns the value of the '<em><b>Implementation Defined Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implementation Defined Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implementation Defined Pragma</em>' containment reference.
	 * @see #setImplementationDefinedPragma(ImplementationDefinedPragma)
	 * @see Ada.AdaPackage#getExpressionClass_ImplementationDefinedPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='implementation_defined_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ImplementationDefinedPragma getImplementationDefinedPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Implementation Defined Pragma</em>' containment reference.
	 * @see #getImplementationDefinedPragma()
	 * @generated
	 */
	void setImplementationDefinedPragma(ImplementationDefinedPragma value);

	/**
	 * Returns the value of the '<em><b>Unknown Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unknown Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unknown Pragma</em>' containment reference.
	 * @see #setUnknownPragma(UnknownPragma)
	 * @see Ada.AdaPackage#getExpressionClass_UnknownPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unknown_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	UnknownPragma getUnknownPragma();

	/**
	 * Sets the value of the '{@link Ada.ExpressionClass#getUnknownPragma <em>Unknown Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unknown Pragma</em>' containment reference.
	 * @see #getUnknownPragma()
	 * @generated
	 */
	void setUnknownPragma(UnknownPragma value);

} // ExpressionClass
