/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.AssociationList;
import Ada.ExpressionClass;
import Ada.FunctionCall;
import Ada.IsPrefixCallQType;
import Ada.IsPrefixNotationQType;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Function Call</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.FunctionCallImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.FunctionCallImpl#getPrefixQ <em>Prefix Q</em>}</li>
 *   <li>{@link Ada.impl.FunctionCallImpl#getFunctionCallParametersQl <em>Function Call Parameters Ql</em>}</li>
 *   <li>{@link Ada.impl.FunctionCallImpl#getIsPrefixCallQ <em>Is Prefix Call Q</em>}</li>
 *   <li>{@link Ada.impl.FunctionCallImpl#getIsPrefixNotationQ <em>Is Prefix Notation Q</em>}</li>
 *   <li>{@link Ada.impl.FunctionCallImpl#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.impl.FunctionCallImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FunctionCallImpl extends MinimalEObjectImpl.Container implements FunctionCall {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getPrefixQ() <em>Prefix Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrefixQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass prefixQ;

	/**
	 * The cached value of the '{@link #getFunctionCallParametersQl() <em>Function Call Parameters Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunctionCallParametersQl()
	 * @generated
	 * @ordered
	 */
	protected AssociationList functionCallParametersQl;

	/**
	 * The cached value of the '{@link #getIsPrefixCallQ() <em>Is Prefix Call Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsPrefixCallQ()
	 * @generated
	 * @ordered
	 */
	protected IsPrefixCallQType isPrefixCallQ;

	/**
	 * The cached value of the '{@link #getIsPrefixNotationQ() <em>Is Prefix Notation Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsPrefixNotationQ()
	 * @generated
	 * @ordered
	 */
	protected IsPrefixNotationQType isPrefixNotationQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FunctionCallImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getFunctionCall();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FUNCTION_CALL__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FUNCTION_CALL__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FUNCTION_CALL__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FUNCTION_CALL__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getPrefixQ() {
		return prefixQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrefixQ(ExpressionClass newPrefixQ, NotificationChain msgs) {
		ExpressionClass oldPrefixQ = prefixQ;
		prefixQ = newPrefixQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FUNCTION_CALL__PREFIX_Q, oldPrefixQ, newPrefixQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrefixQ(ExpressionClass newPrefixQ) {
		if (newPrefixQ != prefixQ) {
			NotificationChain msgs = null;
			if (prefixQ != null)
				msgs = ((InternalEObject)prefixQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FUNCTION_CALL__PREFIX_Q, null, msgs);
			if (newPrefixQ != null)
				msgs = ((InternalEObject)newPrefixQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FUNCTION_CALL__PREFIX_Q, null, msgs);
			msgs = basicSetPrefixQ(newPrefixQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FUNCTION_CALL__PREFIX_Q, newPrefixQ, newPrefixQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationList getFunctionCallParametersQl() {
		return functionCallParametersQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFunctionCallParametersQl(AssociationList newFunctionCallParametersQl, NotificationChain msgs) {
		AssociationList oldFunctionCallParametersQl = functionCallParametersQl;
		functionCallParametersQl = newFunctionCallParametersQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FUNCTION_CALL__FUNCTION_CALL_PARAMETERS_QL, oldFunctionCallParametersQl, newFunctionCallParametersQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFunctionCallParametersQl(AssociationList newFunctionCallParametersQl) {
		if (newFunctionCallParametersQl != functionCallParametersQl) {
			NotificationChain msgs = null;
			if (functionCallParametersQl != null)
				msgs = ((InternalEObject)functionCallParametersQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FUNCTION_CALL__FUNCTION_CALL_PARAMETERS_QL, null, msgs);
			if (newFunctionCallParametersQl != null)
				msgs = ((InternalEObject)newFunctionCallParametersQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FUNCTION_CALL__FUNCTION_CALL_PARAMETERS_QL, null, msgs);
			msgs = basicSetFunctionCallParametersQl(newFunctionCallParametersQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FUNCTION_CALL__FUNCTION_CALL_PARAMETERS_QL, newFunctionCallParametersQl, newFunctionCallParametersQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsPrefixCallQType getIsPrefixCallQ() {
		return isPrefixCallQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIsPrefixCallQ(IsPrefixCallQType newIsPrefixCallQ, NotificationChain msgs) {
		IsPrefixCallQType oldIsPrefixCallQ = isPrefixCallQ;
		isPrefixCallQ = newIsPrefixCallQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FUNCTION_CALL__IS_PREFIX_CALL_Q, oldIsPrefixCallQ, newIsPrefixCallQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsPrefixCallQ(IsPrefixCallQType newIsPrefixCallQ) {
		if (newIsPrefixCallQ != isPrefixCallQ) {
			NotificationChain msgs = null;
			if (isPrefixCallQ != null)
				msgs = ((InternalEObject)isPrefixCallQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FUNCTION_CALL__IS_PREFIX_CALL_Q, null, msgs);
			if (newIsPrefixCallQ != null)
				msgs = ((InternalEObject)newIsPrefixCallQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FUNCTION_CALL__IS_PREFIX_CALL_Q, null, msgs);
			msgs = basicSetIsPrefixCallQ(newIsPrefixCallQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FUNCTION_CALL__IS_PREFIX_CALL_Q, newIsPrefixCallQ, newIsPrefixCallQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsPrefixNotationQType getIsPrefixNotationQ() {
		return isPrefixNotationQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIsPrefixNotationQ(IsPrefixNotationQType newIsPrefixNotationQ, NotificationChain msgs) {
		IsPrefixNotationQType oldIsPrefixNotationQ = isPrefixNotationQ;
		isPrefixNotationQ = newIsPrefixNotationQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FUNCTION_CALL__IS_PREFIX_NOTATION_Q, oldIsPrefixNotationQ, newIsPrefixNotationQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsPrefixNotationQ(IsPrefixNotationQType newIsPrefixNotationQ) {
		if (newIsPrefixNotationQ != isPrefixNotationQ) {
			NotificationChain msgs = null;
			if (isPrefixNotationQ != null)
				msgs = ((InternalEObject)isPrefixNotationQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FUNCTION_CALL__IS_PREFIX_NOTATION_Q, null, msgs);
			if (newIsPrefixNotationQ != null)
				msgs = ((InternalEObject)newIsPrefixNotationQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FUNCTION_CALL__IS_PREFIX_NOTATION_Q, null, msgs);
			msgs = basicSetIsPrefixNotationQ(newIsPrefixNotationQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FUNCTION_CALL__IS_PREFIX_NOTATION_Q, newIsPrefixNotationQ, newIsPrefixNotationQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FUNCTION_CALL__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FUNCTION_CALL__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.FUNCTION_CALL__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.FUNCTION_CALL__PREFIX_Q:
				return basicSetPrefixQ(null, msgs);
			case AdaPackage.FUNCTION_CALL__FUNCTION_CALL_PARAMETERS_QL:
				return basicSetFunctionCallParametersQl(null, msgs);
			case AdaPackage.FUNCTION_CALL__IS_PREFIX_CALL_Q:
				return basicSetIsPrefixCallQ(null, msgs);
			case AdaPackage.FUNCTION_CALL__IS_PREFIX_NOTATION_Q:
				return basicSetIsPrefixNotationQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.FUNCTION_CALL__SLOC:
				return getSloc();
			case AdaPackage.FUNCTION_CALL__PREFIX_Q:
				return getPrefixQ();
			case AdaPackage.FUNCTION_CALL__FUNCTION_CALL_PARAMETERS_QL:
				return getFunctionCallParametersQl();
			case AdaPackage.FUNCTION_CALL__IS_PREFIX_CALL_Q:
				return getIsPrefixCallQ();
			case AdaPackage.FUNCTION_CALL__IS_PREFIX_NOTATION_Q:
				return getIsPrefixNotationQ();
			case AdaPackage.FUNCTION_CALL__CHECKS:
				return getChecks();
			case AdaPackage.FUNCTION_CALL__TYPE:
				return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.FUNCTION_CALL__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.FUNCTION_CALL__PREFIX_Q:
				setPrefixQ((ExpressionClass)newValue);
				return;
			case AdaPackage.FUNCTION_CALL__FUNCTION_CALL_PARAMETERS_QL:
				setFunctionCallParametersQl((AssociationList)newValue);
				return;
			case AdaPackage.FUNCTION_CALL__IS_PREFIX_CALL_Q:
				setIsPrefixCallQ((IsPrefixCallQType)newValue);
				return;
			case AdaPackage.FUNCTION_CALL__IS_PREFIX_NOTATION_Q:
				setIsPrefixNotationQ((IsPrefixNotationQType)newValue);
				return;
			case AdaPackage.FUNCTION_CALL__CHECKS:
				setChecks((String)newValue);
				return;
			case AdaPackage.FUNCTION_CALL__TYPE:
				setType((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.FUNCTION_CALL__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.FUNCTION_CALL__PREFIX_Q:
				setPrefixQ((ExpressionClass)null);
				return;
			case AdaPackage.FUNCTION_CALL__FUNCTION_CALL_PARAMETERS_QL:
				setFunctionCallParametersQl((AssociationList)null);
				return;
			case AdaPackage.FUNCTION_CALL__IS_PREFIX_CALL_Q:
				setIsPrefixCallQ((IsPrefixCallQType)null);
				return;
			case AdaPackage.FUNCTION_CALL__IS_PREFIX_NOTATION_Q:
				setIsPrefixNotationQ((IsPrefixNotationQType)null);
				return;
			case AdaPackage.FUNCTION_CALL__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
			case AdaPackage.FUNCTION_CALL__TYPE:
				setType(TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.FUNCTION_CALL__SLOC:
				return sloc != null;
			case AdaPackage.FUNCTION_CALL__PREFIX_Q:
				return prefixQ != null;
			case AdaPackage.FUNCTION_CALL__FUNCTION_CALL_PARAMETERS_QL:
				return functionCallParametersQl != null;
			case AdaPackage.FUNCTION_CALL__IS_PREFIX_CALL_Q:
				return isPrefixCallQ != null;
			case AdaPackage.FUNCTION_CALL__IS_PREFIX_NOTATION_Q:
				return isPrefixNotationQ != null;
			case AdaPackage.FUNCTION_CALL__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
			case AdaPackage.FUNCTION_CALL__TYPE:
				return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(", type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //FunctionCallImpl
