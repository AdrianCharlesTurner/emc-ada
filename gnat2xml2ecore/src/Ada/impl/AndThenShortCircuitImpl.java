/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.AndThenShortCircuit;
import Ada.ExpressionClass;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>And Then Short Circuit</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.AndThenShortCircuitImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.AndThenShortCircuitImpl#getShortCircuitOperationLeftExpressionQ <em>Short Circuit Operation Left Expression Q</em>}</li>
 *   <li>{@link Ada.impl.AndThenShortCircuitImpl#getShortCircuitOperationRightExpressionQ <em>Short Circuit Operation Right Expression Q</em>}</li>
 *   <li>{@link Ada.impl.AndThenShortCircuitImpl#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.impl.AndThenShortCircuitImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AndThenShortCircuitImpl extends MinimalEObjectImpl.Container implements AndThenShortCircuit {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getShortCircuitOperationLeftExpressionQ() <em>Short Circuit Operation Left Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShortCircuitOperationLeftExpressionQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass shortCircuitOperationLeftExpressionQ;

	/**
	 * The cached value of the '{@link #getShortCircuitOperationRightExpressionQ() <em>Short Circuit Operation Right Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShortCircuitOperationRightExpressionQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass shortCircuitOperationRightExpressionQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AndThenShortCircuitImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getAndThenShortCircuit();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.AND_THEN_SHORT_CIRCUIT__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.AND_THEN_SHORT_CIRCUIT__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.AND_THEN_SHORT_CIRCUIT__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.AND_THEN_SHORT_CIRCUIT__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getShortCircuitOperationLeftExpressionQ() {
		return shortCircuitOperationLeftExpressionQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShortCircuitOperationLeftExpressionQ(ExpressionClass newShortCircuitOperationLeftExpressionQ, NotificationChain msgs) {
		ExpressionClass oldShortCircuitOperationLeftExpressionQ = shortCircuitOperationLeftExpressionQ;
		shortCircuitOperationLeftExpressionQ = newShortCircuitOperationLeftExpressionQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.AND_THEN_SHORT_CIRCUIT__SHORT_CIRCUIT_OPERATION_LEFT_EXPRESSION_Q, oldShortCircuitOperationLeftExpressionQ, newShortCircuitOperationLeftExpressionQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShortCircuitOperationLeftExpressionQ(ExpressionClass newShortCircuitOperationLeftExpressionQ) {
		if (newShortCircuitOperationLeftExpressionQ != shortCircuitOperationLeftExpressionQ) {
			NotificationChain msgs = null;
			if (shortCircuitOperationLeftExpressionQ != null)
				msgs = ((InternalEObject)shortCircuitOperationLeftExpressionQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.AND_THEN_SHORT_CIRCUIT__SHORT_CIRCUIT_OPERATION_LEFT_EXPRESSION_Q, null, msgs);
			if (newShortCircuitOperationLeftExpressionQ != null)
				msgs = ((InternalEObject)newShortCircuitOperationLeftExpressionQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.AND_THEN_SHORT_CIRCUIT__SHORT_CIRCUIT_OPERATION_LEFT_EXPRESSION_Q, null, msgs);
			msgs = basicSetShortCircuitOperationLeftExpressionQ(newShortCircuitOperationLeftExpressionQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.AND_THEN_SHORT_CIRCUIT__SHORT_CIRCUIT_OPERATION_LEFT_EXPRESSION_Q, newShortCircuitOperationLeftExpressionQ, newShortCircuitOperationLeftExpressionQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getShortCircuitOperationRightExpressionQ() {
		return shortCircuitOperationRightExpressionQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetShortCircuitOperationRightExpressionQ(ExpressionClass newShortCircuitOperationRightExpressionQ, NotificationChain msgs) {
		ExpressionClass oldShortCircuitOperationRightExpressionQ = shortCircuitOperationRightExpressionQ;
		shortCircuitOperationRightExpressionQ = newShortCircuitOperationRightExpressionQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.AND_THEN_SHORT_CIRCUIT__SHORT_CIRCUIT_OPERATION_RIGHT_EXPRESSION_Q, oldShortCircuitOperationRightExpressionQ, newShortCircuitOperationRightExpressionQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShortCircuitOperationRightExpressionQ(ExpressionClass newShortCircuitOperationRightExpressionQ) {
		if (newShortCircuitOperationRightExpressionQ != shortCircuitOperationRightExpressionQ) {
			NotificationChain msgs = null;
			if (shortCircuitOperationRightExpressionQ != null)
				msgs = ((InternalEObject)shortCircuitOperationRightExpressionQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.AND_THEN_SHORT_CIRCUIT__SHORT_CIRCUIT_OPERATION_RIGHT_EXPRESSION_Q, null, msgs);
			if (newShortCircuitOperationRightExpressionQ != null)
				msgs = ((InternalEObject)newShortCircuitOperationRightExpressionQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.AND_THEN_SHORT_CIRCUIT__SHORT_CIRCUIT_OPERATION_RIGHT_EXPRESSION_Q, null, msgs);
			msgs = basicSetShortCircuitOperationRightExpressionQ(newShortCircuitOperationRightExpressionQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.AND_THEN_SHORT_CIRCUIT__SHORT_CIRCUIT_OPERATION_RIGHT_EXPRESSION_Q, newShortCircuitOperationRightExpressionQ, newShortCircuitOperationRightExpressionQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.AND_THEN_SHORT_CIRCUIT__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.AND_THEN_SHORT_CIRCUIT__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.AND_THEN_SHORT_CIRCUIT__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.AND_THEN_SHORT_CIRCUIT__SHORT_CIRCUIT_OPERATION_LEFT_EXPRESSION_Q:
				return basicSetShortCircuitOperationLeftExpressionQ(null, msgs);
			case AdaPackage.AND_THEN_SHORT_CIRCUIT__SHORT_CIRCUIT_OPERATION_RIGHT_EXPRESSION_Q:
				return basicSetShortCircuitOperationRightExpressionQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.AND_THEN_SHORT_CIRCUIT__SLOC:
				return getSloc();
			case AdaPackage.AND_THEN_SHORT_CIRCUIT__SHORT_CIRCUIT_OPERATION_LEFT_EXPRESSION_Q:
				return getShortCircuitOperationLeftExpressionQ();
			case AdaPackage.AND_THEN_SHORT_CIRCUIT__SHORT_CIRCUIT_OPERATION_RIGHT_EXPRESSION_Q:
				return getShortCircuitOperationRightExpressionQ();
			case AdaPackage.AND_THEN_SHORT_CIRCUIT__CHECKS:
				return getChecks();
			case AdaPackage.AND_THEN_SHORT_CIRCUIT__TYPE:
				return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.AND_THEN_SHORT_CIRCUIT__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.AND_THEN_SHORT_CIRCUIT__SHORT_CIRCUIT_OPERATION_LEFT_EXPRESSION_Q:
				setShortCircuitOperationLeftExpressionQ((ExpressionClass)newValue);
				return;
			case AdaPackage.AND_THEN_SHORT_CIRCUIT__SHORT_CIRCUIT_OPERATION_RIGHT_EXPRESSION_Q:
				setShortCircuitOperationRightExpressionQ((ExpressionClass)newValue);
				return;
			case AdaPackage.AND_THEN_SHORT_CIRCUIT__CHECKS:
				setChecks((String)newValue);
				return;
			case AdaPackage.AND_THEN_SHORT_CIRCUIT__TYPE:
				setType((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.AND_THEN_SHORT_CIRCUIT__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.AND_THEN_SHORT_CIRCUIT__SHORT_CIRCUIT_OPERATION_LEFT_EXPRESSION_Q:
				setShortCircuitOperationLeftExpressionQ((ExpressionClass)null);
				return;
			case AdaPackage.AND_THEN_SHORT_CIRCUIT__SHORT_CIRCUIT_OPERATION_RIGHT_EXPRESSION_Q:
				setShortCircuitOperationRightExpressionQ((ExpressionClass)null);
				return;
			case AdaPackage.AND_THEN_SHORT_CIRCUIT__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
			case AdaPackage.AND_THEN_SHORT_CIRCUIT__TYPE:
				setType(TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.AND_THEN_SHORT_CIRCUIT__SLOC:
				return sloc != null;
			case AdaPackage.AND_THEN_SHORT_CIRCUIT__SHORT_CIRCUIT_OPERATION_LEFT_EXPRESSION_Q:
				return shortCircuitOperationLeftExpressionQ != null;
			case AdaPackage.AND_THEN_SHORT_CIRCUIT__SHORT_CIRCUIT_OPERATION_RIGHT_EXPRESSION_Q:
				return shortCircuitOperationRightExpressionQ != null;
			case AdaPackage.AND_THEN_SHORT_CIRCUIT__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
			case AdaPackage.AND_THEN_SHORT_CIRCUIT__TYPE:
				return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(", type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //AndThenShortCircuitImpl
