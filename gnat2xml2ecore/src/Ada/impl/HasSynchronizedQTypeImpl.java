/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.HasSynchronizedQType;
import Ada.NotAnElement;
import Ada.Synchronized;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Has Synchronized QType</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.HasSynchronizedQTypeImpl#getSynchronized <em>Synchronized</em>}</li>
 *   <li>{@link Ada.impl.HasSynchronizedQTypeImpl#getNotAnElement <em>Not An Element</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HasSynchronizedQTypeImpl extends MinimalEObjectImpl.Container implements HasSynchronizedQType {
	/**
	 * The cached value of the '{@link #getSynchronized() <em>Synchronized</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSynchronized()
	 * @generated
	 * @ordered
	 */
	protected Synchronized synchronized_;

	/**
	 * The cached value of the '{@link #getNotAnElement() <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotAnElement()
	 * @generated
	 * @ordered
	 */
	protected NotAnElement notAnElement;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HasSynchronizedQTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getHasSynchronizedQType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Synchronized getSynchronized() {
		return synchronized_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSynchronized(Synchronized newSynchronized, NotificationChain msgs) {
		Synchronized oldSynchronized = synchronized_;
		synchronized_ = newSynchronized;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.HAS_SYNCHRONIZED_QTYPE__SYNCHRONIZED, oldSynchronized, newSynchronized);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSynchronized(Synchronized newSynchronized) {
		if (newSynchronized != synchronized_) {
			NotificationChain msgs = null;
			if (synchronized_ != null)
				msgs = ((InternalEObject)synchronized_).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.HAS_SYNCHRONIZED_QTYPE__SYNCHRONIZED, null, msgs);
			if (newSynchronized != null)
				msgs = ((InternalEObject)newSynchronized).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.HAS_SYNCHRONIZED_QTYPE__SYNCHRONIZED, null, msgs);
			msgs = basicSetSynchronized(newSynchronized, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.HAS_SYNCHRONIZED_QTYPE__SYNCHRONIZED, newSynchronized, newSynchronized));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotAnElement getNotAnElement() {
		return notAnElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNotAnElement(NotAnElement newNotAnElement, NotificationChain msgs) {
		NotAnElement oldNotAnElement = notAnElement;
		notAnElement = newNotAnElement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.HAS_SYNCHRONIZED_QTYPE__NOT_AN_ELEMENT, oldNotAnElement, newNotAnElement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotAnElement(NotAnElement newNotAnElement) {
		if (newNotAnElement != notAnElement) {
			NotificationChain msgs = null;
			if (notAnElement != null)
				msgs = ((InternalEObject)notAnElement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.HAS_SYNCHRONIZED_QTYPE__NOT_AN_ELEMENT, null, msgs);
			if (newNotAnElement != null)
				msgs = ((InternalEObject)newNotAnElement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.HAS_SYNCHRONIZED_QTYPE__NOT_AN_ELEMENT, null, msgs);
			msgs = basicSetNotAnElement(newNotAnElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.HAS_SYNCHRONIZED_QTYPE__NOT_AN_ELEMENT, newNotAnElement, newNotAnElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.HAS_SYNCHRONIZED_QTYPE__SYNCHRONIZED:
				return basicSetSynchronized(null, msgs);
			case AdaPackage.HAS_SYNCHRONIZED_QTYPE__NOT_AN_ELEMENT:
				return basicSetNotAnElement(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.HAS_SYNCHRONIZED_QTYPE__SYNCHRONIZED:
				return getSynchronized();
			case AdaPackage.HAS_SYNCHRONIZED_QTYPE__NOT_AN_ELEMENT:
				return getNotAnElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.HAS_SYNCHRONIZED_QTYPE__SYNCHRONIZED:
				setSynchronized((Synchronized)newValue);
				return;
			case AdaPackage.HAS_SYNCHRONIZED_QTYPE__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.HAS_SYNCHRONIZED_QTYPE__SYNCHRONIZED:
				setSynchronized((Synchronized)null);
				return;
			case AdaPackage.HAS_SYNCHRONIZED_QTYPE__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.HAS_SYNCHRONIZED_QTYPE__SYNCHRONIZED:
				return synchronized_ != null;
			case AdaPackage.HAS_SYNCHRONIZED_QTYPE__NOT_AN_ELEMENT:
				return notAnElement != null;
		}
		return super.eIsSet(featureID);
	}

} //HasSynchronizedQTypeImpl
