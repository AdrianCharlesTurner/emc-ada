/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Qualified Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.QualifiedExpression#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.QualifiedExpression#getConvertedOrQualifiedSubtypeMarkQ <em>Converted Or Qualified Subtype Mark Q</em>}</li>
 *   <li>{@link Ada.QualifiedExpression#getConvertedOrQualifiedExpressionQ <em>Converted Or Qualified Expression Q</em>}</li>
 *   <li>{@link Ada.QualifiedExpression#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.QualifiedExpression#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getQualifiedExpression()
 * @model extendedMetaData="name='Qualified_Expression' kind='elementOnly'"
 * @generated
 */
public interface QualifiedExpression extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getQualifiedExpression_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.QualifiedExpression#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Converted Or Qualified Subtype Mark Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Converted Or Qualified Subtype Mark Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Converted Or Qualified Subtype Mark Q</em>' containment reference.
	 * @see #setConvertedOrQualifiedSubtypeMarkQ(ExpressionClass)
	 * @see Ada.AdaPackage#getQualifiedExpression_ConvertedOrQualifiedSubtypeMarkQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='converted_or_qualified_subtype_mark_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getConvertedOrQualifiedSubtypeMarkQ();

	/**
	 * Sets the value of the '{@link Ada.QualifiedExpression#getConvertedOrQualifiedSubtypeMarkQ <em>Converted Or Qualified Subtype Mark Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Converted Or Qualified Subtype Mark Q</em>' containment reference.
	 * @see #getConvertedOrQualifiedSubtypeMarkQ()
	 * @generated
	 */
	void setConvertedOrQualifiedSubtypeMarkQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Converted Or Qualified Expression Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Converted Or Qualified Expression Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Converted Or Qualified Expression Q</em>' containment reference.
	 * @see #setConvertedOrQualifiedExpressionQ(ExpressionClass)
	 * @see Ada.AdaPackage#getQualifiedExpression_ConvertedOrQualifiedExpressionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='converted_or_qualified_expression_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getConvertedOrQualifiedExpressionQ();

	/**
	 * Sets the value of the '{@link Ada.QualifiedExpression#getConvertedOrQualifiedExpressionQ <em>Converted Or Qualified Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Converted Or Qualified Expression Q</em>' containment reference.
	 * @see #getConvertedOrQualifiedExpressionQ()
	 * @generated
	 */
	void setConvertedOrQualifiedExpressionQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getQualifiedExpression_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.QualifiedExpression#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see Ada.AdaPackage#getQualifiedExpression_Type()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='type' namespace='##targetNamespace'"
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link Ada.QualifiedExpression#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

} // QualifiedExpression
