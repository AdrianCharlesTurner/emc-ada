/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.ComponentDefinition;
import Ada.DefinitionClass;
import Ada.HasAliasedQType6;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.ComponentDefinitionImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.ComponentDefinitionImpl#getHasAliasedQ <em>Has Aliased Q</em>}</li>
 *   <li>{@link Ada.impl.ComponentDefinitionImpl#getComponentDefinitionViewQ <em>Component Definition View Q</em>}</li>
 *   <li>{@link Ada.impl.ComponentDefinitionImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComponentDefinitionImpl extends MinimalEObjectImpl.Container implements ComponentDefinition {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getHasAliasedQ() <em>Has Aliased Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasAliasedQ()
	 * @generated
	 * @ordered
	 */
	protected HasAliasedQType6 hasAliasedQ;

	/**
	 * The cached value of the '{@link #getComponentDefinitionViewQ() <em>Component Definition View Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentDefinitionViewQ()
	 * @generated
	 * @ordered
	 */
	protected DefinitionClass componentDefinitionViewQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getComponentDefinition();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.COMPONENT_DEFINITION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.COMPONENT_DEFINITION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.COMPONENT_DEFINITION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.COMPONENT_DEFINITION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAliasedQType6 getHasAliasedQ() {
		return hasAliasedQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasAliasedQ(HasAliasedQType6 newHasAliasedQ, NotificationChain msgs) {
		HasAliasedQType6 oldHasAliasedQ = hasAliasedQ;
		hasAliasedQ = newHasAliasedQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.COMPONENT_DEFINITION__HAS_ALIASED_Q, oldHasAliasedQ, newHasAliasedQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasAliasedQ(HasAliasedQType6 newHasAliasedQ) {
		if (newHasAliasedQ != hasAliasedQ) {
			NotificationChain msgs = null;
			if (hasAliasedQ != null)
				msgs = ((InternalEObject)hasAliasedQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.COMPONENT_DEFINITION__HAS_ALIASED_Q, null, msgs);
			if (newHasAliasedQ != null)
				msgs = ((InternalEObject)newHasAliasedQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.COMPONENT_DEFINITION__HAS_ALIASED_Q, null, msgs);
			msgs = basicSetHasAliasedQ(newHasAliasedQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.COMPONENT_DEFINITION__HAS_ALIASED_Q, newHasAliasedQ, newHasAliasedQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefinitionClass getComponentDefinitionViewQ() {
		return componentDefinitionViewQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComponentDefinitionViewQ(DefinitionClass newComponentDefinitionViewQ, NotificationChain msgs) {
		DefinitionClass oldComponentDefinitionViewQ = componentDefinitionViewQ;
		componentDefinitionViewQ = newComponentDefinitionViewQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.COMPONENT_DEFINITION__COMPONENT_DEFINITION_VIEW_Q, oldComponentDefinitionViewQ, newComponentDefinitionViewQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponentDefinitionViewQ(DefinitionClass newComponentDefinitionViewQ) {
		if (newComponentDefinitionViewQ != componentDefinitionViewQ) {
			NotificationChain msgs = null;
			if (componentDefinitionViewQ != null)
				msgs = ((InternalEObject)componentDefinitionViewQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.COMPONENT_DEFINITION__COMPONENT_DEFINITION_VIEW_Q, null, msgs);
			if (newComponentDefinitionViewQ != null)
				msgs = ((InternalEObject)newComponentDefinitionViewQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.COMPONENT_DEFINITION__COMPONENT_DEFINITION_VIEW_Q, null, msgs);
			msgs = basicSetComponentDefinitionViewQ(newComponentDefinitionViewQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.COMPONENT_DEFINITION__COMPONENT_DEFINITION_VIEW_Q, newComponentDefinitionViewQ, newComponentDefinitionViewQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.COMPONENT_DEFINITION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.COMPONENT_DEFINITION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.COMPONENT_DEFINITION__HAS_ALIASED_Q:
				return basicSetHasAliasedQ(null, msgs);
			case AdaPackage.COMPONENT_DEFINITION__COMPONENT_DEFINITION_VIEW_Q:
				return basicSetComponentDefinitionViewQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.COMPONENT_DEFINITION__SLOC:
				return getSloc();
			case AdaPackage.COMPONENT_DEFINITION__HAS_ALIASED_Q:
				return getHasAliasedQ();
			case AdaPackage.COMPONENT_DEFINITION__COMPONENT_DEFINITION_VIEW_Q:
				return getComponentDefinitionViewQ();
			case AdaPackage.COMPONENT_DEFINITION__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.COMPONENT_DEFINITION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.COMPONENT_DEFINITION__HAS_ALIASED_Q:
				setHasAliasedQ((HasAliasedQType6)newValue);
				return;
			case AdaPackage.COMPONENT_DEFINITION__COMPONENT_DEFINITION_VIEW_Q:
				setComponentDefinitionViewQ((DefinitionClass)newValue);
				return;
			case AdaPackage.COMPONENT_DEFINITION__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.COMPONENT_DEFINITION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.COMPONENT_DEFINITION__HAS_ALIASED_Q:
				setHasAliasedQ((HasAliasedQType6)null);
				return;
			case AdaPackage.COMPONENT_DEFINITION__COMPONENT_DEFINITION_VIEW_Q:
				setComponentDefinitionViewQ((DefinitionClass)null);
				return;
			case AdaPackage.COMPONENT_DEFINITION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.COMPONENT_DEFINITION__SLOC:
				return sloc != null;
			case AdaPackage.COMPONENT_DEFINITION__HAS_ALIASED_Q:
				return hasAliasedQ != null;
			case AdaPackage.COMPONENT_DEFINITION__COMPONENT_DEFINITION_VIEW_Q:
				return componentDefinitionViewQ != null;
			case AdaPackage.COMPONENT_DEFINITION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //ComponentDefinitionImpl
