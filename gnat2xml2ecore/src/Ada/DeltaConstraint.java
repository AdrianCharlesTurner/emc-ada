/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Delta Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.DeltaConstraint#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.DeltaConstraint#getDeltaExpressionQ <em>Delta Expression Q</em>}</li>
 *   <li>{@link Ada.DeltaConstraint#getRealRangeConstraintQ <em>Real Range Constraint Q</em>}</li>
 *   <li>{@link Ada.DeltaConstraint#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getDeltaConstraint()
 * @model extendedMetaData="name='Delta_Constraint' kind='elementOnly'"
 * @generated
 */
public interface DeltaConstraint extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getDeltaConstraint_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.DeltaConstraint#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Delta Expression Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delta Expression Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delta Expression Q</em>' containment reference.
	 * @see #setDeltaExpressionQ(ExpressionClass)
	 * @see Ada.AdaPackage#getDeltaConstraint_DeltaExpressionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='delta_expression_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getDeltaExpressionQ();

	/**
	 * Sets the value of the '{@link Ada.DeltaConstraint#getDeltaExpressionQ <em>Delta Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delta Expression Q</em>' containment reference.
	 * @see #getDeltaExpressionQ()
	 * @generated
	 */
	void setDeltaExpressionQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Real Range Constraint Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Real Range Constraint Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Real Range Constraint Q</em>' containment reference.
	 * @see #setRealRangeConstraintQ(RangeConstraintClass)
	 * @see Ada.AdaPackage#getDeltaConstraint_RealRangeConstraintQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='real_range_constraint_q' namespace='##targetNamespace'"
	 * @generated
	 */
	RangeConstraintClass getRealRangeConstraintQ();

	/**
	 * Sets the value of the '{@link Ada.DeltaConstraint#getRealRangeConstraintQ <em>Real Range Constraint Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Real Range Constraint Q</em>' containment reference.
	 * @see #getRealRangeConstraintQ()
	 * @generated
	 */
	void setRealRangeConstraintQ(RangeConstraintClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getDeltaConstraint_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.DeltaConstraint#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // DeltaConstraint
