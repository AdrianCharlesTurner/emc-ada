/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.AnonymousAccessToProtectedFunction;
import Ada.ElementClass;
import Ada.HasNullExclusionQType3;
import Ada.IsNotNullReturnQType6;
import Ada.ParameterSpecificationList;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Anonymous Access To Protected Function</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.AnonymousAccessToProtectedFunctionImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.AnonymousAccessToProtectedFunctionImpl#getHasNullExclusionQ <em>Has Null Exclusion Q</em>}</li>
 *   <li>{@link Ada.impl.AnonymousAccessToProtectedFunctionImpl#getAccessToSubprogramParameterProfileQl <em>Access To Subprogram Parameter Profile Ql</em>}</li>
 *   <li>{@link Ada.impl.AnonymousAccessToProtectedFunctionImpl#getIsNotNullReturnQ <em>Is Not Null Return Q</em>}</li>
 *   <li>{@link Ada.impl.AnonymousAccessToProtectedFunctionImpl#getAccessToFunctionResultProfileQ <em>Access To Function Result Profile Q</em>}</li>
 *   <li>{@link Ada.impl.AnonymousAccessToProtectedFunctionImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AnonymousAccessToProtectedFunctionImpl extends MinimalEObjectImpl.Container implements AnonymousAccessToProtectedFunction {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getHasNullExclusionQ() <em>Has Null Exclusion Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasNullExclusionQ()
	 * @generated
	 * @ordered
	 */
	protected HasNullExclusionQType3 hasNullExclusionQ;

	/**
	 * The cached value of the '{@link #getAccessToSubprogramParameterProfileQl() <em>Access To Subprogram Parameter Profile Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessToSubprogramParameterProfileQl()
	 * @generated
	 * @ordered
	 */
	protected ParameterSpecificationList accessToSubprogramParameterProfileQl;

	/**
	 * The cached value of the '{@link #getIsNotNullReturnQ() <em>Is Not Null Return Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsNotNullReturnQ()
	 * @generated
	 * @ordered
	 */
	protected IsNotNullReturnQType6 isNotNullReturnQ;

	/**
	 * The cached value of the '{@link #getAccessToFunctionResultProfileQ() <em>Access To Function Result Profile Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessToFunctionResultProfileQ()
	 * @generated
	 * @ordered
	 */
	protected ElementClass accessToFunctionResultProfileQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnonymousAccessToProtectedFunctionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getAnonymousAccessToProtectedFunction();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType3 getHasNullExclusionQ() {
		return hasNullExclusionQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasNullExclusionQ(HasNullExclusionQType3 newHasNullExclusionQ, NotificationChain msgs) {
		HasNullExclusionQType3 oldHasNullExclusionQ = hasNullExclusionQ;
		hasNullExclusionQ = newHasNullExclusionQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__HAS_NULL_EXCLUSION_Q, oldHasNullExclusionQ, newHasNullExclusionQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasNullExclusionQ(HasNullExclusionQType3 newHasNullExclusionQ) {
		if (newHasNullExclusionQ != hasNullExclusionQ) {
			NotificationChain msgs = null;
			if (hasNullExclusionQ != null)
				msgs = ((InternalEObject)hasNullExclusionQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__HAS_NULL_EXCLUSION_Q, null, msgs);
			if (newHasNullExclusionQ != null)
				msgs = ((InternalEObject)newHasNullExclusionQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__HAS_NULL_EXCLUSION_Q, null, msgs);
			msgs = basicSetHasNullExclusionQ(newHasNullExclusionQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__HAS_NULL_EXCLUSION_Q, newHasNullExclusionQ, newHasNullExclusionQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterSpecificationList getAccessToSubprogramParameterProfileQl() {
		return accessToSubprogramParameterProfileQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccessToSubprogramParameterProfileQl(ParameterSpecificationList newAccessToSubprogramParameterProfileQl, NotificationChain msgs) {
		ParameterSpecificationList oldAccessToSubprogramParameterProfileQl = accessToSubprogramParameterProfileQl;
		accessToSubprogramParameterProfileQl = newAccessToSubprogramParameterProfileQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__ACCESS_TO_SUBPROGRAM_PARAMETER_PROFILE_QL, oldAccessToSubprogramParameterProfileQl, newAccessToSubprogramParameterProfileQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessToSubprogramParameterProfileQl(ParameterSpecificationList newAccessToSubprogramParameterProfileQl) {
		if (newAccessToSubprogramParameterProfileQl != accessToSubprogramParameterProfileQl) {
			NotificationChain msgs = null;
			if (accessToSubprogramParameterProfileQl != null)
				msgs = ((InternalEObject)accessToSubprogramParameterProfileQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__ACCESS_TO_SUBPROGRAM_PARAMETER_PROFILE_QL, null, msgs);
			if (newAccessToSubprogramParameterProfileQl != null)
				msgs = ((InternalEObject)newAccessToSubprogramParameterProfileQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__ACCESS_TO_SUBPROGRAM_PARAMETER_PROFILE_QL, null, msgs);
			msgs = basicSetAccessToSubprogramParameterProfileQl(newAccessToSubprogramParameterProfileQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__ACCESS_TO_SUBPROGRAM_PARAMETER_PROFILE_QL, newAccessToSubprogramParameterProfileQl, newAccessToSubprogramParameterProfileQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotNullReturnQType6 getIsNotNullReturnQ() {
		return isNotNullReturnQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIsNotNullReturnQ(IsNotNullReturnQType6 newIsNotNullReturnQ, NotificationChain msgs) {
		IsNotNullReturnQType6 oldIsNotNullReturnQ = isNotNullReturnQ;
		isNotNullReturnQ = newIsNotNullReturnQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__IS_NOT_NULL_RETURN_Q, oldIsNotNullReturnQ, newIsNotNullReturnQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsNotNullReturnQ(IsNotNullReturnQType6 newIsNotNullReturnQ) {
		if (newIsNotNullReturnQ != isNotNullReturnQ) {
			NotificationChain msgs = null;
			if (isNotNullReturnQ != null)
				msgs = ((InternalEObject)isNotNullReturnQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__IS_NOT_NULL_RETURN_Q, null, msgs);
			if (newIsNotNullReturnQ != null)
				msgs = ((InternalEObject)newIsNotNullReturnQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__IS_NOT_NULL_RETURN_Q, null, msgs);
			msgs = basicSetIsNotNullReturnQ(newIsNotNullReturnQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__IS_NOT_NULL_RETURN_Q, newIsNotNullReturnQ, newIsNotNullReturnQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementClass getAccessToFunctionResultProfileQ() {
		return accessToFunctionResultProfileQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccessToFunctionResultProfileQ(ElementClass newAccessToFunctionResultProfileQ, NotificationChain msgs) {
		ElementClass oldAccessToFunctionResultProfileQ = accessToFunctionResultProfileQ;
		accessToFunctionResultProfileQ = newAccessToFunctionResultProfileQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__ACCESS_TO_FUNCTION_RESULT_PROFILE_Q, oldAccessToFunctionResultProfileQ, newAccessToFunctionResultProfileQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessToFunctionResultProfileQ(ElementClass newAccessToFunctionResultProfileQ) {
		if (newAccessToFunctionResultProfileQ != accessToFunctionResultProfileQ) {
			NotificationChain msgs = null;
			if (accessToFunctionResultProfileQ != null)
				msgs = ((InternalEObject)accessToFunctionResultProfileQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__ACCESS_TO_FUNCTION_RESULT_PROFILE_Q, null, msgs);
			if (newAccessToFunctionResultProfileQ != null)
				msgs = ((InternalEObject)newAccessToFunctionResultProfileQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__ACCESS_TO_FUNCTION_RESULT_PROFILE_Q, null, msgs);
			msgs = basicSetAccessToFunctionResultProfileQ(newAccessToFunctionResultProfileQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__ACCESS_TO_FUNCTION_RESULT_PROFILE_Q, newAccessToFunctionResultProfileQ, newAccessToFunctionResultProfileQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__HAS_NULL_EXCLUSION_Q:
				return basicSetHasNullExclusionQ(null, msgs);
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__ACCESS_TO_SUBPROGRAM_PARAMETER_PROFILE_QL:
				return basicSetAccessToSubprogramParameterProfileQl(null, msgs);
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__IS_NOT_NULL_RETURN_Q:
				return basicSetIsNotNullReturnQ(null, msgs);
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__ACCESS_TO_FUNCTION_RESULT_PROFILE_Q:
				return basicSetAccessToFunctionResultProfileQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__SLOC:
				return getSloc();
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__HAS_NULL_EXCLUSION_Q:
				return getHasNullExclusionQ();
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__ACCESS_TO_SUBPROGRAM_PARAMETER_PROFILE_QL:
				return getAccessToSubprogramParameterProfileQl();
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__IS_NOT_NULL_RETURN_Q:
				return getIsNotNullReturnQ();
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__ACCESS_TO_FUNCTION_RESULT_PROFILE_Q:
				return getAccessToFunctionResultProfileQ();
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__HAS_NULL_EXCLUSION_Q:
				setHasNullExclusionQ((HasNullExclusionQType3)newValue);
				return;
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__ACCESS_TO_SUBPROGRAM_PARAMETER_PROFILE_QL:
				setAccessToSubprogramParameterProfileQl((ParameterSpecificationList)newValue);
				return;
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__IS_NOT_NULL_RETURN_Q:
				setIsNotNullReturnQ((IsNotNullReturnQType6)newValue);
				return;
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__ACCESS_TO_FUNCTION_RESULT_PROFILE_Q:
				setAccessToFunctionResultProfileQ((ElementClass)newValue);
				return;
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__HAS_NULL_EXCLUSION_Q:
				setHasNullExclusionQ((HasNullExclusionQType3)null);
				return;
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__ACCESS_TO_SUBPROGRAM_PARAMETER_PROFILE_QL:
				setAccessToSubprogramParameterProfileQl((ParameterSpecificationList)null);
				return;
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__IS_NOT_NULL_RETURN_Q:
				setIsNotNullReturnQ((IsNotNullReturnQType6)null);
				return;
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__ACCESS_TO_FUNCTION_RESULT_PROFILE_Q:
				setAccessToFunctionResultProfileQ((ElementClass)null);
				return;
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__SLOC:
				return sloc != null;
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__HAS_NULL_EXCLUSION_Q:
				return hasNullExclusionQ != null;
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__ACCESS_TO_SUBPROGRAM_PARAMETER_PROFILE_QL:
				return accessToSubprogramParameterProfileQl != null;
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__IS_NOT_NULL_RETURN_Q:
				return isNotNullReturnQ != null;
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__ACCESS_TO_FUNCTION_RESULT_PROFILE_Q:
				return accessToFunctionResultProfileQ != null;
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //AnonymousAccessToProtectedFunctionImpl
