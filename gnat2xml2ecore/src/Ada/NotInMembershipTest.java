/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Not In Membership Test</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.NotInMembershipTest#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.NotInMembershipTest#getMembershipTestExpressionQ <em>Membership Test Expression Q</em>}</li>
 *   <li>{@link Ada.NotInMembershipTest#getMembershipTestChoicesQl <em>Membership Test Choices Ql</em>}</li>
 *   <li>{@link Ada.NotInMembershipTest#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.NotInMembershipTest#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getNotInMembershipTest()
 * @model extendedMetaData="name='Not_In_Membership_Test' kind='elementOnly'"
 * @generated
 */
public interface NotInMembershipTest extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getNotInMembershipTest_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.NotInMembershipTest#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Membership Test Expression Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Membership Test Expression Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Membership Test Expression Q</em>' containment reference.
	 * @see #setMembershipTestExpressionQ(ExpressionClass)
	 * @see Ada.AdaPackage#getNotInMembershipTest_MembershipTestExpressionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='membership_test_expression_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getMembershipTestExpressionQ();

	/**
	 * Sets the value of the '{@link Ada.NotInMembershipTest#getMembershipTestExpressionQ <em>Membership Test Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Membership Test Expression Q</em>' containment reference.
	 * @see #getMembershipTestExpressionQ()
	 * @generated
	 */
	void setMembershipTestExpressionQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Membership Test Choices Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Membership Test Choices Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Membership Test Choices Ql</em>' containment reference.
	 * @see #setMembershipTestChoicesQl(ElementList)
	 * @see Ada.AdaPackage#getNotInMembershipTest_MembershipTestChoicesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='membership_test_choices_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementList getMembershipTestChoicesQl();

	/**
	 * Sets the value of the '{@link Ada.NotInMembershipTest#getMembershipTestChoicesQl <em>Membership Test Choices Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Membership Test Choices Ql</em>' containment reference.
	 * @see #getMembershipTestChoicesQl()
	 * @generated
	 */
	void setMembershipTestChoicesQl(ElementList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getNotInMembershipTest_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.NotInMembershipTest#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see Ada.AdaPackage#getNotInMembershipTest_Type()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='type' namespace='##targetNamespace'"
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link Ada.NotInMembershipTest#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

} // NotInMembershipTest
