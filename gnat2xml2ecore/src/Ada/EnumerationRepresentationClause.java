/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enumeration Representation Clause</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.EnumerationRepresentationClause#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.EnumerationRepresentationClause#getRepresentationClauseNameQ <em>Representation Clause Name Q</em>}</li>
 *   <li>{@link Ada.EnumerationRepresentationClause#getRepresentationClauseExpressionQ <em>Representation Clause Expression Q</em>}</li>
 *   <li>{@link Ada.EnumerationRepresentationClause#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getEnumerationRepresentationClause()
 * @model extendedMetaData="name='Enumeration_Representation_Clause' kind='elementOnly'"
 * @generated
 */
public interface EnumerationRepresentationClause extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getEnumerationRepresentationClause_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.EnumerationRepresentationClause#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Representation Clause Name Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Representation Clause Name Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Representation Clause Name Q</em>' containment reference.
	 * @see #setRepresentationClauseNameQ(NameClass)
	 * @see Ada.AdaPackage#getEnumerationRepresentationClause_RepresentationClauseNameQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='representation_clause_name_q' namespace='##targetNamespace'"
	 * @generated
	 */
	NameClass getRepresentationClauseNameQ();

	/**
	 * Sets the value of the '{@link Ada.EnumerationRepresentationClause#getRepresentationClauseNameQ <em>Representation Clause Name Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Representation Clause Name Q</em>' containment reference.
	 * @see #getRepresentationClauseNameQ()
	 * @generated
	 */
	void setRepresentationClauseNameQ(NameClass value);

	/**
	 * Returns the value of the '<em><b>Representation Clause Expression Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Representation Clause Expression Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Representation Clause Expression Q</em>' containment reference.
	 * @see #setRepresentationClauseExpressionQ(ExpressionClass)
	 * @see Ada.AdaPackage#getEnumerationRepresentationClause_RepresentationClauseExpressionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='representation_clause_expression_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getRepresentationClauseExpressionQ();

	/**
	 * Sets the value of the '{@link Ada.EnumerationRepresentationClause#getRepresentationClauseExpressionQ <em>Representation Clause Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Representation Clause Expression Q</em>' containment reference.
	 * @see #getRepresentationClauseExpressionQ()
	 * @generated
	 */
	void setRepresentationClauseExpressionQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getEnumerationRepresentationClause_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.EnumerationRepresentationClause#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // EnumerationRepresentationClause
