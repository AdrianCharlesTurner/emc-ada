/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task Dispatching Policy Pragma</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.TaskDispatchingPolicyPragma#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.TaskDispatchingPolicyPragma#getPragmaArgumentAssociationsQl <em>Pragma Argument Associations Ql</em>}</li>
 *   <li>{@link Ada.TaskDispatchingPolicyPragma#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.TaskDispatchingPolicyPragma#getPragmaName <em>Pragma Name</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getTaskDispatchingPolicyPragma()
 * @model extendedMetaData="name='Task_Dispatching_Policy_Pragma' kind='elementOnly'"
 * @generated
 */
public interface TaskDispatchingPolicyPragma extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getTaskDispatchingPolicyPragma_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.TaskDispatchingPolicyPragma#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Pragma Argument Associations Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pragma Argument Associations Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pragma Argument Associations Ql</em>' containment reference.
	 * @see #setPragmaArgumentAssociationsQl(AssociationList)
	 * @see Ada.AdaPackage#getTaskDispatchingPolicyPragma_PragmaArgumentAssociationsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='pragma_argument_associations_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	AssociationList getPragmaArgumentAssociationsQl();

	/**
	 * Sets the value of the '{@link Ada.TaskDispatchingPolicyPragma#getPragmaArgumentAssociationsQl <em>Pragma Argument Associations Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pragma Argument Associations Ql</em>' containment reference.
	 * @see #getPragmaArgumentAssociationsQl()
	 * @generated
	 */
	void setPragmaArgumentAssociationsQl(AssociationList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getTaskDispatchingPolicyPragma_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.TaskDispatchingPolicyPragma#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

	/**
	 * Returns the value of the '<em><b>Pragma Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pragma Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pragma Name</em>' attribute.
	 * @see #setPragmaName(String)
	 * @see Ada.AdaPackage#getTaskDispatchingPolicyPragma_PragmaName()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='pragma_name' namespace='##targetNamespace'"
	 * @generated
	 */
	String getPragmaName();

	/**
	 * Sets the value of the '{@link Ada.TaskDispatchingPolicyPragma#getPragmaName <em>Pragma Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pragma Name</em>' attribute.
	 * @see #getPragmaName()
	 * @generated
	 */
	void setPragmaName(String value);

} // TaskDispatchingPolicyPragma
