/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.DeclarativeItemList;
import Ada.DefiningNameList;
import Ada.ElementList;
import Ada.PackageDeclaration;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Package Declaration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.PackageDeclarationImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.PackageDeclarationImpl#getNamesQl <em>Names Ql</em>}</li>
 *   <li>{@link Ada.impl.PackageDeclarationImpl#getAspectSpecificationsQl <em>Aspect Specifications Ql</em>}</li>
 *   <li>{@link Ada.impl.PackageDeclarationImpl#getVisiblePartDeclarativeItemsQl <em>Visible Part Declarative Items Ql</em>}</li>
 *   <li>{@link Ada.impl.PackageDeclarationImpl#getPrivatePartDeclarativeItemsQl <em>Private Part Declarative Items Ql</em>}</li>
 *   <li>{@link Ada.impl.PackageDeclarationImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PackageDeclarationImpl extends MinimalEObjectImpl.Container implements PackageDeclaration {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getNamesQl() <em>Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamesQl()
	 * @generated
	 * @ordered
	 */
	protected DefiningNameList namesQl;

	/**
	 * The cached value of the '{@link #getAspectSpecificationsQl() <em>Aspect Specifications Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAspectSpecificationsQl()
	 * @generated
	 * @ordered
	 */
	protected ElementList aspectSpecificationsQl;

	/**
	 * The cached value of the '{@link #getVisiblePartDeclarativeItemsQl() <em>Visible Part Declarative Items Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisiblePartDeclarativeItemsQl()
	 * @generated
	 * @ordered
	 */
	protected DeclarativeItemList visiblePartDeclarativeItemsQl;

	/**
	 * The cached value of the '{@link #getPrivatePartDeclarativeItemsQl() <em>Private Part Declarative Items Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrivatePartDeclarativeItemsQl()
	 * @generated
	 * @ordered
	 */
	protected DeclarativeItemList privatePartDeclarativeItemsQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PackageDeclarationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getPackageDeclaration();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.PACKAGE_DECLARATION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PACKAGE_DECLARATION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PACKAGE_DECLARATION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PACKAGE_DECLARATION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNameList getNamesQl() {
		return namesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNamesQl(DefiningNameList newNamesQl, NotificationChain msgs) {
		DefiningNameList oldNamesQl = namesQl;
		namesQl = newNamesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.PACKAGE_DECLARATION__NAMES_QL, oldNamesQl, newNamesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNamesQl(DefiningNameList newNamesQl) {
		if (newNamesQl != namesQl) {
			NotificationChain msgs = null;
			if (namesQl != null)
				msgs = ((InternalEObject)namesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PACKAGE_DECLARATION__NAMES_QL, null, msgs);
			if (newNamesQl != null)
				msgs = ((InternalEObject)newNamesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PACKAGE_DECLARATION__NAMES_QL, null, msgs);
			msgs = basicSetNamesQl(newNamesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PACKAGE_DECLARATION__NAMES_QL, newNamesQl, newNamesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementList getAspectSpecificationsQl() {
		return aspectSpecificationsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAspectSpecificationsQl(ElementList newAspectSpecificationsQl, NotificationChain msgs) {
		ElementList oldAspectSpecificationsQl = aspectSpecificationsQl;
		aspectSpecificationsQl = newAspectSpecificationsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.PACKAGE_DECLARATION__ASPECT_SPECIFICATIONS_QL, oldAspectSpecificationsQl, newAspectSpecificationsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAspectSpecificationsQl(ElementList newAspectSpecificationsQl) {
		if (newAspectSpecificationsQl != aspectSpecificationsQl) {
			NotificationChain msgs = null;
			if (aspectSpecificationsQl != null)
				msgs = ((InternalEObject)aspectSpecificationsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PACKAGE_DECLARATION__ASPECT_SPECIFICATIONS_QL, null, msgs);
			if (newAspectSpecificationsQl != null)
				msgs = ((InternalEObject)newAspectSpecificationsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PACKAGE_DECLARATION__ASPECT_SPECIFICATIONS_QL, null, msgs);
			msgs = basicSetAspectSpecificationsQl(newAspectSpecificationsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PACKAGE_DECLARATION__ASPECT_SPECIFICATIONS_QL, newAspectSpecificationsQl, newAspectSpecificationsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeclarativeItemList getVisiblePartDeclarativeItemsQl() {
		return visiblePartDeclarativeItemsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVisiblePartDeclarativeItemsQl(DeclarativeItemList newVisiblePartDeclarativeItemsQl, NotificationChain msgs) {
		DeclarativeItemList oldVisiblePartDeclarativeItemsQl = visiblePartDeclarativeItemsQl;
		visiblePartDeclarativeItemsQl = newVisiblePartDeclarativeItemsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.PACKAGE_DECLARATION__VISIBLE_PART_DECLARATIVE_ITEMS_QL, oldVisiblePartDeclarativeItemsQl, newVisiblePartDeclarativeItemsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVisiblePartDeclarativeItemsQl(DeclarativeItemList newVisiblePartDeclarativeItemsQl) {
		if (newVisiblePartDeclarativeItemsQl != visiblePartDeclarativeItemsQl) {
			NotificationChain msgs = null;
			if (visiblePartDeclarativeItemsQl != null)
				msgs = ((InternalEObject)visiblePartDeclarativeItemsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PACKAGE_DECLARATION__VISIBLE_PART_DECLARATIVE_ITEMS_QL, null, msgs);
			if (newVisiblePartDeclarativeItemsQl != null)
				msgs = ((InternalEObject)newVisiblePartDeclarativeItemsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PACKAGE_DECLARATION__VISIBLE_PART_DECLARATIVE_ITEMS_QL, null, msgs);
			msgs = basicSetVisiblePartDeclarativeItemsQl(newVisiblePartDeclarativeItemsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PACKAGE_DECLARATION__VISIBLE_PART_DECLARATIVE_ITEMS_QL, newVisiblePartDeclarativeItemsQl, newVisiblePartDeclarativeItemsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeclarativeItemList getPrivatePartDeclarativeItemsQl() {
		return privatePartDeclarativeItemsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrivatePartDeclarativeItemsQl(DeclarativeItemList newPrivatePartDeclarativeItemsQl, NotificationChain msgs) {
		DeclarativeItemList oldPrivatePartDeclarativeItemsQl = privatePartDeclarativeItemsQl;
		privatePartDeclarativeItemsQl = newPrivatePartDeclarativeItemsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.PACKAGE_DECLARATION__PRIVATE_PART_DECLARATIVE_ITEMS_QL, oldPrivatePartDeclarativeItemsQl, newPrivatePartDeclarativeItemsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrivatePartDeclarativeItemsQl(DeclarativeItemList newPrivatePartDeclarativeItemsQl) {
		if (newPrivatePartDeclarativeItemsQl != privatePartDeclarativeItemsQl) {
			NotificationChain msgs = null;
			if (privatePartDeclarativeItemsQl != null)
				msgs = ((InternalEObject)privatePartDeclarativeItemsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PACKAGE_DECLARATION__PRIVATE_PART_DECLARATIVE_ITEMS_QL, null, msgs);
			if (newPrivatePartDeclarativeItemsQl != null)
				msgs = ((InternalEObject)newPrivatePartDeclarativeItemsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PACKAGE_DECLARATION__PRIVATE_PART_DECLARATIVE_ITEMS_QL, null, msgs);
			msgs = basicSetPrivatePartDeclarativeItemsQl(newPrivatePartDeclarativeItemsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PACKAGE_DECLARATION__PRIVATE_PART_DECLARATIVE_ITEMS_QL, newPrivatePartDeclarativeItemsQl, newPrivatePartDeclarativeItemsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PACKAGE_DECLARATION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.PACKAGE_DECLARATION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.PACKAGE_DECLARATION__NAMES_QL:
				return basicSetNamesQl(null, msgs);
			case AdaPackage.PACKAGE_DECLARATION__ASPECT_SPECIFICATIONS_QL:
				return basicSetAspectSpecificationsQl(null, msgs);
			case AdaPackage.PACKAGE_DECLARATION__VISIBLE_PART_DECLARATIVE_ITEMS_QL:
				return basicSetVisiblePartDeclarativeItemsQl(null, msgs);
			case AdaPackage.PACKAGE_DECLARATION__PRIVATE_PART_DECLARATIVE_ITEMS_QL:
				return basicSetPrivatePartDeclarativeItemsQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.PACKAGE_DECLARATION__SLOC:
				return getSloc();
			case AdaPackage.PACKAGE_DECLARATION__NAMES_QL:
				return getNamesQl();
			case AdaPackage.PACKAGE_DECLARATION__ASPECT_SPECIFICATIONS_QL:
				return getAspectSpecificationsQl();
			case AdaPackage.PACKAGE_DECLARATION__VISIBLE_PART_DECLARATIVE_ITEMS_QL:
				return getVisiblePartDeclarativeItemsQl();
			case AdaPackage.PACKAGE_DECLARATION__PRIVATE_PART_DECLARATIVE_ITEMS_QL:
				return getPrivatePartDeclarativeItemsQl();
			case AdaPackage.PACKAGE_DECLARATION__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.PACKAGE_DECLARATION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.PACKAGE_DECLARATION__NAMES_QL:
				setNamesQl((DefiningNameList)newValue);
				return;
			case AdaPackage.PACKAGE_DECLARATION__ASPECT_SPECIFICATIONS_QL:
				setAspectSpecificationsQl((ElementList)newValue);
				return;
			case AdaPackage.PACKAGE_DECLARATION__VISIBLE_PART_DECLARATIVE_ITEMS_QL:
				setVisiblePartDeclarativeItemsQl((DeclarativeItemList)newValue);
				return;
			case AdaPackage.PACKAGE_DECLARATION__PRIVATE_PART_DECLARATIVE_ITEMS_QL:
				setPrivatePartDeclarativeItemsQl((DeclarativeItemList)newValue);
				return;
			case AdaPackage.PACKAGE_DECLARATION__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.PACKAGE_DECLARATION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.PACKAGE_DECLARATION__NAMES_QL:
				setNamesQl((DefiningNameList)null);
				return;
			case AdaPackage.PACKAGE_DECLARATION__ASPECT_SPECIFICATIONS_QL:
				setAspectSpecificationsQl((ElementList)null);
				return;
			case AdaPackage.PACKAGE_DECLARATION__VISIBLE_PART_DECLARATIVE_ITEMS_QL:
				setVisiblePartDeclarativeItemsQl((DeclarativeItemList)null);
				return;
			case AdaPackage.PACKAGE_DECLARATION__PRIVATE_PART_DECLARATIVE_ITEMS_QL:
				setPrivatePartDeclarativeItemsQl((DeclarativeItemList)null);
				return;
			case AdaPackage.PACKAGE_DECLARATION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.PACKAGE_DECLARATION__SLOC:
				return sloc != null;
			case AdaPackage.PACKAGE_DECLARATION__NAMES_QL:
				return namesQl != null;
			case AdaPackage.PACKAGE_DECLARATION__ASPECT_SPECIFICATIONS_QL:
				return aspectSpecificationsQl != null;
			case AdaPackage.PACKAGE_DECLARATION__VISIBLE_PART_DECLARATIVE_ITEMS_QL:
				return visiblePartDeclarativeItemsQl != null;
			case AdaPackage.PACKAGE_DECLARATION__PRIVATE_PART_DECLARATIVE_ITEMS_QL:
				return privatePartDeclarativeItemsQl != null;
			case AdaPackage.PACKAGE_DECLARATION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //PackageDeclarationImpl
