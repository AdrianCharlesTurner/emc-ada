/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Has Reverse QType2</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.HasReverseQType2#getReverse <em>Reverse</em>}</li>
 *   <li>{@link Ada.HasReverseQType2#getNotAnElement <em>Not An Element</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getHasReverseQType2()
 * @model extendedMetaData="name='has_reverse_q_._2_._type' kind='elementOnly'"
 * @generated
 */
public interface HasReverseQType2 extends EObject {
	/**
	 * Returns the value of the '<em><b>Reverse</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reverse</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reverse</em>' containment reference.
	 * @see #setReverse(Reverse)
	 * @see Ada.AdaPackage#getHasReverseQType2_Reverse()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='reverse' namespace='##targetNamespace'"
	 * @generated
	 */
	Reverse getReverse();

	/**
	 * Sets the value of the '{@link Ada.HasReverseQType2#getReverse <em>Reverse</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reverse</em>' containment reference.
	 * @see #getReverse()
	 * @generated
	 */
	void setReverse(Reverse value);

	/**
	 * Returns the value of the '<em><b>Not An Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not An Element</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not An Element</em>' containment reference.
	 * @see #setNotAnElement(NotAnElement)
	 * @see Ada.AdaPackage#getHasReverseQType2_NotAnElement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not_an_element' namespace='##targetNamespace'"
	 * @generated
	 */
	NotAnElement getNotAnElement();

	/**
	 * Sets the value of the '{@link Ada.HasReverseQType2#getNotAnElement <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not An Element</em>' containment reference.
	 * @see #getNotAnElement()
	 * @generated
	 */
	void setNotAnElement(NotAnElement value);

} // HasReverseQType2
