/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.AspectSpecification;
import Ada.ElementClass;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Aspect Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.AspectSpecificationImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.AspectSpecificationImpl#getAspectMarkQ <em>Aspect Mark Q</em>}</li>
 *   <li>{@link Ada.impl.AspectSpecificationImpl#getAspectDefinitionQ <em>Aspect Definition Q</em>}</li>
 *   <li>{@link Ada.impl.AspectSpecificationImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AspectSpecificationImpl extends MinimalEObjectImpl.Container implements AspectSpecification {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getAspectMarkQ() <em>Aspect Mark Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAspectMarkQ()
	 * @generated
	 * @ordered
	 */
	protected ElementClass aspectMarkQ;

	/**
	 * The cached value of the '{@link #getAspectDefinitionQ() <em>Aspect Definition Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAspectDefinitionQ()
	 * @generated
	 * @ordered
	 */
	protected ElementClass aspectDefinitionQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AspectSpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getAspectSpecification();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASPECT_SPECIFICATION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASPECT_SPECIFICATION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASPECT_SPECIFICATION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASPECT_SPECIFICATION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementClass getAspectMarkQ() {
		return aspectMarkQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAspectMarkQ(ElementClass newAspectMarkQ, NotificationChain msgs) {
		ElementClass oldAspectMarkQ = aspectMarkQ;
		aspectMarkQ = newAspectMarkQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASPECT_SPECIFICATION__ASPECT_MARK_Q, oldAspectMarkQ, newAspectMarkQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAspectMarkQ(ElementClass newAspectMarkQ) {
		if (newAspectMarkQ != aspectMarkQ) {
			NotificationChain msgs = null;
			if (aspectMarkQ != null)
				msgs = ((InternalEObject)aspectMarkQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASPECT_SPECIFICATION__ASPECT_MARK_Q, null, msgs);
			if (newAspectMarkQ != null)
				msgs = ((InternalEObject)newAspectMarkQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASPECT_SPECIFICATION__ASPECT_MARK_Q, null, msgs);
			msgs = basicSetAspectMarkQ(newAspectMarkQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASPECT_SPECIFICATION__ASPECT_MARK_Q, newAspectMarkQ, newAspectMarkQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementClass getAspectDefinitionQ() {
		return aspectDefinitionQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAspectDefinitionQ(ElementClass newAspectDefinitionQ, NotificationChain msgs) {
		ElementClass oldAspectDefinitionQ = aspectDefinitionQ;
		aspectDefinitionQ = newAspectDefinitionQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ASPECT_SPECIFICATION__ASPECT_DEFINITION_Q, oldAspectDefinitionQ, newAspectDefinitionQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAspectDefinitionQ(ElementClass newAspectDefinitionQ) {
		if (newAspectDefinitionQ != aspectDefinitionQ) {
			NotificationChain msgs = null;
			if (aspectDefinitionQ != null)
				msgs = ((InternalEObject)aspectDefinitionQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASPECT_SPECIFICATION__ASPECT_DEFINITION_Q, null, msgs);
			if (newAspectDefinitionQ != null)
				msgs = ((InternalEObject)newAspectDefinitionQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ASPECT_SPECIFICATION__ASPECT_DEFINITION_Q, null, msgs);
			msgs = basicSetAspectDefinitionQ(newAspectDefinitionQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASPECT_SPECIFICATION__ASPECT_DEFINITION_Q, newAspectDefinitionQ, newAspectDefinitionQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ASPECT_SPECIFICATION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.ASPECT_SPECIFICATION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.ASPECT_SPECIFICATION__ASPECT_MARK_Q:
				return basicSetAspectMarkQ(null, msgs);
			case AdaPackage.ASPECT_SPECIFICATION__ASPECT_DEFINITION_Q:
				return basicSetAspectDefinitionQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.ASPECT_SPECIFICATION__SLOC:
				return getSloc();
			case AdaPackage.ASPECT_SPECIFICATION__ASPECT_MARK_Q:
				return getAspectMarkQ();
			case AdaPackage.ASPECT_SPECIFICATION__ASPECT_DEFINITION_Q:
				return getAspectDefinitionQ();
			case AdaPackage.ASPECT_SPECIFICATION__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.ASPECT_SPECIFICATION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.ASPECT_SPECIFICATION__ASPECT_MARK_Q:
				setAspectMarkQ((ElementClass)newValue);
				return;
			case AdaPackage.ASPECT_SPECIFICATION__ASPECT_DEFINITION_Q:
				setAspectDefinitionQ((ElementClass)newValue);
				return;
			case AdaPackage.ASPECT_SPECIFICATION__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.ASPECT_SPECIFICATION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.ASPECT_SPECIFICATION__ASPECT_MARK_Q:
				setAspectMarkQ((ElementClass)null);
				return;
			case AdaPackage.ASPECT_SPECIFICATION__ASPECT_DEFINITION_Q:
				setAspectDefinitionQ((ElementClass)null);
				return;
			case AdaPackage.ASPECT_SPECIFICATION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.ASPECT_SPECIFICATION__SLOC:
				return sloc != null;
			case AdaPackage.ASPECT_SPECIFICATION__ASPECT_MARK_Q:
				return aspectMarkQ != null;
			case AdaPackage.ASPECT_SPECIFICATION__ASPECT_DEFINITION_Q:
				return aspectDefinitionQ != null;
			case AdaPackage.ASPECT_SPECIFICATION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //AspectSpecificationImpl
