/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.DeclarativeItemList;
import Ada.ProtectedDefinition;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Protected Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.ProtectedDefinitionImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.ProtectedDefinitionImpl#getVisiblePartItemsQl <em>Visible Part Items Ql</em>}</li>
 *   <li>{@link Ada.impl.ProtectedDefinitionImpl#getPrivatePartItemsQl <em>Private Part Items Ql</em>}</li>
 *   <li>{@link Ada.impl.ProtectedDefinitionImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProtectedDefinitionImpl extends MinimalEObjectImpl.Container implements ProtectedDefinition {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getVisiblePartItemsQl() <em>Visible Part Items Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisiblePartItemsQl()
	 * @generated
	 * @ordered
	 */
	protected DeclarativeItemList visiblePartItemsQl;

	/**
	 * The cached value of the '{@link #getPrivatePartItemsQl() <em>Private Part Items Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrivatePartItemsQl()
	 * @generated
	 * @ordered
	 */
	protected DeclarativeItemList privatePartItemsQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProtectedDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getProtectedDefinition();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.PROTECTED_DEFINITION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PROTECTED_DEFINITION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PROTECTED_DEFINITION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PROTECTED_DEFINITION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeclarativeItemList getVisiblePartItemsQl() {
		return visiblePartItemsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVisiblePartItemsQl(DeclarativeItemList newVisiblePartItemsQl, NotificationChain msgs) {
		DeclarativeItemList oldVisiblePartItemsQl = visiblePartItemsQl;
		visiblePartItemsQl = newVisiblePartItemsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.PROTECTED_DEFINITION__VISIBLE_PART_ITEMS_QL, oldVisiblePartItemsQl, newVisiblePartItemsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVisiblePartItemsQl(DeclarativeItemList newVisiblePartItemsQl) {
		if (newVisiblePartItemsQl != visiblePartItemsQl) {
			NotificationChain msgs = null;
			if (visiblePartItemsQl != null)
				msgs = ((InternalEObject)visiblePartItemsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PROTECTED_DEFINITION__VISIBLE_PART_ITEMS_QL, null, msgs);
			if (newVisiblePartItemsQl != null)
				msgs = ((InternalEObject)newVisiblePartItemsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PROTECTED_DEFINITION__VISIBLE_PART_ITEMS_QL, null, msgs);
			msgs = basicSetVisiblePartItemsQl(newVisiblePartItemsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PROTECTED_DEFINITION__VISIBLE_PART_ITEMS_QL, newVisiblePartItemsQl, newVisiblePartItemsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeclarativeItemList getPrivatePartItemsQl() {
		return privatePartItemsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrivatePartItemsQl(DeclarativeItemList newPrivatePartItemsQl, NotificationChain msgs) {
		DeclarativeItemList oldPrivatePartItemsQl = privatePartItemsQl;
		privatePartItemsQl = newPrivatePartItemsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.PROTECTED_DEFINITION__PRIVATE_PART_ITEMS_QL, oldPrivatePartItemsQl, newPrivatePartItemsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrivatePartItemsQl(DeclarativeItemList newPrivatePartItemsQl) {
		if (newPrivatePartItemsQl != privatePartItemsQl) {
			NotificationChain msgs = null;
			if (privatePartItemsQl != null)
				msgs = ((InternalEObject)privatePartItemsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PROTECTED_DEFINITION__PRIVATE_PART_ITEMS_QL, null, msgs);
			if (newPrivatePartItemsQl != null)
				msgs = ((InternalEObject)newPrivatePartItemsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PROTECTED_DEFINITION__PRIVATE_PART_ITEMS_QL, null, msgs);
			msgs = basicSetPrivatePartItemsQl(newPrivatePartItemsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PROTECTED_DEFINITION__PRIVATE_PART_ITEMS_QL, newPrivatePartItemsQl, newPrivatePartItemsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PROTECTED_DEFINITION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.PROTECTED_DEFINITION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.PROTECTED_DEFINITION__VISIBLE_PART_ITEMS_QL:
				return basicSetVisiblePartItemsQl(null, msgs);
			case AdaPackage.PROTECTED_DEFINITION__PRIVATE_PART_ITEMS_QL:
				return basicSetPrivatePartItemsQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.PROTECTED_DEFINITION__SLOC:
				return getSloc();
			case AdaPackage.PROTECTED_DEFINITION__VISIBLE_PART_ITEMS_QL:
				return getVisiblePartItemsQl();
			case AdaPackage.PROTECTED_DEFINITION__PRIVATE_PART_ITEMS_QL:
				return getPrivatePartItemsQl();
			case AdaPackage.PROTECTED_DEFINITION__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.PROTECTED_DEFINITION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.PROTECTED_DEFINITION__VISIBLE_PART_ITEMS_QL:
				setVisiblePartItemsQl((DeclarativeItemList)newValue);
				return;
			case AdaPackage.PROTECTED_DEFINITION__PRIVATE_PART_ITEMS_QL:
				setPrivatePartItemsQl((DeclarativeItemList)newValue);
				return;
			case AdaPackage.PROTECTED_DEFINITION__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.PROTECTED_DEFINITION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.PROTECTED_DEFINITION__VISIBLE_PART_ITEMS_QL:
				setVisiblePartItemsQl((DeclarativeItemList)null);
				return;
			case AdaPackage.PROTECTED_DEFINITION__PRIVATE_PART_ITEMS_QL:
				setPrivatePartItemsQl((DeclarativeItemList)null);
				return;
			case AdaPackage.PROTECTED_DEFINITION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.PROTECTED_DEFINITION__SLOC:
				return sloc != null;
			case AdaPackage.PROTECTED_DEFINITION__VISIBLE_PART_ITEMS_QL:
				return visiblePartItemsQl != null;
			case AdaPackage.PROTECTED_DEFINITION__PRIVATE_PART_ITEMS_QL:
				return privatePartItemsQl != null;
			case AdaPackage.PROTECTED_DEFINITION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //ProtectedDefinitionImpl
