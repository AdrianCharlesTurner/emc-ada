/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Allocation From Qualified Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.AllocationFromQualifiedExpression#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.AllocationFromQualifiedExpression#getSubpoolNameQ <em>Subpool Name Q</em>}</li>
 *   <li>{@link Ada.AllocationFromQualifiedExpression#getAllocatorQualifiedExpressionQ <em>Allocator Qualified Expression Q</em>}</li>
 *   <li>{@link Ada.AllocationFromQualifiedExpression#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.AllocationFromQualifiedExpression#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getAllocationFromQualifiedExpression()
 * @model extendedMetaData="name='Allocation_From_Qualified_Expression' kind='elementOnly'"
 * @generated
 */
public interface AllocationFromQualifiedExpression extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getAllocationFromQualifiedExpression_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.AllocationFromQualifiedExpression#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Subpool Name Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subpool Name Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subpool Name Q</em>' containment reference.
	 * @see #setSubpoolNameQ(ExpressionClass)
	 * @see Ada.AdaPackage#getAllocationFromQualifiedExpression_SubpoolNameQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='subpool_name_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getSubpoolNameQ();

	/**
	 * Sets the value of the '{@link Ada.AllocationFromQualifiedExpression#getSubpoolNameQ <em>Subpool Name Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Subpool Name Q</em>' containment reference.
	 * @see #getSubpoolNameQ()
	 * @generated
	 */
	void setSubpoolNameQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Allocator Qualified Expression Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Allocator Qualified Expression Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Allocator Qualified Expression Q</em>' containment reference.
	 * @see #setAllocatorQualifiedExpressionQ(ExpressionClass)
	 * @see Ada.AdaPackage#getAllocationFromQualifiedExpression_AllocatorQualifiedExpressionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='allocator_qualified_expression_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getAllocatorQualifiedExpressionQ();

	/**
	 * Sets the value of the '{@link Ada.AllocationFromQualifiedExpression#getAllocatorQualifiedExpressionQ <em>Allocator Qualified Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Allocator Qualified Expression Q</em>' containment reference.
	 * @see #getAllocatorQualifiedExpressionQ()
	 * @generated
	 */
	void setAllocatorQualifiedExpressionQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getAllocationFromQualifiedExpression_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.AllocationFromQualifiedExpression#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see Ada.AdaPackage#getAllocationFromQualifiedExpression_Type()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='type' namespace='##targetNamespace'"
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link Ada.AllocationFromQualifiedExpression#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

} // AllocationFromQualifiedExpression
