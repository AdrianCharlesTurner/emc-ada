/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.BlockStatement;
import Ada.DeclarativeItemList;
import Ada.DefiningNameClass;
import Ada.DefiningNameList;
import Ada.ExceptionHandlerList;
import Ada.SourceLocation;
import Ada.StatementList;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Block Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.BlockStatementImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.BlockStatementImpl#getLabelNamesQl <em>Label Names Ql</em>}</li>
 *   <li>{@link Ada.impl.BlockStatementImpl#getStatementIdentifierQ <em>Statement Identifier Q</em>}</li>
 *   <li>{@link Ada.impl.BlockStatementImpl#getBlockDeclarativeItemsQl <em>Block Declarative Items Ql</em>}</li>
 *   <li>{@link Ada.impl.BlockStatementImpl#getBlockStatementsQl <em>Block Statements Ql</em>}</li>
 *   <li>{@link Ada.impl.BlockStatementImpl#getBlockExceptionHandlersQl <em>Block Exception Handlers Ql</em>}</li>
 *   <li>{@link Ada.impl.BlockStatementImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BlockStatementImpl extends MinimalEObjectImpl.Container implements BlockStatement {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getLabelNamesQl() <em>Label Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabelNamesQl()
	 * @generated
	 * @ordered
	 */
	protected DefiningNameList labelNamesQl;

	/**
	 * The cached value of the '{@link #getStatementIdentifierQ() <em>Statement Identifier Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatementIdentifierQ()
	 * @generated
	 * @ordered
	 */
	protected DefiningNameClass statementIdentifierQ;

	/**
	 * The cached value of the '{@link #getBlockDeclarativeItemsQl() <em>Block Declarative Items Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBlockDeclarativeItemsQl()
	 * @generated
	 * @ordered
	 */
	protected DeclarativeItemList blockDeclarativeItemsQl;

	/**
	 * The cached value of the '{@link #getBlockStatementsQl() <em>Block Statements Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBlockStatementsQl()
	 * @generated
	 * @ordered
	 */
	protected StatementList blockStatementsQl;

	/**
	 * The cached value of the '{@link #getBlockExceptionHandlersQl() <em>Block Exception Handlers Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBlockExceptionHandlersQl()
	 * @generated
	 * @ordered
	 */
	protected ExceptionHandlerList blockExceptionHandlersQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BlockStatementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getBlockStatement();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.BLOCK_STATEMENT__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.BLOCK_STATEMENT__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.BLOCK_STATEMENT__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.BLOCK_STATEMENT__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNameList getLabelNamesQl() {
		return labelNamesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLabelNamesQl(DefiningNameList newLabelNamesQl, NotificationChain msgs) {
		DefiningNameList oldLabelNamesQl = labelNamesQl;
		labelNamesQl = newLabelNamesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.BLOCK_STATEMENT__LABEL_NAMES_QL, oldLabelNamesQl, newLabelNamesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabelNamesQl(DefiningNameList newLabelNamesQl) {
		if (newLabelNamesQl != labelNamesQl) {
			NotificationChain msgs = null;
			if (labelNamesQl != null)
				msgs = ((InternalEObject)labelNamesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.BLOCK_STATEMENT__LABEL_NAMES_QL, null, msgs);
			if (newLabelNamesQl != null)
				msgs = ((InternalEObject)newLabelNamesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.BLOCK_STATEMENT__LABEL_NAMES_QL, null, msgs);
			msgs = basicSetLabelNamesQl(newLabelNamesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.BLOCK_STATEMENT__LABEL_NAMES_QL, newLabelNamesQl, newLabelNamesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNameClass getStatementIdentifierQ() {
		return statementIdentifierQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStatementIdentifierQ(DefiningNameClass newStatementIdentifierQ, NotificationChain msgs) {
		DefiningNameClass oldStatementIdentifierQ = statementIdentifierQ;
		statementIdentifierQ = newStatementIdentifierQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.BLOCK_STATEMENT__STATEMENT_IDENTIFIER_Q, oldStatementIdentifierQ, newStatementIdentifierQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStatementIdentifierQ(DefiningNameClass newStatementIdentifierQ) {
		if (newStatementIdentifierQ != statementIdentifierQ) {
			NotificationChain msgs = null;
			if (statementIdentifierQ != null)
				msgs = ((InternalEObject)statementIdentifierQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.BLOCK_STATEMENT__STATEMENT_IDENTIFIER_Q, null, msgs);
			if (newStatementIdentifierQ != null)
				msgs = ((InternalEObject)newStatementIdentifierQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.BLOCK_STATEMENT__STATEMENT_IDENTIFIER_Q, null, msgs);
			msgs = basicSetStatementIdentifierQ(newStatementIdentifierQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.BLOCK_STATEMENT__STATEMENT_IDENTIFIER_Q, newStatementIdentifierQ, newStatementIdentifierQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeclarativeItemList getBlockDeclarativeItemsQl() {
		return blockDeclarativeItemsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBlockDeclarativeItemsQl(DeclarativeItemList newBlockDeclarativeItemsQl, NotificationChain msgs) {
		DeclarativeItemList oldBlockDeclarativeItemsQl = blockDeclarativeItemsQl;
		blockDeclarativeItemsQl = newBlockDeclarativeItemsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.BLOCK_STATEMENT__BLOCK_DECLARATIVE_ITEMS_QL, oldBlockDeclarativeItemsQl, newBlockDeclarativeItemsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBlockDeclarativeItemsQl(DeclarativeItemList newBlockDeclarativeItemsQl) {
		if (newBlockDeclarativeItemsQl != blockDeclarativeItemsQl) {
			NotificationChain msgs = null;
			if (blockDeclarativeItemsQl != null)
				msgs = ((InternalEObject)blockDeclarativeItemsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.BLOCK_STATEMENT__BLOCK_DECLARATIVE_ITEMS_QL, null, msgs);
			if (newBlockDeclarativeItemsQl != null)
				msgs = ((InternalEObject)newBlockDeclarativeItemsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.BLOCK_STATEMENT__BLOCK_DECLARATIVE_ITEMS_QL, null, msgs);
			msgs = basicSetBlockDeclarativeItemsQl(newBlockDeclarativeItemsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.BLOCK_STATEMENT__BLOCK_DECLARATIVE_ITEMS_QL, newBlockDeclarativeItemsQl, newBlockDeclarativeItemsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatementList getBlockStatementsQl() {
		return blockStatementsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBlockStatementsQl(StatementList newBlockStatementsQl, NotificationChain msgs) {
		StatementList oldBlockStatementsQl = blockStatementsQl;
		blockStatementsQl = newBlockStatementsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.BLOCK_STATEMENT__BLOCK_STATEMENTS_QL, oldBlockStatementsQl, newBlockStatementsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBlockStatementsQl(StatementList newBlockStatementsQl) {
		if (newBlockStatementsQl != blockStatementsQl) {
			NotificationChain msgs = null;
			if (blockStatementsQl != null)
				msgs = ((InternalEObject)blockStatementsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.BLOCK_STATEMENT__BLOCK_STATEMENTS_QL, null, msgs);
			if (newBlockStatementsQl != null)
				msgs = ((InternalEObject)newBlockStatementsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.BLOCK_STATEMENT__BLOCK_STATEMENTS_QL, null, msgs);
			msgs = basicSetBlockStatementsQl(newBlockStatementsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.BLOCK_STATEMENT__BLOCK_STATEMENTS_QL, newBlockStatementsQl, newBlockStatementsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExceptionHandlerList getBlockExceptionHandlersQl() {
		return blockExceptionHandlersQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBlockExceptionHandlersQl(ExceptionHandlerList newBlockExceptionHandlersQl, NotificationChain msgs) {
		ExceptionHandlerList oldBlockExceptionHandlersQl = blockExceptionHandlersQl;
		blockExceptionHandlersQl = newBlockExceptionHandlersQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.BLOCK_STATEMENT__BLOCK_EXCEPTION_HANDLERS_QL, oldBlockExceptionHandlersQl, newBlockExceptionHandlersQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBlockExceptionHandlersQl(ExceptionHandlerList newBlockExceptionHandlersQl) {
		if (newBlockExceptionHandlersQl != blockExceptionHandlersQl) {
			NotificationChain msgs = null;
			if (blockExceptionHandlersQl != null)
				msgs = ((InternalEObject)blockExceptionHandlersQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.BLOCK_STATEMENT__BLOCK_EXCEPTION_HANDLERS_QL, null, msgs);
			if (newBlockExceptionHandlersQl != null)
				msgs = ((InternalEObject)newBlockExceptionHandlersQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.BLOCK_STATEMENT__BLOCK_EXCEPTION_HANDLERS_QL, null, msgs);
			msgs = basicSetBlockExceptionHandlersQl(newBlockExceptionHandlersQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.BLOCK_STATEMENT__BLOCK_EXCEPTION_HANDLERS_QL, newBlockExceptionHandlersQl, newBlockExceptionHandlersQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.BLOCK_STATEMENT__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.BLOCK_STATEMENT__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.BLOCK_STATEMENT__LABEL_NAMES_QL:
				return basicSetLabelNamesQl(null, msgs);
			case AdaPackage.BLOCK_STATEMENT__STATEMENT_IDENTIFIER_Q:
				return basicSetStatementIdentifierQ(null, msgs);
			case AdaPackage.BLOCK_STATEMENT__BLOCK_DECLARATIVE_ITEMS_QL:
				return basicSetBlockDeclarativeItemsQl(null, msgs);
			case AdaPackage.BLOCK_STATEMENT__BLOCK_STATEMENTS_QL:
				return basicSetBlockStatementsQl(null, msgs);
			case AdaPackage.BLOCK_STATEMENT__BLOCK_EXCEPTION_HANDLERS_QL:
				return basicSetBlockExceptionHandlersQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.BLOCK_STATEMENT__SLOC:
				return getSloc();
			case AdaPackage.BLOCK_STATEMENT__LABEL_NAMES_QL:
				return getLabelNamesQl();
			case AdaPackage.BLOCK_STATEMENT__STATEMENT_IDENTIFIER_Q:
				return getStatementIdentifierQ();
			case AdaPackage.BLOCK_STATEMENT__BLOCK_DECLARATIVE_ITEMS_QL:
				return getBlockDeclarativeItemsQl();
			case AdaPackage.BLOCK_STATEMENT__BLOCK_STATEMENTS_QL:
				return getBlockStatementsQl();
			case AdaPackage.BLOCK_STATEMENT__BLOCK_EXCEPTION_HANDLERS_QL:
				return getBlockExceptionHandlersQl();
			case AdaPackage.BLOCK_STATEMENT__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.BLOCK_STATEMENT__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.BLOCK_STATEMENT__LABEL_NAMES_QL:
				setLabelNamesQl((DefiningNameList)newValue);
				return;
			case AdaPackage.BLOCK_STATEMENT__STATEMENT_IDENTIFIER_Q:
				setStatementIdentifierQ((DefiningNameClass)newValue);
				return;
			case AdaPackage.BLOCK_STATEMENT__BLOCK_DECLARATIVE_ITEMS_QL:
				setBlockDeclarativeItemsQl((DeclarativeItemList)newValue);
				return;
			case AdaPackage.BLOCK_STATEMENT__BLOCK_STATEMENTS_QL:
				setBlockStatementsQl((StatementList)newValue);
				return;
			case AdaPackage.BLOCK_STATEMENT__BLOCK_EXCEPTION_HANDLERS_QL:
				setBlockExceptionHandlersQl((ExceptionHandlerList)newValue);
				return;
			case AdaPackage.BLOCK_STATEMENT__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.BLOCK_STATEMENT__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.BLOCK_STATEMENT__LABEL_NAMES_QL:
				setLabelNamesQl((DefiningNameList)null);
				return;
			case AdaPackage.BLOCK_STATEMENT__STATEMENT_IDENTIFIER_Q:
				setStatementIdentifierQ((DefiningNameClass)null);
				return;
			case AdaPackage.BLOCK_STATEMENT__BLOCK_DECLARATIVE_ITEMS_QL:
				setBlockDeclarativeItemsQl((DeclarativeItemList)null);
				return;
			case AdaPackage.BLOCK_STATEMENT__BLOCK_STATEMENTS_QL:
				setBlockStatementsQl((StatementList)null);
				return;
			case AdaPackage.BLOCK_STATEMENT__BLOCK_EXCEPTION_HANDLERS_QL:
				setBlockExceptionHandlersQl((ExceptionHandlerList)null);
				return;
			case AdaPackage.BLOCK_STATEMENT__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.BLOCK_STATEMENT__SLOC:
				return sloc != null;
			case AdaPackage.BLOCK_STATEMENT__LABEL_NAMES_QL:
				return labelNamesQl != null;
			case AdaPackage.BLOCK_STATEMENT__STATEMENT_IDENTIFIER_Q:
				return statementIdentifierQ != null;
			case AdaPackage.BLOCK_STATEMENT__BLOCK_DECLARATIVE_ITEMS_QL:
				return blockDeclarativeItemsQl != null;
			case AdaPackage.BLOCK_STATEMENT__BLOCK_STATEMENTS_QL:
				return blockStatementsQl != null;
			case AdaPackage.BLOCK_STATEMENT__BLOCK_EXCEPTION_HANDLERS_QL:
				return blockExceptionHandlersQl != null;
			case AdaPackage.BLOCK_STATEMENT__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //BlockStatementImpl
