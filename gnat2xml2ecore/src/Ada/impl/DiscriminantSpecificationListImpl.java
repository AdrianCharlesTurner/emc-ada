/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.AllCallsRemotePragma;
import Ada.AssertPragma;
import Ada.AssertionPolicyPragma;
import Ada.AsynchronousPragma;
import Ada.AtomicComponentsPragma;
import Ada.AtomicPragma;
import Ada.AttachHandlerPragma;
import Ada.ControlledPragma;
import Ada.ConventionPragma;
import Ada.CpuPragma;
import Ada.DefaultStoragePoolPragma;
import Ada.DetectBlockingPragma;
import Ada.DiscardNamesPragma;
import Ada.DiscriminantSpecification;
import Ada.DiscriminantSpecificationList;
import Ada.DispatchingDomainPragma;
import Ada.ElaborateAllPragma;
import Ada.ElaborateBodyPragma;
import Ada.ElaboratePragma;
import Ada.ExportPragma;
import Ada.ImplementationDefinedPragma;
import Ada.ImportPragma;
import Ada.IndependentComponentsPragma;
import Ada.IndependentPragma;
import Ada.InlinePragma;
import Ada.InspectionPointPragma;
import Ada.InterruptHandlerPragma;
import Ada.InterruptPriorityPragma;
import Ada.LinkerOptionsPragma;
import Ada.ListPragma;
import Ada.LockingPolicyPragma;
import Ada.NoReturnPragma;
import Ada.NormalizeScalarsPragma;
import Ada.NotAnElement;
import Ada.OptimizePragma;
import Ada.PackPragma;
import Ada.PagePragma;
import Ada.PartitionElaborationPolicyPragma;
import Ada.PreelaborableInitializationPragma;
import Ada.PreelaboratePragma;
import Ada.PriorityPragma;
import Ada.PrioritySpecificDispatchingPragma;
import Ada.ProfilePragma;
import Ada.PurePragma;
import Ada.QueuingPolicyPragma;
import Ada.RelativeDeadlinePragma;
import Ada.RemoteCallInterfacePragma;
import Ada.RemoteTypesPragma;
import Ada.RestrictionsPragma;
import Ada.ReviewablePragma;
import Ada.SharedPassivePragma;
import Ada.StorageSizePragma;
import Ada.SuppressPragma;
import Ada.TaskDispatchingPolicyPragma;
import Ada.UncheckedUnionPragma;
import Ada.UnknownPragma;
import Ada.UnsuppressPragma;
import Ada.VolatileComponentsPragma;
import Ada.VolatilePragma;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Discriminant Specification List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getNotAnElement <em>Not An Element</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getDiscriminantSpecification <em>Discriminant Specification</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getAsynchronousPragma <em>Asynchronous Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getAtomicPragma <em>Atomic Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getAttachHandlerPragma <em>Attach Handler Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getControlledPragma <em>Controlled Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getConventionPragma <em>Convention Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getDiscardNamesPragma <em>Discard Names Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getElaboratePragma <em>Elaborate Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getElaborateAllPragma <em>Elaborate All Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getExportPragma <em>Export Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getImportPragma <em>Import Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getInlinePragma <em>Inline Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getInspectionPointPragma <em>Inspection Point Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getLinkerOptionsPragma <em>Linker Options Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getListPragma <em>List Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getLockingPolicyPragma <em>Locking Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getOptimizePragma <em>Optimize Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getPackPragma <em>Pack Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getPagePragma <em>Page Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getPreelaboratePragma <em>Preelaborate Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getPriorityPragma <em>Priority Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getPurePragma <em>Pure Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getRemoteTypesPragma <em>Remote Types Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getRestrictionsPragma <em>Restrictions Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getReviewablePragma <em>Reviewable Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getSharedPassivePragma <em>Shared Passive Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getStorageSizePragma <em>Storage Size Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getSuppressPragma <em>Suppress Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getVolatilePragma <em>Volatile Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getAssertPragma <em>Assert Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getNoReturnPragma <em>No Return Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getProfilePragma <em>Profile Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getUnsuppressPragma <em>Unsuppress Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getCpuPragma <em>Cpu Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getIndependentPragma <em>Independent Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getIndependentComponentsPragma <em>Independent Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantSpecificationListImpl#getUnknownPragma <em>Unknown Pragma</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DiscriminantSpecificationListImpl extends MinimalEObjectImpl.Container implements DiscriminantSpecificationList {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DiscriminantSpecificationListImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getDiscriminantSpecificationList();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NotAnElement> getNotAnElement() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_NotAnElement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscriminantSpecification> getDiscriminantSpecification() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_DiscriminantSpecification());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AllCallsRemotePragma> getAllCallsRemotePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_AllCallsRemotePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AsynchronousPragma> getAsynchronousPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_AsynchronousPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AtomicPragma> getAtomicPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_AtomicPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AtomicComponentsPragma> getAtomicComponentsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_AtomicComponentsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AttachHandlerPragma> getAttachHandlerPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_AttachHandlerPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlledPragma> getControlledPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_ControlledPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConventionPragma> getConventionPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_ConventionPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscardNamesPragma> getDiscardNamesPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_DiscardNamesPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElaboratePragma> getElaboratePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_ElaboratePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElaborateAllPragma> getElaborateAllPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_ElaborateAllPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElaborateBodyPragma> getElaborateBodyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_ElaborateBodyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExportPragma> getExportPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_ExportPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ImportPragma> getImportPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_ImportPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InlinePragma> getInlinePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_InlinePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InspectionPointPragma> getInspectionPointPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_InspectionPointPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InterruptHandlerPragma> getInterruptHandlerPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_InterruptHandlerPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InterruptPriorityPragma> getInterruptPriorityPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_InterruptPriorityPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LinkerOptionsPragma> getLinkerOptionsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_LinkerOptionsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ListPragma> getListPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_ListPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LockingPolicyPragma> getLockingPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_LockingPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NormalizeScalarsPragma> getNormalizeScalarsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_NormalizeScalarsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OptimizePragma> getOptimizePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_OptimizePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackPragma> getPackPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_PackPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PagePragma> getPagePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_PagePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PreelaboratePragma> getPreelaboratePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_PreelaboratePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PriorityPragma> getPriorityPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_PriorityPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PurePragma> getPurePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_PurePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QueuingPolicyPragma> getQueuingPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_QueuingPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RemoteCallInterfacePragma> getRemoteCallInterfacePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_RemoteCallInterfacePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RemoteTypesPragma> getRemoteTypesPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_RemoteTypesPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RestrictionsPragma> getRestrictionsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_RestrictionsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReviewablePragma> getReviewablePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_ReviewablePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SharedPassivePragma> getSharedPassivePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_SharedPassivePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StorageSizePragma> getStorageSizePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_StorageSizePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SuppressPragma> getSuppressPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_SuppressPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskDispatchingPolicyPragma> getTaskDispatchingPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_TaskDispatchingPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VolatilePragma> getVolatilePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_VolatilePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VolatileComponentsPragma> getVolatileComponentsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_VolatileComponentsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssertPragma> getAssertPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_AssertPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssertionPolicyPragma> getAssertionPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_AssertionPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DetectBlockingPragma> getDetectBlockingPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_DetectBlockingPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NoReturnPragma> getNoReturnPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_NoReturnPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PartitionElaborationPolicyPragma> getPartitionElaborationPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_PartitionElaborationPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PreelaborableInitializationPragma> getPreelaborableInitializationPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_PreelaborableInitializationPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PrioritySpecificDispatchingPragma> getPrioritySpecificDispatchingPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_PrioritySpecificDispatchingPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProfilePragma> getProfilePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_ProfilePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RelativeDeadlinePragma> getRelativeDeadlinePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_RelativeDeadlinePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UncheckedUnionPragma> getUncheckedUnionPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_UncheckedUnionPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnsuppressPragma> getUnsuppressPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_UnsuppressPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefaultStoragePoolPragma> getDefaultStoragePoolPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_DefaultStoragePoolPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DispatchingDomainPragma> getDispatchingDomainPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_DispatchingDomainPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CpuPragma> getCpuPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_CpuPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IndependentPragma> getIndependentPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_IndependentPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IndependentComponentsPragma> getIndependentComponentsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_IndependentComponentsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ImplementationDefinedPragma> getImplementationDefinedPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_ImplementationDefinedPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnknownPragma> getUnknownPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDiscriminantSpecificationList_UnknownPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__NOT_AN_ELEMENT:
				return ((InternalEList<?>)getNotAnElement()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__DISCRIMINANT_SPECIFICATION:
				return ((InternalEList<?>)getDiscriminantSpecification()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ALL_CALLS_REMOTE_PRAGMA:
				return ((InternalEList<?>)getAllCallsRemotePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ASYNCHRONOUS_PRAGMA:
				return ((InternalEList<?>)getAsynchronousPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ATOMIC_PRAGMA:
				return ((InternalEList<?>)getAtomicPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ATOMIC_COMPONENTS_PRAGMA:
				return ((InternalEList<?>)getAtomicComponentsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ATTACH_HANDLER_PRAGMA:
				return ((InternalEList<?>)getAttachHandlerPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__CONTROLLED_PRAGMA:
				return ((InternalEList<?>)getControlledPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__CONVENTION_PRAGMA:
				return ((InternalEList<?>)getConventionPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__DISCARD_NAMES_PRAGMA:
				return ((InternalEList<?>)getDiscardNamesPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ELABORATE_PRAGMA:
				return ((InternalEList<?>)getElaboratePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ELABORATE_ALL_PRAGMA:
				return ((InternalEList<?>)getElaborateAllPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ELABORATE_BODY_PRAGMA:
				return ((InternalEList<?>)getElaborateBodyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__EXPORT_PRAGMA:
				return ((InternalEList<?>)getExportPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__IMPORT_PRAGMA:
				return ((InternalEList<?>)getImportPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INLINE_PRAGMA:
				return ((InternalEList<?>)getInlinePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INSPECTION_POINT_PRAGMA:
				return ((InternalEList<?>)getInspectionPointPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INTERRUPT_HANDLER_PRAGMA:
				return ((InternalEList<?>)getInterruptHandlerPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INTERRUPT_PRIORITY_PRAGMA:
				return ((InternalEList<?>)getInterruptPriorityPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__LINKER_OPTIONS_PRAGMA:
				return ((InternalEList<?>)getLinkerOptionsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__LIST_PRAGMA:
				return ((InternalEList<?>)getListPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__LOCKING_POLICY_PRAGMA:
				return ((InternalEList<?>)getLockingPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__NORMALIZE_SCALARS_PRAGMA:
				return ((InternalEList<?>)getNormalizeScalarsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__OPTIMIZE_PRAGMA:
				return ((InternalEList<?>)getOptimizePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PACK_PRAGMA:
				return ((InternalEList<?>)getPackPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PAGE_PRAGMA:
				return ((InternalEList<?>)getPagePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PREELABORATE_PRAGMA:
				return ((InternalEList<?>)getPreelaboratePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PRIORITY_PRAGMA:
				return ((InternalEList<?>)getPriorityPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PURE_PRAGMA:
				return ((InternalEList<?>)getPurePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__QUEUING_POLICY_PRAGMA:
				return ((InternalEList<?>)getQueuingPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				return ((InternalEList<?>)getRemoteCallInterfacePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__REMOTE_TYPES_PRAGMA:
				return ((InternalEList<?>)getRemoteTypesPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__RESTRICTIONS_PRAGMA:
				return ((InternalEList<?>)getRestrictionsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__REVIEWABLE_PRAGMA:
				return ((InternalEList<?>)getReviewablePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__SHARED_PASSIVE_PRAGMA:
				return ((InternalEList<?>)getSharedPassivePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__STORAGE_SIZE_PRAGMA:
				return ((InternalEList<?>)getStorageSizePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__SUPPRESS_PRAGMA:
				return ((InternalEList<?>)getSuppressPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				return ((InternalEList<?>)getTaskDispatchingPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__VOLATILE_PRAGMA:
				return ((InternalEList<?>)getVolatilePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__VOLATILE_COMPONENTS_PRAGMA:
				return ((InternalEList<?>)getVolatileComponentsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ASSERT_PRAGMA:
				return ((InternalEList<?>)getAssertPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ASSERTION_POLICY_PRAGMA:
				return ((InternalEList<?>)getAssertionPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__DETECT_BLOCKING_PRAGMA:
				return ((InternalEList<?>)getDetectBlockingPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__NO_RETURN_PRAGMA:
				return ((InternalEList<?>)getNoReturnPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				return ((InternalEList<?>)getPartitionElaborationPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				return ((InternalEList<?>)getPreelaborableInitializationPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return ((InternalEList<?>)getPrioritySpecificDispatchingPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PROFILE_PRAGMA:
				return ((InternalEList<?>)getProfilePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__RELATIVE_DEADLINE_PRAGMA:
				return ((InternalEList<?>)getRelativeDeadlinePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__UNCHECKED_UNION_PRAGMA:
				return ((InternalEList<?>)getUncheckedUnionPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__UNSUPPRESS_PRAGMA:
				return ((InternalEList<?>)getUnsuppressPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				return ((InternalEList<?>)getDefaultStoragePoolPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__DISPATCHING_DOMAIN_PRAGMA:
				return ((InternalEList<?>)getDispatchingDomainPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__CPU_PRAGMA:
				return ((InternalEList<?>)getCpuPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INDEPENDENT_PRAGMA:
				return ((InternalEList<?>)getIndependentPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				return ((InternalEList<?>)getIndependentComponentsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				return ((InternalEList<?>)getImplementationDefinedPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__UNKNOWN_PRAGMA:
				return ((InternalEList<?>)getUnknownPragma()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__NOT_AN_ELEMENT:
				return getNotAnElement();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__DISCRIMINANT_SPECIFICATION:
				return getDiscriminantSpecification();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ALL_CALLS_REMOTE_PRAGMA:
				return getAllCallsRemotePragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ASYNCHRONOUS_PRAGMA:
				return getAsynchronousPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ATOMIC_PRAGMA:
				return getAtomicPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ATOMIC_COMPONENTS_PRAGMA:
				return getAtomicComponentsPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ATTACH_HANDLER_PRAGMA:
				return getAttachHandlerPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__CONTROLLED_PRAGMA:
				return getControlledPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__CONVENTION_PRAGMA:
				return getConventionPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__DISCARD_NAMES_PRAGMA:
				return getDiscardNamesPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ELABORATE_PRAGMA:
				return getElaboratePragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ELABORATE_ALL_PRAGMA:
				return getElaborateAllPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ELABORATE_BODY_PRAGMA:
				return getElaborateBodyPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__EXPORT_PRAGMA:
				return getExportPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__IMPORT_PRAGMA:
				return getImportPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INLINE_PRAGMA:
				return getInlinePragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INSPECTION_POINT_PRAGMA:
				return getInspectionPointPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INTERRUPT_HANDLER_PRAGMA:
				return getInterruptHandlerPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INTERRUPT_PRIORITY_PRAGMA:
				return getInterruptPriorityPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__LINKER_OPTIONS_PRAGMA:
				return getLinkerOptionsPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__LIST_PRAGMA:
				return getListPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__LOCKING_POLICY_PRAGMA:
				return getLockingPolicyPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__NORMALIZE_SCALARS_PRAGMA:
				return getNormalizeScalarsPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__OPTIMIZE_PRAGMA:
				return getOptimizePragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PACK_PRAGMA:
				return getPackPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PAGE_PRAGMA:
				return getPagePragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PREELABORATE_PRAGMA:
				return getPreelaboratePragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PRIORITY_PRAGMA:
				return getPriorityPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PURE_PRAGMA:
				return getPurePragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__QUEUING_POLICY_PRAGMA:
				return getQueuingPolicyPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				return getRemoteCallInterfacePragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__REMOTE_TYPES_PRAGMA:
				return getRemoteTypesPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__RESTRICTIONS_PRAGMA:
				return getRestrictionsPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__REVIEWABLE_PRAGMA:
				return getReviewablePragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__SHARED_PASSIVE_PRAGMA:
				return getSharedPassivePragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__STORAGE_SIZE_PRAGMA:
				return getStorageSizePragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__SUPPRESS_PRAGMA:
				return getSuppressPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				return getTaskDispatchingPolicyPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__VOLATILE_PRAGMA:
				return getVolatilePragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__VOLATILE_COMPONENTS_PRAGMA:
				return getVolatileComponentsPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ASSERT_PRAGMA:
				return getAssertPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ASSERTION_POLICY_PRAGMA:
				return getAssertionPolicyPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__DETECT_BLOCKING_PRAGMA:
				return getDetectBlockingPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__NO_RETURN_PRAGMA:
				return getNoReturnPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				return getPartitionElaborationPolicyPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				return getPreelaborableInitializationPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return getPrioritySpecificDispatchingPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PROFILE_PRAGMA:
				return getProfilePragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__RELATIVE_DEADLINE_PRAGMA:
				return getRelativeDeadlinePragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__UNCHECKED_UNION_PRAGMA:
				return getUncheckedUnionPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__UNSUPPRESS_PRAGMA:
				return getUnsuppressPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				return getDefaultStoragePoolPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__DISPATCHING_DOMAIN_PRAGMA:
				return getDispatchingDomainPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__CPU_PRAGMA:
				return getCpuPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INDEPENDENT_PRAGMA:
				return getIndependentPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				return getIndependentComponentsPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				return getImplementationDefinedPragma();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__UNKNOWN_PRAGMA:
				return getUnknownPragma();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__NOT_AN_ELEMENT:
				getNotAnElement().clear();
				getNotAnElement().addAll((Collection<? extends NotAnElement>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__DISCRIMINANT_SPECIFICATION:
				getDiscriminantSpecification().clear();
				getDiscriminantSpecification().addAll((Collection<? extends DiscriminantSpecification>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ALL_CALLS_REMOTE_PRAGMA:
				getAllCallsRemotePragma().clear();
				getAllCallsRemotePragma().addAll((Collection<? extends AllCallsRemotePragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ASYNCHRONOUS_PRAGMA:
				getAsynchronousPragma().clear();
				getAsynchronousPragma().addAll((Collection<? extends AsynchronousPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ATOMIC_PRAGMA:
				getAtomicPragma().clear();
				getAtomicPragma().addAll((Collection<? extends AtomicPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ATOMIC_COMPONENTS_PRAGMA:
				getAtomicComponentsPragma().clear();
				getAtomicComponentsPragma().addAll((Collection<? extends AtomicComponentsPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ATTACH_HANDLER_PRAGMA:
				getAttachHandlerPragma().clear();
				getAttachHandlerPragma().addAll((Collection<? extends AttachHandlerPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__CONTROLLED_PRAGMA:
				getControlledPragma().clear();
				getControlledPragma().addAll((Collection<? extends ControlledPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__CONVENTION_PRAGMA:
				getConventionPragma().clear();
				getConventionPragma().addAll((Collection<? extends ConventionPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__DISCARD_NAMES_PRAGMA:
				getDiscardNamesPragma().clear();
				getDiscardNamesPragma().addAll((Collection<? extends DiscardNamesPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ELABORATE_PRAGMA:
				getElaboratePragma().clear();
				getElaboratePragma().addAll((Collection<? extends ElaboratePragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ELABORATE_ALL_PRAGMA:
				getElaborateAllPragma().clear();
				getElaborateAllPragma().addAll((Collection<? extends ElaborateAllPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ELABORATE_BODY_PRAGMA:
				getElaborateBodyPragma().clear();
				getElaborateBodyPragma().addAll((Collection<? extends ElaborateBodyPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__EXPORT_PRAGMA:
				getExportPragma().clear();
				getExportPragma().addAll((Collection<? extends ExportPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__IMPORT_PRAGMA:
				getImportPragma().clear();
				getImportPragma().addAll((Collection<? extends ImportPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INLINE_PRAGMA:
				getInlinePragma().clear();
				getInlinePragma().addAll((Collection<? extends InlinePragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INSPECTION_POINT_PRAGMA:
				getInspectionPointPragma().clear();
				getInspectionPointPragma().addAll((Collection<? extends InspectionPointPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INTERRUPT_HANDLER_PRAGMA:
				getInterruptHandlerPragma().clear();
				getInterruptHandlerPragma().addAll((Collection<? extends InterruptHandlerPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INTERRUPT_PRIORITY_PRAGMA:
				getInterruptPriorityPragma().clear();
				getInterruptPriorityPragma().addAll((Collection<? extends InterruptPriorityPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__LINKER_OPTIONS_PRAGMA:
				getLinkerOptionsPragma().clear();
				getLinkerOptionsPragma().addAll((Collection<? extends LinkerOptionsPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__LIST_PRAGMA:
				getListPragma().clear();
				getListPragma().addAll((Collection<? extends ListPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__LOCKING_POLICY_PRAGMA:
				getLockingPolicyPragma().clear();
				getLockingPolicyPragma().addAll((Collection<? extends LockingPolicyPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__NORMALIZE_SCALARS_PRAGMA:
				getNormalizeScalarsPragma().clear();
				getNormalizeScalarsPragma().addAll((Collection<? extends NormalizeScalarsPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__OPTIMIZE_PRAGMA:
				getOptimizePragma().clear();
				getOptimizePragma().addAll((Collection<? extends OptimizePragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PACK_PRAGMA:
				getPackPragma().clear();
				getPackPragma().addAll((Collection<? extends PackPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PAGE_PRAGMA:
				getPagePragma().clear();
				getPagePragma().addAll((Collection<? extends PagePragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PREELABORATE_PRAGMA:
				getPreelaboratePragma().clear();
				getPreelaboratePragma().addAll((Collection<? extends PreelaboratePragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PRIORITY_PRAGMA:
				getPriorityPragma().clear();
				getPriorityPragma().addAll((Collection<? extends PriorityPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PURE_PRAGMA:
				getPurePragma().clear();
				getPurePragma().addAll((Collection<? extends PurePragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__QUEUING_POLICY_PRAGMA:
				getQueuingPolicyPragma().clear();
				getQueuingPolicyPragma().addAll((Collection<? extends QueuingPolicyPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				getRemoteCallInterfacePragma().clear();
				getRemoteCallInterfacePragma().addAll((Collection<? extends RemoteCallInterfacePragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__REMOTE_TYPES_PRAGMA:
				getRemoteTypesPragma().clear();
				getRemoteTypesPragma().addAll((Collection<? extends RemoteTypesPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__RESTRICTIONS_PRAGMA:
				getRestrictionsPragma().clear();
				getRestrictionsPragma().addAll((Collection<? extends RestrictionsPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__REVIEWABLE_PRAGMA:
				getReviewablePragma().clear();
				getReviewablePragma().addAll((Collection<? extends ReviewablePragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__SHARED_PASSIVE_PRAGMA:
				getSharedPassivePragma().clear();
				getSharedPassivePragma().addAll((Collection<? extends SharedPassivePragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__STORAGE_SIZE_PRAGMA:
				getStorageSizePragma().clear();
				getStorageSizePragma().addAll((Collection<? extends StorageSizePragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__SUPPRESS_PRAGMA:
				getSuppressPragma().clear();
				getSuppressPragma().addAll((Collection<? extends SuppressPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				getTaskDispatchingPolicyPragma().clear();
				getTaskDispatchingPolicyPragma().addAll((Collection<? extends TaskDispatchingPolicyPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__VOLATILE_PRAGMA:
				getVolatilePragma().clear();
				getVolatilePragma().addAll((Collection<? extends VolatilePragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__VOLATILE_COMPONENTS_PRAGMA:
				getVolatileComponentsPragma().clear();
				getVolatileComponentsPragma().addAll((Collection<? extends VolatileComponentsPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ASSERT_PRAGMA:
				getAssertPragma().clear();
				getAssertPragma().addAll((Collection<? extends AssertPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ASSERTION_POLICY_PRAGMA:
				getAssertionPolicyPragma().clear();
				getAssertionPolicyPragma().addAll((Collection<? extends AssertionPolicyPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__DETECT_BLOCKING_PRAGMA:
				getDetectBlockingPragma().clear();
				getDetectBlockingPragma().addAll((Collection<? extends DetectBlockingPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__NO_RETURN_PRAGMA:
				getNoReturnPragma().clear();
				getNoReturnPragma().addAll((Collection<? extends NoReturnPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				getPartitionElaborationPolicyPragma().clear();
				getPartitionElaborationPolicyPragma().addAll((Collection<? extends PartitionElaborationPolicyPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				getPreelaborableInitializationPragma().clear();
				getPreelaborableInitializationPragma().addAll((Collection<? extends PreelaborableInitializationPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				getPrioritySpecificDispatchingPragma().clear();
				getPrioritySpecificDispatchingPragma().addAll((Collection<? extends PrioritySpecificDispatchingPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PROFILE_PRAGMA:
				getProfilePragma().clear();
				getProfilePragma().addAll((Collection<? extends ProfilePragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__RELATIVE_DEADLINE_PRAGMA:
				getRelativeDeadlinePragma().clear();
				getRelativeDeadlinePragma().addAll((Collection<? extends RelativeDeadlinePragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__UNCHECKED_UNION_PRAGMA:
				getUncheckedUnionPragma().clear();
				getUncheckedUnionPragma().addAll((Collection<? extends UncheckedUnionPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__UNSUPPRESS_PRAGMA:
				getUnsuppressPragma().clear();
				getUnsuppressPragma().addAll((Collection<? extends UnsuppressPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				getDefaultStoragePoolPragma().clear();
				getDefaultStoragePoolPragma().addAll((Collection<? extends DefaultStoragePoolPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__DISPATCHING_DOMAIN_PRAGMA:
				getDispatchingDomainPragma().clear();
				getDispatchingDomainPragma().addAll((Collection<? extends DispatchingDomainPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__CPU_PRAGMA:
				getCpuPragma().clear();
				getCpuPragma().addAll((Collection<? extends CpuPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INDEPENDENT_PRAGMA:
				getIndependentPragma().clear();
				getIndependentPragma().addAll((Collection<? extends IndependentPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				getIndependentComponentsPragma().clear();
				getIndependentComponentsPragma().addAll((Collection<? extends IndependentComponentsPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				getImplementationDefinedPragma().clear();
				getImplementationDefinedPragma().addAll((Collection<? extends ImplementationDefinedPragma>)newValue);
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__UNKNOWN_PRAGMA:
				getUnknownPragma().clear();
				getUnknownPragma().addAll((Collection<? extends UnknownPragma>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__GROUP:
				getGroup().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__NOT_AN_ELEMENT:
				getNotAnElement().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__DISCRIMINANT_SPECIFICATION:
				getDiscriminantSpecification().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ALL_CALLS_REMOTE_PRAGMA:
				getAllCallsRemotePragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ASYNCHRONOUS_PRAGMA:
				getAsynchronousPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ATOMIC_PRAGMA:
				getAtomicPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ATOMIC_COMPONENTS_PRAGMA:
				getAtomicComponentsPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ATTACH_HANDLER_PRAGMA:
				getAttachHandlerPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__CONTROLLED_PRAGMA:
				getControlledPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__CONVENTION_PRAGMA:
				getConventionPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__DISCARD_NAMES_PRAGMA:
				getDiscardNamesPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ELABORATE_PRAGMA:
				getElaboratePragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ELABORATE_ALL_PRAGMA:
				getElaborateAllPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ELABORATE_BODY_PRAGMA:
				getElaborateBodyPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__EXPORT_PRAGMA:
				getExportPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__IMPORT_PRAGMA:
				getImportPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INLINE_PRAGMA:
				getInlinePragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INSPECTION_POINT_PRAGMA:
				getInspectionPointPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INTERRUPT_HANDLER_PRAGMA:
				getInterruptHandlerPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INTERRUPT_PRIORITY_PRAGMA:
				getInterruptPriorityPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__LINKER_OPTIONS_PRAGMA:
				getLinkerOptionsPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__LIST_PRAGMA:
				getListPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__LOCKING_POLICY_PRAGMA:
				getLockingPolicyPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__NORMALIZE_SCALARS_PRAGMA:
				getNormalizeScalarsPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__OPTIMIZE_PRAGMA:
				getOptimizePragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PACK_PRAGMA:
				getPackPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PAGE_PRAGMA:
				getPagePragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PREELABORATE_PRAGMA:
				getPreelaboratePragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PRIORITY_PRAGMA:
				getPriorityPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PURE_PRAGMA:
				getPurePragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__QUEUING_POLICY_PRAGMA:
				getQueuingPolicyPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				getRemoteCallInterfacePragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__REMOTE_TYPES_PRAGMA:
				getRemoteTypesPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__RESTRICTIONS_PRAGMA:
				getRestrictionsPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__REVIEWABLE_PRAGMA:
				getReviewablePragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__SHARED_PASSIVE_PRAGMA:
				getSharedPassivePragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__STORAGE_SIZE_PRAGMA:
				getStorageSizePragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__SUPPRESS_PRAGMA:
				getSuppressPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				getTaskDispatchingPolicyPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__VOLATILE_PRAGMA:
				getVolatilePragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__VOLATILE_COMPONENTS_PRAGMA:
				getVolatileComponentsPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ASSERT_PRAGMA:
				getAssertPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ASSERTION_POLICY_PRAGMA:
				getAssertionPolicyPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__DETECT_BLOCKING_PRAGMA:
				getDetectBlockingPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__NO_RETURN_PRAGMA:
				getNoReturnPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				getPartitionElaborationPolicyPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				getPreelaborableInitializationPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				getPrioritySpecificDispatchingPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PROFILE_PRAGMA:
				getProfilePragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__RELATIVE_DEADLINE_PRAGMA:
				getRelativeDeadlinePragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__UNCHECKED_UNION_PRAGMA:
				getUncheckedUnionPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__UNSUPPRESS_PRAGMA:
				getUnsuppressPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				getDefaultStoragePoolPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__DISPATCHING_DOMAIN_PRAGMA:
				getDispatchingDomainPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__CPU_PRAGMA:
				getCpuPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INDEPENDENT_PRAGMA:
				getIndependentPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				getIndependentComponentsPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				getImplementationDefinedPragma().clear();
				return;
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__UNKNOWN_PRAGMA:
				getUnknownPragma().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__GROUP:
				return group != null && !group.isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__NOT_AN_ELEMENT:
				return !getNotAnElement().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__DISCRIMINANT_SPECIFICATION:
				return !getDiscriminantSpecification().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ALL_CALLS_REMOTE_PRAGMA:
				return !getAllCallsRemotePragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ASYNCHRONOUS_PRAGMA:
				return !getAsynchronousPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ATOMIC_PRAGMA:
				return !getAtomicPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ATOMIC_COMPONENTS_PRAGMA:
				return !getAtomicComponentsPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ATTACH_HANDLER_PRAGMA:
				return !getAttachHandlerPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__CONTROLLED_PRAGMA:
				return !getControlledPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__CONVENTION_PRAGMA:
				return !getConventionPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__DISCARD_NAMES_PRAGMA:
				return !getDiscardNamesPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ELABORATE_PRAGMA:
				return !getElaboratePragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ELABORATE_ALL_PRAGMA:
				return !getElaborateAllPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ELABORATE_BODY_PRAGMA:
				return !getElaborateBodyPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__EXPORT_PRAGMA:
				return !getExportPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__IMPORT_PRAGMA:
				return !getImportPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INLINE_PRAGMA:
				return !getInlinePragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INSPECTION_POINT_PRAGMA:
				return !getInspectionPointPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INTERRUPT_HANDLER_PRAGMA:
				return !getInterruptHandlerPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INTERRUPT_PRIORITY_PRAGMA:
				return !getInterruptPriorityPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__LINKER_OPTIONS_PRAGMA:
				return !getLinkerOptionsPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__LIST_PRAGMA:
				return !getListPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__LOCKING_POLICY_PRAGMA:
				return !getLockingPolicyPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__NORMALIZE_SCALARS_PRAGMA:
				return !getNormalizeScalarsPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__OPTIMIZE_PRAGMA:
				return !getOptimizePragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PACK_PRAGMA:
				return !getPackPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PAGE_PRAGMA:
				return !getPagePragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PREELABORATE_PRAGMA:
				return !getPreelaboratePragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PRIORITY_PRAGMA:
				return !getPriorityPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PURE_PRAGMA:
				return !getPurePragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__QUEUING_POLICY_PRAGMA:
				return !getQueuingPolicyPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				return !getRemoteCallInterfacePragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__REMOTE_TYPES_PRAGMA:
				return !getRemoteTypesPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__RESTRICTIONS_PRAGMA:
				return !getRestrictionsPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__REVIEWABLE_PRAGMA:
				return !getReviewablePragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__SHARED_PASSIVE_PRAGMA:
				return !getSharedPassivePragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__STORAGE_SIZE_PRAGMA:
				return !getStorageSizePragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__SUPPRESS_PRAGMA:
				return !getSuppressPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				return !getTaskDispatchingPolicyPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__VOLATILE_PRAGMA:
				return !getVolatilePragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__VOLATILE_COMPONENTS_PRAGMA:
				return !getVolatileComponentsPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ASSERT_PRAGMA:
				return !getAssertPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__ASSERTION_POLICY_PRAGMA:
				return !getAssertionPolicyPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__DETECT_BLOCKING_PRAGMA:
				return !getDetectBlockingPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__NO_RETURN_PRAGMA:
				return !getNoReturnPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				return !getPartitionElaborationPolicyPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				return !getPreelaborableInitializationPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return !getPrioritySpecificDispatchingPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__PROFILE_PRAGMA:
				return !getProfilePragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__RELATIVE_DEADLINE_PRAGMA:
				return !getRelativeDeadlinePragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__UNCHECKED_UNION_PRAGMA:
				return !getUncheckedUnionPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__UNSUPPRESS_PRAGMA:
				return !getUnsuppressPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				return !getDefaultStoragePoolPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__DISPATCHING_DOMAIN_PRAGMA:
				return !getDispatchingDomainPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__CPU_PRAGMA:
				return !getCpuPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INDEPENDENT_PRAGMA:
				return !getIndependentPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				return !getIndependentComponentsPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				return !getImplementationDefinedPragma().isEmpty();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST__UNKNOWN_PRAGMA:
				return !getUnknownPragma().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(')');
		return result.toString();
	}

} //DiscriminantSpecificationListImpl
