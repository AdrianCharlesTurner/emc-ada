with AUnit.Assertions; use AUnit.Assertions;

package body DUKPT.Test is

   procedure Set_Up (T : in out Initial_Test_Data) is
   begin
      T.BDK  := (16#01#, 16#23#, 16#45#, 16#67#,
                 16#89#, 16#AB#, 16#CD#, 16#EF#,
                 16#FE#, 16#DC#, 16#BA#, 16#98#,
                 16#76#, 16#54#, 16#32#, 16#10#);
      T.IKSN := (16#00#, 16#00#, 16#00#, 16#00#,
                 16#00#, 16#00#, 16#00#, 16#00#,
                 16#00#, 16#00#, 16#00#, 16#00#,
                 16#00#, 16#00#, 16#00#, 16#00#);
   end Set_Up;

   procedure Test_Derive_IPEK_001 (T : in out Initial_Test_Data) is
      IPEK_Result : B_Block128;
      IPEK_Known  : B_Block128 := (16#D5#, 16#C8#, 16#25#, 16#A2#,
                                   16#1F#, 16#04#, 16#64#, 16#3B#,
                                   16#43#, 16#E2#, 16#DF#, 16#32#,
                                   16#78#, 16#A7#, 16#62#, 16#F7#);
   begin
      -- Function under test
      Derive_IPEK(T.BDK, T.IKSN, IPEK_Result);
      -- Check consistency
      Assert (IPEK_Result = IPEK_Known, "Incorrect result after IPEK derivation");
   end Test_Derive_IPEK_001;

   procedure Test_DUKPT_Key_1 (T : in out Initial_Test_Data) is
      IPEK          : B_Block128;
      KSN           : B_Block128;
      Encrypt_Key_S : B_Block128;
      MAC_Key_S     : B_Block128;
      Encrypt_Key_R : B_Block128;
      MAC_Key_R     : B_Block128;
   begin
      -- Terminal side
      Derive_IPEK(T.BDK, T.IKSN, IPEK);
      Load_Initial_Keys(IPEK, T.IKSN);
      Get_Sender_Transaction_Keys(KSN, Encrypt_Key_S, MAC_Key_S);
      -- Server side
      Derive_IPEK(T.BDK, KSN, IPEK);
      Retrieve_Transaction_Keys(IPEK, KSN, Encrypt_Key_R, MAC_Key_R);
      -- Check consistency
      Assert (Encrypt_Key_R = Encrypt_Key_S, "Encryption Keys do not match");
      Assert (MAC_Key_R = MAC_Key_S, "MAC Keys do not match");
   end Test_DUKPT_Key_1;

   procedure Test_DUKPT_Key_2 (T : in out Initial_Test_Data) is
      IPEK          : B_Block128;
      KSN           : B_Block128;
      Encrypt_Key_S : B_Block128;
      MAC_Key_S     : B_Block128;
      Encrypt_Key_R : B_Block128;
      MAC_Key_R     : B_Block128;
      Key_Num       : Integer := 2;
   begin
      -- Terminal side
      Derive_IPEK(T.BDK, T.IKSN, IPEK);
      Load_Initial_Keys(IPEK, T.IKSN);
      for A in 1 .. Key_Num loop
         Get_Sender_Transaction_Keys(KSN, Encrypt_Key_S, MAC_Key_S);
      end loop;
      -- Server side
      Derive_IPEK(T.BDK, KSN, IPEK);
      Retrieve_Transaction_Keys(IPEK, KSN, Encrypt_Key_R, MAC_Key_R);
      -- Check consistency
      Assert (Encrypt_Key_R = Encrypt_Key_S, "Encryption Keys do not match");
      Assert (MAC_Key_R = MAC_Key_S, "MAC Keys do not match");
   end Test_DUKPT_Key_2;

   procedure Test_DUKPT_Key_10 (T : in out Initial_Test_Data) is
      IPEK          : B_Block128;
      KSN           : B_Block128;
      Encrypt_Key_S : B_Block128;
      MAC_Key_S     : B_Block128;
      Encrypt_Key_R : B_Block128;
      MAC_Key_R     : B_Block128;
      Key_Num       : Integer := 10;
   begin
      -- Terminal side
      Derive_IPEK(T.BDK, T.IKSN, IPEK);
      Load_Initial_Keys(IPEK, T.IKSN);
      for A in 1 .. Key_Num loop
         Get_Sender_Transaction_Keys(KSN, Encrypt_Key_S, MAC_Key_S);
      end loop;
      -- Server side
      Derive_IPEK(T.BDK, KSN, IPEK);
      Retrieve_Transaction_Keys(IPEK, KSN, Encrypt_Key_R, MAC_Key_R);
      -- Check consistency
      Assert (Encrypt_Key_R = Encrypt_Key_S, "Encryption Keys do not match");
      Assert (MAC_Key_R = MAC_Key_S, "MAC Keys do not match");
   end Test_DUKPT_Key_10;

   procedure Test_DUKPT_Key_2097151 (T : in out Initial_Test_Data) is
      IPEK          : B_Block128;
      KSN           : B_Block128;
      Encrypt_Key_S : B_Block128;
      MAC_Key_S     : B_Block128;
      Encrypt_Key_R : B_Block128;
      MAC_Key_R     : B_Block128;
      Key_Num       : Integer := 2097151;
   begin
      -- Terminal side
      Derive_IPEK(T.BDK, T.IKSN, IPEK);
      Load_Initial_Keys(IPEK, T.IKSN);
      for A in 1 .. Key_Num loop
         Get_Sender_Transaction_Keys(KSN, Encrypt_Key_S, MAC_Key_S);
      end loop;
      -- Server side
      Derive_IPEK(T.BDK, KSN, IPEK);
      Retrieve_Transaction_Keys(IPEK, KSN, Encrypt_Key_R, MAC_Key_R);
      -- Check consistency
      Assert (Encrypt_Key_R = Encrypt_Key_S, "Encryption Keys do not match");
      Assert (MAC_Key_R = MAC_Key_S, "MAC Keys do not match");
   end Test_DUKPT_Key_2097151;

end DUKPT.Test;
