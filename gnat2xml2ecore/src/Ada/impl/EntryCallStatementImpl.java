/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.AssociationList;
import Ada.DefiningNameList;
import Ada.EntryCallStatement;
import Ada.ExpressionClass;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Entry Call Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.EntryCallStatementImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.EntryCallStatementImpl#getLabelNamesQl <em>Label Names Ql</em>}</li>
 *   <li>{@link Ada.impl.EntryCallStatementImpl#getCalledNameQ <em>Called Name Q</em>}</li>
 *   <li>{@link Ada.impl.EntryCallStatementImpl#getCallStatementParametersQl <em>Call Statement Parameters Ql</em>}</li>
 *   <li>{@link Ada.impl.EntryCallStatementImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EntryCallStatementImpl extends MinimalEObjectImpl.Container implements EntryCallStatement {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getLabelNamesQl() <em>Label Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabelNamesQl()
	 * @generated
	 * @ordered
	 */
	protected DefiningNameList labelNamesQl;

	/**
	 * The cached value of the '{@link #getCalledNameQ() <em>Called Name Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalledNameQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass calledNameQ;

	/**
	 * The cached value of the '{@link #getCallStatementParametersQl() <em>Call Statement Parameters Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCallStatementParametersQl()
	 * @generated
	 * @ordered
	 */
	protected AssociationList callStatementParametersQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EntryCallStatementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getEntryCallStatement();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_CALL_STATEMENT__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_CALL_STATEMENT__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_CALL_STATEMENT__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_CALL_STATEMENT__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNameList getLabelNamesQl() {
		return labelNamesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLabelNamesQl(DefiningNameList newLabelNamesQl, NotificationChain msgs) {
		DefiningNameList oldLabelNamesQl = labelNamesQl;
		labelNamesQl = newLabelNamesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_CALL_STATEMENT__LABEL_NAMES_QL, oldLabelNamesQl, newLabelNamesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabelNamesQl(DefiningNameList newLabelNamesQl) {
		if (newLabelNamesQl != labelNamesQl) {
			NotificationChain msgs = null;
			if (labelNamesQl != null)
				msgs = ((InternalEObject)labelNamesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_CALL_STATEMENT__LABEL_NAMES_QL, null, msgs);
			if (newLabelNamesQl != null)
				msgs = ((InternalEObject)newLabelNamesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_CALL_STATEMENT__LABEL_NAMES_QL, null, msgs);
			msgs = basicSetLabelNamesQl(newLabelNamesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_CALL_STATEMENT__LABEL_NAMES_QL, newLabelNamesQl, newLabelNamesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getCalledNameQ() {
		return calledNameQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCalledNameQ(ExpressionClass newCalledNameQ, NotificationChain msgs) {
		ExpressionClass oldCalledNameQ = calledNameQ;
		calledNameQ = newCalledNameQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_CALL_STATEMENT__CALLED_NAME_Q, oldCalledNameQ, newCalledNameQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCalledNameQ(ExpressionClass newCalledNameQ) {
		if (newCalledNameQ != calledNameQ) {
			NotificationChain msgs = null;
			if (calledNameQ != null)
				msgs = ((InternalEObject)calledNameQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_CALL_STATEMENT__CALLED_NAME_Q, null, msgs);
			if (newCalledNameQ != null)
				msgs = ((InternalEObject)newCalledNameQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_CALL_STATEMENT__CALLED_NAME_Q, null, msgs);
			msgs = basicSetCalledNameQ(newCalledNameQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_CALL_STATEMENT__CALLED_NAME_Q, newCalledNameQ, newCalledNameQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationList getCallStatementParametersQl() {
		return callStatementParametersQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCallStatementParametersQl(AssociationList newCallStatementParametersQl, NotificationChain msgs) {
		AssociationList oldCallStatementParametersQl = callStatementParametersQl;
		callStatementParametersQl = newCallStatementParametersQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_CALL_STATEMENT__CALL_STATEMENT_PARAMETERS_QL, oldCallStatementParametersQl, newCallStatementParametersQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCallStatementParametersQl(AssociationList newCallStatementParametersQl) {
		if (newCallStatementParametersQl != callStatementParametersQl) {
			NotificationChain msgs = null;
			if (callStatementParametersQl != null)
				msgs = ((InternalEObject)callStatementParametersQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_CALL_STATEMENT__CALL_STATEMENT_PARAMETERS_QL, null, msgs);
			if (newCallStatementParametersQl != null)
				msgs = ((InternalEObject)newCallStatementParametersQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_CALL_STATEMENT__CALL_STATEMENT_PARAMETERS_QL, null, msgs);
			msgs = basicSetCallStatementParametersQl(newCallStatementParametersQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_CALL_STATEMENT__CALL_STATEMENT_PARAMETERS_QL, newCallStatementParametersQl, newCallStatementParametersQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_CALL_STATEMENT__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.ENTRY_CALL_STATEMENT__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.ENTRY_CALL_STATEMENT__LABEL_NAMES_QL:
				return basicSetLabelNamesQl(null, msgs);
			case AdaPackage.ENTRY_CALL_STATEMENT__CALLED_NAME_Q:
				return basicSetCalledNameQ(null, msgs);
			case AdaPackage.ENTRY_CALL_STATEMENT__CALL_STATEMENT_PARAMETERS_QL:
				return basicSetCallStatementParametersQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.ENTRY_CALL_STATEMENT__SLOC:
				return getSloc();
			case AdaPackage.ENTRY_CALL_STATEMENT__LABEL_NAMES_QL:
				return getLabelNamesQl();
			case AdaPackage.ENTRY_CALL_STATEMENT__CALLED_NAME_Q:
				return getCalledNameQ();
			case AdaPackage.ENTRY_CALL_STATEMENT__CALL_STATEMENT_PARAMETERS_QL:
				return getCallStatementParametersQl();
			case AdaPackage.ENTRY_CALL_STATEMENT__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.ENTRY_CALL_STATEMENT__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.ENTRY_CALL_STATEMENT__LABEL_NAMES_QL:
				setLabelNamesQl((DefiningNameList)newValue);
				return;
			case AdaPackage.ENTRY_CALL_STATEMENT__CALLED_NAME_Q:
				setCalledNameQ((ExpressionClass)newValue);
				return;
			case AdaPackage.ENTRY_CALL_STATEMENT__CALL_STATEMENT_PARAMETERS_QL:
				setCallStatementParametersQl((AssociationList)newValue);
				return;
			case AdaPackage.ENTRY_CALL_STATEMENT__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.ENTRY_CALL_STATEMENT__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.ENTRY_CALL_STATEMENT__LABEL_NAMES_QL:
				setLabelNamesQl((DefiningNameList)null);
				return;
			case AdaPackage.ENTRY_CALL_STATEMENT__CALLED_NAME_Q:
				setCalledNameQ((ExpressionClass)null);
				return;
			case AdaPackage.ENTRY_CALL_STATEMENT__CALL_STATEMENT_PARAMETERS_QL:
				setCallStatementParametersQl((AssociationList)null);
				return;
			case AdaPackage.ENTRY_CALL_STATEMENT__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.ENTRY_CALL_STATEMENT__SLOC:
				return sloc != null;
			case AdaPackage.ENTRY_CALL_STATEMENT__LABEL_NAMES_QL:
				return labelNamesQl != null;
			case AdaPackage.ENTRY_CALL_STATEMENT__CALLED_NAME_Q:
				return calledNameQ != null;
			case AdaPackage.ENTRY_CALL_STATEMENT__CALL_STATEMENT_PARAMETERS_QL:
				return callStatementParametersQl != null;
			case AdaPackage.ENTRY_CALL_STATEMENT__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //EntryCallStatementImpl
