/**
 */
package Ada.impl;

import Ada.AccessToConstant;
import Ada.AdaPackage;
import Ada.ElementClass;
import Ada.HasNullExclusionQType18;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Access To Constant</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.AccessToConstantImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.AccessToConstantImpl#getHasNullExclusionQ <em>Has Null Exclusion Q</em>}</li>
 *   <li>{@link Ada.impl.AccessToConstantImpl#getAccessToObjectDefinitionQ <em>Access To Object Definition Q</em>}</li>
 *   <li>{@link Ada.impl.AccessToConstantImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AccessToConstantImpl extends MinimalEObjectImpl.Container implements AccessToConstant {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getHasNullExclusionQ() <em>Has Null Exclusion Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasNullExclusionQ()
	 * @generated
	 * @ordered
	 */
	protected HasNullExclusionQType18 hasNullExclusionQ;

	/**
	 * The cached value of the '{@link #getAccessToObjectDefinitionQ() <em>Access To Object Definition Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessToObjectDefinitionQ()
	 * @generated
	 * @ordered
	 */
	protected ElementClass accessToObjectDefinitionQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AccessToConstantImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getAccessToConstant();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ACCESS_TO_CONSTANT__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ACCESS_TO_CONSTANT__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ACCESS_TO_CONSTANT__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ACCESS_TO_CONSTANT__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType18 getHasNullExclusionQ() {
		return hasNullExclusionQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasNullExclusionQ(HasNullExclusionQType18 newHasNullExclusionQ, NotificationChain msgs) {
		HasNullExclusionQType18 oldHasNullExclusionQ = hasNullExclusionQ;
		hasNullExclusionQ = newHasNullExclusionQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ACCESS_TO_CONSTANT__HAS_NULL_EXCLUSION_Q, oldHasNullExclusionQ, newHasNullExclusionQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasNullExclusionQ(HasNullExclusionQType18 newHasNullExclusionQ) {
		if (newHasNullExclusionQ != hasNullExclusionQ) {
			NotificationChain msgs = null;
			if (hasNullExclusionQ != null)
				msgs = ((InternalEObject)hasNullExclusionQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ACCESS_TO_CONSTANT__HAS_NULL_EXCLUSION_Q, null, msgs);
			if (newHasNullExclusionQ != null)
				msgs = ((InternalEObject)newHasNullExclusionQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ACCESS_TO_CONSTANT__HAS_NULL_EXCLUSION_Q, null, msgs);
			msgs = basicSetHasNullExclusionQ(newHasNullExclusionQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ACCESS_TO_CONSTANT__HAS_NULL_EXCLUSION_Q, newHasNullExclusionQ, newHasNullExclusionQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementClass getAccessToObjectDefinitionQ() {
		return accessToObjectDefinitionQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccessToObjectDefinitionQ(ElementClass newAccessToObjectDefinitionQ, NotificationChain msgs) {
		ElementClass oldAccessToObjectDefinitionQ = accessToObjectDefinitionQ;
		accessToObjectDefinitionQ = newAccessToObjectDefinitionQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ACCESS_TO_CONSTANT__ACCESS_TO_OBJECT_DEFINITION_Q, oldAccessToObjectDefinitionQ, newAccessToObjectDefinitionQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessToObjectDefinitionQ(ElementClass newAccessToObjectDefinitionQ) {
		if (newAccessToObjectDefinitionQ != accessToObjectDefinitionQ) {
			NotificationChain msgs = null;
			if (accessToObjectDefinitionQ != null)
				msgs = ((InternalEObject)accessToObjectDefinitionQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ACCESS_TO_CONSTANT__ACCESS_TO_OBJECT_DEFINITION_Q, null, msgs);
			if (newAccessToObjectDefinitionQ != null)
				msgs = ((InternalEObject)newAccessToObjectDefinitionQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ACCESS_TO_CONSTANT__ACCESS_TO_OBJECT_DEFINITION_Q, null, msgs);
			msgs = basicSetAccessToObjectDefinitionQ(newAccessToObjectDefinitionQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ACCESS_TO_CONSTANT__ACCESS_TO_OBJECT_DEFINITION_Q, newAccessToObjectDefinitionQ, newAccessToObjectDefinitionQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ACCESS_TO_CONSTANT__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.ACCESS_TO_CONSTANT__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.ACCESS_TO_CONSTANT__HAS_NULL_EXCLUSION_Q:
				return basicSetHasNullExclusionQ(null, msgs);
			case AdaPackage.ACCESS_TO_CONSTANT__ACCESS_TO_OBJECT_DEFINITION_Q:
				return basicSetAccessToObjectDefinitionQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.ACCESS_TO_CONSTANT__SLOC:
				return getSloc();
			case AdaPackage.ACCESS_TO_CONSTANT__HAS_NULL_EXCLUSION_Q:
				return getHasNullExclusionQ();
			case AdaPackage.ACCESS_TO_CONSTANT__ACCESS_TO_OBJECT_DEFINITION_Q:
				return getAccessToObjectDefinitionQ();
			case AdaPackage.ACCESS_TO_CONSTANT__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.ACCESS_TO_CONSTANT__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.ACCESS_TO_CONSTANT__HAS_NULL_EXCLUSION_Q:
				setHasNullExclusionQ((HasNullExclusionQType18)newValue);
				return;
			case AdaPackage.ACCESS_TO_CONSTANT__ACCESS_TO_OBJECT_DEFINITION_Q:
				setAccessToObjectDefinitionQ((ElementClass)newValue);
				return;
			case AdaPackage.ACCESS_TO_CONSTANT__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.ACCESS_TO_CONSTANT__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.ACCESS_TO_CONSTANT__HAS_NULL_EXCLUSION_Q:
				setHasNullExclusionQ((HasNullExclusionQType18)null);
				return;
			case AdaPackage.ACCESS_TO_CONSTANT__ACCESS_TO_OBJECT_DEFINITION_Q:
				setAccessToObjectDefinitionQ((ElementClass)null);
				return;
			case AdaPackage.ACCESS_TO_CONSTANT__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.ACCESS_TO_CONSTANT__SLOC:
				return sloc != null;
			case AdaPackage.ACCESS_TO_CONSTANT__HAS_NULL_EXCLUSION_Q:
				return hasNullExclusionQ != null;
			case AdaPackage.ACCESS_TO_CONSTANT__ACCESS_TO_OBJECT_DEFINITION_Q:
				return accessToObjectDefinitionQ != null;
			case AdaPackage.ACCESS_TO_CONSTANT__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //AccessToConstantImpl
