/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Clause</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.ComponentClause#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.ComponentClause#getRepresentationClauseNameQ <em>Representation Clause Name Q</em>}</li>
 *   <li>{@link Ada.ComponentClause#getComponentClausePositionQ <em>Component Clause Position Q</em>}</li>
 *   <li>{@link Ada.ComponentClause#getComponentClauseRangeQ <em>Component Clause Range Q</em>}</li>
 *   <li>{@link Ada.ComponentClause#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getComponentClause()
 * @model extendedMetaData="name='Component_Clause' kind='elementOnly'"
 * @generated
 */
public interface ComponentClause extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getComponentClause_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.ComponentClause#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Representation Clause Name Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Representation Clause Name Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Representation Clause Name Q</em>' containment reference.
	 * @see #setRepresentationClauseNameQ(NameClass)
	 * @see Ada.AdaPackage#getComponentClause_RepresentationClauseNameQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='representation_clause_name_q' namespace='##targetNamespace'"
	 * @generated
	 */
	NameClass getRepresentationClauseNameQ();

	/**
	 * Sets the value of the '{@link Ada.ComponentClause#getRepresentationClauseNameQ <em>Representation Clause Name Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Representation Clause Name Q</em>' containment reference.
	 * @see #getRepresentationClauseNameQ()
	 * @generated
	 */
	void setRepresentationClauseNameQ(NameClass value);

	/**
	 * Returns the value of the '<em><b>Component Clause Position Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Clause Position Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Clause Position Q</em>' containment reference.
	 * @see #setComponentClausePositionQ(ExpressionClass)
	 * @see Ada.AdaPackage#getComponentClause_ComponentClausePositionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='component_clause_position_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getComponentClausePositionQ();

	/**
	 * Sets the value of the '{@link Ada.ComponentClause#getComponentClausePositionQ <em>Component Clause Position Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component Clause Position Q</em>' containment reference.
	 * @see #getComponentClausePositionQ()
	 * @generated
	 */
	void setComponentClausePositionQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Component Clause Range Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Clause Range Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Clause Range Q</em>' containment reference.
	 * @see #setComponentClauseRangeQ(DiscreteRangeClass)
	 * @see Ada.AdaPackage#getComponentClause_ComponentClauseRangeQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='component_clause_range_q' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscreteRangeClass getComponentClauseRangeQ();

	/**
	 * Sets the value of the '{@link Ada.ComponentClause#getComponentClauseRangeQ <em>Component Clause Range Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component Clause Range Q</em>' containment reference.
	 * @see #getComponentClauseRangeQ()
	 * @generated
	 */
	void setComponentClauseRangeQ(DiscreteRangeClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getComponentClause_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.ComponentClause#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // ComponentClause
