/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.DefiningNameList;
import Ada.DiscreteSubtypeDefinitionClass;
import Ada.EntryIndexSpecification;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Entry Index Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.EntryIndexSpecificationImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.EntryIndexSpecificationImpl#getNamesQl <em>Names Ql</em>}</li>
 *   <li>{@link Ada.impl.EntryIndexSpecificationImpl#getSpecificationSubtypeDefinitionQ <em>Specification Subtype Definition Q</em>}</li>
 *   <li>{@link Ada.impl.EntryIndexSpecificationImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EntryIndexSpecificationImpl extends MinimalEObjectImpl.Container implements EntryIndexSpecification {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getNamesQl() <em>Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamesQl()
	 * @generated
	 * @ordered
	 */
	protected DefiningNameList namesQl;

	/**
	 * The cached value of the '{@link #getSpecificationSubtypeDefinitionQ() <em>Specification Subtype Definition Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecificationSubtypeDefinitionQ()
	 * @generated
	 * @ordered
	 */
	protected DiscreteSubtypeDefinitionClass specificationSubtypeDefinitionQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EntryIndexSpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getEntryIndexSpecification();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_INDEX_SPECIFICATION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_INDEX_SPECIFICATION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_INDEX_SPECIFICATION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_INDEX_SPECIFICATION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNameList getNamesQl() {
		return namesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNamesQl(DefiningNameList newNamesQl, NotificationChain msgs) {
		DefiningNameList oldNamesQl = namesQl;
		namesQl = newNamesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_INDEX_SPECIFICATION__NAMES_QL, oldNamesQl, newNamesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNamesQl(DefiningNameList newNamesQl) {
		if (newNamesQl != namesQl) {
			NotificationChain msgs = null;
			if (namesQl != null)
				msgs = ((InternalEObject)namesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_INDEX_SPECIFICATION__NAMES_QL, null, msgs);
			if (newNamesQl != null)
				msgs = ((InternalEObject)newNamesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_INDEX_SPECIFICATION__NAMES_QL, null, msgs);
			msgs = basicSetNamesQl(newNamesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_INDEX_SPECIFICATION__NAMES_QL, newNamesQl, newNamesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscreteSubtypeDefinitionClass getSpecificationSubtypeDefinitionQ() {
		return specificationSubtypeDefinitionQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSpecificationSubtypeDefinitionQ(DiscreteSubtypeDefinitionClass newSpecificationSubtypeDefinitionQ, NotificationChain msgs) {
		DiscreteSubtypeDefinitionClass oldSpecificationSubtypeDefinitionQ = specificationSubtypeDefinitionQ;
		specificationSubtypeDefinitionQ = newSpecificationSubtypeDefinitionQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_INDEX_SPECIFICATION__SPECIFICATION_SUBTYPE_DEFINITION_Q, oldSpecificationSubtypeDefinitionQ, newSpecificationSubtypeDefinitionQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpecificationSubtypeDefinitionQ(DiscreteSubtypeDefinitionClass newSpecificationSubtypeDefinitionQ) {
		if (newSpecificationSubtypeDefinitionQ != specificationSubtypeDefinitionQ) {
			NotificationChain msgs = null;
			if (specificationSubtypeDefinitionQ != null)
				msgs = ((InternalEObject)specificationSubtypeDefinitionQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_INDEX_SPECIFICATION__SPECIFICATION_SUBTYPE_DEFINITION_Q, null, msgs);
			if (newSpecificationSubtypeDefinitionQ != null)
				msgs = ((InternalEObject)newSpecificationSubtypeDefinitionQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_INDEX_SPECIFICATION__SPECIFICATION_SUBTYPE_DEFINITION_Q, null, msgs);
			msgs = basicSetSpecificationSubtypeDefinitionQ(newSpecificationSubtypeDefinitionQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_INDEX_SPECIFICATION__SPECIFICATION_SUBTYPE_DEFINITION_Q, newSpecificationSubtypeDefinitionQ, newSpecificationSubtypeDefinitionQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_INDEX_SPECIFICATION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.ENTRY_INDEX_SPECIFICATION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.ENTRY_INDEX_SPECIFICATION__NAMES_QL:
				return basicSetNamesQl(null, msgs);
			case AdaPackage.ENTRY_INDEX_SPECIFICATION__SPECIFICATION_SUBTYPE_DEFINITION_Q:
				return basicSetSpecificationSubtypeDefinitionQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.ENTRY_INDEX_SPECIFICATION__SLOC:
				return getSloc();
			case AdaPackage.ENTRY_INDEX_SPECIFICATION__NAMES_QL:
				return getNamesQl();
			case AdaPackage.ENTRY_INDEX_SPECIFICATION__SPECIFICATION_SUBTYPE_DEFINITION_Q:
				return getSpecificationSubtypeDefinitionQ();
			case AdaPackage.ENTRY_INDEX_SPECIFICATION__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.ENTRY_INDEX_SPECIFICATION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.ENTRY_INDEX_SPECIFICATION__NAMES_QL:
				setNamesQl((DefiningNameList)newValue);
				return;
			case AdaPackage.ENTRY_INDEX_SPECIFICATION__SPECIFICATION_SUBTYPE_DEFINITION_Q:
				setSpecificationSubtypeDefinitionQ((DiscreteSubtypeDefinitionClass)newValue);
				return;
			case AdaPackage.ENTRY_INDEX_SPECIFICATION__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.ENTRY_INDEX_SPECIFICATION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.ENTRY_INDEX_SPECIFICATION__NAMES_QL:
				setNamesQl((DefiningNameList)null);
				return;
			case AdaPackage.ENTRY_INDEX_SPECIFICATION__SPECIFICATION_SUBTYPE_DEFINITION_Q:
				setSpecificationSubtypeDefinitionQ((DiscreteSubtypeDefinitionClass)null);
				return;
			case AdaPackage.ENTRY_INDEX_SPECIFICATION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.ENTRY_INDEX_SPECIFICATION__SLOC:
				return sloc != null;
			case AdaPackage.ENTRY_INDEX_SPECIFICATION__NAMES_QL:
				return namesQl != null;
			case AdaPackage.ENTRY_INDEX_SPECIFICATION__SPECIFICATION_SUBTYPE_DEFINITION_Q:
				return specificationSubtypeDefinitionQ != null;
			case AdaPackage.ENTRY_INDEX_SPECIFICATION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //EntryIndexSpecificationImpl
