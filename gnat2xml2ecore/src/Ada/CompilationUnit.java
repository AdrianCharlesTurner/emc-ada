/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Compilation Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.CompilationUnit#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.CompilationUnit#getContextClauseElementsQl <em>Context Clause Elements Ql</em>}</li>
 *   <li>{@link Ada.CompilationUnit#getUnitDeclarationQ <em>Unit Declaration Q</em>}</li>
 *   <li>{@link Ada.CompilationUnit#getPragmasAfterQl <em>Pragmas After Ql</em>}</li>
 *   <li>{@link Ada.CompilationUnit#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.CompilationUnit#getDefName <em>Def Name</em>}</li>
 *   <li>{@link Ada.CompilationUnit#getSourceFile <em>Source File</em>}</li>
 *   <li>{@link Ada.CompilationUnit#getUnitClass <em>Unit Class</em>}</li>
 *   <li>{@link Ada.CompilationUnit#getUnitFullName <em>Unit Full Name</em>}</li>
 *   <li>{@link Ada.CompilationUnit#getUnitKind <em>Unit Kind</em>}</li>
 *   <li>{@link Ada.CompilationUnit#getUnitOrigin <em>Unit Origin</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getCompilationUnit()
 * @model extendedMetaData="name='Compilation_Unit' kind='elementOnly'"
 * @generated
 */
public interface CompilationUnit extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getCompilationUnit_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.CompilationUnit#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Context Clause Elements Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Context Clause Elements Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Context Clause Elements Ql</em>' containment reference.
	 * @see #setContextClauseElementsQl(ContextClauseList)
	 * @see Ada.AdaPackage#getCompilationUnit_ContextClauseElementsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='context_clause_elements_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ContextClauseList getContextClauseElementsQl();

	/**
	 * Sets the value of the '{@link Ada.CompilationUnit#getContextClauseElementsQl <em>Context Clause Elements Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Context Clause Elements Ql</em>' containment reference.
	 * @see #getContextClauseElementsQl()
	 * @generated
	 */
	void setContextClauseElementsQl(ContextClauseList value);

	/**
	 * Returns the value of the '<em><b>Unit Declaration Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit Declaration Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit Declaration Q</em>' containment reference.
	 * @see #setUnitDeclarationQ(DeclarationClass)
	 * @see Ada.AdaPackage#getCompilationUnit_UnitDeclarationQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='unit_declaration_q' namespace='##targetNamespace'"
	 * @generated
	 */
	DeclarationClass getUnitDeclarationQ();

	/**
	 * Sets the value of the '{@link Ada.CompilationUnit#getUnitDeclarationQ <em>Unit Declaration Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit Declaration Q</em>' containment reference.
	 * @see #getUnitDeclarationQ()
	 * @generated
	 */
	void setUnitDeclarationQ(DeclarationClass value);

	/**
	 * Returns the value of the '<em><b>Pragmas After Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pragmas After Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pragmas After Ql</em>' containment reference.
	 * @see #setPragmasAfterQl(ElementList)
	 * @see Ada.AdaPackage#getCompilationUnit_PragmasAfterQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='pragmas_after_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementList getPragmasAfterQl();

	/**
	 * Sets the value of the '{@link Ada.CompilationUnit#getPragmasAfterQl <em>Pragmas After Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pragmas After Ql</em>' containment reference.
	 * @see #getPragmasAfterQl()
	 * @generated
	 */
	void setPragmasAfterQl(ElementList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getCompilationUnit_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.CompilationUnit#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

	/**
	 * Returns the value of the '<em><b>Def Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Def Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Def Name</em>' attribute.
	 * @see #setDefName(String)
	 * @see Ada.AdaPackage#getCompilationUnit_DefName()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='def_name' namespace='##targetNamespace'"
	 * @generated
	 */
	String getDefName();

	/**
	 * Sets the value of the '{@link Ada.CompilationUnit#getDefName <em>Def Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Def Name</em>' attribute.
	 * @see #getDefName()
	 * @generated
	 */
	void setDefName(String value);

	/**
	 * Returns the value of the '<em><b>Source File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source File</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source File</em>' attribute.
	 * @see #setSourceFile(String)
	 * @see Ada.AdaPackage#getCompilationUnit_SourceFile()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='source_file' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSourceFile();

	/**
	 * Sets the value of the '{@link Ada.CompilationUnit#getSourceFile <em>Source File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source File</em>' attribute.
	 * @see #getSourceFile()
	 * @generated
	 */
	void setSourceFile(String value);

	/**
	 * Returns the value of the '<em><b>Unit Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit Class</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit Class</em>' attribute.
	 * @see #setUnitClass(String)
	 * @see Ada.AdaPackage#getCompilationUnit_UnitClass()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='unit_class' namespace='##targetNamespace'"
	 * @generated
	 */
	String getUnitClass();

	/**
	 * Sets the value of the '{@link Ada.CompilationUnit#getUnitClass <em>Unit Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit Class</em>' attribute.
	 * @see #getUnitClass()
	 * @generated
	 */
	void setUnitClass(String value);

	/**
	 * Returns the value of the '<em><b>Unit Full Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit Full Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit Full Name</em>' attribute.
	 * @see #setUnitFullName(String)
	 * @see Ada.AdaPackage#getCompilationUnit_UnitFullName()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='unit_full_name' namespace='##targetNamespace'"
	 * @generated
	 */
	String getUnitFullName();

	/**
	 * Sets the value of the '{@link Ada.CompilationUnit#getUnitFullName <em>Unit Full Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit Full Name</em>' attribute.
	 * @see #getUnitFullName()
	 * @generated
	 */
	void setUnitFullName(String value);

	/**
	 * Returns the value of the '<em><b>Unit Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit Kind</em>' attribute.
	 * @see #setUnitKind(String)
	 * @see Ada.AdaPackage#getCompilationUnit_UnitKind()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='unit_kind' namespace='##targetNamespace'"
	 * @generated
	 */
	String getUnitKind();

	/**
	 * Sets the value of the '{@link Ada.CompilationUnit#getUnitKind <em>Unit Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit Kind</em>' attribute.
	 * @see #getUnitKind()
	 * @generated
	 */
	void setUnitKind(String value);

	/**
	 * Returns the value of the '<em><b>Unit Origin</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit Origin</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit Origin</em>' attribute.
	 * @see #setUnitOrigin(String)
	 * @see Ada.AdaPackage#getCompilationUnit_UnitOrigin()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='unit_origin' namespace='##targetNamespace'"
	 * @generated
	 */
	String getUnitOrigin();

	/**
	 * Sets the value of the '{@link Ada.CompilationUnit#getUnitOrigin <em>Unit Origin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit Origin</em>' attribute.
	 * @see #getUnitOrigin()
	 * @generated
	 */
	void setUnitOrigin(String value);

} // CompilationUnit
