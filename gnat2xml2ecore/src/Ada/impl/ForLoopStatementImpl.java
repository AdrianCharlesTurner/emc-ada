/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.DeclarationClass;
import Ada.DefiningNameClass;
import Ada.DefiningNameList;
import Ada.ForLoopStatement;
import Ada.SourceLocation;
import Ada.StatementList;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>For Loop Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.ForLoopStatementImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.ForLoopStatementImpl#getLabelNamesQl <em>Label Names Ql</em>}</li>
 *   <li>{@link Ada.impl.ForLoopStatementImpl#getStatementIdentifierQ <em>Statement Identifier Q</em>}</li>
 *   <li>{@link Ada.impl.ForLoopStatementImpl#getForLoopParameterSpecificationQ <em>For Loop Parameter Specification Q</em>}</li>
 *   <li>{@link Ada.impl.ForLoopStatementImpl#getLoopStatementsQl <em>Loop Statements Ql</em>}</li>
 *   <li>{@link Ada.impl.ForLoopStatementImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ForLoopStatementImpl extends MinimalEObjectImpl.Container implements ForLoopStatement {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getLabelNamesQl() <em>Label Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabelNamesQl()
	 * @generated
	 * @ordered
	 */
	protected DefiningNameList labelNamesQl;

	/**
	 * The cached value of the '{@link #getStatementIdentifierQ() <em>Statement Identifier Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatementIdentifierQ()
	 * @generated
	 * @ordered
	 */
	protected DefiningNameClass statementIdentifierQ;

	/**
	 * The cached value of the '{@link #getForLoopParameterSpecificationQ() <em>For Loop Parameter Specification Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getForLoopParameterSpecificationQ()
	 * @generated
	 * @ordered
	 */
	protected DeclarationClass forLoopParameterSpecificationQ;

	/**
	 * The cached value of the '{@link #getLoopStatementsQl() <em>Loop Statements Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoopStatementsQl()
	 * @generated
	 * @ordered
	 */
	protected StatementList loopStatementsQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ForLoopStatementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getForLoopStatement();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FOR_LOOP_STATEMENT__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FOR_LOOP_STATEMENT__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FOR_LOOP_STATEMENT__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FOR_LOOP_STATEMENT__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNameList getLabelNamesQl() {
		return labelNamesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLabelNamesQl(DefiningNameList newLabelNamesQl, NotificationChain msgs) {
		DefiningNameList oldLabelNamesQl = labelNamesQl;
		labelNamesQl = newLabelNamesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FOR_LOOP_STATEMENT__LABEL_NAMES_QL, oldLabelNamesQl, newLabelNamesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabelNamesQl(DefiningNameList newLabelNamesQl) {
		if (newLabelNamesQl != labelNamesQl) {
			NotificationChain msgs = null;
			if (labelNamesQl != null)
				msgs = ((InternalEObject)labelNamesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FOR_LOOP_STATEMENT__LABEL_NAMES_QL, null, msgs);
			if (newLabelNamesQl != null)
				msgs = ((InternalEObject)newLabelNamesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FOR_LOOP_STATEMENT__LABEL_NAMES_QL, null, msgs);
			msgs = basicSetLabelNamesQl(newLabelNamesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FOR_LOOP_STATEMENT__LABEL_NAMES_QL, newLabelNamesQl, newLabelNamesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNameClass getStatementIdentifierQ() {
		return statementIdentifierQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStatementIdentifierQ(DefiningNameClass newStatementIdentifierQ, NotificationChain msgs) {
		DefiningNameClass oldStatementIdentifierQ = statementIdentifierQ;
		statementIdentifierQ = newStatementIdentifierQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FOR_LOOP_STATEMENT__STATEMENT_IDENTIFIER_Q, oldStatementIdentifierQ, newStatementIdentifierQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStatementIdentifierQ(DefiningNameClass newStatementIdentifierQ) {
		if (newStatementIdentifierQ != statementIdentifierQ) {
			NotificationChain msgs = null;
			if (statementIdentifierQ != null)
				msgs = ((InternalEObject)statementIdentifierQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FOR_LOOP_STATEMENT__STATEMENT_IDENTIFIER_Q, null, msgs);
			if (newStatementIdentifierQ != null)
				msgs = ((InternalEObject)newStatementIdentifierQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FOR_LOOP_STATEMENT__STATEMENT_IDENTIFIER_Q, null, msgs);
			msgs = basicSetStatementIdentifierQ(newStatementIdentifierQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FOR_LOOP_STATEMENT__STATEMENT_IDENTIFIER_Q, newStatementIdentifierQ, newStatementIdentifierQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeclarationClass getForLoopParameterSpecificationQ() {
		return forLoopParameterSpecificationQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetForLoopParameterSpecificationQ(DeclarationClass newForLoopParameterSpecificationQ, NotificationChain msgs) {
		DeclarationClass oldForLoopParameterSpecificationQ = forLoopParameterSpecificationQ;
		forLoopParameterSpecificationQ = newForLoopParameterSpecificationQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FOR_LOOP_STATEMENT__FOR_LOOP_PARAMETER_SPECIFICATION_Q, oldForLoopParameterSpecificationQ, newForLoopParameterSpecificationQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setForLoopParameterSpecificationQ(DeclarationClass newForLoopParameterSpecificationQ) {
		if (newForLoopParameterSpecificationQ != forLoopParameterSpecificationQ) {
			NotificationChain msgs = null;
			if (forLoopParameterSpecificationQ != null)
				msgs = ((InternalEObject)forLoopParameterSpecificationQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FOR_LOOP_STATEMENT__FOR_LOOP_PARAMETER_SPECIFICATION_Q, null, msgs);
			if (newForLoopParameterSpecificationQ != null)
				msgs = ((InternalEObject)newForLoopParameterSpecificationQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FOR_LOOP_STATEMENT__FOR_LOOP_PARAMETER_SPECIFICATION_Q, null, msgs);
			msgs = basicSetForLoopParameterSpecificationQ(newForLoopParameterSpecificationQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FOR_LOOP_STATEMENT__FOR_LOOP_PARAMETER_SPECIFICATION_Q, newForLoopParameterSpecificationQ, newForLoopParameterSpecificationQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatementList getLoopStatementsQl() {
		return loopStatementsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLoopStatementsQl(StatementList newLoopStatementsQl, NotificationChain msgs) {
		StatementList oldLoopStatementsQl = loopStatementsQl;
		loopStatementsQl = newLoopStatementsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FOR_LOOP_STATEMENT__LOOP_STATEMENTS_QL, oldLoopStatementsQl, newLoopStatementsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLoopStatementsQl(StatementList newLoopStatementsQl) {
		if (newLoopStatementsQl != loopStatementsQl) {
			NotificationChain msgs = null;
			if (loopStatementsQl != null)
				msgs = ((InternalEObject)loopStatementsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FOR_LOOP_STATEMENT__LOOP_STATEMENTS_QL, null, msgs);
			if (newLoopStatementsQl != null)
				msgs = ((InternalEObject)newLoopStatementsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FOR_LOOP_STATEMENT__LOOP_STATEMENTS_QL, null, msgs);
			msgs = basicSetLoopStatementsQl(newLoopStatementsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FOR_LOOP_STATEMENT__LOOP_STATEMENTS_QL, newLoopStatementsQl, newLoopStatementsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FOR_LOOP_STATEMENT__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.FOR_LOOP_STATEMENT__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.FOR_LOOP_STATEMENT__LABEL_NAMES_QL:
				return basicSetLabelNamesQl(null, msgs);
			case AdaPackage.FOR_LOOP_STATEMENT__STATEMENT_IDENTIFIER_Q:
				return basicSetStatementIdentifierQ(null, msgs);
			case AdaPackage.FOR_LOOP_STATEMENT__FOR_LOOP_PARAMETER_SPECIFICATION_Q:
				return basicSetForLoopParameterSpecificationQ(null, msgs);
			case AdaPackage.FOR_LOOP_STATEMENT__LOOP_STATEMENTS_QL:
				return basicSetLoopStatementsQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.FOR_LOOP_STATEMENT__SLOC:
				return getSloc();
			case AdaPackage.FOR_LOOP_STATEMENT__LABEL_NAMES_QL:
				return getLabelNamesQl();
			case AdaPackage.FOR_LOOP_STATEMENT__STATEMENT_IDENTIFIER_Q:
				return getStatementIdentifierQ();
			case AdaPackage.FOR_LOOP_STATEMENT__FOR_LOOP_PARAMETER_SPECIFICATION_Q:
				return getForLoopParameterSpecificationQ();
			case AdaPackage.FOR_LOOP_STATEMENT__LOOP_STATEMENTS_QL:
				return getLoopStatementsQl();
			case AdaPackage.FOR_LOOP_STATEMENT__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.FOR_LOOP_STATEMENT__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.FOR_LOOP_STATEMENT__LABEL_NAMES_QL:
				setLabelNamesQl((DefiningNameList)newValue);
				return;
			case AdaPackage.FOR_LOOP_STATEMENT__STATEMENT_IDENTIFIER_Q:
				setStatementIdentifierQ((DefiningNameClass)newValue);
				return;
			case AdaPackage.FOR_LOOP_STATEMENT__FOR_LOOP_PARAMETER_SPECIFICATION_Q:
				setForLoopParameterSpecificationQ((DeclarationClass)newValue);
				return;
			case AdaPackage.FOR_LOOP_STATEMENT__LOOP_STATEMENTS_QL:
				setLoopStatementsQl((StatementList)newValue);
				return;
			case AdaPackage.FOR_LOOP_STATEMENT__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.FOR_LOOP_STATEMENT__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.FOR_LOOP_STATEMENT__LABEL_NAMES_QL:
				setLabelNamesQl((DefiningNameList)null);
				return;
			case AdaPackage.FOR_LOOP_STATEMENT__STATEMENT_IDENTIFIER_Q:
				setStatementIdentifierQ((DefiningNameClass)null);
				return;
			case AdaPackage.FOR_LOOP_STATEMENT__FOR_LOOP_PARAMETER_SPECIFICATION_Q:
				setForLoopParameterSpecificationQ((DeclarationClass)null);
				return;
			case AdaPackage.FOR_LOOP_STATEMENT__LOOP_STATEMENTS_QL:
				setLoopStatementsQl((StatementList)null);
				return;
			case AdaPackage.FOR_LOOP_STATEMENT__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.FOR_LOOP_STATEMENT__SLOC:
				return sloc != null;
			case AdaPackage.FOR_LOOP_STATEMENT__LABEL_NAMES_QL:
				return labelNamesQl != null;
			case AdaPackage.FOR_LOOP_STATEMENT__STATEMENT_IDENTIFIER_Q:
				return statementIdentifierQ != null;
			case AdaPackage.FOR_LOOP_STATEMENT__FOR_LOOP_PARAMETER_SPECIFICATION_Q:
				return forLoopParameterSpecificationQ != null;
			case AdaPackage.FOR_LOOP_STATEMENT__LOOP_STATEMENTS_QL:
				return loopStatementsQl != null;
			case AdaPackage.FOR_LOOP_STATEMENT__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //ForLoopStatementImpl
