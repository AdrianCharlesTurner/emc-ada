/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.DeclarationClass;
import Ada.DefiningNameList;
import Ada.ExceptionHandlerList;
import Ada.ExtendedReturnStatement;
import Ada.SourceLocation;
import Ada.StatementList;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Extended Return Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.ExtendedReturnStatementImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.ExtendedReturnStatementImpl#getLabelNamesQl <em>Label Names Ql</em>}</li>
 *   <li>{@link Ada.impl.ExtendedReturnStatementImpl#getReturnObjectDeclarationQ <em>Return Object Declaration Q</em>}</li>
 *   <li>{@link Ada.impl.ExtendedReturnStatementImpl#getExtendedReturnStatementsQl <em>Extended Return Statements Ql</em>}</li>
 *   <li>{@link Ada.impl.ExtendedReturnStatementImpl#getExtendedReturnExceptionHandlersQl <em>Extended Return Exception Handlers Ql</em>}</li>
 *   <li>{@link Ada.impl.ExtendedReturnStatementImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExtendedReturnStatementImpl extends MinimalEObjectImpl.Container implements ExtendedReturnStatement {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getLabelNamesQl() <em>Label Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabelNamesQl()
	 * @generated
	 * @ordered
	 */
	protected DefiningNameList labelNamesQl;

	/**
	 * The cached value of the '{@link #getReturnObjectDeclarationQ() <em>Return Object Declaration Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReturnObjectDeclarationQ()
	 * @generated
	 * @ordered
	 */
	protected DeclarationClass returnObjectDeclarationQ;

	/**
	 * The cached value of the '{@link #getExtendedReturnStatementsQl() <em>Extended Return Statements Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtendedReturnStatementsQl()
	 * @generated
	 * @ordered
	 */
	protected StatementList extendedReturnStatementsQl;

	/**
	 * The cached value of the '{@link #getExtendedReturnExceptionHandlersQl() <em>Extended Return Exception Handlers Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtendedReturnExceptionHandlersQl()
	 * @generated
	 * @ordered
	 */
	protected ExceptionHandlerList extendedReturnExceptionHandlersQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExtendedReturnStatementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getExtendedReturnStatement();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.EXTENDED_RETURN_STATEMENT__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXTENDED_RETURN_STATEMENT__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXTENDED_RETURN_STATEMENT__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.EXTENDED_RETURN_STATEMENT__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNameList getLabelNamesQl() {
		return labelNamesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLabelNamesQl(DefiningNameList newLabelNamesQl, NotificationChain msgs) {
		DefiningNameList oldLabelNamesQl = labelNamesQl;
		labelNamesQl = newLabelNamesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.EXTENDED_RETURN_STATEMENT__LABEL_NAMES_QL, oldLabelNamesQl, newLabelNamesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabelNamesQl(DefiningNameList newLabelNamesQl) {
		if (newLabelNamesQl != labelNamesQl) {
			NotificationChain msgs = null;
			if (labelNamesQl != null)
				msgs = ((InternalEObject)labelNamesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXTENDED_RETURN_STATEMENT__LABEL_NAMES_QL, null, msgs);
			if (newLabelNamesQl != null)
				msgs = ((InternalEObject)newLabelNamesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXTENDED_RETURN_STATEMENT__LABEL_NAMES_QL, null, msgs);
			msgs = basicSetLabelNamesQl(newLabelNamesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.EXTENDED_RETURN_STATEMENT__LABEL_NAMES_QL, newLabelNamesQl, newLabelNamesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeclarationClass getReturnObjectDeclarationQ() {
		return returnObjectDeclarationQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReturnObjectDeclarationQ(DeclarationClass newReturnObjectDeclarationQ, NotificationChain msgs) {
		DeclarationClass oldReturnObjectDeclarationQ = returnObjectDeclarationQ;
		returnObjectDeclarationQ = newReturnObjectDeclarationQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.EXTENDED_RETURN_STATEMENT__RETURN_OBJECT_DECLARATION_Q, oldReturnObjectDeclarationQ, newReturnObjectDeclarationQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReturnObjectDeclarationQ(DeclarationClass newReturnObjectDeclarationQ) {
		if (newReturnObjectDeclarationQ != returnObjectDeclarationQ) {
			NotificationChain msgs = null;
			if (returnObjectDeclarationQ != null)
				msgs = ((InternalEObject)returnObjectDeclarationQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXTENDED_RETURN_STATEMENT__RETURN_OBJECT_DECLARATION_Q, null, msgs);
			if (newReturnObjectDeclarationQ != null)
				msgs = ((InternalEObject)newReturnObjectDeclarationQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXTENDED_RETURN_STATEMENT__RETURN_OBJECT_DECLARATION_Q, null, msgs);
			msgs = basicSetReturnObjectDeclarationQ(newReturnObjectDeclarationQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.EXTENDED_RETURN_STATEMENT__RETURN_OBJECT_DECLARATION_Q, newReturnObjectDeclarationQ, newReturnObjectDeclarationQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatementList getExtendedReturnStatementsQl() {
		return extendedReturnStatementsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExtendedReturnStatementsQl(StatementList newExtendedReturnStatementsQl, NotificationChain msgs) {
		StatementList oldExtendedReturnStatementsQl = extendedReturnStatementsQl;
		extendedReturnStatementsQl = newExtendedReturnStatementsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.EXTENDED_RETURN_STATEMENT__EXTENDED_RETURN_STATEMENTS_QL, oldExtendedReturnStatementsQl, newExtendedReturnStatementsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExtendedReturnStatementsQl(StatementList newExtendedReturnStatementsQl) {
		if (newExtendedReturnStatementsQl != extendedReturnStatementsQl) {
			NotificationChain msgs = null;
			if (extendedReturnStatementsQl != null)
				msgs = ((InternalEObject)extendedReturnStatementsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXTENDED_RETURN_STATEMENT__EXTENDED_RETURN_STATEMENTS_QL, null, msgs);
			if (newExtendedReturnStatementsQl != null)
				msgs = ((InternalEObject)newExtendedReturnStatementsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXTENDED_RETURN_STATEMENT__EXTENDED_RETURN_STATEMENTS_QL, null, msgs);
			msgs = basicSetExtendedReturnStatementsQl(newExtendedReturnStatementsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.EXTENDED_RETURN_STATEMENT__EXTENDED_RETURN_STATEMENTS_QL, newExtendedReturnStatementsQl, newExtendedReturnStatementsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExceptionHandlerList getExtendedReturnExceptionHandlersQl() {
		return extendedReturnExceptionHandlersQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExtendedReturnExceptionHandlersQl(ExceptionHandlerList newExtendedReturnExceptionHandlersQl, NotificationChain msgs) {
		ExceptionHandlerList oldExtendedReturnExceptionHandlersQl = extendedReturnExceptionHandlersQl;
		extendedReturnExceptionHandlersQl = newExtendedReturnExceptionHandlersQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.EXTENDED_RETURN_STATEMENT__EXTENDED_RETURN_EXCEPTION_HANDLERS_QL, oldExtendedReturnExceptionHandlersQl, newExtendedReturnExceptionHandlersQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExtendedReturnExceptionHandlersQl(ExceptionHandlerList newExtendedReturnExceptionHandlersQl) {
		if (newExtendedReturnExceptionHandlersQl != extendedReturnExceptionHandlersQl) {
			NotificationChain msgs = null;
			if (extendedReturnExceptionHandlersQl != null)
				msgs = ((InternalEObject)extendedReturnExceptionHandlersQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXTENDED_RETURN_STATEMENT__EXTENDED_RETURN_EXCEPTION_HANDLERS_QL, null, msgs);
			if (newExtendedReturnExceptionHandlersQl != null)
				msgs = ((InternalEObject)newExtendedReturnExceptionHandlersQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXTENDED_RETURN_STATEMENT__EXTENDED_RETURN_EXCEPTION_HANDLERS_QL, null, msgs);
			msgs = basicSetExtendedReturnExceptionHandlersQl(newExtendedReturnExceptionHandlersQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.EXTENDED_RETURN_STATEMENT__EXTENDED_RETURN_EXCEPTION_HANDLERS_QL, newExtendedReturnExceptionHandlersQl, newExtendedReturnExceptionHandlersQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.EXTENDED_RETURN_STATEMENT__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.EXTENDED_RETURN_STATEMENT__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.EXTENDED_RETURN_STATEMENT__LABEL_NAMES_QL:
				return basicSetLabelNamesQl(null, msgs);
			case AdaPackage.EXTENDED_RETURN_STATEMENT__RETURN_OBJECT_DECLARATION_Q:
				return basicSetReturnObjectDeclarationQ(null, msgs);
			case AdaPackage.EXTENDED_RETURN_STATEMENT__EXTENDED_RETURN_STATEMENTS_QL:
				return basicSetExtendedReturnStatementsQl(null, msgs);
			case AdaPackage.EXTENDED_RETURN_STATEMENT__EXTENDED_RETURN_EXCEPTION_HANDLERS_QL:
				return basicSetExtendedReturnExceptionHandlersQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.EXTENDED_RETURN_STATEMENT__SLOC:
				return getSloc();
			case AdaPackage.EXTENDED_RETURN_STATEMENT__LABEL_NAMES_QL:
				return getLabelNamesQl();
			case AdaPackage.EXTENDED_RETURN_STATEMENT__RETURN_OBJECT_DECLARATION_Q:
				return getReturnObjectDeclarationQ();
			case AdaPackage.EXTENDED_RETURN_STATEMENT__EXTENDED_RETURN_STATEMENTS_QL:
				return getExtendedReturnStatementsQl();
			case AdaPackage.EXTENDED_RETURN_STATEMENT__EXTENDED_RETURN_EXCEPTION_HANDLERS_QL:
				return getExtendedReturnExceptionHandlersQl();
			case AdaPackage.EXTENDED_RETURN_STATEMENT__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.EXTENDED_RETURN_STATEMENT__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.EXTENDED_RETURN_STATEMENT__LABEL_NAMES_QL:
				setLabelNamesQl((DefiningNameList)newValue);
				return;
			case AdaPackage.EXTENDED_RETURN_STATEMENT__RETURN_OBJECT_DECLARATION_Q:
				setReturnObjectDeclarationQ((DeclarationClass)newValue);
				return;
			case AdaPackage.EXTENDED_RETURN_STATEMENT__EXTENDED_RETURN_STATEMENTS_QL:
				setExtendedReturnStatementsQl((StatementList)newValue);
				return;
			case AdaPackage.EXTENDED_RETURN_STATEMENT__EXTENDED_RETURN_EXCEPTION_HANDLERS_QL:
				setExtendedReturnExceptionHandlersQl((ExceptionHandlerList)newValue);
				return;
			case AdaPackage.EXTENDED_RETURN_STATEMENT__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.EXTENDED_RETURN_STATEMENT__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.EXTENDED_RETURN_STATEMENT__LABEL_NAMES_QL:
				setLabelNamesQl((DefiningNameList)null);
				return;
			case AdaPackage.EXTENDED_RETURN_STATEMENT__RETURN_OBJECT_DECLARATION_Q:
				setReturnObjectDeclarationQ((DeclarationClass)null);
				return;
			case AdaPackage.EXTENDED_RETURN_STATEMENT__EXTENDED_RETURN_STATEMENTS_QL:
				setExtendedReturnStatementsQl((StatementList)null);
				return;
			case AdaPackage.EXTENDED_RETURN_STATEMENT__EXTENDED_RETURN_EXCEPTION_HANDLERS_QL:
				setExtendedReturnExceptionHandlersQl((ExceptionHandlerList)null);
				return;
			case AdaPackage.EXTENDED_RETURN_STATEMENT__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.EXTENDED_RETURN_STATEMENT__SLOC:
				return sloc != null;
			case AdaPackage.EXTENDED_RETURN_STATEMENT__LABEL_NAMES_QL:
				return labelNamesQl != null;
			case AdaPackage.EXTENDED_RETURN_STATEMENT__RETURN_OBJECT_DECLARATION_Q:
				return returnObjectDeclarationQ != null;
			case AdaPackage.EXTENDED_RETURN_STATEMENT__EXTENDED_RETURN_STATEMENTS_QL:
				return extendedReturnStatementsQl != null;
			case AdaPackage.EXTENDED_RETURN_STATEMENT__EXTENDED_RETURN_EXCEPTION_HANDLERS_QL:
				return extendedReturnExceptionHandlersQl != null;
			case AdaPackage.EXTENDED_RETURN_STATEMENT__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //ExtendedReturnStatementImpl
