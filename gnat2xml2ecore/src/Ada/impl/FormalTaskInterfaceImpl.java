/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.ExpressionList;
import Ada.FormalTaskInterface;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Formal Task Interface</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.FormalTaskInterfaceImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.FormalTaskInterfaceImpl#getDefinitionInterfaceListQl <em>Definition Interface List Ql</em>}</li>
 *   <li>{@link Ada.impl.FormalTaskInterfaceImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FormalTaskInterfaceImpl extends MinimalEObjectImpl.Container implements FormalTaskInterface {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getDefinitionInterfaceListQl() <em>Definition Interface List Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefinitionInterfaceListQl()
	 * @generated
	 * @ordered
	 */
	protected ExpressionList definitionInterfaceListQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FormalTaskInterfaceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getFormalTaskInterface();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_TASK_INTERFACE__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_TASK_INTERFACE__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_TASK_INTERFACE__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_TASK_INTERFACE__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionList getDefinitionInterfaceListQl() {
		return definitionInterfaceListQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefinitionInterfaceListQl(ExpressionList newDefinitionInterfaceListQl, NotificationChain msgs) {
		ExpressionList oldDefinitionInterfaceListQl = definitionInterfaceListQl;
		definitionInterfaceListQl = newDefinitionInterfaceListQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_TASK_INTERFACE__DEFINITION_INTERFACE_LIST_QL, oldDefinitionInterfaceListQl, newDefinitionInterfaceListQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefinitionInterfaceListQl(ExpressionList newDefinitionInterfaceListQl) {
		if (newDefinitionInterfaceListQl != definitionInterfaceListQl) {
			NotificationChain msgs = null;
			if (definitionInterfaceListQl != null)
				msgs = ((InternalEObject)definitionInterfaceListQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_TASK_INTERFACE__DEFINITION_INTERFACE_LIST_QL, null, msgs);
			if (newDefinitionInterfaceListQl != null)
				msgs = ((InternalEObject)newDefinitionInterfaceListQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_TASK_INTERFACE__DEFINITION_INTERFACE_LIST_QL, null, msgs);
			msgs = basicSetDefinitionInterfaceListQl(newDefinitionInterfaceListQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_TASK_INTERFACE__DEFINITION_INTERFACE_LIST_QL, newDefinitionInterfaceListQl, newDefinitionInterfaceListQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_TASK_INTERFACE__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.FORMAL_TASK_INTERFACE__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.FORMAL_TASK_INTERFACE__DEFINITION_INTERFACE_LIST_QL:
				return basicSetDefinitionInterfaceListQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.FORMAL_TASK_INTERFACE__SLOC:
				return getSloc();
			case AdaPackage.FORMAL_TASK_INTERFACE__DEFINITION_INTERFACE_LIST_QL:
				return getDefinitionInterfaceListQl();
			case AdaPackage.FORMAL_TASK_INTERFACE__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.FORMAL_TASK_INTERFACE__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.FORMAL_TASK_INTERFACE__DEFINITION_INTERFACE_LIST_QL:
				setDefinitionInterfaceListQl((ExpressionList)newValue);
				return;
			case AdaPackage.FORMAL_TASK_INTERFACE__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.FORMAL_TASK_INTERFACE__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.FORMAL_TASK_INTERFACE__DEFINITION_INTERFACE_LIST_QL:
				setDefinitionInterfaceListQl((ExpressionList)null);
				return;
			case AdaPackage.FORMAL_TASK_INTERFACE__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.FORMAL_TASK_INTERFACE__SLOC:
				return sloc != null;
			case AdaPackage.FORMAL_TASK_INTERFACE__DEFINITION_INTERFACE_LIST_QL:
				return definitionInterfaceListQl != null;
			case AdaPackage.FORMAL_TASK_INTERFACE__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //FormalTaskInterfaceImpl
