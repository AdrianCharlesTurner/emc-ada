/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.CompilationUnit;
import Ada.ContextClauseList;
import Ada.DeclarationClass;
import Ada.ElementList;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Compilation Unit</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.CompilationUnitImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.CompilationUnitImpl#getContextClauseElementsQl <em>Context Clause Elements Ql</em>}</li>
 *   <li>{@link Ada.impl.CompilationUnitImpl#getUnitDeclarationQ <em>Unit Declaration Q</em>}</li>
 *   <li>{@link Ada.impl.CompilationUnitImpl#getPragmasAfterQl <em>Pragmas After Ql</em>}</li>
 *   <li>{@link Ada.impl.CompilationUnitImpl#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.impl.CompilationUnitImpl#getDefName <em>Def Name</em>}</li>
 *   <li>{@link Ada.impl.CompilationUnitImpl#getSourceFile <em>Source File</em>}</li>
 *   <li>{@link Ada.impl.CompilationUnitImpl#getUnitClass <em>Unit Class</em>}</li>
 *   <li>{@link Ada.impl.CompilationUnitImpl#getUnitFullName <em>Unit Full Name</em>}</li>
 *   <li>{@link Ada.impl.CompilationUnitImpl#getUnitKind <em>Unit Kind</em>}</li>
 *   <li>{@link Ada.impl.CompilationUnitImpl#getUnitOrigin <em>Unit Origin</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CompilationUnitImpl extends MinimalEObjectImpl.Container implements CompilationUnit {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getContextClauseElementsQl() <em>Context Clause Elements Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContextClauseElementsQl()
	 * @generated
	 * @ordered
	 */
	protected ContextClauseList contextClauseElementsQl;

	/**
	 * The cached value of the '{@link #getUnitDeclarationQ() <em>Unit Declaration Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitDeclarationQ()
	 * @generated
	 * @ordered
	 */
	protected DeclarationClass unitDeclarationQ;

	/**
	 * The cached value of the '{@link #getPragmasAfterQl() <em>Pragmas After Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPragmasAfterQl()
	 * @generated
	 * @ordered
	 */
	protected ElementList pragmasAfterQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * The default value of the '{@link #getDefName() <em>Def Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefName()
	 * @generated
	 * @ordered
	 */
	protected static final String DEF_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDefName() <em>Def Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefName()
	 * @generated
	 * @ordered
	 */
	protected String defName = DEF_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getSourceFile() <em>Source File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceFile()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_FILE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSourceFile() <em>Source File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceFile()
	 * @generated
	 * @ordered
	 */
	protected String sourceFile = SOURCE_FILE_EDEFAULT;

	/**
	 * The default value of the '{@link #getUnitClass() <em>Unit Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitClass()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIT_CLASS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnitClass() <em>Unit Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitClass()
	 * @generated
	 * @ordered
	 */
	protected String unitClass = UNIT_CLASS_EDEFAULT;

	/**
	 * The default value of the '{@link #getUnitFullName() <em>Unit Full Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitFullName()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIT_FULL_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnitFullName() <em>Unit Full Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitFullName()
	 * @generated
	 * @ordered
	 */
	protected String unitFullName = UNIT_FULL_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getUnitKind() <em>Unit Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitKind()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIT_KIND_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnitKind() <em>Unit Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitKind()
	 * @generated
	 * @ordered
	 */
	protected String unitKind = UNIT_KIND_EDEFAULT;

	/**
	 * The default value of the '{@link #getUnitOrigin() <em>Unit Origin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitOrigin()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIT_ORIGIN_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnitOrigin() <em>Unit Origin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnitOrigin()
	 * @generated
	 * @ordered
	 */
	protected String unitOrigin = UNIT_ORIGIN_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CompilationUnitImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getCompilationUnit();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.COMPILATION_UNIT__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.COMPILATION_UNIT__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.COMPILATION_UNIT__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.COMPILATION_UNIT__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ContextClauseList getContextClauseElementsQl() {
		return contextClauseElementsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContextClauseElementsQl(ContextClauseList newContextClauseElementsQl, NotificationChain msgs) {
		ContextClauseList oldContextClauseElementsQl = contextClauseElementsQl;
		contextClauseElementsQl = newContextClauseElementsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.COMPILATION_UNIT__CONTEXT_CLAUSE_ELEMENTS_QL, oldContextClauseElementsQl, newContextClauseElementsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContextClauseElementsQl(ContextClauseList newContextClauseElementsQl) {
		if (newContextClauseElementsQl != contextClauseElementsQl) {
			NotificationChain msgs = null;
			if (contextClauseElementsQl != null)
				msgs = ((InternalEObject)contextClauseElementsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.COMPILATION_UNIT__CONTEXT_CLAUSE_ELEMENTS_QL, null, msgs);
			if (newContextClauseElementsQl != null)
				msgs = ((InternalEObject)newContextClauseElementsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.COMPILATION_UNIT__CONTEXT_CLAUSE_ELEMENTS_QL, null, msgs);
			msgs = basicSetContextClauseElementsQl(newContextClauseElementsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.COMPILATION_UNIT__CONTEXT_CLAUSE_ELEMENTS_QL, newContextClauseElementsQl, newContextClauseElementsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeclarationClass getUnitDeclarationQ() {
		return unitDeclarationQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnitDeclarationQ(DeclarationClass newUnitDeclarationQ, NotificationChain msgs) {
		DeclarationClass oldUnitDeclarationQ = unitDeclarationQ;
		unitDeclarationQ = newUnitDeclarationQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.COMPILATION_UNIT__UNIT_DECLARATION_Q, oldUnitDeclarationQ, newUnitDeclarationQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnitDeclarationQ(DeclarationClass newUnitDeclarationQ) {
		if (newUnitDeclarationQ != unitDeclarationQ) {
			NotificationChain msgs = null;
			if (unitDeclarationQ != null)
				msgs = ((InternalEObject)unitDeclarationQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.COMPILATION_UNIT__UNIT_DECLARATION_Q, null, msgs);
			if (newUnitDeclarationQ != null)
				msgs = ((InternalEObject)newUnitDeclarationQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.COMPILATION_UNIT__UNIT_DECLARATION_Q, null, msgs);
			msgs = basicSetUnitDeclarationQ(newUnitDeclarationQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.COMPILATION_UNIT__UNIT_DECLARATION_Q, newUnitDeclarationQ, newUnitDeclarationQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementList getPragmasAfterQl() {
		return pragmasAfterQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPragmasAfterQl(ElementList newPragmasAfterQl, NotificationChain msgs) {
		ElementList oldPragmasAfterQl = pragmasAfterQl;
		pragmasAfterQl = newPragmasAfterQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.COMPILATION_UNIT__PRAGMAS_AFTER_QL, oldPragmasAfterQl, newPragmasAfterQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPragmasAfterQl(ElementList newPragmasAfterQl) {
		if (newPragmasAfterQl != pragmasAfterQl) {
			NotificationChain msgs = null;
			if (pragmasAfterQl != null)
				msgs = ((InternalEObject)pragmasAfterQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.COMPILATION_UNIT__PRAGMAS_AFTER_QL, null, msgs);
			if (newPragmasAfterQl != null)
				msgs = ((InternalEObject)newPragmasAfterQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.COMPILATION_UNIT__PRAGMAS_AFTER_QL, null, msgs);
			msgs = basicSetPragmasAfterQl(newPragmasAfterQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.COMPILATION_UNIT__PRAGMAS_AFTER_QL, newPragmasAfterQl, newPragmasAfterQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.COMPILATION_UNIT__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDefName() {
		return defName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefName(String newDefName) {
		String oldDefName = defName;
		defName = newDefName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.COMPILATION_UNIT__DEF_NAME, oldDefName, defName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSourceFile() {
		return sourceFile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceFile(String newSourceFile) {
		String oldSourceFile = sourceFile;
		sourceFile = newSourceFile;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.COMPILATION_UNIT__SOURCE_FILE, oldSourceFile, sourceFile));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUnitClass() {
		return unitClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnitClass(String newUnitClass) {
		String oldUnitClass = unitClass;
		unitClass = newUnitClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.COMPILATION_UNIT__UNIT_CLASS, oldUnitClass, unitClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUnitFullName() {
		return unitFullName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnitFullName(String newUnitFullName) {
		String oldUnitFullName = unitFullName;
		unitFullName = newUnitFullName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.COMPILATION_UNIT__UNIT_FULL_NAME, oldUnitFullName, unitFullName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUnitKind() {
		return unitKind;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnitKind(String newUnitKind) {
		String oldUnitKind = unitKind;
		unitKind = newUnitKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.COMPILATION_UNIT__UNIT_KIND, oldUnitKind, unitKind));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUnitOrigin() {
		return unitOrigin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnitOrigin(String newUnitOrigin) {
		String oldUnitOrigin = unitOrigin;
		unitOrigin = newUnitOrigin;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.COMPILATION_UNIT__UNIT_ORIGIN, oldUnitOrigin, unitOrigin));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.COMPILATION_UNIT__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.COMPILATION_UNIT__CONTEXT_CLAUSE_ELEMENTS_QL:
				return basicSetContextClauseElementsQl(null, msgs);
			case AdaPackage.COMPILATION_UNIT__UNIT_DECLARATION_Q:
				return basicSetUnitDeclarationQ(null, msgs);
			case AdaPackage.COMPILATION_UNIT__PRAGMAS_AFTER_QL:
				return basicSetPragmasAfterQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.COMPILATION_UNIT__SLOC:
				return getSloc();
			case AdaPackage.COMPILATION_UNIT__CONTEXT_CLAUSE_ELEMENTS_QL:
				return getContextClauseElementsQl();
			case AdaPackage.COMPILATION_UNIT__UNIT_DECLARATION_Q:
				return getUnitDeclarationQ();
			case AdaPackage.COMPILATION_UNIT__PRAGMAS_AFTER_QL:
				return getPragmasAfterQl();
			case AdaPackage.COMPILATION_UNIT__CHECKS:
				return getChecks();
			case AdaPackage.COMPILATION_UNIT__DEF_NAME:
				return getDefName();
			case AdaPackage.COMPILATION_UNIT__SOURCE_FILE:
				return getSourceFile();
			case AdaPackage.COMPILATION_UNIT__UNIT_CLASS:
				return getUnitClass();
			case AdaPackage.COMPILATION_UNIT__UNIT_FULL_NAME:
				return getUnitFullName();
			case AdaPackage.COMPILATION_UNIT__UNIT_KIND:
				return getUnitKind();
			case AdaPackage.COMPILATION_UNIT__UNIT_ORIGIN:
				return getUnitOrigin();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.COMPILATION_UNIT__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.COMPILATION_UNIT__CONTEXT_CLAUSE_ELEMENTS_QL:
				setContextClauseElementsQl((ContextClauseList)newValue);
				return;
			case AdaPackage.COMPILATION_UNIT__UNIT_DECLARATION_Q:
				setUnitDeclarationQ((DeclarationClass)newValue);
				return;
			case AdaPackage.COMPILATION_UNIT__PRAGMAS_AFTER_QL:
				setPragmasAfterQl((ElementList)newValue);
				return;
			case AdaPackage.COMPILATION_UNIT__CHECKS:
				setChecks((String)newValue);
				return;
			case AdaPackage.COMPILATION_UNIT__DEF_NAME:
				setDefName((String)newValue);
				return;
			case AdaPackage.COMPILATION_UNIT__SOURCE_FILE:
				setSourceFile((String)newValue);
				return;
			case AdaPackage.COMPILATION_UNIT__UNIT_CLASS:
				setUnitClass((String)newValue);
				return;
			case AdaPackage.COMPILATION_UNIT__UNIT_FULL_NAME:
				setUnitFullName((String)newValue);
				return;
			case AdaPackage.COMPILATION_UNIT__UNIT_KIND:
				setUnitKind((String)newValue);
				return;
			case AdaPackage.COMPILATION_UNIT__UNIT_ORIGIN:
				setUnitOrigin((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.COMPILATION_UNIT__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.COMPILATION_UNIT__CONTEXT_CLAUSE_ELEMENTS_QL:
				setContextClauseElementsQl((ContextClauseList)null);
				return;
			case AdaPackage.COMPILATION_UNIT__UNIT_DECLARATION_Q:
				setUnitDeclarationQ((DeclarationClass)null);
				return;
			case AdaPackage.COMPILATION_UNIT__PRAGMAS_AFTER_QL:
				setPragmasAfterQl((ElementList)null);
				return;
			case AdaPackage.COMPILATION_UNIT__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
			case AdaPackage.COMPILATION_UNIT__DEF_NAME:
				setDefName(DEF_NAME_EDEFAULT);
				return;
			case AdaPackage.COMPILATION_UNIT__SOURCE_FILE:
				setSourceFile(SOURCE_FILE_EDEFAULT);
				return;
			case AdaPackage.COMPILATION_UNIT__UNIT_CLASS:
				setUnitClass(UNIT_CLASS_EDEFAULT);
				return;
			case AdaPackage.COMPILATION_UNIT__UNIT_FULL_NAME:
				setUnitFullName(UNIT_FULL_NAME_EDEFAULT);
				return;
			case AdaPackage.COMPILATION_UNIT__UNIT_KIND:
				setUnitKind(UNIT_KIND_EDEFAULT);
				return;
			case AdaPackage.COMPILATION_UNIT__UNIT_ORIGIN:
				setUnitOrigin(UNIT_ORIGIN_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.COMPILATION_UNIT__SLOC:
				return sloc != null;
			case AdaPackage.COMPILATION_UNIT__CONTEXT_CLAUSE_ELEMENTS_QL:
				return contextClauseElementsQl != null;
			case AdaPackage.COMPILATION_UNIT__UNIT_DECLARATION_Q:
				return unitDeclarationQ != null;
			case AdaPackage.COMPILATION_UNIT__PRAGMAS_AFTER_QL:
				return pragmasAfterQl != null;
			case AdaPackage.COMPILATION_UNIT__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
			case AdaPackage.COMPILATION_UNIT__DEF_NAME:
				return DEF_NAME_EDEFAULT == null ? defName != null : !DEF_NAME_EDEFAULT.equals(defName);
			case AdaPackage.COMPILATION_UNIT__SOURCE_FILE:
				return SOURCE_FILE_EDEFAULT == null ? sourceFile != null : !SOURCE_FILE_EDEFAULT.equals(sourceFile);
			case AdaPackage.COMPILATION_UNIT__UNIT_CLASS:
				return UNIT_CLASS_EDEFAULT == null ? unitClass != null : !UNIT_CLASS_EDEFAULT.equals(unitClass);
			case AdaPackage.COMPILATION_UNIT__UNIT_FULL_NAME:
				return UNIT_FULL_NAME_EDEFAULT == null ? unitFullName != null : !UNIT_FULL_NAME_EDEFAULT.equals(unitFullName);
			case AdaPackage.COMPILATION_UNIT__UNIT_KIND:
				return UNIT_KIND_EDEFAULT == null ? unitKind != null : !UNIT_KIND_EDEFAULT.equals(unitKind);
			case AdaPackage.COMPILATION_UNIT__UNIT_ORIGIN:
				return UNIT_ORIGIN_EDEFAULT == null ? unitOrigin != null : !UNIT_ORIGIN_EDEFAULT.equals(unitOrigin);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(", defName: ");
		result.append(defName);
		result.append(", sourceFile: ");
		result.append(sourceFile);
		result.append(", unitClass: ");
		result.append(unitClass);
		result.append(", unitFullName: ");
		result.append(unitFullName);
		result.append(", unitKind: ");
		result.append(unitKind);
		result.append(", unitOrigin: ");
		result.append(unitOrigin);
		result.append(')');
		return result.toString();
	}

} //CompilationUnitImpl
