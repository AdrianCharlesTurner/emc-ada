/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Definition Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.DefinitionClass#getNotAnElement <em>Not An Element</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getDerivedTypeDefinition <em>Derived Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getDerivedRecordExtensionDefinition <em>Derived Record Extension Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getEnumerationTypeDefinition <em>Enumeration Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getSignedIntegerTypeDefinition <em>Signed Integer Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getModularTypeDefinition <em>Modular Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getRootIntegerDefinition <em>Root Integer Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getRootRealDefinition <em>Root Real Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getUniversalIntegerDefinition <em>Universal Integer Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getUniversalRealDefinition <em>Universal Real Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getUniversalFixedDefinition <em>Universal Fixed Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getFloatingPointDefinition <em>Floating Point Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getOrdinaryFixedPointDefinition <em>Ordinary Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getDecimalFixedPointDefinition <em>Decimal Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getUnconstrainedArrayDefinition <em>Unconstrained Array Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getConstrainedArrayDefinition <em>Constrained Array Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getRecordTypeDefinition <em>Record Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getTaggedRecordTypeDefinition <em>Tagged Record Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getOrdinaryInterface <em>Ordinary Interface</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getLimitedInterface <em>Limited Interface</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getTaskInterface <em>Task Interface</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getProtectedInterface <em>Protected Interface</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getSynchronizedInterface <em>Synchronized Interface</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getPoolSpecificAccessToVariable <em>Pool Specific Access To Variable</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getAccessToVariable <em>Access To Variable</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getAccessToConstant <em>Access To Constant</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getAccessToProcedure <em>Access To Procedure</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getAccessToProtectedProcedure <em>Access To Protected Procedure</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getAccessToFunction <em>Access To Function</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getAccessToProtectedFunction <em>Access To Protected Function</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getSubtypeIndication <em>Subtype Indication</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getRangeAttributeReference <em>Range Attribute Reference</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getSimpleExpressionRange <em>Simple Expression Range</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getDigitsConstraint <em>Digits Constraint</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getDeltaConstraint <em>Delta Constraint</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getIndexConstraint <em>Index Constraint</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getDiscriminantConstraint <em>Discriminant Constraint</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getComponentDefinition <em>Component Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getDiscreteSubtypeIndicationAsSubtypeDefinition <em>Discrete Subtype Indication As Subtype Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getDiscreteRangeAttributeReferenceAsSubtypeDefinition <em>Discrete Range Attribute Reference As Subtype Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getDiscreteSimpleExpressionRangeAsSubtypeDefinition <em>Discrete Simple Expression Range As Subtype Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getDiscreteSubtypeIndication <em>Discrete Subtype Indication</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getDiscreteRangeAttributeReference <em>Discrete Range Attribute Reference</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getDiscreteSimpleExpressionRange <em>Discrete Simple Expression Range</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getUnknownDiscriminantPart <em>Unknown Discriminant Part</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getKnownDiscriminantPart <em>Known Discriminant Part</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getRecordDefinition <em>Record Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getNullRecordDefinition <em>Null Record Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getNullComponent <em>Null Component</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getVariantPart <em>Variant Part</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getVariant <em>Variant</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getOthersChoice <em>Others Choice</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getAnonymousAccessToVariable <em>Anonymous Access To Variable</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getAnonymousAccessToConstant <em>Anonymous Access To Constant</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getAnonymousAccessToProcedure <em>Anonymous Access To Procedure</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getAnonymousAccessToProtectedProcedure <em>Anonymous Access To Protected Procedure</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getAnonymousAccessToFunction <em>Anonymous Access To Function</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getAnonymousAccessToProtectedFunction <em>Anonymous Access To Protected Function</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getPrivateTypeDefinition <em>Private Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getTaggedPrivateTypeDefinition <em>Tagged Private Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getPrivateExtensionDefinition <em>Private Extension Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getTaskDefinition <em>Task Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getProtectedDefinition <em>Protected Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getFormalPrivateTypeDefinition <em>Formal Private Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getFormalTaggedPrivateTypeDefinition <em>Formal Tagged Private Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getFormalDerivedTypeDefinition <em>Formal Derived Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getFormalDiscreteTypeDefinition <em>Formal Discrete Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getFormalSignedIntegerTypeDefinition <em>Formal Signed Integer Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getFormalModularTypeDefinition <em>Formal Modular Type Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getFormalFloatingPointDefinition <em>Formal Floating Point Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getFormalOrdinaryFixedPointDefinition <em>Formal Ordinary Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getFormalDecimalFixedPointDefinition <em>Formal Decimal Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getFormalOrdinaryInterface <em>Formal Ordinary Interface</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getFormalLimitedInterface <em>Formal Limited Interface</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getFormalTaskInterface <em>Formal Task Interface</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getFormalProtectedInterface <em>Formal Protected Interface</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getFormalSynchronizedInterface <em>Formal Synchronized Interface</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getFormalUnconstrainedArrayDefinition <em>Formal Unconstrained Array Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getFormalConstrainedArrayDefinition <em>Formal Constrained Array Definition</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getFormalPoolSpecificAccessToVariable <em>Formal Pool Specific Access To Variable</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getFormalAccessToVariable <em>Formal Access To Variable</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getFormalAccessToConstant <em>Formal Access To Constant</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getFormalAccessToProcedure <em>Formal Access To Procedure</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getFormalAccessToProtectedProcedure <em>Formal Access To Protected Procedure</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getFormalAccessToFunction <em>Formal Access To Function</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getFormalAccessToProtectedFunction <em>Formal Access To Protected Function</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getAspectSpecification <em>Aspect Specification</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getSelectedComponent <em>Selected Component</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getBaseAttribute <em>Base Attribute</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getClassAttribute <em>Class Attribute</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getComment <em>Comment</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getAsynchronousPragma <em>Asynchronous Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getAtomicPragma <em>Atomic Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getAttachHandlerPragma <em>Attach Handler Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getControlledPragma <em>Controlled Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getConventionPragma <em>Convention Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getDiscardNamesPragma <em>Discard Names Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getElaboratePragma <em>Elaborate Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getElaborateAllPragma <em>Elaborate All Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getExportPragma <em>Export Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getImportPragma <em>Import Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getInlinePragma <em>Inline Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getInspectionPointPragma <em>Inspection Point Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getLinkerOptionsPragma <em>Linker Options Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getListPragma <em>List Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getLockingPolicyPragma <em>Locking Policy Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getOptimizePragma <em>Optimize Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getPackPragma <em>Pack Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getPagePragma <em>Page Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getPreelaboratePragma <em>Preelaborate Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getPriorityPragma <em>Priority Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getPurePragma <em>Pure Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getRemoteTypesPragma <em>Remote Types Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getRestrictionsPragma <em>Restrictions Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getReviewablePragma <em>Reviewable Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getSharedPassivePragma <em>Shared Passive Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getStorageSizePragma <em>Storage Size Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getSuppressPragma <em>Suppress Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getVolatilePragma <em>Volatile Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getAssertPragma <em>Assert Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getNoReturnPragma <em>No Return Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getProfilePragma <em>Profile Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getUnsuppressPragma <em>Unsuppress Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getCpuPragma <em>Cpu Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getIndependentPragma <em>Independent Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getIndependentComponentsPragma <em>Independent Components Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}</li>
 *   <li>{@link Ada.DefinitionClass#getUnknownPragma <em>Unknown Pragma</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getDefinitionClass()
 * @model extendedMetaData="name='Definition_Class' kind='elementOnly'"
 * @generated
 */
public interface DefinitionClass extends EObject {
	/**
	 * Returns the value of the '<em><b>Not An Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not An Element</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not An Element</em>' containment reference.
	 * @see #setNotAnElement(NotAnElement)
	 * @see Ada.AdaPackage#getDefinitionClass_NotAnElement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not_an_element' namespace='##targetNamespace'"
	 * @generated
	 */
	NotAnElement getNotAnElement();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getNotAnElement <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not An Element</em>' containment reference.
	 * @see #getNotAnElement()
	 * @generated
	 */
	void setNotAnElement(NotAnElement value);

	/**
	 * Returns the value of the '<em><b>Derived Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived Type Definition</em>' containment reference.
	 * @see #setDerivedTypeDefinition(DerivedTypeDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_DerivedTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='derived_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	DerivedTypeDefinition getDerivedTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getDerivedTypeDefinition <em>Derived Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Derived Type Definition</em>' containment reference.
	 * @see #getDerivedTypeDefinition()
	 * @generated
	 */
	void setDerivedTypeDefinition(DerivedTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Derived Record Extension Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived Record Extension Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived Record Extension Definition</em>' containment reference.
	 * @see #setDerivedRecordExtensionDefinition(DerivedRecordExtensionDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_DerivedRecordExtensionDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='derived_record_extension_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	DerivedRecordExtensionDefinition getDerivedRecordExtensionDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getDerivedRecordExtensionDefinition <em>Derived Record Extension Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Derived Record Extension Definition</em>' containment reference.
	 * @see #getDerivedRecordExtensionDefinition()
	 * @generated
	 */
	void setDerivedRecordExtensionDefinition(DerivedRecordExtensionDefinition value);

	/**
	 * Returns the value of the '<em><b>Enumeration Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumeration Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumeration Type Definition</em>' containment reference.
	 * @see #setEnumerationTypeDefinition(EnumerationTypeDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_EnumerationTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='enumeration_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	EnumerationTypeDefinition getEnumerationTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getEnumerationTypeDefinition <em>Enumeration Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enumeration Type Definition</em>' containment reference.
	 * @see #getEnumerationTypeDefinition()
	 * @generated
	 */
	void setEnumerationTypeDefinition(EnumerationTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Signed Integer Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signed Integer Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signed Integer Type Definition</em>' containment reference.
	 * @see #setSignedIntegerTypeDefinition(SignedIntegerTypeDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_SignedIntegerTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='signed_integer_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	SignedIntegerTypeDefinition getSignedIntegerTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getSignedIntegerTypeDefinition <em>Signed Integer Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Signed Integer Type Definition</em>' containment reference.
	 * @see #getSignedIntegerTypeDefinition()
	 * @generated
	 */
	void setSignedIntegerTypeDefinition(SignedIntegerTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Modular Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Modular Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Modular Type Definition</em>' containment reference.
	 * @see #setModularTypeDefinition(ModularTypeDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_ModularTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='modular_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	ModularTypeDefinition getModularTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getModularTypeDefinition <em>Modular Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Modular Type Definition</em>' containment reference.
	 * @see #getModularTypeDefinition()
	 * @generated
	 */
	void setModularTypeDefinition(ModularTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Root Integer Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Integer Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Integer Definition</em>' containment reference.
	 * @see #setRootIntegerDefinition(RootIntegerDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_RootIntegerDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='root_integer_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	RootIntegerDefinition getRootIntegerDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getRootIntegerDefinition <em>Root Integer Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root Integer Definition</em>' containment reference.
	 * @see #getRootIntegerDefinition()
	 * @generated
	 */
	void setRootIntegerDefinition(RootIntegerDefinition value);

	/**
	 * Returns the value of the '<em><b>Root Real Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Real Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Real Definition</em>' containment reference.
	 * @see #setRootRealDefinition(RootRealDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_RootRealDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='root_real_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	RootRealDefinition getRootRealDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getRootRealDefinition <em>Root Real Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root Real Definition</em>' containment reference.
	 * @see #getRootRealDefinition()
	 * @generated
	 */
	void setRootRealDefinition(RootRealDefinition value);

	/**
	 * Returns the value of the '<em><b>Universal Integer Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Universal Integer Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Universal Integer Definition</em>' containment reference.
	 * @see #setUniversalIntegerDefinition(UniversalIntegerDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_UniversalIntegerDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='universal_integer_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	UniversalIntegerDefinition getUniversalIntegerDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getUniversalIntegerDefinition <em>Universal Integer Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Universal Integer Definition</em>' containment reference.
	 * @see #getUniversalIntegerDefinition()
	 * @generated
	 */
	void setUniversalIntegerDefinition(UniversalIntegerDefinition value);

	/**
	 * Returns the value of the '<em><b>Universal Real Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Universal Real Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Universal Real Definition</em>' containment reference.
	 * @see #setUniversalRealDefinition(UniversalRealDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_UniversalRealDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='universal_real_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	UniversalRealDefinition getUniversalRealDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getUniversalRealDefinition <em>Universal Real Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Universal Real Definition</em>' containment reference.
	 * @see #getUniversalRealDefinition()
	 * @generated
	 */
	void setUniversalRealDefinition(UniversalRealDefinition value);

	/**
	 * Returns the value of the '<em><b>Universal Fixed Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Universal Fixed Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Universal Fixed Definition</em>' containment reference.
	 * @see #setUniversalFixedDefinition(UniversalFixedDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_UniversalFixedDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='universal_fixed_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	UniversalFixedDefinition getUniversalFixedDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getUniversalFixedDefinition <em>Universal Fixed Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Universal Fixed Definition</em>' containment reference.
	 * @see #getUniversalFixedDefinition()
	 * @generated
	 */
	void setUniversalFixedDefinition(UniversalFixedDefinition value);

	/**
	 * Returns the value of the '<em><b>Floating Point Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Floating Point Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Floating Point Definition</em>' containment reference.
	 * @see #setFloatingPointDefinition(FloatingPointDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_FloatingPointDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='floating_point_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	FloatingPointDefinition getFloatingPointDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getFloatingPointDefinition <em>Floating Point Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Floating Point Definition</em>' containment reference.
	 * @see #getFloatingPointDefinition()
	 * @generated
	 */
	void setFloatingPointDefinition(FloatingPointDefinition value);

	/**
	 * Returns the value of the '<em><b>Ordinary Fixed Point Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ordinary Fixed Point Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ordinary Fixed Point Definition</em>' containment reference.
	 * @see #setOrdinaryFixedPointDefinition(OrdinaryFixedPointDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_OrdinaryFixedPointDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ordinary_fixed_point_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	OrdinaryFixedPointDefinition getOrdinaryFixedPointDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getOrdinaryFixedPointDefinition <em>Ordinary Fixed Point Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ordinary Fixed Point Definition</em>' containment reference.
	 * @see #getOrdinaryFixedPointDefinition()
	 * @generated
	 */
	void setOrdinaryFixedPointDefinition(OrdinaryFixedPointDefinition value);

	/**
	 * Returns the value of the '<em><b>Decimal Fixed Point Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Decimal Fixed Point Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Decimal Fixed Point Definition</em>' containment reference.
	 * @see #setDecimalFixedPointDefinition(DecimalFixedPointDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_DecimalFixedPointDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='decimal_fixed_point_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	DecimalFixedPointDefinition getDecimalFixedPointDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getDecimalFixedPointDefinition <em>Decimal Fixed Point Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Decimal Fixed Point Definition</em>' containment reference.
	 * @see #getDecimalFixedPointDefinition()
	 * @generated
	 */
	void setDecimalFixedPointDefinition(DecimalFixedPointDefinition value);

	/**
	 * Returns the value of the '<em><b>Unconstrained Array Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unconstrained Array Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unconstrained Array Definition</em>' containment reference.
	 * @see #setUnconstrainedArrayDefinition(UnconstrainedArrayDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_UnconstrainedArrayDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unconstrained_array_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	UnconstrainedArrayDefinition getUnconstrainedArrayDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getUnconstrainedArrayDefinition <em>Unconstrained Array Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unconstrained Array Definition</em>' containment reference.
	 * @see #getUnconstrainedArrayDefinition()
	 * @generated
	 */
	void setUnconstrainedArrayDefinition(UnconstrainedArrayDefinition value);

	/**
	 * Returns the value of the '<em><b>Constrained Array Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constrained Array Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constrained Array Definition</em>' containment reference.
	 * @see #setConstrainedArrayDefinition(ConstrainedArrayDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_ConstrainedArrayDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='constrained_array_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	ConstrainedArrayDefinition getConstrainedArrayDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getConstrainedArrayDefinition <em>Constrained Array Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constrained Array Definition</em>' containment reference.
	 * @see #getConstrainedArrayDefinition()
	 * @generated
	 */
	void setConstrainedArrayDefinition(ConstrainedArrayDefinition value);

	/**
	 * Returns the value of the '<em><b>Record Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Type Definition</em>' containment reference.
	 * @see #setRecordTypeDefinition(RecordTypeDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_RecordTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='record_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	RecordTypeDefinition getRecordTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getRecordTypeDefinition <em>Record Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Record Type Definition</em>' containment reference.
	 * @see #getRecordTypeDefinition()
	 * @generated
	 */
	void setRecordTypeDefinition(RecordTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Tagged Record Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tagged Record Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tagged Record Type Definition</em>' containment reference.
	 * @see #setTaggedRecordTypeDefinition(TaggedRecordTypeDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_TaggedRecordTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='tagged_record_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	TaggedRecordTypeDefinition getTaggedRecordTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getTaggedRecordTypeDefinition <em>Tagged Record Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tagged Record Type Definition</em>' containment reference.
	 * @see #getTaggedRecordTypeDefinition()
	 * @generated
	 */
	void setTaggedRecordTypeDefinition(TaggedRecordTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Ordinary Interface</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ordinary Interface</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ordinary Interface</em>' containment reference.
	 * @see #setOrdinaryInterface(OrdinaryInterface)
	 * @see Ada.AdaPackage#getDefinitionClass_OrdinaryInterface()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='ordinary_interface' namespace='##targetNamespace'"
	 * @generated
	 */
	OrdinaryInterface getOrdinaryInterface();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getOrdinaryInterface <em>Ordinary Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ordinary Interface</em>' containment reference.
	 * @see #getOrdinaryInterface()
	 * @generated
	 */
	void setOrdinaryInterface(OrdinaryInterface value);

	/**
	 * Returns the value of the '<em><b>Limited Interface</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Limited Interface</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Limited Interface</em>' containment reference.
	 * @see #setLimitedInterface(LimitedInterface)
	 * @see Ada.AdaPackage#getDefinitionClass_LimitedInterface()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='limited_interface' namespace='##targetNamespace'"
	 * @generated
	 */
	LimitedInterface getLimitedInterface();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getLimitedInterface <em>Limited Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Limited Interface</em>' containment reference.
	 * @see #getLimitedInterface()
	 * @generated
	 */
	void setLimitedInterface(LimitedInterface value);

	/**
	 * Returns the value of the '<em><b>Task Interface</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Interface</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Interface</em>' containment reference.
	 * @see #setTaskInterface(TaskInterface)
	 * @see Ada.AdaPackage#getDefinitionClass_TaskInterface()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='task_interface' namespace='##targetNamespace'"
	 * @generated
	 */
	TaskInterface getTaskInterface();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getTaskInterface <em>Task Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Interface</em>' containment reference.
	 * @see #getTaskInterface()
	 * @generated
	 */
	void setTaskInterface(TaskInterface value);

	/**
	 * Returns the value of the '<em><b>Protected Interface</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protected Interface</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protected Interface</em>' containment reference.
	 * @see #setProtectedInterface(ProtectedInterface)
	 * @see Ada.AdaPackage#getDefinitionClass_ProtectedInterface()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='protected_interface' namespace='##targetNamespace'"
	 * @generated
	 */
	ProtectedInterface getProtectedInterface();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getProtectedInterface <em>Protected Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Protected Interface</em>' containment reference.
	 * @see #getProtectedInterface()
	 * @generated
	 */
	void setProtectedInterface(ProtectedInterface value);

	/**
	 * Returns the value of the '<em><b>Synchronized Interface</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Synchronized Interface</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Synchronized Interface</em>' containment reference.
	 * @see #setSynchronizedInterface(SynchronizedInterface)
	 * @see Ada.AdaPackage#getDefinitionClass_SynchronizedInterface()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='synchronized_interface' namespace='##targetNamespace'"
	 * @generated
	 */
	SynchronizedInterface getSynchronizedInterface();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getSynchronizedInterface <em>Synchronized Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Synchronized Interface</em>' containment reference.
	 * @see #getSynchronizedInterface()
	 * @generated
	 */
	void setSynchronizedInterface(SynchronizedInterface value);

	/**
	 * Returns the value of the '<em><b>Pool Specific Access To Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pool Specific Access To Variable</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pool Specific Access To Variable</em>' containment reference.
	 * @see #setPoolSpecificAccessToVariable(PoolSpecificAccessToVariable)
	 * @see Ada.AdaPackage#getDefinitionClass_PoolSpecificAccessToVariable()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='pool_specific_access_to_variable' namespace='##targetNamespace'"
	 * @generated
	 */
	PoolSpecificAccessToVariable getPoolSpecificAccessToVariable();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getPoolSpecificAccessToVariable <em>Pool Specific Access To Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pool Specific Access To Variable</em>' containment reference.
	 * @see #getPoolSpecificAccessToVariable()
	 * @generated
	 */
	void setPoolSpecificAccessToVariable(PoolSpecificAccessToVariable value);

	/**
	 * Returns the value of the '<em><b>Access To Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Variable</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Variable</em>' containment reference.
	 * @see #setAccessToVariable(AccessToVariable)
	 * @see Ada.AdaPackage#getDefinitionClass_AccessToVariable()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='access_to_variable' namespace='##targetNamespace'"
	 * @generated
	 */
	AccessToVariable getAccessToVariable();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getAccessToVariable <em>Access To Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access To Variable</em>' containment reference.
	 * @see #getAccessToVariable()
	 * @generated
	 */
	void setAccessToVariable(AccessToVariable value);

	/**
	 * Returns the value of the '<em><b>Access To Constant</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Constant</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Constant</em>' containment reference.
	 * @see #setAccessToConstant(AccessToConstant)
	 * @see Ada.AdaPackage#getDefinitionClass_AccessToConstant()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='access_to_constant' namespace='##targetNamespace'"
	 * @generated
	 */
	AccessToConstant getAccessToConstant();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getAccessToConstant <em>Access To Constant</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access To Constant</em>' containment reference.
	 * @see #getAccessToConstant()
	 * @generated
	 */
	void setAccessToConstant(AccessToConstant value);

	/**
	 * Returns the value of the '<em><b>Access To Procedure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Procedure</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Procedure</em>' containment reference.
	 * @see #setAccessToProcedure(AccessToProcedure)
	 * @see Ada.AdaPackage#getDefinitionClass_AccessToProcedure()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='access_to_procedure' namespace='##targetNamespace'"
	 * @generated
	 */
	AccessToProcedure getAccessToProcedure();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getAccessToProcedure <em>Access To Procedure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access To Procedure</em>' containment reference.
	 * @see #getAccessToProcedure()
	 * @generated
	 */
	void setAccessToProcedure(AccessToProcedure value);

	/**
	 * Returns the value of the '<em><b>Access To Protected Procedure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Protected Procedure</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Protected Procedure</em>' containment reference.
	 * @see #setAccessToProtectedProcedure(AccessToProtectedProcedure)
	 * @see Ada.AdaPackage#getDefinitionClass_AccessToProtectedProcedure()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='access_to_protected_procedure' namespace='##targetNamespace'"
	 * @generated
	 */
	AccessToProtectedProcedure getAccessToProtectedProcedure();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getAccessToProtectedProcedure <em>Access To Protected Procedure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access To Protected Procedure</em>' containment reference.
	 * @see #getAccessToProtectedProcedure()
	 * @generated
	 */
	void setAccessToProtectedProcedure(AccessToProtectedProcedure value);

	/**
	 * Returns the value of the '<em><b>Access To Function</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Function</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Function</em>' containment reference.
	 * @see #setAccessToFunction(AccessToFunction)
	 * @see Ada.AdaPackage#getDefinitionClass_AccessToFunction()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='access_to_function' namespace='##targetNamespace'"
	 * @generated
	 */
	AccessToFunction getAccessToFunction();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getAccessToFunction <em>Access To Function</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access To Function</em>' containment reference.
	 * @see #getAccessToFunction()
	 * @generated
	 */
	void setAccessToFunction(AccessToFunction value);

	/**
	 * Returns the value of the '<em><b>Access To Protected Function</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Protected Function</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Protected Function</em>' containment reference.
	 * @see #setAccessToProtectedFunction(AccessToProtectedFunction)
	 * @see Ada.AdaPackage#getDefinitionClass_AccessToProtectedFunction()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='access_to_protected_function' namespace='##targetNamespace'"
	 * @generated
	 */
	AccessToProtectedFunction getAccessToProtectedFunction();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getAccessToProtectedFunction <em>Access To Protected Function</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access To Protected Function</em>' containment reference.
	 * @see #getAccessToProtectedFunction()
	 * @generated
	 */
	void setAccessToProtectedFunction(AccessToProtectedFunction value);

	/**
	 * Returns the value of the '<em><b>Subtype Indication</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subtype Indication</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subtype Indication</em>' containment reference.
	 * @see #setSubtypeIndication(SubtypeIndication)
	 * @see Ada.AdaPackage#getDefinitionClass_SubtypeIndication()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='subtype_indication' namespace='##targetNamespace'"
	 * @generated
	 */
	SubtypeIndication getSubtypeIndication();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getSubtypeIndication <em>Subtype Indication</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Subtype Indication</em>' containment reference.
	 * @see #getSubtypeIndication()
	 * @generated
	 */
	void setSubtypeIndication(SubtypeIndication value);

	/**
	 * Returns the value of the '<em><b>Range Attribute Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Range Attribute Reference</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range Attribute Reference</em>' containment reference.
	 * @see #setRangeAttributeReference(RangeAttributeReference)
	 * @see Ada.AdaPackage#getDefinitionClass_RangeAttributeReference()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='range_attribute_reference' namespace='##targetNamespace'"
	 * @generated
	 */
	RangeAttributeReference getRangeAttributeReference();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getRangeAttributeReference <em>Range Attribute Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Range Attribute Reference</em>' containment reference.
	 * @see #getRangeAttributeReference()
	 * @generated
	 */
	void setRangeAttributeReference(RangeAttributeReference value);

	/**
	 * Returns the value of the '<em><b>Simple Expression Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simple Expression Range</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simple Expression Range</em>' containment reference.
	 * @see #setSimpleExpressionRange(SimpleExpressionRange)
	 * @see Ada.AdaPackage#getDefinitionClass_SimpleExpressionRange()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='simple_expression_range' namespace='##targetNamespace'"
	 * @generated
	 */
	SimpleExpressionRange getSimpleExpressionRange();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getSimpleExpressionRange <em>Simple Expression Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Simple Expression Range</em>' containment reference.
	 * @see #getSimpleExpressionRange()
	 * @generated
	 */
	void setSimpleExpressionRange(SimpleExpressionRange value);

	/**
	 * Returns the value of the '<em><b>Digits Constraint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Digits Constraint</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Digits Constraint</em>' containment reference.
	 * @see #setDigitsConstraint(DigitsConstraint)
	 * @see Ada.AdaPackage#getDefinitionClass_DigitsConstraint()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='digits_constraint' namespace='##targetNamespace'"
	 * @generated
	 */
	DigitsConstraint getDigitsConstraint();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getDigitsConstraint <em>Digits Constraint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Digits Constraint</em>' containment reference.
	 * @see #getDigitsConstraint()
	 * @generated
	 */
	void setDigitsConstraint(DigitsConstraint value);

	/**
	 * Returns the value of the '<em><b>Delta Constraint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delta Constraint</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delta Constraint</em>' containment reference.
	 * @see #setDeltaConstraint(DeltaConstraint)
	 * @see Ada.AdaPackage#getDefinitionClass_DeltaConstraint()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='delta_constraint' namespace='##targetNamespace'"
	 * @generated
	 */
	DeltaConstraint getDeltaConstraint();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getDeltaConstraint <em>Delta Constraint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delta Constraint</em>' containment reference.
	 * @see #getDeltaConstraint()
	 * @generated
	 */
	void setDeltaConstraint(DeltaConstraint value);

	/**
	 * Returns the value of the '<em><b>Index Constraint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Index Constraint</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Index Constraint</em>' containment reference.
	 * @see #setIndexConstraint(IndexConstraint)
	 * @see Ada.AdaPackage#getDefinitionClass_IndexConstraint()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='index_constraint' namespace='##targetNamespace'"
	 * @generated
	 */
	IndexConstraint getIndexConstraint();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getIndexConstraint <em>Index Constraint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Index Constraint</em>' containment reference.
	 * @see #getIndexConstraint()
	 * @generated
	 */
	void setIndexConstraint(IndexConstraint value);

	/**
	 * Returns the value of the '<em><b>Discriminant Constraint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discriminant Constraint</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discriminant Constraint</em>' containment reference.
	 * @see #setDiscriminantConstraint(DiscriminantConstraint)
	 * @see Ada.AdaPackage#getDefinitionClass_DiscriminantConstraint()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='discriminant_constraint' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscriminantConstraint getDiscriminantConstraint();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getDiscriminantConstraint <em>Discriminant Constraint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discriminant Constraint</em>' containment reference.
	 * @see #getDiscriminantConstraint()
	 * @generated
	 */
	void setDiscriminantConstraint(DiscriminantConstraint value);

	/**
	 * Returns the value of the '<em><b>Component Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Definition</em>' containment reference.
	 * @see #setComponentDefinition(ComponentDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_ComponentDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='component_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	ComponentDefinition getComponentDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getComponentDefinition <em>Component Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component Definition</em>' containment reference.
	 * @see #getComponentDefinition()
	 * @generated
	 */
	void setComponentDefinition(ComponentDefinition value);

	/**
	 * Returns the value of the '<em><b>Discrete Subtype Indication As Subtype Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Subtype Indication As Subtype Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Subtype Indication As Subtype Definition</em>' containment reference.
	 * @see #setDiscreteSubtypeIndicationAsSubtypeDefinition(DiscreteSubtypeIndicationAsSubtypeDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_DiscreteSubtypeIndicationAsSubtypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='discrete_subtype_indication_as_subtype_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscreteSubtypeIndicationAsSubtypeDefinition getDiscreteSubtypeIndicationAsSubtypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getDiscreteSubtypeIndicationAsSubtypeDefinition <em>Discrete Subtype Indication As Subtype Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discrete Subtype Indication As Subtype Definition</em>' containment reference.
	 * @see #getDiscreteSubtypeIndicationAsSubtypeDefinition()
	 * @generated
	 */
	void setDiscreteSubtypeIndicationAsSubtypeDefinition(DiscreteSubtypeIndicationAsSubtypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Discrete Range Attribute Reference As Subtype Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Range Attribute Reference As Subtype Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Range Attribute Reference As Subtype Definition</em>' containment reference.
	 * @see #setDiscreteRangeAttributeReferenceAsSubtypeDefinition(DiscreteRangeAttributeReferenceAsSubtypeDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_DiscreteRangeAttributeReferenceAsSubtypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='discrete_range_attribute_reference_as_subtype_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscreteRangeAttributeReferenceAsSubtypeDefinition getDiscreteRangeAttributeReferenceAsSubtypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getDiscreteRangeAttributeReferenceAsSubtypeDefinition <em>Discrete Range Attribute Reference As Subtype Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discrete Range Attribute Reference As Subtype Definition</em>' containment reference.
	 * @see #getDiscreteRangeAttributeReferenceAsSubtypeDefinition()
	 * @generated
	 */
	void setDiscreteRangeAttributeReferenceAsSubtypeDefinition(DiscreteRangeAttributeReferenceAsSubtypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Discrete Simple Expression Range As Subtype Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Simple Expression Range As Subtype Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Simple Expression Range As Subtype Definition</em>' containment reference.
	 * @see #setDiscreteSimpleExpressionRangeAsSubtypeDefinition(DiscreteSimpleExpressionRangeAsSubtypeDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_DiscreteSimpleExpressionRangeAsSubtypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='discrete_simple_expression_range_as_subtype_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscreteSimpleExpressionRangeAsSubtypeDefinition getDiscreteSimpleExpressionRangeAsSubtypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getDiscreteSimpleExpressionRangeAsSubtypeDefinition <em>Discrete Simple Expression Range As Subtype Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discrete Simple Expression Range As Subtype Definition</em>' containment reference.
	 * @see #getDiscreteSimpleExpressionRangeAsSubtypeDefinition()
	 * @generated
	 */
	void setDiscreteSimpleExpressionRangeAsSubtypeDefinition(DiscreteSimpleExpressionRangeAsSubtypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Discrete Subtype Indication</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Subtype Indication</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Subtype Indication</em>' containment reference.
	 * @see #setDiscreteSubtypeIndication(DiscreteSubtypeIndication)
	 * @see Ada.AdaPackage#getDefinitionClass_DiscreteSubtypeIndication()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='discrete_subtype_indication' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscreteSubtypeIndication getDiscreteSubtypeIndication();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getDiscreteSubtypeIndication <em>Discrete Subtype Indication</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discrete Subtype Indication</em>' containment reference.
	 * @see #getDiscreteSubtypeIndication()
	 * @generated
	 */
	void setDiscreteSubtypeIndication(DiscreteSubtypeIndication value);

	/**
	 * Returns the value of the '<em><b>Discrete Range Attribute Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Range Attribute Reference</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Range Attribute Reference</em>' containment reference.
	 * @see #setDiscreteRangeAttributeReference(DiscreteRangeAttributeReference)
	 * @see Ada.AdaPackage#getDefinitionClass_DiscreteRangeAttributeReference()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='discrete_range_attribute_reference' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscreteRangeAttributeReference getDiscreteRangeAttributeReference();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getDiscreteRangeAttributeReference <em>Discrete Range Attribute Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discrete Range Attribute Reference</em>' containment reference.
	 * @see #getDiscreteRangeAttributeReference()
	 * @generated
	 */
	void setDiscreteRangeAttributeReference(DiscreteRangeAttributeReference value);

	/**
	 * Returns the value of the '<em><b>Discrete Simple Expression Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Simple Expression Range</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Simple Expression Range</em>' containment reference.
	 * @see #setDiscreteSimpleExpressionRange(DiscreteSimpleExpressionRange)
	 * @see Ada.AdaPackage#getDefinitionClass_DiscreteSimpleExpressionRange()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='discrete_simple_expression_range' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscreteSimpleExpressionRange getDiscreteSimpleExpressionRange();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getDiscreteSimpleExpressionRange <em>Discrete Simple Expression Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discrete Simple Expression Range</em>' containment reference.
	 * @see #getDiscreteSimpleExpressionRange()
	 * @generated
	 */
	void setDiscreteSimpleExpressionRange(DiscreteSimpleExpressionRange value);

	/**
	 * Returns the value of the '<em><b>Unknown Discriminant Part</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unknown Discriminant Part</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unknown Discriminant Part</em>' containment reference.
	 * @see #setUnknownDiscriminantPart(UnknownDiscriminantPart)
	 * @see Ada.AdaPackage#getDefinitionClass_UnknownDiscriminantPart()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unknown_discriminant_part' namespace='##targetNamespace'"
	 * @generated
	 */
	UnknownDiscriminantPart getUnknownDiscriminantPart();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getUnknownDiscriminantPart <em>Unknown Discriminant Part</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unknown Discriminant Part</em>' containment reference.
	 * @see #getUnknownDiscriminantPart()
	 * @generated
	 */
	void setUnknownDiscriminantPart(UnknownDiscriminantPart value);

	/**
	 * Returns the value of the '<em><b>Known Discriminant Part</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Known Discriminant Part</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Known Discriminant Part</em>' containment reference.
	 * @see #setKnownDiscriminantPart(KnownDiscriminantPart)
	 * @see Ada.AdaPackage#getDefinitionClass_KnownDiscriminantPart()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='known_discriminant_part' namespace='##targetNamespace'"
	 * @generated
	 */
	KnownDiscriminantPart getKnownDiscriminantPart();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getKnownDiscriminantPart <em>Known Discriminant Part</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Known Discriminant Part</em>' containment reference.
	 * @see #getKnownDiscriminantPart()
	 * @generated
	 */
	void setKnownDiscriminantPart(KnownDiscriminantPart value);

	/**
	 * Returns the value of the '<em><b>Record Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Definition</em>' containment reference.
	 * @see #setRecordDefinition(RecordDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_RecordDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='record_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	RecordDefinition getRecordDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getRecordDefinition <em>Record Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Record Definition</em>' containment reference.
	 * @see #getRecordDefinition()
	 * @generated
	 */
	void setRecordDefinition(RecordDefinition value);

	/**
	 * Returns the value of the '<em><b>Null Record Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Null Record Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Null Record Definition</em>' containment reference.
	 * @see #setNullRecordDefinition(NullRecordDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_NullRecordDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='null_record_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	NullRecordDefinition getNullRecordDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getNullRecordDefinition <em>Null Record Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Null Record Definition</em>' containment reference.
	 * @see #getNullRecordDefinition()
	 * @generated
	 */
	void setNullRecordDefinition(NullRecordDefinition value);

	/**
	 * Returns the value of the '<em><b>Null Component</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Null Component</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Null Component</em>' containment reference.
	 * @see #setNullComponent(NullComponent)
	 * @see Ada.AdaPackage#getDefinitionClass_NullComponent()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='null_component' namespace='##targetNamespace'"
	 * @generated
	 */
	NullComponent getNullComponent();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getNullComponent <em>Null Component</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Null Component</em>' containment reference.
	 * @see #getNullComponent()
	 * @generated
	 */
	void setNullComponent(NullComponent value);

	/**
	 * Returns the value of the '<em><b>Variant Part</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variant Part</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variant Part</em>' containment reference.
	 * @see #setVariantPart(VariantPart)
	 * @see Ada.AdaPackage#getDefinitionClass_VariantPart()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='variant_part' namespace='##targetNamespace'"
	 * @generated
	 */
	VariantPart getVariantPart();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getVariantPart <em>Variant Part</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variant Part</em>' containment reference.
	 * @see #getVariantPart()
	 * @generated
	 */
	void setVariantPart(VariantPart value);

	/**
	 * Returns the value of the '<em><b>Variant</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variant</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variant</em>' containment reference.
	 * @see #setVariant(Variant)
	 * @see Ada.AdaPackage#getDefinitionClass_Variant()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='variant' namespace='##targetNamespace'"
	 * @generated
	 */
	Variant getVariant();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getVariant <em>Variant</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variant</em>' containment reference.
	 * @see #getVariant()
	 * @generated
	 */
	void setVariant(Variant value);

	/**
	 * Returns the value of the '<em><b>Others Choice</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Others Choice</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Others Choice</em>' containment reference.
	 * @see #setOthersChoice(OthersChoice)
	 * @see Ada.AdaPackage#getDefinitionClass_OthersChoice()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='others_choice' namespace='##targetNamespace'"
	 * @generated
	 */
	OthersChoice getOthersChoice();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getOthersChoice <em>Others Choice</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Others Choice</em>' containment reference.
	 * @see #getOthersChoice()
	 * @generated
	 */
	void setOthersChoice(OthersChoice value);

	/**
	 * Returns the value of the '<em><b>Anonymous Access To Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Access To Variable</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Access To Variable</em>' containment reference.
	 * @see #setAnonymousAccessToVariable(AnonymousAccessToVariable)
	 * @see Ada.AdaPackage#getDefinitionClass_AnonymousAccessToVariable()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='anonymous_access_to_variable' namespace='##targetNamespace'"
	 * @generated
	 */
	AnonymousAccessToVariable getAnonymousAccessToVariable();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getAnonymousAccessToVariable <em>Anonymous Access To Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Anonymous Access To Variable</em>' containment reference.
	 * @see #getAnonymousAccessToVariable()
	 * @generated
	 */
	void setAnonymousAccessToVariable(AnonymousAccessToVariable value);

	/**
	 * Returns the value of the '<em><b>Anonymous Access To Constant</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Access To Constant</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Access To Constant</em>' containment reference.
	 * @see #setAnonymousAccessToConstant(AnonymousAccessToConstant)
	 * @see Ada.AdaPackage#getDefinitionClass_AnonymousAccessToConstant()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='anonymous_access_to_constant' namespace='##targetNamespace'"
	 * @generated
	 */
	AnonymousAccessToConstant getAnonymousAccessToConstant();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getAnonymousAccessToConstant <em>Anonymous Access To Constant</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Anonymous Access To Constant</em>' containment reference.
	 * @see #getAnonymousAccessToConstant()
	 * @generated
	 */
	void setAnonymousAccessToConstant(AnonymousAccessToConstant value);

	/**
	 * Returns the value of the '<em><b>Anonymous Access To Procedure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Access To Procedure</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Access To Procedure</em>' containment reference.
	 * @see #setAnonymousAccessToProcedure(AnonymousAccessToProcedure)
	 * @see Ada.AdaPackage#getDefinitionClass_AnonymousAccessToProcedure()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='anonymous_access_to_procedure' namespace='##targetNamespace'"
	 * @generated
	 */
	AnonymousAccessToProcedure getAnonymousAccessToProcedure();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getAnonymousAccessToProcedure <em>Anonymous Access To Procedure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Anonymous Access To Procedure</em>' containment reference.
	 * @see #getAnonymousAccessToProcedure()
	 * @generated
	 */
	void setAnonymousAccessToProcedure(AnonymousAccessToProcedure value);

	/**
	 * Returns the value of the '<em><b>Anonymous Access To Protected Procedure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Access To Protected Procedure</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Access To Protected Procedure</em>' containment reference.
	 * @see #setAnonymousAccessToProtectedProcedure(AnonymousAccessToProtectedProcedure)
	 * @see Ada.AdaPackage#getDefinitionClass_AnonymousAccessToProtectedProcedure()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='anonymous_access_to_protected_procedure' namespace='##targetNamespace'"
	 * @generated
	 */
	AnonymousAccessToProtectedProcedure getAnonymousAccessToProtectedProcedure();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getAnonymousAccessToProtectedProcedure <em>Anonymous Access To Protected Procedure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Anonymous Access To Protected Procedure</em>' containment reference.
	 * @see #getAnonymousAccessToProtectedProcedure()
	 * @generated
	 */
	void setAnonymousAccessToProtectedProcedure(AnonymousAccessToProtectedProcedure value);

	/**
	 * Returns the value of the '<em><b>Anonymous Access To Function</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Access To Function</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Access To Function</em>' containment reference.
	 * @see #setAnonymousAccessToFunction(AnonymousAccessToFunction)
	 * @see Ada.AdaPackage#getDefinitionClass_AnonymousAccessToFunction()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='anonymous_access_to_function' namespace='##targetNamespace'"
	 * @generated
	 */
	AnonymousAccessToFunction getAnonymousAccessToFunction();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getAnonymousAccessToFunction <em>Anonymous Access To Function</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Anonymous Access To Function</em>' containment reference.
	 * @see #getAnonymousAccessToFunction()
	 * @generated
	 */
	void setAnonymousAccessToFunction(AnonymousAccessToFunction value);

	/**
	 * Returns the value of the '<em><b>Anonymous Access To Protected Function</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Access To Protected Function</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Access To Protected Function</em>' containment reference.
	 * @see #setAnonymousAccessToProtectedFunction(AnonymousAccessToProtectedFunction)
	 * @see Ada.AdaPackage#getDefinitionClass_AnonymousAccessToProtectedFunction()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='anonymous_access_to_protected_function' namespace='##targetNamespace'"
	 * @generated
	 */
	AnonymousAccessToProtectedFunction getAnonymousAccessToProtectedFunction();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getAnonymousAccessToProtectedFunction <em>Anonymous Access To Protected Function</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Anonymous Access To Protected Function</em>' containment reference.
	 * @see #getAnonymousAccessToProtectedFunction()
	 * @generated
	 */
	void setAnonymousAccessToProtectedFunction(AnonymousAccessToProtectedFunction value);

	/**
	 * Returns the value of the '<em><b>Private Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Private Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Private Type Definition</em>' containment reference.
	 * @see #setPrivateTypeDefinition(PrivateTypeDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_PrivateTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='private_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	PrivateTypeDefinition getPrivateTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getPrivateTypeDefinition <em>Private Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Private Type Definition</em>' containment reference.
	 * @see #getPrivateTypeDefinition()
	 * @generated
	 */
	void setPrivateTypeDefinition(PrivateTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Tagged Private Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tagged Private Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tagged Private Type Definition</em>' containment reference.
	 * @see #setTaggedPrivateTypeDefinition(TaggedPrivateTypeDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_TaggedPrivateTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='tagged_private_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	TaggedPrivateTypeDefinition getTaggedPrivateTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getTaggedPrivateTypeDefinition <em>Tagged Private Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tagged Private Type Definition</em>' containment reference.
	 * @see #getTaggedPrivateTypeDefinition()
	 * @generated
	 */
	void setTaggedPrivateTypeDefinition(TaggedPrivateTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Private Extension Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Private Extension Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Private Extension Definition</em>' containment reference.
	 * @see #setPrivateExtensionDefinition(PrivateExtensionDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_PrivateExtensionDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='private_extension_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	PrivateExtensionDefinition getPrivateExtensionDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getPrivateExtensionDefinition <em>Private Extension Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Private Extension Definition</em>' containment reference.
	 * @see #getPrivateExtensionDefinition()
	 * @generated
	 */
	void setPrivateExtensionDefinition(PrivateExtensionDefinition value);

	/**
	 * Returns the value of the '<em><b>Task Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Definition</em>' containment reference.
	 * @see #setTaskDefinition(TaskDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_TaskDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='task_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	TaskDefinition getTaskDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getTaskDefinition <em>Task Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Definition</em>' containment reference.
	 * @see #getTaskDefinition()
	 * @generated
	 */
	void setTaskDefinition(TaskDefinition value);

	/**
	 * Returns the value of the '<em><b>Protected Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protected Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protected Definition</em>' containment reference.
	 * @see #setProtectedDefinition(ProtectedDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_ProtectedDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='protected_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	ProtectedDefinition getProtectedDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getProtectedDefinition <em>Protected Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Protected Definition</em>' containment reference.
	 * @see #getProtectedDefinition()
	 * @generated
	 */
	void setProtectedDefinition(ProtectedDefinition value);

	/**
	 * Returns the value of the '<em><b>Formal Private Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Private Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Private Type Definition</em>' containment reference.
	 * @see #setFormalPrivateTypeDefinition(FormalPrivateTypeDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_FormalPrivateTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_private_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalPrivateTypeDefinition getFormalPrivateTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getFormalPrivateTypeDefinition <em>Formal Private Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Private Type Definition</em>' containment reference.
	 * @see #getFormalPrivateTypeDefinition()
	 * @generated
	 */
	void setFormalPrivateTypeDefinition(FormalPrivateTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Formal Tagged Private Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Tagged Private Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Tagged Private Type Definition</em>' containment reference.
	 * @see #setFormalTaggedPrivateTypeDefinition(FormalTaggedPrivateTypeDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_FormalTaggedPrivateTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_tagged_private_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalTaggedPrivateTypeDefinition getFormalTaggedPrivateTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getFormalTaggedPrivateTypeDefinition <em>Formal Tagged Private Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Tagged Private Type Definition</em>' containment reference.
	 * @see #getFormalTaggedPrivateTypeDefinition()
	 * @generated
	 */
	void setFormalTaggedPrivateTypeDefinition(FormalTaggedPrivateTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Formal Derived Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Derived Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Derived Type Definition</em>' containment reference.
	 * @see #setFormalDerivedTypeDefinition(FormalDerivedTypeDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_FormalDerivedTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_derived_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalDerivedTypeDefinition getFormalDerivedTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getFormalDerivedTypeDefinition <em>Formal Derived Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Derived Type Definition</em>' containment reference.
	 * @see #getFormalDerivedTypeDefinition()
	 * @generated
	 */
	void setFormalDerivedTypeDefinition(FormalDerivedTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Formal Discrete Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Discrete Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Discrete Type Definition</em>' containment reference.
	 * @see #setFormalDiscreteTypeDefinition(FormalDiscreteTypeDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_FormalDiscreteTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_discrete_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalDiscreteTypeDefinition getFormalDiscreteTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getFormalDiscreteTypeDefinition <em>Formal Discrete Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Discrete Type Definition</em>' containment reference.
	 * @see #getFormalDiscreteTypeDefinition()
	 * @generated
	 */
	void setFormalDiscreteTypeDefinition(FormalDiscreteTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Formal Signed Integer Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Signed Integer Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Signed Integer Type Definition</em>' containment reference.
	 * @see #setFormalSignedIntegerTypeDefinition(FormalSignedIntegerTypeDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_FormalSignedIntegerTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_signed_integer_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalSignedIntegerTypeDefinition getFormalSignedIntegerTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getFormalSignedIntegerTypeDefinition <em>Formal Signed Integer Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Signed Integer Type Definition</em>' containment reference.
	 * @see #getFormalSignedIntegerTypeDefinition()
	 * @generated
	 */
	void setFormalSignedIntegerTypeDefinition(FormalSignedIntegerTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Formal Modular Type Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Modular Type Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Modular Type Definition</em>' containment reference.
	 * @see #setFormalModularTypeDefinition(FormalModularTypeDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_FormalModularTypeDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_modular_type_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalModularTypeDefinition getFormalModularTypeDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getFormalModularTypeDefinition <em>Formal Modular Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Modular Type Definition</em>' containment reference.
	 * @see #getFormalModularTypeDefinition()
	 * @generated
	 */
	void setFormalModularTypeDefinition(FormalModularTypeDefinition value);

	/**
	 * Returns the value of the '<em><b>Formal Floating Point Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Floating Point Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Floating Point Definition</em>' containment reference.
	 * @see #setFormalFloatingPointDefinition(FormalFloatingPointDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_FormalFloatingPointDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_floating_point_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalFloatingPointDefinition getFormalFloatingPointDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getFormalFloatingPointDefinition <em>Formal Floating Point Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Floating Point Definition</em>' containment reference.
	 * @see #getFormalFloatingPointDefinition()
	 * @generated
	 */
	void setFormalFloatingPointDefinition(FormalFloatingPointDefinition value);

	/**
	 * Returns the value of the '<em><b>Formal Ordinary Fixed Point Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Ordinary Fixed Point Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Ordinary Fixed Point Definition</em>' containment reference.
	 * @see #setFormalOrdinaryFixedPointDefinition(FormalOrdinaryFixedPointDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_FormalOrdinaryFixedPointDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_ordinary_fixed_point_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalOrdinaryFixedPointDefinition getFormalOrdinaryFixedPointDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getFormalOrdinaryFixedPointDefinition <em>Formal Ordinary Fixed Point Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Ordinary Fixed Point Definition</em>' containment reference.
	 * @see #getFormalOrdinaryFixedPointDefinition()
	 * @generated
	 */
	void setFormalOrdinaryFixedPointDefinition(FormalOrdinaryFixedPointDefinition value);

	/**
	 * Returns the value of the '<em><b>Formal Decimal Fixed Point Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Decimal Fixed Point Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Decimal Fixed Point Definition</em>' containment reference.
	 * @see #setFormalDecimalFixedPointDefinition(FormalDecimalFixedPointDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_FormalDecimalFixedPointDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_decimal_fixed_point_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalDecimalFixedPointDefinition getFormalDecimalFixedPointDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getFormalDecimalFixedPointDefinition <em>Formal Decimal Fixed Point Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Decimal Fixed Point Definition</em>' containment reference.
	 * @see #getFormalDecimalFixedPointDefinition()
	 * @generated
	 */
	void setFormalDecimalFixedPointDefinition(FormalDecimalFixedPointDefinition value);

	/**
	 * Returns the value of the '<em><b>Formal Ordinary Interface</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Ordinary Interface</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Ordinary Interface</em>' containment reference.
	 * @see #setFormalOrdinaryInterface(FormalOrdinaryInterface)
	 * @see Ada.AdaPackage#getDefinitionClass_FormalOrdinaryInterface()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_ordinary_interface' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalOrdinaryInterface getFormalOrdinaryInterface();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getFormalOrdinaryInterface <em>Formal Ordinary Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Ordinary Interface</em>' containment reference.
	 * @see #getFormalOrdinaryInterface()
	 * @generated
	 */
	void setFormalOrdinaryInterface(FormalOrdinaryInterface value);

	/**
	 * Returns the value of the '<em><b>Formal Limited Interface</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Limited Interface</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Limited Interface</em>' containment reference.
	 * @see #setFormalLimitedInterface(FormalLimitedInterface)
	 * @see Ada.AdaPackage#getDefinitionClass_FormalLimitedInterface()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_limited_interface' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalLimitedInterface getFormalLimitedInterface();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getFormalLimitedInterface <em>Formal Limited Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Limited Interface</em>' containment reference.
	 * @see #getFormalLimitedInterface()
	 * @generated
	 */
	void setFormalLimitedInterface(FormalLimitedInterface value);

	/**
	 * Returns the value of the '<em><b>Formal Task Interface</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Task Interface</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Task Interface</em>' containment reference.
	 * @see #setFormalTaskInterface(FormalTaskInterface)
	 * @see Ada.AdaPackage#getDefinitionClass_FormalTaskInterface()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_task_interface' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalTaskInterface getFormalTaskInterface();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getFormalTaskInterface <em>Formal Task Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Task Interface</em>' containment reference.
	 * @see #getFormalTaskInterface()
	 * @generated
	 */
	void setFormalTaskInterface(FormalTaskInterface value);

	/**
	 * Returns the value of the '<em><b>Formal Protected Interface</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Protected Interface</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Protected Interface</em>' containment reference.
	 * @see #setFormalProtectedInterface(FormalProtectedInterface)
	 * @see Ada.AdaPackage#getDefinitionClass_FormalProtectedInterface()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_protected_interface' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalProtectedInterface getFormalProtectedInterface();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getFormalProtectedInterface <em>Formal Protected Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Protected Interface</em>' containment reference.
	 * @see #getFormalProtectedInterface()
	 * @generated
	 */
	void setFormalProtectedInterface(FormalProtectedInterface value);

	/**
	 * Returns the value of the '<em><b>Formal Synchronized Interface</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Synchronized Interface</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Synchronized Interface</em>' containment reference.
	 * @see #setFormalSynchronizedInterface(FormalSynchronizedInterface)
	 * @see Ada.AdaPackage#getDefinitionClass_FormalSynchronizedInterface()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_synchronized_interface' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalSynchronizedInterface getFormalSynchronizedInterface();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getFormalSynchronizedInterface <em>Formal Synchronized Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Synchronized Interface</em>' containment reference.
	 * @see #getFormalSynchronizedInterface()
	 * @generated
	 */
	void setFormalSynchronizedInterface(FormalSynchronizedInterface value);

	/**
	 * Returns the value of the '<em><b>Formal Unconstrained Array Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Unconstrained Array Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Unconstrained Array Definition</em>' containment reference.
	 * @see #setFormalUnconstrainedArrayDefinition(FormalUnconstrainedArrayDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_FormalUnconstrainedArrayDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_unconstrained_array_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalUnconstrainedArrayDefinition getFormalUnconstrainedArrayDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getFormalUnconstrainedArrayDefinition <em>Formal Unconstrained Array Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Unconstrained Array Definition</em>' containment reference.
	 * @see #getFormalUnconstrainedArrayDefinition()
	 * @generated
	 */
	void setFormalUnconstrainedArrayDefinition(FormalUnconstrainedArrayDefinition value);

	/**
	 * Returns the value of the '<em><b>Formal Constrained Array Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Constrained Array Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Constrained Array Definition</em>' containment reference.
	 * @see #setFormalConstrainedArrayDefinition(FormalConstrainedArrayDefinition)
	 * @see Ada.AdaPackage#getDefinitionClass_FormalConstrainedArrayDefinition()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_constrained_array_definition' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalConstrainedArrayDefinition getFormalConstrainedArrayDefinition();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getFormalConstrainedArrayDefinition <em>Formal Constrained Array Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Constrained Array Definition</em>' containment reference.
	 * @see #getFormalConstrainedArrayDefinition()
	 * @generated
	 */
	void setFormalConstrainedArrayDefinition(FormalConstrainedArrayDefinition value);

	/**
	 * Returns the value of the '<em><b>Formal Pool Specific Access To Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Pool Specific Access To Variable</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Pool Specific Access To Variable</em>' containment reference.
	 * @see #setFormalPoolSpecificAccessToVariable(FormalPoolSpecificAccessToVariable)
	 * @see Ada.AdaPackage#getDefinitionClass_FormalPoolSpecificAccessToVariable()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_pool_specific_access_to_variable' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalPoolSpecificAccessToVariable getFormalPoolSpecificAccessToVariable();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getFormalPoolSpecificAccessToVariable <em>Formal Pool Specific Access To Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Pool Specific Access To Variable</em>' containment reference.
	 * @see #getFormalPoolSpecificAccessToVariable()
	 * @generated
	 */
	void setFormalPoolSpecificAccessToVariable(FormalPoolSpecificAccessToVariable value);

	/**
	 * Returns the value of the '<em><b>Formal Access To Variable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Access To Variable</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Access To Variable</em>' containment reference.
	 * @see #setFormalAccessToVariable(FormalAccessToVariable)
	 * @see Ada.AdaPackage#getDefinitionClass_FormalAccessToVariable()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_access_to_variable' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalAccessToVariable getFormalAccessToVariable();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getFormalAccessToVariable <em>Formal Access To Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Access To Variable</em>' containment reference.
	 * @see #getFormalAccessToVariable()
	 * @generated
	 */
	void setFormalAccessToVariable(FormalAccessToVariable value);

	/**
	 * Returns the value of the '<em><b>Formal Access To Constant</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Access To Constant</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Access To Constant</em>' containment reference.
	 * @see #setFormalAccessToConstant(FormalAccessToConstant)
	 * @see Ada.AdaPackage#getDefinitionClass_FormalAccessToConstant()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_access_to_constant' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalAccessToConstant getFormalAccessToConstant();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getFormalAccessToConstant <em>Formal Access To Constant</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Access To Constant</em>' containment reference.
	 * @see #getFormalAccessToConstant()
	 * @generated
	 */
	void setFormalAccessToConstant(FormalAccessToConstant value);

	/**
	 * Returns the value of the '<em><b>Formal Access To Procedure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Access To Procedure</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Access To Procedure</em>' containment reference.
	 * @see #setFormalAccessToProcedure(FormalAccessToProcedure)
	 * @see Ada.AdaPackage#getDefinitionClass_FormalAccessToProcedure()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_access_to_procedure' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalAccessToProcedure getFormalAccessToProcedure();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getFormalAccessToProcedure <em>Formal Access To Procedure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Access To Procedure</em>' containment reference.
	 * @see #getFormalAccessToProcedure()
	 * @generated
	 */
	void setFormalAccessToProcedure(FormalAccessToProcedure value);

	/**
	 * Returns the value of the '<em><b>Formal Access To Protected Procedure</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Access To Protected Procedure</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Access To Protected Procedure</em>' containment reference.
	 * @see #setFormalAccessToProtectedProcedure(FormalAccessToProtectedProcedure)
	 * @see Ada.AdaPackage#getDefinitionClass_FormalAccessToProtectedProcedure()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_access_to_protected_procedure' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalAccessToProtectedProcedure getFormalAccessToProtectedProcedure();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getFormalAccessToProtectedProcedure <em>Formal Access To Protected Procedure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Access To Protected Procedure</em>' containment reference.
	 * @see #getFormalAccessToProtectedProcedure()
	 * @generated
	 */
	void setFormalAccessToProtectedProcedure(FormalAccessToProtectedProcedure value);

	/**
	 * Returns the value of the '<em><b>Formal Access To Function</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Access To Function</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Access To Function</em>' containment reference.
	 * @see #setFormalAccessToFunction(FormalAccessToFunction)
	 * @see Ada.AdaPackage#getDefinitionClass_FormalAccessToFunction()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_access_to_function' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalAccessToFunction getFormalAccessToFunction();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getFormalAccessToFunction <em>Formal Access To Function</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Access To Function</em>' containment reference.
	 * @see #getFormalAccessToFunction()
	 * @generated
	 */
	void setFormalAccessToFunction(FormalAccessToFunction value);

	/**
	 * Returns the value of the '<em><b>Formal Access To Protected Function</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Access To Protected Function</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Access To Protected Function</em>' containment reference.
	 * @see #setFormalAccessToProtectedFunction(FormalAccessToProtectedFunction)
	 * @see Ada.AdaPackage#getDefinitionClass_FormalAccessToProtectedFunction()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='formal_access_to_protected_function' namespace='##targetNamespace'"
	 * @generated
	 */
	FormalAccessToProtectedFunction getFormalAccessToProtectedFunction();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getFormalAccessToProtectedFunction <em>Formal Access To Protected Function</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Access To Protected Function</em>' containment reference.
	 * @see #getFormalAccessToProtectedFunction()
	 * @generated
	 */
	void setFormalAccessToProtectedFunction(FormalAccessToProtectedFunction value);

	/**
	 * Returns the value of the '<em><b>Aspect Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aspect Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aspect Specification</em>' containment reference.
	 * @see #setAspectSpecification(AspectSpecification)
	 * @see Ada.AdaPackage#getDefinitionClass_AspectSpecification()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='aspect_specification' namespace='##targetNamespace'"
	 * @generated
	 */
	AspectSpecification getAspectSpecification();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getAspectSpecification <em>Aspect Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aspect Specification</em>' containment reference.
	 * @see #getAspectSpecification()
	 * @generated
	 */
	void setAspectSpecification(AspectSpecification value);

	/**
	 * Returns the value of the '<em><b>Identifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Identifier</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identifier</em>' containment reference.
	 * @see #setIdentifier(Identifier)
	 * @see Ada.AdaPackage#getDefinitionClass_Identifier()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='identifier' namespace='##targetNamespace'"
	 * @generated
	 */
	Identifier getIdentifier();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getIdentifier <em>Identifier</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Identifier</em>' containment reference.
	 * @see #getIdentifier()
	 * @generated
	 */
	void setIdentifier(Identifier value);

	/**
	 * Returns the value of the '<em><b>Selected Component</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selected Component</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selected Component</em>' containment reference.
	 * @see #setSelectedComponent(SelectedComponent)
	 * @see Ada.AdaPackage#getDefinitionClass_SelectedComponent()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='selected_component' namespace='##targetNamespace'"
	 * @generated
	 */
	SelectedComponent getSelectedComponent();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getSelectedComponent <em>Selected Component</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selected Component</em>' containment reference.
	 * @see #getSelectedComponent()
	 * @generated
	 */
	void setSelectedComponent(SelectedComponent value);

	/**
	 * Returns the value of the '<em><b>Base Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Attribute</em>' containment reference.
	 * @see #setBaseAttribute(BaseAttribute)
	 * @see Ada.AdaPackage#getDefinitionClass_BaseAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='base_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	BaseAttribute getBaseAttribute();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getBaseAttribute <em>Base Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Attribute</em>' containment reference.
	 * @see #getBaseAttribute()
	 * @generated
	 */
	void setBaseAttribute(BaseAttribute value);

	/**
	 * Returns the value of the '<em><b>Class Attribute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Attribute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Attribute</em>' containment reference.
	 * @see #setClassAttribute(ClassAttribute)
	 * @see Ada.AdaPackage#getDefinitionClass_ClassAttribute()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='class_attribute' namespace='##targetNamespace'"
	 * @generated
	 */
	ClassAttribute getClassAttribute();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getClassAttribute <em>Class Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class Attribute</em>' containment reference.
	 * @see #getClassAttribute()
	 * @generated
	 */
	void setClassAttribute(ClassAttribute value);

	/**
	 * Returns the value of the '<em><b>Comment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comment</em>' containment reference.
	 * @see #setComment(Comment)
	 * @see Ada.AdaPackage#getDefinitionClass_Comment()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='comment' namespace='##targetNamespace'"
	 * @generated
	 */
	Comment getComment();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getComment <em>Comment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comment</em>' containment reference.
	 * @see #getComment()
	 * @generated
	 */
	void setComment(Comment value);

	/**
	 * Returns the value of the '<em><b>All Calls Remote Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Calls Remote Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Calls Remote Pragma</em>' containment reference.
	 * @see #setAllCallsRemotePragma(AllCallsRemotePragma)
	 * @see Ada.AdaPackage#getDefinitionClass_AllCallsRemotePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='all_calls_remote_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AllCallsRemotePragma getAllCallsRemotePragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>All Calls Remote Pragma</em>' containment reference.
	 * @see #getAllCallsRemotePragma()
	 * @generated
	 */
	void setAllCallsRemotePragma(AllCallsRemotePragma value);

	/**
	 * Returns the value of the '<em><b>Asynchronous Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asynchronous Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asynchronous Pragma</em>' containment reference.
	 * @see #setAsynchronousPragma(AsynchronousPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_AsynchronousPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='asynchronous_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AsynchronousPragma getAsynchronousPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getAsynchronousPragma <em>Asynchronous Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Asynchronous Pragma</em>' containment reference.
	 * @see #getAsynchronousPragma()
	 * @generated
	 */
	void setAsynchronousPragma(AsynchronousPragma value);

	/**
	 * Returns the value of the '<em><b>Atomic Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Pragma</em>' containment reference.
	 * @see #setAtomicPragma(AtomicPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_AtomicPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='atomic_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AtomicPragma getAtomicPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getAtomicPragma <em>Atomic Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Atomic Pragma</em>' containment reference.
	 * @see #getAtomicPragma()
	 * @generated
	 */
	void setAtomicPragma(AtomicPragma value);

	/**
	 * Returns the value of the '<em><b>Atomic Components Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Components Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Components Pragma</em>' containment reference.
	 * @see #setAtomicComponentsPragma(AtomicComponentsPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_AtomicComponentsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='atomic_components_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AtomicComponentsPragma getAtomicComponentsPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Atomic Components Pragma</em>' containment reference.
	 * @see #getAtomicComponentsPragma()
	 * @generated
	 */
	void setAtomicComponentsPragma(AtomicComponentsPragma value);

	/**
	 * Returns the value of the '<em><b>Attach Handler Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attach Handler Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attach Handler Pragma</em>' containment reference.
	 * @see #setAttachHandlerPragma(AttachHandlerPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_AttachHandlerPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='attach_handler_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AttachHandlerPragma getAttachHandlerPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getAttachHandlerPragma <em>Attach Handler Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attach Handler Pragma</em>' containment reference.
	 * @see #getAttachHandlerPragma()
	 * @generated
	 */
	void setAttachHandlerPragma(AttachHandlerPragma value);

	/**
	 * Returns the value of the '<em><b>Controlled Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controlled Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controlled Pragma</em>' containment reference.
	 * @see #setControlledPragma(ControlledPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_ControlledPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='controlled_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ControlledPragma getControlledPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getControlledPragma <em>Controlled Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Controlled Pragma</em>' containment reference.
	 * @see #getControlledPragma()
	 * @generated
	 */
	void setControlledPragma(ControlledPragma value);

	/**
	 * Returns the value of the '<em><b>Convention Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Convention Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Convention Pragma</em>' containment reference.
	 * @see #setConventionPragma(ConventionPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_ConventionPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='convention_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ConventionPragma getConventionPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getConventionPragma <em>Convention Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Convention Pragma</em>' containment reference.
	 * @see #getConventionPragma()
	 * @generated
	 */
	void setConventionPragma(ConventionPragma value);

	/**
	 * Returns the value of the '<em><b>Discard Names Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discard Names Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discard Names Pragma</em>' containment reference.
	 * @see #setDiscardNamesPragma(DiscardNamesPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_DiscardNamesPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='discard_names_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscardNamesPragma getDiscardNamesPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getDiscardNamesPragma <em>Discard Names Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discard Names Pragma</em>' containment reference.
	 * @see #getDiscardNamesPragma()
	 * @generated
	 */
	void setDiscardNamesPragma(DiscardNamesPragma value);

	/**
	 * Returns the value of the '<em><b>Elaborate Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Pragma</em>' containment reference.
	 * @see #setElaboratePragma(ElaboratePragma)
	 * @see Ada.AdaPackage#getDefinitionClass_ElaboratePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='elaborate_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ElaboratePragma getElaboratePragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getElaboratePragma <em>Elaborate Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Elaborate Pragma</em>' containment reference.
	 * @see #getElaboratePragma()
	 * @generated
	 */
	void setElaboratePragma(ElaboratePragma value);

	/**
	 * Returns the value of the '<em><b>Elaborate All Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate All Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate All Pragma</em>' containment reference.
	 * @see #setElaborateAllPragma(ElaborateAllPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_ElaborateAllPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='elaborate_all_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ElaborateAllPragma getElaborateAllPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getElaborateAllPragma <em>Elaborate All Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Elaborate All Pragma</em>' containment reference.
	 * @see #getElaborateAllPragma()
	 * @generated
	 */
	void setElaborateAllPragma(ElaborateAllPragma value);

	/**
	 * Returns the value of the '<em><b>Elaborate Body Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Body Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Body Pragma</em>' containment reference.
	 * @see #setElaborateBodyPragma(ElaborateBodyPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_ElaborateBodyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='elaborate_body_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ElaborateBodyPragma getElaborateBodyPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Elaborate Body Pragma</em>' containment reference.
	 * @see #getElaborateBodyPragma()
	 * @generated
	 */
	void setElaborateBodyPragma(ElaborateBodyPragma value);

	/**
	 * Returns the value of the '<em><b>Export Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Export Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Export Pragma</em>' containment reference.
	 * @see #setExportPragma(ExportPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_ExportPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='export_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ExportPragma getExportPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getExportPragma <em>Export Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Export Pragma</em>' containment reference.
	 * @see #getExportPragma()
	 * @generated
	 */
	void setExportPragma(ExportPragma value);

	/**
	 * Returns the value of the '<em><b>Import Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Import Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Import Pragma</em>' containment reference.
	 * @see #setImportPragma(ImportPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_ImportPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='import_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ImportPragma getImportPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getImportPragma <em>Import Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Import Pragma</em>' containment reference.
	 * @see #getImportPragma()
	 * @generated
	 */
	void setImportPragma(ImportPragma value);

	/**
	 * Returns the value of the '<em><b>Inline Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inline Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inline Pragma</em>' containment reference.
	 * @see #setInlinePragma(InlinePragma)
	 * @see Ada.AdaPackage#getDefinitionClass_InlinePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='inline_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	InlinePragma getInlinePragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getInlinePragma <em>Inline Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inline Pragma</em>' containment reference.
	 * @see #getInlinePragma()
	 * @generated
	 */
	void setInlinePragma(InlinePragma value);

	/**
	 * Returns the value of the '<em><b>Inspection Point Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inspection Point Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inspection Point Pragma</em>' containment reference.
	 * @see #setInspectionPointPragma(InspectionPointPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_InspectionPointPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='inspection_point_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	InspectionPointPragma getInspectionPointPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getInspectionPointPragma <em>Inspection Point Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inspection Point Pragma</em>' containment reference.
	 * @see #getInspectionPointPragma()
	 * @generated
	 */
	void setInspectionPointPragma(InspectionPointPragma value);

	/**
	 * Returns the value of the '<em><b>Interrupt Handler Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Handler Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Handler Pragma</em>' containment reference.
	 * @see #setInterruptHandlerPragma(InterruptHandlerPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_InterruptHandlerPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='interrupt_handler_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	InterruptHandlerPragma getInterruptHandlerPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interrupt Handler Pragma</em>' containment reference.
	 * @see #getInterruptHandlerPragma()
	 * @generated
	 */
	void setInterruptHandlerPragma(InterruptHandlerPragma value);

	/**
	 * Returns the value of the '<em><b>Interrupt Priority Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Priority Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Priority Pragma</em>' containment reference.
	 * @see #setInterruptPriorityPragma(InterruptPriorityPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_InterruptPriorityPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='interrupt_priority_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	InterruptPriorityPragma getInterruptPriorityPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interrupt Priority Pragma</em>' containment reference.
	 * @see #getInterruptPriorityPragma()
	 * @generated
	 */
	void setInterruptPriorityPragma(InterruptPriorityPragma value);

	/**
	 * Returns the value of the '<em><b>Linker Options Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Linker Options Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Linker Options Pragma</em>' containment reference.
	 * @see #setLinkerOptionsPragma(LinkerOptionsPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_LinkerOptionsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='linker_options_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	LinkerOptionsPragma getLinkerOptionsPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getLinkerOptionsPragma <em>Linker Options Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Linker Options Pragma</em>' containment reference.
	 * @see #getLinkerOptionsPragma()
	 * @generated
	 */
	void setLinkerOptionsPragma(LinkerOptionsPragma value);

	/**
	 * Returns the value of the '<em><b>List Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Pragma</em>' containment reference.
	 * @see #setListPragma(ListPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_ListPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='list_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ListPragma getListPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getListPragma <em>List Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>List Pragma</em>' containment reference.
	 * @see #getListPragma()
	 * @generated
	 */
	void setListPragma(ListPragma value);

	/**
	 * Returns the value of the '<em><b>Locking Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Locking Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Locking Policy Pragma</em>' containment reference.
	 * @see #setLockingPolicyPragma(LockingPolicyPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_LockingPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='locking_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	LockingPolicyPragma getLockingPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getLockingPolicyPragma <em>Locking Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Locking Policy Pragma</em>' containment reference.
	 * @see #getLockingPolicyPragma()
	 * @generated
	 */
	void setLockingPolicyPragma(LockingPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Normalize Scalars Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Normalize Scalars Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Normalize Scalars Pragma</em>' containment reference.
	 * @see #setNormalizeScalarsPragma(NormalizeScalarsPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_NormalizeScalarsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='normalize_scalars_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	NormalizeScalarsPragma getNormalizeScalarsPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Normalize Scalars Pragma</em>' containment reference.
	 * @see #getNormalizeScalarsPragma()
	 * @generated
	 */
	void setNormalizeScalarsPragma(NormalizeScalarsPragma value);

	/**
	 * Returns the value of the '<em><b>Optimize Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Optimize Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Optimize Pragma</em>' containment reference.
	 * @see #setOptimizePragma(OptimizePragma)
	 * @see Ada.AdaPackage#getDefinitionClass_OptimizePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='optimize_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	OptimizePragma getOptimizePragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getOptimizePragma <em>Optimize Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Optimize Pragma</em>' containment reference.
	 * @see #getOptimizePragma()
	 * @generated
	 */
	void setOptimizePragma(OptimizePragma value);

	/**
	 * Returns the value of the '<em><b>Pack Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pack Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pack Pragma</em>' containment reference.
	 * @see #setPackPragma(PackPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_PackPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='pack_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PackPragma getPackPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getPackPragma <em>Pack Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pack Pragma</em>' containment reference.
	 * @see #getPackPragma()
	 * @generated
	 */
	void setPackPragma(PackPragma value);

	/**
	 * Returns the value of the '<em><b>Page Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Page Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Page Pragma</em>' containment reference.
	 * @see #setPagePragma(PagePragma)
	 * @see Ada.AdaPackage#getDefinitionClass_PagePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='page_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PagePragma getPagePragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getPagePragma <em>Page Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Page Pragma</em>' containment reference.
	 * @see #getPagePragma()
	 * @generated
	 */
	void setPagePragma(PagePragma value);

	/**
	 * Returns the value of the '<em><b>Preelaborate Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborate Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborate Pragma</em>' containment reference.
	 * @see #setPreelaboratePragma(PreelaboratePragma)
	 * @see Ada.AdaPackage#getDefinitionClass_PreelaboratePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='preelaborate_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PreelaboratePragma getPreelaboratePragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getPreelaboratePragma <em>Preelaborate Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preelaborate Pragma</em>' containment reference.
	 * @see #getPreelaboratePragma()
	 * @generated
	 */
	void setPreelaboratePragma(PreelaboratePragma value);

	/**
	 * Returns the value of the '<em><b>Priority Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Pragma</em>' containment reference.
	 * @see #setPriorityPragma(PriorityPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_PriorityPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='priority_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PriorityPragma getPriorityPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getPriorityPragma <em>Priority Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Priority Pragma</em>' containment reference.
	 * @see #getPriorityPragma()
	 * @generated
	 */
	void setPriorityPragma(PriorityPragma value);

	/**
	 * Returns the value of the '<em><b>Pure Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pure Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pure Pragma</em>' containment reference.
	 * @see #setPurePragma(PurePragma)
	 * @see Ada.AdaPackage#getDefinitionClass_PurePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='pure_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PurePragma getPurePragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getPurePragma <em>Pure Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pure Pragma</em>' containment reference.
	 * @see #getPurePragma()
	 * @generated
	 */
	void setPurePragma(PurePragma value);

	/**
	 * Returns the value of the '<em><b>Queuing Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Queuing Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Queuing Policy Pragma</em>' containment reference.
	 * @see #setQueuingPolicyPragma(QueuingPolicyPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_QueuingPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='queuing_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	QueuingPolicyPragma getQueuingPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Queuing Policy Pragma</em>' containment reference.
	 * @see #getQueuingPolicyPragma()
	 * @generated
	 */
	void setQueuingPolicyPragma(QueuingPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Remote Call Interface Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Call Interface Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Call Interface Pragma</em>' containment reference.
	 * @see #setRemoteCallInterfacePragma(RemoteCallInterfacePragma)
	 * @see Ada.AdaPackage#getDefinitionClass_RemoteCallInterfacePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='remote_call_interface_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	RemoteCallInterfacePragma getRemoteCallInterfacePragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Remote Call Interface Pragma</em>' containment reference.
	 * @see #getRemoteCallInterfacePragma()
	 * @generated
	 */
	void setRemoteCallInterfacePragma(RemoteCallInterfacePragma value);

	/**
	 * Returns the value of the '<em><b>Remote Types Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Types Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Types Pragma</em>' containment reference.
	 * @see #setRemoteTypesPragma(RemoteTypesPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_RemoteTypesPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='remote_types_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	RemoteTypesPragma getRemoteTypesPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getRemoteTypesPragma <em>Remote Types Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Remote Types Pragma</em>' containment reference.
	 * @see #getRemoteTypesPragma()
	 * @generated
	 */
	void setRemoteTypesPragma(RemoteTypesPragma value);

	/**
	 * Returns the value of the '<em><b>Restrictions Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Restrictions Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Restrictions Pragma</em>' containment reference.
	 * @see #setRestrictionsPragma(RestrictionsPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_RestrictionsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='restrictions_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	RestrictionsPragma getRestrictionsPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getRestrictionsPragma <em>Restrictions Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Restrictions Pragma</em>' containment reference.
	 * @see #getRestrictionsPragma()
	 * @generated
	 */
	void setRestrictionsPragma(RestrictionsPragma value);

	/**
	 * Returns the value of the '<em><b>Reviewable Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reviewable Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reviewable Pragma</em>' containment reference.
	 * @see #setReviewablePragma(ReviewablePragma)
	 * @see Ada.AdaPackage#getDefinitionClass_ReviewablePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='reviewable_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ReviewablePragma getReviewablePragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getReviewablePragma <em>Reviewable Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reviewable Pragma</em>' containment reference.
	 * @see #getReviewablePragma()
	 * @generated
	 */
	void setReviewablePragma(ReviewablePragma value);

	/**
	 * Returns the value of the '<em><b>Shared Passive Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shared Passive Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shared Passive Pragma</em>' containment reference.
	 * @see #setSharedPassivePragma(SharedPassivePragma)
	 * @see Ada.AdaPackage#getDefinitionClass_SharedPassivePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='shared_passive_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	SharedPassivePragma getSharedPassivePragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getSharedPassivePragma <em>Shared Passive Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Shared Passive Pragma</em>' containment reference.
	 * @see #getSharedPassivePragma()
	 * @generated
	 */
	void setSharedPassivePragma(SharedPassivePragma value);

	/**
	 * Returns the value of the '<em><b>Storage Size Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Size Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Size Pragma</em>' containment reference.
	 * @see #setStorageSizePragma(StorageSizePragma)
	 * @see Ada.AdaPackage#getDefinitionClass_StorageSizePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='storage_size_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	StorageSizePragma getStorageSizePragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getStorageSizePragma <em>Storage Size Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Storage Size Pragma</em>' containment reference.
	 * @see #getStorageSizePragma()
	 * @generated
	 */
	void setStorageSizePragma(StorageSizePragma value);

	/**
	 * Returns the value of the '<em><b>Suppress Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Suppress Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Suppress Pragma</em>' containment reference.
	 * @see #setSuppressPragma(SuppressPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_SuppressPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='suppress_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	SuppressPragma getSuppressPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getSuppressPragma <em>Suppress Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Suppress Pragma</em>' containment reference.
	 * @see #getSuppressPragma()
	 * @generated
	 */
	void setSuppressPragma(SuppressPragma value);

	/**
	 * Returns the value of the '<em><b>Task Dispatching Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Dispatching Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Dispatching Policy Pragma</em>' containment reference.
	 * @see #setTaskDispatchingPolicyPragma(TaskDispatchingPolicyPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_TaskDispatchingPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='task_dispatching_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	TaskDispatchingPolicyPragma getTaskDispatchingPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Dispatching Policy Pragma</em>' containment reference.
	 * @see #getTaskDispatchingPolicyPragma()
	 * @generated
	 */
	void setTaskDispatchingPolicyPragma(TaskDispatchingPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Volatile Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Pragma</em>' containment reference.
	 * @see #setVolatilePragma(VolatilePragma)
	 * @see Ada.AdaPackage#getDefinitionClass_VolatilePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='volatile_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	VolatilePragma getVolatilePragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getVolatilePragma <em>Volatile Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Volatile Pragma</em>' containment reference.
	 * @see #getVolatilePragma()
	 * @generated
	 */
	void setVolatilePragma(VolatilePragma value);

	/**
	 * Returns the value of the '<em><b>Volatile Components Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Components Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Components Pragma</em>' containment reference.
	 * @see #setVolatileComponentsPragma(VolatileComponentsPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_VolatileComponentsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='volatile_components_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	VolatileComponentsPragma getVolatileComponentsPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Volatile Components Pragma</em>' containment reference.
	 * @see #getVolatileComponentsPragma()
	 * @generated
	 */
	void setVolatileComponentsPragma(VolatileComponentsPragma value);

	/**
	 * Returns the value of the '<em><b>Assert Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assert Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assert Pragma</em>' containment reference.
	 * @see #setAssertPragma(AssertPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_AssertPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='assert_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AssertPragma getAssertPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getAssertPragma <em>Assert Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assert Pragma</em>' containment reference.
	 * @see #getAssertPragma()
	 * @generated
	 */
	void setAssertPragma(AssertPragma value);

	/**
	 * Returns the value of the '<em><b>Assertion Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assertion Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assertion Policy Pragma</em>' containment reference.
	 * @see #setAssertionPolicyPragma(AssertionPolicyPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_AssertionPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='assertion_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AssertionPolicyPragma getAssertionPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assertion Policy Pragma</em>' containment reference.
	 * @see #getAssertionPolicyPragma()
	 * @generated
	 */
	void setAssertionPolicyPragma(AssertionPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Detect Blocking Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Detect Blocking Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Detect Blocking Pragma</em>' containment reference.
	 * @see #setDetectBlockingPragma(DetectBlockingPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_DetectBlockingPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='detect_blocking_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	DetectBlockingPragma getDetectBlockingPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Detect Blocking Pragma</em>' containment reference.
	 * @see #getDetectBlockingPragma()
	 * @generated
	 */
	void setDetectBlockingPragma(DetectBlockingPragma value);

	/**
	 * Returns the value of the '<em><b>No Return Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>No Return Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>No Return Pragma</em>' containment reference.
	 * @see #setNoReturnPragma(NoReturnPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_NoReturnPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='no_return_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	NoReturnPragma getNoReturnPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getNoReturnPragma <em>No Return Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>No Return Pragma</em>' containment reference.
	 * @see #getNoReturnPragma()
	 * @generated
	 */
	void setNoReturnPragma(NoReturnPragma value);

	/**
	 * Returns the value of the '<em><b>Partition Elaboration Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partition Elaboration Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partition Elaboration Policy Pragma</em>' containment reference.
	 * @see #setPartitionElaborationPolicyPragma(PartitionElaborationPolicyPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_PartitionElaborationPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='partition_elaboration_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PartitionElaborationPolicyPragma getPartitionElaborationPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Partition Elaboration Policy Pragma</em>' containment reference.
	 * @see #getPartitionElaborationPolicyPragma()
	 * @generated
	 */
	void setPartitionElaborationPolicyPragma(PartitionElaborationPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Preelaborable Initialization Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborable Initialization Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborable Initialization Pragma</em>' containment reference.
	 * @see #setPreelaborableInitializationPragma(PreelaborableInitializationPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_PreelaborableInitializationPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='preelaborable_initialization_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PreelaborableInitializationPragma getPreelaborableInitializationPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preelaborable Initialization Pragma</em>' containment reference.
	 * @see #getPreelaborableInitializationPragma()
	 * @generated
	 */
	void setPreelaborableInitializationPragma(PreelaborableInitializationPragma value);

	/**
	 * Returns the value of the '<em><b>Priority Specific Dispatching Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Specific Dispatching Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Specific Dispatching Pragma</em>' containment reference.
	 * @see #setPrioritySpecificDispatchingPragma(PrioritySpecificDispatchingPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_PrioritySpecificDispatchingPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='priority_specific_dispatching_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PrioritySpecificDispatchingPragma getPrioritySpecificDispatchingPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Priority Specific Dispatching Pragma</em>' containment reference.
	 * @see #getPrioritySpecificDispatchingPragma()
	 * @generated
	 */
	void setPrioritySpecificDispatchingPragma(PrioritySpecificDispatchingPragma value);

	/**
	 * Returns the value of the '<em><b>Profile Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Profile Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Profile Pragma</em>' containment reference.
	 * @see #setProfilePragma(ProfilePragma)
	 * @see Ada.AdaPackage#getDefinitionClass_ProfilePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='profile_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ProfilePragma getProfilePragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getProfilePragma <em>Profile Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Profile Pragma</em>' containment reference.
	 * @see #getProfilePragma()
	 * @generated
	 */
	void setProfilePragma(ProfilePragma value);

	/**
	 * Returns the value of the '<em><b>Relative Deadline Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relative Deadline Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relative Deadline Pragma</em>' containment reference.
	 * @see #setRelativeDeadlinePragma(RelativeDeadlinePragma)
	 * @see Ada.AdaPackage#getDefinitionClass_RelativeDeadlinePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='relative_deadline_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	RelativeDeadlinePragma getRelativeDeadlinePragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relative Deadline Pragma</em>' containment reference.
	 * @see #getRelativeDeadlinePragma()
	 * @generated
	 */
	void setRelativeDeadlinePragma(RelativeDeadlinePragma value);

	/**
	 * Returns the value of the '<em><b>Unchecked Union Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unchecked Union Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unchecked Union Pragma</em>' containment reference.
	 * @see #setUncheckedUnionPragma(UncheckedUnionPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_UncheckedUnionPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unchecked_union_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	UncheckedUnionPragma getUncheckedUnionPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unchecked Union Pragma</em>' containment reference.
	 * @see #getUncheckedUnionPragma()
	 * @generated
	 */
	void setUncheckedUnionPragma(UncheckedUnionPragma value);

	/**
	 * Returns the value of the '<em><b>Unsuppress Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unsuppress Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unsuppress Pragma</em>' containment reference.
	 * @see #setUnsuppressPragma(UnsuppressPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_UnsuppressPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unsuppress_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	UnsuppressPragma getUnsuppressPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getUnsuppressPragma <em>Unsuppress Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unsuppress Pragma</em>' containment reference.
	 * @see #getUnsuppressPragma()
	 * @generated
	 */
	void setUnsuppressPragma(UnsuppressPragma value);

	/**
	 * Returns the value of the '<em><b>Default Storage Pool Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Storage Pool Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Storage Pool Pragma</em>' containment reference.
	 * @see #setDefaultStoragePoolPragma(DefaultStoragePoolPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_DefaultStoragePoolPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='default_storage_pool_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	DefaultStoragePoolPragma getDefaultStoragePoolPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Storage Pool Pragma</em>' containment reference.
	 * @see #getDefaultStoragePoolPragma()
	 * @generated
	 */
	void setDefaultStoragePoolPragma(DefaultStoragePoolPragma value);

	/**
	 * Returns the value of the '<em><b>Dispatching Domain Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dispatching Domain Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dispatching Domain Pragma</em>' containment reference.
	 * @see #setDispatchingDomainPragma(DispatchingDomainPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_DispatchingDomainPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='dispatching_domain_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	DispatchingDomainPragma getDispatchingDomainPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dispatching Domain Pragma</em>' containment reference.
	 * @see #getDispatchingDomainPragma()
	 * @generated
	 */
	void setDispatchingDomainPragma(DispatchingDomainPragma value);

	/**
	 * Returns the value of the '<em><b>Cpu Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cpu Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cpu Pragma</em>' containment reference.
	 * @see #setCpuPragma(CpuPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_CpuPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='cpu_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	CpuPragma getCpuPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getCpuPragma <em>Cpu Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cpu Pragma</em>' containment reference.
	 * @see #getCpuPragma()
	 * @generated
	 */
	void setCpuPragma(CpuPragma value);

	/**
	 * Returns the value of the '<em><b>Independent Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Pragma</em>' containment reference.
	 * @see #setIndependentPragma(IndependentPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_IndependentPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='independent_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	IndependentPragma getIndependentPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getIndependentPragma <em>Independent Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Independent Pragma</em>' containment reference.
	 * @see #getIndependentPragma()
	 * @generated
	 */
	void setIndependentPragma(IndependentPragma value);

	/**
	 * Returns the value of the '<em><b>Independent Components Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Components Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Components Pragma</em>' containment reference.
	 * @see #setIndependentComponentsPragma(IndependentComponentsPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_IndependentComponentsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='independent_components_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	IndependentComponentsPragma getIndependentComponentsPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getIndependentComponentsPragma <em>Independent Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Independent Components Pragma</em>' containment reference.
	 * @see #getIndependentComponentsPragma()
	 * @generated
	 */
	void setIndependentComponentsPragma(IndependentComponentsPragma value);

	/**
	 * Returns the value of the '<em><b>Implementation Defined Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implementation Defined Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implementation Defined Pragma</em>' containment reference.
	 * @see #setImplementationDefinedPragma(ImplementationDefinedPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_ImplementationDefinedPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='implementation_defined_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ImplementationDefinedPragma getImplementationDefinedPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Implementation Defined Pragma</em>' containment reference.
	 * @see #getImplementationDefinedPragma()
	 * @generated
	 */
	void setImplementationDefinedPragma(ImplementationDefinedPragma value);

	/**
	 * Returns the value of the '<em><b>Unknown Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unknown Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unknown Pragma</em>' containment reference.
	 * @see #setUnknownPragma(UnknownPragma)
	 * @see Ada.AdaPackage#getDefinitionClass_UnknownPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unknown_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	UnknownPragma getUnknownPragma();

	/**
	 * Sets the value of the '{@link Ada.DefinitionClass#getUnknownPragma <em>Unknown Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unknown Pragma</em>' containment reference.
	 * @see #getUnknownPragma()
	 * @generated
	 */
	void setUnknownPragma(UnknownPragma value);

} // DefinitionClass
