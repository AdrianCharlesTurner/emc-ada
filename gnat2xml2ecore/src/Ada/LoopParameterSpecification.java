/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Loop Parameter Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.LoopParameterSpecification#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.LoopParameterSpecification#getNamesQl <em>Names Ql</em>}</li>
 *   <li>{@link Ada.LoopParameterSpecification#getHasReverseQ <em>Has Reverse Q</em>}</li>
 *   <li>{@link Ada.LoopParameterSpecification#getSpecificationSubtypeDefinitionQ <em>Specification Subtype Definition Q</em>}</li>
 *   <li>{@link Ada.LoopParameterSpecification#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getLoopParameterSpecification()
 * @model extendedMetaData="name='Loop_Parameter_Specification' kind='elementOnly'"
 * @generated
 */
public interface LoopParameterSpecification extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getLoopParameterSpecification_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.LoopParameterSpecification#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Names Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Names Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Names Ql</em>' containment reference.
	 * @see #setNamesQl(DefiningNameList)
	 * @see Ada.AdaPackage#getLoopParameterSpecification_NamesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='names_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameList getNamesQl();

	/**
	 * Sets the value of the '{@link Ada.LoopParameterSpecification#getNamesQl <em>Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Names Ql</em>' containment reference.
	 * @see #getNamesQl()
	 * @generated
	 */
	void setNamesQl(DefiningNameList value);

	/**
	 * Returns the value of the '<em><b>Has Reverse Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Reverse Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Reverse Q</em>' containment reference.
	 * @see #setHasReverseQ(HasReverseQType2)
	 * @see Ada.AdaPackage#getLoopParameterSpecification_HasReverseQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='has_reverse_q' namespace='##targetNamespace'"
	 * @generated
	 */
	HasReverseQType2 getHasReverseQ();

	/**
	 * Sets the value of the '{@link Ada.LoopParameterSpecification#getHasReverseQ <em>Has Reverse Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Reverse Q</em>' containment reference.
	 * @see #getHasReverseQ()
	 * @generated
	 */
	void setHasReverseQ(HasReverseQType2 value);

	/**
	 * Returns the value of the '<em><b>Specification Subtype Definition Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Specification Subtype Definition Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specification Subtype Definition Q</em>' containment reference.
	 * @see #setSpecificationSubtypeDefinitionQ(DiscreteSubtypeDefinitionClass)
	 * @see Ada.AdaPackage#getLoopParameterSpecification_SpecificationSubtypeDefinitionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='specification_subtype_definition_q' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscreteSubtypeDefinitionClass getSpecificationSubtypeDefinitionQ();

	/**
	 * Sets the value of the '{@link Ada.LoopParameterSpecification#getSpecificationSubtypeDefinitionQ <em>Specification Subtype Definition Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Specification Subtype Definition Q</em>' containment reference.
	 * @see #getSpecificationSubtypeDefinitionQ()
	 * @generated
	 */
	void setSpecificationSubtypeDefinitionQ(DiscreteSubtypeDefinitionClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getLoopParameterSpecification_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.LoopParameterSpecification#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // LoopParameterSpecification
