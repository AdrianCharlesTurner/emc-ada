/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.DefiningNameList;
import Ada.ExitStatement;
import Ada.ExpressionClass;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Exit Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.ExitStatementImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.ExitStatementImpl#getLabelNamesQl <em>Label Names Ql</em>}</li>
 *   <li>{@link Ada.impl.ExitStatementImpl#getExitLoopNameQ <em>Exit Loop Name Q</em>}</li>
 *   <li>{@link Ada.impl.ExitStatementImpl#getExitConditionQ <em>Exit Condition Q</em>}</li>
 *   <li>{@link Ada.impl.ExitStatementImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExitStatementImpl extends MinimalEObjectImpl.Container implements ExitStatement {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getLabelNamesQl() <em>Label Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabelNamesQl()
	 * @generated
	 * @ordered
	 */
	protected DefiningNameList labelNamesQl;

	/**
	 * The cached value of the '{@link #getExitLoopNameQ() <em>Exit Loop Name Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExitLoopNameQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass exitLoopNameQ;

	/**
	 * The cached value of the '{@link #getExitConditionQ() <em>Exit Condition Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExitConditionQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass exitConditionQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExitStatementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getExitStatement();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.EXIT_STATEMENT__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXIT_STATEMENT__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXIT_STATEMENT__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.EXIT_STATEMENT__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNameList getLabelNamesQl() {
		return labelNamesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLabelNamesQl(DefiningNameList newLabelNamesQl, NotificationChain msgs) {
		DefiningNameList oldLabelNamesQl = labelNamesQl;
		labelNamesQl = newLabelNamesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.EXIT_STATEMENT__LABEL_NAMES_QL, oldLabelNamesQl, newLabelNamesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabelNamesQl(DefiningNameList newLabelNamesQl) {
		if (newLabelNamesQl != labelNamesQl) {
			NotificationChain msgs = null;
			if (labelNamesQl != null)
				msgs = ((InternalEObject)labelNamesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXIT_STATEMENT__LABEL_NAMES_QL, null, msgs);
			if (newLabelNamesQl != null)
				msgs = ((InternalEObject)newLabelNamesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXIT_STATEMENT__LABEL_NAMES_QL, null, msgs);
			msgs = basicSetLabelNamesQl(newLabelNamesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.EXIT_STATEMENT__LABEL_NAMES_QL, newLabelNamesQl, newLabelNamesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getExitLoopNameQ() {
		return exitLoopNameQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExitLoopNameQ(ExpressionClass newExitLoopNameQ, NotificationChain msgs) {
		ExpressionClass oldExitLoopNameQ = exitLoopNameQ;
		exitLoopNameQ = newExitLoopNameQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.EXIT_STATEMENT__EXIT_LOOP_NAME_Q, oldExitLoopNameQ, newExitLoopNameQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExitLoopNameQ(ExpressionClass newExitLoopNameQ) {
		if (newExitLoopNameQ != exitLoopNameQ) {
			NotificationChain msgs = null;
			if (exitLoopNameQ != null)
				msgs = ((InternalEObject)exitLoopNameQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXIT_STATEMENT__EXIT_LOOP_NAME_Q, null, msgs);
			if (newExitLoopNameQ != null)
				msgs = ((InternalEObject)newExitLoopNameQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXIT_STATEMENT__EXIT_LOOP_NAME_Q, null, msgs);
			msgs = basicSetExitLoopNameQ(newExitLoopNameQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.EXIT_STATEMENT__EXIT_LOOP_NAME_Q, newExitLoopNameQ, newExitLoopNameQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getExitConditionQ() {
		return exitConditionQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExitConditionQ(ExpressionClass newExitConditionQ, NotificationChain msgs) {
		ExpressionClass oldExitConditionQ = exitConditionQ;
		exitConditionQ = newExitConditionQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.EXIT_STATEMENT__EXIT_CONDITION_Q, oldExitConditionQ, newExitConditionQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExitConditionQ(ExpressionClass newExitConditionQ) {
		if (newExitConditionQ != exitConditionQ) {
			NotificationChain msgs = null;
			if (exitConditionQ != null)
				msgs = ((InternalEObject)exitConditionQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXIT_STATEMENT__EXIT_CONDITION_Q, null, msgs);
			if (newExitConditionQ != null)
				msgs = ((InternalEObject)newExitConditionQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXIT_STATEMENT__EXIT_CONDITION_Q, null, msgs);
			msgs = basicSetExitConditionQ(newExitConditionQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.EXIT_STATEMENT__EXIT_CONDITION_Q, newExitConditionQ, newExitConditionQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.EXIT_STATEMENT__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.EXIT_STATEMENT__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.EXIT_STATEMENT__LABEL_NAMES_QL:
				return basicSetLabelNamesQl(null, msgs);
			case AdaPackage.EXIT_STATEMENT__EXIT_LOOP_NAME_Q:
				return basicSetExitLoopNameQ(null, msgs);
			case AdaPackage.EXIT_STATEMENT__EXIT_CONDITION_Q:
				return basicSetExitConditionQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.EXIT_STATEMENT__SLOC:
				return getSloc();
			case AdaPackage.EXIT_STATEMENT__LABEL_NAMES_QL:
				return getLabelNamesQl();
			case AdaPackage.EXIT_STATEMENT__EXIT_LOOP_NAME_Q:
				return getExitLoopNameQ();
			case AdaPackage.EXIT_STATEMENT__EXIT_CONDITION_Q:
				return getExitConditionQ();
			case AdaPackage.EXIT_STATEMENT__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.EXIT_STATEMENT__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.EXIT_STATEMENT__LABEL_NAMES_QL:
				setLabelNamesQl((DefiningNameList)newValue);
				return;
			case AdaPackage.EXIT_STATEMENT__EXIT_LOOP_NAME_Q:
				setExitLoopNameQ((ExpressionClass)newValue);
				return;
			case AdaPackage.EXIT_STATEMENT__EXIT_CONDITION_Q:
				setExitConditionQ((ExpressionClass)newValue);
				return;
			case AdaPackage.EXIT_STATEMENT__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.EXIT_STATEMENT__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.EXIT_STATEMENT__LABEL_NAMES_QL:
				setLabelNamesQl((DefiningNameList)null);
				return;
			case AdaPackage.EXIT_STATEMENT__EXIT_LOOP_NAME_Q:
				setExitLoopNameQ((ExpressionClass)null);
				return;
			case AdaPackage.EXIT_STATEMENT__EXIT_CONDITION_Q:
				setExitConditionQ((ExpressionClass)null);
				return;
			case AdaPackage.EXIT_STATEMENT__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.EXIT_STATEMENT__SLOC:
				return sloc != null;
			case AdaPackage.EXIT_STATEMENT__LABEL_NAMES_QL:
				return labelNamesQl != null;
			case AdaPackage.EXIT_STATEMENT__EXIT_LOOP_NAME_Q:
				return exitLoopNameQ != null;
			case AdaPackage.EXIT_STATEMENT__EXIT_CONDITION_Q:
				return exitConditionQ != null;
			case AdaPackage.EXIT_STATEMENT__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //ExitStatementImpl
