/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.HasTaggedQType;
import Ada.NotAnElement;
import Ada.Tagged;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Has Tagged QType</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.HasTaggedQTypeImpl#getTagged <em>Tagged</em>}</li>
 *   <li>{@link Ada.impl.HasTaggedQTypeImpl#getNotAnElement <em>Not An Element</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HasTaggedQTypeImpl extends MinimalEObjectImpl.Container implements HasTaggedQType {
	/**
	 * The cached value of the '{@link #getTagged() <em>Tagged</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTagged()
	 * @generated
	 * @ordered
	 */
	protected Tagged tagged;

	/**
	 * The cached value of the '{@link #getNotAnElement() <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotAnElement()
	 * @generated
	 * @ordered
	 */
	protected NotAnElement notAnElement;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HasTaggedQTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getHasTaggedQType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Tagged getTagged() {
		return tagged;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTagged(Tagged newTagged, NotificationChain msgs) {
		Tagged oldTagged = tagged;
		tagged = newTagged;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.HAS_TAGGED_QTYPE__TAGGED, oldTagged, newTagged);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTagged(Tagged newTagged) {
		if (newTagged != tagged) {
			NotificationChain msgs = null;
			if (tagged != null)
				msgs = ((InternalEObject)tagged).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.HAS_TAGGED_QTYPE__TAGGED, null, msgs);
			if (newTagged != null)
				msgs = ((InternalEObject)newTagged).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.HAS_TAGGED_QTYPE__TAGGED, null, msgs);
			msgs = basicSetTagged(newTagged, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.HAS_TAGGED_QTYPE__TAGGED, newTagged, newTagged));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotAnElement getNotAnElement() {
		return notAnElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNotAnElement(NotAnElement newNotAnElement, NotificationChain msgs) {
		NotAnElement oldNotAnElement = notAnElement;
		notAnElement = newNotAnElement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.HAS_TAGGED_QTYPE__NOT_AN_ELEMENT, oldNotAnElement, newNotAnElement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotAnElement(NotAnElement newNotAnElement) {
		if (newNotAnElement != notAnElement) {
			NotificationChain msgs = null;
			if (notAnElement != null)
				msgs = ((InternalEObject)notAnElement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.HAS_TAGGED_QTYPE__NOT_AN_ELEMENT, null, msgs);
			if (newNotAnElement != null)
				msgs = ((InternalEObject)newNotAnElement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.HAS_TAGGED_QTYPE__NOT_AN_ELEMENT, null, msgs);
			msgs = basicSetNotAnElement(newNotAnElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.HAS_TAGGED_QTYPE__NOT_AN_ELEMENT, newNotAnElement, newNotAnElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.HAS_TAGGED_QTYPE__TAGGED:
				return basicSetTagged(null, msgs);
			case AdaPackage.HAS_TAGGED_QTYPE__NOT_AN_ELEMENT:
				return basicSetNotAnElement(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.HAS_TAGGED_QTYPE__TAGGED:
				return getTagged();
			case AdaPackage.HAS_TAGGED_QTYPE__NOT_AN_ELEMENT:
				return getNotAnElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.HAS_TAGGED_QTYPE__TAGGED:
				setTagged((Tagged)newValue);
				return;
			case AdaPackage.HAS_TAGGED_QTYPE__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.HAS_TAGGED_QTYPE__TAGGED:
				setTagged((Tagged)null);
				return;
			case AdaPackage.HAS_TAGGED_QTYPE__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.HAS_TAGGED_QTYPE__TAGGED:
				return tagged != null;
			case AdaPackage.HAS_TAGGED_QTYPE__NOT_AN_ELEMENT:
				return notAnElement != null;
		}
		return super.eIsSet(featureID);
	}

} //HasTaggedQTypeImpl
