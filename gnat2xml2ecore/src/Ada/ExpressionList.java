/**
 */
package Ada;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expression List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.ExpressionList#getGroup <em>Group</em>}</li>
 *   <li>{@link Ada.ExpressionList#getNotAnElement <em>Not An Element</em>}</li>
 *   <li>{@link Ada.ExpressionList#getDiscreteRangeAttributeReference <em>Discrete Range Attribute Reference</em>}</li>
 *   <li>{@link Ada.ExpressionList#getDiscreteSimpleExpressionRange <em>Discrete Simple Expression Range</em>}</li>
 *   <li>{@link Ada.ExpressionList#getOthersChoice <em>Others Choice</em>}</li>
 *   <li>{@link Ada.ExpressionList#getBoxExpression <em>Box Expression</em>}</li>
 *   <li>{@link Ada.ExpressionList#getIntegerLiteral <em>Integer Literal</em>}</li>
 *   <li>{@link Ada.ExpressionList#getRealLiteral <em>Real Literal</em>}</li>
 *   <li>{@link Ada.ExpressionList#getStringLiteral <em>String Literal</em>}</li>
 *   <li>{@link Ada.ExpressionList#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link Ada.ExpressionList#getAndOperator <em>And Operator</em>}</li>
 *   <li>{@link Ada.ExpressionList#getOrOperator <em>Or Operator</em>}</li>
 *   <li>{@link Ada.ExpressionList#getXorOperator <em>Xor Operator</em>}</li>
 *   <li>{@link Ada.ExpressionList#getEqualOperator <em>Equal Operator</em>}</li>
 *   <li>{@link Ada.ExpressionList#getNotEqualOperator <em>Not Equal Operator</em>}</li>
 *   <li>{@link Ada.ExpressionList#getLessThanOperator <em>Less Than Operator</em>}</li>
 *   <li>{@link Ada.ExpressionList#getLessThanOrEqualOperator <em>Less Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.ExpressionList#getGreaterThanOperator <em>Greater Than Operator</em>}</li>
 *   <li>{@link Ada.ExpressionList#getGreaterThanOrEqualOperator <em>Greater Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.ExpressionList#getPlusOperator <em>Plus Operator</em>}</li>
 *   <li>{@link Ada.ExpressionList#getMinusOperator <em>Minus Operator</em>}</li>
 *   <li>{@link Ada.ExpressionList#getConcatenateOperator <em>Concatenate Operator</em>}</li>
 *   <li>{@link Ada.ExpressionList#getUnaryPlusOperator <em>Unary Plus Operator</em>}</li>
 *   <li>{@link Ada.ExpressionList#getUnaryMinusOperator <em>Unary Minus Operator</em>}</li>
 *   <li>{@link Ada.ExpressionList#getMultiplyOperator <em>Multiply Operator</em>}</li>
 *   <li>{@link Ada.ExpressionList#getDivideOperator <em>Divide Operator</em>}</li>
 *   <li>{@link Ada.ExpressionList#getModOperator <em>Mod Operator</em>}</li>
 *   <li>{@link Ada.ExpressionList#getRemOperator <em>Rem Operator</em>}</li>
 *   <li>{@link Ada.ExpressionList#getExponentiateOperator <em>Exponentiate Operator</em>}</li>
 *   <li>{@link Ada.ExpressionList#getAbsOperator <em>Abs Operator</em>}</li>
 *   <li>{@link Ada.ExpressionList#getNotOperator <em>Not Operator</em>}</li>
 *   <li>{@link Ada.ExpressionList#getCharacterLiteral <em>Character Literal</em>}</li>
 *   <li>{@link Ada.ExpressionList#getEnumerationLiteral <em>Enumeration Literal</em>}</li>
 *   <li>{@link Ada.ExpressionList#getExplicitDereference <em>Explicit Dereference</em>}</li>
 *   <li>{@link Ada.ExpressionList#getFunctionCall <em>Function Call</em>}</li>
 *   <li>{@link Ada.ExpressionList#getIndexedComponent <em>Indexed Component</em>}</li>
 *   <li>{@link Ada.ExpressionList#getSlice <em>Slice</em>}</li>
 *   <li>{@link Ada.ExpressionList#getSelectedComponent <em>Selected Component</em>}</li>
 *   <li>{@link Ada.ExpressionList#getAccessAttribute <em>Access Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getAddressAttribute <em>Address Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getAdjacentAttribute <em>Adjacent Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getAftAttribute <em>Aft Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getAlignmentAttribute <em>Alignment Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getBaseAttribute <em>Base Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getBitOrderAttribute <em>Bit Order Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getBodyVersionAttribute <em>Body Version Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getCallableAttribute <em>Callable Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getCallerAttribute <em>Caller Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getCeilingAttribute <em>Ceiling Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getClassAttribute <em>Class Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getComponentSizeAttribute <em>Component Size Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getComposeAttribute <em>Compose Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getConstrainedAttribute <em>Constrained Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getCopySignAttribute <em>Copy Sign Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getCountAttribute <em>Count Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getDefiniteAttribute <em>Definite Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getDeltaAttribute <em>Delta Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getDenormAttribute <em>Denorm Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getDigitsAttribute <em>Digits Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getExponentAttribute <em>Exponent Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getExternalTagAttribute <em>External Tag Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getFirstAttribute <em>First Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getFirstBitAttribute <em>First Bit Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getFloorAttribute <em>Floor Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getForeAttribute <em>Fore Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getFractionAttribute <em>Fraction Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getIdentityAttribute <em>Identity Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getImageAttribute <em>Image Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getInputAttribute <em>Input Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getLastAttribute <em>Last Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getLastBitAttribute <em>Last Bit Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getLeadingPartAttribute <em>Leading Part Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getLengthAttribute <em>Length Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getMachineAttribute <em>Machine Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getMachineEmaxAttribute <em>Machine Emax Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getMachineEminAttribute <em>Machine Emin Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getMachineMantissaAttribute <em>Machine Mantissa Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getMachineOverflowsAttribute <em>Machine Overflows Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getMachineRadixAttribute <em>Machine Radix Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getMachineRoundsAttribute <em>Machine Rounds Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getMaxAttribute <em>Max Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getMaxSizeInStorageElementsAttribute <em>Max Size In Storage Elements Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getMinAttribute <em>Min Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getModelAttribute <em>Model Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getModelEminAttribute <em>Model Emin Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getModelEpsilonAttribute <em>Model Epsilon Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getModelMantissaAttribute <em>Model Mantissa Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getModelSmallAttribute <em>Model Small Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getModulusAttribute <em>Modulus Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getOutputAttribute <em>Output Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getPartitionIdAttribute <em>Partition Id Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getPosAttribute <em>Pos Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getPositionAttribute <em>Position Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getPredAttribute <em>Pred Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getRangeAttribute <em>Range Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getReadAttribute <em>Read Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getRemainderAttribute <em>Remainder Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getRoundAttribute <em>Round Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getRoundingAttribute <em>Rounding Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getSafeFirstAttribute <em>Safe First Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getSafeLastAttribute <em>Safe Last Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getScaleAttribute <em>Scale Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getScalingAttribute <em>Scaling Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getSignedZerosAttribute <em>Signed Zeros Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getSizeAttribute <em>Size Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getSmallAttribute <em>Small Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getStoragePoolAttribute <em>Storage Pool Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getStorageSizeAttribute <em>Storage Size Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getSuccAttribute <em>Succ Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getTagAttribute <em>Tag Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getTerminatedAttribute <em>Terminated Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getTruncationAttribute <em>Truncation Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getUnbiasedRoundingAttribute <em>Unbiased Rounding Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getUncheckedAccessAttribute <em>Unchecked Access Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getValAttribute <em>Val Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getValidAttribute <em>Valid Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getValueAttribute <em>Value Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getVersionAttribute <em>Version Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getWideImageAttribute <em>Wide Image Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getWideValueAttribute <em>Wide Value Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getWideWidthAttribute <em>Wide Width Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getWidthAttribute <em>Width Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getWriteAttribute <em>Write Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getMachineRoundingAttribute <em>Machine Rounding Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getModAttribute <em>Mod Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getPriorityAttribute <em>Priority Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getStreamSizeAttribute <em>Stream Size Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getWideWideImageAttribute <em>Wide Wide Image Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getWideWideValueAttribute <em>Wide Wide Value Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getWideWideWidthAttribute <em>Wide Wide Width Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getMaxAlignmentForAllocationAttribute <em>Max Alignment For Allocation Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getOverlapsStorageAttribute <em>Overlaps Storage Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getImplementationDefinedAttribute <em>Implementation Defined Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getUnknownAttribute <em>Unknown Attribute</em>}</li>
 *   <li>{@link Ada.ExpressionList#getRecordAggregate <em>Record Aggregate</em>}</li>
 *   <li>{@link Ada.ExpressionList#getExtensionAggregate <em>Extension Aggregate</em>}</li>
 *   <li>{@link Ada.ExpressionList#getPositionalArrayAggregate <em>Positional Array Aggregate</em>}</li>
 *   <li>{@link Ada.ExpressionList#getNamedArrayAggregate <em>Named Array Aggregate</em>}</li>
 *   <li>{@link Ada.ExpressionList#getAndThenShortCircuit <em>And Then Short Circuit</em>}</li>
 *   <li>{@link Ada.ExpressionList#getOrElseShortCircuit <em>Or Else Short Circuit</em>}</li>
 *   <li>{@link Ada.ExpressionList#getInMembershipTest <em>In Membership Test</em>}</li>
 *   <li>{@link Ada.ExpressionList#getNotInMembershipTest <em>Not In Membership Test</em>}</li>
 *   <li>{@link Ada.ExpressionList#getNullLiteral <em>Null Literal</em>}</li>
 *   <li>{@link Ada.ExpressionList#getParenthesizedExpression <em>Parenthesized Expression</em>}</li>
 *   <li>{@link Ada.ExpressionList#getRaiseExpression <em>Raise Expression</em>}</li>
 *   <li>{@link Ada.ExpressionList#getTypeConversion <em>Type Conversion</em>}</li>
 *   <li>{@link Ada.ExpressionList#getQualifiedExpression <em>Qualified Expression</em>}</li>
 *   <li>{@link Ada.ExpressionList#getAllocationFromSubtype <em>Allocation From Subtype</em>}</li>
 *   <li>{@link Ada.ExpressionList#getAllocationFromQualifiedExpression <em>Allocation From Qualified Expression</em>}</li>
 *   <li>{@link Ada.ExpressionList#getCaseExpression <em>Case Expression</em>}</li>
 *   <li>{@link Ada.ExpressionList#getIfExpression <em>If Expression</em>}</li>
 *   <li>{@link Ada.ExpressionList#getForAllQuantifiedExpression <em>For All Quantified Expression</em>}</li>
 *   <li>{@link Ada.ExpressionList#getForSomeQuantifiedExpression <em>For Some Quantified Expression</em>}</li>
 *   <li>{@link Ada.ExpressionList#getComment <em>Comment</em>}</li>
 *   <li>{@link Ada.ExpressionList#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getAsynchronousPragma <em>Asynchronous Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getAtomicPragma <em>Atomic Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getAttachHandlerPragma <em>Attach Handler Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getControlledPragma <em>Controlled Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getConventionPragma <em>Convention Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getDiscardNamesPragma <em>Discard Names Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getElaboratePragma <em>Elaborate Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getElaborateAllPragma <em>Elaborate All Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getExportPragma <em>Export Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getImportPragma <em>Import Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getInlinePragma <em>Inline Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getInspectionPointPragma <em>Inspection Point Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getLinkerOptionsPragma <em>Linker Options Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getListPragma <em>List Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getLockingPolicyPragma <em>Locking Policy Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getOptimizePragma <em>Optimize Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getPackPragma <em>Pack Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getPagePragma <em>Page Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getPreelaboratePragma <em>Preelaborate Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getPriorityPragma <em>Priority Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getPurePragma <em>Pure Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getRemoteTypesPragma <em>Remote Types Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getRestrictionsPragma <em>Restrictions Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getReviewablePragma <em>Reviewable Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getSharedPassivePragma <em>Shared Passive Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getStorageSizePragma <em>Storage Size Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getSuppressPragma <em>Suppress Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getVolatilePragma <em>Volatile Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getAssertPragma <em>Assert Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getNoReturnPragma <em>No Return Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getProfilePragma <em>Profile Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getUnsuppressPragma <em>Unsuppress Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getCpuPragma <em>Cpu Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getIndependentPragma <em>Independent Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getIndependentComponentsPragma <em>Independent Components Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}</li>
 *   <li>{@link Ada.ExpressionList#getUnknownPragma <em>Unknown Pragma</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getExpressionList()
 * @model extendedMetaData="name='Expression_List' kind='elementOnly'"
 * @generated
 */
public interface ExpressionList extends EObject {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' attribute list.
	 * @see Ada.AdaPackage#getExpressionList_Group()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:0'"
	 * @generated
	 */
	FeatureMap getGroup();

	/**
	 * Returns the value of the '<em><b>Not An Element</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NotAnElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not An Element</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not An Element</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_NotAnElement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='not_an_element' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NotAnElement> getNotAnElement();

	/**
	 * Returns the value of the '<em><b>Discrete Range Attribute Reference</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscreteRangeAttributeReference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Range Attribute Reference</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Range Attribute Reference</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_DiscreteRangeAttributeReference()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discrete_range_attribute_reference' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscreteRangeAttributeReference> getDiscreteRangeAttributeReference();

	/**
	 * Returns the value of the '<em><b>Discrete Simple Expression Range</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscreteSimpleExpressionRange}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Simple Expression Range</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Simple Expression Range</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_DiscreteSimpleExpressionRange()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discrete_simple_expression_range' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscreteSimpleExpressionRange> getDiscreteSimpleExpressionRange();

	/**
	 * Returns the value of the '<em><b>Others Choice</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OthersChoice}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Others Choice</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Others Choice</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_OthersChoice()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='others_choice' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OthersChoice> getOthersChoice();

	/**
	 * Returns the value of the '<em><b>Box Expression</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.BoxExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Box Expression</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Box Expression</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_BoxExpression()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='box_expression' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<BoxExpression> getBoxExpression();

	/**
	 * Returns the value of the '<em><b>Integer Literal</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IntegerLiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integer Literal</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integer Literal</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_IntegerLiteral()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='integer_literal' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IntegerLiteral> getIntegerLiteral();

	/**
	 * Returns the value of the '<em><b>Real Literal</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RealLiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Real Literal</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Real Literal</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_RealLiteral()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='real_literal' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RealLiteral> getRealLiteral();

	/**
	 * Returns the value of the '<em><b>String Literal</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.StringLiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>String Literal</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String Literal</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_StringLiteral()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='string_literal' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<StringLiteral> getStringLiteral();

	/**
	 * Returns the value of the '<em><b>Identifier</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.Identifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Identifier</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identifier</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_Identifier()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='identifier' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<Identifier> getIdentifier();

	/**
	 * Returns the value of the '<em><b>And Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AndOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>And Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>And Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_AndOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='and_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AndOperator> getAndOperator();

	/**
	 * Returns the value of the '<em><b>Or Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OrOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Or Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Or Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_OrOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='or_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OrOperator> getOrOperator();

	/**
	 * Returns the value of the '<em><b>Xor Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.XorOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Xor Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Xor Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_XorOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='xor_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<XorOperator> getXorOperator();

	/**
	 * Returns the value of the '<em><b>Equal Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.EqualOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equal Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equal Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_EqualOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='equal_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<EqualOperator> getEqualOperator();

	/**
	 * Returns the value of the '<em><b>Not Equal Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NotEqualOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not Equal Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not Equal Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_NotEqualOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='not_equal_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NotEqualOperator> getNotEqualOperator();

	/**
	 * Returns the value of the '<em><b>Less Than Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LessThanOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Less Than Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Less Than Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_LessThanOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='less_than_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LessThanOperator> getLessThanOperator();

	/**
	 * Returns the value of the '<em><b>Less Than Or Equal Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LessThanOrEqualOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Less Than Or Equal Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Less Than Or Equal Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_LessThanOrEqualOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='less_than_or_equal_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LessThanOrEqualOperator> getLessThanOrEqualOperator();

	/**
	 * Returns the value of the '<em><b>Greater Than Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.GreaterThanOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Greater Than Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Greater Than Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_GreaterThanOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='greater_than_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<GreaterThanOperator> getGreaterThanOperator();

	/**
	 * Returns the value of the '<em><b>Greater Than Or Equal Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.GreaterThanOrEqualOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Greater Than Or Equal Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Greater Than Or Equal Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_GreaterThanOrEqualOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='greater_than_or_equal_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<GreaterThanOrEqualOperator> getGreaterThanOrEqualOperator();

	/**
	 * Returns the value of the '<em><b>Plus Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PlusOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Plus Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Plus Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_PlusOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='plus_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PlusOperator> getPlusOperator();

	/**
	 * Returns the value of the '<em><b>Minus Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MinusOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Minus Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Minus Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_MinusOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='minus_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MinusOperator> getMinusOperator();

	/**
	 * Returns the value of the '<em><b>Concatenate Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ConcatenateOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Concatenate Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Concatenate Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ConcatenateOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='concatenate_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ConcatenateOperator> getConcatenateOperator();

	/**
	 * Returns the value of the '<em><b>Unary Plus Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnaryPlusOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unary Plus Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unary Plus Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_UnaryPlusOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unary_plus_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnaryPlusOperator> getUnaryPlusOperator();

	/**
	 * Returns the value of the '<em><b>Unary Minus Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnaryMinusOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unary Minus Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unary Minus Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_UnaryMinusOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unary_minus_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnaryMinusOperator> getUnaryMinusOperator();

	/**
	 * Returns the value of the '<em><b>Multiply Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MultiplyOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Multiply Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multiply Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_MultiplyOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='multiply_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MultiplyOperator> getMultiplyOperator();

	/**
	 * Returns the value of the '<em><b>Divide Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DivideOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Divide Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Divide Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_DivideOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='divide_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DivideOperator> getDivideOperator();

	/**
	 * Returns the value of the '<em><b>Mod Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ModOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mod Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mod Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ModOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='mod_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ModOperator> getModOperator();

	/**
	 * Returns the value of the '<em><b>Rem Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RemOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rem Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rem Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_RemOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='rem_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RemOperator> getRemOperator();

	/**
	 * Returns the value of the '<em><b>Exponentiate Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExponentiateOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exponentiate Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exponentiate Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ExponentiateOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='exponentiate_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExponentiateOperator> getExponentiateOperator();

	/**
	 * Returns the value of the '<em><b>Abs Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AbsOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abs Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abs Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_AbsOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='abs_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AbsOperator> getAbsOperator();

	/**
	 * Returns the value of the '<em><b>Not Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NotOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_NotOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='not_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NotOperator> getNotOperator();

	/**
	 * Returns the value of the '<em><b>Character Literal</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CharacterLiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Character Literal</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Character Literal</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_CharacterLiteral()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='character_literal' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CharacterLiteral> getCharacterLiteral();

	/**
	 * Returns the value of the '<em><b>Enumeration Literal</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.EnumerationLiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumeration Literal</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumeration Literal</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_EnumerationLiteral()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='enumeration_literal' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<EnumerationLiteral> getEnumerationLiteral();

	/**
	 * Returns the value of the '<em><b>Explicit Dereference</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExplicitDereference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Explicit Dereference</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Explicit Dereference</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ExplicitDereference()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='explicit_dereference' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExplicitDereference> getExplicitDereference();

	/**
	 * Returns the value of the '<em><b>Function Call</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FunctionCall}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Call</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Call</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_FunctionCall()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='function_call' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FunctionCall> getFunctionCall();

	/**
	 * Returns the value of the '<em><b>Indexed Component</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IndexedComponent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Indexed Component</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Indexed Component</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_IndexedComponent()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='indexed_component' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IndexedComponent> getIndexedComponent();

	/**
	 * Returns the value of the '<em><b>Slice</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.Slice}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Slice</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Slice</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_Slice()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='slice' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<Slice> getSlice();

	/**
	 * Returns the value of the '<em><b>Selected Component</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SelectedComponent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selected Component</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selected Component</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_SelectedComponent()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='selected_component' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SelectedComponent> getSelectedComponent();

	/**
	 * Returns the value of the '<em><b>Access Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AccessAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_AccessAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='access_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AccessAttribute> getAccessAttribute();

	/**
	 * Returns the value of the '<em><b>Address Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AddressAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Address Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Address Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_AddressAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='address_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AddressAttribute> getAddressAttribute();

	/**
	 * Returns the value of the '<em><b>Adjacent Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AdjacentAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Adjacent Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Adjacent Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_AdjacentAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='adjacent_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AdjacentAttribute> getAdjacentAttribute();

	/**
	 * Returns the value of the '<em><b>Aft Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AftAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aft Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aft Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_AftAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='aft_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AftAttribute> getAftAttribute();

	/**
	 * Returns the value of the '<em><b>Alignment Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AlignmentAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alignment Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alignment Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_AlignmentAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='alignment_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AlignmentAttribute> getAlignmentAttribute();

	/**
	 * Returns the value of the '<em><b>Base Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.BaseAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_BaseAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='base_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<BaseAttribute> getBaseAttribute();

	/**
	 * Returns the value of the '<em><b>Bit Order Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.BitOrderAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bit Order Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bit Order Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_BitOrderAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='bit_order_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<BitOrderAttribute> getBitOrderAttribute();

	/**
	 * Returns the value of the '<em><b>Body Version Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.BodyVersionAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body Version Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body Version Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_BodyVersionAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='body_version_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<BodyVersionAttribute> getBodyVersionAttribute();

	/**
	 * Returns the value of the '<em><b>Callable Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CallableAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Callable Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Callable Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_CallableAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='callable_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CallableAttribute> getCallableAttribute();

	/**
	 * Returns the value of the '<em><b>Caller Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CallerAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Caller Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Caller Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_CallerAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='caller_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CallerAttribute> getCallerAttribute();

	/**
	 * Returns the value of the '<em><b>Ceiling Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CeilingAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ceiling Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ceiling Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_CeilingAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ceiling_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CeilingAttribute> getCeilingAttribute();

	/**
	 * Returns the value of the '<em><b>Class Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ClassAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ClassAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='class_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ClassAttribute> getClassAttribute();

	/**
	 * Returns the value of the '<em><b>Component Size Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ComponentSizeAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Size Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Size Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ComponentSizeAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='component_size_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ComponentSizeAttribute> getComponentSizeAttribute();

	/**
	 * Returns the value of the '<em><b>Compose Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ComposeAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Compose Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Compose Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ComposeAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='compose_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ComposeAttribute> getComposeAttribute();

	/**
	 * Returns the value of the '<em><b>Constrained Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ConstrainedAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constrained Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constrained Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ConstrainedAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='constrained_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ConstrainedAttribute> getConstrainedAttribute();

	/**
	 * Returns the value of the '<em><b>Copy Sign Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CopySignAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Copy Sign Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Copy Sign Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_CopySignAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='copy_sign_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CopySignAttribute> getCopySignAttribute();

	/**
	 * Returns the value of the '<em><b>Count Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CountAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Count Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Count Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_CountAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='count_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CountAttribute> getCountAttribute();

	/**
	 * Returns the value of the '<em><b>Definite Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiniteAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Definite Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definite Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_DefiniteAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='definite_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiniteAttribute> getDefiniteAttribute();

	/**
	 * Returns the value of the '<em><b>Delta Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DeltaAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delta Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delta Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_DeltaAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='delta_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DeltaAttribute> getDeltaAttribute();

	/**
	 * Returns the value of the '<em><b>Denorm Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DenormAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Denorm Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Denorm Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_DenormAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='denorm_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DenormAttribute> getDenormAttribute();

	/**
	 * Returns the value of the '<em><b>Digits Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DigitsAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Digits Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Digits Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_DigitsAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='digits_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DigitsAttribute> getDigitsAttribute();

	/**
	 * Returns the value of the '<em><b>Exponent Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExponentAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exponent Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exponent Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ExponentAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='exponent_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExponentAttribute> getExponentAttribute();

	/**
	 * Returns the value of the '<em><b>External Tag Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExternalTagAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External Tag Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Tag Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ExternalTagAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='external_tag_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExternalTagAttribute> getExternalTagAttribute();

	/**
	 * Returns the value of the '<em><b>First Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FirstAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_FirstAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='first_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FirstAttribute> getFirstAttribute();

	/**
	 * Returns the value of the '<em><b>First Bit Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FirstBitAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Bit Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Bit Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_FirstBitAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='first_bit_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FirstBitAttribute> getFirstBitAttribute();

	/**
	 * Returns the value of the '<em><b>Floor Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FloorAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Floor Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Floor Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_FloorAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='floor_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FloorAttribute> getFloorAttribute();

	/**
	 * Returns the value of the '<em><b>Fore Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ForeAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fore Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fore Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ForeAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='fore_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ForeAttribute> getForeAttribute();

	/**
	 * Returns the value of the '<em><b>Fraction Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FractionAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fraction Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fraction Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_FractionAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='fraction_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FractionAttribute> getFractionAttribute();

	/**
	 * Returns the value of the '<em><b>Identity Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IdentityAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Identity Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identity Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_IdentityAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='identity_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IdentityAttribute> getIdentityAttribute();

	/**
	 * Returns the value of the '<em><b>Image Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ImageAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Image Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Image Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ImageAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='image_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ImageAttribute> getImageAttribute();

	/**
	 * Returns the value of the '<em><b>Input Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InputAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_InputAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='input_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InputAttribute> getInputAttribute();

	/**
	 * Returns the value of the '<em><b>Last Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LastAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_LastAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='last_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LastAttribute> getLastAttribute();

	/**
	 * Returns the value of the '<em><b>Last Bit Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LastBitAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Bit Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Bit Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_LastBitAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='last_bit_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LastBitAttribute> getLastBitAttribute();

	/**
	 * Returns the value of the '<em><b>Leading Part Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LeadingPartAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Leading Part Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Leading Part Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_LeadingPartAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='leading_part_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LeadingPartAttribute> getLeadingPartAttribute();

	/**
	 * Returns the value of the '<em><b>Length Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LengthAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Length Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Length Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_LengthAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='length_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LengthAttribute> getLengthAttribute();

	/**
	 * Returns the value of the '<em><b>Machine Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MachineAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_MachineAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='machine_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MachineAttribute> getMachineAttribute();

	/**
	 * Returns the value of the '<em><b>Machine Emax Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MachineEmaxAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Emax Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Emax Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_MachineEmaxAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='machine_emax_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MachineEmaxAttribute> getMachineEmaxAttribute();

	/**
	 * Returns the value of the '<em><b>Machine Emin Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MachineEminAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Emin Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Emin Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_MachineEminAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='machine_emin_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MachineEminAttribute> getMachineEminAttribute();

	/**
	 * Returns the value of the '<em><b>Machine Mantissa Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MachineMantissaAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Mantissa Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Mantissa Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_MachineMantissaAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='machine_mantissa_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MachineMantissaAttribute> getMachineMantissaAttribute();

	/**
	 * Returns the value of the '<em><b>Machine Overflows Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MachineOverflowsAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Overflows Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Overflows Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_MachineOverflowsAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='machine_overflows_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MachineOverflowsAttribute> getMachineOverflowsAttribute();

	/**
	 * Returns the value of the '<em><b>Machine Radix Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MachineRadixAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Radix Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Radix Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_MachineRadixAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='machine_radix_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MachineRadixAttribute> getMachineRadixAttribute();

	/**
	 * Returns the value of the '<em><b>Machine Rounds Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MachineRoundsAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Rounds Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Rounds Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_MachineRoundsAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='machine_rounds_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MachineRoundsAttribute> getMachineRoundsAttribute();

	/**
	 * Returns the value of the '<em><b>Max Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MaxAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_MaxAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='max_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MaxAttribute> getMaxAttribute();

	/**
	 * Returns the value of the '<em><b>Max Size In Storage Elements Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MaxSizeInStorageElementsAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Size In Storage Elements Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Size In Storage Elements Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_MaxSizeInStorageElementsAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='max_size_in_storage_elements_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MaxSizeInStorageElementsAttribute> getMaxSizeInStorageElementsAttribute();

	/**
	 * Returns the value of the '<em><b>Min Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MinAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_MinAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='min_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MinAttribute> getMinAttribute();

	/**
	 * Returns the value of the '<em><b>Model Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ModelAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ModelAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='model_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ModelAttribute> getModelAttribute();

	/**
	 * Returns the value of the '<em><b>Model Emin Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ModelEminAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Emin Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Emin Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ModelEminAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='model_emin_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ModelEminAttribute> getModelEminAttribute();

	/**
	 * Returns the value of the '<em><b>Model Epsilon Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ModelEpsilonAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Epsilon Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Epsilon Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ModelEpsilonAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='model_epsilon_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ModelEpsilonAttribute> getModelEpsilonAttribute();

	/**
	 * Returns the value of the '<em><b>Model Mantissa Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ModelMantissaAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Mantissa Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Mantissa Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ModelMantissaAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='model_mantissa_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ModelMantissaAttribute> getModelMantissaAttribute();

	/**
	 * Returns the value of the '<em><b>Model Small Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ModelSmallAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Small Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Small Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ModelSmallAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='model_small_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ModelSmallAttribute> getModelSmallAttribute();

	/**
	 * Returns the value of the '<em><b>Modulus Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ModulusAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Modulus Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Modulus Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ModulusAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='modulus_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ModulusAttribute> getModulusAttribute();

	/**
	 * Returns the value of the '<em><b>Output Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OutputAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_OutputAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='output_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OutputAttribute> getOutputAttribute();

	/**
	 * Returns the value of the '<em><b>Partition Id Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PartitionIdAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partition Id Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partition Id Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_PartitionIdAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='partition_id_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PartitionIdAttribute> getPartitionIdAttribute();

	/**
	 * Returns the value of the '<em><b>Pos Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PosAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pos Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pos Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_PosAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='pos_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PosAttribute> getPosAttribute();

	/**
	 * Returns the value of the '<em><b>Position Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PositionAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Position Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_PositionAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='position_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PositionAttribute> getPositionAttribute();

	/**
	 * Returns the value of the '<em><b>Pred Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PredAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pred Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pred Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_PredAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='pred_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PredAttribute> getPredAttribute();

	/**
	 * Returns the value of the '<em><b>Range Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RangeAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Range Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_RangeAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='range_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RangeAttribute> getRangeAttribute();

	/**
	 * Returns the value of the '<em><b>Read Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ReadAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Read Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Read Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ReadAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='read_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ReadAttribute> getReadAttribute();

	/**
	 * Returns the value of the '<em><b>Remainder Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RemainderAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remainder Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remainder Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_RemainderAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='remainder_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RemainderAttribute> getRemainderAttribute();

	/**
	 * Returns the value of the '<em><b>Round Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RoundAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Round Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Round Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_RoundAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='round_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RoundAttribute> getRoundAttribute();

	/**
	 * Returns the value of the '<em><b>Rounding Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RoundingAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rounding Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rounding Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_RoundingAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='rounding_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RoundingAttribute> getRoundingAttribute();

	/**
	 * Returns the value of the '<em><b>Safe First Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SafeFirstAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Safe First Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Safe First Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_SafeFirstAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='safe_first_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SafeFirstAttribute> getSafeFirstAttribute();

	/**
	 * Returns the value of the '<em><b>Safe Last Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SafeLastAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Safe Last Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Safe Last Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_SafeLastAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='safe_last_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SafeLastAttribute> getSafeLastAttribute();

	/**
	 * Returns the value of the '<em><b>Scale Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ScaleAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scale Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scale Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ScaleAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='scale_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ScaleAttribute> getScaleAttribute();

	/**
	 * Returns the value of the '<em><b>Scaling Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ScalingAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scaling Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scaling Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ScalingAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='scaling_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ScalingAttribute> getScalingAttribute();

	/**
	 * Returns the value of the '<em><b>Signed Zeros Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SignedZerosAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signed Zeros Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signed Zeros Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_SignedZerosAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='signed_zeros_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SignedZerosAttribute> getSignedZerosAttribute();

	/**
	 * Returns the value of the '<em><b>Size Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SizeAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Size Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Size Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_SizeAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='size_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SizeAttribute> getSizeAttribute();

	/**
	 * Returns the value of the '<em><b>Small Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SmallAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Small Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Small Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_SmallAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='small_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SmallAttribute> getSmallAttribute();

	/**
	 * Returns the value of the '<em><b>Storage Pool Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.StoragePoolAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Pool Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Pool Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_StoragePoolAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='storage_pool_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<StoragePoolAttribute> getStoragePoolAttribute();

	/**
	 * Returns the value of the '<em><b>Storage Size Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.StorageSizeAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Size Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Size Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_StorageSizeAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='storage_size_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<StorageSizeAttribute> getStorageSizeAttribute();

	/**
	 * Returns the value of the '<em><b>Succ Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SuccAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Succ Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Succ Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_SuccAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='succ_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SuccAttribute> getSuccAttribute();

	/**
	 * Returns the value of the '<em><b>Tag Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TagAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tag Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tag Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_TagAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='tag_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TagAttribute> getTagAttribute();

	/**
	 * Returns the value of the '<em><b>Terminated Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TerminatedAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Terminated Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Terminated Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_TerminatedAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='terminated_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TerminatedAttribute> getTerminatedAttribute();

	/**
	 * Returns the value of the '<em><b>Truncation Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TruncationAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Truncation Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Truncation Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_TruncationAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='truncation_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TruncationAttribute> getTruncationAttribute();

	/**
	 * Returns the value of the '<em><b>Unbiased Rounding Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnbiasedRoundingAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unbiased Rounding Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unbiased Rounding Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_UnbiasedRoundingAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unbiased_rounding_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnbiasedRoundingAttribute> getUnbiasedRoundingAttribute();

	/**
	 * Returns the value of the '<em><b>Unchecked Access Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UncheckedAccessAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unchecked Access Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unchecked Access Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_UncheckedAccessAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unchecked_access_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UncheckedAccessAttribute> getUncheckedAccessAttribute();

	/**
	 * Returns the value of the '<em><b>Val Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ValAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Val Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Val Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ValAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='val_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ValAttribute> getValAttribute();

	/**
	 * Returns the value of the '<em><b>Valid Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ValidAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Valid Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Valid Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ValidAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='valid_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ValidAttribute> getValidAttribute();

	/**
	 * Returns the value of the '<em><b>Value Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ValueAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ValueAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='value_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ValueAttribute> getValueAttribute();

	/**
	 * Returns the value of the '<em><b>Version Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.VersionAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_VersionAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='version_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<VersionAttribute> getVersionAttribute();

	/**
	 * Returns the value of the '<em><b>Wide Image Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WideImageAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Image Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Image Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_WideImageAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='wide_image_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WideImageAttribute> getWideImageAttribute();

	/**
	 * Returns the value of the '<em><b>Wide Value Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WideValueAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Value Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Value Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_WideValueAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='wide_value_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WideValueAttribute> getWideValueAttribute();

	/**
	 * Returns the value of the '<em><b>Wide Width Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WideWidthAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Width Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Width Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_WideWidthAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='wide_width_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WideWidthAttribute> getWideWidthAttribute();

	/**
	 * Returns the value of the '<em><b>Width Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WidthAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Width Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Width Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_WidthAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='width_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WidthAttribute> getWidthAttribute();

	/**
	 * Returns the value of the '<em><b>Write Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WriteAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Write Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Write Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_WriteAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='write_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WriteAttribute> getWriteAttribute();

	/**
	 * Returns the value of the '<em><b>Machine Rounding Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MachineRoundingAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Rounding Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Rounding Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_MachineRoundingAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='machine_rounding_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MachineRoundingAttribute> getMachineRoundingAttribute();

	/**
	 * Returns the value of the '<em><b>Mod Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ModAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mod Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mod Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ModAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='mod_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ModAttribute> getModAttribute();

	/**
	 * Returns the value of the '<em><b>Priority Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PriorityAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_PriorityAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='priority_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PriorityAttribute> getPriorityAttribute();

	/**
	 * Returns the value of the '<em><b>Stream Size Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.StreamSizeAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stream Size Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stream Size Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_StreamSizeAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='stream_size_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<StreamSizeAttribute> getStreamSizeAttribute();

	/**
	 * Returns the value of the '<em><b>Wide Wide Image Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WideWideImageAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Wide Image Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Wide Image Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_WideWideImageAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='wide_wide_image_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WideWideImageAttribute> getWideWideImageAttribute();

	/**
	 * Returns the value of the '<em><b>Wide Wide Value Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WideWideValueAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Wide Value Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Wide Value Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_WideWideValueAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='wide_wide_value_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WideWideValueAttribute> getWideWideValueAttribute();

	/**
	 * Returns the value of the '<em><b>Wide Wide Width Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WideWideWidthAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Wide Width Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Wide Width Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_WideWideWidthAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='wide_wide_width_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WideWideWidthAttribute> getWideWideWidthAttribute();

	/**
	 * Returns the value of the '<em><b>Max Alignment For Allocation Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MaxAlignmentForAllocationAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Alignment For Allocation Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Alignment For Allocation Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_MaxAlignmentForAllocationAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='max_alignment_for_allocation_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MaxAlignmentForAllocationAttribute> getMaxAlignmentForAllocationAttribute();

	/**
	 * Returns the value of the '<em><b>Overlaps Storage Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OverlapsStorageAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overlaps Storage Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overlaps Storage Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_OverlapsStorageAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='overlaps_storage_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OverlapsStorageAttribute> getOverlapsStorageAttribute();

	/**
	 * Returns the value of the '<em><b>Implementation Defined Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ImplementationDefinedAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implementation Defined Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implementation Defined Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ImplementationDefinedAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='implementation_defined_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ImplementationDefinedAttribute> getImplementationDefinedAttribute();

	/**
	 * Returns the value of the '<em><b>Unknown Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnknownAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unknown Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unknown Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_UnknownAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unknown_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnknownAttribute> getUnknownAttribute();

	/**
	 * Returns the value of the '<em><b>Record Aggregate</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RecordAggregate}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Aggregate</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Aggregate</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_RecordAggregate()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='record_aggregate' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RecordAggregate> getRecordAggregate();

	/**
	 * Returns the value of the '<em><b>Extension Aggregate</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExtensionAggregate}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extension Aggregate</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extension Aggregate</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ExtensionAggregate()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='extension_aggregate' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExtensionAggregate> getExtensionAggregate();

	/**
	 * Returns the value of the '<em><b>Positional Array Aggregate</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PositionalArrayAggregate}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Positional Array Aggregate</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Positional Array Aggregate</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_PositionalArrayAggregate()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='positional_array_aggregate' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PositionalArrayAggregate> getPositionalArrayAggregate();

	/**
	 * Returns the value of the '<em><b>Named Array Aggregate</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NamedArrayAggregate}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Named Array Aggregate</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Named Array Aggregate</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_NamedArrayAggregate()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='named_array_aggregate' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NamedArrayAggregate> getNamedArrayAggregate();

	/**
	 * Returns the value of the '<em><b>And Then Short Circuit</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AndThenShortCircuit}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>And Then Short Circuit</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>And Then Short Circuit</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_AndThenShortCircuit()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='and_then_short_circuit' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AndThenShortCircuit> getAndThenShortCircuit();

	/**
	 * Returns the value of the '<em><b>Or Else Short Circuit</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OrElseShortCircuit}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Or Else Short Circuit</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Or Else Short Circuit</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_OrElseShortCircuit()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='or_else_short_circuit' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OrElseShortCircuit> getOrElseShortCircuit();

	/**
	 * Returns the value of the '<em><b>In Membership Test</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InMembershipTest}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In Membership Test</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In Membership Test</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_InMembershipTest()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='in_membership_test' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InMembershipTest> getInMembershipTest();

	/**
	 * Returns the value of the '<em><b>Not In Membership Test</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NotInMembershipTest}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not In Membership Test</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not In Membership Test</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_NotInMembershipTest()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='not_in_membership_test' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NotInMembershipTest> getNotInMembershipTest();

	/**
	 * Returns the value of the '<em><b>Null Literal</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NullLiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Null Literal</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Null Literal</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_NullLiteral()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='null_literal' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NullLiteral> getNullLiteral();

	/**
	 * Returns the value of the '<em><b>Parenthesized Expression</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ParenthesizedExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parenthesized Expression</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parenthesized Expression</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ParenthesizedExpression()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='parenthesized_expression' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ParenthesizedExpression> getParenthesizedExpression();

	/**
	 * Returns the value of the '<em><b>Raise Expression</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RaiseExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Raise Expression</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Raise Expression</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_RaiseExpression()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='raise_expression' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RaiseExpression> getRaiseExpression();

	/**
	 * Returns the value of the '<em><b>Type Conversion</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TypeConversion}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Conversion</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Conversion</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_TypeConversion()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='type_conversion' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TypeConversion> getTypeConversion();

	/**
	 * Returns the value of the '<em><b>Qualified Expression</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.QualifiedExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Qualified Expression</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Qualified Expression</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_QualifiedExpression()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='qualified_expression' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<QualifiedExpression> getQualifiedExpression();

	/**
	 * Returns the value of the '<em><b>Allocation From Subtype</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AllocationFromSubtype}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Allocation From Subtype</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Allocation From Subtype</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_AllocationFromSubtype()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='allocation_from_subtype' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AllocationFromSubtype> getAllocationFromSubtype();

	/**
	 * Returns the value of the '<em><b>Allocation From Qualified Expression</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AllocationFromQualifiedExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Allocation From Qualified Expression</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Allocation From Qualified Expression</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_AllocationFromQualifiedExpression()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='allocation_from_qualified_expression' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AllocationFromQualifiedExpression> getAllocationFromQualifiedExpression();

	/**
	 * Returns the value of the '<em><b>Case Expression</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CaseExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Case Expression</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Case Expression</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_CaseExpression()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='case_expression' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CaseExpression> getCaseExpression();

	/**
	 * Returns the value of the '<em><b>If Expression</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IfExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>If Expression</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>If Expression</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_IfExpression()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='if_expression' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IfExpression> getIfExpression();

	/**
	 * Returns the value of the '<em><b>For All Quantified Expression</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ForAllQuantifiedExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>For All Quantified Expression</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>For All Quantified Expression</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ForAllQuantifiedExpression()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='for_all_quantified_expression' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ForAllQuantifiedExpression> getForAllQuantifiedExpression();

	/**
	 * Returns the value of the '<em><b>For Some Quantified Expression</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ForSomeQuantifiedExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>For Some Quantified Expression</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>For Some Quantified Expression</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ForSomeQuantifiedExpression()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='for_some_quantified_expression' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ForSomeQuantifiedExpression> getForSomeQuantifiedExpression();

	/**
	 * Returns the value of the '<em><b>Comment</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.Comment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comment</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comment</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_Comment()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='comment' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<Comment> getComment();

	/**
	 * Returns the value of the '<em><b>All Calls Remote Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AllCallsRemotePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Calls Remote Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Calls Remote Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_AllCallsRemotePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='all_calls_remote_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AllCallsRemotePragma> getAllCallsRemotePragma();

	/**
	 * Returns the value of the '<em><b>Asynchronous Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AsynchronousPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asynchronous Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asynchronous Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_AsynchronousPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='asynchronous_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AsynchronousPragma> getAsynchronousPragma();

	/**
	 * Returns the value of the '<em><b>Atomic Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AtomicPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_AtomicPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='atomic_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AtomicPragma> getAtomicPragma();

	/**
	 * Returns the value of the '<em><b>Atomic Components Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AtomicComponentsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Components Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Components Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_AtomicComponentsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='atomic_components_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AtomicComponentsPragma> getAtomicComponentsPragma();

	/**
	 * Returns the value of the '<em><b>Attach Handler Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AttachHandlerPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attach Handler Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attach Handler Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_AttachHandlerPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='attach_handler_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AttachHandlerPragma> getAttachHandlerPragma();

	/**
	 * Returns the value of the '<em><b>Controlled Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ControlledPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controlled Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controlled Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ControlledPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='controlled_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ControlledPragma> getControlledPragma();

	/**
	 * Returns the value of the '<em><b>Convention Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ConventionPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Convention Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Convention Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ConventionPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='convention_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ConventionPragma> getConventionPragma();

	/**
	 * Returns the value of the '<em><b>Discard Names Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscardNamesPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discard Names Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discard Names Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_DiscardNamesPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discard_names_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscardNamesPragma> getDiscardNamesPragma();

	/**
	 * Returns the value of the '<em><b>Elaborate Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElaboratePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ElaboratePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='elaborate_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElaboratePragma> getElaboratePragma();

	/**
	 * Returns the value of the '<em><b>Elaborate All Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElaborateAllPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate All Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate All Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ElaborateAllPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='elaborate_all_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElaborateAllPragma> getElaborateAllPragma();

	/**
	 * Returns the value of the '<em><b>Elaborate Body Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElaborateBodyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Body Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Body Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ElaborateBodyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='elaborate_body_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElaborateBodyPragma> getElaborateBodyPragma();

	/**
	 * Returns the value of the '<em><b>Export Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExportPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Export Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Export Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ExportPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='export_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExportPragma> getExportPragma();

	/**
	 * Returns the value of the '<em><b>Import Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ImportPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Import Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Import Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ImportPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='import_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ImportPragma> getImportPragma();

	/**
	 * Returns the value of the '<em><b>Inline Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InlinePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inline Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inline Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_InlinePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='inline_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InlinePragma> getInlinePragma();

	/**
	 * Returns the value of the '<em><b>Inspection Point Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InspectionPointPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inspection Point Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inspection Point Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_InspectionPointPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='inspection_point_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InspectionPointPragma> getInspectionPointPragma();

	/**
	 * Returns the value of the '<em><b>Interrupt Handler Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InterruptHandlerPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Handler Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Handler Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_InterruptHandlerPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='interrupt_handler_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InterruptHandlerPragma> getInterruptHandlerPragma();

	/**
	 * Returns the value of the '<em><b>Interrupt Priority Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InterruptPriorityPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Priority Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Priority Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_InterruptPriorityPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='interrupt_priority_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InterruptPriorityPragma> getInterruptPriorityPragma();

	/**
	 * Returns the value of the '<em><b>Linker Options Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LinkerOptionsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Linker Options Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Linker Options Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_LinkerOptionsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='linker_options_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LinkerOptionsPragma> getLinkerOptionsPragma();

	/**
	 * Returns the value of the '<em><b>List Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ListPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ListPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='list_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ListPragma> getListPragma();

	/**
	 * Returns the value of the '<em><b>Locking Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LockingPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Locking Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Locking Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_LockingPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='locking_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LockingPolicyPragma> getLockingPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Normalize Scalars Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NormalizeScalarsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Normalize Scalars Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Normalize Scalars Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_NormalizeScalarsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='normalize_scalars_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NormalizeScalarsPragma> getNormalizeScalarsPragma();

	/**
	 * Returns the value of the '<em><b>Optimize Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OptimizePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Optimize Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Optimize Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_OptimizePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='optimize_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OptimizePragma> getOptimizePragma();

	/**
	 * Returns the value of the '<em><b>Pack Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PackPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pack Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pack Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_PackPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='pack_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PackPragma> getPackPragma();

	/**
	 * Returns the value of the '<em><b>Page Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PagePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Page Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Page Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_PagePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='page_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PagePragma> getPagePragma();

	/**
	 * Returns the value of the '<em><b>Preelaborate Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PreelaboratePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborate Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborate Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_PreelaboratePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='preelaborate_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PreelaboratePragma> getPreelaboratePragma();

	/**
	 * Returns the value of the '<em><b>Priority Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PriorityPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_PriorityPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='priority_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PriorityPragma> getPriorityPragma();

	/**
	 * Returns the value of the '<em><b>Pure Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PurePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pure Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pure Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_PurePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='pure_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PurePragma> getPurePragma();

	/**
	 * Returns the value of the '<em><b>Queuing Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.QueuingPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Queuing Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Queuing Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_QueuingPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='queuing_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<QueuingPolicyPragma> getQueuingPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Remote Call Interface Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RemoteCallInterfacePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Call Interface Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Call Interface Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_RemoteCallInterfacePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='remote_call_interface_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RemoteCallInterfacePragma> getRemoteCallInterfacePragma();

	/**
	 * Returns the value of the '<em><b>Remote Types Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RemoteTypesPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Types Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Types Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_RemoteTypesPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='remote_types_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RemoteTypesPragma> getRemoteTypesPragma();

	/**
	 * Returns the value of the '<em><b>Restrictions Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RestrictionsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Restrictions Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Restrictions Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_RestrictionsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='restrictions_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RestrictionsPragma> getRestrictionsPragma();

	/**
	 * Returns the value of the '<em><b>Reviewable Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ReviewablePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reviewable Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reviewable Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ReviewablePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='reviewable_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ReviewablePragma> getReviewablePragma();

	/**
	 * Returns the value of the '<em><b>Shared Passive Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SharedPassivePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shared Passive Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shared Passive Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_SharedPassivePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='shared_passive_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SharedPassivePragma> getSharedPassivePragma();

	/**
	 * Returns the value of the '<em><b>Storage Size Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.StorageSizePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Size Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Size Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_StorageSizePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='storage_size_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<StorageSizePragma> getStorageSizePragma();

	/**
	 * Returns the value of the '<em><b>Suppress Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SuppressPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Suppress Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Suppress Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_SuppressPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='suppress_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SuppressPragma> getSuppressPragma();

	/**
	 * Returns the value of the '<em><b>Task Dispatching Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TaskDispatchingPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Dispatching Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Dispatching Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_TaskDispatchingPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='task_dispatching_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TaskDispatchingPolicyPragma> getTaskDispatchingPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Volatile Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.VolatilePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_VolatilePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='volatile_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<VolatilePragma> getVolatilePragma();

	/**
	 * Returns the value of the '<em><b>Volatile Components Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.VolatileComponentsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Components Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Components Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_VolatileComponentsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='volatile_components_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<VolatileComponentsPragma> getVolatileComponentsPragma();

	/**
	 * Returns the value of the '<em><b>Assert Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AssertPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assert Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assert Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_AssertPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='assert_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AssertPragma> getAssertPragma();

	/**
	 * Returns the value of the '<em><b>Assertion Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AssertionPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assertion Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assertion Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_AssertionPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='assertion_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AssertionPolicyPragma> getAssertionPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Detect Blocking Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DetectBlockingPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Detect Blocking Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Detect Blocking Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_DetectBlockingPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='detect_blocking_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DetectBlockingPragma> getDetectBlockingPragma();

	/**
	 * Returns the value of the '<em><b>No Return Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NoReturnPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>No Return Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>No Return Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_NoReturnPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='no_return_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NoReturnPragma> getNoReturnPragma();

	/**
	 * Returns the value of the '<em><b>Partition Elaboration Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PartitionElaborationPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partition Elaboration Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partition Elaboration Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_PartitionElaborationPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='partition_elaboration_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PartitionElaborationPolicyPragma> getPartitionElaborationPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Preelaborable Initialization Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PreelaborableInitializationPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborable Initialization Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborable Initialization Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_PreelaborableInitializationPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='preelaborable_initialization_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PreelaborableInitializationPragma> getPreelaborableInitializationPragma();

	/**
	 * Returns the value of the '<em><b>Priority Specific Dispatching Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PrioritySpecificDispatchingPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Specific Dispatching Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Specific Dispatching Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_PrioritySpecificDispatchingPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='priority_specific_dispatching_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PrioritySpecificDispatchingPragma> getPrioritySpecificDispatchingPragma();

	/**
	 * Returns the value of the '<em><b>Profile Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProfilePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Profile Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Profile Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ProfilePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='profile_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProfilePragma> getProfilePragma();

	/**
	 * Returns the value of the '<em><b>Relative Deadline Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RelativeDeadlinePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relative Deadline Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relative Deadline Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_RelativeDeadlinePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='relative_deadline_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RelativeDeadlinePragma> getRelativeDeadlinePragma();

	/**
	 * Returns the value of the '<em><b>Unchecked Union Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UncheckedUnionPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unchecked Union Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unchecked Union Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_UncheckedUnionPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unchecked_union_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UncheckedUnionPragma> getUncheckedUnionPragma();

	/**
	 * Returns the value of the '<em><b>Unsuppress Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnsuppressPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unsuppress Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unsuppress Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_UnsuppressPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unsuppress_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnsuppressPragma> getUnsuppressPragma();

	/**
	 * Returns the value of the '<em><b>Default Storage Pool Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefaultStoragePoolPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Storage Pool Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Storage Pool Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_DefaultStoragePoolPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='default_storage_pool_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefaultStoragePoolPragma> getDefaultStoragePoolPragma();

	/**
	 * Returns the value of the '<em><b>Dispatching Domain Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DispatchingDomainPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dispatching Domain Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dispatching Domain Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_DispatchingDomainPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='dispatching_domain_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DispatchingDomainPragma> getDispatchingDomainPragma();

	/**
	 * Returns the value of the '<em><b>Cpu Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CpuPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cpu Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cpu Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_CpuPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='cpu_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CpuPragma> getCpuPragma();

	/**
	 * Returns the value of the '<em><b>Independent Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IndependentPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_IndependentPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='independent_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IndependentPragma> getIndependentPragma();

	/**
	 * Returns the value of the '<em><b>Independent Components Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IndependentComponentsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Components Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Components Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_IndependentComponentsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='independent_components_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IndependentComponentsPragma> getIndependentComponentsPragma();

	/**
	 * Returns the value of the '<em><b>Implementation Defined Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ImplementationDefinedPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implementation Defined Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implementation Defined Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_ImplementationDefinedPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='implementation_defined_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ImplementationDefinedPragma> getImplementationDefinedPragma();

	/**
	 * Returns the value of the '<em><b>Unknown Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnknownPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unknown Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unknown Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getExpressionList_UnknownPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unknown_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnknownPragma> getUnknownPragma();

} // ExpressionList
