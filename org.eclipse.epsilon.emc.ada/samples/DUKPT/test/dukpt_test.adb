with Crypto.Types; use Crypto.Types;
with DUKPT; use DUKPT;
with DUKPT.Utilities; use DUKPT.Utilities;
with Ada.Text_IO; use Ada.Text_IO;
with Interfaces; use Interfaces;
with Crypto.Symmetric.Blockcipher_AES128; use Crypto.Symmetric.Blockcipher_AES128;

procedure DUKPT_Test is

   Iterations : Integer;

   TestData_BDK  : B_Block128 := (16#01#, 16#23#, 16#45#, 16#67#,
                                  16#89#, 16#AB#, 16#CD#, 16#EF#,
                                  16#FE#, 16#DC#, 16#BA#, 16#98#,
                                  16#76#, 16#54#, 16#32#, 16#10#);
   TestData_IKSN : B_Block128 := (16#00#, 16#00#, 16#00#, 16#00#,
                                  16#00#, 16#00#, 16#00#, 16#00#,
                                  16#00#, 16#00#, 16#00#, 16#00#,
                                  16#00#, 16#00#, 16#00#, 16#00#);
   IPEK          : B_Block128;
   KSN           : B_Block128;
   Encrypt_Key_S : B_Block128;
   MAC_Key_S     : B_Block128;
   Encrypt_Key_R : B_Block128;
   MAC_Key_R     : B_Block128;

begin

   Iterations := 2#1_1111_1111_1111_1111_1111#;

   -- Initialisation and key generation
   DUKPT.Derive_IPEK(TestData_BDK, TestData_IKSN, IPEK);
   DUKPT.Load_Initial_Keys(IPEK, TestData_IKSN);
   for A in 1 .. Iterations loop
      DUKPT.Get_Sender_Transaction_Keys(KSN, Encrypt_Key_S, MAC_Key_S);
   end loop;

   New_Line;
   Put_Line("Sender details:");
   Put("  IPEK: ");
   for A in 0..15 loop
      Put(To_Hex(Natural(IPEK(A))) & " ");
   end loop;
   New_Line;
   Put("  KSN : ");
   for A in 0..15 loop
      Put(To_Hex(Natural(KSN(A))) & " ");
   end loop;
   New_Line;
   Put("  EK  : ");
   for A in 0..15 loop
      Put(To_Hex(Natural(Encrypt_Key_S(A))) & " ");
   end loop;
   New_Line;
   Put("  MACK: ");
   for A in 0..15 loop
      Put(To_Hex(Natural(MAC_Key_S(A))) & " ");
   end loop;
   New_Line;
   New_Line;

   -- Server side function must be stateless
   -- This is not stateless
   DUKPT.Derive_IPEK(TestData_BDK, KSN, IPEK);
   DUKPT.Retrieve_Transaction_Keys(IPEK, KSN, Encrypt_Key_R, MAC_Key_R);

   New_Line;
   Put_Line("Receiver details:");
   Put("  IPEK: ");
   for A in 0..15 loop
      Put(To_Hex(Natural(IPEK(A))) & " ");
   end loop;
   New_Line;
   Put("  KSN : ");
   for A in 0..15 loop
      Put(To_Hex(Natural(KSN(A))) & " ");
   end loop;
   New_Line;
   Put("  EK  : ");
   for A in 0..15 loop
      Put(To_Hex(Natural(Encrypt_Key_R(A))) & " ");
   end loop;
   New_Line;
   Put("  MACK: ");
   for A in 0..15 loop
      Put(To_Hex(Natural(MAC_Key_R(A))) & " ");
   end loop;
   New_Line;New_Line;

   pragma Assert_And_Cut(Encrypt_Key_S = Encrypt_Key_R and MAC_Key_S = MAC_Key_R);



end DUKPT_Test;
