/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Defining Name Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.DefiningNameClass#getNotAnElement <em>Not An Element</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefiningIdentifier <em>Defining Identifier</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefiningCharacterLiteral <em>Defining Character Literal</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefiningEnumerationLiteral <em>Defining Enumeration Literal</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefiningAndOperator <em>Defining And Operator</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefiningOrOperator <em>Defining Or Operator</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefiningXorOperator <em>Defining Xor Operator</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefiningEqualOperator <em>Defining Equal Operator</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefiningNotEqualOperator <em>Defining Not Equal Operator</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefiningLessThanOperator <em>Defining Less Than Operator</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefiningLessThanOrEqualOperator <em>Defining Less Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefiningGreaterThanOperator <em>Defining Greater Than Operator</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefiningGreaterThanOrEqualOperator <em>Defining Greater Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefiningPlusOperator <em>Defining Plus Operator</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefiningMinusOperator <em>Defining Minus Operator</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefiningConcatenateOperator <em>Defining Concatenate Operator</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefiningUnaryPlusOperator <em>Defining Unary Plus Operator</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefiningUnaryMinusOperator <em>Defining Unary Minus Operator</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefiningMultiplyOperator <em>Defining Multiply Operator</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefiningDivideOperator <em>Defining Divide Operator</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefiningModOperator <em>Defining Mod Operator</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefiningRemOperator <em>Defining Rem Operator</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefiningExponentiateOperator <em>Defining Exponentiate Operator</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefiningAbsOperator <em>Defining Abs Operator</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefiningNotOperator <em>Defining Not Operator</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefiningExpandedName <em>Defining Expanded Name</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getComment <em>Comment</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getAsynchronousPragma <em>Asynchronous Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getAtomicPragma <em>Atomic Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getAttachHandlerPragma <em>Attach Handler Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getControlledPragma <em>Controlled Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getConventionPragma <em>Convention Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDiscardNamesPragma <em>Discard Names Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getElaboratePragma <em>Elaborate Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getElaborateAllPragma <em>Elaborate All Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getExportPragma <em>Export Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getImportPragma <em>Import Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getInlinePragma <em>Inline Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getInspectionPointPragma <em>Inspection Point Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getLinkerOptionsPragma <em>Linker Options Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getListPragma <em>List Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getLockingPolicyPragma <em>Locking Policy Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getOptimizePragma <em>Optimize Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getPackPragma <em>Pack Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getPagePragma <em>Page Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getPreelaboratePragma <em>Preelaborate Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getPriorityPragma <em>Priority Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getPurePragma <em>Pure Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getRemoteTypesPragma <em>Remote Types Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getRestrictionsPragma <em>Restrictions Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getReviewablePragma <em>Reviewable Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getSharedPassivePragma <em>Shared Passive Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getStorageSizePragma <em>Storage Size Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getSuppressPragma <em>Suppress Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getVolatilePragma <em>Volatile Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getAssertPragma <em>Assert Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getNoReturnPragma <em>No Return Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getProfilePragma <em>Profile Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getUnsuppressPragma <em>Unsuppress Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getCpuPragma <em>Cpu Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getIndependentPragma <em>Independent Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getIndependentComponentsPragma <em>Independent Components Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}</li>
 *   <li>{@link Ada.DefiningNameClass#getUnknownPragma <em>Unknown Pragma</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getDefiningNameClass()
 * @model extendedMetaData="name='Defining_Name_Class' kind='elementOnly'"
 * @generated
 */
public interface DefiningNameClass extends EObject {
	/**
	 * Returns the value of the '<em><b>Not An Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not An Element</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not An Element</em>' containment reference.
	 * @see #setNotAnElement(NotAnElement)
	 * @see Ada.AdaPackage#getDefiningNameClass_NotAnElement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not_an_element' namespace='##targetNamespace'"
	 * @generated
	 */
	NotAnElement getNotAnElement();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getNotAnElement <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not An Element</em>' containment reference.
	 * @see #getNotAnElement()
	 * @generated
	 */
	void setNotAnElement(NotAnElement value);

	/**
	 * Returns the value of the '<em><b>Defining Identifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Identifier</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Identifier</em>' containment reference.
	 * @see #setDefiningIdentifier(DefiningIdentifier)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefiningIdentifier()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_identifier' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningIdentifier getDefiningIdentifier();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefiningIdentifier <em>Defining Identifier</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Identifier</em>' containment reference.
	 * @see #getDefiningIdentifier()
	 * @generated
	 */
	void setDefiningIdentifier(DefiningIdentifier value);

	/**
	 * Returns the value of the '<em><b>Defining Character Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Character Literal</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Character Literal</em>' containment reference.
	 * @see #setDefiningCharacterLiteral(DefiningCharacterLiteral)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefiningCharacterLiteral()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_character_literal' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningCharacterLiteral getDefiningCharacterLiteral();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefiningCharacterLiteral <em>Defining Character Literal</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Character Literal</em>' containment reference.
	 * @see #getDefiningCharacterLiteral()
	 * @generated
	 */
	void setDefiningCharacterLiteral(DefiningCharacterLiteral value);

	/**
	 * Returns the value of the '<em><b>Defining Enumeration Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Enumeration Literal</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Enumeration Literal</em>' containment reference.
	 * @see #setDefiningEnumerationLiteral(DefiningEnumerationLiteral)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefiningEnumerationLiteral()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_enumeration_literal' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningEnumerationLiteral getDefiningEnumerationLiteral();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefiningEnumerationLiteral <em>Defining Enumeration Literal</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Enumeration Literal</em>' containment reference.
	 * @see #getDefiningEnumerationLiteral()
	 * @generated
	 */
	void setDefiningEnumerationLiteral(DefiningEnumerationLiteral value);

	/**
	 * Returns the value of the '<em><b>Defining And Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining And Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining And Operator</em>' containment reference.
	 * @see #setDefiningAndOperator(DefiningAndOperator)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefiningAndOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_and_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningAndOperator getDefiningAndOperator();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefiningAndOperator <em>Defining And Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining And Operator</em>' containment reference.
	 * @see #getDefiningAndOperator()
	 * @generated
	 */
	void setDefiningAndOperator(DefiningAndOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Or Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Or Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Or Operator</em>' containment reference.
	 * @see #setDefiningOrOperator(DefiningOrOperator)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefiningOrOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_or_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningOrOperator getDefiningOrOperator();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefiningOrOperator <em>Defining Or Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Or Operator</em>' containment reference.
	 * @see #getDefiningOrOperator()
	 * @generated
	 */
	void setDefiningOrOperator(DefiningOrOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Xor Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Xor Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Xor Operator</em>' containment reference.
	 * @see #setDefiningXorOperator(DefiningXorOperator)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefiningXorOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_xor_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningXorOperator getDefiningXorOperator();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefiningXorOperator <em>Defining Xor Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Xor Operator</em>' containment reference.
	 * @see #getDefiningXorOperator()
	 * @generated
	 */
	void setDefiningXorOperator(DefiningXorOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Equal Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Equal Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Equal Operator</em>' containment reference.
	 * @see #setDefiningEqualOperator(DefiningEqualOperator)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefiningEqualOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_equal_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningEqualOperator getDefiningEqualOperator();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefiningEqualOperator <em>Defining Equal Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Equal Operator</em>' containment reference.
	 * @see #getDefiningEqualOperator()
	 * @generated
	 */
	void setDefiningEqualOperator(DefiningEqualOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Not Equal Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Not Equal Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Not Equal Operator</em>' containment reference.
	 * @see #setDefiningNotEqualOperator(DefiningNotEqualOperator)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefiningNotEqualOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_not_equal_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNotEqualOperator getDefiningNotEqualOperator();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefiningNotEqualOperator <em>Defining Not Equal Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Not Equal Operator</em>' containment reference.
	 * @see #getDefiningNotEqualOperator()
	 * @generated
	 */
	void setDefiningNotEqualOperator(DefiningNotEqualOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Less Than Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Less Than Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Less Than Operator</em>' containment reference.
	 * @see #setDefiningLessThanOperator(DefiningLessThanOperator)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefiningLessThanOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_less_than_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningLessThanOperator getDefiningLessThanOperator();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefiningLessThanOperator <em>Defining Less Than Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Less Than Operator</em>' containment reference.
	 * @see #getDefiningLessThanOperator()
	 * @generated
	 */
	void setDefiningLessThanOperator(DefiningLessThanOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Less Than Or Equal Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Less Than Or Equal Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Less Than Or Equal Operator</em>' containment reference.
	 * @see #setDefiningLessThanOrEqualOperator(DefiningLessThanOrEqualOperator)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefiningLessThanOrEqualOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_less_than_or_equal_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningLessThanOrEqualOperator getDefiningLessThanOrEqualOperator();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefiningLessThanOrEqualOperator <em>Defining Less Than Or Equal Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Less Than Or Equal Operator</em>' containment reference.
	 * @see #getDefiningLessThanOrEqualOperator()
	 * @generated
	 */
	void setDefiningLessThanOrEqualOperator(DefiningLessThanOrEqualOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Greater Than Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Greater Than Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Greater Than Operator</em>' containment reference.
	 * @see #setDefiningGreaterThanOperator(DefiningGreaterThanOperator)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefiningGreaterThanOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_greater_than_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningGreaterThanOperator getDefiningGreaterThanOperator();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefiningGreaterThanOperator <em>Defining Greater Than Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Greater Than Operator</em>' containment reference.
	 * @see #getDefiningGreaterThanOperator()
	 * @generated
	 */
	void setDefiningGreaterThanOperator(DefiningGreaterThanOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Greater Than Or Equal Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Greater Than Or Equal Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Greater Than Or Equal Operator</em>' containment reference.
	 * @see #setDefiningGreaterThanOrEqualOperator(DefiningGreaterThanOrEqualOperator)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefiningGreaterThanOrEqualOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_greater_than_or_equal_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningGreaterThanOrEqualOperator getDefiningGreaterThanOrEqualOperator();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefiningGreaterThanOrEqualOperator <em>Defining Greater Than Or Equal Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Greater Than Or Equal Operator</em>' containment reference.
	 * @see #getDefiningGreaterThanOrEqualOperator()
	 * @generated
	 */
	void setDefiningGreaterThanOrEqualOperator(DefiningGreaterThanOrEqualOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Plus Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Plus Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Plus Operator</em>' containment reference.
	 * @see #setDefiningPlusOperator(DefiningPlusOperator)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefiningPlusOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_plus_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningPlusOperator getDefiningPlusOperator();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefiningPlusOperator <em>Defining Plus Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Plus Operator</em>' containment reference.
	 * @see #getDefiningPlusOperator()
	 * @generated
	 */
	void setDefiningPlusOperator(DefiningPlusOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Minus Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Minus Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Minus Operator</em>' containment reference.
	 * @see #setDefiningMinusOperator(DefiningMinusOperator)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefiningMinusOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_minus_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningMinusOperator getDefiningMinusOperator();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefiningMinusOperator <em>Defining Minus Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Minus Operator</em>' containment reference.
	 * @see #getDefiningMinusOperator()
	 * @generated
	 */
	void setDefiningMinusOperator(DefiningMinusOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Concatenate Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Concatenate Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Concatenate Operator</em>' containment reference.
	 * @see #setDefiningConcatenateOperator(DefiningConcatenateOperator)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefiningConcatenateOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_concatenate_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningConcatenateOperator getDefiningConcatenateOperator();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefiningConcatenateOperator <em>Defining Concatenate Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Concatenate Operator</em>' containment reference.
	 * @see #getDefiningConcatenateOperator()
	 * @generated
	 */
	void setDefiningConcatenateOperator(DefiningConcatenateOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Unary Plus Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Unary Plus Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Unary Plus Operator</em>' containment reference.
	 * @see #setDefiningUnaryPlusOperator(DefiningUnaryPlusOperator)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefiningUnaryPlusOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_unary_plus_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningUnaryPlusOperator getDefiningUnaryPlusOperator();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefiningUnaryPlusOperator <em>Defining Unary Plus Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Unary Plus Operator</em>' containment reference.
	 * @see #getDefiningUnaryPlusOperator()
	 * @generated
	 */
	void setDefiningUnaryPlusOperator(DefiningUnaryPlusOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Unary Minus Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Unary Minus Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Unary Minus Operator</em>' containment reference.
	 * @see #setDefiningUnaryMinusOperator(DefiningUnaryMinusOperator)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefiningUnaryMinusOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_unary_minus_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningUnaryMinusOperator getDefiningUnaryMinusOperator();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefiningUnaryMinusOperator <em>Defining Unary Minus Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Unary Minus Operator</em>' containment reference.
	 * @see #getDefiningUnaryMinusOperator()
	 * @generated
	 */
	void setDefiningUnaryMinusOperator(DefiningUnaryMinusOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Multiply Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Multiply Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Multiply Operator</em>' containment reference.
	 * @see #setDefiningMultiplyOperator(DefiningMultiplyOperator)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefiningMultiplyOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_multiply_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningMultiplyOperator getDefiningMultiplyOperator();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefiningMultiplyOperator <em>Defining Multiply Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Multiply Operator</em>' containment reference.
	 * @see #getDefiningMultiplyOperator()
	 * @generated
	 */
	void setDefiningMultiplyOperator(DefiningMultiplyOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Divide Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Divide Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Divide Operator</em>' containment reference.
	 * @see #setDefiningDivideOperator(DefiningDivideOperator)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefiningDivideOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_divide_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningDivideOperator getDefiningDivideOperator();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefiningDivideOperator <em>Defining Divide Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Divide Operator</em>' containment reference.
	 * @see #getDefiningDivideOperator()
	 * @generated
	 */
	void setDefiningDivideOperator(DefiningDivideOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Mod Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Mod Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Mod Operator</em>' containment reference.
	 * @see #setDefiningModOperator(DefiningModOperator)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefiningModOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_mod_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningModOperator getDefiningModOperator();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefiningModOperator <em>Defining Mod Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Mod Operator</em>' containment reference.
	 * @see #getDefiningModOperator()
	 * @generated
	 */
	void setDefiningModOperator(DefiningModOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Rem Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Rem Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Rem Operator</em>' containment reference.
	 * @see #setDefiningRemOperator(DefiningRemOperator)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefiningRemOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_rem_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningRemOperator getDefiningRemOperator();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefiningRemOperator <em>Defining Rem Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Rem Operator</em>' containment reference.
	 * @see #getDefiningRemOperator()
	 * @generated
	 */
	void setDefiningRemOperator(DefiningRemOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Exponentiate Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Exponentiate Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Exponentiate Operator</em>' containment reference.
	 * @see #setDefiningExponentiateOperator(DefiningExponentiateOperator)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefiningExponentiateOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_exponentiate_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningExponentiateOperator getDefiningExponentiateOperator();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefiningExponentiateOperator <em>Defining Exponentiate Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Exponentiate Operator</em>' containment reference.
	 * @see #getDefiningExponentiateOperator()
	 * @generated
	 */
	void setDefiningExponentiateOperator(DefiningExponentiateOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Abs Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Abs Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Abs Operator</em>' containment reference.
	 * @see #setDefiningAbsOperator(DefiningAbsOperator)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefiningAbsOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_abs_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningAbsOperator getDefiningAbsOperator();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefiningAbsOperator <em>Defining Abs Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Abs Operator</em>' containment reference.
	 * @see #getDefiningAbsOperator()
	 * @generated
	 */
	void setDefiningAbsOperator(DefiningAbsOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Not Operator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Not Operator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Not Operator</em>' containment reference.
	 * @see #setDefiningNotOperator(DefiningNotOperator)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefiningNotOperator()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_not_operator' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNotOperator getDefiningNotOperator();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefiningNotOperator <em>Defining Not Operator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Not Operator</em>' containment reference.
	 * @see #getDefiningNotOperator()
	 * @generated
	 */
	void setDefiningNotOperator(DefiningNotOperator value);

	/**
	 * Returns the value of the '<em><b>Defining Expanded Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Expanded Name</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Expanded Name</em>' containment reference.
	 * @see #setDefiningExpandedName(DefiningExpandedName)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefiningExpandedName()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='defining_expanded_name' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningExpandedName getDefiningExpandedName();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefiningExpandedName <em>Defining Expanded Name</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Defining Expanded Name</em>' containment reference.
	 * @see #getDefiningExpandedName()
	 * @generated
	 */
	void setDefiningExpandedName(DefiningExpandedName value);

	/**
	 * Returns the value of the '<em><b>Comment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comment</em>' containment reference.
	 * @see #setComment(Comment)
	 * @see Ada.AdaPackage#getDefiningNameClass_Comment()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='comment' namespace='##targetNamespace'"
	 * @generated
	 */
	Comment getComment();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getComment <em>Comment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comment</em>' containment reference.
	 * @see #getComment()
	 * @generated
	 */
	void setComment(Comment value);

	/**
	 * Returns the value of the '<em><b>All Calls Remote Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Calls Remote Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Calls Remote Pragma</em>' containment reference.
	 * @see #setAllCallsRemotePragma(AllCallsRemotePragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_AllCallsRemotePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='all_calls_remote_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AllCallsRemotePragma getAllCallsRemotePragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>All Calls Remote Pragma</em>' containment reference.
	 * @see #getAllCallsRemotePragma()
	 * @generated
	 */
	void setAllCallsRemotePragma(AllCallsRemotePragma value);

	/**
	 * Returns the value of the '<em><b>Asynchronous Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asynchronous Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asynchronous Pragma</em>' containment reference.
	 * @see #setAsynchronousPragma(AsynchronousPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_AsynchronousPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='asynchronous_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AsynchronousPragma getAsynchronousPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getAsynchronousPragma <em>Asynchronous Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Asynchronous Pragma</em>' containment reference.
	 * @see #getAsynchronousPragma()
	 * @generated
	 */
	void setAsynchronousPragma(AsynchronousPragma value);

	/**
	 * Returns the value of the '<em><b>Atomic Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Pragma</em>' containment reference.
	 * @see #setAtomicPragma(AtomicPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_AtomicPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='atomic_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AtomicPragma getAtomicPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getAtomicPragma <em>Atomic Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Atomic Pragma</em>' containment reference.
	 * @see #getAtomicPragma()
	 * @generated
	 */
	void setAtomicPragma(AtomicPragma value);

	/**
	 * Returns the value of the '<em><b>Atomic Components Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Components Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Components Pragma</em>' containment reference.
	 * @see #setAtomicComponentsPragma(AtomicComponentsPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_AtomicComponentsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='atomic_components_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AtomicComponentsPragma getAtomicComponentsPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Atomic Components Pragma</em>' containment reference.
	 * @see #getAtomicComponentsPragma()
	 * @generated
	 */
	void setAtomicComponentsPragma(AtomicComponentsPragma value);

	/**
	 * Returns the value of the '<em><b>Attach Handler Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attach Handler Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attach Handler Pragma</em>' containment reference.
	 * @see #setAttachHandlerPragma(AttachHandlerPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_AttachHandlerPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='attach_handler_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AttachHandlerPragma getAttachHandlerPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getAttachHandlerPragma <em>Attach Handler Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attach Handler Pragma</em>' containment reference.
	 * @see #getAttachHandlerPragma()
	 * @generated
	 */
	void setAttachHandlerPragma(AttachHandlerPragma value);

	/**
	 * Returns the value of the '<em><b>Controlled Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controlled Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controlled Pragma</em>' containment reference.
	 * @see #setControlledPragma(ControlledPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_ControlledPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='controlled_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ControlledPragma getControlledPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getControlledPragma <em>Controlled Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Controlled Pragma</em>' containment reference.
	 * @see #getControlledPragma()
	 * @generated
	 */
	void setControlledPragma(ControlledPragma value);

	/**
	 * Returns the value of the '<em><b>Convention Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Convention Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Convention Pragma</em>' containment reference.
	 * @see #setConventionPragma(ConventionPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_ConventionPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='convention_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ConventionPragma getConventionPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getConventionPragma <em>Convention Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Convention Pragma</em>' containment reference.
	 * @see #getConventionPragma()
	 * @generated
	 */
	void setConventionPragma(ConventionPragma value);

	/**
	 * Returns the value of the '<em><b>Discard Names Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discard Names Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discard Names Pragma</em>' containment reference.
	 * @see #setDiscardNamesPragma(DiscardNamesPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_DiscardNamesPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='discard_names_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscardNamesPragma getDiscardNamesPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDiscardNamesPragma <em>Discard Names Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discard Names Pragma</em>' containment reference.
	 * @see #getDiscardNamesPragma()
	 * @generated
	 */
	void setDiscardNamesPragma(DiscardNamesPragma value);

	/**
	 * Returns the value of the '<em><b>Elaborate Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Pragma</em>' containment reference.
	 * @see #setElaboratePragma(ElaboratePragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_ElaboratePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='elaborate_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ElaboratePragma getElaboratePragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getElaboratePragma <em>Elaborate Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Elaborate Pragma</em>' containment reference.
	 * @see #getElaboratePragma()
	 * @generated
	 */
	void setElaboratePragma(ElaboratePragma value);

	/**
	 * Returns the value of the '<em><b>Elaborate All Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate All Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate All Pragma</em>' containment reference.
	 * @see #setElaborateAllPragma(ElaborateAllPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_ElaborateAllPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='elaborate_all_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ElaborateAllPragma getElaborateAllPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getElaborateAllPragma <em>Elaborate All Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Elaborate All Pragma</em>' containment reference.
	 * @see #getElaborateAllPragma()
	 * @generated
	 */
	void setElaborateAllPragma(ElaborateAllPragma value);

	/**
	 * Returns the value of the '<em><b>Elaborate Body Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Body Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Body Pragma</em>' containment reference.
	 * @see #setElaborateBodyPragma(ElaborateBodyPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_ElaborateBodyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='elaborate_body_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ElaborateBodyPragma getElaborateBodyPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Elaborate Body Pragma</em>' containment reference.
	 * @see #getElaborateBodyPragma()
	 * @generated
	 */
	void setElaborateBodyPragma(ElaborateBodyPragma value);

	/**
	 * Returns the value of the '<em><b>Export Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Export Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Export Pragma</em>' containment reference.
	 * @see #setExportPragma(ExportPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_ExportPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='export_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ExportPragma getExportPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getExportPragma <em>Export Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Export Pragma</em>' containment reference.
	 * @see #getExportPragma()
	 * @generated
	 */
	void setExportPragma(ExportPragma value);

	/**
	 * Returns the value of the '<em><b>Import Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Import Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Import Pragma</em>' containment reference.
	 * @see #setImportPragma(ImportPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_ImportPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='import_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ImportPragma getImportPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getImportPragma <em>Import Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Import Pragma</em>' containment reference.
	 * @see #getImportPragma()
	 * @generated
	 */
	void setImportPragma(ImportPragma value);

	/**
	 * Returns the value of the '<em><b>Inline Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inline Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inline Pragma</em>' containment reference.
	 * @see #setInlinePragma(InlinePragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_InlinePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='inline_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	InlinePragma getInlinePragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getInlinePragma <em>Inline Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inline Pragma</em>' containment reference.
	 * @see #getInlinePragma()
	 * @generated
	 */
	void setInlinePragma(InlinePragma value);

	/**
	 * Returns the value of the '<em><b>Inspection Point Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inspection Point Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inspection Point Pragma</em>' containment reference.
	 * @see #setInspectionPointPragma(InspectionPointPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_InspectionPointPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='inspection_point_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	InspectionPointPragma getInspectionPointPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getInspectionPointPragma <em>Inspection Point Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inspection Point Pragma</em>' containment reference.
	 * @see #getInspectionPointPragma()
	 * @generated
	 */
	void setInspectionPointPragma(InspectionPointPragma value);

	/**
	 * Returns the value of the '<em><b>Interrupt Handler Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Handler Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Handler Pragma</em>' containment reference.
	 * @see #setInterruptHandlerPragma(InterruptHandlerPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_InterruptHandlerPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='interrupt_handler_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	InterruptHandlerPragma getInterruptHandlerPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interrupt Handler Pragma</em>' containment reference.
	 * @see #getInterruptHandlerPragma()
	 * @generated
	 */
	void setInterruptHandlerPragma(InterruptHandlerPragma value);

	/**
	 * Returns the value of the '<em><b>Interrupt Priority Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Priority Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Priority Pragma</em>' containment reference.
	 * @see #setInterruptPriorityPragma(InterruptPriorityPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_InterruptPriorityPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='interrupt_priority_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	InterruptPriorityPragma getInterruptPriorityPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interrupt Priority Pragma</em>' containment reference.
	 * @see #getInterruptPriorityPragma()
	 * @generated
	 */
	void setInterruptPriorityPragma(InterruptPriorityPragma value);

	/**
	 * Returns the value of the '<em><b>Linker Options Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Linker Options Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Linker Options Pragma</em>' containment reference.
	 * @see #setLinkerOptionsPragma(LinkerOptionsPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_LinkerOptionsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='linker_options_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	LinkerOptionsPragma getLinkerOptionsPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getLinkerOptionsPragma <em>Linker Options Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Linker Options Pragma</em>' containment reference.
	 * @see #getLinkerOptionsPragma()
	 * @generated
	 */
	void setLinkerOptionsPragma(LinkerOptionsPragma value);

	/**
	 * Returns the value of the '<em><b>List Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Pragma</em>' containment reference.
	 * @see #setListPragma(ListPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_ListPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='list_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ListPragma getListPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getListPragma <em>List Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>List Pragma</em>' containment reference.
	 * @see #getListPragma()
	 * @generated
	 */
	void setListPragma(ListPragma value);

	/**
	 * Returns the value of the '<em><b>Locking Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Locking Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Locking Policy Pragma</em>' containment reference.
	 * @see #setLockingPolicyPragma(LockingPolicyPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_LockingPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='locking_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	LockingPolicyPragma getLockingPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getLockingPolicyPragma <em>Locking Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Locking Policy Pragma</em>' containment reference.
	 * @see #getLockingPolicyPragma()
	 * @generated
	 */
	void setLockingPolicyPragma(LockingPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Normalize Scalars Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Normalize Scalars Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Normalize Scalars Pragma</em>' containment reference.
	 * @see #setNormalizeScalarsPragma(NormalizeScalarsPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_NormalizeScalarsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='normalize_scalars_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	NormalizeScalarsPragma getNormalizeScalarsPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Normalize Scalars Pragma</em>' containment reference.
	 * @see #getNormalizeScalarsPragma()
	 * @generated
	 */
	void setNormalizeScalarsPragma(NormalizeScalarsPragma value);

	/**
	 * Returns the value of the '<em><b>Optimize Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Optimize Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Optimize Pragma</em>' containment reference.
	 * @see #setOptimizePragma(OptimizePragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_OptimizePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='optimize_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	OptimizePragma getOptimizePragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getOptimizePragma <em>Optimize Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Optimize Pragma</em>' containment reference.
	 * @see #getOptimizePragma()
	 * @generated
	 */
	void setOptimizePragma(OptimizePragma value);

	/**
	 * Returns the value of the '<em><b>Pack Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pack Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pack Pragma</em>' containment reference.
	 * @see #setPackPragma(PackPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_PackPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='pack_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PackPragma getPackPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getPackPragma <em>Pack Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pack Pragma</em>' containment reference.
	 * @see #getPackPragma()
	 * @generated
	 */
	void setPackPragma(PackPragma value);

	/**
	 * Returns the value of the '<em><b>Page Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Page Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Page Pragma</em>' containment reference.
	 * @see #setPagePragma(PagePragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_PagePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='page_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PagePragma getPagePragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getPagePragma <em>Page Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Page Pragma</em>' containment reference.
	 * @see #getPagePragma()
	 * @generated
	 */
	void setPagePragma(PagePragma value);

	/**
	 * Returns the value of the '<em><b>Preelaborate Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborate Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborate Pragma</em>' containment reference.
	 * @see #setPreelaboratePragma(PreelaboratePragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_PreelaboratePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='preelaborate_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PreelaboratePragma getPreelaboratePragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getPreelaboratePragma <em>Preelaborate Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preelaborate Pragma</em>' containment reference.
	 * @see #getPreelaboratePragma()
	 * @generated
	 */
	void setPreelaboratePragma(PreelaboratePragma value);

	/**
	 * Returns the value of the '<em><b>Priority Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Pragma</em>' containment reference.
	 * @see #setPriorityPragma(PriorityPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_PriorityPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='priority_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PriorityPragma getPriorityPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getPriorityPragma <em>Priority Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Priority Pragma</em>' containment reference.
	 * @see #getPriorityPragma()
	 * @generated
	 */
	void setPriorityPragma(PriorityPragma value);

	/**
	 * Returns the value of the '<em><b>Pure Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pure Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pure Pragma</em>' containment reference.
	 * @see #setPurePragma(PurePragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_PurePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='pure_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PurePragma getPurePragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getPurePragma <em>Pure Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pure Pragma</em>' containment reference.
	 * @see #getPurePragma()
	 * @generated
	 */
	void setPurePragma(PurePragma value);

	/**
	 * Returns the value of the '<em><b>Queuing Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Queuing Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Queuing Policy Pragma</em>' containment reference.
	 * @see #setQueuingPolicyPragma(QueuingPolicyPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_QueuingPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='queuing_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	QueuingPolicyPragma getQueuingPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Queuing Policy Pragma</em>' containment reference.
	 * @see #getQueuingPolicyPragma()
	 * @generated
	 */
	void setQueuingPolicyPragma(QueuingPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Remote Call Interface Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Call Interface Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Call Interface Pragma</em>' containment reference.
	 * @see #setRemoteCallInterfacePragma(RemoteCallInterfacePragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_RemoteCallInterfacePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='remote_call_interface_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	RemoteCallInterfacePragma getRemoteCallInterfacePragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Remote Call Interface Pragma</em>' containment reference.
	 * @see #getRemoteCallInterfacePragma()
	 * @generated
	 */
	void setRemoteCallInterfacePragma(RemoteCallInterfacePragma value);

	/**
	 * Returns the value of the '<em><b>Remote Types Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Types Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Types Pragma</em>' containment reference.
	 * @see #setRemoteTypesPragma(RemoteTypesPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_RemoteTypesPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='remote_types_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	RemoteTypesPragma getRemoteTypesPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getRemoteTypesPragma <em>Remote Types Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Remote Types Pragma</em>' containment reference.
	 * @see #getRemoteTypesPragma()
	 * @generated
	 */
	void setRemoteTypesPragma(RemoteTypesPragma value);

	/**
	 * Returns the value of the '<em><b>Restrictions Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Restrictions Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Restrictions Pragma</em>' containment reference.
	 * @see #setRestrictionsPragma(RestrictionsPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_RestrictionsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='restrictions_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	RestrictionsPragma getRestrictionsPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getRestrictionsPragma <em>Restrictions Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Restrictions Pragma</em>' containment reference.
	 * @see #getRestrictionsPragma()
	 * @generated
	 */
	void setRestrictionsPragma(RestrictionsPragma value);

	/**
	 * Returns the value of the '<em><b>Reviewable Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reviewable Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reviewable Pragma</em>' containment reference.
	 * @see #setReviewablePragma(ReviewablePragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_ReviewablePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='reviewable_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ReviewablePragma getReviewablePragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getReviewablePragma <em>Reviewable Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reviewable Pragma</em>' containment reference.
	 * @see #getReviewablePragma()
	 * @generated
	 */
	void setReviewablePragma(ReviewablePragma value);

	/**
	 * Returns the value of the '<em><b>Shared Passive Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shared Passive Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shared Passive Pragma</em>' containment reference.
	 * @see #setSharedPassivePragma(SharedPassivePragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_SharedPassivePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='shared_passive_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	SharedPassivePragma getSharedPassivePragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getSharedPassivePragma <em>Shared Passive Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Shared Passive Pragma</em>' containment reference.
	 * @see #getSharedPassivePragma()
	 * @generated
	 */
	void setSharedPassivePragma(SharedPassivePragma value);

	/**
	 * Returns the value of the '<em><b>Storage Size Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Size Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Size Pragma</em>' containment reference.
	 * @see #setStorageSizePragma(StorageSizePragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_StorageSizePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='storage_size_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	StorageSizePragma getStorageSizePragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getStorageSizePragma <em>Storage Size Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Storage Size Pragma</em>' containment reference.
	 * @see #getStorageSizePragma()
	 * @generated
	 */
	void setStorageSizePragma(StorageSizePragma value);

	/**
	 * Returns the value of the '<em><b>Suppress Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Suppress Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Suppress Pragma</em>' containment reference.
	 * @see #setSuppressPragma(SuppressPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_SuppressPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='suppress_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	SuppressPragma getSuppressPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getSuppressPragma <em>Suppress Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Suppress Pragma</em>' containment reference.
	 * @see #getSuppressPragma()
	 * @generated
	 */
	void setSuppressPragma(SuppressPragma value);

	/**
	 * Returns the value of the '<em><b>Task Dispatching Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Dispatching Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Dispatching Policy Pragma</em>' containment reference.
	 * @see #setTaskDispatchingPolicyPragma(TaskDispatchingPolicyPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_TaskDispatchingPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='task_dispatching_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	TaskDispatchingPolicyPragma getTaskDispatchingPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Dispatching Policy Pragma</em>' containment reference.
	 * @see #getTaskDispatchingPolicyPragma()
	 * @generated
	 */
	void setTaskDispatchingPolicyPragma(TaskDispatchingPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Volatile Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Pragma</em>' containment reference.
	 * @see #setVolatilePragma(VolatilePragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_VolatilePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='volatile_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	VolatilePragma getVolatilePragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getVolatilePragma <em>Volatile Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Volatile Pragma</em>' containment reference.
	 * @see #getVolatilePragma()
	 * @generated
	 */
	void setVolatilePragma(VolatilePragma value);

	/**
	 * Returns the value of the '<em><b>Volatile Components Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Components Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Components Pragma</em>' containment reference.
	 * @see #setVolatileComponentsPragma(VolatileComponentsPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_VolatileComponentsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='volatile_components_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	VolatileComponentsPragma getVolatileComponentsPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Volatile Components Pragma</em>' containment reference.
	 * @see #getVolatileComponentsPragma()
	 * @generated
	 */
	void setVolatileComponentsPragma(VolatileComponentsPragma value);

	/**
	 * Returns the value of the '<em><b>Assert Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assert Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assert Pragma</em>' containment reference.
	 * @see #setAssertPragma(AssertPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_AssertPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='assert_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AssertPragma getAssertPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getAssertPragma <em>Assert Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assert Pragma</em>' containment reference.
	 * @see #getAssertPragma()
	 * @generated
	 */
	void setAssertPragma(AssertPragma value);

	/**
	 * Returns the value of the '<em><b>Assertion Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assertion Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assertion Policy Pragma</em>' containment reference.
	 * @see #setAssertionPolicyPragma(AssertionPolicyPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_AssertionPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='assertion_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AssertionPolicyPragma getAssertionPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assertion Policy Pragma</em>' containment reference.
	 * @see #getAssertionPolicyPragma()
	 * @generated
	 */
	void setAssertionPolicyPragma(AssertionPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Detect Blocking Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Detect Blocking Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Detect Blocking Pragma</em>' containment reference.
	 * @see #setDetectBlockingPragma(DetectBlockingPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_DetectBlockingPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='detect_blocking_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	DetectBlockingPragma getDetectBlockingPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Detect Blocking Pragma</em>' containment reference.
	 * @see #getDetectBlockingPragma()
	 * @generated
	 */
	void setDetectBlockingPragma(DetectBlockingPragma value);

	/**
	 * Returns the value of the '<em><b>No Return Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>No Return Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>No Return Pragma</em>' containment reference.
	 * @see #setNoReturnPragma(NoReturnPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_NoReturnPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='no_return_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	NoReturnPragma getNoReturnPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getNoReturnPragma <em>No Return Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>No Return Pragma</em>' containment reference.
	 * @see #getNoReturnPragma()
	 * @generated
	 */
	void setNoReturnPragma(NoReturnPragma value);

	/**
	 * Returns the value of the '<em><b>Partition Elaboration Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partition Elaboration Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partition Elaboration Policy Pragma</em>' containment reference.
	 * @see #setPartitionElaborationPolicyPragma(PartitionElaborationPolicyPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_PartitionElaborationPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='partition_elaboration_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PartitionElaborationPolicyPragma getPartitionElaborationPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Partition Elaboration Policy Pragma</em>' containment reference.
	 * @see #getPartitionElaborationPolicyPragma()
	 * @generated
	 */
	void setPartitionElaborationPolicyPragma(PartitionElaborationPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Preelaborable Initialization Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborable Initialization Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborable Initialization Pragma</em>' containment reference.
	 * @see #setPreelaborableInitializationPragma(PreelaborableInitializationPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_PreelaborableInitializationPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='preelaborable_initialization_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PreelaborableInitializationPragma getPreelaborableInitializationPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preelaborable Initialization Pragma</em>' containment reference.
	 * @see #getPreelaborableInitializationPragma()
	 * @generated
	 */
	void setPreelaborableInitializationPragma(PreelaborableInitializationPragma value);

	/**
	 * Returns the value of the '<em><b>Priority Specific Dispatching Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Specific Dispatching Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Specific Dispatching Pragma</em>' containment reference.
	 * @see #setPrioritySpecificDispatchingPragma(PrioritySpecificDispatchingPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_PrioritySpecificDispatchingPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='priority_specific_dispatching_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PrioritySpecificDispatchingPragma getPrioritySpecificDispatchingPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Priority Specific Dispatching Pragma</em>' containment reference.
	 * @see #getPrioritySpecificDispatchingPragma()
	 * @generated
	 */
	void setPrioritySpecificDispatchingPragma(PrioritySpecificDispatchingPragma value);

	/**
	 * Returns the value of the '<em><b>Profile Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Profile Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Profile Pragma</em>' containment reference.
	 * @see #setProfilePragma(ProfilePragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_ProfilePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='profile_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ProfilePragma getProfilePragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getProfilePragma <em>Profile Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Profile Pragma</em>' containment reference.
	 * @see #getProfilePragma()
	 * @generated
	 */
	void setProfilePragma(ProfilePragma value);

	/**
	 * Returns the value of the '<em><b>Relative Deadline Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relative Deadline Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relative Deadline Pragma</em>' containment reference.
	 * @see #setRelativeDeadlinePragma(RelativeDeadlinePragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_RelativeDeadlinePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='relative_deadline_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	RelativeDeadlinePragma getRelativeDeadlinePragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relative Deadline Pragma</em>' containment reference.
	 * @see #getRelativeDeadlinePragma()
	 * @generated
	 */
	void setRelativeDeadlinePragma(RelativeDeadlinePragma value);

	/**
	 * Returns the value of the '<em><b>Unchecked Union Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unchecked Union Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unchecked Union Pragma</em>' containment reference.
	 * @see #setUncheckedUnionPragma(UncheckedUnionPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_UncheckedUnionPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unchecked_union_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	UncheckedUnionPragma getUncheckedUnionPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unchecked Union Pragma</em>' containment reference.
	 * @see #getUncheckedUnionPragma()
	 * @generated
	 */
	void setUncheckedUnionPragma(UncheckedUnionPragma value);

	/**
	 * Returns the value of the '<em><b>Unsuppress Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unsuppress Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unsuppress Pragma</em>' containment reference.
	 * @see #setUnsuppressPragma(UnsuppressPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_UnsuppressPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unsuppress_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	UnsuppressPragma getUnsuppressPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getUnsuppressPragma <em>Unsuppress Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unsuppress Pragma</em>' containment reference.
	 * @see #getUnsuppressPragma()
	 * @generated
	 */
	void setUnsuppressPragma(UnsuppressPragma value);

	/**
	 * Returns the value of the '<em><b>Default Storage Pool Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Storage Pool Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Storage Pool Pragma</em>' containment reference.
	 * @see #setDefaultStoragePoolPragma(DefaultStoragePoolPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_DefaultStoragePoolPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='default_storage_pool_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	DefaultStoragePoolPragma getDefaultStoragePoolPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Storage Pool Pragma</em>' containment reference.
	 * @see #getDefaultStoragePoolPragma()
	 * @generated
	 */
	void setDefaultStoragePoolPragma(DefaultStoragePoolPragma value);

	/**
	 * Returns the value of the '<em><b>Dispatching Domain Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dispatching Domain Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dispatching Domain Pragma</em>' containment reference.
	 * @see #setDispatchingDomainPragma(DispatchingDomainPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_DispatchingDomainPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='dispatching_domain_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	DispatchingDomainPragma getDispatchingDomainPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dispatching Domain Pragma</em>' containment reference.
	 * @see #getDispatchingDomainPragma()
	 * @generated
	 */
	void setDispatchingDomainPragma(DispatchingDomainPragma value);

	/**
	 * Returns the value of the '<em><b>Cpu Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cpu Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cpu Pragma</em>' containment reference.
	 * @see #setCpuPragma(CpuPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_CpuPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='cpu_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	CpuPragma getCpuPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getCpuPragma <em>Cpu Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cpu Pragma</em>' containment reference.
	 * @see #getCpuPragma()
	 * @generated
	 */
	void setCpuPragma(CpuPragma value);

	/**
	 * Returns the value of the '<em><b>Independent Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Pragma</em>' containment reference.
	 * @see #setIndependentPragma(IndependentPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_IndependentPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='independent_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	IndependentPragma getIndependentPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getIndependentPragma <em>Independent Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Independent Pragma</em>' containment reference.
	 * @see #getIndependentPragma()
	 * @generated
	 */
	void setIndependentPragma(IndependentPragma value);

	/**
	 * Returns the value of the '<em><b>Independent Components Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Components Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Components Pragma</em>' containment reference.
	 * @see #setIndependentComponentsPragma(IndependentComponentsPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_IndependentComponentsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='independent_components_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	IndependentComponentsPragma getIndependentComponentsPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getIndependentComponentsPragma <em>Independent Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Independent Components Pragma</em>' containment reference.
	 * @see #getIndependentComponentsPragma()
	 * @generated
	 */
	void setIndependentComponentsPragma(IndependentComponentsPragma value);

	/**
	 * Returns the value of the '<em><b>Implementation Defined Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implementation Defined Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implementation Defined Pragma</em>' containment reference.
	 * @see #setImplementationDefinedPragma(ImplementationDefinedPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_ImplementationDefinedPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='implementation_defined_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ImplementationDefinedPragma getImplementationDefinedPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Implementation Defined Pragma</em>' containment reference.
	 * @see #getImplementationDefinedPragma()
	 * @generated
	 */
	void setImplementationDefinedPragma(ImplementationDefinedPragma value);

	/**
	 * Returns the value of the '<em><b>Unknown Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unknown Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unknown Pragma</em>' containment reference.
	 * @see #setUnknownPragma(UnknownPragma)
	 * @see Ada.AdaPackage#getDefiningNameClass_UnknownPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unknown_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	UnknownPragma getUnknownPragma();

	/**
	 * Sets the value of the '{@link Ada.DefiningNameClass#getUnknownPragma <em>Unknown Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unknown Pragma</em>' containment reference.
	 * @see #getUnknownPragma()
	 * @generated
	 */
	void setUnknownPragma(UnknownPragma value);

} // DefiningNameClass
