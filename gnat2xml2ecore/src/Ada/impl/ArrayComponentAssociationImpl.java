/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.ArrayComponentAssociation;
import Ada.ElementList;
import Ada.ExpressionClass;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Array Component Association</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.ArrayComponentAssociationImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.ArrayComponentAssociationImpl#getArrayComponentChoicesQl <em>Array Component Choices Ql</em>}</li>
 *   <li>{@link Ada.impl.ArrayComponentAssociationImpl#getComponentExpressionQ <em>Component Expression Q</em>}</li>
 *   <li>{@link Ada.impl.ArrayComponentAssociationImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ArrayComponentAssociationImpl extends MinimalEObjectImpl.Container implements ArrayComponentAssociation {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getArrayComponentChoicesQl() <em>Array Component Choices Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getArrayComponentChoicesQl()
	 * @generated
	 * @ordered
	 */
	protected ElementList arrayComponentChoicesQl;

	/**
	 * The cached value of the '{@link #getComponentExpressionQ() <em>Component Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentExpressionQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass componentExpressionQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArrayComponentAssociationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getArrayComponentAssociation();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ARRAY_COMPONENT_ASSOCIATION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ARRAY_COMPONENT_ASSOCIATION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ARRAY_COMPONENT_ASSOCIATION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ARRAY_COMPONENT_ASSOCIATION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementList getArrayComponentChoicesQl() {
		return arrayComponentChoicesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetArrayComponentChoicesQl(ElementList newArrayComponentChoicesQl, NotificationChain msgs) {
		ElementList oldArrayComponentChoicesQl = arrayComponentChoicesQl;
		arrayComponentChoicesQl = newArrayComponentChoicesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ARRAY_COMPONENT_ASSOCIATION__ARRAY_COMPONENT_CHOICES_QL, oldArrayComponentChoicesQl, newArrayComponentChoicesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setArrayComponentChoicesQl(ElementList newArrayComponentChoicesQl) {
		if (newArrayComponentChoicesQl != arrayComponentChoicesQl) {
			NotificationChain msgs = null;
			if (arrayComponentChoicesQl != null)
				msgs = ((InternalEObject)arrayComponentChoicesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ARRAY_COMPONENT_ASSOCIATION__ARRAY_COMPONENT_CHOICES_QL, null, msgs);
			if (newArrayComponentChoicesQl != null)
				msgs = ((InternalEObject)newArrayComponentChoicesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ARRAY_COMPONENT_ASSOCIATION__ARRAY_COMPONENT_CHOICES_QL, null, msgs);
			msgs = basicSetArrayComponentChoicesQl(newArrayComponentChoicesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ARRAY_COMPONENT_ASSOCIATION__ARRAY_COMPONENT_CHOICES_QL, newArrayComponentChoicesQl, newArrayComponentChoicesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getComponentExpressionQ() {
		return componentExpressionQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComponentExpressionQ(ExpressionClass newComponentExpressionQ, NotificationChain msgs) {
		ExpressionClass oldComponentExpressionQ = componentExpressionQ;
		componentExpressionQ = newComponentExpressionQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ARRAY_COMPONENT_ASSOCIATION__COMPONENT_EXPRESSION_Q, oldComponentExpressionQ, newComponentExpressionQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponentExpressionQ(ExpressionClass newComponentExpressionQ) {
		if (newComponentExpressionQ != componentExpressionQ) {
			NotificationChain msgs = null;
			if (componentExpressionQ != null)
				msgs = ((InternalEObject)componentExpressionQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ARRAY_COMPONENT_ASSOCIATION__COMPONENT_EXPRESSION_Q, null, msgs);
			if (newComponentExpressionQ != null)
				msgs = ((InternalEObject)newComponentExpressionQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ARRAY_COMPONENT_ASSOCIATION__COMPONENT_EXPRESSION_Q, null, msgs);
			msgs = basicSetComponentExpressionQ(newComponentExpressionQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ARRAY_COMPONENT_ASSOCIATION__COMPONENT_EXPRESSION_Q, newComponentExpressionQ, newComponentExpressionQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ARRAY_COMPONENT_ASSOCIATION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.ARRAY_COMPONENT_ASSOCIATION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.ARRAY_COMPONENT_ASSOCIATION__ARRAY_COMPONENT_CHOICES_QL:
				return basicSetArrayComponentChoicesQl(null, msgs);
			case AdaPackage.ARRAY_COMPONENT_ASSOCIATION__COMPONENT_EXPRESSION_Q:
				return basicSetComponentExpressionQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.ARRAY_COMPONENT_ASSOCIATION__SLOC:
				return getSloc();
			case AdaPackage.ARRAY_COMPONENT_ASSOCIATION__ARRAY_COMPONENT_CHOICES_QL:
				return getArrayComponentChoicesQl();
			case AdaPackage.ARRAY_COMPONENT_ASSOCIATION__COMPONENT_EXPRESSION_Q:
				return getComponentExpressionQ();
			case AdaPackage.ARRAY_COMPONENT_ASSOCIATION__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.ARRAY_COMPONENT_ASSOCIATION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.ARRAY_COMPONENT_ASSOCIATION__ARRAY_COMPONENT_CHOICES_QL:
				setArrayComponentChoicesQl((ElementList)newValue);
				return;
			case AdaPackage.ARRAY_COMPONENT_ASSOCIATION__COMPONENT_EXPRESSION_Q:
				setComponentExpressionQ((ExpressionClass)newValue);
				return;
			case AdaPackage.ARRAY_COMPONENT_ASSOCIATION__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.ARRAY_COMPONENT_ASSOCIATION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.ARRAY_COMPONENT_ASSOCIATION__ARRAY_COMPONENT_CHOICES_QL:
				setArrayComponentChoicesQl((ElementList)null);
				return;
			case AdaPackage.ARRAY_COMPONENT_ASSOCIATION__COMPONENT_EXPRESSION_Q:
				setComponentExpressionQ((ExpressionClass)null);
				return;
			case AdaPackage.ARRAY_COMPONENT_ASSOCIATION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.ARRAY_COMPONENT_ASSOCIATION__SLOC:
				return sloc != null;
			case AdaPackage.ARRAY_COMPONENT_ASSOCIATION__ARRAY_COMPONENT_CHOICES_QL:
				return arrayComponentChoicesQl != null;
			case AdaPackage.ARRAY_COMPONENT_ASSOCIATION__COMPONENT_EXPRESSION_Q:
				return componentExpressionQ != null;
			case AdaPackage.ARRAY_COMPONENT_ASSOCIATION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //ArrayComponentAssociationImpl
