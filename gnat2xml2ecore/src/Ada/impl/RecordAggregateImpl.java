/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.AssociationList;
import Ada.RecordAggregate;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Record Aggregate</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.RecordAggregateImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.RecordAggregateImpl#getRecordComponentAssociationsQl <em>Record Component Associations Ql</em>}</li>
 *   <li>{@link Ada.impl.RecordAggregateImpl#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.impl.RecordAggregateImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RecordAggregateImpl extends MinimalEObjectImpl.Container implements RecordAggregate {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getRecordComponentAssociationsQl() <em>Record Component Associations Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRecordComponentAssociationsQl()
	 * @generated
	 * @ordered
	 */
	protected AssociationList recordComponentAssociationsQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RecordAggregateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getRecordAggregate();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.RECORD_AGGREGATE__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.RECORD_AGGREGATE__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.RECORD_AGGREGATE__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.RECORD_AGGREGATE__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationList getRecordComponentAssociationsQl() {
		return recordComponentAssociationsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRecordComponentAssociationsQl(AssociationList newRecordComponentAssociationsQl, NotificationChain msgs) {
		AssociationList oldRecordComponentAssociationsQl = recordComponentAssociationsQl;
		recordComponentAssociationsQl = newRecordComponentAssociationsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.RECORD_AGGREGATE__RECORD_COMPONENT_ASSOCIATIONS_QL, oldRecordComponentAssociationsQl, newRecordComponentAssociationsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRecordComponentAssociationsQl(AssociationList newRecordComponentAssociationsQl) {
		if (newRecordComponentAssociationsQl != recordComponentAssociationsQl) {
			NotificationChain msgs = null;
			if (recordComponentAssociationsQl != null)
				msgs = ((InternalEObject)recordComponentAssociationsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.RECORD_AGGREGATE__RECORD_COMPONENT_ASSOCIATIONS_QL, null, msgs);
			if (newRecordComponentAssociationsQl != null)
				msgs = ((InternalEObject)newRecordComponentAssociationsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.RECORD_AGGREGATE__RECORD_COMPONENT_ASSOCIATIONS_QL, null, msgs);
			msgs = basicSetRecordComponentAssociationsQl(newRecordComponentAssociationsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.RECORD_AGGREGATE__RECORD_COMPONENT_ASSOCIATIONS_QL, newRecordComponentAssociationsQl, newRecordComponentAssociationsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.RECORD_AGGREGATE__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.RECORD_AGGREGATE__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.RECORD_AGGREGATE__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.RECORD_AGGREGATE__RECORD_COMPONENT_ASSOCIATIONS_QL:
				return basicSetRecordComponentAssociationsQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.RECORD_AGGREGATE__SLOC:
				return getSloc();
			case AdaPackage.RECORD_AGGREGATE__RECORD_COMPONENT_ASSOCIATIONS_QL:
				return getRecordComponentAssociationsQl();
			case AdaPackage.RECORD_AGGREGATE__CHECKS:
				return getChecks();
			case AdaPackage.RECORD_AGGREGATE__TYPE:
				return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.RECORD_AGGREGATE__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.RECORD_AGGREGATE__RECORD_COMPONENT_ASSOCIATIONS_QL:
				setRecordComponentAssociationsQl((AssociationList)newValue);
				return;
			case AdaPackage.RECORD_AGGREGATE__CHECKS:
				setChecks((String)newValue);
				return;
			case AdaPackage.RECORD_AGGREGATE__TYPE:
				setType((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.RECORD_AGGREGATE__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.RECORD_AGGREGATE__RECORD_COMPONENT_ASSOCIATIONS_QL:
				setRecordComponentAssociationsQl((AssociationList)null);
				return;
			case AdaPackage.RECORD_AGGREGATE__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
			case AdaPackage.RECORD_AGGREGATE__TYPE:
				setType(TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.RECORD_AGGREGATE__SLOC:
				return sloc != null;
			case AdaPackage.RECORD_AGGREGATE__RECORD_COMPONENT_ASSOCIATIONS_QL:
				return recordComponentAssociationsQl != null;
			case AdaPackage.RECORD_AGGREGATE__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
			case AdaPackage.RECORD_AGGREGATE__TYPE:
				return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(", type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //RecordAggregateImpl
