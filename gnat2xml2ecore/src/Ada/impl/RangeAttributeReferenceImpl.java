/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.ExpressionClass;
import Ada.RangeAttributeReference;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Range Attribute Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.RangeAttributeReferenceImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.RangeAttributeReferenceImpl#getRangeAttributeQ <em>Range Attribute Q</em>}</li>
 *   <li>{@link Ada.impl.RangeAttributeReferenceImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RangeAttributeReferenceImpl extends MinimalEObjectImpl.Container implements RangeAttributeReference {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getRangeAttributeQ() <em>Range Attribute Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRangeAttributeQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass rangeAttributeQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RangeAttributeReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getRangeAttributeReference();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.RANGE_ATTRIBUTE_REFERENCE__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.RANGE_ATTRIBUTE_REFERENCE__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.RANGE_ATTRIBUTE_REFERENCE__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.RANGE_ATTRIBUTE_REFERENCE__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getRangeAttributeQ() {
		return rangeAttributeQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRangeAttributeQ(ExpressionClass newRangeAttributeQ, NotificationChain msgs) {
		ExpressionClass oldRangeAttributeQ = rangeAttributeQ;
		rangeAttributeQ = newRangeAttributeQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.RANGE_ATTRIBUTE_REFERENCE__RANGE_ATTRIBUTE_Q, oldRangeAttributeQ, newRangeAttributeQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRangeAttributeQ(ExpressionClass newRangeAttributeQ) {
		if (newRangeAttributeQ != rangeAttributeQ) {
			NotificationChain msgs = null;
			if (rangeAttributeQ != null)
				msgs = ((InternalEObject)rangeAttributeQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.RANGE_ATTRIBUTE_REFERENCE__RANGE_ATTRIBUTE_Q, null, msgs);
			if (newRangeAttributeQ != null)
				msgs = ((InternalEObject)newRangeAttributeQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.RANGE_ATTRIBUTE_REFERENCE__RANGE_ATTRIBUTE_Q, null, msgs);
			msgs = basicSetRangeAttributeQ(newRangeAttributeQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.RANGE_ATTRIBUTE_REFERENCE__RANGE_ATTRIBUTE_Q, newRangeAttributeQ, newRangeAttributeQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.RANGE_ATTRIBUTE_REFERENCE__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.RANGE_ATTRIBUTE_REFERENCE__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.RANGE_ATTRIBUTE_REFERENCE__RANGE_ATTRIBUTE_Q:
				return basicSetRangeAttributeQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.RANGE_ATTRIBUTE_REFERENCE__SLOC:
				return getSloc();
			case AdaPackage.RANGE_ATTRIBUTE_REFERENCE__RANGE_ATTRIBUTE_Q:
				return getRangeAttributeQ();
			case AdaPackage.RANGE_ATTRIBUTE_REFERENCE__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.RANGE_ATTRIBUTE_REFERENCE__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.RANGE_ATTRIBUTE_REFERENCE__RANGE_ATTRIBUTE_Q:
				setRangeAttributeQ((ExpressionClass)newValue);
				return;
			case AdaPackage.RANGE_ATTRIBUTE_REFERENCE__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.RANGE_ATTRIBUTE_REFERENCE__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.RANGE_ATTRIBUTE_REFERENCE__RANGE_ATTRIBUTE_Q:
				setRangeAttributeQ((ExpressionClass)null);
				return;
			case AdaPackage.RANGE_ATTRIBUTE_REFERENCE__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.RANGE_ATTRIBUTE_REFERENCE__SLOC:
				return sloc != null;
			case AdaPackage.RANGE_ATTRIBUTE_REFERENCE__RANGE_ATTRIBUTE_Q:
				return rangeAttributeQ != null;
			case AdaPackage.RANGE_ATTRIBUTE_REFERENCE__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //RangeAttributeReferenceImpl
