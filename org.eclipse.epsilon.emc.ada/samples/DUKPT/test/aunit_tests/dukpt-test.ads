with AUnit;
with AUnit.Test_Fixtures;

package DUKPT.Test is

   type Initial_Test_Data is new AUnit.Test_Fixtures.Test_Fixture with record
      BDK  : B_Block128;
      IKSN : B_Block128;
   end record;

   procedure Set_Up (T : in out Initial_Test_Data);

   procedure Test_Derive_IPEK_001 (T : in out Initial_Test_Data);
   procedure Test_DUKPT_Key_1 (T : in out Initial_Test_Data);
   procedure Test_DUKPT_Key_2 (T : in out Initial_Test_Data);
   procedure Test_DUKPT_Key_10 (T : in out Initial_Test_Data);
   procedure Test_DUKPT_Key_2097151 (T : in out Initial_Test_Data);

end DUKPT.Test;
