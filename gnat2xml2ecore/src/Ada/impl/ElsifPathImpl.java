/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.ElsifPath;
import Ada.ExpressionClass;
import Ada.SourceLocation;
import Ada.StatementList;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Elsif Path</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.ElsifPathImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.ElsifPathImpl#getConditionExpressionQ <em>Condition Expression Q</em>}</li>
 *   <li>{@link Ada.impl.ElsifPathImpl#getSequenceOfStatementsQl <em>Sequence Of Statements Ql</em>}</li>
 *   <li>{@link Ada.impl.ElsifPathImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ElsifPathImpl extends MinimalEObjectImpl.Container implements ElsifPath {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getConditionExpressionQ() <em>Condition Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConditionExpressionQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass conditionExpressionQ;

	/**
	 * The cached value of the '{@link #getSequenceOfStatementsQl() <em>Sequence Of Statements Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSequenceOfStatementsQl()
	 * @generated
	 * @ordered
	 */
	protected StatementList sequenceOfStatementsQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ElsifPathImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getElsifPath();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ELSIF_PATH__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ELSIF_PATH__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ELSIF_PATH__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ELSIF_PATH__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getConditionExpressionQ() {
		return conditionExpressionQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConditionExpressionQ(ExpressionClass newConditionExpressionQ, NotificationChain msgs) {
		ExpressionClass oldConditionExpressionQ = conditionExpressionQ;
		conditionExpressionQ = newConditionExpressionQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ELSIF_PATH__CONDITION_EXPRESSION_Q, oldConditionExpressionQ, newConditionExpressionQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConditionExpressionQ(ExpressionClass newConditionExpressionQ) {
		if (newConditionExpressionQ != conditionExpressionQ) {
			NotificationChain msgs = null;
			if (conditionExpressionQ != null)
				msgs = ((InternalEObject)conditionExpressionQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ELSIF_PATH__CONDITION_EXPRESSION_Q, null, msgs);
			if (newConditionExpressionQ != null)
				msgs = ((InternalEObject)newConditionExpressionQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ELSIF_PATH__CONDITION_EXPRESSION_Q, null, msgs);
			msgs = basicSetConditionExpressionQ(newConditionExpressionQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ELSIF_PATH__CONDITION_EXPRESSION_Q, newConditionExpressionQ, newConditionExpressionQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatementList getSequenceOfStatementsQl() {
		return sequenceOfStatementsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSequenceOfStatementsQl(StatementList newSequenceOfStatementsQl, NotificationChain msgs) {
		StatementList oldSequenceOfStatementsQl = sequenceOfStatementsQl;
		sequenceOfStatementsQl = newSequenceOfStatementsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ELSIF_PATH__SEQUENCE_OF_STATEMENTS_QL, oldSequenceOfStatementsQl, newSequenceOfStatementsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSequenceOfStatementsQl(StatementList newSequenceOfStatementsQl) {
		if (newSequenceOfStatementsQl != sequenceOfStatementsQl) {
			NotificationChain msgs = null;
			if (sequenceOfStatementsQl != null)
				msgs = ((InternalEObject)sequenceOfStatementsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ELSIF_PATH__SEQUENCE_OF_STATEMENTS_QL, null, msgs);
			if (newSequenceOfStatementsQl != null)
				msgs = ((InternalEObject)newSequenceOfStatementsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ELSIF_PATH__SEQUENCE_OF_STATEMENTS_QL, null, msgs);
			msgs = basicSetSequenceOfStatementsQl(newSequenceOfStatementsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ELSIF_PATH__SEQUENCE_OF_STATEMENTS_QL, newSequenceOfStatementsQl, newSequenceOfStatementsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ELSIF_PATH__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.ELSIF_PATH__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.ELSIF_PATH__CONDITION_EXPRESSION_Q:
				return basicSetConditionExpressionQ(null, msgs);
			case AdaPackage.ELSIF_PATH__SEQUENCE_OF_STATEMENTS_QL:
				return basicSetSequenceOfStatementsQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.ELSIF_PATH__SLOC:
				return getSloc();
			case AdaPackage.ELSIF_PATH__CONDITION_EXPRESSION_Q:
				return getConditionExpressionQ();
			case AdaPackage.ELSIF_PATH__SEQUENCE_OF_STATEMENTS_QL:
				return getSequenceOfStatementsQl();
			case AdaPackage.ELSIF_PATH__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.ELSIF_PATH__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.ELSIF_PATH__CONDITION_EXPRESSION_Q:
				setConditionExpressionQ((ExpressionClass)newValue);
				return;
			case AdaPackage.ELSIF_PATH__SEQUENCE_OF_STATEMENTS_QL:
				setSequenceOfStatementsQl((StatementList)newValue);
				return;
			case AdaPackage.ELSIF_PATH__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.ELSIF_PATH__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.ELSIF_PATH__CONDITION_EXPRESSION_Q:
				setConditionExpressionQ((ExpressionClass)null);
				return;
			case AdaPackage.ELSIF_PATH__SEQUENCE_OF_STATEMENTS_QL:
				setSequenceOfStatementsQl((StatementList)null);
				return;
			case AdaPackage.ELSIF_PATH__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.ELSIF_PATH__SLOC:
				return sloc != null;
			case AdaPackage.ELSIF_PATH__CONDITION_EXPRESSION_Q:
				return conditionExpressionQ != null;
			case AdaPackage.ELSIF_PATH__SEQUENCE_OF_STATEMENTS_QL:
				return sequenceOfStatementsQl != null;
			case AdaPackage.ELSIF_PATH__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //ElsifPathImpl
