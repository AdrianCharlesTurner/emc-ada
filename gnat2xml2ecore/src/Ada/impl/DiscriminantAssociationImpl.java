/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.DiscriminantAssociation;
import Ada.ExpressionClass;
import Ada.ExpressionList;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Discriminant Association</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.DiscriminantAssociationImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantAssociationImpl#getDiscriminantSelectorNamesQl <em>Discriminant Selector Names Ql</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantAssociationImpl#getDiscriminantExpressionQ <em>Discriminant Expression Q</em>}</li>
 *   <li>{@link Ada.impl.DiscriminantAssociationImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DiscriminantAssociationImpl extends MinimalEObjectImpl.Container implements DiscriminantAssociation {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getDiscriminantSelectorNamesQl() <em>Discriminant Selector Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiscriminantSelectorNamesQl()
	 * @generated
	 * @ordered
	 */
	protected ExpressionList discriminantSelectorNamesQl;

	/**
	 * The cached value of the '{@link #getDiscriminantExpressionQ() <em>Discriminant Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiscriminantExpressionQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass discriminantExpressionQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DiscriminantAssociationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getDiscriminantAssociation();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DISCRIMINANT_ASSOCIATION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DISCRIMINANT_ASSOCIATION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DISCRIMINANT_ASSOCIATION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DISCRIMINANT_ASSOCIATION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionList getDiscriminantSelectorNamesQl() {
		return discriminantSelectorNamesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscriminantSelectorNamesQl(ExpressionList newDiscriminantSelectorNamesQl, NotificationChain msgs) {
		ExpressionList oldDiscriminantSelectorNamesQl = discriminantSelectorNamesQl;
		discriminantSelectorNamesQl = newDiscriminantSelectorNamesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DISCRIMINANT_ASSOCIATION__DISCRIMINANT_SELECTOR_NAMES_QL, oldDiscriminantSelectorNamesQl, newDiscriminantSelectorNamesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscriminantSelectorNamesQl(ExpressionList newDiscriminantSelectorNamesQl) {
		if (newDiscriminantSelectorNamesQl != discriminantSelectorNamesQl) {
			NotificationChain msgs = null;
			if (discriminantSelectorNamesQl != null)
				msgs = ((InternalEObject)discriminantSelectorNamesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DISCRIMINANT_ASSOCIATION__DISCRIMINANT_SELECTOR_NAMES_QL, null, msgs);
			if (newDiscriminantSelectorNamesQl != null)
				msgs = ((InternalEObject)newDiscriminantSelectorNamesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DISCRIMINANT_ASSOCIATION__DISCRIMINANT_SELECTOR_NAMES_QL, null, msgs);
			msgs = basicSetDiscriminantSelectorNamesQl(newDiscriminantSelectorNamesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DISCRIMINANT_ASSOCIATION__DISCRIMINANT_SELECTOR_NAMES_QL, newDiscriminantSelectorNamesQl, newDiscriminantSelectorNamesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getDiscriminantExpressionQ() {
		return discriminantExpressionQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscriminantExpressionQ(ExpressionClass newDiscriminantExpressionQ, NotificationChain msgs) {
		ExpressionClass oldDiscriminantExpressionQ = discriminantExpressionQ;
		discriminantExpressionQ = newDiscriminantExpressionQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DISCRIMINANT_ASSOCIATION__DISCRIMINANT_EXPRESSION_Q, oldDiscriminantExpressionQ, newDiscriminantExpressionQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscriminantExpressionQ(ExpressionClass newDiscriminantExpressionQ) {
		if (newDiscriminantExpressionQ != discriminantExpressionQ) {
			NotificationChain msgs = null;
			if (discriminantExpressionQ != null)
				msgs = ((InternalEObject)discriminantExpressionQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DISCRIMINANT_ASSOCIATION__DISCRIMINANT_EXPRESSION_Q, null, msgs);
			if (newDiscriminantExpressionQ != null)
				msgs = ((InternalEObject)newDiscriminantExpressionQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DISCRIMINANT_ASSOCIATION__DISCRIMINANT_EXPRESSION_Q, null, msgs);
			msgs = basicSetDiscriminantExpressionQ(newDiscriminantExpressionQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DISCRIMINANT_ASSOCIATION__DISCRIMINANT_EXPRESSION_Q, newDiscriminantExpressionQ, newDiscriminantExpressionQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DISCRIMINANT_ASSOCIATION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.DISCRIMINANT_ASSOCIATION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.DISCRIMINANT_ASSOCIATION__DISCRIMINANT_SELECTOR_NAMES_QL:
				return basicSetDiscriminantSelectorNamesQl(null, msgs);
			case AdaPackage.DISCRIMINANT_ASSOCIATION__DISCRIMINANT_EXPRESSION_Q:
				return basicSetDiscriminantExpressionQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.DISCRIMINANT_ASSOCIATION__SLOC:
				return getSloc();
			case AdaPackage.DISCRIMINANT_ASSOCIATION__DISCRIMINANT_SELECTOR_NAMES_QL:
				return getDiscriminantSelectorNamesQl();
			case AdaPackage.DISCRIMINANT_ASSOCIATION__DISCRIMINANT_EXPRESSION_Q:
				return getDiscriminantExpressionQ();
			case AdaPackage.DISCRIMINANT_ASSOCIATION__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.DISCRIMINANT_ASSOCIATION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.DISCRIMINANT_ASSOCIATION__DISCRIMINANT_SELECTOR_NAMES_QL:
				setDiscriminantSelectorNamesQl((ExpressionList)newValue);
				return;
			case AdaPackage.DISCRIMINANT_ASSOCIATION__DISCRIMINANT_EXPRESSION_Q:
				setDiscriminantExpressionQ((ExpressionClass)newValue);
				return;
			case AdaPackage.DISCRIMINANT_ASSOCIATION__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.DISCRIMINANT_ASSOCIATION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.DISCRIMINANT_ASSOCIATION__DISCRIMINANT_SELECTOR_NAMES_QL:
				setDiscriminantSelectorNamesQl((ExpressionList)null);
				return;
			case AdaPackage.DISCRIMINANT_ASSOCIATION__DISCRIMINANT_EXPRESSION_Q:
				setDiscriminantExpressionQ((ExpressionClass)null);
				return;
			case AdaPackage.DISCRIMINANT_ASSOCIATION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.DISCRIMINANT_ASSOCIATION__SLOC:
				return sloc != null;
			case AdaPackage.DISCRIMINANT_ASSOCIATION__DISCRIMINANT_SELECTOR_NAMES_QL:
				return discriminantSelectorNamesQl != null;
			case AdaPackage.DISCRIMINANT_ASSOCIATION__DISCRIMINANT_EXPRESSION_Q:
				return discriminantExpressionQ != null;
			case AdaPackage.DISCRIMINANT_ASSOCIATION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //DiscriminantAssociationImpl
