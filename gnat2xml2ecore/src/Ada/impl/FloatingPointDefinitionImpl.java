/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.ExpressionClass;
import Ada.FloatingPointDefinition;
import Ada.RangeConstraintClass;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Floating Point Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.FloatingPointDefinitionImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.FloatingPointDefinitionImpl#getDigitsExpressionQ <em>Digits Expression Q</em>}</li>
 *   <li>{@link Ada.impl.FloatingPointDefinitionImpl#getRealRangeConstraintQ <em>Real Range Constraint Q</em>}</li>
 *   <li>{@link Ada.impl.FloatingPointDefinitionImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FloatingPointDefinitionImpl extends MinimalEObjectImpl.Container implements FloatingPointDefinition {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getDigitsExpressionQ() <em>Digits Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDigitsExpressionQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass digitsExpressionQ;

	/**
	 * The cached value of the '{@link #getRealRangeConstraintQ() <em>Real Range Constraint Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRealRangeConstraintQ()
	 * @generated
	 * @ordered
	 */
	protected RangeConstraintClass realRangeConstraintQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FloatingPointDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getFloatingPointDefinition();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FLOATING_POINT_DEFINITION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FLOATING_POINT_DEFINITION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FLOATING_POINT_DEFINITION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FLOATING_POINT_DEFINITION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getDigitsExpressionQ() {
		return digitsExpressionQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDigitsExpressionQ(ExpressionClass newDigitsExpressionQ, NotificationChain msgs) {
		ExpressionClass oldDigitsExpressionQ = digitsExpressionQ;
		digitsExpressionQ = newDigitsExpressionQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FLOATING_POINT_DEFINITION__DIGITS_EXPRESSION_Q, oldDigitsExpressionQ, newDigitsExpressionQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDigitsExpressionQ(ExpressionClass newDigitsExpressionQ) {
		if (newDigitsExpressionQ != digitsExpressionQ) {
			NotificationChain msgs = null;
			if (digitsExpressionQ != null)
				msgs = ((InternalEObject)digitsExpressionQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FLOATING_POINT_DEFINITION__DIGITS_EXPRESSION_Q, null, msgs);
			if (newDigitsExpressionQ != null)
				msgs = ((InternalEObject)newDigitsExpressionQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FLOATING_POINT_DEFINITION__DIGITS_EXPRESSION_Q, null, msgs);
			msgs = basicSetDigitsExpressionQ(newDigitsExpressionQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FLOATING_POINT_DEFINITION__DIGITS_EXPRESSION_Q, newDigitsExpressionQ, newDigitsExpressionQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RangeConstraintClass getRealRangeConstraintQ() {
		return realRangeConstraintQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRealRangeConstraintQ(RangeConstraintClass newRealRangeConstraintQ, NotificationChain msgs) {
		RangeConstraintClass oldRealRangeConstraintQ = realRangeConstraintQ;
		realRangeConstraintQ = newRealRangeConstraintQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FLOATING_POINT_DEFINITION__REAL_RANGE_CONSTRAINT_Q, oldRealRangeConstraintQ, newRealRangeConstraintQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRealRangeConstraintQ(RangeConstraintClass newRealRangeConstraintQ) {
		if (newRealRangeConstraintQ != realRangeConstraintQ) {
			NotificationChain msgs = null;
			if (realRangeConstraintQ != null)
				msgs = ((InternalEObject)realRangeConstraintQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FLOATING_POINT_DEFINITION__REAL_RANGE_CONSTRAINT_Q, null, msgs);
			if (newRealRangeConstraintQ != null)
				msgs = ((InternalEObject)newRealRangeConstraintQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FLOATING_POINT_DEFINITION__REAL_RANGE_CONSTRAINT_Q, null, msgs);
			msgs = basicSetRealRangeConstraintQ(newRealRangeConstraintQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FLOATING_POINT_DEFINITION__REAL_RANGE_CONSTRAINT_Q, newRealRangeConstraintQ, newRealRangeConstraintQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FLOATING_POINT_DEFINITION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.FLOATING_POINT_DEFINITION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.FLOATING_POINT_DEFINITION__DIGITS_EXPRESSION_Q:
				return basicSetDigitsExpressionQ(null, msgs);
			case AdaPackage.FLOATING_POINT_DEFINITION__REAL_RANGE_CONSTRAINT_Q:
				return basicSetRealRangeConstraintQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.FLOATING_POINT_DEFINITION__SLOC:
				return getSloc();
			case AdaPackage.FLOATING_POINT_DEFINITION__DIGITS_EXPRESSION_Q:
				return getDigitsExpressionQ();
			case AdaPackage.FLOATING_POINT_DEFINITION__REAL_RANGE_CONSTRAINT_Q:
				return getRealRangeConstraintQ();
			case AdaPackage.FLOATING_POINT_DEFINITION__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.FLOATING_POINT_DEFINITION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.FLOATING_POINT_DEFINITION__DIGITS_EXPRESSION_Q:
				setDigitsExpressionQ((ExpressionClass)newValue);
				return;
			case AdaPackage.FLOATING_POINT_DEFINITION__REAL_RANGE_CONSTRAINT_Q:
				setRealRangeConstraintQ((RangeConstraintClass)newValue);
				return;
			case AdaPackage.FLOATING_POINT_DEFINITION__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.FLOATING_POINT_DEFINITION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.FLOATING_POINT_DEFINITION__DIGITS_EXPRESSION_Q:
				setDigitsExpressionQ((ExpressionClass)null);
				return;
			case AdaPackage.FLOATING_POINT_DEFINITION__REAL_RANGE_CONSTRAINT_Q:
				setRealRangeConstraintQ((RangeConstraintClass)null);
				return;
			case AdaPackage.FLOATING_POINT_DEFINITION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.FLOATING_POINT_DEFINITION__SLOC:
				return sloc != null;
			case AdaPackage.FLOATING_POINT_DEFINITION__DIGITS_EXPRESSION_Q:
				return digitsExpressionQ != null;
			case AdaPackage.FLOATING_POINT_DEFINITION__REAL_RANGE_CONSTRAINT_Q:
				return realRangeConstraintQ != null;
			case AdaPackage.FLOATING_POINT_DEFINITION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //FloatingPointDefinitionImpl
