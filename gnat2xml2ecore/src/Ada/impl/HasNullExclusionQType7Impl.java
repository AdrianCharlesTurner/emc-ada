/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.HasNullExclusionQType7;
import Ada.NotAnElement;
import Ada.NullExclusion;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Has Null Exclusion QType7</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.HasNullExclusionQType7Impl#getNullExclusion <em>Null Exclusion</em>}</li>
 *   <li>{@link Ada.impl.HasNullExclusionQType7Impl#getNotAnElement <em>Not An Element</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HasNullExclusionQType7Impl extends MinimalEObjectImpl.Container implements HasNullExclusionQType7 {
	/**
	 * The cached value of the '{@link #getNullExclusion() <em>Null Exclusion</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNullExclusion()
	 * @generated
	 * @ordered
	 */
	protected NullExclusion nullExclusion;

	/**
	 * The cached value of the '{@link #getNotAnElement() <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotAnElement()
	 * @generated
	 * @ordered
	 */
	protected NotAnElement notAnElement;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HasNullExclusionQType7Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getHasNullExclusionQType7();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NullExclusion getNullExclusion() {
		return nullExclusion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNullExclusion(NullExclusion newNullExclusion, NotificationChain msgs) {
		NullExclusion oldNullExclusion = nullExclusion;
		nullExclusion = newNullExclusion;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.HAS_NULL_EXCLUSION_QTYPE7__NULL_EXCLUSION, oldNullExclusion, newNullExclusion);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNullExclusion(NullExclusion newNullExclusion) {
		if (newNullExclusion != nullExclusion) {
			NotificationChain msgs = null;
			if (nullExclusion != null)
				msgs = ((InternalEObject)nullExclusion).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.HAS_NULL_EXCLUSION_QTYPE7__NULL_EXCLUSION, null, msgs);
			if (newNullExclusion != null)
				msgs = ((InternalEObject)newNullExclusion).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.HAS_NULL_EXCLUSION_QTYPE7__NULL_EXCLUSION, null, msgs);
			msgs = basicSetNullExclusion(newNullExclusion, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.HAS_NULL_EXCLUSION_QTYPE7__NULL_EXCLUSION, newNullExclusion, newNullExclusion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotAnElement getNotAnElement() {
		return notAnElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNotAnElement(NotAnElement newNotAnElement, NotificationChain msgs) {
		NotAnElement oldNotAnElement = notAnElement;
		notAnElement = newNotAnElement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.HAS_NULL_EXCLUSION_QTYPE7__NOT_AN_ELEMENT, oldNotAnElement, newNotAnElement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotAnElement(NotAnElement newNotAnElement) {
		if (newNotAnElement != notAnElement) {
			NotificationChain msgs = null;
			if (notAnElement != null)
				msgs = ((InternalEObject)notAnElement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.HAS_NULL_EXCLUSION_QTYPE7__NOT_AN_ELEMENT, null, msgs);
			if (newNotAnElement != null)
				msgs = ((InternalEObject)newNotAnElement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.HAS_NULL_EXCLUSION_QTYPE7__NOT_AN_ELEMENT, null, msgs);
			msgs = basicSetNotAnElement(newNotAnElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.HAS_NULL_EXCLUSION_QTYPE7__NOT_AN_ELEMENT, newNotAnElement, newNotAnElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE7__NULL_EXCLUSION:
				return basicSetNullExclusion(null, msgs);
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE7__NOT_AN_ELEMENT:
				return basicSetNotAnElement(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE7__NULL_EXCLUSION:
				return getNullExclusion();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE7__NOT_AN_ELEMENT:
				return getNotAnElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE7__NULL_EXCLUSION:
				setNullExclusion((NullExclusion)newValue);
				return;
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE7__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE7__NULL_EXCLUSION:
				setNullExclusion((NullExclusion)null);
				return;
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE7__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE7__NULL_EXCLUSION:
				return nullExclusion != null;
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE7__NOT_AN_ELEMENT:
				return notAnElement != null;
		}
		return super.eIsSet(featureID);
	}

} //HasNullExclusionQType7Impl
