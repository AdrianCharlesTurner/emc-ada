/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.DefiningNameList;
import Ada.ElementClass;
import Ada.ElementList;
import Ada.ExpressionClass;
import Ada.FormalFunctionDeclaration;
import Ada.HasAbstractQType10;
import Ada.IsNotNullReturnQType5;
import Ada.ParameterSpecificationList;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Formal Function Declaration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.FormalFunctionDeclarationImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.FormalFunctionDeclarationImpl#getNamesQl <em>Names Ql</em>}</li>
 *   <li>{@link Ada.impl.FormalFunctionDeclarationImpl#getParameterProfileQl <em>Parameter Profile Ql</em>}</li>
 *   <li>{@link Ada.impl.FormalFunctionDeclarationImpl#getIsNotNullReturnQ <em>Is Not Null Return Q</em>}</li>
 *   <li>{@link Ada.impl.FormalFunctionDeclarationImpl#getResultProfileQ <em>Result Profile Q</em>}</li>
 *   <li>{@link Ada.impl.FormalFunctionDeclarationImpl#getFormalSubprogramDefaultQ <em>Formal Subprogram Default Q</em>}</li>
 *   <li>{@link Ada.impl.FormalFunctionDeclarationImpl#getHasAbstractQ <em>Has Abstract Q</em>}</li>
 *   <li>{@link Ada.impl.FormalFunctionDeclarationImpl#getAspectSpecificationsQl <em>Aspect Specifications Ql</em>}</li>
 *   <li>{@link Ada.impl.FormalFunctionDeclarationImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FormalFunctionDeclarationImpl extends MinimalEObjectImpl.Container implements FormalFunctionDeclaration {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getNamesQl() <em>Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamesQl()
	 * @generated
	 * @ordered
	 */
	protected DefiningNameList namesQl;

	/**
	 * The cached value of the '{@link #getParameterProfileQl() <em>Parameter Profile Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterProfileQl()
	 * @generated
	 * @ordered
	 */
	protected ParameterSpecificationList parameterProfileQl;

	/**
	 * The cached value of the '{@link #getIsNotNullReturnQ() <em>Is Not Null Return Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsNotNullReturnQ()
	 * @generated
	 * @ordered
	 */
	protected IsNotNullReturnQType5 isNotNullReturnQ;

	/**
	 * The cached value of the '{@link #getResultProfileQ() <em>Result Profile Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResultProfileQ()
	 * @generated
	 * @ordered
	 */
	protected ElementClass resultProfileQ;

	/**
	 * The cached value of the '{@link #getFormalSubprogramDefaultQ() <em>Formal Subprogram Default Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalSubprogramDefaultQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass formalSubprogramDefaultQ;

	/**
	 * The cached value of the '{@link #getHasAbstractQ() <em>Has Abstract Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasAbstractQ()
	 * @generated
	 * @ordered
	 */
	protected HasAbstractQType10 hasAbstractQ;

	/**
	 * The cached value of the '{@link #getAspectSpecificationsQl() <em>Aspect Specifications Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAspectSpecificationsQl()
	 * @generated
	 * @ordered
	 */
	protected ElementList aspectSpecificationsQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FormalFunctionDeclarationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getFormalFunctionDeclaration();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_FUNCTION_DECLARATION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_FUNCTION_DECLARATION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_FUNCTION_DECLARATION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_FUNCTION_DECLARATION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNameList getNamesQl() {
		return namesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNamesQl(DefiningNameList newNamesQl, NotificationChain msgs) {
		DefiningNameList oldNamesQl = namesQl;
		namesQl = newNamesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_FUNCTION_DECLARATION__NAMES_QL, oldNamesQl, newNamesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNamesQl(DefiningNameList newNamesQl) {
		if (newNamesQl != namesQl) {
			NotificationChain msgs = null;
			if (namesQl != null)
				msgs = ((InternalEObject)namesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_FUNCTION_DECLARATION__NAMES_QL, null, msgs);
			if (newNamesQl != null)
				msgs = ((InternalEObject)newNamesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_FUNCTION_DECLARATION__NAMES_QL, null, msgs);
			msgs = basicSetNamesQl(newNamesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_FUNCTION_DECLARATION__NAMES_QL, newNamesQl, newNamesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterSpecificationList getParameterProfileQl() {
		return parameterProfileQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParameterProfileQl(ParameterSpecificationList newParameterProfileQl, NotificationChain msgs) {
		ParameterSpecificationList oldParameterProfileQl = parameterProfileQl;
		parameterProfileQl = newParameterProfileQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_FUNCTION_DECLARATION__PARAMETER_PROFILE_QL, oldParameterProfileQl, newParameterProfileQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterProfileQl(ParameterSpecificationList newParameterProfileQl) {
		if (newParameterProfileQl != parameterProfileQl) {
			NotificationChain msgs = null;
			if (parameterProfileQl != null)
				msgs = ((InternalEObject)parameterProfileQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_FUNCTION_DECLARATION__PARAMETER_PROFILE_QL, null, msgs);
			if (newParameterProfileQl != null)
				msgs = ((InternalEObject)newParameterProfileQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_FUNCTION_DECLARATION__PARAMETER_PROFILE_QL, null, msgs);
			msgs = basicSetParameterProfileQl(newParameterProfileQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_FUNCTION_DECLARATION__PARAMETER_PROFILE_QL, newParameterProfileQl, newParameterProfileQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotNullReturnQType5 getIsNotNullReturnQ() {
		return isNotNullReturnQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIsNotNullReturnQ(IsNotNullReturnQType5 newIsNotNullReturnQ, NotificationChain msgs) {
		IsNotNullReturnQType5 oldIsNotNullReturnQ = isNotNullReturnQ;
		isNotNullReturnQ = newIsNotNullReturnQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_FUNCTION_DECLARATION__IS_NOT_NULL_RETURN_Q, oldIsNotNullReturnQ, newIsNotNullReturnQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsNotNullReturnQ(IsNotNullReturnQType5 newIsNotNullReturnQ) {
		if (newIsNotNullReturnQ != isNotNullReturnQ) {
			NotificationChain msgs = null;
			if (isNotNullReturnQ != null)
				msgs = ((InternalEObject)isNotNullReturnQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_FUNCTION_DECLARATION__IS_NOT_NULL_RETURN_Q, null, msgs);
			if (newIsNotNullReturnQ != null)
				msgs = ((InternalEObject)newIsNotNullReturnQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_FUNCTION_DECLARATION__IS_NOT_NULL_RETURN_Q, null, msgs);
			msgs = basicSetIsNotNullReturnQ(newIsNotNullReturnQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_FUNCTION_DECLARATION__IS_NOT_NULL_RETURN_Q, newIsNotNullReturnQ, newIsNotNullReturnQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementClass getResultProfileQ() {
		return resultProfileQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetResultProfileQ(ElementClass newResultProfileQ, NotificationChain msgs) {
		ElementClass oldResultProfileQ = resultProfileQ;
		resultProfileQ = newResultProfileQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_FUNCTION_DECLARATION__RESULT_PROFILE_Q, oldResultProfileQ, newResultProfileQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResultProfileQ(ElementClass newResultProfileQ) {
		if (newResultProfileQ != resultProfileQ) {
			NotificationChain msgs = null;
			if (resultProfileQ != null)
				msgs = ((InternalEObject)resultProfileQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_FUNCTION_DECLARATION__RESULT_PROFILE_Q, null, msgs);
			if (newResultProfileQ != null)
				msgs = ((InternalEObject)newResultProfileQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_FUNCTION_DECLARATION__RESULT_PROFILE_Q, null, msgs);
			msgs = basicSetResultProfileQ(newResultProfileQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_FUNCTION_DECLARATION__RESULT_PROFILE_Q, newResultProfileQ, newResultProfileQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getFormalSubprogramDefaultQ() {
		return formalSubprogramDefaultQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalSubprogramDefaultQ(ExpressionClass newFormalSubprogramDefaultQ, NotificationChain msgs) {
		ExpressionClass oldFormalSubprogramDefaultQ = formalSubprogramDefaultQ;
		formalSubprogramDefaultQ = newFormalSubprogramDefaultQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_FUNCTION_DECLARATION__FORMAL_SUBPROGRAM_DEFAULT_Q, oldFormalSubprogramDefaultQ, newFormalSubprogramDefaultQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalSubprogramDefaultQ(ExpressionClass newFormalSubprogramDefaultQ) {
		if (newFormalSubprogramDefaultQ != formalSubprogramDefaultQ) {
			NotificationChain msgs = null;
			if (formalSubprogramDefaultQ != null)
				msgs = ((InternalEObject)formalSubprogramDefaultQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_FUNCTION_DECLARATION__FORMAL_SUBPROGRAM_DEFAULT_Q, null, msgs);
			if (newFormalSubprogramDefaultQ != null)
				msgs = ((InternalEObject)newFormalSubprogramDefaultQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_FUNCTION_DECLARATION__FORMAL_SUBPROGRAM_DEFAULT_Q, null, msgs);
			msgs = basicSetFormalSubprogramDefaultQ(newFormalSubprogramDefaultQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_FUNCTION_DECLARATION__FORMAL_SUBPROGRAM_DEFAULT_Q, newFormalSubprogramDefaultQ, newFormalSubprogramDefaultQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAbstractQType10 getHasAbstractQ() {
		return hasAbstractQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasAbstractQ(HasAbstractQType10 newHasAbstractQ, NotificationChain msgs) {
		HasAbstractQType10 oldHasAbstractQ = hasAbstractQ;
		hasAbstractQ = newHasAbstractQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_FUNCTION_DECLARATION__HAS_ABSTRACT_Q, oldHasAbstractQ, newHasAbstractQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasAbstractQ(HasAbstractQType10 newHasAbstractQ) {
		if (newHasAbstractQ != hasAbstractQ) {
			NotificationChain msgs = null;
			if (hasAbstractQ != null)
				msgs = ((InternalEObject)hasAbstractQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_FUNCTION_DECLARATION__HAS_ABSTRACT_Q, null, msgs);
			if (newHasAbstractQ != null)
				msgs = ((InternalEObject)newHasAbstractQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_FUNCTION_DECLARATION__HAS_ABSTRACT_Q, null, msgs);
			msgs = basicSetHasAbstractQ(newHasAbstractQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_FUNCTION_DECLARATION__HAS_ABSTRACT_Q, newHasAbstractQ, newHasAbstractQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementList getAspectSpecificationsQl() {
		return aspectSpecificationsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAspectSpecificationsQl(ElementList newAspectSpecificationsQl, NotificationChain msgs) {
		ElementList oldAspectSpecificationsQl = aspectSpecificationsQl;
		aspectSpecificationsQl = newAspectSpecificationsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_FUNCTION_DECLARATION__ASPECT_SPECIFICATIONS_QL, oldAspectSpecificationsQl, newAspectSpecificationsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAspectSpecificationsQl(ElementList newAspectSpecificationsQl) {
		if (newAspectSpecificationsQl != aspectSpecificationsQl) {
			NotificationChain msgs = null;
			if (aspectSpecificationsQl != null)
				msgs = ((InternalEObject)aspectSpecificationsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_FUNCTION_DECLARATION__ASPECT_SPECIFICATIONS_QL, null, msgs);
			if (newAspectSpecificationsQl != null)
				msgs = ((InternalEObject)newAspectSpecificationsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FORMAL_FUNCTION_DECLARATION__ASPECT_SPECIFICATIONS_QL, null, msgs);
			msgs = basicSetAspectSpecificationsQl(newAspectSpecificationsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_FUNCTION_DECLARATION__ASPECT_SPECIFICATIONS_QL, newAspectSpecificationsQl, newAspectSpecificationsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FORMAL_FUNCTION_DECLARATION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__NAMES_QL:
				return basicSetNamesQl(null, msgs);
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__PARAMETER_PROFILE_QL:
				return basicSetParameterProfileQl(null, msgs);
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__IS_NOT_NULL_RETURN_Q:
				return basicSetIsNotNullReturnQ(null, msgs);
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__RESULT_PROFILE_Q:
				return basicSetResultProfileQ(null, msgs);
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__FORMAL_SUBPROGRAM_DEFAULT_Q:
				return basicSetFormalSubprogramDefaultQ(null, msgs);
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__HAS_ABSTRACT_Q:
				return basicSetHasAbstractQ(null, msgs);
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__ASPECT_SPECIFICATIONS_QL:
				return basicSetAspectSpecificationsQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__SLOC:
				return getSloc();
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__NAMES_QL:
				return getNamesQl();
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__PARAMETER_PROFILE_QL:
				return getParameterProfileQl();
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__IS_NOT_NULL_RETURN_Q:
				return getIsNotNullReturnQ();
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__RESULT_PROFILE_Q:
				return getResultProfileQ();
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__FORMAL_SUBPROGRAM_DEFAULT_Q:
				return getFormalSubprogramDefaultQ();
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__HAS_ABSTRACT_Q:
				return getHasAbstractQ();
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__ASPECT_SPECIFICATIONS_QL:
				return getAspectSpecificationsQl();
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__NAMES_QL:
				setNamesQl((DefiningNameList)newValue);
				return;
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__PARAMETER_PROFILE_QL:
				setParameterProfileQl((ParameterSpecificationList)newValue);
				return;
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__IS_NOT_NULL_RETURN_Q:
				setIsNotNullReturnQ((IsNotNullReturnQType5)newValue);
				return;
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__RESULT_PROFILE_Q:
				setResultProfileQ((ElementClass)newValue);
				return;
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__FORMAL_SUBPROGRAM_DEFAULT_Q:
				setFormalSubprogramDefaultQ((ExpressionClass)newValue);
				return;
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__HAS_ABSTRACT_Q:
				setHasAbstractQ((HasAbstractQType10)newValue);
				return;
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__ASPECT_SPECIFICATIONS_QL:
				setAspectSpecificationsQl((ElementList)newValue);
				return;
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__NAMES_QL:
				setNamesQl((DefiningNameList)null);
				return;
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__PARAMETER_PROFILE_QL:
				setParameterProfileQl((ParameterSpecificationList)null);
				return;
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__IS_NOT_NULL_RETURN_Q:
				setIsNotNullReturnQ((IsNotNullReturnQType5)null);
				return;
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__RESULT_PROFILE_Q:
				setResultProfileQ((ElementClass)null);
				return;
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__FORMAL_SUBPROGRAM_DEFAULT_Q:
				setFormalSubprogramDefaultQ((ExpressionClass)null);
				return;
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__HAS_ABSTRACT_Q:
				setHasAbstractQ((HasAbstractQType10)null);
				return;
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__ASPECT_SPECIFICATIONS_QL:
				setAspectSpecificationsQl((ElementList)null);
				return;
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__SLOC:
				return sloc != null;
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__NAMES_QL:
				return namesQl != null;
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__PARAMETER_PROFILE_QL:
				return parameterProfileQl != null;
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__IS_NOT_NULL_RETURN_Q:
				return isNotNullReturnQ != null;
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__RESULT_PROFILE_Q:
				return resultProfileQ != null;
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__FORMAL_SUBPROGRAM_DEFAULT_Q:
				return formalSubprogramDefaultQ != null;
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__HAS_ABSTRACT_Q:
				return hasAbstractQ != null;
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__ASPECT_SPECIFICATIONS_QL:
				return aspectSpecificationsQl != null;
			case AdaPackage.FORMAL_FUNCTION_DECLARATION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //FormalFunctionDeclarationImpl
