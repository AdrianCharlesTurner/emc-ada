/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.AssociationList;
import Ada.RemoteCallInterfacePragma;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Remote Call Interface Pragma</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.RemoteCallInterfacePragmaImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.RemoteCallInterfacePragmaImpl#getPragmaArgumentAssociationsQl <em>Pragma Argument Associations Ql</em>}</li>
 *   <li>{@link Ada.impl.RemoteCallInterfacePragmaImpl#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.impl.RemoteCallInterfacePragmaImpl#getPragmaName <em>Pragma Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RemoteCallInterfacePragmaImpl extends MinimalEObjectImpl.Container implements RemoteCallInterfacePragma {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getPragmaArgumentAssociationsQl() <em>Pragma Argument Associations Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPragmaArgumentAssociationsQl()
	 * @generated
	 * @ordered
	 */
	protected AssociationList pragmaArgumentAssociationsQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * The default value of the '{@link #getPragmaName() <em>Pragma Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPragmaName()
	 * @generated
	 * @ordered
	 */
	protected static final String PRAGMA_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPragmaName() <em>Pragma Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPragmaName()
	 * @generated
	 * @ordered
	 */
	protected String pragmaName = PRAGMA_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RemoteCallInterfacePragmaImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getRemoteCallInterfacePragma();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationList getPragmaArgumentAssociationsQl() {
		return pragmaArgumentAssociationsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPragmaArgumentAssociationsQl(AssociationList newPragmaArgumentAssociationsQl, NotificationChain msgs) {
		AssociationList oldPragmaArgumentAssociationsQl = pragmaArgumentAssociationsQl;
		pragmaArgumentAssociationsQl = newPragmaArgumentAssociationsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__PRAGMA_ARGUMENT_ASSOCIATIONS_QL, oldPragmaArgumentAssociationsQl, newPragmaArgumentAssociationsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPragmaArgumentAssociationsQl(AssociationList newPragmaArgumentAssociationsQl) {
		if (newPragmaArgumentAssociationsQl != pragmaArgumentAssociationsQl) {
			NotificationChain msgs = null;
			if (pragmaArgumentAssociationsQl != null)
				msgs = ((InternalEObject)pragmaArgumentAssociationsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__PRAGMA_ARGUMENT_ASSOCIATIONS_QL, null, msgs);
			if (newPragmaArgumentAssociationsQl != null)
				msgs = ((InternalEObject)newPragmaArgumentAssociationsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__PRAGMA_ARGUMENT_ASSOCIATIONS_QL, null, msgs);
			msgs = basicSetPragmaArgumentAssociationsQl(newPragmaArgumentAssociationsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__PRAGMA_ARGUMENT_ASSOCIATIONS_QL, newPragmaArgumentAssociationsQl, newPragmaArgumentAssociationsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPragmaName() {
		return pragmaName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPragmaName(String newPragmaName) {
		String oldPragmaName = pragmaName;
		pragmaName = newPragmaName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__PRAGMA_NAME, oldPragmaName, pragmaName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__PRAGMA_ARGUMENT_ASSOCIATIONS_QL:
				return basicSetPragmaArgumentAssociationsQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__SLOC:
				return getSloc();
			case AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__PRAGMA_ARGUMENT_ASSOCIATIONS_QL:
				return getPragmaArgumentAssociationsQl();
			case AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__CHECKS:
				return getChecks();
			case AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__PRAGMA_NAME:
				return getPragmaName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__PRAGMA_ARGUMENT_ASSOCIATIONS_QL:
				setPragmaArgumentAssociationsQl((AssociationList)newValue);
				return;
			case AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__CHECKS:
				setChecks((String)newValue);
				return;
			case AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__PRAGMA_NAME:
				setPragmaName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__PRAGMA_ARGUMENT_ASSOCIATIONS_QL:
				setPragmaArgumentAssociationsQl((AssociationList)null);
				return;
			case AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
			case AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__PRAGMA_NAME:
				setPragmaName(PRAGMA_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__SLOC:
				return sloc != null;
			case AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__PRAGMA_ARGUMENT_ASSOCIATIONS_QL:
				return pragmaArgumentAssociationsQl != null;
			case AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
			case AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA__PRAGMA_NAME:
				return PRAGMA_NAME_EDEFAULT == null ? pragmaName != null : !PRAGMA_NAME_EDEFAULT.equals(pragmaName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(", pragmaName: ");
		result.append(pragmaName);
		result.append(')');
		return result.toString();
	}

} //RemoteCallInterfacePragmaImpl
