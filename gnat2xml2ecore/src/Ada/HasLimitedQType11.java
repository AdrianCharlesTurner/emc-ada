/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Has Limited QType11</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.HasLimitedQType11#getLimited <em>Limited</em>}</li>
 *   <li>{@link Ada.HasLimitedQType11#getNotAnElement <em>Not An Element</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getHasLimitedQType11()
 * @model extendedMetaData="name='has_limited_q_._11_._type' kind='elementOnly'"
 * @generated
 */
public interface HasLimitedQType11 extends EObject {
	/**
	 * Returns the value of the '<em><b>Limited</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Limited</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Limited</em>' containment reference.
	 * @see #setLimited(Limited)
	 * @see Ada.AdaPackage#getHasLimitedQType11_Limited()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='limited' namespace='##targetNamespace'"
	 * @generated
	 */
	Limited getLimited();

	/**
	 * Sets the value of the '{@link Ada.HasLimitedQType11#getLimited <em>Limited</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Limited</em>' containment reference.
	 * @see #getLimited()
	 * @generated
	 */
	void setLimited(Limited value);

	/**
	 * Returns the value of the '<em><b>Not An Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not An Element</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not An Element</em>' containment reference.
	 * @see #setNotAnElement(NotAnElement)
	 * @see Ada.AdaPackage#getHasLimitedQType11_NotAnElement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not_an_element' namespace='##targetNamespace'"
	 * @generated
	 */
	NotAnElement getNotAnElement();

	/**
	 * Sets the value of the '{@link Ada.HasLimitedQType11#getNotAnElement <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not An Element</em>' containment reference.
	 * @see #getNotAnElement()
	 * @generated
	 */
	void setNotAnElement(NotAnElement value);

} // HasLimitedQType11
