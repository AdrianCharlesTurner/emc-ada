/**
 */
package Ada.impl;

import Ada.AbsOperator;
import Ada.AccessAttribute;
import Ada.AdaPackage;
import Ada.AddressAttribute;
import Ada.AdjacentAttribute;
import Ada.AftAttribute;
import Ada.AlignmentAttribute;
import Ada.AllCallsRemotePragma;
import Ada.AllocationFromQualifiedExpression;
import Ada.AllocationFromSubtype;
import Ada.AndOperator;
import Ada.AndThenShortCircuit;
import Ada.AssertPragma;
import Ada.AssertionPolicyPragma;
import Ada.AsynchronousPragma;
import Ada.AtomicComponentsPragma;
import Ada.AtomicPragma;
import Ada.AttachHandlerPragma;
import Ada.BaseAttribute;
import Ada.BitOrderAttribute;
import Ada.BodyVersionAttribute;
import Ada.BoxExpression;
import Ada.CallableAttribute;
import Ada.CallerAttribute;
import Ada.CaseExpression;
import Ada.CeilingAttribute;
import Ada.CharacterLiteral;
import Ada.ClassAttribute;
import Ada.Comment;
import Ada.ComponentSizeAttribute;
import Ada.ComposeAttribute;
import Ada.ConcatenateOperator;
import Ada.ConstrainedAttribute;
import Ada.ControlledPragma;
import Ada.ConventionPragma;
import Ada.CopySignAttribute;
import Ada.CountAttribute;
import Ada.CpuPragma;
import Ada.DefaultStoragePoolPragma;
import Ada.DefiniteAttribute;
import Ada.DeltaAttribute;
import Ada.DenormAttribute;
import Ada.DetectBlockingPragma;
import Ada.DigitsAttribute;
import Ada.DiscardNamesPragma;
import Ada.DiscreteRangeAttributeReference;
import Ada.DiscreteSimpleExpressionRange;
import Ada.DispatchingDomainPragma;
import Ada.DivideOperator;
import Ada.ElaborateAllPragma;
import Ada.ElaborateBodyPragma;
import Ada.ElaboratePragma;
import Ada.EnumerationLiteral;
import Ada.EqualOperator;
import Ada.ExplicitDereference;
import Ada.ExponentAttribute;
import Ada.ExponentiateOperator;
import Ada.ExportPragma;
import Ada.ExpressionList;
import Ada.ExtensionAggregate;
import Ada.ExternalTagAttribute;
import Ada.FirstAttribute;
import Ada.FirstBitAttribute;
import Ada.FloorAttribute;
import Ada.ForAllQuantifiedExpression;
import Ada.ForSomeQuantifiedExpression;
import Ada.ForeAttribute;
import Ada.FractionAttribute;
import Ada.FunctionCall;
import Ada.GreaterThanOperator;
import Ada.GreaterThanOrEqualOperator;
import Ada.Identifier;
import Ada.IdentityAttribute;
import Ada.IfExpression;
import Ada.ImageAttribute;
import Ada.ImplementationDefinedAttribute;
import Ada.ImplementationDefinedPragma;
import Ada.ImportPragma;
import Ada.InMembershipTest;
import Ada.IndependentComponentsPragma;
import Ada.IndependentPragma;
import Ada.IndexedComponent;
import Ada.InlinePragma;
import Ada.InputAttribute;
import Ada.InspectionPointPragma;
import Ada.IntegerLiteral;
import Ada.InterruptHandlerPragma;
import Ada.InterruptPriorityPragma;
import Ada.LastAttribute;
import Ada.LastBitAttribute;
import Ada.LeadingPartAttribute;
import Ada.LengthAttribute;
import Ada.LessThanOperator;
import Ada.LessThanOrEqualOperator;
import Ada.LinkerOptionsPragma;
import Ada.ListPragma;
import Ada.LockingPolicyPragma;
import Ada.MachineAttribute;
import Ada.MachineEmaxAttribute;
import Ada.MachineEminAttribute;
import Ada.MachineMantissaAttribute;
import Ada.MachineOverflowsAttribute;
import Ada.MachineRadixAttribute;
import Ada.MachineRoundingAttribute;
import Ada.MachineRoundsAttribute;
import Ada.MaxAlignmentForAllocationAttribute;
import Ada.MaxAttribute;
import Ada.MaxSizeInStorageElementsAttribute;
import Ada.MinAttribute;
import Ada.MinusOperator;
import Ada.ModAttribute;
import Ada.ModOperator;
import Ada.ModelAttribute;
import Ada.ModelEminAttribute;
import Ada.ModelEpsilonAttribute;
import Ada.ModelMantissaAttribute;
import Ada.ModelSmallAttribute;
import Ada.ModulusAttribute;
import Ada.MultiplyOperator;
import Ada.NamedArrayAggregate;
import Ada.NoReturnPragma;
import Ada.NormalizeScalarsPragma;
import Ada.NotAnElement;
import Ada.NotEqualOperator;
import Ada.NotInMembershipTest;
import Ada.NotOperator;
import Ada.NullLiteral;
import Ada.OptimizePragma;
import Ada.OrElseShortCircuit;
import Ada.OrOperator;
import Ada.OthersChoice;
import Ada.OutputAttribute;
import Ada.OverlapsStorageAttribute;
import Ada.PackPragma;
import Ada.PagePragma;
import Ada.ParenthesizedExpression;
import Ada.PartitionElaborationPolicyPragma;
import Ada.PartitionIdAttribute;
import Ada.PlusOperator;
import Ada.PosAttribute;
import Ada.PositionAttribute;
import Ada.PositionalArrayAggregate;
import Ada.PredAttribute;
import Ada.PreelaborableInitializationPragma;
import Ada.PreelaboratePragma;
import Ada.PriorityAttribute;
import Ada.PriorityPragma;
import Ada.PrioritySpecificDispatchingPragma;
import Ada.ProfilePragma;
import Ada.PurePragma;
import Ada.QualifiedExpression;
import Ada.QueuingPolicyPragma;
import Ada.RaiseExpression;
import Ada.RangeAttribute;
import Ada.ReadAttribute;
import Ada.RealLiteral;
import Ada.RecordAggregate;
import Ada.RelativeDeadlinePragma;
import Ada.RemOperator;
import Ada.RemainderAttribute;
import Ada.RemoteCallInterfacePragma;
import Ada.RemoteTypesPragma;
import Ada.RestrictionsPragma;
import Ada.ReviewablePragma;
import Ada.RoundAttribute;
import Ada.RoundingAttribute;
import Ada.SafeFirstAttribute;
import Ada.SafeLastAttribute;
import Ada.ScaleAttribute;
import Ada.ScalingAttribute;
import Ada.SelectedComponent;
import Ada.SharedPassivePragma;
import Ada.SignedZerosAttribute;
import Ada.SizeAttribute;
import Ada.Slice;
import Ada.SmallAttribute;
import Ada.StoragePoolAttribute;
import Ada.StorageSizeAttribute;
import Ada.StorageSizePragma;
import Ada.StreamSizeAttribute;
import Ada.StringLiteral;
import Ada.SuccAttribute;
import Ada.SuppressPragma;
import Ada.TagAttribute;
import Ada.TaskDispatchingPolicyPragma;
import Ada.TerminatedAttribute;
import Ada.TruncationAttribute;
import Ada.TypeConversion;
import Ada.UnaryMinusOperator;
import Ada.UnaryPlusOperator;
import Ada.UnbiasedRoundingAttribute;
import Ada.UncheckedAccessAttribute;
import Ada.UncheckedUnionPragma;
import Ada.UnknownAttribute;
import Ada.UnknownPragma;
import Ada.UnsuppressPragma;
import Ada.ValAttribute;
import Ada.ValidAttribute;
import Ada.ValueAttribute;
import Ada.VersionAttribute;
import Ada.VolatileComponentsPragma;
import Ada.VolatilePragma;
import Ada.WideImageAttribute;
import Ada.WideValueAttribute;
import Ada.WideWideImageAttribute;
import Ada.WideWideValueAttribute;
import Ada.WideWideWidthAttribute;
import Ada.WideWidthAttribute;
import Ada.WidthAttribute;
import Ada.WriteAttribute;
import Ada.XorOperator;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Expression List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.ExpressionListImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getNotAnElement <em>Not An Element</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getDiscreteRangeAttributeReference <em>Discrete Range Attribute Reference</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getDiscreteSimpleExpressionRange <em>Discrete Simple Expression Range</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getOthersChoice <em>Others Choice</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getBoxExpression <em>Box Expression</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getIntegerLiteral <em>Integer Literal</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getRealLiteral <em>Real Literal</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getStringLiteral <em>String Literal</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getAndOperator <em>And Operator</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getOrOperator <em>Or Operator</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getXorOperator <em>Xor Operator</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getEqualOperator <em>Equal Operator</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getNotEqualOperator <em>Not Equal Operator</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getLessThanOperator <em>Less Than Operator</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getLessThanOrEqualOperator <em>Less Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getGreaterThanOperator <em>Greater Than Operator</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getGreaterThanOrEqualOperator <em>Greater Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getPlusOperator <em>Plus Operator</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getMinusOperator <em>Minus Operator</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getConcatenateOperator <em>Concatenate Operator</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getUnaryPlusOperator <em>Unary Plus Operator</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getUnaryMinusOperator <em>Unary Minus Operator</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getMultiplyOperator <em>Multiply Operator</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getDivideOperator <em>Divide Operator</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getModOperator <em>Mod Operator</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getRemOperator <em>Rem Operator</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getExponentiateOperator <em>Exponentiate Operator</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getAbsOperator <em>Abs Operator</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getNotOperator <em>Not Operator</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getCharacterLiteral <em>Character Literal</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getEnumerationLiteral <em>Enumeration Literal</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getExplicitDereference <em>Explicit Dereference</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getFunctionCall <em>Function Call</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getIndexedComponent <em>Indexed Component</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getSlice <em>Slice</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getSelectedComponent <em>Selected Component</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getAccessAttribute <em>Access Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getAddressAttribute <em>Address Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getAdjacentAttribute <em>Adjacent Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getAftAttribute <em>Aft Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getAlignmentAttribute <em>Alignment Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getBaseAttribute <em>Base Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getBitOrderAttribute <em>Bit Order Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getBodyVersionAttribute <em>Body Version Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getCallableAttribute <em>Callable Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getCallerAttribute <em>Caller Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getCeilingAttribute <em>Ceiling Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getClassAttribute <em>Class Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getComponentSizeAttribute <em>Component Size Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getComposeAttribute <em>Compose Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getConstrainedAttribute <em>Constrained Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getCopySignAttribute <em>Copy Sign Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getCountAttribute <em>Count Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getDefiniteAttribute <em>Definite Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getDeltaAttribute <em>Delta Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getDenormAttribute <em>Denorm Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getDigitsAttribute <em>Digits Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getExponentAttribute <em>Exponent Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getExternalTagAttribute <em>External Tag Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getFirstAttribute <em>First Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getFirstBitAttribute <em>First Bit Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getFloorAttribute <em>Floor Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getForeAttribute <em>Fore Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getFractionAttribute <em>Fraction Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getIdentityAttribute <em>Identity Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getImageAttribute <em>Image Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getInputAttribute <em>Input Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getLastAttribute <em>Last Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getLastBitAttribute <em>Last Bit Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getLeadingPartAttribute <em>Leading Part Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getLengthAttribute <em>Length Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getMachineAttribute <em>Machine Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getMachineEmaxAttribute <em>Machine Emax Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getMachineEminAttribute <em>Machine Emin Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getMachineMantissaAttribute <em>Machine Mantissa Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getMachineOverflowsAttribute <em>Machine Overflows Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getMachineRadixAttribute <em>Machine Radix Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getMachineRoundsAttribute <em>Machine Rounds Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getMaxAttribute <em>Max Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getMaxSizeInStorageElementsAttribute <em>Max Size In Storage Elements Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getMinAttribute <em>Min Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getModelAttribute <em>Model Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getModelEminAttribute <em>Model Emin Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getModelEpsilonAttribute <em>Model Epsilon Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getModelMantissaAttribute <em>Model Mantissa Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getModelSmallAttribute <em>Model Small Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getModulusAttribute <em>Modulus Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getOutputAttribute <em>Output Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getPartitionIdAttribute <em>Partition Id Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getPosAttribute <em>Pos Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getPositionAttribute <em>Position Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getPredAttribute <em>Pred Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getRangeAttribute <em>Range Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getReadAttribute <em>Read Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getRemainderAttribute <em>Remainder Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getRoundAttribute <em>Round Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getRoundingAttribute <em>Rounding Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getSafeFirstAttribute <em>Safe First Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getSafeLastAttribute <em>Safe Last Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getScaleAttribute <em>Scale Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getScalingAttribute <em>Scaling Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getSignedZerosAttribute <em>Signed Zeros Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getSizeAttribute <em>Size Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getSmallAttribute <em>Small Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getStoragePoolAttribute <em>Storage Pool Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getStorageSizeAttribute <em>Storage Size Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getSuccAttribute <em>Succ Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getTagAttribute <em>Tag Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getTerminatedAttribute <em>Terminated Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getTruncationAttribute <em>Truncation Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getUnbiasedRoundingAttribute <em>Unbiased Rounding Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getUncheckedAccessAttribute <em>Unchecked Access Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getValAttribute <em>Val Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getValidAttribute <em>Valid Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getValueAttribute <em>Value Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getVersionAttribute <em>Version Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getWideImageAttribute <em>Wide Image Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getWideValueAttribute <em>Wide Value Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getWideWidthAttribute <em>Wide Width Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getWidthAttribute <em>Width Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getWriteAttribute <em>Write Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getMachineRoundingAttribute <em>Machine Rounding Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getModAttribute <em>Mod Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getPriorityAttribute <em>Priority Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getStreamSizeAttribute <em>Stream Size Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getWideWideImageAttribute <em>Wide Wide Image Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getWideWideValueAttribute <em>Wide Wide Value Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getWideWideWidthAttribute <em>Wide Wide Width Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getMaxAlignmentForAllocationAttribute <em>Max Alignment For Allocation Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getOverlapsStorageAttribute <em>Overlaps Storage Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getImplementationDefinedAttribute <em>Implementation Defined Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getUnknownAttribute <em>Unknown Attribute</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getRecordAggregate <em>Record Aggregate</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getExtensionAggregate <em>Extension Aggregate</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getPositionalArrayAggregate <em>Positional Array Aggregate</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getNamedArrayAggregate <em>Named Array Aggregate</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getAndThenShortCircuit <em>And Then Short Circuit</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getOrElseShortCircuit <em>Or Else Short Circuit</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getInMembershipTest <em>In Membership Test</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getNotInMembershipTest <em>Not In Membership Test</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getNullLiteral <em>Null Literal</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getParenthesizedExpression <em>Parenthesized Expression</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getRaiseExpression <em>Raise Expression</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getTypeConversion <em>Type Conversion</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getQualifiedExpression <em>Qualified Expression</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getAllocationFromSubtype <em>Allocation From Subtype</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getAllocationFromQualifiedExpression <em>Allocation From Qualified Expression</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getCaseExpression <em>Case Expression</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getIfExpression <em>If Expression</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getForAllQuantifiedExpression <em>For All Quantified Expression</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getForSomeQuantifiedExpression <em>For Some Quantified Expression</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getAsynchronousPragma <em>Asynchronous Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getAtomicPragma <em>Atomic Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getAttachHandlerPragma <em>Attach Handler Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getControlledPragma <em>Controlled Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getConventionPragma <em>Convention Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getDiscardNamesPragma <em>Discard Names Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getElaboratePragma <em>Elaborate Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getElaborateAllPragma <em>Elaborate All Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getExportPragma <em>Export Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getImportPragma <em>Import Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getInlinePragma <em>Inline Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getInspectionPointPragma <em>Inspection Point Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getLinkerOptionsPragma <em>Linker Options Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getListPragma <em>List Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getLockingPolicyPragma <em>Locking Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getOptimizePragma <em>Optimize Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getPackPragma <em>Pack Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getPagePragma <em>Page Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getPreelaboratePragma <em>Preelaborate Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getPriorityPragma <em>Priority Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getPurePragma <em>Pure Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getRemoteTypesPragma <em>Remote Types Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getRestrictionsPragma <em>Restrictions Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getReviewablePragma <em>Reviewable Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getSharedPassivePragma <em>Shared Passive Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getStorageSizePragma <em>Storage Size Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getSuppressPragma <em>Suppress Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getVolatilePragma <em>Volatile Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getAssertPragma <em>Assert Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getNoReturnPragma <em>No Return Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getProfilePragma <em>Profile Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getUnsuppressPragma <em>Unsuppress Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getCpuPragma <em>Cpu Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getIndependentPragma <em>Independent Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getIndependentComponentsPragma <em>Independent Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}</li>
 *   <li>{@link Ada.impl.ExpressionListImpl#getUnknownPragma <em>Unknown Pragma</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExpressionListImpl extends MinimalEObjectImpl.Container implements ExpressionList {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExpressionListImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getExpressionList();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, AdaPackage.EXPRESSION_LIST__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NotAnElement> getNotAnElement() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_NotAnElement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscreteRangeAttributeReference> getDiscreteRangeAttributeReference() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_DiscreteRangeAttributeReference());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscreteSimpleExpressionRange> getDiscreteSimpleExpressionRange() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_DiscreteSimpleExpressionRange());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OthersChoice> getOthersChoice() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_OthersChoice());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BoxExpression> getBoxExpression() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_BoxExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IntegerLiteral> getIntegerLiteral() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_IntegerLiteral());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RealLiteral> getRealLiteral() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_RealLiteral());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StringLiteral> getStringLiteral() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_StringLiteral());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Identifier> getIdentifier() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_Identifier());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AndOperator> getAndOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_AndOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OrOperator> getOrOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_OrOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<XorOperator> getXorOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_XorOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EqualOperator> getEqualOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_EqualOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NotEqualOperator> getNotEqualOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_NotEqualOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LessThanOperator> getLessThanOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_LessThanOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LessThanOrEqualOperator> getLessThanOrEqualOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_LessThanOrEqualOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GreaterThanOperator> getGreaterThanOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_GreaterThanOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GreaterThanOrEqualOperator> getGreaterThanOrEqualOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_GreaterThanOrEqualOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PlusOperator> getPlusOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_PlusOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MinusOperator> getMinusOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_MinusOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConcatenateOperator> getConcatenateOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ConcatenateOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnaryPlusOperator> getUnaryPlusOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_UnaryPlusOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnaryMinusOperator> getUnaryMinusOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_UnaryMinusOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MultiplyOperator> getMultiplyOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_MultiplyOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DivideOperator> getDivideOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_DivideOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModOperator> getModOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ModOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RemOperator> getRemOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_RemOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExponentiateOperator> getExponentiateOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ExponentiateOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AbsOperator> getAbsOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_AbsOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NotOperator> getNotOperator() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_NotOperator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CharacterLiteral> getCharacterLiteral() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_CharacterLiteral());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EnumerationLiteral> getEnumerationLiteral() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_EnumerationLiteral());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExplicitDereference> getExplicitDereference() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ExplicitDereference());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FunctionCall> getFunctionCall() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_FunctionCall());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IndexedComponent> getIndexedComponent() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_IndexedComponent());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Slice> getSlice() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_Slice());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SelectedComponent> getSelectedComponent() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_SelectedComponent());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AccessAttribute> getAccessAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_AccessAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AddressAttribute> getAddressAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_AddressAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AdjacentAttribute> getAdjacentAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_AdjacentAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AftAttribute> getAftAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_AftAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AlignmentAttribute> getAlignmentAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_AlignmentAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BaseAttribute> getBaseAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_BaseAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BitOrderAttribute> getBitOrderAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_BitOrderAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BodyVersionAttribute> getBodyVersionAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_BodyVersionAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CallableAttribute> getCallableAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_CallableAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CallerAttribute> getCallerAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_CallerAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CeilingAttribute> getCeilingAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_CeilingAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ClassAttribute> getClassAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ClassAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComponentSizeAttribute> getComponentSizeAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ComponentSizeAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComposeAttribute> getComposeAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ComposeAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConstrainedAttribute> getConstrainedAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ConstrainedAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CopySignAttribute> getCopySignAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_CopySignAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CountAttribute> getCountAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_CountAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefiniteAttribute> getDefiniteAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_DefiniteAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DeltaAttribute> getDeltaAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_DeltaAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DenormAttribute> getDenormAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_DenormAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DigitsAttribute> getDigitsAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_DigitsAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExponentAttribute> getExponentAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ExponentAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExternalTagAttribute> getExternalTagAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ExternalTagAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FirstAttribute> getFirstAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_FirstAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FirstBitAttribute> getFirstBitAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_FirstBitAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FloorAttribute> getFloorAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_FloorAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ForeAttribute> getForeAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ForeAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FractionAttribute> getFractionAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_FractionAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IdentityAttribute> getIdentityAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_IdentityAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ImageAttribute> getImageAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ImageAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InputAttribute> getInputAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_InputAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LastAttribute> getLastAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_LastAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LastBitAttribute> getLastBitAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_LastBitAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LeadingPartAttribute> getLeadingPartAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_LeadingPartAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LengthAttribute> getLengthAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_LengthAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MachineAttribute> getMachineAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_MachineAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MachineEmaxAttribute> getMachineEmaxAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_MachineEmaxAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MachineEminAttribute> getMachineEminAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_MachineEminAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MachineMantissaAttribute> getMachineMantissaAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_MachineMantissaAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MachineOverflowsAttribute> getMachineOverflowsAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_MachineOverflowsAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MachineRadixAttribute> getMachineRadixAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_MachineRadixAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MachineRoundsAttribute> getMachineRoundsAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_MachineRoundsAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaxAttribute> getMaxAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_MaxAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaxSizeInStorageElementsAttribute> getMaxSizeInStorageElementsAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_MaxSizeInStorageElementsAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MinAttribute> getMinAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_MinAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModelAttribute> getModelAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ModelAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModelEminAttribute> getModelEminAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ModelEminAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModelEpsilonAttribute> getModelEpsilonAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ModelEpsilonAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModelMantissaAttribute> getModelMantissaAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ModelMantissaAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModelSmallAttribute> getModelSmallAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ModelSmallAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModulusAttribute> getModulusAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ModulusAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OutputAttribute> getOutputAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_OutputAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PartitionIdAttribute> getPartitionIdAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_PartitionIdAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PosAttribute> getPosAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_PosAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PositionAttribute> getPositionAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_PositionAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PredAttribute> getPredAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_PredAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RangeAttribute> getRangeAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_RangeAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReadAttribute> getReadAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ReadAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RemainderAttribute> getRemainderAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_RemainderAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RoundAttribute> getRoundAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_RoundAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RoundingAttribute> getRoundingAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_RoundingAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SafeFirstAttribute> getSafeFirstAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_SafeFirstAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SafeLastAttribute> getSafeLastAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_SafeLastAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScaleAttribute> getScaleAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ScaleAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScalingAttribute> getScalingAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ScalingAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SignedZerosAttribute> getSignedZerosAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_SignedZerosAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SizeAttribute> getSizeAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_SizeAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SmallAttribute> getSmallAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_SmallAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StoragePoolAttribute> getStoragePoolAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_StoragePoolAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StorageSizeAttribute> getStorageSizeAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_StorageSizeAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SuccAttribute> getSuccAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_SuccAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TagAttribute> getTagAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_TagAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TerminatedAttribute> getTerminatedAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_TerminatedAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TruncationAttribute> getTruncationAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_TruncationAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnbiasedRoundingAttribute> getUnbiasedRoundingAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_UnbiasedRoundingAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UncheckedAccessAttribute> getUncheckedAccessAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_UncheckedAccessAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ValAttribute> getValAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ValAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ValidAttribute> getValidAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ValidAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ValueAttribute> getValueAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ValueAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VersionAttribute> getVersionAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_VersionAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<WideImageAttribute> getWideImageAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_WideImageAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<WideValueAttribute> getWideValueAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_WideValueAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<WideWidthAttribute> getWideWidthAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_WideWidthAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<WidthAttribute> getWidthAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_WidthAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<WriteAttribute> getWriteAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_WriteAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MachineRoundingAttribute> getMachineRoundingAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_MachineRoundingAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModAttribute> getModAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ModAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PriorityAttribute> getPriorityAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_PriorityAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StreamSizeAttribute> getStreamSizeAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_StreamSizeAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<WideWideImageAttribute> getWideWideImageAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_WideWideImageAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<WideWideValueAttribute> getWideWideValueAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_WideWideValueAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<WideWideWidthAttribute> getWideWideWidthAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_WideWideWidthAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MaxAlignmentForAllocationAttribute> getMaxAlignmentForAllocationAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_MaxAlignmentForAllocationAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OverlapsStorageAttribute> getOverlapsStorageAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_OverlapsStorageAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ImplementationDefinedAttribute> getImplementationDefinedAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ImplementationDefinedAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnknownAttribute> getUnknownAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_UnknownAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RecordAggregate> getRecordAggregate() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_RecordAggregate());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExtensionAggregate> getExtensionAggregate() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ExtensionAggregate());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PositionalArrayAggregate> getPositionalArrayAggregate() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_PositionalArrayAggregate());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NamedArrayAggregate> getNamedArrayAggregate() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_NamedArrayAggregate());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AndThenShortCircuit> getAndThenShortCircuit() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_AndThenShortCircuit());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OrElseShortCircuit> getOrElseShortCircuit() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_OrElseShortCircuit());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InMembershipTest> getInMembershipTest() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_InMembershipTest());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NotInMembershipTest> getNotInMembershipTest() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_NotInMembershipTest());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NullLiteral> getNullLiteral() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_NullLiteral());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ParenthesizedExpression> getParenthesizedExpression() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ParenthesizedExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RaiseExpression> getRaiseExpression() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_RaiseExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TypeConversion> getTypeConversion() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_TypeConversion());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QualifiedExpression> getQualifiedExpression() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_QualifiedExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AllocationFromSubtype> getAllocationFromSubtype() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_AllocationFromSubtype());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AllocationFromQualifiedExpression> getAllocationFromQualifiedExpression() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_AllocationFromQualifiedExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CaseExpression> getCaseExpression() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_CaseExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IfExpression> getIfExpression() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_IfExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ForAllQuantifiedExpression> getForAllQuantifiedExpression() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ForAllQuantifiedExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ForSomeQuantifiedExpression> getForSomeQuantifiedExpression() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ForSomeQuantifiedExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Comment> getComment() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_Comment());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AllCallsRemotePragma> getAllCallsRemotePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_AllCallsRemotePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AsynchronousPragma> getAsynchronousPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_AsynchronousPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AtomicPragma> getAtomicPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_AtomicPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AtomicComponentsPragma> getAtomicComponentsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_AtomicComponentsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AttachHandlerPragma> getAttachHandlerPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_AttachHandlerPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlledPragma> getControlledPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ControlledPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConventionPragma> getConventionPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ConventionPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscardNamesPragma> getDiscardNamesPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_DiscardNamesPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElaboratePragma> getElaboratePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ElaboratePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElaborateAllPragma> getElaborateAllPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ElaborateAllPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElaborateBodyPragma> getElaborateBodyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ElaborateBodyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExportPragma> getExportPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ExportPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ImportPragma> getImportPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ImportPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InlinePragma> getInlinePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_InlinePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InspectionPointPragma> getInspectionPointPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_InspectionPointPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InterruptHandlerPragma> getInterruptHandlerPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_InterruptHandlerPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InterruptPriorityPragma> getInterruptPriorityPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_InterruptPriorityPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LinkerOptionsPragma> getLinkerOptionsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_LinkerOptionsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ListPragma> getListPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ListPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LockingPolicyPragma> getLockingPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_LockingPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NormalizeScalarsPragma> getNormalizeScalarsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_NormalizeScalarsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OptimizePragma> getOptimizePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_OptimizePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackPragma> getPackPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_PackPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PagePragma> getPagePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_PagePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PreelaboratePragma> getPreelaboratePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_PreelaboratePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PriorityPragma> getPriorityPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_PriorityPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PurePragma> getPurePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_PurePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QueuingPolicyPragma> getQueuingPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_QueuingPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RemoteCallInterfacePragma> getRemoteCallInterfacePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_RemoteCallInterfacePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RemoteTypesPragma> getRemoteTypesPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_RemoteTypesPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RestrictionsPragma> getRestrictionsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_RestrictionsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReviewablePragma> getReviewablePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ReviewablePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SharedPassivePragma> getSharedPassivePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_SharedPassivePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StorageSizePragma> getStorageSizePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_StorageSizePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SuppressPragma> getSuppressPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_SuppressPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskDispatchingPolicyPragma> getTaskDispatchingPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_TaskDispatchingPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VolatilePragma> getVolatilePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_VolatilePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VolatileComponentsPragma> getVolatileComponentsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_VolatileComponentsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssertPragma> getAssertPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_AssertPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssertionPolicyPragma> getAssertionPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_AssertionPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DetectBlockingPragma> getDetectBlockingPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_DetectBlockingPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NoReturnPragma> getNoReturnPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_NoReturnPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PartitionElaborationPolicyPragma> getPartitionElaborationPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_PartitionElaborationPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PreelaborableInitializationPragma> getPreelaborableInitializationPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_PreelaborableInitializationPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PrioritySpecificDispatchingPragma> getPrioritySpecificDispatchingPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_PrioritySpecificDispatchingPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProfilePragma> getProfilePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ProfilePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RelativeDeadlinePragma> getRelativeDeadlinePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_RelativeDeadlinePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UncheckedUnionPragma> getUncheckedUnionPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_UncheckedUnionPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnsuppressPragma> getUnsuppressPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_UnsuppressPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefaultStoragePoolPragma> getDefaultStoragePoolPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_DefaultStoragePoolPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DispatchingDomainPragma> getDispatchingDomainPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_DispatchingDomainPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CpuPragma> getCpuPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_CpuPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IndependentPragma> getIndependentPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_IndependentPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IndependentComponentsPragma> getIndependentComponentsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_IndependentComponentsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ImplementationDefinedPragma> getImplementationDefinedPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_ImplementationDefinedPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnknownPragma> getUnknownPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getExpressionList_UnknownPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.EXPRESSION_LIST__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__NOT_AN_ELEMENT:
				return ((InternalEList<?>)getNotAnElement()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__DISCRETE_RANGE_ATTRIBUTE_REFERENCE:
				return ((InternalEList<?>)getDiscreteRangeAttributeReference()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__DISCRETE_SIMPLE_EXPRESSION_RANGE:
				return ((InternalEList<?>)getDiscreteSimpleExpressionRange()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__OTHERS_CHOICE:
				return ((InternalEList<?>)getOthersChoice()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__BOX_EXPRESSION:
				return ((InternalEList<?>)getBoxExpression()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__INTEGER_LITERAL:
				return ((InternalEList<?>)getIntegerLiteral()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__REAL_LITERAL:
				return ((InternalEList<?>)getRealLiteral()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__STRING_LITERAL:
				return ((InternalEList<?>)getStringLiteral()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__IDENTIFIER:
				return ((InternalEList<?>)getIdentifier()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__AND_OPERATOR:
				return ((InternalEList<?>)getAndOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__OR_OPERATOR:
				return ((InternalEList<?>)getOrOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__XOR_OPERATOR:
				return ((InternalEList<?>)getXorOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__EQUAL_OPERATOR:
				return ((InternalEList<?>)getEqualOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__NOT_EQUAL_OPERATOR:
				return ((InternalEList<?>)getNotEqualOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__LESS_THAN_OPERATOR:
				return ((InternalEList<?>)getLessThanOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__LESS_THAN_OR_EQUAL_OPERATOR:
				return ((InternalEList<?>)getLessThanOrEqualOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__GREATER_THAN_OPERATOR:
				return ((InternalEList<?>)getGreaterThanOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__GREATER_THAN_OR_EQUAL_OPERATOR:
				return ((InternalEList<?>)getGreaterThanOrEqualOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__PLUS_OPERATOR:
				return ((InternalEList<?>)getPlusOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__MINUS_OPERATOR:
				return ((InternalEList<?>)getMinusOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__CONCATENATE_OPERATOR:
				return ((InternalEList<?>)getConcatenateOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__UNARY_PLUS_OPERATOR:
				return ((InternalEList<?>)getUnaryPlusOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__UNARY_MINUS_OPERATOR:
				return ((InternalEList<?>)getUnaryMinusOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__MULTIPLY_OPERATOR:
				return ((InternalEList<?>)getMultiplyOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__DIVIDE_OPERATOR:
				return ((InternalEList<?>)getDivideOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__MOD_OPERATOR:
				return ((InternalEList<?>)getModOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__REM_OPERATOR:
				return ((InternalEList<?>)getRemOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__EXPONENTIATE_OPERATOR:
				return ((InternalEList<?>)getExponentiateOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__ABS_OPERATOR:
				return ((InternalEList<?>)getAbsOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__NOT_OPERATOR:
				return ((InternalEList<?>)getNotOperator()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__CHARACTER_LITERAL:
				return ((InternalEList<?>)getCharacterLiteral()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__ENUMERATION_LITERAL:
				return ((InternalEList<?>)getEnumerationLiteral()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__EXPLICIT_DEREFERENCE:
				return ((InternalEList<?>)getExplicitDereference()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__FUNCTION_CALL:
				return ((InternalEList<?>)getFunctionCall()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__INDEXED_COMPONENT:
				return ((InternalEList<?>)getIndexedComponent()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__SLICE:
				return ((InternalEList<?>)getSlice()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__SELECTED_COMPONENT:
				return ((InternalEList<?>)getSelectedComponent()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__ACCESS_ATTRIBUTE:
				return ((InternalEList<?>)getAccessAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__ADDRESS_ATTRIBUTE:
				return ((InternalEList<?>)getAddressAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__ADJACENT_ATTRIBUTE:
				return ((InternalEList<?>)getAdjacentAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__AFT_ATTRIBUTE:
				return ((InternalEList<?>)getAftAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__ALIGNMENT_ATTRIBUTE:
				return ((InternalEList<?>)getAlignmentAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__BASE_ATTRIBUTE:
				return ((InternalEList<?>)getBaseAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__BIT_ORDER_ATTRIBUTE:
				return ((InternalEList<?>)getBitOrderAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__BODY_VERSION_ATTRIBUTE:
				return ((InternalEList<?>)getBodyVersionAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__CALLABLE_ATTRIBUTE:
				return ((InternalEList<?>)getCallableAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__CALLER_ATTRIBUTE:
				return ((InternalEList<?>)getCallerAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__CEILING_ATTRIBUTE:
				return ((InternalEList<?>)getCeilingAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__CLASS_ATTRIBUTE:
				return ((InternalEList<?>)getClassAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__COMPONENT_SIZE_ATTRIBUTE:
				return ((InternalEList<?>)getComponentSizeAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__COMPOSE_ATTRIBUTE:
				return ((InternalEList<?>)getComposeAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__CONSTRAINED_ATTRIBUTE:
				return ((InternalEList<?>)getConstrainedAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__COPY_SIGN_ATTRIBUTE:
				return ((InternalEList<?>)getCopySignAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__COUNT_ATTRIBUTE:
				return ((InternalEList<?>)getCountAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__DEFINITE_ATTRIBUTE:
				return ((InternalEList<?>)getDefiniteAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__DELTA_ATTRIBUTE:
				return ((InternalEList<?>)getDeltaAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__DENORM_ATTRIBUTE:
				return ((InternalEList<?>)getDenormAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__DIGITS_ATTRIBUTE:
				return ((InternalEList<?>)getDigitsAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__EXPONENT_ATTRIBUTE:
				return ((InternalEList<?>)getExponentAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__EXTERNAL_TAG_ATTRIBUTE:
				return ((InternalEList<?>)getExternalTagAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__FIRST_ATTRIBUTE:
				return ((InternalEList<?>)getFirstAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__FIRST_BIT_ATTRIBUTE:
				return ((InternalEList<?>)getFirstBitAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__FLOOR_ATTRIBUTE:
				return ((InternalEList<?>)getFloorAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__FORE_ATTRIBUTE:
				return ((InternalEList<?>)getForeAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__FRACTION_ATTRIBUTE:
				return ((InternalEList<?>)getFractionAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__IDENTITY_ATTRIBUTE:
				return ((InternalEList<?>)getIdentityAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__IMAGE_ATTRIBUTE:
				return ((InternalEList<?>)getImageAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__INPUT_ATTRIBUTE:
				return ((InternalEList<?>)getInputAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__LAST_ATTRIBUTE:
				return ((InternalEList<?>)getLastAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__LAST_BIT_ATTRIBUTE:
				return ((InternalEList<?>)getLastBitAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__LEADING_PART_ATTRIBUTE:
				return ((InternalEList<?>)getLeadingPartAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__LENGTH_ATTRIBUTE:
				return ((InternalEList<?>)getLengthAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__MACHINE_ATTRIBUTE:
				return ((InternalEList<?>)getMachineAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__MACHINE_EMAX_ATTRIBUTE:
				return ((InternalEList<?>)getMachineEmaxAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__MACHINE_EMIN_ATTRIBUTE:
				return ((InternalEList<?>)getMachineEminAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__MACHINE_MANTISSA_ATTRIBUTE:
				return ((InternalEList<?>)getMachineMantissaAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__MACHINE_OVERFLOWS_ATTRIBUTE:
				return ((InternalEList<?>)getMachineOverflowsAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__MACHINE_RADIX_ATTRIBUTE:
				return ((InternalEList<?>)getMachineRadixAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__MACHINE_ROUNDS_ATTRIBUTE:
				return ((InternalEList<?>)getMachineRoundsAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__MAX_ATTRIBUTE:
				return ((InternalEList<?>)getMaxAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE:
				return ((InternalEList<?>)getMaxSizeInStorageElementsAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__MIN_ATTRIBUTE:
				return ((InternalEList<?>)getMinAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__MODEL_ATTRIBUTE:
				return ((InternalEList<?>)getModelAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__MODEL_EMIN_ATTRIBUTE:
				return ((InternalEList<?>)getModelEminAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__MODEL_EPSILON_ATTRIBUTE:
				return ((InternalEList<?>)getModelEpsilonAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__MODEL_MANTISSA_ATTRIBUTE:
				return ((InternalEList<?>)getModelMantissaAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__MODEL_SMALL_ATTRIBUTE:
				return ((InternalEList<?>)getModelSmallAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__MODULUS_ATTRIBUTE:
				return ((InternalEList<?>)getModulusAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__OUTPUT_ATTRIBUTE:
				return ((InternalEList<?>)getOutputAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__PARTITION_ID_ATTRIBUTE:
				return ((InternalEList<?>)getPartitionIdAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__POS_ATTRIBUTE:
				return ((InternalEList<?>)getPosAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__POSITION_ATTRIBUTE:
				return ((InternalEList<?>)getPositionAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__PRED_ATTRIBUTE:
				return ((InternalEList<?>)getPredAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__RANGE_ATTRIBUTE:
				return ((InternalEList<?>)getRangeAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__READ_ATTRIBUTE:
				return ((InternalEList<?>)getReadAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__REMAINDER_ATTRIBUTE:
				return ((InternalEList<?>)getRemainderAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__ROUND_ATTRIBUTE:
				return ((InternalEList<?>)getRoundAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__ROUNDING_ATTRIBUTE:
				return ((InternalEList<?>)getRoundingAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__SAFE_FIRST_ATTRIBUTE:
				return ((InternalEList<?>)getSafeFirstAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__SAFE_LAST_ATTRIBUTE:
				return ((InternalEList<?>)getSafeLastAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__SCALE_ATTRIBUTE:
				return ((InternalEList<?>)getScaleAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__SCALING_ATTRIBUTE:
				return ((InternalEList<?>)getScalingAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__SIGNED_ZEROS_ATTRIBUTE:
				return ((InternalEList<?>)getSignedZerosAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__SIZE_ATTRIBUTE:
				return ((InternalEList<?>)getSizeAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__SMALL_ATTRIBUTE:
				return ((InternalEList<?>)getSmallAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__STORAGE_POOL_ATTRIBUTE:
				return ((InternalEList<?>)getStoragePoolAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__STORAGE_SIZE_ATTRIBUTE:
				return ((InternalEList<?>)getStorageSizeAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__SUCC_ATTRIBUTE:
				return ((InternalEList<?>)getSuccAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__TAG_ATTRIBUTE:
				return ((InternalEList<?>)getTagAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__TERMINATED_ATTRIBUTE:
				return ((InternalEList<?>)getTerminatedAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__TRUNCATION_ATTRIBUTE:
				return ((InternalEList<?>)getTruncationAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__UNBIASED_ROUNDING_ATTRIBUTE:
				return ((InternalEList<?>)getUnbiasedRoundingAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__UNCHECKED_ACCESS_ATTRIBUTE:
				return ((InternalEList<?>)getUncheckedAccessAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__VAL_ATTRIBUTE:
				return ((InternalEList<?>)getValAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__VALID_ATTRIBUTE:
				return ((InternalEList<?>)getValidAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__VALUE_ATTRIBUTE:
				return ((InternalEList<?>)getValueAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__VERSION_ATTRIBUTE:
				return ((InternalEList<?>)getVersionAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__WIDE_IMAGE_ATTRIBUTE:
				return ((InternalEList<?>)getWideImageAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__WIDE_VALUE_ATTRIBUTE:
				return ((InternalEList<?>)getWideValueAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__WIDE_WIDTH_ATTRIBUTE:
				return ((InternalEList<?>)getWideWidthAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__WIDTH_ATTRIBUTE:
				return ((InternalEList<?>)getWidthAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__WRITE_ATTRIBUTE:
				return ((InternalEList<?>)getWriteAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__MACHINE_ROUNDING_ATTRIBUTE:
				return ((InternalEList<?>)getMachineRoundingAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__MOD_ATTRIBUTE:
				return ((InternalEList<?>)getModAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__PRIORITY_ATTRIBUTE:
				return ((InternalEList<?>)getPriorityAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__STREAM_SIZE_ATTRIBUTE:
				return ((InternalEList<?>)getStreamSizeAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__WIDE_WIDE_IMAGE_ATTRIBUTE:
				return ((InternalEList<?>)getWideWideImageAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__WIDE_WIDE_VALUE_ATTRIBUTE:
				return ((InternalEList<?>)getWideWideValueAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__WIDE_WIDE_WIDTH_ATTRIBUTE:
				return ((InternalEList<?>)getWideWideWidthAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE:
				return ((InternalEList<?>)getMaxAlignmentForAllocationAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__OVERLAPS_STORAGE_ATTRIBUTE:
				return ((InternalEList<?>)getOverlapsStorageAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__IMPLEMENTATION_DEFINED_ATTRIBUTE:
				return ((InternalEList<?>)getImplementationDefinedAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__UNKNOWN_ATTRIBUTE:
				return ((InternalEList<?>)getUnknownAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__RECORD_AGGREGATE:
				return ((InternalEList<?>)getRecordAggregate()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__EXTENSION_AGGREGATE:
				return ((InternalEList<?>)getExtensionAggregate()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__POSITIONAL_ARRAY_AGGREGATE:
				return ((InternalEList<?>)getPositionalArrayAggregate()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__NAMED_ARRAY_AGGREGATE:
				return ((InternalEList<?>)getNamedArrayAggregate()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__AND_THEN_SHORT_CIRCUIT:
				return ((InternalEList<?>)getAndThenShortCircuit()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__OR_ELSE_SHORT_CIRCUIT:
				return ((InternalEList<?>)getOrElseShortCircuit()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__IN_MEMBERSHIP_TEST:
				return ((InternalEList<?>)getInMembershipTest()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__NOT_IN_MEMBERSHIP_TEST:
				return ((InternalEList<?>)getNotInMembershipTest()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__NULL_LITERAL:
				return ((InternalEList<?>)getNullLiteral()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__PARENTHESIZED_EXPRESSION:
				return ((InternalEList<?>)getParenthesizedExpression()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__RAISE_EXPRESSION:
				return ((InternalEList<?>)getRaiseExpression()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__TYPE_CONVERSION:
				return ((InternalEList<?>)getTypeConversion()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__QUALIFIED_EXPRESSION:
				return ((InternalEList<?>)getQualifiedExpression()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__ALLOCATION_FROM_SUBTYPE:
				return ((InternalEList<?>)getAllocationFromSubtype()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__ALLOCATION_FROM_QUALIFIED_EXPRESSION:
				return ((InternalEList<?>)getAllocationFromQualifiedExpression()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__CASE_EXPRESSION:
				return ((InternalEList<?>)getCaseExpression()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__IF_EXPRESSION:
				return ((InternalEList<?>)getIfExpression()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__FOR_ALL_QUANTIFIED_EXPRESSION:
				return ((InternalEList<?>)getForAllQuantifiedExpression()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__FOR_SOME_QUANTIFIED_EXPRESSION:
				return ((InternalEList<?>)getForSomeQuantifiedExpression()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__COMMENT:
				return ((InternalEList<?>)getComment()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__ALL_CALLS_REMOTE_PRAGMA:
				return ((InternalEList<?>)getAllCallsRemotePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__ASYNCHRONOUS_PRAGMA:
				return ((InternalEList<?>)getAsynchronousPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__ATOMIC_PRAGMA:
				return ((InternalEList<?>)getAtomicPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__ATOMIC_COMPONENTS_PRAGMA:
				return ((InternalEList<?>)getAtomicComponentsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__ATTACH_HANDLER_PRAGMA:
				return ((InternalEList<?>)getAttachHandlerPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__CONTROLLED_PRAGMA:
				return ((InternalEList<?>)getControlledPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__CONVENTION_PRAGMA:
				return ((InternalEList<?>)getConventionPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__DISCARD_NAMES_PRAGMA:
				return ((InternalEList<?>)getDiscardNamesPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__ELABORATE_PRAGMA:
				return ((InternalEList<?>)getElaboratePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__ELABORATE_ALL_PRAGMA:
				return ((InternalEList<?>)getElaborateAllPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__ELABORATE_BODY_PRAGMA:
				return ((InternalEList<?>)getElaborateBodyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__EXPORT_PRAGMA:
				return ((InternalEList<?>)getExportPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__IMPORT_PRAGMA:
				return ((InternalEList<?>)getImportPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__INLINE_PRAGMA:
				return ((InternalEList<?>)getInlinePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__INSPECTION_POINT_PRAGMA:
				return ((InternalEList<?>)getInspectionPointPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__INTERRUPT_HANDLER_PRAGMA:
				return ((InternalEList<?>)getInterruptHandlerPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__INTERRUPT_PRIORITY_PRAGMA:
				return ((InternalEList<?>)getInterruptPriorityPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__LINKER_OPTIONS_PRAGMA:
				return ((InternalEList<?>)getLinkerOptionsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__LIST_PRAGMA:
				return ((InternalEList<?>)getListPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__LOCKING_POLICY_PRAGMA:
				return ((InternalEList<?>)getLockingPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__NORMALIZE_SCALARS_PRAGMA:
				return ((InternalEList<?>)getNormalizeScalarsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__OPTIMIZE_PRAGMA:
				return ((InternalEList<?>)getOptimizePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__PACK_PRAGMA:
				return ((InternalEList<?>)getPackPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__PAGE_PRAGMA:
				return ((InternalEList<?>)getPagePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__PREELABORATE_PRAGMA:
				return ((InternalEList<?>)getPreelaboratePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__PRIORITY_PRAGMA:
				return ((InternalEList<?>)getPriorityPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__PURE_PRAGMA:
				return ((InternalEList<?>)getPurePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__QUEUING_POLICY_PRAGMA:
				return ((InternalEList<?>)getQueuingPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				return ((InternalEList<?>)getRemoteCallInterfacePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__REMOTE_TYPES_PRAGMA:
				return ((InternalEList<?>)getRemoteTypesPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__RESTRICTIONS_PRAGMA:
				return ((InternalEList<?>)getRestrictionsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__REVIEWABLE_PRAGMA:
				return ((InternalEList<?>)getReviewablePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__SHARED_PASSIVE_PRAGMA:
				return ((InternalEList<?>)getSharedPassivePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__STORAGE_SIZE_PRAGMA:
				return ((InternalEList<?>)getStorageSizePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__SUPPRESS_PRAGMA:
				return ((InternalEList<?>)getSuppressPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				return ((InternalEList<?>)getTaskDispatchingPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__VOLATILE_PRAGMA:
				return ((InternalEList<?>)getVolatilePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__VOLATILE_COMPONENTS_PRAGMA:
				return ((InternalEList<?>)getVolatileComponentsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__ASSERT_PRAGMA:
				return ((InternalEList<?>)getAssertPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__ASSERTION_POLICY_PRAGMA:
				return ((InternalEList<?>)getAssertionPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__DETECT_BLOCKING_PRAGMA:
				return ((InternalEList<?>)getDetectBlockingPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__NO_RETURN_PRAGMA:
				return ((InternalEList<?>)getNoReturnPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				return ((InternalEList<?>)getPartitionElaborationPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				return ((InternalEList<?>)getPreelaborableInitializationPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return ((InternalEList<?>)getPrioritySpecificDispatchingPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__PROFILE_PRAGMA:
				return ((InternalEList<?>)getProfilePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__RELATIVE_DEADLINE_PRAGMA:
				return ((InternalEList<?>)getRelativeDeadlinePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__UNCHECKED_UNION_PRAGMA:
				return ((InternalEList<?>)getUncheckedUnionPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__UNSUPPRESS_PRAGMA:
				return ((InternalEList<?>)getUnsuppressPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				return ((InternalEList<?>)getDefaultStoragePoolPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__DISPATCHING_DOMAIN_PRAGMA:
				return ((InternalEList<?>)getDispatchingDomainPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__CPU_PRAGMA:
				return ((InternalEList<?>)getCpuPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__INDEPENDENT_PRAGMA:
				return ((InternalEList<?>)getIndependentPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				return ((InternalEList<?>)getIndependentComponentsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				return ((InternalEList<?>)getImplementationDefinedPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.EXPRESSION_LIST__UNKNOWN_PRAGMA:
				return ((InternalEList<?>)getUnknownPragma()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.EXPRESSION_LIST__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case AdaPackage.EXPRESSION_LIST__NOT_AN_ELEMENT:
				return getNotAnElement();
			case AdaPackage.EXPRESSION_LIST__DISCRETE_RANGE_ATTRIBUTE_REFERENCE:
				return getDiscreteRangeAttributeReference();
			case AdaPackage.EXPRESSION_LIST__DISCRETE_SIMPLE_EXPRESSION_RANGE:
				return getDiscreteSimpleExpressionRange();
			case AdaPackage.EXPRESSION_LIST__OTHERS_CHOICE:
				return getOthersChoice();
			case AdaPackage.EXPRESSION_LIST__BOX_EXPRESSION:
				return getBoxExpression();
			case AdaPackage.EXPRESSION_LIST__INTEGER_LITERAL:
				return getIntegerLiteral();
			case AdaPackage.EXPRESSION_LIST__REAL_LITERAL:
				return getRealLiteral();
			case AdaPackage.EXPRESSION_LIST__STRING_LITERAL:
				return getStringLiteral();
			case AdaPackage.EXPRESSION_LIST__IDENTIFIER:
				return getIdentifier();
			case AdaPackage.EXPRESSION_LIST__AND_OPERATOR:
				return getAndOperator();
			case AdaPackage.EXPRESSION_LIST__OR_OPERATOR:
				return getOrOperator();
			case AdaPackage.EXPRESSION_LIST__XOR_OPERATOR:
				return getXorOperator();
			case AdaPackage.EXPRESSION_LIST__EQUAL_OPERATOR:
				return getEqualOperator();
			case AdaPackage.EXPRESSION_LIST__NOT_EQUAL_OPERATOR:
				return getNotEqualOperator();
			case AdaPackage.EXPRESSION_LIST__LESS_THAN_OPERATOR:
				return getLessThanOperator();
			case AdaPackage.EXPRESSION_LIST__LESS_THAN_OR_EQUAL_OPERATOR:
				return getLessThanOrEqualOperator();
			case AdaPackage.EXPRESSION_LIST__GREATER_THAN_OPERATOR:
				return getGreaterThanOperator();
			case AdaPackage.EXPRESSION_LIST__GREATER_THAN_OR_EQUAL_OPERATOR:
				return getGreaterThanOrEqualOperator();
			case AdaPackage.EXPRESSION_LIST__PLUS_OPERATOR:
				return getPlusOperator();
			case AdaPackage.EXPRESSION_LIST__MINUS_OPERATOR:
				return getMinusOperator();
			case AdaPackage.EXPRESSION_LIST__CONCATENATE_OPERATOR:
				return getConcatenateOperator();
			case AdaPackage.EXPRESSION_LIST__UNARY_PLUS_OPERATOR:
				return getUnaryPlusOperator();
			case AdaPackage.EXPRESSION_LIST__UNARY_MINUS_OPERATOR:
				return getUnaryMinusOperator();
			case AdaPackage.EXPRESSION_LIST__MULTIPLY_OPERATOR:
				return getMultiplyOperator();
			case AdaPackage.EXPRESSION_LIST__DIVIDE_OPERATOR:
				return getDivideOperator();
			case AdaPackage.EXPRESSION_LIST__MOD_OPERATOR:
				return getModOperator();
			case AdaPackage.EXPRESSION_LIST__REM_OPERATOR:
				return getRemOperator();
			case AdaPackage.EXPRESSION_LIST__EXPONENTIATE_OPERATOR:
				return getExponentiateOperator();
			case AdaPackage.EXPRESSION_LIST__ABS_OPERATOR:
				return getAbsOperator();
			case AdaPackage.EXPRESSION_LIST__NOT_OPERATOR:
				return getNotOperator();
			case AdaPackage.EXPRESSION_LIST__CHARACTER_LITERAL:
				return getCharacterLiteral();
			case AdaPackage.EXPRESSION_LIST__ENUMERATION_LITERAL:
				return getEnumerationLiteral();
			case AdaPackage.EXPRESSION_LIST__EXPLICIT_DEREFERENCE:
				return getExplicitDereference();
			case AdaPackage.EXPRESSION_LIST__FUNCTION_CALL:
				return getFunctionCall();
			case AdaPackage.EXPRESSION_LIST__INDEXED_COMPONENT:
				return getIndexedComponent();
			case AdaPackage.EXPRESSION_LIST__SLICE:
				return getSlice();
			case AdaPackage.EXPRESSION_LIST__SELECTED_COMPONENT:
				return getSelectedComponent();
			case AdaPackage.EXPRESSION_LIST__ACCESS_ATTRIBUTE:
				return getAccessAttribute();
			case AdaPackage.EXPRESSION_LIST__ADDRESS_ATTRIBUTE:
				return getAddressAttribute();
			case AdaPackage.EXPRESSION_LIST__ADJACENT_ATTRIBUTE:
				return getAdjacentAttribute();
			case AdaPackage.EXPRESSION_LIST__AFT_ATTRIBUTE:
				return getAftAttribute();
			case AdaPackage.EXPRESSION_LIST__ALIGNMENT_ATTRIBUTE:
				return getAlignmentAttribute();
			case AdaPackage.EXPRESSION_LIST__BASE_ATTRIBUTE:
				return getBaseAttribute();
			case AdaPackage.EXPRESSION_LIST__BIT_ORDER_ATTRIBUTE:
				return getBitOrderAttribute();
			case AdaPackage.EXPRESSION_LIST__BODY_VERSION_ATTRIBUTE:
				return getBodyVersionAttribute();
			case AdaPackage.EXPRESSION_LIST__CALLABLE_ATTRIBUTE:
				return getCallableAttribute();
			case AdaPackage.EXPRESSION_LIST__CALLER_ATTRIBUTE:
				return getCallerAttribute();
			case AdaPackage.EXPRESSION_LIST__CEILING_ATTRIBUTE:
				return getCeilingAttribute();
			case AdaPackage.EXPRESSION_LIST__CLASS_ATTRIBUTE:
				return getClassAttribute();
			case AdaPackage.EXPRESSION_LIST__COMPONENT_SIZE_ATTRIBUTE:
				return getComponentSizeAttribute();
			case AdaPackage.EXPRESSION_LIST__COMPOSE_ATTRIBUTE:
				return getComposeAttribute();
			case AdaPackage.EXPRESSION_LIST__CONSTRAINED_ATTRIBUTE:
				return getConstrainedAttribute();
			case AdaPackage.EXPRESSION_LIST__COPY_SIGN_ATTRIBUTE:
				return getCopySignAttribute();
			case AdaPackage.EXPRESSION_LIST__COUNT_ATTRIBUTE:
				return getCountAttribute();
			case AdaPackage.EXPRESSION_LIST__DEFINITE_ATTRIBUTE:
				return getDefiniteAttribute();
			case AdaPackage.EXPRESSION_LIST__DELTA_ATTRIBUTE:
				return getDeltaAttribute();
			case AdaPackage.EXPRESSION_LIST__DENORM_ATTRIBUTE:
				return getDenormAttribute();
			case AdaPackage.EXPRESSION_LIST__DIGITS_ATTRIBUTE:
				return getDigitsAttribute();
			case AdaPackage.EXPRESSION_LIST__EXPONENT_ATTRIBUTE:
				return getExponentAttribute();
			case AdaPackage.EXPRESSION_LIST__EXTERNAL_TAG_ATTRIBUTE:
				return getExternalTagAttribute();
			case AdaPackage.EXPRESSION_LIST__FIRST_ATTRIBUTE:
				return getFirstAttribute();
			case AdaPackage.EXPRESSION_LIST__FIRST_BIT_ATTRIBUTE:
				return getFirstBitAttribute();
			case AdaPackage.EXPRESSION_LIST__FLOOR_ATTRIBUTE:
				return getFloorAttribute();
			case AdaPackage.EXPRESSION_LIST__FORE_ATTRIBUTE:
				return getForeAttribute();
			case AdaPackage.EXPRESSION_LIST__FRACTION_ATTRIBUTE:
				return getFractionAttribute();
			case AdaPackage.EXPRESSION_LIST__IDENTITY_ATTRIBUTE:
				return getIdentityAttribute();
			case AdaPackage.EXPRESSION_LIST__IMAGE_ATTRIBUTE:
				return getImageAttribute();
			case AdaPackage.EXPRESSION_LIST__INPUT_ATTRIBUTE:
				return getInputAttribute();
			case AdaPackage.EXPRESSION_LIST__LAST_ATTRIBUTE:
				return getLastAttribute();
			case AdaPackage.EXPRESSION_LIST__LAST_BIT_ATTRIBUTE:
				return getLastBitAttribute();
			case AdaPackage.EXPRESSION_LIST__LEADING_PART_ATTRIBUTE:
				return getLeadingPartAttribute();
			case AdaPackage.EXPRESSION_LIST__LENGTH_ATTRIBUTE:
				return getLengthAttribute();
			case AdaPackage.EXPRESSION_LIST__MACHINE_ATTRIBUTE:
				return getMachineAttribute();
			case AdaPackage.EXPRESSION_LIST__MACHINE_EMAX_ATTRIBUTE:
				return getMachineEmaxAttribute();
			case AdaPackage.EXPRESSION_LIST__MACHINE_EMIN_ATTRIBUTE:
				return getMachineEminAttribute();
			case AdaPackage.EXPRESSION_LIST__MACHINE_MANTISSA_ATTRIBUTE:
				return getMachineMantissaAttribute();
			case AdaPackage.EXPRESSION_LIST__MACHINE_OVERFLOWS_ATTRIBUTE:
				return getMachineOverflowsAttribute();
			case AdaPackage.EXPRESSION_LIST__MACHINE_RADIX_ATTRIBUTE:
				return getMachineRadixAttribute();
			case AdaPackage.EXPRESSION_LIST__MACHINE_ROUNDS_ATTRIBUTE:
				return getMachineRoundsAttribute();
			case AdaPackage.EXPRESSION_LIST__MAX_ATTRIBUTE:
				return getMaxAttribute();
			case AdaPackage.EXPRESSION_LIST__MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE:
				return getMaxSizeInStorageElementsAttribute();
			case AdaPackage.EXPRESSION_LIST__MIN_ATTRIBUTE:
				return getMinAttribute();
			case AdaPackage.EXPRESSION_LIST__MODEL_ATTRIBUTE:
				return getModelAttribute();
			case AdaPackage.EXPRESSION_LIST__MODEL_EMIN_ATTRIBUTE:
				return getModelEminAttribute();
			case AdaPackage.EXPRESSION_LIST__MODEL_EPSILON_ATTRIBUTE:
				return getModelEpsilonAttribute();
			case AdaPackage.EXPRESSION_LIST__MODEL_MANTISSA_ATTRIBUTE:
				return getModelMantissaAttribute();
			case AdaPackage.EXPRESSION_LIST__MODEL_SMALL_ATTRIBUTE:
				return getModelSmallAttribute();
			case AdaPackage.EXPRESSION_LIST__MODULUS_ATTRIBUTE:
				return getModulusAttribute();
			case AdaPackage.EXPRESSION_LIST__OUTPUT_ATTRIBUTE:
				return getOutputAttribute();
			case AdaPackage.EXPRESSION_LIST__PARTITION_ID_ATTRIBUTE:
				return getPartitionIdAttribute();
			case AdaPackage.EXPRESSION_LIST__POS_ATTRIBUTE:
				return getPosAttribute();
			case AdaPackage.EXPRESSION_LIST__POSITION_ATTRIBUTE:
				return getPositionAttribute();
			case AdaPackage.EXPRESSION_LIST__PRED_ATTRIBUTE:
				return getPredAttribute();
			case AdaPackage.EXPRESSION_LIST__RANGE_ATTRIBUTE:
				return getRangeAttribute();
			case AdaPackage.EXPRESSION_LIST__READ_ATTRIBUTE:
				return getReadAttribute();
			case AdaPackage.EXPRESSION_LIST__REMAINDER_ATTRIBUTE:
				return getRemainderAttribute();
			case AdaPackage.EXPRESSION_LIST__ROUND_ATTRIBUTE:
				return getRoundAttribute();
			case AdaPackage.EXPRESSION_LIST__ROUNDING_ATTRIBUTE:
				return getRoundingAttribute();
			case AdaPackage.EXPRESSION_LIST__SAFE_FIRST_ATTRIBUTE:
				return getSafeFirstAttribute();
			case AdaPackage.EXPRESSION_LIST__SAFE_LAST_ATTRIBUTE:
				return getSafeLastAttribute();
			case AdaPackage.EXPRESSION_LIST__SCALE_ATTRIBUTE:
				return getScaleAttribute();
			case AdaPackage.EXPRESSION_LIST__SCALING_ATTRIBUTE:
				return getScalingAttribute();
			case AdaPackage.EXPRESSION_LIST__SIGNED_ZEROS_ATTRIBUTE:
				return getSignedZerosAttribute();
			case AdaPackage.EXPRESSION_LIST__SIZE_ATTRIBUTE:
				return getSizeAttribute();
			case AdaPackage.EXPRESSION_LIST__SMALL_ATTRIBUTE:
				return getSmallAttribute();
			case AdaPackage.EXPRESSION_LIST__STORAGE_POOL_ATTRIBUTE:
				return getStoragePoolAttribute();
			case AdaPackage.EXPRESSION_LIST__STORAGE_SIZE_ATTRIBUTE:
				return getStorageSizeAttribute();
			case AdaPackage.EXPRESSION_LIST__SUCC_ATTRIBUTE:
				return getSuccAttribute();
			case AdaPackage.EXPRESSION_LIST__TAG_ATTRIBUTE:
				return getTagAttribute();
			case AdaPackage.EXPRESSION_LIST__TERMINATED_ATTRIBUTE:
				return getTerminatedAttribute();
			case AdaPackage.EXPRESSION_LIST__TRUNCATION_ATTRIBUTE:
				return getTruncationAttribute();
			case AdaPackage.EXPRESSION_LIST__UNBIASED_ROUNDING_ATTRIBUTE:
				return getUnbiasedRoundingAttribute();
			case AdaPackage.EXPRESSION_LIST__UNCHECKED_ACCESS_ATTRIBUTE:
				return getUncheckedAccessAttribute();
			case AdaPackage.EXPRESSION_LIST__VAL_ATTRIBUTE:
				return getValAttribute();
			case AdaPackage.EXPRESSION_LIST__VALID_ATTRIBUTE:
				return getValidAttribute();
			case AdaPackage.EXPRESSION_LIST__VALUE_ATTRIBUTE:
				return getValueAttribute();
			case AdaPackage.EXPRESSION_LIST__VERSION_ATTRIBUTE:
				return getVersionAttribute();
			case AdaPackage.EXPRESSION_LIST__WIDE_IMAGE_ATTRIBUTE:
				return getWideImageAttribute();
			case AdaPackage.EXPRESSION_LIST__WIDE_VALUE_ATTRIBUTE:
				return getWideValueAttribute();
			case AdaPackage.EXPRESSION_LIST__WIDE_WIDTH_ATTRIBUTE:
				return getWideWidthAttribute();
			case AdaPackage.EXPRESSION_LIST__WIDTH_ATTRIBUTE:
				return getWidthAttribute();
			case AdaPackage.EXPRESSION_LIST__WRITE_ATTRIBUTE:
				return getWriteAttribute();
			case AdaPackage.EXPRESSION_LIST__MACHINE_ROUNDING_ATTRIBUTE:
				return getMachineRoundingAttribute();
			case AdaPackage.EXPRESSION_LIST__MOD_ATTRIBUTE:
				return getModAttribute();
			case AdaPackage.EXPRESSION_LIST__PRIORITY_ATTRIBUTE:
				return getPriorityAttribute();
			case AdaPackage.EXPRESSION_LIST__STREAM_SIZE_ATTRIBUTE:
				return getStreamSizeAttribute();
			case AdaPackage.EXPRESSION_LIST__WIDE_WIDE_IMAGE_ATTRIBUTE:
				return getWideWideImageAttribute();
			case AdaPackage.EXPRESSION_LIST__WIDE_WIDE_VALUE_ATTRIBUTE:
				return getWideWideValueAttribute();
			case AdaPackage.EXPRESSION_LIST__WIDE_WIDE_WIDTH_ATTRIBUTE:
				return getWideWideWidthAttribute();
			case AdaPackage.EXPRESSION_LIST__MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE:
				return getMaxAlignmentForAllocationAttribute();
			case AdaPackage.EXPRESSION_LIST__OVERLAPS_STORAGE_ATTRIBUTE:
				return getOverlapsStorageAttribute();
			case AdaPackage.EXPRESSION_LIST__IMPLEMENTATION_DEFINED_ATTRIBUTE:
				return getImplementationDefinedAttribute();
			case AdaPackage.EXPRESSION_LIST__UNKNOWN_ATTRIBUTE:
				return getUnknownAttribute();
			case AdaPackage.EXPRESSION_LIST__RECORD_AGGREGATE:
				return getRecordAggregate();
			case AdaPackage.EXPRESSION_LIST__EXTENSION_AGGREGATE:
				return getExtensionAggregate();
			case AdaPackage.EXPRESSION_LIST__POSITIONAL_ARRAY_AGGREGATE:
				return getPositionalArrayAggregate();
			case AdaPackage.EXPRESSION_LIST__NAMED_ARRAY_AGGREGATE:
				return getNamedArrayAggregate();
			case AdaPackage.EXPRESSION_LIST__AND_THEN_SHORT_CIRCUIT:
				return getAndThenShortCircuit();
			case AdaPackage.EXPRESSION_LIST__OR_ELSE_SHORT_CIRCUIT:
				return getOrElseShortCircuit();
			case AdaPackage.EXPRESSION_LIST__IN_MEMBERSHIP_TEST:
				return getInMembershipTest();
			case AdaPackage.EXPRESSION_LIST__NOT_IN_MEMBERSHIP_TEST:
				return getNotInMembershipTest();
			case AdaPackage.EXPRESSION_LIST__NULL_LITERAL:
				return getNullLiteral();
			case AdaPackage.EXPRESSION_LIST__PARENTHESIZED_EXPRESSION:
				return getParenthesizedExpression();
			case AdaPackage.EXPRESSION_LIST__RAISE_EXPRESSION:
				return getRaiseExpression();
			case AdaPackage.EXPRESSION_LIST__TYPE_CONVERSION:
				return getTypeConversion();
			case AdaPackage.EXPRESSION_LIST__QUALIFIED_EXPRESSION:
				return getQualifiedExpression();
			case AdaPackage.EXPRESSION_LIST__ALLOCATION_FROM_SUBTYPE:
				return getAllocationFromSubtype();
			case AdaPackage.EXPRESSION_LIST__ALLOCATION_FROM_QUALIFIED_EXPRESSION:
				return getAllocationFromQualifiedExpression();
			case AdaPackage.EXPRESSION_LIST__CASE_EXPRESSION:
				return getCaseExpression();
			case AdaPackage.EXPRESSION_LIST__IF_EXPRESSION:
				return getIfExpression();
			case AdaPackage.EXPRESSION_LIST__FOR_ALL_QUANTIFIED_EXPRESSION:
				return getForAllQuantifiedExpression();
			case AdaPackage.EXPRESSION_LIST__FOR_SOME_QUANTIFIED_EXPRESSION:
				return getForSomeQuantifiedExpression();
			case AdaPackage.EXPRESSION_LIST__COMMENT:
				return getComment();
			case AdaPackage.EXPRESSION_LIST__ALL_CALLS_REMOTE_PRAGMA:
				return getAllCallsRemotePragma();
			case AdaPackage.EXPRESSION_LIST__ASYNCHRONOUS_PRAGMA:
				return getAsynchronousPragma();
			case AdaPackage.EXPRESSION_LIST__ATOMIC_PRAGMA:
				return getAtomicPragma();
			case AdaPackage.EXPRESSION_LIST__ATOMIC_COMPONENTS_PRAGMA:
				return getAtomicComponentsPragma();
			case AdaPackage.EXPRESSION_LIST__ATTACH_HANDLER_PRAGMA:
				return getAttachHandlerPragma();
			case AdaPackage.EXPRESSION_LIST__CONTROLLED_PRAGMA:
				return getControlledPragma();
			case AdaPackage.EXPRESSION_LIST__CONVENTION_PRAGMA:
				return getConventionPragma();
			case AdaPackage.EXPRESSION_LIST__DISCARD_NAMES_PRAGMA:
				return getDiscardNamesPragma();
			case AdaPackage.EXPRESSION_LIST__ELABORATE_PRAGMA:
				return getElaboratePragma();
			case AdaPackage.EXPRESSION_LIST__ELABORATE_ALL_PRAGMA:
				return getElaborateAllPragma();
			case AdaPackage.EXPRESSION_LIST__ELABORATE_BODY_PRAGMA:
				return getElaborateBodyPragma();
			case AdaPackage.EXPRESSION_LIST__EXPORT_PRAGMA:
				return getExportPragma();
			case AdaPackage.EXPRESSION_LIST__IMPORT_PRAGMA:
				return getImportPragma();
			case AdaPackage.EXPRESSION_LIST__INLINE_PRAGMA:
				return getInlinePragma();
			case AdaPackage.EXPRESSION_LIST__INSPECTION_POINT_PRAGMA:
				return getInspectionPointPragma();
			case AdaPackage.EXPRESSION_LIST__INTERRUPT_HANDLER_PRAGMA:
				return getInterruptHandlerPragma();
			case AdaPackage.EXPRESSION_LIST__INTERRUPT_PRIORITY_PRAGMA:
				return getInterruptPriorityPragma();
			case AdaPackage.EXPRESSION_LIST__LINKER_OPTIONS_PRAGMA:
				return getLinkerOptionsPragma();
			case AdaPackage.EXPRESSION_LIST__LIST_PRAGMA:
				return getListPragma();
			case AdaPackage.EXPRESSION_LIST__LOCKING_POLICY_PRAGMA:
				return getLockingPolicyPragma();
			case AdaPackage.EXPRESSION_LIST__NORMALIZE_SCALARS_PRAGMA:
				return getNormalizeScalarsPragma();
			case AdaPackage.EXPRESSION_LIST__OPTIMIZE_PRAGMA:
				return getOptimizePragma();
			case AdaPackage.EXPRESSION_LIST__PACK_PRAGMA:
				return getPackPragma();
			case AdaPackage.EXPRESSION_LIST__PAGE_PRAGMA:
				return getPagePragma();
			case AdaPackage.EXPRESSION_LIST__PREELABORATE_PRAGMA:
				return getPreelaboratePragma();
			case AdaPackage.EXPRESSION_LIST__PRIORITY_PRAGMA:
				return getPriorityPragma();
			case AdaPackage.EXPRESSION_LIST__PURE_PRAGMA:
				return getPurePragma();
			case AdaPackage.EXPRESSION_LIST__QUEUING_POLICY_PRAGMA:
				return getQueuingPolicyPragma();
			case AdaPackage.EXPRESSION_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				return getRemoteCallInterfacePragma();
			case AdaPackage.EXPRESSION_LIST__REMOTE_TYPES_PRAGMA:
				return getRemoteTypesPragma();
			case AdaPackage.EXPRESSION_LIST__RESTRICTIONS_PRAGMA:
				return getRestrictionsPragma();
			case AdaPackage.EXPRESSION_LIST__REVIEWABLE_PRAGMA:
				return getReviewablePragma();
			case AdaPackage.EXPRESSION_LIST__SHARED_PASSIVE_PRAGMA:
				return getSharedPassivePragma();
			case AdaPackage.EXPRESSION_LIST__STORAGE_SIZE_PRAGMA:
				return getStorageSizePragma();
			case AdaPackage.EXPRESSION_LIST__SUPPRESS_PRAGMA:
				return getSuppressPragma();
			case AdaPackage.EXPRESSION_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				return getTaskDispatchingPolicyPragma();
			case AdaPackage.EXPRESSION_LIST__VOLATILE_PRAGMA:
				return getVolatilePragma();
			case AdaPackage.EXPRESSION_LIST__VOLATILE_COMPONENTS_PRAGMA:
				return getVolatileComponentsPragma();
			case AdaPackage.EXPRESSION_LIST__ASSERT_PRAGMA:
				return getAssertPragma();
			case AdaPackage.EXPRESSION_LIST__ASSERTION_POLICY_PRAGMA:
				return getAssertionPolicyPragma();
			case AdaPackage.EXPRESSION_LIST__DETECT_BLOCKING_PRAGMA:
				return getDetectBlockingPragma();
			case AdaPackage.EXPRESSION_LIST__NO_RETURN_PRAGMA:
				return getNoReturnPragma();
			case AdaPackage.EXPRESSION_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				return getPartitionElaborationPolicyPragma();
			case AdaPackage.EXPRESSION_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				return getPreelaborableInitializationPragma();
			case AdaPackage.EXPRESSION_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return getPrioritySpecificDispatchingPragma();
			case AdaPackage.EXPRESSION_LIST__PROFILE_PRAGMA:
				return getProfilePragma();
			case AdaPackage.EXPRESSION_LIST__RELATIVE_DEADLINE_PRAGMA:
				return getRelativeDeadlinePragma();
			case AdaPackage.EXPRESSION_LIST__UNCHECKED_UNION_PRAGMA:
				return getUncheckedUnionPragma();
			case AdaPackage.EXPRESSION_LIST__UNSUPPRESS_PRAGMA:
				return getUnsuppressPragma();
			case AdaPackage.EXPRESSION_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				return getDefaultStoragePoolPragma();
			case AdaPackage.EXPRESSION_LIST__DISPATCHING_DOMAIN_PRAGMA:
				return getDispatchingDomainPragma();
			case AdaPackage.EXPRESSION_LIST__CPU_PRAGMA:
				return getCpuPragma();
			case AdaPackage.EXPRESSION_LIST__INDEPENDENT_PRAGMA:
				return getIndependentPragma();
			case AdaPackage.EXPRESSION_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				return getIndependentComponentsPragma();
			case AdaPackage.EXPRESSION_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				return getImplementationDefinedPragma();
			case AdaPackage.EXPRESSION_LIST__UNKNOWN_PRAGMA:
				return getUnknownPragma();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.EXPRESSION_LIST__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__NOT_AN_ELEMENT:
				getNotAnElement().clear();
				getNotAnElement().addAll((Collection<? extends NotAnElement>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__DISCRETE_RANGE_ATTRIBUTE_REFERENCE:
				getDiscreteRangeAttributeReference().clear();
				getDiscreteRangeAttributeReference().addAll((Collection<? extends DiscreteRangeAttributeReference>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__DISCRETE_SIMPLE_EXPRESSION_RANGE:
				getDiscreteSimpleExpressionRange().clear();
				getDiscreteSimpleExpressionRange().addAll((Collection<? extends DiscreteSimpleExpressionRange>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__OTHERS_CHOICE:
				getOthersChoice().clear();
				getOthersChoice().addAll((Collection<? extends OthersChoice>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__BOX_EXPRESSION:
				getBoxExpression().clear();
				getBoxExpression().addAll((Collection<? extends BoxExpression>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__INTEGER_LITERAL:
				getIntegerLiteral().clear();
				getIntegerLiteral().addAll((Collection<? extends IntegerLiteral>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__REAL_LITERAL:
				getRealLiteral().clear();
				getRealLiteral().addAll((Collection<? extends RealLiteral>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__STRING_LITERAL:
				getStringLiteral().clear();
				getStringLiteral().addAll((Collection<? extends StringLiteral>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__IDENTIFIER:
				getIdentifier().clear();
				getIdentifier().addAll((Collection<? extends Identifier>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__AND_OPERATOR:
				getAndOperator().clear();
				getAndOperator().addAll((Collection<? extends AndOperator>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__OR_OPERATOR:
				getOrOperator().clear();
				getOrOperator().addAll((Collection<? extends OrOperator>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__XOR_OPERATOR:
				getXorOperator().clear();
				getXorOperator().addAll((Collection<? extends XorOperator>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__EQUAL_OPERATOR:
				getEqualOperator().clear();
				getEqualOperator().addAll((Collection<? extends EqualOperator>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__NOT_EQUAL_OPERATOR:
				getNotEqualOperator().clear();
				getNotEqualOperator().addAll((Collection<? extends NotEqualOperator>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__LESS_THAN_OPERATOR:
				getLessThanOperator().clear();
				getLessThanOperator().addAll((Collection<? extends LessThanOperator>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__LESS_THAN_OR_EQUAL_OPERATOR:
				getLessThanOrEqualOperator().clear();
				getLessThanOrEqualOperator().addAll((Collection<? extends LessThanOrEqualOperator>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__GREATER_THAN_OPERATOR:
				getGreaterThanOperator().clear();
				getGreaterThanOperator().addAll((Collection<? extends GreaterThanOperator>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__GREATER_THAN_OR_EQUAL_OPERATOR:
				getGreaterThanOrEqualOperator().clear();
				getGreaterThanOrEqualOperator().addAll((Collection<? extends GreaterThanOrEqualOperator>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__PLUS_OPERATOR:
				getPlusOperator().clear();
				getPlusOperator().addAll((Collection<? extends PlusOperator>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__MINUS_OPERATOR:
				getMinusOperator().clear();
				getMinusOperator().addAll((Collection<? extends MinusOperator>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__CONCATENATE_OPERATOR:
				getConcatenateOperator().clear();
				getConcatenateOperator().addAll((Collection<? extends ConcatenateOperator>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__UNARY_PLUS_OPERATOR:
				getUnaryPlusOperator().clear();
				getUnaryPlusOperator().addAll((Collection<? extends UnaryPlusOperator>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__UNARY_MINUS_OPERATOR:
				getUnaryMinusOperator().clear();
				getUnaryMinusOperator().addAll((Collection<? extends UnaryMinusOperator>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__MULTIPLY_OPERATOR:
				getMultiplyOperator().clear();
				getMultiplyOperator().addAll((Collection<? extends MultiplyOperator>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__DIVIDE_OPERATOR:
				getDivideOperator().clear();
				getDivideOperator().addAll((Collection<? extends DivideOperator>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__MOD_OPERATOR:
				getModOperator().clear();
				getModOperator().addAll((Collection<? extends ModOperator>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__REM_OPERATOR:
				getRemOperator().clear();
				getRemOperator().addAll((Collection<? extends RemOperator>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__EXPONENTIATE_OPERATOR:
				getExponentiateOperator().clear();
				getExponentiateOperator().addAll((Collection<? extends ExponentiateOperator>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__ABS_OPERATOR:
				getAbsOperator().clear();
				getAbsOperator().addAll((Collection<? extends AbsOperator>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__NOT_OPERATOR:
				getNotOperator().clear();
				getNotOperator().addAll((Collection<? extends NotOperator>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__CHARACTER_LITERAL:
				getCharacterLiteral().clear();
				getCharacterLiteral().addAll((Collection<? extends CharacterLiteral>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__ENUMERATION_LITERAL:
				getEnumerationLiteral().clear();
				getEnumerationLiteral().addAll((Collection<? extends EnumerationLiteral>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__EXPLICIT_DEREFERENCE:
				getExplicitDereference().clear();
				getExplicitDereference().addAll((Collection<? extends ExplicitDereference>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__FUNCTION_CALL:
				getFunctionCall().clear();
				getFunctionCall().addAll((Collection<? extends FunctionCall>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__INDEXED_COMPONENT:
				getIndexedComponent().clear();
				getIndexedComponent().addAll((Collection<? extends IndexedComponent>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__SLICE:
				getSlice().clear();
				getSlice().addAll((Collection<? extends Slice>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__SELECTED_COMPONENT:
				getSelectedComponent().clear();
				getSelectedComponent().addAll((Collection<? extends SelectedComponent>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__ACCESS_ATTRIBUTE:
				getAccessAttribute().clear();
				getAccessAttribute().addAll((Collection<? extends AccessAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__ADDRESS_ATTRIBUTE:
				getAddressAttribute().clear();
				getAddressAttribute().addAll((Collection<? extends AddressAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__ADJACENT_ATTRIBUTE:
				getAdjacentAttribute().clear();
				getAdjacentAttribute().addAll((Collection<? extends AdjacentAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__AFT_ATTRIBUTE:
				getAftAttribute().clear();
				getAftAttribute().addAll((Collection<? extends AftAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__ALIGNMENT_ATTRIBUTE:
				getAlignmentAttribute().clear();
				getAlignmentAttribute().addAll((Collection<? extends AlignmentAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__BASE_ATTRIBUTE:
				getBaseAttribute().clear();
				getBaseAttribute().addAll((Collection<? extends BaseAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__BIT_ORDER_ATTRIBUTE:
				getBitOrderAttribute().clear();
				getBitOrderAttribute().addAll((Collection<? extends BitOrderAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__BODY_VERSION_ATTRIBUTE:
				getBodyVersionAttribute().clear();
				getBodyVersionAttribute().addAll((Collection<? extends BodyVersionAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__CALLABLE_ATTRIBUTE:
				getCallableAttribute().clear();
				getCallableAttribute().addAll((Collection<? extends CallableAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__CALLER_ATTRIBUTE:
				getCallerAttribute().clear();
				getCallerAttribute().addAll((Collection<? extends CallerAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__CEILING_ATTRIBUTE:
				getCeilingAttribute().clear();
				getCeilingAttribute().addAll((Collection<? extends CeilingAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__CLASS_ATTRIBUTE:
				getClassAttribute().clear();
				getClassAttribute().addAll((Collection<? extends ClassAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__COMPONENT_SIZE_ATTRIBUTE:
				getComponentSizeAttribute().clear();
				getComponentSizeAttribute().addAll((Collection<? extends ComponentSizeAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__COMPOSE_ATTRIBUTE:
				getComposeAttribute().clear();
				getComposeAttribute().addAll((Collection<? extends ComposeAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__CONSTRAINED_ATTRIBUTE:
				getConstrainedAttribute().clear();
				getConstrainedAttribute().addAll((Collection<? extends ConstrainedAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__COPY_SIGN_ATTRIBUTE:
				getCopySignAttribute().clear();
				getCopySignAttribute().addAll((Collection<? extends CopySignAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__COUNT_ATTRIBUTE:
				getCountAttribute().clear();
				getCountAttribute().addAll((Collection<? extends CountAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__DEFINITE_ATTRIBUTE:
				getDefiniteAttribute().clear();
				getDefiniteAttribute().addAll((Collection<? extends DefiniteAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__DELTA_ATTRIBUTE:
				getDeltaAttribute().clear();
				getDeltaAttribute().addAll((Collection<? extends DeltaAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__DENORM_ATTRIBUTE:
				getDenormAttribute().clear();
				getDenormAttribute().addAll((Collection<? extends DenormAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__DIGITS_ATTRIBUTE:
				getDigitsAttribute().clear();
				getDigitsAttribute().addAll((Collection<? extends DigitsAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__EXPONENT_ATTRIBUTE:
				getExponentAttribute().clear();
				getExponentAttribute().addAll((Collection<? extends ExponentAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__EXTERNAL_TAG_ATTRIBUTE:
				getExternalTagAttribute().clear();
				getExternalTagAttribute().addAll((Collection<? extends ExternalTagAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__FIRST_ATTRIBUTE:
				getFirstAttribute().clear();
				getFirstAttribute().addAll((Collection<? extends FirstAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__FIRST_BIT_ATTRIBUTE:
				getFirstBitAttribute().clear();
				getFirstBitAttribute().addAll((Collection<? extends FirstBitAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__FLOOR_ATTRIBUTE:
				getFloorAttribute().clear();
				getFloorAttribute().addAll((Collection<? extends FloorAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__FORE_ATTRIBUTE:
				getForeAttribute().clear();
				getForeAttribute().addAll((Collection<? extends ForeAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__FRACTION_ATTRIBUTE:
				getFractionAttribute().clear();
				getFractionAttribute().addAll((Collection<? extends FractionAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__IDENTITY_ATTRIBUTE:
				getIdentityAttribute().clear();
				getIdentityAttribute().addAll((Collection<? extends IdentityAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__IMAGE_ATTRIBUTE:
				getImageAttribute().clear();
				getImageAttribute().addAll((Collection<? extends ImageAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__INPUT_ATTRIBUTE:
				getInputAttribute().clear();
				getInputAttribute().addAll((Collection<? extends InputAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__LAST_ATTRIBUTE:
				getLastAttribute().clear();
				getLastAttribute().addAll((Collection<? extends LastAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__LAST_BIT_ATTRIBUTE:
				getLastBitAttribute().clear();
				getLastBitAttribute().addAll((Collection<? extends LastBitAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__LEADING_PART_ATTRIBUTE:
				getLeadingPartAttribute().clear();
				getLeadingPartAttribute().addAll((Collection<? extends LeadingPartAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__LENGTH_ATTRIBUTE:
				getLengthAttribute().clear();
				getLengthAttribute().addAll((Collection<? extends LengthAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__MACHINE_ATTRIBUTE:
				getMachineAttribute().clear();
				getMachineAttribute().addAll((Collection<? extends MachineAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__MACHINE_EMAX_ATTRIBUTE:
				getMachineEmaxAttribute().clear();
				getMachineEmaxAttribute().addAll((Collection<? extends MachineEmaxAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__MACHINE_EMIN_ATTRIBUTE:
				getMachineEminAttribute().clear();
				getMachineEminAttribute().addAll((Collection<? extends MachineEminAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__MACHINE_MANTISSA_ATTRIBUTE:
				getMachineMantissaAttribute().clear();
				getMachineMantissaAttribute().addAll((Collection<? extends MachineMantissaAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__MACHINE_OVERFLOWS_ATTRIBUTE:
				getMachineOverflowsAttribute().clear();
				getMachineOverflowsAttribute().addAll((Collection<? extends MachineOverflowsAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__MACHINE_RADIX_ATTRIBUTE:
				getMachineRadixAttribute().clear();
				getMachineRadixAttribute().addAll((Collection<? extends MachineRadixAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__MACHINE_ROUNDS_ATTRIBUTE:
				getMachineRoundsAttribute().clear();
				getMachineRoundsAttribute().addAll((Collection<? extends MachineRoundsAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__MAX_ATTRIBUTE:
				getMaxAttribute().clear();
				getMaxAttribute().addAll((Collection<? extends MaxAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE:
				getMaxSizeInStorageElementsAttribute().clear();
				getMaxSizeInStorageElementsAttribute().addAll((Collection<? extends MaxSizeInStorageElementsAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__MIN_ATTRIBUTE:
				getMinAttribute().clear();
				getMinAttribute().addAll((Collection<? extends MinAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__MODEL_ATTRIBUTE:
				getModelAttribute().clear();
				getModelAttribute().addAll((Collection<? extends ModelAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__MODEL_EMIN_ATTRIBUTE:
				getModelEminAttribute().clear();
				getModelEminAttribute().addAll((Collection<? extends ModelEminAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__MODEL_EPSILON_ATTRIBUTE:
				getModelEpsilonAttribute().clear();
				getModelEpsilonAttribute().addAll((Collection<? extends ModelEpsilonAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__MODEL_MANTISSA_ATTRIBUTE:
				getModelMantissaAttribute().clear();
				getModelMantissaAttribute().addAll((Collection<? extends ModelMantissaAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__MODEL_SMALL_ATTRIBUTE:
				getModelSmallAttribute().clear();
				getModelSmallAttribute().addAll((Collection<? extends ModelSmallAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__MODULUS_ATTRIBUTE:
				getModulusAttribute().clear();
				getModulusAttribute().addAll((Collection<? extends ModulusAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__OUTPUT_ATTRIBUTE:
				getOutputAttribute().clear();
				getOutputAttribute().addAll((Collection<? extends OutputAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__PARTITION_ID_ATTRIBUTE:
				getPartitionIdAttribute().clear();
				getPartitionIdAttribute().addAll((Collection<? extends PartitionIdAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__POS_ATTRIBUTE:
				getPosAttribute().clear();
				getPosAttribute().addAll((Collection<? extends PosAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__POSITION_ATTRIBUTE:
				getPositionAttribute().clear();
				getPositionAttribute().addAll((Collection<? extends PositionAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__PRED_ATTRIBUTE:
				getPredAttribute().clear();
				getPredAttribute().addAll((Collection<? extends PredAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__RANGE_ATTRIBUTE:
				getRangeAttribute().clear();
				getRangeAttribute().addAll((Collection<? extends RangeAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__READ_ATTRIBUTE:
				getReadAttribute().clear();
				getReadAttribute().addAll((Collection<? extends ReadAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__REMAINDER_ATTRIBUTE:
				getRemainderAttribute().clear();
				getRemainderAttribute().addAll((Collection<? extends RemainderAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__ROUND_ATTRIBUTE:
				getRoundAttribute().clear();
				getRoundAttribute().addAll((Collection<? extends RoundAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__ROUNDING_ATTRIBUTE:
				getRoundingAttribute().clear();
				getRoundingAttribute().addAll((Collection<? extends RoundingAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__SAFE_FIRST_ATTRIBUTE:
				getSafeFirstAttribute().clear();
				getSafeFirstAttribute().addAll((Collection<? extends SafeFirstAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__SAFE_LAST_ATTRIBUTE:
				getSafeLastAttribute().clear();
				getSafeLastAttribute().addAll((Collection<? extends SafeLastAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__SCALE_ATTRIBUTE:
				getScaleAttribute().clear();
				getScaleAttribute().addAll((Collection<? extends ScaleAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__SCALING_ATTRIBUTE:
				getScalingAttribute().clear();
				getScalingAttribute().addAll((Collection<? extends ScalingAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__SIGNED_ZEROS_ATTRIBUTE:
				getSignedZerosAttribute().clear();
				getSignedZerosAttribute().addAll((Collection<? extends SignedZerosAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__SIZE_ATTRIBUTE:
				getSizeAttribute().clear();
				getSizeAttribute().addAll((Collection<? extends SizeAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__SMALL_ATTRIBUTE:
				getSmallAttribute().clear();
				getSmallAttribute().addAll((Collection<? extends SmallAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__STORAGE_POOL_ATTRIBUTE:
				getStoragePoolAttribute().clear();
				getStoragePoolAttribute().addAll((Collection<? extends StoragePoolAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__STORAGE_SIZE_ATTRIBUTE:
				getStorageSizeAttribute().clear();
				getStorageSizeAttribute().addAll((Collection<? extends StorageSizeAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__SUCC_ATTRIBUTE:
				getSuccAttribute().clear();
				getSuccAttribute().addAll((Collection<? extends SuccAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__TAG_ATTRIBUTE:
				getTagAttribute().clear();
				getTagAttribute().addAll((Collection<? extends TagAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__TERMINATED_ATTRIBUTE:
				getTerminatedAttribute().clear();
				getTerminatedAttribute().addAll((Collection<? extends TerminatedAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__TRUNCATION_ATTRIBUTE:
				getTruncationAttribute().clear();
				getTruncationAttribute().addAll((Collection<? extends TruncationAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__UNBIASED_ROUNDING_ATTRIBUTE:
				getUnbiasedRoundingAttribute().clear();
				getUnbiasedRoundingAttribute().addAll((Collection<? extends UnbiasedRoundingAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__UNCHECKED_ACCESS_ATTRIBUTE:
				getUncheckedAccessAttribute().clear();
				getUncheckedAccessAttribute().addAll((Collection<? extends UncheckedAccessAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__VAL_ATTRIBUTE:
				getValAttribute().clear();
				getValAttribute().addAll((Collection<? extends ValAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__VALID_ATTRIBUTE:
				getValidAttribute().clear();
				getValidAttribute().addAll((Collection<? extends ValidAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__VALUE_ATTRIBUTE:
				getValueAttribute().clear();
				getValueAttribute().addAll((Collection<? extends ValueAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__VERSION_ATTRIBUTE:
				getVersionAttribute().clear();
				getVersionAttribute().addAll((Collection<? extends VersionAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__WIDE_IMAGE_ATTRIBUTE:
				getWideImageAttribute().clear();
				getWideImageAttribute().addAll((Collection<? extends WideImageAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__WIDE_VALUE_ATTRIBUTE:
				getWideValueAttribute().clear();
				getWideValueAttribute().addAll((Collection<? extends WideValueAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__WIDE_WIDTH_ATTRIBUTE:
				getWideWidthAttribute().clear();
				getWideWidthAttribute().addAll((Collection<? extends WideWidthAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__WIDTH_ATTRIBUTE:
				getWidthAttribute().clear();
				getWidthAttribute().addAll((Collection<? extends WidthAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__WRITE_ATTRIBUTE:
				getWriteAttribute().clear();
				getWriteAttribute().addAll((Collection<? extends WriteAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__MACHINE_ROUNDING_ATTRIBUTE:
				getMachineRoundingAttribute().clear();
				getMachineRoundingAttribute().addAll((Collection<? extends MachineRoundingAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__MOD_ATTRIBUTE:
				getModAttribute().clear();
				getModAttribute().addAll((Collection<? extends ModAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__PRIORITY_ATTRIBUTE:
				getPriorityAttribute().clear();
				getPriorityAttribute().addAll((Collection<? extends PriorityAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__STREAM_SIZE_ATTRIBUTE:
				getStreamSizeAttribute().clear();
				getStreamSizeAttribute().addAll((Collection<? extends StreamSizeAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__WIDE_WIDE_IMAGE_ATTRIBUTE:
				getWideWideImageAttribute().clear();
				getWideWideImageAttribute().addAll((Collection<? extends WideWideImageAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__WIDE_WIDE_VALUE_ATTRIBUTE:
				getWideWideValueAttribute().clear();
				getWideWideValueAttribute().addAll((Collection<? extends WideWideValueAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__WIDE_WIDE_WIDTH_ATTRIBUTE:
				getWideWideWidthAttribute().clear();
				getWideWideWidthAttribute().addAll((Collection<? extends WideWideWidthAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE:
				getMaxAlignmentForAllocationAttribute().clear();
				getMaxAlignmentForAllocationAttribute().addAll((Collection<? extends MaxAlignmentForAllocationAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__OVERLAPS_STORAGE_ATTRIBUTE:
				getOverlapsStorageAttribute().clear();
				getOverlapsStorageAttribute().addAll((Collection<? extends OverlapsStorageAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__IMPLEMENTATION_DEFINED_ATTRIBUTE:
				getImplementationDefinedAttribute().clear();
				getImplementationDefinedAttribute().addAll((Collection<? extends ImplementationDefinedAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__UNKNOWN_ATTRIBUTE:
				getUnknownAttribute().clear();
				getUnknownAttribute().addAll((Collection<? extends UnknownAttribute>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__RECORD_AGGREGATE:
				getRecordAggregate().clear();
				getRecordAggregate().addAll((Collection<? extends RecordAggregate>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__EXTENSION_AGGREGATE:
				getExtensionAggregate().clear();
				getExtensionAggregate().addAll((Collection<? extends ExtensionAggregate>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__POSITIONAL_ARRAY_AGGREGATE:
				getPositionalArrayAggregate().clear();
				getPositionalArrayAggregate().addAll((Collection<? extends PositionalArrayAggregate>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__NAMED_ARRAY_AGGREGATE:
				getNamedArrayAggregate().clear();
				getNamedArrayAggregate().addAll((Collection<? extends NamedArrayAggregate>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__AND_THEN_SHORT_CIRCUIT:
				getAndThenShortCircuit().clear();
				getAndThenShortCircuit().addAll((Collection<? extends AndThenShortCircuit>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__OR_ELSE_SHORT_CIRCUIT:
				getOrElseShortCircuit().clear();
				getOrElseShortCircuit().addAll((Collection<? extends OrElseShortCircuit>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__IN_MEMBERSHIP_TEST:
				getInMembershipTest().clear();
				getInMembershipTest().addAll((Collection<? extends InMembershipTest>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__NOT_IN_MEMBERSHIP_TEST:
				getNotInMembershipTest().clear();
				getNotInMembershipTest().addAll((Collection<? extends NotInMembershipTest>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__NULL_LITERAL:
				getNullLiteral().clear();
				getNullLiteral().addAll((Collection<? extends NullLiteral>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__PARENTHESIZED_EXPRESSION:
				getParenthesizedExpression().clear();
				getParenthesizedExpression().addAll((Collection<? extends ParenthesizedExpression>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__RAISE_EXPRESSION:
				getRaiseExpression().clear();
				getRaiseExpression().addAll((Collection<? extends RaiseExpression>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__TYPE_CONVERSION:
				getTypeConversion().clear();
				getTypeConversion().addAll((Collection<? extends TypeConversion>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__QUALIFIED_EXPRESSION:
				getQualifiedExpression().clear();
				getQualifiedExpression().addAll((Collection<? extends QualifiedExpression>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__ALLOCATION_FROM_SUBTYPE:
				getAllocationFromSubtype().clear();
				getAllocationFromSubtype().addAll((Collection<? extends AllocationFromSubtype>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__ALLOCATION_FROM_QUALIFIED_EXPRESSION:
				getAllocationFromQualifiedExpression().clear();
				getAllocationFromQualifiedExpression().addAll((Collection<? extends AllocationFromQualifiedExpression>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__CASE_EXPRESSION:
				getCaseExpression().clear();
				getCaseExpression().addAll((Collection<? extends CaseExpression>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__IF_EXPRESSION:
				getIfExpression().clear();
				getIfExpression().addAll((Collection<? extends IfExpression>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__FOR_ALL_QUANTIFIED_EXPRESSION:
				getForAllQuantifiedExpression().clear();
				getForAllQuantifiedExpression().addAll((Collection<? extends ForAllQuantifiedExpression>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__FOR_SOME_QUANTIFIED_EXPRESSION:
				getForSomeQuantifiedExpression().clear();
				getForSomeQuantifiedExpression().addAll((Collection<? extends ForSomeQuantifiedExpression>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__COMMENT:
				getComment().clear();
				getComment().addAll((Collection<? extends Comment>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__ALL_CALLS_REMOTE_PRAGMA:
				getAllCallsRemotePragma().clear();
				getAllCallsRemotePragma().addAll((Collection<? extends AllCallsRemotePragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__ASYNCHRONOUS_PRAGMA:
				getAsynchronousPragma().clear();
				getAsynchronousPragma().addAll((Collection<? extends AsynchronousPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__ATOMIC_PRAGMA:
				getAtomicPragma().clear();
				getAtomicPragma().addAll((Collection<? extends AtomicPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__ATOMIC_COMPONENTS_PRAGMA:
				getAtomicComponentsPragma().clear();
				getAtomicComponentsPragma().addAll((Collection<? extends AtomicComponentsPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__ATTACH_HANDLER_PRAGMA:
				getAttachHandlerPragma().clear();
				getAttachHandlerPragma().addAll((Collection<? extends AttachHandlerPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__CONTROLLED_PRAGMA:
				getControlledPragma().clear();
				getControlledPragma().addAll((Collection<? extends ControlledPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__CONVENTION_PRAGMA:
				getConventionPragma().clear();
				getConventionPragma().addAll((Collection<? extends ConventionPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__DISCARD_NAMES_PRAGMA:
				getDiscardNamesPragma().clear();
				getDiscardNamesPragma().addAll((Collection<? extends DiscardNamesPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__ELABORATE_PRAGMA:
				getElaboratePragma().clear();
				getElaboratePragma().addAll((Collection<? extends ElaboratePragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__ELABORATE_ALL_PRAGMA:
				getElaborateAllPragma().clear();
				getElaborateAllPragma().addAll((Collection<? extends ElaborateAllPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__ELABORATE_BODY_PRAGMA:
				getElaborateBodyPragma().clear();
				getElaborateBodyPragma().addAll((Collection<? extends ElaborateBodyPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__EXPORT_PRAGMA:
				getExportPragma().clear();
				getExportPragma().addAll((Collection<? extends ExportPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__IMPORT_PRAGMA:
				getImportPragma().clear();
				getImportPragma().addAll((Collection<? extends ImportPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__INLINE_PRAGMA:
				getInlinePragma().clear();
				getInlinePragma().addAll((Collection<? extends InlinePragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__INSPECTION_POINT_PRAGMA:
				getInspectionPointPragma().clear();
				getInspectionPointPragma().addAll((Collection<? extends InspectionPointPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__INTERRUPT_HANDLER_PRAGMA:
				getInterruptHandlerPragma().clear();
				getInterruptHandlerPragma().addAll((Collection<? extends InterruptHandlerPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__INTERRUPT_PRIORITY_PRAGMA:
				getInterruptPriorityPragma().clear();
				getInterruptPriorityPragma().addAll((Collection<? extends InterruptPriorityPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__LINKER_OPTIONS_PRAGMA:
				getLinkerOptionsPragma().clear();
				getLinkerOptionsPragma().addAll((Collection<? extends LinkerOptionsPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__LIST_PRAGMA:
				getListPragma().clear();
				getListPragma().addAll((Collection<? extends ListPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__LOCKING_POLICY_PRAGMA:
				getLockingPolicyPragma().clear();
				getLockingPolicyPragma().addAll((Collection<? extends LockingPolicyPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__NORMALIZE_SCALARS_PRAGMA:
				getNormalizeScalarsPragma().clear();
				getNormalizeScalarsPragma().addAll((Collection<? extends NormalizeScalarsPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__OPTIMIZE_PRAGMA:
				getOptimizePragma().clear();
				getOptimizePragma().addAll((Collection<? extends OptimizePragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__PACK_PRAGMA:
				getPackPragma().clear();
				getPackPragma().addAll((Collection<? extends PackPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__PAGE_PRAGMA:
				getPagePragma().clear();
				getPagePragma().addAll((Collection<? extends PagePragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__PREELABORATE_PRAGMA:
				getPreelaboratePragma().clear();
				getPreelaboratePragma().addAll((Collection<? extends PreelaboratePragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__PRIORITY_PRAGMA:
				getPriorityPragma().clear();
				getPriorityPragma().addAll((Collection<? extends PriorityPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__PURE_PRAGMA:
				getPurePragma().clear();
				getPurePragma().addAll((Collection<? extends PurePragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__QUEUING_POLICY_PRAGMA:
				getQueuingPolicyPragma().clear();
				getQueuingPolicyPragma().addAll((Collection<? extends QueuingPolicyPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				getRemoteCallInterfacePragma().clear();
				getRemoteCallInterfacePragma().addAll((Collection<? extends RemoteCallInterfacePragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__REMOTE_TYPES_PRAGMA:
				getRemoteTypesPragma().clear();
				getRemoteTypesPragma().addAll((Collection<? extends RemoteTypesPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__RESTRICTIONS_PRAGMA:
				getRestrictionsPragma().clear();
				getRestrictionsPragma().addAll((Collection<? extends RestrictionsPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__REVIEWABLE_PRAGMA:
				getReviewablePragma().clear();
				getReviewablePragma().addAll((Collection<? extends ReviewablePragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__SHARED_PASSIVE_PRAGMA:
				getSharedPassivePragma().clear();
				getSharedPassivePragma().addAll((Collection<? extends SharedPassivePragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__STORAGE_SIZE_PRAGMA:
				getStorageSizePragma().clear();
				getStorageSizePragma().addAll((Collection<? extends StorageSizePragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__SUPPRESS_PRAGMA:
				getSuppressPragma().clear();
				getSuppressPragma().addAll((Collection<? extends SuppressPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				getTaskDispatchingPolicyPragma().clear();
				getTaskDispatchingPolicyPragma().addAll((Collection<? extends TaskDispatchingPolicyPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__VOLATILE_PRAGMA:
				getVolatilePragma().clear();
				getVolatilePragma().addAll((Collection<? extends VolatilePragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__VOLATILE_COMPONENTS_PRAGMA:
				getVolatileComponentsPragma().clear();
				getVolatileComponentsPragma().addAll((Collection<? extends VolatileComponentsPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__ASSERT_PRAGMA:
				getAssertPragma().clear();
				getAssertPragma().addAll((Collection<? extends AssertPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__ASSERTION_POLICY_PRAGMA:
				getAssertionPolicyPragma().clear();
				getAssertionPolicyPragma().addAll((Collection<? extends AssertionPolicyPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__DETECT_BLOCKING_PRAGMA:
				getDetectBlockingPragma().clear();
				getDetectBlockingPragma().addAll((Collection<? extends DetectBlockingPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__NO_RETURN_PRAGMA:
				getNoReturnPragma().clear();
				getNoReturnPragma().addAll((Collection<? extends NoReturnPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				getPartitionElaborationPolicyPragma().clear();
				getPartitionElaborationPolicyPragma().addAll((Collection<? extends PartitionElaborationPolicyPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				getPreelaborableInitializationPragma().clear();
				getPreelaborableInitializationPragma().addAll((Collection<? extends PreelaborableInitializationPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				getPrioritySpecificDispatchingPragma().clear();
				getPrioritySpecificDispatchingPragma().addAll((Collection<? extends PrioritySpecificDispatchingPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__PROFILE_PRAGMA:
				getProfilePragma().clear();
				getProfilePragma().addAll((Collection<? extends ProfilePragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__RELATIVE_DEADLINE_PRAGMA:
				getRelativeDeadlinePragma().clear();
				getRelativeDeadlinePragma().addAll((Collection<? extends RelativeDeadlinePragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__UNCHECKED_UNION_PRAGMA:
				getUncheckedUnionPragma().clear();
				getUncheckedUnionPragma().addAll((Collection<? extends UncheckedUnionPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__UNSUPPRESS_PRAGMA:
				getUnsuppressPragma().clear();
				getUnsuppressPragma().addAll((Collection<? extends UnsuppressPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				getDefaultStoragePoolPragma().clear();
				getDefaultStoragePoolPragma().addAll((Collection<? extends DefaultStoragePoolPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__DISPATCHING_DOMAIN_PRAGMA:
				getDispatchingDomainPragma().clear();
				getDispatchingDomainPragma().addAll((Collection<? extends DispatchingDomainPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__CPU_PRAGMA:
				getCpuPragma().clear();
				getCpuPragma().addAll((Collection<? extends CpuPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__INDEPENDENT_PRAGMA:
				getIndependentPragma().clear();
				getIndependentPragma().addAll((Collection<? extends IndependentPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				getIndependentComponentsPragma().clear();
				getIndependentComponentsPragma().addAll((Collection<? extends IndependentComponentsPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				getImplementationDefinedPragma().clear();
				getImplementationDefinedPragma().addAll((Collection<? extends ImplementationDefinedPragma>)newValue);
				return;
			case AdaPackage.EXPRESSION_LIST__UNKNOWN_PRAGMA:
				getUnknownPragma().clear();
				getUnknownPragma().addAll((Collection<? extends UnknownPragma>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.EXPRESSION_LIST__GROUP:
				getGroup().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__NOT_AN_ELEMENT:
				getNotAnElement().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__DISCRETE_RANGE_ATTRIBUTE_REFERENCE:
				getDiscreteRangeAttributeReference().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__DISCRETE_SIMPLE_EXPRESSION_RANGE:
				getDiscreteSimpleExpressionRange().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__OTHERS_CHOICE:
				getOthersChoice().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__BOX_EXPRESSION:
				getBoxExpression().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__INTEGER_LITERAL:
				getIntegerLiteral().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__REAL_LITERAL:
				getRealLiteral().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__STRING_LITERAL:
				getStringLiteral().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__IDENTIFIER:
				getIdentifier().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__AND_OPERATOR:
				getAndOperator().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__OR_OPERATOR:
				getOrOperator().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__XOR_OPERATOR:
				getXorOperator().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__EQUAL_OPERATOR:
				getEqualOperator().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__NOT_EQUAL_OPERATOR:
				getNotEqualOperator().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__LESS_THAN_OPERATOR:
				getLessThanOperator().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__LESS_THAN_OR_EQUAL_OPERATOR:
				getLessThanOrEqualOperator().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__GREATER_THAN_OPERATOR:
				getGreaterThanOperator().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__GREATER_THAN_OR_EQUAL_OPERATOR:
				getGreaterThanOrEqualOperator().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__PLUS_OPERATOR:
				getPlusOperator().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__MINUS_OPERATOR:
				getMinusOperator().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__CONCATENATE_OPERATOR:
				getConcatenateOperator().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__UNARY_PLUS_OPERATOR:
				getUnaryPlusOperator().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__UNARY_MINUS_OPERATOR:
				getUnaryMinusOperator().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__MULTIPLY_OPERATOR:
				getMultiplyOperator().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__DIVIDE_OPERATOR:
				getDivideOperator().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__MOD_OPERATOR:
				getModOperator().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__REM_OPERATOR:
				getRemOperator().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__EXPONENTIATE_OPERATOR:
				getExponentiateOperator().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__ABS_OPERATOR:
				getAbsOperator().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__NOT_OPERATOR:
				getNotOperator().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__CHARACTER_LITERAL:
				getCharacterLiteral().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__ENUMERATION_LITERAL:
				getEnumerationLiteral().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__EXPLICIT_DEREFERENCE:
				getExplicitDereference().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__FUNCTION_CALL:
				getFunctionCall().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__INDEXED_COMPONENT:
				getIndexedComponent().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__SLICE:
				getSlice().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__SELECTED_COMPONENT:
				getSelectedComponent().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__ACCESS_ATTRIBUTE:
				getAccessAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__ADDRESS_ATTRIBUTE:
				getAddressAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__ADJACENT_ATTRIBUTE:
				getAdjacentAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__AFT_ATTRIBUTE:
				getAftAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__ALIGNMENT_ATTRIBUTE:
				getAlignmentAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__BASE_ATTRIBUTE:
				getBaseAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__BIT_ORDER_ATTRIBUTE:
				getBitOrderAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__BODY_VERSION_ATTRIBUTE:
				getBodyVersionAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__CALLABLE_ATTRIBUTE:
				getCallableAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__CALLER_ATTRIBUTE:
				getCallerAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__CEILING_ATTRIBUTE:
				getCeilingAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__CLASS_ATTRIBUTE:
				getClassAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__COMPONENT_SIZE_ATTRIBUTE:
				getComponentSizeAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__COMPOSE_ATTRIBUTE:
				getComposeAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__CONSTRAINED_ATTRIBUTE:
				getConstrainedAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__COPY_SIGN_ATTRIBUTE:
				getCopySignAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__COUNT_ATTRIBUTE:
				getCountAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__DEFINITE_ATTRIBUTE:
				getDefiniteAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__DELTA_ATTRIBUTE:
				getDeltaAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__DENORM_ATTRIBUTE:
				getDenormAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__DIGITS_ATTRIBUTE:
				getDigitsAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__EXPONENT_ATTRIBUTE:
				getExponentAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__EXTERNAL_TAG_ATTRIBUTE:
				getExternalTagAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__FIRST_ATTRIBUTE:
				getFirstAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__FIRST_BIT_ATTRIBUTE:
				getFirstBitAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__FLOOR_ATTRIBUTE:
				getFloorAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__FORE_ATTRIBUTE:
				getForeAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__FRACTION_ATTRIBUTE:
				getFractionAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__IDENTITY_ATTRIBUTE:
				getIdentityAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__IMAGE_ATTRIBUTE:
				getImageAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__INPUT_ATTRIBUTE:
				getInputAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__LAST_ATTRIBUTE:
				getLastAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__LAST_BIT_ATTRIBUTE:
				getLastBitAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__LEADING_PART_ATTRIBUTE:
				getLeadingPartAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__LENGTH_ATTRIBUTE:
				getLengthAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__MACHINE_ATTRIBUTE:
				getMachineAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__MACHINE_EMAX_ATTRIBUTE:
				getMachineEmaxAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__MACHINE_EMIN_ATTRIBUTE:
				getMachineEminAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__MACHINE_MANTISSA_ATTRIBUTE:
				getMachineMantissaAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__MACHINE_OVERFLOWS_ATTRIBUTE:
				getMachineOverflowsAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__MACHINE_RADIX_ATTRIBUTE:
				getMachineRadixAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__MACHINE_ROUNDS_ATTRIBUTE:
				getMachineRoundsAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__MAX_ATTRIBUTE:
				getMaxAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE:
				getMaxSizeInStorageElementsAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__MIN_ATTRIBUTE:
				getMinAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__MODEL_ATTRIBUTE:
				getModelAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__MODEL_EMIN_ATTRIBUTE:
				getModelEminAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__MODEL_EPSILON_ATTRIBUTE:
				getModelEpsilonAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__MODEL_MANTISSA_ATTRIBUTE:
				getModelMantissaAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__MODEL_SMALL_ATTRIBUTE:
				getModelSmallAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__MODULUS_ATTRIBUTE:
				getModulusAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__OUTPUT_ATTRIBUTE:
				getOutputAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__PARTITION_ID_ATTRIBUTE:
				getPartitionIdAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__POS_ATTRIBUTE:
				getPosAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__POSITION_ATTRIBUTE:
				getPositionAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__PRED_ATTRIBUTE:
				getPredAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__RANGE_ATTRIBUTE:
				getRangeAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__READ_ATTRIBUTE:
				getReadAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__REMAINDER_ATTRIBUTE:
				getRemainderAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__ROUND_ATTRIBUTE:
				getRoundAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__ROUNDING_ATTRIBUTE:
				getRoundingAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__SAFE_FIRST_ATTRIBUTE:
				getSafeFirstAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__SAFE_LAST_ATTRIBUTE:
				getSafeLastAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__SCALE_ATTRIBUTE:
				getScaleAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__SCALING_ATTRIBUTE:
				getScalingAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__SIGNED_ZEROS_ATTRIBUTE:
				getSignedZerosAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__SIZE_ATTRIBUTE:
				getSizeAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__SMALL_ATTRIBUTE:
				getSmallAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__STORAGE_POOL_ATTRIBUTE:
				getStoragePoolAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__STORAGE_SIZE_ATTRIBUTE:
				getStorageSizeAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__SUCC_ATTRIBUTE:
				getSuccAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__TAG_ATTRIBUTE:
				getTagAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__TERMINATED_ATTRIBUTE:
				getTerminatedAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__TRUNCATION_ATTRIBUTE:
				getTruncationAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__UNBIASED_ROUNDING_ATTRIBUTE:
				getUnbiasedRoundingAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__UNCHECKED_ACCESS_ATTRIBUTE:
				getUncheckedAccessAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__VAL_ATTRIBUTE:
				getValAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__VALID_ATTRIBUTE:
				getValidAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__VALUE_ATTRIBUTE:
				getValueAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__VERSION_ATTRIBUTE:
				getVersionAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__WIDE_IMAGE_ATTRIBUTE:
				getWideImageAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__WIDE_VALUE_ATTRIBUTE:
				getWideValueAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__WIDE_WIDTH_ATTRIBUTE:
				getWideWidthAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__WIDTH_ATTRIBUTE:
				getWidthAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__WRITE_ATTRIBUTE:
				getWriteAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__MACHINE_ROUNDING_ATTRIBUTE:
				getMachineRoundingAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__MOD_ATTRIBUTE:
				getModAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__PRIORITY_ATTRIBUTE:
				getPriorityAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__STREAM_SIZE_ATTRIBUTE:
				getStreamSizeAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__WIDE_WIDE_IMAGE_ATTRIBUTE:
				getWideWideImageAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__WIDE_WIDE_VALUE_ATTRIBUTE:
				getWideWideValueAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__WIDE_WIDE_WIDTH_ATTRIBUTE:
				getWideWideWidthAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE:
				getMaxAlignmentForAllocationAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__OVERLAPS_STORAGE_ATTRIBUTE:
				getOverlapsStorageAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__IMPLEMENTATION_DEFINED_ATTRIBUTE:
				getImplementationDefinedAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__UNKNOWN_ATTRIBUTE:
				getUnknownAttribute().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__RECORD_AGGREGATE:
				getRecordAggregate().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__EXTENSION_AGGREGATE:
				getExtensionAggregate().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__POSITIONAL_ARRAY_AGGREGATE:
				getPositionalArrayAggregate().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__NAMED_ARRAY_AGGREGATE:
				getNamedArrayAggregate().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__AND_THEN_SHORT_CIRCUIT:
				getAndThenShortCircuit().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__OR_ELSE_SHORT_CIRCUIT:
				getOrElseShortCircuit().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__IN_MEMBERSHIP_TEST:
				getInMembershipTest().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__NOT_IN_MEMBERSHIP_TEST:
				getNotInMembershipTest().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__NULL_LITERAL:
				getNullLiteral().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__PARENTHESIZED_EXPRESSION:
				getParenthesizedExpression().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__RAISE_EXPRESSION:
				getRaiseExpression().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__TYPE_CONVERSION:
				getTypeConversion().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__QUALIFIED_EXPRESSION:
				getQualifiedExpression().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__ALLOCATION_FROM_SUBTYPE:
				getAllocationFromSubtype().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__ALLOCATION_FROM_QUALIFIED_EXPRESSION:
				getAllocationFromQualifiedExpression().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__CASE_EXPRESSION:
				getCaseExpression().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__IF_EXPRESSION:
				getIfExpression().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__FOR_ALL_QUANTIFIED_EXPRESSION:
				getForAllQuantifiedExpression().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__FOR_SOME_QUANTIFIED_EXPRESSION:
				getForSomeQuantifiedExpression().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__COMMENT:
				getComment().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__ALL_CALLS_REMOTE_PRAGMA:
				getAllCallsRemotePragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__ASYNCHRONOUS_PRAGMA:
				getAsynchronousPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__ATOMIC_PRAGMA:
				getAtomicPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__ATOMIC_COMPONENTS_PRAGMA:
				getAtomicComponentsPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__ATTACH_HANDLER_PRAGMA:
				getAttachHandlerPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__CONTROLLED_PRAGMA:
				getControlledPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__CONVENTION_PRAGMA:
				getConventionPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__DISCARD_NAMES_PRAGMA:
				getDiscardNamesPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__ELABORATE_PRAGMA:
				getElaboratePragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__ELABORATE_ALL_PRAGMA:
				getElaborateAllPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__ELABORATE_BODY_PRAGMA:
				getElaborateBodyPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__EXPORT_PRAGMA:
				getExportPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__IMPORT_PRAGMA:
				getImportPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__INLINE_PRAGMA:
				getInlinePragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__INSPECTION_POINT_PRAGMA:
				getInspectionPointPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__INTERRUPT_HANDLER_PRAGMA:
				getInterruptHandlerPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__INTERRUPT_PRIORITY_PRAGMA:
				getInterruptPriorityPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__LINKER_OPTIONS_PRAGMA:
				getLinkerOptionsPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__LIST_PRAGMA:
				getListPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__LOCKING_POLICY_PRAGMA:
				getLockingPolicyPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__NORMALIZE_SCALARS_PRAGMA:
				getNormalizeScalarsPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__OPTIMIZE_PRAGMA:
				getOptimizePragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__PACK_PRAGMA:
				getPackPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__PAGE_PRAGMA:
				getPagePragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__PREELABORATE_PRAGMA:
				getPreelaboratePragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__PRIORITY_PRAGMA:
				getPriorityPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__PURE_PRAGMA:
				getPurePragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__QUEUING_POLICY_PRAGMA:
				getQueuingPolicyPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				getRemoteCallInterfacePragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__REMOTE_TYPES_PRAGMA:
				getRemoteTypesPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__RESTRICTIONS_PRAGMA:
				getRestrictionsPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__REVIEWABLE_PRAGMA:
				getReviewablePragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__SHARED_PASSIVE_PRAGMA:
				getSharedPassivePragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__STORAGE_SIZE_PRAGMA:
				getStorageSizePragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__SUPPRESS_PRAGMA:
				getSuppressPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				getTaskDispatchingPolicyPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__VOLATILE_PRAGMA:
				getVolatilePragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__VOLATILE_COMPONENTS_PRAGMA:
				getVolatileComponentsPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__ASSERT_PRAGMA:
				getAssertPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__ASSERTION_POLICY_PRAGMA:
				getAssertionPolicyPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__DETECT_BLOCKING_PRAGMA:
				getDetectBlockingPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__NO_RETURN_PRAGMA:
				getNoReturnPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				getPartitionElaborationPolicyPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				getPreelaborableInitializationPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				getPrioritySpecificDispatchingPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__PROFILE_PRAGMA:
				getProfilePragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__RELATIVE_DEADLINE_PRAGMA:
				getRelativeDeadlinePragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__UNCHECKED_UNION_PRAGMA:
				getUncheckedUnionPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__UNSUPPRESS_PRAGMA:
				getUnsuppressPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				getDefaultStoragePoolPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__DISPATCHING_DOMAIN_PRAGMA:
				getDispatchingDomainPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__CPU_PRAGMA:
				getCpuPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__INDEPENDENT_PRAGMA:
				getIndependentPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				getIndependentComponentsPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				getImplementationDefinedPragma().clear();
				return;
			case AdaPackage.EXPRESSION_LIST__UNKNOWN_PRAGMA:
				getUnknownPragma().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.EXPRESSION_LIST__GROUP:
				return group != null && !group.isEmpty();
			case AdaPackage.EXPRESSION_LIST__NOT_AN_ELEMENT:
				return !getNotAnElement().isEmpty();
			case AdaPackage.EXPRESSION_LIST__DISCRETE_RANGE_ATTRIBUTE_REFERENCE:
				return !getDiscreteRangeAttributeReference().isEmpty();
			case AdaPackage.EXPRESSION_LIST__DISCRETE_SIMPLE_EXPRESSION_RANGE:
				return !getDiscreteSimpleExpressionRange().isEmpty();
			case AdaPackage.EXPRESSION_LIST__OTHERS_CHOICE:
				return !getOthersChoice().isEmpty();
			case AdaPackage.EXPRESSION_LIST__BOX_EXPRESSION:
				return !getBoxExpression().isEmpty();
			case AdaPackage.EXPRESSION_LIST__INTEGER_LITERAL:
				return !getIntegerLiteral().isEmpty();
			case AdaPackage.EXPRESSION_LIST__REAL_LITERAL:
				return !getRealLiteral().isEmpty();
			case AdaPackage.EXPRESSION_LIST__STRING_LITERAL:
				return !getStringLiteral().isEmpty();
			case AdaPackage.EXPRESSION_LIST__IDENTIFIER:
				return !getIdentifier().isEmpty();
			case AdaPackage.EXPRESSION_LIST__AND_OPERATOR:
				return !getAndOperator().isEmpty();
			case AdaPackage.EXPRESSION_LIST__OR_OPERATOR:
				return !getOrOperator().isEmpty();
			case AdaPackage.EXPRESSION_LIST__XOR_OPERATOR:
				return !getXorOperator().isEmpty();
			case AdaPackage.EXPRESSION_LIST__EQUAL_OPERATOR:
				return !getEqualOperator().isEmpty();
			case AdaPackage.EXPRESSION_LIST__NOT_EQUAL_OPERATOR:
				return !getNotEqualOperator().isEmpty();
			case AdaPackage.EXPRESSION_LIST__LESS_THAN_OPERATOR:
				return !getLessThanOperator().isEmpty();
			case AdaPackage.EXPRESSION_LIST__LESS_THAN_OR_EQUAL_OPERATOR:
				return !getLessThanOrEqualOperator().isEmpty();
			case AdaPackage.EXPRESSION_LIST__GREATER_THAN_OPERATOR:
				return !getGreaterThanOperator().isEmpty();
			case AdaPackage.EXPRESSION_LIST__GREATER_THAN_OR_EQUAL_OPERATOR:
				return !getGreaterThanOrEqualOperator().isEmpty();
			case AdaPackage.EXPRESSION_LIST__PLUS_OPERATOR:
				return !getPlusOperator().isEmpty();
			case AdaPackage.EXPRESSION_LIST__MINUS_OPERATOR:
				return !getMinusOperator().isEmpty();
			case AdaPackage.EXPRESSION_LIST__CONCATENATE_OPERATOR:
				return !getConcatenateOperator().isEmpty();
			case AdaPackage.EXPRESSION_LIST__UNARY_PLUS_OPERATOR:
				return !getUnaryPlusOperator().isEmpty();
			case AdaPackage.EXPRESSION_LIST__UNARY_MINUS_OPERATOR:
				return !getUnaryMinusOperator().isEmpty();
			case AdaPackage.EXPRESSION_LIST__MULTIPLY_OPERATOR:
				return !getMultiplyOperator().isEmpty();
			case AdaPackage.EXPRESSION_LIST__DIVIDE_OPERATOR:
				return !getDivideOperator().isEmpty();
			case AdaPackage.EXPRESSION_LIST__MOD_OPERATOR:
				return !getModOperator().isEmpty();
			case AdaPackage.EXPRESSION_LIST__REM_OPERATOR:
				return !getRemOperator().isEmpty();
			case AdaPackage.EXPRESSION_LIST__EXPONENTIATE_OPERATOR:
				return !getExponentiateOperator().isEmpty();
			case AdaPackage.EXPRESSION_LIST__ABS_OPERATOR:
				return !getAbsOperator().isEmpty();
			case AdaPackage.EXPRESSION_LIST__NOT_OPERATOR:
				return !getNotOperator().isEmpty();
			case AdaPackage.EXPRESSION_LIST__CHARACTER_LITERAL:
				return !getCharacterLiteral().isEmpty();
			case AdaPackage.EXPRESSION_LIST__ENUMERATION_LITERAL:
				return !getEnumerationLiteral().isEmpty();
			case AdaPackage.EXPRESSION_LIST__EXPLICIT_DEREFERENCE:
				return !getExplicitDereference().isEmpty();
			case AdaPackage.EXPRESSION_LIST__FUNCTION_CALL:
				return !getFunctionCall().isEmpty();
			case AdaPackage.EXPRESSION_LIST__INDEXED_COMPONENT:
				return !getIndexedComponent().isEmpty();
			case AdaPackage.EXPRESSION_LIST__SLICE:
				return !getSlice().isEmpty();
			case AdaPackage.EXPRESSION_LIST__SELECTED_COMPONENT:
				return !getSelectedComponent().isEmpty();
			case AdaPackage.EXPRESSION_LIST__ACCESS_ATTRIBUTE:
				return !getAccessAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__ADDRESS_ATTRIBUTE:
				return !getAddressAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__ADJACENT_ATTRIBUTE:
				return !getAdjacentAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__AFT_ATTRIBUTE:
				return !getAftAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__ALIGNMENT_ATTRIBUTE:
				return !getAlignmentAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__BASE_ATTRIBUTE:
				return !getBaseAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__BIT_ORDER_ATTRIBUTE:
				return !getBitOrderAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__BODY_VERSION_ATTRIBUTE:
				return !getBodyVersionAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__CALLABLE_ATTRIBUTE:
				return !getCallableAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__CALLER_ATTRIBUTE:
				return !getCallerAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__CEILING_ATTRIBUTE:
				return !getCeilingAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__CLASS_ATTRIBUTE:
				return !getClassAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__COMPONENT_SIZE_ATTRIBUTE:
				return !getComponentSizeAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__COMPOSE_ATTRIBUTE:
				return !getComposeAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__CONSTRAINED_ATTRIBUTE:
				return !getConstrainedAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__COPY_SIGN_ATTRIBUTE:
				return !getCopySignAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__COUNT_ATTRIBUTE:
				return !getCountAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__DEFINITE_ATTRIBUTE:
				return !getDefiniteAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__DELTA_ATTRIBUTE:
				return !getDeltaAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__DENORM_ATTRIBUTE:
				return !getDenormAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__DIGITS_ATTRIBUTE:
				return !getDigitsAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__EXPONENT_ATTRIBUTE:
				return !getExponentAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__EXTERNAL_TAG_ATTRIBUTE:
				return !getExternalTagAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__FIRST_ATTRIBUTE:
				return !getFirstAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__FIRST_BIT_ATTRIBUTE:
				return !getFirstBitAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__FLOOR_ATTRIBUTE:
				return !getFloorAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__FORE_ATTRIBUTE:
				return !getForeAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__FRACTION_ATTRIBUTE:
				return !getFractionAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__IDENTITY_ATTRIBUTE:
				return !getIdentityAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__IMAGE_ATTRIBUTE:
				return !getImageAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__INPUT_ATTRIBUTE:
				return !getInputAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__LAST_ATTRIBUTE:
				return !getLastAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__LAST_BIT_ATTRIBUTE:
				return !getLastBitAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__LEADING_PART_ATTRIBUTE:
				return !getLeadingPartAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__LENGTH_ATTRIBUTE:
				return !getLengthAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__MACHINE_ATTRIBUTE:
				return !getMachineAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__MACHINE_EMAX_ATTRIBUTE:
				return !getMachineEmaxAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__MACHINE_EMIN_ATTRIBUTE:
				return !getMachineEminAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__MACHINE_MANTISSA_ATTRIBUTE:
				return !getMachineMantissaAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__MACHINE_OVERFLOWS_ATTRIBUTE:
				return !getMachineOverflowsAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__MACHINE_RADIX_ATTRIBUTE:
				return !getMachineRadixAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__MACHINE_ROUNDS_ATTRIBUTE:
				return !getMachineRoundsAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__MAX_ATTRIBUTE:
				return !getMaxAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE:
				return !getMaxSizeInStorageElementsAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__MIN_ATTRIBUTE:
				return !getMinAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__MODEL_ATTRIBUTE:
				return !getModelAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__MODEL_EMIN_ATTRIBUTE:
				return !getModelEminAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__MODEL_EPSILON_ATTRIBUTE:
				return !getModelEpsilonAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__MODEL_MANTISSA_ATTRIBUTE:
				return !getModelMantissaAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__MODEL_SMALL_ATTRIBUTE:
				return !getModelSmallAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__MODULUS_ATTRIBUTE:
				return !getModulusAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__OUTPUT_ATTRIBUTE:
				return !getOutputAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__PARTITION_ID_ATTRIBUTE:
				return !getPartitionIdAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__POS_ATTRIBUTE:
				return !getPosAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__POSITION_ATTRIBUTE:
				return !getPositionAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__PRED_ATTRIBUTE:
				return !getPredAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__RANGE_ATTRIBUTE:
				return !getRangeAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__READ_ATTRIBUTE:
				return !getReadAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__REMAINDER_ATTRIBUTE:
				return !getRemainderAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__ROUND_ATTRIBUTE:
				return !getRoundAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__ROUNDING_ATTRIBUTE:
				return !getRoundingAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__SAFE_FIRST_ATTRIBUTE:
				return !getSafeFirstAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__SAFE_LAST_ATTRIBUTE:
				return !getSafeLastAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__SCALE_ATTRIBUTE:
				return !getScaleAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__SCALING_ATTRIBUTE:
				return !getScalingAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__SIGNED_ZEROS_ATTRIBUTE:
				return !getSignedZerosAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__SIZE_ATTRIBUTE:
				return !getSizeAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__SMALL_ATTRIBUTE:
				return !getSmallAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__STORAGE_POOL_ATTRIBUTE:
				return !getStoragePoolAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__STORAGE_SIZE_ATTRIBUTE:
				return !getStorageSizeAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__SUCC_ATTRIBUTE:
				return !getSuccAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__TAG_ATTRIBUTE:
				return !getTagAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__TERMINATED_ATTRIBUTE:
				return !getTerminatedAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__TRUNCATION_ATTRIBUTE:
				return !getTruncationAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__UNBIASED_ROUNDING_ATTRIBUTE:
				return !getUnbiasedRoundingAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__UNCHECKED_ACCESS_ATTRIBUTE:
				return !getUncheckedAccessAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__VAL_ATTRIBUTE:
				return !getValAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__VALID_ATTRIBUTE:
				return !getValidAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__VALUE_ATTRIBUTE:
				return !getValueAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__VERSION_ATTRIBUTE:
				return !getVersionAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__WIDE_IMAGE_ATTRIBUTE:
				return !getWideImageAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__WIDE_VALUE_ATTRIBUTE:
				return !getWideValueAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__WIDE_WIDTH_ATTRIBUTE:
				return !getWideWidthAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__WIDTH_ATTRIBUTE:
				return !getWidthAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__WRITE_ATTRIBUTE:
				return !getWriteAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__MACHINE_ROUNDING_ATTRIBUTE:
				return !getMachineRoundingAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__MOD_ATTRIBUTE:
				return !getModAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__PRIORITY_ATTRIBUTE:
				return !getPriorityAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__STREAM_SIZE_ATTRIBUTE:
				return !getStreamSizeAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__WIDE_WIDE_IMAGE_ATTRIBUTE:
				return !getWideWideImageAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__WIDE_WIDE_VALUE_ATTRIBUTE:
				return !getWideWideValueAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__WIDE_WIDE_WIDTH_ATTRIBUTE:
				return !getWideWideWidthAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE:
				return !getMaxAlignmentForAllocationAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__OVERLAPS_STORAGE_ATTRIBUTE:
				return !getOverlapsStorageAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__IMPLEMENTATION_DEFINED_ATTRIBUTE:
				return !getImplementationDefinedAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__UNKNOWN_ATTRIBUTE:
				return !getUnknownAttribute().isEmpty();
			case AdaPackage.EXPRESSION_LIST__RECORD_AGGREGATE:
				return !getRecordAggregate().isEmpty();
			case AdaPackage.EXPRESSION_LIST__EXTENSION_AGGREGATE:
				return !getExtensionAggregate().isEmpty();
			case AdaPackage.EXPRESSION_LIST__POSITIONAL_ARRAY_AGGREGATE:
				return !getPositionalArrayAggregate().isEmpty();
			case AdaPackage.EXPRESSION_LIST__NAMED_ARRAY_AGGREGATE:
				return !getNamedArrayAggregate().isEmpty();
			case AdaPackage.EXPRESSION_LIST__AND_THEN_SHORT_CIRCUIT:
				return !getAndThenShortCircuit().isEmpty();
			case AdaPackage.EXPRESSION_LIST__OR_ELSE_SHORT_CIRCUIT:
				return !getOrElseShortCircuit().isEmpty();
			case AdaPackage.EXPRESSION_LIST__IN_MEMBERSHIP_TEST:
				return !getInMembershipTest().isEmpty();
			case AdaPackage.EXPRESSION_LIST__NOT_IN_MEMBERSHIP_TEST:
				return !getNotInMembershipTest().isEmpty();
			case AdaPackage.EXPRESSION_LIST__NULL_LITERAL:
				return !getNullLiteral().isEmpty();
			case AdaPackage.EXPRESSION_LIST__PARENTHESIZED_EXPRESSION:
				return !getParenthesizedExpression().isEmpty();
			case AdaPackage.EXPRESSION_LIST__RAISE_EXPRESSION:
				return !getRaiseExpression().isEmpty();
			case AdaPackage.EXPRESSION_LIST__TYPE_CONVERSION:
				return !getTypeConversion().isEmpty();
			case AdaPackage.EXPRESSION_LIST__QUALIFIED_EXPRESSION:
				return !getQualifiedExpression().isEmpty();
			case AdaPackage.EXPRESSION_LIST__ALLOCATION_FROM_SUBTYPE:
				return !getAllocationFromSubtype().isEmpty();
			case AdaPackage.EXPRESSION_LIST__ALLOCATION_FROM_QUALIFIED_EXPRESSION:
				return !getAllocationFromQualifiedExpression().isEmpty();
			case AdaPackage.EXPRESSION_LIST__CASE_EXPRESSION:
				return !getCaseExpression().isEmpty();
			case AdaPackage.EXPRESSION_LIST__IF_EXPRESSION:
				return !getIfExpression().isEmpty();
			case AdaPackage.EXPRESSION_LIST__FOR_ALL_QUANTIFIED_EXPRESSION:
				return !getForAllQuantifiedExpression().isEmpty();
			case AdaPackage.EXPRESSION_LIST__FOR_SOME_QUANTIFIED_EXPRESSION:
				return !getForSomeQuantifiedExpression().isEmpty();
			case AdaPackage.EXPRESSION_LIST__COMMENT:
				return !getComment().isEmpty();
			case AdaPackage.EXPRESSION_LIST__ALL_CALLS_REMOTE_PRAGMA:
				return !getAllCallsRemotePragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__ASYNCHRONOUS_PRAGMA:
				return !getAsynchronousPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__ATOMIC_PRAGMA:
				return !getAtomicPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__ATOMIC_COMPONENTS_PRAGMA:
				return !getAtomicComponentsPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__ATTACH_HANDLER_PRAGMA:
				return !getAttachHandlerPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__CONTROLLED_PRAGMA:
				return !getControlledPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__CONVENTION_PRAGMA:
				return !getConventionPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__DISCARD_NAMES_PRAGMA:
				return !getDiscardNamesPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__ELABORATE_PRAGMA:
				return !getElaboratePragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__ELABORATE_ALL_PRAGMA:
				return !getElaborateAllPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__ELABORATE_BODY_PRAGMA:
				return !getElaborateBodyPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__EXPORT_PRAGMA:
				return !getExportPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__IMPORT_PRAGMA:
				return !getImportPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__INLINE_PRAGMA:
				return !getInlinePragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__INSPECTION_POINT_PRAGMA:
				return !getInspectionPointPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__INTERRUPT_HANDLER_PRAGMA:
				return !getInterruptHandlerPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__INTERRUPT_PRIORITY_PRAGMA:
				return !getInterruptPriorityPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__LINKER_OPTIONS_PRAGMA:
				return !getLinkerOptionsPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__LIST_PRAGMA:
				return !getListPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__LOCKING_POLICY_PRAGMA:
				return !getLockingPolicyPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__NORMALIZE_SCALARS_PRAGMA:
				return !getNormalizeScalarsPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__OPTIMIZE_PRAGMA:
				return !getOptimizePragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__PACK_PRAGMA:
				return !getPackPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__PAGE_PRAGMA:
				return !getPagePragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__PREELABORATE_PRAGMA:
				return !getPreelaboratePragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__PRIORITY_PRAGMA:
				return !getPriorityPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__PURE_PRAGMA:
				return !getPurePragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__QUEUING_POLICY_PRAGMA:
				return !getQueuingPolicyPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				return !getRemoteCallInterfacePragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__REMOTE_TYPES_PRAGMA:
				return !getRemoteTypesPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__RESTRICTIONS_PRAGMA:
				return !getRestrictionsPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__REVIEWABLE_PRAGMA:
				return !getReviewablePragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__SHARED_PASSIVE_PRAGMA:
				return !getSharedPassivePragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__STORAGE_SIZE_PRAGMA:
				return !getStorageSizePragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__SUPPRESS_PRAGMA:
				return !getSuppressPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				return !getTaskDispatchingPolicyPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__VOLATILE_PRAGMA:
				return !getVolatilePragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__VOLATILE_COMPONENTS_PRAGMA:
				return !getVolatileComponentsPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__ASSERT_PRAGMA:
				return !getAssertPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__ASSERTION_POLICY_PRAGMA:
				return !getAssertionPolicyPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__DETECT_BLOCKING_PRAGMA:
				return !getDetectBlockingPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__NO_RETURN_PRAGMA:
				return !getNoReturnPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				return !getPartitionElaborationPolicyPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				return !getPreelaborableInitializationPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return !getPrioritySpecificDispatchingPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__PROFILE_PRAGMA:
				return !getProfilePragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__RELATIVE_DEADLINE_PRAGMA:
				return !getRelativeDeadlinePragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__UNCHECKED_UNION_PRAGMA:
				return !getUncheckedUnionPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__UNSUPPRESS_PRAGMA:
				return !getUnsuppressPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				return !getDefaultStoragePoolPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__DISPATCHING_DOMAIN_PRAGMA:
				return !getDispatchingDomainPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__CPU_PRAGMA:
				return !getCpuPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__INDEPENDENT_PRAGMA:
				return !getIndependentPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				return !getIndependentComponentsPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				return !getImplementationDefinedPragma().isEmpty();
			case AdaPackage.EXPRESSION_LIST__UNKNOWN_PRAGMA:
				return !getUnknownPragma().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(')');
		return result.toString();
	}

} //ExpressionListImpl
