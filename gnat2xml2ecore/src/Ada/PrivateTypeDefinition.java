/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Private Type Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.PrivateTypeDefinition#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.PrivateTypeDefinition#getHasAbstractQ <em>Has Abstract Q</em>}</li>
 *   <li>{@link Ada.PrivateTypeDefinition#getHasLimitedQ <em>Has Limited Q</em>}</li>
 *   <li>{@link Ada.PrivateTypeDefinition#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getPrivateTypeDefinition()
 * @model extendedMetaData="name='Private_Type_Definition' kind='elementOnly'"
 * @generated
 */
public interface PrivateTypeDefinition extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getPrivateTypeDefinition_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.PrivateTypeDefinition#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Has Abstract Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Abstract Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Abstract Q</em>' containment reference.
	 * @see #setHasAbstractQ(HasAbstractQType2)
	 * @see Ada.AdaPackage#getPrivateTypeDefinition_HasAbstractQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='has_abstract_q' namespace='##targetNamespace'"
	 * @generated
	 */
	HasAbstractQType2 getHasAbstractQ();

	/**
	 * Sets the value of the '{@link Ada.PrivateTypeDefinition#getHasAbstractQ <em>Has Abstract Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Abstract Q</em>' containment reference.
	 * @see #getHasAbstractQ()
	 * @generated
	 */
	void setHasAbstractQ(HasAbstractQType2 value);

	/**
	 * Returns the value of the '<em><b>Has Limited Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Limited Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Limited Q</em>' containment reference.
	 * @see #setHasLimitedQ(HasLimitedQType5)
	 * @see Ada.AdaPackage#getPrivateTypeDefinition_HasLimitedQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='has_limited_q' namespace='##targetNamespace'"
	 * @generated
	 */
	HasLimitedQType5 getHasLimitedQ();

	/**
	 * Sets the value of the '{@link Ada.PrivateTypeDefinition#getHasLimitedQ <em>Has Limited Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Limited Q</em>' containment reference.
	 * @see #getHasLimitedQ()
	 * @generated
	 */
	void setHasLimitedQ(HasLimitedQType5 value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getPrivateTypeDefinition_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.PrivateTypeDefinition#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // PrivateTypeDefinition
