/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.DefiningNameList;
import Ada.ElementClass;
import Ada.ElementIteratorSpecification;
import Ada.HasReverseQType;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Element Iterator Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.ElementIteratorSpecificationImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.ElementIteratorSpecificationImpl#getNamesQl <em>Names Ql</em>}</li>
 *   <li>{@link Ada.impl.ElementIteratorSpecificationImpl#getSubtypeIndicationQ <em>Subtype Indication Q</em>}</li>
 *   <li>{@link Ada.impl.ElementIteratorSpecificationImpl#getHasReverseQ <em>Has Reverse Q</em>}</li>
 *   <li>{@link Ada.impl.ElementIteratorSpecificationImpl#getIterationSchemeNameQ <em>Iteration Scheme Name Q</em>}</li>
 *   <li>{@link Ada.impl.ElementIteratorSpecificationImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ElementIteratorSpecificationImpl extends MinimalEObjectImpl.Container implements ElementIteratorSpecification {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getNamesQl() <em>Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamesQl()
	 * @generated
	 * @ordered
	 */
	protected DefiningNameList namesQl;

	/**
	 * The cached value of the '{@link #getSubtypeIndicationQ() <em>Subtype Indication Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubtypeIndicationQ()
	 * @generated
	 * @ordered
	 */
	protected ElementClass subtypeIndicationQ;

	/**
	 * The cached value of the '{@link #getHasReverseQ() <em>Has Reverse Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasReverseQ()
	 * @generated
	 * @ordered
	 */
	protected HasReverseQType hasReverseQ;

	/**
	 * The cached value of the '{@link #getIterationSchemeNameQ() <em>Iteration Scheme Name Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIterationSchemeNameQ()
	 * @generated
	 * @ordered
	 */
	protected ElementClass iterationSchemeNameQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ElementIteratorSpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getElementIteratorSpecification();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNameList getNamesQl() {
		return namesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNamesQl(DefiningNameList newNamesQl, NotificationChain msgs) {
		DefiningNameList oldNamesQl = namesQl;
		namesQl = newNamesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__NAMES_QL, oldNamesQl, newNamesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNamesQl(DefiningNameList newNamesQl) {
		if (newNamesQl != namesQl) {
			NotificationChain msgs = null;
			if (namesQl != null)
				msgs = ((InternalEObject)namesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__NAMES_QL, null, msgs);
			if (newNamesQl != null)
				msgs = ((InternalEObject)newNamesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__NAMES_QL, null, msgs);
			msgs = basicSetNamesQl(newNamesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__NAMES_QL, newNamesQl, newNamesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementClass getSubtypeIndicationQ() {
		return subtypeIndicationQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSubtypeIndicationQ(ElementClass newSubtypeIndicationQ, NotificationChain msgs) {
		ElementClass oldSubtypeIndicationQ = subtypeIndicationQ;
		subtypeIndicationQ = newSubtypeIndicationQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__SUBTYPE_INDICATION_Q, oldSubtypeIndicationQ, newSubtypeIndicationQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubtypeIndicationQ(ElementClass newSubtypeIndicationQ) {
		if (newSubtypeIndicationQ != subtypeIndicationQ) {
			NotificationChain msgs = null;
			if (subtypeIndicationQ != null)
				msgs = ((InternalEObject)subtypeIndicationQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__SUBTYPE_INDICATION_Q, null, msgs);
			if (newSubtypeIndicationQ != null)
				msgs = ((InternalEObject)newSubtypeIndicationQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__SUBTYPE_INDICATION_Q, null, msgs);
			msgs = basicSetSubtypeIndicationQ(newSubtypeIndicationQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__SUBTYPE_INDICATION_Q, newSubtypeIndicationQ, newSubtypeIndicationQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasReverseQType getHasReverseQ() {
		return hasReverseQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasReverseQ(HasReverseQType newHasReverseQ, NotificationChain msgs) {
		HasReverseQType oldHasReverseQ = hasReverseQ;
		hasReverseQ = newHasReverseQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__HAS_REVERSE_Q, oldHasReverseQ, newHasReverseQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasReverseQ(HasReverseQType newHasReverseQ) {
		if (newHasReverseQ != hasReverseQ) {
			NotificationChain msgs = null;
			if (hasReverseQ != null)
				msgs = ((InternalEObject)hasReverseQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__HAS_REVERSE_Q, null, msgs);
			if (newHasReverseQ != null)
				msgs = ((InternalEObject)newHasReverseQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__HAS_REVERSE_Q, null, msgs);
			msgs = basicSetHasReverseQ(newHasReverseQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__HAS_REVERSE_Q, newHasReverseQ, newHasReverseQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementClass getIterationSchemeNameQ() {
		return iterationSchemeNameQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIterationSchemeNameQ(ElementClass newIterationSchemeNameQ, NotificationChain msgs) {
		ElementClass oldIterationSchemeNameQ = iterationSchemeNameQ;
		iterationSchemeNameQ = newIterationSchemeNameQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__ITERATION_SCHEME_NAME_Q, oldIterationSchemeNameQ, newIterationSchemeNameQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIterationSchemeNameQ(ElementClass newIterationSchemeNameQ) {
		if (newIterationSchemeNameQ != iterationSchemeNameQ) {
			NotificationChain msgs = null;
			if (iterationSchemeNameQ != null)
				msgs = ((InternalEObject)iterationSchemeNameQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__ITERATION_SCHEME_NAME_Q, null, msgs);
			if (newIterationSchemeNameQ != null)
				msgs = ((InternalEObject)newIterationSchemeNameQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__ITERATION_SCHEME_NAME_Q, null, msgs);
			msgs = basicSetIterationSchemeNameQ(newIterationSchemeNameQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__ITERATION_SCHEME_NAME_Q, newIterationSchemeNameQ, newIterationSchemeNameQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__NAMES_QL:
				return basicSetNamesQl(null, msgs);
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__SUBTYPE_INDICATION_Q:
				return basicSetSubtypeIndicationQ(null, msgs);
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__HAS_REVERSE_Q:
				return basicSetHasReverseQ(null, msgs);
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__ITERATION_SCHEME_NAME_Q:
				return basicSetIterationSchemeNameQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__SLOC:
				return getSloc();
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__NAMES_QL:
				return getNamesQl();
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__SUBTYPE_INDICATION_Q:
				return getSubtypeIndicationQ();
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__HAS_REVERSE_Q:
				return getHasReverseQ();
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__ITERATION_SCHEME_NAME_Q:
				return getIterationSchemeNameQ();
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__NAMES_QL:
				setNamesQl((DefiningNameList)newValue);
				return;
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__SUBTYPE_INDICATION_Q:
				setSubtypeIndicationQ((ElementClass)newValue);
				return;
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__HAS_REVERSE_Q:
				setHasReverseQ((HasReverseQType)newValue);
				return;
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__ITERATION_SCHEME_NAME_Q:
				setIterationSchemeNameQ((ElementClass)newValue);
				return;
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__NAMES_QL:
				setNamesQl((DefiningNameList)null);
				return;
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__SUBTYPE_INDICATION_Q:
				setSubtypeIndicationQ((ElementClass)null);
				return;
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__HAS_REVERSE_Q:
				setHasReverseQ((HasReverseQType)null);
				return;
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__ITERATION_SCHEME_NAME_Q:
				setIterationSchemeNameQ((ElementClass)null);
				return;
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__SLOC:
				return sloc != null;
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__NAMES_QL:
				return namesQl != null;
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__SUBTYPE_INDICATION_Q:
				return subtypeIndicationQ != null;
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__HAS_REVERSE_Q:
				return hasReverseQ != null;
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__ITERATION_SCHEME_NAME_Q:
				return iterationSchemeNameQ != null;
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //ElementIteratorSpecificationImpl
