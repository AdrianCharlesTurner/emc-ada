/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.DeclarationClass;
import Ada.ExpressionClass;
import Ada.ForSomeQuantifiedExpression;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>For Some Quantified Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.ForSomeQuantifiedExpressionImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.ForSomeQuantifiedExpressionImpl#getIteratorSpecificationQ <em>Iterator Specification Q</em>}</li>
 *   <li>{@link Ada.impl.ForSomeQuantifiedExpressionImpl#getPredicateQ <em>Predicate Q</em>}</li>
 *   <li>{@link Ada.impl.ForSomeQuantifiedExpressionImpl#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.impl.ForSomeQuantifiedExpressionImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ForSomeQuantifiedExpressionImpl extends MinimalEObjectImpl.Container implements ForSomeQuantifiedExpression {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getIteratorSpecificationQ() <em>Iterator Specification Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIteratorSpecificationQ()
	 * @generated
	 * @ordered
	 */
	protected DeclarationClass iteratorSpecificationQ;

	/**
	 * The cached value of the '{@link #getPredicateQ() <em>Predicate Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPredicateQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass predicateQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ForSomeQuantifiedExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getForSomeQuantifiedExpression();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeclarationClass getIteratorSpecificationQ() {
		return iteratorSpecificationQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIteratorSpecificationQ(DeclarationClass newIteratorSpecificationQ, NotificationChain msgs) {
		DeclarationClass oldIteratorSpecificationQ = iteratorSpecificationQ;
		iteratorSpecificationQ = newIteratorSpecificationQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__ITERATOR_SPECIFICATION_Q, oldIteratorSpecificationQ, newIteratorSpecificationQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIteratorSpecificationQ(DeclarationClass newIteratorSpecificationQ) {
		if (newIteratorSpecificationQ != iteratorSpecificationQ) {
			NotificationChain msgs = null;
			if (iteratorSpecificationQ != null)
				msgs = ((InternalEObject)iteratorSpecificationQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__ITERATOR_SPECIFICATION_Q, null, msgs);
			if (newIteratorSpecificationQ != null)
				msgs = ((InternalEObject)newIteratorSpecificationQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__ITERATOR_SPECIFICATION_Q, null, msgs);
			msgs = basicSetIteratorSpecificationQ(newIteratorSpecificationQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__ITERATOR_SPECIFICATION_Q, newIteratorSpecificationQ, newIteratorSpecificationQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getPredicateQ() {
		return predicateQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPredicateQ(ExpressionClass newPredicateQ, NotificationChain msgs) {
		ExpressionClass oldPredicateQ = predicateQ;
		predicateQ = newPredicateQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__PREDICATE_Q, oldPredicateQ, newPredicateQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPredicateQ(ExpressionClass newPredicateQ) {
		if (newPredicateQ != predicateQ) {
			NotificationChain msgs = null;
			if (predicateQ != null)
				msgs = ((InternalEObject)predicateQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__PREDICATE_Q, null, msgs);
			if (newPredicateQ != null)
				msgs = ((InternalEObject)newPredicateQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__PREDICATE_Q, null, msgs);
			msgs = basicSetPredicateQ(newPredicateQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__PREDICATE_Q, newPredicateQ, newPredicateQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__ITERATOR_SPECIFICATION_Q:
				return basicSetIteratorSpecificationQ(null, msgs);
			case AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__PREDICATE_Q:
				return basicSetPredicateQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__SLOC:
				return getSloc();
			case AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__ITERATOR_SPECIFICATION_Q:
				return getIteratorSpecificationQ();
			case AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__PREDICATE_Q:
				return getPredicateQ();
			case AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__CHECKS:
				return getChecks();
			case AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__TYPE:
				return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__ITERATOR_SPECIFICATION_Q:
				setIteratorSpecificationQ((DeclarationClass)newValue);
				return;
			case AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__PREDICATE_Q:
				setPredicateQ((ExpressionClass)newValue);
				return;
			case AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__CHECKS:
				setChecks((String)newValue);
				return;
			case AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__TYPE:
				setType((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__ITERATOR_SPECIFICATION_Q:
				setIteratorSpecificationQ((DeclarationClass)null);
				return;
			case AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__PREDICATE_Q:
				setPredicateQ((ExpressionClass)null);
				return;
			case AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
			case AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__TYPE:
				setType(TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__SLOC:
				return sloc != null;
			case AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__ITERATOR_SPECIFICATION_Q:
				return iteratorSpecificationQ != null;
			case AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__PREDICATE_Q:
				return predicateQ != null;
			case AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
			case AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION__TYPE:
				return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(", type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //ForSomeQuantifiedExpressionImpl
