/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>For Loop Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.ForLoopStatement#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.ForLoopStatement#getLabelNamesQl <em>Label Names Ql</em>}</li>
 *   <li>{@link Ada.ForLoopStatement#getStatementIdentifierQ <em>Statement Identifier Q</em>}</li>
 *   <li>{@link Ada.ForLoopStatement#getForLoopParameterSpecificationQ <em>For Loop Parameter Specification Q</em>}</li>
 *   <li>{@link Ada.ForLoopStatement#getLoopStatementsQl <em>Loop Statements Ql</em>}</li>
 *   <li>{@link Ada.ForLoopStatement#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getForLoopStatement()
 * @model extendedMetaData="name='For_Loop_Statement' kind='elementOnly'"
 * @generated
 */
public interface ForLoopStatement extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getForLoopStatement_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.ForLoopStatement#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Label Names Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label Names Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label Names Ql</em>' containment reference.
	 * @see #setLabelNamesQl(DefiningNameList)
	 * @see Ada.AdaPackage#getForLoopStatement_LabelNamesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='label_names_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameList getLabelNamesQl();

	/**
	 * Sets the value of the '{@link Ada.ForLoopStatement#getLabelNamesQl <em>Label Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label Names Ql</em>' containment reference.
	 * @see #getLabelNamesQl()
	 * @generated
	 */
	void setLabelNamesQl(DefiningNameList value);

	/**
	 * Returns the value of the '<em><b>Statement Identifier Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Statement Identifier Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Statement Identifier Q</em>' containment reference.
	 * @see #setStatementIdentifierQ(DefiningNameClass)
	 * @see Ada.AdaPackage#getForLoopStatement_StatementIdentifierQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='statement_identifier_q' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameClass getStatementIdentifierQ();

	/**
	 * Sets the value of the '{@link Ada.ForLoopStatement#getStatementIdentifierQ <em>Statement Identifier Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Statement Identifier Q</em>' containment reference.
	 * @see #getStatementIdentifierQ()
	 * @generated
	 */
	void setStatementIdentifierQ(DefiningNameClass value);

	/**
	 * Returns the value of the '<em><b>For Loop Parameter Specification Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>For Loop Parameter Specification Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>For Loop Parameter Specification Q</em>' containment reference.
	 * @see #setForLoopParameterSpecificationQ(DeclarationClass)
	 * @see Ada.AdaPackage#getForLoopStatement_ForLoopParameterSpecificationQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='for_loop_parameter_specification_q' namespace='##targetNamespace'"
	 * @generated
	 */
	DeclarationClass getForLoopParameterSpecificationQ();

	/**
	 * Sets the value of the '{@link Ada.ForLoopStatement#getForLoopParameterSpecificationQ <em>For Loop Parameter Specification Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>For Loop Parameter Specification Q</em>' containment reference.
	 * @see #getForLoopParameterSpecificationQ()
	 * @generated
	 */
	void setForLoopParameterSpecificationQ(DeclarationClass value);

	/**
	 * Returns the value of the '<em><b>Loop Statements Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Loop Statements Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Loop Statements Ql</em>' containment reference.
	 * @see #setLoopStatementsQl(StatementList)
	 * @see Ada.AdaPackage#getForLoopStatement_LoopStatementsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='loop_statements_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	StatementList getLoopStatementsQl();

	/**
	 * Sets the value of the '{@link Ada.ForLoopStatement#getLoopStatementsQl <em>Loop Statements Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Loop Statements Ql</em>' containment reference.
	 * @see #getLoopStatementsQl()
	 * @generated
	 */
	void setLoopStatementsQl(StatementList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getForLoopStatement_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.ForLoopStatement#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // ForLoopStatement
