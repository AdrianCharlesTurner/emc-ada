/**
 */
package Ada.impl;

import Ada.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AdaFactoryImpl extends EFactoryImpl implements AdaFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AdaFactory init() {
		try {
			AdaFactory theAdaFactory = (AdaFactory)EPackage.Registry.INSTANCE.getEFactory(AdaPackage.eNS_URI);
			if (theAdaFactory != null) {
				return theAdaFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new AdaFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdaFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case AdaPackage.ABORT_STATEMENT: return createAbortStatement();
			case AdaPackage.ABS_OPERATOR: return createAbsOperator();
			case AdaPackage.ABSTRACT: return createAbstract();
			case AdaPackage.ACCEPT_STATEMENT: return createAcceptStatement();
			case AdaPackage.ACCESS_ATTRIBUTE: return createAccessAttribute();
			case AdaPackage.ACCESS_TO_CONSTANT: return createAccessToConstant();
			case AdaPackage.ACCESS_TO_FUNCTION: return createAccessToFunction();
			case AdaPackage.ACCESS_TO_PROCEDURE: return createAccessToProcedure();
			case AdaPackage.ACCESS_TO_PROTECTED_FUNCTION: return createAccessToProtectedFunction();
			case AdaPackage.ACCESS_TO_PROTECTED_PROCEDURE: return createAccessToProtectedProcedure();
			case AdaPackage.ACCESS_TO_VARIABLE: return createAccessToVariable();
			case AdaPackage.ADDRESS_ATTRIBUTE: return createAddressAttribute();
			case AdaPackage.ADJACENT_ATTRIBUTE: return createAdjacentAttribute();
			case AdaPackage.AFT_ATTRIBUTE: return createAftAttribute();
			case AdaPackage.ALIASED: return createAliased();
			case AdaPackage.ALIGNMENT_ATTRIBUTE: return createAlignmentAttribute();
			case AdaPackage.ALL_CALLS_REMOTE_PRAGMA: return createAllCallsRemotePragma();
			case AdaPackage.ALLOCATION_FROM_QUALIFIED_EXPRESSION: return createAllocationFromQualifiedExpression();
			case AdaPackage.ALLOCATION_FROM_SUBTYPE: return createAllocationFromSubtype();
			case AdaPackage.AND_OPERATOR: return createAndOperator();
			case AdaPackage.AND_THEN_SHORT_CIRCUIT: return createAndThenShortCircuit();
			case AdaPackage.ANONYMOUS_ACCESS_TO_CONSTANT: return createAnonymousAccessToConstant();
			case AdaPackage.ANONYMOUS_ACCESS_TO_FUNCTION: return createAnonymousAccessToFunction();
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROCEDURE: return createAnonymousAccessToProcedure();
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION: return createAnonymousAccessToProtectedFunction();
			case AdaPackage.ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE: return createAnonymousAccessToProtectedProcedure();
			case AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE: return createAnonymousAccessToVariable();
			case AdaPackage.ARRAY_COMPONENT_ASSOCIATION: return createArrayComponentAssociation();
			case AdaPackage.ASPECT_SPECIFICATION: return createAspectSpecification();
			case AdaPackage.ASSERTION_POLICY_PRAGMA: return createAssertionPolicyPragma();
			case AdaPackage.ASSERT_PRAGMA: return createAssertPragma();
			case AdaPackage.ASSIGNMENT_STATEMENT: return createAssignmentStatement();
			case AdaPackage.ASSOCIATION_CLASS: return createAssociationClass();
			case AdaPackage.ASSOCIATION_LIST: return createAssociationList();
			case AdaPackage.ASYNCHRONOUS_PRAGMA: return createAsynchronousPragma();
			case AdaPackage.ASYNCHRONOUS_SELECT_STATEMENT: return createAsynchronousSelectStatement();
			case AdaPackage.AT_CLAUSE: return createAtClause();
			case AdaPackage.ATOMIC_COMPONENTS_PRAGMA: return createAtomicComponentsPragma();
			case AdaPackage.ATOMIC_PRAGMA: return createAtomicPragma();
			case AdaPackage.ATTACH_HANDLER_PRAGMA: return createAttachHandlerPragma();
			case AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE: return createAttributeDefinitionClause();
			case AdaPackage.BASE_ATTRIBUTE: return createBaseAttribute();
			case AdaPackage.BIT_ORDER_ATTRIBUTE: return createBitOrderAttribute();
			case AdaPackage.BLOCK_STATEMENT: return createBlockStatement();
			case AdaPackage.BODY_VERSION_ATTRIBUTE: return createBodyVersionAttribute();
			case AdaPackage.BOX_EXPRESSION: return createBoxExpression();
			case AdaPackage.CALLABLE_ATTRIBUTE: return createCallableAttribute();
			case AdaPackage.CALLER_ATTRIBUTE: return createCallerAttribute();
			case AdaPackage.CASE_EXPRESSION: return createCaseExpression();
			case AdaPackage.CASE_EXPRESSION_PATH: return createCaseExpressionPath();
			case AdaPackage.CASE_PATH: return createCasePath();
			case AdaPackage.CASE_STATEMENT: return createCaseStatement();
			case AdaPackage.CEILING_ATTRIBUTE: return createCeilingAttribute();
			case AdaPackage.CHARACTER_LITERAL: return createCharacterLiteral();
			case AdaPackage.CHOICE_PARAMETER_SPECIFICATION: return createChoiceParameterSpecification();
			case AdaPackage.CLASS_ATTRIBUTE: return createClassAttribute();
			case AdaPackage.CODE_STATEMENT: return createCodeStatement();
			case AdaPackage.COMMENT: return createComment();
			case AdaPackage.COMPILATION_UNIT: return createCompilationUnit();
			case AdaPackage.COMPONENT_CLAUSE: return createComponentClause();
			case AdaPackage.COMPONENT_CLAUSE_LIST: return createComponentClauseList();
			case AdaPackage.COMPONENT_DECLARATION: return createComponentDeclaration();
			case AdaPackage.COMPONENT_DEFINITION: return createComponentDefinition();
			case AdaPackage.COMPONENT_SIZE_ATTRIBUTE: return createComponentSizeAttribute();
			case AdaPackage.COMPOSE_ATTRIBUTE: return createComposeAttribute();
			case AdaPackage.CONCATENATE_OPERATOR: return createConcatenateOperator();
			case AdaPackage.CONDITIONAL_ENTRY_CALL_STATEMENT: return createConditionalEntryCallStatement();
			case AdaPackage.CONSTANT_DECLARATION: return createConstantDeclaration();
			case AdaPackage.CONSTRAINED_ARRAY_DEFINITION: return createConstrainedArrayDefinition();
			case AdaPackage.CONSTRAINED_ATTRIBUTE: return createConstrainedAttribute();
			case AdaPackage.CONSTRAINT_CLASS: return createConstraintClass();
			case AdaPackage.CONTEXT_CLAUSE_CLASS: return createContextClauseClass();
			case AdaPackage.CONTEXT_CLAUSE_LIST: return createContextClauseList();
			case AdaPackage.CONTROLLED_PRAGMA: return createControlledPragma();
			case AdaPackage.CONVENTION_PRAGMA: return createConventionPragma();
			case AdaPackage.COPY_SIGN_ATTRIBUTE: return createCopySignAttribute();
			case AdaPackage.COUNT_ATTRIBUTE: return createCountAttribute();
			case AdaPackage.CPU_PRAGMA: return createCpuPragma();
			case AdaPackage.DECIMAL_FIXED_POINT_DEFINITION: return createDecimalFixedPointDefinition();
			case AdaPackage.DECLARATION_CLASS: return createDeclarationClass();
			case AdaPackage.DECLARATION_LIST: return createDeclarationList();
			case AdaPackage.DECLARATIVE_ITEM_CLASS: return createDeclarativeItemClass();
			case AdaPackage.DECLARATIVE_ITEM_LIST: return createDeclarativeItemList();
			case AdaPackage.DEFAULT_STORAGE_POOL_PRAGMA: return createDefaultStoragePoolPragma();
			case AdaPackage.DEFERRED_CONSTANT_DECLARATION: return createDeferredConstantDeclaration();
			case AdaPackage.DEFINING_ABS_OPERATOR: return createDefiningAbsOperator();
			case AdaPackage.DEFINING_AND_OPERATOR: return createDefiningAndOperator();
			case AdaPackage.DEFINING_CHARACTER_LITERAL: return createDefiningCharacterLiteral();
			case AdaPackage.DEFINING_CONCATENATE_OPERATOR: return createDefiningConcatenateOperator();
			case AdaPackage.DEFINING_DIVIDE_OPERATOR: return createDefiningDivideOperator();
			case AdaPackage.DEFINING_ENUMERATION_LITERAL: return createDefiningEnumerationLiteral();
			case AdaPackage.DEFINING_EQUAL_OPERATOR: return createDefiningEqualOperator();
			case AdaPackage.DEFINING_EXPANDED_NAME: return createDefiningExpandedName();
			case AdaPackage.DEFINING_EXPONENTIATE_OPERATOR: return createDefiningExponentiateOperator();
			case AdaPackage.DEFINING_GREATER_THAN_OPERATOR: return createDefiningGreaterThanOperator();
			case AdaPackage.DEFINING_GREATER_THAN_OR_EQUAL_OPERATOR: return createDefiningGreaterThanOrEqualOperator();
			case AdaPackage.DEFINING_IDENTIFIER: return createDefiningIdentifier();
			case AdaPackage.DEFINING_LESS_THAN_OPERATOR: return createDefiningLessThanOperator();
			case AdaPackage.DEFINING_LESS_THAN_OR_EQUAL_OPERATOR: return createDefiningLessThanOrEqualOperator();
			case AdaPackage.DEFINING_MINUS_OPERATOR: return createDefiningMinusOperator();
			case AdaPackage.DEFINING_MOD_OPERATOR: return createDefiningModOperator();
			case AdaPackage.DEFINING_MULTIPLY_OPERATOR: return createDefiningMultiplyOperator();
			case AdaPackage.DEFINING_NAME_CLASS: return createDefiningNameClass();
			case AdaPackage.DEFINING_NAME_LIST: return createDefiningNameList();
			case AdaPackage.DEFINING_NOT_EQUAL_OPERATOR: return createDefiningNotEqualOperator();
			case AdaPackage.DEFINING_NOT_OPERATOR: return createDefiningNotOperator();
			case AdaPackage.DEFINING_OR_OPERATOR: return createDefiningOrOperator();
			case AdaPackage.DEFINING_PLUS_OPERATOR: return createDefiningPlusOperator();
			case AdaPackage.DEFINING_REM_OPERATOR: return createDefiningRemOperator();
			case AdaPackage.DEFINING_UNARY_MINUS_OPERATOR: return createDefiningUnaryMinusOperator();
			case AdaPackage.DEFINING_UNARY_PLUS_OPERATOR: return createDefiningUnaryPlusOperator();
			case AdaPackage.DEFINING_XOR_OPERATOR: return createDefiningXorOperator();
			case AdaPackage.DEFINITE_ATTRIBUTE: return createDefiniteAttribute();
			case AdaPackage.DEFINITION_CLASS: return createDefinitionClass();
			case AdaPackage.DEFINITION_LIST: return createDefinitionList();
			case AdaPackage.DELAY_RELATIVE_STATEMENT: return createDelayRelativeStatement();
			case AdaPackage.DELAY_UNTIL_STATEMENT: return createDelayUntilStatement();
			case AdaPackage.DELTA_ATTRIBUTE: return createDeltaAttribute();
			case AdaPackage.DELTA_CONSTRAINT: return createDeltaConstraint();
			case AdaPackage.DENORM_ATTRIBUTE: return createDenormAttribute();
			case AdaPackage.DERIVED_RECORD_EXTENSION_DEFINITION: return createDerivedRecordExtensionDefinition();
			case AdaPackage.DERIVED_TYPE_DEFINITION: return createDerivedTypeDefinition();
			case AdaPackage.DETECT_BLOCKING_PRAGMA: return createDetectBlockingPragma();
			case AdaPackage.DIGITS_ATTRIBUTE: return createDigitsAttribute();
			case AdaPackage.DIGITS_CONSTRAINT: return createDigitsConstraint();
			case AdaPackage.DISCARD_NAMES_PRAGMA: return createDiscardNamesPragma();
			case AdaPackage.DISCRETE_RANGE_ATTRIBUTE_REFERENCE: return createDiscreteRangeAttributeReference();
			case AdaPackage.DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION: return createDiscreteRangeAttributeReferenceAsSubtypeDefinition();
			case AdaPackage.DISCRETE_RANGE_CLASS: return createDiscreteRangeClass();
			case AdaPackage.DISCRETE_RANGE_LIST: return createDiscreteRangeList();
			case AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE: return createDiscreteSimpleExpressionRange();
			case AdaPackage.DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION: return createDiscreteSimpleExpressionRangeAsSubtypeDefinition();
			case AdaPackage.DISCRETE_SUBTYPE_DEFINITION_CLASS: return createDiscreteSubtypeDefinitionClass();
			case AdaPackage.DISCRETE_SUBTYPE_INDICATION: return createDiscreteSubtypeIndication();
			case AdaPackage.DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION: return createDiscreteSubtypeIndicationAsSubtypeDefinition();
			case AdaPackage.DISCRIMINANT_ASSOCIATION: return createDiscriminantAssociation();
			case AdaPackage.DISCRIMINANT_ASSOCIATION_LIST: return createDiscriminantAssociationList();
			case AdaPackage.DISCRIMINANT_CONSTRAINT: return createDiscriminantConstraint();
			case AdaPackage.DISCRIMINANT_SPECIFICATION: return createDiscriminantSpecification();
			case AdaPackage.DISCRIMINANT_SPECIFICATION_LIST: return createDiscriminantSpecificationList();
			case AdaPackage.DISPATCHING_DOMAIN_PRAGMA: return createDispatchingDomainPragma();
			case AdaPackage.DIVIDE_OPERATOR: return createDivideOperator();
			case AdaPackage.DOCUMENT_ROOT: return createDocumentRoot();
			case AdaPackage.ELABORATE_ALL_PRAGMA: return createElaborateAllPragma();
			case AdaPackage.ELABORATE_BODY_PRAGMA: return createElaborateBodyPragma();
			case AdaPackage.ELABORATE_PRAGMA: return createElaboratePragma();
			case AdaPackage.ELEMENT_CLASS: return createElementClass();
			case AdaPackage.ELEMENT_ITERATOR_SPECIFICATION: return createElementIteratorSpecification();
			case AdaPackage.ELEMENT_LIST: return createElementList();
			case AdaPackage.ELSE_EXPRESSION_PATH: return createElseExpressionPath();
			case AdaPackage.ELSE_PATH: return createElsePath();
			case AdaPackage.ELSIF_EXPRESSION_PATH: return createElsifExpressionPath();
			case AdaPackage.ELSIF_PATH: return createElsifPath();
			case AdaPackage.ENTRY_BODY_DECLARATION: return createEntryBodyDeclaration();
			case AdaPackage.ENTRY_CALL_STATEMENT: return createEntryCallStatement();
			case AdaPackage.ENTRY_DECLARATION: return createEntryDeclaration();
			case AdaPackage.ENTRY_INDEX_SPECIFICATION: return createEntryIndexSpecification();
			case AdaPackage.ENUMERATION_LITERAL: return createEnumerationLiteral();
			case AdaPackage.ENUMERATION_LITERAL_SPECIFICATION: return createEnumerationLiteralSpecification();
			case AdaPackage.ENUMERATION_REPRESENTATION_CLAUSE: return createEnumerationRepresentationClause();
			case AdaPackage.ENUMERATION_TYPE_DEFINITION: return createEnumerationTypeDefinition();
			case AdaPackage.EQUAL_OPERATOR: return createEqualOperator();
			case AdaPackage.EXCEPTION_DECLARATION: return createExceptionDeclaration();
			case AdaPackage.EXCEPTION_HANDLER: return createExceptionHandler();
			case AdaPackage.EXCEPTION_HANDLER_LIST: return createExceptionHandlerList();
			case AdaPackage.EXCEPTION_RENAMING_DECLARATION: return createExceptionRenamingDeclaration();
			case AdaPackage.EXIT_STATEMENT: return createExitStatement();
			case AdaPackage.EXPLICIT_DEREFERENCE: return createExplicitDereference();
			case AdaPackage.EXPONENT_ATTRIBUTE: return createExponentAttribute();
			case AdaPackage.EXPONENTIATE_OPERATOR: return createExponentiateOperator();
			case AdaPackage.EXPORT_PRAGMA: return createExportPragma();
			case AdaPackage.EXPRESSION_CLASS: return createExpressionClass();
			case AdaPackage.EXPRESSION_FUNCTION_DECLARATION: return createExpressionFunctionDeclaration();
			case AdaPackage.EXPRESSION_LIST: return createExpressionList();
			case AdaPackage.EXTENDED_RETURN_STATEMENT: return createExtendedReturnStatement();
			case AdaPackage.EXTENSION_AGGREGATE: return createExtensionAggregate();
			case AdaPackage.EXTERNAL_TAG_ATTRIBUTE: return createExternalTagAttribute();
			case AdaPackage.FIRST_ATTRIBUTE: return createFirstAttribute();
			case AdaPackage.FIRST_BIT_ATTRIBUTE: return createFirstBitAttribute();
			case AdaPackage.FLOATING_POINT_DEFINITION: return createFloatingPointDefinition();
			case AdaPackage.FLOOR_ATTRIBUTE: return createFloorAttribute();
			case AdaPackage.FOR_ALL_QUANTIFIED_EXPRESSION: return createForAllQuantifiedExpression();
			case AdaPackage.FORE_ATTRIBUTE: return createForeAttribute();
			case AdaPackage.FOR_LOOP_STATEMENT: return createForLoopStatement();
			case AdaPackage.FORMAL_ACCESS_TO_CONSTANT: return createFormalAccessToConstant();
			case AdaPackage.FORMAL_ACCESS_TO_FUNCTION: return createFormalAccessToFunction();
			case AdaPackage.FORMAL_ACCESS_TO_PROCEDURE: return createFormalAccessToProcedure();
			case AdaPackage.FORMAL_ACCESS_TO_PROTECTED_FUNCTION: return createFormalAccessToProtectedFunction();
			case AdaPackage.FORMAL_ACCESS_TO_PROTECTED_PROCEDURE: return createFormalAccessToProtectedProcedure();
			case AdaPackage.FORMAL_ACCESS_TO_VARIABLE: return createFormalAccessToVariable();
			case AdaPackage.FORMAL_CONSTRAINED_ARRAY_DEFINITION: return createFormalConstrainedArrayDefinition();
			case AdaPackage.FORMAL_DECIMAL_FIXED_POINT_DEFINITION: return createFormalDecimalFixedPointDefinition();
			case AdaPackage.FORMAL_DERIVED_TYPE_DEFINITION: return createFormalDerivedTypeDefinition();
			case AdaPackage.FORMAL_DISCRETE_TYPE_DEFINITION: return createFormalDiscreteTypeDefinition();
			case AdaPackage.FORMAL_FLOATING_POINT_DEFINITION: return createFormalFloatingPointDefinition();
			case AdaPackage.FORMAL_FUNCTION_DECLARATION: return createFormalFunctionDeclaration();
			case AdaPackage.FORMAL_INCOMPLETE_TYPE_DECLARATION: return createFormalIncompleteTypeDeclaration();
			case AdaPackage.FORMAL_LIMITED_INTERFACE: return createFormalLimitedInterface();
			case AdaPackage.FORMAL_MODULAR_TYPE_DEFINITION: return createFormalModularTypeDefinition();
			case AdaPackage.FORMAL_OBJECT_DECLARATION: return createFormalObjectDeclaration();
			case AdaPackage.FORMAL_ORDINARY_FIXED_POINT_DEFINITION: return createFormalOrdinaryFixedPointDefinition();
			case AdaPackage.FORMAL_ORDINARY_INTERFACE: return createFormalOrdinaryInterface();
			case AdaPackage.FORMAL_PACKAGE_DECLARATION: return createFormalPackageDeclaration();
			case AdaPackage.FORMAL_PACKAGE_DECLARATION_WITH_BOX: return createFormalPackageDeclarationWithBox();
			case AdaPackage.FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE: return createFormalPoolSpecificAccessToVariable();
			case AdaPackage.FORMAL_PRIVATE_TYPE_DEFINITION: return createFormalPrivateTypeDefinition();
			case AdaPackage.FORMAL_PROCEDURE_DECLARATION: return createFormalProcedureDeclaration();
			case AdaPackage.FORMAL_PROTECTED_INTERFACE: return createFormalProtectedInterface();
			case AdaPackage.FORMAL_SIGNED_INTEGER_TYPE_DEFINITION: return createFormalSignedIntegerTypeDefinition();
			case AdaPackage.FORMAL_SYNCHRONIZED_INTERFACE: return createFormalSynchronizedInterface();
			case AdaPackage.FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION: return createFormalTaggedPrivateTypeDefinition();
			case AdaPackage.FORMAL_TASK_INTERFACE: return createFormalTaskInterface();
			case AdaPackage.FORMAL_TYPE_DECLARATION: return createFormalTypeDeclaration();
			case AdaPackage.FORMAL_UNCONSTRAINED_ARRAY_DEFINITION: return createFormalUnconstrainedArrayDefinition();
			case AdaPackage.FOR_SOME_QUANTIFIED_EXPRESSION: return createForSomeQuantifiedExpression();
			case AdaPackage.FRACTION_ATTRIBUTE: return createFractionAttribute();
			case AdaPackage.FUNCTION_BODY_DECLARATION: return createFunctionBodyDeclaration();
			case AdaPackage.FUNCTION_BODY_STUB: return createFunctionBodyStub();
			case AdaPackage.FUNCTION_CALL: return createFunctionCall();
			case AdaPackage.FUNCTION_DECLARATION: return createFunctionDeclaration();
			case AdaPackage.FUNCTION_INSTANTIATION: return createFunctionInstantiation();
			case AdaPackage.FUNCTION_RENAMING_DECLARATION: return createFunctionRenamingDeclaration();
			case AdaPackage.GENERALIZED_ITERATOR_SPECIFICATION: return createGeneralizedIteratorSpecification();
			case AdaPackage.GENERIC_ASSOCIATION: return createGenericAssociation();
			case AdaPackage.GENERIC_FUNCTION_DECLARATION: return createGenericFunctionDeclaration();
			case AdaPackage.GENERIC_FUNCTION_RENAMING_DECLARATION: return createGenericFunctionRenamingDeclaration();
			case AdaPackage.GENERIC_PACKAGE_DECLARATION: return createGenericPackageDeclaration();
			case AdaPackage.GENERIC_PACKAGE_RENAMING_DECLARATION: return createGenericPackageRenamingDeclaration();
			case AdaPackage.GENERIC_PROCEDURE_DECLARATION: return createGenericProcedureDeclaration();
			case AdaPackage.GENERIC_PROCEDURE_RENAMING_DECLARATION: return createGenericProcedureRenamingDeclaration();
			case AdaPackage.GOTO_STATEMENT: return createGotoStatement();
			case AdaPackage.GREATER_THAN_OPERATOR: return createGreaterThanOperator();
			case AdaPackage.GREATER_THAN_OR_EQUAL_OPERATOR: return createGreaterThanOrEqualOperator();
			case AdaPackage.HAS_ABSTRACT_QTYPE: return createHasAbstractQType();
			case AdaPackage.HAS_ABSTRACT_QTYPE1: return createHasAbstractQType1();
			case AdaPackage.HAS_ABSTRACT_QTYPE2: return createHasAbstractQType2();
			case AdaPackage.HAS_ABSTRACT_QTYPE3: return createHasAbstractQType3();
			case AdaPackage.HAS_ABSTRACT_QTYPE4: return createHasAbstractQType4();
			case AdaPackage.HAS_ABSTRACT_QTYPE5: return createHasAbstractQType5();
			case AdaPackage.HAS_ABSTRACT_QTYPE6: return createHasAbstractQType6();
			case AdaPackage.HAS_ABSTRACT_QTYPE7: return createHasAbstractQType7();
			case AdaPackage.HAS_ABSTRACT_QTYPE8: return createHasAbstractQType8();
			case AdaPackage.HAS_ABSTRACT_QTYPE9: return createHasAbstractQType9();
			case AdaPackage.HAS_ABSTRACT_QTYPE10: return createHasAbstractQType10();
			case AdaPackage.HAS_ABSTRACT_QTYPE11: return createHasAbstractQType11();
			case AdaPackage.HAS_ABSTRACT_QTYPE12: return createHasAbstractQType12();
			case AdaPackage.HAS_ABSTRACT_QTYPE13: return createHasAbstractQType13();
			case AdaPackage.HAS_ALIASED_QTYPE: return createHasAliasedQType();
			case AdaPackage.HAS_ALIASED_QTYPE1: return createHasAliasedQType1();
			case AdaPackage.HAS_ALIASED_QTYPE2: return createHasAliasedQType2();
			case AdaPackage.HAS_ALIASED_QTYPE3: return createHasAliasedQType3();
			case AdaPackage.HAS_ALIASED_QTYPE4: return createHasAliasedQType4();
			case AdaPackage.HAS_ALIASED_QTYPE5: return createHasAliasedQType5();
			case AdaPackage.HAS_ALIASED_QTYPE6: return createHasAliasedQType6();
			case AdaPackage.HAS_ALIASED_QTYPE7: return createHasAliasedQType7();
			case AdaPackage.HAS_ALIASED_QTYPE8: return createHasAliasedQType8();
			case AdaPackage.HAS_LIMITED_QTYPE: return createHasLimitedQType();
			case AdaPackage.HAS_LIMITED_QTYPE1: return createHasLimitedQType1();
			case AdaPackage.HAS_LIMITED_QTYPE2: return createHasLimitedQType2();
			case AdaPackage.HAS_LIMITED_QTYPE3: return createHasLimitedQType3();
			case AdaPackage.HAS_LIMITED_QTYPE4: return createHasLimitedQType4();
			case AdaPackage.HAS_LIMITED_QTYPE5: return createHasLimitedQType5();
			case AdaPackage.HAS_LIMITED_QTYPE6: return createHasLimitedQType6();
			case AdaPackage.HAS_LIMITED_QTYPE7: return createHasLimitedQType7();
			case AdaPackage.HAS_LIMITED_QTYPE8: return createHasLimitedQType8();
			case AdaPackage.HAS_LIMITED_QTYPE9: return createHasLimitedQType9();
			case AdaPackage.HAS_LIMITED_QTYPE10: return createHasLimitedQType10();
			case AdaPackage.HAS_LIMITED_QTYPE11: return createHasLimitedQType11();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE: return createHasNullExclusionQType();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE1: return createHasNullExclusionQType1();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE2: return createHasNullExclusionQType2();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE3: return createHasNullExclusionQType3();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE4: return createHasNullExclusionQType4();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE5: return createHasNullExclusionQType5();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE6: return createHasNullExclusionQType6();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE7: return createHasNullExclusionQType7();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE8: return createHasNullExclusionQType8();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE9: return createHasNullExclusionQType9();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE10: return createHasNullExclusionQType10();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE11: return createHasNullExclusionQType11();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE12: return createHasNullExclusionQType12();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE13: return createHasNullExclusionQType13();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE14: return createHasNullExclusionQType14();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE15: return createHasNullExclusionQType15();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE16: return createHasNullExclusionQType16();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE17: return createHasNullExclusionQType17();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE18: return createHasNullExclusionQType18();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE19: return createHasNullExclusionQType19();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE20: return createHasNullExclusionQType20();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE21: return createHasNullExclusionQType21();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE22: return createHasNullExclusionQType22();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE23: return createHasNullExclusionQType23();
			case AdaPackage.HAS_NULL_EXCLUSION_QTYPE24: return createHasNullExclusionQType24();
			case AdaPackage.HAS_PRIVATE_QTYPE: return createHasPrivateQType();
			case AdaPackage.HAS_PRIVATE_QTYPE1: return createHasPrivateQType1();
			case AdaPackage.HAS_REVERSE_QTYPE: return createHasReverseQType();
			case AdaPackage.HAS_REVERSE_QTYPE1: return createHasReverseQType1();
			case AdaPackage.HAS_REVERSE_QTYPE2: return createHasReverseQType2();
			case AdaPackage.HAS_SYNCHRONIZED_QTYPE: return createHasSynchronizedQType();
			case AdaPackage.HAS_SYNCHRONIZED_QTYPE1: return createHasSynchronizedQType1();
			case AdaPackage.HAS_TAGGED_QTYPE: return createHasTaggedQType();
			case AdaPackage.IDENTIFIER: return createIdentifier();
			case AdaPackage.IDENTITY_ATTRIBUTE: return createIdentityAttribute();
			case AdaPackage.IF_EXPRESSION: return createIfExpression();
			case AdaPackage.IF_EXPRESSION_PATH: return createIfExpressionPath();
			case AdaPackage.IF_PATH: return createIfPath();
			case AdaPackage.IF_STATEMENT: return createIfStatement();
			case AdaPackage.IMAGE_ATTRIBUTE: return createImageAttribute();
			case AdaPackage.IMPLEMENTATION_DEFINED_ATTRIBUTE: return createImplementationDefinedAttribute();
			case AdaPackage.IMPLEMENTATION_DEFINED_PRAGMA: return createImplementationDefinedPragma();
			case AdaPackage.IMPORT_PRAGMA: return createImportPragma();
			case AdaPackage.INCOMPLETE_TYPE_DECLARATION: return createIncompleteTypeDeclaration();
			case AdaPackage.INDEPENDENT_COMPONENTS_PRAGMA: return createIndependentComponentsPragma();
			case AdaPackage.INDEPENDENT_PRAGMA: return createIndependentPragma();
			case AdaPackage.INDEX_CONSTRAINT: return createIndexConstraint();
			case AdaPackage.INDEXED_COMPONENT: return createIndexedComponent();
			case AdaPackage.INLINE_PRAGMA: return createInlinePragma();
			case AdaPackage.IN_MEMBERSHIP_TEST: return createInMembershipTest();
			case AdaPackage.INPUT_ATTRIBUTE: return createInputAttribute();
			case AdaPackage.INSPECTION_POINT_PRAGMA: return createInspectionPointPragma();
			case AdaPackage.INTEGER_LITERAL: return createIntegerLiteral();
			case AdaPackage.INTEGER_NUMBER_DECLARATION: return createIntegerNumberDeclaration();
			case AdaPackage.INTERRUPT_HANDLER_PRAGMA: return createInterruptHandlerPragma();
			case AdaPackage.INTERRUPT_PRIORITY_PRAGMA: return createInterruptPriorityPragma();
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE: return createIsNotNullReturnQType();
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE1: return createIsNotNullReturnQType1();
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE2: return createIsNotNullReturnQType2();
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE3: return createIsNotNullReturnQType3();
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE4: return createIsNotNullReturnQType4();
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE5: return createIsNotNullReturnQType5();
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE6: return createIsNotNullReturnQType6();
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE7: return createIsNotNullReturnQType7();
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE8: return createIsNotNullReturnQType8();
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE9: return createIsNotNullReturnQType9();
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE10: return createIsNotNullReturnQType10();
			case AdaPackage.IS_NOT_NULL_RETURN_QTYPE11: return createIsNotNullReturnQType11();
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE: return createIsNotOverridingDeclarationQType();
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE1: return createIsNotOverridingDeclarationQType1();
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE2: return createIsNotOverridingDeclarationQType2();
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE3: return createIsNotOverridingDeclarationQType3();
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE4: return createIsNotOverridingDeclarationQType4();
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE5: return createIsNotOverridingDeclarationQType5();
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE6: return createIsNotOverridingDeclarationQType6();
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE7: return createIsNotOverridingDeclarationQType7();
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE8: return createIsNotOverridingDeclarationQType8();
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE9: return createIsNotOverridingDeclarationQType9();
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE10: return createIsNotOverridingDeclarationQType10();
			case AdaPackage.IS_NOT_OVERRIDING_DECLARATION_QTYPE11: return createIsNotOverridingDeclarationQType11();
			case AdaPackage.IS_OVERRIDING_DECLARATION_QTYPE: return createIsOverridingDeclarationQType();
			case AdaPackage.IS_OVERRIDING_DECLARATION_QTYPE1: return createIsOverridingDeclarationQType1();
			case AdaPackage.IS_OVERRIDING_DECLARATION_QTYPE2: return createIsOverridingDeclarationQType2();
			case AdaPackage.IS_OVERRIDING_DECLARATION_QTYPE3: return createIsOverridingDeclarationQType3();
			case AdaPackage.IS_OVERRIDING_DECLARATION_QTYPE4: return createIsOverridingDeclarationQType4();
			case AdaPackage.IS_OVERRIDING_DECLARATION_QTYPE5: return createIsOverridingDeclarationQType5();
			case AdaPackage.IS_OVERRIDING_DECLARATION_QTYPE6: return createIsOverridingDeclarationQType6();
			case AdaPackage.IS_OVERRIDING_DECLARATION_QTYPE7: return createIsOverridingDeclarationQType7();
			case AdaPackage.IS_OVERRIDING_DECLARATION_QTYPE8: return createIsOverridingDeclarationQType8();
			case AdaPackage.IS_OVERRIDING_DECLARATION_QTYPE9: return createIsOverridingDeclarationQType9();
			case AdaPackage.IS_OVERRIDING_DECLARATION_QTYPE10: return createIsOverridingDeclarationQType10();
			case AdaPackage.IS_OVERRIDING_DECLARATION_QTYPE11: return createIsOverridingDeclarationQType11();
			case AdaPackage.IS_PREFIX_CALL: return createIsPrefixCall();
			case AdaPackage.IS_PREFIX_CALL_QTYPE: return createIsPrefixCallQType();
			case AdaPackage.IS_PREFIX_NOTATION: return createIsPrefixNotation();
			case AdaPackage.IS_PREFIX_NOTATION_QTYPE: return createIsPrefixNotationQType();
			case AdaPackage.IS_PREFIX_NOTATION_QTYPE1: return createIsPrefixNotationQType1();
			case AdaPackage.KNOWN_DISCRIMINANT_PART: return createKnownDiscriminantPart();
			case AdaPackage.LAST_ATTRIBUTE: return createLastAttribute();
			case AdaPackage.LAST_BIT_ATTRIBUTE: return createLastBitAttribute();
			case AdaPackage.LEADING_PART_ATTRIBUTE: return createLeadingPartAttribute();
			case AdaPackage.LENGTH_ATTRIBUTE: return createLengthAttribute();
			case AdaPackage.LESS_THAN_OPERATOR: return createLessThanOperator();
			case AdaPackage.LESS_THAN_OR_EQUAL_OPERATOR: return createLessThanOrEqualOperator();
			case AdaPackage.LIMITED: return createLimited();
			case AdaPackage.LIMITED_INTERFACE: return createLimitedInterface();
			case AdaPackage.LINKER_OPTIONS_PRAGMA: return createLinkerOptionsPragma();
			case AdaPackage.LIST_PRAGMA: return createListPragma();
			case AdaPackage.LOCKING_POLICY_PRAGMA: return createLockingPolicyPragma();
			case AdaPackage.LOOP_PARAMETER_SPECIFICATION: return createLoopParameterSpecification();
			case AdaPackage.LOOP_STATEMENT: return createLoopStatement();
			case AdaPackage.MACHINE_ATTRIBUTE: return createMachineAttribute();
			case AdaPackage.MACHINE_EMAX_ATTRIBUTE: return createMachineEmaxAttribute();
			case AdaPackage.MACHINE_EMIN_ATTRIBUTE: return createMachineEminAttribute();
			case AdaPackage.MACHINE_MANTISSA_ATTRIBUTE: return createMachineMantissaAttribute();
			case AdaPackage.MACHINE_OVERFLOWS_ATTRIBUTE: return createMachineOverflowsAttribute();
			case AdaPackage.MACHINE_RADIX_ATTRIBUTE: return createMachineRadixAttribute();
			case AdaPackage.MACHINE_ROUNDING_ATTRIBUTE: return createMachineRoundingAttribute();
			case AdaPackage.MACHINE_ROUNDS_ATTRIBUTE: return createMachineRoundsAttribute();
			case AdaPackage.MAX_ALIGNMENT_FOR_ALLOCATION_ATTRIBUTE: return createMaxAlignmentForAllocationAttribute();
			case AdaPackage.MAX_ATTRIBUTE: return createMaxAttribute();
			case AdaPackage.MAX_SIZE_IN_STORAGE_ELEMENTS_ATTRIBUTE: return createMaxSizeInStorageElementsAttribute();
			case AdaPackage.MIN_ATTRIBUTE: return createMinAttribute();
			case AdaPackage.MINUS_OPERATOR: return createMinusOperator();
			case AdaPackage.MOD_ATTRIBUTE: return createModAttribute();
			case AdaPackage.MODEL_ATTRIBUTE: return createModelAttribute();
			case AdaPackage.MODEL_EMIN_ATTRIBUTE: return createModelEminAttribute();
			case AdaPackage.MODEL_EPSILON_ATTRIBUTE: return createModelEpsilonAttribute();
			case AdaPackage.MODEL_MANTISSA_ATTRIBUTE: return createModelMantissaAttribute();
			case AdaPackage.MODEL_SMALL_ATTRIBUTE: return createModelSmallAttribute();
			case AdaPackage.MOD_OPERATOR: return createModOperator();
			case AdaPackage.MODULAR_TYPE_DEFINITION: return createModularTypeDefinition();
			case AdaPackage.MODULUS_ATTRIBUTE: return createModulusAttribute();
			case AdaPackage.MULTIPLY_OPERATOR: return createMultiplyOperator();
			case AdaPackage.NAME_CLASS: return createNameClass();
			case AdaPackage.NAMED_ARRAY_AGGREGATE: return createNamedArrayAggregate();
			case AdaPackage.NAME_LIST: return createNameList();
			case AdaPackage.NO_RETURN_PRAGMA: return createNoReturnPragma();
			case AdaPackage.NORMALIZE_SCALARS_PRAGMA: return createNormalizeScalarsPragma();
			case AdaPackage.NOT_AN_ELEMENT: return createNotAnElement();
			case AdaPackage.NOT_EQUAL_OPERATOR: return createNotEqualOperator();
			case AdaPackage.NOT_IN_MEMBERSHIP_TEST: return createNotInMembershipTest();
			case AdaPackage.NOT_NULL_RETURN: return createNotNullReturn();
			case AdaPackage.NOT_OPERATOR: return createNotOperator();
			case AdaPackage.NOT_OVERRIDING: return createNotOverriding();
			case AdaPackage.NULL_COMPONENT: return createNullComponent();
			case AdaPackage.NULL_EXCLUSION: return createNullExclusion();
			case AdaPackage.NULL_LITERAL: return createNullLiteral();
			case AdaPackage.NULL_PROCEDURE_DECLARATION: return createNullProcedureDeclaration();
			case AdaPackage.NULL_RECORD_DEFINITION: return createNullRecordDefinition();
			case AdaPackage.NULL_STATEMENT: return createNullStatement();
			case AdaPackage.OBJECT_RENAMING_DECLARATION: return createObjectRenamingDeclaration();
			case AdaPackage.OPTIMIZE_PRAGMA: return createOptimizePragma();
			case AdaPackage.ORDINARY_FIXED_POINT_DEFINITION: return createOrdinaryFixedPointDefinition();
			case AdaPackage.ORDINARY_INTERFACE: return createOrdinaryInterface();
			case AdaPackage.ORDINARY_TYPE_DECLARATION: return createOrdinaryTypeDeclaration();
			case AdaPackage.OR_ELSE_SHORT_CIRCUIT: return createOrElseShortCircuit();
			case AdaPackage.OR_OPERATOR: return createOrOperator();
			case AdaPackage.OR_PATH: return createOrPath();
			case AdaPackage.OTHERS_CHOICE: return createOthersChoice();
			case AdaPackage.OUTPUT_ATTRIBUTE: return createOutputAttribute();
			case AdaPackage.OVERLAPS_STORAGE_ATTRIBUTE: return createOverlapsStorageAttribute();
			case AdaPackage.OVERRIDING: return createOverriding();
			case AdaPackage.PACKAGE_BODY_DECLARATION: return createPackageBodyDeclaration();
			case AdaPackage.PACKAGE_BODY_STUB: return createPackageBodyStub();
			case AdaPackage.PACKAGE_DECLARATION: return createPackageDeclaration();
			case AdaPackage.PACKAGE_INSTANTIATION: return createPackageInstantiation();
			case AdaPackage.PACKAGE_RENAMING_DECLARATION: return createPackageRenamingDeclaration();
			case AdaPackage.PACK_PRAGMA: return createPackPragma();
			case AdaPackage.PAGE_PRAGMA: return createPagePragma();
			case AdaPackage.PARAMETER_ASSOCIATION: return createParameterAssociation();
			case AdaPackage.PARAMETER_SPECIFICATION: return createParameterSpecification();
			case AdaPackage.PARAMETER_SPECIFICATION_LIST: return createParameterSpecificationList();
			case AdaPackage.PARENTHESIZED_EXPRESSION: return createParenthesizedExpression();
			case AdaPackage.PARTITION_ELABORATION_POLICY_PRAGMA: return createPartitionElaborationPolicyPragma();
			case AdaPackage.PARTITION_ID_ATTRIBUTE: return createPartitionIdAttribute();
			case AdaPackage.PATH_CLASS: return createPathClass();
			case AdaPackage.PATH_LIST: return createPathList();
			case AdaPackage.PLUS_OPERATOR: return createPlusOperator();
			case AdaPackage.POOL_SPECIFIC_ACCESS_TO_VARIABLE: return createPoolSpecificAccessToVariable();
			case AdaPackage.POS_ATTRIBUTE: return createPosAttribute();
			case AdaPackage.POSITIONAL_ARRAY_AGGREGATE: return createPositionalArrayAggregate();
			case AdaPackage.POSITION_ATTRIBUTE: return createPositionAttribute();
			case AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION: return createPragmaArgumentAssociation();
			case AdaPackage.PRAGMA_ELEMENT_CLASS: return createPragmaElementClass();
			case AdaPackage.PRED_ATTRIBUTE: return createPredAttribute();
			case AdaPackage.PREELABORABLE_INITIALIZATION_PRAGMA: return createPreelaborableInitializationPragma();
			case AdaPackage.PREELABORATE_PRAGMA: return createPreelaboratePragma();
			case AdaPackage.PRIORITY_ATTRIBUTE: return createPriorityAttribute();
			case AdaPackage.PRIORITY_PRAGMA: return createPriorityPragma();
			case AdaPackage.PRIORITY_SPECIFIC_DISPATCHING_PRAGMA: return createPrioritySpecificDispatchingPragma();
			case AdaPackage.PRIVATE: return createPrivate();
			case AdaPackage.PRIVATE_EXTENSION_DECLARATION: return createPrivateExtensionDeclaration();
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION: return createPrivateExtensionDefinition();
			case AdaPackage.PRIVATE_TYPE_DECLARATION: return createPrivateTypeDeclaration();
			case AdaPackage.PRIVATE_TYPE_DEFINITION: return createPrivateTypeDefinition();
			case AdaPackage.PROCEDURE_BODY_DECLARATION: return createProcedureBodyDeclaration();
			case AdaPackage.PROCEDURE_BODY_STUB: return createProcedureBodyStub();
			case AdaPackage.PROCEDURE_CALL_STATEMENT: return createProcedureCallStatement();
			case AdaPackage.PROCEDURE_DECLARATION: return createProcedureDeclaration();
			case AdaPackage.PROCEDURE_INSTANTIATION: return createProcedureInstantiation();
			case AdaPackage.PROCEDURE_RENAMING_DECLARATION: return createProcedureRenamingDeclaration();
			case AdaPackage.PROFILE_PRAGMA: return createProfilePragma();
			case AdaPackage.PROTECTED_BODY_DECLARATION: return createProtectedBodyDeclaration();
			case AdaPackage.PROTECTED_BODY_STUB: return createProtectedBodyStub();
			case AdaPackage.PROTECTED_DEFINITION: return createProtectedDefinition();
			case AdaPackage.PROTECTED_INTERFACE: return createProtectedInterface();
			case AdaPackage.PROTECTED_TYPE_DECLARATION: return createProtectedTypeDeclaration();
			case AdaPackage.PURE_PRAGMA: return createPurePragma();
			case AdaPackage.QUALIFIED_EXPRESSION: return createQualifiedExpression();
			case AdaPackage.QUEUING_POLICY_PRAGMA: return createQueuingPolicyPragma();
			case AdaPackage.RAISE_EXPRESSION: return createRaiseExpression();
			case AdaPackage.RAISE_STATEMENT: return createRaiseStatement();
			case AdaPackage.RANGE_ATTRIBUTE: return createRangeAttribute();
			case AdaPackage.RANGE_ATTRIBUTE_REFERENCE: return createRangeAttributeReference();
			case AdaPackage.RANGE_CONSTRAINT_CLASS: return createRangeConstraintClass();
			case AdaPackage.READ_ATTRIBUTE: return createReadAttribute();
			case AdaPackage.REAL_LITERAL: return createRealLiteral();
			case AdaPackage.REAL_NUMBER_DECLARATION: return createRealNumberDeclaration();
			case AdaPackage.RECORD_AGGREGATE: return createRecordAggregate();
			case AdaPackage.RECORD_COMPONENT_ASSOCIATION: return createRecordComponentAssociation();
			case AdaPackage.RECORD_COMPONENT_CLASS: return createRecordComponentClass();
			case AdaPackage.RECORD_COMPONENT_LIST: return createRecordComponentList();
			case AdaPackage.RECORD_DEFINITION: return createRecordDefinition();
			case AdaPackage.RECORD_REPRESENTATION_CLAUSE: return createRecordRepresentationClause();
			case AdaPackage.RECORD_TYPE_DEFINITION: return createRecordTypeDefinition();
			case AdaPackage.RELATIVE_DEADLINE_PRAGMA: return createRelativeDeadlinePragma();
			case AdaPackage.REMAINDER_ATTRIBUTE: return createRemainderAttribute();
			case AdaPackage.REM_OPERATOR: return createRemOperator();
			case AdaPackage.REMOTE_CALL_INTERFACE_PRAGMA: return createRemoteCallInterfacePragma();
			case AdaPackage.REMOTE_TYPES_PRAGMA: return createRemoteTypesPragma();
			case AdaPackage.REQUEUE_STATEMENT: return createRequeueStatement();
			case AdaPackage.REQUEUE_STATEMENT_WITH_ABORT: return createRequeueStatementWithAbort();
			case AdaPackage.RESTRICTIONS_PRAGMA: return createRestrictionsPragma();
			case AdaPackage.RETURN_CONSTANT_SPECIFICATION: return createReturnConstantSpecification();
			case AdaPackage.RETURN_STATEMENT: return createReturnStatement();
			case AdaPackage.RETURN_VARIABLE_SPECIFICATION: return createReturnVariableSpecification();
			case AdaPackage.REVERSE: return createReverse();
			case AdaPackage.REVIEWABLE_PRAGMA: return createReviewablePragma();
			case AdaPackage.ROOT_INTEGER_DEFINITION: return createRootIntegerDefinition();
			case AdaPackage.ROOT_REAL_DEFINITION: return createRootRealDefinition();
			case AdaPackage.ROUND_ATTRIBUTE: return createRoundAttribute();
			case AdaPackage.ROUNDING_ATTRIBUTE: return createRoundingAttribute();
			case AdaPackage.SAFE_FIRST_ATTRIBUTE: return createSafeFirstAttribute();
			case AdaPackage.SAFE_LAST_ATTRIBUTE: return createSafeLastAttribute();
			case AdaPackage.SCALE_ATTRIBUTE: return createScaleAttribute();
			case AdaPackage.SCALING_ATTRIBUTE: return createScalingAttribute();
			case AdaPackage.SELECTED_COMPONENT: return createSelectedComponent();
			case AdaPackage.SELECTIVE_ACCEPT_STATEMENT: return createSelectiveAcceptStatement();
			case AdaPackage.SELECT_PATH: return createSelectPath();
			case AdaPackage.SHARED_PASSIVE_PRAGMA: return createSharedPassivePragma();
			case AdaPackage.SIGNED_INTEGER_TYPE_DEFINITION: return createSignedIntegerTypeDefinition();
			case AdaPackage.SIGNED_ZEROS_ATTRIBUTE: return createSignedZerosAttribute();
			case AdaPackage.SIMPLE_EXPRESSION_RANGE: return createSimpleExpressionRange();
			case AdaPackage.SINGLE_PROTECTED_DECLARATION: return createSingleProtectedDeclaration();
			case AdaPackage.SINGLE_TASK_DECLARATION: return createSingleTaskDeclaration();
			case AdaPackage.SIZE_ATTRIBUTE: return createSizeAttribute();
			case AdaPackage.SLICE: return createSlice();
			case AdaPackage.SMALL_ATTRIBUTE: return createSmallAttribute();
			case AdaPackage.SOURCE_LOCATION: return createSourceLocation();
			case AdaPackage.STATEMENT_CLASS: return createStatementClass();
			case AdaPackage.STATEMENT_LIST: return createStatementList();
			case AdaPackage.STORAGE_POOL_ATTRIBUTE: return createStoragePoolAttribute();
			case AdaPackage.STORAGE_SIZE_ATTRIBUTE: return createStorageSizeAttribute();
			case AdaPackage.STORAGE_SIZE_PRAGMA: return createStorageSizePragma();
			case AdaPackage.STREAM_SIZE_ATTRIBUTE: return createStreamSizeAttribute();
			case AdaPackage.STRING_LITERAL: return createStringLiteral();
			case AdaPackage.SUBTYPE_DECLARATION: return createSubtypeDeclaration();
			case AdaPackage.SUBTYPE_INDICATION: return createSubtypeIndication();
			case AdaPackage.SUCC_ATTRIBUTE: return createSuccAttribute();
			case AdaPackage.SUPPRESS_PRAGMA: return createSuppressPragma();
			case AdaPackage.SYNCHRONIZED: return createSynchronized();
			case AdaPackage.SYNCHRONIZED_INTERFACE: return createSynchronizedInterface();
			case AdaPackage.TAG_ATTRIBUTE: return createTagAttribute();
			case AdaPackage.TAGGED: return createTagged();
			case AdaPackage.TAGGED_INCOMPLETE_TYPE_DECLARATION: return createTaggedIncompleteTypeDeclaration();
			case AdaPackage.TAGGED_PRIVATE_TYPE_DEFINITION: return createTaggedPrivateTypeDefinition();
			case AdaPackage.TAGGED_RECORD_TYPE_DEFINITION: return createTaggedRecordTypeDefinition();
			case AdaPackage.TASK_BODY_DECLARATION: return createTaskBodyDeclaration();
			case AdaPackage.TASK_BODY_STUB: return createTaskBodyStub();
			case AdaPackage.TASK_DEFINITION: return createTaskDefinition();
			case AdaPackage.TASK_DISPATCHING_POLICY_PRAGMA: return createTaskDispatchingPolicyPragma();
			case AdaPackage.TASK_INTERFACE: return createTaskInterface();
			case AdaPackage.TASK_TYPE_DECLARATION: return createTaskTypeDeclaration();
			case AdaPackage.TERMINATE_ALTERNATIVE_STATEMENT: return createTerminateAlternativeStatement();
			case AdaPackage.TERMINATED_ATTRIBUTE: return createTerminatedAttribute();
			case AdaPackage.THEN_ABORT_PATH: return createThenAbortPath();
			case AdaPackage.TIMED_ENTRY_CALL_STATEMENT: return createTimedEntryCallStatement();
			case AdaPackage.TRUNCATION_ATTRIBUTE: return createTruncationAttribute();
			case AdaPackage.TYPE_CONVERSION: return createTypeConversion();
			case AdaPackage.UNARY_MINUS_OPERATOR: return createUnaryMinusOperator();
			case AdaPackage.UNARY_PLUS_OPERATOR: return createUnaryPlusOperator();
			case AdaPackage.UNBIASED_ROUNDING_ATTRIBUTE: return createUnbiasedRoundingAttribute();
			case AdaPackage.UNCHECKED_ACCESS_ATTRIBUTE: return createUncheckedAccessAttribute();
			case AdaPackage.UNCHECKED_UNION_PRAGMA: return createUncheckedUnionPragma();
			case AdaPackage.UNCONSTRAINED_ARRAY_DEFINITION: return createUnconstrainedArrayDefinition();
			case AdaPackage.UNIVERSAL_FIXED_DEFINITION: return createUniversalFixedDefinition();
			case AdaPackage.UNIVERSAL_INTEGER_DEFINITION: return createUniversalIntegerDefinition();
			case AdaPackage.UNIVERSAL_REAL_DEFINITION: return createUniversalRealDefinition();
			case AdaPackage.UNKNOWN_ATTRIBUTE: return createUnknownAttribute();
			case AdaPackage.UNKNOWN_DISCRIMINANT_PART: return createUnknownDiscriminantPart();
			case AdaPackage.UNKNOWN_PRAGMA: return createUnknownPragma();
			case AdaPackage.UNSUPPRESS_PRAGMA: return createUnsuppressPragma();
			case AdaPackage.USE_ALL_TYPE_CLAUSE: return createUseAllTypeClause();
			case AdaPackage.USE_PACKAGE_CLAUSE: return createUsePackageClause();
			case AdaPackage.USE_TYPE_CLAUSE: return createUseTypeClause();
			case AdaPackage.VAL_ATTRIBUTE: return createValAttribute();
			case AdaPackage.VALID_ATTRIBUTE: return createValidAttribute();
			case AdaPackage.VALUE_ATTRIBUTE: return createValueAttribute();
			case AdaPackage.VARIABLE_DECLARATION: return createVariableDeclaration();
			case AdaPackage.VARIANT: return createVariant();
			case AdaPackage.VARIANT_LIST: return createVariantList();
			case AdaPackage.VARIANT_PART: return createVariantPart();
			case AdaPackage.VERSION_ATTRIBUTE: return createVersionAttribute();
			case AdaPackage.VOLATILE_COMPONENTS_PRAGMA: return createVolatileComponentsPragma();
			case AdaPackage.VOLATILE_PRAGMA: return createVolatilePragma();
			case AdaPackage.WHILE_LOOP_STATEMENT: return createWhileLoopStatement();
			case AdaPackage.WIDE_IMAGE_ATTRIBUTE: return createWideImageAttribute();
			case AdaPackage.WIDE_VALUE_ATTRIBUTE: return createWideValueAttribute();
			case AdaPackage.WIDE_WIDE_IMAGE_ATTRIBUTE: return createWideWideImageAttribute();
			case AdaPackage.WIDE_WIDE_VALUE_ATTRIBUTE: return createWideWideValueAttribute();
			case AdaPackage.WIDE_WIDE_WIDTH_ATTRIBUTE: return createWideWideWidthAttribute();
			case AdaPackage.WIDE_WIDTH_ATTRIBUTE: return createWideWidthAttribute();
			case AdaPackage.WIDTH_ATTRIBUTE: return createWidthAttribute();
			case AdaPackage.WITH_CLAUSE: return createWithClause();
			case AdaPackage.WRITE_ATTRIBUTE: return createWriteAttribute();
			case AdaPackage.XOR_OPERATOR: return createXorOperator();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbortStatement createAbortStatement() {
		AbortStatementImpl abortStatement = new AbortStatementImpl();
		return abortStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbsOperator createAbsOperator() {
		AbsOperatorImpl absOperator = new AbsOperatorImpl();
		return absOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Abstract createAbstract() {
		AbstractImpl abstract_ = new AbstractImpl();
		return abstract_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcceptStatement createAcceptStatement() {
		AcceptStatementImpl acceptStatement = new AcceptStatementImpl();
		return acceptStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessAttribute createAccessAttribute() {
		AccessAttributeImpl accessAttribute = new AccessAttributeImpl();
		return accessAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessToConstant createAccessToConstant() {
		AccessToConstantImpl accessToConstant = new AccessToConstantImpl();
		return accessToConstant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessToFunction createAccessToFunction() {
		AccessToFunctionImpl accessToFunction = new AccessToFunctionImpl();
		return accessToFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessToProcedure createAccessToProcedure() {
		AccessToProcedureImpl accessToProcedure = new AccessToProcedureImpl();
		return accessToProcedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessToProtectedFunction createAccessToProtectedFunction() {
		AccessToProtectedFunctionImpl accessToProtectedFunction = new AccessToProtectedFunctionImpl();
		return accessToProtectedFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessToProtectedProcedure createAccessToProtectedProcedure() {
		AccessToProtectedProcedureImpl accessToProtectedProcedure = new AccessToProtectedProcedureImpl();
		return accessToProtectedProcedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessToVariable createAccessToVariable() {
		AccessToVariableImpl accessToVariable = new AccessToVariableImpl();
		return accessToVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AddressAttribute createAddressAttribute() {
		AddressAttributeImpl addressAttribute = new AddressAttributeImpl();
		return addressAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdjacentAttribute createAdjacentAttribute() {
		AdjacentAttributeImpl adjacentAttribute = new AdjacentAttributeImpl();
		return adjacentAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AftAttribute createAftAttribute() {
		AftAttributeImpl aftAttribute = new AftAttributeImpl();
		return aftAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Aliased createAliased() {
		AliasedImpl aliased = new AliasedImpl();
		return aliased;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlignmentAttribute createAlignmentAttribute() {
		AlignmentAttributeImpl alignmentAttribute = new AlignmentAttributeImpl();
		return alignmentAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AllCallsRemotePragma createAllCallsRemotePragma() {
		AllCallsRemotePragmaImpl allCallsRemotePragma = new AllCallsRemotePragmaImpl();
		return allCallsRemotePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AllocationFromQualifiedExpression createAllocationFromQualifiedExpression() {
		AllocationFromQualifiedExpressionImpl allocationFromQualifiedExpression = new AllocationFromQualifiedExpressionImpl();
		return allocationFromQualifiedExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AllocationFromSubtype createAllocationFromSubtype() {
		AllocationFromSubtypeImpl allocationFromSubtype = new AllocationFromSubtypeImpl();
		return allocationFromSubtype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AndOperator createAndOperator() {
		AndOperatorImpl andOperator = new AndOperatorImpl();
		return andOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AndThenShortCircuit createAndThenShortCircuit() {
		AndThenShortCircuitImpl andThenShortCircuit = new AndThenShortCircuitImpl();
		return andThenShortCircuit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnonymousAccessToConstant createAnonymousAccessToConstant() {
		AnonymousAccessToConstantImpl anonymousAccessToConstant = new AnonymousAccessToConstantImpl();
		return anonymousAccessToConstant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnonymousAccessToFunction createAnonymousAccessToFunction() {
		AnonymousAccessToFunctionImpl anonymousAccessToFunction = new AnonymousAccessToFunctionImpl();
		return anonymousAccessToFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnonymousAccessToProcedure createAnonymousAccessToProcedure() {
		AnonymousAccessToProcedureImpl anonymousAccessToProcedure = new AnonymousAccessToProcedureImpl();
		return anonymousAccessToProcedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnonymousAccessToProtectedFunction createAnonymousAccessToProtectedFunction() {
		AnonymousAccessToProtectedFunctionImpl anonymousAccessToProtectedFunction = new AnonymousAccessToProtectedFunctionImpl();
		return anonymousAccessToProtectedFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnonymousAccessToProtectedProcedure createAnonymousAccessToProtectedProcedure() {
		AnonymousAccessToProtectedProcedureImpl anonymousAccessToProtectedProcedure = new AnonymousAccessToProtectedProcedureImpl();
		return anonymousAccessToProtectedProcedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnonymousAccessToVariable createAnonymousAccessToVariable() {
		AnonymousAccessToVariableImpl anonymousAccessToVariable = new AnonymousAccessToVariableImpl();
		return anonymousAccessToVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArrayComponentAssociation createArrayComponentAssociation() {
		ArrayComponentAssociationImpl arrayComponentAssociation = new ArrayComponentAssociationImpl();
		return arrayComponentAssociation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AspectSpecification createAspectSpecification() {
		AspectSpecificationImpl aspectSpecification = new AspectSpecificationImpl();
		return aspectSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertionPolicyPragma createAssertionPolicyPragma() {
		AssertionPolicyPragmaImpl assertionPolicyPragma = new AssertionPolicyPragmaImpl();
		return assertionPolicyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertPragma createAssertPragma() {
		AssertPragmaImpl assertPragma = new AssertPragmaImpl();
		return assertPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssignmentStatement createAssignmentStatement() {
		AssignmentStatementImpl assignmentStatement = new AssignmentStatementImpl();
		return assignmentStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationClass createAssociationClass() {
		AssociationClassImpl associationClass = new AssociationClassImpl();
		return associationClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationList createAssociationList() {
		AssociationListImpl associationList = new AssociationListImpl();
		return associationList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AsynchronousPragma createAsynchronousPragma() {
		AsynchronousPragmaImpl asynchronousPragma = new AsynchronousPragmaImpl();
		return asynchronousPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AsynchronousSelectStatement createAsynchronousSelectStatement() {
		AsynchronousSelectStatementImpl asynchronousSelectStatement = new AsynchronousSelectStatementImpl();
		return asynchronousSelectStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtClause createAtClause() {
		AtClauseImpl atClause = new AtClauseImpl();
		return atClause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtomicComponentsPragma createAtomicComponentsPragma() {
		AtomicComponentsPragmaImpl atomicComponentsPragma = new AtomicComponentsPragmaImpl();
		return atomicComponentsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtomicPragma createAtomicPragma() {
		AtomicPragmaImpl atomicPragma = new AtomicPragmaImpl();
		return atomicPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttachHandlerPragma createAttachHandlerPragma() {
		AttachHandlerPragmaImpl attachHandlerPragma = new AttachHandlerPragmaImpl();
		return attachHandlerPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeDefinitionClause createAttributeDefinitionClause() {
		AttributeDefinitionClauseImpl attributeDefinitionClause = new AttributeDefinitionClauseImpl();
		return attributeDefinitionClause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseAttribute createBaseAttribute() {
		BaseAttributeImpl baseAttribute = new BaseAttributeImpl();
		return baseAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BitOrderAttribute createBitOrderAttribute() {
		BitOrderAttributeImpl bitOrderAttribute = new BitOrderAttributeImpl();
		return bitOrderAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BlockStatement createBlockStatement() {
		BlockStatementImpl blockStatement = new BlockStatementImpl();
		return blockStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BodyVersionAttribute createBodyVersionAttribute() {
		BodyVersionAttributeImpl bodyVersionAttribute = new BodyVersionAttributeImpl();
		return bodyVersionAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BoxExpression createBoxExpression() {
		BoxExpressionImpl boxExpression = new BoxExpressionImpl();
		return boxExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallableAttribute createCallableAttribute() {
		CallableAttributeImpl callableAttribute = new CallableAttributeImpl();
		return callableAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallerAttribute createCallerAttribute() {
		CallerAttributeImpl callerAttribute = new CallerAttributeImpl();
		return callerAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CaseExpression createCaseExpression() {
		CaseExpressionImpl caseExpression = new CaseExpressionImpl();
		return caseExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CaseExpressionPath createCaseExpressionPath() {
		CaseExpressionPathImpl caseExpressionPath = new CaseExpressionPathImpl();
		return caseExpressionPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CasePath createCasePath() {
		CasePathImpl casePath = new CasePathImpl();
		return casePath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CaseStatement createCaseStatement() {
		CaseStatementImpl caseStatement = new CaseStatementImpl();
		return caseStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CeilingAttribute createCeilingAttribute() {
		CeilingAttributeImpl ceilingAttribute = new CeilingAttributeImpl();
		return ceilingAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CharacterLiteral createCharacterLiteral() {
		CharacterLiteralImpl characterLiteral = new CharacterLiteralImpl();
		return characterLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChoiceParameterSpecification createChoiceParameterSpecification() {
		ChoiceParameterSpecificationImpl choiceParameterSpecification = new ChoiceParameterSpecificationImpl();
		return choiceParameterSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassAttribute createClassAttribute() {
		ClassAttributeImpl classAttribute = new ClassAttributeImpl();
		return classAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeStatement createCodeStatement() {
		CodeStatementImpl codeStatement = new CodeStatementImpl();
		return codeStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Comment createComment() {
		CommentImpl comment = new CommentImpl();
		return comment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompilationUnit createCompilationUnit() {
		CompilationUnitImpl compilationUnit = new CompilationUnitImpl();
		return compilationUnit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentClause createComponentClause() {
		ComponentClauseImpl componentClause = new ComponentClauseImpl();
		return componentClause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentClauseList createComponentClauseList() {
		ComponentClauseListImpl componentClauseList = new ComponentClauseListImpl();
		return componentClauseList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentDeclaration createComponentDeclaration() {
		ComponentDeclarationImpl componentDeclaration = new ComponentDeclarationImpl();
		return componentDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentDefinition createComponentDefinition() {
		ComponentDefinitionImpl componentDefinition = new ComponentDefinitionImpl();
		return componentDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentSizeAttribute createComponentSizeAttribute() {
		ComponentSizeAttributeImpl componentSizeAttribute = new ComponentSizeAttributeImpl();
		return componentSizeAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComposeAttribute createComposeAttribute() {
		ComposeAttributeImpl composeAttribute = new ComposeAttributeImpl();
		return composeAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConcatenateOperator createConcatenateOperator() {
		ConcatenateOperatorImpl concatenateOperator = new ConcatenateOperatorImpl();
		return concatenateOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionalEntryCallStatement createConditionalEntryCallStatement() {
		ConditionalEntryCallStatementImpl conditionalEntryCallStatement = new ConditionalEntryCallStatementImpl();
		return conditionalEntryCallStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstantDeclaration createConstantDeclaration() {
		ConstantDeclarationImpl constantDeclaration = new ConstantDeclarationImpl();
		return constantDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstrainedArrayDefinition createConstrainedArrayDefinition() {
		ConstrainedArrayDefinitionImpl constrainedArrayDefinition = new ConstrainedArrayDefinitionImpl();
		return constrainedArrayDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstrainedAttribute createConstrainedAttribute() {
		ConstrainedAttributeImpl constrainedAttribute = new ConstrainedAttributeImpl();
		return constrainedAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstraintClass createConstraintClass() {
		ConstraintClassImpl constraintClass = new ConstraintClassImpl();
		return constraintClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ContextClauseClass createContextClauseClass() {
		ContextClauseClassImpl contextClauseClass = new ContextClauseClassImpl();
		return contextClauseClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ContextClauseList createContextClauseList() {
		ContextClauseListImpl contextClauseList = new ContextClauseListImpl();
		return contextClauseList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlledPragma createControlledPragma() {
		ControlledPragmaImpl controlledPragma = new ControlledPragmaImpl();
		return controlledPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConventionPragma createConventionPragma() {
		ConventionPragmaImpl conventionPragma = new ConventionPragmaImpl();
		return conventionPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CopySignAttribute createCopySignAttribute() {
		CopySignAttributeImpl copySignAttribute = new CopySignAttributeImpl();
		return copySignAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CountAttribute createCountAttribute() {
		CountAttributeImpl countAttribute = new CountAttributeImpl();
		return countAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CpuPragma createCpuPragma() {
		CpuPragmaImpl cpuPragma = new CpuPragmaImpl();
		return cpuPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DecimalFixedPointDefinition createDecimalFixedPointDefinition() {
		DecimalFixedPointDefinitionImpl decimalFixedPointDefinition = new DecimalFixedPointDefinitionImpl();
		return decimalFixedPointDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeclarationClass createDeclarationClass() {
		DeclarationClassImpl declarationClass = new DeclarationClassImpl();
		return declarationClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeclarationList createDeclarationList() {
		DeclarationListImpl declarationList = new DeclarationListImpl();
		return declarationList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeclarativeItemClass createDeclarativeItemClass() {
		DeclarativeItemClassImpl declarativeItemClass = new DeclarativeItemClassImpl();
		return declarativeItemClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeclarativeItemList createDeclarativeItemList() {
		DeclarativeItemListImpl declarativeItemList = new DeclarativeItemListImpl();
		return declarativeItemList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefaultStoragePoolPragma createDefaultStoragePoolPragma() {
		DefaultStoragePoolPragmaImpl defaultStoragePoolPragma = new DefaultStoragePoolPragmaImpl();
		return defaultStoragePoolPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeferredConstantDeclaration createDeferredConstantDeclaration() {
		DeferredConstantDeclarationImpl deferredConstantDeclaration = new DeferredConstantDeclarationImpl();
		return deferredConstantDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningAbsOperator createDefiningAbsOperator() {
		DefiningAbsOperatorImpl definingAbsOperator = new DefiningAbsOperatorImpl();
		return definingAbsOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningAndOperator createDefiningAndOperator() {
		DefiningAndOperatorImpl definingAndOperator = new DefiningAndOperatorImpl();
		return definingAndOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningCharacterLiteral createDefiningCharacterLiteral() {
		DefiningCharacterLiteralImpl definingCharacterLiteral = new DefiningCharacterLiteralImpl();
		return definingCharacterLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningConcatenateOperator createDefiningConcatenateOperator() {
		DefiningConcatenateOperatorImpl definingConcatenateOperator = new DefiningConcatenateOperatorImpl();
		return definingConcatenateOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningDivideOperator createDefiningDivideOperator() {
		DefiningDivideOperatorImpl definingDivideOperator = new DefiningDivideOperatorImpl();
		return definingDivideOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningEnumerationLiteral createDefiningEnumerationLiteral() {
		DefiningEnumerationLiteralImpl definingEnumerationLiteral = new DefiningEnumerationLiteralImpl();
		return definingEnumerationLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningEqualOperator createDefiningEqualOperator() {
		DefiningEqualOperatorImpl definingEqualOperator = new DefiningEqualOperatorImpl();
		return definingEqualOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningExpandedName createDefiningExpandedName() {
		DefiningExpandedNameImpl definingExpandedName = new DefiningExpandedNameImpl();
		return definingExpandedName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningExponentiateOperator createDefiningExponentiateOperator() {
		DefiningExponentiateOperatorImpl definingExponentiateOperator = new DefiningExponentiateOperatorImpl();
		return definingExponentiateOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningGreaterThanOperator createDefiningGreaterThanOperator() {
		DefiningGreaterThanOperatorImpl definingGreaterThanOperator = new DefiningGreaterThanOperatorImpl();
		return definingGreaterThanOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningGreaterThanOrEqualOperator createDefiningGreaterThanOrEqualOperator() {
		DefiningGreaterThanOrEqualOperatorImpl definingGreaterThanOrEqualOperator = new DefiningGreaterThanOrEqualOperatorImpl();
		return definingGreaterThanOrEqualOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningIdentifier createDefiningIdentifier() {
		DefiningIdentifierImpl definingIdentifier = new DefiningIdentifierImpl();
		return definingIdentifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningLessThanOperator createDefiningLessThanOperator() {
		DefiningLessThanOperatorImpl definingLessThanOperator = new DefiningLessThanOperatorImpl();
		return definingLessThanOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningLessThanOrEqualOperator createDefiningLessThanOrEqualOperator() {
		DefiningLessThanOrEqualOperatorImpl definingLessThanOrEqualOperator = new DefiningLessThanOrEqualOperatorImpl();
		return definingLessThanOrEqualOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningMinusOperator createDefiningMinusOperator() {
		DefiningMinusOperatorImpl definingMinusOperator = new DefiningMinusOperatorImpl();
		return definingMinusOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningModOperator createDefiningModOperator() {
		DefiningModOperatorImpl definingModOperator = new DefiningModOperatorImpl();
		return definingModOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningMultiplyOperator createDefiningMultiplyOperator() {
		DefiningMultiplyOperatorImpl definingMultiplyOperator = new DefiningMultiplyOperatorImpl();
		return definingMultiplyOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNameClass createDefiningNameClass() {
		DefiningNameClassImpl definingNameClass = new DefiningNameClassImpl();
		return definingNameClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNameList createDefiningNameList() {
		DefiningNameListImpl definingNameList = new DefiningNameListImpl();
		return definingNameList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNotEqualOperator createDefiningNotEqualOperator() {
		DefiningNotEqualOperatorImpl definingNotEqualOperator = new DefiningNotEqualOperatorImpl();
		return definingNotEqualOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNotOperator createDefiningNotOperator() {
		DefiningNotOperatorImpl definingNotOperator = new DefiningNotOperatorImpl();
		return definingNotOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningOrOperator createDefiningOrOperator() {
		DefiningOrOperatorImpl definingOrOperator = new DefiningOrOperatorImpl();
		return definingOrOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningPlusOperator createDefiningPlusOperator() {
		DefiningPlusOperatorImpl definingPlusOperator = new DefiningPlusOperatorImpl();
		return definingPlusOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningRemOperator createDefiningRemOperator() {
		DefiningRemOperatorImpl definingRemOperator = new DefiningRemOperatorImpl();
		return definingRemOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningUnaryMinusOperator createDefiningUnaryMinusOperator() {
		DefiningUnaryMinusOperatorImpl definingUnaryMinusOperator = new DefiningUnaryMinusOperatorImpl();
		return definingUnaryMinusOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningUnaryPlusOperator createDefiningUnaryPlusOperator() {
		DefiningUnaryPlusOperatorImpl definingUnaryPlusOperator = new DefiningUnaryPlusOperatorImpl();
		return definingUnaryPlusOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningXorOperator createDefiningXorOperator() {
		DefiningXorOperatorImpl definingXorOperator = new DefiningXorOperatorImpl();
		return definingXorOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiniteAttribute createDefiniteAttribute() {
		DefiniteAttributeImpl definiteAttribute = new DefiniteAttributeImpl();
		return definiteAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefinitionClass createDefinitionClass() {
		DefinitionClassImpl definitionClass = new DefinitionClassImpl();
		return definitionClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefinitionList createDefinitionList() {
		DefinitionListImpl definitionList = new DefinitionListImpl();
		return definitionList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DelayRelativeStatement createDelayRelativeStatement() {
		DelayRelativeStatementImpl delayRelativeStatement = new DelayRelativeStatementImpl();
		return delayRelativeStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DelayUntilStatement createDelayUntilStatement() {
		DelayUntilStatementImpl delayUntilStatement = new DelayUntilStatementImpl();
		return delayUntilStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeltaAttribute createDeltaAttribute() {
		DeltaAttributeImpl deltaAttribute = new DeltaAttributeImpl();
		return deltaAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeltaConstraint createDeltaConstraint() {
		DeltaConstraintImpl deltaConstraint = new DeltaConstraintImpl();
		return deltaConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DenormAttribute createDenormAttribute() {
		DenormAttributeImpl denormAttribute = new DenormAttributeImpl();
		return denormAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DerivedRecordExtensionDefinition createDerivedRecordExtensionDefinition() {
		DerivedRecordExtensionDefinitionImpl derivedRecordExtensionDefinition = new DerivedRecordExtensionDefinitionImpl();
		return derivedRecordExtensionDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DerivedTypeDefinition createDerivedTypeDefinition() {
		DerivedTypeDefinitionImpl derivedTypeDefinition = new DerivedTypeDefinitionImpl();
		return derivedTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DetectBlockingPragma createDetectBlockingPragma() {
		DetectBlockingPragmaImpl detectBlockingPragma = new DetectBlockingPragmaImpl();
		return detectBlockingPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DigitsAttribute createDigitsAttribute() {
		DigitsAttributeImpl digitsAttribute = new DigitsAttributeImpl();
		return digitsAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DigitsConstraint createDigitsConstraint() {
		DigitsConstraintImpl digitsConstraint = new DigitsConstraintImpl();
		return digitsConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscardNamesPragma createDiscardNamesPragma() {
		DiscardNamesPragmaImpl discardNamesPragma = new DiscardNamesPragmaImpl();
		return discardNamesPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscreteRangeAttributeReference createDiscreteRangeAttributeReference() {
		DiscreteRangeAttributeReferenceImpl discreteRangeAttributeReference = new DiscreteRangeAttributeReferenceImpl();
		return discreteRangeAttributeReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscreteRangeAttributeReferenceAsSubtypeDefinition createDiscreteRangeAttributeReferenceAsSubtypeDefinition() {
		DiscreteRangeAttributeReferenceAsSubtypeDefinitionImpl discreteRangeAttributeReferenceAsSubtypeDefinition = new DiscreteRangeAttributeReferenceAsSubtypeDefinitionImpl();
		return discreteRangeAttributeReferenceAsSubtypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscreteRangeClass createDiscreteRangeClass() {
		DiscreteRangeClassImpl discreteRangeClass = new DiscreteRangeClassImpl();
		return discreteRangeClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscreteRangeList createDiscreteRangeList() {
		DiscreteRangeListImpl discreteRangeList = new DiscreteRangeListImpl();
		return discreteRangeList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscreteSimpleExpressionRange createDiscreteSimpleExpressionRange() {
		DiscreteSimpleExpressionRangeImpl discreteSimpleExpressionRange = new DiscreteSimpleExpressionRangeImpl();
		return discreteSimpleExpressionRange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscreteSimpleExpressionRangeAsSubtypeDefinition createDiscreteSimpleExpressionRangeAsSubtypeDefinition() {
		DiscreteSimpleExpressionRangeAsSubtypeDefinitionImpl discreteSimpleExpressionRangeAsSubtypeDefinition = new DiscreteSimpleExpressionRangeAsSubtypeDefinitionImpl();
		return discreteSimpleExpressionRangeAsSubtypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscreteSubtypeDefinitionClass createDiscreteSubtypeDefinitionClass() {
		DiscreteSubtypeDefinitionClassImpl discreteSubtypeDefinitionClass = new DiscreteSubtypeDefinitionClassImpl();
		return discreteSubtypeDefinitionClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscreteSubtypeIndication createDiscreteSubtypeIndication() {
		DiscreteSubtypeIndicationImpl discreteSubtypeIndication = new DiscreteSubtypeIndicationImpl();
		return discreteSubtypeIndication;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscreteSubtypeIndicationAsSubtypeDefinition createDiscreteSubtypeIndicationAsSubtypeDefinition() {
		DiscreteSubtypeIndicationAsSubtypeDefinitionImpl discreteSubtypeIndicationAsSubtypeDefinition = new DiscreteSubtypeIndicationAsSubtypeDefinitionImpl();
		return discreteSubtypeIndicationAsSubtypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscriminantAssociation createDiscriminantAssociation() {
		DiscriminantAssociationImpl discriminantAssociation = new DiscriminantAssociationImpl();
		return discriminantAssociation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscriminantAssociationList createDiscriminantAssociationList() {
		DiscriminantAssociationListImpl discriminantAssociationList = new DiscriminantAssociationListImpl();
		return discriminantAssociationList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscriminantConstraint createDiscriminantConstraint() {
		DiscriminantConstraintImpl discriminantConstraint = new DiscriminantConstraintImpl();
		return discriminantConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscriminantSpecification createDiscriminantSpecification() {
		DiscriminantSpecificationImpl discriminantSpecification = new DiscriminantSpecificationImpl();
		return discriminantSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscriminantSpecificationList createDiscriminantSpecificationList() {
		DiscriminantSpecificationListImpl discriminantSpecificationList = new DiscriminantSpecificationListImpl();
		return discriminantSpecificationList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DispatchingDomainPragma createDispatchingDomainPragma() {
		DispatchingDomainPragmaImpl dispatchingDomainPragma = new DispatchingDomainPragmaImpl();
		return dispatchingDomainPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DivideOperator createDivideOperator() {
		DivideOperatorImpl divideOperator = new DivideOperatorImpl();
		return divideOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DocumentRoot createDocumentRoot() {
		DocumentRootImpl documentRoot = new DocumentRootImpl();
		return documentRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElaborateAllPragma createElaborateAllPragma() {
		ElaborateAllPragmaImpl elaborateAllPragma = new ElaborateAllPragmaImpl();
		return elaborateAllPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElaborateBodyPragma createElaborateBodyPragma() {
		ElaborateBodyPragmaImpl elaborateBodyPragma = new ElaborateBodyPragmaImpl();
		return elaborateBodyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElaboratePragma createElaboratePragma() {
		ElaboratePragmaImpl elaboratePragma = new ElaboratePragmaImpl();
		return elaboratePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementClass createElementClass() {
		ElementClassImpl elementClass = new ElementClassImpl();
		return elementClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementIteratorSpecification createElementIteratorSpecification() {
		ElementIteratorSpecificationImpl elementIteratorSpecification = new ElementIteratorSpecificationImpl();
		return elementIteratorSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementList createElementList() {
		ElementListImpl elementList = new ElementListImpl();
		return elementList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElseExpressionPath createElseExpressionPath() {
		ElseExpressionPathImpl elseExpressionPath = new ElseExpressionPathImpl();
		return elseExpressionPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElsePath createElsePath() {
		ElsePathImpl elsePath = new ElsePathImpl();
		return elsePath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElsifExpressionPath createElsifExpressionPath() {
		ElsifExpressionPathImpl elsifExpressionPath = new ElsifExpressionPathImpl();
		return elsifExpressionPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElsifPath createElsifPath() {
		ElsifPathImpl elsifPath = new ElsifPathImpl();
		return elsifPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntryBodyDeclaration createEntryBodyDeclaration() {
		EntryBodyDeclarationImpl entryBodyDeclaration = new EntryBodyDeclarationImpl();
		return entryBodyDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntryCallStatement createEntryCallStatement() {
		EntryCallStatementImpl entryCallStatement = new EntryCallStatementImpl();
		return entryCallStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntryDeclaration createEntryDeclaration() {
		EntryDeclarationImpl entryDeclaration = new EntryDeclarationImpl();
		return entryDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntryIndexSpecification createEntryIndexSpecification() {
		EntryIndexSpecificationImpl entryIndexSpecification = new EntryIndexSpecificationImpl();
		return entryIndexSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationLiteral createEnumerationLiteral() {
		EnumerationLiteralImpl enumerationLiteral = new EnumerationLiteralImpl();
		return enumerationLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationLiteralSpecification createEnumerationLiteralSpecification() {
		EnumerationLiteralSpecificationImpl enumerationLiteralSpecification = new EnumerationLiteralSpecificationImpl();
		return enumerationLiteralSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationRepresentationClause createEnumerationRepresentationClause() {
		EnumerationRepresentationClauseImpl enumerationRepresentationClause = new EnumerationRepresentationClauseImpl();
		return enumerationRepresentationClause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationTypeDefinition createEnumerationTypeDefinition() {
		EnumerationTypeDefinitionImpl enumerationTypeDefinition = new EnumerationTypeDefinitionImpl();
		return enumerationTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EqualOperator createEqualOperator() {
		EqualOperatorImpl equalOperator = new EqualOperatorImpl();
		return equalOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExceptionDeclaration createExceptionDeclaration() {
		ExceptionDeclarationImpl exceptionDeclaration = new ExceptionDeclarationImpl();
		return exceptionDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExceptionHandler createExceptionHandler() {
		ExceptionHandlerImpl exceptionHandler = new ExceptionHandlerImpl();
		return exceptionHandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExceptionHandlerList createExceptionHandlerList() {
		ExceptionHandlerListImpl exceptionHandlerList = new ExceptionHandlerListImpl();
		return exceptionHandlerList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExceptionRenamingDeclaration createExceptionRenamingDeclaration() {
		ExceptionRenamingDeclarationImpl exceptionRenamingDeclaration = new ExceptionRenamingDeclarationImpl();
		return exceptionRenamingDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExitStatement createExitStatement() {
		ExitStatementImpl exitStatement = new ExitStatementImpl();
		return exitStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExplicitDereference createExplicitDereference() {
		ExplicitDereferenceImpl explicitDereference = new ExplicitDereferenceImpl();
		return explicitDereference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExponentAttribute createExponentAttribute() {
		ExponentAttributeImpl exponentAttribute = new ExponentAttributeImpl();
		return exponentAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExponentiateOperator createExponentiateOperator() {
		ExponentiateOperatorImpl exponentiateOperator = new ExponentiateOperatorImpl();
		return exponentiateOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExportPragma createExportPragma() {
		ExportPragmaImpl exportPragma = new ExportPragmaImpl();
		return exportPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass createExpressionClass() {
		ExpressionClassImpl expressionClass = new ExpressionClassImpl();
		return expressionClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionFunctionDeclaration createExpressionFunctionDeclaration() {
		ExpressionFunctionDeclarationImpl expressionFunctionDeclaration = new ExpressionFunctionDeclarationImpl();
		return expressionFunctionDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionList createExpressionList() {
		ExpressionListImpl expressionList = new ExpressionListImpl();
		return expressionList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExtendedReturnStatement createExtendedReturnStatement() {
		ExtendedReturnStatementImpl extendedReturnStatement = new ExtendedReturnStatementImpl();
		return extendedReturnStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExtensionAggregate createExtensionAggregate() {
		ExtensionAggregateImpl extensionAggregate = new ExtensionAggregateImpl();
		return extensionAggregate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExternalTagAttribute createExternalTagAttribute() {
		ExternalTagAttributeImpl externalTagAttribute = new ExternalTagAttributeImpl();
		return externalTagAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FirstAttribute createFirstAttribute() {
		FirstAttributeImpl firstAttribute = new FirstAttributeImpl();
		return firstAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FirstBitAttribute createFirstBitAttribute() {
		FirstBitAttributeImpl firstBitAttribute = new FirstBitAttributeImpl();
		return firstBitAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FloatingPointDefinition createFloatingPointDefinition() {
		FloatingPointDefinitionImpl floatingPointDefinition = new FloatingPointDefinitionImpl();
		return floatingPointDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FloorAttribute createFloorAttribute() {
		FloorAttributeImpl floorAttribute = new FloorAttributeImpl();
		return floorAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ForAllQuantifiedExpression createForAllQuantifiedExpression() {
		ForAllQuantifiedExpressionImpl forAllQuantifiedExpression = new ForAllQuantifiedExpressionImpl();
		return forAllQuantifiedExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ForeAttribute createForeAttribute() {
		ForeAttributeImpl foreAttribute = new ForeAttributeImpl();
		return foreAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ForLoopStatement createForLoopStatement() {
		ForLoopStatementImpl forLoopStatement = new ForLoopStatementImpl();
		return forLoopStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalAccessToConstant createFormalAccessToConstant() {
		FormalAccessToConstantImpl formalAccessToConstant = new FormalAccessToConstantImpl();
		return formalAccessToConstant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalAccessToFunction createFormalAccessToFunction() {
		FormalAccessToFunctionImpl formalAccessToFunction = new FormalAccessToFunctionImpl();
		return formalAccessToFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalAccessToProcedure createFormalAccessToProcedure() {
		FormalAccessToProcedureImpl formalAccessToProcedure = new FormalAccessToProcedureImpl();
		return formalAccessToProcedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalAccessToProtectedFunction createFormalAccessToProtectedFunction() {
		FormalAccessToProtectedFunctionImpl formalAccessToProtectedFunction = new FormalAccessToProtectedFunctionImpl();
		return formalAccessToProtectedFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalAccessToProtectedProcedure createFormalAccessToProtectedProcedure() {
		FormalAccessToProtectedProcedureImpl formalAccessToProtectedProcedure = new FormalAccessToProtectedProcedureImpl();
		return formalAccessToProtectedProcedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalAccessToVariable createFormalAccessToVariable() {
		FormalAccessToVariableImpl formalAccessToVariable = new FormalAccessToVariableImpl();
		return formalAccessToVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalConstrainedArrayDefinition createFormalConstrainedArrayDefinition() {
		FormalConstrainedArrayDefinitionImpl formalConstrainedArrayDefinition = new FormalConstrainedArrayDefinitionImpl();
		return formalConstrainedArrayDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalDecimalFixedPointDefinition createFormalDecimalFixedPointDefinition() {
		FormalDecimalFixedPointDefinitionImpl formalDecimalFixedPointDefinition = new FormalDecimalFixedPointDefinitionImpl();
		return formalDecimalFixedPointDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalDerivedTypeDefinition createFormalDerivedTypeDefinition() {
		FormalDerivedTypeDefinitionImpl formalDerivedTypeDefinition = new FormalDerivedTypeDefinitionImpl();
		return formalDerivedTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalDiscreteTypeDefinition createFormalDiscreteTypeDefinition() {
		FormalDiscreteTypeDefinitionImpl formalDiscreteTypeDefinition = new FormalDiscreteTypeDefinitionImpl();
		return formalDiscreteTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalFloatingPointDefinition createFormalFloatingPointDefinition() {
		FormalFloatingPointDefinitionImpl formalFloatingPointDefinition = new FormalFloatingPointDefinitionImpl();
		return formalFloatingPointDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalFunctionDeclaration createFormalFunctionDeclaration() {
		FormalFunctionDeclarationImpl formalFunctionDeclaration = new FormalFunctionDeclarationImpl();
		return formalFunctionDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalIncompleteTypeDeclaration createFormalIncompleteTypeDeclaration() {
		FormalIncompleteTypeDeclarationImpl formalIncompleteTypeDeclaration = new FormalIncompleteTypeDeclarationImpl();
		return formalIncompleteTypeDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalLimitedInterface createFormalLimitedInterface() {
		FormalLimitedInterfaceImpl formalLimitedInterface = new FormalLimitedInterfaceImpl();
		return formalLimitedInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalModularTypeDefinition createFormalModularTypeDefinition() {
		FormalModularTypeDefinitionImpl formalModularTypeDefinition = new FormalModularTypeDefinitionImpl();
		return formalModularTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalObjectDeclaration createFormalObjectDeclaration() {
		FormalObjectDeclarationImpl formalObjectDeclaration = new FormalObjectDeclarationImpl();
		return formalObjectDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalOrdinaryFixedPointDefinition createFormalOrdinaryFixedPointDefinition() {
		FormalOrdinaryFixedPointDefinitionImpl formalOrdinaryFixedPointDefinition = new FormalOrdinaryFixedPointDefinitionImpl();
		return formalOrdinaryFixedPointDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalOrdinaryInterface createFormalOrdinaryInterface() {
		FormalOrdinaryInterfaceImpl formalOrdinaryInterface = new FormalOrdinaryInterfaceImpl();
		return formalOrdinaryInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalPackageDeclaration createFormalPackageDeclaration() {
		FormalPackageDeclarationImpl formalPackageDeclaration = new FormalPackageDeclarationImpl();
		return formalPackageDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalPackageDeclarationWithBox createFormalPackageDeclarationWithBox() {
		FormalPackageDeclarationWithBoxImpl formalPackageDeclarationWithBox = new FormalPackageDeclarationWithBoxImpl();
		return formalPackageDeclarationWithBox;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalPoolSpecificAccessToVariable createFormalPoolSpecificAccessToVariable() {
		FormalPoolSpecificAccessToVariableImpl formalPoolSpecificAccessToVariable = new FormalPoolSpecificAccessToVariableImpl();
		return formalPoolSpecificAccessToVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalPrivateTypeDefinition createFormalPrivateTypeDefinition() {
		FormalPrivateTypeDefinitionImpl formalPrivateTypeDefinition = new FormalPrivateTypeDefinitionImpl();
		return formalPrivateTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalProcedureDeclaration createFormalProcedureDeclaration() {
		FormalProcedureDeclarationImpl formalProcedureDeclaration = new FormalProcedureDeclarationImpl();
		return formalProcedureDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalProtectedInterface createFormalProtectedInterface() {
		FormalProtectedInterfaceImpl formalProtectedInterface = new FormalProtectedInterfaceImpl();
		return formalProtectedInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalSignedIntegerTypeDefinition createFormalSignedIntegerTypeDefinition() {
		FormalSignedIntegerTypeDefinitionImpl formalSignedIntegerTypeDefinition = new FormalSignedIntegerTypeDefinitionImpl();
		return formalSignedIntegerTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalSynchronizedInterface createFormalSynchronizedInterface() {
		FormalSynchronizedInterfaceImpl formalSynchronizedInterface = new FormalSynchronizedInterfaceImpl();
		return formalSynchronizedInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalTaggedPrivateTypeDefinition createFormalTaggedPrivateTypeDefinition() {
		FormalTaggedPrivateTypeDefinitionImpl formalTaggedPrivateTypeDefinition = new FormalTaggedPrivateTypeDefinitionImpl();
		return formalTaggedPrivateTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalTaskInterface createFormalTaskInterface() {
		FormalTaskInterfaceImpl formalTaskInterface = new FormalTaskInterfaceImpl();
		return formalTaskInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalTypeDeclaration createFormalTypeDeclaration() {
		FormalTypeDeclarationImpl formalTypeDeclaration = new FormalTypeDeclarationImpl();
		return formalTypeDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalUnconstrainedArrayDefinition createFormalUnconstrainedArrayDefinition() {
		FormalUnconstrainedArrayDefinitionImpl formalUnconstrainedArrayDefinition = new FormalUnconstrainedArrayDefinitionImpl();
		return formalUnconstrainedArrayDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ForSomeQuantifiedExpression createForSomeQuantifiedExpression() {
		ForSomeQuantifiedExpressionImpl forSomeQuantifiedExpression = new ForSomeQuantifiedExpressionImpl();
		return forSomeQuantifiedExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FractionAttribute createFractionAttribute() {
		FractionAttributeImpl fractionAttribute = new FractionAttributeImpl();
		return fractionAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionBodyDeclaration createFunctionBodyDeclaration() {
		FunctionBodyDeclarationImpl functionBodyDeclaration = new FunctionBodyDeclarationImpl();
		return functionBodyDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionBodyStub createFunctionBodyStub() {
		FunctionBodyStubImpl functionBodyStub = new FunctionBodyStubImpl();
		return functionBodyStub;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionCall createFunctionCall() {
		FunctionCallImpl functionCall = new FunctionCallImpl();
		return functionCall;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionDeclaration createFunctionDeclaration() {
		FunctionDeclarationImpl functionDeclaration = new FunctionDeclarationImpl();
		return functionDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionInstantiation createFunctionInstantiation() {
		FunctionInstantiationImpl functionInstantiation = new FunctionInstantiationImpl();
		return functionInstantiation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionRenamingDeclaration createFunctionRenamingDeclaration() {
		FunctionRenamingDeclarationImpl functionRenamingDeclaration = new FunctionRenamingDeclarationImpl();
		return functionRenamingDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GeneralizedIteratorSpecification createGeneralizedIteratorSpecification() {
		GeneralizedIteratorSpecificationImpl generalizedIteratorSpecification = new GeneralizedIteratorSpecificationImpl();
		return generalizedIteratorSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericAssociation createGenericAssociation() {
		GenericAssociationImpl genericAssociation = new GenericAssociationImpl();
		return genericAssociation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericFunctionDeclaration createGenericFunctionDeclaration() {
		GenericFunctionDeclarationImpl genericFunctionDeclaration = new GenericFunctionDeclarationImpl();
		return genericFunctionDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericFunctionRenamingDeclaration createGenericFunctionRenamingDeclaration() {
		GenericFunctionRenamingDeclarationImpl genericFunctionRenamingDeclaration = new GenericFunctionRenamingDeclarationImpl();
		return genericFunctionRenamingDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericPackageDeclaration createGenericPackageDeclaration() {
		GenericPackageDeclarationImpl genericPackageDeclaration = new GenericPackageDeclarationImpl();
		return genericPackageDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericPackageRenamingDeclaration createGenericPackageRenamingDeclaration() {
		GenericPackageRenamingDeclarationImpl genericPackageRenamingDeclaration = new GenericPackageRenamingDeclarationImpl();
		return genericPackageRenamingDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericProcedureDeclaration createGenericProcedureDeclaration() {
		GenericProcedureDeclarationImpl genericProcedureDeclaration = new GenericProcedureDeclarationImpl();
		return genericProcedureDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericProcedureRenamingDeclaration createGenericProcedureRenamingDeclaration() {
		GenericProcedureRenamingDeclarationImpl genericProcedureRenamingDeclaration = new GenericProcedureRenamingDeclarationImpl();
		return genericProcedureRenamingDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GotoStatement createGotoStatement() {
		GotoStatementImpl gotoStatement = new GotoStatementImpl();
		return gotoStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GreaterThanOperator createGreaterThanOperator() {
		GreaterThanOperatorImpl greaterThanOperator = new GreaterThanOperatorImpl();
		return greaterThanOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GreaterThanOrEqualOperator createGreaterThanOrEqualOperator() {
		GreaterThanOrEqualOperatorImpl greaterThanOrEqualOperator = new GreaterThanOrEqualOperatorImpl();
		return greaterThanOrEqualOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAbstractQType createHasAbstractQType() {
		HasAbstractQTypeImpl hasAbstractQType = new HasAbstractQTypeImpl();
		return hasAbstractQType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAbstractQType1 createHasAbstractQType1() {
		HasAbstractQType1Impl hasAbstractQType1 = new HasAbstractQType1Impl();
		return hasAbstractQType1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAbstractQType2 createHasAbstractQType2() {
		HasAbstractQType2Impl hasAbstractQType2 = new HasAbstractQType2Impl();
		return hasAbstractQType2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAbstractQType3 createHasAbstractQType3() {
		HasAbstractQType3Impl hasAbstractQType3 = new HasAbstractQType3Impl();
		return hasAbstractQType3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAbstractQType4 createHasAbstractQType4() {
		HasAbstractQType4Impl hasAbstractQType4 = new HasAbstractQType4Impl();
		return hasAbstractQType4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAbstractQType5 createHasAbstractQType5() {
		HasAbstractQType5Impl hasAbstractQType5 = new HasAbstractQType5Impl();
		return hasAbstractQType5;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAbstractQType6 createHasAbstractQType6() {
		HasAbstractQType6Impl hasAbstractQType6 = new HasAbstractQType6Impl();
		return hasAbstractQType6;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAbstractQType7 createHasAbstractQType7() {
		HasAbstractQType7Impl hasAbstractQType7 = new HasAbstractQType7Impl();
		return hasAbstractQType7;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAbstractQType8 createHasAbstractQType8() {
		HasAbstractQType8Impl hasAbstractQType8 = new HasAbstractQType8Impl();
		return hasAbstractQType8;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAbstractQType9 createHasAbstractQType9() {
		HasAbstractQType9Impl hasAbstractQType9 = new HasAbstractQType9Impl();
		return hasAbstractQType9;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAbstractQType10 createHasAbstractQType10() {
		HasAbstractQType10Impl hasAbstractQType10 = new HasAbstractQType10Impl();
		return hasAbstractQType10;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAbstractQType11 createHasAbstractQType11() {
		HasAbstractQType11Impl hasAbstractQType11 = new HasAbstractQType11Impl();
		return hasAbstractQType11;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAbstractQType12 createHasAbstractQType12() {
		HasAbstractQType12Impl hasAbstractQType12 = new HasAbstractQType12Impl();
		return hasAbstractQType12;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAbstractQType13 createHasAbstractQType13() {
		HasAbstractQType13Impl hasAbstractQType13 = new HasAbstractQType13Impl();
		return hasAbstractQType13;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAliasedQType createHasAliasedQType() {
		HasAliasedQTypeImpl hasAliasedQType = new HasAliasedQTypeImpl();
		return hasAliasedQType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAliasedQType1 createHasAliasedQType1() {
		HasAliasedQType1Impl hasAliasedQType1 = new HasAliasedQType1Impl();
		return hasAliasedQType1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAliasedQType2 createHasAliasedQType2() {
		HasAliasedQType2Impl hasAliasedQType2 = new HasAliasedQType2Impl();
		return hasAliasedQType2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAliasedQType3 createHasAliasedQType3() {
		HasAliasedQType3Impl hasAliasedQType3 = new HasAliasedQType3Impl();
		return hasAliasedQType3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAliasedQType4 createHasAliasedQType4() {
		HasAliasedQType4Impl hasAliasedQType4 = new HasAliasedQType4Impl();
		return hasAliasedQType4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAliasedQType5 createHasAliasedQType5() {
		HasAliasedQType5Impl hasAliasedQType5 = new HasAliasedQType5Impl();
		return hasAliasedQType5;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAliasedQType6 createHasAliasedQType6() {
		HasAliasedQType6Impl hasAliasedQType6 = new HasAliasedQType6Impl();
		return hasAliasedQType6;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAliasedQType7 createHasAliasedQType7() {
		HasAliasedQType7Impl hasAliasedQType7 = new HasAliasedQType7Impl();
		return hasAliasedQType7;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAliasedQType8 createHasAliasedQType8() {
		HasAliasedQType8Impl hasAliasedQType8 = new HasAliasedQType8Impl();
		return hasAliasedQType8;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasLimitedQType createHasLimitedQType() {
		HasLimitedQTypeImpl hasLimitedQType = new HasLimitedQTypeImpl();
		return hasLimitedQType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasLimitedQType1 createHasLimitedQType1() {
		HasLimitedQType1Impl hasLimitedQType1 = new HasLimitedQType1Impl();
		return hasLimitedQType1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasLimitedQType2 createHasLimitedQType2() {
		HasLimitedQType2Impl hasLimitedQType2 = new HasLimitedQType2Impl();
		return hasLimitedQType2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasLimitedQType3 createHasLimitedQType3() {
		HasLimitedQType3Impl hasLimitedQType3 = new HasLimitedQType3Impl();
		return hasLimitedQType3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasLimitedQType4 createHasLimitedQType4() {
		HasLimitedQType4Impl hasLimitedQType4 = new HasLimitedQType4Impl();
		return hasLimitedQType4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasLimitedQType5 createHasLimitedQType5() {
		HasLimitedQType5Impl hasLimitedQType5 = new HasLimitedQType5Impl();
		return hasLimitedQType5;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasLimitedQType6 createHasLimitedQType6() {
		HasLimitedQType6Impl hasLimitedQType6 = new HasLimitedQType6Impl();
		return hasLimitedQType6;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasLimitedQType7 createHasLimitedQType7() {
		HasLimitedQType7Impl hasLimitedQType7 = new HasLimitedQType7Impl();
		return hasLimitedQType7;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasLimitedQType8 createHasLimitedQType8() {
		HasLimitedQType8Impl hasLimitedQType8 = new HasLimitedQType8Impl();
		return hasLimitedQType8;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasLimitedQType9 createHasLimitedQType9() {
		HasLimitedQType9Impl hasLimitedQType9 = new HasLimitedQType9Impl();
		return hasLimitedQType9;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasLimitedQType10 createHasLimitedQType10() {
		HasLimitedQType10Impl hasLimitedQType10 = new HasLimitedQType10Impl();
		return hasLimitedQType10;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasLimitedQType11 createHasLimitedQType11() {
		HasLimitedQType11Impl hasLimitedQType11 = new HasLimitedQType11Impl();
		return hasLimitedQType11;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType createHasNullExclusionQType() {
		HasNullExclusionQTypeImpl hasNullExclusionQType = new HasNullExclusionQTypeImpl();
		return hasNullExclusionQType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType1 createHasNullExclusionQType1() {
		HasNullExclusionQType1Impl hasNullExclusionQType1 = new HasNullExclusionQType1Impl();
		return hasNullExclusionQType1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType2 createHasNullExclusionQType2() {
		HasNullExclusionQType2Impl hasNullExclusionQType2 = new HasNullExclusionQType2Impl();
		return hasNullExclusionQType2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType3 createHasNullExclusionQType3() {
		HasNullExclusionQType3Impl hasNullExclusionQType3 = new HasNullExclusionQType3Impl();
		return hasNullExclusionQType3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType4 createHasNullExclusionQType4() {
		HasNullExclusionQType4Impl hasNullExclusionQType4 = new HasNullExclusionQType4Impl();
		return hasNullExclusionQType4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType5 createHasNullExclusionQType5() {
		HasNullExclusionQType5Impl hasNullExclusionQType5 = new HasNullExclusionQType5Impl();
		return hasNullExclusionQType5;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType6 createHasNullExclusionQType6() {
		HasNullExclusionQType6Impl hasNullExclusionQType6 = new HasNullExclusionQType6Impl();
		return hasNullExclusionQType6;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType7 createHasNullExclusionQType7() {
		HasNullExclusionQType7Impl hasNullExclusionQType7 = new HasNullExclusionQType7Impl();
		return hasNullExclusionQType7;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType8 createHasNullExclusionQType8() {
		HasNullExclusionQType8Impl hasNullExclusionQType8 = new HasNullExclusionQType8Impl();
		return hasNullExclusionQType8;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType9 createHasNullExclusionQType9() {
		HasNullExclusionQType9Impl hasNullExclusionQType9 = new HasNullExclusionQType9Impl();
		return hasNullExclusionQType9;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType10 createHasNullExclusionQType10() {
		HasNullExclusionQType10Impl hasNullExclusionQType10 = new HasNullExclusionQType10Impl();
		return hasNullExclusionQType10;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType11 createHasNullExclusionQType11() {
		HasNullExclusionQType11Impl hasNullExclusionQType11 = new HasNullExclusionQType11Impl();
		return hasNullExclusionQType11;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType12 createHasNullExclusionQType12() {
		HasNullExclusionQType12Impl hasNullExclusionQType12 = new HasNullExclusionQType12Impl();
		return hasNullExclusionQType12;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType13 createHasNullExclusionQType13() {
		HasNullExclusionQType13Impl hasNullExclusionQType13 = new HasNullExclusionQType13Impl();
		return hasNullExclusionQType13;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType14 createHasNullExclusionQType14() {
		HasNullExclusionQType14Impl hasNullExclusionQType14 = new HasNullExclusionQType14Impl();
		return hasNullExclusionQType14;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType15 createHasNullExclusionQType15() {
		HasNullExclusionQType15Impl hasNullExclusionQType15 = new HasNullExclusionQType15Impl();
		return hasNullExclusionQType15;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType16 createHasNullExclusionQType16() {
		HasNullExclusionQType16Impl hasNullExclusionQType16 = new HasNullExclusionQType16Impl();
		return hasNullExclusionQType16;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType17 createHasNullExclusionQType17() {
		HasNullExclusionQType17Impl hasNullExclusionQType17 = new HasNullExclusionQType17Impl();
		return hasNullExclusionQType17;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType18 createHasNullExclusionQType18() {
		HasNullExclusionQType18Impl hasNullExclusionQType18 = new HasNullExclusionQType18Impl();
		return hasNullExclusionQType18;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType19 createHasNullExclusionQType19() {
		HasNullExclusionQType19Impl hasNullExclusionQType19 = new HasNullExclusionQType19Impl();
		return hasNullExclusionQType19;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType20 createHasNullExclusionQType20() {
		HasNullExclusionQType20Impl hasNullExclusionQType20 = new HasNullExclusionQType20Impl();
		return hasNullExclusionQType20;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType21 createHasNullExclusionQType21() {
		HasNullExclusionQType21Impl hasNullExclusionQType21 = new HasNullExclusionQType21Impl();
		return hasNullExclusionQType21;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType22 createHasNullExclusionQType22() {
		HasNullExclusionQType22Impl hasNullExclusionQType22 = new HasNullExclusionQType22Impl();
		return hasNullExclusionQType22;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType23 createHasNullExclusionQType23() {
		HasNullExclusionQType23Impl hasNullExclusionQType23 = new HasNullExclusionQType23Impl();
		return hasNullExclusionQType23;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType24 createHasNullExclusionQType24() {
		HasNullExclusionQType24Impl hasNullExclusionQType24 = new HasNullExclusionQType24Impl();
		return hasNullExclusionQType24;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasPrivateQType createHasPrivateQType() {
		HasPrivateQTypeImpl hasPrivateQType = new HasPrivateQTypeImpl();
		return hasPrivateQType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasPrivateQType1 createHasPrivateQType1() {
		HasPrivateQType1Impl hasPrivateQType1 = new HasPrivateQType1Impl();
		return hasPrivateQType1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasReverseQType createHasReverseQType() {
		HasReverseQTypeImpl hasReverseQType = new HasReverseQTypeImpl();
		return hasReverseQType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasReverseQType1 createHasReverseQType1() {
		HasReverseQType1Impl hasReverseQType1 = new HasReverseQType1Impl();
		return hasReverseQType1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasReverseQType2 createHasReverseQType2() {
		HasReverseQType2Impl hasReverseQType2 = new HasReverseQType2Impl();
		return hasReverseQType2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasSynchronizedQType createHasSynchronizedQType() {
		HasSynchronizedQTypeImpl hasSynchronizedQType = new HasSynchronizedQTypeImpl();
		return hasSynchronizedQType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasSynchronizedQType1 createHasSynchronizedQType1() {
		HasSynchronizedQType1Impl hasSynchronizedQType1 = new HasSynchronizedQType1Impl();
		return hasSynchronizedQType1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasTaggedQType createHasTaggedQType() {
		HasTaggedQTypeImpl hasTaggedQType = new HasTaggedQTypeImpl();
		return hasTaggedQType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Identifier createIdentifier() {
		IdentifierImpl identifier = new IdentifierImpl();
		return identifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IdentityAttribute createIdentityAttribute() {
		IdentityAttributeImpl identityAttribute = new IdentityAttributeImpl();
		return identityAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfExpression createIfExpression() {
		IfExpressionImpl ifExpression = new IfExpressionImpl();
		return ifExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfExpressionPath createIfExpressionPath() {
		IfExpressionPathImpl ifExpressionPath = new IfExpressionPathImpl();
		return ifExpressionPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfPath createIfPath() {
		IfPathImpl ifPath = new IfPathImpl();
		return ifPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfStatement createIfStatement() {
		IfStatementImpl ifStatement = new IfStatementImpl();
		return ifStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImageAttribute createImageAttribute() {
		ImageAttributeImpl imageAttribute = new ImageAttributeImpl();
		return imageAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImplementationDefinedAttribute createImplementationDefinedAttribute() {
		ImplementationDefinedAttributeImpl implementationDefinedAttribute = new ImplementationDefinedAttributeImpl();
		return implementationDefinedAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImplementationDefinedPragma createImplementationDefinedPragma() {
		ImplementationDefinedPragmaImpl implementationDefinedPragma = new ImplementationDefinedPragmaImpl();
		return implementationDefinedPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImportPragma createImportPragma() {
		ImportPragmaImpl importPragma = new ImportPragmaImpl();
		return importPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IncompleteTypeDeclaration createIncompleteTypeDeclaration() {
		IncompleteTypeDeclarationImpl incompleteTypeDeclaration = new IncompleteTypeDeclarationImpl();
		return incompleteTypeDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IndependentComponentsPragma createIndependentComponentsPragma() {
		IndependentComponentsPragmaImpl independentComponentsPragma = new IndependentComponentsPragmaImpl();
		return independentComponentsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IndependentPragma createIndependentPragma() {
		IndependentPragmaImpl independentPragma = new IndependentPragmaImpl();
		return independentPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IndexConstraint createIndexConstraint() {
		IndexConstraintImpl indexConstraint = new IndexConstraintImpl();
		return indexConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IndexedComponent createIndexedComponent() {
		IndexedComponentImpl indexedComponent = new IndexedComponentImpl();
		return indexedComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InlinePragma createInlinePragma() {
		InlinePragmaImpl inlinePragma = new InlinePragmaImpl();
		return inlinePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InMembershipTest createInMembershipTest() {
		InMembershipTestImpl inMembershipTest = new InMembershipTestImpl();
		return inMembershipTest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InputAttribute createInputAttribute() {
		InputAttributeImpl inputAttribute = new InputAttributeImpl();
		return inputAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InspectionPointPragma createInspectionPointPragma() {
		InspectionPointPragmaImpl inspectionPointPragma = new InspectionPointPragmaImpl();
		return inspectionPointPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntegerLiteral createIntegerLiteral() {
		IntegerLiteralImpl integerLiteral = new IntegerLiteralImpl();
		return integerLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntegerNumberDeclaration createIntegerNumberDeclaration() {
		IntegerNumberDeclarationImpl integerNumberDeclaration = new IntegerNumberDeclarationImpl();
		return integerNumberDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterruptHandlerPragma createInterruptHandlerPragma() {
		InterruptHandlerPragmaImpl interruptHandlerPragma = new InterruptHandlerPragmaImpl();
		return interruptHandlerPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterruptPriorityPragma createInterruptPriorityPragma() {
		InterruptPriorityPragmaImpl interruptPriorityPragma = new InterruptPriorityPragmaImpl();
		return interruptPriorityPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotNullReturnQType createIsNotNullReturnQType() {
		IsNotNullReturnQTypeImpl isNotNullReturnQType = new IsNotNullReturnQTypeImpl();
		return isNotNullReturnQType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotNullReturnQType1 createIsNotNullReturnQType1() {
		IsNotNullReturnQType1Impl isNotNullReturnQType1 = new IsNotNullReturnQType1Impl();
		return isNotNullReturnQType1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotNullReturnQType2 createIsNotNullReturnQType2() {
		IsNotNullReturnQType2Impl isNotNullReturnQType2 = new IsNotNullReturnQType2Impl();
		return isNotNullReturnQType2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotNullReturnQType3 createIsNotNullReturnQType3() {
		IsNotNullReturnQType3Impl isNotNullReturnQType3 = new IsNotNullReturnQType3Impl();
		return isNotNullReturnQType3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotNullReturnQType4 createIsNotNullReturnQType4() {
		IsNotNullReturnQType4Impl isNotNullReturnQType4 = new IsNotNullReturnQType4Impl();
		return isNotNullReturnQType4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotNullReturnQType5 createIsNotNullReturnQType5() {
		IsNotNullReturnQType5Impl isNotNullReturnQType5 = new IsNotNullReturnQType5Impl();
		return isNotNullReturnQType5;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotNullReturnQType6 createIsNotNullReturnQType6() {
		IsNotNullReturnQType6Impl isNotNullReturnQType6 = new IsNotNullReturnQType6Impl();
		return isNotNullReturnQType6;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotNullReturnQType7 createIsNotNullReturnQType7() {
		IsNotNullReturnQType7Impl isNotNullReturnQType7 = new IsNotNullReturnQType7Impl();
		return isNotNullReturnQType7;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotNullReturnQType8 createIsNotNullReturnQType8() {
		IsNotNullReturnQType8Impl isNotNullReturnQType8 = new IsNotNullReturnQType8Impl();
		return isNotNullReturnQType8;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotNullReturnQType9 createIsNotNullReturnQType9() {
		IsNotNullReturnQType9Impl isNotNullReturnQType9 = new IsNotNullReturnQType9Impl();
		return isNotNullReturnQType9;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotNullReturnQType10 createIsNotNullReturnQType10() {
		IsNotNullReturnQType10Impl isNotNullReturnQType10 = new IsNotNullReturnQType10Impl();
		return isNotNullReturnQType10;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotNullReturnQType11 createIsNotNullReturnQType11() {
		IsNotNullReturnQType11Impl isNotNullReturnQType11 = new IsNotNullReturnQType11Impl();
		return isNotNullReturnQType11;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotOverridingDeclarationQType createIsNotOverridingDeclarationQType() {
		IsNotOverridingDeclarationQTypeImpl isNotOverridingDeclarationQType = new IsNotOverridingDeclarationQTypeImpl();
		return isNotOverridingDeclarationQType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotOverridingDeclarationQType1 createIsNotOverridingDeclarationQType1() {
		IsNotOverridingDeclarationQType1Impl isNotOverridingDeclarationQType1 = new IsNotOverridingDeclarationQType1Impl();
		return isNotOverridingDeclarationQType1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotOverridingDeclarationQType2 createIsNotOverridingDeclarationQType2() {
		IsNotOverridingDeclarationQType2Impl isNotOverridingDeclarationQType2 = new IsNotOverridingDeclarationQType2Impl();
		return isNotOverridingDeclarationQType2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotOverridingDeclarationQType3 createIsNotOverridingDeclarationQType3() {
		IsNotOverridingDeclarationQType3Impl isNotOverridingDeclarationQType3 = new IsNotOverridingDeclarationQType3Impl();
		return isNotOverridingDeclarationQType3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotOverridingDeclarationQType4 createIsNotOverridingDeclarationQType4() {
		IsNotOverridingDeclarationQType4Impl isNotOverridingDeclarationQType4 = new IsNotOverridingDeclarationQType4Impl();
		return isNotOverridingDeclarationQType4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotOverridingDeclarationQType5 createIsNotOverridingDeclarationQType5() {
		IsNotOverridingDeclarationQType5Impl isNotOverridingDeclarationQType5 = new IsNotOverridingDeclarationQType5Impl();
		return isNotOverridingDeclarationQType5;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotOverridingDeclarationQType6 createIsNotOverridingDeclarationQType6() {
		IsNotOverridingDeclarationQType6Impl isNotOverridingDeclarationQType6 = new IsNotOverridingDeclarationQType6Impl();
		return isNotOverridingDeclarationQType6;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotOverridingDeclarationQType7 createIsNotOverridingDeclarationQType7() {
		IsNotOverridingDeclarationQType7Impl isNotOverridingDeclarationQType7 = new IsNotOverridingDeclarationQType7Impl();
		return isNotOverridingDeclarationQType7;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotOverridingDeclarationQType8 createIsNotOverridingDeclarationQType8() {
		IsNotOverridingDeclarationQType8Impl isNotOverridingDeclarationQType8 = new IsNotOverridingDeclarationQType8Impl();
		return isNotOverridingDeclarationQType8;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotOverridingDeclarationQType9 createIsNotOverridingDeclarationQType9() {
		IsNotOverridingDeclarationQType9Impl isNotOverridingDeclarationQType9 = new IsNotOverridingDeclarationQType9Impl();
		return isNotOverridingDeclarationQType9;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotOverridingDeclarationQType10 createIsNotOverridingDeclarationQType10() {
		IsNotOverridingDeclarationQType10Impl isNotOverridingDeclarationQType10 = new IsNotOverridingDeclarationQType10Impl();
		return isNotOverridingDeclarationQType10;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsNotOverridingDeclarationQType11 createIsNotOverridingDeclarationQType11() {
		IsNotOverridingDeclarationQType11Impl isNotOverridingDeclarationQType11 = new IsNotOverridingDeclarationQType11Impl();
		return isNotOverridingDeclarationQType11;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsOverridingDeclarationQType createIsOverridingDeclarationQType() {
		IsOverridingDeclarationQTypeImpl isOverridingDeclarationQType = new IsOverridingDeclarationQTypeImpl();
		return isOverridingDeclarationQType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsOverridingDeclarationQType1 createIsOverridingDeclarationQType1() {
		IsOverridingDeclarationQType1Impl isOverridingDeclarationQType1 = new IsOverridingDeclarationQType1Impl();
		return isOverridingDeclarationQType1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsOverridingDeclarationQType2 createIsOverridingDeclarationQType2() {
		IsOverridingDeclarationQType2Impl isOverridingDeclarationQType2 = new IsOverridingDeclarationQType2Impl();
		return isOverridingDeclarationQType2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsOverridingDeclarationQType3 createIsOverridingDeclarationQType3() {
		IsOverridingDeclarationQType3Impl isOverridingDeclarationQType3 = new IsOverridingDeclarationQType3Impl();
		return isOverridingDeclarationQType3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsOverridingDeclarationQType4 createIsOverridingDeclarationQType4() {
		IsOverridingDeclarationQType4Impl isOverridingDeclarationQType4 = new IsOverridingDeclarationQType4Impl();
		return isOverridingDeclarationQType4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsOverridingDeclarationQType5 createIsOverridingDeclarationQType5() {
		IsOverridingDeclarationQType5Impl isOverridingDeclarationQType5 = new IsOverridingDeclarationQType5Impl();
		return isOverridingDeclarationQType5;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsOverridingDeclarationQType6 createIsOverridingDeclarationQType6() {
		IsOverridingDeclarationQType6Impl isOverridingDeclarationQType6 = new IsOverridingDeclarationQType6Impl();
		return isOverridingDeclarationQType6;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsOverridingDeclarationQType7 createIsOverridingDeclarationQType7() {
		IsOverridingDeclarationQType7Impl isOverridingDeclarationQType7 = new IsOverridingDeclarationQType7Impl();
		return isOverridingDeclarationQType7;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsOverridingDeclarationQType8 createIsOverridingDeclarationQType8() {
		IsOverridingDeclarationQType8Impl isOverridingDeclarationQType8 = new IsOverridingDeclarationQType8Impl();
		return isOverridingDeclarationQType8;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsOverridingDeclarationQType9 createIsOverridingDeclarationQType9() {
		IsOverridingDeclarationQType9Impl isOverridingDeclarationQType9 = new IsOverridingDeclarationQType9Impl();
		return isOverridingDeclarationQType9;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsOverridingDeclarationQType10 createIsOverridingDeclarationQType10() {
		IsOverridingDeclarationQType10Impl isOverridingDeclarationQType10 = new IsOverridingDeclarationQType10Impl();
		return isOverridingDeclarationQType10;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsOverridingDeclarationQType11 createIsOverridingDeclarationQType11() {
		IsOverridingDeclarationQType11Impl isOverridingDeclarationQType11 = new IsOverridingDeclarationQType11Impl();
		return isOverridingDeclarationQType11;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsPrefixCall createIsPrefixCall() {
		IsPrefixCallImpl isPrefixCall = new IsPrefixCallImpl();
		return isPrefixCall;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsPrefixCallQType createIsPrefixCallQType() {
		IsPrefixCallQTypeImpl isPrefixCallQType = new IsPrefixCallQTypeImpl();
		return isPrefixCallQType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsPrefixNotation createIsPrefixNotation() {
		IsPrefixNotationImpl isPrefixNotation = new IsPrefixNotationImpl();
		return isPrefixNotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsPrefixNotationQType createIsPrefixNotationQType() {
		IsPrefixNotationQTypeImpl isPrefixNotationQType = new IsPrefixNotationQTypeImpl();
		return isPrefixNotationQType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsPrefixNotationQType1 createIsPrefixNotationQType1() {
		IsPrefixNotationQType1Impl isPrefixNotationQType1 = new IsPrefixNotationQType1Impl();
		return isPrefixNotationQType1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public KnownDiscriminantPart createKnownDiscriminantPart() {
		KnownDiscriminantPartImpl knownDiscriminantPart = new KnownDiscriminantPartImpl();
		return knownDiscriminantPart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LastAttribute createLastAttribute() {
		LastAttributeImpl lastAttribute = new LastAttributeImpl();
		return lastAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LastBitAttribute createLastBitAttribute() {
		LastBitAttributeImpl lastBitAttribute = new LastBitAttributeImpl();
		return lastBitAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeadingPartAttribute createLeadingPartAttribute() {
		LeadingPartAttributeImpl leadingPartAttribute = new LeadingPartAttributeImpl();
		return leadingPartAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LengthAttribute createLengthAttribute() {
		LengthAttributeImpl lengthAttribute = new LengthAttributeImpl();
		return lengthAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LessThanOperator createLessThanOperator() {
		LessThanOperatorImpl lessThanOperator = new LessThanOperatorImpl();
		return lessThanOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LessThanOrEqualOperator createLessThanOrEqualOperator() {
		LessThanOrEqualOperatorImpl lessThanOrEqualOperator = new LessThanOrEqualOperatorImpl();
		return lessThanOrEqualOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Limited createLimited() {
		LimitedImpl limited = new LimitedImpl();
		return limited;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LimitedInterface createLimitedInterface() {
		LimitedInterfaceImpl limitedInterface = new LimitedInterfaceImpl();
		return limitedInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkerOptionsPragma createLinkerOptionsPragma() {
		LinkerOptionsPragmaImpl linkerOptionsPragma = new LinkerOptionsPragmaImpl();
		return linkerOptionsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ListPragma createListPragma() {
		ListPragmaImpl listPragma = new ListPragmaImpl();
		return listPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LockingPolicyPragma createLockingPolicyPragma() {
		LockingPolicyPragmaImpl lockingPolicyPragma = new LockingPolicyPragmaImpl();
		return lockingPolicyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LoopParameterSpecification createLoopParameterSpecification() {
		LoopParameterSpecificationImpl loopParameterSpecification = new LoopParameterSpecificationImpl();
		return loopParameterSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LoopStatement createLoopStatement() {
		LoopStatementImpl loopStatement = new LoopStatementImpl();
		return loopStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MachineAttribute createMachineAttribute() {
		MachineAttributeImpl machineAttribute = new MachineAttributeImpl();
		return machineAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MachineEmaxAttribute createMachineEmaxAttribute() {
		MachineEmaxAttributeImpl machineEmaxAttribute = new MachineEmaxAttributeImpl();
		return machineEmaxAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MachineEminAttribute createMachineEminAttribute() {
		MachineEminAttributeImpl machineEminAttribute = new MachineEminAttributeImpl();
		return machineEminAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MachineMantissaAttribute createMachineMantissaAttribute() {
		MachineMantissaAttributeImpl machineMantissaAttribute = new MachineMantissaAttributeImpl();
		return machineMantissaAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MachineOverflowsAttribute createMachineOverflowsAttribute() {
		MachineOverflowsAttributeImpl machineOverflowsAttribute = new MachineOverflowsAttributeImpl();
		return machineOverflowsAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MachineRadixAttribute createMachineRadixAttribute() {
		MachineRadixAttributeImpl machineRadixAttribute = new MachineRadixAttributeImpl();
		return machineRadixAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MachineRoundingAttribute createMachineRoundingAttribute() {
		MachineRoundingAttributeImpl machineRoundingAttribute = new MachineRoundingAttributeImpl();
		return machineRoundingAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MachineRoundsAttribute createMachineRoundsAttribute() {
		MachineRoundsAttributeImpl machineRoundsAttribute = new MachineRoundsAttributeImpl();
		return machineRoundsAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaxAlignmentForAllocationAttribute createMaxAlignmentForAllocationAttribute() {
		MaxAlignmentForAllocationAttributeImpl maxAlignmentForAllocationAttribute = new MaxAlignmentForAllocationAttributeImpl();
		return maxAlignmentForAllocationAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaxAttribute createMaxAttribute() {
		MaxAttributeImpl maxAttribute = new MaxAttributeImpl();
		return maxAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaxSizeInStorageElementsAttribute createMaxSizeInStorageElementsAttribute() {
		MaxSizeInStorageElementsAttributeImpl maxSizeInStorageElementsAttribute = new MaxSizeInStorageElementsAttributeImpl();
		return maxSizeInStorageElementsAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MinAttribute createMinAttribute() {
		MinAttributeImpl minAttribute = new MinAttributeImpl();
		return minAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MinusOperator createMinusOperator() {
		MinusOperatorImpl minusOperator = new MinusOperatorImpl();
		return minusOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModAttribute createModAttribute() {
		ModAttributeImpl modAttribute = new ModAttributeImpl();
		return modAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelAttribute createModelAttribute() {
		ModelAttributeImpl modelAttribute = new ModelAttributeImpl();
		return modelAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelEminAttribute createModelEminAttribute() {
		ModelEminAttributeImpl modelEminAttribute = new ModelEminAttributeImpl();
		return modelEminAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelEpsilonAttribute createModelEpsilonAttribute() {
		ModelEpsilonAttributeImpl modelEpsilonAttribute = new ModelEpsilonAttributeImpl();
		return modelEpsilonAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelMantissaAttribute createModelMantissaAttribute() {
		ModelMantissaAttributeImpl modelMantissaAttribute = new ModelMantissaAttributeImpl();
		return modelMantissaAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelSmallAttribute createModelSmallAttribute() {
		ModelSmallAttributeImpl modelSmallAttribute = new ModelSmallAttributeImpl();
		return modelSmallAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModOperator createModOperator() {
		ModOperatorImpl modOperator = new ModOperatorImpl();
		return modOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModularTypeDefinition createModularTypeDefinition() {
		ModularTypeDefinitionImpl modularTypeDefinition = new ModularTypeDefinitionImpl();
		return modularTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModulusAttribute createModulusAttribute() {
		ModulusAttributeImpl modulusAttribute = new ModulusAttributeImpl();
		return modulusAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiplyOperator createMultiplyOperator() {
		MultiplyOperatorImpl multiplyOperator = new MultiplyOperatorImpl();
		return multiplyOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NameClass createNameClass() {
		NameClassImpl nameClass = new NameClassImpl();
		return nameClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NamedArrayAggregate createNamedArrayAggregate() {
		NamedArrayAggregateImpl namedArrayAggregate = new NamedArrayAggregateImpl();
		return namedArrayAggregate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NameList createNameList() {
		NameListImpl nameList = new NameListImpl();
		return nameList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NoReturnPragma createNoReturnPragma() {
		NoReturnPragmaImpl noReturnPragma = new NoReturnPragmaImpl();
		return noReturnPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NormalizeScalarsPragma createNormalizeScalarsPragma() {
		NormalizeScalarsPragmaImpl normalizeScalarsPragma = new NormalizeScalarsPragmaImpl();
		return normalizeScalarsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotAnElement createNotAnElement() {
		NotAnElementImpl notAnElement = new NotAnElementImpl();
		return notAnElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotEqualOperator createNotEqualOperator() {
		NotEqualOperatorImpl notEqualOperator = new NotEqualOperatorImpl();
		return notEqualOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotInMembershipTest createNotInMembershipTest() {
		NotInMembershipTestImpl notInMembershipTest = new NotInMembershipTestImpl();
		return notInMembershipTest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotNullReturn createNotNullReturn() {
		NotNullReturnImpl notNullReturn = new NotNullReturnImpl();
		return notNullReturn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotOperator createNotOperator() {
		NotOperatorImpl notOperator = new NotOperatorImpl();
		return notOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotOverriding createNotOverriding() {
		NotOverridingImpl notOverriding = new NotOverridingImpl();
		return notOverriding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NullComponent createNullComponent() {
		NullComponentImpl nullComponent = new NullComponentImpl();
		return nullComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NullExclusion createNullExclusion() {
		NullExclusionImpl nullExclusion = new NullExclusionImpl();
		return nullExclusion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NullLiteral createNullLiteral() {
		NullLiteralImpl nullLiteral = new NullLiteralImpl();
		return nullLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NullProcedureDeclaration createNullProcedureDeclaration() {
		NullProcedureDeclarationImpl nullProcedureDeclaration = new NullProcedureDeclarationImpl();
		return nullProcedureDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NullRecordDefinition createNullRecordDefinition() {
		NullRecordDefinitionImpl nullRecordDefinition = new NullRecordDefinitionImpl();
		return nullRecordDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NullStatement createNullStatement() {
		NullStatementImpl nullStatement = new NullStatementImpl();
		return nullStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectRenamingDeclaration createObjectRenamingDeclaration() {
		ObjectRenamingDeclarationImpl objectRenamingDeclaration = new ObjectRenamingDeclarationImpl();
		return objectRenamingDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OptimizePragma createOptimizePragma() {
		OptimizePragmaImpl optimizePragma = new OptimizePragmaImpl();
		return optimizePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrdinaryFixedPointDefinition createOrdinaryFixedPointDefinition() {
		OrdinaryFixedPointDefinitionImpl ordinaryFixedPointDefinition = new OrdinaryFixedPointDefinitionImpl();
		return ordinaryFixedPointDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrdinaryInterface createOrdinaryInterface() {
		OrdinaryInterfaceImpl ordinaryInterface = new OrdinaryInterfaceImpl();
		return ordinaryInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrdinaryTypeDeclaration createOrdinaryTypeDeclaration() {
		OrdinaryTypeDeclarationImpl ordinaryTypeDeclaration = new OrdinaryTypeDeclarationImpl();
		return ordinaryTypeDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrElseShortCircuit createOrElseShortCircuit() {
		OrElseShortCircuitImpl orElseShortCircuit = new OrElseShortCircuitImpl();
		return orElseShortCircuit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrOperator createOrOperator() {
		OrOperatorImpl orOperator = new OrOperatorImpl();
		return orOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrPath createOrPath() {
		OrPathImpl orPath = new OrPathImpl();
		return orPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OthersChoice createOthersChoice() {
		OthersChoiceImpl othersChoice = new OthersChoiceImpl();
		return othersChoice;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutputAttribute createOutputAttribute() {
		OutputAttributeImpl outputAttribute = new OutputAttributeImpl();
		return outputAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OverlapsStorageAttribute createOverlapsStorageAttribute() {
		OverlapsStorageAttributeImpl overlapsStorageAttribute = new OverlapsStorageAttributeImpl();
		return overlapsStorageAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Overriding createOverriding() {
		OverridingImpl overriding = new OverridingImpl();
		return overriding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackageBodyDeclaration createPackageBodyDeclaration() {
		PackageBodyDeclarationImpl packageBodyDeclaration = new PackageBodyDeclarationImpl();
		return packageBodyDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackageBodyStub createPackageBodyStub() {
		PackageBodyStubImpl packageBodyStub = new PackageBodyStubImpl();
		return packageBodyStub;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackageDeclaration createPackageDeclaration() {
		PackageDeclarationImpl packageDeclaration = new PackageDeclarationImpl();
		return packageDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackageInstantiation createPackageInstantiation() {
		PackageInstantiationImpl packageInstantiation = new PackageInstantiationImpl();
		return packageInstantiation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackageRenamingDeclaration createPackageRenamingDeclaration() {
		PackageRenamingDeclarationImpl packageRenamingDeclaration = new PackageRenamingDeclarationImpl();
		return packageRenamingDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackPragma createPackPragma() {
		PackPragmaImpl packPragma = new PackPragmaImpl();
		return packPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PagePragma createPagePragma() {
		PagePragmaImpl pagePragma = new PagePragmaImpl();
		return pagePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterAssociation createParameterAssociation() {
		ParameterAssociationImpl parameterAssociation = new ParameterAssociationImpl();
		return parameterAssociation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterSpecification createParameterSpecification() {
		ParameterSpecificationImpl parameterSpecification = new ParameterSpecificationImpl();
		return parameterSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterSpecificationList createParameterSpecificationList() {
		ParameterSpecificationListImpl parameterSpecificationList = new ParameterSpecificationListImpl();
		return parameterSpecificationList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParenthesizedExpression createParenthesizedExpression() {
		ParenthesizedExpressionImpl parenthesizedExpression = new ParenthesizedExpressionImpl();
		return parenthesizedExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PartitionElaborationPolicyPragma createPartitionElaborationPolicyPragma() {
		PartitionElaborationPolicyPragmaImpl partitionElaborationPolicyPragma = new PartitionElaborationPolicyPragmaImpl();
		return partitionElaborationPolicyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PartitionIdAttribute createPartitionIdAttribute() {
		PartitionIdAttributeImpl partitionIdAttribute = new PartitionIdAttributeImpl();
		return partitionIdAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PathClass createPathClass() {
		PathClassImpl pathClass = new PathClassImpl();
		return pathClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PathList createPathList() {
		PathListImpl pathList = new PathListImpl();
		return pathList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PlusOperator createPlusOperator() {
		PlusOperatorImpl plusOperator = new PlusOperatorImpl();
		return plusOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PoolSpecificAccessToVariable createPoolSpecificAccessToVariable() {
		PoolSpecificAccessToVariableImpl poolSpecificAccessToVariable = new PoolSpecificAccessToVariableImpl();
		return poolSpecificAccessToVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PosAttribute createPosAttribute() {
		PosAttributeImpl posAttribute = new PosAttributeImpl();
		return posAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PositionalArrayAggregate createPositionalArrayAggregate() {
		PositionalArrayAggregateImpl positionalArrayAggregate = new PositionalArrayAggregateImpl();
		return positionalArrayAggregate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PositionAttribute createPositionAttribute() {
		PositionAttributeImpl positionAttribute = new PositionAttributeImpl();
		return positionAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PragmaArgumentAssociation createPragmaArgumentAssociation() {
		PragmaArgumentAssociationImpl pragmaArgumentAssociation = new PragmaArgumentAssociationImpl();
		return pragmaArgumentAssociation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PragmaElementClass createPragmaElementClass() {
		PragmaElementClassImpl pragmaElementClass = new PragmaElementClassImpl();
		return pragmaElementClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PredAttribute createPredAttribute() {
		PredAttributeImpl predAttribute = new PredAttributeImpl();
		return predAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PreelaborableInitializationPragma createPreelaborableInitializationPragma() {
		PreelaborableInitializationPragmaImpl preelaborableInitializationPragma = new PreelaborableInitializationPragmaImpl();
		return preelaborableInitializationPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PreelaboratePragma createPreelaboratePragma() {
		PreelaboratePragmaImpl preelaboratePragma = new PreelaboratePragmaImpl();
		return preelaboratePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PriorityAttribute createPriorityAttribute() {
		PriorityAttributeImpl priorityAttribute = new PriorityAttributeImpl();
		return priorityAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PriorityPragma createPriorityPragma() {
		PriorityPragmaImpl priorityPragma = new PriorityPragmaImpl();
		return priorityPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrioritySpecificDispatchingPragma createPrioritySpecificDispatchingPragma() {
		PrioritySpecificDispatchingPragmaImpl prioritySpecificDispatchingPragma = new PrioritySpecificDispatchingPragmaImpl();
		return prioritySpecificDispatchingPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Private createPrivate() {
		PrivateImpl private_ = new PrivateImpl();
		return private_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrivateExtensionDeclaration createPrivateExtensionDeclaration() {
		PrivateExtensionDeclarationImpl privateExtensionDeclaration = new PrivateExtensionDeclarationImpl();
		return privateExtensionDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrivateExtensionDefinition createPrivateExtensionDefinition() {
		PrivateExtensionDefinitionImpl privateExtensionDefinition = new PrivateExtensionDefinitionImpl();
		return privateExtensionDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrivateTypeDeclaration createPrivateTypeDeclaration() {
		PrivateTypeDeclarationImpl privateTypeDeclaration = new PrivateTypeDeclarationImpl();
		return privateTypeDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrivateTypeDefinition createPrivateTypeDefinition() {
		PrivateTypeDefinitionImpl privateTypeDefinition = new PrivateTypeDefinitionImpl();
		return privateTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureBodyDeclaration createProcedureBodyDeclaration() {
		ProcedureBodyDeclarationImpl procedureBodyDeclaration = new ProcedureBodyDeclarationImpl();
		return procedureBodyDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureBodyStub createProcedureBodyStub() {
		ProcedureBodyStubImpl procedureBodyStub = new ProcedureBodyStubImpl();
		return procedureBodyStub;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureCallStatement createProcedureCallStatement() {
		ProcedureCallStatementImpl procedureCallStatement = new ProcedureCallStatementImpl();
		return procedureCallStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureDeclaration createProcedureDeclaration() {
		ProcedureDeclarationImpl procedureDeclaration = new ProcedureDeclarationImpl();
		return procedureDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureInstantiation createProcedureInstantiation() {
		ProcedureInstantiationImpl procedureInstantiation = new ProcedureInstantiationImpl();
		return procedureInstantiation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureRenamingDeclaration createProcedureRenamingDeclaration() {
		ProcedureRenamingDeclarationImpl procedureRenamingDeclaration = new ProcedureRenamingDeclarationImpl();
		return procedureRenamingDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProfilePragma createProfilePragma() {
		ProfilePragmaImpl profilePragma = new ProfilePragmaImpl();
		return profilePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProtectedBodyDeclaration createProtectedBodyDeclaration() {
		ProtectedBodyDeclarationImpl protectedBodyDeclaration = new ProtectedBodyDeclarationImpl();
		return protectedBodyDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProtectedBodyStub createProtectedBodyStub() {
		ProtectedBodyStubImpl protectedBodyStub = new ProtectedBodyStubImpl();
		return protectedBodyStub;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProtectedDefinition createProtectedDefinition() {
		ProtectedDefinitionImpl protectedDefinition = new ProtectedDefinitionImpl();
		return protectedDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProtectedInterface createProtectedInterface() {
		ProtectedInterfaceImpl protectedInterface = new ProtectedInterfaceImpl();
		return protectedInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProtectedTypeDeclaration createProtectedTypeDeclaration() {
		ProtectedTypeDeclarationImpl protectedTypeDeclaration = new ProtectedTypeDeclarationImpl();
		return protectedTypeDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PurePragma createPurePragma() {
		PurePragmaImpl purePragma = new PurePragmaImpl();
		return purePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QualifiedExpression createQualifiedExpression() {
		QualifiedExpressionImpl qualifiedExpression = new QualifiedExpressionImpl();
		return qualifiedExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QueuingPolicyPragma createQueuingPolicyPragma() {
		QueuingPolicyPragmaImpl queuingPolicyPragma = new QueuingPolicyPragmaImpl();
		return queuingPolicyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RaiseExpression createRaiseExpression() {
		RaiseExpressionImpl raiseExpression = new RaiseExpressionImpl();
		return raiseExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RaiseStatement createRaiseStatement() {
		RaiseStatementImpl raiseStatement = new RaiseStatementImpl();
		return raiseStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RangeAttribute createRangeAttribute() {
		RangeAttributeImpl rangeAttribute = new RangeAttributeImpl();
		return rangeAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RangeAttributeReference createRangeAttributeReference() {
		RangeAttributeReferenceImpl rangeAttributeReference = new RangeAttributeReferenceImpl();
		return rangeAttributeReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RangeConstraintClass createRangeConstraintClass() {
		RangeConstraintClassImpl rangeConstraintClass = new RangeConstraintClassImpl();
		return rangeConstraintClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReadAttribute createReadAttribute() {
		ReadAttributeImpl readAttribute = new ReadAttributeImpl();
		return readAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RealLiteral createRealLiteral() {
		RealLiteralImpl realLiteral = new RealLiteralImpl();
		return realLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RealNumberDeclaration createRealNumberDeclaration() {
		RealNumberDeclarationImpl realNumberDeclaration = new RealNumberDeclarationImpl();
		return realNumberDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordAggregate createRecordAggregate() {
		RecordAggregateImpl recordAggregate = new RecordAggregateImpl();
		return recordAggregate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordComponentAssociation createRecordComponentAssociation() {
		RecordComponentAssociationImpl recordComponentAssociation = new RecordComponentAssociationImpl();
		return recordComponentAssociation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordComponentClass createRecordComponentClass() {
		RecordComponentClassImpl recordComponentClass = new RecordComponentClassImpl();
		return recordComponentClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordComponentList createRecordComponentList() {
		RecordComponentListImpl recordComponentList = new RecordComponentListImpl();
		return recordComponentList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordDefinition createRecordDefinition() {
		RecordDefinitionImpl recordDefinition = new RecordDefinitionImpl();
		return recordDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordRepresentationClause createRecordRepresentationClause() {
		RecordRepresentationClauseImpl recordRepresentationClause = new RecordRepresentationClauseImpl();
		return recordRepresentationClause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordTypeDefinition createRecordTypeDefinition() {
		RecordTypeDefinitionImpl recordTypeDefinition = new RecordTypeDefinitionImpl();
		return recordTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelativeDeadlinePragma createRelativeDeadlinePragma() {
		RelativeDeadlinePragmaImpl relativeDeadlinePragma = new RelativeDeadlinePragmaImpl();
		return relativeDeadlinePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RemainderAttribute createRemainderAttribute() {
		RemainderAttributeImpl remainderAttribute = new RemainderAttributeImpl();
		return remainderAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RemOperator createRemOperator() {
		RemOperatorImpl remOperator = new RemOperatorImpl();
		return remOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RemoteCallInterfacePragma createRemoteCallInterfacePragma() {
		RemoteCallInterfacePragmaImpl remoteCallInterfacePragma = new RemoteCallInterfacePragmaImpl();
		return remoteCallInterfacePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RemoteTypesPragma createRemoteTypesPragma() {
		RemoteTypesPragmaImpl remoteTypesPragma = new RemoteTypesPragmaImpl();
		return remoteTypesPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequeueStatement createRequeueStatement() {
		RequeueStatementImpl requeueStatement = new RequeueStatementImpl();
		return requeueStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequeueStatementWithAbort createRequeueStatementWithAbort() {
		RequeueStatementWithAbortImpl requeueStatementWithAbort = new RequeueStatementWithAbortImpl();
		return requeueStatementWithAbort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RestrictionsPragma createRestrictionsPragma() {
		RestrictionsPragmaImpl restrictionsPragma = new RestrictionsPragmaImpl();
		return restrictionsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReturnConstantSpecification createReturnConstantSpecification() {
		ReturnConstantSpecificationImpl returnConstantSpecification = new ReturnConstantSpecificationImpl();
		return returnConstantSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReturnStatement createReturnStatement() {
		ReturnStatementImpl returnStatement = new ReturnStatementImpl();
		return returnStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReturnVariableSpecification createReturnVariableSpecification() {
		ReturnVariableSpecificationImpl returnVariableSpecification = new ReturnVariableSpecificationImpl();
		return returnVariableSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reverse createReverse() {
		ReverseImpl reverse = new ReverseImpl();
		return reverse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReviewablePragma createReviewablePragma() {
		ReviewablePragmaImpl reviewablePragma = new ReviewablePragmaImpl();
		return reviewablePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RootIntegerDefinition createRootIntegerDefinition() {
		RootIntegerDefinitionImpl rootIntegerDefinition = new RootIntegerDefinitionImpl();
		return rootIntegerDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RootRealDefinition createRootRealDefinition() {
		RootRealDefinitionImpl rootRealDefinition = new RootRealDefinitionImpl();
		return rootRealDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoundAttribute createRoundAttribute() {
		RoundAttributeImpl roundAttribute = new RoundAttributeImpl();
		return roundAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoundingAttribute createRoundingAttribute() {
		RoundingAttributeImpl roundingAttribute = new RoundingAttributeImpl();
		return roundingAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SafeFirstAttribute createSafeFirstAttribute() {
		SafeFirstAttributeImpl safeFirstAttribute = new SafeFirstAttributeImpl();
		return safeFirstAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SafeLastAttribute createSafeLastAttribute() {
		SafeLastAttributeImpl safeLastAttribute = new SafeLastAttributeImpl();
		return safeLastAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScaleAttribute createScaleAttribute() {
		ScaleAttributeImpl scaleAttribute = new ScaleAttributeImpl();
		return scaleAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScalingAttribute createScalingAttribute() {
		ScalingAttributeImpl scalingAttribute = new ScalingAttributeImpl();
		return scalingAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SelectedComponent createSelectedComponent() {
		SelectedComponentImpl selectedComponent = new SelectedComponentImpl();
		return selectedComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SelectiveAcceptStatement createSelectiveAcceptStatement() {
		SelectiveAcceptStatementImpl selectiveAcceptStatement = new SelectiveAcceptStatementImpl();
		return selectiveAcceptStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SelectPath createSelectPath() {
		SelectPathImpl selectPath = new SelectPathImpl();
		return selectPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SharedPassivePragma createSharedPassivePragma() {
		SharedPassivePragmaImpl sharedPassivePragma = new SharedPassivePragmaImpl();
		return sharedPassivePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignedIntegerTypeDefinition createSignedIntegerTypeDefinition() {
		SignedIntegerTypeDefinitionImpl signedIntegerTypeDefinition = new SignedIntegerTypeDefinitionImpl();
		return signedIntegerTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignedZerosAttribute createSignedZerosAttribute() {
		SignedZerosAttributeImpl signedZerosAttribute = new SignedZerosAttributeImpl();
		return signedZerosAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleExpressionRange createSimpleExpressionRange() {
		SimpleExpressionRangeImpl simpleExpressionRange = new SimpleExpressionRangeImpl();
		return simpleExpressionRange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SingleProtectedDeclaration createSingleProtectedDeclaration() {
		SingleProtectedDeclarationImpl singleProtectedDeclaration = new SingleProtectedDeclarationImpl();
		return singleProtectedDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SingleTaskDeclaration createSingleTaskDeclaration() {
		SingleTaskDeclarationImpl singleTaskDeclaration = new SingleTaskDeclarationImpl();
		return singleTaskDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SizeAttribute createSizeAttribute() {
		SizeAttributeImpl sizeAttribute = new SizeAttributeImpl();
		return sizeAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Slice createSlice() {
		SliceImpl slice = new SliceImpl();
		return slice;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SmallAttribute createSmallAttribute() {
		SmallAttributeImpl smallAttribute = new SmallAttributeImpl();
		return smallAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation createSourceLocation() {
		SourceLocationImpl sourceLocation = new SourceLocationImpl();
		return sourceLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatementClass createStatementClass() {
		StatementClassImpl statementClass = new StatementClassImpl();
		return statementClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatementList createStatementList() {
		StatementListImpl statementList = new StatementListImpl();
		return statementList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StoragePoolAttribute createStoragePoolAttribute() {
		StoragePoolAttributeImpl storagePoolAttribute = new StoragePoolAttributeImpl();
		return storagePoolAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StorageSizeAttribute createStorageSizeAttribute() {
		StorageSizeAttributeImpl storageSizeAttribute = new StorageSizeAttributeImpl();
		return storageSizeAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StorageSizePragma createStorageSizePragma() {
		StorageSizePragmaImpl storageSizePragma = new StorageSizePragmaImpl();
		return storageSizePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StreamSizeAttribute createStreamSizeAttribute() {
		StreamSizeAttributeImpl streamSizeAttribute = new StreamSizeAttributeImpl();
		return streamSizeAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StringLiteral createStringLiteral() {
		StringLiteralImpl stringLiteral = new StringLiteralImpl();
		return stringLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubtypeDeclaration createSubtypeDeclaration() {
		SubtypeDeclarationImpl subtypeDeclaration = new SubtypeDeclarationImpl();
		return subtypeDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubtypeIndication createSubtypeIndication() {
		SubtypeIndicationImpl subtypeIndication = new SubtypeIndicationImpl();
		return subtypeIndication;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SuccAttribute createSuccAttribute() {
		SuccAttributeImpl succAttribute = new SuccAttributeImpl();
		return succAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SuppressPragma createSuppressPragma() {
		SuppressPragmaImpl suppressPragma = new SuppressPragmaImpl();
		return suppressPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Synchronized createSynchronized() {
		SynchronizedImpl synchronized_ = new SynchronizedImpl();
		return synchronized_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SynchronizedInterface createSynchronizedInterface() {
		SynchronizedInterfaceImpl synchronizedInterface = new SynchronizedInterfaceImpl();
		return synchronizedInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TagAttribute createTagAttribute() {
		TagAttributeImpl tagAttribute = new TagAttributeImpl();
		return tagAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Tagged createTagged() {
		TaggedImpl tagged = new TaggedImpl();
		return tagged;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaggedIncompleteTypeDeclaration createTaggedIncompleteTypeDeclaration() {
		TaggedIncompleteTypeDeclarationImpl taggedIncompleteTypeDeclaration = new TaggedIncompleteTypeDeclarationImpl();
		return taggedIncompleteTypeDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaggedPrivateTypeDefinition createTaggedPrivateTypeDefinition() {
		TaggedPrivateTypeDefinitionImpl taggedPrivateTypeDefinition = new TaggedPrivateTypeDefinitionImpl();
		return taggedPrivateTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaggedRecordTypeDefinition createTaggedRecordTypeDefinition() {
		TaggedRecordTypeDefinitionImpl taggedRecordTypeDefinition = new TaggedRecordTypeDefinitionImpl();
		return taggedRecordTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskBodyDeclaration createTaskBodyDeclaration() {
		TaskBodyDeclarationImpl taskBodyDeclaration = new TaskBodyDeclarationImpl();
		return taskBodyDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskBodyStub createTaskBodyStub() {
		TaskBodyStubImpl taskBodyStub = new TaskBodyStubImpl();
		return taskBodyStub;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskDefinition createTaskDefinition() {
		TaskDefinitionImpl taskDefinition = new TaskDefinitionImpl();
		return taskDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskDispatchingPolicyPragma createTaskDispatchingPolicyPragma() {
		TaskDispatchingPolicyPragmaImpl taskDispatchingPolicyPragma = new TaskDispatchingPolicyPragmaImpl();
		return taskDispatchingPolicyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskInterface createTaskInterface() {
		TaskInterfaceImpl taskInterface = new TaskInterfaceImpl();
		return taskInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskTypeDeclaration createTaskTypeDeclaration() {
		TaskTypeDeclarationImpl taskTypeDeclaration = new TaskTypeDeclarationImpl();
		return taskTypeDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TerminateAlternativeStatement createTerminateAlternativeStatement() {
		TerminateAlternativeStatementImpl terminateAlternativeStatement = new TerminateAlternativeStatementImpl();
		return terminateAlternativeStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TerminatedAttribute createTerminatedAttribute() {
		TerminatedAttributeImpl terminatedAttribute = new TerminatedAttributeImpl();
		return terminatedAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ThenAbortPath createThenAbortPath() {
		ThenAbortPathImpl thenAbortPath = new ThenAbortPathImpl();
		return thenAbortPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimedEntryCallStatement createTimedEntryCallStatement() {
		TimedEntryCallStatementImpl timedEntryCallStatement = new TimedEntryCallStatementImpl();
		return timedEntryCallStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TruncationAttribute createTruncationAttribute() {
		TruncationAttributeImpl truncationAttribute = new TruncationAttributeImpl();
		return truncationAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeConversion createTypeConversion() {
		TypeConversionImpl typeConversion = new TypeConversionImpl();
		return typeConversion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnaryMinusOperator createUnaryMinusOperator() {
		UnaryMinusOperatorImpl unaryMinusOperator = new UnaryMinusOperatorImpl();
		return unaryMinusOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnaryPlusOperator createUnaryPlusOperator() {
		UnaryPlusOperatorImpl unaryPlusOperator = new UnaryPlusOperatorImpl();
		return unaryPlusOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnbiasedRoundingAttribute createUnbiasedRoundingAttribute() {
		UnbiasedRoundingAttributeImpl unbiasedRoundingAttribute = new UnbiasedRoundingAttributeImpl();
		return unbiasedRoundingAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UncheckedAccessAttribute createUncheckedAccessAttribute() {
		UncheckedAccessAttributeImpl uncheckedAccessAttribute = new UncheckedAccessAttributeImpl();
		return uncheckedAccessAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UncheckedUnionPragma createUncheckedUnionPragma() {
		UncheckedUnionPragmaImpl uncheckedUnionPragma = new UncheckedUnionPragmaImpl();
		return uncheckedUnionPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnconstrainedArrayDefinition createUnconstrainedArrayDefinition() {
		UnconstrainedArrayDefinitionImpl unconstrainedArrayDefinition = new UnconstrainedArrayDefinitionImpl();
		return unconstrainedArrayDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UniversalFixedDefinition createUniversalFixedDefinition() {
		UniversalFixedDefinitionImpl universalFixedDefinition = new UniversalFixedDefinitionImpl();
		return universalFixedDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UniversalIntegerDefinition createUniversalIntegerDefinition() {
		UniversalIntegerDefinitionImpl universalIntegerDefinition = new UniversalIntegerDefinitionImpl();
		return universalIntegerDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UniversalRealDefinition createUniversalRealDefinition() {
		UniversalRealDefinitionImpl universalRealDefinition = new UniversalRealDefinitionImpl();
		return universalRealDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnknownAttribute createUnknownAttribute() {
		UnknownAttributeImpl unknownAttribute = new UnknownAttributeImpl();
		return unknownAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnknownDiscriminantPart createUnknownDiscriminantPart() {
		UnknownDiscriminantPartImpl unknownDiscriminantPart = new UnknownDiscriminantPartImpl();
		return unknownDiscriminantPart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnknownPragma createUnknownPragma() {
		UnknownPragmaImpl unknownPragma = new UnknownPragmaImpl();
		return unknownPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnsuppressPragma createUnsuppressPragma() {
		UnsuppressPragmaImpl unsuppressPragma = new UnsuppressPragmaImpl();
		return unsuppressPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseAllTypeClause createUseAllTypeClause() {
		UseAllTypeClauseImpl useAllTypeClause = new UseAllTypeClauseImpl();
		return useAllTypeClause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UsePackageClause createUsePackageClause() {
		UsePackageClauseImpl usePackageClause = new UsePackageClauseImpl();
		return usePackageClause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UseTypeClause createUseTypeClause() {
		UseTypeClauseImpl useTypeClause = new UseTypeClauseImpl();
		return useTypeClause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValAttribute createValAttribute() {
		ValAttributeImpl valAttribute = new ValAttributeImpl();
		return valAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValidAttribute createValidAttribute() {
		ValidAttributeImpl validAttribute = new ValidAttributeImpl();
		return validAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueAttribute createValueAttribute() {
		ValueAttributeImpl valueAttribute = new ValueAttributeImpl();
		return valueAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariableDeclaration createVariableDeclaration() {
		VariableDeclarationImpl variableDeclaration = new VariableDeclarationImpl();
		return variableDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variant createVariant() {
		VariantImpl variant = new VariantImpl();
		return variant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariantList createVariantList() {
		VariantListImpl variantList = new VariantListImpl();
		return variantList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariantPart createVariantPart() {
		VariantPartImpl variantPart = new VariantPartImpl();
		return variantPart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VersionAttribute createVersionAttribute() {
		VersionAttributeImpl versionAttribute = new VersionAttributeImpl();
		return versionAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VolatileComponentsPragma createVolatileComponentsPragma() {
		VolatileComponentsPragmaImpl volatileComponentsPragma = new VolatileComponentsPragmaImpl();
		return volatileComponentsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VolatilePragma createVolatilePragma() {
		VolatilePragmaImpl volatilePragma = new VolatilePragmaImpl();
		return volatilePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WhileLoopStatement createWhileLoopStatement() {
		WhileLoopStatementImpl whileLoopStatement = new WhileLoopStatementImpl();
		return whileLoopStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WideImageAttribute createWideImageAttribute() {
		WideImageAttributeImpl wideImageAttribute = new WideImageAttributeImpl();
		return wideImageAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WideValueAttribute createWideValueAttribute() {
		WideValueAttributeImpl wideValueAttribute = new WideValueAttributeImpl();
		return wideValueAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WideWideImageAttribute createWideWideImageAttribute() {
		WideWideImageAttributeImpl wideWideImageAttribute = new WideWideImageAttributeImpl();
		return wideWideImageAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WideWideValueAttribute createWideWideValueAttribute() {
		WideWideValueAttributeImpl wideWideValueAttribute = new WideWideValueAttributeImpl();
		return wideWideValueAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WideWideWidthAttribute createWideWideWidthAttribute() {
		WideWideWidthAttributeImpl wideWideWidthAttribute = new WideWideWidthAttributeImpl();
		return wideWideWidthAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WideWidthAttribute createWideWidthAttribute() {
		WideWidthAttributeImpl wideWidthAttribute = new WideWidthAttributeImpl();
		return wideWidthAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WidthAttribute createWidthAttribute() {
		WidthAttributeImpl widthAttribute = new WidthAttributeImpl();
		return widthAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WithClause createWithClause() {
		WithClauseImpl withClause = new WithClauseImpl();
		return withClause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WriteAttribute createWriteAttribute() {
		WriteAttributeImpl writeAttribute = new WriteAttributeImpl();
		return writeAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XorOperator createXorOperator() {
		XorOperatorImpl xorOperator = new XorOperatorImpl();
		return xorOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AdaPackage getAdaPackage() {
		return (AdaPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static AdaPackage getPackage() {
		return AdaPackage.eINSTANCE;
	}

} //AdaFactoryImpl
