/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Access To Protected Function</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.AccessToProtectedFunction#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.AccessToProtectedFunction#getHasNullExclusionQ <em>Has Null Exclusion Q</em>}</li>
 *   <li>{@link Ada.AccessToProtectedFunction#getAccessToSubprogramParameterProfileQl <em>Access To Subprogram Parameter Profile Ql</em>}</li>
 *   <li>{@link Ada.AccessToProtectedFunction#getIsNotNullReturnQ <em>Is Not Null Return Q</em>}</li>
 *   <li>{@link Ada.AccessToProtectedFunction#getAccessToFunctionResultProfileQ <em>Access To Function Result Profile Q</em>}</li>
 *   <li>{@link Ada.AccessToProtectedFunction#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getAccessToProtectedFunction()
 * @model extendedMetaData="name='Access_To_Protected_Function' kind='elementOnly'"
 * @generated
 */
public interface AccessToProtectedFunction extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getAccessToProtectedFunction_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.AccessToProtectedFunction#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Has Null Exclusion Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Null Exclusion Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Null Exclusion Q</em>' containment reference.
	 * @see #setHasNullExclusionQ(HasNullExclusionQType)
	 * @see Ada.AdaPackage#getAccessToProtectedFunction_HasNullExclusionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='has_null_exclusion_q' namespace='##targetNamespace'"
	 * @generated
	 */
	HasNullExclusionQType getHasNullExclusionQ();

	/**
	 * Sets the value of the '{@link Ada.AccessToProtectedFunction#getHasNullExclusionQ <em>Has Null Exclusion Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Null Exclusion Q</em>' containment reference.
	 * @see #getHasNullExclusionQ()
	 * @generated
	 */
	void setHasNullExclusionQ(HasNullExclusionQType value);

	/**
	 * Returns the value of the '<em><b>Access To Subprogram Parameter Profile Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Subprogram Parameter Profile Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Subprogram Parameter Profile Ql</em>' containment reference.
	 * @see #setAccessToSubprogramParameterProfileQl(ParameterSpecificationList)
	 * @see Ada.AdaPackage#getAccessToProtectedFunction_AccessToSubprogramParameterProfileQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='access_to_subprogram_parameter_profile_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ParameterSpecificationList getAccessToSubprogramParameterProfileQl();

	/**
	 * Sets the value of the '{@link Ada.AccessToProtectedFunction#getAccessToSubprogramParameterProfileQl <em>Access To Subprogram Parameter Profile Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access To Subprogram Parameter Profile Ql</em>' containment reference.
	 * @see #getAccessToSubprogramParameterProfileQl()
	 * @generated
	 */
	void setAccessToSubprogramParameterProfileQl(ParameterSpecificationList value);

	/**
	 * Returns the value of the '<em><b>Is Not Null Return Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Not Null Return Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Not Null Return Q</em>' containment reference.
	 * @see #setIsNotNullReturnQ(IsNotNullReturnQType)
	 * @see Ada.AdaPackage#getAccessToProtectedFunction_IsNotNullReturnQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='is_not_null_return_q' namespace='##targetNamespace'"
	 * @generated
	 */
	IsNotNullReturnQType getIsNotNullReturnQ();

	/**
	 * Sets the value of the '{@link Ada.AccessToProtectedFunction#getIsNotNullReturnQ <em>Is Not Null Return Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Not Null Return Q</em>' containment reference.
	 * @see #getIsNotNullReturnQ()
	 * @generated
	 */
	void setIsNotNullReturnQ(IsNotNullReturnQType value);

	/**
	 * Returns the value of the '<em><b>Access To Function Result Profile Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Function Result Profile Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Function Result Profile Q</em>' containment reference.
	 * @see #setAccessToFunctionResultProfileQ(ElementClass)
	 * @see Ada.AdaPackage#getAccessToProtectedFunction_AccessToFunctionResultProfileQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='access_to_function_result_profile_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementClass getAccessToFunctionResultProfileQ();

	/**
	 * Sets the value of the '{@link Ada.AccessToProtectedFunction#getAccessToFunctionResultProfileQ <em>Access To Function Result Profile Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Access To Function Result Profile Q</em>' containment reference.
	 * @see #getAccessToFunctionResultProfileQ()
	 * @generated
	 */
	void setAccessToFunctionResultProfileQ(ElementClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getAccessToProtectedFunction_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.AccessToProtectedFunction#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // AccessToProtectedFunction
