/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Discrete Range Attribute Reference As Subtype Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.DiscreteRangeAttributeReferenceAsSubtypeDefinition#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.DiscreteRangeAttributeReferenceAsSubtypeDefinition#getRangeAttributeQ <em>Range Attribute Q</em>}</li>
 *   <li>{@link Ada.DiscreteRangeAttributeReferenceAsSubtypeDefinition#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getDiscreteRangeAttributeReferenceAsSubtypeDefinition()
 * @model extendedMetaData="name='Discrete_Range_Attribute_Reference_As_Subtype_Definition' kind='elementOnly'"
 * @generated
 */
public interface DiscreteRangeAttributeReferenceAsSubtypeDefinition extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getDiscreteRangeAttributeReferenceAsSubtypeDefinition_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.DiscreteRangeAttributeReferenceAsSubtypeDefinition#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Range Attribute Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Range Attribute Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range Attribute Q</em>' containment reference.
	 * @see #setRangeAttributeQ(ExpressionClass)
	 * @see Ada.AdaPackage#getDiscreteRangeAttributeReferenceAsSubtypeDefinition_RangeAttributeQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='range_attribute_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getRangeAttributeQ();

	/**
	 * Sets the value of the '{@link Ada.DiscreteRangeAttributeReferenceAsSubtypeDefinition#getRangeAttributeQ <em>Range Attribute Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Range Attribute Q</em>' containment reference.
	 * @see #getRangeAttributeQ()
	 * @generated
	 */
	void setRangeAttributeQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getDiscreteRangeAttributeReferenceAsSubtypeDefinition_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.DiscreteRangeAttributeReferenceAsSubtypeDefinition#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // DiscreteRangeAttributeReferenceAsSubtypeDefinition
