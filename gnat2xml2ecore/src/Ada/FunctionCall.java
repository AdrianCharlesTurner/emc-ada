/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Call</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.FunctionCall#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.FunctionCall#getPrefixQ <em>Prefix Q</em>}</li>
 *   <li>{@link Ada.FunctionCall#getFunctionCallParametersQl <em>Function Call Parameters Ql</em>}</li>
 *   <li>{@link Ada.FunctionCall#getIsPrefixCallQ <em>Is Prefix Call Q</em>}</li>
 *   <li>{@link Ada.FunctionCall#getIsPrefixNotationQ <em>Is Prefix Notation Q</em>}</li>
 *   <li>{@link Ada.FunctionCall#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.FunctionCall#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getFunctionCall()
 * @model extendedMetaData="name='Function_Call' kind='elementOnly'"
 * @generated
 */
public interface FunctionCall extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getFunctionCall_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.FunctionCall#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Prefix Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Prefix Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Prefix Q</em>' containment reference.
	 * @see #setPrefixQ(ExpressionClass)
	 * @see Ada.AdaPackage#getFunctionCall_PrefixQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='prefix_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getPrefixQ();

	/**
	 * Sets the value of the '{@link Ada.FunctionCall#getPrefixQ <em>Prefix Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Prefix Q</em>' containment reference.
	 * @see #getPrefixQ()
	 * @generated
	 */
	void setPrefixQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Function Call Parameters Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Call Parameters Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Call Parameters Ql</em>' containment reference.
	 * @see #setFunctionCallParametersQl(AssociationList)
	 * @see Ada.AdaPackage#getFunctionCall_FunctionCallParametersQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='function_call_parameters_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	AssociationList getFunctionCallParametersQl();

	/**
	 * Sets the value of the '{@link Ada.FunctionCall#getFunctionCallParametersQl <em>Function Call Parameters Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function Call Parameters Ql</em>' containment reference.
	 * @see #getFunctionCallParametersQl()
	 * @generated
	 */
	void setFunctionCallParametersQl(AssociationList value);

	/**
	 * Returns the value of the '<em><b>Is Prefix Call Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Prefix Call Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Prefix Call Q</em>' containment reference.
	 * @see #setIsPrefixCallQ(IsPrefixCallQType)
	 * @see Ada.AdaPackage#getFunctionCall_IsPrefixCallQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='is_prefix_call_q' namespace='##targetNamespace'"
	 * @generated
	 */
	IsPrefixCallQType getIsPrefixCallQ();

	/**
	 * Sets the value of the '{@link Ada.FunctionCall#getIsPrefixCallQ <em>Is Prefix Call Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Prefix Call Q</em>' containment reference.
	 * @see #getIsPrefixCallQ()
	 * @generated
	 */
	void setIsPrefixCallQ(IsPrefixCallQType value);

	/**
	 * Returns the value of the '<em><b>Is Prefix Notation Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Prefix Notation Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Prefix Notation Q</em>' containment reference.
	 * @see #setIsPrefixNotationQ(IsPrefixNotationQType)
	 * @see Ada.AdaPackage#getFunctionCall_IsPrefixNotationQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='is_prefix_notation_q' namespace='##targetNamespace'"
	 * @generated
	 */
	IsPrefixNotationQType getIsPrefixNotationQ();

	/**
	 * Sets the value of the '{@link Ada.FunctionCall#getIsPrefixNotationQ <em>Is Prefix Notation Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Prefix Notation Q</em>' containment reference.
	 * @see #getIsPrefixNotationQ()
	 * @generated
	 */
	void setIsPrefixNotationQ(IsPrefixNotationQType value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getFunctionCall_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.FunctionCall#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see Ada.AdaPackage#getFunctionCall_Type()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='type' namespace='##targetNamespace'"
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link Ada.FunctionCall#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

} // FunctionCall
