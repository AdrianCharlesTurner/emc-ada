/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enumeration Type Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.EnumerationTypeDefinition#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.EnumerationTypeDefinition#getEnumerationLiteralDeclarationsQl <em>Enumeration Literal Declarations Ql</em>}</li>
 *   <li>{@link Ada.EnumerationTypeDefinition#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getEnumerationTypeDefinition()
 * @model extendedMetaData="name='Enumeration_Type_Definition' kind='elementOnly'"
 * @generated
 */
public interface EnumerationTypeDefinition extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getEnumerationTypeDefinition_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.EnumerationTypeDefinition#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Enumeration Literal Declarations Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumeration Literal Declarations Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumeration Literal Declarations Ql</em>' containment reference.
	 * @see #setEnumerationLiteralDeclarationsQl(DeclarationList)
	 * @see Ada.AdaPackage#getEnumerationTypeDefinition_EnumerationLiteralDeclarationsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='enumeration_literal_declarations_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DeclarationList getEnumerationLiteralDeclarationsQl();

	/**
	 * Sets the value of the '{@link Ada.EnumerationTypeDefinition#getEnumerationLiteralDeclarationsQl <em>Enumeration Literal Declarations Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enumeration Literal Declarations Ql</em>' containment reference.
	 * @see #getEnumerationLiteralDeclarationsQl()
	 * @generated
	 */
	void setEnumerationLiteralDeclarationsQl(DeclarationList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getEnumerationTypeDefinition_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.EnumerationTypeDefinition#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // EnumerationTypeDefinition
