package org.eclipse.epsilon.emc.ada;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Scanner;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.xmi.XMLOptions;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMLOptionsImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

public class AdaResource extends XMLResourceImpl {
	
	protected static String encoding = "UTF-8";

	public AdaResource(URI uri) {
		super(uri);
	    setEncoding(encoding);

	    getDefaultSaveOptions().put(XMLResource.OPTION_EXTENDED_META_DATA, Boolean.TRUE);
	    getDefaultLoadOptions().put(XMLResource.OPTION_EXTENDED_META_DATA, Boolean.TRUE);
	    
	    XMLOptions xmlOptions = new XMLOptionsImpl();
	    xmlOptions.setProcessSchemaLocations(false);
	    
	    getDefaultLoadOptions().put(XMLResource.OPTION_XML_OPTIONS, xmlOptions);
	}

	@Override
	public void doLoad(InputStream inputStream, Map<?, ?> options) throws IOException {
		Scanner scanner = new Scanner(inputStream);
		scanner.useDelimiter("\\A");
		// Need to transform the scanned input stream into xml with gnat2xml and then transform this into
		// a readable form with the snake to camel converter.
		byte[] xmlBytes = gnat2xml(scanner.next());
		scanner.close();
		super.doLoad(new ByteArrayInputStream(xmlBytes), options);
		

		
		
		
		
//		try {
//			XmlString xmlString = (XmlString) manager.get(new Key<XmlString>(source.getName(), XmlString.class));
//			super.doLoad(new ByteArrayInputStream(xmlString.toString().getBytes()), options);
//		} catch (CommandException e) {
//			throw new IOException(e);
//		}

	}

	@Override
	public void doSave(OutputStream outputStream, Map<?, ?> options) throws IOException {

//		ByteArrayOutputStream bos = new ByteArrayOutputStream();
//		super.doSave(bos, options);
//
//		SectionManager manager = new SectionManager(Dialect.Z);
//		Source source = new StringSource(new String(bos.toByteArray(), encoding));
//		source.setMarkup(Markup.ZML);
//		manager.put(new Key<Source>(source.getName(), Source.class), source);
//
//		try {
//			LatexString latexString = (LatexString) manager
//					.get(new Key<LatexString>(source.getName(), LatexString.class));
//			outputStream.write(latexString.toString().getBytes());
//		} catch (CommandException e) {
//			throw new IOException(e);
//		}

	}
	
	private static String getTokens(String line) {
		return line;
		
		
	}
	
	private static byte[] gnat2xml(String adaSourceContent) {
		Runtime rt = Runtime.getRuntime();
		try {
			String workspacePath = ResourcesPlugin.getWorkspace().getRoot().getLocation().toString();
			File tmpFile = new File(workspacePath + "/tmp.ada");
			tmpFile.createNewFile();
			PrintWriter out = new PrintWriter(tmpFile);
			out.write(adaSourceContent);
			out.close();

			String[] commands = {"gnat2xml", workspacePath + "/tmp.ada", "--output-dir=" + workspacePath};
			Process proc = rt.exec(commands);
			
			String xmlString = null;
			proc.waitFor();
			xmlString = new String(Files.readAllBytes(Paths.get(workspacePath + "/tmp.ada.xml")), encoding);
			

			System.out.println(xmlString);
			
			
			
			
			xmlString = xmlString.replace("<compilation_unit", "<Ada:CompilationUnit xmi:version=\"2.0\" xmlns:xmi=\"http://www.omg.org/XMI\" xmlns:Ada=\"file:/srv/home/aturner/work/sp1708/sandbox/tools/ada.xsd\"");
			xmlString = xmlString.replace("</compilation_unit", "</Ada:CompilationUnit");
			xmlString = xmlString.replace("unit_kind", "unitKind");
			xmlString = xmlString.replace("unit_class", "unitClass");
			xmlString = xmlString.replace("unit_origin", "unitOrigin");
			xmlString = xmlString.replace("unit_full_name", "unitFullName");
			xmlString = xmlString.replace("def_name", "defName");
			xmlString = xmlString.replace("source_file", "sourceFile");
			xmlString = xmlString.replace("UTF-8", "ASCII");
			
			System.out.println(xmlString);
			
			String shouldwork = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n<Ada:CompilationUnit xmi:version=\"2.0\" xsi:schemaLocation=\"file:/srv/home/aturner/work/sp1708/sandbox/tools/ada.xsd\" xmlns:xmi=\"http://www.omg.org/XMI\" xmlns:Ada=\"file:/srv/home/aturner/work/sp1708/sandbox/tools/ada.xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" unitKind=\"A_Package\" unitClass=\"A_Public_Declaration\" unitOrigin=\"An_Application_Unit\" unitFullName=\"DUKPT\" defName=\"DUKPT\" sourceFile=\"dukpt.ads\">\n</Ada:CompilationUnit>";
			
			System.out.println(shouldwork);
			
			return shouldwork.getBytes();


		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	private static void copyInputStreamToFile(InputStream in, File file) {
	    try {
	        OutputStream out = new FileOutputStream(file);
	        byte[] buf = new byte[1024];
	        int len;
	        while((len=in.read(buf))>0){
	            out.write(buf,0,len);
	        }
	        out.close();
	        in.close();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
	
	private static String toJavaMethodName(String xmlmethodName) {
		StringBuilder nameBuilder = new StringBuilder(xmlmethodName.length());
		boolean capitalizeNextChar = false;

		for (char c : xmlmethodName.toCharArray()) {
			if (c == '_') {
				capitalizeNextChar = true;
				continue;
			}
			if (capitalizeNextChar) {
				nameBuilder.append(Character.toUpperCase(c));
			} else {
				nameBuilder.append(c);
			}
			capitalizeNextChar = false;
		}
		return nameBuilder.toString();
	}

}
