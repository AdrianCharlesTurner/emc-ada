/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.CaseExpression;
import Ada.ElementList;
import Ada.ExpressionClass;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Case Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.CaseExpressionImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.CaseExpressionImpl#getCaseExpressionQ <em>Case Expression Q</em>}</li>
 *   <li>{@link Ada.impl.CaseExpressionImpl#getExpressionPathsQl <em>Expression Paths Ql</em>}</li>
 *   <li>{@link Ada.impl.CaseExpressionImpl#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.impl.CaseExpressionImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CaseExpressionImpl extends MinimalEObjectImpl.Container implements CaseExpression {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getCaseExpressionQ() <em>Case Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCaseExpressionQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass caseExpressionQ;

	/**
	 * The cached value of the '{@link #getExpressionPathsQl() <em>Expression Paths Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpressionPathsQl()
	 * @generated
	 * @ordered
	 */
	protected ElementList expressionPathsQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CaseExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getCaseExpression();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.CASE_EXPRESSION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.CASE_EXPRESSION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.CASE_EXPRESSION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.CASE_EXPRESSION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getCaseExpressionQ() {
		return caseExpressionQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCaseExpressionQ(ExpressionClass newCaseExpressionQ, NotificationChain msgs) {
		ExpressionClass oldCaseExpressionQ = caseExpressionQ;
		caseExpressionQ = newCaseExpressionQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.CASE_EXPRESSION__CASE_EXPRESSION_Q, oldCaseExpressionQ, newCaseExpressionQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCaseExpressionQ(ExpressionClass newCaseExpressionQ) {
		if (newCaseExpressionQ != caseExpressionQ) {
			NotificationChain msgs = null;
			if (caseExpressionQ != null)
				msgs = ((InternalEObject)caseExpressionQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.CASE_EXPRESSION__CASE_EXPRESSION_Q, null, msgs);
			if (newCaseExpressionQ != null)
				msgs = ((InternalEObject)newCaseExpressionQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.CASE_EXPRESSION__CASE_EXPRESSION_Q, null, msgs);
			msgs = basicSetCaseExpressionQ(newCaseExpressionQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.CASE_EXPRESSION__CASE_EXPRESSION_Q, newCaseExpressionQ, newCaseExpressionQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementList getExpressionPathsQl() {
		return expressionPathsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExpressionPathsQl(ElementList newExpressionPathsQl, NotificationChain msgs) {
		ElementList oldExpressionPathsQl = expressionPathsQl;
		expressionPathsQl = newExpressionPathsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.CASE_EXPRESSION__EXPRESSION_PATHS_QL, oldExpressionPathsQl, newExpressionPathsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpressionPathsQl(ElementList newExpressionPathsQl) {
		if (newExpressionPathsQl != expressionPathsQl) {
			NotificationChain msgs = null;
			if (expressionPathsQl != null)
				msgs = ((InternalEObject)expressionPathsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.CASE_EXPRESSION__EXPRESSION_PATHS_QL, null, msgs);
			if (newExpressionPathsQl != null)
				msgs = ((InternalEObject)newExpressionPathsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.CASE_EXPRESSION__EXPRESSION_PATHS_QL, null, msgs);
			msgs = basicSetExpressionPathsQl(newExpressionPathsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.CASE_EXPRESSION__EXPRESSION_PATHS_QL, newExpressionPathsQl, newExpressionPathsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.CASE_EXPRESSION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.CASE_EXPRESSION__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.CASE_EXPRESSION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.CASE_EXPRESSION__CASE_EXPRESSION_Q:
				return basicSetCaseExpressionQ(null, msgs);
			case AdaPackage.CASE_EXPRESSION__EXPRESSION_PATHS_QL:
				return basicSetExpressionPathsQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.CASE_EXPRESSION__SLOC:
				return getSloc();
			case AdaPackage.CASE_EXPRESSION__CASE_EXPRESSION_Q:
				return getCaseExpressionQ();
			case AdaPackage.CASE_EXPRESSION__EXPRESSION_PATHS_QL:
				return getExpressionPathsQl();
			case AdaPackage.CASE_EXPRESSION__CHECKS:
				return getChecks();
			case AdaPackage.CASE_EXPRESSION__TYPE:
				return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.CASE_EXPRESSION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.CASE_EXPRESSION__CASE_EXPRESSION_Q:
				setCaseExpressionQ((ExpressionClass)newValue);
				return;
			case AdaPackage.CASE_EXPRESSION__EXPRESSION_PATHS_QL:
				setExpressionPathsQl((ElementList)newValue);
				return;
			case AdaPackage.CASE_EXPRESSION__CHECKS:
				setChecks((String)newValue);
				return;
			case AdaPackage.CASE_EXPRESSION__TYPE:
				setType((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.CASE_EXPRESSION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.CASE_EXPRESSION__CASE_EXPRESSION_Q:
				setCaseExpressionQ((ExpressionClass)null);
				return;
			case AdaPackage.CASE_EXPRESSION__EXPRESSION_PATHS_QL:
				setExpressionPathsQl((ElementList)null);
				return;
			case AdaPackage.CASE_EXPRESSION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
			case AdaPackage.CASE_EXPRESSION__TYPE:
				setType(TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.CASE_EXPRESSION__SLOC:
				return sloc != null;
			case AdaPackage.CASE_EXPRESSION__CASE_EXPRESSION_Q:
				return caseExpressionQ != null;
			case AdaPackage.CASE_EXPRESSION__EXPRESSION_PATHS_QL:
				return expressionPathsQl != null;
			case AdaPackage.CASE_EXPRESSION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
			case AdaPackage.CASE_EXPRESSION__TYPE:
				return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(", type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //CaseExpressionImpl
