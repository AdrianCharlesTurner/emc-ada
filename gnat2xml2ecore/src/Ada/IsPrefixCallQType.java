/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Is Prefix Call QType</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.IsPrefixCallQType#getIsPrefixCall <em>Is Prefix Call</em>}</li>
 *   <li>{@link Ada.IsPrefixCallQType#getNotAnElement <em>Not An Element</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getIsPrefixCallQType()
 * @model extendedMetaData="name='is_prefix_call_q_._type' kind='elementOnly'"
 * @generated
 */
public interface IsPrefixCallQType extends EObject {
	/**
	 * Returns the value of the '<em><b>Is Prefix Call</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Prefix Call</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Prefix Call</em>' containment reference.
	 * @see #setIsPrefixCall(IsPrefixCall)
	 * @see Ada.AdaPackage#getIsPrefixCallQType_IsPrefixCall()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='is_prefix_call' namespace='##targetNamespace'"
	 * @generated
	 */
	IsPrefixCall getIsPrefixCall();

	/**
	 * Sets the value of the '{@link Ada.IsPrefixCallQType#getIsPrefixCall <em>Is Prefix Call</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Prefix Call</em>' containment reference.
	 * @see #getIsPrefixCall()
	 * @generated
	 */
	void setIsPrefixCall(IsPrefixCall value);

	/**
	 * Returns the value of the '<em><b>Not An Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not An Element</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not An Element</em>' containment reference.
	 * @see #setNotAnElement(NotAnElement)
	 * @see Ada.AdaPackage#getIsPrefixCallQType_NotAnElement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not_an_element' namespace='##targetNamespace'"
	 * @generated
	 */
	NotAnElement getNotAnElement();

	/**
	 * Sets the value of the '{@link Ada.IsPrefixCallQType#getNotAnElement <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not An Element</em>' containment reference.
	 * @see #getNotAnElement()
	 * @generated
	 */
	void setNotAnElement(NotAnElement value);

} // IsPrefixCallQType
