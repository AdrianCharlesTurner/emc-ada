/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Discrete Simple Expression Range</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.DiscreteSimpleExpressionRange#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.DiscreteSimpleExpressionRange#getLowerBoundQ <em>Lower Bound Q</em>}</li>
 *   <li>{@link Ada.DiscreteSimpleExpressionRange#getUpperBoundQ <em>Upper Bound Q</em>}</li>
 *   <li>{@link Ada.DiscreteSimpleExpressionRange#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getDiscreteSimpleExpressionRange()
 * @model extendedMetaData="name='Discrete_Simple_Expression_Range' kind='elementOnly'"
 * @generated
 */
public interface DiscreteSimpleExpressionRange extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getDiscreteSimpleExpressionRange_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.DiscreteSimpleExpressionRange#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Lower Bound Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lower Bound Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lower Bound Q</em>' containment reference.
	 * @see #setLowerBoundQ(ExpressionClass)
	 * @see Ada.AdaPackage#getDiscreteSimpleExpressionRange_LowerBoundQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='lower_bound_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getLowerBoundQ();

	/**
	 * Sets the value of the '{@link Ada.DiscreteSimpleExpressionRange#getLowerBoundQ <em>Lower Bound Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lower Bound Q</em>' containment reference.
	 * @see #getLowerBoundQ()
	 * @generated
	 */
	void setLowerBoundQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Upper Bound Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Upper Bound Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Upper Bound Q</em>' containment reference.
	 * @see #setUpperBoundQ(ExpressionClass)
	 * @see Ada.AdaPackage#getDiscreteSimpleExpressionRange_UpperBoundQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='upper_bound_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getUpperBoundQ();

	/**
	 * Sets the value of the '{@link Ada.DiscreteSimpleExpressionRange#getUpperBoundQ <em>Upper Bound Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Upper Bound Q</em>' containment reference.
	 * @see #getUpperBoundQ()
	 * @generated
	 */
	void setUpperBoundQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getDiscreteSimpleExpressionRange_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.DiscreteSimpleExpressionRange#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // DiscreteSimpleExpressionRange
