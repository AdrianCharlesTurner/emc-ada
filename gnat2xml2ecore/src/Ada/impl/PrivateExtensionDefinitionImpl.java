/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.ElementClass;
import Ada.ExpressionList;
import Ada.HasAbstractQType5;
import Ada.HasLimitedQType2;
import Ada.HasSynchronizedQType1;
import Ada.PrivateExtensionDefinition;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Private Extension Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.PrivateExtensionDefinitionImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.PrivateExtensionDefinitionImpl#getHasAbstractQ <em>Has Abstract Q</em>}</li>
 *   <li>{@link Ada.impl.PrivateExtensionDefinitionImpl#getHasLimitedQ <em>Has Limited Q</em>}</li>
 *   <li>{@link Ada.impl.PrivateExtensionDefinitionImpl#getHasSynchronizedQ <em>Has Synchronized Q</em>}</li>
 *   <li>{@link Ada.impl.PrivateExtensionDefinitionImpl#getAncestorSubtypeIndicationQ <em>Ancestor Subtype Indication Q</em>}</li>
 *   <li>{@link Ada.impl.PrivateExtensionDefinitionImpl#getDefinitionInterfaceListQl <em>Definition Interface List Ql</em>}</li>
 *   <li>{@link Ada.impl.PrivateExtensionDefinitionImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PrivateExtensionDefinitionImpl extends MinimalEObjectImpl.Container implements PrivateExtensionDefinition {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getHasAbstractQ() <em>Has Abstract Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasAbstractQ()
	 * @generated
	 * @ordered
	 */
	protected HasAbstractQType5 hasAbstractQ;

	/**
	 * The cached value of the '{@link #getHasLimitedQ() <em>Has Limited Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasLimitedQ()
	 * @generated
	 * @ordered
	 */
	protected HasLimitedQType2 hasLimitedQ;

	/**
	 * The cached value of the '{@link #getHasSynchronizedQ() <em>Has Synchronized Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasSynchronizedQ()
	 * @generated
	 * @ordered
	 */
	protected HasSynchronizedQType1 hasSynchronizedQ;

	/**
	 * The cached value of the '{@link #getAncestorSubtypeIndicationQ() <em>Ancestor Subtype Indication Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAncestorSubtypeIndicationQ()
	 * @generated
	 * @ordered
	 */
	protected ElementClass ancestorSubtypeIndicationQ;

	/**
	 * The cached value of the '{@link #getDefinitionInterfaceListQl() <em>Definition Interface List Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefinitionInterfaceListQl()
	 * @generated
	 * @ordered
	 */
	protected ExpressionList definitionInterfaceListQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PrivateExtensionDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getPrivateExtensionDefinition();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.PRIVATE_EXTENSION_DEFINITION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIVATE_EXTENSION_DEFINITION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIVATE_EXTENSION_DEFINITION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PRIVATE_EXTENSION_DEFINITION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasAbstractQType5 getHasAbstractQ() {
		return hasAbstractQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasAbstractQ(HasAbstractQType5 newHasAbstractQ, NotificationChain msgs) {
		HasAbstractQType5 oldHasAbstractQ = hasAbstractQ;
		hasAbstractQ = newHasAbstractQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_ABSTRACT_Q, oldHasAbstractQ, newHasAbstractQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasAbstractQ(HasAbstractQType5 newHasAbstractQ) {
		if (newHasAbstractQ != hasAbstractQ) {
			NotificationChain msgs = null;
			if (hasAbstractQ != null)
				msgs = ((InternalEObject)hasAbstractQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_ABSTRACT_Q, null, msgs);
			if (newHasAbstractQ != null)
				msgs = ((InternalEObject)newHasAbstractQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_ABSTRACT_Q, null, msgs);
			msgs = basicSetHasAbstractQ(newHasAbstractQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_ABSTRACT_Q, newHasAbstractQ, newHasAbstractQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasLimitedQType2 getHasLimitedQ() {
		return hasLimitedQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasLimitedQ(HasLimitedQType2 newHasLimitedQ, NotificationChain msgs) {
		HasLimitedQType2 oldHasLimitedQ = hasLimitedQ;
		hasLimitedQ = newHasLimitedQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_LIMITED_Q, oldHasLimitedQ, newHasLimitedQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasLimitedQ(HasLimitedQType2 newHasLimitedQ) {
		if (newHasLimitedQ != hasLimitedQ) {
			NotificationChain msgs = null;
			if (hasLimitedQ != null)
				msgs = ((InternalEObject)hasLimitedQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_LIMITED_Q, null, msgs);
			if (newHasLimitedQ != null)
				msgs = ((InternalEObject)newHasLimitedQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_LIMITED_Q, null, msgs);
			msgs = basicSetHasLimitedQ(newHasLimitedQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_LIMITED_Q, newHasLimitedQ, newHasLimitedQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasSynchronizedQType1 getHasSynchronizedQ() {
		return hasSynchronizedQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasSynchronizedQ(HasSynchronizedQType1 newHasSynchronizedQ, NotificationChain msgs) {
		HasSynchronizedQType1 oldHasSynchronizedQ = hasSynchronizedQ;
		hasSynchronizedQ = newHasSynchronizedQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_SYNCHRONIZED_Q, oldHasSynchronizedQ, newHasSynchronizedQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasSynchronizedQ(HasSynchronizedQType1 newHasSynchronizedQ) {
		if (newHasSynchronizedQ != hasSynchronizedQ) {
			NotificationChain msgs = null;
			if (hasSynchronizedQ != null)
				msgs = ((InternalEObject)hasSynchronizedQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_SYNCHRONIZED_Q, null, msgs);
			if (newHasSynchronizedQ != null)
				msgs = ((InternalEObject)newHasSynchronizedQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_SYNCHRONIZED_Q, null, msgs);
			msgs = basicSetHasSynchronizedQ(newHasSynchronizedQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_SYNCHRONIZED_Q, newHasSynchronizedQ, newHasSynchronizedQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementClass getAncestorSubtypeIndicationQ() {
		return ancestorSubtypeIndicationQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAncestorSubtypeIndicationQ(ElementClass newAncestorSubtypeIndicationQ, NotificationChain msgs) {
		ElementClass oldAncestorSubtypeIndicationQ = ancestorSubtypeIndicationQ;
		ancestorSubtypeIndicationQ = newAncestorSubtypeIndicationQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.PRIVATE_EXTENSION_DEFINITION__ANCESTOR_SUBTYPE_INDICATION_Q, oldAncestorSubtypeIndicationQ, newAncestorSubtypeIndicationQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAncestorSubtypeIndicationQ(ElementClass newAncestorSubtypeIndicationQ) {
		if (newAncestorSubtypeIndicationQ != ancestorSubtypeIndicationQ) {
			NotificationChain msgs = null;
			if (ancestorSubtypeIndicationQ != null)
				msgs = ((InternalEObject)ancestorSubtypeIndicationQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIVATE_EXTENSION_DEFINITION__ANCESTOR_SUBTYPE_INDICATION_Q, null, msgs);
			if (newAncestorSubtypeIndicationQ != null)
				msgs = ((InternalEObject)newAncestorSubtypeIndicationQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIVATE_EXTENSION_DEFINITION__ANCESTOR_SUBTYPE_INDICATION_Q, null, msgs);
			msgs = basicSetAncestorSubtypeIndicationQ(newAncestorSubtypeIndicationQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PRIVATE_EXTENSION_DEFINITION__ANCESTOR_SUBTYPE_INDICATION_Q, newAncestorSubtypeIndicationQ, newAncestorSubtypeIndicationQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionList getDefinitionInterfaceListQl() {
		return definitionInterfaceListQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefinitionInterfaceListQl(ExpressionList newDefinitionInterfaceListQl, NotificationChain msgs) {
		ExpressionList oldDefinitionInterfaceListQl = definitionInterfaceListQl;
		definitionInterfaceListQl = newDefinitionInterfaceListQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.PRIVATE_EXTENSION_DEFINITION__DEFINITION_INTERFACE_LIST_QL, oldDefinitionInterfaceListQl, newDefinitionInterfaceListQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefinitionInterfaceListQl(ExpressionList newDefinitionInterfaceListQl) {
		if (newDefinitionInterfaceListQl != definitionInterfaceListQl) {
			NotificationChain msgs = null;
			if (definitionInterfaceListQl != null)
				msgs = ((InternalEObject)definitionInterfaceListQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIVATE_EXTENSION_DEFINITION__DEFINITION_INTERFACE_LIST_QL, null, msgs);
			if (newDefinitionInterfaceListQl != null)
				msgs = ((InternalEObject)newDefinitionInterfaceListQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRIVATE_EXTENSION_DEFINITION__DEFINITION_INTERFACE_LIST_QL, null, msgs);
			msgs = basicSetDefinitionInterfaceListQl(newDefinitionInterfaceListQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PRIVATE_EXTENSION_DEFINITION__DEFINITION_INTERFACE_LIST_QL, newDefinitionInterfaceListQl, newDefinitionInterfaceListQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PRIVATE_EXTENSION_DEFINITION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_ABSTRACT_Q:
				return basicSetHasAbstractQ(null, msgs);
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_LIMITED_Q:
				return basicSetHasLimitedQ(null, msgs);
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_SYNCHRONIZED_Q:
				return basicSetHasSynchronizedQ(null, msgs);
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__ANCESTOR_SUBTYPE_INDICATION_Q:
				return basicSetAncestorSubtypeIndicationQ(null, msgs);
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__DEFINITION_INTERFACE_LIST_QL:
				return basicSetDefinitionInterfaceListQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__SLOC:
				return getSloc();
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_ABSTRACT_Q:
				return getHasAbstractQ();
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_LIMITED_Q:
				return getHasLimitedQ();
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_SYNCHRONIZED_Q:
				return getHasSynchronizedQ();
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__ANCESTOR_SUBTYPE_INDICATION_Q:
				return getAncestorSubtypeIndicationQ();
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__DEFINITION_INTERFACE_LIST_QL:
				return getDefinitionInterfaceListQl();
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_ABSTRACT_Q:
				setHasAbstractQ((HasAbstractQType5)newValue);
				return;
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_LIMITED_Q:
				setHasLimitedQ((HasLimitedQType2)newValue);
				return;
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_SYNCHRONIZED_Q:
				setHasSynchronizedQ((HasSynchronizedQType1)newValue);
				return;
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__ANCESTOR_SUBTYPE_INDICATION_Q:
				setAncestorSubtypeIndicationQ((ElementClass)newValue);
				return;
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__DEFINITION_INTERFACE_LIST_QL:
				setDefinitionInterfaceListQl((ExpressionList)newValue);
				return;
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_ABSTRACT_Q:
				setHasAbstractQ((HasAbstractQType5)null);
				return;
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_LIMITED_Q:
				setHasLimitedQ((HasLimitedQType2)null);
				return;
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_SYNCHRONIZED_Q:
				setHasSynchronizedQ((HasSynchronizedQType1)null);
				return;
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__ANCESTOR_SUBTYPE_INDICATION_Q:
				setAncestorSubtypeIndicationQ((ElementClass)null);
				return;
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__DEFINITION_INTERFACE_LIST_QL:
				setDefinitionInterfaceListQl((ExpressionList)null);
				return;
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__SLOC:
				return sloc != null;
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_ABSTRACT_Q:
				return hasAbstractQ != null;
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_LIMITED_Q:
				return hasLimitedQ != null;
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__HAS_SYNCHRONIZED_Q:
				return hasSynchronizedQ != null;
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__ANCESTOR_SUBTYPE_INDICATION_Q:
				return ancestorSubtypeIndicationQ != null;
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__DEFINITION_INTERFACE_LIST_QL:
				return definitionInterfaceListQl != null;
			case AdaPackage.PRIVATE_EXTENSION_DEFINITION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //PrivateExtensionDefinitionImpl
