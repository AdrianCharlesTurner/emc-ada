/**
 */
package Ada.impl;

import Ada.AcceptStatement;
import Ada.AdaPackage;
import Ada.DefiningNameList;
import Ada.ExpressionClass;
import Ada.NameClass;
import Ada.ParameterSpecificationList;
import Ada.SourceLocation;
import Ada.StatementList;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Accept Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.AcceptStatementImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.AcceptStatementImpl#getLabelNamesQl <em>Label Names Ql</em>}</li>
 *   <li>{@link Ada.impl.AcceptStatementImpl#getAcceptEntryDirectNameQ <em>Accept Entry Direct Name Q</em>}</li>
 *   <li>{@link Ada.impl.AcceptStatementImpl#getAcceptEntryIndexQ <em>Accept Entry Index Q</em>}</li>
 *   <li>{@link Ada.impl.AcceptStatementImpl#getAcceptParametersQl <em>Accept Parameters Ql</em>}</li>
 *   <li>{@link Ada.impl.AcceptStatementImpl#getAcceptBodyStatementsQl <em>Accept Body Statements Ql</em>}</li>
 *   <li>{@link Ada.impl.AcceptStatementImpl#getAcceptBodyExceptionHandlersQl <em>Accept Body Exception Handlers Ql</em>}</li>
 *   <li>{@link Ada.impl.AcceptStatementImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AcceptStatementImpl extends MinimalEObjectImpl.Container implements AcceptStatement {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getLabelNamesQl() <em>Label Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabelNamesQl()
	 * @generated
	 * @ordered
	 */
	protected DefiningNameList labelNamesQl;

	/**
	 * The cached value of the '{@link #getAcceptEntryDirectNameQ() <em>Accept Entry Direct Name Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAcceptEntryDirectNameQ()
	 * @generated
	 * @ordered
	 */
	protected NameClass acceptEntryDirectNameQ;

	/**
	 * The cached value of the '{@link #getAcceptEntryIndexQ() <em>Accept Entry Index Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAcceptEntryIndexQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass acceptEntryIndexQ;

	/**
	 * The cached value of the '{@link #getAcceptParametersQl() <em>Accept Parameters Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAcceptParametersQl()
	 * @generated
	 * @ordered
	 */
	protected ParameterSpecificationList acceptParametersQl;

	/**
	 * The cached value of the '{@link #getAcceptBodyStatementsQl() <em>Accept Body Statements Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAcceptBodyStatementsQl()
	 * @generated
	 * @ordered
	 */
	protected StatementList acceptBodyStatementsQl;

	/**
	 * The cached value of the '{@link #getAcceptBodyExceptionHandlersQl() <em>Accept Body Exception Handlers Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAcceptBodyExceptionHandlersQl()
	 * @generated
	 * @ordered
	 */
	protected StatementList acceptBodyExceptionHandlersQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AcceptStatementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getAcceptStatement();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ACCEPT_STATEMENT__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ACCEPT_STATEMENT__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ACCEPT_STATEMENT__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ACCEPT_STATEMENT__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNameList getLabelNamesQl() {
		return labelNamesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLabelNamesQl(DefiningNameList newLabelNamesQl, NotificationChain msgs) {
		DefiningNameList oldLabelNamesQl = labelNamesQl;
		labelNamesQl = newLabelNamesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ACCEPT_STATEMENT__LABEL_NAMES_QL, oldLabelNamesQl, newLabelNamesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabelNamesQl(DefiningNameList newLabelNamesQl) {
		if (newLabelNamesQl != labelNamesQl) {
			NotificationChain msgs = null;
			if (labelNamesQl != null)
				msgs = ((InternalEObject)labelNamesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ACCEPT_STATEMENT__LABEL_NAMES_QL, null, msgs);
			if (newLabelNamesQl != null)
				msgs = ((InternalEObject)newLabelNamesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ACCEPT_STATEMENT__LABEL_NAMES_QL, null, msgs);
			msgs = basicSetLabelNamesQl(newLabelNamesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ACCEPT_STATEMENT__LABEL_NAMES_QL, newLabelNamesQl, newLabelNamesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NameClass getAcceptEntryDirectNameQ() {
		return acceptEntryDirectNameQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcceptEntryDirectNameQ(NameClass newAcceptEntryDirectNameQ, NotificationChain msgs) {
		NameClass oldAcceptEntryDirectNameQ = acceptEntryDirectNameQ;
		acceptEntryDirectNameQ = newAcceptEntryDirectNameQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ACCEPT_STATEMENT__ACCEPT_ENTRY_DIRECT_NAME_Q, oldAcceptEntryDirectNameQ, newAcceptEntryDirectNameQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcceptEntryDirectNameQ(NameClass newAcceptEntryDirectNameQ) {
		if (newAcceptEntryDirectNameQ != acceptEntryDirectNameQ) {
			NotificationChain msgs = null;
			if (acceptEntryDirectNameQ != null)
				msgs = ((InternalEObject)acceptEntryDirectNameQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ACCEPT_STATEMENT__ACCEPT_ENTRY_DIRECT_NAME_Q, null, msgs);
			if (newAcceptEntryDirectNameQ != null)
				msgs = ((InternalEObject)newAcceptEntryDirectNameQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ACCEPT_STATEMENT__ACCEPT_ENTRY_DIRECT_NAME_Q, null, msgs);
			msgs = basicSetAcceptEntryDirectNameQ(newAcceptEntryDirectNameQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ACCEPT_STATEMENT__ACCEPT_ENTRY_DIRECT_NAME_Q, newAcceptEntryDirectNameQ, newAcceptEntryDirectNameQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getAcceptEntryIndexQ() {
		return acceptEntryIndexQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcceptEntryIndexQ(ExpressionClass newAcceptEntryIndexQ, NotificationChain msgs) {
		ExpressionClass oldAcceptEntryIndexQ = acceptEntryIndexQ;
		acceptEntryIndexQ = newAcceptEntryIndexQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ACCEPT_STATEMENT__ACCEPT_ENTRY_INDEX_Q, oldAcceptEntryIndexQ, newAcceptEntryIndexQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcceptEntryIndexQ(ExpressionClass newAcceptEntryIndexQ) {
		if (newAcceptEntryIndexQ != acceptEntryIndexQ) {
			NotificationChain msgs = null;
			if (acceptEntryIndexQ != null)
				msgs = ((InternalEObject)acceptEntryIndexQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ACCEPT_STATEMENT__ACCEPT_ENTRY_INDEX_Q, null, msgs);
			if (newAcceptEntryIndexQ != null)
				msgs = ((InternalEObject)newAcceptEntryIndexQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ACCEPT_STATEMENT__ACCEPT_ENTRY_INDEX_Q, null, msgs);
			msgs = basicSetAcceptEntryIndexQ(newAcceptEntryIndexQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ACCEPT_STATEMENT__ACCEPT_ENTRY_INDEX_Q, newAcceptEntryIndexQ, newAcceptEntryIndexQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterSpecificationList getAcceptParametersQl() {
		return acceptParametersQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcceptParametersQl(ParameterSpecificationList newAcceptParametersQl, NotificationChain msgs) {
		ParameterSpecificationList oldAcceptParametersQl = acceptParametersQl;
		acceptParametersQl = newAcceptParametersQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ACCEPT_STATEMENT__ACCEPT_PARAMETERS_QL, oldAcceptParametersQl, newAcceptParametersQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcceptParametersQl(ParameterSpecificationList newAcceptParametersQl) {
		if (newAcceptParametersQl != acceptParametersQl) {
			NotificationChain msgs = null;
			if (acceptParametersQl != null)
				msgs = ((InternalEObject)acceptParametersQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ACCEPT_STATEMENT__ACCEPT_PARAMETERS_QL, null, msgs);
			if (newAcceptParametersQl != null)
				msgs = ((InternalEObject)newAcceptParametersQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ACCEPT_STATEMENT__ACCEPT_PARAMETERS_QL, null, msgs);
			msgs = basicSetAcceptParametersQl(newAcceptParametersQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ACCEPT_STATEMENT__ACCEPT_PARAMETERS_QL, newAcceptParametersQl, newAcceptParametersQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatementList getAcceptBodyStatementsQl() {
		return acceptBodyStatementsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcceptBodyStatementsQl(StatementList newAcceptBodyStatementsQl, NotificationChain msgs) {
		StatementList oldAcceptBodyStatementsQl = acceptBodyStatementsQl;
		acceptBodyStatementsQl = newAcceptBodyStatementsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ACCEPT_STATEMENT__ACCEPT_BODY_STATEMENTS_QL, oldAcceptBodyStatementsQl, newAcceptBodyStatementsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcceptBodyStatementsQl(StatementList newAcceptBodyStatementsQl) {
		if (newAcceptBodyStatementsQl != acceptBodyStatementsQl) {
			NotificationChain msgs = null;
			if (acceptBodyStatementsQl != null)
				msgs = ((InternalEObject)acceptBodyStatementsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ACCEPT_STATEMENT__ACCEPT_BODY_STATEMENTS_QL, null, msgs);
			if (newAcceptBodyStatementsQl != null)
				msgs = ((InternalEObject)newAcceptBodyStatementsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ACCEPT_STATEMENT__ACCEPT_BODY_STATEMENTS_QL, null, msgs);
			msgs = basicSetAcceptBodyStatementsQl(newAcceptBodyStatementsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ACCEPT_STATEMENT__ACCEPT_BODY_STATEMENTS_QL, newAcceptBodyStatementsQl, newAcceptBodyStatementsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatementList getAcceptBodyExceptionHandlersQl() {
		return acceptBodyExceptionHandlersQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAcceptBodyExceptionHandlersQl(StatementList newAcceptBodyExceptionHandlersQl, NotificationChain msgs) {
		StatementList oldAcceptBodyExceptionHandlersQl = acceptBodyExceptionHandlersQl;
		acceptBodyExceptionHandlersQl = newAcceptBodyExceptionHandlersQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ACCEPT_STATEMENT__ACCEPT_BODY_EXCEPTION_HANDLERS_QL, oldAcceptBodyExceptionHandlersQl, newAcceptBodyExceptionHandlersQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAcceptBodyExceptionHandlersQl(StatementList newAcceptBodyExceptionHandlersQl) {
		if (newAcceptBodyExceptionHandlersQl != acceptBodyExceptionHandlersQl) {
			NotificationChain msgs = null;
			if (acceptBodyExceptionHandlersQl != null)
				msgs = ((InternalEObject)acceptBodyExceptionHandlersQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ACCEPT_STATEMENT__ACCEPT_BODY_EXCEPTION_HANDLERS_QL, null, msgs);
			if (newAcceptBodyExceptionHandlersQl != null)
				msgs = ((InternalEObject)newAcceptBodyExceptionHandlersQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ACCEPT_STATEMENT__ACCEPT_BODY_EXCEPTION_HANDLERS_QL, null, msgs);
			msgs = basicSetAcceptBodyExceptionHandlersQl(newAcceptBodyExceptionHandlersQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ACCEPT_STATEMENT__ACCEPT_BODY_EXCEPTION_HANDLERS_QL, newAcceptBodyExceptionHandlersQl, newAcceptBodyExceptionHandlersQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ACCEPT_STATEMENT__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.ACCEPT_STATEMENT__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.ACCEPT_STATEMENT__LABEL_NAMES_QL:
				return basicSetLabelNamesQl(null, msgs);
			case AdaPackage.ACCEPT_STATEMENT__ACCEPT_ENTRY_DIRECT_NAME_Q:
				return basicSetAcceptEntryDirectNameQ(null, msgs);
			case AdaPackage.ACCEPT_STATEMENT__ACCEPT_ENTRY_INDEX_Q:
				return basicSetAcceptEntryIndexQ(null, msgs);
			case AdaPackage.ACCEPT_STATEMENT__ACCEPT_PARAMETERS_QL:
				return basicSetAcceptParametersQl(null, msgs);
			case AdaPackage.ACCEPT_STATEMENT__ACCEPT_BODY_STATEMENTS_QL:
				return basicSetAcceptBodyStatementsQl(null, msgs);
			case AdaPackage.ACCEPT_STATEMENT__ACCEPT_BODY_EXCEPTION_HANDLERS_QL:
				return basicSetAcceptBodyExceptionHandlersQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.ACCEPT_STATEMENT__SLOC:
				return getSloc();
			case AdaPackage.ACCEPT_STATEMENT__LABEL_NAMES_QL:
				return getLabelNamesQl();
			case AdaPackage.ACCEPT_STATEMENT__ACCEPT_ENTRY_DIRECT_NAME_Q:
				return getAcceptEntryDirectNameQ();
			case AdaPackage.ACCEPT_STATEMENT__ACCEPT_ENTRY_INDEX_Q:
				return getAcceptEntryIndexQ();
			case AdaPackage.ACCEPT_STATEMENT__ACCEPT_PARAMETERS_QL:
				return getAcceptParametersQl();
			case AdaPackage.ACCEPT_STATEMENT__ACCEPT_BODY_STATEMENTS_QL:
				return getAcceptBodyStatementsQl();
			case AdaPackage.ACCEPT_STATEMENT__ACCEPT_BODY_EXCEPTION_HANDLERS_QL:
				return getAcceptBodyExceptionHandlersQl();
			case AdaPackage.ACCEPT_STATEMENT__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.ACCEPT_STATEMENT__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.ACCEPT_STATEMENT__LABEL_NAMES_QL:
				setLabelNamesQl((DefiningNameList)newValue);
				return;
			case AdaPackage.ACCEPT_STATEMENT__ACCEPT_ENTRY_DIRECT_NAME_Q:
				setAcceptEntryDirectNameQ((NameClass)newValue);
				return;
			case AdaPackage.ACCEPT_STATEMENT__ACCEPT_ENTRY_INDEX_Q:
				setAcceptEntryIndexQ((ExpressionClass)newValue);
				return;
			case AdaPackage.ACCEPT_STATEMENT__ACCEPT_PARAMETERS_QL:
				setAcceptParametersQl((ParameterSpecificationList)newValue);
				return;
			case AdaPackage.ACCEPT_STATEMENT__ACCEPT_BODY_STATEMENTS_QL:
				setAcceptBodyStatementsQl((StatementList)newValue);
				return;
			case AdaPackage.ACCEPT_STATEMENT__ACCEPT_BODY_EXCEPTION_HANDLERS_QL:
				setAcceptBodyExceptionHandlersQl((StatementList)newValue);
				return;
			case AdaPackage.ACCEPT_STATEMENT__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.ACCEPT_STATEMENT__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.ACCEPT_STATEMENT__LABEL_NAMES_QL:
				setLabelNamesQl((DefiningNameList)null);
				return;
			case AdaPackage.ACCEPT_STATEMENT__ACCEPT_ENTRY_DIRECT_NAME_Q:
				setAcceptEntryDirectNameQ((NameClass)null);
				return;
			case AdaPackage.ACCEPT_STATEMENT__ACCEPT_ENTRY_INDEX_Q:
				setAcceptEntryIndexQ((ExpressionClass)null);
				return;
			case AdaPackage.ACCEPT_STATEMENT__ACCEPT_PARAMETERS_QL:
				setAcceptParametersQl((ParameterSpecificationList)null);
				return;
			case AdaPackage.ACCEPT_STATEMENT__ACCEPT_BODY_STATEMENTS_QL:
				setAcceptBodyStatementsQl((StatementList)null);
				return;
			case AdaPackage.ACCEPT_STATEMENT__ACCEPT_BODY_EXCEPTION_HANDLERS_QL:
				setAcceptBodyExceptionHandlersQl((StatementList)null);
				return;
			case AdaPackage.ACCEPT_STATEMENT__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.ACCEPT_STATEMENT__SLOC:
				return sloc != null;
			case AdaPackage.ACCEPT_STATEMENT__LABEL_NAMES_QL:
				return labelNamesQl != null;
			case AdaPackage.ACCEPT_STATEMENT__ACCEPT_ENTRY_DIRECT_NAME_Q:
				return acceptEntryDirectNameQ != null;
			case AdaPackage.ACCEPT_STATEMENT__ACCEPT_ENTRY_INDEX_Q:
				return acceptEntryIndexQ != null;
			case AdaPackage.ACCEPT_STATEMENT__ACCEPT_PARAMETERS_QL:
				return acceptParametersQl != null;
			case AdaPackage.ACCEPT_STATEMENT__ACCEPT_BODY_STATEMENTS_QL:
				return acceptBodyStatementsQl != null;
			case AdaPackage.ACCEPT_STATEMENT__ACCEPT_BODY_EXCEPTION_HANDLERS_QL:
				return acceptBodyExceptionHandlersQl != null;
			case AdaPackage.ACCEPT_STATEMENT__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //AcceptStatementImpl
