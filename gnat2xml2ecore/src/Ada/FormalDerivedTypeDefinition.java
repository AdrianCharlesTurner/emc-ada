/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Formal Derived Type Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.FormalDerivedTypeDefinition#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.FormalDerivedTypeDefinition#getHasAbstractQ <em>Has Abstract Q</em>}</li>
 *   <li>{@link Ada.FormalDerivedTypeDefinition#getHasLimitedQ <em>Has Limited Q</em>}</li>
 *   <li>{@link Ada.FormalDerivedTypeDefinition#getHasSynchronizedQ <em>Has Synchronized Q</em>}</li>
 *   <li>{@link Ada.FormalDerivedTypeDefinition#getSubtypeMarkQ <em>Subtype Mark Q</em>}</li>
 *   <li>{@link Ada.FormalDerivedTypeDefinition#getDefinitionInterfaceListQl <em>Definition Interface List Ql</em>}</li>
 *   <li>{@link Ada.FormalDerivedTypeDefinition#getHasPrivateQ <em>Has Private Q</em>}</li>
 *   <li>{@link Ada.FormalDerivedTypeDefinition#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getFormalDerivedTypeDefinition()
 * @model extendedMetaData="name='Formal_Derived_Type_Definition' kind='elementOnly'"
 * @generated
 */
public interface FormalDerivedTypeDefinition extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getFormalDerivedTypeDefinition_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.FormalDerivedTypeDefinition#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Has Abstract Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Abstract Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Abstract Q</em>' containment reference.
	 * @see #setHasAbstractQ(HasAbstractQType7)
	 * @see Ada.AdaPackage#getFormalDerivedTypeDefinition_HasAbstractQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='has_abstract_q' namespace='##targetNamespace'"
	 * @generated
	 */
	HasAbstractQType7 getHasAbstractQ();

	/**
	 * Sets the value of the '{@link Ada.FormalDerivedTypeDefinition#getHasAbstractQ <em>Has Abstract Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Abstract Q</em>' containment reference.
	 * @see #getHasAbstractQ()
	 * @generated
	 */
	void setHasAbstractQ(HasAbstractQType7 value);

	/**
	 * Returns the value of the '<em><b>Has Limited Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Limited Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Limited Q</em>' containment reference.
	 * @see #setHasLimitedQ(HasLimitedQType9)
	 * @see Ada.AdaPackage#getFormalDerivedTypeDefinition_HasLimitedQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='has_limited_q' namespace='##targetNamespace'"
	 * @generated
	 */
	HasLimitedQType9 getHasLimitedQ();

	/**
	 * Sets the value of the '{@link Ada.FormalDerivedTypeDefinition#getHasLimitedQ <em>Has Limited Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Limited Q</em>' containment reference.
	 * @see #getHasLimitedQ()
	 * @generated
	 */
	void setHasLimitedQ(HasLimitedQType9 value);

	/**
	 * Returns the value of the '<em><b>Has Synchronized Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Synchronized Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Synchronized Q</em>' containment reference.
	 * @see #setHasSynchronizedQ(HasSynchronizedQType)
	 * @see Ada.AdaPackage#getFormalDerivedTypeDefinition_HasSynchronizedQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='has_synchronized_q' namespace='##targetNamespace'"
	 * @generated
	 */
	HasSynchronizedQType getHasSynchronizedQ();

	/**
	 * Sets the value of the '{@link Ada.FormalDerivedTypeDefinition#getHasSynchronizedQ <em>Has Synchronized Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Synchronized Q</em>' containment reference.
	 * @see #getHasSynchronizedQ()
	 * @generated
	 */
	void setHasSynchronizedQ(HasSynchronizedQType value);

	/**
	 * Returns the value of the '<em><b>Subtype Mark Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subtype Mark Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subtype Mark Q</em>' containment reference.
	 * @see #setSubtypeMarkQ(ExpressionClass)
	 * @see Ada.AdaPackage#getFormalDerivedTypeDefinition_SubtypeMarkQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='subtype_mark_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getSubtypeMarkQ();

	/**
	 * Sets the value of the '{@link Ada.FormalDerivedTypeDefinition#getSubtypeMarkQ <em>Subtype Mark Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Subtype Mark Q</em>' containment reference.
	 * @see #getSubtypeMarkQ()
	 * @generated
	 */
	void setSubtypeMarkQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Definition Interface List Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Definition Interface List Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definition Interface List Ql</em>' containment reference.
	 * @see #setDefinitionInterfaceListQl(ExpressionList)
	 * @see Ada.AdaPackage#getFormalDerivedTypeDefinition_DefinitionInterfaceListQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='definition_interface_list_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionList getDefinitionInterfaceListQl();

	/**
	 * Sets the value of the '{@link Ada.FormalDerivedTypeDefinition#getDefinitionInterfaceListQl <em>Definition Interface List Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Definition Interface List Ql</em>' containment reference.
	 * @see #getDefinitionInterfaceListQl()
	 * @generated
	 */
	void setDefinitionInterfaceListQl(ExpressionList value);

	/**
	 * Returns the value of the '<em><b>Has Private Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Private Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Private Q</em>' containment reference.
	 * @see #setHasPrivateQ(HasPrivateQType)
	 * @see Ada.AdaPackage#getFormalDerivedTypeDefinition_HasPrivateQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='has_private_q' namespace='##targetNamespace'"
	 * @generated
	 */
	HasPrivateQType getHasPrivateQ();

	/**
	 * Sets the value of the '{@link Ada.FormalDerivedTypeDefinition#getHasPrivateQ <em>Has Private Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Private Q</em>' containment reference.
	 * @see #getHasPrivateQ()
	 * @generated
	 */
	void setHasPrivateQ(HasPrivateQType value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getFormalDerivedTypeDefinition_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.FormalDerivedTypeDefinition#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // FormalDerivedTypeDefinition
