/**
 */
package Ada;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Element List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.ElementList#getGroup <em>Group</em>}</li>
 *   <li>{@link Ada.ElementList#getNotAnElement <em>Not An Element</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiningIdentifier <em>Defining Identifier</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiningCharacterLiteral <em>Defining Character Literal</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiningEnumerationLiteral <em>Defining Enumeration Literal</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiningAndOperator <em>Defining And Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiningOrOperator <em>Defining Or Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiningXorOperator <em>Defining Xor Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiningEqualOperator <em>Defining Equal Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiningNotEqualOperator <em>Defining Not Equal Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiningLessThanOperator <em>Defining Less Than Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiningLessThanOrEqualOperator <em>Defining Less Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiningGreaterThanOperator <em>Defining Greater Than Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiningGreaterThanOrEqualOperator <em>Defining Greater Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiningPlusOperator <em>Defining Plus Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiningMinusOperator <em>Defining Minus Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiningConcatenateOperator <em>Defining Concatenate Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiningUnaryPlusOperator <em>Defining Unary Plus Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiningUnaryMinusOperator <em>Defining Unary Minus Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiningMultiplyOperator <em>Defining Multiply Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiningDivideOperator <em>Defining Divide Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiningModOperator <em>Defining Mod Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiningRemOperator <em>Defining Rem Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiningExponentiateOperator <em>Defining Exponentiate Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiningAbsOperator <em>Defining Abs Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiningNotOperator <em>Defining Not Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiningExpandedName <em>Defining Expanded Name</em>}</li>
 *   <li>{@link Ada.ElementList#getOrdinaryTypeDeclaration <em>Ordinary Type Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getTaskTypeDeclaration <em>Task Type Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getProtectedTypeDeclaration <em>Protected Type Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getIncompleteTypeDeclaration <em>Incomplete Type Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getTaggedIncompleteTypeDeclaration <em>Tagged Incomplete Type Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getPrivateTypeDeclaration <em>Private Type Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getPrivateExtensionDeclaration <em>Private Extension Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getSubtypeDeclaration <em>Subtype Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getVariableDeclaration <em>Variable Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getConstantDeclaration <em>Constant Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getDeferredConstantDeclaration <em>Deferred Constant Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getSingleTaskDeclaration <em>Single Task Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getSingleProtectedDeclaration <em>Single Protected Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getIntegerNumberDeclaration <em>Integer Number Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getRealNumberDeclaration <em>Real Number Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getEnumerationLiteralSpecification <em>Enumeration Literal Specification</em>}</li>
 *   <li>{@link Ada.ElementList#getDiscriminantSpecification <em>Discriminant Specification</em>}</li>
 *   <li>{@link Ada.ElementList#getComponentDeclaration <em>Component Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getLoopParameterSpecification <em>Loop Parameter Specification</em>}</li>
 *   <li>{@link Ada.ElementList#getGeneralizedIteratorSpecification <em>Generalized Iterator Specification</em>}</li>
 *   <li>{@link Ada.ElementList#getElementIteratorSpecification <em>Element Iterator Specification</em>}</li>
 *   <li>{@link Ada.ElementList#getProcedureDeclaration <em>Procedure Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getFunctionDeclaration <em>Function Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getParameterSpecification <em>Parameter Specification</em>}</li>
 *   <li>{@link Ada.ElementList#getProcedureBodyDeclaration <em>Procedure Body Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getFunctionBodyDeclaration <em>Function Body Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getReturnVariableSpecification <em>Return Variable Specification</em>}</li>
 *   <li>{@link Ada.ElementList#getReturnConstantSpecification <em>Return Constant Specification</em>}</li>
 *   <li>{@link Ada.ElementList#getNullProcedureDeclaration <em>Null Procedure Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getExpressionFunctionDeclaration <em>Expression Function Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getPackageDeclaration <em>Package Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getPackageBodyDeclaration <em>Package Body Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getObjectRenamingDeclaration <em>Object Renaming Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getExceptionRenamingDeclaration <em>Exception Renaming Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getPackageRenamingDeclaration <em>Package Renaming Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getProcedureRenamingDeclaration <em>Procedure Renaming Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getFunctionRenamingDeclaration <em>Function Renaming Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getGenericPackageRenamingDeclaration <em>Generic Package Renaming Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getGenericProcedureRenamingDeclaration <em>Generic Procedure Renaming Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getGenericFunctionRenamingDeclaration <em>Generic Function Renaming Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getTaskBodyDeclaration <em>Task Body Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getProtectedBodyDeclaration <em>Protected Body Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getEntryDeclaration <em>Entry Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getEntryBodyDeclaration <em>Entry Body Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getEntryIndexSpecification <em>Entry Index Specification</em>}</li>
 *   <li>{@link Ada.ElementList#getProcedureBodyStub <em>Procedure Body Stub</em>}</li>
 *   <li>{@link Ada.ElementList#getFunctionBodyStub <em>Function Body Stub</em>}</li>
 *   <li>{@link Ada.ElementList#getPackageBodyStub <em>Package Body Stub</em>}</li>
 *   <li>{@link Ada.ElementList#getTaskBodyStub <em>Task Body Stub</em>}</li>
 *   <li>{@link Ada.ElementList#getProtectedBodyStub <em>Protected Body Stub</em>}</li>
 *   <li>{@link Ada.ElementList#getExceptionDeclaration <em>Exception Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getChoiceParameterSpecification <em>Choice Parameter Specification</em>}</li>
 *   <li>{@link Ada.ElementList#getGenericProcedureDeclaration <em>Generic Procedure Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getGenericFunctionDeclaration <em>Generic Function Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getGenericPackageDeclaration <em>Generic Package Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getPackageInstantiation <em>Package Instantiation</em>}</li>
 *   <li>{@link Ada.ElementList#getProcedureInstantiation <em>Procedure Instantiation</em>}</li>
 *   <li>{@link Ada.ElementList#getFunctionInstantiation <em>Function Instantiation</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalObjectDeclaration <em>Formal Object Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalTypeDeclaration <em>Formal Type Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalIncompleteTypeDeclaration <em>Formal Incomplete Type Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalProcedureDeclaration <em>Formal Procedure Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalFunctionDeclaration <em>Formal Function Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalPackageDeclaration <em>Formal Package Declaration</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalPackageDeclarationWithBox <em>Formal Package Declaration With Box</em>}</li>
 *   <li>{@link Ada.ElementList#getDerivedTypeDefinition <em>Derived Type Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getDerivedRecordExtensionDefinition <em>Derived Record Extension Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getEnumerationTypeDefinition <em>Enumeration Type Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getSignedIntegerTypeDefinition <em>Signed Integer Type Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getModularTypeDefinition <em>Modular Type Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getRootIntegerDefinition <em>Root Integer Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getRootRealDefinition <em>Root Real Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getUniversalIntegerDefinition <em>Universal Integer Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getUniversalRealDefinition <em>Universal Real Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getUniversalFixedDefinition <em>Universal Fixed Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getFloatingPointDefinition <em>Floating Point Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getOrdinaryFixedPointDefinition <em>Ordinary Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getDecimalFixedPointDefinition <em>Decimal Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getUnconstrainedArrayDefinition <em>Unconstrained Array Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getConstrainedArrayDefinition <em>Constrained Array Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getRecordTypeDefinition <em>Record Type Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getTaggedRecordTypeDefinition <em>Tagged Record Type Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getOrdinaryInterface <em>Ordinary Interface</em>}</li>
 *   <li>{@link Ada.ElementList#getLimitedInterface <em>Limited Interface</em>}</li>
 *   <li>{@link Ada.ElementList#getTaskInterface <em>Task Interface</em>}</li>
 *   <li>{@link Ada.ElementList#getProtectedInterface <em>Protected Interface</em>}</li>
 *   <li>{@link Ada.ElementList#getSynchronizedInterface <em>Synchronized Interface</em>}</li>
 *   <li>{@link Ada.ElementList#getPoolSpecificAccessToVariable <em>Pool Specific Access To Variable</em>}</li>
 *   <li>{@link Ada.ElementList#getAccessToVariable <em>Access To Variable</em>}</li>
 *   <li>{@link Ada.ElementList#getAccessToConstant <em>Access To Constant</em>}</li>
 *   <li>{@link Ada.ElementList#getAccessToProcedure <em>Access To Procedure</em>}</li>
 *   <li>{@link Ada.ElementList#getAccessToProtectedProcedure <em>Access To Protected Procedure</em>}</li>
 *   <li>{@link Ada.ElementList#getAccessToFunction <em>Access To Function</em>}</li>
 *   <li>{@link Ada.ElementList#getAccessToProtectedFunction <em>Access To Protected Function</em>}</li>
 *   <li>{@link Ada.ElementList#getSubtypeIndication <em>Subtype Indication</em>}</li>
 *   <li>{@link Ada.ElementList#getRangeAttributeReference <em>Range Attribute Reference</em>}</li>
 *   <li>{@link Ada.ElementList#getSimpleExpressionRange <em>Simple Expression Range</em>}</li>
 *   <li>{@link Ada.ElementList#getDigitsConstraint <em>Digits Constraint</em>}</li>
 *   <li>{@link Ada.ElementList#getDeltaConstraint <em>Delta Constraint</em>}</li>
 *   <li>{@link Ada.ElementList#getIndexConstraint <em>Index Constraint</em>}</li>
 *   <li>{@link Ada.ElementList#getDiscriminantConstraint <em>Discriminant Constraint</em>}</li>
 *   <li>{@link Ada.ElementList#getComponentDefinition <em>Component Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getDiscreteSubtypeIndicationAsSubtypeDefinition <em>Discrete Subtype Indication As Subtype Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getDiscreteRangeAttributeReferenceAsSubtypeDefinition <em>Discrete Range Attribute Reference As Subtype Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getDiscreteSimpleExpressionRangeAsSubtypeDefinition <em>Discrete Simple Expression Range As Subtype Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getDiscreteSubtypeIndication <em>Discrete Subtype Indication</em>}</li>
 *   <li>{@link Ada.ElementList#getDiscreteRangeAttributeReference <em>Discrete Range Attribute Reference</em>}</li>
 *   <li>{@link Ada.ElementList#getDiscreteSimpleExpressionRange <em>Discrete Simple Expression Range</em>}</li>
 *   <li>{@link Ada.ElementList#getUnknownDiscriminantPart <em>Unknown Discriminant Part</em>}</li>
 *   <li>{@link Ada.ElementList#getKnownDiscriminantPart <em>Known Discriminant Part</em>}</li>
 *   <li>{@link Ada.ElementList#getRecordDefinition <em>Record Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getNullRecordDefinition <em>Null Record Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getNullComponent <em>Null Component</em>}</li>
 *   <li>{@link Ada.ElementList#getVariantPart <em>Variant Part</em>}</li>
 *   <li>{@link Ada.ElementList#getVariant <em>Variant</em>}</li>
 *   <li>{@link Ada.ElementList#getOthersChoice <em>Others Choice</em>}</li>
 *   <li>{@link Ada.ElementList#getAnonymousAccessToVariable <em>Anonymous Access To Variable</em>}</li>
 *   <li>{@link Ada.ElementList#getAnonymousAccessToConstant <em>Anonymous Access To Constant</em>}</li>
 *   <li>{@link Ada.ElementList#getAnonymousAccessToProcedure <em>Anonymous Access To Procedure</em>}</li>
 *   <li>{@link Ada.ElementList#getAnonymousAccessToProtectedProcedure <em>Anonymous Access To Protected Procedure</em>}</li>
 *   <li>{@link Ada.ElementList#getAnonymousAccessToFunction <em>Anonymous Access To Function</em>}</li>
 *   <li>{@link Ada.ElementList#getAnonymousAccessToProtectedFunction <em>Anonymous Access To Protected Function</em>}</li>
 *   <li>{@link Ada.ElementList#getPrivateTypeDefinition <em>Private Type Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getTaggedPrivateTypeDefinition <em>Tagged Private Type Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getPrivateExtensionDefinition <em>Private Extension Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getTaskDefinition <em>Task Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getProtectedDefinition <em>Protected Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalPrivateTypeDefinition <em>Formal Private Type Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalTaggedPrivateTypeDefinition <em>Formal Tagged Private Type Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalDerivedTypeDefinition <em>Formal Derived Type Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalDiscreteTypeDefinition <em>Formal Discrete Type Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalSignedIntegerTypeDefinition <em>Formal Signed Integer Type Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalModularTypeDefinition <em>Formal Modular Type Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalFloatingPointDefinition <em>Formal Floating Point Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalOrdinaryFixedPointDefinition <em>Formal Ordinary Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalDecimalFixedPointDefinition <em>Formal Decimal Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalOrdinaryInterface <em>Formal Ordinary Interface</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalLimitedInterface <em>Formal Limited Interface</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalTaskInterface <em>Formal Task Interface</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalProtectedInterface <em>Formal Protected Interface</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalSynchronizedInterface <em>Formal Synchronized Interface</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalUnconstrainedArrayDefinition <em>Formal Unconstrained Array Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalConstrainedArrayDefinition <em>Formal Constrained Array Definition</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalPoolSpecificAccessToVariable <em>Formal Pool Specific Access To Variable</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalAccessToVariable <em>Formal Access To Variable</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalAccessToConstant <em>Formal Access To Constant</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalAccessToProcedure <em>Formal Access To Procedure</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalAccessToProtectedProcedure <em>Formal Access To Protected Procedure</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalAccessToFunction <em>Formal Access To Function</em>}</li>
 *   <li>{@link Ada.ElementList#getFormalAccessToProtectedFunction <em>Formal Access To Protected Function</em>}</li>
 *   <li>{@link Ada.ElementList#getAspectSpecification <em>Aspect Specification</em>}</li>
 *   <li>{@link Ada.ElementList#getBoxExpression <em>Box Expression</em>}</li>
 *   <li>{@link Ada.ElementList#getIntegerLiteral <em>Integer Literal</em>}</li>
 *   <li>{@link Ada.ElementList#getRealLiteral <em>Real Literal</em>}</li>
 *   <li>{@link Ada.ElementList#getStringLiteral <em>String Literal</em>}</li>
 *   <li>{@link Ada.ElementList#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link Ada.ElementList#getAndOperator <em>And Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getOrOperator <em>Or Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getXorOperator <em>Xor Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getEqualOperator <em>Equal Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getNotEqualOperator <em>Not Equal Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getLessThanOperator <em>Less Than Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getLessThanOrEqualOperator <em>Less Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getGreaterThanOperator <em>Greater Than Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getGreaterThanOrEqualOperator <em>Greater Than Or Equal Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getPlusOperator <em>Plus Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getMinusOperator <em>Minus Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getConcatenateOperator <em>Concatenate Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getUnaryPlusOperator <em>Unary Plus Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getUnaryMinusOperator <em>Unary Minus Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getMultiplyOperator <em>Multiply Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getDivideOperator <em>Divide Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getModOperator <em>Mod Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getRemOperator <em>Rem Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getExponentiateOperator <em>Exponentiate Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getAbsOperator <em>Abs Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getNotOperator <em>Not Operator</em>}</li>
 *   <li>{@link Ada.ElementList#getCharacterLiteral <em>Character Literal</em>}</li>
 *   <li>{@link Ada.ElementList#getEnumerationLiteral <em>Enumeration Literal</em>}</li>
 *   <li>{@link Ada.ElementList#getExplicitDereference <em>Explicit Dereference</em>}</li>
 *   <li>{@link Ada.ElementList#getFunctionCall <em>Function Call</em>}</li>
 *   <li>{@link Ada.ElementList#getIndexedComponent <em>Indexed Component</em>}</li>
 *   <li>{@link Ada.ElementList#getSlice <em>Slice</em>}</li>
 *   <li>{@link Ada.ElementList#getSelectedComponent <em>Selected Component</em>}</li>
 *   <li>{@link Ada.ElementList#getAccessAttribute <em>Access Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getAddressAttribute <em>Address Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getAdjacentAttribute <em>Adjacent Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getAftAttribute <em>Aft Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getAlignmentAttribute <em>Alignment Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getBaseAttribute <em>Base Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getBitOrderAttribute <em>Bit Order Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getBodyVersionAttribute <em>Body Version Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getCallableAttribute <em>Callable Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getCallerAttribute <em>Caller Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getCeilingAttribute <em>Ceiling Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getClassAttribute <em>Class Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getComponentSizeAttribute <em>Component Size Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getComposeAttribute <em>Compose Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getConstrainedAttribute <em>Constrained Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getCopySignAttribute <em>Copy Sign Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getCountAttribute <em>Count Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getDefiniteAttribute <em>Definite Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getDeltaAttribute <em>Delta Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getDenormAttribute <em>Denorm Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getDigitsAttribute <em>Digits Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getExponentAttribute <em>Exponent Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getExternalTagAttribute <em>External Tag Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getFirstAttribute <em>First Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getFirstBitAttribute <em>First Bit Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getFloorAttribute <em>Floor Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getForeAttribute <em>Fore Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getFractionAttribute <em>Fraction Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getIdentityAttribute <em>Identity Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getImageAttribute <em>Image Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getInputAttribute <em>Input Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getLastAttribute <em>Last Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getLastBitAttribute <em>Last Bit Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getLeadingPartAttribute <em>Leading Part Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getLengthAttribute <em>Length Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getMachineAttribute <em>Machine Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getMachineEmaxAttribute <em>Machine Emax Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getMachineEminAttribute <em>Machine Emin Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getMachineMantissaAttribute <em>Machine Mantissa Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getMachineOverflowsAttribute <em>Machine Overflows Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getMachineRadixAttribute <em>Machine Radix Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getMachineRoundsAttribute <em>Machine Rounds Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getMaxAttribute <em>Max Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getMaxSizeInStorageElementsAttribute <em>Max Size In Storage Elements Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getMinAttribute <em>Min Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getModelAttribute <em>Model Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getModelEminAttribute <em>Model Emin Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getModelEpsilonAttribute <em>Model Epsilon Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getModelMantissaAttribute <em>Model Mantissa Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getModelSmallAttribute <em>Model Small Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getModulusAttribute <em>Modulus Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getOutputAttribute <em>Output Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getPartitionIdAttribute <em>Partition Id Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getPosAttribute <em>Pos Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getPositionAttribute <em>Position Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getPredAttribute <em>Pred Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getRangeAttribute <em>Range Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getReadAttribute <em>Read Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getRemainderAttribute <em>Remainder Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getRoundAttribute <em>Round Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getRoundingAttribute <em>Rounding Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getSafeFirstAttribute <em>Safe First Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getSafeLastAttribute <em>Safe Last Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getScaleAttribute <em>Scale Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getScalingAttribute <em>Scaling Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getSignedZerosAttribute <em>Signed Zeros Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getSizeAttribute <em>Size Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getSmallAttribute <em>Small Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getStoragePoolAttribute <em>Storage Pool Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getStorageSizeAttribute <em>Storage Size Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getSuccAttribute <em>Succ Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getTagAttribute <em>Tag Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getTerminatedAttribute <em>Terminated Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getTruncationAttribute <em>Truncation Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getUnbiasedRoundingAttribute <em>Unbiased Rounding Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getUncheckedAccessAttribute <em>Unchecked Access Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getValAttribute <em>Val Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getValidAttribute <em>Valid Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getValueAttribute <em>Value Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getVersionAttribute <em>Version Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getWideImageAttribute <em>Wide Image Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getWideValueAttribute <em>Wide Value Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getWideWidthAttribute <em>Wide Width Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getWidthAttribute <em>Width Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getWriteAttribute <em>Write Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getMachineRoundingAttribute <em>Machine Rounding Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getModAttribute <em>Mod Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getPriorityAttribute <em>Priority Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getStreamSizeAttribute <em>Stream Size Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getWideWideImageAttribute <em>Wide Wide Image Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getWideWideValueAttribute <em>Wide Wide Value Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getWideWideWidthAttribute <em>Wide Wide Width Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getMaxAlignmentForAllocationAttribute <em>Max Alignment For Allocation Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getOverlapsStorageAttribute <em>Overlaps Storage Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getImplementationDefinedAttribute <em>Implementation Defined Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getUnknownAttribute <em>Unknown Attribute</em>}</li>
 *   <li>{@link Ada.ElementList#getRecordAggregate <em>Record Aggregate</em>}</li>
 *   <li>{@link Ada.ElementList#getExtensionAggregate <em>Extension Aggregate</em>}</li>
 *   <li>{@link Ada.ElementList#getPositionalArrayAggregate <em>Positional Array Aggregate</em>}</li>
 *   <li>{@link Ada.ElementList#getNamedArrayAggregate <em>Named Array Aggregate</em>}</li>
 *   <li>{@link Ada.ElementList#getAndThenShortCircuit <em>And Then Short Circuit</em>}</li>
 *   <li>{@link Ada.ElementList#getOrElseShortCircuit <em>Or Else Short Circuit</em>}</li>
 *   <li>{@link Ada.ElementList#getInMembershipTest <em>In Membership Test</em>}</li>
 *   <li>{@link Ada.ElementList#getNotInMembershipTest <em>Not In Membership Test</em>}</li>
 *   <li>{@link Ada.ElementList#getNullLiteral <em>Null Literal</em>}</li>
 *   <li>{@link Ada.ElementList#getParenthesizedExpression <em>Parenthesized Expression</em>}</li>
 *   <li>{@link Ada.ElementList#getRaiseExpression <em>Raise Expression</em>}</li>
 *   <li>{@link Ada.ElementList#getTypeConversion <em>Type Conversion</em>}</li>
 *   <li>{@link Ada.ElementList#getQualifiedExpression <em>Qualified Expression</em>}</li>
 *   <li>{@link Ada.ElementList#getAllocationFromSubtype <em>Allocation From Subtype</em>}</li>
 *   <li>{@link Ada.ElementList#getAllocationFromQualifiedExpression <em>Allocation From Qualified Expression</em>}</li>
 *   <li>{@link Ada.ElementList#getCaseExpression <em>Case Expression</em>}</li>
 *   <li>{@link Ada.ElementList#getIfExpression <em>If Expression</em>}</li>
 *   <li>{@link Ada.ElementList#getForAllQuantifiedExpression <em>For All Quantified Expression</em>}</li>
 *   <li>{@link Ada.ElementList#getForSomeQuantifiedExpression <em>For Some Quantified Expression</em>}</li>
 *   <li>{@link Ada.ElementList#getPragmaArgumentAssociation <em>Pragma Argument Association</em>}</li>
 *   <li>{@link Ada.ElementList#getDiscriminantAssociation <em>Discriminant Association</em>}</li>
 *   <li>{@link Ada.ElementList#getRecordComponentAssociation <em>Record Component Association</em>}</li>
 *   <li>{@link Ada.ElementList#getArrayComponentAssociation <em>Array Component Association</em>}</li>
 *   <li>{@link Ada.ElementList#getParameterAssociation <em>Parameter Association</em>}</li>
 *   <li>{@link Ada.ElementList#getGenericAssociation <em>Generic Association</em>}</li>
 *   <li>{@link Ada.ElementList#getNullStatement <em>Null Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getAssignmentStatement <em>Assignment Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getIfStatement <em>If Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getCaseStatement <em>Case Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getLoopStatement <em>Loop Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getWhileLoopStatement <em>While Loop Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getForLoopStatement <em>For Loop Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getBlockStatement <em>Block Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getExitStatement <em>Exit Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getGotoStatement <em>Goto Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getProcedureCallStatement <em>Procedure Call Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getReturnStatement <em>Return Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getExtendedReturnStatement <em>Extended Return Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getAcceptStatement <em>Accept Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getEntryCallStatement <em>Entry Call Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getRequeueStatement <em>Requeue Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getRequeueStatementWithAbort <em>Requeue Statement With Abort</em>}</li>
 *   <li>{@link Ada.ElementList#getDelayUntilStatement <em>Delay Until Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getDelayRelativeStatement <em>Delay Relative Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getTerminateAlternativeStatement <em>Terminate Alternative Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getSelectiveAcceptStatement <em>Selective Accept Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getTimedEntryCallStatement <em>Timed Entry Call Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getConditionalEntryCallStatement <em>Conditional Entry Call Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getAsynchronousSelectStatement <em>Asynchronous Select Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getAbortStatement <em>Abort Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getRaiseStatement <em>Raise Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getCodeStatement <em>Code Statement</em>}</li>
 *   <li>{@link Ada.ElementList#getIfPath <em>If Path</em>}</li>
 *   <li>{@link Ada.ElementList#getElsifPath <em>Elsif Path</em>}</li>
 *   <li>{@link Ada.ElementList#getElsePath <em>Else Path</em>}</li>
 *   <li>{@link Ada.ElementList#getCasePath <em>Case Path</em>}</li>
 *   <li>{@link Ada.ElementList#getSelectPath <em>Select Path</em>}</li>
 *   <li>{@link Ada.ElementList#getOrPath <em>Or Path</em>}</li>
 *   <li>{@link Ada.ElementList#getThenAbortPath <em>Then Abort Path</em>}</li>
 *   <li>{@link Ada.ElementList#getCaseExpressionPath <em>Case Expression Path</em>}</li>
 *   <li>{@link Ada.ElementList#getIfExpressionPath <em>If Expression Path</em>}</li>
 *   <li>{@link Ada.ElementList#getElsifExpressionPath <em>Elsif Expression Path</em>}</li>
 *   <li>{@link Ada.ElementList#getElseExpressionPath <em>Else Expression Path</em>}</li>
 *   <li>{@link Ada.ElementList#getUsePackageClause <em>Use Package Clause</em>}</li>
 *   <li>{@link Ada.ElementList#getUseTypeClause <em>Use Type Clause</em>}</li>
 *   <li>{@link Ada.ElementList#getUseAllTypeClause <em>Use All Type Clause</em>}</li>
 *   <li>{@link Ada.ElementList#getWithClause <em>With Clause</em>}</li>
 *   <li>{@link Ada.ElementList#getAttributeDefinitionClause <em>Attribute Definition Clause</em>}</li>
 *   <li>{@link Ada.ElementList#getEnumerationRepresentationClause <em>Enumeration Representation Clause</em>}</li>
 *   <li>{@link Ada.ElementList#getRecordRepresentationClause <em>Record Representation Clause</em>}</li>
 *   <li>{@link Ada.ElementList#getAtClause <em>At Clause</em>}</li>
 *   <li>{@link Ada.ElementList#getComponentClause <em>Component Clause</em>}</li>
 *   <li>{@link Ada.ElementList#getExceptionHandler <em>Exception Handler</em>}</li>
 *   <li>{@link Ada.ElementList#getComment <em>Comment</em>}</li>
 *   <li>{@link Ada.ElementList#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getAsynchronousPragma <em>Asynchronous Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getAtomicPragma <em>Atomic Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getAttachHandlerPragma <em>Attach Handler Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getControlledPragma <em>Controlled Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getConventionPragma <em>Convention Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getDiscardNamesPragma <em>Discard Names Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getElaboratePragma <em>Elaborate Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getElaborateAllPragma <em>Elaborate All Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getExportPragma <em>Export Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getImportPragma <em>Import Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getInlinePragma <em>Inline Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getInspectionPointPragma <em>Inspection Point Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getLinkerOptionsPragma <em>Linker Options Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getListPragma <em>List Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getLockingPolicyPragma <em>Locking Policy Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getOptimizePragma <em>Optimize Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getPackPragma <em>Pack Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getPagePragma <em>Page Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getPreelaboratePragma <em>Preelaborate Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getPriorityPragma <em>Priority Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getPurePragma <em>Pure Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getRemoteTypesPragma <em>Remote Types Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getRestrictionsPragma <em>Restrictions Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getReviewablePragma <em>Reviewable Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getSharedPassivePragma <em>Shared Passive Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getStorageSizePragma <em>Storage Size Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getSuppressPragma <em>Suppress Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getVolatilePragma <em>Volatile Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getAssertPragma <em>Assert Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getNoReturnPragma <em>No Return Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getProfilePragma <em>Profile Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getUnsuppressPragma <em>Unsuppress Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getCpuPragma <em>Cpu Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getIndependentPragma <em>Independent Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getIndependentComponentsPragma <em>Independent Components Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}</li>
 *   <li>{@link Ada.ElementList#getUnknownPragma <em>Unknown Pragma</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getElementList()
 * @model extendedMetaData="name='Element_List' kind='elementOnly'"
 * @generated
 */
public interface ElementList extends EObject {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' attribute list.
	 * @see Ada.AdaPackage#getElementList_Group()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:0'"
	 * @generated
	 */
	FeatureMap getGroup();

	/**
	 * Returns the value of the '<em><b>Not An Element</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NotAnElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not An Element</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not An Element</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_NotAnElement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='not_an_element' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NotAnElement> getNotAnElement();

	/**
	 * Returns the value of the '<em><b>Defining Identifier</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiningIdentifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Identifier</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Identifier</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiningIdentifier()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='defining_identifier' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiningIdentifier> getDefiningIdentifier();

	/**
	 * Returns the value of the '<em><b>Defining Character Literal</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiningCharacterLiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Character Literal</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Character Literal</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiningCharacterLiteral()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='defining_character_literal' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiningCharacterLiteral> getDefiningCharacterLiteral();

	/**
	 * Returns the value of the '<em><b>Defining Enumeration Literal</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiningEnumerationLiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Enumeration Literal</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Enumeration Literal</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiningEnumerationLiteral()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='defining_enumeration_literal' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiningEnumerationLiteral> getDefiningEnumerationLiteral();

	/**
	 * Returns the value of the '<em><b>Defining And Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiningAndOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining And Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining And Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiningAndOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='defining_and_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiningAndOperator> getDefiningAndOperator();

	/**
	 * Returns the value of the '<em><b>Defining Or Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiningOrOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Or Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Or Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiningOrOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='defining_or_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiningOrOperator> getDefiningOrOperator();

	/**
	 * Returns the value of the '<em><b>Defining Xor Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiningXorOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Xor Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Xor Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiningXorOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='defining_xor_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiningXorOperator> getDefiningXorOperator();

	/**
	 * Returns the value of the '<em><b>Defining Equal Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiningEqualOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Equal Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Equal Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiningEqualOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='defining_equal_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiningEqualOperator> getDefiningEqualOperator();

	/**
	 * Returns the value of the '<em><b>Defining Not Equal Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiningNotEqualOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Not Equal Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Not Equal Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiningNotEqualOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='defining_not_equal_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiningNotEqualOperator> getDefiningNotEqualOperator();

	/**
	 * Returns the value of the '<em><b>Defining Less Than Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiningLessThanOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Less Than Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Less Than Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiningLessThanOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='defining_less_than_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiningLessThanOperator> getDefiningLessThanOperator();

	/**
	 * Returns the value of the '<em><b>Defining Less Than Or Equal Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiningLessThanOrEqualOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Less Than Or Equal Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Less Than Or Equal Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiningLessThanOrEqualOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='defining_less_than_or_equal_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiningLessThanOrEqualOperator> getDefiningLessThanOrEqualOperator();

	/**
	 * Returns the value of the '<em><b>Defining Greater Than Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiningGreaterThanOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Greater Than Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Greater Than Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiningGreaterThanOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='defining_greater_than_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiningGreaterThanOperator> getDefiningGreaterThanOperator();

	/**
	 * Returns the value of the '<em><b>Defining Greater Than Or Equal Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiningGreaterThanOrEqualOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Greater Than Or Equal Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Greater Than Or Equal Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiningGreaterThanOrEqualOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='defining_greater_than_or_equal_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiningGreaterThanOrEqualOperator> getDefiningGreaterThanOrEqualOperator();

	/**
	 * Returns the value of the '<em><b>Defining Plus Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiningPlusOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Plus Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Plus Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiningPlusOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='defining_plus_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiningPlusOperator> getDefiningPlusOperator();

	/**
	 * Returns the value of the '<em><b>Defining Minus Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiningMinusOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Minus Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Minus Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiningMinusOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='defining_minus_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiningMinusOperator> getDefiningMinusOperator();

	/**
	 * Returns the value of the '<em><b>Defining Concatenate Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiningConcatenateOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Concatenate Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Concatenate Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiningConcatenateOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='defining_concatenate_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiningConcatenateOperator> getDefiningConcatenateOperator();

	/**
	 * Returns the value of the '<em><b>Defining Unary Plus Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiningUnaryPlusOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Unary Plus Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Unary Plus Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiningUnaryPlusOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='defining_unary_plus_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiningUnaryPlusOperator> getDefiningUnaryPlusOperator();

	/**
	 * Returns the value of the '<em><b>Defining Unary Minus Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiningUnaryMinusOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Unary Minus Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Unary Minus Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiningUnaryMinusOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='defining_unary_minus_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiningUnaryMinusOperator> getDefiningUnaryMinusOperator();

	/**
	 * Returns the value of the '<em><b>Defining Multiply Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiningMultiplyOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Multiply Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Multiply Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiningMultiplyOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='defining_multiply_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiningMultiplyOperator> getDefiningMultiplyOperator();

	/**
	 * Returns the value of the '<em><b>Defining Divide Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiningDivideOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Divide Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Divide Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiningDivideOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='defining_divide_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiningDivideOperator> getDefiningDivideOperator();

	/**
	 * Returns the value of the '<em><b>Defining Mod Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiningModOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Mod Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Mod Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiningModOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='defining_mod_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiningModOperator> getDefiningModOperator();

	/**
	 * Returns the value of the '<em><b>Defining Rem Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiningRemOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Rem Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Rem Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiningRemOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='defining_rem_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiningRemOperator> getDefiningRemOperator();

	/**
	 * Returns the value of the '<em><b>Defining Exponentiate Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiningExponentiateOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Exponentiate Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Exponentiate Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiningExponentiateOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='defining_exponentiate_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiningExponentiateOperator> getDefiningExponentiateOperator();

	/**
	 * Returns the value of the '<em><b>Defining Abs Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiningAbsOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Abs Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Abs Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiningAbsOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='defining_abs_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiningAbsOperator> getDefiningAbsOperator();

	/**
	 * Returns the value of the '<em><b>Defining Not Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiningNotOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Not Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Not Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiningNotOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='defining_not_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiningNotOperator> getDefiningNotOperator();

	/**
	 * Returns the value of the '<em><b>Defining Expanded Name</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiningExpandedName}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Defining Expanded Name</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Defining Expanded Name</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiningExpandedName()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='defining_expanded_name' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiningExpandedName> getDefiningExpandedName();

	/**
	 * Returns the value of the '<em><b>Ordinary Type Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OrdinaryTypeDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ordinary Type Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ordinary Type Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_OrdinaryTypeDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ordinary_type_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OrdinaryTypeDeclaration> getOrdinaryTypeDeclaration();

	/**
	 * Returns the value of the '<em><b>Task Type Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TaskTypeDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Type Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Type Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_TaskTypeDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='task_type_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TaskTypeDeclaration> getTaskTypeDeclaration();

	/**
	 * Returns the value of the '<em><b>Protected Type Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProtectedTypeDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protected Type Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protected Type Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ProtectedTypeDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='protected_type_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProtectedTypeDeclaration> getProtectedTypeDeclaration();

	/**
	 * Returns the value of the '<em><b>Incomplete Type Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IncompleteTypeDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incomplete Type Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incomplete Type Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_IncompleteTypeDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='incomplete_type_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IncompleteTypeDeclaration> getIncompleteTypeDeclaration();

	/**
	 * Returns the value of the '<em><b>Tagged Incomplete Type Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TaggedIncompleteTypeDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tagged Incomplete Type Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tagged Incomplete Type Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_TaggedIncompleteTypeDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='tagged_incomplete_type_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TaggedIncompleteTypeDeclaration> getTaggedIncompleteTypeDeclaration();

	/**
	 * Returns the value of the '<em><b>Private Type Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PrivateTypeDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Private Type Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Private Type Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PrivateTypeDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='private_type_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PrivateTypeDeclaration> getPrivateTypeDeclaration();

	/**
	 * Returns the value of the '<em><b>Private Extension Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PrivateExtensionDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Private Extension Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Private Extension Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PrivateExtensionDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='private_extension_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PrivateExtensionDeclaration> getPrivateExtensionDeclaration();

	/**
	 * Returns the value of the '<em><b>Subtype Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SubtypeDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subtype Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subtype Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_SubtypeDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='subtype_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SubtypeDeclaration> getSubtypeDeclaration();

	/**
	 * Returns the value of the '<em><b>Variable Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.VariableDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variable Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variable Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_VariableDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='variable_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<VariableDeclaration> getVariableDeclaration();

	/**
	 * Returns the value of the '<em><b>Constant Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ConstantDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ConstantDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='constant_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ConstantDeclaration> getConstantDeclaration();

	/**
	 * Returns the value of the '<em><b>Deferred Constant Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DeferredConstantDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deferred Constant Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deferred Constant Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DeferredConstantDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='deferred_constant_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DeferredConstantDeclaration> getDeferredConstantDeclaration();

	/**
	 * Returns the value of the '<em><b>Single Task Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SingleTaskDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Single Task Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Single Task Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_SingleTaskDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='single_task_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SingleTaskDeclaration> getSingleTaskDeclaration();

	/**
	 * Returns the value of the '<em><b>Single Protected Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SingleProtectedDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Single Protected Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Single Protected Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_SingleProtectedDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='single_protected_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SingleProtectedDeclaration> getSingleProtectedDeclaration();

	/**
	 * Returns the value of the '<em><b>Integer Number Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IntegerNumberDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integer Number Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integer Number Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_IntegerNumberDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='integer_number_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IntegerNumberDeclaration> getIntegerNumberDeclaration();

	/**
	 * Returns the value of the '<em><b>Real Number Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RealNumberDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Real Number Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Real Number Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_RealNumberDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='real_number_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RealNumberDeclaration> getRealNumberDeclaration();

	/**
	 * Returns the value of the '<em><b>Enumeration Literal Specification</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.EnumerationLiteralSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumeration Literal Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumeration Literal Specification</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_EnumerationLiteralSpecification()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='enumeration_literal_specification' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<EnumerationLiteralSpecification> getEnumerationLiteralSpecification();

	/**
	 * Returns the value of the '<em><b>Discriminant Specification</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscriminantSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discriminant Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discriminant Specification</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DiscriminantSpecification()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discriminant_specification' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscriminantSpecification> getDiscriminantSpecification();

	/**
	 * Returns the value of the '<em><b>Component Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ComponentDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ComponentDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='component_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ComponentDeclaration> getComponentDeclaration();

	/**
	 * Returns the value of the '<em><b>Loop Parameter Specification</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LoopParameterSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Loop Parameter Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Loop Parameter Specification</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_LoopParameterSpecification()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='loop_parameter_specification' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LoopParameterSpecification> getLoopParameterSpecification();

	/**
	 * Returns the value of the '<em><b>Generalized Iterator Specification</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.GeneralizedIteratorSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generalized Iterator Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generalized Iterator Specification</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_GeneralizedIteratorSpecification()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='generalized_iterator_specification' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<GeneralizedIteratorSpecification> getGeneralizedIteratorSpecification();

	/**
	 * Returns the value of the '<em><b>Element Iterator Specification</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElementIteratorSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element Iterator Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Iterator Specification</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ElementIteratorSpecification()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='element_iterator_specification' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElementIteratorSpecification> getElementIteratorSpecification();

	/**
	 * Returns the value of the '<em><b>Procedure Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProcedureDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ProcedureDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='procedure_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProcedureDeclaration> getProcedureDeclaration();

	/**
	 * Returns the value of the '<em><b>Function Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FunctionDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FunctionDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='function_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FunctionDeclaration> getFunctionDeclaration();

	/**
	 * Returns the value of the '<em><b>Parameter Specification</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ParameterSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Specification</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ParameterSpecification()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='parameter_specification' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ParameterSpecification> getParameterSpecification();

	/**
	 * Returns the value of the '<em><b>Procedure Body Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProcedureBodyDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure Body Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure Body Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ProcedureBodyDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='procedure_body_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProcedureBodyDeclaration> getProcedureBodyDeclaration();

	/**
	 * Returns the value of the '<em><b>Function Body Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FunctionBodyDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Body Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Body Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FunctionBodyDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='function_body_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FunctionBodyDeclaration> getFunctionBodyDeclaration();

	/**
	 * Returns the value of the '<em><b>Return Variable Specification</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ReturnVariableSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Return Variable Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Return Variable Specification</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ReturnVariableSpecification()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='return_variable_specification' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ReturnVariableSpecification> getReturnVariableSpecification();

	/**
	 * Returns the value of the '<em><b>Return Constant Specification</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ReturnConstantSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Return Constant Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Return Constant Specification</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ReturnConstantSpecification()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='return_constant_specification' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ReturnConstantSpecification> getReturnConstantSpecification();

	/**
	 * Returns the value of the '<em><b>Null Procedure Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NullProcedureDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Null Procedure Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Null Procedure Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_NullProcedureDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='null_procedure_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NullProcedureDeclaration> getNullProcedureDeclaration();

	/**
	 * Returns the value of the '<em><b>Expression Function Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExpressionFunctionDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression Function Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression Function Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ExpressionFunctionDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='expression_function_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExpressionFunctionDeclaration> getExpressionFunctionDeclaration();

	/**
	 * Returns the value of the '<em><b>Package Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PackageDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PackageDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='package_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PackageDeclaration> getPackageDeclaration();

	/**
	 * Returns the value of the '<em><b>Package Body Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PackageBodyDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Body Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Body Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PackageBodyDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='package_body_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PackageBodyDeclaration> getPackageBodyDeclaration();

	/**
	 * Returns the value of the '<em><b>Object Renaming Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ObjectRenamingDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Renaming Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Renaming Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ObjectRenamingDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='object_renaming_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ObjectRenamingDeclaration> getObjectRenamingDeclaration();

	/**
	 * Returns the value of the '<em><b>Exception Renaming Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExceptionRenamingDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exception Renaming Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exception Renaming Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ExceptionRenamingDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='exception_renaming_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExceptionRenamingDeclaration> getExceptionRenamingDeclaration();

	/**
	 * Returns the value of the '<em><b>Package Renaming Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PackageRenamingDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Renaming Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Renaming Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PackageRenamingDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='package_renaming_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PackageRenamingDeclaration> getPackageRenamingDeclaration();

	/**
	 * Returns the value of the '<em><b>Procedure Renaming Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProcedureRenamingDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure Renaming Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure Renaming Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ProcedureRenamingDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='procedure_renaming_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProcedureRenamingDeclaration> getProcedureRenamingDeclaration();

	/**
	 * Returns the value of the '<em><b>Function Renaming Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FunctionRenamingDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Renaming Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Renaming Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FunctionRenamingDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='function_renaming_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FunctionRenamingDeclaration> getFunctionRenamingDeclaration();

	/**
	 * Returns the value of the '<em><b>Generic Package Renaming Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.GenericPackageRenamingDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Package Renaming Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Package Renaming Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_GenericPackageRenamingDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='generic_package_renaming_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<GenericPackageRenamingDeclaration> getGenericPackageRenamingDeclaration();

	/**
	 * Returns the value of the '<em><b>Generic Procedure Renaming Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.GenericProcedureRenamingDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Procedure Renaming Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Procedure Renaming Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_GenericProcedureRenamingDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='generic_procedure_renaming_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<GenericProcedureRenamingDeclaration> getGenericProcedureRenamingDeclaration();

	/**
	 * Returns the value of the '<em><b>Generic Function Renaming Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.GenericFunctionRenamingDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Function Renaming Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Function Renaming Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_GenericFunctionRenamingDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='generic_function_renaming_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<GenericFunctionRenamingDeclaration> getGenericFunctionRenamingDeclaration();

	/**
	 * Returns the value of the '<em><b>Task Body Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TaskBodyDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Body Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Body Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_TaskBodyDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='task_body_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TaskBodyDeclaration> getTaskBodyDeclaration();

	/**
	 * Returns the value of the '<em><b>Protected Body Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProtectedBodyDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protected Body Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protected Body Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ProtectedBodyDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='protected_body_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProtectedBodyDeclaration> getProtectedBodyDeclaration();

	/**
	 * Returns the value of the '<em><b>Entry Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.EntryDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_EntryDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='entry_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<EntryDeclaration> getEntryDeclaration();

	/**
	 * Returns the value of the '<em><b>Entry Body Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.EntryBodyDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry Body Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry Body Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_EntryBodyDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='entry_body_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<EntryBodyDeclaration> getEntryBodyDeclaration();

	/**
	 * Returns the value of the '<em><b>Entry Index Specification</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.EntryIndexSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry Index Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry Index Specification</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_EntryIndexSpecification()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='entry_index_specification' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<EntryIndexSpecification> getEntryIndexSpecification();

	/**
	 * Returns the value of the '<em><b>Procedure Body Stub</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProcedureBodyStub}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure Body Stub</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure Body Stub</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ProcedureBodyStub()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='procedure_body_stub' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProcedureBodyStub> getProcedureBodyStub();

	/**
	 * Returns the value of the '<em><b>Function Body Stub</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FunctionBodyStub}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Body Stub</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Body Stub</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FunctionBodyStub()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='function_body_stub' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FunctionBodyStub> getFunctionBodyStub();

	/**
	 * Returns the value of the '<em><b>Package Body Stub</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PackageBodyStub}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Body Stub</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Body Stub</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PackageBodyStub()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='package_body_stub' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PackageBodyStub> getPackageBodyStub();

	/**
	 * Returns the value of the '<em><b>Task Body Stub</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TaskBodyStub}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Body Stub</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Body Stub</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_TaskBodyStub()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='task_body_stub' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TaskBodyStub> getTaskBodyStub();

	/**
	 * Returns the value of the '<em><b>Protected Body Stub</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProtectedBodyStub}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protected Body Stub</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protected Body Stub</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ProtectedBodyStub()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='protected_body_stub' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProtectedBodyStub> getProtectedBodyStub();

	/**
	 * Returns the value of the '<em><b>Exception Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExceptionDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exception Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exception Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ExceptionDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='exception_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExceptionDeclaration> getExceptionDeclaration();

	/**
	 * Returns the value of the '<em><b>Choice Parameter Specification</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ChoiceParameterSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Choice Parameter Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Choice Parameter Specification</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ChoiceParameterSpecification()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='choice_parameter_specification' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ChoiceParameterSpecification> getChoiceParameterSpecification();

	/**
	 * Returns the value of the '<em><b>Generic Procedure Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.GenericProcedureDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Procedure Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Procedure Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_GenericProcedureDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='generic_procedure_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<GenericProcedureDeclaration> getGenericProcedureDeclaration();

	/**
	 * Returns the value of the '<em><b>Generic Function Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.GenericFunctionDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Function Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Function Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_GenericFunctionDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='generic_function_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<GenericFunctionDeclaration> getGenericFunctionDeclaration();

	/**
	 * Returns the value of the '<em><b>Generic Package Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.GenericPackageDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Package Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Package Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_GenericPackageDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='generic_package_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<GenericPackageDeclaration> getGenericPackageDeclaration();

	/**
	 * Returns the value of the '<em><b>Package Instantiation</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PackageInstantiation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Instantiation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Instantiation</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PackageInstantiation()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='package_instantiation' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PackageInstantiation> getPackageInstantiation();

	/**
	 * Returns the value of the '<em><b>Procedure Instantiation</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProcedureInstantiation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure Instantiation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure Instantiation</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ProcedureInstantiation()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='procedure_instantiation' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProcedureInstantiation> getProcedureInstantiation();

	/**
	 * Returns the value of the '<em><b>Function Instantiation</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FunctionInstantiation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Instantiation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Instantiation</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FunctionInstantiation()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='function_instantiation' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FunctionInstantiation> getFunctionInstantiation();

	/**
	 * Returns the value of the '<em><b>Formal Object Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalObjectDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Object Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Object Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalObjectDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_object_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalObjectDeclaration> getFormalObjectDeclaration();

	/**
	 * Returns the value of the '<em><b>Formal Type Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalTypeDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Type Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Type Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalTypeDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_type_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalTypeDeclaration> getFormalTypeDeclaration();

	/**
	 * Returns the value of the '<em><b>Formal Incomplete Type Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalIncompleteTypeDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Incomplete Type Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Incomplete Type Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalIncompleteTypeDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_incomplete_type_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalIncompleteTypeDeclaration> getFormalIncompleteTypeDeclaration();

	/**
	 * Returns the value of the '<em><b>Formal Procedure Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalProcedureDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Procedure Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Procedure Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalProcedureDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_procedure_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalProcedureDeclaration> getFormalProcedureDeclaration();

	/**
	 * Returns the value of the '<em><b>Formal Function Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalFunctionDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Function Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Function Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalFunctionDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_function_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalFunctionDeclaration> getFormalFunctionDeclaration();

	/**
	 * Returns the value of the '<em><b>Formal Package Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalPackageDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Package Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Package Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalPackageDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_package_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalPackageDeclaration> getFormalPackageDeclaration();

	/**
	 * Returns the value of the '<em><b>Formal Package Declaration With Box</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalPackageDeclarationWithBox}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Package Declaration With Box</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Package Declaration With Box</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalPackageDeclarationWithBox()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_package_declaration_with_box' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalPackageDeclarationWithBox> getFormalPackageDeclarationWithBox();

	/**
	 * Returns the value of the '<em><b>Derived Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DerivedTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DerivedTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='derived_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DerivedTypeDefinition> getDerivedTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Derived Record Extension Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DerivedRecordExtensionDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived Record Extension Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived Record Extension Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DerivedRecordExtensionDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='derived_record_extension_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DerivedRecordExtensionDefinition> getDerivedRecordExtensionDefinition();

	/**
	 * Returns the value of the '<em><b>Enumeration Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.EnumerationTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumeration Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumeration Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_EnumerationTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='enumeration_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<EnumerationTypeDefinition> getEnumerationTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Signed Integer Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SignedIntegerTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signed Integer Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signed Integer Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_SignedIntegerTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='signed_integer_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SignedIntegerTypeDefinition> getSignedIntegerTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Modular Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ModularTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Modular Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Modular Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ModularTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='modular_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ModularTypeDefinition> getModularTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Root Integer Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RootIntegerDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Integer Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Integer Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_RootIntegerDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='root_integer_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RootIntegerDefinition> getRootIntegerDefinition();

	/**
	 * Returns the value of the '<em><b>Root Real Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RootRealDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Real Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Real Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_RootRealDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='root_real_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RootRealDefinition> getRootRealDefinition();

	/**
	 * Returns the value of the '<em><b>Universal Integer Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UniversalIntegerDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Universal Integer Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Universal Integer Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_UniversalIntegerDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='universal_integer_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UniversalIntegerDefinition> getUniversalIntegerDefinition();

	/**
	 * Returns the value of the '<em><b>Universal Real Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UniversalRealDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Universal Real Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Universal Real Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_UniversalRealDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='universal_real_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UniversalRealDefinition> getUniversalRealDefinition();

	/**
	 * Returns the value of the '<em><b>Universal Fixed Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UniversalFixedDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Universal Fixed Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Universal Fixed Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_UniversalFixedDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='universal_fixed_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UniversalFixedDefinition> getUniversalFixedDefinition();

	/**
	 * Returns the value of the '<em><b>Floating Point Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FloatingPointDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Floating Point Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Floating Point Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FloatingPointDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='floating_point_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FloatingPointDefinition> getFloatingPointDefinition();

	/**
	 * Returns the value of the '<em><b>Ordinary Fixed Point Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OrdinaryFixedPointDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ordinary Fixed Point Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ordinary Fixed Point Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_OrdinaryFixedPointDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ordinary_fixed_point_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OrdinaryFixedPointDefinition> getOrdinaryFixedPointDefinition();

	/**
	 * Returns the value of the '<em><b>Decimal Fixed Point Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DecimalFixedPointDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Decimal Fixed Point Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Decimal Fixed Point Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DecimalFixedPointDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='decimal_fixed_point_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DecimalFixedPointDefinition> getDecimalFixedPointDefinition();

	/**
	 * Returns the value of the '<em><b>Unconstrained Array Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnconstrainedArrayDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unconstrained Array Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unconstrained Array Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_UnconstrainedArrayDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unconstrained_array_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnconstrainedArrayDefinition> getUnconstrainedArrayDefinition();

	/**
	 * Returns the value of the '<em><b>Constrained Array Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ConstrainedArrayDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constrained Array Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constrained Array Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ConstrainedArrayDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='constrained_array_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ConstrainedArrayDefinition> getConstrainedArrayDefinition();

	/**
	 * Returns the value of the '<em><b>Record Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RecordTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_RecordTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='record_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RecordTypeDefinition> getRecordTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Tagged Record Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TaggedRecordTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tagged Record Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tagged Record Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_TaggedRecordTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='tagged_record_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TaggedRecordTypeDefinition> getTaggedRecordTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Ordinary Interface</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OrdinaryInterface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ordinary Interface</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ordinary Interface</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_OrdinaryInterface()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ordinary_interface' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OrdinaryInterface> getOrdinaryInterface();

	/**
	 * Returns the value of the '<em><b>Limited Interface</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LimitedInterface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Limited Interface</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Limited Interface</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_LimitedInterface()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='limited_interface' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LimitedInterface> getLimitedInterface();

	/**
	 * Returns the value of the '<em><b>Task Interface</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TaskInterface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Interface</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Interface</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_TaskInterface()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='task_interface' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TaskInterface> getTaskInterface();

	/**
	 * Returns the value of the '<em><b>Protected Interface</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProtectedInterface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protected Interface</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protected Interface</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ProtectedInterface()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='protected_interface' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProtectedInterface> getProtectedInterface();

	/**
	 * Returns the value of the '<em><b>Synchronized Interface</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SynchronizedInterface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Synchronized Interface</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Synchronized Interface</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_SynchronizedInterface()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='synchronized_interface' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SynchronizedInterface> getSynchronizedInterface();

	/**
	 * Returns the value of the '<em><b>Pool Specific Access To Variable</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PoolSpecificAccessToVariable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pool Specific Access To Variable</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pool Specific Access To Variable</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PoolSpecificAccessToVariable()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='pool_specific_access_to_variable' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PoolSpecificAccessToVariable> getPoolSpecificAccessToVariable();

	/**
	 * Returns the value of the '<em><b>Access To Variable</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AccessToVariable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Variable</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Variable</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AccessToVariable()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='access_to_variable' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AccessToVariable> getAccessToVariable();

	/**
	 * Returns the value of the '<em><b>Access To Constant</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AccessToConstant}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Constant</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Constant</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AccessToConstant()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='access_to_constant' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AccessToConstant> getAccessToConstant();

	/**
	 * Returns the value of the '<em><b>Access To Procedure</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AccessToProcedure}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Procedure</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Procedure</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AccessToProcedure()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='access_to_procedure' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AccessToProcedure> getAccessToProcedure();

	/**
	 * Returns the value of the '<em><b>Access To Protected Procedure</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AccessToProtectedProcedure}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Protected Procedure</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Protected Procedure</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AccessToProtectedProcedure()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='access_to_protected_procedure' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AccessToProtectedProcedure> getAccessToProtectedProcedure();

	/**
	 * Returns the value of the '<em><b>Access To Function</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AccessToFunction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Function</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Function</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AccessToFunction()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='access_to_function' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AccessToFunction> getAccessToFunction();

	/**
	 * Returns the value of the '<em><b>Access To Protected Function</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AccessToProtectedFunction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access To Protected Function</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access To Protected Function</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AccessToProtectedFunction()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='access_to_protected_function' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AccessToProtectedFunction> getAccessToProtectedFunction();

	/**
	 * Returns the value of the '<em><b>Subtype Indication</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SubtypeIndication}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subtype Indication</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subtype Indication</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_SubtypeIndication()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='subtype_indication' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SubtypeIndication> getSubtypeIndication();

	/**
	 * Returns the value of the '<em><b>Range Attribute Reference</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RangeAttributeReference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Range Attribute Reference</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range Attribute Reference</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_RangeAttributeReference()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='range_attribute_reference' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RangeAttributeReference> getRangeAttributeReference();

	/**
	 * Returns the value of the '<em><b>Simple Expression Range</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SimpleExpressionRange}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simple Expression Range</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simple Expression Range</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_SimpleExpressionRange()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='simple_expression_range' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SimpleExpressionRange> getSimpleExpressionRange();

	/**
	 * Returns the value of the '<em><b>Digits Constraint</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DigitsConstraint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Digits Constraint</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Digits Constraint</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DigitsConstraint()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='digits_constraint' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DigitsConstraint> getDigitsConstraint();

	/**
	 * Returns the value of the '<em><b>Delta Constraint</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DeltaConstraint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delta Constraint</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delta Constraint</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DeltaConstraint()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='delta_constraint' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DeltaConstraint> getDeltaConstraint();

	/**
	 * Returns the value of the '<em><b>Index Constraint</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IndexConstraint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Index Constraint</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Index Constraint</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_IndexConstraint()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='index_constraint' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IndexConstraint> getIndexConstraint();

	/**
	 * Returns the value of the '<em><b>Discriminant Constraint</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscriminantConstraint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discriminant Constraint</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discriminant Constraint</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DiscriminantConstraint()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discriminant_constraint' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscriminantConstraint> getDiscriminantConstraint();

	/**
	 * Returns the value of the '<em><b>Component Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ComponentDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ComponentDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='component_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ComponentDefinition> getComponentDefinition();

	/**
	 * Returns the value of the '<em><b>Discrete Subtype Indication As Subtype Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscreteSubtypeIndicationAsSubtypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Subtype Indication As Subtype Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Subtype Indication As Subtype Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DiscreteSubtypeIndicationAsSubtypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discrete_subtype_indication_as_subtype_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscreteSubtypeIndicationAsSubtypeDefinition> getDiscreteSubtypeIndicationAsSubtypeDefinition();

	/**
	 * Returns the value of the '<em><b>Discrete Range Attribute Reference As Subtype Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscreteRangeAttributeReferenceAsSubtypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Range Attribute Reference As Subtype Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Range Attribute Reference As Subtype Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DiscreteRangeAttributeReferenceAsSubtypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discrete_range_attribute_reference_as_subtype_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscreteRangeAttributeReferenceAsSubtypeDefinition> getDiscreteRangeAttributeReferenceAsSubtypeDefinition();

	/**
	 * Returns the value of the '<em><b>Discrete Simple Expression Range As Subtype Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscreteSimpleExpressionRangeAsSubtypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Simple Expression Range As Subtype Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Simple Expression Range As Subtype Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DiscreteSimpleExpressionRangeAsSubtypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discrete_simple_expression_range_as_subtype_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscreteSimpleExpressionRangeAsSubtypeDefinition> getDiscreteSimpleExpressionRangeAsSubtypeDefinition();

	/**
	 * Returns the value of the '<em><b>Discrete Subtype Indication</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscreteSubtypeIndication}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Subtype Indication</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Subtype Indication</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DiscreteSubtypeIndication()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discrete_subtype_indication' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscreteSubtypeIndication> getDiscreteSubtypeIndication();

	/**
	 * Returns the value of the '<em><b>Discrete Range Attribute Reference</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscreteRangeAttributeReference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Range Attribute Reference</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Range Attribute Reference</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DiscreteRangeAttributeReference()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discrete_range_attribute_reference' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscreteRangeAttributeReference> getDiscreteRangeAttributeReference();

	/**
	 * Returns the value of the '<em><b>Discrete Simple Expression Range</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscreteSimpleExpressionRange}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discrete Simple Expression Range</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discrete Simple Expression Range</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DiscreteSimpleExpressionRange()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discrete_simple_expression_range' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscreteSimpleExpressionRange> getDiscreteSimpleExpressionRange();

	/**
	 * Returns the value of the '<em><b>Unknown Discriminant Part</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnknownDiscriminantPart}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unknown Discriminant Part</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unknown Discriminant Part</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_UnknownDiscriminantPart()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unknown_discriminant_part' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnknownDiscriminantPart> getUnknownDiscriminantPart();

	/**
	 * Returns the value of the '<em><b>Known Discriminant Part</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.KnownDiscriminantPart}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Known Discriminant Part</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Known Discriminant Part</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_KnownDiscriminantPart()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='known_discriminant_part' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<KnownDiscriminantPart> getKnownDiscriminantPart();

	/**
	 * Returns the value of the '<em><b>Record Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RecordDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_RecordDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='record_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RecordDefinition> getRecordDefinition();

	/**
	 * Returns the value of the '<em><b>Null Record Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NullRecordDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Null Record Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Null Record Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_NullRecordDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='null_record_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NullRecordDefinition> getNullRecordDefinition();

	/**
	 * Returns the value of the '<em><b>Null Component</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NullComponent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Null Component</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Null Component</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_NullComponent()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='null_component' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NullComponent> getNullComponent();

	/**
	 * Returns the value of the '<em><b>Variant Part</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.VariantPart}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variant Part</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variant Part</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_VariantPart()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='variant_part' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<VariantPart> getVariantPart();

	/**
	 * Returns the value of the '<em><b>Variant</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.Variant}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variant</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variant</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_Variant()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='variant' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<Variant> getVariant();

	/**
	 * Returns the value of the '<em><b>Others Choice</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OthersChoice}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Others Choice</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Others Choice</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_OthersChoice()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='others_choice' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OthersChoice> getOthersChoice();

	/**
	 * Returns the value of the '<em><b>Anonymous Access To Variable</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AnonymousAccessToVariable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Access To Variable</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Access To Variable</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AnonymousAccessToVariable()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='anonymous_access_to_variable' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AnonymousAccessToVariable> getAnonymousAccessToVariable();

	/**
	 * Returns the value of the '<em><b>Anonymous Access To Constant</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AnonymousAccessToConstant}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Access To Constant</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Access To Constant</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AnonymousAccessToConstant()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='anonymous_access_to_constant' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AnonymousAccessToConstant> getAnonymousAccessToConstant();

	/**
	 * Returns the value of the '<em><b>Anonymous Access To Procedure</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AnonymousAccessToProcedure}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Access To Procedure</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Access To Procedure</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AnonymousAccessToProcedure()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='anonymous_access_to_procedure' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AnonymousAccessToProcedure> getAnonymousAccessToProcedure();

	/**
	 * Returns the value of the '<em><b>Anonymous Access To Protected Procedure</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AnonymousAccessToProtectedProcedure}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Access To Protected Procedure</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Access To Protected Procedure</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AnonymousAccessToProtectedProcedure()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='anonymous_access_to_protected_procedure' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AnonymousAccessToProtectedProcedure> getAnonymousAccessToProtectedProcedure();

	/**
	 * Returns the value of the '<em><b>Anonymous Access To Function</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AnonymousAccessToFunction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Access To Function</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Access To Function</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AnonymousAccessToFunction()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='anonymous_access_to_function' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AnonymousAccessToFunction> getAnonymousAccessToFunction();

	/**
	 * Returns the value of the '<em><b>Anonymous Access To Protected Function</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AnonymousAccessToProtectedFunction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Anonymous Access To Protected Function</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Anonymous Access To Protected Function</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AnonymousAccessToProtectedFunction()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='anonymous_access_to_protected_function' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AnonymousAccessToProtectedFunction> getAnonymousAccessToProtectedFunction();

	/**
	 * Returns the value of the '<em><b>Private Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PrivateTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Private Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Private Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PrivateTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='private_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PrivateTypeDefinition> getPrivateTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Tagged Private Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TaggedPrivateTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tagged Private Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tagged Private Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_TaggedPrivateTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='tagged_private_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TaggedPrivateTypeDefinition> getTaggedPrivateTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Private Extension Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PrivateExtensionDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Private Extension Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Private Extension Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PrivateExtensionDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='private_extension_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PrivateExtensionDefinition> getPrivateExtensionDefinition();

	/**
	 * Returns the value of the '<em><b>Task Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TaskDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_TaskDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='task_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TaskDefinition> getTaskDefinition();

	/**
	 * Returns the value of the '<em><b>Protected Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProtectedDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protected Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protected Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ProtectedDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='protected_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProtectedDefinition> getProtectedDefinition();

	/**
	 * Returns the value of the '<em><b>Formal Private Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalPrivateTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Private Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Private Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalPrivateTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_private_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalPrivateTypeDefinition> getFormalPrivateTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Formal Tagged Private Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalTaggedPrivateTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Tagged Private Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Tagged Private Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalTaggedPrivateTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_tagged_private_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalTaggedPrivateTypeDefinition> getFormalTaggedPrivateTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Formal Derived Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalDerivedTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Derived Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Derived Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalDerivedTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_derived_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalDerivedTypeDefinition> getFormalDerivedTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Formal Discrete Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalDiscreteTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Discrete Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Discrete Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalDiscreteTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_discrete_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalDiscreteTypeDefinition> getFormalDiscreteTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Formal Signed Integer Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalSignedIntegerTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Signed Integer Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Signed Integer Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalSignedIntegerTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_signed_integer_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalSignedIntegerTypeDefinition> getFormalSignedIntegerTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Formal Modular Type Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalModularTypeDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Modular Type Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Modular Type Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalModularTypeDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_modular_type_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalModularTypeDefinition> getFormalModularTypeDefinition();

	/**
	 * Returns the value of the '<em><b>Formal Floating Point Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalFloatingPointDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Floating Point Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Floating Point Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalFloatingPointDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_floating_point_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalFloatingPointDefinition> getFormalFloatingPointDefinition();

	/**
	 * Returns the value of the '<em><b>Formal Ordinary Fixed Point Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalOrdinaryFixedPointDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Ordinary Fixed Point Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Ordinary Fixed Point Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalOrdinaryFixedPointDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_ordinary_fixed_point_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalOrdinaryFixedPointDefinition> getFormalOrdinaryFixedPointDefinition();

	/**
	 * Returns the value of the '<em><b>Formal Decimal Fixed Point Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalDecimalFixedPointDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Decimal Fixed Point Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Decimal Fixed Point Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalDecimalFixedPointDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_decimal_fixed_point_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalDecimalFixedPointDefinition> getFormalDecimalFixedPointDefinition();

	/**
	 * Returns the value of the '<em><b>Formal Ordinary Interface</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalOrdinaryInterface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Ordinary Interface</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Ordinary Interface</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalOrdinaryInterface()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_ordinary_interface' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalOrdinaryInterface> getFormalOrdinaryInterface();

	/**
	 * Returns the value of the '<em><b>Formal Limited Interface</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalLimitedInterface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Limited Interface</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Limited Interface</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalLimitedInterface()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_limited_interface' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalLimitedInterface> getFormalLimitedInterface();

	/**
	 * Returns the value of the '<em><b>Formal Task Interface</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalTaskInterface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Task Interface</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Task Interface</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalTaskInterface()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_task_interface' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalTaskInterface> getFormalTaskInterface();

	/**
	 * Returns the value of the '<em><b>Formal Protected Interface</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalProtectedInterface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Protected Interface</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Protected Interface</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalProtectedInterface()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_protected_interface' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalProtectedInterface> getFormalProtectedInterface();

	/**
	 * Returns the value of the '<em><b>Formal Synchronized Interface</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalSynchronizedInterface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Synchronized Interface</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Synchronized Interface</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalSynchronizedInterface()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_synchronized_interface' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalSynchronizedInterface> getFormalSynchronizedInterface();

	/**
	 * Returns the value of the '<em><b>Formal Unconstrained Array Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalUnconstrainedArrayDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Unconstrained Array Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Unconstrained Array Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalUnconstrainedArrayDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_unconstrained_array_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalUnconstrainedArrayDefinition> getFormalUnconstrainedArrayDefinition();

	/**
	 * Returns the value of the '<em><b>Formal Constrained Array Definition</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalConstrainedArrayDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Constrained Array Definition</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Constrained Array Definition</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalConstrainedArrayDefinition()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_constrained_array_definition' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalConstrainedArrayDefinition> getFormalConstrainedArrayDefinition();

	/**
	 * Returns the value of the '<em><b>Formal Pool Specific Access To Variable</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalPoolSpecificAccessToVariable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Pool Specific Access To Variable</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Pool Specific Access To Variable</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalPoolSpecificAccessToVariable()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_pool_specific_access_to_variable' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalPoolSpecificAccessToVariable> getFormalPoolSpecificAccessToVariable();

	/**
	 * Returns the value of the '<em><b>Formal Access To Variable</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalAccessToVariable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Access To Variable</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Access To Variable</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalAccessToVariable()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_access_to_variable' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalAccessToVariable> getFormalAccessToVariable();

	/**
	 * Returns the value of the '<em><b>Formal Access To Constant</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalAccessToConstant}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Access To Constant</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Access To Constant</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalAccessToConstant()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_access_to_constant' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalAccessToConstant> getFormalAccessToConstant();

	/**
	 * Returns the value of the '<em><b>Formal Access To Procedure</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalAccessToProcedure}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Access To Procedure</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Access To Procedure</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalAccessToProcedure()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_access_to_procedure' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalAccessToProcedure> getFormalAccessToProcedure();

	/**
	 * Returns the value of the '<em><b>Formal Access To Protected Procedure</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalAccessToProtectedProcedure}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Access To Protected Procedure</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Access To Protected Procedure</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalAccessToProtectedProcedure()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_access_to_protected_procedure' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalAccessToProtectedProcedure> getFormalAccessToProtectedProcedure();

	/**
	 * Returns the value of the '<em><b>Formal Access To Function</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalAccessToFunction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Access To Function</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Access To Function</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalAccessToFunction()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_access_to_function' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalAccessToFunction> getFormalAccessToFunction();

	/**
	 * Returns the value of the '<em><b>Formal Access To Protected Function</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalAccessToProtectedFunction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Access To Protected Function</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Access To Protected Function</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FormalAccessToProtectedFunction()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_access_to_protected_function' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalAccessToProtectedFunction> getFormalAccessToProtectedFunction();

	/**
	 * Returns the value of the '<em><b>Aspect Specification</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AspectSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aspect Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aspect Specification</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AspectSpecification()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='aspect_specification' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AspectSpecification> getAspectSpecification();

	/**
	 * Returns the value of the '<em><b>Box Expression</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.BoxExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Box Expression</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Box Expression</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_BoxExpression()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='box_expression' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<BoxExpression> getBoxExpression();

	/**
	 * Returns the value of the '<em><b>Integer Literal</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IntegerLiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integer Literal</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integer Literal</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_IntegerLiteral()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='integer_literal' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IntegerLiteral> getIntegerLiteral();

	/**
	 * Returns the value of the '<em><b>Real Literal</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RealLiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Real Literal</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Real Literal</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_RealLiteral()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='real_literal' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RealLiteral> getRealLiteral();

	/**
	 * Returns the value of the '<em><b>String Literal</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.StringLiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>String Literal</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String Literal</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_StringLiteral()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='string_literal' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<StringLiteral> getStringLiteral();

	/**
	 * Returns the value of the '<em><b>Identifier</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.Identifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Identifier</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identifier</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_Identifier()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='identifier' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<Identifier> getIdentifier();

	/**
	 * Returns the value of the '<em><b>And Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AndOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>And Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>And Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AndOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='and_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AndOperator> getAndOperator();

	/**
	 * Returns the value of the '<em><b>Or Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OrOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Or Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Or Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_OrOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='or_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OrOperator> getOrOperator();

	/**
	 * Returns the value of the '<em><b>Xor Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.XorOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Xor Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Xor Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_XorOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='xor_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<XorOperator> getXorOperator();

	/**
	 * Returns the value of the '<em><b>Equal Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.EqualOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Equal Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Equal Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_EqualOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='equal_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<EqualOperator> getEqualOperator();

	/**
	 * Returns the value of the '<em><b>Not Equal Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NotEqualOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not Equal Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not Equal Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_NotEqualOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='not_equal_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NotEqualOperator> getNotEqualOperator();

	/**
	 * Returns the value of the '<em><b>Less Than Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LessThanOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Less Than Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Less Than Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_LessThanOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='less_than_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LessThanOperator> getLessThanOperator();

	/**
	 * Returns the value of the '<em><b>Less Than Or Equal Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LessThanOrEqualOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Less Than Or Equal Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Less Than Or Equal Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_LessThanOrEqualOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='less_than_or_equal_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LessThanOrEqualOperator> getLessThanOrEqualOperator();

	/**
	 * Returns the value of the '<em><b>Greater Than Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.GreaterThanOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Greater Than Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Greater Than Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_GreaterThanOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='greater_than_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<GreaterThanOperator> getGreaterThanOperator();

	/**
	 * Returns the value of the '<em><b>Greater Than Or Equal Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.GreaterThanOrEqualOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Greater Than Or Equal Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Greater Than Or Equal Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_GreaterThanOrEqualOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='greater_than_or_equal_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<GreaterThanOrEqualOperator> getGreaterThanOrEqualOperator();

	/**
	 * Returns the value of the '<em><b>Plus Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PlusOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Plus Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Plus Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PlusOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='plus_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PlusOperator> getPlusOperator();

	/**
	 * Returns the value of the '<em><b>Minus Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MinusOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Minus Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Minus Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_MinusOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='minus_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MinusOperator> getMinusOperator();

	/**
	 * Returns the value of the '<em><b>Concatenate Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ConcatenateOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Concatenate Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Concatenate Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ConcatenateOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='concatenate_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ConcatenateOperator> getConcatenateOperator();

	/**
	 * Returns the value of the '<em><b>Unary Plus Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnaryPlusOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unary Plus Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unary Plus Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_UnaryPlusOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unary_plus_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnaryPlusOperator> getUnaryPlusOperator();

	/**
	 * Returns the value of the '<em><b>Unary Minus Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnaryMinusOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unary Minus Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unary Minus Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_UnaryMinusOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unary_minus_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnaryMinusOperator> getUnaryMinusOperator();

	/**
	 * Returns the value of the '<em><b>Multiply Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MultiplyOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Multiply Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multiply Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_MultiplyOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='multiply_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MultiplyOperator> getMultiplyOperator();

	/**
	 * Returns the value of the '<em><b>Divide Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DivideOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Divide Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Divide Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DivideOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='divide_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DivideOperator> getDivideOperator();

	/**
	 * Returns the value of the '<em><b>Mod Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ModOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mod Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mod Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ModOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='mod_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ModOperator> getModOperator();

	/**
	 * Returns the value of the '<em><b>Rem Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RemOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rem Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rem Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_RemOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='rem_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RemOperator> getRemOperator();

	/**
	 * Returns the value of the '<em><b>Exponentiate Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExponentiateOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exponentiate Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exponentiate Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ExponentiateOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='exponentiate_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExponentiateOperator> getExponentiateOperator();

	/**
	 * Returns the value of the '<em><b>Abs Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AbsOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abs Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abs Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AbsOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='abs_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AbsOperator> getAbsOperator();

	/**
	 * Returns the value of the '<em><b>Not Operator</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NotOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not Operator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not Operator</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_NotOperator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='not_operator' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NotOperator> getNotOperator();

	/**
	 * Returns the value of the '<em><b>Character Literal</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CharacterLiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Character Literal</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Character Literal</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_CharacterLiteral()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='character_literal' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CharacterLiteral> getCharacterLiteral();

	/**
	 * Returns the value of the '<em><b>Enumeration Literal</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.EnumerationLiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumeration Literal</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumeration Literal</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_EnumerationLiteral()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='enumeration_literal' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<EnumerationLiteral> getEnumerationLiteral();

	/**
	 * Returns the value of the '<em><b>Explicit Dereference</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExplicitDereference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Explicit Dereference</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Explicit Dereference</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ExplicitDereference()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='explicit_dereference' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExplicitDereference> getExplicitDereference();

	/**
	 * Returns the value of the '<em><b>Function Call</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FunctionCall}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Call</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Call</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FunctionCall()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='function_call' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FunctionCall> getFunctionCall();

	/**
	 * Returns the value of the '<em><b>Indexed Component</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IndexedComponent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Indexed Component</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Indexed Component</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_IndexedComponent()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='indexed_component' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IndexedComponent> getIndexedComponent();

	/**
	 * Returns the value of the '<em><b>Slice</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.Slice}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Slice</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Slice</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_Slice()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='slice' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<Slice> getSlice();

	/**
	 * Returns the value of the '<em><b>Selected Component</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SelectedComponent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selected Component</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selected Component</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_SelectedComponent()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='selected_component' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SelectedComponent> getSelectedComponent();

	/**
	 * Returns the value of the '<em><b>Access Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AccessAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AccessAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='access_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AccessAttribute> getAccessAttribute();

	/**
	 * Returns the value of the '<em><b>Address Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AddressAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Address Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Address Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AddressAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='address_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AddressAttribute> getAddressAttribute();

	/**
	 * Returns the value of the '<em><b>Adjacent Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AdjacentAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Adjacent Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Adjacent Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AdjacentAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='adjacent_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AdjacentAttribute> getAdjacentAttribute();

	/**
	 * Returns the value of the '<em><b>Aft Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AftAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aft Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aft Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AftAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='aft_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AftAttribute> getAftAttribute();

	/**
	 * Returns the value of the '<em><b>Alignment Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AlignmentAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alignment Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alignment Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AlignmentAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='alignment_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AlignmentAttribute> getAlignmentAttribute();

	/**
	 * Returns the value of the '<em><b>Base Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.BaseAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_BaseAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='base_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<BaseAttribute> getBaseAttribute();

	/**
	 * Returns the value of the '<em><b>Bit Order Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.BitOrderAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bit Order Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bit Order Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_BitOrderAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='bit_order_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<BitOrderAttribute> getBitOrderAttribute();

	/**
	 * Returns the value of the '<em><b>Body Version Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.BodyVersionAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body Version Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body Version Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_BodyVersionAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='body_version_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<BodyVersionAttribute> getBodyVersionAttribute();

	/**
	 * Returns the value of the '<em><b>Callable Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CallableAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Callable Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Callable Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_CallableAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='callable_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CallableAttribute> getCallableAttribute();

	/**
	 * Returns the value of the '<em><b>Caller Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CallerAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Caller Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Caller Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_CallerAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='caller_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CallerAttribute> getCallerAttribute();

	/**
	 * Returns the value of the '<em><b>Ceiling Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CeilingAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ceiling Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ceiling Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_CeilingAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ceiling_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CeilingAttribute> getCeilingAttribute();

	/**
	 * Returns the value of the '<em><b>Class Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ClassAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ClassAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='class_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ClassAttribute> getClassAttribute();

	/**
	 * Returns the value of the '<em><b>Component Size Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ComponentSizeAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Size Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Size Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ComponentSizeAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='component_size_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ComponentSizeAttribute> getComponentSizeAttribute();

	/**
	 * Returns the value of the '<em><b>Compose Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ComposeAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Compose Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Compose Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ComposeAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='compose_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ComposeAttribute> getComposeAttribute();

	/**
	 * Returns the value of the '<em><b>Constrained Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ConstrainedAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constrained Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constrained Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ConstrainedAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='constrained_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ConstrainedAttribute> getConstrainedAttribute();

	/**
	 * Returns the value of the '<em><b>Copy Sign Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CopySignAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Copy Sign Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Copy Sign Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_CopySignAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='copy_sign_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CopySignAttribute> getCopySignAttribute();

	/**
	 * Returns the value of the '<em><b>Count Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CountAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Count Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Count Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_CountAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='count_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CountAttribute> getCountAttribute();

	/**
	 * Returns the value of the '<em><b>Definite Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiniteAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Definite Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definite Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefiniteAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='definite_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiniteAttribute> getDefiniteAttribute();

	/**
	 * Returns the value of the '<em><b>Delta Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DeltaAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delta Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delta Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DeltaAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='delta_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DeltaAttribute> getDeltaAttribute();

	/**
	 * Returns the value of the '<em><b>Denorm Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DenormAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Denorm Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Denorm Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DenormAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='denorm_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DenormAttribute> getDenormAttribute();

	/**
	 * Returns the value of the '<em><b>Digits Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DigitsAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Digits Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Digits Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DigitsAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='digits_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DigitsAttribute> getDigitsAttribute();

	/**
	 * Returns the value of the '<em><b>Exponent Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExponentAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exponent Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exponent Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ExponentAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='exponent_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExponentAttribute> getExponentAttribute();

	/**
	 * Returns the value of the '<em><b>External Tag Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExternalTagAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External Tag Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Tag Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ExternalTagAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='external_tag_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExternalTagAttribute> getExternalTagAttribute();

	/**
	 * Returns the value of the '<em><b>First Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FirstAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FirstAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='first_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FirstAttribute> getFirstAttribute();

	/**
	 * Returns the value of the '<em><b>First Bit Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FirstBitAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Bit Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Bit Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FirstBitAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='first_bit_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FirstBitAttribute> getFirstBitAttribute();

	/**
	 * Returns the value of the '<em><b>Floor Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FloorAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Floor Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Floor Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FloorAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='floor_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FloorAttribute> getFloorAttribute();

	/**
	 * Returns the value of the '<em><b>Fore Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ForeAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fore Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fore Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ForeAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='fore_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ForeAttribute> getForeAttribute();

	/**
	 * Returns the value of the '<em><b>Fraction Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FractionAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fraction Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fraction Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_FractionAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='fraction_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FractionAttribute> getFractionAttribute();

	/**
	 * Returns the value of the '<em><b>Identity Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IdentityAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Identity Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identity Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_IdentityAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='identity_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IdentityAttribute> getIdentityAttribute();

	/**
	 * Returns the value of the '<em><b>Image Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ImageAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Image Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Image Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ImageAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='image_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ImageAttribute> getImageAttribute();

	/**
	 * Returns the value of the '<em><b>Input Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InputAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_InputAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='input_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InputAttribute> getInputAttribute();

	/**
	 * Returns the value of the '<em><b>Last Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LastAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_LastAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='last_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LastAttribute> getLastAttribute();

	/**
	 * Returns the value of the '<em><b>Last Bit Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LastBitAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Bit Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Bit Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_LastBitAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='last_bit_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LastBitAttribute> getLastBitAttribute();

	/**
	 * Returns the value of the '<em><b>Leading Part Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LeadingPartAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Leading Part Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Leading Part Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_LeadingPartAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='leading_part_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LeadingPartAttribute> getLeadingPartAttribute();

	/**
	 * Returns the value of the '<em><b>Length Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LengthAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Length Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Length Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_LengthAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='length_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LengthAttribute> getLengthAttribute();

	/**
	 * Returns the value of the '<em><b>Machine Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MachineAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_MachineAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='machine_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MachineAttribute> getMachineAttribute();

	/**
	 * Returns the value of the '<em><b>Machine Emax Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MachineEmaxAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Emax Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Emax Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_MachineEmaxAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='machine_emax_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MachineEmaxAttribute> getMachineEmaxAttribute();

	/**
	 * Returns the value of the '<em><b>Machine Emin Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MachineEminAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Emin Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Emin Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_MachineEminAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='machine_emin_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MachineEminAttribute> getMachineEminAttribute();

	/**
	 * Returns the value of the '<em><b>Machine Mantissa Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MachineMantissaAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Mantissa Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Mantissa Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_MachineMantissaAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='machine_mantissa_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MachineMantissaAttribute> getMachineMantissaAttribute();

	/**
	 * Returns the value of the '<em><b>Machine Overflows Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MachineOverflowsAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Overflows Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Overflows Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_MachineOverflowsAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='machine_overflows_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MachineOverflowsAttribute> getMachineOverflowsAttribute();

	/**
	 * Returns the value of the '<em><b>Machine Radix Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MachineRadixAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Radix Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Radix Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_MachineRadixAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='machine_radix_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MachineRadixAttribute> getMachineRadixAttribute();

	/**
	 * Returns the value of the '<em><b>Machine Rounds Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MachineRoundsAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Rounds Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Rounds Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_MachineRoundsAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='machine_rounds_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MachineRoundsAttribute> getMachineRoundsAttribute();

	/**
	 * Returns the value of the '<em><b>Max Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MaxAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_MaxAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='max_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MaxAttribute> getMaxAttribute();

	/**
	 * Returns the value of the '<em><b>Max Size In Storage Elements Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MaxSizeInStorageElementsAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Size In Storage Elements Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Size In Storage Elements Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_MaxSizeInStorageElementsAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='max_size_in_storage_elements_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MaxSizeInStorageElementsAttribute> getMaxSizeInStorageElementsAttribute();

	/**
	 * Returns the value of the '<em><b>Min Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MinAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_MinAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='min_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MinAttribute> getMinAttribute();

	/**
	 * Returns the value of the '<em><b>Model Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ModelAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ModelAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='model_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ModelAttribute> getModelAttribute();

	/**
	 * Returns the value of the '<em><b>Model Emin Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ModelEminAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Emin Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Emin Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ModelEminAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='model_emin_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ModelEminAttribute> getModelEminAttribute();

	/**
	 * Returns the value of the '<em><b>Model Epsilon Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ModelEpsilonAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Epsilon Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Epsilon Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ModelEpsilonAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='model_epsilon_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ModelEpsilonAttribute> getModelEpsilonAttribute();

	/**
	 * Returns the value of the '<em><b>Model Mantissa Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ModelMantissaAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Mantissa Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Mantissa Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ModelMantissaAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='model_mantissa_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ModelMantissaAttribute> getModelMantissaAttribute();

	/**
	 * Returns the value of the '<em><b>Model Small Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ModelSmallAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Small Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Small Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ModelSmallAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='model_small_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ModelSmallAttribute> getModelSmallAttribute();

	/**
	 * Returns the value of the '<em><b>Modulus Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ModulusAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Modulus Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Modulus Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ModulusAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='modulus_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ModulusAttribute> getModulusAttribute();

	/**
	 * Returns the value of the '<em><b>Output Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OutputAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_OutputAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='output_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OutputAttribute> getOutputAttribute();

	/**
	 * Returns the value of the '<em><b>Partition Id Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PartitionIdAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partition Id Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partition Id Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PartitionIdAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='partition_id_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PartitionIdAttribute> getPartitionIdAttribute();

	/**
	 * Returns the value of the '<em><b>Pos Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PosAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pos Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pos Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PosAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='pos_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PosAttribute> getPosAttribute();

	/**
	 * Returns the value of the '<em><b>Position Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PositionAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Position Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PositionAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='position_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PositionAttribute> getPositionAttribute();

	/**
	 * Returns the value of the '<em><b>Pred Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PredAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pred Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pred Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PredAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='pred_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PredAttribute> getPredAttribute();

	/**
	 * Returns the value of the '<em><b>Range Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RangeAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Range Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_RangeAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='range_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RangeAttribute> getRangeAttribute();

	/**
	 * Returns the value of the '<em><b>Read Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ReadAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Read Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Read Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ReadAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='read_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ReadAttribute> getReadAttribute();

	/**
	 * Returns the value of the '<em><b>Remainder Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RemainderAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remainder Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remainder Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_RemainderAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='remainder_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RemainderAttribute> getRemainderAttribute();

	/**
	 * Returns the value of the '<em><b>Round Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RoundAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Round Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Round Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_RoundAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='round_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RoundAttribute> getRoundAttribute();

	/**
	 * Returns the value of the '<em><b>Rounding Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RoundingAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rounding Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rounding Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_RoundingAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='rounding_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RoundingAttribute> getRoundingAttribute();

	/**
	 * Returns the value of the '<em><b>Safe First Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SafeFirstAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Safe First Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Safe First Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_SafeFirstAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='safe_first_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SafeFirstAttribute> getSafeFirstAttribute();

	/**
	 * Returns the value of the '<em><b>Safe Last Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SafeLastAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Safe Last Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Safe Last Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_SafeLastAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='safe_last_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SafeLastAttribute> getSafeLastAttribute();

	/**
	 * Returns the value of the '<em><b>Scale Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ScaleAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scale Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scale Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ScaleAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='scale_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ScaleAttribute> getScaleAttribute();

	/**
	 * Returns the value of the '<em><b>Scaling Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ScalingAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scaling Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scaling Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ScalingAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='scaling_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ScalingAttribute> getScalingAttribute();

	/**
	 * Returns the value of the '<em><b>Signed Zeros Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SignedZerosAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signed Zeros Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signed Zeros Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_SignedZerosAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='signed_zeros_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SignedZerosAttribute> getSignedZerosAttribute();

	/**
	 * Returns the value of the '<em><b>Size Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SizeAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Size Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Size Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_SizeAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='size_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SizeAttribute> getSizeAttribute();

	/**
	 * Returns the value of the '<em><b>Small Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SmallAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Small Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Small Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_SmallAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='small_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SmallAttribute> getSmallAttribute();

	/**
	 * Returns the value of the '<em><b>Storage Pool Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.StoragePoolAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Pool Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Pool Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_StoragePoolAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='storage_pool_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<StoragePoolAttribute> getStoragePoolAttribute();

	/**
	 * Returns the value of the '<em><b>Storage Size Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.StorageSizeAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Size Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Size Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_StorageSizeAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='storage_size_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<StorageSizeAttribute> getStorageSizeAttribute();

	/**
	 * Returns the value of the '<em><b>Succ Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SuccAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Succ Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Succ Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_SuccAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='succ_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SuccAttribute> getSuccAttribute();

	/**
	 * Returns the value of the '<em><b>Tag Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TagAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tag Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tag Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_TagAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='tag_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TagAttribute> getTagAttribute();

	/**
	 * Returns the value of the '<em><b>Terminated Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TerminatedAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Terminated Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Terminated Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_TerminatedAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='terminated_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TerminatedAttribute> getTerminatedAttribute();

	/**
	 * Returns the value of the '<em><b>Truncation Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TruncationAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Truncation Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Truncation Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_TruncationAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='truncation_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TruncationAttribute> getTruncationAttribute();

	/**
	 * Returns the value of the '<em><b>Unbiased Rounding Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnbiasedRoundingAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unbiased Rounding Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unbiased Rounding Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_UnbiasedRoundingAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unbiased_rounding_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnbiasedRoundingAttribute> getUnbiasedRoundingAttribute();

	/**
	 * Returns the value of the '<em><b>Unchecked Access Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UncheckedAccessAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unchecked Access Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unchecked Access Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_UncheckedAccessAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unchecked_access_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UncheckedAccessAttribute> getUncheckedAccessAttribute();

	/**
	 * Returns the value of the '<em><b>Val Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ValAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Val Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Val Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ValAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='val_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ValAttribute> getValAttribute();

	/**
	 * Returns the value of the '<em><b>Valid Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ValidAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Valid Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Valid Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ValidAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='valid_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ValidAttribute> getValidAttribute();

	/**
	 * Returns the value of the '<em><b>Value Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ValueAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ValueAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='value_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ValueAttribute> getValueAttribute();

	/**
	 * Returns the value of the '<em><b>Version Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.VersionAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_VersionAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='version_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<VersionAttribute> getVersionAttribute();

	/**
	 * Returns the value of the '<em><b>Wide Image Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WideImageAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Image Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Image Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_WideImageAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='wide_image_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WideImageAttribute> getWideImageAttribute();

	/**
	 * Returns the value of the '<em><b>Wide Value Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WideValueAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Value Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Value Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_WideValueAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='wide_value_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WideValueAttribute> getWideValueAttribute();

	/**
	 * Returns the value of the '<em><b>Wide Width Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WideWidthAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Width Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Width Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_WideWidthAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='wide_width_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WideWidthAttribute> getWideWidthAttribute();

	/**
	 * Returns the value of the '<em><b>Width Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WidthAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Width Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Width Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_WidthAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='width_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WidthAttribute> getWidthAttribute();

	/**
	 * Returns the value of the '<em><b>Write Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WriteAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Write Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Write Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_WriteAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='write_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WriteAttribute> getWriteAttribute();

	/**
	 * Returns the value of the '<em><b>Machine Rounding Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MachineRoundingAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Rounding Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Rounding Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_MachineRoundingAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='machine_rounding_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MachineRoundingAttribute> getMachineRoundingAttribute();

	/**
	 * Returns the value of the '<em><b>Mod Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ModAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mod Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mod Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ModAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='mod_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ModAttribute> getModAttribute();

	/**
	 * Returns the value of the '<em><b>Priority Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PriorityAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PriorityAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='priority_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PriorityAttribute> getPriorityAttribute();

	/**
	 * Returns the value of the '<em><b>Stream Size Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.StreamSizeAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stream Size Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stream Size Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_StreamSizeAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='stream_size_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<StreamSizeAttribute> getStreamSizeAttribute();

	/**
	 * Returns the value of the '<em><b>Wide Wide Image Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WideWideImageAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Wide Image Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Wide Image Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_WideWideImageAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='wide_wide_image_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WideWideImageAttribute> getWideWideImageAttribute();

	/**
	 * Returns the value of the '<em><b>Wide Wide Value Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WideWideValueAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Wide Value Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Wide Value Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_WideWideValueAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='wide_wide_value_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WideWideValueAttribute> getWideWideValueAttribute();

	/**
	 * Returns the value of the '<em><b>Wide Wide Width Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WideWideWidthAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Wide Width Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Wide Width Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_WideWideWidthAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='wide_wide_width_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WideWideWidthAttribute> getWideWideWidthAttribute();

	/**
	 * Returns the value of the '<em><b>Max Alignment For Allocation Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MaxAlignmentForAllocationAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Alignment For Allocation Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Alignment For Allocation Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_MaxAlignmentForAllocationAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='max_alignment_for_allocation_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MaxAlignmentForAllocationAttribute> getMaxAlignmentForAllocationAttribute();

	/**
	 * Returns the value of the '<em><b>Overlaps Storage Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OverlapsStorageAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overlaps Storage Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overlaps Storage Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_OverlapsStorageAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='overlaps_storage_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OverlapsStorageAttribute> getOverlapsStorageAttribute();

	/**
	 * Returns the value of the '<em><b>Implementation Defined Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ImplementationDefinedAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implementation Defined Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implementation Defined Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ImplementationDefinedAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='implementation_defined_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ImplementationDefinedAttribute> getImplementationDefinedAttribute();

	/**
	 * Returns the value of the '<em><b>Unknown Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnknownAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unknown Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unknown Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_UnknownAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unknown_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnknownAttribute> getUnknownAttribute();

	/**
	 * Returns the value of the '<em><b>Record Aggregate</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RecordAggregate}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Aggregate</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Aggregate</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_RecordAggregate()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='record_aggregate' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RecordAggregate> getRecordAggregate();

	/**
	 * Returns the value of the '<em><b>Extension Aggregate</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExtensionAggregate}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extension Aggregate</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extension Aggregate</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ExtensionAggregate()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='extension_aggregate' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExtensionAggregate> getExtensionAggregate();

	/**
	 * Returns the value of the '<em><b>Positional Array Aggregate</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PositionalArrayAggregate}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Positional Array Aggregate</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Positional Array Aggregate</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PositionalArrayAggregate()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='positional_array_aggregate' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PositionalArrayAggregate> getPositionalArrayAggregate();

	/**
	 * Returns the value of the '<em><b>Named Array Aggregate</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NamedArrayAggregate}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Named Array Aggregate</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Named Array Aggregate</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_NamedArrayAggregate()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='named_array_aggregate' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NamedArrayAggregate> getNamedArrayAggregate();

	/**
	 * Returns the value of the '<em><b>And Then Short Circuit</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AndThenShortCircuit}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>And Then Short Circuit</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>And Then Short Circuit</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AndThenShortCircuit()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='and_then_short_circuit' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AndThenShortCircuit> getAndThenShortCircuit();

	/**
	 * Returns the value of the '<em><b>Or Else Short Circuit</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OrElseShortCircuit}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Or Else Short Circuit</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Or Else Short Circuit</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_OrElseShortCircuit()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='or_else_short_circuit' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OrElseShortCircuit> getOrElseShortCircuit();

	/**
	 * Returns the value of the '<em><b>In Membership Test</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InMembershipTest}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In Membership Test</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In Membership Test</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_InMembershipTest()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='in_membership_test' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InMembershipTest> getInMembershipTest();

	/**
	 * Returns the value of the '<em><b>Not In Membership Test</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NotInMembershipTest}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not In Membership Test</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not In Membership Test</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_NotInMembershipTest()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='not_in_membership_test' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NotInMembershipTest> getNotInMembershipTest();

	/**
	 * Returns the value of the '<em><b>Null Literal</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NullLiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Null Literal</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Null Literal</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_NullLiteral()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='null_literal' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NullLiteral> getNullLiteral();

	/**
	 * Returns the value of the '<em><b>Parenthesized Expression</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ParenthesizedExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parenthesized Expression</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parenthesized Expression</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ParenthesizedExpression()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='parenthesized_expression' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ParenthesizedExpression> getParenthesizedExpression();

	/**
	 * Returns the value of the '<em><b>Raise Expression</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RaiseExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Raise Expression</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Raise Expression</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_RaiseExpression()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='raise_expression' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RaiseExpression> getRaiseExpression();

	/**
	 * Returns the value of the '<em><b>Type Conversion</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TypeConversion}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Conversion</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Conversion</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_TypeConversion()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='type_conversion' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TypeConversion> getTypeConversion();

	/**
	 * Returns the value of the '<em><b>Qualified Expression</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.QualifiedExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Qualified Expression</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Qualified Expression</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_QualifiedExpression()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='qualified_expression' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<QualifiedExpression> getQualifiedExpression();

	/**
	 * Returns the value of the '<em><b>Allocation From Subtype</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AllocationFromSubtype}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Allocation From Subtype</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Allocation From Subtype</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AllocationFromSubtype()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='allocation_from_subtype' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AllocationFromSubtype> getAllocationFromSubtype();

	/**
	 * Returns the value of the '<em><b>Allocation From Qualified Expression</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AllocationFromQualifiedExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Allocation From Qualified Expression</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Allocation From Qualified Expression</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AllocationFromQualifiedExpression()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='allocation_from_qualified_expression' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AllocationFromQualifiedExpression> getAllocationFromQualifiedExpression();

	/**
	 * Returns the value of the '<em><b>Case Expression</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CaseExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Case Expression</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Case Expression</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_CaseExpression()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='case_expression' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CaseExpression> getCaseExpression();

	/**
	 * Returns the value of the '<em><b>If Expression</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IfExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>If Expression</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>If Expression</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_IfExpression()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='if_expression' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IfExpression> getIfExpression();

	/**
	 * Returns the value of the '<em><b>For All Quantified Expression</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ForAllQuantifiedExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>For All Quantified Expression</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>For All Quantified Expression</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ForAllQuantifiedExpression()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='for_all_quantified_expression' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ForAllQuantifiedExpression> getForAllQuantifiedExpression();

	/**
	 * Returns the value of the '<em><b>For Some Quantified Expression</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ForSomeQuantifiedExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>For Some Quantified Expression</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>For Some Quantified Expression</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ForSomeQuantifiedExpression()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='for_some_quantified_expression' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ForSomeQuantifiedExpression> getForSomeQuantifiedExpression();

	/**
	 * Returns the value of the '<em><b>Pragma Argument Association</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PragmaArgumentAssociation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pragma Argument Association</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pragma Argument Association</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PragmaArgumentAssociation()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='pragma_argument_association' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PragmaArgumentAssociation> getPragmaArgumentAssociation();

	/**
	 * Returns the value of the '<em><b>Discriminant Association</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscriminantAssociation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discriminant Association</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discriminant Association</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DiscriminantAssociation()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discriminant_association' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscriminantAssociation> getDiscriminantAssociation();

	/**
	 * Returns the value of the '<em><b>Record Component Association</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RecordComponentAssociation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Component Association</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Component Association</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_RecordComponentAssociation()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='record_component_association' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RecordComponentAssociation> getRecordComponentAssociation();

	/**
	 * Returns the value of the '<em><b>Array Component Association</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ArrayComponentAssociation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Array Component Association</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Array Component Association</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ArrayComponentAssociation()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='array_component_association' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ArrayComponentAssociation> getArrayComponentAssociation();

	/**
	 * Returns the value of the '<em><b>Parameter Association</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ParameterAssociation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Association</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Association</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ParameterAssociation()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='parameter_association' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ParameterAssociation> getParameterAssociation();

	/**
	 * Returns the value of the '<em><b>Generic Association</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.GenericAssociation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Association</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Association</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_GenericAssociation()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='generic_association' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<GenericAssociation> getGenericAssociation();

	/**
	 * Returns the value of the '<em><b>Null Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NullStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Null Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Null Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_NullStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='null_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NullStatement> getNullStatement();

	/**
	 * Returns the value of the '<em><b>Assignment Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AssignmentStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assignment Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assignment Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AssignmentStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='assignment_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AssignmentStatement> getAssignmentStatement();

	/**
	 * Returns the value of the '<em><b>If Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IfStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>If Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>If Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_IfStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='if_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IfStatement> getIfStatement();

	/**
	 * Returns the value of the '<em><b>Case Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CaseStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Case Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Case Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_CaseStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='case_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CaseStatement> getCaseStatement();

	/**
	 * Returns the value of the '<em><b>Loop Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LoopStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Loop Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Loop Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_LoopStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='loop_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LoopStatement> getLoopStatement();

	/**
	 * Returns the value of the '<em><b>While Loop Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WhileLoopStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>While Loop Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>While Loop Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_WhileLoopStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='while_loop_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WhileLoopStatement> getWhileLoopStatement();

	/**
	 * Returns the value of the '<em><b>For Loop Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ForLoopStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>For Loop Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>For Loop Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ForLoopStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='for_loop_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ForLoopStatement> getForLoopStatement();

	/**
	 * Returns the value of the '<em><b>Block Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.BlockStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Block Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Block Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_BlockStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='block_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<BlockStatement> getBlockStatement();

	/**
	 * Returns the value of the '<em><b>Exit Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExitStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exit Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exit Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ExitStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='exit_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExitStatement> getExitStatement();

	/**
	 * Returns the value of the '<em><b>Goto Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.GotoStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Goto Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Goto Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_GotoStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='goto_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<GotoStatement> getGotoStatement();

	/**
	 * Returns the value of the '<em><b>Procedure Call Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProcedureCallStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure Call Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure Call Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ProcedureCallStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='procedure_call_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProcedureCallStatement> getProcedureCallStatement();

	/**
	 * Returns the value of the '<em><b>Return Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ReturnStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Return Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Return Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ReturnStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='return_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ReturnStatement> getReturnStatement();

	/**
	 * Returns the value of the '<em><b>Extended Return Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExtendedReturnStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extended Return Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extended Return Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ExtendedReturnStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='extended_return_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExtendedReturnStatement> getExtendedReturnStatement();

	/**
	 * Returns the value of the '<em><b>Accept Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AcceptStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Accept Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Accept Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AcceptStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='accept_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AcceptStatement> getAcceptStatement();

	/**
	 * Returns the value of the '<em><b>Entry Call Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.EntryCallStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry Call Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry Call Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_EntryCallStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='entry_call_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<EntryCallStatement> getEntryCallStatement();

	/**
	 * Returns the value of the '<em><b>Requeue Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RequeueStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requeue Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requeue Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_RequeueStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='requeue_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RequeueStatement> getRequeueStatement();

	/**
	 * Returns the value of the '<em><b>Requeue Statement With Abort</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RequeueStatementWithAbort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requeue Statement With Abort</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requeue Statement With Abort</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_RequeueStatementWithAbort()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='requeue_statement_with_abort' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RequeueStatementWithAbort> getRequeueStatementWithAbort();

	/**
	 * Returns the value of the '<em><b>Delay Until Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DelayUntilStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delay Until Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delay Until Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DelayUntilStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='delay_until_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DelayUntilStatement> getDelayUntilStatement();

	/**
	 * Returns the value of the '<em><b>Delay Relative Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DelayRelativeStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delay Relative Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delay Relative Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DelayRelativeStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='delay_relative_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DelayRelativeStatement> getDelayRelativeStatement();

	/**
	 * Returns the value of the '<em><b>Terminate Alternative Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TerminateAlternativeStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Terminate Alternative Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Terminate Alternative Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_TerminateAlternativeStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='terminate_alternative_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TerminateAlternativeStatement> getTerminateAlternativeStatement();

	/**
	 * Returns the value of the '<em><b>Selective Accept Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SelectiveAcceptStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selective Accept Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selective Accept Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_SelectiveAcceptStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='selective_accept_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SelectiveAcceptStatement> getSelectiveAcceptStatement();

	/**
	 * Returns the value of the '<em><b>Timed Entry Call Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TimedEntryCallStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timed Entry Call Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timed Entry Call Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_TimedEntryCallStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='timed_entry_call_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TimedEntryCallStatement> getTimedEntryCallStatement();

	/**
	 * Returns the value of the '<em><b>Conditional Entry Call Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ConditionalEntryCallStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conditional Entry Call Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conditional Entry Call Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ConditionalEntryCallStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='conditional_entry_call_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ConditionalEntryCallStatement> getConditionalEntryCallStatement();

	/**
	 * Returns the value of the '<em><b>Asynchronous Select Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AsynchronousSelectStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asynchronous Select Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asynchronous Select Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AsynchronousSelectStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='asynchronous_select_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AsynchronousSelectStatement> getAsynchronousSelectStatement();

	/**
	 * Returns the value of the '<em><b>Abort Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AbortStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abort Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abort Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AbortStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='abort_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AbortStatement> getAbortStatement();

	/**
	 * Returns the value of the '<em><b>Raise Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RaiseStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Raise Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Raise Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_RaiseStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='raise_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RaiseStatement> getRaiseStatement();

	/**
	 * Returns the value of the '<em><b>Code Statement</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CodeStatement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Code Statement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code Statement</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_CodeStatement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='code_statement' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CodeStatement> getCodeStatement();

	/**
	 * Returns the value of the '<em><b>If Path</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IfPath}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>If Path</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>If Path</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_IfPath()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='if_path' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IfPath> getIfPath();

	/**
	 * Returns the value of the '<em><b>Elsif Path</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElsifPath}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elsif Path</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elsif Path</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ElsifPath()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='elsif_path' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElsifPath> getElsifPath();

	/**
	 * Returns the value of the '<em><b>Else Path</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElsePath}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Else Path</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Else Path</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ElsePath()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='else_path' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElsePath> getElsePath();

	/**
	 * Returns the value of the '<em><b>Case Path</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CasePath}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Case Path</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Case Path</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_CasePath()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='case_path' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CasePath> getCasePath();

	/**
	 * Returns the value of the '<em><b>Select Path</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SelectPath}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Select Path</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Select Path</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_SelectPath()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='select_path' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SelectPath> getSelectPath();

	/**
	 * Returns the value of the '<em><b>Or Path</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OrPath}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Or Path</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Or Path</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_OrPath()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='or_path' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OrPath> getOrPath();

	/**
	 * Returns the value of the '<em><b>Then Abort Path</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ThenAbortPath}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Then Abort Path</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Then Abort Path</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ThenAbortPath()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='then_abort_path' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ThenAbortPath> getThenAbortPath();

	/**
	 * Returns the value of the '<em><b>Case Expression Path</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CaseExpressionPath}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Case Expression Path</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Case Expression Path</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_CaseExpressionPath()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='case_expression_path' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CaseExpressionPath> getCaseExpressionPath();

	/**
	 * Returns the value of the '<em><b>If Expression Path</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IfExpressionPath}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>If Expression Path</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>If Expression Path</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_IfExpressionPath()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='if_expression_path' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IfExpressionPath> getIfExpressionPath();

	/**
	 * Returns the value of the '<em><b>Elsif Expression Path</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElsifExpressionPath}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elsif Expression Path</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elsif Expression Path</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ElsifExpressionPath()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='elsif_expression_path' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElsifExpressionPath> getElsifExpressionPath();

	/**
	 * Returns the value of the '<em><b>Else Expression Path</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElseExpressionPath}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Else Expression Path</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Else Expression Path</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ElseExpressionPath()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='else_expression_path' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElseExpressionPath> getElseExpressionPath();

	/**
	 * Returns the value of the '<em><b>Use Package Clause</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UsePackageClause}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use Package Clause</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Package Clause</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_UsePackageClause()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='use_package_clause' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UsePackageClause> getUsePackageClause();

	/**
	 * Returns the value of the '<em><b>Use Type Clause</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UseTypeClause}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use Type Clause</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Type Clause</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_UseTypeClause()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='use_type_clause' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UseTypeClause> getUseTypeClause();

	/**
	 * Returns the value of the '<em><b>Use All Type Clause</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UseAllTypeClause}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use All Type Clause</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use All Type Clause</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_UseAllTypeClause()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='use_all_type_clause' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UseAllTypeClause> getUseAllTypeClause();

	/**
	 * Returns the value of the '<em><b>With Clause</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WithClause}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>With Clause</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>With Clause</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_WithClause()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='with_clause' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WithClause> getWithClause();

	/**
	 * Returns the value of the '<em><b>Attribute Definition Clause</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AttributeDefinitionClause}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute Definition Clause</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Definition Clause</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AttributeDefinitionClause()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='attribute_definition_clause' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AttributeDefinitionClause> getAttributeDefinitionClause();

	/**
	 * Returns the value of the '<em><b>Enumeration Representation Clause</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.EnumerationRepresentationClause}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumeration Representation Clause</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumeration Representation Clause</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_EnumerationRepresentationClause()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='enumeration_representation_clause' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<EnumerationRepresentationClause> getEnumerationRepresentationClause();

	/**
	 * Returns the value of the '<em><b>Record Representation Clause</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RecordRepresentationClause}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Representation Clause</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Representation Clause</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_RecordRepresentationClause()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='record_representation_clause' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RecordRepresentationClause> getRecordRepresentationClause();

	/**
	 * Returns the value of the '<em><b>At Clause</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AtClause}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>At Clause</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>At Clause</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AtClause()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='at_clause' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AtClause> getAtClause();

	/**
	 * Returns the value of the '<em><b>Component Clause</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ComponentClause}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Clause</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Clause</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ComponentClause()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='component_clause' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ComponentClause> getComponentClause();

	/**
	 * Returns the value of the '<em><b>Exception Handler</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExceptionHandler}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exception Handler</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exception Handler</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ExceptionHandler()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='exception_handler' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExceptionHandler> getExceptionHandler();

	/**
	 * Returns the value of the '<em><b>Comment</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.Comment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comment</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comment</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_Comment()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='comment' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<Comment> getComment();

	/**
	 * Returns the value of the '<em><b>All Calls Remote Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AllCallsRemotePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Calls Remote Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Calls Remote Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AllCallsRemotePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='all_calls_remote_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AllCallsRemotePragma> getAllCallsRemotePragma();

	/**
	 * Returns the value of the '<em><b>Asynchronous Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AsynchronousPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asynchronous Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asynchronous Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AsynchronousPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='asynchronous_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AsynchronousPragma> getAsynchronousPragma();

	/**
	 * Returns the value of the '<em><b>Atomic Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AtomicPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AtomicPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='atomic_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AtomicPragma> getAtomicPragma();

	/**
	 * Returns the value of the '<em><b>Atomic Components Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AtomicComponentsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Components Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Components Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AtomicComponentsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='atomic_components_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AtomicComponentsPragma> getAtomicComponentsPragma();

	/**
	 * Returns the value of the '<em><b>Attach Handler Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AttachHandlerPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attach Handler Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attach Handler Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AttachHandlerPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='attach_handler_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AttachHandlerPragma> getAttachHandlerPragma();

	/**
	 * Returns the value of the '<em><b>Controlled Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ControlledPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controlled Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controlled Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ControlledPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='controlled_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ControlledPragma> getControlledPragma();

	/**
	 * Returns the value of the '<em><b>Convention Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ConventionPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Convention Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Convention Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ConventionPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='convention_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ConventionPragma> getConventionPragma();

	/**
	 * Returns the value of the '<em><b>Discard Names Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscardNamesPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discard Names Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discard Names Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DiscardNamesPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discard_names_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscardNamesPragma> getDiscardNamesPragma();

	/**
	 * Returns the value of the '<em><b>Elaborate Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElaboratePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ElaboratePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='elaborate_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElaboratePragma> getElaboratePragma();

	/**
	 * Returns the value of the '<em><b>Elaborate All Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElaborateAllPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate All Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate All Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ElaborateAllPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='elaborate_all_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElaborateAllPragma> getElaborateAllPragma();

	/**
	 * Returns the value of the '<em><b>Elaborate Body Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElaborateBodyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Body Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Body Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ElaborateBodyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='elaborate_body_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElaborateBodyPragma> getElaborateBodyPragma();

	/**
	 * Returns the value of the '<em><b>Export Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExportPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Export Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Export Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ExportPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='export_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExportPragma> getExportPragma();

	/**
	 * Returns the value of the '<em><b>Import Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ImportPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Import Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Import Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ImportPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='import_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ImportPragma> getImportPragma();

	/**
	 * Returns the value of the '<em><b>Inline Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InlinePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inline Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inline Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_InlinePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='inline_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InlinePragma> getInlinePragma();

	/**
	 * Returns the value of the '<em><b>Inspection Point Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InspectionPointPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inspection Point Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inspection Point Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_InspectionPointPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='inspection_point_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InspectionPointPragma> getInspectionPointPragma();

	/**
	 * Returns the value of the '<em><b>Interrupt Handler Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InterruptHandlerPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Handler Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Handler Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_InterruptHandlerPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='interrupt_handler_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InterruptHandlerPragma> getInterruptHandlerPragma();

	/**
	 * Returns the value of the '<em><b>Interrupt Priority Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InterruptPriorityPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Priority Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Priority Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_InterruptPriorityPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='interrupt_priority_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InterruptPriorityPragma> getInterruptPriorityPragma();

	/**
	 * Returns the value of the '<em><b>Linker Options Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LinkerOptionsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Linker Options Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Linker Options Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_LinkerOptionsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='linker_options_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LinkerOptionsPragma> getLinkerOptionsPragma();

	/**
	 * Returns the value of the '<em><b>List Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ListPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ListPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='list_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ListPragma> getListPragma();

	/**
	 * Returns the value of the '<em><b>Locking Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LockingPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Locking Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Locking Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_LockingPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='locking_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LockingPolicyPragma> getLockingPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Normalize Scalars Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NormalizeScalarsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Normalize Scalars Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Normalize Scalars Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_NormalizeScalarsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='normalize_scalars_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NormalizeScalarsPragma> getNormalizeScalarsPragma();

	/**
	 * Returns the value of the '<em><b>Optimize Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OptimizePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Optimize Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Optimize Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_OptimizePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='optimize_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OptimizePragma> getOptimizePragma();

	/**
	 * Returns the value of the '<em><b>Pack Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PackPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pack Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pack Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PackPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='pack_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PackPragma> getPackPragma();

	/**
	 * Returns the value of the '<em><b>Page Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PagePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Page Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Page Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PagePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='page_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PagePragma> getPagePragma();

	/**
	 * Returns the value of the '<em><b>Preelaborate Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PreelaboratePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborate Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborate Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PreelaboratePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='preelaborate_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PreelaboratePragma> getPreelaboratePragma();

	/**
	 * Returns the value of the '<em><b>Priority Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PriorityPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PriorityPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='priority_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PriorityPragma> getPriorityPragma();

	/**
	 * Returns the value of the '<em><b>Pure Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PurePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pure Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pure Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PurePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='pure_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PurePragma> getPurePragma();

	/**
	 * Returns the value of the '<em><b>Queuing Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.QueuingPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Queuing Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Queuing Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_QueuingPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='queuing_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<QueuingPolicyPragma> getQueuingPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Remote Call Interface Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RemoteCallInterfacePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Call Interface Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Call Interface Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_RemoteCallInterfacePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='remote_call_interface_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RemoteCallInterfacePragma> getRemoteCallInterfacePragma();

	/**
	 * Returns the value of the '<em><b>Remote Types Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RemoteTypesPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Types Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Types Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_RemoteTypesPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='remote_types_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RemoteTypesPragma> getRemoteTypesPragma();

	/**
	 * Returns the value of the '<em><b>Restrictions Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RestrictionsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Restrictions Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Restrictions Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_RestrictionsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='restrictions_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RestrictionsPragma> getRestrictionsPragma();

	/**
	 * Returns the value of the '<em><b>Reviewable Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ReviewablePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reviewable Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reviewable Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ReviewablePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='reviewable_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ReviewablePragma> getReviewablePragma();

	/**
	 * Returns the value of the '<em><b>Shared Passive Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SharedPassivePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shared Passive Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shared Passive Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_SharedPassivePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='shared_passive_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SharedPassivePragma> getSharedPassivePragma();

	/**
	 * Returns the value of the '<em><b>Storage Size Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.StorageSizePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Size Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Size Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_StorageSizePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='storage_size_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<StorageSizePragma> getStorageSizePragma();

	/**
	 * Returns the value of the '<em><b>Suppress Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SuppressPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Suppress Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Suppress Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_SuppressPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='suppress_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SuppressPragma> getSuppressPragma();

	/**
	 * Returns the value of the '<em><b>Task Dispatching Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TaskDispatchingPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Dispatching Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Dispatching Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_TaskDispatchingPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='task_dispatching_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TaskDispatchingPolicyPragma> getTaskDispatchingPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Volatile Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.VolatilePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_VolatilePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='volatile_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<VolatilePragma> getVolatilePragma();

	/**
	 * Returns the value of the '<em><b>Volatile Components Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.VolatileComponentsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Components Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Components Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_VolatileComponentsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='volatile_components_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<VolatileComponentsPragma> getVolatileComponentsPragma();

	/**
	 * Returns the value of the '<em><b>Assert Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AssertPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assert Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assert Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AssertPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='assert_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AssertPragma> getAssertPragma();

	/**
	 * Returns the value of the '<em><b>Assertion Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AssertionPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assertion Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assertion Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_AssertionPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='assertion_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AssertionPolicyPragma> getAssertionPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Detect Blocking Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DetectBlockingPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Detect Blocking Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Detect Blocking Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DetectBlockingPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='detect_blocking_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DetectBlockingPragma> getDetectBlockingPragma();

	/**
	 * Returns the value of the '<em><b>No Return Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NoReturnPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>No Return Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>No Return Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_NoReturnPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='no_return_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NoReturnPragma> getNoReturnPragma();

	/**
	 * Returns the value of the '<em><b>Partition Elaboration Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PartitionElaborationPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partition Elaboration Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partition Elaboration Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PartitionElaborationPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='partition_elaboration_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PartitionElaborationPolicyPragma> getPartitionElaborationPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Preelaborable Initialization Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PreelaborableInitializationPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborable Initialization Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborable Initialization Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PreelaborableInitializationPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='preelaborable_initialization_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PreelaborableInitializationPragma> getPreelaborableInitializationPragma();

	/**
	 * Returns the value of the '<em><b>Priority Specific Dispatching Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PrioritySpecificDispatchingPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Specific Dispatching Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Specific Dispatching Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_PrioritySpecificDispatchingPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='priority_specific_dispatching_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PrioritySpecificDispatchingPragma> getPrioritySpecificDispatchingPragma();

	/**
	 * Returns the value of the '<em><b>Profile Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProfilePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Profile Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Profile Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ProfilePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='profile_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProfilePragma> getProfilePragma();

	/**
	 * Returns the value of the '<em><b>Relative Deadline Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RelativeDeadlinePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relative Deadline Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relative Deadline Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_RelativeDeadlinePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='relative_deadline_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RelativeDeadlinePragma> getRelativeDeadlinePragma();

	/**
	 * Returns the value of the '<em><b>Unchecked Union Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UncheckedUnionPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unchecked Union Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unchecked Union Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_UncheckedUnionPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unchecked_union_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UncheckedUnionPragma> getUncheckedUnionPragma();

	/**
	 * Returns the value of the '<em><b>Unsuppress Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnsuppressPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unsuppress Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unsuppress Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_UnsuppressPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unsuppress_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnsuppressPragma> getUnsuppressPragma();

	/**
	 * Returns the value of the '<em><b>Default Storage Pool Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefaultStoragePoolPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Storage Pool Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Storage Pool Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DefaultStoragePoolPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='default_storage_pool_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefaultStoragePoolPragma> getDefaultStoragePoolPragma();

	/**
	 * Returns the value of the '<em><b>Dispatching Domain Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DispatchingDomainPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dispatching Domain Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dispatching Domain Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_DispatchingDomainPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='dispatching_domain_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DispatchingDomainPragma> getDispatchingDomainPragma();

	/**
	 * Returns the value of the '<em><b>Cpu Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CpuPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cpu Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cpu Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_CpuPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='cpu_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CpuPragma> getCpuPragma();

	/**
	 * Returns the value of the '<em><b>Independent Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IndependentPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_IndependentPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='independent_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IndependentPragma> getIndependentPragma();

	/**
	 * Returns the value of the '<em><b>Independent Components Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IndependentComponentsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Components Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Components Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_IndependentComponentsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='independent_components_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IndependentComponentsPragma> getIndependentComponentsPragma();

	/**
	 * Returns the value of the '<em><b>Implementation Defined Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ImplementationDefinedPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implementation Defined Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implementation Defined Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_ImplementationDefinedPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='implementation_defined_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ImplementationDefinedPragma> getImplementationDefinedPragma();

	/**
	 * Returns the value of the '<em><b>Unknown Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnknownPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unknown Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unknown Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getElementList_UnknownPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unknown_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnknownPragma> getUnknownPragma();

} // ElementList
