/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Case Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.CaseExpression#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.CaseExpression#getCaseExpressionQ <em>Case Expression Q</em>}</li>
 *   <li>{@link Ada.CaseExpression#getExpressionPathsQl <em>Expression Paths Ql</em>}</li>
 *   <li>{@link Ada.CaseExpression#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.CaseExpression#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getCaseExpression()
 * @model extendedMetaData="name='Case_Expression' kind='elementOnly'"
 * @generated
 */
public interface CaseExpression extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getCaseExpression_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.CaseExpression#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Case Expression Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Case Expression Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Case Expression Q</em>' containment reference.
	 * @see #setCaseExpressionQ(ExpressionClass)
	 * @see Ada.AdaPackage#getCaseExpression_CaseExpressionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='case_expression_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getCaseExpressionQ();

	/**
	 * Sets the value of the '{@link Ada.CaseExpression#getCaseExpressionQ <em>Case Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Case Expression Q</em>' containment reference.
	 * @see #getCaseExpressionQ()
	 * @generated
	 */
	void setCaseExpressionQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Expression Paths Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression Paths Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression Paths Ql</em>' containment reference.
	 * @see #setExpressionPathsQl(ElementList)
	 * @see Ada.AdaPackage#getCaseExpression_ExpressionPathsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='expression_paths_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementList getExpressionPathsQl();

	/**
	 * Sets the value of the '{@link Ada.CaseExpression#getExpressionPathsQl <em>Expression Paths Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression Paths Ql</em>' containment reference.
	 * @see #getExpressionPathsQl()
	 * @generated
	 */
	void setExpressionPathsQl(ElementList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getCaseExpression_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.CaseExpression#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see Ada.AdaPackage#getCaseExpression_Type()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='type' namespace='##targetNamespace'"
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link Ada.CaseExpression#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

} // CaseExpression
