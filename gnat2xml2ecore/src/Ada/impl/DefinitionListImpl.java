/**
 */
package Ada.impl;

import Ada.AccessToConstant;
import Ada.AccessToFunction;
import Ada.AccessToProcedure;
import Ada.AccessToProtectedFunction;
import Ada.AccessToProtectedProcedure;
import Ada.AccessToVariable;
import Ada.AdaPackage;
import Ada.AllCallsRemotePragma;
import Ada.AnonymousAccessToConstant;
import Ada.AnonymousAccessToFunction;
import Ada.AnonymousAccessToProcedure;
import Ada.AnonymousAccessToProtectedFunction;
import Ada.AnonymousAccessToProtectedProcedure;
import Ada.AnonymousAccessToVariable;
import Ada.AspectSpecification;
import Ada.AssertPragma;
import Ada.AssertionPolicyPragma;
import Ada.AsynchronousPragma;
import Ada.AtomicComponentsPragma;
import Ada.AtomicPragma;
import Ada.AttachHandlerPragma;
import Ada.BaseAttribute;
import Ada.ClassAttribute;
import Ada.Comment;
import Ada.ComponentDefinition;
import Ada.ConstrainedArrayDefinition;
import Ada.ControlledPragma;
import Ada.ConventionPragma;
import Ada.CpuPragma;
import Ada.DecimalFixedPointDefinition;
import Ada.DefaultStoragePoolPragma;
import Ada.DefinitionList;
import Ada.DeltaConstraint;
import Ada.DerivedRecordExtensionDefinition;
import Ada.DerivedTypeDefinition;
import Ada.DetectBlockingPragma;
import Ada.DigitsConstraint;
import Ada.DiscardNamesPragma;
import Ada.DiscreteRangeAttributeReference;
import Ada.DiscreteRangeAttributeReferenceAsSubtypeDefinition;
import Ada.DiscreteSimpleExpressionRange;
import Ada.DiscreteSimpleExpressionRangeAsSubtypeDefinition;
import Ada.DiscreteSubtypeIndication;
import Ada.DiscreteSubtypeIndicationAsSubtypeDefinition;
import Ada.DiscriminantConstraint;
import Ada.DispatchingDomainPragma;
import Ada.ElaborateAllPragma;
import Ada.ElaborateBodyPragma;
import Ada.ElaboratePragma;
import Ada.EnumerationTypeDefinition;
import Ada.ExportPragma;
import Ada.FloatingPointDefinition;
import Ada.FormalAccessToConstant;
import Ada.FormalAccessToFunction;
import Ada.FormalAccessToProcedure;
import Ada.FormalAccessToProtectedFunction;
import Ada.FormalAccessToProtectedProcedure;
import Ada.FormalAccessToVariable;
import Ada.FormalConstrainedArrayDefinition;
import Ada.FormalDecimalFixedPointDefinition;
import Ada.FormalDerivedTypeDefinition;
import Ada.FormalDiscreteTypeDefinition;
import Ada.FormalFloatingPointDefinition;
import Ada.FormalLimitedInterface;
import Ada.FormalModularTypeDefinition;
import Ada.FormalOrdinaryFixedPointDefinition;
import Ada.FormalOrdinaryInterface;
import Ada.FormalPoolSpecificAccessToVariable;
import Ada.FormalPrivateTypeDefinition;
import Ada.FormalProtectedInterface;
import Ada.FormalSignedIntegerTypeDefinition;
import Ada.FormalSynchronizedInterface;
import Ada.FormalTaggedPrivateTypeDefinition;
import Ada.FormalTaskInterface;
import Ada.FormalUnconstrainedArrayDefinition;
import Ada.Identifier;
import Ada.ImplementationDefinedPragma;
import Ada.ImportPragma;
import Ada.IndependentComponentsPragma;
import Ada.IndependentPragma;
import Ada.IndexConstraint;
import Ada.InlinePragma;
import Ada.InspectionPointPragma;
import Ada.InterruptHandlerPragma;
import Ada.InterruptPriorityPragma;
import Ada.KnownDiscriminantPart;
import Ada.LimitedInterface;
import Ada.LinkerOptionsPragma;
import Ada.ListPragma;
import Ada.LockingPolicyPragma;
import Ada.ModularTypeDefinition;
import Ada.NoReturnPragma;
import Ada.NormalizeScalarsPragma;
import Ada.NotAnElement;
import Ada.NullComponent;
import Ada.NullRecordDefinition;
import Ada.OptimizePragma;
import Ada.OrdinaryFixedPointDefinition;
import Ada.OrdinaryInterface;
import Ada.OthersChoice;
import Ada.PackPragma;
import Ada.PagePragma;
import Ada.PartitionElaborationPolicyPragma;
import Ada.PoolSpecificAccessToVariable;
import Ada.PreelaborableInitializationPragma;
import Ada.PreelaboratePragma;
import Ada.PriorityPragma;
import Ada.PrioritySpecificDispatchingPragma;
import Ada.PrivateExtensionDefinition;
import Ada.PrivateTypeDefinition;
import Ada.ProfilePragma;
import Ada.ProtectedDefinition;
import Ada.ProtectedInterface;
import Ada.PurePragma;
import Ada.QueuingPolicyPragma;
import Ada.RangeAttributeReference;
import Ada.RecordDefinition;
import Ada.RecordTypeDefinition;
import Ada.RelativeDeadlinePragma;
import Ada.RemoteCallInterfacePragma;
import Ada.RemoteTypesPragma;
import Ada.RestrictionsPragma;
import Ada.ReviewablePragma;
import Ada.RootIntegerDefinition;
import Ada.RootRealDefinition;
import Ada.SelectedComponent;
import Ada.SharedPassivePragma;
import Ada.SignedIntegerTypeDefinition;
import Ada.SimpleExpressionRange;
import Ada.StorageSizePragma;
import Ada.SubtypeIndication;
import Ada.SuppressPragma;
import Ada.SynchronizedInterface;
import Ada.TaggedPrivateTypeDefinition;
import Ada.TaggedRecordTypeDefinition;
import Ada.TaskDefinition;
import Ada.TaskDispatchingPolicyPragma;
import Ada.TaskInterface;
import Ada.UncheckedUnionPragma;
import Ada.UnconstrainedArrayDefinition;
import Ada.UniversalFixedDefinition;
import Ada.UniversalIntegerDefinition;
import Ada.UniversalRealDefinition;
import Ada.UnknownDiscriminantPart;
import Ada.UnknownPragma;
import Ada.UnsuppressPragma;
import Ada.Variant;
import Ada.VariantPart;
import Ada.VolatileComponentsPragma;
import Ada.VolatilePragma;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Definition List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.DefinitionListImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getNotAnElement <em>Not An Element</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getDerivedTypeDefinition <em>Derived Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getDerivedRecordExtensionDefinition <em>Derived Record Extension Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getEnumerationTypeDefinition <em>Enumeration Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getSignedIntegerTypeDefinition <em>Signed Integer Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getModularTypeDefinition <em>Modular Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getRootIntegerDefinition <em>Root Integer Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getRootRealDefinition <em>Root Real Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getUniversalIntegerDefinition <em>Universal Integer Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getUniversalRealDefinition <em>Universal Real Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getUniversalFixedDefinition <em>Universal Fixed Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getFloatingPointDefinition <em>Floating Point Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getOrdinaryFixedPointDefinition <em>Ordinary Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getDecimalFixedPointDefinition <em>Decimal Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getUnconstrainedArrayDefinition <em>Unconstrained Array Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getConstrainedArrayDefinition <em>Constrained Array Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getRecordTypeDefinition <em>Record Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getTaggedRecordTypeDefinition <em>Tagged Record Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getOrdinaryInterface <em>Ordinary Interface</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getLimitedInterface <em>Limited Interface</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getTaskInterface <em>Task Interface</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getProtectedInterface <em>Protected Interface</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getSynchronizedInterface <em>Synchronized Interface</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getPoolSpecificAccessToVariable <em>Pool Specific Access To Variable</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getAccessToVariable <em>Access To Variable</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getAccessToConstant <em>Access To Constant</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getAccessToProcedure <em>Access To Procedure</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getAccessToProtectedProcedure <em>Access To Protected Procedure</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getAccessToFunction <em>Access To Function</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getAccessToProtectedFunction <em>Access To Protected Function</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getSubtypeIndication <em>Subtype Indication</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getRangeAttributeReference <em>Range Attribute Reference</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getSimpleExpressionRange <em>Simple Expression Range</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getDigitsConstraint <em>Digits Constraint</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getDeltaConstraint <em>Delta Constraint</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getIndexConstraint <em>Index Constraint</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getDiscriminantConstraint <em>Discriminant Constraint</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getComponentDefinition <em>Component Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getDiscreteSubtypeIndicationAsSubtypeDefinition <em>Discrete Subtype Indication As Subtype Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getDiscreteRangeAttributeReferenceAsSubtypeDefinition <em>Discrete Range Attribute Reference As Subtype Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getDiscreteSimpleExpressionRangeAsSubtypeDefinition <em>Discrete Simple Expression Range As Subtype Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getDiscreteSubtypeIndication <em>Discrete Subtype Indication</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getDiscreteRangeAttributeReference <em>Discrete Range Attribute Reference</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getDiscreteSimpleExpressionRange <em>Discrete Simple Expression Range</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getUnknownDiscriminantPart <em>Unknown Discriminant Part</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getKnownDiscriminantPart <em>Known Discriminant Part</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getRecordDefinition <em>Record Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getNullRecordDefinition <em>Null Record Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getNullComponent <em>Null Component</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getVariantPart <em>Variant Part</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getVariant <em>Variant</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getOthersChoice <em>Others Choice</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getAnonymousAccessToVariable <em>Anonymous Access To Variable</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getAnonymousAccessToConstant <em>Anonymous Access To Constant</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getAnonymousAccessToProcedure <em>Anonymous Access To Procedure</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getAnonymousAccessToProtectedProcedure <em>Anonymous Access To Protected Procedure</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getAnonymousAccessToFunction <em>Anonymous Access To Function</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getAnonymousAccessToProtectedFunction <em>Anonymous Access To Protected Function</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getPrivateTypeDefinition <em>Private Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getTaggedPrivateTypeDefinition <em>Tagged Private Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getPrivateExtensionDefinition <em>Private Extension Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getTaskDefinition <em>Task Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getProtectedDefinition <em>Protected Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getFormalPrivateTypeDefinition <em>Formal Private Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getFormalTaggedPrivateTypeDefinition <em>Formal Tagged Private Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getFormalDerivedTypeDefinition <em>Formal Derived Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getFormalDiscreteTypeDefinition <em>Formal Discrete Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getFormalSignedIntegerTypeDefinition <em>Formal Signed Integer Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getFormalModularTypeDefinition <em>Formal Modular Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getFormalFloatingPointDefinition <em>Formal Floating Point Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getFormalOrdinaryFixedPointDefinition <em>Formal Ordinary Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getFormalDecimalFixedPointDefinition <em>Formal Decimal Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getFormalOrdinaryInterface <em>Formal Ordinary Interface</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getFormalLimitedInterface <em>Formal Limited Interface</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getFormalTaskInterface <em>Formal Task Interface</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getFormalProtectedInterface <em>Formal Protected Interface</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getFormalSynchronizedInterface <em>Formal Synchronized Interface</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getFormalUnconstrainedArrayDefinition <em>Formal Unconstrained Array Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getFormalConstrainedArrayDefinition <em>Formal Constrained Array Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getFormalPoolSpecificAccessToVariable <em>Formal Pool Specific Access To Variable</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getFormalAccessToVariable <em>Formal Access To Variable</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getFormalAccessToConstant <em>Formal Access To Constant</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getFormalAccessToProcedure <em>Formal Access To Procedure</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getFormalAccessToProtectedProcedure <em>Formal Access To Protected Procedure</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getFormalAccessToFunction <em>Formal Access To Function</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getFormalAccessToProtectedFunction <em>Formal Access To Protected Function</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getAspectSpecification <em>Aspect Specification</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getSelectedComponent <em>Selected Component</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getBaseAttribute <em>Base Attribute</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getClassAttribute <em>Class Attribute</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getAsynchronousPragma <em>Asynchronous Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getAtomicPragma <em>Atomic Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getAttachHandlerPragma <em>Attach Handler Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getControlledPragma <em>Controlled Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getConventionPragma <em>Convention Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getDiscardNamesPragma <em>Discard Names Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getElaboratePragma <em>Elaborate Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getElaborateAllPragma <em>Elaborate All Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getExportPragma <em>Export Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getImportPragma <em>Import Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getInlinePragma <em>Inline Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getInspectionPointPragma <em>Inspection Point Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getLinkerOptionsPragma <em>Linker Options Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getListPragma <em>List Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getLockingPolicyPragma <em>Locking Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getOptimizePragma <em>Optimize Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getPackPragma <em>Pack Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getPagePragma <em>Page Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getPreelaboratePragma <em>Preelaborate Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getPriorityPragma <em>Priority Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getPurePragma <em>Pure Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getRemoteTypesPragma <em>Remote Types Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getRestrictionsPragma <em>Restrictions Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getReviewablePragma <em>Reviewable Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getSharedPassivePragma <em>Shared Passive Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getStorageSizePragma <em>Storage Size Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getSuppressPragma <em>Suppress Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getVolatilePragma <em>Volatile Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getAssertPragma <em>Assert Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getNoReturnPragma <em>No Return Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getProfilePragma <em>Profile Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getUnsuppressPragma <em>Unsuppress Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getCpuPragma <em>Cpu Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getIndependentPragma <em>Independent Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getIndependentComponentsPragma <em>Independent Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionListImpl#getUnknownPragma <em>Unknown Pragma</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DefinitionListImpl extends MinimalEObjectImpl.Container implements DefinitionList {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DefinitionListImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getDefinitionList();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, AdaPackage.DEFINITION_LIST__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NotAnElement> getNotAnElement() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_NotAnElement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DerivedTypeDefinition> getDerivedTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_DerivedTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DerivedRecordExtensionDefinition> getDerivedRecordExtensionDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_DerivedRecordExtensionDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EnumerationTypeDefinition> getEnumerationTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_EnumerationTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SignedIntegerTypeDefinition> getSignedIntegerTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_SignedIntegerTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModularTypeDefinition> getModularTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_ModularTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RootIntegerDefinition> getRootIntegerDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_RootIntegerDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RootRealDefinition> getRootRealDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_RootRealDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UniversalIntegerDefinition> getUniversalIntegerDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_UniversalIntegerDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UniversalRealDefinition> getUniversalRealDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_UniversalRealDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UniversalFixedDefinition> getUniversalFixedDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_UniversalFixedDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FloatingPointDefinition> getFloatingPointDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_FloatingPointDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OrdinaryFixedPointDefinition> getOrdinaryFixedPointDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_OrdinaryFixedPointDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DecimalFixedPointDefinition> getDecimalFixedPointDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_DecimalFixedPointDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnconstrainedArrayDefinition> getUnconstrainedArrayDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_UnconstrainedArrayDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConstrainedArrayDefinition> getConstrainedArrayDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_ConstrainedArrayDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RecordTypeDefinition> getRecordTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_RecordTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaggedRecordTypeDefinition> getTaggedRecordTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_TaggedRecordTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OrdinaryInterface> getOrdinaryInterface() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_OrdinaryInterface());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LimitedInterface> getLimitedInterface() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_LimitedInterface());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskInterface> getTaskInterface() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_TaskInterface());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProtectedInterface> getProtectedInterface() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_ProtectedInterface());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SynchronizedInterface> getSynchronizedInterface() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_SynchronizedInterface());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PoolSpecificAccessToVariable> getPoolSpecificAccessToVariable() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_PoolSpecificAccessToVariable());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AccessToVariable> getAccessToVariable() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_AccessToVariable());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AccessToConstant> getAccessToConstant() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_AccessToConstant());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AccessToProcedure> getAccessToProcedure() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_AccessToProcedure());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AccessToProtectedProcedure> getAccessToProtectedProcedure() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_AccessToProtectedProcedure());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AccessToFunction> getAccessToFunction() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_AccessToFunction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AccessToProtectedFunction> getAccessToProtectedFunction() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_AccessToProtectedFunction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SubtypeIndication> getSubtypeIndication() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_SubtypeIndication());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RangeAttributeReference> getRangeAttributeReference() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_RangeAttributeReference());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SimpleExpressionRange> getSimpleExpressionRange() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_SimpleExpressionRange());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DigitsConstraint> getDigitsConstraint() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_DigitsConstraint());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DeltaConstraint> getDeltaConstraint() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_DeltaConstraint());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IndexConstraint> getIndexConstraint() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_IndexConstraint());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscriminantConstraint> getDiscriminantConstraint() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_DiscriminantConstraint());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComponentDefinition> getComponentDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_ComponentDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscreteSubtypeIndicationAsSubtypeDefinition> getDiscreteSubtypeIndicationAsSubtypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_DiscreteSubtypeIndicationAsSubtypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscreteRangeAttributeReferenceAsSubtypeDefinition> getDiscreteRangeAttributeReferenceAsSubtypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_DiscreteRangeAttributeReferenceAsSubtypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscreteSimpleExpressionRangeAsSubtypeDefinition> getDiscreteSimpleExpressionRangeAsSubtypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_DiscreteSimpleExpressionRangeAsSubtypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscreteSubtypeIndication> getDiscreteSubtypeIndication() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_DiscreteSubtypeIndication());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscreteRangeAttributeReference> getDiscreteRangeAttributeReference() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_DiscreteRangeAttributeReference());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscreteSimpleExpressionRange> getDiscreteSimpleExpressionRange() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_DiscreteSimpleExpressionRange());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnknownDiscriminantPart> getUnknownDiscriminantPart() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_UnknownDiscriminantPart());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<KnownDiscriminantPart> getKnownDiscriminantPart() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_KnownDiscriminantPart());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RecordDefinition> getRecordDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_RecordDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NullRecordDefinition> getNullRecordDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_NullRecordDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NullComponent> getNullComponent() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_NullComponent());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VariantPart> getVariantPart() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_VariantPart());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Variant> getVariant() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_Variant());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OthersChoice> getOthersChoice() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_OthersChoice());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AnonymousAccessToVariable> getAnonymousAccessToVariable() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_AnonymousAccessToVariable());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AnonymousAccessToConstant> getAnonymousAccessToConstant() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_AnonymousAccessToConstant());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AnonymousAccessToProcedure> getAnonymousAccessToProcedure() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_AnonymousAccessToProcedure());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AnonymousAccessToProtectedProcedure> getAnonymousAccessToProtectedProcedure() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_AnonymousAccessToProtectedProcedure());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AnonymousAccessToFunction> getAnonymousAccessToFunction() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_AnonymousAccessToFunction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AnonymousAccessToProtectedFunction> getAnonymousAccessToProtectedFunction() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_AnonymousAccessToProtectedFunction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PrivateTypeDefinition> getPrivateTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_PrivateTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaggedPrivateTypeDefinition> getTaggedPrivateTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_TaggedPrivateTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PrivateExtensionDefinition> getPrivateExtensionDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_PrivateExtensionDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskDefinition> getTaskDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_TaskDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProtectedDefinition> getProtectedDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_ProtectedDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalPrivateTypeDefinition> getFormalPrivateTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_FormalPrivateTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalTaggedPrivateTypeDefinition> getFormalTaggedPrivateTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_FormalTaggedPrivateTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalDerivedTypeDefinition> getFormalDerivedTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_FormalDerivedTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalDiscreteTypeDefinition> getFormalDiscreteTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_FormalDiscreteTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalSignedIntegerTypeDefinition> getFormalSignedIntegerTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_FormalSignedIntegerTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalModularTypeDefinition> getFormalModularTypeDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_FormalModularTypeDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalFloatingPointDefinition> getFormalFloatingPointDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_FormalFloatingPointDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalOrdinaryFixedPointDefinition> getFormalOrdinaryFixedPointDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_FormalOrdinaryFixedPointDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalDecimalFixedPointDefinition> getFormalDecimalFixedPointDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_FormalDecimalFixedPointDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalOrdinaryInterface> getFormalOrdinaryInterface() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_FormalOrdinaryInterface());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalLimitedInterface> getFormalLimitedInterface() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_FormalLimitedInterface());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalTaskInterface> getFormalTaskInterface() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_FormalTaskInterface());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalProtectedInterface> getFormalProtectedInterface() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_FormalProtectedInterface());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalSynchronizedInterface> getFormalSynchronizedInterface() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_FormalSynchronizedInterface());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalUnconstrainedArrayDefinition> getFormalUnconstrainedArrayDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_FormalUnconstrainedArrayDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalConstrainedArrayDefinition> getFormalConstrainedArrayDefinition() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_FormalConstrainedArrayDefinition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalPoolSpecificAccessToVariable> getFormalPoolSpecificAccessToVariable() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_FormalPoolSpecificAccessToVariable());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalAccessToVariable> getFormalAccessToVariable() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_FormalAccessToVariable());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalAccessToConstant> getFormalAccessToConstant() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_FormalAccessToConstant());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalAccessToProcedure> getFormalAccessToProcedure() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_FormalAccessToProcedure());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalAccessToProtectedProcedure> getFormalAccessToProtectedProcedure() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_FormalAccessToProtectedProcedure());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalAccessToFunction> getFormalAccessToFunction() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_FormalAccessToFunction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FormalAccessToProtectedFunction> getFormalAccessToProtectedFunction() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_FormalAccessToProtectedFunction());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AspectSpecification> getAspectSpecification() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_AspectSpecification());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Identifier> getIdentifier() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_Identifier());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SelectedComponent> getSelectedComponent() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_SelectedComponent());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BaseAttribute> getBaseAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_BaseAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ClassAttribute> getClassAttribute() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_ClassAttribute());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Comment> getComment() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_Comment());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AllCallsRemotePragma> getAllCallsRemotePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_AllCallsRemotePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AsynchronousPragma> getAsynchronousPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_AsynchronousPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AtomicPragma> getAtomicPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_AtomicPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AtomicComponentsPragma> getAtomicComponentsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_AtomicComponentsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AttachHandlerPragma> getAttachHandlerPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_AttachHandlerPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlledPragma> getControlledPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_ControlledPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConventionPragma> getConventionPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_ConventionPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscardNamesPragma> getDiscardNamesPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_DiscardNamesPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElaboratePragma> getElaboratePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_ElaboratePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElaborateAllPragma> getElaborateAllPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_ElaborateAllPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElaborateBodyPragma> getElaborateBodyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_ElaborateBodyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExportPragma> getExportPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_ExportPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ImportPragma> getImportPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_ImportPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InlinePragma> getInlinePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_InlinePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InspectionPointPragma> getInspectionPointPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_InspectionPointPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InterruptHandlerPragma> getInterruptHandlerPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_InterruptHandlerPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InterruptPriorityPragma> getInterruptPriorityPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_InterruptPriorityPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LinkerOptionsPragma> getLinkerOptionsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_LinkerOptionsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ListPragma> getListPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_ListPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LockingPolicyPragma> getLockingPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_LockingPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NormalizeScalarsPragma> getNormalizeScalarsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_NormalizeScalarsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OptimizePragma> getOptimizePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_OptimizePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackPragma> getPackPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_PackPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PagePragma> getPagePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_PagePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PreelaboratePragma> getPreelaboratePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_PreelaboratePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PriorityPragma> getPriorityPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_PriorityPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PurePragma> getPurePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_PurePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QueuingPolicyPragma> getQueuingPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_QueuingPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RemoteCallInterfacePragma> getRemoteCallInterfacePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_RemoteCallInterfacePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RemoteTypesPragma> getRemoteTypesPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_RemoteTypesPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RestrictionsPragma> getRestrictionsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_RestrictionsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReviewablePragma> getReviewablePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_ReviewablePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SharedPassivePragma> getSharedPassivePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_SharedPassivePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StorageSizePragma> getStorageSizePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_StorageSizePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SuppressPragma> getSuppressPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_SuppressPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskDispatchingPolicyPragma> getTaskDispatchingPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_TaskDispatchingPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VolatilePragma> getVolatilePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_VolatilePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VolatileComponentsPragma> getVolatileComponentsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_VolatileComponentsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssertPragma> getAssertPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_AssertPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssertionPolicyPragma> getAssertionPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_AssertionPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DetectBlockingPragma> getDetectBlockingPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_DetectBlockingPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NoReturnPragma> getNoReturnPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_NoReturnPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PartitionElaborationPolicyPragma> getPartitionElaborationPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_PartitionElaborationPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PreelaborableInitializationPragma> getPreelaborableInitializationPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_PreelaborableInitializationPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PrioritySpecificDispatchingPragma> getPrioritySpecificDispatchingPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_PrioritySpecificDispatchingPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProfilePragma> getProfilePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_ProfilePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RelativeDeadlinePragma> getRelativeDeadlinePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_RelativeDeadlinePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UncheckedUnionPragma> getUncheckedUnionPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_UncheckedUnionPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnsuppressPragma> getUnsuppressPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_UnsuppressPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefaultStoragePoolPragma> getDefaultStoragePoolPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_DefaultStoragePoolPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DispatchingDomainPragma> getDispatchingDomainPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_DispatchingDomainPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CpuPragma> getCpuPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_CpuPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IndependentPragma> getIndependentPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_IndependentPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IndependentComponentsPragma> getIndependentComponentsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_IndependentComponentsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ImplementationDefinedPragma> getImplementationDefinedPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_ImplementationDefinedPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnknownPragma> getUnknownPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getDefinitionList_UnknownPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.DEFINITION_LIST__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__NOT_AN_ELEMENT:
				return ((InternalEList<?>)getNotAnElement()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__DERIVED_TYPE_DEFINITION:
				return ((InternalEList<?>)getDerivedTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__DERIVED_RECORD_EXTENSION_DEFINITION:
				return ((InternalEList<?>)getDerivedRecordExtensionDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ENUMERATION_TYPE_DEFINITION:
				return ((InternalEList<?>)getEnumerationTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__SIGNED_INTEGER_TYPE_DEFINITION:
				return ((InternalEList<?>)getSignedIntegerTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__MODULAR_TYPE_DEFINITION:
				return ((InternalEList<?>)getModularTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ROOT_INTEGER_DEFINITION:
				return ((InternalEList<?>)getRootIntegerDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ROOT_REAL_DEFINITION:
				return ((InternalEList<?>)getRootRealDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__UNIVERSAL_INTEGER_DEFINITION:
				return ((InternalEList<?>)getUniversalIntegerDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__UNIVERSAL_REAL_DEFINITION:
				return ((InternalEList<?>)getUniversalRealDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__UNIVERSAL_FIXED_DEFINITION:
				return ((InternalEList<?>)getUniversalFixedDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__FLOATING_POINT_DEFINITION:
				return ((InternalEList<?>)getFloatingPointDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ORDINARY_FIXED_POINT_DEFINITION:
				return ((InternalEList<?>)getOrdinaryFixedPointDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__DECIMAL_FIXED_POINT_DEFINITION:
				return ((InternalEList<?>)getDecimalFixedPointDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__UNCONSTRAINED_ARRAY_DEFINITION:
				return ((InternalEList<?>)getUnconstrainedArrayDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__CONSTRAINED_ARRAY_DEFINITION:
				return ((InternalEList<?>)getConstrainedArrayDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__RECORD_TYPE_DEFINITION:
				return ((InternalEList<?>)getRecordTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__TAGGED_RECORD_TYPE_DEFINITION:
				return ((InternalEList<?>)getTaggedRecordTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ORDINARY_INTERFACE:
				return ((InternalEList<?>)getOrdinaryInterface()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__LIMITED_INTERFACE:
				return ((InternalEList<?>)getLimitedInterface()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__TASK_INTERFACE:
				return ((InternalEList<?>)getTaskInterface()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__PROTECTED_INTERFACE:
				return ((InternalEList<?>)getProtectedInterface()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__SYNCHRONIZED_INTERFACE:
				return ((InternalEList<?>)getSynchronizedInterface()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				return ((InternalEList<?>)getPoolSpecificAccessToVariable()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_VARIABLE:
				return ((InternalEList<?>)getAccessToVariable()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_CONSTANT:
				return ((InternalEList<?>)getAccessToConstant()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_PROCEDURE:
				return ((InternalEList<?>)getAccessToProcedure()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_PROTECTED_PROCEDURE:
				return ((InternalEList<?>)getAccessToProtectedProcedure()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_FUNCTION:
				return ((InternalEList<?>)getAccessToFunction()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_PROTECTED_FUNCTION:
				return ((InternalEList<?>)getAccessToProtectedFunction()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__SUBTYPE_INDICATION:
				return ((InternalEList<?>)getSubtypeIndication()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__RANGE_ATTRIBUTE_REFERENCE:
				return ((InternalEList<?>)getRangeAttributeReference()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__SIMPLE_EXPRESSION_RANGE:
				return ((InternalEList<?>)getSimpleExpressionRange()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__DIGITS_CONSTRAINT:
				return ((InternalEList<?>)getDigitsConstraint()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__DELTA_CONSTRAINT:
				return ((InternalEList<?>)getDeltaConstraint()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__INDEX_CONSTRAINT:
				return ((InternalEList<?>)getIndexConstraint()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__DISCRIMINANT_CONSTRAINT:
				return ((InternalEList<?>)getDiscriminantConstraint()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__COMPONENT_DEFINITION:
				return ((InternalEList<?>)getComponentDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION:
				return ((InternalEList<?>)getDiscreteSubtypeIndicationAsSubtypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION:
				return ((InternalEList<?>)getDiscreteRangeAttributeReferenceAsSubtypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION:
				return ((InternalEList<?>)getDiscreteSimpleExpressionRangeAsSubtypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__DISCRETE_SUBTYPE_INDICATION:
				return ((InternalEList<?>)getDiscreteSubtypeIndication()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__DISCRETE_RANGE_ATTRIBUTE_REFERENCE:
				return ((InternalEList<?>)getDiscreteRangeAttributeReference()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__DISCRETE_SIMPLE_EXPRESSION_RANGE:
				return ((InternalEList<?>)getDiscreteSimpleExpressionRange()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__UNKNOWN_DISCRIMINANT_PART:
				return ((InternalEList<?>)getUnknownDiscriminantPart()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__KNOWN_DISCRIMINANT_PART:
				return ((InternalEList<?>)getKnownDiscriminantPart()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__RECORD_DEFINITION:
				return ((InternalEList<?>)getRecordDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__NULL_RECORD_DEFINITION:
				return ((InternalEList<?>)getNullRecordDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__NULL_COMPONENT:
				return ((InternalEList<?>)getNullComponent()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__VARIANT_PART:
				return ((InternalEList<?>)getVariantPart()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__VARIANT:
				return ((InternalEList<?>)getVariant()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__OTHERS_CHOICE:
				return ((InternalEList<?>)getOthersChoice()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_VARIABLE:
				return ((InternalEList<?>)getAnonymousAccessToVariable()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_CONSTANT:
				return ((InternalEList<?>)getAnonymousAccessToConstant()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_PROCEDURE:
				return ((InternalEList<?>)getAnonymousAccessToProcedure()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE:
				return ((InternalEList<?>)getAnonymousAccessToProtectedProcedure()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_FUNCTION:
				return ((InternalEList<?>)getAnonymousAccessToFunction()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION:
				return ((InternalEList<?>)getAnonymousAccessToProtectedFunction()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__PRIVATE_TYPE_DEFINITION:
				return ((InternalEList<?>)getPrivateTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__TAGGED_PRIVATE_TYPE_DEFINITION:
				return ((InternalEList<?>)getTaggedPrivateTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__PRIVATE_EXTENSION_DEFINITION:
				return ((InternalEList<?>)getPrivateExtensionDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__TASK_DEFINITION:
				return ((InternalEList<?>)getTaskDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__PROTECTED_DEFINITION:
				return ((InternalEList<?>)getProtectedDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__FORMAL_PRIVATE_TYPE_DEFINITION:
				return ((InternalEList<?>)getFormalPrivateTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION:
				return ((InternalEList<?>)getFormalTaggedPrivateTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__FORMAL_DERIVED_TYPE_DEFINITION:
				return ((InternalEList<?>)getFormalDerivedTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__FORMAL_DISCRETE_TYPE_DEFINITION:
				return ((InternalEList<?>)getFormalDiscreteTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__FORMAL_SIGNED_INTEGER_TYPE_DEFINITION:
				return ((InternalEList<?>)getFormalSignedIntegerTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__FORMAL_MODULAR_TYPE_DEFINITION:
				return ((InternalEList<?>)getFormalModularTypeDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__FORMAL_FLOATING_POINT_DEFINITION:
				return ((InternalEList<?>)getFormalFloatingPointDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__FORMAL_ORDINARY_FIXED_POINT_DEFINITION:
				return ((InternalEList<?>)getFormalOrdinaryFixedPointDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__FORMAL_DECIMAL_FIXED_POINT_DEFINITION:
				return ((InternalEList<?>)getFormalDecimalFixedPointDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__FORMAL_ORDINARY_INTERFACE:
				return ((InternalEList<?>)getFormalOrdinaryInterface()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__FORMAL_LIMITED_INTERFACE:
				return ((InternalEList<?>)getFormalLimitedInterface()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__FORMAL_TASK_INTERFACE:
				return ((InternalEList<?>)getFormalTaskInterface()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__FORMAL_PROTECTED_INTERFACE:
				return ((InternalEList<?>)getFormalProtectedInterface()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__FORMAL_SYNCHRONIZED_INTERFACE:
				return ((InternalEList<?>)getFormalSynchronizedInterface()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__FORMAL_UNCONSTRAINED_ARRAY_DEFINITION:
				return ((InternalEList<?>)getFormalUnconstrainedArrayDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__FORMAL_CONSTRAINED_ARRAY_DEFINITION:
				return ((InternalEList<?>)getFormalConstrainedArrayDefinition()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				return ((InternalEList<?>)getFormalPoolSpecificAccessToVariable()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_VARIABLE:
				return ((InternalEList<?>)getFormalAccessToVariable()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_CONSTANT:
				return ((InternalEList<?>)getFormalAccessToConstant()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_PROCEDURE:
				return ((InternalEList<?>)getFormalAccessToProcedure()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_PROTECTED_PROCEDURE:
				return ((InternalEList<?>)getFormalAccessToProtectedProcedure()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_FUNCTION:
				return ((InternalEList<?>)getFormalAccessToFunction()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_PROTECTED_FUNCTION:
				return ((InternalEList<?>)getFormalAccessToProtectedFunction()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ASPECT_SPECIFICATION:
				return ((InternalEList<?>)getAspectSpecification()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__IDENTIFIER:
				return ((InternalEList<?>)getIdentifier()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__SELECTED_COMPONENT:
				return ((InternalEList<?>)getSelectedComponent()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__BASE_ATTRIBUTE:
				return ((InternalEList<?>)getBaseAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__CLASS_ATTRIBUTE:
				return ((InternalEList<?>)getClassAttribute()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__COMMENT:
				return ((InternalEList<?>)getComment()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ALL_CALLS_REMOTE_PRAGMA:
				return ((InternalEList<?>)getAllCallsRemotePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ASYNCHRONOUS_PRAGMA:
				return ((InternalEList<?>)getAsynchronousPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ATOMIC_PRAGMA:
				return ((InternalEList<?>)getAtomicPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ATOMIC_COMPONENTS_PRAGMA:
				return ((InternalEList<?>)getAtomicComponentsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ATTACH_HANDLER_PRAGMA:
				return ((InternalEList<?>)getAttachHandlerPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__CONTROLLED_PRAGMA:
				return ((InternalEList<?>)getControlledPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__CONVENTION_PRAGMA:
				return ((InternalEList<?>)getConventionPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__DISCARD_NAMES_PRAGMA:
				return ((InternalEList<?>)getDiscardNamesPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ELABORATE_PRAGMA:
				return ((InternalEList<?>)getElaboratePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ELABORATE_ALL_PRAGMA:
				return ((InternalEList<?>)getElaborateAllPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ELABORATE_BODY_PRAGMA:
				return ((InternalEList<?>)getElaborateBodyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__EXPORT_PRAGMA:
				return ((InternalEList<?>)getExportPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__IMPORT_PRAGMA:
				return ((InternalEList<?>)getImportPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__INLINE_PRAGMA:
				return ((InternalEList<?>)getInlinePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__INSPECTION_POINT_PRAGMA:
				return ((InternalEList<?>)getInspectionPointPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__INTERRUPT_HANDLER_PRAGMA:
				return ((InternalEList<?>)getInterruptHandlerPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__INTERRUPT_PRIORITY_PRAGMA:
				return ((InternalEList<?>)getInterruptPriorityPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__LINKER_OPTIONS_PRAGMA:
				return ((InternalEList<?>)getLinkerOptionsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__LIST_PRAGMA:
				return ((InternalEList<?>)getListPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__LOCKING_POLICY_PRAGMA:
				return ((InternalEList<?>)getLockingPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__NORMALIZE_SCALARS_PRAGMA:
				return ((InternalEList<?>)getNormalizeScalarsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__OPTIMIZE_PRAGMA:
				return ((InternalEList<?>)getOptimizePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__PACK_PRAGMA:
				return ((InternalEList<?>)getPackPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__PAGE_PRAGMA:
				return ((InternalEList<?>)getPagePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__PREELABORATE_PRAGMA:
				return ((InternalEList<?>)getPreelaboratePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__PRIORITY_PRAGMA:
				return ((InternalEList<?>)getPriorityPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__PURE_PRAGMA:
				return ((InternalEList<?>)getPurePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__QUEUING_POLICY_PRAGMA:
				return ((InternalEList<?>)getQueuingPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				return ((InternalEList<?>)getRemoteCallInterfacePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__REMOTE_TYPES_PRAGMA:
				return ((InternalEList<?>)getRemoteTypesPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__RESTRICTIONS_PRAGMA:
				return ((InternalEList<?>)getRestrictionsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__REVIEWABLE_PRAGMA:
				return ((InternalEList<?>)getReviewablePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__SHARED_PASSIVE_PRAGMA:
				return ((InternalEList<?>)getSharedPassivePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__STORAGE_SIZE_PRAGMA:
				return ((InternalEList<?>)getStorageSizePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__SUPPRESS_PRAGMA:
				return ((InternalEList<?>)getSuppressPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				return ((InternalEList<?>)getTaskDispatchingPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__VOLATILE_PRAGMA:
				return ((InternalEList<?>)getVolatilePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__VOLATILE_COMPONENTS_PRAGMA:
				return ((InternalEList<?>)getVolatileComponentsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ASSERT_PRAGMA:
				return ((InternalEList<?>)getAssertPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__ASSERTION_POLICY_PRAGMA:
				return ((InternalEList<?>)getAssertionPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__DETECT_BLOCKING_PRAGMA:
				return ((InternalEList<?>)getDetectBlockingPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__NO_RETURN_PRAGMA:
				return ((InternalEList<?>)getNoReturnPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				return ((InternalEList<?>)getPartitionElaborationPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				return ((InternalEList<?>)getPreelaborableInitializationPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return ((InternalEList<?>)getPrioritySpecificDispatchingPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__PROFILE_PRAGMA:
				return ((InternalEList<?>)getProfilePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__RELATIVE_DEADLINE_PRAGMA:
				return ((InternalEList<?>)getRelativeDeadlinePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__UNCHECKED_UNION_PRAGMA:
				return ((InternalEList<?>)getUncheckedUnionPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__UNSUPPRESS_PRAGMA:
				return ((InternalEList<?>)getUnsuppressPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				return ((InternalEList<?>)getDefaultStoragePoolPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__DISPATCHING_DOMAIN_PRAGMA:
				return ((InternalEList<?>)getDispatchingDomainPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__CPU_PRAGMA:
				return ((InternalEList<?>)getCpuPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__INDEPENDENT_PRAGMA:
				return ((InternalEList<?>)getIndependentPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				return ((InternalEList<?>)getIndependentComponentsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				return ((InternalEList<?>)getImplementationDefinedPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.DEFINITION_LIST__UNKNOWN_PRAGMA:
				return ((InternalEList<?>)getUnknownPragma()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.DEFINITION_LIST__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case AdaPackage.DEFINITION_LIST__NOT_AN_ELEMENT:
				return getNotAnElement();
			case AdaPackage.DEFINITION_LIST__DERIVED_TYPE_DEFINITION:
				return getDerivedTypeDefinition();
			case AdaPackage.DEFINITION_LIST__DERIVED_RECORD_EXTENSION_DEFINITION:
				return getDerivedRecordExtensionDefinition();
			case AdaPackage.DEFINITION_LIST__ENUMERATION_TYPE_DEFINITION:
				return getEnumerationTypeDefinition();
			case AdaPackage.DEFINITION_LIST__SIGNED_INTEGER_TYPE_DEFINITION:
				return getSignedIntegerTypeDefinition();
			case AdaPackage.DEFINITION_LIST__MODULAR_TYPE_DEFINITION:
				return getModularTypeDefinition();
			case AdaPackage.DEFINITION_LIST__ROOT_INTEGER_DEFINITION:
				return getRootIntegerDefinition();
			case AdaPackage.DEFINITION_LIST__ROOT_REAL_DEFINITION:
				return getRootRealDefinition();
			case AdaPackage.DEFINITION_LIST__UNIVERSAL_INTEGER_DEFINITION:
				return getUniversalIntegerDefinition();
			case AdaPackage.DEFINITION_LIST__UNIVERSAL_REAL_DEFINITION:
				return getUniversalRealDefinition();
			case AdaPackage.DEFINITION_LIST__UNIVERSAL_FIXED_DEFINITION:
				return getUniversalFixedDefinition();
			case AdaPackage.DEFINITION_LIST__FLOATING_POINT_DEFINITION:
				return getFloatingPointDefinition();
			case AdaPackage.DEFINITION_LIST__ORDINARY_FIXED_POINT_DEFINITION:
				return getOrdinaryFixedPointDefinition();
			case AdaPackage.DEFINITION_LIST__DECIMAL_FIXED_POINT_DEFINITION:
				return getDecimalFixedPointDefinition();
			case AdaPackage.DEFINITION_LIST__UNCONSTRAINED_ARRAY_DEFINITION:
				return getUnconstrainedArrayDefinition();
			case AdaPackage.DEFINITION_LIST__CONSTRAINED_ARRAY_DEFINITION:
				return getConstrainedArrayDefinition();
			case AdaPackage.DEFINITION_LIST__RECORD_TYPE_DEFINITION:
				return getRecordTypeDefinition();
			case AdaPackage.DEFINITION_LIST__TAGGED_RECORD_TYPE_DEFINITION:
				return getTaggedRecordTypeDefinition();
			case AdaPackage.DEFINITION_LIST__ORDINARY_INTERFACE:
				return getOrdinaryInterface();
			case AdaPackage.DEFINITION_LIST__LIMITED_INTERFACE:
				return getLimitedInterface();
			case AdaPackage.DEFINITION_LIST__TASK_INTERFACE:
				return getTaskInterface();
			case AdaPackage.DEFINITION_LIST__PROTECTED_INTERFACE:
				return getProtectedInterface();
			case AdaPackage.DEFINITION_LIST__SYNCHRONIZED_INTERFACE:
				return getSynchronizedInterface();
			case AdaPackage.DEFINITION_LIST__POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				return getPoolSpecificAccessToVariable();
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_VARIABLE:
				return getAccessToVariable();
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_CONSTANT:
				return getAccessToConstant();
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_PROCEDURE:
				return getAccessToProcedure();
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_PROTECTED_PROCEDURE:
				return getAccessToProtectedProcedure();
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_FUNCTION:
				return getAccessToFunction();
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_PROTECTED_FUNCTION:
				return getAccessToProtectedFunction();
			case AdaPackage.DEFINITION_LIST__SUBTYPE_INDICATION:
				return getSubtypeIndication();
			case AdaPackage.DEFINITION_LIST__RANGE_ATTRIBUTE_REFERENCE:
				return getRangeAttributeReference();
			case AdaPackage.DEFINITION_LIST__SIMPLE_EXPRESSION_RANGE:
				return getSimpleExpressionRange();
			case AdaPackage.DEFINITION_LIST__DIGITS_CONSTRAINT:
				return getDigitsConstraint();
			case AdaPackage.DEFINITION_LIST__DELTA_CONSTRAINT:
				return getDeltaConstraint();
			case AdaPackage.DEFINITION_LIST__INDEX_CONSTRAINT:
				return getIndexConstraint();
			case AdaPackage.DEFINITION_LIST__DISCRIMINANT_CONSTRAINT:
				return getDiscriminantConstraint();
			case AdaPackage.DEFINITION_LIST__COMPONENT_DEFINITION:
				return getComponentDefinition();
			case AdaPackage.DEFINITION_LIST__DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION:
				return getDiscreteSubtypeIndicationAsSubtypeDefinition();
			case AdaPackage.DEFINITION_LIST__DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION:
				return getDiscreteRangeAttributeReferenceAsSubtypeDefinition();
			case AdaPackage.DEFINITION_LIST__DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION:
				return getDiscreteSimpleExpressionRangeAsSubtypeDefinition();
			case AdaPackage.DEFINITION_LIST__DISCRETE_SUBTYPE_INDICATION:
				return getDiscreteSubtypeIndication();
			case AdaPackage.DEFINITION_LIST__DISCRETE_RANGE_ATTRIBUTE_REFERENCE:
				return getDiscreteRangeAttributeReference();
			case AdaPackage.DEFINITION_LIST__DISCRETE_SIMPLE_EXPRESSION_RANGE:
				return getDiscreteSimpleExpressionRange();
			case AdaPackage.DEFINITION_LIST__UNKNOWN_DISCRIMINANT_PART:
				return getUnknownDiscriminantPart();
			case AdaPackage.DEFINITION_LIST__KNOWN_DISCRIMINANT_PART:
				return getKnownDiscriminantPart();
			case AdaPackage.DEFINITION_LIST__RECORD_DEFINITION:
				return getRecordDefinition();
			case AdaPackage.DEFINITION_LIST__NULL_RECORD_DEFINITION:
				return getNullRecordDefinition();
			case AdaPackage.DEFINITION_LIST__NULL_COMPONENT:
				return getNullComponent();
			case AdaPackage.DEFINITION_LIST__VARIANT_PART:
				return getVariantPart();
			case AdaPackage.DEFINITION_LIST__VARIANT:
				return getVariant();
			case AdaPackage.DEFINITION_LIST__OTHERS_CHOICE:
				return getOthersChoice();
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_VARIABLE:
				return getAnonymousAccessToVariable();
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_CONSTANT:
				return getAnonymousAccessToConstant();
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_PROCEDURE:
				return getAnonymousAccessToProcedure();
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE:
				return getAnonymousAccessToProtectedProcedure();
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_FUNCTION:
				return getAnonymousAccessToFunction();
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION:
				return getAnonymousAccessToProtectedFunction();
			case AdaPackage.DEFINITION_LIST__PRIVATE_TYPE_DEFINITION:
				return getPrivateTypeDefinition();
			case AdaPackage.DEFINITION_LIST__TAGGED_PRIVATE_TYPE_DEFINITION:
				return getTaggedPrivateTypeDefinition();
			case AdaPackage.DEFINITION_LIST__PRIVATE_EXTENSION_DEFINITION:
				return getPrivateExtensionDefinition();
			case AdaPackage.DEFINITION_LIST__TASK_DEFINITION:
				return getTaskDefinition();
			case AdaPackage.DEFINITION_LIST__PROTECTED_DEFINITION:
				return getProtectedDefinition();
			case AdaPackage.DEFINITION_LIST__FORMAL_PRIVATE_TYPE_DEFINITION:
				return getFormalPrivateTypeDefinition();
			case AdaPackage.DEFINITION_LIST__FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION:
				return getFormalTaggedPrivateTypeDefinition();
			case AdaPackage.DEFINITION_LIST__FORMAL_DERIVED_TYPE_DEFINITION:
				return getFormalDerivedTypeDefinition();
			case AdaPackage.DEFINITION_LIST__FORMAL_DISCRETE_TYPE_DEFINITION:
				return getFormalDiscreteTypeDefinition();
			case AdaPackage.DEFINITION_LIST__FORMAL_SIGNED_INTEGER_TYPE_DEFINITION:
				return getFormalSignedIntegerTypeDefinition();
			case AdaPackage.DEFINITION_LIST__FORMAL_MODULAR_TYPE_DEFINITION:
				return getFormalModularTypeDefinition();
			case AdaPackage.DEFINITION_LIST__FORMAL_FLOATING_POINT_DEFINITION:
				return getFormalFloatingPointDefinition();
			case AdaPackage.DEFINITION_LIST__FORMAL_ORDINARY_FIXED_POINT_DEFINITION:
				return getFormalOrdinaryFixedPointDefinition();
			case AdaPackage.DEFINITION_LIST__FORMAL_DECIMAL_FIXED_POINT_DEFINITION:
				return getFormalDecimalFixedPointDefinition();
			case AdaPackage.DEFINITION_LIST__FORMAL_ORDINARY_INTERFACE:
				return getFormalOrdinaryInterface();
			case AdaPackage.DEFINITION_LIST__FORMAL_LIMITED_INTERFACE:
				return getFormalLimitedInterface();
			case AdaPackage.DEFINITION_LIST__FORMAL_TASK_INTERFACE:
				return getFormalTaskInterface();
			case AdaPackage.DEFINITION_LIST__FORMAL_PROTECTED_INTERFACE:
				return getFormalProtectedInterface();
			case AdaPackage.DEFINITION_LIST__FORMAL_SYNCHRONIZED_INTERFACE:
				return getFormalSynchronizedInterface();
			case AdaPackage.DEFINITION_LIST__FORMAL_UNCONSTRAINED_ARRAY_DEFINITION:
				return getFormalUnconstrainedArrayDefinition();
			case AdaPackage.DEFINITION_LIST__FORMAL_CONSTRAINED_ARRAY_DEFINITION:
				return getFormalConstrainedArrayDefinition();
			case AdaPackage.DEFINITION_LIST__FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				return getFormalPoolSpecificAccessToVariable();
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_VARIABLE:
				return getFormalAccessToVariable();
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_CONSTANT:
				return getFormalAccessToConstant();
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_PROCEDURE:
				return getFormalAccessToProcedure();
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_PROTECTED_PROCEDURE:
				return getFormalAccessToProtectedProcedure();
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_FUNCTION:
				return getFormalAccessToFunction();
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_PROTECTED_FUNCTION:
				return getFormalAccessToProtectedFunction();
			case AdaPackage.DEFINITION_LIST__ASPECT_SPECIFICATION:
				return getAspectSpecification();
			case AdaPackage.DEFINITION_LIST__IDENTIFIER:
				return getIdentifier();
			case AdaPackage.DEFINITION_LIST__SELECTED_COMPONENT:
				return getSelectedComponent();
			case AdaPackage.DEFINITION_LIST__BASE_ATTRIBUTE:
				return getBaseAttribute();
			case AdaPackage.DEFINITION_LIST__CLASS_ATTRIBUTE:
				return getClassAttribute();
			case AdaPackage.DEFINITION_LIST__COMMENT:
				return getComment();
			case AdaPackage.DEFINITION_LIST__ALL_CALLS_REMOTE_PRAGMA:
				return getAllCallsRemotePragma();
			case AdaPackage.DEFINITION_LIST__ASYNCHRONOUS_PRAGMA:
				return getAsynchronousPragma();
			case AdaPackage.DEFINITION_LIST__ATOMIC_PRAGMA:
				return getAtomicPragma();
			case AdaPackage.DEFINITION_LIST__ATOMIC_COMPONENTS_PRAGMA:
				return getAtomicComponentsPragma();
			case AdaPackage.DEFINITION_LIST__ATTACH_HANDLER_PRAGMA:
				return getAttachHandlerPragma();
			case AdaPackage.DEFINITION_LIST__CONTROLLED_PRAGMA:
				return getControlledPragma();
			case AdaPackage.DEFINITION_LIST__CONVENTION_PRAGMA:
				return getConventionPragma();
			case AdaPackage.DEFINITION_LIST__DISCARD_NAMES_PRAGMA:
				return getDiscardNamesPragma();
			case AdaPackage.DEFINITION_LIST__ELABORATE_PRAGMA:
				return getElaboratePragma();
			case AdaPackage.DEFINITION_LIST__ELABORATE_ALL_PRAGMA:
				return getElaborateAllPragma();
			case AdaPackage.DEFINITION_LIST__ELABORATE_BODY_PRAGMA:
				return getElaborateBodyPragma();
			case AdaPackage.DEFINITION_LIST__EXPORT_PRAGMA:
				return getExportPragma();
			case AdaPackage.DEFINITION_LIST__IMPORT_PRAGMA:
				return getImportPragma();
			case AdaPackage.DEFINITION_LIST__INLINE_PRAGMA:
				return getInlinePragma();
			case AdaPackage.DEFINITION_LIST__INSPECTION_POINT_PRAGMA:
				return getInspectionPointPragma();
			case AdaPackage.DEFINITION_LIST__INTERRUPT_HANDLER_PRAGMA:
				return getInterruptHandlerPragma();
			case AdaPackage.DEFINITION_LIST__INTERRUPT_PRIORITY_PRAGMA:
				return getInterruptPriorityPragma();
			case AdaPackage.DEFINITION_LIST__LINKER_OPTIONS_PRAGMA:
				return getLinkerOptionsPragma();
			case AdaPackage.DEFINITION_LIST__LIST_PRAGMA:
				return getListPragma();
			case AdaPackage.DEFINITION_LIST__LOCKING_POLICY_PRAGMA:
				return getLockingPolicyPragma();
			case AdaPackage.DEFINITION_LIST__NORMALIZE_SCALARS_PRAGMA:
				return getNormalizeScalarsPragma();
			case AdaPackage.DEFINITION_LIST__OPTIMIZE_PRAGMA:
				return getOptimizePragma();
			case AdaPackage.DEFINITION_LIST__PACK_PRAGMA:
				return getPackPragma();
			case AdaPackage.DEFINITION_LIST__PAGE_PRAGMA:
				return getPagePragma();
			case AdaPackage.DEFINITION_LIST__PREELABORATE_PRAGMA:
				return getPreelaboratePragma();
			case AdaPackage.DEFINITION_LIST__PRIORITY_PRAGMA:
				return getPriorityPragma();
			case AdaPackage.DEFINITION_LIST__PURE_PRAGMA:
				return getPurePragma();
			case AdaPackage.DEFINITION_LIST__QUEUING_POLICY_PRAGMA:
				return getQueuingPolicyPragma();
			case AdaPackage.DEFINITION_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				return getRemoteCallInterfacePragma();
			case AdaPackage.DEFINITION_LIST__REMOTE_TYPES_PRAGMA:
				return getRemoteTypesPragma();
			case AdaPackage.DEFINITION_LIST__RESTRICTIONS_PRAGMA:
				return getRestrictionsPragma();
			case AdaPackage.DEFINITION_LIST__REVIEWABLE_PRAGMA:
				return getReviewablePragma();
			case AdaPackage.DEFINITION_LIST__SHARED_PASSIVE_PRAGMA:
				return getSharedPassivePragma();
			case AdaPackage.DEFINITION_LIST__STORAGE_SIZE_PRAGMA:
				return getStorageSizePragma();
			case AdaPackage.DEFINITION_LIST__SUPPRESS_PRAGMA:
				return getSuppressPragma();
			case AdaPackage.DEFINITION_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				return getTaskDispatchingPolicyPragma();
			case AdaPackage.DEFINITION_LIST__VOLATILE_PRAGMA:
				return getVolatilePragma();
			case AdaPackage.DEFINITION_LIST__VOLATILE_COMPONENTS_PRAGMA:
				return getVolatileComponentsPragma();
			case AdaPackage.DEFINITION_LIST__ASSERT_PRAGMA:
				return getAssertPragma();
			case AdaPackage.DEFINITION_LIST__ASSERTION_POLICY_PRAGMA:
				return getAssertionPolicyPragma();
			case AdaPackage.DEFINITION_LIST__DETECT_BLOCKING_PRAGMA:
				return getDetectBlockingPragma();
			case AdaPackage.DEFINITION_LIST__NO_RETURN_PRAGMA:
				return getNoReturnPragma();
			case AdaPackage.DEFINITION_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				return getPartitionElaborationPolicyPragma();
			case AdaPackage.DEFINITION_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				return getPreelaborableInitializationPragma();
			case AdaPackage.DEFINITION_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return getPrioritySpecificDispatchingPragma();
			case AdaPackage.DEFINITION_LIST__PROFILE_PRAGMA:
				return getProfilePragma();
			case AdaPackage.DEFINITION_LIST__RELATIVE_DEADLINE_PRAGMA:
				return getRelativeDeadlinePragma();
			case AdaPackage.DEFINITION_LIST__UNCHECKED_UNION_PRAGMA:
				return getUncheckedUnionPragma();
			case AdaPackage.DEFINITION_LIST__UNSUPPRESS_PRAGMA:
				return getUnsuppressPragma();
			case AdaPackage.DEFINITION_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				return getDefaultStoragePoolPragma();
			case AdaPackage.DEFINITION_LIST__DISPATCHING_DOMAIN_PRAGMA:
				return getDispatchingDomainPragma();
			case AdaPackage.DEFINITION_LIST__CPU_PRAGMA:
				return getCpuPragma();
			case AdaPackage.DEFINITION_LIST__INDEPENDENT_PRAGMA:
				return getIndependentPragma();
			case AdaPackage.DEFINITION_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				return getIndependentComponentsPragma();
			case AdaPackage.DEFINITION_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				return getImplementationDefinedPragma();
			case AdaPackage.DEFINITION_LIST__UNKNOWN_PRAGMA:
				return getUnknownPragma();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.DEFINITION_LIST__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case AdaPackage.DEFINITION_LIST__NOT_AN_ELEMENT:
				getNotAnElement().clear();
				getNotAnElement().addAll((Collection<? extends NotAnElement>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__DERIVED_TYPE_DEFINITION:
				getDerivedTypeDefinition().clear();
				getDerivedTypeDefinition().addAll((Collection<? extends DerivedTypeDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__DERIVED_RECORD_EXTENSION_DEFINITION:
				getDerivedRecordExtensionDefinition().clear();
				getDerivedRecordExtensionDefinition().addAll((Collection<? extends DerivedRecordExtensionDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ENUMERATION_TYPE_DEFINITION:
				getEnumerationTypeDefinition().clear();
				getEnumerationTypeDefinition().addAll((Collection<? extends EnumerationTypeDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__SIGNED_INTEGER_TYPE_DEFINITION:
				getSignedIntegerTypeDefinition().clear();
				getSignedIntegerTypeDefinition().addAll((Collection<? extends SignedIntegerTypeDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__MODULAR_TYPE_DEFINITION:
				getModularTypeDefinition().clear();
				getModularTypeDefinition().addAll((Collection<? extends ModularTypeDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ROOT_INTEGER_DEFINITION:
				getRootIntegerDefinition().clear();
				getRootIntegerDefinition().addAll((Collection<? extends RootIntegerDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ROOT_REAL_DEFINITION:
				getRootRealDefinition().clear();
				getRootRealDefinition().addAll((Collection<? extends RootRealDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__UNIVERSAL_INTEGER_DEFINITION:
				getUniversalIntegerDefinition().clear();
				getUniversalIntegerDefinition().addAll((Collection<? extends UniversalIntegerDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__UNIVERSAL_REAL_DEFINITION:
				getUniversalRealDefinition().clear();
				getUniversalRealDefinition().addAll((Collection<? extends UniversalRealDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__UNIVERSAL_FIXED_DEFINITION:
				getUniversalFixedDefinition().clear();
				getUniversalFixedDefinition().addAll((Collection<? extends UniversalFixedDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__FLOATING_POINT_DEFINITION:
				getFloatingPointDefinition().clear();
				getFloatingPointDefinition().addAll((Collection<? extends FloatingPointDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ORDINARY_FIXED_POINT_DEFINITION:
				getOrdinaryFixedPointDefinition().clear();
				getOrdinaryFixedPointDefinition().addAll((Collection<? extends OrdinaryFixedPointDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__DECIMAL_FIXED_POINT_DEFINITION:
				getDecimalFixedPointDefinition().clear();
				getDecimalFixedPointDefinition().addAll((Collection<? extends DecimalFixedPointDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__UNCONSTRAINED_ARRAY_DEFINITION:
				getUnconstrainedArrayDefinition().clear();
				getUnconstrainedArrayDefinition().addAll((Collection<? extends UnconstrainedArrayDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__CONSTRAINED_ARRAY_DEFINITION:
				getConstrainedArrayDefinition().clear();
				getConstrainedArrayDefinition().addAll((Collection<? extends ConstrainedArrayDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__RECORD_TYPE_DEFINITION:
				getRecordTypeDefinition().clear();
				getRecordTypeDefinition().addAll((Collection<? extends RecordTypeDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__TAGGED_RECORD_TYPE_DEFINITION:
				getTaggedRecordTypeDefinition().clear();
				getTaggedRecordTypeDefinition().addAll((Collection<? extends TaggedRecordTypeDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ORDINARY_INTERFACE:
				getOrdinaryInterface().clear();
				getOrdinaryInterface().addAll((Collection<? extends OrdinaryInterface>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__LIMITED_INTERFACE:
				getLimitedInterface().clear();
				getLimitedInterface().addAll((Collection<? extends LimitedInterface>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__TASK_INTERFACE:
				getTaskInterface().clear();
				getTaskInterface().addAll((Collection<? extends TaskInterface>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__PROTECTED_INTERFACE:
				getProtectedInterface().clear();
				getProtectedInterface().addAll((Collection<? extends ProtectedInterface>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__SYNCHRONIZED_INTERFACE:
				getSynchronizedInterface().clear();
				getSynchronizedInterface().addAll((Collection<? extends SynchronizedInterface>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				getPoolSpecificAccessToVariable().clear();
				getPoolSpecificAccessToVariable().addAll((Collection<? extends PoolSpecificAccessToVariable>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_VARIABLE:
				getAccessToVariable().clear();
				getAccessToVariable().addAll((Collection<? extends AccessToVariable>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_CONSTANT:
				getAccessToConstant().clear();
				getAccessToConstant().addAll((Collection<? extends AccessToConstant>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_PROCEDURE:
				getAccessToProcedure().clear();
				getAccessToProcedure().addAll((Collection<? extends AccessToProcedure>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_PROTECTED_PROCEDURE:
				getAccessToProtectedProcedure().clear();
				getAccessToProtectedProcedure().addAll((Collection<? extends AccessToProtectedProcedure>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_FUNCTION:
				getAccessToFunction().clear();
				getAccessToFunction().addAll((Collection<? extends AccessToFunction>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_PROTECTED_FUNCTION:
				getAccessToProtectedFunction().clear();
				getAccessToProtectedFunction().addAll((Collection<? extends AccessToProtectedFunction>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__SUBTYPE_INDICATION:
				getSubtypeIndication().clear();
				getSubtypeIndication().addAll((Collection<? extends SubtypeIndication>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__RANGE_ATTRIBUTE_REFERENCE:
				getRangeAttributeReference().clear();
				getRangeAttributeReference().addAll((Collection<? extends RangeAttributeReference>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__SIMPLE_EXPRESSION_RANGE:
				getSimpleExpressionRange().clear();
				getSimpleExpressionRange().addAll((Collection<? extends SimpleExpressionRange>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__DIGITS_CONSTRAINT:
				getDigitsConstraint().clear();
				getDigitsConstraint().addAll((Collection<? extends DigitsConstraint>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__DELTA_CONSTRAINT:
				getDeltaConstraint().clear();
				getDeltaConstraint().addAll((Collection<? extends DeltaConstraint>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__INDEX_CONSTRAINT:
				getIndexConstraint().clear();
				getIndexConstraint().addAll((Collection<? extends IndexConstraint>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__DISCRIMINANT_CONSTRAINT:
				getDiscriminantConstraint().clear();
				getDiscriminantConstraint().addAll((Collection<? extends DiscriminantConstraint>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__COMPONENT_DEFINITION:
				getComponentDefinition().clear();
				getComponentDefinition().addAll((Collection<? extends ComponentDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION:
				getDiscreteSubtypeIndicationAsSubtypeDefinition().clear();
				getDiscreteSubtypeIndicationAsSubtypeDefinition().addAll((Collection<? extends DiscreteSubtypeIndicationAsSubtypeDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION:
				getDiscreteRangeAttributeReferenceAsSubtypeDefinition().clear();
				getDiscreteRangeAttributeReferenceAsSubtypeDefinition().addAll((Collection<? extends DiscreteRangeAttributeReferenceAsSubtypeDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION:
				getDiscreteSimpleExpressionRangeAsSubtypeDefinition().clear();
				getDiscreteSimpleExpressionRangeAsSubtypeDefinition().addAll((Collection<? extends DiscreteSimpleExpressionRangeAsSubtypeDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__DISCRETE_SUBTYPE_INDICATION:
				getDiscreteSubtypeIndication().clear();
				getDiscreteSubtypeIndication().addAll((Collection<? extends DiscreteSubtypeIndication>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__DISCRETE_RANGE_ATTRIBUTE_REFERENCE:
				getDiscreteRangeAttributeReference().clear();
				getDiscreteRangeAttributeReference().addAll((Collection<? extends DiscreteRangeAttributeReference>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__DISCRETE_SIMPLE_EXPRESSION_RANGE:
				getDiscreteSimpleExpressionRange().clear();
				getDiscreteSimpleExpressionRange().addAll((Collection<? extends DiscreteSimpleExpressionRange>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__UNKNOWN_DISCRIMINANT_PART:
				getUnknownDiscriminantPart().clear();
				getUnknownDiscriminantPart().addAll((Collection<? extends UnknownDiscriminantPart>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__KNOWN_DISCRIMINANT_PART:
				getKnownDiscriminantPart().clear();
				getKnownDiscriminantPart().addAll((Collection<? extends KnownDiscriminantPart>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__RECORD_DEFINITION:
				getRecordDefinition().clear();
				getRecordDefinition().addAll((Collection<? extends RecordDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__NULL_RECORD_DEFINITION:
				getNullRecordDefinition().clear();
				getNullRecordDefinition().addAll((Collection<? extends NullRecordDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__NULL_COMPONENT:
				getNullComponent().clear();
				getNullComponent().addAll((Collection<? extends NullComponent>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__VARIANT_PART:
				getVariantPart().clear();
				getVariantPart().addAll((Collection<? extends VariantPart>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__VARIANT:
				getVariant().clear();
				getVariant().addAll((Collection<? extends Variant>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__OTHERS_CHOICE:
				getOthersChoice().clear();
				getOthersChoice().addAll((Collection<? extends OthersChoice>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_VARIABLE:
				getAnonymousAccessToVariable().clear();
				getAnonymousAccessToVariable().addAll((Collection<? extends AnonymousAccessToVariable>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_CONSTANT:
				getAnonymousAccessToConstant().clear();
				getAnonymousAccessToConstant().addAll((Collection<? extends AnonymousAccessToConstant>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_PROCEDURE:
				getAnonymousAccessToProcedure().clear();
				getAnonymousAccessToProcedure().addAll((Collection<? extends AnonymousAccessToProcedure>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE:
				getAnonymousAccessToProtectedProcedure().clear();
				getAnonymousAccessToProtectedProcedure().addAll((Collection<? extends AnonymousAccessToProtectedProcedure>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_FUNCTION:
				getAnonymousAccessToFunction().clear();
				getAnonymousAccessToFunction().addAll((Collection<? extends AnonymousAccessToFunction>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION:
				getAnonymousAccessToProtectedFunction().clear();
				getAnonymousAccessToProtectedFunction().addAll((Collection<? extends AnonymousAccessToProtectedFunction>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__PRIVATE_TYPE_DEFINITION:
				getPrivateTypeDefinition().clear();
				getPrivateTypeDefinition().addAll((Collection<? extends PrivateTypeDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__TAGGED_PRIVATE_TYPE_DEFINITION:
				getTaggedPrivateTypeDefinition().clear();
				getTaggedPrivateTypeDefinition().addAll((Collection<? extends TaggedPrivateTypeDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__PRIVATE_EXTENSION_DEFINITION:
				getPrivateExtensionDefinition().clear();
				getPrivateExtensionDefinition().addAll((Collection<? extends PrivateExtensionDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__TASK_DEFINITION:
				getTaskDefinition().clear();
				getTaskDefinition().addAll((Collection<? extends TaskDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__PROTECTED_DEFINITION:
				getProtectedDefinition().clear();
				getProtectedDefinition().addAll((Collection<? extends ProtectedDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_PRIVATE_TYPE_DEFINITION:
				getFormalPrivateTypeDefinition().clear();
				getFormalPrivateTypeDefinition().addAll((Collection<? extends FormalPrivateTypeDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION:
				getFormalTaggedPrivateTypeDefinition().clear();
				getFormalTaggedPrivateTypeDefinition().addAll((Collection<? extends FormalTaggedPrivateTypeDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_DERIVED_TYPE_DEFINITION:
				getFormalDerivedTypeDefinition().clear();
				getFormalDerivedTypeDefinition().addAll((Collection<? extends FormalDerivedTypeDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_DISCRETE_TYPE_DEFINITION:
				getFormalDiscreteTypeDefinition().clear();
				getFormalDiscreteTypeDefinition().addAll((Collection<? extends FormalDiscreteTypeDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_SIGNED_INTEGER_TYPE_DEFINITION:
				getFormalSignedIntegerTypeDefinition().clear();
				getFormalSignedIntegerTypeDefinition().addAll((Collection<? extends FormalSignedIntegerTypeDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_MODULAR_TYPE_DEFINITION:
				getFormalModularTypeDefinition().clear();
				getFormalModularTypeDefinition().addAll((Collection<? extends FormalModularTypeDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_FLOATING_POINT_DEFINITION:
				getFormalFloatingPointDefinition().clear();
				getFormalFloatingPointDefinition().addAll((Collection<? extends FormalFloatingPointDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_ORDINARY_FIXED_POINT_DEFINITION:
				getFormalOrdinaryFixedPointDefinition().clear();
				getFormalOrdinaryFixedPointDefinition().addAll((Collection<? extends FormalOrdinaryFixedPointDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_DECIMAL_FIXED_POINT_DEFINITION:
				getFormalDecimalFixedPointDefinition().clear();
				getFormalDecimalFixedPointDefinition().addAll((Collection<? extends FormalDecimalFixedPointDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_ORDINARY_INTERFACE:
				getFormalOrdinaryInterface().clear();
				getFormalOrdinaryInterface().addAll((Collection<? extends FormalOrdinaryInterface>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_LIMITED_INTERFACE:
				getFormalLimitedInterface().clear();
				getFormalLimitedInterface().addAll((Collection<? extends FormalLimitedInterface>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_TASK_INTERFACE:
				getFormalTaskInterface().clear();
				getFormalTaskInterface().addAll((Collection<? extends FormalTaskInterface>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_PROTECTED_INTERFACE:
				getFormalProtectedInterface().clear();
				getFormalProtectedInterface().addAll((Collection<? extends FormalProtectedInterface>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_SYNCHRONIZED_INTERFACE:
				getFormalSynchronizedInterface().clear();
				getFormalSynchronizedInterface().addAll((Collection<? extends FormalSynchronizedInterface>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_UNCONSTRAINED_ARRAY_DEFINITION:
				getFormalUnconstrainedArrayDefinition().clear();
				getFormalUnconstrainedArrayDefinition().addAll((Collection<? extends FormalUnconstrainedArrayDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_CONSTRAINED_ARRAY_DEFINITION:
				getFormalConstrainedArrayDefinition().clear();
				getFormalConstrainedArrayDefinition().addAll((Collection<? extends FormalConstrainedArrayDefinition>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				getFormalPoolSpecificAccessToVariable().clear();
				getFormalPoolSpecificAccessToVariable().addAll((Collection<? extends FormalPoolSpecificAccessToVariable>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_VARIABLE:
				getFormalAccessToVariable().clear();
				getFormalAccessToVariable().addAll((Collection<? extends FormalAccessToVariable>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_CONSTANT:
				getFormalAccessToConstant().clear();
				getFormalAccessToConstant().addAll((Collection<? extends FormalAccessToConstant>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_PROCEDURE:
				getFormalAccessToProcedure().clear();
				getFormalAccessToProcedure().addAll((Collection<? extends FormalAccessToProcedure>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_PROTECTED_PROCEDURE:
				getFormalAccessToProtectedProcedure().clear();
				getFormalAccessToProtectedProcedure().addAll((Collection<? extends FormalAccessToProtectedProcedure>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_FUNCTION:
				getFormalAccessToFunction().clear();
				getFormalAccessToFunction().addAll((Collection<? extends FormalAccessToFunction>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_PROTECTED_FUNCTION:
				getFormalAccessToProtectedFunction().clear();
				getFormalAccessToProtectedFunction().addAll((Collection<? extends FormalAccessToProtectedFunction>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ASPECT_SPECIFICATION:
				getAspectSpecification().clear();
				getAspectSpecification().addAll((Collection<? extends AspectSpecification>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__IDENTIFIER:
				getIdentifier().clear();
				getIdentifier().addAll((Collection<? extends Identifier>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__SELECTED_COMPONENT:
				getSelectedComponent().clear();
				getSelectedComponent().addAll((Collection<? extends SelectedComponent>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__BASE_ATTRIBUTE:
				getBaseAttribute().clear();
				getBaseAttribute().addAll((Collection<? extends BaseAttribute>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__CLASS_ATTRIBUTE:
				getClassAttribute().clear();
				getClassAttribute().addAll((Collection<? extends ClassAttribute>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__COMMENT:
				getComment().clear();
				getComment().addAll((Collection<? extends Comment>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ALL_CALLS_REMOTE_PRAGMA:
				getAllCallsRemotePragma().clear();
				getAllCallsRemotePragma().addAll((Collection<? extends AllCallsRemotePragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ASYNCHRONOUS_PRAGMA:
				getAsynchronousPragma().clear();
				getAsynchronousPragma().addAll((Collection<? extends AsynchronousPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ATOMIC_PRAGMA:
				getAtomicPragma().clear();
				getAtomicPragma().addAll((Collection<? extends AtomicPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ATOMIC_COMPONENTS_PRAGMA:
				getAtomicComponentsPragma().clear();
				getAtomicComponentsPragma().addAll((Collection<? extends AtomicComponentsPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ATTACH_HANDLER_PRAGMA:
				getAttachHandlerPragma().clear();
				getAttachHandlerPragma().addAll((Collection<? extends AttachHandlerPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__CONTROLLED_PRAGMA:
				getControlledPragma().clear();
				getControlledPragma().addAll((Collection<? extends ControlledPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__CONVENTION_PRAGMA:
				getConventionPragma().clear();
				getConventionPragma().addAll((Collection<? extends ConventionPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__DISCARD_NAMES_PRAGMA:
				getDiscardNamesPragma().clear();
				getDiscardNamesPragma().addAll((Collection<? extends DiscardNamesPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ELABORATE_PRAGMA:
				getElaboratePragma().clear();
				getElaboratePragma().addAll((Collection<? extends ElaboratePragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ELABORATE_ALL_PRAGMA:
				getElaborateAllPragma().clear();
				getElaborateAllPragma().addAll((Collection<? extends ElaborateAllPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ELABORATE_BODY_PRAGMA:
				getElaborateBodyPragma().clear();
				getElaborateBodyPragma().addAll((Collection<? extends ElaborateBodyPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__EXPORT_PRAGMA:
				getExportPragma().clear();
				getExportPragma().addAll((Collection<? extends ExportPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__IMPORT_PRAGMA:
				getImportPragma().clear();
				getImportPragma().addAll((Collection<? extends ImportPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__INLINE_PRAGMA:
				getInlinePragma().clear();
				getInlinePragma().addAll((Collection<? extends InlinePragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__INSPECTION_POINT_PRAGMA:
				getInspectionPointPragma().clear();
				getInspectionPointPragma().addAll((Collection<? extends InspectionPointPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__INTERRUPT_HANDLER_PRAGMA:
				getInterruptHandlerPragma().clear();
				getInterruptHandlerPragma().addAll((Collection<? extends InterruptHandlerPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__INTERRUPT_PRIORITY_PRAGMA:
				getInterruptPriorityPragma().clear();
				getInterruptPriorityPragma().addAll((Collection<? extends InterruptPriorityPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__LINKER_OPTIONS_PRAGMA:
				getLinkerOptionsPragma().clear();
				getLinkerOptionsPragma().addAll((Collection<? extends LinkerOptionsPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__LIST_PRAGMA:
				getListPragma().clear();
				getListPragma().addAll((Collection<? extends ListPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__LOCKING_POLICY_PRAGMA:
				getLockingPolicyPragma().clear();
				getLockingPolicyPragma().addAll((Collection<? extends LockingPolicyPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__NORMALIZE_SCALARS_PRAGMA:
				getNormalizeScalarsPragma().clear();
				getNormalizeScalarsPragma().addAll((Collection<? extends NormalizeScalarsPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__OPTIMIZE_PRAGMA:
				getOptimizePragma().clear();
				getOptimizePragma().addAll((Collection<? extends OptimizePragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__PACK_PRAGMA:
				getPackPragma().clear();
				getPackPragma().addAll((Collection<? extends PackPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__PAGE_PRAGMA:
				getPagePragma().clear();
				getPagePragma().addAll((Collection<? extends PagePragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__PREELABORATE_PRAGMA:
				getPreelaboratePragma().clear();
				getPreelaboratePragma().addAll((Collection<? extends PreelaboratePragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__PRIORITY_PRAGMA:
				getPriorityPragma().clear();
				getPriorityPragma().addAll((Collection<? extends PriorityPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__PURE_PRAGMA:
				getPurePragma().clear();
				getPurePragma().addAll((Collection<? extends PurePragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__QUEUING_POLICY_PRAGMA:
				getQueuingPolicyPragma().clear();
				getQueuingPolicyPragma().addAll((Collection<? extends QueuingPolicyPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				getRemoteCallInterfacePragma().clear();
				getRemoteCallInterfacePragma().addAll((Collection<? extends RemoteCallInterfacePragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__REMOTE_TYPES_PRAGMA:
				getRemoteTypesPragma().clear();
				getRemoteTypesPragma().addAll((Collection<? extends RemoteTypesPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__RESTRICTIONS_PRAGMA:
				getRestrictionsPragma().clear();
				getRestrictionsPragma().addAll((Collection<? extends RestrictionsPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__REVIEWABLE_PRAGMA:
				getReviewablePragma().clear();
				getReviewablePragma().addAll((Collection<? extends ReviewablePragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__SHARED_PASSIVE_PRAGMA:
				getSharedPassivePragma().clear();
				getSharedPassivePragma().addAll((Collection<? extends SharedPassivePragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__STORAGE_SIZE_PRAGMA:
				getStorageSizePragma().clear();
				getStorageSizePragma().addAll((Collection<? extends StorageSizePragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__SUPPRESS_PRAGMA:
				getSuppressPragma().clear();
				getSuppressPragma().addAll((Collection<? extends SuppressPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				getTaskDispatchingPolicyPragma().clear();
				getTaskDispatchingPolicyPragma().addAll((Collection<? extends TaskDispatchingPolicyPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__VOLATILE_PRAGMA:
				getVolatilePragma().clear();
				getVolatilePragma().addAll((Collection<? extends VolatilePragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__VOLATILE_COMPONENTS_PRAGMA:
				getVolatileComponentsPragma().clear();
				getVolatileComponentsPragma().addAll((Collection<? extends VolatileComponentsPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ASSERT_PRAGMA:
				getAssertPragma().clear();
				getAssertPragma().addAll((Collection<? extends AssertPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__ASSERTION_POLICY_PRAGMA:
				getAssertionPolicyPragma().clear();
				getAssertionPolicyPragma().addAll((Collection<? extends AssertionPolicyPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__DETECT_BLOCKING_PRAGMA:
				getDetectBlockingPragma().clear();
				getDetectBlockingPragma().addAll((Collection<? extends DetectBlockingPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__NO_RETURN_PRAGMA:
				getNoReturnPragma().clear();
				getNoReturnPragma().addAll((Collection<? extends NoReturnPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				getPartitionElaborationPolicyPragma().clear();
				getPartitionElaborationPolicyPragma().addAll((Collection<? extends PartitionElaborationPolicyPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				getPreelaborableInitializationPragma().clear();
				getPreelaborableInitializationPragma().addAll((Collection<? extends PreelaborableInitializationPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				getPrioritySpecificDispatchingPragma().clear();
				getPrioritySpecificDispatchingPragma().addAll((Collection<? extends PrioritySpecificDispatchingPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__PROFILE_PRAGMA:
				getProfilePragma().clear();
				getProfilePragma().addAll((Collection<? extends ProfilePragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__RELATIVE_DEADLINE_PRAGMA:
				getRelativeDeadlinePragma().clear();
				getRelativeDeadlinePragma().addAll((Collection<? extends RelativeDeadlinePragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__UNCHECKED_UNION_PRAGMA:
				getUncheckedUnionPragma().clear();
				getUncheckedUnionPragma().addAll((Collection<? extends UncheckedUnionPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__UNSUPPRESS_PRAGMA:
				getUnsuppressPragma().clear();
				getUnsuppressPragma().addAll((Collection<? extends UnsuppressPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				getDefaultStoragePoolPragma().clear();
				getDefaultStoragePoolPragma().addAll((Collection<? extends DefaultStoragePoolPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__DISPATCHING_DOMAIN_PRAGMA:
				getDispatchingDomainPragma().clear();
				getDispatchingDomainPragma().addAll((Collection<? extends DispatchingDomainPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__CPU_PRAGMA:
				getCpuPragma().clear();
				getCpuPragma().addAll((Collection<? extends CpuPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__INDEPENDENT_PRAGMA:
				getIndependentPragma().clear();
				getIndependentPragma().addAll((Collection<? extends IndependentPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				getIndependentComponentsPragma().clear();
				getIndependentComponentsPragma().addAll((Collection<? extends IndependentComponentsPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				getImplementationDefinedPragma().clear();
				getImplementationDefinedPragma().addAll((Collection<? extends ImplementationDefinedPragma>)newValue);
				return;
			case AdaPackage.DEFINITION_LIST__UNKNOWN_PRAGMA:
				getUnknownPragma().clear();
				getUnknownPragma().addAll((Collection<? extends UnknownPragma>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.DEFINITION_LIST__GROUP:
				getGroup().clear();
				return;
			case AdaPackage.DEFINITION_LIST__NOT_AN_ELEMENT:
				getNotAnElement().clear();
				return;
			case AdaPackage.DEFINITION_LIST__DERIVED_TYPE_DEFINITION:
				getDerivedTypeDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__DERIVED_RECORD_EXTENSION_DEFINITION:
				getDerivedRecordExtensionDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ENUMERATION_TYPE_DEFINITION:
				getEnumerationTypeDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__SIGNED_INTEGER_TYPE_DEFINITION:
				getSignedIntegerTypeDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__MODULAR_TYPE_DEFINITION:
				getModularTypeDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ROOT_INTEGER_DEFINITION:
				getRootIntegerDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ROOT_REAL_DEFINITION:
				getRootRealDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__UNIVERSAL_INTEGER_DEFINITION:
				getUniversalIntegerDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__UNIVERSAL_REAL_DEFINITION:
				getUniversalRealDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__UNIVERSAL_FIXED_DEFINITION:
				getUniversalFixedDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__FLOATING_POINT_DEFINITION:
				getFloatingPointDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ORDINARY_FIXED_POINT_DEFINITION:
				getOrdinaryFixedPointDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__DECIMAL_FIXED_POINT_DEFINITION:
				getDecimalFixedPointDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__UNCONSTRAINED_ARRAY_DEFINITION:
				getUnconstrainedArrayDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__CONSTRAINED_ARRAY_DEFINITION:
				getConstrainedArrayDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__RECORD_TYPE_DEFINITION:
				getRecordTypeDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__TAGGED_RECORD_TYPE_DEFINITION:
				getTaggedRecordTypeDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ORDINARY_INTERFACE:
				getOrdinaryInterface().clear();
				return;
			case AdaPackage.DEFINITION_LIST__LIMITED_INTERFACE:
				getLimitedInterface().clear();
				return;
			case AdaPackage.DEFINITION_LIST__TASK_INTERFACE:
				getTaskInterface().clear();
				return;
			case AdaPackage.DEFINITION_LIST__PROTECTED_INTERFACE:
				getProtectedInterface().clear();
				return;
			case AdaPackage.DEFINITION_LIST__SYNCHRONIZED_INTERFACE:
				getSynchronizedInterface().clear();
				return;
			case AdaPackage.DEFINITION_LIST__POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				getPoolSpecificAccessToVariable().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_VARIABLE:
				getAccessToVariable().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_CONSTANT:
				getAccessToConstant().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_PROCEDURE:
				getAccessToProcedure().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_PROTECTED_PROCEDURE:
				getAccessToProtectedProcedure().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_FUNCTION:
				getAccessToFunction().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_PROTECTED_FUNCTION:
				getAccessToProtectedFunction().clear();
				return;
			case AdaPackage.DEFINITION_LIST__SUBTYPE_INDICATION:
				getSubtypeIndication().clear();
				return;
			case AdaPackage.DEFINITION_LIST__RANGE_ATTRIBUTE_REFERENCE:
				getRangeAttributeReference().clear();
				return;
			case AdaPackage.DEFINITION_LIST__SIMPLE_EXPRESSION_RANGE:
				getSimpleExpressionRange().clear();
				return;
			case AdaPackage.DEFINITION_LIST__DIGITS_CONSTRAINT:
				getDigitsConstraint().clear();
				return;
			case AdaPackage.DEFINITION_LIST__DELTA_CONSTRAINT:
				getDeltaConstraint().clear();
				return;
			case AdaPackage.DEFINITION_LIST__INDEX_CONSTRAINT:
				getIndexConstraint().clear();
				return;
			case AdaPackage.DEFINITION_LIST__DISCRIMINANT_CONSTRAINT:
				getDiscriminantConstraint().clear();
				return;
			case AdaPackage.DEFINITION_LIST__COMPONENT_DEFINITION:
				getComponentDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION:
				getDiscreteSubtypeIndicationAsSubtypeDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION:
				getDiscreteRangeAttributeReferenceAsSubtypeDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION:
				getDiscreteSimpleExpressionRangeAsSubtypeDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__DISCRETE_SUBTYPE_INDICATION:
				getDiscreteSubtypeIndication().clear();
				return;
			case AdaPackage.DEFINITION_LIST__DISCRETE_RANGE_ATTRIBUTE_REFERENCE:
				getDiscreteRangeAttributeReference().clear();
				return;
			case AdaPackage.DEFINITION_LIST__DISCRETE_SIMPLE_EXPRESSION_RANGE:
				getDiscreteSimpleExpressionRange().clear();
				return;
			case AdaPackage.DEFINITION_LIST__UNKNOWN_DISCRIMINANT_PART:
				getUnknownDiscriminantPart().clear();
				return;
			case AdaPackage.DEFINITION_LIST__KNOWN_DISCRIMINANT_PART:
				getKnownDiscriminantPart().clear();
				return;
			case AdaPackage.DEFINITION_LIST__RECORD_DEFINITION:
				getRecordDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__NULL_RECORD_DEFINITION:
				getNullRecordDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__NULL_COMPONENT:
				getNullComponent().clear();
				return;
			case AdaPackage.DEFINITION_LIST__VARIANT_PART:
				getVariantPart().clear();
				return;
			case AdaPackage.DEFINITION_LIST__VARIANT:
				getVariant().clear();
				return;
			case AdaPackage.DEFINITION_LIST__OTHERS_CHOICE:
				getOthersChoice().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_VARIABLE:
				getAnonymousAccessToVariable().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_CONSTANT:
				getAnonymousAccessToConstant().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_PROCEDURE:
				getAnonymousAccessToProcedure().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE:
				getAnonymousAccessToProtectedProcedure().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_FUNCTION:
				getAnonymousAccessToFunction().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION:
				getAnonymousAccessToProtectedFunction().clear();
				return;
			case AdaPackage.DEFINITION_LIST__PRIVATE_TYPE_DEFINITION:
				getPrivateTypeDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__TAGGED_PRIVATE_TYPE_DEFINITION:
				getTaggedPrivateTypeDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__PRIVATE_EXTENSION_DEFINITION:
				getPrivateExtensionDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__TASK_DEFINITION:
				getTaskDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__PROTECTED_DEFINITION:
				getProtectedDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_PRIVATE_TYPE_DEFINITION:
				getFormalPrivateTypeDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION:
				getFormalTaggedPrivateTypeDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_DERIVED_TYPE_DEFINITION:
				getFormalDerivedTypeDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_DISCRETE_TYPE_DEFINITION:
				getFormalDiscreteTypeDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_SIGNED_INTEGER_TYPE_DEFINITION:
				getFormalSignedIntegerTypeDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_MODULAR_TYPE_DEFINITION:
				getFormalModularTypeDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_FLOATING_POINT_DEFINITION:
				getFormalFloatingPointDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_ORDINARY_FIXED_POINT_DEFINITION:
				getFormalOrdinaryFixedPointDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_DECIMAL_FIXED_POINT_DEFINITION:
				getFormalDecimalFixedPointDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_ORDINARY_INTERFACE:
				getFormalOrdinaryInterface().clear();
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_LIMITED_INTERFACE:
				getFormalLimitedInterface().clear();
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_TASK_INTERFACE:
				getFormalTaskInterface().clear();
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_PROTECTED_INTERFACE:
				getFormalProtectedInterface().clear();
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_SYNCHRONIZED_INTERFACE:
				getFormalSynchronizedInterface().clear();
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_UNCONSTRAINED_ARRAY_DEFINITION:
				getFormalUnconstrainedArrayDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_CONSTRAINED_ARRAY_DEFINITION:
				getFormalConstrainedArrayDefinition().clear();
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				getFormalPoolSpecificAccessToVariable().clear();
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_VARIABLE:
				getFormalAccessToVariable().clear();
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_CONSTANT:
				getFormalAccessToConstant().clear();
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_PROCEDURE:
				getFormalAccessToProcedure().clear();
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_PROTECTED_PROCEDURE:
				getFormalAccessToProtectedProcedure().clear();
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_FUNCTION:
				getFormalAccessToFunction().clear();
				return;
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_PROTECTED_FUNCTION:
				getFormalAccessToProtectedFunction().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ASPECT_SPECIFICATION:
				getAspectSpecification().clear();
				return;
			case AdaPackage.DEFINITION_LIST__IDENTIFIER:
				getIdentifier().clear();
				return;
			case AdaPackage.DEFINITION_LIST__SELECTED_COMPONENT:
				getSelectedComponent().clear();
				return;
			case AdaPackage.DEFINITION_LIST__BASE_ATTRIBUTE:
				getBaseAttribute().clear();
				return;
			case AdaPackage.DEFINITION_LIST__CLASS_ATTRIBUTE:
				getClassAttribute().clear();
				return;
			case AdaPackage.DEFINITION_LIST__COMMENT:
				getComment().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ALL_CALLS_REMOTE_PRAGMA:
				getAllCallsRemotePragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ASYNCHRONOUS_PRAGMA:
				getAsynchronousPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ATOMIC_PRAGMA:
				getAtomicPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ATOMIC_COMPONENTS_PRAGMA:
				getAtomicComponentsPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ATTACH_HANDLER_PRAGMA:
				getAttachHandlerPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__CONTROLLED_PRAGMA:
				getControlledPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__CONVENTION_PRAGMA:
				getConventionPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__DISCARD_NAMES_PRAGMA:
				getDiscardNamesPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ELABORATE_PRAGMA:
				getElaboratePragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ELABORATE_ALL_PRAGMA:
				getElaborateAllPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ELABORATE_BODY_PRAGMA:
				getElaborateBodyPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__EXPORT_PRAGMA:
				getExportPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__IMPORT_PRAGMA:
				getImportPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__INLINE_PRAGMA:
				getInlinePragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__INSPECTION_POINT_PRAGMA:
				getInspectionPointPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__INTERRUPT_HANDLER_PRAGMA:
				getInterruptHandlerPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__INTERRUPT_PRIORITY_PRAGMA:
				getInterruptPriorityPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__LINKER_OPTIONS_PRAGMA:
				getLinkerOptionsPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__LIST_PRAGMA:
				getListPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__LOCKING_POLICY_PRAGMA:
				getLockingPolicyPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__NORMALIZE_SCALARS_PRAGMA:
				getNormalizeScalarsPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__OPTIMIZE_PRAGMA:
				getOptimizePragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__PACK_PRAGMA:
				getPackPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__PAGE_PRAGMA:
				getPagePragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__PREELABORATE_PRAGMA:
				getPreelaboratePragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__PRIORITY_PRAGMA:
				getPriorityPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__PURE_PRAGMA:
				getPurePragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__QUEUING_POLICY_PRAGMA:
				getQueuingPolicyPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				getRemoteCallInterfacePragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__REMOTE_TYPES_PRAGMA:
				getRemoteTypesPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__RESTRICTIONS_PRAGMA:
				getRestrictionsPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__REVIEWABLE_PRAGMA:
				getReviewablePragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__SHARED_PASSIVE_PRAGMA:
				getSharedPassivePragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__STORAGE_SIZE_PRAGMA:
				getStorageSizePragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__SUPPRESS_PRAGMA:
				getSuppressPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				getTaskDispatchingPolicyPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__VOLATILE_PRAGMA:
				getVolatilePragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__VOLATILE_COMPONENTS_PRAGMA:
				getVolatileComponentsPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ASSERT_PRAGMA:
				getAssertPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__ASSERTION_POLICY_PRAGMA:
				getAssertionPolicyPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__DETECT_BLOCKING_PRAGMA:
				getDetectBlockingPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__NO_RETURN_PRAGMA:
				getNoReturnPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				getPartitionElaborationPolicyPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				getPreelaborableInitializationPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				getPrioritySpecificDispatchingPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__PROFILE_PRAGMA:
				getProfilePragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__RELATIVE_DEADLINE_PRAGMA:
				getRelativeDeadlinePragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__UNCHECKED_UNION_PRAGMA:
				getUncheckedUnionPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__UNSUPPRESS_PRAGMA:
				getUnsuppressPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				getDefaultStoragePoolPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__DISPATCHING_DOMAIN_PRAGMA:
				getDispatchingDomainPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__CPU_PRAGMA:
				getCpuPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__INDEPENDENT_PRAGMA:
				getIndependentPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				getIndependentComponentsPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				getImplementationDefinedPragma().clear();
				return;
			case AdaPackage.DEFINITION_LIST__UNKNOWN_PRAGMA:
				getUnknownPragma().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.DEFINITION_LIST__GROUP:
				return group != null && !group.isEmpty();
			case AdaPackage.DEFINITION_LIST__NOT_AN_ELEMENT:
				return !getNotAnElement().isEmpty();
			case AdaPackage.DEFINITION_LIST__DERIVED_TYPE_DEFINITION:
				return !getDerivedTypeDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__DERIVED_RECORD_EXTENSION_DEFINITION:
				return !getDerivedRecordExtensionDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__ENUMERATION_TYPE_DEFINITION:
				return !getEnumerationTypeDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__SIGNED_INTEGER_TYPE_DEFINITION:
				return !getSignedIntegerTypeDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__MODULAR_TYPE_DEFINITION:
				return !getModularTypeDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__ROOT_INTEGER_DEFINITION:
				return !getRootIntegerDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__ROOT_REAL_DEFINITION:
				return !getRootRealDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__UNIVERSAL_INTEGER_DEFINITION:
				return !getUniversalIntegerDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__UNIVERSAL_REAL_DEFINITION:
				return !getUniversalRealDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__UNIVERSAL_FIXED_DEFINITION:
				return !getUniversalFixedDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__FLOATING_POINT_DEFINITION:
				return !getFloatingPointDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__ORDINARY_FIXED_POINT_DEFINITION:
				return !getOrdinaryFixedPointDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__DECIMAL_FIXED_POINT_DEFINITION:
				return !getDecimalFixedPointDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__UNCONSTRAINED_ARRAY_DEFINITION:
				return !getUnconstrainedArrayDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__CONSTRAINED_ARRAY_DEFINITION:
				return !getConstrainedArrayDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__RECORD_TYPE_DEFINITION:
				return !getRecordTypeDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__TAGGED_RECORD_TYPE_DEFINITION:
				return !getTaggedRecordTypeDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__ORDINARY_INTERFACE:
				return !getOrdinaryInterface().isEmpty();
			case AdaPackage.DEFINITION_LIST__LIMITED_INTERFACE:
				return !getLimitedInterface().isEmpty();
			case AdaPackage.DEFINITION_LIST__TASK_INTERFACE:
				return !getTaskInterface().isEmpty();
			case AdaPackage.DEFINITION_LIST__PROTECTED_INTERFACE:
				return !getProtectedInterface().isEmpty();
			case AdaPackage.DEFINITION_LIST__SYNCHRONIZED_INTERFACE:
				return !getSynchronizedInterface().isEmpty();
			case AdaPackage.DEFINITION_LIST__POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				return !getPoolSpecificAccessToVariable().isEmpty();
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_VARIABLE:
				return !getAccessToVariable().isEmpty();
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_CONSTANT:
				return !getAccessToConstant().isEmpty();
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_PROCEDURE:
				return !getAccessToProcedure().isEmpty();
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_PROTECTED_PROCEDURE:
				return !getAccessToProtectedProcedure().isEmpty();
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_FUNCTION:
				return !getAccessToFunction().isEmpty();
			case AdaPackage.DEFINITION_LIST__ACCESS_TO_PROTECTED_FUNCTION:
				return !getAccessToProtectedFunction().isEmpty();
			case AdaPackage.DEFINITION_LIST__SUBTYPE_INDICATION:
				return !getSubtypeIndication().isEmpty();
			case AdaPackage.DEFINITION_LIST__RANGE_ATTRIBUTE_REFERENCE:
				return !getRangeAttributeReference().isEmpty();
			case AdaPackage.DEFINITION_LIST__SIMPLE_EXPRESSION_RANGE:
				return !getSimpleExpressionRange().isEmpty();
			case AdaPackage.DEFINITION_LIST__DIGITS_CONSTRAINT:
				return !getDigitsConstraint().isEmpty();
			case AdaPackage.DEFINITION_LIST__DELTA_CONSTRAINT:
				return !getDeltaConstraint().isEmpty();
			case AdaPackage.DEFINITION_LIST__INDEX_CONSTRAINT:
				return !getIndexConstraint().isEmpty();
			case AdaPackage.DEFINITION_LIST__DISCRIMINANT_CONSTRAINT:
				return !getDiscriminantConstraint().isEmpty();
			case AdaPackage.DEFINITION_LIST__COMPONENT_DEFINITION:
				return !getComponentDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION:
				return !getDiscreteSubtypeIndicationAsSubtypeDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION:
				return !getDiscreteRangeAttributeReferenceAsSubtypeDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION:
				return !getDiscreteSimpleExpressionRangeAsSubtypeDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__DISCRETE_SUBTYPE_INDICATION:
				return !getDiscreteSubtypeIndication().isEmpty();
			case AdaPackage.DEFINITION_LIST__DISCRETE_RANGE_ATTRIBUTE_REFERENCE:
				return !getDiscreteRangeAttributeReference().isEmpty();
			case AdaPackage.DEFINITION_LIST__DISCRETE_SIMPLE_EXPRESSION_RANGE:
				return !getDiscreteSimpleExpressionRange().isEmpty();
			case AdaPackage.DEFINITION_LIST__UNKNOWN_DISCRIMINANT_PART:
				return !getUnknownDiscriminantPart().isEmpty();
			case AdaPackage.DEFINITION_LIST__KNOWN_DISCRIMINANT_PART:
				return !getKnownDiscriminantPart().isEmpty();
			case AdaPackage.DEFINITION_LIST__RECORD_DEFINITION:
				return !getRecordDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__NULL_RECORD_DEFINITION:
				return !getNullRecordDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__NULL_COMPONENT:
				return !getNullComponent().isEmpty();
			case AdaPackage.DEFINITION_LIST__VARIANT_PART:
				return !getVariantPart().isEmpty();
			case AdaPackage.DEFINITION_LIST__VARIANT:
				return !getVariant().isEmpty();
			case AdaPackage.DEFINITION_LIST__OTHERS_CHOICE:
				return !getOthersChoice().isEmpty();
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_VARIABLE:
				return !getAnonymousAccessToVariable().isEmpty();
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_CONSTANT:
				return !getAnonymousAccessToConstant().isEmpty();
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_PROCEDURE:
				return !getAnonymousAccessToProcedure().isEmpty();
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE:
				return !getAnonymousAccessToProtectedProcedure().isEmpty();
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_FUNCTION:
				return !getAnonymousAccessToFunction().isEmpty();
			case AdaPackage.DEFINITION_LIST__ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION:
				return !getAnonymousAccessToProtectedFunction().isEmpty();
			case AdaPackage.DEFINITION_LIST__PRIVATE_TYPE_DEFINITION:
				return !getPrivateTypeDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__TAGGED_PRIVATE_TYPE_DEFINITION:
				return !getTaggedPrivateTypeDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__PRIVATE_EXTENSION_DEFINITION:
				return !getPrivateExtensionDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__TASK_DEFINITION:
				return !getTaskDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__PROTECTED_DEFINITION:
				return !getProtectedDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__FORMAL_PRIVATE_TYPE_DEFINITION:
				return !getFormalPrivateTypeDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION:
				return !getFormalTaggedPrivateTypeDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__FORMAL_DERIVED_TYPE_DEFINITION:
				return !getFormalDerivedTypeDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__FORMAL_DISCRETE_TYPE_DEFINITION:
				return !getFormalDiscreteTypeDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__FORMAL_SIGNED_INTEGER_TYPE_DEFINITION:
				return !getFormalSignedIntegerTypeDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__FORMAL_MODULAR_TYPE_DEFINITION:
				return !getFormalModularTypeDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__FORMAL_FLOATING_POINT_DEFINITION:
				return !getFormalFloatingPointDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__FORMAL_ORDINARY_FIXED_POINT_DEFINITION:
				return !getFormalOrdinaryFixedPointDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__FORMAL_DECIMAL_FIXED_POINT_DEFINITION:
				return !getFormalDecimalFixedPointDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__FORMAL_ORDINARY_INTERFACE:
				return !getFormalOrdinaryInterface().isEmpty();
			case AdaPackage.DEFINITION_LIST__FORMAL_LIMITED_INTERFACE:
				return !getFormalLimitedInterface().isEmpty();
			case AdaPackage.DEFINITION_LIST__FORMAL_TASK_INTERFACE:
				return !getFormalTaskInterface().isEmpty();
			case AdaPackage.DEFINITION_LIST__FORMAL_PROTECTED_INTERFACE:
				return !getFormalProtectedInterface().isEmpty();
			case AdaPackage.DEFINITION_LIST__FORMAL_SYNCHRONIZED_INTERFACE:
				return !getFormalSynchronizedInterface().isEmpty();
			case AdaPackage.DEFINITION_LIST__FORMAL_UNCONSTRAINED_ARRAY_DEFINITION:
				return !getFormalUnconstrainedArrayDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__FORMAL_CONSTRAINED_ARRAY_DEFINITION:
				return !getFormalConstrainedArrayDefinition().isEmpty();
			case AdaPackage.DEFINITION_LIST__FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				return !getFormalPoolSpecificAccessToVariable().isEmpty();
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_VARIABLE:
				return !getFormalAccessToVariable().isEmpty();
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_CONSTANT:
				return !getFormalAccessToConstant().isEmpty();
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_PROCEDURE:
				return !getFormalAccessToProcedure().isEmpty();
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_PROTECTED_PROCEDURE:
				return !getFormalAccessToProtectedProcedure().isEmpty();
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_FUNCTION:
				return !getFormalAccessToFunction().isEmpty();
			case AdaPackage.DEFINITION_LIST__FORMAL_ACCESS_TO_PROTECTED_FUNCTION:
				return !getFormalAccessToProtectedFunction().isEmpty();
			case AdaPackage.DEFINITION_LIST__ASPECT_SPECIFICATION:
				return !getAspectSpecification().isEmpty();
			case AdaPackage.DEFINITION_LIST__IDENTIFIER:
				return !getIdentifier().isEmpty();
			case AdaPackage.DEFINITION_LIST__SELECTED_COMPONENT:
				return !getSelectedComponent().isEmpty();
			case AdaPackage.DEFINITION_LIST__BASE_ATTRIBUTE:
				return !getBaseAttribute().isEmpty();
			case AdaPackage.DEFINITION_LIST__CLASS_ATTRIBUTE:
				return !getClassAttribute().isEmpty();
			case AdaPackage.DEFINITION_LIST__COMMENT:
				return !getComment().isEmpty();
			case AdaPackage.DEFINITION_LIST__ALL_CALLS_REMOTE_PRAGMA:
				return !getAllCallsRemotePragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__ASYNCHRONOUS_PRAGMA:
				return !getAsynchronousPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__ATOMIC_PRAGMA:
				return !getAtomicPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__ATOMIC_COMPONENTS_PRAGMA:
				return !getAtomicComponentsPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__ATTACH_HANDLER_PRAGMA:
				return !getAttachHandlerPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__CONTROLLED_PRAGMA:
				return !getControlledPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__CONVENTION_PRAGMA:
				return !getConventionPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__DISCARD_NAMES_PRAGMA:
				return !getDiscardNamesPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__ELABORATE_PRAGMA:
				return !getElaboratePragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__ELABORATE_ALL_PRAGMA:
				return !getElaborateAllPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__ELABORATE_BODY_PRAGMA:
				return !getElaborateBodyPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__EXPORT_PRAGMA:
				return !getExportPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__IMPORT_PRAGMA:
				return !getImportPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__INLINE_PRAGMA:
				return !getInlinePragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__INSPECTION_POINT_PRAGMA:
				return !getInspectionPointPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__INTERRUPT_HANDLER_PRAGMA:
				return !getInterruptHandlerPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__INTERRUPT_PRIORITY_PRAGMA:
				return !getInterruptPriorityPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__LINKER_OPTIONS_PRAGMA:
				return !getLinkerOptionsPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__LIST_PRAGMA:
				return !getListPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__LOCKING_POLICY_PRAGMA:
				return !getLockingPolicyPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__NORMALIZE_SCALARS_PRAGMA:
				return !getNormalizeScalarsPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__OPTIMIZE_PRAGMA:
				return !getOptimizePragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__PACK_PRAGMA:
				return !getPackPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__PAGE_PRAGMA:
				return !getPagePragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__PREELABORATE_PRAGMA:
				return !getPreelaboratePragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__PRIORITY_PRAGMA:
				return !getPriorityPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__PURE_PRAGMA:
				return !getPurePragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__QUEUING_POLICY_PRAGMA:
				return !getQueuingPolicyPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				return !getRemoteCallInterfacePragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__REMOTE_TYPES_PRAGMA:
				return !getRemoteTypesPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__RESTRICTIONS_PRAGMA:
				return !getRestrictionsPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__REVIEWABLE_PRAGMA:
				return !getReviewablePragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__SHARED_PASSIVE_PRAGMA:
				return !getSharedPassivePragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__STORAGE_SIZE_PRAGMA:
				return !getStorageSizePragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__SUPPRESS_PRAGMA:
				return !getSuppressPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				return !getTaskDispatchingPolicyPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__VOLATILE_PRAGMA:
				return !getVolatilePragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__VOLATILE_COMPONENTS_PRAGMA:
				return !getVolatileComponentsPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__ASSERT_PRAGMA:
				return !getAssertPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__ASSERTION_POLICY_PRAGMA:
				return !getAssertionPolicyPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__DETECT_BLOCKING_PRAGMA:
				return !getDetectBlockingPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__NO_RETURN_PRAGMA:
				return !getNoReturnPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				return !getPartitionElaborationPolicyPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				return !getPreelaborableInitializationPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return !getPrioritySpecificDispatchingPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__PROFILE_PRAGMA:
				return !getProfilePragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__RELATIVE_DEADLINE_PRAGMA:
				return !getRelativeDeadlinePragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__UNCHECKED_UNION_PRAGMA:
				return !getUncheckedUnionPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__UNSUPPRESS_PRAGMA:
				return !getUnsuppressPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				return !getDefaultStoragePoolPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__DISPATCHING_DOMAIN_PRAGMA:
				return !getDispatchingDomainPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__CPU_PRAGMA:
				return !getCpuPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__INDEPENDENT_PRAGMA:
				return !getIndependentPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				return !getIndependentComponentsPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				return !getImplementationDefinedPragma().isEmpty();
			case AdaPackage.DEFINITION_LIST__UNKNOWN_PRAGMA:
				return !getUnknownPragma().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(')');
		return result.toString();
	}

} //DefinitionListImpl
