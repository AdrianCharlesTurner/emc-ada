/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.FunctionDeclaration#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.FunctionDeclaration#getIsOverridingDeclarationQ <em>Is Overriding Declaration Q</em>}</li>
 *   <li>{@link Ada.FunctionDeclaration#getIsNotOverridingDeclarationQ <em>Is Not Overriding Declaration Q</em>}</li>
 *   <li>{@link Ada.FunctionDeclaration#getNamesQl <em>Names Ql</em>}</li>
 *   <li>{@link Ada.FunctionDeclaration#getParameterProfileQl <em>Parameter Profile Ql</em>}</li>
 *   <li>{@link Ada.FunctionDeclaration#getIsNotNullReturnQ <em>Is Not Null Return Q</em>}</li>
 *   <li>{@link Ada.FunctionDeclaration#getResultProfileQ <em>Result Profile Q</em>}</li>
 *   <li>{@link Ada.FunctionDeclaration#getHasAbstractQ <em>Has Abstract Q</em>}</li>
 *   <li>{@link Ada.FunctionDeclaration#getAspectSpecificationsQl <em>Aspect Specifications Ql</em>}</li>
 *   <li>{@link Ada.FunctionDeclaration#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getFunctionDeclaration()
 * @model extendedMetaData="name='Function_Declaration' kind='elementOnly'"
 * @generated
 */
public interface FunctionDeclaration extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getFunctionDeclaration_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.FunctionDeclaration#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Is Overriding Declaration Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Overriding Declaration Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Overriding Declaration Q</em>' containment reference.
	 * @see #setIsOverridingDeclarationQ(IsOverridingDeclarationQType)
	 * @see Ada.AdaPackage#getFunctionDeclaration_IsOverridingDeclarationQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='is_overriding_declaration_q' namespace='##targetNamespace'"
	 * @generated
	 */
	IsOverridingDeclarationQType getIsOverridingDeclarationQ();

	/**
	 * Sets the value of the '{@link Ada.FunctionDeclaration#getIsOverridingDeclarationQ <em>Is Overriding Declaration Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Overriding Declaration Q</em>' containment reference.
	 * @see #getIsOverridingDeclarationQ()
	 * @generated
	 */
	void setIsOverridingDeclarationQ(IsOverridingDeclarationQType value);

	/**
	 * Returns the value of the '<em><b>Is Not Overriding Declaration Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Not Overriding Declaration Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Not Overriding Declaration Q</em>' containment reference.
	 * @see #setIsNotOverridingDeclarationQ(IsNotOverridingDeclarationQType)
	 * @see Ada.AdaPackage#getFunctionDeclaration_IsNotOverridingDeclarationQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='is_not_overriding_declaration_q' namespace='##targetNamespace'"
	 * @generated
	 */
	IsNotOverridingDeclarationQType getIsNotOverridingDeclarationQ();

	/**
	 * Sets the value of the '{@link Ada.FunctionDeclaration#getIsNotOverridingDeclarationQ <em>Is Not Overriding Declaration Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Not Overriding Declaration Q</em>' containment reference.
	 * @see #getIsNotOverridingDeclarationQ()
	 * @generated
	 */
	void setIsNotOverridingDeclarationQ(IsNotOverridingDeclarationQType value);

	/**
	 * Returns the value of the '<em><b>Names Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Names Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Names Ql</em>' containment reference.
	 * @see #setNamesQl(DefiningNameList)
	 * @see Ada.AdaPackage#getFunctionDeclaration_NamesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='names_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameList getNamesQl();

	/**
	 * Sets the value of the '{@link Ada.FunctionDeclaration#getNamesQl <em>Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Names Ql</em>' containment reference.
	 * @see #getNamesQl()
	 * @generated
	 */
	void setNamesQl(DefiningNameList value);

	/**
	 * Returns the value of the '<em><b>Parameter Profile Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Profile Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Profile Ql</em>' containment reference.
	 * @see #setParameterProfileQl(ParameterSpecificationList)
	 * @see Ada.AdaPackage#getFunctionDeclaration_ParameterProfileQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='parameter_profile_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ParameterSpecificationList getParameterProfileQl();

	/**
	 * Sets the value of the '{@link Ada.FunctionDeclaration#getParameterProfileQl <em>Parameter Profile Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Profile Ql</em>' containment reference.
	 * @see #getParameterProfileQl()
	 * @generated
	 */
	void setParameterProfileQl(ParameterSpecificationList value);

	/**
	 * Returns the value of the '<em><b>Is Not Null Return Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Not Null Return Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Not Null Return Q</em>' containment reference.
	 * @see #setIsNotNullReturnQ(IsNotNullReturnQType11)
	 * @see Ada.AdaPackage#getFunctionDeclaration_IsNotNullReturnQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='is_not_null_return_q' namespace='##targetNamespace'"
	 * @generated
	 */
	IsNotNullReturnQType11 getIsNotNullReturnQ();

	/**
	 * Sets the value of the '{@link Ada.FunctionDeclaration#getIsNotNullReturnQ <em>Is Not Null Return Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Not Null Return Q</em>' containment reference.
	 * @see #getIsNotNullReturnQ()
	 * @generated
	 */
	void setIsNotNullReturnQ(IsNotNullReturnQType11 value);

	/**
	 * Returns the value of the '<em><b>Result Profile Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Result Profile Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Result Profile Q</em>' containment reference.
	 * @see #setResultProfileQ(ElementClass)
	 * @see Ada.AdaPackage#getFunctionDeclaration_ResultProfileQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='result_profile_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementClass getResultProfileQ();

	/**
	 * Sets the value of the '{@link Ada.FunctionDeclaration#getResultProfileQ <em>Result Profile Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Result Profile Q</em>' containment reference.
	 * @see #getResultProfileQ()
	 * @generated
	 */
	void setResultProfileQ(ElementClass value);

	/**
	 * Returns the value of the '<em><b>Has Abstract Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Abstract Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Abstract Q</em>' containment reference.
	 * @see #setHasAbstractQ(HasAbstractQType8)
	 * @see Ada.AdaPackage#getFunctionDeclaration_HasAbstractQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='has_abstract_q' namespace='##targetNamespace'"
	 * @generated
	 */
	HasAbstractQType8 getHasAbstractQ();

	/**
	 * Sets the value of the '{@link Ada.FunctionDeclaration#getHasAbstractQ <em>Has Abstract Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Abstract Q</em>' containment reference.
	 * @see #getHasAbstractQ()
	 * @generated
	 */
	void setHasAbstractQ(HasAbstractQType8 value);

	/**
	 * Returns the value of the '<em><b>Aspect Specifications Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aspect Specifications Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aspect Specifications Ql</em>' containment reference.
	 * @see #setAspectSpecificationsQl(ElementList)
	 * @see Ada.AdaPackage#getFunctionDeclaration_AspectSpecificationsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='aspect_specifications_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementList getAspectSpecificationsQl();

	/**
	 * Sets the value of the '{@link Ada.FunctionDeclaration#getAspectSpecificationsQl <em>Aspect Specifications Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aspect Specifications Ql</em>' containment reference.
	 * @see #getAspectSpecificationsQl()
	 * @generated
	 */
	void setAspectSpecificationsQl(ElementList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getFunctionDeclaration_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.FunctionDeclaration#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // FunctionDeclaration
