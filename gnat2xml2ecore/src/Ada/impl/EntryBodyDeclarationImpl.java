/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.DeclarationClass;
import Ada.DefiningNameList;
import Ada.ElementList;
import Ada.EntryBodyDeclaration;
import Ada.ExceptionHandlerList;
import Ada.ExpressionClass;
import Ada.ParameterSpecificationList;
import Ada.SourceLocation;
import Ada.StatementList;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Entry Body Declaration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.EntryBodyDeclarationImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.EntryBodyDeclarationImpl#getNamesQl <em>Names Ql</em>}</li>
 *   <li>{@link Ada.impl.EntryBodyDeclarationImpl#getEntryIndexSpecificationQ <em>Entry Index Specification Q</em>}</li>
 *   <li>{@link Ada.impl.EntryBodyDeclarationImpl#getParameterProfileQl <em>Parameter Profile Ql</em>}</li>
 *   <li>{@link Ada.impl.EntryBodyDeclarationImpl#getEntryBarrierQ <em>Entry Barrier Q</em>}</li>
 *   <li>{@link Ada.impl.EntryBodyDeclarationImpl#getBodyDeclarativeItemsQl <em>Body Declarative Items Ql</em>}</li>
 *   <li>{@link Ada.impl.EntryBodyDeclarationImpl#getBodyStatementsQl <em>Body Statements Ql</em>}</li>
 *   <li>{@link Ada.impl.EntryBodyDeclarationImpl#getBodyExceptionHandlersQl <em>Body Exception Handlers Ql</em>}</li>
 *   <li>{@link Ada.impl.EntryBodyDeclarationImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EntryBodyDeclarationImpl extends MinimalEObjectImpl.Container implements EntryBodyDeclaration {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getNamesQl() <em>Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamesQl()
	 * @generated
	 * @ordered
	 */
	protected DefiningNameList namesQl;

	/**
	 * The cached value of the '{@link #getEntryIndexSpecificationQ() <em>Entry Index Specification Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntryIndexSpecificationQ()
	 * @generated
	 * @ordered
	 */
	protected DeclarationClass entryIndexSpecificationQ;

	/**
	 * The cached value of the '{@link #getParameterProfileQl() <em>Parameter Profile Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterProfileQl()
	 * @generated
	 * @ordered
	 */
	protected ParameterSpecificationList parameterProfileQl;

	/**
	 * The cached value of the '{@link #getEntryBarrierQ() <em>Entry Barrier Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntryBarrierQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass entryBarrierQ;

	/**
	 * The cached value of the '{@link #getBodyDeclarativeItemsQl() <em>Body Declarative Items Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBodyDeclarativeItemsQl()
	 * @generated
	 * @ordered
	 */
	protected ElementList bodyDeclarativeItemsQl;

	/**
	 * The cached value of the '{@link #getBodyStatementsQl() <em>Body Statements Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBodyStatementsQl()
	 * @generated
	 * @ordered
	 */
	protected StatementList bodyStatementsQl;

	/**
	 * The cached value of the '{@link #getBodyExceptionHandlersQl() <em>Body Exception Handlers Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBodyExceptionHandlersQl()
	 * @generated
	 * @ordered
	 */
	protected ExceptionHandlerList bodyExceptionHandlersQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EntryBodyDeclarationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getEntryBodyDeclaration();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_BODY_DECLARATION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_BODY_DECLARATION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_BODY_DECLARATION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_BODY_DECLARATION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefiningNameList getNamesQl() {
		return namesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNamesQl(DefiningNameList newNamesQl, NotificationChain msgs) {
		DefiningNameList oldNamesQl = namesQl;
		namesQl = newNamesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_BODY_DECLARATION__NAMES_QL, oldNamesQl, newNamesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNamesQl(DefiningNameList newNamesQl) {
		if (newNamesQl != namesQl) {
			NotificationChain msgs = null;
			if (namesQl != null)
				msgs = ((InternalEObject)namesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_BODY_DECLARATION__NAMES_QL, null, msgs);
			if (newNamesQl != null)
				msgs = ((InternalEObject)newNamesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_BODY_DECLARATION__NAMES_QL, null, msgs);
			msgs = basicSetNamesQl(newNamesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_BODY_DECLARATION__NAMES_QL, newNamesQl, newNamesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeclarationClass getEntryIndexSpecificationQ() {
		return entryIndexSpecificationQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEntryIndexSpecificationQ(DeclarationClass newEntryIndexSpecificationQ, NotificationChain msgs) {
		DeclarationClass oldEntryIndexSpecificationQ = entryIndexSpecificationQ;
		entryIndexSpecificationQ = newEntryIndexSpecificationQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_BODY_DECLARATION__ENTRY_INDEX_SPECIFICATION_Q, oldEntryIndexSpecificationQ, newEntryIndexSpecificationQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntryIndexSpecificationQ(DeclarationClass newEntryIndexSpecificationQ) {
		if (newEntryIndexSpecificationQ != entryIndexSpecificationQ) {
			NotificationChain msgs = null;
			if (entryIndexSpecificationQ != null)
				msgs = ((InternalEObject)entryIndexSpecificationQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_BODY_DECLARATION__ENTRY_INDEX_SPECIFICATION_Q, null, msgs);
			if (newEntryIndexSpecificationQ != null)
				msgs = ((InternalEObject)newEntryIndexSpecificationQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_BODY_DECLARATION__ENTRY_INDEX_SPECIFICATION_Q, null, msgs);
			msgs = basicSetEntryIndexSpecificationQ(newEntryIndexSpecificationQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_BODY_DECLARATION__ENTRY_INDEX_SPECIFICATION_Q, newEntryIndexSpecificationQ, newEntryIndexSpecificationQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterSpecificationList getParameterProfileQl() {
		return parameterProfileQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParameterProfileQl(ParameterSpecificationList newParameterProfileQl, NotificationChain msgs) {
		ParameterSpecificationList oldParameterProfileQl = parameterProfileQl;
		parameterProfileQl = newParameterProfileQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_BODY_DECLARATION__PARAMETER_PROFILE_QL, oldParameterProfileQl, newParameterProfileQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterProfileQl(ParameterSpecificationList newParameterProfileQl) {
		if (newParameterProfileQl != parameterProfileQl) {
			NotificationChain msgs = null;
			if (parameterProfileQl != null)
				msgs = ((InternalEObject)parameterProfileQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_BODY_DECLARATION__PARAMETER_PROFILE_QL, null, msgs);
			if (newParameterProfileQl != null)
				msgs = ((InternalEObject)newParameterProfileQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_BODY_DECLARATION__PARAMETER_PROFILE_QL, null, msgs);
			msgs = basicSetParameterProfileQl(newParameterProfileQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_BODY_DECLARATION__PARAMETER_PROFILE_QL, newParameterProfileQl, newParameterProfileQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getEntryBarrierQ() {
		return entryBarrierQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEntryBarrierQ(ExpressionClass newEntryBarrierQ, NotificationChain msgs) {
		ExpressionClass oldEntryBarrierQ = entryBarrierQ;
		entryBarrierQ = newEntryBarrierQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_BODY_DECLARATION__ENTRY_BARRIER_Q, oldEntryBarrierQ, newEntryBarrierQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntryBarrierQ(ExpressionClass newEntryBarrierQ) {
		if (newEntryBarrierQ != entryBarrierQ) {
			NotificationChain msgs = null;
			if (entryBarrierQ != null)
				msgs = ((InternalEObject)entryBarrierQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_BODY_DECLARATION__ENTRY_BARRIER_Q, null, msgs);
			if (newEntryBarrierQ != null)
				msgs = ((InternalEObject)newEntryBarrierQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_BODY_DECLARATION__ENTRY_BARRIER_Q, null, msgs);
			msgs = basicSetEntryBarrierQ(newEntryBarrierQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_BODY_DECLARATION__ENTRY_BARRIER_Q, newEntryBarrierQ, newEntryBarrierQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementList getBodyDeclarativeItemsQl() {
		return bodyDeclarativeItemsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBodyDeclarativeItemsQl(ElementList newBodyDeclarativeItemsQl, NotificationChain msgs) {
		ElementList oldBodyDeclarativeItemsQl = bodyDeclarativeItemsQl;
		bodyDeclarativeItemsQl = newBodyDeclarativeItemsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_BODY_DECLARATION__BODY_DECLARATIVE_ITEMS_QL, oldBodyDeclarativeItemsQl, newBodyDeclarativeItemsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBodyDeclarativeItemsQl(ElementList newBodyDeclarativeItemsQl) {
		if (newBodyDeclarativeItemsQl != bodyDeclarativeItemsQl) {
			NotificationChain msgs = null;
			if (bodyDeclarativeItemsQl != null)
				msgs = ((InternalEObject)bodyDeclarativeItemsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_BODY_DECLARATION__BODY_DECLARATIVE_ITEMS_QL, null, msgs);
			if (newBodyDeclarativeItemsQl != null)
				msgs = ((InternalEObject)newBodyDeclarativeItemsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_BODY_DECLARATION__BODY_DECLARATIVE_ITEMS_QL, null, msgs);
			msgs = basicSetBodyDeclarativeItemsQl(newBodyDeclarativeItemsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_BODY_DECLARATION__BODY_DECLARATIVE_ITEMS_QL, newBodyDeclarativeItemsQl, newBodyDeclarativeItemsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatementList getBodyStatementsQl() {
		return bodyStatementsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBodyStatementsQl(StatementList newBodyStatementsQl, NotificationChain msgs) {
		StatementList oldBodyStatementsQl = bodyStatementsQl;
		bodyStatementsQl = newBodyStatementsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_BODY_DECLARATION__BODY_STATEMENTS_QL, oldBodyStatementsQl, newBodyStatementsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBodyStatementsQl(StatementList newBodyStatementsQl) {
		if (newBodyStatementsQl != bodyStatementsQl) {
			NotificationChain msgs = null;
			if (bodyStatementsQl != null)
				msgs = ((InternalEObject)bodyStatementsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_BODY_DECLARATION__BODY_STATEMENTS_QL, null, msgs);
			if (newBodyStatementsQl != null)
				msgs = ((InternalEObject)newBodyStatementsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_BODY_DECLARATION__BODY_STATEMENTS_QL, null, msgs);
			msgs = basicSetBodyStatementsQl(newBodyStatementsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_BODY_DECLARATION__BODY_STATEMENTS_QL, newBodyStatementsQl, newBodyStatementsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExceptionHandlerList getBodyExceptionHandlersQl() {
		return bodyExceptionHandlersQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBodyExceptionHandlersQl(ExceptionHandlerList newBodyExceptionHandlersQl, NotificationChain msgs) {
		ExceptionHandlerList oldBodyExceptionHandlersQl = bodyExceptionHandlersQl;
		bodyExceptionHandlersQl = newBodyExceptionHandlersQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_BODY_DECLARATION__BODY_EXCEPTION_HANDLERS_QL, oldBodyExceptionHandlersQl, newBodyExceptionHandlersQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBodyExceptionHandlersQl(ExceptionHandlerList newBodyExceptionHandlersQl) {
		if (newBodyExceptionHandlersQl != bodyExceptionHandlersQl) {
			NotificationChain msgs = null;
			if (bodyExceptionHandlersQl != null)
				msgs = ((InternalEObject)bodyExceptionHandlersQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_BODY_DECLARATION__BODY_EXCEPTION_HANDLERS_QL, null, msgs);
			if (newBodyExceptionHandlersQl != null)
				msgs = ((InternalEObject)newBodyExceptionHandlersQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ENTRY_BODY_DECLARATION__BODY_EXCEPTION_HANDLERS_QL, null, msgs);
			msgs = basicSetBodyExceptionHandlersQl(newBodyExceptionHandlersQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_BODY_DECLARATION__BODY_EXCEPTION_HANDLERS_QL, newBodyExceptionHandlersQl, newBodyExceptionHandlersQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ENTRY_BODY_DECLARATION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.ENTRY_BODY_DECLARATION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.ENTRY_BODY_DECLARATION__NAMES_QL:
				return basicSetNamesQl(null, msgs);
			case AdaPackage.ENTRY_BODY_DECLARATION__ENTRY_INDEX_SPECIFICATION_Q:
				return basicSetEntryIndexSpecificationQ(null, msgs);
			case AdaPackage.ENTRY_BODY_DECLARATION__PARAMETER_PROFILE_QL:
				return basicSetParameterProfileQl(null, msgs);
			case AdaPackage.ENTRY_BODY_DECLARATION__ENTRY_BARRIER_Q:
				return basicSetEntryBarrierQ(null, msgs);
			case AdaPackage.ENTRY_BODY_DECLARATION__BODY_DECLARATIVE_ITEMS_QL:
				return basicSetBodyDeclarativeItemsQl(null, msgs);
			case AdaPackage.ENTRY_BODY_DECLARATION__BODY_STATEMENTS_QL:
				return basicSetBodyStatementsQl(null, msgs);
			case AdaPackage.ENTRY_BODY_DECLARATION__BODY_EXCEPTION_HANDLERS_QL:
				return basicSetBodyExceptionHandlersQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.ENTRY_BODY_DECLARATION__SLOC:
				return getSloc();
			case AdaPackage.ENTRY_BODY_DECLARATION__NAMES_QL:
				return getNamesQl();
			case AdaPackage.ENTRY_BODY_DECLARATION__ENTRY_INDEX_SPECIFICATION_Q:
				return getEntryIndexSpecificationQ();
			case AdaPackage.ENTRY_BODY_DECLARATION__PARAMETER_PROFILE_QL:
				return getParameterProfileQl();
			case AdaPackage.ENTRY_BODY_DECLARATION__ENTRY_BARRIER_Q:
				return getEntryBarrierQ();
			case AdaPackage.ENTRY_BODY_DECLARATION__BODY_DECLARATIVE_ITEMS_QL:
				return getBodyDeclarativeItemsQl();
			case AdaPackage.ENTRY_BODY_DECLARATION__BODY_STATEMENTS_QL:
				return getBodyStatementsQl();
			case AdaPackage.ENTRY_BODY_DECLARATION__BODY_EXCEPTION_HANDLERS_QL:
				return getBodyExceptionHandlersQl();
			case AdaPackage.ENTRY_BODY_DECLARATION__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.ENTRY_BODY_DECLARATION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.ENTRY_BODY_DECLARATION__NAMES_QL:
				setNamesQl((DefiningNameList)newValue);
				return;
			case AdaPackage.ENTRY_BODY_DECLARATION__ENTRY_INDEX_SPECIFICATION_Q:
				setEntryIndexSpecificationQ((DeclarationClass)newValue);
				return;
			case AdaPackage.ENTRY_BODY_DECLARATION__PARAMETER_PROFILE_QL:
				setParameterProfileQl((ParameterSpecificationList)newValue);
				return;
			case AdaPackage.ENTRY_BODY_DECLARATION__ENTRY_BARRIER_Q:
				setEntryBarrierQ((ExpressionClass)newValue);
				return;
			case AdaPackage.ENTRY_BODY_DECLARATION__BODY_DECLARATIVE_ITEMS_QL:
				setBodyDeclarativeItemsQl((ElementList)newValue);
				return;
			case AdaPackage.ENTRY_BODY_DECLARATION__BODY_STATEMENTS_QL:
				setBodyStatementsQl((StatementList)newValue);
				return;
			case AdaPackage.ENTRY_BODY_DECLARATION__BODY_EXCEPTION_HANDLERS_QL:
				setBodyExceptionHandlersQl((ExceptionHandlerList)newValue);
				return;
			case AdaPackage.ENTRY_BODY_DECLARATION__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.ENTRY_BODY_DECLARATION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.ENTRY_BODY_DECLARATION__NAMES_QL:
				setNamesQl((DefiningNameList)null);
				return;
			case AdaPackage.ENTRY_BODY_DECLARATION__ENTRY_INDEX_SPECIFICATION_Q:
				setEntryIndexSpecificationQ((DeclarationClass)null);
				return;
			case AdaPackage.ENTRY_BODY_DECLARATION__PARAMETER_PROFILE_QL:
				setParameterProfileQl((ParameterSpecificationList)null);
				return;
			case AdaPackage.ENTRY_BODY_DECLARATION__ENTRY_BARRIER_Q:
				setEntryBarrierQ((ExpressionClass)null);
				return;
			case AdaPackage.ENTRY_BODY_DECLARATION__BODY_DECLARATIVE_ITEMS_QL:
				setBodyDeclarativeItemsQl((ElementList)null);
				return;
			case AdaPackage.ENTRY_BODY_DECLARATION__BODY_STATEMENTS_QL:
				setBodyStatementsQl((StatementList)null);
				return;
			case AdaPackage.ENTRY_BODY_DECLARATION__BODY_EXCEPTION_HANDLERS_QL:
				setBodyExceptionHandlersQl((ExceptionHandlerList)null);
				return;
			case AdaPackage.ENTRY_BODY_DECLARATION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.ENTRY_BODY_DECLARATION__SLOC:
				return sloc != null;
			case AdaPackage.ENTRY_BODY_DECLARATION__NAMES_QL:
				return namesQl != null;
			case AdaPackage.ENTRY_BODY_DECLARATION__ENTRY_INDEX_SPECIFICATION_Q:
				return entryIndexSpecificationQ != null;
			case AdaPackage.ENTRY_BODY_DECLARATION__PARAMETER_PROFILE_QL:
				return parameterProfileQl != null;
			case AdaPackage.ENTRY_BODY_DECLARATION__ENTRY_BARRIER_Q:
				return entryBarrierQ != null;
			case AdaPackage.ENTRY_BODY_DECLARATION__BODY_DECLARATIVE_ITEMS_QL:
				return bodyDeclarativeItemsQl != null;
			case AdaPackage.ENTRY_BODY_DECLARATION__BODY_STATEMENTS_QL:
				return bodyStatementsQl != null;
			case AdaPackage.ENTRY_BODY_DECLARATION__BODY_EXCEPTION_HANDLERS_QL:
				return bodyExceptionHandlersQl != null;
			case AdaPackage.ENTRY_BODY_DECLARATION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //EntryBodyDeclarationImpl
