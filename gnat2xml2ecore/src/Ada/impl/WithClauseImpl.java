/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.HasLimitedQType;
import Ada.HasPrivateQType1;
import Ada.NameList;
import Ada.SourceLocation;
import Ada.WithClause;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>With Clause</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.WithClauseImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.WithClauseImpl#getHasLimitedQ <em>Has Limited Q</em>}</li>
 *   <li>{@link Ada.impl.WithClauseImpl#getHasPrivateQ <em>Has Private Q</em>}</li>
 *   <li>{@link Ada.impl.WithClauseImpl#getClauseNamesQl <em>Clause Names Ql</em>}</li>
 *   <li>{@link Ada.impl.WithClauseImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class WithClauseImpl extends MinimalEObjectImpl.Container implements WithClause {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getHasLimitedQ() <em>Has Limited Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasLimitedQ()
	 * @generated
	 * @ordered
	 */
	protected HasLimitedQType hasLimitedQ;

	/**
	 * The cached value of the '{@link #getHasPrivateQ() <em>Has Private Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasPrivateQ()
	 * @generated
	 * @ordered
	 */
	protected HasPrivateQType1 hasPrivateQ;

	/**
	 * The cached value of the '{@link #getClauseNamesQl() <em>Clause Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClauseNamesQl()
	 * @generated
	 * @ordered
	 */
	protected NameList clauseNamesQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WithClauseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getWithClause();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.WITH_CLAUSE__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.WITH_CLAUSE__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.WITH_CLAUSE__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.WITH_CLAUSE__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasLimitedQType getHasLimitedQ() {
		return hasLimitedQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasLimitedQ(HasLimitedQType newHasLimitedQ, NotificationChain msgs) {
		HasLimitedQType oldHasLimitedQ = hasLimitedQ;
		hasLimitedQ = newHasLimitedQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.WITH_CLAUSE__HAS_LIMITED_Q, oldHasLimitedQ, newHasLimitedQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasLimitedQ(HasLimitedQType newHasLimitedQ) {
		if (newHasLimitedQ != hasLimitedQ) {
			NotificationChain msgs = null;
			if (hasLimitedQ != null)
				msgs = ((InternalEObject)hasLimitedQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.WITH_CLAUSE__HAS_LIMITED_Q, null, msgs);
			if (newHasLimitedQ != null)
				msgs = ((InternalEObject)newHasLimitedQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.WITH_CLAUSE__HAS_LIMITED_Q, null, msgs);
			msgs = basicSetHasLimitedQ(newHasLimitedQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.WITH_CLAUSE__HAS_LIMITED_Q, newHasLimitedQ, newHasLimitedQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasPrivateQType1 getHasPrivateQ() {
		return hasPrivateQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasPrivateQ(HasPrivateQType1 newHasPrivateQ, NotificationChain msgs) {
		HasPrivateQType1 oldHasPrivateQ = hasPrivateQ;
		hasPrivateQ = newHasPrivateQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.WITH_CLAUSE__HAS_PRIVATE_Q, oldHasPrivateQ, newHasPrivateQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasPrivateQ(HasPrivateQType1 newHasPrivateQ) {
		if (newHasPrivateQ != hasPrivateQ) {
			NotificationChain msgs = null;
			if (hasPrivateQ != null)
				msgs = ((InternalEObject)hasPrivateQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.WITH_CLAUSE__HAS_PRIVATE_Q, null, msgs);
			if (newHasPrivateQ != null)
				msgs = ((InternalEObject)newHasPrivateQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.WITH_CLAUSE__HAS_PRIVATE_Q, null, msgs);
			msgs = basicSetHasPrivateQ(newHasPrivateQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.WITH_CLAUSE__HAS_PRIVATE_Q, newHasPrivateQ, newHasPrivateQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NameList getClauseNamesQl() {
		return clauseNamesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClauseNamesQl(NameList newClauseNamesQl, NotificationChain msgs) {
		NameList oldClauseNamesQl = clauseNamesQl;
		clauseNamesQl = newClauseNamesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.WITH_CLAUSE__CLAUSE_NAMES_QL, oldClauseNamesQl, newClauseNamesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClauseNamesQl(NameList newClauseNamesQl) {
		if (newClauseNamesQl != clauseNamesQl) {
			NotificationChain msgs = null;
			if (clauseNamesQl != null)
				msgs = ((InternalEObject)clauseNamesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.WITH_CLAUSE__CLAUSE_NAMES_QL, null, msgs);
			if (newClauseNamesQl != null)
				msgs = ((InternalEObject)newClauseNamesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.WITH_CLAUSE__CLAUSE_NAMES_QL, null, msgs);
			msgs = basicSetClauseNamesQl(newClauseNamesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.WITH_CLAUSE__CLAUSE_NAMES_QL, newClauseNamesQl, newClauseNamesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.WITH_CLAUSE__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.WITH_CLAUSE__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.WITH_CLAUSE__HAS_LIMITED_Q:
				return basicSetHasLimitedQ(null, msgs);
			case AdaPackage.WITH_CLAUSE__HAS_PRIVATE_Q:
				return basicSetHasPrivateQ(null, msgs);
			case AdaPackage.WITH_CLAUSE__CLAUSE_NAMES_QL:
				return basicSetClauseNamesQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.WITH_CLAUSE__SLOC:
				return getSloc();
			case AdaPackage.WITH_CLAUSE__HAS_LIMITED_Q:
				return getHasLimitedQ();
			case AdaPackage.WITH_CLAUSE__HAS_PRIVATE_Q:
				return getHasPrivateQ();
			case AdaPackage.WITH_CLAUSE__CLAUSE_NAMES_QL:
				return getClauseNamesQl();
			case AdaPackage.WITH_CLAUSE__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.WITH_CLAUSE__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.WITH_CLAUSE__HAS_LIMITED_Q:
				setHasLimitedQ((HasLimitedQType)newValue);
				return;
			case AdaPackage.WITH_CLAUSE__HAS_PRIVATE_Q:
				setHasPrivateQ((HasPrivateQType1)newValue);
				return;
			case AdaPackage.WITH_CLAUSE__CLAUSE_NAMES_QL:
				setClauseNamesQl((NameList)newValue);
				return;
			case AdaPackage.WITH_CLAUSE__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.WITH_CLAUSE__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.WITH_CLAUSE__HAS_LIMITED_Q:
				setHasLimitedQ((HasLimitedQType)null);
				return;
			case AdaPackage.WITH_CLAUSE__HAS_PRIVATE_Q:
				setHasPrivateQ((HasPrivateQType1)null);
				return;
			case AdaPackage.WITH_CLAUSE__CLAUSE_NAMES_QL:
				setClauseNamesQl((NameList)null);
				return;
			case AdaPackage.WITH_CLAUSE__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.WITH_CLAUSE__SLOC:
				return sloc != null;
			case AdaPackage.WITH_CLAUSE__HAS_LIMITED_Q:
				return hasLimitedQ != null;
			case AdaPackage.WITH_CLAUSE__HAS_PRIVATE_Q:
				return hasPrivateQ != null;
			case AdaPackage.WITH_CLAUSE__CLAUSE_NAMES_QL:
				return clauseNamesQl != null;
			case AdaPackage.WITH_CLAUSE__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //WithClauseImpl
