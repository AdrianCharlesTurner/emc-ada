/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>While Loop Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.WhileLoopStatement#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.WhileLoopStatement#getLabelNamesQl <em>Label Names Ql</em>}</li>
 *   <li>{@link Ada.WhileLoopStatement#getStatementIdentifierQ <em>Statement Identifier Q</em>}</li>
 *   <li>{@link Ada.WhileLoopStatement#getWhileConditionQ <em>While Condition Q</em>}</li>
 *   <li>{@link Ada.WhileLoopStatement#getLoopStatementsQl <em>Loop Statements Ql</em>}</li>
 *   <li>{@link Ada.WhileLoopStatement#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getWhileLoopStatement()
 * @model extendedMetaData="name='While_Loop_Statement' kind='elementOnly'"
 * @generated
 */
public interface WhileLoopStatement extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getWhileLoopStatement_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.WhileLoopStatement#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Label Names Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label Names Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label Names Ql</em>' containment reference.
	 * @see #setLabelNamesQl(DefiningNameList)
	 * @see Ada.AdaPackage#getWhileLoopStatement_LabelNamesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='label_names_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameList getLabelNamesQl();

	/**
	 * Sets the value of the '{@link Ada.WhileLoopStatement#getLabelNamesQl <em>Label Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label Names Ql</em>' containment reference.
	 * @see #getLabelNamesQl()
	 * @generated
	 */
	void setLabelNamesQl(DefiningNameList value);

	/**
	 * Returns the value of the '<em><b>Statement Identifier Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Statement Identifier Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Statement Identifier Q</em>' containment reference.
	 * @see #setStatementIdentifierQ(DefiningNameClass)
	 * @see Ada.AdaPackage#getWhileLoopStatement_StatementIdentifierQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='statement_identifier_q' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameClass getStatementIdentifierQ();

	/**
	 * Sets the value of the '{@link Ada.WhileLoopStatement#getStatementIdentifierQ <em>Statement Identifier Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Statement Identifier Q</em>' containment reference.
	 * @see #getStatementIdentifierQ()
	 * @generated
	 */
	void setStatementIdentifierQ(DefiningNameClass value);

	/**
	 * Returns the value of the '<em><b>While Condition Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>While Condition Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>While Condition Q</em>' containment reference.
	 * @see #setWhileConditionQ(ExpressionClass)
	 * @see Ada.AdaPackage#getWhileLoopStatement_WhileConditionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='while_condition_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getWhileConditionQ();

	/**
	 * Sets the value of the '{@link Ada.WhileLoopStatement#getWhileConditionQ <em>While Condition Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>While Condition Q</em>' containment reference.
	 * @see #getWhileConditionQ()
	 * @generated
	 */
	void setWhileConditionQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Loop Statements Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Loop Statements Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Loop Statements Ql</em>' containment reference.
	 * @see #setLoopStatementsQl(StatementList)
	 * @see Ada.AdaPackage#getWhileLoopStatement_LoopStatementsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='loop_statements_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	StatementList getLoopStatementsQl();

	/**
	 * Sets the value of the '{@link Ada.WhileLoopStatement#getLoopStatementsQl <em>Loop Statements Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Loop Statements Ql</em>' containment reference.
	 * @see #getLoopStatementsQl()
	 * @generated
	 */
	void setLoopStatementsQl(StatementList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getWhileLoopStatement_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.WhileLoopStatement#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // WhileLoopStatement
