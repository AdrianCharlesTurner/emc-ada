/**
 */
package Ada.impl;

import Ada.AccessToConstant;
import Ada.AccessToFunction;
import Ada.AccessToProcedure;
import Ada.AccessToProtectedFunction;
import Ada.AccessToProtectedProcedure;
import Ada.AccessToVariable;
import Ada.AdaPackage;
import Ada.AllCallsRemotePragma;
import Ada.AnonymousAccessToConstant;
import Ada.AnonymousAccessToFunction;
import Ada.AnonymousAccessToProcedure;
import Ada.AnonymousAccessToProtectedFunction;
import Ada.AnonymousAccessToProtectedProcedure;
import Ada.AnonymousAccessToVariable;
import Ada.AspectSpecification;
import Ada.AssertPragma;
import Ada.AssertionPolicyPragma;
import Ada.AsynchronousPragma;
import Ada.AtomicComponentsPragma;
import Ada.AtomicPragma;
import Ada.AttachHandlerPragma;
import Ada.BaseAttribute;
import Ada.ClassAttribute;
import Ada.Comment;
import Ada.ComponentDefinition;
import Ada.ConstrainedArrayDefinition;
import Ada.ControlledPragma;
import Ada.ConventionPragma;
import Ada.CpuPragma;
import Ada.DecimalFixedPointDefinition;
import Ada.DefaultStoragePoolPragma;
import Ada.DefinitionClass;
import Ada.DeltaConstraint;
import Ada.DerivedRecordExtensionDefinition;
import Ada.DerivedTypeDefinition;
import Ada.DetectBlockingPragma;
import Ada.DigitsConstraint;
import Ada.DiscardNamesPragma;
import Ada.DiscreteRangeAttributeReference;
import Ada.DiscreteRangeAttributeReferenceAsSubtypeDefinition;
import Ada.DiscreteSimpleExpressionRange;
import Ada.DiscreteSimpleExpressionRangeAsSubtypeDefinition;
import Ada.DiscreteSubtypeIndication;
import Ada.DiscreteSubtypeIndicationAsSubtypeDefinition;
import Ada.DiscriminantConstraint;
import Ada.DispatchingDomainPragma;
import Ada.ElaborateAllPragma;
import Ada.ElaborateBodyPragma;
import Ada.ElaboratePragma;
import Ada.EnumerationTypeDefinition;
import Ada.ExportPragma;
import Ada.FloatingPointDefinition;
import Ada.FormalAccessToConstant;
import Ada.FormalAccessToFunction;
import Ada.FormalAccessToProcedure;
import Ada.FormalAccessToProtectedFunction;
import Ada.FormalAccessToProtectedProcedure;
import Ada.FormalAccessToVariable;
import Ada.FormalConstrainedArrayDefinition;
import Ada.FormalDecimalFixedPointDefinition;
import Ada.FormalDerivedTypeDefinition;
import Ada.FormalDiscreteTypeDefinition;
import Ada.FormalFloatingPointDefinition;
import Ada.FormalLimitedInterface;
import Ada.FormalModularTypeDefinition;
import Ada.FormalOrdinaryFixedPointDefinition;
import Ada.FormalOrdinaryInterface;
import Ada.FormalPoolSpecificAccessToVariable;
import Ada.FormalPrivateTypeDefinition;
import Ada.FormalProtectedInterface;
import Ada.FormalSignedIntegerTypeDefinition;
import Ada.FormalSynchronizedInterface;
import Ada.FormalTaggedPrivateTypeDefinition;
import Ada.FormalTaskInterface;
import Ada.FormalUnconstrainedArrayDefinition;
import Ada.Identifier;
import Ada.ImplementationDefinedPragma;
import Ada.ImportPragma;
import Ada.IndependentComponentsPragma;
import Ada.IndependentPragma;
import Ada.IndexConstraint;
import Ada.InlinePragma;
import Ada.InspectionPointPragma;
import Ada.InterruptHandlerPragma;
import Ada.InterruptPriorityPragma;
import Ada.KnownDiscriminantPart;
import Ada.LimitedInterface;
import Ada.LinkerOptionsPragma;
import Ada.ListPragma;
import Ada.LockingPolicyPragma;
import Ada.ModularTypeDefinition;
import Ada.NoReturnPragma;
import Ada.NormalizeScalarsPragma;
import Ada.NotAnElement;
import Ada.NullComponent;
import Ada.NullRecordDefinition;
import Ada.OptimizePragma;
import Ada.OrdinaryFixedPointDefinition;
import Ada.OrdinaryInterface;
import Ada.OthersChoice;
import Ada.PackPragma;
import Ada.PagePragma;
import Ada.PartitionElaborationPolicyPragma;
import Ada.PoolSpecificAccessToVariable;
import Ada.PreelaborableInitializationPragma;
import Ada.PreelaboratePragma;
import Ada.PriorityPragma;
import Ada.PrioritySpecificDispatchingPragma;
import Ada.PrivateExtensionDefinition;
import Ada.PrivateTypeDefinition;
import Ada.ProfilePragma;
import Ada.ProtectedDefinition;
import Ada.ProtectedInterface;
import Ada.PurePragma;
import Ada.QueuingPolicyPragma;
import Ada.RangeAttributeReference;
import Ada.RecordDefinition;
import Ada.RecordTypeDefinition;
import Ada.RelativeDeadlinePragma;
import Ada.RemoteCallInterfacePragma;
import Ada.RemoteTypesPragma;
import Ada.RestrictionsPragma;
import Ada.ReviewablePragma;
import Ada.RootIntegerDefinition;
import Ada.RootRealDefinition;
import Ada.SelectedComponent;
import Ada.SharedPassivePragma;
import Ada.SignedIntegerTypeDefinition;
import Ada.SimpleExpressionRange;
import Ada.StorageSizePragma;
import Ada.SubtypeIndication;
import Ada.SuppressPragma;
import Ada.SynchronizedInterface;
import Ada.TaggedPrivateTypeDefinition;
import Ada.TaggedRecordTypeDefinition;
import Ada.TaskDefinition;
import Ada.TaskDispatchingPolicyPragma;
import Ada.TaskInterface;
import Ada.UncheckedUnionPragma;
import Ada.UnconstrainedArrayDefinition;
import Ada.UniversalFixedDefinition;
import Ada.UniversalIntegerDefinition;
import Ada.UniversalRealDefinition;
import Ada.UnknownDiscriminantPart;
import Ada.UnknownPragma;
import Ada.UnsuppressPragma;
import Ada.Variant;
import Ada.VariantPart;
import Ada.VolatileComponentsPragma;
import Ada.VolatilePragma;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Definition Class</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getNotAnElement <em>Not An Element</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getDerivedTypeDefinition <em>Derived Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getDerivedRecordExtensionDefinition <em>Derived Record Extension Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getEnumerationTypeDefinition <em>Enumeration Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getSignedIntegerTypeDefinition <em>Signed Integer Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getModularTypeDefinition <em>Modular Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getRootIntegerDefinition <em>Root Integer Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getRootRealDefinition <em>Root Real Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getUniversalIntegerDefinition <em>Universal Integer Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getUniversalRealDefinition <em>Universal Real Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getUniversalFixedDefinition <em>Universal Fixed Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getFloatingPointDefinition <em>Floating Point Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getOrdinaryFixedPointDefinition <em>Ordinary Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getDecimalFixedPointDefinition <em>Decimal Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getUnconstrainedArrayDefinition <em>Unconstrained Array Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getConstrainedArrayDefinition <em>Constrained Array Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getRecordTypeDefinition <em>Record Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getTaggedRecordTypeDefinition <em>Tagged Record Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getOrdinaryInterface <em>Ordinary Interface</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getLimitedInterface <em>Limited Interface</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getTaskInterface <em>Task Interface</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getProtectedInterface <em>Protected Interface</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getSynchronizedInterface <em>Synchronized Interface</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getPoolSpecificAccessToVariable <em>Pool Specific Access To Variable</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getAccessToVariable <em>Access To Variable</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getAccessToConstant <em>Access To Constant</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getAccessToProcedure <em>Access To Procedure</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getAccessToProtectedProcedure <em>Access To Protected Procedure</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getAccessToFunction <em>Access To Function</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getAccessToProtectedFunction <em>Access To Protected Function</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getSubtypeIndication <em>Subtype Indication</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getRangeAttributeReference <em>Range Attribute Reference</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getSimpleExpressionRange <em>Simple Expression Range</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getDigitsConstraint <em>Digits Constraint</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getDeltaConstraint <em>Delta Constraint</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getIndexConstraint <em>Index Constraint</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getDiscriminantConstraint <em>Discriminant Constraint</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getComponentDefinition <em>Component Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getDiscreteSubtypeIndicationAsSubtypeDefinition <em>Discrete Subtype Indication As Subtype Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getDiscreteRangeAttributeReferenceAsSubtypeDefinition <em>Discrete Range Attribute Reference As Subtype Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getDiscreteSimpleExpressionRangeAsSubtypeDefinition <em>Discrete Simple Expression Range As Subtype Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getDiscreteSubtypeIndication <em>Discrete Subtype Indication</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getDiscreteRangeAttributeReference <em>Discrete Range Attribute Reference</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getDiscreteSimpleExpressionRange <em>Discrete Simple Expression Range</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getUnknownDiscriminantPart <em>Unknown Discriminant Part</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getKnownDiscriminantPart <em>Known Discriminant Part</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getRecordDefinition <em>Record Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getNullRecordDefinition <em>Null Record Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getNullComponent <em>Null Component</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getVariantPart <em>Variant Part</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getVariant <em>Variant</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getOthersChoice <em>Others Choice</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getAnonymousAccessToVariable <em>Anonymous Access To Variable</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getAnonymousAccessToConstant <em>Anonymous Access To Constant</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getAnonymousAccessToProcedure <em>Anonymous Access To Procedure</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getAnonymousAccessToProtectedProcedure <em>Anonymous Access To Protected Procedure</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getAnonymousAccessToFunction <em>Anonymous Access To Function</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getAnonymousAccessToProtectedFunction <em>Anonymous Access To Protected Function</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getPrivateTypeDefinition <em>Private Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getTaggedPrivateTypeDefinition <em>Tagged Private Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getPrivateExtensionDefinition <em>Private Extension Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getTaskDefinition <em>Task Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getProtectedDefinition <em>Protected Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getFormalPrivateTypeDefinition <em>Formal Private Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getFormalTaggedPrivateTypeDefinition <em>Formal Tagged Private Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getFormalDerivedTypeDefinition <em>Formal Derived Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getFormalDiscreteTypeDefinition <em>Formal Discrete Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getFormalSignedIntegerTypeDefinition <em>Formal Signed Integer Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getFormalModularTypeDefinition <em>Formal Modular Type Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getFormalFloatingPointDefinition <em>Formal Floating Point Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getFormalOrdinaryFixedPointDefinition <em>Formal Ordinary Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getFormalDecimalFixedPointDefinition <em>Formal Decimal Fixed Point Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getFormalOrdinaryInterface <em>Formal Ordinary Interface</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getFormalLimitedInterface <em>Formal Limited Interface</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getFormalTaskInterface <em>Formal Task Interface</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getFormalProtectedInterface <em>Formal Protected Interface</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getFormalSynchronizedInterface <em>Formal Synchronized Interface</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getFormalUnconstrainedArrayDefinition <em>Formal Unconstrained Array Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getFormalConstrainedArrayDefinition <em>Formal Constrained Array Definition</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getFormalPoolSpecificAccessToVariable <em>Formal Pool Specific Access To Variable</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getFormalAccessToVariable <em>Formal Access To Variable</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getFormalAccessToConstant <em>Formal Access To Constant</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getFormalAccessToProcedure <em>Formal Access To Procedure</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getFormalAccessToProtectedProcedure <em>Formal Access To Protected Procedure</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getFormalAccessToFunction <em>Formal Access To Function</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getFormalAccessToProtectedFunction <em>Formal Access To Protected Function</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getAspectSpecification <em>Aspect Specification</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getSelectedComponent <em>Selected Component</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getBaseAttribute <em>Base Attribute</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getClassAttribute <em>Class Attribute</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getAsynchronousPragma <em>Asynchronous Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getAtomicPragma <em>Atomic Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getAttachHandlerPragma <em>Attach Handler Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getControlledPragma <em>Controlled Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getConventionPragma <em>Convention Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getDiscardNamesPragma <em>Discard Names Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getElaboratePragma <em>Elaborate Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getElaborateAllPragma <em>Elaborate All Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getExportPragma <em>Export Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getImportPragma <em>Import Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getInlinePragma <em>Inline Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getInspectionPointPragma <em>Inspection Point Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getLinkerOptionsPragma <em>Linker Options Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getListPragma <em>List Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getLockingPolicyPragma <em>Locking Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getOptimizePragma <em>Optimize Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getPackPragma <em>Pack Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getPagePragma <em>Page Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getPreelaboratePragma <em>Preelaborate Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getPriorityPragma <em>Priority Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getPurePragma <em>Pure Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getRemoteTypesPragma <em>Remote Types Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getRestrictionsPragma <em>Restrictions Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getReviewablePragma <em>Reviewable Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getSharedPassivePragma <em>Shared Passive Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getStorageSizePragma <em>Storage Size Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getSuppressPragma <em>Suppress Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getVolatilePragma <em>Volatile Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getAssertPragma <em>Assert Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getNoReturnPragma <em>No Return Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getProfilePragma <em>Profile Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getUnsuppressPragma <em>Unsuppress Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getCpuPragma <em>Cpu Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getIndependentPragma <em>Independent Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getIndependentComponentsPragma <em>Independent Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}</li>
 *   <li>{@link Ada.impl.DefinitionClassImpl#getUnknownPragma <em>Unknown Pragma</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DefinitionClassImpl extends MinimalEObjectImpl.Container implements DefinitionClass {
	/**
	 * The cached value of the '{@link #getNotAnElement() <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotAnElement()
	 * @generated
	 * @ordered
	 */
	protected NotAnElement notAnElement;

	/**
	 * The cached value of the '{@link #getDerivedTypeDefinition() <em>Derived Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedTypeDefinition()
	 * @generated
	 * @ordered
	 */
	protected DerivedTypeDefinition derivedTypeDefinition;

	/**
	 * The cached value of the '{@link #getDerivedRecordExtensionDefinition() <em>Derived Record Extension Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedRecordExtensionDefinition()
	 * @generated
	 * @ordered
	 */
	protected DerivedRecordExtensionDefinition derivedRecordExtensionDefinition;

	/**
	 * The cached value of the '{@link #getEnumerationTypeDefinition() <em>Enumeration Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnumerationTypeDefinition()
	 * @generated
	 * @ordered
	 */
	protected EnumerationTypeDefinition enumerationTypeDefinition;

	/**
	 * The cached value of the '{@link #getSignedIntegerTypeDefinition() <em>Signed Integer Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignedIntegerTypeDefinition()
	 * @generated
	 * @ordered
	 */
	protected SignedIntegerTypeDefinition signedIntegerTypeDefinition;

	/**
	 * The cached value of the '{@link #getModularTypeDefinition() <em>Modular Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModularTypeDefinition()
	 * @generated
	 * @ordered
	 */
	protected ModularTypeDefinition modularTypeDefinition;

	/**
	 * The cached value of the '{@link #getRootIntegerDefinition() <em>Root Integer Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootIntegerDefinition()
	 * @generated
	 * @ordered
	 */
	protected RootIntegerDefinition rootIntegerDefinition;

	/**
	 * The cached value of the '{@link #getRootRealDefinition() <em>Root Real Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootRealDefinition()
	 * @generated
	 * @ordered
	 */
	protected RootRealDefinition rootRealDefinition;

	/**
	 * The cached value of the '{@link #getUniversalIntegerDefinition() <em>Universal Integer Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUniversalIntegerDefinition()
	 * @generated
	 * @ordered
	 */
	protected UniversalIntegerDefinition universalIntegerDefinition;

	/**
	 * The cached value of the '{@link #getUniversalRealDefinition() <em>Universal Real Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUniversalRealDefinition()
	 * @generated
	 * @ordered
	 */
	protected UniversalRealDefinition universalRealDefinition;

	/**
	 * The cached value of the '{@link #getUniversalFixedDefinition() <em>Universal Fixed Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUniversalFixedDefinition()
	 * @generated
	 * @ordered
	 */
	protected UniversalFixedDefinition universalFixedDefinition;

	/**
	 * The cached value of the '{@link #getFloatingPointDefinition() <em>Floating Point Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFloatingPointDefinition()
	 * @generated
	 * @ordered
	 */
	protected FloatingPointDefinition floatingPointDefinition;

	/**
	 * The cached value of the '{@link #getOrdinaryFixedPointDefinition() <em>Ordinary Fixed Point Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrdinaryFixedPointDefinition()
	 * @generated
	 * @ordered
	 */
	protected OrdinaryFixedPointDefinition ordinaryFixedPointDefinition;

	/**
	 * The cached value of the '{@link #getDecimalFixedPointDefinition() <em>Decimal Fixed Point Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDecimalFixedPointDefinition()
	 * @generated
	 * @ordered
	 */
	protected DecimalFixedPointDefinition decimalFixedPointDefinition;

	/**
	 * The cached value of the '{@link #getUnconstrainedArrayDefinition() <em>Unconstrained Array Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnconstrainedArrayDefinition()
	 * @generated
	 * @ordered
	 */
	protected UnconstrainedArrayDefinition unconstrainedArrayDefinition;

	/**
	 * The cached value of the '{@link #getConstrainedArrayDefinition() <em>Constrained Array Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstrainedArrayDefinition()
	 * @generated
	 * @ordered
	 */
	protected ConstrainedArrayDefinition constrainedArrayDefinition;

	/**
	 * The cached value of the '{@link #getRecordTypeDefinition() <em>Record Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRecordTypeDefinition()
	 * @generated
	 * @ordered
	 */
	protected RecordTypeDefinition recordTypeDefinition;

	/**
	 * The cached value of the '{@link #getTaggedRecordTypeDefinition() <em>Tagged Record Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaggedRecordTypeDefinition()
	 * @generated
	 * @ordered
	 */
	protected TaggedRecordTypeDefinition taggedRecordTypeDefinition;

	/**
	 * The cached value of the '{@link #getOrdinaryInterface() <em>Ordinary Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrdinaryInterface()
	 * @generated
	 * @ordered
	 */
	protected OrdinaryInterface ordinaryInterface;

	/**
	 * The cached value of the '{@link #getLimitedInterface() <em>Limited Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLimitedInterface()
	 * @generated
	 * @ordered
	 */
	protected LimitedInterface limitedInterface;

	/**
	 * The cached value of the '{@link #getTaskInterface() <em>Task Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskInterface()
	 * @generated
	 * @ordered
	 */
	protected TaskInterface taskInterface;

	/**
	 * The cached value of the '{@link #getProtectedInterface() <em>Protected Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProtectedInterface()
	 * @generated
	 * @ordered
	 */
	protected ProtectedInterface protectedInterface;

	/**
	 * The cached value of the '{@link #getSynchronizedInterface() <em>Synchronized Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSynchronizedInterface()
	 * @generated
	 * @ordered
	 */
	protected SynchronizedInterface synchronizedInterface;

	/**
	 * The cached value of the '{@link #getPoolSpecificAccessToVariable() <em>Pool Specific Access To Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPoolSpecificAccessToVariable()
	 * @generated
	 * @ordered
	 */
	protected PoolSpecificAccessToVariable poolSpecificAccessToVariable;

	/**
	 * The cached value of the '{@link #getAccessToVariable() <em>Access To Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessToVariable()
	 * @generated
	 * @ordered
	 */
	protected AccessToVariable accessToVariable;

	/**
	 * The cached value of the '{@link #getAccessToConstant() <em>Access To Constant</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessToConstant()
	 * @generated
	 * @ordered
	 */
	protected AccessToConstant accessToConstant;

	/**
	 * The cached value of the '{@link #getAccessToProcedure() <em>Access To Procedure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessToProcedure()
	 * @generated
	 * @ordered
	 */
	protected AccessToProcedure accessToProcedure;

	/**
	 * The cached value of the '{@link #getAccessToProtectedProcedure() <em>Access To Protected Procedure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessToProtectedProcedure()
	 * @generated
	 * @ordered
	 */
	protected AccessToProtectedProcedure accessToProtectedProcedure;

	/**
	 * The cached value of the '{@link #getAccessToFunction() <em>Access To Function</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessToFunction()
	 * @generated
	 * @ordered
	 */
	protected AccessToFunction accessToFunction;

	/**
	 * The cached value of the '{@link #getAccessToProtectedFunction() <em>Access To Protected Function</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccessToProtectedFunction()
	 * @generated
	 * @ordered
	 */
	protected AccessToProtectedFunction accessToProtectedFunction;

	/**
	 * The cached value of the '{@link #getSubtypeIndication() <em>Subtype Indication</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubtypeIndication()
	 * @generated
	 * @ordered
	 */
	protected SubtypeIndication subtypeIndication;

	/**
	 * The cached value of the '{@link #getRangeAttributeReference() <em>Range Attribute Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRangeAttributeReference()
	 * @generated
	 * @ordered
	 */
	protected RangeAttributeReference rangeAttributeReference;

	/**
	 * The cached value of the '{@link #getSimpleExpressionRange() <em>Simple Expression Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimpleExpressionRange()
	 * @generated
	 * @ordered
	 */
	protected SimpleExpressionRange simpleExpressionRange;

	/**
	 * The cached value of the '{@link #getDigitsConstraint() <em>Digits Constraint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDigitsConstraint()
	 * @generated
	 * @ordered
	 */
	protected DigitsConstraint digitsConstraint;

	/**
	 * The cached value of the '{@link #getDeltaConstraint() <em>Delta Constraint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeltaConstraint()
	 * @generated
	 * @ordered
	 */
	protected DeltaConstraint deltaConstraint;

	/**
	 * The cached value of the '{@link #getIndexConstraint() <em>Index Constraint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndexConstraint()
	 * @generated
	 * @ordered
	 */
	protected IndexConstraint indexConstraint;

	/**
	 * The cached value of the '{@link #getDiscriminantConstraint() <em>Discriminant Constraint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiscriminantConstraint()
	 * @generated
	 * @ordered
	 */
	protected DiscriminantConstraint discriminantConstraint;

	/**
	 * The cached value of the '{@link #getComponentDefinition() <em>Component Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentDefinition()
	 * @generated
	 * @ordered
	 */
	protected ComponentDefinition componentDefinition;

	/**
	 * The cached value of the '{@link #getDiscreteSubtypeIndicationAsSubtypeDefinition() <em>Discrete Subtype Indication As Subtype Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiscreteSubtypeIndicationAsSubtypeDefinition()
	 * @generated
	 * @ordered
	 */
	protected DiscreteSubtypeIndicationAsSubtypeDefinition discreteSubtypeIndicationAsSubtypeDefinition;

	/**
	 * The cached value of the '{@link #getDiscreteRangeAttributeReferenceAsSubtypeDefinition() <em>Discrete Range Attribute Reference As Subtype Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiscreteRangeAttributeReferenceAsSubtypeDefinition()
	 * @generated
	 * @ordered
	 */
	protected DiscreteRangeAttributeReferenceAsSubtypeDefinition discreteRangeAttributeReferenceAsSubtypeDefinition;

	/**
	 * The cached value of the '{@link #getDiscreteSimpleExpressionRangeAsSubtypeDefinition() <em>Discrete Simple Expression Range As Subtype Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiscreteSimpleExpressionRangeAsSubtypeDefinition()
	 * @generated
	 * @ordered
	 */
	protected DiscreteSimpleExpressionRangeAsSubtypeDefinition discreteSimpleExpressionRangeAsSubtypeDefinition;

	/**
	 * The cached value of the '{@link #getDiscreteSubtypeIndication() <em>Discrete Subtype Indication</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiscreteSubtypeIndication()
	 * @generated
	 * @ordered
	 */
	protected DiscreteSubtypeIndication discreteSubtypeIndication;

	/**
	 * The cached value of the '{@link #getDiscreteRangeAttributeReference() <em>Discrete Range Attribute Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiscreteRangeAttributeReference()
	 * @generated
	 * @ordered
	 */
	protected DiscreteRangeAttributeReference discreteRangeAttributeReference;

	/**
	 * The cached value of the '{@link #getDiscreteSimpleExpressionRange() <em>Discrete Simple Expression Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiscreteSimpleExpressionRange()
	 * @generated
	 * @ordered
	 */
	protected DiscreteSimpleExpressionRange discreteSimpleExpressionRange;

	/**
	 * The cached value of the '{@link #getUnknownDiscriminantPart() <em>Unknown Discriminant Part</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnknownDiscriminantPart()
	 * @generated
	 * @ordered
	 */
	protected UnknownDiscriminantPart unknownDiscriminantPart;

	/**
	 * The cached value of the '{@link #getKnownDiscriminantPart() <em>Known Discriminant Part</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKnownDiscriminantPart()
	 * @generated
	 * @ordered
	 */
	protected KnownDiscriminantPart knownDiscriminantPart;

	/**
	 * The cached value of the '{@link #getRecordDefinition() <em>Record Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRecordDefinition()
	 * @generated
	 * @ordered
	 */
	protected RecordDefinition recordDefinition;

	/**
	 * The cached value of the '{@link #getNullRecordDefinition() <em>Null Record Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNullRecordDefinition()
	 * @generated
	 * @ordered
	 */
	protected NullRecordDefinition nullRecordDefinition;

	/**
	 * The cached value of the '{@link #getNullComponent() <em>Null Component</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNullComponent()
	 * @generated
	 * @ordered
	 */
	protected NullComponent nullComponent;

	/**
	 * The cached value of the '{@link #getVariantPart() <em>Variant Part</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariantPart()
	 * @generated
	 * @ordered
	 */
	protected VariantPart variantPart;

	/**
	 * The cached value of the '{@link #getVariant() <em>Variant</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariant()
	 * @generated
	 * @ordered
	 */
	protected Variant variant;

	/**
	 * The cached value of the '{@link #getOthersChoice() <em>Others Choice</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOthersChoice()
	 * @generated
	 * @ordered
	 */
	protected OthersChoice othersChoice;

	/**
	 * The cached value of the '{@link #getAnonymousAccessToVariable() <em>Anonymous Access To Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnonymousAccessToVariable()
	 * @generated
	 * @ordered
	 */
	protected AnonymousAccessToVariable anonymousAccessToVariable;

	/**
	 * The cached value of the '{@link #getAnonymousAccessToConstant() <em>Anonymous Access To Constant</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnonymousAccessToConstant()
	 * @generated
	 * @ordered
	 */
	protected AnonymousAccessToConstant anonymousAccessToConstant;

	/**
	 * The cached value of the '{@link #getAnonymousAccessToProcedure() <em>Anonymous Access To Procedure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnonymousAccessToProcedure()
	 * @generated
	 * @ordered
	 */
	protected AnonymousAccessToProcedure anonymousAccessToProcedure;

	/**
	 * The cached value of the '{@link #getAnonymousAccessToProtectedProcedure() <em>Anonymous Access To Protected Procedure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnonymousAccessToProtectedProcedure()
	 * @generated
	 * @ordered
	 */
	protected AnonymousAccessToProtectedProcedure anonymousAccessToProtectedProcedure;

	/**
	 * The cached value of the '{@link #getAnonymousAccessToFunction() <em>Anonymous Access To Function</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnonymousAccessToFunction()
	 * @generated
	 * @ordered
	 */
	protected AnonymousAccessToFunction anonymousAccessToFunction;

	/**
	 * The cached value of the '{@link #getAnonymousAccessToProtectedFunction() <em>Anonymous Access To Protected Function</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnonymousAccessToProtectedFunction()
	 * @generated
	 * @ordered
	 */
	protected AnonymousAccessToProtectedFunction anonymousAccessToProtectedFunction;

	/**
	 * The cached value of the '{@link #getPrivateTypeDefinition() <em>Private Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrivateTypeDefinition()
	 * @generated
	 * @ordered
	 */
	protected PrivateTypeDefinition privateTypeDefinition;

	/**
	 * The cached value of the '{@link #getTaggedPrivateTypeDefinition() <em>Tagged Private Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaggedPrivateTypeDefinition()
	 * @generated
	 * @ordered
	 */
	protected TaggedPrivateTypeDefinition taggedPrivateTypeDefinition;

	/**
	 * The cached value of the '{@link #getPrivateExtensionDefinition() <em>Private Extension Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrivateExtensionDefinition()
	 * @generated
	 * @ordered
	 */
	protected PrivateExtensionDefinition privateExtensionDefinition;

	/**
	 * The cached value of the '{@link #getTaskDefinition() <em>Task Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskDefinition()
	 * @generated
	 * @ordered
	 */
	protected TaskDefinition taskDefinition;

	/**
	 * The cached value of the '{@link #getProtectedDefinition() <em>Protected Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProtectedDefinition()
	 * @generated
	 * @ordered
	 */
	protected ProtectedDefinition protectedDefinition;

	/**
	 * The cached value of the '{@link #getFormalPrivateTypeDefinition() <em>Formal Private Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalPrivateTypeDefinition()
	 * @generated
	 * @ordered
	 */
	protected FormalPrivateTypeDefinition formalPrivateTypeDefinition;

	/**
	 * The cached value of the '{@link #getFormalTaggedPrivateTypeDefinition() <em>Formal Tagged Private Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalTaggedPrivateTypeDefinition()
	 * @generated
	 * @ordered
	 */
	protected FormalTaggedPrivateTypeDefinition formalTaggedPrivateTypeDefinition;

	/**
	 * The cached value of the '{@link #getFormalDerivedTypeDefinition() <em>Formal Derived Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalDerivedTypeDefinition()
	 * @generated
	 * @ordered
	 */
	protected FormalDerivedTypeDefinition formalDerivedTypeDefinition;

	/**
	 * The cached value of the '{@link #getFormalDiscreteTypeDefinition() <em>Formal Discrete Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalDiscreteTypeDefinition()
	 * @generated
	 * @ordered
	 */
	protected FormalDiscreteTypeDefinition formalDiscreteTypeDefinition;

	/**
	 * The cached value of the '{@link #getFormalSignedIntegerTypeDefinition() <em>Formal Signed Integer Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalSignedIntegerTypeDefinition()
	 * @generated
	 * @ordered
	 */
	protected FormalSignedIntegerTypeDefinition formalSignedIntegerTypeDefinition;

	/**
	 * The cached value of the '{@link #getFormalModularTypeDefinition() <em>Formal Modular Type Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalModularTypeDefinition()
	 * @generated
	 * @ordered
	 */
	protected FormalModularTypeDefinition formalModularTypeDefinition;

	/**
	 * The cached value of the '{@link #getFormalFloatingPointDefinition() <em>Formal Floating Point Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalFloatingPointDefinition()
	 * @generated
	 * @ordered
	 */
	protected FormalFloatingPointDefinition formalFloatingPointDefinition;

	/**
	 * The cached value of the '{@link #getFormalOrdinaryFixedPointDefinition() <em>Formal Ordinary Fixed Point Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalOrdinaryFixedPointDefinition()
	 * @generated
	 * @ordered
	 */
	protected FormalOrdinaryFixedPointDefinition formalOrdinaryFixedPointDefinition;

	/**
	 * The cached value of the '{@link #getFormalDecimalFixedPointDefinition() <em>Formal Decimal Fixed Point Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalDecimalFixedPointDefinition()
	 * @generated
	 * @ordered
	 */
	protected FormalDecimalFixedPointDefinition formalDecimalFixedPointDefinition;

	/**
	 * The cached value of the '{@link #getFormalOrdinaryInterface() <em>Formal Ordinary Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalOrdinaryInterface()
	 * @generated
	 * @ordered
	 */
	protected FormalOrdinaryInterface formalOrdinaryInterface;

	/**
	 * The cached value of the '{@link #getFormalLimitedInterface() <em>Formal Limited Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalLimitedInterface()
	 * @generated
	 * @ordered
	 */
	protected FormalLimitedInterface formalLimitedInterface;

	/**
	 * The cached value of the '{@link #getFormalTaskInterface() <em>Formal Task Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalTaskInterface()
	 * @generated
	 * @ordered
	 */
	protected FormalTaskInterface formalTaskInterface;

	/**
	 * The cached value of the '{@link #getFormalProtectedInterface() <em>Formal Protected Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalProtectedInterface()
	 * @generated
	 * @ordered
	 */
	protected FormalProtectedInterface formalProtectedInterface;

	/**
	 * The cached value of the '{@link #getFormalSynchronizedInterface() <em>Formal Synchronized Interface</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalSynchronizedInterface()
	 * @generated
	 * @ordered
	 */
	protected FormalSynchronizedInterface formalSynchronizedInterface;

	/**
	 * The cached value of the '{@link #getFormalUnconstrainedArrayDefinition() <em>Formal Unconstrained Array Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalUnconstrainedArrayDefinition()
	 * @generated
	 * @ordered
	 */
	protected FormalUnconstrainedArrayDefinition formalUnconstrainedArrayDefinition;

	/**
	 * The cached value of the '{@link #getFormalConstrainedArrayDefinition() <em>Formal Constrained Array Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalConstrainedArrayDefinition()
	 * @generated
	 * @ordered
	 */
	protected FormalConstrainedArrayDefinition formalConstrainedArrayDefinition;

	/**
	 * The cached value of the '{@link #getFormalPoolSpecificAccessToVariable() <em>Formal Pool Specific Access To Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalPoolSpecificAccessToVariable()
	 * @generated
	 * @ordered
	 */
	protected FormalPoolSpecificAccessToVariable formalPoolSpecificAccessToVariable;

	/**
	 * The cached value of the '{@link #getFormalAccessToVariable() <em>Formal Access To Variable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalAccessToVariable()
	 * @generated
	 * @ordered
	 */
	protected FormalAccessToVariable formalAccessToVariable;

	/**
	 * The cached value of the '{@link #getFormalAccessToConstant() <em>Formal Access To Constant</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalAccessToConstant()
	 * @generated
	 * @ordered
	 */
	protected FormalAccessToConstant formalAccessToConstant;

	/**
	 * The cached value of the '{@link #getFormalAccessToProcedure() <em>Formal Access To Procedure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalAccessToProcedure()
	 * @generated
	 * @ordered
	 */
	protected FormalAccessToProcedure formalAccessToProcedure;

	/**
	 * The cached value of the '{@link #getFormalAccessToProtectedProcedure() <em>Formal Access To Protected Procedure</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalAccessToProtectedProcedure()
	 * @generated
	 * @ordered
	 */
	protected FormalAccessToProtectedProcedure formalAccessToProtectedProcedure;

	/**
	 * The cached value of the '{@link #getFormalAccessToFunction() <em>Formal Access To Function</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalAccessToFunction()
	 * @generated
	 * @ordered
	 */
	protected FormalAccessToFunction formalAccessToFunction;

	/**
	 * The cached value of the '{@link #getFormalAccessToProtectedFunction() <em>Formal Access To Protected Function</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalAccessToProtectedFunction()
	 * @generated
	 * @ordered
	 */
	protected FormalAccessToProtectedFunction formalAccessToProtectedFunction;

	/**
	 * The cached value of the '{@link #getAspectSpecification() <em>Aspect Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAspectSpecification()
	 * @generated
	 * @ordered
	 */
	protected AspectSpecification aspectSpecification;

	/**
	 * The cached value of the '{@link #getIdentifier() <em>Identifier</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdentifier()
	 * @generated
	 * @ordered
	 */
	protected Identifier identifier;

	/**
	 * The cached value of the '{@link #getSelectedComponent() <em>Selected Component</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelectedComponent()
	 * @generated
	 * @ordered
	 */
	protected SelectedComponent selectedComponent;

	/**
	 * The cached value of the '{@link #getBaseAttribute() <em>Base Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseAttribute()
	 * @generated
	 * @ordered
	 */
	protected BaseAttribute baseAttribute;

	/**
	 * The cached value of the '{@link #getClassAttribute() <em>Class Attribute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassAttribute()
	 * @generated
	 * @ordered
	 */
	protected ClassAttribute classAttribute;

	/**
	 * The cached value of the '{@link #getComment() <em>Comment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected Comment comment;

	/**
	 * The cached value of the '{@link #getAllCallsRemotePragma() <em>All Calls Remote Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllCallsRemotePragma()
	 * @generated
	 * @ordered
	 */
	protected AllCallsRemotePragma allCallsRemotePragma;

	/**
	 * The cached value of the '{@link #getAsynchronousPragma() <em>Asynchronous Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsynchronousPragma()
	 * @generated
	 * @ordered
	 */
	protected AsynchronousPragma asynchronousPragma;

	/**
	 * The cached value of the '{@link #getAtomicPragma() <em>Atomic Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAtomicPragma()
	 * @generated
	 * @ordered
	 */
	protected AtomicPragma atomicPragma;

	/**
	 * The cached value of the '{@link #getAtomicComponentsPragma() <em>Atomic Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAtomicComponentsPragma()
	 * @generated
	 * @ordered
	 */
	protected AtomicComponentsPragma atomicComponentsPragma;

	/**
	 * The cached value of the '{@link #getAttachHandlerPragma() <em>Attach Handler Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttachHandlerPragma()
	 * @generated
	 * @ordered
	 */
	protected AttachHandlerPragma attachHandlerPragma;

	/**
	 * The cached value of the '{@link #getControlledPragma() <em>Controlled Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getControlledPragma()
	 * @generated
	 * @ordered
	 */
	protected ControlledPragma controlledPragma;

	/**
	 * The cached value of the '{@link #getConventionPragma() <em>Convention Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConventionPragma()
	 * @generated
	 * @ordered
	 */
	protected ConventionPragma conventionPragma;

	/**
	 * The cached value of the '{@link #getDiscardNamesPragma() <em>Discard Names Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiscardNamesPragma()
	 * @generated
	 * @ordered
	 */
	protected DiscardNamesPragma discardNamesPragma;

	/**
	 * The cached value of the '{@link #getElaboratePragma() <em>Elaborate Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElaboratePragma()
	 * @generated
	 * @ordered
	 */
	protected ElaboratePragma elaboratePragma;

	/**
	 * The cached value of the '{@link #getElaborateAllPragma() <em>Elaborate All Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElaborateAllPragma()
	 * @generated
	 * @ordered
	 */
	protected ElaborateAllPragma elaborateAllPragma;

	/**
	 * The cached value of the '{@link #getElaborateBodyPragma() <em>Elaborate Body Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElaborateBodyPragma()
	 * @generated
	 * @ordered
	 */
	protected ElaborateBodyPragma elaborateBodyPragma;

	/**
	 * The cached value of the '{@link #getExportPragma() <em>Export Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExportPragma()
	 * @generated
	 * @ordered
	 */
	protected ExportPragma exportPragma;

	/**
	 * The cached value of the '{@link #getImportPragma() <em>Import Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImportPragma()
	 * @generated
	 * @ordered
	 */
	protected ImportPragma importPragma;

	/**
	 * The cached value of the '{@link #getInlinePragma() <em>Inline Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInlinePragma()
	 * @generated
	 * @ordered
	 */
	protected InlinePragma inlinePragma;

	/**
	 * The cached value of the '{@link #getInspectionPointPragma() <em>Inspection Point Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInspectionPointPragma()
	 * @generated
	 * @ordered
	 */
	protected InspectionPointPragma inspectionPointPragma;

	/**
	 * The cached value of the '{@link #getInterruptHandlerPragma() <em>Interrupt Handler Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterruptHandlerPragma()
	 * @generated
	 * @ordered
	 */
	protected InterruptHandlerPragma interruptHandlerPragma;

	/**
	 * The cached value of the '{@link #getInterruptPriorityPragma() <em>Interrupt Priority Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterruptPriorityPragma()
	 * @generated
	 * @ordered
	 */
	protected InterruptPriorityPragma interruptPriorityPragma;

	/**
	 * The cached value of the '{@link #getLinkerOptionsPragma() <em>Linker Options Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkerOptionsPragma()
	 * @generated
	 * @ordered
	 */
	protected LinkerOptionsPragma linkerOptionsPragma;

	/**
	 * The cached value of the '{@link #getListPragma() <em>List Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListPragma()
	 * @generated
	 * @ordered
	 */
	protected ListPragma listPragma;

	/**
	 * The cached value of the '{@link #getLockingPolicyPragma() <em>Locking Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLockingPolicyPragma()
	 * @generated
	 * @ordered
	 */
	protected LockingPolicyPragma lockingPolicyPragma;

	/**
	 * The cached value of the '{@link #getNormalizeScalarsPragma() <em>Normalize Scalars Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNormalizeScalarsPragma()
	 * @generated
	 * @ordered
	 */
	protected NormalizeScalarsPragma normalizeScalarsPragma;

	/**
	 * The cached value of the '{@link #getOptimizePragma() <em>Optimize Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOptimizePragma()
	 * @generated
	 * @ordered
	 */
	protected OptimizePragma optimizePragma;

	/**
	 * The cached value of the '{@link #getPackPragma() <em>Pack Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackPragma()
	 * @generated
	 * @ordered
	 */
	protected PackPragma packPragma;

	/**
	 * The cached value of the '{@link #getPagePragma() <em>Page Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPagePragma()
	 * @generated
	 * @ordered
	 */
	protected PagePragma pagePragma;

	/**
	 * The cached value of the '{@link #getPreelaboratePragma() <em>Preelaborate Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreelaboratePragma()
	 * @generated
	 * @ordered
	 */
	protected PreelaboratePragma preelaboratePragma;

	/**
	 * The cached value of the '{@link #getPriorityPragma() <em>Priority Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPriorityPragma()
	 * @generated
	 * @ordered
	 */
	protected PriorityPragma priorityPragma;

	/**
	 * The cached value of the '{@link #getPurePragma() <em>Pure Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPurePragma()
	 * @generated
	 * @ordered
	 */
	protected PurePragma purePragma;

	/**
	 * The cached value of the '{@link #getQueuingPolicyPragma() <em>Queuing Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQueuingPolicyPragma()
	 * @generated
	 * @ordered
	 */
	protected QueuingPolicyPragma queuingPolicyPragma;

	/**
	 * The cached value of the '{@link #getRemoteCallInterfacePragma() <em>Remote Call Interface Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRemoteCallInterfacePragma()
	 * @generated
	 * @ordered
	 */
	protected RemoteCallInterfacePragma remoteCallInterfacePragma;

	/**
	 * The cached value of the '{@link #getRemoteTypesPragma() <em>Remote Types Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRemoteTypesPragma()
	 * @generated
	 * @ordered
	 */
	protected RemoteTypesPragma remoteTypesPragma;

	/**
	 * The cached value of the '{@link #getRestrictionsPragma() <em>Restrictions Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRestrictionsPragma()
	 * @generated
	 * @ordered
	 */
	protected RestrictionsPragma restrictionsPragma;

	/**
	 * The cached value of the '{@link #getReviewablePragma() <em>Reviewable Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReviewablePragma()
	 * @generated
	 * @ordered
	 */
	protected ReviewablePragma reviewablePragma;

	/**
	 * The cached value of the '{@link #getSharedPassivePragma() <em>Shared Passive Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSharedPassivePragma()
	 * @generated
	 * @ordered
	 */
	protected SharedPassivePragma sharedPassivePragma;

	/**
	 * The cached value of the '{@link #getStorageSizePragma() <em>Storage Size Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStorageSizePragma()
	 * @generated
	 * @ordered
	 */
	protected StorageSizePragma storageSizePragma;

	/**
	 * The cached value of the '{@link #getSuppressPragma() <em>Suppress Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuppressPragma()
	 * @generated
	 * @ordered
	 */
	protected SuppressPragma suppressPragma;

	/**
	 * The cached value of the '{@link #getTaskDispatchingPolicyPragma() <em>Task Dispatching Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskDispatchingPolicyPragma()
	 * @generated
	 * @ordered
	 */
	protected TaskDispatchingPolicyPragma taskDispatchingPolicyPragma;

	/**
	 * The cached value of the '{@link #getVolatilePragma() <em>Volatile Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVolatilePragma()
	 * @generated
	 * @ordered
	 */
	protected VolatilePragma volatilePragma;

	/**
	 * The cached value of the '{@link #getVolatileComponentsPragma() <em>Volatile Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVolatileComponentsPragma()
	 * @generated
	 * @ordered
	 */
	protected VolatileComponentsPragma volatileComponentsPragma;

	/**
	 * The cached value of the '{@link #getAssertPragma() <em>Assert Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssertPragma()
	 * @generated
	 * @ordered
	 */
	protected AssertPragma assertPragma;

	/**
	 * The cached value of the '{@link #getAssertionPolicyPragma() <em>Assertion Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssertionPolicyPragma()
	 * @generated
	 * @ordered
	 */
	protected AssertionPolicyPragma assertionPolicyPragma;

	/**
	 * The cached value of the '{@link #getDetectBlockingPragma() <em>Detect Blocking Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDetectBlockingPragma()
	 * @generated
	 * @ordered
	 */
	protected DetectBlockingPragma detectBlockingPragma;

	/**
	 * The cached value of the '{@link #getNoReturnPragma() <em>No Return Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNoReturnPragma()
	 * @generated
	 * @ordered
	 */
	protected NoReturnPragma noReturnPragma;

	/**
	 * The cached value of the '{@link #getPartitionElaborationPolicyPragma() <em>Partition Elaboration Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPartitionElaborationPolicyPragma()
	 * @generated
	 * @ordered
	 */
	protected PartitionElaborationPolicyPragma partitionElaborationPolicyPragma;

	/**
	 * The cached value of the '{@link #getPreelaborableInitializationPragma() <em>Preelaborable Initialization Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreelaborableInitializationPragma()
	 * @generated
	 * @ordered
	 */
	protected PreelaborableInitializationPragma preelaborableInitializationPragma;

	/**
	 * The cached value of the '{@link #getPrioritySpecificDispatchingPragma() <em>Priority Specific Dispatching Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrioritySpecificDispatchingPragma()
	 * @generated
	 * @ordered
	 */
	protected PrioritySpecificDispatchingPragma prioritySpecificDispatchingPragma;

	/**
	 * The cached value of the '{@link #getProfilePragma() <em>Profile Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProfilePragma()
	 * @generated
	 * @ordered
	 */
	protected ProfilePragma profilePragma;

	/**
	 * The cached value of the '{@link #getRelativeDeadlinePragma() <em>Relative Deadline Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelativeDeadlinePragma()
	 * @generated
	 * @ordered
	 */
	protected RelativeDeadlinePragma relativeDeadlinePragma;

	/**
	 * The cached value of the '{@link #getUncheckedUnionPragma() <em>Unchecked Union Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUncheckedUnionPragma()
	 * @generated
	 * @ordered
	 */
	protected UncheckedUnionPragma uncheckedUnionPragma;

	/**
	 * The cached value of the '{@link #getUnsuppressPragma() <em>Unsuppress Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnsuppressPragma()
	 * @generated
	 * @ordered
	 */
	protected UnsuppressPragma unsuppressPragma;

	/**
	 * The cached value of the '{@link #getDefaultStoragePoolPragma() <em>Default Storage Pool Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultStoragePoolPragma()
	 * @generated
	 * @ordered
	 */
	protected DefaultStoragePoolPragma defaultStoragePoolPragma;

	/**
	 * The cached value of the '{@link #getDispatchingDomainPragma() <em>Dispatching Domain Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDispatchingDomainPragma()
	 * @generated
	 * @ordered
	 */
	protected DispatchingDomainPragma dispatchingDomainPragma;

	/**
	 * The cached value of the '{@link #getCpuPragma() <em>Cpu Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCpuPragma()
	 * @generated
	 * @ordered
	 */
	protected CpuPragma cpuPragma;

	/**
	 * The cached value of the '{@link #getIndependentPragma() <em>Independent Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndependentPragma()
	 * @generated
	 * @ordered
	 */
	protected IndependentPragma independentPragma;

	/**
	 * The cached value of the '{@link #getIndependentComponentsPragma() <em>Independent Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndependentComponentsPragma()
	 * @generated
	 * @ordered
	 */
	protected IndependentComponentsPragma independentComponentsPragma;

	/**
	 * The cached value of the '{@link #getImplementationDefinedPragma() <em>Implementation Defined Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImplementationDefinedPragma()
	 * @generated
	 * @ordered
	 */
	protected ImplementationDefinedPragma implementationDefinedPragma;

	/**
	 * The cached value of the '{@link #getUnknownPragma() <em>Unknown Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnknownPragma()
	 * @generated
	 * @ordered
	 */
	protected UnknownPragma unknownPragma;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DefinitionClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getDefinitionClass();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotAnElement getNotAnElement() {
		return notAnElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNotAnElement(NotAnElement newNotAnElement, NotificationChain msgs) {
		NotAnElement oldNotAnElement = notAnElement;
		notAnElement = newNotAnElement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__NOT_AN_ELEMENT, oldNotAnElement, newNotAnElement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotAnElement(NotAnElement newNotAnElement) {
		if (newNotAnElement != notAnElement) {
			NotificationChain msgs = null;
			if (notAnElement != null)
				msgs = ((InternalEObject)notAnElement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__NOT_AN_ELEMENT, null, msgs);
			if (newNotAnElement != null)
				msgs = ((InternalEObject)newNotAnElement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__NOT_AN_ELEMENT, null, msgs);
			msgs = basicSetNotAnElement(newNotAnElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__NOT_AN_ELEMENT, newNotAnElement, newNotAnElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DerivedTypeDefinition getDerivedTypeDefinition() {
		return derivedTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDerivedTypeDefinition(DerivedTypeDefinition newDerivedTypeDefinition, NotificationChain msgs) {
		DerivedTypeDefinition oldDerivedTypeDefinition = derivedTypeDefinition;
		derivedTypeDefinition = newDerivedTypeDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DERIVED_TYPE_DEFINITION, oldDerivedTypeDefinition, newDerivedTypeDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDerivedTypeDefinition(DerivedTypeDefinition newDerivedTypeDefinition) {
		if (newDerivedTypeDefinition != derivedTypeDefinition) {
			NotificationChain msgs = null;
			if (derivedTypeDefinition != null)
				msgs = ((InternalEObject)derivedTypeDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DERIVED_TYPE_DEFINITION, null, msgs);
			if (newDerivedTypeDefinition != null)
				msgs = ((InternalEObject)newDerivedTypeDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DERIVED_TYPE_DEFINITION, null, msgs);
			msgs = basicSetDerivedTypeDefinition(newDerivedTypeDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DERIVED_TYPE_DEFINITION, newDerivedTypeDefinition, newDerivedTypeDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DerivedRecordExtensionDefinition getDerivedRecordExtensionDefinition() {
		return derivedRecordExtensionDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDerivedRecordExtensionDefinition(DerivedRecordExtensionDefinition newDerivedRecordExtensionDefinition, NotificationChain msgs) {
		DerivedRecordExtensionDefinition oldDerivedRecordExtensionDefinition = derivedRecordExtensionDefinition;
		derivedRecordExtensionDefinition = newDerivedRecordExtensionDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DERIVED_RECORD_EXTENSION_DEFINITION, oldDerivedRecordExtensionDefinition, newDerivedRecordExtensionDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDerivedRecordExtensionDefinition(DerivedRecordExtensionDefinition newDerivedRecordExtensionDefinition) {
		if (newDerivedRecordExtensionDefinition != derivedRecordExtensionDefinition) {
			NotificationChain msgs = null;
			if (derivedRecordExtensionDefinition != null)
				msgs = ((InternalEObject)derivedRecordExtensionDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DERIVED_RECORD_EXTENSION_DEFINITION, null, msgs);
			if (newDerivedRecordExtensionDefinition != null)
				msgs = ((InternalEObject)newDerivedRecordExtensionDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DERIVED_RECORD_EXTENSION_DEFINITION, null, msgs);
			msgs = basicSetDerivedRecordExtensionDefinition(newDerivedRecordExtensionDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DERIVED_RECORD_EXTENSION_DEFINITION, newDerivedRecordExtensionDefinition, newDerivedRecordExtensionDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationTypeDefinition getEnumerationTypeDefinition() {
		return enumerationTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEnumerationTypeDefinition(EnumerationTypeDefinition newEnumerationTypeDefinition, NotificationChain msgs) {
		EnumerationTypeDefinition oldEnumerationTypeDefinition = enumerationTypeDefinition;
		enumerationTypeDefinition = newEnumerationTypeDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ENUMERATION_TYPE_DEFINITION, oldEnumerationTypeDefinition, newEnumerationTypeDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnumerationTypeDefinition(EnumerationTypeDefinition newEnumerationTypeDefinition) {
		if (newEnumerationTypeDefinition != enumerationTypeDefinition) {
			NotificationChain msgs = null;
			if (enumerationTypeDefinition != null)
				msgs = ((InternalEObject)enumerationTypeDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ENUMERATION_TYPE_DEFINITION, null, msgs);
			if (newEnumerationTypeDefinition != null)
				msgs = ((InternalEObject)newEnumerationTypeDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ENUMERATION_TYPE_DEFINITION, null, msgs);
			msgs = basicSetEnumerationTypeDefinition(newEnumerationTypeDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ENUMERATION_TYPE_DEFINITION, newEnumerationTypeDefinition, newEnumerationTypeDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignedIntegerTypeDefinition getSignedIntegerTypeDefinition() {
		return signedIntegerTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSignedIntegerTypeDefinition(SignedIntegerTypeDefinition newSignedIntegerTypeDefinition, NotificationChain msgs) {
		SignedIntegerTypeDefinition oldSignedIntegerTypeDefinition = signedIntegerTypeDefinition;
		signedIntegerTypeDefinition = newSignedIntegerTypeDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__SIGNED_INTEGER_TYPE_DEFINITION, oldSignedIntegerTypeDefinition, newSignedIntegerTypeDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSignedIntegerTypeDefinition(SignedIntegerTypeDefinition newSignedIntegerTypeDefinition) {
		if (newSignedIntegerTypeDefinition != signedIntegerTypeDefinition) {
			NotificationChain msgs = null;
			if (signedIntegerTypeDefinition != null)
				msgs = ((InternalEObject)signedIntegerTypeDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__SIGNED_INTEGER_TYPE_DEFINITION, null, msgs);
			if (newSignedIntegerTypeDefinition != null)
				msgs = ((InternalEObject)newSignedIntegerTypeDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__SIGNED_INTEGER_TYPE_DEFINITION, null, msgs);
			msgs = basicSetSignedIntegerTypeDefinition(newSignedIntegerTypeDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__SIGNED_INTEGER_TYPE_DEFINITION, newSignedIntegerTypeDefinition, newSignedIntegerTypeDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModularTypeDefinition getModularTypeDefinition() {
		return modularTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetModularTypeDefinition(ModularTypeDefinition newModularTypeDefinition, NotificationChain msgs) {
		ModularTypeDefinition oldModularTypeDefinition = modularTypeDefinition;
		modularTypeDefinition = newModularTypeDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__MODULAR_TYPE_DEFINITION, oldModularTypeDefinition, newModularTypeDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModularTypeDefinition(ModularTypeDefinition newModularTypeDefinition) {
		if (newModularTypeDefinition != modularTypeDefinition) {
			NotificationChain msgs = null;
			if (modularTypeDefinition != null)
				msgs = ((InternalEObject)modularTypeDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__MODULAR_TYPE_DEFINITION, null, msgs);
			if (newModularTypeDefinition != null)
				msgs = ((InternalEObject)newModularTypeDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__MODULAR_TYPE_DEFINITION, null, msgs);
			msgs = basicSetModularTypeDefinition(newModularTypeDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__MODULAR_TYPE_DEFINITION, newModularTypeDefinition, newModularTypeDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RootIntegerDefinition getRootIntegerDefinition() {
		return rootIntegerDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRootIntegerDefinition(RootIntegerDefinition newRootIntegerDefinition, NotificationChain msgs) {
		RootIntegerDefinition oldRootIntegerDefinition = rootIntegerDefinition;
		rootIntegerDefinition = newRootIntegerDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ROOT_INTEGER_DEFINITION, oldRootIntegerDefinition, newRootIntegerDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRootIntegerDefinition(RootIntegerDefinition newRootIntegerDefinition) {
		if (newRootIntegerDefinition != rootIntegerDefinition) {
			NotificationChain msgs = null;
			if (rootIntegerDefinition != null)
				msgs = ((InternalEObject)rootIntegerDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ROOT_INTEGER_DEFINITION, null, msgs);
			if (newRootIntegerDefinition != null)
				msgs = ((InternalEObject)newRootIntegerDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ROOT_INTEGER_DEFINITION, null, msgs);
			msgs = basicSetRootIntegerDefinition(newRootIntegerDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ROOT_INTEGER_DEFINITION, newRootIntegerDefinition, newRootIntegerDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RootRealDefinition getRootRealDefinition() {
		return rootRealDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRootRealDefinition(RootRealDefinition newRootRealDefinition, NotificationChain msgs) {
		RootRealDefinition oldRootRealDefinition = rootRealDefinition;
		rootRealDefinition = newRootRealDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ROOT_REAL_DEFINITION, oldRootRealDefinition, newRootRealDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRootRealDefinition(RootRealDefinition newRootRealDefinition) {
		if (newRootRealDefinition != rootRealDefinition) {
			NotificationChain msgs = null;
			if (rootRealDefinition != null)
				msgs = ((InternalEObject)rootRealDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ROOT_REAL_DEFINITION, null, msgs);
			if (newRootRealDefinition != null)
				msgs = ((InternalEObject)newRootRealDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ROOT_REAL_DEFINITION, null, msgs);
			msgs = basicSetRootRealDefinition(newRootRealDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ROOT_REAL_DEFINITION, newRootRealDefinition, newRootRealDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UniversalIntegerDefinition getUniversalIntegerDefinition() {
		return universalIntegerDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUniversalIntegerDefinition(UniversalIntegerDefinition newUniversalIntegerDefinition, NotificationChain msgs) {
		UniversalIntegerDefinition oldUniversalIntegerDefinition = universalIntegerDefinition;
		universalIntegerDefinition = newUniversalIntegerDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__UNIVERSAL_INTEGER_DEFINITION, oldUniversalIntegerDefinition, newUniversalIntegerDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUniversalIntegerDefinition(UniversalIntegerDefinition newUniversalIntegerDefinition) {
		if (newUniversalIntegerDefinition != universalIntegerDefinition) {
			NotificationChain msgs = null;
			if (universalIntegerDefinition != null)
				msgs = ((InternalEObject)universalIntegerDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__UNIVERSAL_INTEGER_DEFINITION, null, msgs);
			if (newUniversalIntegerDefinition != null)
				msgs = ((InternalEObject)newUniversalIntegerDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__UNIVERSAL_INTEGER_DEFINITION, null, msgs);
			msgs = basicSetUniversalIntegerDefinition(newUniversalIntegerDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__UNIVERSAL_INTEGER_DEFINITION, newUniversalIntegerDefinition, newUniversalIntegerDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UniversalRealDefinition getUniversalRealDefinition() {
		return universalRealDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUniversalRealDefinition(UniversalRealDefinition newUniversalRealDefinition, NotificationChain msgs) {
		UniversalRealDefinition oldUniversalRealDefinition = universalRealDefinition;
		universalRealDefinition = newUniversalRealDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__UNIVERSAL_REAL_DEFINITION, oldUniversalRealDefinition, newUniversalRealDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUniversalRealDefinition(UniversalRealDefinition newUniversalRealDefinition) {
		if (newUniversalRealDefinition != universalRealDefinition) {
			NotificationChain msgs = null;
			if (universalRealDefinition != null)
				msgs = ((InternalEObject)universalRealDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__UNIVERSAL_REAL_DEFINITION, null, msgs);
			if (newUniversalRealDefinition != null)
				msgs = ((InternalEObject)newUniversalRealDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__UNIVERSAL_REAL_DEFINITION, null, msgs);
			msgs = basicSetUniversalRealDefinition(newUniversalRealDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__UNIVERSAL_REAL_DEFINITION, newUniversalRealDefinition, newUniversalRealDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UniversalFixedDefinition getUniversalFixedDefinition() {
		return universalFixedDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUniversalFixedDefinition(UniversalFixedDefinition newUniversalFixedDefinition, NotificationChain msgs) {
		UniversalFixedDefinition oldUniversalFixedDefinition = universalFixedDefinition;
		universalFixedDefinition = newUniversalFixedDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__UNIVERSAL_FIXED_DEFINITION, oldUniversalFixedDefinition, newUniversalFixedDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUniversalFixedDefinition(UniversalFixedDefinition newUniversalFixedDefinition) {
		if (newUniversalFixedDefinition != universalFixedDefinition) {
			NotificationChain msgs = null;
			if (universalFixedDefinition != null)
				msgs = ((InternalEObject)universalFixedDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__UNIVERSAL_FIXED_DEFINITION, null, msgs);
			if (newUniversalFixedDefinition != null)
				msgs = ((InternalEObject)newUniversalFixedDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__UNIVERSAL_FIXED_DEFINITION, null, msgs);
			msgs = basicSetUniversalFixedDefinition(newUniversalFixedDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__UNIVERSAL_FIXED_DEFINITION, newUniversalFixedDefinition, newUniversalFixedDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FloatingPointDefinition getFloatingPointDefinition() {
		return floatingPointDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFloatingPointDefinition(FloatingPointDefinition newFloatingPointDefinition, NotificationChain msgs) {
		FloatingPointDefinition oldFloatingPointDefinition = floatingPointDefinition;
		floatingPointDefinition = newFloatingPointDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FLOATING_POINT_DEFINITION, oldFloatingPointDefinition, newFloatingPointDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFloatingPointDefinition(FloatingPointDefinition newFloatingPointDefinition) {
		if (newFloatingPointDefinition != floatingPointDefinition) {
			NotificationChain msgs = null;
			if (floatingPointDefinition != null)
				msgs = ((InternalEObject)floatingPointDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FLOATING_POINT_DEFINITION, null, msgs);
			if (newFloatingPointDefinition != null)
				msgs = ((InternalEObject)newFloatingPointDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FLOATING_POINT_DEFINITION, null, msgs);
			msgs = basicSetFloatingPointDefinition(newFloatingPointDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FLOATING_POINT_DEFINITION, newFloatingPointDefinition, newFloatingPointDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrdinaryFixedPointDefinition getOrdinaryFixedPointDefinition() {
		return ordinaryFixedPointDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOrdinaryFixedPointDefinition(OrdinaryFixedPointDefinition newOrdinaryFixedPointDefinition, NotificationChain msgs) {
		OrdinaryFixedPointDefinition oldOrdinaryFixedPointDefinition = ordinaryFixedPointDefinition;
		ordinaryFixedPointDefinition = newOrdinaryFixedPointDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ORDINARY_FIXED_POINT_DEFINITION, oldOrdinaryFixedPointDefinition, newOrdinaryFixedPointDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrdinaryFixedPointDefinition(OrdinaryFixedPointDefinition newOrdinaryFixedPointDefinition) {
		if (newOrdinaryFixedPointDefinition != ordinaryFixedPointDefinition) {
			NotificationChain msgs = null;
			if (ordinaryFixedPointDefinition != null)
				msgs = ((InternalEObject)ordinaryFixedPointDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ORDINARY_FIXED_POINT_DEFINITION, null, msgs);
			if (newOrdinaryFixedPointDefinition != null)
				msgs = ((InternalEObject)newOrdinaryFixedPointDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ORDINARY_FIXED_POINT_DEFINITION, null, msgs);
			msgs = basicSetOrdinaryFixedPointDefinition(newOrdinaryFixedPointDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ORDINARY_FIXED_POINT_DEFINITION, newOrdinaryFixedPointDefinition, newOrdinaryFixedPointDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DecimalFixedPointDefinition getDecimalFixedPointDefinition() {
		return decimalFixedPointDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDecimalFixedPointDefinition(DecimalFixedPointDefinition newDecimalFixedPointDefinition, NotificationChain msgs) {
		DecimalFixedPointDefinition oldDecimalFixedPointDefinition = decimalFixedPointDefinition;
		decimalFixedPointDefinition = newDecimalFixedPointDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DECIMAL_FIXED_POINT_DEFINITION, oldDecimalFixedPointDefinition, newDecimalFixedPointDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDecimalFixedPointDefinition(DecimalFixedPointDefinition newDecimalFixedPointDefinition) {
		if (newDecimalFixedPointDefinition != decimalFixedPointDefinition) {
			NotificationChain msgs = null;
			if (decimalFixedPointDefinition != null)
				msgs = ((InternalEObject)decimalFixedPointDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DECIMAL_FIXED_POINT_DEFINITION, null, msgs);
			if (newDecimalFixedPointDefinition != null)
				msgs = ((InternalEObject)newDecimalFixedPointDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DECIMAL_FIXED_POINT_DEFINITION, null, msgs);
			msgs = basicSetDecimalFixedPointDefinition(newDecimalFixedPointDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DECIMAL_FIXED_POINT_DEFINITION, newDecimalFixedPointDefinition, newDecimalFixedPointDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnconstrainedArrayDefinition getUnconstrainedArrayDefinition() {
		return unconstrainedArrayDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnconstrainedArrayDefinition(UnconstrainedArrayDefinition newUnconstrainedArrayDefinition, NotificationChain msgs) {
		UnconstrainedArrayDefinition oldUnconstrainedArrayDefinition = unconstrainedArrayDefinition;
		unconstrainedArrayDefinition = newUnconstrainedArrayDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__UNCONSTRAINED_ARRAY_DEFINITION, oldUnconstrainedArrayDefinition, newUnconstrainedArrayDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnconstrainedArrayDefinition(UnconstrainedArrayDefinition newUnconstrainedArrayDefinition) {
		if (newUnconstrainedArrayDefinition != unconstrainedArrayDefinition) {
			NotificationChain msgs = null;
			if (unconstrainedArrayDefinition != null)
				msgs = ((InternalEObject)unconstrainedArrayDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__UNCONSTRAINED_ARRAY_DEFINITION, null, msgs);
			if (newUnconstrainedArrayDefinition != null)
				msgs = ((InternalEObject)newUnconstrainedArrayDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__UNCONSTRAINED_ARRAY_DEFINITION, null, msgs);
			msgs = basicSetUnconstrainedArrayDefinition(newUnconstrainedArrayDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__UNCONSTRAINED_ARRAY_DEFINITION, newUnconstrainedArrayDefinition, newUnconstrainedArrayDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstrainedArrayDefinition getConstrainedArrayDefinition() {
		return constrainedArrayDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConstrainedArrayDefinition(ConstrainedArrayDefinition newConstrainedArrayDefinition, NotificationChain msgs) {
		ConstrainedArrayDefinition oldConstrainedArrayDefinition = constrainedArrayDefinition;
		constrainedArrayDefinition = newConstrainedArrayDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__CONSTRAINED_ARRAY_DEFINITION, oldConstrainedArrayDefinition, newConstrainedArrayDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstrainedArrayDefinition(ConstrainedArrayDefinition newConstrainedArrayDefinition) {
		if (newConstrainedArrayDefinition != constrainedArrayDefinition) {
			NotificationChain msgs = null;
			if (constrainedArrayDefinition != null)
				msgs = ((InternalEObject)constrainedArrayDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__CONSTRAINED_ARRAY_DEFINITION, null, msgs);
			if (newConstrainedArrayDefinition != null)
				msgs = ((InternalEObject)newConstrainedArrayDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__CONSTRAINED_ARRAY_DEFINITION, null, msgs);
			msgs = basicSetConstrainedArrayDefinition(newConstrainedArrayDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__CONSTRAINED_ARRAY_DEFINITION, newConstrainedArrayDefinition, newConstrainedArrayDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordTypeDefinition getRecordTypeDefinition() {
		return recordTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRecordTypeDefinition(RecordTypeDefinition newRecordTypeDefinition, NotificationChain msgs) {
		RecordTypeDefinition oldRecordTypeDefinition = recordTypeDefinition;
		recordTypeDefinition = newRecordTypeDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__RECORD_TYPE_DEFINITION, oldRecordTypeDefinition, newRecordTypeDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRecordTypeDefinition(RecordTypeDefinition newRecordTypeDefinition) {
		if (newRecordTypeDefinition != recordTypeDefinition) {
			NotificationChain msgs = null;
			if (recordTypeDefinition != null)
				msgs = ((InternalEObject)recordTypeDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__RECORD_TYPE_DEFINITION, null, msgs);
			if (newRecordTypeDefinition != null)
				msgs = ((InternalEObject)newRecordTypeDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__RECORD_TYPE_DEFINITION, null, msgs);
			msgs = basicSetRecordTypeDefinition(newRecordTypeDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__RECORD_TYPE_DEFINITION, newRecordTypeDefinition, newRecordTypeDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaggedRecordTypeDefinition getTaggedRecordTypeDefinition() {
		return taggedRecordTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTaggedRecordTypeDefinition(TaggedRecordTypeDefinition newTaggedRecordTypeDefinition, NotificationChain msgs) {
		TaggedRecordTypeDefinition oldTaggedRecordTypeDefinition = taggedRecordTypeDefinition;
		taggedRecordTypeDefinition = newTaggedRecordTypeDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__TAGGED_RECORD_TYPE_DEFINITION, oldTaggedRecordTypeDefinition, newTaggedRecordTypeDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaggedRecordTypeDefinition(TaggedRecordTypeDefinition newTaggedRecordTypeDefinition) {
		if (newTaggedRecordTypeDefinition != taggedRecordTypeDefinition) {
			NotificationChain msgs = null;
			if (taggedRecordTypeDefinition != null)
				msgs = ((InternalEObject)taggedRecordTypeDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__TAGGED_RECORD_TYPE_DEFINITION, null, msgs);
			if (newTaggedRecordTypeDefinition != null)
				msgs = ((InternalEObject)newTaggedRecordTypeDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__TAGGED_RECORD_TYPE_DEFINITION, null, msgs);
			msgs = basicSetTaggedRecordTypeDefinition(newTaggedRecordTypeDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__TAGGED_RECORD_TYPE_DEFINITION, newTaggedRecordTypeDefinition, newTaggedRecordTypeDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrdinaryInterface getOrdinaryInterface() {
		return ordinaryInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOrdinaryInterface(OrdinaryInterface newOrdinaryInterface, NotificationChain msgs) {
		OrdinaryInterface oldOrdinaryInterface = ordinaryInterface;
		ordinaryInterface = newOrdinaryInterface;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ORDINARY_INTERFACE, oldOrdinaryInterface, newOrdinaryInterface);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrdinaryInterface(OrdinaryInterface newOrdinaryInterface) {
		if (newOrdinaryInterface != ordinaryInterface) {
			NotificationChain msgs = null;
			if (ordinaryInterface != null)
				msgs = ((InternalEObject)ordinaryInterface).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ORDINARY_INTERFACE, null, msgs);
			if (newOrdinaryInterface != null)
				msgs = ((InternalEObject)newOrdinaryInterface).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ORDINARY_INTERFACE, null, msgs);
			msgs = basicSetOrdinaryInterface(newOrdinaryInterface, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ORDINARY_INTERFACE, newOrdinaryInterface, newOrdinaryInterface));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LimitedInterface getLimitedInterface() {
		return limitedInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLimitedInterface(LimitedInterface newLimitedInterface, NotificationChain msgs) {
		LimitedInterface oldLimitedInterface = limitedInterface;
		limitedInterface = newLimitedInterface;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__LIMITED_INTERFACE, oldLimitedInterface, newLimitedInterface);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLimitedInterface(LimitedInterface newLimitedInterface) {
		if (newLimitedInterface != limitedInterface) {
			NotificationChain msgs = null;
			if (limitedInterface != null)
				msgs = ((InternalEObject)limitedInterface).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__LIMITED_INTERFACE, null, msgs);
			if (newLimitedInterface != null)
				msgs = ((InternalEObject)newLimitedInterface).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__LIMITED_INTERFACE, null, msgs);
			msgs = basicSetLimitedInterface(newLimitedInterface, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__LIMITED_INTERFACE, newLimitedInterface, newLimitedInterface));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskInterface getTaskInterface() {
		return taskInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTaskInterface(TaskInterface newTaskInterface, NotificationChain msgs) {
		TaskInterface oldTaskInterface = taskInterface;
		taskInterface = newTaskInterface;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__TASK_INTERFACE, oldTaskInterface, newTaskInterface);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaskInterface(TaskInterface newTaskInterface) {
		if (newTaskInterface != taskInterface) {
			NotificationChain msgs = null;
			if (taskInterface != null)
				msgs = ((InternalEObject)taskInterface).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__TASK_INTERFACE, null, msgs);
			if (newTaskInterface != null)
				msgs = ((InternalEObject)newTaskInterface).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__TASK_INTERFACE, null, msgs);
			msgs = basicSetTaskInterface(newTaskInterface, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__TASK_INTERFACE, newTaskInterface, newTaskInterface));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProtectedInterface getProtectedInterface() {
		return protectedInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProtectedInterface(ProtectedInterface newProtectedInterface, NotificationChain msgs) {
		ProtectedInterface oldProtectedInterface = protectedInterface;
		protectedInterface = newProtectedInterface;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PROTECTED_INTERFACE, oldProtectedInterface, newProtectedInterface);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProtectedInterface(ProtectedInterface newProtectedInterface) {
		if (newProtectedInterface != protectedInterface) {
			NotificationChain msgs = null;
			if (protectedInterface != null)
				msgs = ((InternalEObject)protectedInterface).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PROTECTED_INTERFACE, null, msgs);
			if (newProtectedInterface != null)
				msgs = ((InternalEObject)newProtectedInterface).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PROTECTED_INTERFACE, null, msgs);
			msgs = basicSetProtectedInterface(newProtectedInterface, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PROTECTED_INTERFACE, newProtectedInterface, newProtectedInterface));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SynchronizedInterface getSynchronizedInterface() {
		return synchronizedInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSynchronizedInterface(SynchronizedInterface newSynchronizedInterface, NotificationChain msgs) {
		SynchronizedInterface oldSynchronizedInterface = synchronizedInterface;
		synchronizedInterface = newSynchronizedInterface;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__SYNCHRONIZED_INTERFACE, oldSynchronizedInterface, newSynchronizedInterface);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSynchronizedInterface(SynchronizedInterface newSynchronizedInterface) {
		if (newSynchronizedInterface != synchronizedInterface) {
			NotificationChain msgs = null;
			if (synchronizedInterface != null)
				msgs = ((InternalEObject)synchronizedInterface).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__SYNCHRONIZED_INTERFACE, null, msgs);
			if (newSynchronizedInterface != null)
				msgs = ((InternalEObject)newSynchronizedInterface).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__SYNCHRONIZED_INTERFACE, null, msgs);
			msgs = basicSetSynchronizedInterface(newSynchronizedInterface, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__SYNCHRONIZED_INTERFACE, newSynchronizedInterface, newSynchronizedInterface));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PoolSpecificAccessToVariable getPoolSpecificAccessToVariable() {
		return poolSpecificAccessToVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPoolSpecificAccessToVariable(PoolSpecificAccessToVariable newPoolSpecificAccessToVariable, NotificationChain msgs) {
		PoolSpecificAccessToVariable oldPoolSpecificAccessToVariable = poolSpecificAccessToVariable;
		poolSpecificAccessToVariable = newPoolSpecificAccessToVariable;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__POOL_SPECIFIC_ACCESS_TO_VARIABLE, oldPoolSpecificAccessToVariable, newPoolSpecificAccessToVariable);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPoolSpecificAccessToVariable(PoolSpecificAccessToVariable newPoolSpecificAccessToVariable) {
		if (newPoolSpecificAccessToVariable != poolSpecificAccessToVariable) {
			NotificationChain msgs = null;
			if (poolSpecificAccessToVariable != null)
				msgs = ((InternalEObject)poolSpecificAccessToVariable).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__POOL_SPECIFIC_ACCESS_TO_VARIABLE, null, msgs);
			if (newPoolSpecificAccessToVariable != null)
				msgs = ((InternalEObject)newPoolSpecificAccessToVariable).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__POOL_SPECIFIC_ACCESS_TO_VARIABLE, null, msgs);
			msgs = basicSetPoolSpecificAccessToVariable(newPoolSpecificAccessToVariable, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__POOL_SPECIFIC_ACCESS_TO_VARIABLE, newPoolSpecificAccessToVariable, newPoolSpecificAccessToVariable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessToVariable getAccessToVariable() {
		return accessToVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccessToVariable(AccessToVariable newAccessToVariable, NotificationChain msgs) {
		AccessToVariable oldAccessToVariable = accessToVariable;
		accessToVariable = newAccessToVariable;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ACCESS_TO_VARIABLE, oldAccessToVariable, newAccessToVariable);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessToVariable(AccessToVariable newAccessToVariable) {
		if (newAccessToVariable != accessToVariable) {
			NotificationChain msgs = null;
			if (accessToVariable != null)
				msgs = ((InternalEObject)accessToVariable).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ACCESS_TO_VARIABLE, null, msgs);
			if (newAccessToVariable != null)
				msgs = ((InternalEObject)newAccessToVariable).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ACCESS_TO_VARIABLE, null, msgs);
			msgs = basicSetAccessToVariable(newAccessToVariable, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ACCESS_TO_VARIABLE, newAccessToVariable, newAccessToVariable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessToConstant getAccessToConstant() {
		return accessToConstant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccessToConstant(AccessToConstant newAccessToConstant, NotificationChain msgs) {
		AccessToConstant oldAccessToConstant = accessToConstant;
		accessToConstant = newAccessToConstant;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ACCESS_TO_CONSTANT, oldAccessToConstant, newAccessToConstant);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessToConstant(AccessToConstant newAccessToConstant) {
		if (newAccessToConstant != accessToConstant) {
			NotificationChain msgs = null;
			if (accessToConstant != null)
				msgs = ((InternalEObject)accessToConstant).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ACCESS_TO_CONSTANT, null, msgs);
			if (newAccessToConstant != null)
				msgs = ((InternalEObject)newAccessToConstant).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ACCESS_TO_CONSTANT, null, msgs);
			msgs = basicSetAccessToConstant(newAccessToConstant, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ACCESS_TO_CONSTANT, newAccessToConstant, newAccessToConstant));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessToProcedure getAccessToProcedure() {
		return accessToProcedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccessToProcedure(AccessToProcedure newAccessToProcedure, NotificationChain msgs) {
		AccessToProcedure oldAccessToProcedure = accessToProcedure;
		accessToProcedure = newAccessToProcedure;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROCEDURE, oldAccessToProcedure, newAccessToProcedure);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessToProcedure(AccessToProcedure newAccessToProcedure) {
		if (newAccessToProcedure != accessToProcedure) {
			NotificationChain msgs = null;
			if (accessToProcedure != null)
				msgs = ((InternalEObject)accessToProcedure).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROCEDURE, null, msgs);
			if (newAccessToProcedure != null)
				msgs = ((InternalEObject)newAccessToProcedure).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROCEDURE, null, msgs);
			msgs = basicSetAccessToProcedure(newAccessToProcedure, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROCEDURE, newAccessToProcedure, newAccessToProcedure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessToProtectedProcedure getAccessToProtectedProcedure() {
		return accessToProtectedProcedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccessToProtectedProcedure(AccessToProtectedProcedure newAccessToProtectedProcedure, NotificationChain msgs) {
		AccessToProtectedProcedure oldAccessToProtectedProcedure = accessToProtectedProcedure;
		accessToProtectedProcedure = newAccessToProtectedProcedure;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROTECTED_PROCEDURE, oldAccessToProtectedProcedure, newAccessToProtectedProcedure);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessToProtectedProcedure(AccessToProtectedProcedure newAccessToProtectedProcedure) {
		if (newAccessToProtectedProcedure != accessToProtectedProcedure) {
			NotificationChain msgs = null;
			if (accessToProtectedProcedure != null)
				msgs = ((InternalEObject)accessToProtectedProcedure).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROTECTED_PROCEDURE, null, msgs);
			if (newAccessToProtectedProcedure != null)
				msgs = ((InternalEObject)newAccessToProtectedProcedure).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROTECTED_PROCEDURE, null, msgs);
			msgs = basicSetAccessToProtectedProcedure(newAccessToProtectedProcedure, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROTECTED_PROCEDURE, newAccessToProtectedProcedure, newAccessToProtectedProcedure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessToFunction getAccessToFunction() {
		return accessToFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccessToFunction(AccessToFunction newAccessToFunction, NotificationChain msgs) {
		AccessToFunction oldAccessToFunction = accessToFunction;
		accessToFunction = newAccessToFunction;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ACCESS_TO_FUNCTION, oldAccessToFunction, newAccessToFunction);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessToFunction(AccessToFunction newAccessToFunction) {
		if (newAccessToFunction != accessToFunction) {
			NotificationChain msgs = null;
			if (accessToFunction != null)
				msgs = ((InternalEObject)accessToFunction).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ACCESS_TO_FUNCTION, null, msgs);
			if (newAccessToFunction != null)
				msgs = ((InternalEObject)newAccessToFunction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ACCESS_TO_FUNCTION, null, msgs);
			msgs = basicSetAccessToFunction(newAccessToFunction, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ACCESS_TO_FUNCTION, newAccessToFunction, newAccessToFunction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccessToProtectedFunction getAccessToProtectedFunction() {
		return accessToProtectedFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccessToProtectedFunction(AccessToProtectedFunction newAccessToProtectedFunction, NotificationChain msgs) {
		AccessToProtectedFunction oldAccessToProtectedFunction = accessToProtectedFunction;
		accessToProtectedFunction = newAccessToProtectedFunction;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROTECTED_FUNCTION, oldAccessToProtectedFunction, newAccessToProtectedFunction);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccessToProtectedFunction(AccessToProtectedFunction newAccessToProtectedFunction) {
		if (newAccessToProtectedFunction != accessToProtectedFunction) {
			NotificationChain msgs = null;
			if (accessToProtectedFunction != null)
				msgs = ((InternalEObject)accessToProtectedFunction).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROTECTED_FUNCTION, null, msgs);
			if (newAccessToProtectedFunction != null)
				msgs = ((InternalEObject)newAccessToProtectedFunction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROTECTED_FUNCTION, null, msgs);
			msgs = basicSetAccessToProtectedFunction(newAccessToProtectedFunction, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROTECTED_FUNCTION, newAccessToProtectedFunction, newAccessToProtectedFunction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubtypeIndication getSubtypeIndication() {
		return subtypeIndication;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSubtypeIndication(SubtypeIndication newSubtypeIndication, NotificationChain msgs) {
		SubtypeIndication oldSubtypeIndication = subtypeIndication;
		subtypeIndication = newSubtypeIndication;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__SUBTYPE_INDICATION, oldSubtypeIndication, newSubtypeIndication);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubtypeIndication(SubtypeIndication newSubtypeIndication) {
		if (newSubtypeIndication != subtypeIndication) {
			NotificationChain msgs = null;
			if (subtypeIndication != null)
				msgs = ((InternalEObject)subtypeIndication).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__SUBTYPE_INDICATION, null, msgs);
			if (newSubtypeIndication != null)
				msgs = ((InternalEObject)newSubtypeIndication).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__SUBTYPE_INDICATION, null, msgs);
			msgs = basicSetSubtypeIndication(newSubtypeIndication, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__SUBTYPE_INDICATION, newSubtypeIndication, newSubtypeIndication));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RangeAttributeReference getRangeAttributeReference() {
		return rangeAttributeReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRangeAttributeReference(RangeAttributeReference newRangeAttributeReference, NotificationChain msgs) {
		RangeAttributeReference oldRangeAttributeReference = rangeAttributeReference;
		rangeAttributeReference = newRangeAttributeReference;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__RANGE_ATTRIBUTE_REFERENCE, oldRangeAttributeReference, newRangeAttributeReference);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRangeAttributeReference(RangeAttributeReference newRangeAttributeReference) {
		if (newRangeAttributeReference != rangeAttributeReference) {
			NotificationChain msgs = null;
			if (rangeAttributeReference != null)
				msgs = ((InternalEObject)rangeAttributeReference).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__RANGE_ATTRIBUTE_REFERENCE, null, msgs);
			if (newRangeAttributeReference != null)
				msgs = ((InternalEObject)newRangeAttributeReference).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__RANGE_ATTRIBUTE_REFERENCE, null, msgs);
			msgs = basicSetRangeAttributeReference(newRangeAttributeReference, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__RANGE_ATTRIBUTE_REFERENCE, newRangeAttributeReference, newRangeAttributeReference));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleExpressionRange getSimpleExpressionRange() {
		return simpleExpressionRange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSimpleExpressionRange(SimpleExpressionRange newSimpleExpressionRange, NotificationChain msgs) {
		SimpleExpressionRange oldSimpleExpressionRange = simpleExpressionRange;
		simpleExpressionRange = newSimpleExpressionRange;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__SIMPLE_EXPRESSION_RANGE, oldSimpleExpressionRange, newSimpleExpressionRange);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSimpleExpressionRange(SimpleExpressionRange newSimpleExpressionRange) {
		if (newSimpleExpressionRange != simpleExpressionRange) {
			NotificationChain msgs = null;
			if (simpleExpressionRange != null)
				msgs = ((InternalEObject)simpleExpressionRange).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__SIMPLE_EXPRESSION_RANGE, null, msgs);
			if (newSimpleExpressionRange != null)
				msgs = ((InternalEObject)newSimpleExpressionRange).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__SIMPLE_EXPRESSION_RANGE, null, msgs);
			msgs = basicSetSimpleExpressionRange(newSimpleExpressionRange, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__SIMPLE_EXPRESSION_RANGE, newSimpleExpressionRange, newSimpleExpressionRange));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DigitsConstraint getDigitsConstraint() {
		return digitsConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDigitsConstraint(DigitsConstraint newDigitsConstraint, NotificationChain msgs) {
		DigitsConstraint oldDigitsConstraint = digitsConstraint;
		digitsConstraint = newDigitsConstraint;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DIGITS_CONSTRAINT, oldDigitsConstraint, newDigitsConstraint);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDigitsConstraint(DigitsConstraint newDigitsConstraint) {
		if (newDigitsConstraint != digitsConstraint) {
			NotificationChain msgs = null;
			if (digitsConstraint != null)
				msgs = ((InternalEObject)digitsConstraint).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DIGITS_CONSTRAINT, null, msgs);
			if (newDigitsConstraint != null)
				msgs = ((InternalEObject)newDigitsConstraint).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DIGITS_CONSTRAINT, null, msgs);
			msgs = basicSetDigitsConstraint(newDigitsConstraint, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DIGITS_CONSTRAINT, newDigitsConstraint, newDigitsConstraint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeltaConstraint getDeltaConstraint() {
		return deltaConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDeltaConstraint(DeltaConstraint newDeltaConstraint, NotificationChain msgs) {
		DeltaConstraint oldDeltaConstraint = deltaConstraint;
		deltaConstraint = newDeltaConstraint;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DELTA_CONSTRAINT, oldDeltaConstraint, newDeltaConstraint);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeltaConstraint(DeltaConstraint newDeltaConstraint) {
		if (newDeltaConstraint != deltaConstraint) {
			NotificationChain msgs = null;
			if (deltaConstraint != null)
				msgs = ((InternalEObject)deltaConstraint).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DELTA_CONSTRAINT, null, msgs);
			if (newDeltaConstraint != null)
				msgs = ((InternalEObject)newDeltaConstraint).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DELTA_CONSTRAINT, null, msgs);
			msgs = basicSetDeltaConstraint(newDeltaConstraint, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DELTA_CONSTRAINT, newDeltaConstraint, newDeltaConstraint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IndexConstraint getIndexConstraint() {
		return indexConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIndexConstraint(IndexConstraint newIndexConstraint, NotificationChain msgs) {
		IndexConstraint oldIndexConstraint = indexConstraint;
		indexConstraint = newIndexConstraint;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__INDEX_CONSTRAINT, oldIndexConstraint, newIndexConstraint);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndexConstraint(IndexConstraint newIndexConstraint) {
		if (newIndexConstraint != indexConstraint) {
			NotificationChain msgs = null;
			if (indexConstraint != null)
				msgs = ((InternalEObject)indexConstraint).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__INDEX_CONSTRAINT, null, msgs);
			if (newIndexConstraint != null)
				msgs = ((InternalEObject)newIndexConstraint).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__INDEX_CONSTRAINT, null, msgs);
			msgs = basicSetIndexConstraint(newIndexConstraint, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__INDEX_CONSTRAINT, newIndexConstraint, newIndexConstraint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscriminantConstraint getDiscriminantConstraint() {
		return discriminantConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscriminantConstraint(DiscriminantConstraint newDiscriminantConstraint, NotificationChain msgs) {
		DiscriminantConstraint oldDiscriminantConstraint = discriminantConstraint;
		discriminantConstraint = newDiscriminantConstraint;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DISCRIMINANT_CONSTRAINT, oldDiscriminantConstraint, newDiscriminantConstraint);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscriminantConstraint(DiscriminantConstraint newDiscriminantConstraint) {
		if (newDiscriminantConstraint != discriminantConstraint) {
			NotificationChain msgs = null;
			if (discriminantConstraint != null)
				msgs = ((InternalEObject)discriminantConstraint).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DISCRIMINANT_CONSTRAINT, null, msgs);
			if (newDiscriminantConstraint != null)
				msgs = ((InternalEObject)newDiscriminantConstraint).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DISCRIMINANT_CONSTRAINT, null, msgs);
			msgs = basicSetDiscriminantConstraint(newDiscriminantConstraint, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DISCRIMINANT_CONSTRAINT, newDiscriminantConstraint, newDiscriminantConstraint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentDefinition getComponentDefinition() {
		return componentDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComponentDefinition(ComponentDefinition newComponentDefinition, NotificationChain msgs) {
		ComponentDefinition oldComponentDefinition = componentDefinition;
		componentDefinition = newComponentDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__COMPONENT_DEFINITION, oldComponentDefinition, newComponentDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponentDefinition(ComponentDefinition newComponentDefinition) {
		if (newComponentDefinition != componentDefinition) {
			NotificationChain msgs = null;
			if (componentDefinition != null)
				msgs = ((InternalEObject)componentDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__COMPONENT_DEFINITION, null, msgs);
			if (newComponentDefinition != null)
				msgs = ((InternalEObject)newComponentDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__COMPONENT_DEFINITION, null, msgs);
			msgs = basicSetComponentDefinition(newComponentDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__COMPONENT_DEFINITION, newComponentDefinition, newComponentDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscreteSubtypeIndicationAsSubtypeDefinition getDiscreteSubtypeIndicationAsSubtypeDefinition() {
		return discreteSubtypeIndicationAsSubtypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscreteSubtypeIndicationAsSubtypeDefinition(DiscreteSubtypeIndicationAsSubtypeDefinition newDiscreteSubtypeIndicationAsSubtypeDefinition, NotificationChain msgs) {
		DiscreteSubtypeIndicationAsSubtypeDefinition oldDiscreteSubtypeIndicationAsSubtypeDefinition = discreteSubtypeIndicationAsSubtypeDefinition;
		discreteSubtypeIndicationAsSubtypeDefinition = newDiscreteSubtypeIndicationAsSubtypeDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION, oldDiscreteSubtypeIndicationAsSubtypeDefinition, newDiscreteSubtypeIndicationAsSubtypeDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscreteSubtypeIndicationAsSubtypeDefinition(DiscreteSubtypeIndicationAsSubtypeDefinition newDiscreteSubtypeIndicationAsSubtypeDefinition) {
		if (newDiscreteSubtypeIndicationAsSubtypeDefinition != discreteSubtypeIndicationAsSubtypeDefinition) {
			NotificationChain msgs = null;
			if (discreteSubtypeIndicationAsSubtypeDefinition != null)
				msgs = ((InternalEObject)discreteSubtypeIndicationAsSubtypeDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION, null, msgs);
			if (newDiscreteSubtypeIndicationAsSubtypeDefinition != null)
				msgs = ((InternalEObject)newDiscreteSubtypeIndicationAsSubtypeDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION, null, msgs);
			msgs = basicSetDiscreteSubtypeIndicationAsSubtypeDefinition(newDiscreteSubtypeIndicationAsSubtypeDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION, newDiscreteSubtypeIndicationAsSubtypeDefinition, newDiscreteSubtypeIndicationAsSubtypeDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscreteRangeAttributeReferenceAsSubtypeDefinition getDiscreteRangeAttributeReferenceAsSubtypeDefinition() {
		return discreteRangeAttributeReferenceAsSubtypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscreteRangeAttributeReferenceAsSubtypeDefinition(DiscreteRangeAttributeReferenceAsSubtypeDefinition newDiscreteRangeAttributeReferenceAsSubtypeDefinition, NotificationChain msgs) {
		DiscreteRangeAttributeReferenceAsSubtypeDefinition oldDiscreteRangeAttributeReferenceAsSubtypeDefinition = discreteRangeAttributeReferenceAsSubtypeDefinition;
		discreteRangeAttributeReferenceAsSubtypeDefinition = newDiscreteRangeAttributeReferenceAsSubtypeDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION, oldDiscreteRangeAttributeReferenceAsSubtypeDefinition, newDiscreteRangeAttributeReferenceAsSubtypeDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscreteRangeAttributeReferenceAsSubtypeDefinition(DiscreteRangeAttributeReferenceAsSubtypeDefinition newDiscreteRangeAttributeReferenceAsSubtypeDefinition) {
		if (newDiscreteRangeAttributeReferenceAsSubtypeDefinition != discreteRangeAttributeReferenceAsSubtypeDefinition) {
			NotificationChain msgs = null;
			if (discreteRangeAttributeReferenceAsSubtypeDefinition != null)
				msgs = ((InternalEObject)discreteRangeAttributeReferenceAsSubtypeDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION, null, msgs);
			if (newDiscreteRangeAttributeReferenceAsSubtypeDefinition != null)
				msgs = ((InternalEObject)newDiscreteRangeAttributeReferenceAsSubtypeDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION, null, msgs);
			msgs = basicSetDiscreteRangeAttributeReferenceAsSubtypeDefinition(newDiscreteRangeAttributeReferenceAsSubtypeDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION, newDiscreteRangeAttributeReferenceAsSubtypeDefinition, newDiscreteRangeAttributeReferenceAsSubtypeDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscreteSimpleExpressionRangeAsSubtypeDefinition getDiscreteSimpleExpressionRangeAsSubtypeDefinition() {
		return discreteSimpleExpressionRangeAsSubtypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscreteSimpleExpressionRangeAsSubtypeDefinition(DiscreteSimpleExpressionRangeAsSubtypeDefinition newDiscreteSimpleExpressionRangeAsSubtypeDefinition, NotificationChain msgs) {
		DiscreteSimpleExpressionRangeAsSubtypeDefinition oldDiscreteSimpleExpressionRangeAsSubtypeDefinition = discreteSimpleExpressionRangeAsSubtypeDefinition;
		discreteSimpleExpressionRangeAsSubtypeDefinition = newDiscreteSimpleExpressionRangeAsSubtypeDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION, oldDiscreteSimpleExpressionRangeAsSubtypeDefinition, newDiscreteSimpleExpressionRangeAsSubtypeDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscreteSimpleExpressionRangeAsSubtypeDefinition(DiscreteSimpleExpressionRangeAsSubtypeDefinition newDiscreteSimpleExpressionRangeAsSubtypeDefinition) {
		if (newDiscreteSimpleExpressionRangeAsSubtypeDefinition != discreteSimpleExpressionRangeAsSubtypeDefinition) {
			NotificationChain msgs = null;
			if (discreteSimpleExpressionRangeAsSubtypeDefinition != null)
				msgs = ((InternalEObject)discreteSimpleExpressionRangeAsSubtypeDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION, null, msgs);
			if (newDiscreteSimpleExpressionRangeAsSubtypeDefinition != null)
				msgs = ((InternalEObject)newDiscreteSimpleExpressionRangeAsSubtypeDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION, null, msgs);
			msgs = basicSetDiscreteSimpleExpressionRangeAsSubtypeDefinition(newDiscreteSimpleExpressionRangeAsSubtypeDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION, newDiscreteSimpleExpressionRangeAsSubtypeDefinition, newDiscreteSimpleExpressionRangeAsSubtypeDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscreteSubtypeIndication getDiscreteSubtypeIndication() {
		return discreteSubtypeIndication;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscreteSubtypeIndication(DiscreteSubtypeIndication newDiscreteSubtypeIndication, NotificationChain msgs) {
		DiscreteSubtypeIndication oldDiscreteSubtypeIndication = discreteSubtypeIndication;
		discreteSubtypeIndication = newDiscreteSubtypeIndication;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DISCRETE_SUBTYPE_INDICATION, oldDiscreteSubtypeIndication, newDiscreteSubtypeIndication);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscreteSubtypeIndication(DiscreteSubtypeIndication newDiscreteSubtypeIndication) {
		if (newDiscreteSubtypeIndication != discreteSubtypeIndication) {
			NotificationChain msgs = null;
			if (discreteSubtypeIndication != null)
				msgs = ((InternalEObject)discreteSubtypeIndication).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DISCRETE_SUBTYPE_INDICATION, null, msgs);
			if (newDiscreteSubtypeIndication != null)
				msgs = ((InternalEObject)newDiscreteSubtypeIndication).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DISCRETE_SUBTYPE_INDICATION, null, msgs);
			msgs = basicSetDiscreteSubtypeIndication(newDiscreteSubtypeIndication, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DISCRETE_SUBTYPE_INDICATION, newDiscreteSubtypeIndication, newDiscreteSubtypeIndication));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscreteRangeAttributeReference getDiscreteRangeAttributeReference() {
		return discreteRangeAttributeReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscreteRangeAttributeReference(DiscreteRangeAttributeReference newDiscreteRangeAttributeReference, NotificationChain msgs) {
		DiscreteRangeAttributeReference oldDiscreteRangeAttributeReference = discreteRangeAttributeReference;
		discreteRangeAttributeReference = newDiscreteRangeAttributeReference;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DISCRETE_RANGE_ATTRIBUTE_REFERENCE, oldDiscreteRangeAttributeReference, newDiscreteRangeAttributeReference);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscreteRangeAttributeReference(DiscreteRangeAttributeReference newDiscreteRangeAttributeReference) {
		if (newDiscreteRangeAttributeReference != discreteRangeAttributeReference) {
			NotificationChain msgs = null;
			if (discreteRangeAttributeReference != null)
				msgs = ((InternalEObject)discreteRangeAttributeReference).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DISCRETE_RANGE_ATTRIBUTE_REFERENCE, null, msgs);
			if (newDiscreteRangeAttributeReference != null)
				msgs = ((InternalEObject)newDiscreteRangeAttributeReference).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DISCRETE_RANGE_ATTRIBUTE_REFERENCE, null, msgs);
			msgs = basicSetDiscreteRangeAttributeReference(newDiscreteRangeAttributeReference, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DISCRETE_RANGE_ATTRIBUTE_REFERENCE, newDiscreteRangeAttributeReference, newDiscreteRangeAttributeReference));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscreteSimpleExpressionRange getDiscreteSimpleExpressionRange() {
		return discreteSimpleExpressionRange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscreteSimpleExpressionRange(DiscreteSimpleExpressionRange newDiscreteSimpleExpressionRange, NotificationChain msgs) {
		DiscreteSimpleExpressionRange oldDiscreteSimpleExpressionRange = discreteSimpleExpressionRange;
		discreteSimpleExpressionRange = newDiscreteSimpleExpressionRange;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DISCRETE_SIMPLE_EXPRESSION_RANGE, oldDiscreteSimpleExpressionRange, newDiscreteSimpleExpressionRange);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscreteSimpleExpressionRange(DiscreteSimpleExpressionRange newDiscreteSimpleExpressionRange) {
		if (newDiscreteSimpleExpressionRange != discreteSimpleExpressionRange) {
			NotificationChain msgs = null;
			if (discreteSimpleExpressionRange != null)
				msgs = ((InternalEObject)discreteSimpleExpressionRange).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DISCRETE_SIMPLE_EXPRESSION_RANGE, null, msgs);
			if (newDiscreteSimpleExpressionRange != null)
				msgs = ((InternalEObject)newDiscreteSimpleExpressionRange).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DISCRETE_SIMPLE_EXPRESSION_RANGE, null, msgs);
			msgs = basicSetDiscreteSimpleExpressionRange(newDiscreteSimpleExpressionRange, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DISCRETE_SIMPLE_EXPRESSION_RANGE, newDiscreteSimpleExpressionRange, newDiscreteSimpleExpressionRange));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnknownDiscriminantPart getUnknownDiscriminantPart() {
		return unknownDiscriminantPart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnknownDiscriminantPart(UnknownDiscriminantPart newUnknownDiscriminantPart, NotificationChain msgs) {
		UnknownDiscriminantPart oldUnknownDiscriminantPart = unknownDiscriminantPart;
		unknownDiscriminantPart = newUnknownDiscriminantPart;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__UNKNOWN_DISCRIMINANT_PART, oldUnknownDiscriminantPart, newUnknownDiscriminantPart);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnknownDiscriminantPart(UnknownDiscriminantPart newUnknownDiscriminantPart) {
		if (newUnknownDiscriminantPart != unknownDiscriminantPart) {
			NotificationChain msgs = null;
			if (unknownDiscriminantPart != null)
				msgs = ((InternalEObject)unknownDiscriminantPart).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__UNKNOWN_DISCRIMINANT_PART, null, msgs);
			if (newUnknownDiscriminantPart != null)
				msgs = ((InternalEObject)newUnknownDiscriminantPart).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__UNKNOWN_DISCRIMINANT_PART, null, msgs);
			msgs = basicSetUnknownDiscriminantPart(newUnknownDiscriminantPart, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__UNKNOWN_DISCRIMINANT_PART, newUnknownDiscriminantPart, newUnknownDiscriminantPart));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public KnownDiscriminantPart getKnownDiscriminantPart() {
		return knownDiscriminantPart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetKnownDiscriminantPart(KnownDiscriminantPart newKnownDiscriminantPart, NotificationChain msgs) {
		KnownDiscriminantPart oldKnownDiscriminantPart = knownDiscriminantPart;
		knownDiscriminantPart = newKnownDiscriminantPart;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__KNOWN_DISCRIMINANT_PART, oldKnownDiscriminantPart, newKnownDiscriminantPart);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKnownDiscriminantPart(KnownDiscriminantPart newKnownDiscriminantPart) {
		if (newKnownDiscriminantPart != knownDiscriminantPart) {
			NotificationChain msgs = null;
			if (knownDiscriminantPart != null)
				msgs = ((InternalEObject)knownDiscriminantPart).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__KNOWN_DISCRIMINANT_PART, null, msgs);
			if (newKnownDiscriminantPart != null)
				msgs = ((InternalEObject)newKnownDiscriminantPart).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__KNOWN_DISCRIMINANT_PART, null, msgs);
			msgs = basicSetKnownDiscriminantPart(newKnownDiscriminantPart, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__KNOWN_DISCRIMINANT_PART, newKnownDiscriminantPart, newKnownDiscriminantPart));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RecordDefinition getRecordDefinition() {
		return recordDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRecordDefinition(RecordDefinition newRecordDefinition, NotificationChain msgs) {
		RecordDefinition oldRecordDefinition = recordDefinition;
		recordDefinition = newRecordDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__RECORD_DEFINITION, oldRecordDefinition, newRecordDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRecordDefinition(RecordDefinition newRecordDefinition) {
		if (newRecordDefinition != recordDefinition) {
			NotificationChain msgs = null;
			if (recordDefinition != null)
				msgs = ((InternalEObject)recordDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__RECORD_DEFINITION, null, msgs);
			if (newRecordDefinition != null)
				msgs = ((InternalEObject)newRecordDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__RECORD_DEFINITION, null, msgs);
			msgs = basicSetRecordDefinition(newRecordDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__RECORD_DEFINITION, newRecordDefinition, newRecordDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NullRecordDefinition getNullRecordDefinition() {
		return nullRecordDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNullRecordDefinition(NullRecordDefinition newNullRecordDefinition, NotificationChain msgs) {
		NullRecordDefinition oldNullRecordDefinition = nullRecordDefinition;
		nullRecordDefinition = newNullRecordDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__NULL_RECORD_DEFINITION, oldNullRecordDefinition, newNullRecordDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNullRecordDefinition(NullRecordDefinition newNullRecordDefinition) {
		if (newNullRecordDefinition != nullRecordDefinition) {
			NotificationChain msgs = null;
			if (nullRecordDefinition != null)
				msgs = ((InternalEObject)nullRecordDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__NULL_RECORD_DEFINITION, null, msgs);
			if (newNullRecordDefinition != null)
				msgs = ((InternalEObject)newNullRecordDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__NULL_RECORD_DEFINITION, null, msgs);
			msgs = basicSetNullRecordDefinition(newNullRecordDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__NULL_RECORD_DEFINITION, newNullRecordDefinition, newNullRecordDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NullComponent getNullComponent() {
		return nullComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNullComponent(NullComponent newNullComponent, NotificationChain msgs) {
		NullComponent oldNullComponent = nullComponent;
		nullComponent = newNullComponent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__NULL_COMPONENT, oldNullComponent, newNullComponent);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNullComponent(NullComponent newNullComponent) {
		if (newNullComponent != nullComponent) {
			NotificationChain msgs = null;
			if (nullComponent != null)
				msgs = ((InternalEObject)nullComponent).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__NULL_COMPONENT, null, msgs);
			if (newNullComponent != null)
				msgs = ((InternalEObject)newNullComponent).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__NULL_COMPONENT, null, msgs);
			msgs = basicSetNullComponent(newNullComponent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__NULL_COMPONENT, newNullComponent, newNullComponent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariantPart getVariantPart() {
		return variantPart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVariantPart(VariantPart newVariantPart, NotificationChain msgs) {
		VariantPart oldVariantPart = variantPart;
		variantPart = newVariantPart;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__VARIANT_PART, oldVariantPart, newVariantPart);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVariantPart(VariantPart newVariantPart) {
		if (newVariantPart != variantPart) {
			NotificationChain msgs = null;
			if (variantPart != null)
				msgs = ((InternalEObject)variantPart).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__VARIANT_PART, null, msgs);
			if (newVariantPart != null)
				msgs = ((InternalEObject)newVariantPart).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__VARIANT_PART, null, msgs);
			msgs = basicSetVariantPart(newVariantPart, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__VARIANT_PART, newVariantPart, newVariantPart));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variant getVariant() {
		return variant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVariant(Variant newVariant, NotificationChain msgs) {
		Variant oldVariant = variant;
		variant = newVariant;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__VARIANT, oldVariant, newVariant);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVariant(Variant newVariant) {
		if (newVariant != variant) {
			NotificationChain msgs = null;
			if (variant != null)
				msgs = ((InternalEObject)variant).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__VARIANT, null, msgs);
			if (newVariant != null)
				msgs = ((InternalEObject)newVariant).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__VARIANT, null, msgs);
			msgs = basicSetVariant(newVariant, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__VARIANT, newVariant, newVariant));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OthersChoice getOthersChoice() {
		return othersChoice;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOthersChoice(OthersChoice newOthersChoice, NotificationChain msgs) {
		OthersChoice oldOthersChoice = othersChoice;
		othersChoice = newOthersChoice;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__OTHERS_CHOICE, oldOthersChoice, newOthersChoice);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOthersChoice(OthersChoice newOthersChoice) {
		if (newOthersChoice != othersChoice) {
			NotificationChain msgs = null;
			if (othersChoice != null)
				msgs = ((InternalEObject)othersChoice).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__OTHERS_CHOICE, null, msgs);
			if (newOthersChoice != null)
				msgs = ((InternalEObject)newOthersChoice).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__OTHERS_CHOICE, null, msgs);
			msgs = basicSetOthersChoice(newOthersChoice, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__OTHERS_CHOICE, newOthersChoice, newOthersChoice));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnonymousAccessToVariable getAnonymousAccessToVariable() {
		return anonymousAccessToVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnonymousAccessToVariable(AnonymousAccessToVariable newAnonymousAccessToVariable, NotificationChain msgs) {
		AnonymousAccessToVariable oldAnonymousAccessToVariable = anonymousAccessToVariable;
		anonymousAccessToVariable = newAnonymousAccessToVariable;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_VARIABLE, oldAnonymousAccessToVariable, newAnonymousAccessToVariable);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnonymousAccessToVariable(AnonymousAccessToVariable newAnonymousAccessToVariable) {
		if (newAnonymousAccessToVariable != anonymousAccessToVariable) {
			NotificationChain msgs = null;
			if (anonymousAccessToVariable != null)
				msgs = ((InternalEObject)anonymousAccessToVariable).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_VARIABLE, null, msgs);
			if (newAnonymousAccessToVariable != null)
				msgs = ((InternalEObject)newAnonymousAccessToVariable).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_VARIABLE, null, msgs);
			msgs = basicSetAnonymousAccessToVariable(newAnonymousAccessToVariable, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_VARIABLE, newAnonymousAccessToVariable, newAnonymousAccessToVariable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnonymousAccessToConstant getAnonymousAccessToConstant() {
		return anonymousAccessToConstant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnonymousAccessToConstant(AnonymousAccessToConstant newAnonymousAccessToConstant, NotificationChain msgs) {
		AnonymousAccessToConstant oldAnonymousAccessToConstant = anonymousAccessToConstant;
		anonymousAccessToConstant = newAnonymousAccessToConstant;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_CONSTANT, oldAnonymousAccessToConstant, newAnonymousAccessToConstant);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnonymousAccessToConstant(AnonymousAccessToConstant newAnonymousAccessToConstant) {
		if (newAnonymousAccessToConstant != anonymousAccessToConstant) {
			NotificationChain msgs = null;
			if (anonymousAccessToConstant != null)
				msgs = ((InternalEObject)anonymousAccessToConstant).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_CONSTANT, null, msgs);
			if (newAnonymousAccessToConstant != null)
				msgs = ((InternalEObject)newAnonymousAccessToConstant).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_CONSTANT, null, msgs);
			msgs = basicSetAnonymousAccessToConstant(newAnonymousAccessToConstant, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_CONSTANT, newAnonymousAccessToConstant, newAnonymousAccessToConstant));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnonymousAccessToProcedure getAnonymousAccessToProcedure() {
		return anonymousAccessToProcedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnonymousAccessToProcedure(AnonymousAccessToProcedure newAnonymousAccessToProcedure, NotificationChain msgs) {
		AnonymousAccessToProcedure oldAnonymousAccessToProcedure = anonymousAccessToProcedure;
		anonymousAccessToProcedure = newAnonymousAccessToProcedure;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROCEDURE, oldAnonymousAccessToProcedure, newAnonymousAccessToProcedure);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnonymousAccessToProcedure(AnonymousAccessToProcedure newAnonymousAccessToProcedure) {
		if (newAnonymousAccessToProcedure != anonymousAccessToProcedure) {
			NotificationChain msgs = null;
			if (anonymousAccessToProcedure != null)
				msgs = ((InternalEObject)anonymousAccessToProcedure).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROCEDURE, null, msgs);
			if (newAnonymousAccessToProcedure != null)
				msgs = ((InternalEObject)newAnonymousAccessToProcedure).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROCEDURE, null, msgs);
			msgs = basicSetAnonymousAccessToProcedure(newAnonymousAccessToProcedure, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROCEDURE, newAnonymousAccessToProcedure, newAnonymousAccessToProcedure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnonymousAccessToProtectedProcedure getAnonymousAccessToProtectedProcedure() {
		return anonymousAccessToProtectedProcedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnonymousAccessToProtectedProcedure(AnonymousAccessToProtectedProcedure newAnonymousAccessToProtectedProcedure, NotificationChain msgs) {
		AnonymousAccessToProtectedProcedure oldAnonymousAccessToProtectedProcedure = anonymousAccessToProtectedProcedure;
		anonymousAccessToProtectedProcedure = newAnonymousAccessToProtectedProcedure;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE, oldAnonymousAccessToProtectedProcedure, newAnonymousAccessToProtectedProcedure);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnonymousAccessToProtectedProcedure(AnonymousAccessToProtectedProcedure newAnonymousAccessToProtectedProcedure) {
		if (newAnonymousAccessToProtectedProcedure != anonymousAccessToProtectedProcedure) {
			NotificationChain msgs = null;
			if (anonymousAccessToProtectedProcedure != null)
				msgs = ((InternalEObject)anonymousAccessToProtectedProcedure).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE, null, msgs);
			if (newAnonymousAccessToProtectedProcedure != null)
				msgs = ((InternalEObject)newAnonymousAccessToProtectedProcedure).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE, null, msgs);
			msgs = basicSetAnonymousAccessToProtectedProcedure(newAnonymousAccessToProtectedProcedure, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE, newAnonymousAccessToProtectedProcedure, newAnonymousAccessToProtectedProcedure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnonymousAccessToFunction getAnonymousAccessToFunction() {
		return anonymousAccessToFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnonymousAccessToFunction(AnonymousAccessToFunction newAnonymousAccessToFunction, NotificationChain msgs) {
		AnonymousAccessToFunction oldAnonymousAccessToFunction = anonymousAccessToFunction;
		anonymousAccessToFunction = newAnonymousAccessToFunction;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_FUNCTION, oldAnonymousAccessToFunction, newAnonymousAccessToFunction);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnonymousAccessToFunction(AnonymousAccessToFunction newAnonymousAccessToFunction) {
		if (newAnonymousAccessToFunction != anonymousAccessToFunction) {
			NotificationChain msgs = null;
			if (anonymousAccessToFunction != null)
				msgs = ((InternalEObject)anonymousAccessToFunction).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_FUNCTION, null, msgs);
			if (newAnonymousAccessToFunction != null)
				msgs = ((InternalEObject)newAnonymousAccessToFunction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_FUNCTION, null, msgs);
			msgs = basicSetAnonymousAccessToFunction(newAnonymousAccessToFunction, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_FUNCTION, newAnonymousAccessToFunction, newAnonymousAccessToFunction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnonymousAccessToProtectedFunction getAnonymousAccessToProtectedFunction() {
		return anonymousAccessToProtectedFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnonymousAccessToProtectedFunction(AnonymousAccessToProtectedFunction newAnonymousAccessToProtectedFunction, NotificationChain msgs) {
		AnonymousAccessToProtectedFunction oldAnonymousAccessToProtectedFunction = anonymousAccessToProtectedFunction;
		anonymousAccessToProtectedFunction = newAnonymousAccessToProtectedFunction;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION, oldAnonymousAccessToProtectedFunction, newAnonymousAccessToProtectedFunction);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnonymousAccessToProtectedFunction(AnonymousAccessToProtectedFunction newAnonymousAccessToProtectedFunction) {
		if (newAnonymousAccessToProtectedFunction != anonymousAccessToProtectedFunction) {
			NotificationChain msgs = null;
			if (anonymousAccessToProtectedFunction != null)
				msgs = ((InternalEObject)anonymousAccessToProtectedFunction).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION, null, msgs);
			if (newAnonymousAccessToProtectedFunction != null)
				msgs = ((InternalEObject)newAnonymousAccessToProtectedFunction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION, null, msgs);
			msgs = basicSetAnonymousAccessToProtectedFunction(newAnonymousAccessToProtectedFunction, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION, newAnonymousAccessToProtectedFunction, newAnonymousAccessToProtectedFunction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrivateTypeDefinition getPrivateTypeDefinition() {
		return privateTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrivateTypeDefinition(PrivateTypeDefinition newPrivateTypeDefinition, NotificationChain msgs) {
		PrivateTypeDefinition oldPrivateTypeDefinition = privateTypeDefinition;
		privateTypeDefinition = newPrivateTypeDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PRIVATE_TYPE_DEFINITION, oldPrivateTypeDefinition, newPrivateTypeDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrivateTypeDefinition(PrivateTypeDefinition newPrivateTypeDefinition) {
		if (newPrivateTypeDefinition != privateTypeDefinition) {
			NotificationChain msgs = null;
			if (privateTypeDefinition != null)
				msgs = ((InternalEObject)privateTypeDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PRIVATE_TYPE_DEFINITION, null, msgs);
			if (newPrivateTypeDefinition != null)
				msgs = ((InternalEObject)newPrivateTypeDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PRIVATE_TYPE_DEFINITION, null, msgs);
			msgs = basicSetPrivateTypeDefinition(newPrivateTypeDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PRIVATE_TYPE_DEFINITION, newPrivateTypeDefinition, newPrivateTypeDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaggedPrivateTypeDefinition getTaggedPrivateTypeDefinition() {
		return taggedPrivateTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTaggedPrivateTypeDefinition(TaggedPrivateTypeDefinition newTaggedPrivateTypeDefinition, NotificationChain msgs) {
		TaggedPrivateTypeDefinition oldTaggedPrivateTypeDefinition = taggedPrivateTypeDefinition;
		taggedPrivateTypeDefinition = newTaggedPrivateTypeDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__TAGGED_PRIVATE_TYPE_DEFINITION, oldTaggedPrivateTypeDefinition, newTaggedPrivateTypeDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaggedPrivateTypeDefinition(TaggedPrivateTypeDefinition newTaggedPrivateTypeDefinition) {
		if (newTaggedPrivateTypeDefinition != taggedPrivateTypeDefinition) {
			NotificationChain msgs = null;
			if (taggedPrivateTypeDefinition != null)
				msgs = ((InternalEObject)taggedPrivateTypeDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__TAGGED_PRIVATE_TYPE_DEFINITION, null, msgs);
			if (newTaggedPrivateTypeDefinition != null)
				msgs = ((InternalEObject)newTaggedPrivateTypeDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__TAGGED_PRIVATE_TYPE_DEFINITION, null, msgs);
			msgs = basicSetTaggedPrivateTypeDefinition(newTaggedPrivateTypeDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__TAGGED_PRIVATE_TYPE_DEFINITION, newTaggedPrivateTypeDefinition, newTaggedPrivateTypeDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrivateExtensionDefinition getPrivateExtensionDefinition() {
		return privateExtensionDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrivateExtensionDefinition(PrivateExtensionDefinition newPrivateExtensionDefinition, NotificationChain msgs) {
		PrivateExtensionDefinition oldPrivateExtensionDefinition = privateExtensionDefinition;
		privateExtensionDefinition = newPrivateExtensionDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PRIVATE_EXTENSION_DEFINITION, oldPrivateExtensionDefinition, newPrivateExtensionDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrivateExtensionDefinition(PrivateExtensionDefinition newPrivateExtensionDefinition) {
		if (newPrivateExtensionDefinition != privateExtensionDefinition) {
			NotificationChain msgs = null;
			if (privateExtensionDefinition != null)
				msgs = ((InternalEObject)privateExtensionDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PRIVATE_EXTENSION_DEFINITION, null, msgs);
			if (newPrivateExtensionDefinition != null)
				msgs = ((InternalEObject)newPrivateExtensionDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PRIVATE_EXTENSION_DEFINITION, null, msgs);
			msgs = basicSetPrivateExtensionDefinition(newPrivateExtensionDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PRIVATE_EXTENSION_DEFINITION, newPrivateExtensionDefinition, newPrivateExtensionDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskDefinition getTaskDefinition() {
		return taskDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTaskDefinition(TaskDefinition newTaskDefinition, NotificationChain msgs) {
		TaskDefinition oldTaskDefinition = taskDefinition;
		taskDefinition = newTaskDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__TASK_DEFINITION, oldTaskDefinition, newTaskDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaskDefinition(TaskDefinition newTaskDefinition) {
		if (newTaskDefinition != taskDefinition) {
			NotificationChain msgs = null;
			if (taskDefinition != null)
				msgs = ((InternalEObject)taskDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__TASK_DEFINITION, null, msgs);
			if (newTaskDefinition != null)
				msgs = ((InternalEObject)newTaskDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__TASK_DEFINITION, null, msgs);
			msgs = basicSetTaskDefinition(newTaskDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__TASK_DEFINITION, newTaskDefinition, newTaskDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProtectedDefinition getProtectedDefinition() {
		return protectedDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProtectedDefinition(ProtectedDefinition newProtectedDefinition, NotificationChain msgs) {
		ProtectedDefinition oldProtectedDefinition = protectedDefinition;
		protectedDefinition = newProtectedDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PROTECTED_DEFINITION, oldProtectedDefinition, newProtectedDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProtectedDefinition(ProtectedDefinition newProtectedDefinition) {
		if (newProtectedDefinition != protectedDefinition) {
			NotificationChain msgs = null;
			if (protectedDefinition != null)
				msgs = ((InternalEObject)protectedDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PROTECTED_DEFINITION, null, msgs);
			if (newProtectedDefinition != null)
				msgs = ((InternalEObject)newProtectedDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PROTECTED_DEFINITION, null, msgs);
			msgs = basicSetProtectedDefinition(newProtectedDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PROTECTED_DEFINITION, newProtectedDefinition, newProtectedDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalPrivateTypeDefinition getFormalPrivateTypeDefinition() {
		return formalPrivateTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalPrivateTypeDefinition(FormalPrivateTypeDefinition newFormalPrivateTypeDefinition, NotificationChain msgs) {
		FormalPrivateTypeDefinition oldFormalPrivateTypeDefinition = formalPrivateTypeDefinition;
		formalPrivateTypeDefinition = newFormalPrivateTypeDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_PRIVATE_TYPE_DEFINITION, oldFormalPrivateTypeDefinition, newFormalPrivateTypeDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalPrivateTypeDefinition(FormalPrivateTypeDefinition newFormalPrivateTypeDefinition) {
		if (newFormalPrivateTypeDefinition != formalPrivateTypeDefinition) {
			NotificationChain msgs = null;
			if (formalPrivateTypeDefinition != null)
				msgs = ((InternalEObject)formalPrivateTypeDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_PRIVATE_TYPE_DEFINITION, null, msgs);
			if (newFormalPrivateTypeDefinition != null)
				msgs = ((InternalEObject)newFormalPrivateTypeDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_PRIVATE_TYPE_DEFINITION, null, msgs);
			msgs = basicSetFormalPrivateTypeDefinition(newFormalPrivateTypeDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_PRIVATE_TYPE_DEFINITION, newFormalPrivateTypeDefinition, newFormalPrivateTypeDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalTaggedPrivateTypeDefinition getFormalTaggedPrivateTypeDefinition() {
		return formalTaggedPrivateTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalTaggedPrivateTypeDefinition(FormalTaggedPrivateTypeDefinition newFormalTaggedPrivateTypeDefinition, NotificationChain msgs) {
		FormalTaggedPrivateTypeDefinition oldFormalTaggedPrivateTypeDefinition = formalTaggedPrivateTypeDefinition;
		formalTaggedPrivateTypeDefinition = newFormalTaggedPrivateTypeDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION, oldFormalTaggedPrivateTypeDefinition, newFormalTaggedPrivateTypeDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalTaggedPrivateTypeDefinition(FormalTaggedPrivateTypeDefinition newFormalTaggedPrivateTypeDefinition) {
		if (newFormalTaggedPrivateTypeDefinition != formalTaggedPrivateTypeDefinition) {
			NotificationChain msgs = null;
			if (formalTaggedPrivateTypeDefinition != null)
				msgs = ((InternalEObject)formalTaggedPrivateTypeDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION, null, msgs);
			if (newFormalTaggedPrivateTypeDefinition != null)
				msgs = ((InternalEObject)newFormalTaggedPrivateTypeDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION, null, msgs);
			msgs = basicSetFormalTaggedPrivateTypeDefinition(newFormalTaggedPrivateTypeDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION, newFormalTaggedPrivateTypeDefinition, newFormalTaggedPrivateTypeDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalDerivedTypeDefinition getFormalDerivedTypeDefinition() {
		return formalDerivedTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalDerivedTypeDefinition(FormalDerivedTypeDefinition newFormalDerivedTypeDefinition, NotificationChain msgs) {
		FormalDerivedTypeDefinition oldFormalDerivedTypeDefinition = formalDerivedTypeDefinition;
		formalDerivedTypeDefinition = newFormalDerivedTypeDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_DERIVED_TYPE_DEFINITION, oldFormalDerivedTypeDefinition, newFormalDerivedTypeDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalDerivedTypeDefinition(FormalDerivedTypeDefinition newFormalDerivedTypeDefinition) {
		if (newFormalDerivedTypeDefinition != formalDerivedTypeDefinition) {
			NotificationChain msgs = null;
			if (formalDerivedTypeDefinition != null)
				msgs = ((InternalEObject)formalDerivedTypeDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_DERIVED_TYPE_DEFINITION, null, msgs);
			if (newFormalDerivedTypeDefinition != null)
				msgs = ((InternalEObject)newFormalDerivedTypeDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_DERIVED_TYPE_DEFINITION, null, msgs);
			msgs = basicSetFormalDerivedTypeDefinition(newFormalDerivedTypeDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_DERIVED_TYPE_DEFINITION, newFormalDerivedTypeDefinition, newFormalDerivedTypeDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalDiscreteTypeDefinition getFormalDiscreteTypeDefinition() {
		return formalDiscreteTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalDiscreteTypeDefinition(FormalDiscreteTypeDefinition newFormalDiscreteTypeDefinition, NotificationChain msgs) {
		FormalDiscreteTypeDefinition oldFormalDiscreteTypeDefinition = formalDiscreteTypeDefinition;
		formalDiscreteTypeDefinition = newFormalDiscreteTypeDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_DISCRETE_TYPE_DEFINITION, oldFormalDiscreteTypeDefinition, newFormalDiscreteTypeDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalDiscreteTypeDefinition(FormalDiscreteTypeDefinition newFormalDiscreteTypeDefinition) {
		if (newFormalDiscreteTypeDefinition != formalDiscreteTypeDefinition) {
			NotificationChain msgs = null;
			if (formalDiscreteTypeDefinition != null)
				msgs = ((InternalEObject)formalDiscreteTypeDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_DISCRETE_TYPE_DEFINITION, null, msgs);
			if (newFormalDiscreteTypeDefinition != null)
				msgs = ((InternalEObject)newFormalDiscreteTypeDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_DISCRETE_TYPE_DEFINITION, null, msgs);
			msgs = basicSetFormalDiscreteTypeDefinition(newFormalDiscreteTypeDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_DISCRETE_TYPE_DEFINITION, newFormalDiscreteTypeDefinition, newFormalDiscreteTypeDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalSignedIntegerTypeDefinition getFormalSignedIntegerTypeDefinition() {
		return formalSignedIntegerTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalSignedIntegerTypeDefinition(FormalSignedIntegerTypeDefinition newFormalSignedIntegerTypeDefinition, NotificationChain msgs) {
		FormalSignedIntegerTypeDefinition oldFormalSignedIntegerTypeDefinition = formalSignedIntegerTypeDefinition;
		formalSignedIntegerTypeDefinition = newFormalSignedIntegerTypeDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_SIGNED_INTEGER_TYPE_DEFINITION, oldFormalSignedIntegerTypeDefinition, newFormalSignedIntegerTypeDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalSignedIntegerTypeDefinition(FormalSignedIntegerTypeDefinition newFormalSignedIntegerTypeDefinition) {
		if (newFormalSignedIntegerTypeDefinition != formalSignedIntegerTypeDefinition) {
			NotificationChain msgs = null;
			if (formalSignedIntegerTypeDefinition != null)
				msgs = ((InternalEObject)formalSignedIntegerTypeDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_SIGNED_INTEGER_TYPE_DEFINITION, null, msgs);
			if (newFormalSignedIntegerTypeDefinition != null)
				msgs = ((InternalEObject)newFormalSignedIntegerTypeDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_SIGNED_INTEGER_TYPE_DEFINITION, null, msgs);
			msgs = basicSetFormalSignedIntegerTypeDefinition(newFormalSignedIntegerTypeDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_SIGNED_INTEGER_TYPE_DEFINITION, newFormalSignedIntegerTypeDefinition, newFormalSignedIntegerTypeDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalModularTypeDefinition getFormalModularTypeDefinition() {
		return formalModularTypeDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalModularTypeDefinition(FormalModularTypeDefinition newFormalModularTypeDefinition, NotificationChain msgs) {
		FormalModularTypeDefinition oldFormalModularTypeDefinition = formalModularTypeDefinition;
		formalModularTypeDefinition = newFormalModularTypeDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_MODULAR_TYPE_DEFINITION, oldFormalModularTypeDefinition, newFormalModularTypeDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalModularTypeDefinition(FormalModularTypeDefinition newFormalModularTypeDefinition) {
		if (newFormalModularTypeDefinition != formalModularTypeDefinition) {
			NotificationChain msgs = null;
			if (formalModularTypeDefinition != null)
				msgs = ((InternalEObject)formalModularTypeDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_MODULAR_TYPE_DEFINITION, null, msgs);
			if (newFormalModularTypeDefinition != null)
				msgs = ((InternalEObject)newFormalModularTypeDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_MODULAR_TYPE_DEFINITION, null, msgs);
			msgs = basicSetFormalModularTypeDefinition(newFormalModularTypeDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_MODULAR_TYPE_DEFINITION, newFormalModularTypeDefinition, newFormalModularTypeDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalFloatingPointDefinition getFormalFloatingPointDefinition() {
		return formalFloatingPointDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalFloatingPointDefinition(FormalFloatingPointDefinition newFormalFloatingPointDefinition, NotificationChain msgs) {
		FormalFloatingPointDefinition oldFormalFloatingPointDefinition = formalFloatingPointDefinition;
		formalFloatingPointDefinition = newFormalFloatingPointDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_FLOATING_POINT_DEFINITION, oldFormalFloatingPointDefinition, newFormalFloatingPointDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalFloatingPointDefinition(FormalFloatingPointDefinition newFormalFloatingPointDefinition) {
		if (newFormalFloatingPointDefinition != formalFloatingPointDefinition) {
			NotificationChain msgs = null;
			if (formalFloatingPointDefinition != null)
				msgs = ((InternalEObject)formalFloatingPointDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_FLOATING_POINT_DEFINITION, null, msgs);
			if (newFormalFloatingPointDefinition != null)
				msgs = ((InternalEObject)newFormalFloatingPointDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_FLOATING_POINT_DEFINITION, null, msgs);
			msgs = basicSetFormalFloatingPointDefinition(newFormalFloatingPointDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_FLOATING_POINT_DEFINITION, newFormalFloatingPointDefinition, newFormalFloatingPointDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalOrdinaryFixedPointDefinition getFormalOrdinaryFixedPointDefinition() {
		return formalOrdinaryFixedPointDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalOrdinaryFixedPointDefinition(FormalOrdinaryFixedPointDefinition newFormalOrdinaryFixedPointDefinition, NotificationChain msgs) {
		FormalOrdinaryFixedPointDefinition oldFormalOrdinaryFixedPointDefinition = formalOrdinaryFixedPointDefinition;
		formalOrdinaryFixedPointDefinition = newFormalOrdinaryFixedPointDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_ORDINARY_FIXED_POINT_DEFINITION, oldFormalOrdinaryFixedPointDefinition, newFormalOrdinaryFixedPointDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalOrdinaryFixedPointDefinition(FormalOrdinaryFixedPointDefinition newFormalOrdinaryFixedPointDefinition) {
		if (newFormalOrdinaryFixedPointDefinition != formalOrdinaryFixedPointDefinition) {
			NotificationChain msgs = null;
			if (formalOrdinaryFixedPointDefinition != null)
				msgs = ((InternalEObject)formalOrdinaryFixedPointDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_ORDINARY_FIXED_POINT_DEFINITION, null, msgs);
			if (newFormalOrdinaryFixedPointDefinition != null)
				msgs = ((InternalEObject)newFormalOrdinaryFixedPointDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_ORDINARY_FIXED_POINT_DEFINITION, null, msgs);
			msgs = basicSetFormalOrdinaryFixedPointDefinition(newFormalOrdinaryFixedPointDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_ORDINARY_FIXED_POINT_DEFINITION, newFormalOrdinaryFixedPointDefinition, newFormalOrdinaryFixedPointDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalDecimalFixedPointDefinition getFormalDecimalFixedPointDefinition() {
		return formalDecimalFixedPointDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalDecimalFixedPointDefinition(FormalDecimalFixedPointDefinition newFormalDecimalFixedPointDefinition, NotificationChain msgs) {
		FormalDecimalFixedPointDefinition oldFormalDecimalFixedPointDefinition = formalDecimalFixedPointDefinition;
		formalDecimalFixedPointDefinition = newFormalDecimalFixedPointDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_DECIMAL_FIXED_POINT_DEFINITION, oldFormalDecimalFixedPointDefinition, newFormalDecimalFixedPointDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalDecimalFixedPointDefinition(FormalDecimalFixedPointDefinition newFormalDecimalFixedPointDefinition) {
		if (newFormalDecimalFixedPointDefinition != formalDecimalFixedPointDefinition) {
			NotificationChain msgs = null;
			if (formalDecimalFixedPointDefinition != null)
				msgs = ((InternalEObject)formalDecimalFixedPointDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_DECIMAL_FIXED_POINT_DEFINITION, null, msgs);
			if (newFormalDecimalFixedPointDefinition != null)
				msgs = ((InternalEObject)newFormalDecimalFixedPointDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_DECIMAL_FIXED_POINT_DEFINITION, null, msgs);
			msgs = basicSetFormalDecimalFixedPointDefinition(newFormalDecimalFixedPointDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_DECIMAL_FIXED_POINT_DEFINITION, newFormalDecimalFixedPointDefinition, newFormalDecimalFixedPointDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalOrdinaryInterface getFormalOrdinaryInterface() {
		return formalOrdinaryInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalOrdinaryInterface(FormalOrdinaryInterface newFormalOrdinaryInterface, NotificationChain msgs) {
		FormalOrdinaryInterface oldFormalOrdinaryInterface = formalOrdinaryInterface;
		formalOrdinaryInterface = newFormalOrdinaryInterface;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_ORDINARY_INTERFACE, oldFormalOrdinaryInterface, newFormalOrdinaryInterface);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalOrdinaryInterface(FormalOrdinaryInterface newFormalOrdinaryInterface) {
		if (newFormalOrdinaryInterface != formalOrdinaryInterface) {
			NotificationChain msgs = null;
			if (formalOrdinaryInterface != null)
				msgs = ((InternalEObject)formalOrdinaryInterface).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_ORDINARY_INTERFACE, null, msgs);
			if (newFormalOrdinaryInterface != null)
				msgs = ((InternalEObject)newFormalOrdinaryInterface).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_ORDINARY_INTERFACE, null, msgs);
			msgs = basicSetFormalOrdinaryInterface(newFormalOrdinaryInterface, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_ORDINARY_INTERFACE, newFormalOrdinaryInterface, newFormalOrdinaryInterface));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalLimitedInterface getFormalLimitedInterface() {
		return formalLimitedInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalLimitedInterface(FormalLimitedInterface newFormalLimitedInterface, NotificationChain msgs) {
		FormalLimitedInterface oldFormalLimitedInterface = formalLimitedInterface;
		formalLimitedInterface = newFormalLimitedInterface;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_LIMITED_INTERFACE, oldFormalLimitedInterface, newFormalLimitedInterface);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalLimitedInterface(FormalLimitedInterface newFormalLimitedInterface) {
		if (newFormalLimitedInterface != formalLimitedInterface) {
			NotificationChain msgs = null;
			if (formalLimitedInterface != null)
				msgs = ((InternalEObject)formalLimitedInterface).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_LIMITED_INTERFACE, null, msgs);
			if (newFormalLimitedInterface != null)
				msgs = ((InternalEObject)newFormalLimitedInterface).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_LIMITED_INTERFACE, null, msgs);
			msgs = basicSetFormalLimitedInterface(newFormalLimitedInterface, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_LIMITED_INTERFACE, newFormalLimitedInterface, newFormalLimitedInterface));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalTaskInterface getFormalTaskInterface() {
		return formalTaskInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalTaskInterface(FormalTaskInterface newFormalTaskInterface, NotificationChain msgs) {
		FormalTaskInterface oldFormalTaskInterface = formalTaskInterface;
		formalTaskInterface = newFormalTaskInterface;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_TASK_INTERFACE, oldFormalTaskInterface, newFormalTaskInterface);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalTaskInterface(FormalTaskInterface newFormalTaskInterface) {
		if (newFormalTaskInterface != formalTaskInterface) {
			NotificationChain msgs = null;
			if (formalTaskInterface != null)
				msgs = ((InternalEObject)formalTaskInterface).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_TASK_INTERFACE, null, msgs);
			if (newFormalTaskInterface != null)
				msgs = ((InternalEObject)newFormalTaskInterface).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_TASK_INTERFACE, null, msgs);
			msgs = basicSetFormalTaskInterface(newFormalTaskInterface, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_TASK_INTERFACE, newFormalTaskInterface, newFormalTaskInterface));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalProtectedInterface getFormalProtectedInterface() {
		return formalProtectedInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalProtectedInterface(FormalProtectedInterface newFormalProtectedInterface, NotificationChain msgs) {
		FormalProtectedInterface oldFormalProtectedInterface = formalProtectedInterface;
		formalProtectedInterface = newFormalProtectedInterface;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_PROTECTED_INTERFACE, oldFormalProtectedInterface, newFormalProtectedInterface);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalProtectedInterface(FormalProtectedInterface newFormalProtectedInterface) {
		if (newFormalProtectedInterface != formalProtectedInterface) {
			NotificationChain msgs = null;
			if (formalProtectedInterface != null)
				msgs = ((InternalEObject)formalProtectedInterface).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_PROTECTED_INTERFACE, null, msgs);
			if (newFormalProtectedInterface != null)
				msgs = ((InternalEObject)newFormalProtectedInterface).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_PROTECTED_INTERFACE, null, msgs);
			msgs = basicSetFormalProtectedInterface(newFormalProtectedInterface, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_PROTECTED_INTERFACE, newFormalProtectedInterface, newFormalProtectedInterface));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalSynchronizedInterface getFormalSynchronizedInterface() {
		return formalSynchronizedInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalSynchronizedInterface(FormalSynchronizedInterface newFormalSynchronizedInterface, NotificationChain msgs) {
		FormalSynchronizedInterface oldFormalSynchronizedInterface = formalSynchronizedInterface;
		formalSynchronizedInterface = newFormalSynchronizedInterface;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_SYNCHRONIZED_INTERFACE, oldFormalSynchronizedInterface, newFormalSynchronizedInterface);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalSynchronizedInterface(FormalSynchronizedInterface newFormalSynchronizedInterface) {
		if (newFormalSynchronizedInterface != formalSynchronizedInterface) {
			NotificationChain msgs = null;
			if (formalSynchronizedInterface != null)
				msgs = ((InternalEObject)formalSynchronizedInterface).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_SYNCHRONIZED_INTERFACE, null, msgs);
			if (newFormalSynchronizedInterface != null)
				msgs = ((InternalEObject)newFormalSynchronizedInterface).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_SYNCHRONIZED_INTERFACE, null, msgs);
			msgs = basicSetFormalSynchronizedInterface(newFormalSynchronizedInterface, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_SYNCHRONIZED_INTERFACE, newFormalSynchronizedInterface, newFormalSynchronizedInterface));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalUnconstrainedArrayDefinition getFormalUnconstrainedArrayDefinition() {
		return formalUnconstrainedArrayDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalUnconstrainedArrayDefinition(FormalUnconstrainedArrayDefinition newFormalUnconstrainedArrayDefinition, NotificationChain msgs) {
		FormalUnconstrainedArrayDefinition oldFormalUnconstrainedArrayDefinition = formalUnconstrainedArrayDefinition;
		formalUnconstrainedArrayDefinition = newFormalUnconstrainedArrayDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_UNCONSTRAINED_ARRAY_DEFINITION, oldFormalUnconstrainedArrayDefinition, newFormalUnconstrainedArrayDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalUnconstrainedArrayDefinition(FormalUnconstrainedArrayDefinition newFormalUnconstrainedArrayDefinition) {
		if (newFormalUnconstrainedArrayDefinition != formalUnconstrainedArrayDefinition) {
			NotificationChain msgs = null;
			if (formalUnconstrainedArrayDefinition != null)
				msgs = ((InternalEObject)formalUnconstrainedArrayDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_UNCONSTRAINED_ARRAY_DEFINITION, null, msgs);
			if (newFormalUnconstrainedArrayDefinition != null)
				msgs = ((InternalEObject)newFormalUnconstrainedArrayDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_UNCONSTRAINED_ARRAY_DEFINITION, null, msgs);
			msgs = basicSetFormalUnconstrainedArrayDefinition(newFormalUnconstrainedArrayDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_UNCONSTRAINED_ARRAY_DEFINITION, newFormalUnconstrainedArrayDefinition, newFormalUnconstrainedArrayDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalConstrainedArrayDefinition getFormalConstrainedArrayDefinition() {
		return formalConstrainedArrayDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalConstrainedArrayDefinition(FormalConstrainedArrayDefinition newFormalConstrainedArrayDefinition, NotificationChain msgs) {
		FormalConstrainedArrayDefinition oldFormalConstrainedArrayDefinition = formalConstrainedArrayDefinition;
		formalConstrainedArrayDefinition = newFormalConstrainedArrayDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_CONSTRAINED_ARRAY_DEFINITION, oldFormalConstrainedArrayDefinition, newFormalConstrainedArrayDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalConstrainedArrayDefinition(FormalConstrainedArrayDefinition newFormalConstrainedArrayDefinition) {
		if (newFormalConstrainedArrayDefinition != formalConstrainedArrayDefinition) {
			NotificationChain msgs = null;
			if (formalConstrainedArrayDefinition != null)
				msgs = ((InternalEObject)formalConstrainedArrayDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_CONSTRAINED_ARRAY_DEFINITION, null, msgs);
			if (newFormalConstrainedArrayDefinition != null)
				msgs = ((InternalEObject)newFormalConstrainedArrayDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_CONSTRAINED_ARRAY_DEFINITION, null, msgs);
			msgs = basicSetFormalConstrainedArrayDefinition(newFormalConstrainedArrayDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_CONSTRAINED_ARRAY_DEFINITION, newFormalConstrainedArrayDefinition, newFormalConstrainedArrayDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalPoolSpecificAccessToVariable getFormalPoolSpecificAccessToVariable() {
		return formalPoolSpecificAccessToVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalPoolSpecificAccessToVariable(FormalPoolSpecificAccessToVariable newFormalPoolSpecificAccessToVariable, NotificationChain msgs) {
		FormalPoolSpecificAccessToVariable oldFormalPoolSpecificAccessToVariable = formalPoolSpecificAccessToVariable;
		formalPoolSpecificAccessToVariable = newFormalPoolSpecificAccessToVariable;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE, oldFormalPoolSpecificAccessToVariable, newFormalPoolSpecificAccessToVariable);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalPoolSpecificAccessToVariable(FormalPoolSpecificAccessToVariable newFormalPoolSpecificAccessToVariable) {
		if (newFormalPoolSpecificAccessToVariable != formalPoolSpecificAccessToVariable) {
			NotificationChain msgs = null;
			if (formalPoolSpecificAccessToVariable != null)
				msgs = ((InternalEObject)formalPoolSpecificAccessToVariable).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE, null, msgs);
			if (newFormalPoolSpecificAccessToVariable != null)
				msgs = ((InternalEObject)newFormalPoolSpecificAccessToVariable).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE, null, msgs);
			msgs = basicSetFormalPoolSpecificAccessToVariable(newFormalPoolSpecificAccessToVariable, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE, newFormalPoolSpecificAccessToVariable, newFormalPoolSpecificAccessToVariable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalAccessToVariable getFormalAccessToVariable() {
		return formalAccessToVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalAccessToVariable(FormalAccessToVariable newFormalAccessToVariable, NotificationChain msgs) {
		FormalAccessToVariable oldFormalAccessToVariable = formalAccessToVariable;
		formalAccessToVariable = newFormalAccessToVariable;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_VARIABLE, oldFormalAccessToVariable, newFormalAccessToVariable);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalAccessToVariable(FormalAccessToVariable newFormalAccessToVariable) {
		if (newFormalAccessToVariable != formalAccessToVariable) {
			NotificationChain msgs = null;
			if (formalAccessToVariable != null)
				msgs = ((InternalEObject)formalAccessToVariable).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_VARIABLE, null, msgs);
			if (newFormalAccessToVariable != null)
				msgs = ((InternalEObject)newFormalAccessToVariable).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_VARIABLE, null, msgs);
			msgs = basicSetFormalAccessToVariable(newFormalAccessToVariable, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_VARIABLE, newFormalAccessToVariable, newFormalAccessToVariable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalAccessToConstant getFormalAccessToConstant() {
		return formalAccessToConstant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalAccessToConstant(FormalAccessToConstant newFormalAccessToConstant, NotificationChain msgs) {
		FormalAccessToConstant oldFormalAccessToConstant = formalAccessToConstant;
		formalAccessToConstant = newFormalAccessToConstant;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_CONSTANT, oldFormalAccessToConstant, newFormalAccessToConstant);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalAccessToConstant(FormalAccessToConstant newFormalAccessToConstant) {
		if (newFormalAccessToConstant != formalAccessToConstant) {
			NotificationChain msgs = null;
			if (formalAccessToConstant != null)
				msgs = ((InternalEObject)formalAccessToConstant).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_CONSTANT, null, msgs);
			if (newFormalAccessToConstant != null)
				msgs = ((InternalEObject)newFormalAccessToConstant).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_CONSTANT, null, msgs);
			msgs = basicSetFormalAccessToConstant(newFormalAccessToConstant, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_CONSTANT, newFormalAccessToConstant, newFormalAccessToConstant));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalAccessToProcedure getFormalAccessToProcedure() {
		return formalAccessToProcedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalAccessToProcedure(FormalAccessToProcedure newFormalAccessToProcedure, NotificationChain msgs) {
		FormalAccessToProcedure oldFormalAccessToProcedure = formalAccessToProcedure;
		formalAccessToProcedure = newFormalAccessToProcedure;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROCEDURE, oldFormalAccessToProcedure, newFormalAccessToProcedure);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalAccessToProcedure(FormalAccessToProcedure newFormalAccessToProcedure) {
		if (newFormalAccessToProcedure != formalAccessToProcedure) {
			NotificationChain msgs = null;
			if (formalAccessToProcedure != null)
				msgs = ((InternalEObject)formalAccessToProcedure).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROCEDURE, null, msgs);
			if (newFormalAccessToProcedure != null)
				msgs = ((InternalEObject)newFormalAccessToProcedure).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROCEDURE, null, msgs);
			msgs = basicSetFormalAccessToProcedure(newFormalAccessToProcedure, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROCEDURE, newFormalAccessToProcedure, newFormalAccessToProcedure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalAccessToProtectedProcedure getFormalAccessToProtectedProcedure() {
		return formalAccessToProtectedProcedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalAccessToProtectedProcedure(FormalAccessToProtectedProcedure newFormalAccessToProtectedProcedure, NotificationChain msgs) {
		FormalAccessToProtectedProcedure oldFormalAccessToProtectedProcedure = formalAccessToProtectedProcedure;
		formalAccessToProtectedProcedure = newFormalAccessToProtectedProcedure;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROTECTED_PROCEDURE, oldFormalAccessToProtectedProcedure, newFormalAccessToProtectedProcedure);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalAccessToProtectedProcedure(FormalAccessToProtectedProcedure newFormalAccessToProtectedProcedure) {
		if (newFormalAccessToProtectedProcedure != formalAccessToProtectedProcedure) {
			NotificationChain msgs = null;
			if (formalAccessToProtectedProcedure != null)
				msgs = ((InternalEObject)formalAccessToProtectedProcedure).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROTECTED_PROCEDURE, null, msgs);
			if (newFormalAccessToProtectedProcedure != null)
				msgs = ((InternalEObject)newFormalAccessToProtectedProcedure).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROTECTED_PROCEDURE, null, msgs);
			msgs = basicSetFormalAccessToProtectedProcedure(newFormalAccessToProtectedProcedure, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROTECTED_PROCEDURE, newFormalAccessToProtectedProcedure, newFormalAccessToProtectedProcedure));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalAccessToFunction getFormalAccessToFunction() {
		return formalAccessToFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalAccessToFunction(FormalAccessToFunction newFormalAccessToFunction, NotificationChain msgs) {
		FormalAccessToFunction oldFormalAccessToFunction = formalAccessToFunction;
		formalAccessToFunction = newFormalAccessToFunction;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_FUNCTION, oldFormalAccessToFunction, newFormalAccessToFunction);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalAccessToFunction(FormalAccessToFunction newFormalAccessToFunction) {
		if (newFormalAccessToFunction != formalAccessToFunction) {
			NotificationChain msgs = null;
			if (formalAccessToFunction != null)
				msgs = ((InternalEObject)formalAccessToFunction).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_FUNCTION, null, msgs);
			if (newFormalAccessToFunction != null)
				msgs = ((InternalEObject)newFormalAccessToFunction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_FUNCTION, null, msgs);
			msgs = basicSetFormalAccessToFunction(newFormalAccessToFunction, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_FUNCTION, newFormalAccessToFunction, newFormalAccessToFunction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalAccessToProtectedFunction getFormalAccessToProtectedFunction() {
		return formalAccessToProtectedFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalAccessToProtectedFunction(FormalAccessToProtectedFunction newFormalAccessToProtectedFunction, NotificationChain msgs) {
		FormalAccessToProtectedFunction oldFormalAccessToProtectedFunction = formalAccessToProtectedFunction;
		formalAccessToProtectedFunction = newFormalAccessToProtectedFunction;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROTECTED_FUNCTION, oldFormalAccessToProtectedFunction, newFormalAccessToProtectedFunction);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalAccessToProtectedFunction(FormalAccessToProtectedFunction newFormalAccessToProtectedFunction) {
		if (newFormalAccessToProtectedFunction != formalAccessToProtectedFunction) {
			NotificationChain msgs = null;
			if (formalAccessToProtectedFunction != null)
				msgs = ((InternalEObject)formalAccessToProtectedFunction).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROTECTED_FUNCTION, null, msgs);
			if (newFormalAccessToProtectedFunction != null)
				msgs = ((InternalEObject)newFormalAccessToProtectedFunction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROTECTED_FUNCTION, null, msgs);
			msgs = basicSetFormalAccessToProtectedFunction(newFormalAccessToProtectedFunction, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROTECTED_FUNCTION, newFormalAccessToProtectedFunction, newFormalAccessToProtectedFunction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AspectSpecification getAspectSpecification() {
		return aspectSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAspectSpecification(AspectSpecification newAspectSpecification, NotificationChain msgs) {
		AspectSpecification oldAspectSpecification = aspectSpecification;
		aspectSpecification = newAspectSpecification;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ASPECT_SPECIFICATION, oldAspectSpecification, newAspectSpecification);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAspectSpecification(AspectSpecification newAspectSpecification) {
		if (newAspectSpecification != aspectSpecification) {
			NotificationChain msgs = null;
			if (aspectSpecification != null)
				msgs = ((InternalEObject)aspectSpecification).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ASPECT_SPECIFICATION, null, msgs);
			if (newAspectSpecification != null)
				msgs = ((InternalEObject)newAspectSpecification).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ASPECT_SPECIFICATION, null, msgs);
			msgs = basicSetAspectSpecification(newAspectSpecification, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ASPECT_SPECIFICATION, newAspectSpecification, newAspectSpecification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Identifier getIdentifier() {
		return identifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIdentifier(Identifier newIdentifier, NotificationChain msgs) {
		Identifier oldIdentifier = identifier;
		identifier = newIdentifier;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__IDENTIFIER, oldIdentifier, newIdentifier);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdentifier(Identifier newIdentifier) {
		if (newIdentifier != identifier) {
			NotificationChain msgs = null;
			if (identifier != null)
				msgs = ((InternalEObject)identifier).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__IDENTIFIER, null, msgs);
			if (newIdentifier != null)
				msgs = ((InternalEObject)newIdentifier).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__IDENTIFIER, null, msgs);
			msgs = basicSetIdentifier(newIdentifier, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__IDENTIFIER, newIdentifier, newIdentifier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SelectedComponent getSelectedComponent() {
		return selectedComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSelectedComponent(SelectedComponent newSelectedComponent, NotificationChain msgs) {
		SelectedComponent oldSelectedComponent = selectedComponent;
		selectedComponent = newSelectedComponent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__SELECTED_COMPONENT, oldSelectedComponent, newSelectedComponent);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelectedComponent(SelectedComponent newSelectedComponent) {
		if (newSelectedComponent != selectedComponent) {
			NotificationChain msgs = null;
			if (selectedComponent != null)
				msgs = ((InternalEObject)selectedComponent).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__SELECTED_COMPONENT, null, msgs);
			if (newSelectedComponent != null)
				msgs = ((InternalEObject)newSelectedComponent).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__SELECTED_COMPONENT, null, msgs);
			msgs = basicSetSelectedComponent(newSelectedComponent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__SELECTED_COMPONENT, newSelectedComponent, newSelectedComponent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseAttribute getBaseAttribute() {
		return baseAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBaseAttribute(BaseAttribute newBaseAttribute, NotificationChain msgs) {
		BaseAttribute oldBaseAttribute = baseAttribute;
		baseAttribute = newBaseAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__BASE_ATTRIBUTE, oldBaseAttribute, newBaseAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBaseAttribute(BaseAttribute newBaseAttribute) {
		if (newBaseAttribute != baseAttribute) {
			NotificationChain msgs = null;
			if (baseAttribute != null)
				msgs = ((InternalEObject)baseAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__BASE_ATTRIBUTE, null, msgs);
			if (newBaseAttribute != null)
				msgs = ((InternalEObject)newBaseAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__BASE_ATTRIBUTE, null, msgs);
			msgs = basicSetBaseAttribute(newBaseAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__BASE_ATTRIBUTE, newBaseAttribute, newBaseAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassAttribute getClassAttribute() {
		return classAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClassAttribute(ClassAttribute newClassAttribute, NotificationChain msgs) {
		ClassAttribute oldClassAttribute = classAttribute;
		classAttribute = newClassAttribute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__CLASS_ATTRIBUTE, oldClassAttribute, newClassAttribute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassAttribute(ClassAttribute newClassAttribute) {
		if (newClassAttribute != classAttribute) {
			NotificationChain msgs = null;
			if (classAttribute != null)
				msgs = ((InternalEObject)classAttribute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__CLASS_ATTRIBUTE, null, msgs);
			if (newClassAttribute != null)
				msgs = ((InternalEObject)newClassAttribute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__CLASS_ATTRIBUTE, null, msgs);
			msgs = basicSetClassAttribute(newClassAttribute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__CLASS_ATTRIBUTE, newClassAttribute, newClassAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Comment getComment() {
		return comment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComment(Comment newComment, NotificationChain msgs) {
		Comment oldComment = comment;
		comment = newComment;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__COMMENT, oldComment, newComment);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComment(Comment newComment) {
		if (newComment != comment) {
			NotificationChain msgs = null;
			if (comment != null)
				msgs = ((InternalEObject)comment).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__COMMENT, null, msgs);
			if (newComment != null)
				msgs = ((InternalEObject)newComment).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__COMMENT, null, msgs);
			msgs = basicSetComment(newComment, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__COMMENT, newComment, newComment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AllCallsRemotePragma getAllCallsRemotePragma() {
		return allCallsRemotePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAllCallsRemotePragma(AllCallsRemotePragma newAllCallsRemotePragma, NotificationChain msgs) {
		AllCallsRemotePragma oldAllCallsRemotePragma = allCallsRemotePragma;
		allCallsRemotePragma = newAllCallsRemotePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ALL_CALLS_REMOTE_PRAGMA, oldAllCallsRemotePragma, newAllCallsRemotePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAllCallsRemotePragma(AllCallsRemotePragma newAllCallsRemotePragma) {
		if (newAllCallsRemotePragma != allCallsRemotePragma) {
			NotificationChain msgs = null;
			if (allCallsRemotePragma != null)
				msgs = ((InternalEObject)allCallsRemotePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ALL_CALLS_REMOTE_PRAGMA, null, msgs);
			if (newAllCallsRemotePragma != null)
				msgs = ((InternalEObject)newAllCallsRemotePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ALL_CALLS_REMOTE_PRAGMA, null, msgs);
			msgs = basicSetAllCallsRemotePragma(newAllCallsRemotePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ALL_CALLS_REMOTE_PRAGMA, newAllCallsRemotePragma, newAllCallsRemotePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AsynchronousPragma getAsynchronousPragma() {
		return asynchronousPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAsynchronousPragma(AsynchronousPragma newAsynchronousPragma, NotificationChain msgs) {
		AsynchronousPragma oldAsynchronousPragma = asynchronousPragma;
		asynchronousPragma = newAsynchronousPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ASYNCHRONOUS_PRAGMA, oldAsynchronousPragma, newAsynchronousPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAsynchronousPragma(AsynchronousPragma newAsynchronousPragma) {
		if (newAsynchronousPragma != asynchronousPragma) {
			NotificationChain msgs = null;
			if (asynchronousPragma != null)
				msgs = ((InternalEObject)asynchronousPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ASYNCHRONOUS_PRAGMA, null, msgs);
			if (newAsynchronousPragma != null)
				msgs = ((InternalEObject)newAsynchronousPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ASYNCHRONOUS_PRAGMA, null, msgs);
			msgs = basicSetAsynchronousPragma(newAsynchronousPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ASYNCHRONOUS_PRAGMA, newAsynchronousPragma, newAsynchronousPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtomicPragma getAtomicPragma() {
		return atomicPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAtomicPragma(AtomicPragma newAtomicPragma, NotificationChain msgs) {
		AtomicPragma oldAtomicPragma = atomicPragma;
		atomicPragma = newAtomicPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ATOMIC_PRAGMA, oldAtomicPragma, newAtomicPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAtomicPragma(AtomicPragma newAtomicPragma) {
		if (newAtomicPragma != atomicPragma) {
			NotificationChain msgs = null;
			if (atomicPragma != null)
				msgs = ((InternalEObject)atomicPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ATOMIC_PRAGMA, null, msgs);
			if (newAtomicPragma != null)
				msgs = ((InternalEObject)newAtomicPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ATOMIC_PRAGMA, null, msgs);
			msgs = basicSetAtomicPragma(newAtomicPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ATOMIC_PRAGMA, newAtomicPragma, newAtomicPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtomicComponentsPragma getAtomicComponentsPragma() {
		return atomicComponentsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAtomicComponentsPragma(AtomicComponentsPragma newAtomicComponentsPragma, NotificationChain msgs) {
		AtomicComponentsPragma oldAtomicComponentsPragma = atomicComponentsPragma;
		atomicComponentsPragma = newAtomicComponentsPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ATOMIC_COMPONENTS_PRAGMA, oldAtomicComponentsPragma, newAtomicComponentsPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAtomicComponentsPragma(AtomicComponentsPragma newAtomicComponentsPragma) {
		if (newAtomicComponentsPragma != atomicComponentsPragma) {
			NotificationChain msgs = null;
			if (atomicComponentsPragma != null)
				msgs = ((InternalEObject)atomicComponentsPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ATOMIC_COMPONENTS_PRAGMA, null, msgs);
			if (newAtomicComponentsPragma != null)
				msgs = ((InternalEObject)newAtomicComponentsPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ATOMIC_COMPONENTS_PRAGMA, null, msgs);
			msgs = basicSetAtomicComponentsPragma(newAtomicComponentsPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ATOMIC_COMPONENTS_PRAGMA, newAtomicComponentsPragma, newAtomicComponentsPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttachHandlerPragma getAttachHandlerPragma() {
		return attachHandlerPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAttachHandlerPragma(AttachHandlerPragma newAttachHandlerPragma, NotificationChain msgs) {
		AttachHandlerPragma oldAttachHandlerPragma = attachHandlerPragma;
		attachHandlerPragma = newAttachHandlerPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ATTACH_HANDLER_PRAGMA, oldAttachHandlerPragma, newAttachHandlerPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttachHandlerPragma(AttachHandlerPragma newAttachHandlerPragma) {
		if (newAttachHandlerPragma != attachHandlerPragma) {
			NotificationChain msgs = null;
			if (attachHandlerPragma != null)
				msgs = ((InternalEObject)attachHandlerPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ATTACH_HANDLER_PRAGMA, null, msgs);
			if (newAttachHandlerPragma != null)
				msgs = ((InternalEObject)newAttachHandlerPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ATTACH_HANDLER_PRAGMA, null, msgs);
			msgs = basicSetAttachHandlerPragma(newAttachHandlerPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ATTACH_HANDLER_PRAGMA, newAttachHandlerPragma, newAttachHandlerPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlledPragma getControlledPragma() {
		return controlledPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetControlledPragma(ControlledPragma newControlledPragma, NotificationChain msgs) {
		ControlledPragma oldControlledPragma = controlledPragma;
		controlledPragma = newControlledPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__CONTROLLED_PRAGMA, oldControlledPragma, newControlledPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setControlledPragma(ControlledPragma newControlledPragma) {
		if (newControlledPragma != controlledPragma) {
			NotificationChain msgs = null;
			if (controlledPragma != null)
				msgs = ((InternalEObject)controlledPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__CONTROLLED_PRAGMA, null, msgs);
			if (newControlledPragma != null)
				msgs = ((InternalEObject)newControlledPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__CONTROLLED_PRAGMA, null, msgs);
			msgs = basicSetControlledPragma(newControlledPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__CONTROLLED_PRAGMA, newControlledPragma, newControlledPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConventionPragma getConventionPragma() {
		return conventionPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConventionPragma(ConventionPragma newConventionPragma, NotificationChain msgs) {
		ConventionPragma oldConventionPragma = conventionPragma;
		conventionPragma = newConventionPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__CONVENTION_PRAGMA, oldConventionPragma, newConventionPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConventionPragma(ConventionPragma newConventionPragma) {
		if (newConventionPragma != conventionPragma) {
			NotificationChain msgs = null;
			if (conventionPragma != null)
				msgs = ((InternalEObject)conventionPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__CONVENTION_PRAGMA, null, msgs);
			if (newConventionPragma != null)
				msgs = ((InternalEObject)newConventionPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__CONVENTION_PRAGMA, null, msgs);
			msgs = basicSetConventionPragma(newConventionPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__CONVENTION_PRAGMA, newConventionPragma, newConventionPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscardNamesPragma getDiscardNamesPragma() {
		return discardNamesPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscardNamesPragma(DiscardNamesPragma newDiscardNamesPragma, NotificationChain msgs) {
		DiscardNamesPragma oldDiscardNamesPragma = discardNamesPragma;
		discardNamesPragma = newDiscardNamesPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DISCARD_NAMES_PRAGMA, oldDiscardNamesPragma, newDiscardNamesPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscardNamesPragma(DiscardNamesPragma newDiscardNamesPragma) {
		if (newDiscardNamesPragma != discardNamesPragma) {
			NotificationChain msgs = null;
			if (discardNamesPragma != null)
				msgs = ((InternalEObject)discardNamesPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DISCARD_NAMES_PRAGMA, null, msgs);
			if (newDiscardNamesPragma != null)
				msgs = ((InternalEObject)newDiscardNamesPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DISCARD_NAMES_PRAGMA, null, msgs);
			msgs = basicSetDiscardNamesPragma(newDiscardNamesPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DISCARD_NAMES_PRAGMA, newDiscardNamesPragma, newDiscardNamesPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElaboratePragma getElaboratePragma() {
		return elaboratePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElaboratePragma(ElaboratePragma newElaboratePragma, NotificationChain msgs) {
		ElaboratePragma oldElaboratePragma = elaboratePragma;
		elaboratePragma = newElaboratePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ELABORATE_PRAGMA, oldElaboratePragma, newElaboratePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElaboratePragma(ElaboratePragma newElaboratePragma) {
		if (newElaboratePragma != elaboratePragma) {
			NotificationChain msgs = null;
			if (elaboratePragma != null)
				msgs = ((InternalEObject)elaboratePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ELABORATE_PRAGMA, null, msgs);
			if (newElaboratePragma != null)
				msgs = ((InternalEObject)newElaboratePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ELABORATE_PRAGMA, null, msgs);
			msgs = basicSetElaboratePragma(newElaboratePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ELABORATE_PRAGMA, newElaboratePragma, newElaboratePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElaborateAllPragma getElaborateAllPragma() {
		return elaborateAllPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElaborateAllPragma(ElaborateAllPragma newElaborateAllPragma, NotificationChain msgs) {
		ElaborateAllPragma oldElaborateAllPragma = elaborateAllPragma;
		elaborateAllPragma = newElaborateAllPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ELABORATE_ALL_PRAGMA, oldElaborateAllPragma, newElaborateAllPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElaborateAllPragma(ElaborateAllPragma newElaborateAllPragma) {
		if (newElaborateAllPragma != elaborateAllPragma) {
			NotificationChain msgs = null;
			if (elaborateAllPragma != null)
				msgs = ((InternalEObject)elaborateAllPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ELABORATE_ALL_PRAGMA, null, msgs);
			if (newElaborateAllPragma != null)
				msgs = ((InternalEObject)newElaborateAllPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ELABORATE_ALL_PRAGMA, null, msgs);
			msgs = basicSetElaborateAllPragma(newElaborateAllPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ELABORATE_ALL_PRAGMA, newElaborateAllPragma, newElaborateAllPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElaborateBodyPragma getElaborateBodyPragma() {
		return elaborateBodyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElaborateBodyPragma(ElaborateBodyPragma newElaborateBodyPragma, NotificationChain msgs) {
		ElaborateBodyPragma oldElaborateBodyPragma = elaborateBodyPragma;
		elaborateBodyPragma = newElaborateBodyPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ELABORATE_BODY_PRAGMA, oldElaborateBodyPragma, newElaborateBodyPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElaborateBodyPragma(ElaborateBodyPragma newElaborateBodyPragma) {
		if (newElaborateBodyPragma != elaborateBodyPragma) {
			NotificationChain msgs = null;
			if (elaborateBodyPragma != null)
				msgs = ((InternalEObject)elaborateBodyPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ELABORATE_BODY_PRAGMA, null, msgs);
			if (newElaborateBodyPragma != null)
				msgs = ((InternalEObject)newElaborateBodyPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ELABORATE_BODY_PRAGMA, null, msgs);
			msgs = basicSetElaborateBodyPragma(newElaborateBodyPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ELABORATE_BODY_PRAGMA, newElaborateBodyPragma, newElaborateBodyPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExportPragma getExportPragma() {
		return exportPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExportPragma(ExportPragma newExportPragma, NotificationChain msgs) {
		ExportPragma oldExportPragma = exportPragma;
		exportPragma = newExportPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__EXPORT_PRAGMA, oldExportPragma, newExportPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExportPragma(ExportPragma newExportPragma) {
		if (newExportPragma != exportPragma) {
			NotificationChain msgs = null;
			if (exportPragma != null)
				msgs = ((InternalEObject)exportPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__EXPORT_PRAGMA, null, msgs);
			if (newExportPragma != null)
				msgs = ((InternalEObject)newExportPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__EXPORT_PRAGMA, null, msgs);
			msgs = basicSetExportPragma(newExportPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__EXPORT_PRAGMA, newExportPragma, newExportPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImportPragma getImportPragma() {
		return importPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImportPragma(ImportPragma newImportPragma, NotificationChain msgs) {
		ImportPragma oldImportPragma = importPragma;
		importPragma = newImportPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__IMPORT_PRAGMA, oldImportPragma, newImportPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImportPragma(ImportPragma newImportPragma) {
		if (newImportPragma != importPragma) {
			NotificationChain msgs = null;
			if (importPragma != null)
				msgs = ((InternalEObject)importPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__IMPORT_PRAGMA, null, msgs);
			if (newImportPragma != null)
				msgs = ((InternalEObject)newImportPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__IMPORT_PRAGMA, null, msgs);
			msgs = basicSetImportPragma(newImportPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__IMPORT_PRAGMA, newImportPragma, newImportPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InlinePragma getInlinePragma() {
		return inlinePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInlinePragma(InlinePragma newInlinePragma, NotificationChain msgs) {
		InlinePragma oldInlinePragma = inlinePragma;
		inlinePragma = newInlinePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__INLINE_PRAGMA, oldInlinePragma, newInlinePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInlinePragma(InlinePragma newInlinePragma) {
		if (newInlinePragma != inlinePragma) {
			NotificationChain msgs = null;
			if (inlinePragma != null)
				msgs = ((InternalEObject)inlinePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__INLINE_PRAGMA, null, msgs);
			if (newInlinePragma != null)
				msgs = ((InternalEObject)newInlinePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__INLINE_PRAGMA, null, msgs);
			msgs = basicSetInlinePragma(newInlinePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__INLINE_PRAGMA, newInlinePragma, newInlinePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InspectionPointPragma getInspectionPointPragma() {
		return inspectionPointPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInspectionPointPragma(InspectionPointPragma newInspectionPointPragma, NotificationChain msgs) {
		InspectionPointPragma oldInspectionPointPragma = inspectionPointPragma;
		inspectionPointPragma = newInspectionPointPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__INSPECTION_POINT_PRAGMA, oldInspectionPointPragma, newInspectionPointPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInspectionPointPragma(InspectionPointPragma newInspectionPointPragma) {
		if (newInspectionPointPragma != inspectionPointPragma) {
			NotificationChain msgs = null;
			if (inspectionPointPragma != null)
				msgs = ((InternalEObject)inspectionPointPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__INSPECTION_POINT_PRAGMA, null, msgs);
			if (newInspectionPointPragma != null)
				msgs = ((InternalEObject)newInspectionPointPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__INSPECTION_POINT_PRAGMA, null, msgs);
			msgs = basicSetInspectionPointPragma(newInspectionPointPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__INSPECTION_POINT_PRAGMA, newInspectionPointPragma, newInspectionPointPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterruptHandlerPragma getInterruptHandlerPragma() {
		return interruptHandlerPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInterruptHandlerPragma(InterruptHandlerPragma newInterruptHandlerPragma, NotificationChain msgs) {
		InterruptHandlerPragma oldInterruptHandlerPragma = interruptHandlerPragma;
		interruptHandlerPragma = newInterruptHandlerPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__INTERRUPT_HANDLER_PRAGMA, oldInterruptHandlerPragma, newInterruptHandlerPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterruptHandlerPragma(InterruptHandlerPragma newInterruptHandlerPragma) {
		if (newInterruptHandlerPragma != interruptHandlerPragma) {
			NotificationChain msgs = null;
			if (interruptHandlerPragma != null)
				msgs = ((InternalEObject)interruptHandlerPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__INTERRUPT_HANDLER_PRAGMA, null, msgs);
			if (newInterruptHandlerPragma != null)
				msgs = ((InternalEObject)newInterruptHandlerPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__INTERRUPT_HANDLER_PRAGMA, null, msgs);
			msgs = basicSetInterruptHandlerPragma(newInterruptHandlerPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__INTERRUPT_HANDLER_PRAGMA, newInterruptHandlerPragma, newInterruptHandlerPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterruptPriorityPragma getInterruptPriorityPragma() {
		return interruptPriorityPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInterruptPriorityPragma(InterruptPriorityPragma newInterruptPriorityPragma, NotificationChain msgs) {
		InterruptPriorityPragma oldInterruptPriorityPragma = interruptPriorityPragma;
		interruptPriorityPragma = newInterruptPriorityPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__INTERRUPT_PRIORITY_PRAGMA, oldInterruptPriorityPragma, newInterruptPriorityPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterruptPriorityPragma(InterruptPriorityPragma newInterruptPriorityPragma) {
		if (newInterruptPriorityPragma != interruptPriorityPragma) {
			NotificationChain msgs = null;
			if (interruptPriorityPragma != null)
				msgs = ((InternalEObject)interruptPriorityPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__INTERRUPT_PRIORITY_PRAGMA, null, msgs);
			if (newInterruptPriorityPragma != null)
				msgs = ((InternalEObject)newInterruptPriorityPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__INTERRUPT_PRIORITY_PRAGMA, null, msgs);
			msgs = basicSetInterruptPriorityPragma(newInterruptPriorityPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__INTERRUPT_PRIORITY_PRAGMA, newInterruptPriorityPragma, newInterruptPriorityPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkerOptionsPragma getLinkerOptionsPragma() {
		return linkerOptionsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLinkerOptionsPragma(LinkerOptionsPragma newLinkerOptionsPragma, NotificationChain msgs) {
		LinkerOptionsPragma oldLinkerOptionsPragma = linkerOptionsPragma;
		linkerOptionsPragma = newLinkerOptionsPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__LINKER_OPTIONS_PRAGMA, oldLinkerOptionsPragma, newLinkerOptionsPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLinkerOptionsPragma(LinkerOptionsPragma newLinkerOptionsPragma) {
		if (newLinkerOptionsPragma != linkerOptionsPragma) {
			NotificationChain msgs = null;
			if (linkerOptionsPragma != null)
				msgs = ((InternalEObject)linkerOptionsPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__LINKER_OPTIONS_PRAGMA, null, msgs);
			if (newLinkerOptionsPragma != null)
				msgs = ((InternalEObject)newLinkerOptionsPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__LINKER_OPTIONS_PRAGMA, null, msgs);
			msgs = basicSetLinkerOptionsPragma(newLinkerOptionsPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__LINKER_OPTIONS_PRAGMA, newLinkerOptionsPragma, newLinkerOptionsPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ListPragma getListPragma() {
		return listPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetListPragma(ListPragma newListPragma, NotificationChain msgs) {
		ListPragma oldListPragma = listPragma;
		listPragma = newListPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__LIST_PRAGMA, oldListPragma, newListPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setListPragma(ListPragma newListPragma) {
		if (newListPragma != listPragma) {
			NotificationChain msgs = null;
			if (listPragma != null)
				msgs = ((InternalEObject)listPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__LIST_PRAGMA, null, msgs);
			if (newListPragma != null)
				msgs = ((InternalEObject)newListPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__LIST_PRAGMA, null, msgs);
			msgs = basicSetListPragma(newListPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__LIST_PRAGMA, newListPragma, newListPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LockingPolicyPragma getLockingPolicyPragma() {
		return lockingPolicyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLockingPolicyPragma(LockingPolicyPragma newLockingPolicyPragma, NotificationChain msgs) {
		LockingPolicyPragma oldLockingPolicyPragma = lockingPolicyPragma;
		lockingPolicyPragma = newLockingPolicyPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__LOCKING_POLICY_PRAGMA, oldLockingPolicyPragma, newLockingPolicyPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLockingPolicyPragma(LockingPolicyPragma newLockingPolicyPragma) {
		if (newLockingPolicyPragma != lockingPolicyPragma) {
			NotificationChain msgs = null;
			if (lockingPolicyPragma != null)
				msgs = ((InternalEObject)lockingPolicyPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__LOCKING_POLICY_PRAGMA, null, msgs);
			if (newLockingPolicyPragma != null)
				msgs = ((InternalEObject)newLockingPolicyPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__LOCKING_POLICY_PRAGMA, null, msgs);
			msgs = basicSetLockingPolicyPragma(newLockingPolicyPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__LOCKING_POLICY_PRAGMA, newLockingPolicyPragma, newLockingPolicyPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NormalizeScalarsPragma getNormalizeScalarsPragma() {
		return normalizeScalarsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNormalizeScalarsPragma(NormalizeScalarsPragma newNormalizeScalarsPragma, NotificationChain msgs) {
		NormalizeScalarsPragma oldNormalizeScalarsPragma = normalizeScalarsPragma;
		normalizeScalarsPragma = newNormalizeScalarsPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__NORMALIZE_SCALARS_PRAGMA, oldNormalizeScalarsPragma, newNormalizeScalarsPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNormalizeScalarsPragma(NormalizeScalarsPragma newNormalizeScalarsPragma) {
		if (newNormalizeScalarsPragma != normalizeScalarsPragma) {
			NotificationChain msgs = null;
			if (normalizeScalarsPragma != null)
				msgs = ((InternalEObject)normalizeScalarsPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__NORMALIZE_SCALARS_PRAGMA, null, msgs);
			if (newNormalizeScalarsPragma != null)
				msgs = ((InternalEObject)newNormalizeScalarsPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__NORMALIZE_SCALARS_PRAGMA, null, msgs);
			msgs = basicSetNormalizeScalarsPragma(newNormalizeScalarsPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__NORMALIZE_SCALARS_PRAGMA, newNormalizeScalarsPragma, newNormalizeScalarsPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OptimizePragma getOptimizePragma() {
		return optimizePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOptimizePragma(OptimizePragma newOptimizePragma, NotificationChain msgs) {
		OptimizePragma oldOptimizePragma = optimizePragma;
		optimizePragma = newOptimizePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__OPTIMIZE_PRAGMA, oldOptimizePragma, newOptimizePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOptimizePragma(OptimizePragma newOptimizePragma) {
		if (newOptimizePragma != optimizePragma) {
			NotificationChain msgs = null;
			if (optimizePragma != null)
				msgs = ((InternalEObject)optimizePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__OPTIMIZE_PRAGMA, null, msgs);
			if (newOptimizePragma != null)
				msgs = ((InternalEObject)newOptimizePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__OPTIMIZE_PRAGMA, null, msgs);
			msgs = basicSetOptimizePragma(newOptimizePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__OPTIMIZE_PRAGMA, newOptimizePragma, newOptimizePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackPragma getPackPragma() {
		return packPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPackPragma(PackPragma newPackPragma, NotificationChain msgs) {
		PackPragma oldPackPragma = packPragma;
		packPragma = newPackPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PACK_PRAGMA, oldPackPragma, newPackPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPackPragma(PackPragma newPackPragma) {
		if (newPackPragma != packPragma) {
			NotificationChain msgs = null;
			if (packPragma != null)
				msgs = ((InternalEObject)packPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PACK_PRAGMA, null, msgs);
			if (newPackPragma != null)
				msgs = ((InternalEObject)newPackPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PACK_PRAGMA, null, msgs);
			msgs = basicSetPackPragma(newPackPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PACK_PRAGMA, newPackPragma, newPackPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PagePragma getPagePragma() {
		return pagePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPagePragma(PagePragma newPagePragma, NotificationChain msgs) {
		PagePragma oldPagePragma = pagePragma;
		pagePragma = newPagePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PAGE_PRAGMA, oldPagePragma, newPagePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPagePragma(PagePragma newPagePragma) {
		if (newPagePragma != pagePragma) {
			NotificationChain msgs = null;
			if (pagePragma != null)
				msgs = ((InternalEObject)pagePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PAGE_PRAGMA, null, msgs);
			if (newPagePragma != null)
				msgs = ((InternalEObject)newPagePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PAGE_PRAGMA, null, msgs);
			msgs = basicSetPagePragma(newPagePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PAGE_PRAGMA, newPagePragma, newPagePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PreelaboratePragma getPreelaboratePragma() {
		return preelaboratePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPreelaboratePragma(PreelaboratePragma newPreelaboratePragma, NotificationChain msgs) {
		PreelaboratePragma oldPreelaboratePragma = preelaboratePragma;
		preelaboratePragma = newPreelaboratePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PREELABORATE_PRAGMA, oldPreelaboratePragma, newPreelaboratePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreelaboratePragma(PreelaboratePragma newPreelaboratePragma) {
		if (newPreelaboratePragma != preelaboratePragma) {
			NotificationChain msgs = null;
			if (preelaboratePragma != null)
				msgs = ((InternalEObject)preelaboratePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PREELABORATE_PRAGMA, null, msgs);
			if (newPreelaboratePragma != null)
				msgs = ((InternalEObject)newPreelaboratePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PREELABORATE_PRAGMA, null, msgs);
			msgs = basicSetPreelaboratePragma(newPreelaboratePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PREELABORATE_PRAGMA, newPreelaboratePragma, newPreelaboratePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PriorityPragma getPriorityPragma() {
		return priorityPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPriorityPragma(PriorityPragma newPriorityPragma, NotificationChain msgs) {
		PriorityPragma oldPriorityPragma = priorityPragma;
		priorityPragma = newPriorityPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PRIORITY_PRAGMA, oldPriorityPragma, newPriorityPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPriorityPragma(PriorityPragma newPriorityPragma) {
		if (newPriorityPragma != priorityPragma) {
			NotificationChain msgs = null;
			if (priorityPragma != null)
				msgs = ((InternalEObject)priorityPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PRIORITY_PRAGMA, null, msgs);
			if (newPriorityPragma != null)
				msgs = ((InternalEObject)newPriorityPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PRIORITY_PRAGMA, null, msgs);
			msgs = basicSetPriorityPragma(newPriorityPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PRIORITY_PRAGMA, newPriorityPragma, newPriorityPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PurePragma getPurePragma() {
		return purePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPurePragma(PurePragma newPurePragma, NotificationChain msgs) {
		PurePragma oldPurePragma = purePragma;
		purePragma = newPurePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PURE_PRAGMA, oldPurePragma, newPurePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPurePragma(PurePragma newPurePragma) {
		if (newPurePragma != purePragma) {
			NotificationChain msgs = null;
			if (purePragma != null)
				msgs = ((InternalEObject)purePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PURE_PRAGMA, null, msgs);
			if (newPurePragma != null)
				msgs = ((InternalEObject)newPurePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PURE_PRAGMA, null, msgs);
			msgs = basicSetPurePragma(newPurePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PURE_PRAGMA, newPurePragma, newPurePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QueuingPolicyPragma getQueuingPolicyPragma() {
		return queuingPolicyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetQueuingPolicyPragma(QueuingPolicyPragma newQueuingPolicyPragma, NotificationChain msgs) {
		QueuingPolicyPragma oldQueuingPolicyPragma = queuingPolicyPragma;
		queuingPolicyPragma = newQueuingPolicyPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__QUEUING_POLICY_PRAGMA, oldQueuingPolicyPragma, newQueuingPolicyPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQueuingPolicyPragma(QueuingPolicyPragma newQueuingPolicyPragma) {
		if (newQueuingPolicyPragma != queuingPolicyPragma) {
			NotificationChain msgs = null;
			if (queuingPolicyPragma != null)
				msgs = ((InternalEObject)queuingPolicyPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__QUEUING_POLICY_PRAGMA, null, msgs);
			if (newQueuingPolicyPragma != null)
				msgs = ((InternalEObject)newQueuingPolicyPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__QUEUING_POLICY_PRAGMA, null, msgs);
			msgs = basicSetQueuingPolicyPragma(newQueuingPolicyPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__QUEUING_POLICY_PRAGMA, newQueuingPolicyPragma, newQueuingPolicyPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RemoteCallInterfacePragma getRemoteCallInterfacePragma() {
		return remoteCallInterfacePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRemoteCallInterfacePragma(RemoteCallInterfacePragma newRemoteCallInterfacePragma, NotificationChain msgs) {
		RemoteCallInterfacePragma oldRemoteCallInterfacePragma = remoteCallInterfacePragma;
		remoteCallInterfacePragma = newRemoteCallInterfacePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA, oldRemoteCallInterfacePragma, newRemoteCallInterfacePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRemoteCallInterfacePragma(RemoteCallInterfacePragma newRemoteCallInterfacePragma) {
		if (newRemoteCallInterfacePragma != remoteCallInterfacePragma) {
			NotificationChain msgs = null;
			if (remoteCallInterfacePragma != null)
				msgs = ((InternalEObject)remoteCallInterfacePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA, null, msgs);
			if (newRemoteCallInterfacePragma != null)
				msgs = ((InternalEObject)newRemoteCallInterfacePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA, null, msgs);
			msgs = basicSetRemoteCallInterfacePragma(newRemoteCallInterfacePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA, newRemoteCallInterfacePragma, newRemoteCallInterfacePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RemoteTypesPragma getRemoteTypesPragma() {
		return remoteTypesPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRemoteTypesPragma(RemoteTypesPragma newRemoteTypesPragma, NotificationChain msgs) {
		RemoteTypesPragma oldRemoteTypesPragma = remoteTypesPragma;
		remoteTypesPragma = newRemoteTypesPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__REMOTE_TYPES_PRAGMA, oldRemoteTypesPragma, newRemoteTypesPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRemoteTypesPragma(RemoteTypesPragma newRemoteTypesPragma) {
		if (newRemoteTypesPragma != remoteTypesPragma) {
			NotificationChain msgs = null;
			if (remoteTypesPragma != null)
				msgs = ((InternalEObject)remoteTypesPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__REMOTE_TYPES_PRAGMA, null, msgs);
			if (newRemoteTypesPragma != null)
				msgs = ((InternalEObject)newRemoteTypesPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__REMOTE_TYPES_PRAGMA, null, msgs);
			msgs = basicSetRemoteTypesPragma(newRemoteTypesPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__REMOTE_TYPES_PRAGMA, newRemoteTypesPragma, newRemoteTypesPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RestrictionsPragma getRestrictionsPragma() {
		return restrictionsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRestrictionsPragma(RestrictionsPragma newRestrictionsPragma, NotificationChain msgs) {
		RestrictionsPragma oldRestrictionsPragma = restrictionsPragma;
		restrictionsPragma = newRestrictionsPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__RESTRICTIONS_PRAGMA, oldRestrictionsPragma, newRestrictionsPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRestrictionsPragma(RestrictionsPragma newRestrictionsPragma) {
		if (newRestrictionsPragma != restrictionsPragma) {
			NotificationChain msgs = null;
			if (restrictionsPragma != null)
				msgs = ((InternalEObject)restrictionsPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__RESTRICTIONS_PRAGMA, null, msgs);
			if (newRestrictionsPragma != null)
				msgs = ((InternalEObject)newRestrictionsPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__RESTRICTIONS_PRAGMA, null, msgs);
			msgs = basicSetRestrictionsPragma(newRestrictionsPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__RESTRICTIONS_PRAGMA, newRestrictionsPragma, newRestrictionsPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReviewablePragma getReviewablePragma() {
		return reviewablePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReviewablePragma(ReviewablePragma newReviewablePragma, NotificationChain msgs) {
		ReviewablePragma oldReviewablePragma = reviewablePragma;
		reviewablePragma = newReviewablePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__REVIEWABLE_PRAGMA, oldReviewablePragma, newReviewablePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReviewablePragma(ReviewablePragma newReviewablePragma) {
		if (newReviewablePragma != reviewablePragma) {
			NotificationChain msgs = null;
			if (reviewablePragma != null)
				msgs = ((InternalEObject)reviewablePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__REVIEWABLE_PRAGMA, null, msgs);
			if (newReviewablePragma != null)
				msgs = ((InternalEObject)newReviewablePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__REVIEWABLE_PRAGMA, null, msgs);
			msgs = basicSetReviewablePragma(newReviewablePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__REVIEWABLE_PRAGMA, newReviewablePragma, newReviewablePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SharedPassivePragma getSharedPassivePragma() {
		return sharedPassivePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSharedPassivePragma(SharedPassivePragma newSharedPassivePragma, NotificationChain msgs) {
		SharedPassivePragma oldSharedPassivePragma = sharedPassivePragma;
		sharedPassivePragma = newSharedPassivePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__SHARED_PASSIVE_PRAGMA, oldSharedPassivePragma, newSharedPassivePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSharedPassivePragma(SharedPassivePragma newSharedPassivePragma) {
		if (newSharedPassivePragma != sharedPassivePragma) {
			NotificationChain msgs = null;
			if (sharedPassivePragma != null)
				msgs = ((InternalEObject)sharedPassivePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__SHARED_PASSIVE_PRAGMA, null, msgs);
			if (newSharedPassivePragma != null)
				msgs = ((InternalEObject)newSharedPassivePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__SHARED_PASSIVE_PRAGMA, null, msgs);
			msgs = basicSetSharedPassivePragma(newSharedPassivePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__SHARED_PASSIVE_PRAGMA, newSharedPassivePragma, newSharedPassivePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StorageSizePragma getStorageSizePragma() {
		return storageSizePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStorageSizePragma(StorageSizePragma newStorageSizePragma, NotificationChain msgs) {
		StorageSizePragma oldStorageSizePragma = storageSizePragma;
		storageSizePragma = newStorageSizePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__STORAGE_SIZE_PRAGMA, oldStorageSizePragma, newStorageSizePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStorageSizePragma(StorageSizePragma newStorageSizePragma) {
		if (newStorageSizePragma != storageSizePragma) {
			NotificationChain msgs = null;
			if (storageSizePragma != null)
				msgs = ((InternalEObject)storageSizePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__STORAGE_SIZE_PRAGMA, null, msgs);
			if (newStorageSizePragma != null)
				msgs = ((InternalEObject)newStorageSizePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__STORAGE_SIZE_PRAGMA, null, msgs);
			msgs = basicSetStorageSizePragma(newStorageSizePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__STORAGE_SIZE_PRAGMA, newStorageSizePragma, newStorageSizePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SuppressPragma getSuppressPragma() {
		return suppressPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSuppressPragma(SuppressPragma newSuppressPragma, NotificationChain msgs) {
		SuppressPragma oldSuppressPragma = suppressPragma;
		suppressPragma = newSuppressPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__SUPPRESS_PRAGMA, oldSuppressPragma, newSuppressPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuppressPragma(SuppressPragma newSuppressPragma) {
		if (newSuppressPragma != suppressPragma) {
			NotificationChain msgs = null;
			if (suppressPragma != null)
				msgs = ((InternalEObject)suppressPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__SUPPRESS_PRAGMA, null, msgs);
			if (newSuppressPragma != null)
				msgs = ((InternalEObject)newSuppressPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__SUPPRESS_PRAGMA, null, msgs);
			msgs = basicSetSuppressPragma(newSuppressPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__SUPPRESS_PRAGMA, newSuppressPragma, newSuppressPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskDispatchingPolicyPragma getTaskDispatchingPolicyPragma() {
		return taskDispatchingPolicyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTaskDispatchingPolicyPragma(TaskDispatchingPolicyPragma newTaskDispatchingPolicyPragma, NotificationChain msgs) {
		TaskDispatchingPolicyPragma oldTaskDispatchingPolicyPragma = taskDispatchingPolicyPragma;
		taskDispatchingPolicyPragma = newTaskDispatchingPolicyPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA, oldTaskDispatchingPolicyPragma, newTaskDispatchingPolicyPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaskDispatchingPolicyPragma(TaskDispatchingPolicyPragma newTaskDispatchingPolicyPragma) {
		if (newTaskDispatchingPolicyPragma != taskDispatchingPolicyPragma) {
			NotificationChain msgs = null;
			if (taskDispatchingPolicyPragma != null)
				msgs = ((InternalEObject)taskDispatchingPolicyPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA, null, msgs);
			if (newTaskDispatchingPolicyPragma != null)
				msgs = ((InternalEObject)newTaskDispatchingPolicyPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA, null, msgs);
			msgs = basicSetTaskDispatchingPolicyPragma(newTaskDispatchingPolicyPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA, newTaskDispatchingPolicyPragma, newTaskDispatchingPolicyPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VolatilePragma getVolatilePragma() {
		return volatilePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVolatilePragma(VolatilePragma newVolatilePragma, NotificationChain msgs) {
		VolatilePragma oldVolatilePragma = volatilePragma;
		volatilePragma = newVolatilePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__VOLATILE_PRAGMA, oldVolatilePragma, newVolatilePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVolatilePragma(VolatilePragma newVolatilePragma) {
		if (newVolatilePragma != volatilePragma) {
			NotificationChain msgs = null;
			if (volatilePragma != null)
				msgs = ((InternalEObject)volatilePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__VOLATILE_PRAGMA, null, msgs);
			if (newVolatilePragma != null)
				msgs = ((InternalEObject)newVolatilePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__VOLATILE_PRAGMA, null, msgs);
			msgs = basicSetVolatilePragma(newVolatilePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__VOLATILE_PRAGMA, newVolatilePragma, newVolatilePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VolatileComponentsPragma getVolatileComponentsPragma() {
		return volatileComponentsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVolatileComponentsPragma(VolatileComponentsPragma newVolatileComponentsPragma, NotificationChain msgs) {
		VolatileComponentsPragma oldVolatileComponentsPragma = volatileComponentsPragma;
		volatileComponentsPragma = newVolatileComponentsPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__VOLATILE_COMPONENTS_PRAGMA, oldVolatileComponentsPragma, newVolatileComponentsPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVolatileComponentsPragma(VolatileComponentsPragma newVolatileComponentsPragma) {
		if (newVolatileComponentsPragma != volatileComponentsPragma) {
			NotificationChain msgs = null;
			if (volatileComponentsPragma != null)
				msgs = ((InternalEObject)volatileComponentsPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__VOLATILE_COMPONENTS_PRAGMA, null, msgs);
			if (newVolatileComponentsPragma != null)
				msgs = ((InternalEObject)newVolatileComponentsPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__VOLATILE_COMPONENTS_PRAGMA, null, msgs);
			msgs = basicSetVolatileComponentsPragma(newVolatileComponentsPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__VOLATILE_COMPONENTS_PRAGMA, newVolatileComponentsPragma, newVolatileComponentsPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertPragma getAssertPragma() {
		return assertPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssertPragma(AssertPragma newAssertPragma, NotificationChain msgs) {
		AssertPragma oldAssertPragma = assertPragma;
		assertPragma = newAssertPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ASSERT_PRAGMA, oldAssertPragma, newAssertPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssertPragma(AssertPragma newAssertPragma) {
		if (newAssertPragma != assertPragma) {
			NotificationChain msgs = null;
			if (assertPragma != null)
				msgs = ((InternalEObject)assertPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ASSERT_PRAGMA, null, msgs);
			if (newAssertPragma != null)
				msgs = ((InternalEObject)newAssertPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ASSERT_PRAGMA, null, msgs);
			msgs = basicSetAssertPragma(newAssertPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ASSERT_PRAGMA, newAssertPragma, newAssertPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertionPolicyPragma getAssertionPolicyPragma() {
		return assertionPolicyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssertionPolicyPragma(AssertionPolicyPragma newAssertionPolicyPragma, NotificationChain msgs) {
		AssertionPolicyPragma oldAssertionPolicyPragma = assertionPolicyPragma;
		assertionPolicyPragma = newAssertionPolicyPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ASSERTION_POLICY_PRAGMA, oldAssertionPolicyPragma, newAssertionPolicyPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssertionPolicyPragma(AssertionPolicyPragma newAssertionPolicyPragma) {
		if (newAssertionPolicyPragma != assertionPolicyPragma) {
			NotificationChain msgs = null;
			if (assertionPolicyPragma != null)
				msgs = ((InternalEObject)assertionPolicyPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ASSERTION_POLICY_PRAGMA, null, msgs);
			if (newAssertionPolicyPragma != null)
				msgs = ((InternalEObject)newAssertionPolicyPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__ASSERTION_POLICY_PRAGMA, null, msgs);
			msgs = basicSetAssertionPolicyPragma(newAssertionPolicyPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__ASSERTION_POLICY_PRAGMA, newAssertionPolicyPragma, newAssertionPolicyPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DetectBlockingPragma getDetectBlockingPragma() {
		return detectBlockingPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDetectBlockingPragma(DetectBlockingPragma newDetectBlockingPragma, NotificationChain msgs) {
		DetectBlockingPragma oldDetectBlockingPragma = detectBlockingPragma;
		detectBlockingPragma = newDetectBlockingPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DETECT_BLOCKING_PRAGMA, oldDetectBlockingPragma, newDetectBlockingPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDetectBlockingPragma(DetectBlockingPragma newDetectBlockingPragma) {
		if (newDetectBlockingPragma != detectBlockingPragma) {
			NotificationChain msgs = null;
			if (detectBlockingPragma != null)
				msgs = ((InternalEObject)detectBlockingPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DETECT_BLOCKING_PRAGMA, null, msgs);
			if (newDetectBlockingPragma != null)
				msgs = ((InternalEObject)newDetectBlockingPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DETECT_BLOCKING_PRAGMA, null, msgs);
			msgs = basicSetDetectBlockingPragma(newDetectBlockingPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DETECT_BLOCKING_PRAGMA, newDetectBlockingPragma, newDetectBlockingPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NoReturnPragma getNoReturnPragma() {
		return noReturnPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNoReturnPragma(NoReturnPragma newNoReturnPragma, NotificationChain msgs) {
		NoReturnPragma oldNoReturnPragma = noReturnPragma;
		noReturnPragma = newNoReturnPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__NO_RETURN_PRAGMA, oldNoReturnPragma, newNoReturnPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNoReturnPragma(NoReturnPragma newNoReturnPragma) {
		if (newNoReturnPragma != noReturnPragma) {
			NotificationChain msgs = null;
			if (noReturnPragma != null)
				msgs = ((InternalEObject)noReturnPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__NO_RETURN_PRAGMA, null, msgs);
			if (newNoReturnPragma != null)
				msgs = ((InternalEObject)newNoReturnPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__NO_RETURN_PRAGMA, null, msgs);
			msgs = basicSetNoReturnPragma(newNoReturnPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__NO_RETURN_PRAGMA, newNoReturnPragma, newNoReturnPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PartitionElaborationPolicyPragma getPartitionElaborationPolicyPragma() {
		return partitionElaborationPolicyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPartitionElaborationPolicyPragma(PartitionElaborationPolicyPragma newPartitionElaborationPolicyPragma, NotificationChain msgs) {
		PartitionElaborationPolicyPragma oldPartitionElaborationPolicyPragma = partitionElaborationPolicyPragma;
		partitionElaborationPolicyPragma = newPartitionElaborationPolicyPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA, oldPartitionElaborationPolicyPragma, newPartitionElaborationPolicyPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPartitionElaborationPolicyPragma(PartitionElaborationPolicyPragma newPartitionElaborationPolicyPragma) {
		if (newPartitionElaborationPolicyPragma != partitionElaborationPolicyPragma) {
			NotificationChain msgs = null;
			if (partitionElaborationPolicyPragma != null)
				msgs = ((InternalEObject)partitionElaborationPolicyPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA, null, msgs);
			if (newPartitionElaborationPolicyPragma != null)
				msgs = ((InternalEObject)newPartitionElaborationPolicyPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA, null, msgs);
			msgs = basicSetPartitionElaborationPolicyPragma(newPartitionElaborationPolicyPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA, newPartitionElaborationPolicyPragma, newPartitionElaborationPolicyPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PreelaborableInitializationPragma getPreelaborableInitializationPragma() {
		return preelaborableInitializationPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPreelaborableInitializationPragma(PreelaborableInitializationPragma newPreelaborableInitializationPragma, NotificationChain msgs) {
		PreelaborableInitializationPragma oldPreelaborableInitializationPragma = preelaborableInitializationPragma;
		preelaborableInitializationPragma = newPreelaborableInitializationPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA, oldPreelaborableInitializationPragma, newPreelaborableInitializationPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreelaborableInitializationPragma(PreelaborableInitializationPragma newPreelaborableInitializationPragma) {
		if (newPreelaborableInitializationPragma != preelaborableInitializationPragma) {
			NotificationChain msgs = null;
			if (preelaborableInitializationPragma != null)
				msgs = ((InternalEObject)preelaborableInitializationPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA, null, msgs);
			if (newPreelaborableInitializationPragma != null)
				msgs = ((InternalEObject)newPreelaborableInitializationPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA, null, msgs);
			msgs = basicSetPreelaborableInitializationPragma(newPreelaborableInitializationPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA, newPreelaborableInitializationPragma, newPreelaborableInitializationPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrioritySpecificDispatchingPragma getPrioritySpecificDispatchingPragma() {
		return prioritySpecificDispatchingPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrioritySpecificDispatchingPragma(PrioritySpecificDispatchingPragma newPrioritySpecificDispatchingPragma, NotificationChain msgs) {
		PrioritySpecificDispatchingPragma oldPrioritySpecificDispatchingPragma = prioritySpecificDispatchingPragma;
		prioritySpecificDispatchingPragma = newPrioritySpecificDispatchingPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA, oldPrioritySpecificDispatchingPragma, newPrioritySpecificDispatchingPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrioritySpecificDispatchingPragma(PrioritySpecificDispatchingPragma newPrioritySpecificDispatchingPragma) {
		if (newPrioritySpecificDispatchingPragma != prioritySpecificDispatchingPragma) {
			NotificationChain msgs = null;
			if (prioritySpecificDispatchingPragma != null)
				msgs = ((InternalEObject)prioritySpecificDispatchingPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA, null, msgs);
			if (newPrioritySpecificDispatchingPragma != null)
				msgs = ((InternalEObject)newPrioritySpecificDispatchingPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA, null, msgs);
			msgs = basicSetPrioritySpecificDispatchingPragma(newPrioritySpecificDispatchingPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA, newPrioritySpecificDispatchingPragma, newPrioritySpecificDispatchingPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProfilePragma getProfilePragma() {
		return profilePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProfilePragma(ProfilePragma newProfilePragma, NotificationChain msgs) {
		ProfilePragma oldProfilePragma = profilePragma;
		profilePragma = newProfilePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PROFILE_PRAGMA, oldProfilePragma, newProfilePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProfilePragma(ProfilePragma newProfilePragma) {
		if (newProfilePragma != profilePragma) {
			NotificationChain msgs = null;
			if (profilePragma != null)
				msgs = ((InternalEObject)profilePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PROFILE_PRAGMA, null, msgs);
			if (newProfilePragma != null)
				msgs = ((InternalEObject)newProfilePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__PROFILE_PRAGMA, null, msgs);
			msgs = basicSetProfilePragma(newProfilePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__PROFILE_PRAGMA, newProfilePragma, newProfilePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelativeDeadlinePragma getRelativeDeadlinePragma() {
		return relativeDeadlinePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRelativeDeadlinePragma(RelativeDeadlinePragma newRelativeDeadlinePragma, NotificationChain msgs) {
		RelativeDeadlinePragma oldRelativeDeadlinePragma = relativeDeadlinePragma;
		relativeDeadlinePragma = newRelativeDeadlinePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__RELATIVE_DEADLINE_PRAGMA, oldRelativeDeadlinePragma, newRelativeDeadlinePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRelativeDeadlinePragma(RelativeDeadlinePragma newRelativeDeadlinePragma) {
		if (newRelativeDeadlinePragma != relativeDeadlinePragma) {
			NotificationChain msgs = null;
			if (relativeDeadlinePragma != null)
				msgs = ((InternalEObject)relativeDeadlinePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__RELATIVE_DEADLINE_PRAGMA, null, msgs);
			if (newRelativeDeadlinePragma != null)
				msgs = ((InternalEObject)newRelativeDeadlinePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__RELATIVE_DEADLINE_PRAGMA, null, msgs);
			msgs = basicSetRelativeDeadlinePragma(newRelativeDeadlinePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__RELATIVE_DEADLINE_PRAGMA, newRelativeDeadlinePragma, newRelativeDeadlinePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UncheckedUnionPragma getUncheckedUnionPragma() {
		return uncheckedUnionPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUncheckedUnionPragma(UncheckedUnionPragma newUncheckedUnionPragma, NotificationChain msgs) {
		UncheckedUnionPragma oldUncheckedUnionPragma = uncheckedUnionPragma;
		uncheckedUnionPragma = newUncheckedUnionPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__UNCHECKED_UNION_PRAGMA, oldUncheckedUnionPragma, newUncheckedUnionPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUncheckedUnionPragma(UncheckedUnionPragma newUncheckedUnionPragma) {
		if (newUncheckedUnionPragma != uncheckedUnionPragma) {
			NotificationChain msgs = null;
			if (uncheckedUnionPragma != null)
				msgs = ((InternalEObject)uncheckedUnionPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__UNCHECKED_UNION_PRAGMA, null, msgs);
			if (newUncheckedUnionPragma != null)
				msgs = ((InternalEObject)newUncheckedUnionPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__UNCHECKED_UNION_PRAGMA, null, msgs);
			msgs = basicSetUncheckedUnionPragma(newUncheckedUnionPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__UNCHECKED_UNION_PRAGMA, newUncheckedUnionPragma, newUncheckedUnionPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnsuppressPragma getUnsuppressPragma() {
		return unsuppressPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnsuppressPragma(UnsuppressPragma newUnsuppressPragma, NotificationChain msgs) {
		UnsuppressPragma oldUnsuppressPragma = unsuppressPragma;
		unsuppressPragma = newUnsuppressPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__UNSUPPRESS_PRAGMA, oldUnsuppressPragma, newUnsuppressPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnsuppressPragma(UnsuppressPragma newUnsuppressPragma) {
		if (newUnsuppressPragma != unsuppressPragma) {
			NotificationChain msgs = null;
			if (unsuppressPragma != null)
				msgs = ((InternalEObject)unsuppressPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__UNSUPPRESS_PRAGMA, null, msgs);
			if (newUnsuppressPragma != null)
				msgs = ((InternalEObject)newUnsuppressPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__UNSUPPRESS_PRAGMA, null, msgs);
			msgs = basicSetUnsuppressPragma(newUnsuppressPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__UNSUPPRESS_PRAGMA, newUnsuppressPragma, newUnsuppressPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefaultStoragePoolPragma getDefaultStoragePoolPragma() {
		return defaultStoragePoolPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefaultStoragePoolPragma(DefaultStoragePoolPragma newDefaultStoragePoolPragma, NotificationChain msgs) {
		DefaultStoragePoolPragma oldDefaultStoragePoolPragma = defaultStoragePoolPragma;
		defaultStoragePoolPragma = newDefaultStoragePoolPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA, oldDefaultStoragePoolPragma, newDefaultStoragePoolPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultStoragePoolPragma(DefaultStoragePoolPragma newDefaultStoragePoolPragma) {
		if (newDefaultStoragePoolPragma != defaultStoragePoolPragma) {
			NotificationChain msgs = null;
			if (defaultStoragePoolPragma != null)
				msgs = ((InternalEObject)defaultStoragePoolPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA, null, msgs);
			if (newDefaultStoragePoolPragma != null)
				msgs = ((InternalEObject)newDefaultStoragePoolPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA, null, msgs);
			msgs = basicSetDefaultStoragePoolPragma(newDefaultStoragePoolPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA, newDefaultStoragePoolPragma, newDefaultStoragePoolPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DispatchingDomainPragma getDispatchingDomainPragma() {
		return dispatchingDomainPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDispatchingDomainPragma(DispatchingDomainPragma newDispatchingDomainPragma, NotificationChain msgs) {
		DispatchingDomainPragma oldDispatchingDomainPragma = dispatchingDomainPragma;
		dispatchingDomainPragma = newDispatchingDomainPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DISPATCHING_DOMAIN_PRAGMA, oldDispatchingDomainPragma, newDispatchingDomainPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDispatchingDomainPragma(DispatchingDomainPragma newDispatchingDomainPragma) {
		if (newDispatchingDomainPragma != dispatchingDomainPragma) {
			NotificationChain msgs = null;
			if (dispatchingDomainPragma != null)
				msgs = ((InternalEObject)dispatchingDomainPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DISPATCHING_DOMAIN_PRAGMA, null, msgs);
			if (newDispatchingDomainPragma != null)
				msgs = ((InternalEObject)newDispatchingDomainPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__DISPATCHING_DOMAIN_PRAGMA, null, msgs);
			msgs = basicSetDispatchingDomainPragma(newDispatchingDomainPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__DISPATCHING_DOMAIN_PRAGMA, newDispatchingDomainPragma, newDispatchingDomainPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CpuPragma getCpuPragma() {
		return cpuPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCpuPragma(CpuPragma newCpuPragma, NotificationChain msgs) {
		CpuPragma oldCpuPragma = cpuPragma;
		cpuPragma = newCpuPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__CPU_PRAGMA, oldCpuPragma, newCpuPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCpuPragma(CpuPragma newCpuPragma) {
		if (newCpuPragma != cpuPragma) {
			NotificationChain msgs = null;
			if (cpuPragma != null)
				msgs = ((InternalEObject)cpuPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__CPU_PRAGMA, null, msgs);
			if (newCpuPragma != null)
				msgs = ((InternalEObject)newCpuPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__CPU_PRAGMA, null, msgs);
			msgs = basicSetCpuPragma(newCpuPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__CPU_PRAGMA, newCpuPragma, newCpuPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IndependentPragma getIndependentPragma() {
		return independentPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIndependentPragma(IndependentPragma newIndependentPragma, NotificationChain msgs) {
		IndependentPragma oldIndependentPragma = independentPragma;
		independentPragma = newIndependentPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__INDEPENDENT_PRAGMA, oldIndependentPragma, newIndependentPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndependentPragma(IndependentPragma newIndependentPragma) {
		if (newIndependentPragma != independentPragma) {
			NotificationChain msgs = null;
			if (independentPragma != null)
				msgs = ((InternalEObject)independentPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__INDEPENDENT_PRAGMA, null, msgs);
			if (newIndependentPragma != null)
				msgs = ((InternalEObject)newIndependentPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__INDEPENDENT_PRAGMA, null, msgs);
			msgs = basicSetIndependentPragma(newIndependentPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__INDEPENDENT_PRAGMA, newIndependentPragma, newIndependentPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IndependentComponentsPragma getIndependentComponentsPragma() {
		return independentComponentsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIndependentComponentsPragma(IndependentComponentsPragma newIndependentComponentsPragma, NotificationChain msgs) {
		IndependentComponentsPragma oldIndependentComponentsPragma = independentComponentsPragma;
		independentComponentsPragma = newIndependentComponentsPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA, oldIndependentComponentsPragma, newIndependentComponentsPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndependentComponentsPragma(IndependentComponentsPragma newIndependentComponentsPragma) {
		if (newIndependentComponentsPragma != independentComponentsPragma) {
			NotificationChain msgs = null;
			if (independentComponentsPragma != null)
				msgs = ((InternalEObject)independentComponentsPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA, null, msgs);
			if (newIndependentComponentsPragma != null)
				msgs = ((InternalEObject)newIndependentComponentsPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA, null, msgs);
			msgs = basicSetIndependentComponentsPragma(newIndependentComponentsPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA, newIndependentComponentsPragma, newIndependentComponentsPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImplementationDefinedPragma getImplementationDefinedPragma() {
		return implementationDefinedPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImplementationDefinedPragma(ImplementationDefinedPragma newImplementationDefinedPragma, NotificationChain msgs) {
		ImplementationDefinedPragma oldImplementationDefinedPragma = implementationDefinedPragma;
		implementationDefinedPragma = newImplementationDefinedPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA, oldImplementationDefinedPragma, newImplementationDefinedPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImplementationDefinedPragma(ImplementationDefinedPragma newImplementationDefinedPragma) {
		if (newImplementationDefinedPragma != implementationDefinedPragma) {
			NotificationChain msgs = null;
			if (implementationDefinedPragma != null)
				msgs = ((InternalEObject)implementationDefinedPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA, null, msgs);
			if (newImplementationDefinedPragma != null)
				msgs = ((InternalEObject)newImplementationDefinedPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA, null, msgs);
			msgs = basicSetImplementationDefinedPragma(newImplementationDefinedPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA, newImplementationDefinedPragma, newImplementationDefinedPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnknownPragma getUnknownPragma() {
		return unknownPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnknownPragma(UnknownPragma newUnknownPragma, NotificationChain msgs) {
		UnknownPragma oldUnknownPragma = unknownPragma;
		unknownPragma = newUnknownPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__UNKNOWN_PRAGMA, oldUnknownPragma, newUnknownPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnknownPragma(UnknownPragma newUnknownPragma) {
		if (newUnknownPragma != unknownPragma) {
			NotificationChain msgs = null;
			if (unknownPragma != null)
				msgs = ((InternalEObject)unknownPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__UNKNOWN_PRAGMA, null, msgs);
			if (newUnknownPragma != null)
				msgs = ((InternalEObject)newUnknownPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DEFINITION_CLASS__UNKNOWN_PRAGMA, null, msgs);
			msgs = basicSetUnknownPragma(newUnknownPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DEFINITION_CLASS__UNKNOWN_PRAGMA, newUnknownPragma, newUnknownPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.DEFINITION_CLASS__NOT_AN_ELEMENT:
				return basicSetNotAnElement(null, msgs);
			case AdaPackage.DEFINITION_CLASS__DERIVED_TYPE_DEFINITION:
				return basicSetDerivedTypeDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__DERIVED_RECORD_EXTENSION_DEFINITION:
				return basicSetDerivedRecordExtensionDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ENUMERATION_TYPE_DEFINITION:
				return basicSetEnumerationTypeDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__SIGNED_INTEGER_TYPE_DEFINITION:
				return basicSetSignedIntegerTypeDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__MODULAR_TYPE_DEFINITION:
				return basicSetModularTypeDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ROOT_INTEGER_DEFINITION:
				return basicSetRootIntegerDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ROOT_REAL_DEFINITION:
				return basicSetRootRealDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__UNIVERSAL_INTEGER_DEFINITION:
				return basicSetUniversalIntegerDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__UNIVERSAL_REAL_DEFINITION:
				return basicSetUniversalRealDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__UNIVERSAL_FIXED_DEFINITION:
				return basicSetUniversalFixedDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__FLOATING_POINT_DEFINITION:
				return basicSetFloatingPointDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ORDINARY_FIXED_POINT_DEFINITION:
				return basicSetOrdinaryFixedPointDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__DECIMAL_FIXED_POINT_DEFINITION:
				return basicSetDecimalFixedPointDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__UNCONSTRAINED_ARRAY_DEFINITION:
				return basicSetUnconstrainedArrayDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__CONSTRAINED_ARRAY_DEFINITION:
				return basicSetConstrainedArrayDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__RECORD_TYPE_DEFINITION:
				return basicSetRecordTypeDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__TAGGED_RECORD_TYPE_DEFINITION:
				return basicSetTaggedRecordTypeDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ORDINARY_INTERFACE:
				return basicSetOrdinaryInterface(null, msgs);
			case AdaPackage.DEFINITION_CLASS__LIMITED_INTERFACE:
				return basicSetLimitedInterface(null, msgs);
			case AdaPackage.DEFINITION_CLASS__TASK_INTERFACE:
				return basicSetTaskInterface(null, msgs);
			case AdaPackage.DEFINITION_CLASS__PROTECTED_INTERFACE:
				return basicSetProtectedInterface(null, msgs);
			case AdaPackage.DEFINITION_CLASS__SYNCHRONIZED_INTERFACE:
				return basicSetSynchronizedInterface(null, msgs);
			case AdaPackage.DEFINITION_CLASS__POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				return basicSetPoolSpecificAccessToVariable(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_VARIABLE:
				return basicSetAccessToVariable(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_CONSTANT:
				return basicSetAccessToConstant(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROCEDURE:
				return basicSetAccessToProcedure(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROTECTED_PROCEDURE:
				return basicSetAccessToProtectedProcedure(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_FUNCTION:
				return basicSetAccessToFunction(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROTECTED_FUNCTION:
				return basicSetAccessToProtectedFunction(null, msgs);
			case AdaPackage.DEFINITION_CLASS__SUBTYPE_INDICATION:
				return basicSetSubtypeIndication(null, msgs);
			case AdaPackage.DEFINITION_CLASS__RANGE_ATTRIBUTE_REFERENCE:
				return basicSetRangeAttributeReference(null, msgs);
			case AdaPackage.DEFINITION_CLASS__SIMPLE_EXPRESSION_RANGE:
				return basicSetSimpleExpressionRange(null, msgs);
			case AdaPackage.DEFINITION_CLASS__DIGITS_CONSTRAINT:
				return basicSetDigitsConstraint(null, msgs);
			case AdaPackage.DEFINITION_CLASS__DELTA_CONSTRAINT:
				return basicSetDeltaConstraint(null, msgs);
			case AdaPackage.DEFINITION_CLASS__INDEX_CONSTRAINT:
				return basicSetIndexConstraint(null, msgs);
			case AdaPackage.DEFINITION_CLASS__DISCRIMINANT_CONSTRAINT:
				return basicSetDiscriminantConstraint(null, msgs);
			case AdaPackage.DEFINITION_CLASS__COMPONENT_DEFINITION:
				return basicSetComponentDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION:
				return basicSetDiscreteSubtypeIndicationAsSubtypeDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION:
				return basicSetDiscreteRangeAttributeReferenceAsSubtypeDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION:
				return basicSetDiscreteSimpleExpressionRangeAsSubtypeDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__DISCRETE_SUBTYPE_INDICATION:
				return basicSetDiscreteSubtypeIndication(null, msgs);
			case AdaPackage.DEFINITION_CLASS__DISCRETE_RANGE_ATTRIBUTE_REFERENCE:
				return basicSetDiscreteRangeAttributeReference(null, msgs);
			case AdaPackage.DEFINITION_CLASS__DISCRETE_SIMPLE_EXPRESSION_RANGE:
				return basicSetDiscreteSimpleExpressionRange(null, msgs);
			case AdaPackage.DEFINITION_CLASS__UNKNOWN_DISCRIMINANT_PART:
				return basicSetUnknownDiscriminantPart(null, msgs);
			case AdaPackage.DEFINITION_CLASS__KNOWN_DISCRIMINANT_PART:
				return basicSetKnownDiscriminantPart(null, msgs);
			case AdaPackage.DEFINITION_CLASS__RECORD_DEFINITION:
				return basicSetRecordDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__NULL_RECORD_DEFINITION:
				return basicSetNullRecordDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__NULL_COMPONENT:
				return basicSetNullComponent(null, msgs);
			case AdaPackage.DEFINITION_CLASS__VARIANT_PART:
				return basicSetVariantPart(null, msgs);
			case AdaPackage.DEFINITION_CLASS__VARIANT:
				return basicSetVariant(null, msgs);
			case AdaPackage.DEFINITION_CLASS__OTHERS_CHOICE:
				return basicSetOthersChoice(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_VARIABLE:
				return basicSetAnonymousAccessToVariable(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_CONSTANT:
				return basicSetAnonymousAccessToConstant(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROCEDURE:
				return basicSetAnonymousAccessToProcedure(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE:
				return basicSetAnonymousAccessToProtectedProcedure(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_FUNCTION:
				return basicSetAnonymousAccessToFunction(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION:
				return basicSetAnonymousAccessToProtectedFunction(null, msgs);
			case AdaPackage.DEFINITION_CLASS__PRIVATE_TYPE_DEFINITION:
				return basicSetPrivateTypeDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__TAGGED_PRIVATE_TYPE_DEFINITION:
				return basicSetTaggedPrivateTypeDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__PRIVATE_EXTENSION_DEFINITION:
				return basicSetPrivateExtensionDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__TASK_DEFINITION:
				return basicSetTaskDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__PROTECTED_DEFINITION:
				return basicSetProtectedDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__FORMAL_PRIVATE_TYPE_DEFINITION:
				return basicSetFormalPrivateTypeDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION:
				return basicSetFormalTaggedPrivateTypeDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__FORMAL_DERIVED_TYPE_DEFINITION:
				return basicSetFormalDerivedTypeDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__FORMAL_DISCRETE_TYPE_DEFINITION:
				return basicSetFormalDiscreteTypeDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__FORMAL_SIGNED_INTEGER_TYPE_DEFINITION:
				return basicSetFormalSignedIntegerTypeDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__FORMAL_MODULAR_TYPE_DEFINITION:
				return basicSetFormalModularTypeDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__FORMAL_FLOATING_POINT_DEFINITION:
				return basicSetFormalFloatingPointDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__FORMAL_ORDINARY_FIXED_POINT_DEFINITION:
				return basicSetFormalOrdinaryFixedPointDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__FORMAL_DECIMAL_FIXED_POINT_DEFINITION:
				return basicSetFormalDecimalFixedPointDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__FORMAL_ORDINARY_INTERFACE:
				return basicSetFormalOrdinaryInterface(null, msgs);
			case AdaPackage.DEFINITION_CLASS__FORMAL_LIMITED_INTERFACE:
				return basicSetFormalLimitedInterface(null, msgs);
			case AdaPackage.DEFINITION_CLASS__FORMAL_TASK_INTERFACE:
				return basicSetFormalTaskInterface(null, msgs);
			case AdaPackage.DEFINITION_CLASS__FORMAL_PROTECTED_INTERFACE:
				return basicSetFormalProtectedInterface(null, msgs);
			case AdaPackage.DEFINITION_CLASS__FORMAL_SYNCHRONIZED_INTERFACE:
				return basicSetFormalSynchronizedInterface(null, msgs);
			case AdaPackage.DEFINITION_CLASS__FORMAL_UNCONSTRAINED_ARRAY_DEFINITION:
				return basicSetFormalUnconstrainedArrayDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__FORMAL_CONSTRAINED_ARRAY_DEFINITION:
				return basicSetFormalConstrainedArrayDefinition(null, msgs);
			case AdaPackage.DEFINITION_CLASS__FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				return basicSetFormalPoolSpecificAccessToVariable(null, msgs);
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_VARIABLE:
				return basicSetFormalAccessToVariable(null, msgs);
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_CONSTANT:
				return basicSetFormalAccessToConstant(null, msgs);
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROCEDURE:
				return basicSetFormalAccessToProcedure(null, msgs);
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROTECTED_PROCEDURE:
				return basicSetFormalAccessToProtectedProcedure(null, msgs);
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_FUNCTION:
				return basicSetFormalAccessToFunction(null, msgs);
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROTECTED_FUNCTION:
				return basicSetFormalAccessToProtectedFunction(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ASPECT_SPECIFICATION:
				return basicSetAspectSpecification(null, msgs);
			case AdaPackage.DEFINITION_CLASS__IDENTIFIER:
				return basicSetIdentifier(null, msgs);
			case AdaPackage.DEFINITION_CLASS__SELECTED_COMPONENT:
				return basicSetSelectedComponent(null, msgs);
			case AdaPackage.DEFINITION_CLASS__BASE_ATTRIBUTE:
				return basicSetBaseAttribute(null, msgs);
			case AdaPackage.DEFINITION_CLASS__CLASS_ATTRIBUTE:
				return basicSetClassAttribute(null, msgs);
			case AdaPackage.DEFINITION_CLASS__COMMENT:
				return basicSetComment(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ALL_CALLS_REMOTE_PRAGMA:
				return basicSetAllCallsRemotePragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ASYNCHRONOUS_PRAGMA:
				return basicSetAsynchronousPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ATOMIC_PRAGMA:
				return basicSetAtomicPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ATOMIC_COMPONENTS_PRAGMA:
				return basicSetAtomicComponentsPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ATTACH_HANDLER_PRAGMA:
				return basicSetAttachHandlerPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__CONTROLLED_PRAGMA:
				return basicSetControlledPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__CONVENTION_PRAGMA:
				return basicSetConventionPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__DISCARD_NAMES_PRAGMA:
				return basicSetDiscardNamesPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ELABORATE_PRAGMA:
				return basicSetElaboratePragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ELABORATE_ALL_PRAGMA:
				return basicSetElaborateAllPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ELABORATE_BODY_PRAGMA:
				return basicSetElaborateBodyPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__EXPORT_PRAGMA:
				return basicSetExportPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__IMPORT_PRAGMA:
				return basicSetImportPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__INLINE_PRAGMA:
				return basicSetInlinePragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__INSPECTION_POINT_PRAGMA:
				return basicSetInspectionPointPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__INTERRUPT_HANDLER_PRAGMA:
				return basicSetInterruptHandlerPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__INTERRUPT_PRIORITY_PRAGMA:
				return basicSetInterruptPriorityPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__LINKER_OPTIONS_PRAGMA:
				return basicSetLinkerOptionsPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__LIST_PRAGMA:
				return basicSetListPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__LOCKING_POLICY_PRAGMA:
				return basicSetLockingPolicyPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__NORMALIZE_SCALARS_PRAGMA:
				return basicSetNormalizeScalarsPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__OPTIMIZE_PRAGMA:
				return basicSetOptimizePragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__PACK_PRAGMA:
				return basicSetPackPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__PAGE_PRAGMA:
				return basicSetPagePragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__PREELABORATE_PRAGMA:
				return basicSetPreelaboratePragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__PRIORITY_PRAGMA:
				return basicSetPriorityPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__PURE_PRAGMA:
				return basicSetPurePragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__QUEUING_POLICY_PRAGMA:
				return basicSetQueuingPolicyPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA:
				return basicSetRemoteCallInterfacePragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__REMOTE_TYPES_PRAGMA:
				return basicSetRemoteTypesPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__RESTRICTIONS_PRAGMA:
				return basicSetRestrictionsPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__REVIEWABLE_PRAGMA:
				return basicSetReviewablePragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__SHARED_PASSIVE_PRAGMA:
				return basicSetSharedPassivePragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__STORAGE_SIZE_PRAGMA:
				return basicSetStorageSizePragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__SUPPRESS_PRAGMA:
				return basicSetSuppressPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA:
				return basicSetTaskDispatchingPolicyPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__VOLATILE_PRAGMA:
				return basicSetVolatilePragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__VOLATILE_COMPONENTS_PRAGMA:
				return basicSetVolatileComponentsPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ASSERT_PRAGMA:
				return basicSetAssertPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__ASSERTION_POLICY_PRAGMA:
				return basicSetAssertionPolicyPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__DETECT_BLOCKING_PRAGMA:
				return basicSetDetectBlockingPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__NO_RETURN_PRAGMA:
				return basicSetNoReturnPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA:
				return basicSetPartitionElaborationPolicyPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA:
				return basicSetPreelaborableInitializationPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return basicSetPrioritySpecificDispatchingPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__PROFILE_PRAGMA:
				return basicSetProfilePragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__RELATIVE_DEADLINE_PRAGMA:
				return basicSetRelativeDeadlinePragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__UNCHECKED_UNION_PRAGMA:
				return basicSetUncheckedUnionPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__UNSUPPRESS_PRAGMA:
				return basicSetUnsuppressPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA:
				return basicSetDefaultStoragePoolPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__DISPATCHING_DOMAIN_PRAGMA:
				return basicSetDispatchingDomainPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__CPU_PRAGMA:
				return basicSetCpuPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__INDEPENDENT_PRAGMA:
				return basicSetIndependentPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA:
				return basicSetIndependentComponentsPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA:
				return basicSetImplementationDefinedPragma(null, msgs);
			case AdaPackage.DEFINITION_CLASS__UNKNOWN_PRAGMA:
				return basicSetUnknownPragma(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.DEFINITION_CLASS__NOT_AN_ELEMENT:
				return getNotAnElement();
			case AdaPackage.DEFINITION_CLASS__DERIVED_TYPE_DEFINITION:
				return getDerivedTypeDefinition();
			case AdaPackage.DEFINITION_CLASS__DERIVED_RECORD_EXTENSION_DEFINITION:
				return getDerivedRecordExtensionDefinition();
			case AdaPackage.DEFINITION_CLASS__ENUMERATION_TYPE_DEFINITION:
				return getEnumerationTypeDefinition();
			case AdaPackage.DEFINITION_CLASS__SIGNED_INTEGER_TYPE_DEFINITION:
				return getSignedIntegerTypeDefinition();
			case AdaPackage.DEFINITION_CLASS__MODULAR_TYPE_DEFINITION:
				return getModularTypeDefinition();
			case AdaPackage.DEFINITION_CLASS__ROOT_INTEGER_DEFINITION:
				return getRootIntegerDefinition();
			case AdaPackage.DEFINITION_CLASS__ROOT_REAL_DEFINITION:
				return getRootRealDefinition();
			case AdaPackage.DEFINITION_CLASS__UNIVERSAL_INTEGER_DEFINITION:
				return getUniversalIntegerDefinition();
			case AdaPackage.DEFINITION_CLASS__UNIVERSAL_REAL_DEFINITION:
				return getUniversalRealDefinition();
			case AdaPackage.DEFINITION_CLASS__UNIVERSAL_FIXED_DEFINITION:
				return getUniversalFixedDefinition();
			case AdaPackage.DEFINITION_CLASS__FLOATING_POINT_DEFINITION:
				return getFloatingPointDefinition();
			case AdaPackage.DEFINITION_CLASS__ORDINARY_FIXED_POINT_DEFINITION:
				return getOrdinaryFixedPointDefinition();
			case AdaPackage.DEFINITION_CLASS__DECIMAL_FIXED_POINT_DEFINITION:
				return getDecimalFixedPointDefinition();
			case AdaPackage.DEFINITION_CLASS__UNCONSTRAINED_ARRAY_DEFINITION:
				return getUnconstrainedArrayDefinition();
			case AdaPackage.DEFINITION_CLASS__CONSTRAINED_ARRAY_DEFINITION:
				return getConstrainedArrayDefinition();
			case AdaPackage.DEFINITION_CLASS__RECORD_TYPE_DEFINITION:
				return getRecordTypeDefinition();
			case AdaPackage.DEFINITION_CLASS__TAGGED_RECORD_TYPE_DEFINITION:
				return getTaggedRecordTypeDefinition();
			case AdaPackage.DEFINITION_CLASS__ORDINARY_INTERFACE:
				return getOrdinaryInterface();
			case AdaPackage.DEFINITION_CLASS__LIMITED_INTERFACE:
				return getLimitedInterface();
			case AdaPackage.DEFINITION_CLASS__TASK_INTERFACE:
				return getTaskInterface();
			case AdaPackage.DEFINITION_CLASS__PROTECTED_INTERFACE:
				return getProtectedInterface();
			case AdaPackage.DEFINITION_CLASS__SYNCHRONIZED_INTERFACE:
				return getSynchronizedInterface();
			case AdaPackage.DEFINITION_CLASS__POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				return getPoolSpecificAccessToVariable();
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_VARIABLE:
				return getAccessToVariable();
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_CONSTANT:
				return getAccessToConstant();
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROCEDURE:
				return getAccessToProcedure();
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROTECTED_PROCEDURE:
				return getAccessToProtectedProcedure();
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_FUNCTION:
				return getAccessToFunction();
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROTECTED_FUNCTION:
				return getAccessToProtectedFunction();
			case AdaPackage.DEFINITION_CLASS__SUBTYPE_INDICATION:
				return getSubtypeIndication();
			case AdaPackage.DEFINITION_CLASS__RANGE_ATTRIBUTE_REFERENCE:
				return getRangeAttributeReference();
			case AdaPackage.DEFINITION_CLASS__SIMPLE_EXPRESSION_RANGE:
				return getSimpleExpressionRange();
			case AdaPackage.DEFINITION_CLASS__DIGITS_CONSTRAINT:
				return getDigitsConstraint();
			case AdaPackage.DEFINITION_CLASS__DELTA_CONSTRAINT:
				return getDeltaConstraint();
			case AdaPackage.DEFINITION_CLASS__INDEX_CONSTRAINT:
				return getIndexConstraint();
			case AdaPackage.DEFINITION_CLASS__DISCRIMINANT_CONSTRAINT:
				return getDiscriminantConstraint();
			case AdaPackage.DEFINITION_CLASS__COMPONENT_DEFINITION:
				return getComponentDefinition();
			case AdaPackage.DEFINITION_CLASS__DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION:
				return getDiscreteSubtypeIndicationAsSubtypeDefinition();
			case AdaPackage.DEFINITION_CLASS__DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION:
				return getDiscreteRangeAttributeReferenceAsSubtypeDefinition();
			case AdaPackage.DEFINITION_CLASS__DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION:
				return getDiscreteSimpleExpressionRangeAsSubtypeDefinition();
			case AdaPackage.DEFINITION_CLASS__DISCRETE_SUBTYPE_INDICATION:
				return getDiscreteSubtypeIndication();
			case AdaPackage.DEFINITION_CLASS__DISCRETE_RANGE_ATTRIBUTE_REFERENCE:
				return getDiscreteRangeAttributeReference();
			case AdaPackage.DEFINITION_CLASS__DISCRETE_SIMPLE_EXPRESSION_RANGE:
				return getDiscreteSimpleExpressionRange();
			case AdaPackage.DEFINITION_CLASS__UNKNOWN_DISCRIMINANT_PART:
				return getUnknownDiscriminantPart();
			case AdaPackage.DEFINITION_CLASS__KNOWN_DISCRIMINANT_PART:
				return getKnownDiscriminantPart();
			case AdaPackage.DEFINITION_CLASS__RECORD_DEFINITION:
				return getRecordDefinition();
			case AdaPackage.DEFINITION_CLASS__NULL_RECORD_DEFINITION:
				return getNullRecordDefinition();
			case AdaPackage.DEFINITION_CLASS__NULL_COMPONENT:
				return getNullComponent();
			case AdaPackage.DEFINITION_CLASS__VARIANT_PART:
				return getVariantPart();
			case AdaPackage.DEFINITION_CLASS__VARIANT:
				return getVariant();
			case AdaPackage.DEFINITION_CLASS__OTHERS_CHOICE:
				return getOthersChoice();
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_VARIABLE:
				return getAnonymousAccessToVariable();
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_CONSTANT:
				return getAnonymousAccessToConstant();
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROCEDURE:
				return getAnonymousAccessToProcedure();
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE:
				return getAnonymousAccessToProtectedProcedure();
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_FUNCTION:
				return getAnonymousAccessToFunction();
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION:
				return getAnonymousAccessToProtectedFunction();
			case AdaPackage.DEFINITION_CLASS__PRIVATE_TYPE_DEFINITION:
				return getPrivateTypeDefinition();
			case AdaPackage.DEFINITION_CLASS__TAGGED_PRIVATE_TYPE_DEFINITION:
				return getTaggedPrivateTypeDefinition();
			case AdaPackage.DEFINITION_CLASS__PRIVATE_EXTENSION_DEFINITION:
				return getPrivateExtensionDefinition();
			case AdaPackage.DEFINITION_CLASS__TASK_DEFINITION:
				return getTaskDefinition();
			case AdaPackage.DEFINITION_CLASS__PROTECTED_DEFINITION:
				return getProtectedDefinition();
			case AdaPackage.DEFINITION_CLASS__FORMAL_PRIVATE_TYPE_DEFINITION:
				return getFormalPrivateTypeDefinition();
			case AdaPackage.DEFINITION_CLASS__FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION:
				return getFormalTaggedPrivateTypeDefinition();
			case AdaPackage.DEFINITION_CLASS__FORMAL_DERIVED_TYPE_DEFINITION:
				return getFormalDerivedTypeDefinition();
			case AdaPackage.DEFINITION_CLASS__FORMAL_DISCRETE_TYPE_DEFINITION:
				return getFormalDiscreteTypeDefinition();
			case AdaPackage.DEFINITION_CLASS__FORMAL_SIGNED_INTEGER_TYPE_DEFINITION:
				return getFormalSignedIntegerTypeDefinition();
			case AdaPackage.DEFINITION_CLASS__FORMAL_MODULAR_TYPE_DEFINITION:
				return getFormalModularTypeDefinition();
			case AdaPackage.DEFINITION_CLASS__FORMAL_FLOATING_POINT_DEFINITION:
				return getFormalFloatingPointDefinition();
			case AdaPackage.DEFINITION_CLASS__FORMAL_ORDINARY_FIXED_POINT_DEFINITION:
				return getFormalOrdinaryFixedPointDefinition();
			case AdaPackage.DEFINITION_CLASS__FORMAL_DECIMAL_FIXED_POINT_DEFINITION:
				return getFormalDecimalFixedPointDefinition();
			case AdaPackage.DEFINITION_CLASS__FORMAL_ORDINARY_INTERFACE:
				return getFormalOrdinaryInterface();
			case AdaPackage.DEFINITION_CLASS__FORMAL_LIMITED_INTERFACE:
				return getFormalLimitedInterface();
			case AdaPackage.DEFINITION_CLASS__FORMAL_TASK_INTERFACE:
				return getFormalTaskInterface();
			case AdaPackage.DEFINITION_CLASS__FORMAL_PROTECTED_INTERFACE:
				return getFormalProtectedInterface();
			case AdaPackage.DEFINITION_CLASS__FORMAL_SYNCHRONIZED_INTERFACE:
				return getFormalSynchronizedInterface();
			case AdaPackage.DEFINITION_CLASS__FORMAL_UNCONSTRAINED_ARRAY_DEFINITION:
				return getFormalUnconstrainedArrayDefinition();
			case AdaPackage.DEFINITION_CLASS__FORMAL_CONSTRAINED_ARRAY_DEFINITION:
				return getFormalConstrainedArrayDefinition();
			case AdaPackage.DEFINITION_CLASS__FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				return getFormalPoolSpecificAccessToVariable();
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_VARIABLE:
				return getFormalAccessToVariable();
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_CONSTANT:
				return getFormalAccessToConstant();
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROCEDURE:
				return getFormalAccessToProcedure();
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROTECTED_PROCEDURE:
				return getFormalAccessToProtectedProcedure();
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_FUNCTION:
				return getFormalAccessToFunction();
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROTECTED_FUNCTION:
				return getFormalAccessToProtectedFunction();
			case AdaPackage.DEFINITION_CLASS__ASPECT_SPECIFICATION:
				return getAspectSpecification();
			case AdaPackage.DEFINITION_CLASS__IDENTIFIER:
				return getIdentifier();
			case AdaPackage.DEFINITION_CLASS__SELECTED_COMPONENT:
				return getSelectedComponent();
			case AdaPackage.DEFINITION_CLASS__BASE_ATTRIBUTE:
				return getBaseAttribute();
			case AdaPackage.DEFINITION_CLASS__CLASS_ATTRIBUTE:
				return getClassAttribute();
			case AdaPackage.DEFINITION_CLASS__COMMENT:
				return getComment();
			case AdaPackage.DEFINITION_CLASS__ALL_CALLS_REMOTE_PRAGMA:
				return getAllCallsRemotePragma();
			case AdaPackage.DEFINITION_CLASS__ASYNCHRONOUS_PRAGMA:
				return getAsynchronousPragma();
			case AdaPackage.DEFINITION_CLASS__ATOMIC_PRAGMA:
				return getAtomicPragma();
			case AdaPackage.DEFINITION_CLASS__ATOMIC_COMPONENTS_PRAGMA:
				return getAtomicComponentsPragma();
			case AdaPackage.DEFINITION_CLASS__ATTACH_HANDLER_PRAGMA:
				return getAttachHandlerPragma();
			case AdaPackage.DEFINITION_CLASS__CONTROLLED_PRAGMA:
				return getControlledPragma();
			case AdaPackage.DEFINITION_CLASS__CONVENTION_PRAGMA:
				return getConventionPragma();
			case AdaPackage.DEFINITION_CLASS__DISCARD_NAMES_PRAGMA:
				return getDiscardNamesPragma();
			case AdaPackage.DEFINITION_CLASS__ELABORATE_PRAGMA:
				return getElaboratePragma();
			case AdaPackage.DEFINITION_CLASS__ELABORATE_ALL_PRAGMA:
				return getElaborateAllPragma();
			case AdaPackage.DEFINITION_CLASS__ELABORATE_BODY_PRAGMA:
				return getElaborateBodyPragma();
			case AdaPackage.DEFINITION_CLASS__EXPORT_PRAGMA:
				return getExportPragma();
			case AdaPackage.DEFINITION_CLASS__IMPORT_PRAGMA:
				return getImportPragma();
			case AdaPackage.DEFINITION_CLASS__INLINE_PRAGMA:
				return getInlinePragma();
			case AdaPackage.DEFINITION_CLASS__INSPECTION_POINT_PRAGMA:
				return getInspectionPointPragma();
			case AdaPackage.DEFINITION_CLASS__INTERRUPT_HANDLER_PRAGMA:
				return getInterruptHandlerPragma();
			case AdaPackage.DEFINITION_CLASS__INTERRUPT_PRIORITY_PRAGMA:
				return getInterruptPriorityPragma();
			case AdaPackage.DEFINITION_CLASS__LINKER_OPTIONS_PRAGMA:
				return getLinkerOptionsPragma();
			case AdaPackage.DEFINITION_CLASS__LIST_PRAGMA:
				return getListPragma();
			case AdaPackage.DEFINITION_CLASS__LOCKING_POLICY_PRAGMA:
				return getLockingPolicyPragma();
			case AdaPackage.DEFINITION_CLASS__NORMALIZE_SCALARS_PRAGMA:
				return getNormalizeScalarsPragma();
			case AdaPackage.DEFINITION_CLASS__OPTIMIZE_PRAGMA:
				return getOptimizePragma();
			case AdaPackage.DEFINITION_CLASS__PACK_PRAGMA:
				return getPackPragma();
			case AdaPackage.DEFINITION_CLASS__PAGE_PRAGMA:
				return getPagePragma();
			case AdaPackage.DEFINITION_CLASS__PREELABORATE_PRAGMA:
				return getPreelaboratePragma();
			case AdaPackage.DEFINITION_CLASS__PRIORITY_PRAGMA:
				return getPriorityPragma();
			case AdaPackage.DEFINITION_CLASS__PURE_PRAGMA:
				return getPurePragma();
			case AdaPackage.DEFINITION_CLASS__QUEUING_POLICY_PRAGMA:
				return getQueuingPolicyPragma();
			case AdaPackage.DEFINITION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA:
				return getRemoteCallInterfacePragma();
			case AdaPackage.DEFINITION_CLASS__REMOTE_TYPES_PRAGMA:
				return getRemoteTypesPragma();
			case AdaPackage.DEFINITION_CLASS__RESTRICTIONS_PRAGMA:
				return getRestrictionsPragma();
			case AdaPackage.DEFINITION_CLASS__REVIEWABLE_PRAGMA:
				return getReviewablePragma();
			case AdaPackage.DEFINITION_CLASS__SHARED_PASSIVE_PRAGMA:
				return getSharedPassivePragma();
			case AdaPackage.DEFINITION_CLASS__STORAGE_SIZE_PRAGMA:
				return getStorageSizePragma();
			case AdaPackage.DEFINITION_CLASS__SUPPRESS_PRAGMA:
				return getSuppressPragma();
			case AdaPackage.DEFINITION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA:
				return getTaskDispatchingPolicyPragma();
			case AdaPackage.DEFINITION_CLASS__VOLATILE_PRAGMA:
				return getVolatilePragma();
			case AdaPackage.DEFINITION_CLASS__VOLATILE_COMPONENTS_PRAGMA:
				return getVolatileComponentsPragma();
			case AdaPackage.DEFINITION_CLASS__ASSERT_PRAGMA:
				return getAssertPragma();
			case AdaPackage.DEFINITION_CLASS__ASSERTION_POLICY_PRAGMA:
				return getAssertionPolicyPragma();
			case AdaPackage.DEFINITION_CLASS__DETECT_BLOCKING_PRAGMA:
				return getDetectBlockingPragma();
			case AdaPackage.DEFINITION_CLASS__NO_RETURN_PRAGMA:
				return getNoReturnPragma();
			case AdaPackage.DEFINITION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA:
				return getPartitionElaborationPolicyPragma();
			case AdaPackage.DEFINITION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA:
				return getPreelaborableInitializationPragma();
			case AdaPackage.DEFINITION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return getPrioritySpecificDispatchingPragma();
			case AdaPackage.DEFINITION_CLASS__PROFILE_PRAGMA:
				return getProfilePragma();
			case AdaPackage.DEFINITION_CLASS__RELATIVE_DEADLINE_PRAGMA:
				return getRelativeDeadlinePragma();
			case AdaPackage.DEFINITION_CLASS__UNCHECKED_UNION_PRAGMA:
				return getUncheckedUnionPragma();
			case AdaPackage.DEFINITION_CLASS__UNSUPPRESS_PRAGMA:
				return getUnsuppressPragma();
			case AdaPackage.DEFINITION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA:
				return getDefaultStoragePoolPragma();
			case AdaPackage.DEFINITION_CLASS__DISPATCHING_DOMAIN_PRAGMA:
				return getDispatchingDomainPragma();
			case AdaPackage.DEFINITION_CLASS__CPU_PRAGMA:
				return getCpuPragma();
			case AdaPackage.DEFINITION_CLASS__INDEPENDENT_PRAGMA:
				return getIndependentPragma();
			case AdaPackage.DEFINITION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA:
				return getIndependentComponentsPragma();
			case AdaPackage.DEFINITION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA:
				return getImplementationDefinedPragma();
			case AdaPackage.DEFINITION_CLASS__UNKNOWN_PRAGMA:
				return getUnknownPragma();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.DEFINITION_CLASS__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__DERIVED_TYPE_DEFINITION:
				setDerivedTypeDefinition((DerivedTypeDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__DERIVED_RECORD_EXTENSION_DEFINITION:
				setDerivedRecordExtensionDefinition((DerivedRecordExtensionDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ENUMERATION_TYPE_DEFINITION:
				setEnumerationTypeDefinition((EnumerationTypeDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__SIGNED_INTEGER_TYPE_DEFINITION:
				setSignedIntegerTypeDefinition((SignedIntegerTypeDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__MODULAR_TYPE_DEFINITION:
				setModularTypeDefinition((ModularTypeDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ROOT_INTEGER_DEFINITION:
				setRootIntegerDefinition((RootIntegerDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ROOT_REAL_DEFINITION:
				setRootRealDefinition((RootRealDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__UNIVERSAL_INTEGER_DEFINITION:
				setUniversalIntegerDefinition((UniversalIntegerDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__UNIVERSAL_REAL_DEFINITION:
				setUniversalRealDefinition((UniversalRealDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__UNIVERSAL_FIXED_DEFINITION:
				setUniversalFixedDefinition((UniversalFixedDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__FLOATING_POINT_DEFINITION:
				setFloatingPointDefinition((FloatingPointDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ORDINARY_FIXED_POINT_DEFINITION:
				setOrdinaryFixedPointDefinition((OrdinaryFixedPointDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__DECIMAL_FIXED_POINT_DEFINITION:
				setDecimalFixedPointDefinition((DecimalFixedPointDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__UNCONSTRAINED_ARRAY_DEFINITION:
				setUnconstrainedArrayDefinition((UnconstrainedArrayDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__CONSTRAINED_ARRAY_DEFINITION:
				setConstrainedArrayDefinition((ConstrainedArrayDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__RECORD_TYPE_DEFINITION:
				setRecordTypeDefinition((RecordTypeDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__TAGGED_RECORD_TYPE_DEFINITION:
				setTaggedRecordTypeDefinition((TaggedRecordTypeDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ORDINARY_INTERFACE:
				setOrdinaryInterface((OrdinaryInterface)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__LIMITED_INTERFACE:
				setLimitedInterface((LimitedInterface)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__TASK_INTERFACE:
				setTaskInterface((TaskInterface)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__PROTECTED_INTERFACE:
				setProtectedInterface((ProtectedInterface)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__SYNCHRONIZED_INTERFACE:
				setSynchronizedInterface((SynchronizedInterface)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				setPoolSpecificAccessToVariable((PoolSpecificAccessToVariable)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_VARIABLE:
				setAccessToVariable((AccessToVariable)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_CONSTANT:
				setAccessToConstant((AccessToConstant)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROCEDURE:
				setAccessToProcedure((AccessToProcedure)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROTECTED_PROCEDURE:
				setAccessToProtectedProcedure((AccessToProtectedProcedure)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_FUNCTION:
				setAccessToFunction((AccessToFunction)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROTECTED_FUNCTION:
				setAccessToProtectedFunction((AccessToProtectedFunction)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__SUBTYPE_INDICATION:
				setSubtypeIndication((SubtypeIndication)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__RANGE_ATTRIBUTE_REFERENCE:
				setRangeAttributeReference((RangeAttributeReference)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__SIMPLE_EXPRESSION_RANGE:
				setSimpleExpressionRange((SimpleExpressionRange)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__DIGITS_CONSTRAINT:
				setDigitsConstraint((DigitsConstraint)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__DELTA_CONSTRAINT:
				setDeltaConstraint((DeltaConstraint)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__INDEX_CONSTRAINT:
				setIndexConstraint((IndexConstraint)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__DISCRIMINANT_CONSTRAINT:
				setDiscriminantConstraint((DiscriminantConstraint)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__COMPONENT_DEFINITION:
				setComponentDefinition((ComponentDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION:
				setDiscreteSubtypeIndicationAsSubtypeDefinition((DiscreteSubtypeIndicationAsSubtypeDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION:
				setDiscreteRangeAttributeReferenceAsSubtypeDefinition((DiscreteRangeAttributeReferenceAsSubtypeDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION:
				setDiscreteSimpleExpressionRangeAsSubtypeDefinition((DiscreteSimpleExpressionRangeAsSubtypeDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__DISCRETE_SUBTYPE_INDICATION:
				setDiscreteSubtypeIndication((DiscreteSubtypeIndication)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__DISCRETE_RANGE_ATTRIBUTE_REFERENCE:
				setDiscreteRangeAttributeReference((DiscreteRangeAttributeReference)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__DISCRETE_SIMPLE_EXPRESSION_RANGE:
				setDiscreteSimpleExpressionRange((DiscreteSimpleExpressionRange)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__UNKNOWN_DISCRIMINANT_PART:
				setUnknownDiscriminantPart((UnknownDiscriminantPart)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__KNOWN_DISCRIMINANT_PART:
				setKnownDiscriminantPart((KnownDiscriminantPart)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__RECORD_DEFINITION:
				setRecordDefinition((RecordDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__NULL_RECORD_DEFINITION:
				setNullRecordDefinition((NullRecordDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__NULL_COMPONENT:
				setNullComponent((NullComponent)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__VARIANT_PART:
				setVariantPart((VariantPart)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__VARIANT:
				setVariant((Variant)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__OTHERS_CHOICE:
				setOthersChoice((OthersChoice)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_VARIABLE:
				setAnonymousAccessToVariable((AnonymousAccessToVariable)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_CONSTANT:
				setAnonymousAccessToConstant((AnonymousAccessToConstant)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROCEDURE:
				setAnonymousAccessToProcedure((AnonymousAccessToProcedure)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE:
				setAnonymousAccessToProtectedProcedure((AnonymousAccessToProtectedProcedure)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_FUNCTION:
				setAnonymousAccessToFunction((AnonymousAccessToFunction)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION:
				setAnonymousAccessToProtectedFunction((AnonymousAccessToProtectedFunction)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__PRIVATE_TYPE_DEFINITION:
				setPrivateTypeDefinition((PrivateTypeDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__TAGGED_PRIVATE_TYPE_DEFINITION:
				setTaggedPrivateTypeDefinition((TaggedPrivateTypeDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__PRIVATE_EXTENSION_DEFINITION:
				setPrivateExtensionDefinition((PrivateExtensionDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__TASK_DEFINITION:
				setTaskDefinition((TaskDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__PROTECTED_DEFINITION:
				setProtectedDefinition((ProtectedDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_PRIVATE_TYPE_DEFINITION:
				setFormalPrivateTypeDefinition((FormalPrivateTypeDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION:
				setFormalTaggedPrivateTypeDefinition((FormalTaggedPrivateTypeDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_DERIVED_TYPE_DEFINITION:
				setFormalDerivedTypeDefinition((FormalDerivedTypeDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_DISCRETE_TYPE_DEFINITION:
				setFormalDiscreteTypeDefinition((FormalDiscreteTypeDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_SIGNED_INTEGER_TYPE_DEFINITION:
				setFormalSignedIntegerTypeDefinition((FormalSignedIntegerTypeDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_MODULAR_TYPE_DEFINITION:
				setFormalModularTypeDefinition((FormalModularTypeDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_FLOATING_POINT_DEFINITION:
				setFormalFloatingPointDefinition((FormalFloatingPointDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_ORDINARY_FIXED_POINT_DEFINITION:
				setFormalOrdinaryFixedPointDefinition((FormalOrdinaryFixedPointDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_DECIMAL_FIXED_POINT_DEFINITION:
				setFormalDecimalFixedPointDefinition((FormalDecimalFixedPointDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_ORDINARY_INTERFACE:
				setFormalOrdinaryInterface((FormalOrdinaryInterface)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_LIMITED_INTERFACE:
				setFormalLimitedInterface((FormalLimitedInterface)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_TASK_INTERFACE:
				setFormalTaskInterface((FormalTaskInterface)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_PROTECTED_INTERFACE:
				setFormalProtectedInterface((FormalProtectedInterface)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_SYNCHRONIZED_INTERFACE:
				setFormalSynchronizedInterface((FormalSynchronizedInterface)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_UNCONSTRAINED_ARRAY_DEFINITION:
				setFormalUnconstrainedArrayDefinition((FormalUnconstrainedArrayDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_CONSTRAINED_ARRAY_DEFINITION:
				setFormalConstrainedArrayDefinition((FormalConstrainedArrayDefinition)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				setFormalPoolSpecificAccessToVariable((FormalPoolSpecificAccessToVariable)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_VARIABLE:
				setFormalAccessToVariable((FormalAccessToVariable)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_CONSTANT:
				setFormalAccessToConstant((FormalAccessToConstant)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROCEDURE:
				setFormalAccessToProcedure((FormalAccessToProcedure)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROTECTED_PROCEDURE:
				setFormalAccessToProtectedProcedure((FormalAccessToProtectedProcedure)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_FUNCTION:
				setFormalAccessToFunction((FormalAccessToFunction)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROTECTED_FUNCTION:
				setFormalAccessToProtectedFunction((FormalAccessToProtectedFunction)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ASPECT_SPECIFICATION:
				setAspectSpecification((AspectSpecification)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__IDENTIFIER:
				setIdentifier((Identifier)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__SELECTED_COMPONENT:
				setSelectedComponent((SelectedComponent)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__BASE_ATTRIBUTE:
				setBaseAttribute((BaseAttribute)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__CLASS_ATTRIBUTE:
				setClassAttribute((ClassAttribute)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__COMMENT:
				setComment((Comment)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ALL_CALLS_REMOTE_PRAGMA:
				setAllCallsRemotePragma((AllCallsRemotePragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ASYNCHRONOUS_PRAGMA:
				setAsynchronousPragma((AsynchronousPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ATOMIC_PRAGMA:
				setAtomicPragma((AtomicPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ATOMIC_COMPONENTS_PRAGMA:
				setAtomicComponentsPragma((AtomicComponentsPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ATTACH_HANDLER_PRAGMA:
				setAttachHandlerPragma((AttachHandlerPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__CONTROLLED_PRAGMA:
				setControlledPragma((ControlledPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__CONVENTION_PRAGMA:
				setConventionPragma((ConventionPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__DISCARD_NAMES_PRAGMA:
				setDiscardNamesPragma((DiscardNamesPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ELABORATE_PRAGMA:
				setElaboratePragma((ElaboratePragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ELABORATE_ALL_PRAGMA:
				setElaborateAllPragma((ElaborateAllPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ELABORATE_BODY_PRAGMA:
				setElaborateBodyPragma((ElaborateBodyPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__EXPORT_PRAGMA:
				setExportPragma((ExportPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__IMPORT_PRAGMA:
				setImportPragma((ImportPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__INLINE_PRAGMA:
				setInlinePragma((InlinePragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__INSPECTION_POINT_PRAGMA:
				setInspectionPointPragma((InspectionPointPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__INTERRUPT_HANDLER_PRAGMA:
				setInterruptHandlerPragma((InterruptHandlerPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__INTERRUPT_PRIORITY_PRAGMA:
				setInterruptPriorityPragma((InterruptPriorityPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__LINKER_OPTIONS_PRAGMA:
				setLinkerOptionsPragma((LinkerOptionsPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__LIST_PRAGMA:
				setListPragma((ListPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__LOCKING_POLICY_PRAGMA:
				setLockingPolicyPragma((LockingPolicyPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__NORMALIZE_SCALARS_PRAGMA:
				setNormalizeScalarsPragma((NormalizeScalarsPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__OPTIMIZE_PRAGMA:
				setOptimizePragma((OptimizePragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__PACK_PRAGMA:
				setPackPragma((PackPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__PAGE_PRAGMA:
				setPagePragma((PagePragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__PREELABORATE_PRAGMA:
				setPreelaboratePragma((PreelaboratePragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__PRIORITY_PRAGMA:
				setPriorityPragma((PriorityPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__PURE_PRAGMA:
				setPurePragma((PurePragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__QUEUING_POLICY_PRAGMA:
				setQueuingPolicyPragma((QueuingPolicyPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA:
				setRemoteCallInterfacePragma((RemoteCallInterfacePragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__REMOTE_TYPES_PRAGMA:
				setRemoteTypesPragma((RemoteTypesPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__RESTRICTIONS_PRAGMA:
				setRestrictionsPragma((RestrictionsPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__REVIEWABLE_PRAGMA:
				setReviewablePragma((ReviewablePragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__SHARED_PASSIVE_PRAGMA:
				setSharedPassivePragma((SharedPassivePragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__STORAGE_SIZE_PRAGMA:
				setStorageSizePragma((StorageSizePragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__SUPPRESS_PRAGMA:
				setSuppressPragma((SuppressPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA:
				setTaskDispatchingPolicyPragma((TaskDispatchingPolicyPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__VOLATILE_PRAGMA:
				setVolatilePragma((VolatilePragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__VOLATILE_COMPONENTS_PRAGMA:
				setVolatileComponentsPragma((VolatileComponentsPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ASSERT_PRAGMA:
				setAssertPragma((AssertPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__ASSERTION_POLICY_PRAGMA:
				setAssertionPolicyPragma((AssertionPolicyPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__DETECT_BLOCKING_PRAGMA:
				setDetectBlockingPragma((DetectBlockingPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__NO_RETURN_PRAGMA:
				setNoReturnPragma((NoReturnPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA:
				setPartitionElaborationPolicyPragma((PartitionElaborationPolicyPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA:
				setPreelaborableInitializationPragma((PreelaborableInitializationPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				setPrioritySpecificDispatchingPragma((PrioritySpecificDispatchingPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__PROFILE_PRAGMA:
				setProfilePragma((ProfilePragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__RELATIVE_DEADLINE_PRAGMA:
				setRelativeDeadlinePragma((RelativeDeadlinePragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__UNCHECKED_UNION_PRAGMA:
				setUncheckedUnionPragma((UncheckedUnionPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__UNSUPPRESS_PRAGMA:
				setUnsuppressPragma((UnsuppressPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA:
				setDefaultStoragePoolPragma((DefaultStoragePoolPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__DISPATCHING_DOMAIN_PRAGMA:
				setDispatchingDomainPragma((DispatchingDomainPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__CPU_PRAGMA:
				setCpuPragma((CpuPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__INDEPENDENT_PRAGMA:
				setIndependentPragma((IndependentPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA:
				setIndependentComponentsPragma((IndependentComponentsPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA:
				setImplementationDefinedPragma((ImplementationDefinedPragma)newValue);
				return;
			case AdaPackage.DEFINITION_CLASS__UNKNOWN_PRAGMA:
				setUnknownPragma((UnknownPragma)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.DEFINITION_CLASS__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)null);
				return;
			case AdaPackage.DEFINITION_CLASS__DERIVED_TYPE_DEFINITION:
				setDerivedTypeDefinition((DerivedTypeDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__DERIVED_RECORD_EXTENSION_DEFINITION:
				setDerivedRecordExtensionDefinition((DerivedRecordExtensionDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ENUMERATION_TYPE_DEFINITION:
				setEnumerationTypeDefinition((EnumerationTypeDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__SIGNED_INTEGER_TYPE_DEFINITION:
				setSignedIntegerTypeDefinition((SignedIntegerTypeDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__MODULAR_TYPE_DEFINITION:
				setModularTypeDefinition((ModularTypeDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ROOT_INTEGER_DEFINITION:
				setRootIntegerDefinition((RootIntegerDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ROOT_REAL_DEFINITION:
				setRootRealDefinition((RootRealDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__UNIVERSAL_INTEGER_DEFINITION:
				setUniversalIntegerDefinition((UniversalIntegerDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__UNIVERSAL_REAL_DEFINITION:
				setUniversalRealDefinition((UniversalRealDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__UNIVERSAL_FIXED_DEFINITION:
				setUniversalFixedDefinition((UniversalFixedDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__FLOATING_POINT_DEFINITION:
				setFloatingPointDefinition((FloatingPointDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ORDINARY_FIXED_POINT_DEFINITION:
				setOrdinaryFixedPointDefinition((OrdinaryFixedPointDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__DECIMAL_FIXED_POINT_DEFINITION:
				setDecimalFixedPointDefinition((DecimalFixedPointDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__UNCONSTRAINED_ARRAY_DEFINITION:
				setUnconstrainedArrayDefinition((UnconstrainedArrayDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__CONSTRAINED_ARRAY_DEFINITION:
				setConstrainedArrayDefinition((ConstrainedArrayDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__RECORD_TYPE_DEFINITION:
				setRecordTypeDefinition((RecordTypeDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__TAGGED_RECORD_TYPE_DEFINITION:
				setTaggedRecordTypeDefinition((TaggedRecordTypeDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ORDINARY_INTERFACE:
				setOrdinaryInterface((OrdinaryInterface)null);
				return;
			case AdaPackage.DEFINITION_CLASS__LIMITED_INTERFACE:
				setLimitedInterface((LimitedInterface)null);
				return;
			case AdaPackage.DEFINITION_CLASS__TASK_INTERFACE:
				setTaskInterface((TaskInterface)null);
				return;
			case AdaPackage.DEFINITION_CLASS__PROTECTED_INTERFACE:
				setProtectedInterface((ProtectedInterface)null);
				return;
			case AdaPackage.DEFINITION_CLASS__SYNCHRONIZED_INTERFACE:
				setSynchronizedInterface((SynchronizedInterface)null);
				return;
			case AdaPackage.DEFINITION_CLASS__POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				setPoolSpecificAccessToVariable((PoolSpecificAccessToVariable)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_VARIABLE:
				setAccessToVariable((AccessToVariable)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_CONSTANT:
				setAccessToConstant((AccessToConstant)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROCEDURE:
				setAccessToProcedure((AccessToProcedure)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROTECTED_PROCEDURE:
				setAccessToProtectedProcedure((AccessToProtectedProcedure)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_FUNCTION:
				setAccessToFunction((AccessToFunction)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROTECTED_FUNCTION:
				setAccessToProtectedFunction((AccessToProtectedFunction)null);
				return;
			case AdaPackage.DEFINITION_CLASS__SUBTYPE_INDICATION:
				setSubtypeIndication((SubtypeIndication)null);
				return;
			case AdaPackage.DEFINITION_CLASS__RANGE_ATTRIBUTE_REFERENCE:
				setRangeAttributeReference((RangeAttributeReference)null);
				return;
			case AdaPackage.DEFINITION_CLASS__SIMPLE_EXPRESSION_RANGE:
				setSimpleExpressionRange((SimpleExpressionRange)null);
				return;
			case AdaPackage.DEFINITION_CLASS__DIGITS_CONSTRAINT:
				setDigitsConstraint((DigitsConstraint)null);
				return;
			case AdaPackage.DEFINITION_CLASS__DELTA_CONSTRAINT:
				setDeltaConstraint((DeltaConstraint)null);
				return;
			case AdaPackage.DEFINITION_CLASS__INDEX_CONSTRAINT:
				setIndexConstraint((IndexConstraint)null);
				return;
			case AdaPackage.DEFINITION_CLASS__DISCRIMINANT_CONSTRAINT:
				setDiscriminantConstraint((DiscriminantConstraint)null);
				return;
			case AdaPackage.DEFINITION_CLASS__COMPONENT_DEFINITION:
				setComponentDefinition((ComponentDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION:
				setDiscreteSubtypeIndicationAsSubtypeDefinition((DiscreteSubtypeIndicationAsSubtypeDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION:
				setDiscreteRangeAttributeReferenceAsSubtypeDefinition((DiscreteRangeAttributeReferenceAsSubtypeDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION:
				setDiscreteSimpleExpressionRangeAsSubtypeDefinition((DiscreteSimpleExpressionRangeAsSubtypeDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__DISCRETE_SUBTYPE_INDICATION:
				setDiscreteSubtypeIndication((DiscreteSubtypeIndication)null);
				return;
			case AdaPackage.DEFINITION_CLASS__DISCRETE_RANGE_ATTRIBUTE_REFERENCE:
				setDiscreteRangeAttributeReference((DiscreteRangeAttributeReference)null);
				return;
			case AdaPackage.DEFINITION_CLASS__DISCRETE_SIMPLE_EXPRESSION_RANGE:
				setDiscreteSimpleExpressionRange((DiscreteSimpleExpressionRange)null);
				return;
			case AdaPackage.DEFINITION_CLASS__UNKNOWN_DISCRIMINANT_PART:
				setUnknownDiscriminantPart((UnknownDiscriminantPart)null);
				return;
			case AdaPackage.DEFINITION_CLASS__KNOWN_DISCRIMINANT_PART:
				setKnownDiscriminantPart((KnownDiscriminantPart)null);
				return;
			case AdaPackage.DEFINITION_CLASS__RECORD_DEFINITION:
				setRecordDefinition((RecordDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__NULL_RECORD_DEFINITION:
				setNullRecordDefinition((NullRecordDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__NULL_COMPONENT:
				setNullComponent((NullComponent)null);
				return;
			case AdaPackage.DEFINITION_CLASS__VARIANT_PART:
				setVariantPart((VariantPart)null);
				return;
			case AdaPackage.DEFINITION_CLASS__VARIANT:
				setVariant((Variant)null);
				return;
			case AdaPackage.DEFINITION_CLASS__OTHERS_CHOICE:
				setOthersChoice((OthersChoice)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_VARIABLE:
				setAnonymousAccessToVariable((AnonymousAccessToVariable)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_CONSTANT:
				setAnonymousAccessToConstant((AnonymousAccessToConstant)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROCEDURE:
				setAnonymousAccessToProcedure((AnonymousAccessToProcedure)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE:
				setAnonymousAccessToProtectedProcedure((AnonymousAccessToProtectedProcedure)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_FUNCTION:
				setAnonymousAccessToFunction((AnonymousAccessToFunction)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION:
				setAnonymousAccessToProtectedFunction((AnonymousAccessToProtectedFunction)null);
				return;
			case AdaPackage.DEFINITION_CLASS__PRIVATE_TYPE_DEFINITION:
				setPrivateTypeDefinition((PrivateTypeDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__TAGGED_PRIVATE_TYPE_DEFINITION:
				setTaggedPrivateTypeDefinition((TaggedPrivateTypeDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__PRIVATE_EXTENSION_DEFINITION:
				setPrivateExtensionDefinition((PrivateExtensionDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__TASK_DEFINITION:
				setTaskDefinition((TaskDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__PROTECTED_DEFINITION:
				setProtectedDefinition((ProtectedDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_PRIVATE_TYPE_DEFINITION:
				setFormalPrivateTypeDefinition((FormalPrivateTypeDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION:
				setFormalTaggedPrivateTypeDefinition((FormalTaggedPrivateTypeDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_DERIVED_TYPE_DEFINITION:
				setFormalDerivedTypeDefinition((FormalDerivedTypeDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_DISCRETE_TYPE_DEFINITION:
				setFormalDiscreteTypeDefinition((FormalDiscreteTypeDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_SIGNED_INTEGER_TYPE_DEFINITION:
				setFormalSignedIntegerTypeDefinition((FormalSignedIntegerTypeDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_MODULAR_TYPE_DEFINITION:
				setFormalModularTypeDefinition((FormalModularTypeDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_FLOATING_POINT_DEFINITION:
				setFormalFloatingPointDefinition((FormalFloatingPointDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_ORDINARY_FIXED_POINT_DEFINITION:
				setFormalOrdinaryFixedPointDefinition((FormalOrdinaryFixedPointDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_DECIMAL_FIXED_POINT_DEFINITION:
				setFormalDecimalFixedPointDefinition((FormalDecimalFixedPointDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_ORDINARY_INTERFACE:
				setFormalOrdinaryInterface((FormalOrdinaryInterface)null);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_LIMITED_INTERFACE:
				setFormalLimitedInterface((FormalLimitedInterface)null);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_TASK_INTERFACE:
				setFormalTaskInterface((FormalTaskInterface)null);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_PROTECTED_INTERFACE:
				setFormalProtectedInterface((FormalProtectedInterface)null);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_SYNCHRONIZED_INTERFACE:
				setFormalSynchronizedInterface((FormalSynchronizedInterface)null);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_UNCONSTRAINED_ARRAY_DEFINITION:
				setFormalUnconstrainedArrayDefinition((FormalUnconstrainedArrayDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_CONSTRAINED_ARRAY_DEFINITION:
				setFormalConstrainedArrayDefinition((FormalConstrainedArrayDefinition)null);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				setFormalPoolSpecificAccessToVariable((FormalPoolSpecificAccessToVariable)null);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_VARIABLE:
				setFormalAccessToVariable((FormalAccessToVariable)null);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_CONSTANT:
				setFormalAccessToConstant((FormalAccessToConstant)null);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROCEDURE:
				setFormalAccessToProcedure((FormalAccessToProcedure)null);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROTECTED_PROCEDURE:
				setFormalAccessToProtectedProcedure((FormalAccessToProtectedProcedure)null);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_FUNCTION:
				setFormalAccessToFunction((FormalAccessToFunction)null);
				return;
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROTECTED_FUNCTION:
				setFormalAccessToProtectedFunction((FormalAccessToProtectedFunction)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ASPECT_SPECIFICATION:
				setAspectSpecification((AspectSpecification)null);
				return;
			case AdaPackage.DEFINITION_CLASS__IDENTIFIER:
				setIdentifier((Identifier)null);
				return;
			case AdaPackage.DEFINITION_CLASS__SELECTED_COMPONENT:
				setSelectedComponent((SelectedComponent)null);
				return;
			case AdaPackage.DEFINITION_CLASS__BASE_ATTRIBUTE:
				setBaseAttribute((BaseAttribute)null);
				return;
			case AdaPackage.DEFINITION_CLASS__CLASS_ATTRIBUTE:
				setClassAttribute((ClassAttribute)null);
				return;
			case AdaPackage.DEFINITION_CLASS__COMMENT:
				setComment((Comment)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ALL_CALLS_REMOTE_PRAGMA:
				setAllCallsRemotePragma((AllCallsRemotePragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ASYNCHRONOUS_PRAGMA:
				setAsynchronousPragma((AsynchronousPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ATOMIC_PRAGMA:
				setAtomicPragma((AtomicPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ATOMIC_COMPONENTS_PRAGMA:
				setAtomicComponentsPragma((AtomicComponentsPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ATTACH_HANDLER_PRAGMA:
				setAttachHandlerPragma((AttachHandlerPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__CONTROLLED_PRAGMA:
				setControlledPragma((ControlledPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__CONVENTION_PRAGMA:
				setConventionPragma((ConventionPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__DISCARD_NAMES_PRAGMA:
				setDiscardNamesPragma((DiscardNamesPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ELABORATE_PRAGMA:
				setElaboratePragma((ElaboratePragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ELABORATE_ALL_PRAGMA:
				setElaborateAllPragma((ElaborateAllPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ELABORATE_BODY_PRAGMA:
				setElaborateBodyPragma((ElaborateBodyPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__EXPORT_PRAGMA:
				setExportPragma((ExportPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__IMPORT_PRAGMA:
				setImportPragma((ImportPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__INLINE_PRAGMA:
				setInlinePragma((InlinePragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__INSPECTION_POINT_PRAGMA:
				setInspectionPointPragma((InspectionPointPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__INTERRUPT_HANDLER_PRAGMA:
				setInterruptHandlerPragma((InterruptHandlerPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__INTERRUPT_PRIORITY_PRAGMA:
				setInterruptPriorityPragma((InterruptPriorityPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__LINKER_OPTIONS_PRAGMA:
				setLinkerOptionsPragma((LinkerOptionsPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__LIST_PRAGMA:
				setListPragma((ListPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__LOCKING_POLICY_PRAGMA:
				setLockingPolicyPragma((LockingPolicyPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__NORMALIZE_SCALARS_PRAGMA:
				setNormalizeScalarsPragma((NormalizeScalarsPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__OPTIMIZE_PRAGMA:
				setOptimizePragma((OptimizePragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__PACK_PRAGMA:
				setPackPragma((PackPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__PAGE_PRAGMA:
				setPagePragma((PagePragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__PREELABORATE_PRAGMA:
				setPreelaboratePragma((PreelaboratePragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__PRIORITY_PRAGMA:
				setPriorityPragma((PriorityPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__PURE_PRAGMA:
				setPurePragma((PurePragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__QUEUING_POLICY_PRAGMA:
				setQueuingPolicyPragma((QueuingPolicyPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA:
				setRemoteCallInterfacePragma((RemoteCallInterfacePragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__REMOTE_TYPES_PRAGMA:
				setRemoteTypesPragma((RemoteTypesPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__RESTRICTIONS_PRAGMA:
				setRestrictionsPragma((RestrictionsPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__REVIEWABLE_PRAGMA:
				setReviewablePragma((ReviewablePragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__SHARED_PASSIVE_PRAGMA:
				setSharedPassivePragma((SharedPassivePragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__STORAGE_SIZE_PRAGMA:
				setStorageSizePragma((StorageSizePragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__SUPPRESS_PRAGMA:
				setSuppressPragma((SuppressPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA:
				setTaskDispatchingPolicyPragma((TaskDispatchingPolicyPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__VOLATILE_PRAGMA:
				setVolatilePragma((VolatilePragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__VOLATILE_COMPONENTS_PRAGMA:
				setVolatileComponentsPragma((VolatileComponentsPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ASSERT_PRAGMA:
				setAssertPragma((AssertPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__ASSERTION_POLICY_PRAGMA:
				setAssertionPolicyPragma((AssertionPolicyPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__DETECT_BLOCKING_PRAGMA:
				setDetectBlockingPragma((DetectBlockingPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__NO_RETURN_PRAGMA:
				setNoReturnPragma((NoReturnPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA:
				setPartitionElaborationPolicyPragma((PartitionElaborationPolicyPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA:
				setPreelaborableInitializationPragma((PreelaborableInitializationPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				setPrioritySpecificDispatchingPragma((PrioritySpecificDispatchingPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__PROFILE_PRAGMA:
				setProfilePragma((ProfilePragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__RELATIVE_DEADLINE_PRAGMA:
				setRelativeDeadlinePragma((RelativeDeadlinePragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__UNCHECKED_UNION_PRAGMA:
				setUncheckedUnionPragma((UncheckedUnionPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__UNSUPPRESS_PRAGMA:
				setUnsuppressPragma((UnsuppressPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA:
				setDefaultStoragePoolPragma((DefaultStoragePoolPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__DISPATCHING_DOMAIN_PRAGMA:
				setDispatchingDomainPragma((DispatchingDomainPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__CPU_PRAGMA:
				setCpuPragma((CpuPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__INDEPENDENT_PRAGMA:
				setIndependentPragma((IndependentPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA:
				setIndependentComponentsPragma((IndependentComponentsPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA:
				setImplementationDefinedPragma((ImplementationDefinedPragma)null);
				return;
			case AdaPackage.DEFINITION_CLASS__UNKNOWN_PRAGMA:
				setUnknownPragma((UnknownPragma)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.DEFINITION_CLASS__NOT_AN_ELEMENT:
				return notAnElement != null;
			case AdaPackage.DEFINITION_CLASS__DERIVED_TYPE_DEFINITION:
				return derivedTypeDefinition != null;
			case AdaPackage.DEFINITION_CLASS__DERIVED_RECORD_EXTENSION_DEFINITION:
				return derivedRecordExtensionDefinition != null;
			case AdaPackage.DEFINITION_CLASS__ENUMERATION_TYPE_DEFINITION:
				return enumerationTypeDefinition != null;
			case AdaPackage.DEFINITION_CLASS__SIGNED_INTEGER_TYPE_DEFINITION:
				return signedIntegerTypeDefinition != null;
			case AdaPackage.DEFINITION_CLASS__MODULAR_TYPE_DEFINITION:
				return modularTypeDefinition != null;
			case AdaPackage.DEFINITION_CLASS__ROOT_INTEGER_DEFINITION:
				return rootIntegerDefinition != null;
			case AdaPackage.DEFINITION_CLASS__ROOT_REAL_DEFINITION:
				return rootRealDefinition != null;
			case AdaPackage.DEFINITION_CLASS__UNIVERSAL_INTEGER_DEFINITION:
				return universalIntegerDefinition != null;
			case AdaPackage.DEFINITION_CLASS__UNIVERSAL_REAL_DEFINITION:
				return universalRealDefinition != null;
			case AdaPackage.DEFINITION_CLASS__UNIVERSAL_FIXED_DEFINITION:
				return universalFixedDefinition != null;
			case AdaPackage.DEFINITION_CLASS__FLOATING_POINT_DEFINITION:
				return floatingPointDefinition != null;
			case AdaPackage.DEFINITION_CLASS__ORDINARY_FIXED_POINT_DEFINITION:
				return ordinaryFixedPointDefinition != null;
			case AdaPackage.DEFINITION_CLASS__DECIMAL_FIXED_POINT_DEFINITION:
				return decimalFixedPointDefinition != null;
			case AdaPackage.DEFINITION_CLASS__UNCONSTRAINED_ARRAY_DEFINITION:
				return unconstrainedArrayDefinition != null;
			case AdaPackage.DEFINITION_CLASS__CONSTRAINED_ARRAY_DEFINITION:
				return constrainedArrayDefinition != null;
			case AdaPackage.DEFINITION_CLASS__RECORD_TYPE_DEFINITION:
				return recordTypeDefinition != null;
			case AdaPackage.DEFINITION_CLASS__TAGGED_RECORD_TYPE_DEFINITION:
				return taggedRecordTypeDefinition != null;
			case AdaPackage.DEFINITION_CLASS__ORDINARY_INTERFACE:
				return ordinaryInterface != null;
			case AdaPackage.DEFINITION_CLASS__LIMITED_INTERFACE:
				return limitedInterface != null;
			case AdaPackage.DEFINITION_CLASS__TASK_INTERFACE:
				return taskInterface != null;
			case AdaPackage.DEFINITION_CLASS__PROTECTED_INTERFACE:
				return protectedInterface != null;
			case AdaPackage.DEFINITION_CLASS__SYNCHRONIZED_INTERFACE:
				return synchronizedInterface != null;
			case AdaPackage.DEFINITION_CLASS__POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				return poolSpecificAccessToVariable != null;
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_VARIABLE:
				return accessToVariable != null;
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_CONSTANT:
				return accessToConstant != null;
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROCEDURE:
				return accessToProcedure != null;
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROTECTED_PROCEDURE:
				return accessToProtectedProcedure != null;
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_FUNCTION:
				return accessToFunction != null;
			case AdaPackage.DEFINITION_CLASS__ACCESS_TO_PROTECTED_FUNCTION:
				return accessToProtectedFunction != null;
			case AdaPackage.DEFINITION_CLASS__SUBTYPE_INDICATION:
				return subtypeIndication != null;
			case AdaPackage.DEFINITION_CLASS__RANGE_ATTRIBUTE_REFERENCE:
				return rangeAttributeReference != null;
			case AdaPackage.DEFINITION_CLASS__SIMPLE_EXPRESSION_RANGE:
				return simpleExpressionRange != null;
			case AdaPackage.DEFINITION_CLASS__DIGITS_CONSTRAINT:
				return digitsConstraint != null;
			case AdaPackage.DEFINITION_CLASS__DELTA_CONSTRAINT:
				return deltaConstraint != null;
			case AdaPackage.DEFINITION_CLASS__INDEX_CONSTRAINT:
				return indexConstraint != null;
			case AdaPackage.DEFINITION_CLASS__DISCRIMINANT_CONSTRAINT:
				return discriminantConstraint != null;
			case AdaPackage.DEFINITION_CLASS__COMPONENT_DEFINITION:
				return componentDefinition != null;
			case AdaPackage.DEFINITION_CLASS__DISCRETE_SUBTYPE_INDICATION_AS_SUBTYPE_DEFINITION:
				return discreteSubtypeIndicationAsSubtypeDefinition != null;
			case AdaPackage.DEFINITION_CLASS__DISCRETE_RANGE_ATTRIBUTE_REFERENCE_AS_SUBTYPE_DEFINITION:
				return discreteRangeAttributeReferenceAsSubtypeDefinition != null;
			case AdaPackage.DEFINITION_CLASS__DISCRETE_SIMPLE_EXPRESSION_RANGE_AS_SUBTYPE_DEFINITION:
				return discreteSimpleExpressionRangeAsSubtypeDefinition != null;
			case AdaPackage.DEFINITION_CLASS__DISCRETE_SUBTYPE_INDICATION:
				return discreteSubtypeIndication != null;
			case AdaPackage.DEFINITION_CLASS__DISCRETE_RANGE_ATTRIBUTE_REFERENCE:
				return discreteRangeAttributeReference != null;
			case AdaPackage.DEFINITION_CLASS__DISCRETE_SIMPLE_EXPRESSION_RANGE:
				return discreteSimpleExpressionRange != null;
			case AdaPackage.DEFINITION_CLASS__UNKNOWN_DISCRIMINANT_PART:
				return unknownDiscriminantPart != null;
			case AdaPackage.DEFINITION_CLASS__KNOWN_DISCRIMINANT_PART:
				return knownDiscriminantPart != null;
			case AdaPackage.DEFINITION_CLASS__RECORD_DEFINITION:
				return recordDefinition != null;
			case AdaPackage.DEFINITION_CLASS__NULL_RECORD_DEFINITION:
				return nullRecordDefinition != null;
			case AdaPackage.DEFINITION_CLASS__NULL_COMPONENT:
				return nullComponent != null;
			case AdaPackage.DEFINITION_CLASS__VARIANT_PART:
				return variantPart != null;
			case AdaPackage.DEFINITION_CLASS__VARIANT:
				return variant != null;
			case AdaPackage.DEFINITION_CLASS__OTHERS_CHOICE:
				return othersChoice != null;
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_VARIABLE:
				return anonymousAccessToVariable != null;
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_CONSTANT:
				return anonymousAccessToConstant != null;
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROCEDURE:
				return anonymousAccessToProcedure != null;
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROTECTED_PROCEDURE:
				return anonymousAccessToProtectedProcedure != null;
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_FUNCTION:
				return anonymousAccessToFunction != null;
			case AdaPackage.DEFINITION_CLASS__ANONYMOUS_ACCESS_TO_PROTECTED_FUNCTION:
				return anonymousAccessToProtectedFunction != null;
			case AdaPackage.DEFINITION_CLASS__PRIVATE_TYPE_DEFINITION:
				return privateTypeDefinition != null;
			case AdaPackage.DEFINITION_CLASS__TAGGED_PRIVATE_TYPE_DEFINITION:
				return taggedPrivateTypeDefinition != null;
			case AdaPackage.DEFINITION_CLASS__PRIVATE_EXTENSION_DEFINITION:
				return privateExtensionDefinition != null;
			case AdaPackage.DEFINITION_CLASS__TASK_DEFINITION:
				return taskDefinition != null;
			case AdaPackage.DEFINITION_CLASS__PROTECTED_DEFINITION:
				return protectedDefinition != null;
			case AdaPackage.DEFINITION_CLASS__FORMAL_PRIVATE_TYPE_DEFINITION:
				return formalPrivateTypeDefinition != null;
			case AdaPackage.DEFINITION_CLASS__FORMAL_TAGGED_PRIVATE_TYPE_DEFINITION:
				return formalTaggedPrivateTypeDefinition != null;
			case AdaPackage.DEFINITION_CLASS__FORMAL_DERIVED_TYPE_DEFINITION:
				return formalDerivedTypeDefinition != null;
			case AdaPackage.DEFINITION_CLASS__FORMAL_DISCRETE_TYPE_DEFINITION:
				return formalDiscreteTypeDefinition != null;
			case AdaPackage.DEFINITION_CLASS__FORMAL_SIGNED_INTEGER_TYPE_DEFINITION:
				return formalSignedIntegerTypeDefinition != null;
			case AdaPackage.DEFINITION_CLASS__FORMAL_MODULAR_TYPE_DEFINITION:
				return formalModularTypeDefinition != null;
			case AdaPackage.DEFINITION_CLASS__FORMAL_FLOATING_POINT_DEFINITION:
				return formalFloatingPointDefinition != null;
			case AdaPackage.DEFINITION_CLASS__FORMAL_ORDINARY_FIXED_POINT_DEFINITION:
				return formalOrdinaryFixedPointDefinition != null;
			case AdaPackage.DEFINITION_CLASS__FORMAL_DECIMAL_FIXED_POINT_DEFINITION:
				return formalDecimalFixedPointDefinition != null;
			case AdaPackage.DEFINITION_CLASS__FORMAL_ORDINARY_INTERFACE:
				return formalOrdinaryInterface != null;
			case AdaPackage.DEFINITION_CLASS__FORMAL_LIMITED_INTERFACE:
				return formalLimitedInterface != null;
			case AdaPackage.DEFINITION_CLASS__FORMAL_TASK_INTERFACE:
				return formalTaskInterface != null;
			case AdaPackage.DEFINITION_CLASS__FORMAL_PROTECTED_INTERFACE:
				return formalProtectedInterface != null;
			case AdaPackage.DEFINITION_CLASS__FORMAL_SYNCHRONIZED_INTERFACE:
				return formalSynchronizedInterface != null;
			case AdaPackage.DEFINITION_CLASS__FORMAL_UNCONSTRAINED_ARRAY_DEFINITION:
				return formalUnconstrainedArrayDefinition != null;
			case AdaPackage.DEFINITION_CLASS__FORMAL_CONSTRAINED_ARRAY_DEFINITION:
				return formalConstrainedArrayDefinition != null;
			case AdaPackage.DEFINITION_CLASS__FORMAL_POOL_SPECIFIC_ACCESS_TO_VARIABLE:
				return formalPoolSpecificAccessToVariable != null;
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_VARIABLE:
				return formalAccessToVariable != null;
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_CONSTANT:
				return formalAccessToConstant != null;
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROCEDURE:
				return formalAccessToProcedure != null;
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROTECTED_PROCEDURE:
				return formalAccessToProtectedProcedure != null;
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_FUNCTION:
				return formalAccessToFunction != null;
			case AdaPackage.DEFINITION_CLASS__FORMAL_ACCESS_TO_PROTECTED_FUNCTION:
				return formalAccessToProtectedFunction != null;
			case AdaPackage.DEFINITION_CLASS__ASPECT_SPECIFICATION:
				return aspectSpecification != null;
			case AdaPackage.DEFINITION_CLASS__IDENTIFIER:
				return identifier != null;
			case AdaPackage.DEFINITION_CLASS__SELECTED_COMPONENT:
				return selectedComponent != null;
			case AdaPackage.DEFINITION_CLASS__BASE_ATTRIBUTE:
				return baseAttribute != null;
			case AdaPackage.DEFINITION_CLASS__CLASS_ATTRIBUTE:
				return classAttribute != null;
			case AdaPackage.DEFINITION_CLASS__COMMENT:
				return comment != null;
			case AdaPackage.DEFINITION_CLASS__ALL_CALLS_REMOTE_PRAGMA:
				return allCallsRemotePragma != null;
			case AdaPackage.DEFINITION_CLASS__ASYNCHRONOUS_PRAGMA:
				return asynchronousPragma != null;
			case AdaPackage.DEFINITION_CLASS__ATOMIC_PRAGMA:
				return atomicPragma != null;
			case AdaPackage.DEFINITION_CLASS__ATOMIC_COMPONENTS_PRAGMA:
				return atomicComponentsPragma != null;
			case AdaPackage.DEFINITION_CLASS__ATTACH_HANDLER_PRAGMA:
				return attachHandlerPragma != null;
			case AdaPackage.DEFINITION_CLASS__CONTROLLED_PRAGMA:
				return controlledPragma != null;
			case AdaPackage.DEFINITION_CLASS__CONVENTION_PRAGMA:
				return conventionPragma != null;
			case AdaPackage.DEFINITION_CLASS__DISCARD_NAMES_PRAGMA:
				return discardNamesPragma != null;
			case AdaPackage.DEFINITION_CLASS__ELABORATE_PRAGMA:
				return elaboratePragma != null;
			case AdaPackage.DEFINITION_CLASS__ELABORATE_ALL_PRAGMA:
				return elaborateAllPragma != null;
			case AdaPackage.DEFINITION_CLASS__ELABORATE_BODY_PRAGMA:
				return elaborateBodyPragma != null;
			case AdaPackage.DEFINITION_CLASS__EXPORT_PRAGMA:
				return exportPragma != null;
			case AdaPackage.DEFINITION_CLASS__IMPORT_PRAGMA:
				return importPragma != null;
			case AdaPackage.DEFINITION_CLASS__INLINE_PRAGMA:
				return inlinePragma != null;
			case AdaPackage.DEFINITION_CLASS__INSPECTION_POINT_PRAGMA:
				return inspectionPointPragma != null;
			case AdaPackage.DEFINITION_CLASS__INTERRUPT_HANDLER_PRAGMA:
				return interruptHandlerPragma != null;
			case AdaPackage.DEFINITION_CLASS__INTERRUPT_PRIORITY_PRAGMA:
				return interruptPriorityPragma != null;
			case AdaPackage.DEFINITION_CLASS__LINKER_OPTIONS_PRAGMA:
				return linkerOptionsPragma != null;
			case AdaPackage.DEFINITION_CLASS__LIST_PRAGMA:
				return listPragma != null;
			case AdaPackage.DEFINITION_CLASS__LOCKING_POLICY_PRAGMA:
				return lockingPolicyPragma != null;
			case AdaPackage.DEFINITION_CLASS__NORMALIZE_SCALARS_PRAGMA:
				return normalizeScalarsPragma != null;
			case AdaPackage.DEFINITION_CLASS__OPTIMIZE_PRAGMA:
				return optimizePragma != null;
			case AdaPackage.DEFINITION_CLASS__PACK_PRAGMA:
				return packPragma != null;
			case AdaPackage.DEFINITION_CLASS__PAGE_PRAGMA:
				return pagePragma != null;
			case AdaPackage.DEFINITION_CLASS__PREELABORATE_PRAGMA:
				return preelaboratePragma != null;
			case AdaPackage.DEFINITION_CLASS__PRIORITY_PRAGMA:
				return priorityPragma != null;
			case AdaPackage.DEFINITION_CLASS__PURE_PRAGMA:
				return purePragma != null;
			case AdaPackage.DEFINITION_CLASS__QUEUING_POLICY_PRAGMA:
				return queuingPolicyPragma != null;
			case AdaPackage.DEFINITION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA:
				return remoteCallInterfacePragma != null;
			case AdaPackage.DEFINITION_CLASS__REMOTE_TYPES_PRAGMA:
				return remoteTypesPragma != null;
			case AdaPackage.DEFINITION_CLASS__RESTRICTIONS_PRAGMA:
				return restrictionsPragma != null;
			case AdaPackage.DEFINITION_CLASS__REVIEWABLE_PRAGMA:
				return reviewablePragma != null;
			case AdaPackage.DEFINITION_CLASS__SHARED_PASSIVE_PRAGMA:
				return sharedPassivePragma != null;
			case AdaPackage.DEFINITION_CLASS__STORAGE_SIZE_PRAGMA:
				return storageSizePragma != null;
			case AdaPackage.DEFINITION_CLASS__SUPPRESS_PRAGMA:
				return suppressPragma != null;
			case AdaPackage.DEFINITION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA:
				return taskDispatchingPolicyPragma != null;
			case AdaPackage.DEFINITION_CLASS__VOLATILE_PRAGMA:
				return volatilePragma != null;
			case AdaPackage.DEFINITION_CLASS__VOLATILE_COMPONENTS_PRAGMA:
				return volatileComponentsPragma != null;
			case AdaPackage.DEFINITION_CLASS__ASSERT_PRAGMA:
				return assertPragma != null;
			case AdaPackage.DEFINITION_CLASS__ASSERTION_POLICY_PRAGMA:
				return assertionPolicyPragma != null;
			case AdaPackage.DEFINITION_CLASS__DETECT_BLOCKING_PRAGMA:
				return detectBlockingPragma != null;
			case AdaPackage.DEFINITION_CLASS__NO_RETURN_PRAGMA:
				return noReturnPragma != null;
			case AdaPackage.DEFINITION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA:
				return partitionElaborationPolicyPragma != null;
			case AdaPackage.DEFINITION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA:
				return preelaborableInitializationPragma != null;
			case AdaPackage.DEFINITION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return prioritySpecificDispatchingPragma != null;
			case AdaPackage.DEFINITION_CLASS__PROFILE_PRAGMA:
				return profilePragma != null;
			case AdaPackage.DEFINITION_CLASS__RELATIVE_DEADLINE_PRAGMA:
				return relativeDeadlinePragma != null;
			case AdaPackage.DEFINITION_CLASS__UNCHECKED_UNION_PRAGMA:
				return uncheckedUnionPragma != null;
			case AdaPackage.DEFINITION_CLASS__UNSUPPRESS_PRAGMA:
				return unsuppressPragma != null;
			case AdaPackage.DEFINITION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA:
				return defaultStoragePoolPragma != null;
			case AdaPackage.DEFINITION_CLASS__DISPATCHING_DOMAIN_PRAGMA:
				return dispatchingDomainPragma != null;
			case AdaPackage.DEFINITION_CLASS__CPU_PRAGMA:
				return cpuPragma != null;
			case AdaPackage.DEFINITION_CLASS__INDEPENDENT_PRAGMA:
				return independentPragma != null;
			case AdaPackage.DEFINITION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA:
				return independentComponentsPragma != null;
			case AdaPackage.DEFINITION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA:
				return implementationDefinedPragma != null;
			case AdaPackage.DEFINITION_CLASS__UNKNOWN_PRAGMA:
				return unknownPragma != null;
		}
		return super.eIsSet(featureID);
	}

} //DefinitionClassImpl
