/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.AnonymousAccessToVariable;
import Ada.ExpressionClass;
import Ada.HasNullExclusionQType7;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Anonymous Access To Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.AnonymousAccessToVariableImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.AnonymousAccessToVariableImpl#getHasNullExclusionQ <em>Has Null Exclusion Q</em>}</li>
 *   <li>{@link Ada.impl.AnonymousAccessToVariableImpl#getAnonymousAccessToObjectSubtypeMarkQ <em>Anonymous Access To Object Subtype Mark Q</em>}</li>
 *   <li>{@link Ada.impl.AnonymousAccessToVariableImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AnonymousAccessToVariableImpl extends MinimalEObjectImpl.Container implements AnonymousAccessToVariable {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getHasNullExclusionQ() <em>Has Null Exclusion Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasNullExclusionQ()
	 * @generated
	 * @ordered
	 */
	protected HasNullExclusionQType7 hasNullExclusionQ;

	/**
	 * The cached value of the '{@link #getAnonymousAccessToObjectSubtypeMarkQ() <em>Anonymous Access To Object Subtype Mark Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnonymousAccessToObjectSubtypeMarkQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass anonymousAccessToObjectSubtypeMarkQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnonymousAccessToVariableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getAnonymousAccessToVariable();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HasNullExclusionQType7 getHasNullExclusionQ() {
		return hasNullExclusionQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasNullExclusionQ(HasNullExclusionQType7 newHasNullExclusionQ, NotificationChain msgs) {
		HasNullExclusionQType7 oldHasNullExclusionQ = hasNullExclusionQ;
		hasNullExclusionQ = newHasNullExclusionQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__HAS_NULL_EXCLUSION_Q, oldHasNullExclusionQ, newHasNullExclusionQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasNullExclusionQ(HasNullExclusionQType7 newHasNullExclusionQ) {
		if (newHasNullExclusionQ != hasNullExclusionQ) {
			NotificationChain msgs = null;
			if (hasNullExclusionQ != null)
				msgs = ((InternalEObject)hasNullExclusionQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__HAS_NULL_EXCLUSION_Q, null, msgs);
			if (newHasNullExclusionQ != null)
				msgs = ((InternalEObject)newHasNullExclusionQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__HAS_NULL_EXCLUSION_Q, null, msgs);
			msgs = basicSetHasNullExclusionQ(newHasNullExclusionQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__HAS_NULL_EXCLUSION_Q, newHasNullExclusionQ, newHasNullExclusionQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getAnonymousAccessToObjectSubtypeMarkQ() {
		return anonymousAccessToObjectSubtypeMarkQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAnonymousAccessToObjectSubtypeMarkQ(ExpressionClass newAnonymousAccessToObjectSubtypeMarkQ, NotificationChain msgs) {
		ExpressionClass oldAnonymousAccessToObjectSubtypeMarkQ = anonymousAccessToObjectSubtypeMarkQ;
		anonymousAccessToObjectSubtypeMarkQ = newAnonymousAccessToObjectSubtypeMarkQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__ANONYMOUS_ACCESS_TO_OBJECT_SUBTYPE_MARK_Q, oldAnonymousAccessToObjectSubtypeMarkQ, newAnonymousAccessToObjectSubtypeMarkQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnonymousAccessToObjectSubtypeMarkQ(ExpressionClass newAnonymousAccessToObjectSubtypeMarkQ) {
		if (newAnonymousAccessToObjectSubtypeMarkQ != anonymousAccessToObjectSubtypeMarkQ) {
			NotificationChain msgs = null;
			if (anonymousAccessToObjectSubtypeMarkQ != null)
				msgs = ((InternalEObject)anonymousAccessToObjectSubtypeMarkQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__ANONYMOUS_ACCESS_TO_OBJECT_SUBTYPE_MARK_Q, null, msgs);
			if (newAnonymousAccessToObjectSubtypeMarkQ != null)
				msgs = ((InternalEObject)newAnonymousAccessToObjectSubtypeMarkQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__ANONYMOUS_ACCESS_TO_OBJECT_SUBTYPE_MARK_Q, null, msgs);
			msgs = basicSetAnonymousAccessToObjectSubtypeMarkQ(newAnonymousAccessToObjectSubtypeMarkQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__ANONYMOUS_ACCESS_TO_OBJECT_SUBTYPE_MARK_Q, newAnonymousAccessToObjectSubtypeMarkQ, newAnonymousAccessToObjectSubtypeMarkQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__HAS_NULL_EXCLUSION_Q:
				return basicSetHasNullExclusionQ(null, msgs);
			case AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__ANONYMOUS_ACCESS_TO_OBJECT_SUBTYPE_MARK_Q:
				return basicSetAnonymousAccessToObjectSubtypeMarkQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__SLOC:
				return getSloc();
			case AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__HAS_NULL_EXCLUSION_Q:
				return getHasNullExclusionQ();
			case AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__ANONYMOUS_ACCESS_TO_OBJECT_SUBTYPE_MARK_Q:
				return getAnonymousAccessToObjectSubtypeMarkQ();
			case AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__HAS_NULL_EXCLUSION_Q:
				setHasNullExclusionQ((HasNullExclusionQType7)newValue);
				return;
			case AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__ANONYMOUS_ACCESS_TO_OBJECT_SUBTYPE_MARK_Q:
				setAnonymousAccessToObjectSubtypeMarkQ((ExpressionClass)newValue);
				return;
			case AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__HAS_NULL_EXCLUSION_Q:
				setHasNullExclusionQ((HasNullExclusionQType7)null);
				return;
			case AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__ANONYMOUS_ACCESS_TO_OBJECT_SUBTYPE_MARK_Q:
				setAnonymousAccessToObjectSubtypeMarkQ((ExpressionClass)null);
				return;
			case AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__SLOC:
				return sloc != null;
			case AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__HAS_NULL_EXCLUSION_Q:
				return hasNullExclusionQ != null;
			case AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__ANONYMOUS_ACCESS_TO_OBJECT_SUBTYPE_MARK_Q:
				return anonymousAccessToObjectSubtypeMarkQ != null;
			case AdaPackage.ANONYMOUS_ACCESS_TO_VARIABLE__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //AnonymousAccessToVariableImpl
