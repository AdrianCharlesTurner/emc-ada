/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.AttributeDefinitionClause;
import Ada.ExpressionClass;
import Ada.NameClass;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attribute Definition Clause</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.AttributeDefinitionClauseImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.AttributeDefinitionClauseImpl#getRepresentationClauseNameQ <em>Representation Clause Name Q</em>}</li>
 *   <li>{@link Ada.impl.AttributeDefinitionClauseImpl#getRepresentationClauseExpressionQ <em>Representation Clause Expression Q</em>}</li>
 *   <li>{@link Ada.impl.AttributeDefinitionClauseImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AttributeDefinitionClauseImpl extends MinimalEObjectImpl.Container implements AttributeDefinitionClause {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getRepresentationClauseNameQ() <em>Representation Clause Name Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepresentationClauseNameQ()
	 * @generated
	 * @ordered
	 */
	protected NameClass representationClauseNameQ;

	/**
	 * The cached value of the '{@link #getRepresentationClauseExpressionQ() <em>Representation Clause Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepresentationClauseExpressionQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass representationClauseExpressionQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttributeDefinitionClauseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getAttributeDefinitionClause();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NameClass getRepresentationClauseNameQ() {
		return representationClauseNameQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRepresentationClauseNameQ(NameClass newRepresentationClauseNameQ, NotificationChain msgs) {
		NameClass oldRepresentationClauseNameQ = representationClauseNameQ;
		representationClauseNameQ = newRepresentationClauseNameQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__REPRESENTATION_CLAUSE_NAME_Q, oldRepresentationClauseNameQ, newRepresentationClauseNameQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRepresentationClauseNameQ(NameClass newRepresentationClauseNameQ) {
		if (newRepresentationClauseNameQ != representationClauseNameQ) {
			NotificationChain msgs = null;
			if (representationClauseNameQ != null)
				msgs = ((InternalEObject)representationClauseNameQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__REPRESENTATION_CLAUSE_NAME_Q, null, msgs);
			if (newRepresentationClauseNameQ != null)
				msgs = ((InternalEObject)newRepresentationClauseNameQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__REPRESENTATION_CLAUSE_NAME_Q, null, msgs);
			msgs = basicSetRepresentationClauseNameQ(newRepresentationClauseNameQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__REPRESENTATION_CLAUSE_NAME_Q, newRepresentationClauseNameQ, newRepresentationClauseNameQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getRepresentationClauseExpressionQ() {
		return representationClauseExpressionQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRepresentationClauseExpressionQ(ExpressionClass newRepresentationClauseExpressionQ, NotificationChain msgs) {
		ExpressionClass oldRepresentationClauseExpressionQ = representationClauseExpressionQ;
		representationClauseExpressionQ = newRepresentationClauseExpressionQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__REPRESENTATION_CLAUSE_EXPRESSION_Q, oldRepresentationClauseExpressionQ, newRepresentationClauseExpressionQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRepresentationClauseExpressionQ(ExpressionClass newRepresentationClauseExpressionQ) {
		if (newRepresentationClauseExpressionQ != representationClauseExpressionQ) {
			NotificationChain msgs = null;
			if (representationClauseExpressionQ != null)
				msgs = ((InternalEObject)representationClauseExpressionQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__REPRESENTATION_CLAUSE_EXPRESSION_Q, null, msgs);
			if (newRepresentationClauseExpressionQ != null)
				msgs = ((InternalEObject)newRepresentationClauseExpressionQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__REPRESENTATION_CLAUSE_EXPRESSION_Q, null, msgs);
			msgs = basicSetRepresentationClauseExpressionQ(newRepresentationClauseExpressionQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__REPRESENTATION_CLAUSE_EXPRESSION_Q, newRepresentationClauseExpressionQ, newRepresentationClauseExpressionQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__REPRESENTATION_CLAUSE_NAME_Q:
				return basicSetRepresentationClauseNameQ(null, msgs);
			case AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__REPRESENTATION_CLAUSE_EXPRESSION_Q:
				return basicSetRepresentationClauseExpressionQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__SLOC:
				return getSloc();
			case AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__REPRESENTATION_CLAUSE_NAME_Q:
				return getRepresentationClauseNameQ();
			case AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__REPRESENTATION_CLAUSE_EXPRESSION_Q:
				return getRepresentationClauseExpressionQ();
			case AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__REPRESENTATION_CLAUSE_NAME_Q:
				setRepresentationClauseNameQ((NameClass)newValue);
				return;
			case AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__REPRESENTATION_CLAUSE_EXPRESSION_Q:
				setRepresentationClauseExpressionQ((ExpressionClass)newValue);
				return;
			case AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__REPRESENTATION_CLAUSE_NAME_Q:
				setRepresentationClauseNameQ((NameClass)null);
				return;
			case AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__REPRESENTATION_CLAUSE_EXPRESSION_Q:
				setRepresentationClauseExpressionQ((ExpressionClass)null);
				return;
			case AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__SLOC:
				return sloc != null;
			case AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__REPRESENTATION_CLAUSE_NAME_Q:
				return representationClauseNameQ != null;
			case AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__REPRESENTATION_CLAUSE_EXPRESSION_Q:
				return representationClauseExpressionQ != null;
			case AdaPackage.ATTRIBUTE_DEFINITION_CLAUSE__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //AttributeDefinitionClauseImpl
