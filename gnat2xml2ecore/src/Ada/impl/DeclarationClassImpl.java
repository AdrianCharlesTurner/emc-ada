/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.AllCallsRemotePragma;
import Ada.AssertPragma;
import Ada.AssertionPolicyPragma;
import Ada.AsynchronousPragma;
import Ada.AtomicComponentsPragma;
import Ada.AtomicPragma;
import Ada.AttachHandlerPragma;
import Ada.ChoiceParameterSpecification;
import Ada.Comment;
import Ada.ComponentDeclaration;
import Ada.ConstantDeclaration;
import Ada.ControlledPragma;
import Ada.ConventionPragma;
import Ada.CpuPragma;
import Ada.DeclarationClass;
import Ada.DefaultStoragePoolPragma;
import Ada.DeferredConstantDeclaration;
import Ada.DetectBlockingPragma;
import Ada.DiscardNamesPragma;
import Ada.DiscriminantSpecification;
import Ada.DispatchingDomainPragma;
import Ada.ElaborateAllPragma;
import Ada.ElaborateBodyPragma;
import Ada.ElaboratePragma;
import Ada.ElementIteratorSpecification;
import Ada.EntryBodyDeclaration;
import Ada.EntryDeclaration;
import Ada.EntryIndexSpecification;
import Ada.EnumerationLiteralSpecification;
import Ada.ExceptionDeclaration;
import Ada.ExceptionRenamingDeclaration;
import Ada.ExportPragma;
import Ada.ExpressionFunctionDeclaration;
import Ada.FormalFunctionDeclaration;
import Ada.FormalIncompleteTypeDeclaration;
import Ada.FormalObjectDeclaration;
import Ada.FormalPackageDeclaration;
import Ada.FormalPackageDeclarationWithBox;
import Ada.FormalProcedureDeclaration;
import Ada.FormalTypeDeclaration;
import Ada.FunctionBodyDeclaration;
import Ada.FunctionBodyStub;
import Ada.FunctionDeclaration;
import Ada.FunctionInstantiation;
import Ada.FunctionRenamingDeclaration;
import Ada.GeneralizedIteratorSpecification;
import Ada.GenericFunctionDeclaration;
import Ada.GenericFunctionRenamingDeclaration;
import Ada.GenericPackageDeclaration;
import Ada.GenericPackageRenamingDeclaration;
import Ada.GenericProcedureDeclaration;
import Ada.GenericProcedureRenamingDeclaration;
import Ada.ImplementationDefinedPragma;
import Ada.ImportPragma;
import Ada.IncompleteTypeDeclaration;
import Ada.IndependentComponentsPragma;
import Ada.IndependentPragma;
import Ada.InlinePragma;
import Ada.InspectionPointPragma;
import Ada.IntegerNumberDeclaration;
import Ada.InterruptHandlerPragma;
import Ada.InterruptPriorityPragma;
import Ada.LinkerOptionsPragma;
import Ada.ListPragma;
import Ada.LockingPolicyPragma;
import Ada.LoopParameterSpecification;
import Ada.NoReturnPragma;
import Ada.NormalizeScalarsPragma;
import Ada.NotAnElement;
import Ada.NullProcedureDeclaration;
import Ada.ObjectRenamingDeclaration;
import Ada.OptimizePragma;
import Ada.OrdinaryTypeDeclaration;
import Ada.PackPragma;
import Ada.PackageBodyDeclaration;
import Ada.PackageBodyStub;
import Ada.PackageDeclaration;
import Ada.PackageInstantiation;
import Ada.PackageRenamingDeclaration;
import Ada.PagePragma;
import Ada.ParameterSpecification;
import Ada.PartitionElaborationPolicyPragma;
import Ada.PreelaborableInitializationPragma;
import Ada.PreelaboratePragma;
import Ada.PriorityPragma;
import Ada.PrioritySpecificDispatchingPragma;
import Ada.PrivateExtensionDeclaration;
import Ada.PrivateTypeDeclaration;
import Ada.ProcedureBodyDeclaration;
import Ada.ProcedureBodyStub;
import Ada.ProcedureDeclaration;
import Ada.ProcedureInstantiation;
import Ada.ProcedureRenamingDeclaration;
import Ada.ProfilePragma;
import Ada.ProtectedBodyDeclaration;
import Ada.ProtectedBodyStub;
import Ada.ProtectedTypeDeclaration;
import Ada.PurePragma;
import Ada.QueuingPolicyPragma;
import Ada.RealNumberDeclaration;
import Ada.RelativeDeadlinePragma;
import Ada.RemoteCallInterfacePragma;
import Ada.RemoteTypesPragma;
import Ada.RestrictionsPragma;
import Ada.ReturnConstantSpecification;
import Ada.ReturnVariableSpecification;
import Ada.ReviewablePragma;
import Ada.SharedPassivePragma;
import Ada.SingleProtectedDeclaration;
import Ada.SingleTaskDeclaration;
import Ada.StorageSizePragma;
import Ada.SubtypeDeclaration;
import Ada.SuppressPragma;
import Ada.TaggedIncompleteTypeDeclaration;
import Ada.TaskBodyDeclaration;
import Ada.TaskBodyStub;
import Ada.TaskDispatchingPolicyPragma;
import Ada.TaskTypeDeclaration;
import Ada.UncheckedUnionPragma;
import Ada.UnknownPragma;
import Ada.UnsuppressPragma;
import Ada.VariableDeclaration;
import Ada.VolatileComponentsPragma;
import Ada.VolatilePragma;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Declaration Class</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getNotAnElement <em>Not An Element</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getOrdinaryTypeDeclaration <em>Ordinary Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getTaskTypeDeclaration <em>Task Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getProtectedTypeDeclaration <em>Protected Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getIncompleteTypeDeclaration <em>Incomplete Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getTaggedIncompleteTypeDeclaration <em>Tagged Incomplete Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getPrivateTypeDeclaration <em>Private Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getPrivateExtensionDeclaration <em>Private Extension Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getSubtypeDeclaration <em>Subtype Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getVariableDeclaration <em>Variable Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getConstantDeclaration <em>Constant Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getDeferredConstantDeclaration <em>Deferred Constant Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getSingleTaskDeclaration <em>Single Task Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getSingleProtectedDeclaration <em>Single Protected Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getIntegerNumberDeclaration <em>Integer Number Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getRealNumberDeclaration <em>Real Number Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getEnumerationLiteralSpecification <em>Enumeration Literal Specification</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getDiscriminantSpecification <em>Discriminant Specification</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getComponentDeclaration <em>Component Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getLoopParameterSpecification <em>Loop Parameter Specification</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getGeneralizedIteratorSpecification <em>Generalized Iterator Specification</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getElementIteratorSpecification <em>Element Iterator Specification</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getProcedureDeclaration <em>Procedure Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getFunctionDeclaration <em>Function Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getParameterSpecification <em>Parameter Specification</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getProcedureBodyDeclaration <em>Procedure Body Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getFunctionBodyDeclaration <em>Function Body Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getReturnVariableSpecification <em>Return Variable Specification</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getReturnConstantSpecification <em>Return Constant Specification</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getNullProcedureDeclaration <em>Null Procedure Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getExpressionFunctionDeclaration <em>Expression Function Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getPackageDeclaration <em>Package Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getPackageBodyDeclaration <em>Package Body Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getObjectRenamingDeclaration <em>Object Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getExceptionRenamingDeclaration <em>Exception Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getPackageRenamingDeclaration <em>Package Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getProcedureRenamingDeclaration <em>Procedure Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getFunctionRenamingDeclaration <em>Function Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getGenericPackageRenamingDeclaration <em>Generic Package Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getGenericProcedureRenamingDeclaration <em>Generic Procedure Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getGenericFunctionRenamingDeclaration <em>Generic Function Renaming Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getTaskBodyDeclaration <em>Task Body Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getProtectedBodyDeclaration <em>Protected Body Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getEntryDeclaration <em>Entry Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getEntryBodyDeclaration <em>Entry Body Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getEntryIndexSpecification <em>Entry Index Specification</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getProcedureBodyStub <em>Procedure Body Stub</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getFunctionBodyStub <em>Function Body Stub</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getPackageBodyStub <em>Package Body Stub</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getTaskBodyStub <em>Task Body Stub</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getProtectedBodyStub <em>Protected Body Stub</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getExceptionDeclaration <em>Exception Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getChoiceParameterSpecification <em>Choice Parameter Specification</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getGenericProcedureDeclaration <em>Generic Procedure Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getGenericFunctionDeclaration <em>Generic Function Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getGenericPackageDeclaration <em>Generic Package Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getPackageInstantiation <em>Package Instantiation</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getProcedureInstantiation <em>Procedure Instantiation</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getFunctionInstantiation <em>Function Instantiation</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getFormalObjectDeclaration <em>Formal Object Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getFormalTypeDeclaration <em>Formal Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getFormalIncompleteTypeDeclaration <em>Formal Incomplete Type Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getFormalProcedureDeclaration <em>Formal Procedure Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getFormalFunctionDeclaration <em>Formal Function Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getFormalPackageDeclaration <em>Formal Package Declaration</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getFormalPackageDeclarationWithBox <em>Formal Package Declaration With Box</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getAsynchronousPragma <em>Asynchronous Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getAtomicPragma <em>Atomic Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getAttachHandlerPragma <em>Attach Handler Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getControlledPragma <em>Controlled Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getConventionPragma <em>Convention Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getDiscardNamesPragma <em>Discard Names Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getElaboratePragma <em>Elaborate Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getElaborateAllPragma <em>Elaborate All Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getExportPragma <em>Export Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getImportPragma <em>Import Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getInlinePragma <em>Inline Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getInspectionPointPragma <em>Inspection Point Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getLinkerOptionsPragma <em>Linker Options Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getListPragma <em>List Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getLockingPolicyPragma <em>Locking Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getOptimizePragma <em>Optimize Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getPackPragma <em>Pack Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getPagePragma <em>Page Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getPreelaboratePragma <em>Preelaborate Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getPriorityPragma <em>Priority Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getPurePragma <em>Pure Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getRemoteTypesPragma <em>Remote Types Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getRestrictionsPragma <em>Restrictions Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getReviewablePragma <em>Reviewable Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getSharedPassivePragma <em>Shared Passive Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getStorageSizePragma <em>Storage Size Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getSuppressPragma <em>Suppress Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getVolatilePragma <em>Volatile Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getAssertPragma <em>Assert Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getNoReturnPragma <em>No Return Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getProfilePragma <em>Profile Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getUnsuppressPragma <em>Unsuppress Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getCpuPragma <em>Cpu Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getIndependentPragma <em>Independent Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getIndependentComponentsPragma <em>Independent Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}</li>
 *   <li>{@link Ada.impl.DeclarationClassImpl#getUnknownPragma <em>Unknown Pragma</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeclarationClassImpl extends MinimalEObjectImpl.Container implements DeclarationClass {
	/**
	 * The cached value of the '{@link #getNotAnElement() <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotAnElement()
	 * @generated
	 * @ordered
	 */
	protected NotAnElement notAnElement;

	/**
	 * The cached value of the '{@link #getOrdinaryTypeDeclaration() <em>Ordinary Type Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrdinaryTypeDeclaration()
	 * @generated
	 * @ordered
	 */
	protected OrdinaryTypeDeclaration ordinaryTypeDeclaration;

	/**
	 * The cached value of the '{@link #getTaskTypeDeclaration() <em>Task Type Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskTypeDeclaration()
	 * @generated
	 * @ordered
	 */
	protected TaskTypeDeclaration taskTypeDeclaration;

	/**
	 * The cached value of the '{@link #getProtectedTypeDeclaration() <em>Protected Type Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProtectedTypeDeclaration()
	 * @generated
	 * @ordered
	 */
	protected ProtectedTypeDeclaration protectedTypeDeclaration;

	/**
	 * The cached value of the '{@link #getIncompleteTypeDeclaration() <em>Incomplete Type Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncompleteTypeDeclaration()
	 * @generated
	 * @ordered
	 */
	protected IncompleteTypeDeclaration incompleteTypeDeclaration;

	/**
	 * The cached value of the '{@link #getTaggedIncompleteTypeDeclaration() <em>Tagged Incomplete Type Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaggedIncompleteTypeDeclaration()
	 * @generated
	 * @ordered
	 */
	protected TaggedIncompleteTypeDeclaration taggedIncompleteTypeDeclaration;

	/**
	 * The cached value of the '{@link #getPrivateTypeDeclaration() <em>Private Type Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrivateTypeDeclaration()
	 * @generated
	 * @ordered
	 */
	protected PrivateTypeDeclaration privateTypeDeclaration;

	/**
	 * The cached value of the '{@link #getPrivateExtensionDeclaration() <em>Private Extension Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrivateExtensionDeclaration()
	 * @generated
	 * @ordered
	 */
	protected PrivateExtensionDeclaration privateExtensionDeclaration;

	/**
	 * The cached value of the '{@link #getSubtypeDeclaration() <em>Subtype Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubtypeDeclaration()
	 * @generated
	 * @ordered
	 */
	protected SubtypeDeclaration subtypeDeclaration;

	/**
	 * The cached value of the '{@link #getVariableDeclaration() <em>Variable Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariableDeclaration()
	 * @generated
	 * @ordered
	 */
	protected VariableDeclaration variableDeclaration;

	/**
	 * The cached value of the '{@link #getConstantDeclaration() <em>Constant Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstantDeclaration()
	 * @generated
	 * @ordered
	 */
	protected ConstantDeclaration constantDeclaration;

	/**
	 * The cached value of the '{@link #getDeferredConstantDeclaration() <em>Deferred Constant Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeferredConstantDeclaration()
	 * @generated
	 * @ordered
	 */
	protected DeferredConstantDeclaration deferredConstantDeclaration;

	/**
	 * The cached value of the '{@link #getSingleTaskDeclaration() <em>Single Task Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSingleTaskDeclaration()
	 * @generated
	 * @ordered
	 */
	protected SingleTaskDeclaration singleTaskDeclaration;

	/**
	 * The cached value of the '{@link #getSingleProtectedDeclaration() <em>Single Protected Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSingleProtectedDeclaration()
	 * @generated
	 * @ordered
	 */
	protected SingleProtectedDeclaration singleProtectedDeclaration;

	/**
	 * The cached value of the '{@link #getIntegerNumberDeclaration() <em>Integer Number Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegerNumberDeclaration()
	 * @generated
	 * @ordered
	 */
	protected IntegerNumberDeclaration integerNumberDeclaration;

	/**
	 * The cached value of the '{@link #getRealNumberDeclaration() <em>Real Number Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRealNumberDeclaration()
	 * @generated
	 * @ordered
	 */
	protected RealNumberDeclaration realNumberDeclaration;

	/**
	 * The cached value of the '{@link #getEnumerationLiteralSpecification() <em>Enumeration Literal Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnumerationLiteralSpecification()
	 * @generated
	 * @ordered
	 */
	protected EnumerationLiteralSpecification enumerationLiteralSpecification;

	/**
	 * The cached value of the '{@link #getDiscriminantSpecification() <em>Discriminant Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiscriminantSpecification()
	 * @generated
	 * @ordered
	 */
	protected DiscriminantSpecification discriminantSpecification;

	/**
	 * The cached value of the '{@link #getComponentDeclaration() <em>Component Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponentDeclaration()
	 * @generated
	 * @ordered
	 */
	protected ComponentDeclaration componentDeclaration;

	/**
	 * The cached value of the '{@link #getLoopParameterSpecification() <em>Loop Parameter Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoopParameterSpecification()
	 * @generated
	 * @ordered
	 */
	protected LoopParameterSpecification loopParameterSpecification;

	/**
	 * The cached value of the '{@link #getGeneralizedIteratorSpecification() <em>Generalized Iterator Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGeneralizedIteratorSpecification()
	 * @generated
	 * @ordered
	 */
	protected GeneralizedIteratorSpecification generalizedIteratorSpecification;

	/**
	 * The cached value of the '{@link #getElementIteratorSpecification() <em>Element Iterator Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementIteratorSpecification()
	 * @generated
	 * @ordered
	 */
	protected ElementIteratorSpecification elementIteratorSpecification;

	/**
	 * The cached value of the '{@link #getProcedureDeclaration() <em>Procedure Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcedureDeclaration()
	 * @generated
	 * @ordered
	 */
	protected ProcedureDeclaration procedureDeclaration;

	/**
	 * The cached value of the '{@link #getFunctionDeclaration() <em>Function Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunctionDeclaration()
	 * @generated
	 * @ordered
	 */
	protected FunctionDeclaration functionDeclaration;

	/**
	 * The cached value of the '{@link #getParameterSpecification() <em>Parameter Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterSpecification()
	 * @generated
	 * @ordered
	 */
	protected ParameterSpecification parameterSpecification;

	/**
	 * The cached value of the '{@link #getProcedureBodyDeclaration() <em>Procedure Body Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcedureBodyDeclaration()
	 * @generated
	 * @ordered
	 */
	protected ProcedureBodyDeclaration procedureBodyDeclaration;

	/**
	 * The cached value of the '{@link #getFunctionBodyDeclaration() <em>Function Body Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunctionBodyDeclaration()
	 * @generated
	 * @ordered
	 */
	protected FunctionBodyDeclaration functionBodyDeclaration;

	/**
	 * The cached value of the '{@link #getReturnVariableSpecification() <em>Return Variable Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReturnVariableSpecification()
	 * @generated
	 * @ordered
	 */
	protected ReturnVariableSpecification returnVariableSpecification;

	/**
	 * The cached value of the '{@link #getReturnConstantSpecification() <em>Return Constant Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReturnConstantSpecification()
	 * @generated
	 * @ordered
	 */
	protected ReturnConstantSpecification returnConstantSpecification;

	/**
	 * The cached value of the '{@link #getNullProcedureDeclaration() <em>Null Procedure Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNullProcedureDeclaration()
	 * @generated
	 * @ordered
	 */
	protected NullProcedureDeclaration nullProcedureDeclaration;

	/**
	 * The cached value of the '{@link #getExpressionFunctionDeclaration() <em>Expression Function Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpressionFunctionDeclaration()
	 * @generated
	 * @ordered
	 */
	protected ExpressionFunctionDeclaration expressionFunctionDeclaration;

	/**
	 * The cached value of the '{@link #getPackageDeclaration() <em>Package Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackageDeclaration()
	 * @generated
	 * @ordered
	 */
	protected PackageDeclaration packageDeclaration;

	/**
	 * The cached value of the '{@link #getPackageBodyDeclaration() <em>Package Body Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackageBodyDeclaration()
	 * @generated
	 * @ordered
	 */
	protected PackageBodyDeclaration packageBodyDeclaration;

	/**
	 * The cached value of the '{@link #getObjectRenamingDeclaration() <em>Object Renaming Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectRenamingDeclaration()
	 * @generated
	 * @ordered
	 */
	protected ObjectRenamingDeclaration objectRenamingDeclaration;

	/**
	 * The cached value of the '{@link #getExceptionRenamingDeclaration() <em>Exception Renaming Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExceptionRenamingDeclaration()
	 * @generated
	 * @ordered
	 */
	protected ExceptionRenamingDeclaration exceptionRenamingDeclaration;

	/**
	 * The cached value of the '{@link #getPackageRenamingDeclaration() <em>Package Renaming Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackageRenamingDeclaration()
	 * @generated
	 * @ordered
	 */
	protected PackageRenamingDeclaration packageRenamingDeclaration;

	/**
	 * The cached value of the '{@link #getProcedureRenamingDeclaration() <em>Procedure Renaming Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcedureRenamingDeclaration()
	 * @generated
	 * @ordered
	 */
	protected ProcedureRenamingDeclaration procedureRenamingDeclaration;

	/**
	 * The cached value of the '{@link #getFunctionRenamingDeclaration() <em>Function Renaming Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunctionRenamingDeclaration()
	 * @generated
	 * @ordered
	 */
	protected FunctionRenamingDeclaration functionRenamingDeclaration;

	/**
	 * The cached value of the '{@link #getGenericPackageRenamingDeclaration() <em>Generic Package Renaming Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGenericPackageRenamingDeclaration()
	 * @generated
	 * @ordered
	 */
	protected GenericPackageRenamingDeclaration genericPackageRenamingDeclaration;

	/**
	 * The cached value of the '{@link #getGenericProcedureRenamingDeclaration() <em>Generic Procedure Renaming Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGenericProcedureRenamingDeclaration()
	 * @generated
	 * @ordered
	 */
	protected GenericProcedureRenamingDeclaration genericProcedureRenamingDeclaration;

	/**
	 * The cached value of the '{@link #getGenericFunctionRenamingDeclaration() <em>Generic Function Renaming Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGenericFunctionRenamingDeclaration()
	 * @generated
	 * @ordered
	 */
	protected GenericFunctionRenamingDeclaration genericFunctionRenamingDeclaration;

	/**
	 * The cached value of the '{@link #getTaskBodyDeclaration() <em>Task Body Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskBodyDeclaration()
	 * @generated
	 * @ordered
	 */
	protected TaskBodyDeclaration taskBodyDeclaration;

	/**
	 * The cached value of the '{@link #getProtectedBodyDeclaration() <em>Protected Body Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProtectedBodyDeclaration()
	 * @generated
	 * @ordered
	 */
	protected ProtectedBodyDeclaration protectedBodyDeclaration;

	/**
	 * The cached value of the '{@link #getEntryDeclaration() <em>Entry Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntryDeclaration()
	 * @generated
	 * @ordered
	 */
	protected EntryDeclaration entryDeclaration;

	/**
	 * The cached value of the '{@link #getEntryBodyDeclaration() <em>Entry Body Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntryBodyDeclaration()
	 * @generated
	 * @ordered
	 */
	protected EntryBodyDeclaration entryBodyDeclaration;

	/**
	 * The cached value of the '{@link #getEntryIndexSpecification() <em>Entry Index Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntryIndexSpecification()
	 * @generated
	 * @ordered
	 */
	protected EntryIndexSpecification entryIndexSpecification;

	/**
	 * The cached value of the '{@link #getProcedureBodyStub() <em>Procedure Body Stub</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcedureBodyStub()
	 * @generated
	 * @ordered
	 */
	protected ProcedureBodyStub procedureBodyStub;

	/**
	 * The cached value of the '{@link #getFunctionBodyStub() <em>Function Body Stub</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunctionBodyStub()
	 * @generated
	 * @ordered
	 */
	protected FunctionBodyStub functionBodyStub;

	/**
	 * The cached value of the '{@link #getPackageBodyStub() <em>Package Body Stub</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackageBodyStub()
	 * @generated
	 * @ordered
	 */
	protected PackageBodyStub packageBodyStub;

	/**
	 * The cached value of the '{@link #getTaskBodyStub() <em>Task Body Stub</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskBodyStub()
	 * @generated
	 * @ordered
	 */
	protected TaskBodyStub taskBodyStub;

	/**
	 * The cached value of the '{@link #getProtectedBodyStub() <em>Protected Body Stub</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProtectedBodyStub()
	 * @generated
	 * @ordered
	 */
	protected ProtectedBodyStub protectedBodyStub;

	/**
	 * The cached value of the '{@link #getExceptionDeclaration() <em>Exception Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExceptionDeclaration()
	 * @generated
	 * @ordered
	 */
	protected ExceptionDeclaration exceptionDeclaration;

	/**
	 * The cached value of the '{@link #getChoiceParameterSpecification() <em>Choice Parameter Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChoiceParameterSpecification()
	 * @generated
	 * @ordered
	 */
	protected ChoiceParameterSpecification choiceParameterSpecification;

	/**
	 * The cached value of the '{@link #getGenericProcedureDeclaration() <em>Generic Procedure Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGenericProcedureDeclaration()
	 * @generated
	 * @ordered
	 */
	protected GenericProcedureDeclaration genericProcedureDeclaration;

	/**
	 * The cached value of the '{@link #getGenericFunctionDeclaration() <em>Generic Function Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGenericFunctionDeclaration()
	 * @generated
	 * @ordered
	 */
	protected GenericFunctionDeclaration genericFunctionDeclaration;

	/**
	 * The cached value of the '{@link #getGenericPackageDeclaration() <em>Generic Package Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGenericPackageDeclaration()
	 * @generated
	 * @ordered
	 */
	protected GenericPackageDeclaration genericPackageDeclaration;

	/**
	 * The cached value of the '{@link #getPackageInstantiation() <em>Package Instantiation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackageInstantiation()
	 * @generated
	 * @ordered
	 */
	protected PackageInstantiation packageInstantiation;

	/**
	 * The cached value of the '{@link #getProcedureInstantiation() <em>Procedure Instantiation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcedureInstantiation()
	 * @generated
	 * @ordered
	 */
	protected ProcedureInstantiation procedureInstantiation;

	/**
	 * The cached value of the '{@link #getFunctionInstantiation() <em>Function Instantiation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunctionInstantiation()
	 * @generated
	 * @ordered
	 */
	protected FunctionInstantiation functionInstantiation;

	/**
	 * The cached value of the '{@link #getFormalObjectDeclaration() <em>Formal Object Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalObjectDeclaration()
	 * @generated
	 * @ordered
	 */
	protected FormalObjectDeclaration formalObjectDeclaration;

	/**
	 * The cached value of the '{@link #getFormalTypeDeclaration() <em>Formal Type Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalTypeDeclaration()
	 * @generated
	 * @ordered
	 */
	protected FormalTypeDeclaration formalTypeDeclaration;

	/**
	 * The cached value of the '{@link #getFormalIncompleteTypeDeclaration() <em>Formal Incomplete Type Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalIncompleteTypeDeclaration()
	 * @generated
	 * @ordered
	 */
	protected FormalIncompleteTypeDeclaration formalIncompleteTypeDeclaration;

	/**
	 * The cached value of the '{@link #getFormalProcedureDeclaration() <em>Formal Procedure Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalProcedureDeclaration()
	 * @generated
	 * @ordered
	 */
	protected FormalProcedureDeclaration formalProcedureDeclaration;

	/**
	 * The cached value of the '{@link #getFormalFunctionDeclaration() <em>Formal Function Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalFunctionDeclaration()
	 * @generated
	 * @ordered
	 */
	protected FormalFunctionDeclaration formalFunctionDeclaration;

	/**
	 * The cached value of the '{@link #getFormalPackageDeclaration() <em>Formal Package Declaration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalPackageDeclaration()
	 * @generated
	 * @ordered
	 */
	protected FormalPackageDeclaration formalPackageDeclaration;

	/**
	 * The cached value of the '{@link #getFormalPackageDeclarationWithBox() <em>Formal Package Declaration With Box</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalPackageDeclarationWithBox()
	 * @generated
	 * @ordered
	 */
	protected FormalPackageDeclarationWithBox formalPackageDeclarationWithBox;

	/**
	 * The cached value of the '{@link #getComment() <em>Comment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected Comment comment;

	/**
	 * The cached value of the '{@link #getAllCallsRemotePragma() <em>All Calls Remote Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllCallsRemotePragma()
	 * @generated
	 * @ordered
	 */
	protected AllCallsRemotePragma allCallsRemotePragma;

	/**
	 * The cached value of the '{@link #getAsynchronousPragma() <em>Asynchronous Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsynchronousPragma()
	 * @generated
	 * @ordered
	 */
	protected AsynchronousPragma asynchronousPragma;

	/**
	 * The cached value of the '{@link #getAtomicPragma() <em>Atomic Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAtomicPragma()
	 * @generated
	 * @ordered
	 */
	protected AtomicPragma atomicPragma;

	/**
	 * The cached value of the '{@link #getAtomicComponentsPragma() <em>Atomic Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAtomicComponentsPragma()
	 * @generated
	 * @ordered
	 */
	protected AtomicComponentsPragma atomicComponentsPragma;

	/**
	 * The cached value of the '{@link #getAttachHandlerPragma() <em>Attach Handler Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttachHandlerPragma()
	 * @generated
	 * @ordered
	 */
	protected AttachHandlerPragma attachHandlerPragma;

	/**
	 * The cached value of the '{@link #getControlledPragma() <em>Controlled Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getControlledPragma()
	 * @generated
	 * @ordered
	 */
	protected ControlledPragma controlledPragma;

	/**
	 * The cached value of the '{@link #getConventionPragma() <em>Convention Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConventionPragma()
	 * @generated
	 * @ordered
	 */
	protected ConventionPragma conventionPragma;

	/**
	 * The cached value of the '{@link #getDiscardNamesPragma() <em>Discard Names Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiscardNamesPragma()
	 * @generated
	 * @ordered
	 */
	protected DiscardNamesPragma discardNamesPragma;

	/**
	 * The cached value of the '{@link #getElaboratePragma() <em>Elaborate Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElaboratePragma()
	 * @generated
	 * @ordered
	 */
	protected ElaboratePragma elaboratePragma;

	/**
	 * The cached value of the '{@link #getElaborateAllPragma() <em>Elaborate All Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElaborateAllPragma()
	 * @generated
	 * @ordered
	 */
	protected ElaborateAllPragma elaborateAllPragma;

	/**
	 * The cached value of the '{@link #getElaborateBodyPragma() <em>Elaborate Body Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElaborateBodyPragma()
	 * @generated
	 * @ordered
	 */
	protected ElaborateBodyPragma elaborateBodyPragma;

	/**
	 * The cached value of the '{@link #getExportPragma() <em>Export Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExportPragma()
	 * @generated
	 * @ordered
	 */
	protected ExportPragma exportPragma;

	/**
	 * The cached value of the '{@link #getImportPragma() <em>Import Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImportPragma()
	 * @generated
	 * @ordered
	 */
	protected ImportPragma importPragma;

	/**
	 * The cached value of the '{@link #getInlinePragma() <em>Inline Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInlinePragma()
	 * @generated
	 * @ordered
	 */
	protected InlinePragma inlinePragma;

	/**
	 * The cached value of the '{@link #getInspectionPointPragma() <em>Inspection Point Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInspectionPointPragma()
	 * @generated
	 * @ordered
	 */
	protected InspectionPointPragma inspectionPointPragma;

	/**
	 * The cached value of the '{@link #getInterruptHandlerPragma() <em>Interrupt Handler Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterruptHandlerPragma()
	 * @generated
	 * @ordered
	 */
	protected InterruptHandlerPragma interruptHandlerPragma;

	/**
	 * The cached value of the '{@link #getInterruptPriorityPragma() <em>Interrupt Priority Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterruptPriorityPragma()
	 * @generated
	 * @ordered
	 */
	protected InterruptPriorityPragma interruptPriorityPragma;

	/**
	 * The cached value of the '{@link #getLinkerOptionsPragma() <em>Linker Options Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkerOptionsPragma()
	 * @generated
	 * @ordered
	 */
	protected LinkerOptionsPragma linkerOptionsPragma;

	/**
	 * The cached value of the '{@link #getListPragma() <em>List Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getListPragma()
	 * @generated
	 * @ordered
	 */
	protected ListPragma listPragma;

	/**
	 * The cached value of the '{@link #getLockingPolicyPragma() <em>Locking Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLockingPolicyPragma()
	 * @generated
	 * @ordered
	 */
	protected LockingPolicyPragma lockingPolicyPragma;

	/**
	 * The cached value of the '{@link #getNormalizeScalarsPragma() <em>Normalize Scalars Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNormalizeScalarsPragma()
	 * @generated
	 * @ordered
	 */
	protected NormalizeScalarsPragma normalizeScalarsPragma;

	/**
	 * The cached value of the '{@link #getOptimizePragma() <em>Optimize Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOptimizePragma()
	 * @generated
	 * @ordered
	 */
	protected OptimizePragma optimizePragma;

	/**
	 * The cached value of the '{@link #getPackPragma() <em>Pack Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackPragma()
	 * @generated
	 * @ordered
	 */
	protected PackPragma packPragma;

	/**
	 * The cached value of the '{@link #getPagePragma() <em>Page Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPagePragma()
	 * @generated
	 * @ordered
	 */
	protected PagePragma pagePragma;

	/**
	 * The cached value of the '{@link #getPreelaboratePragma() <em>Preelaborate Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreelaboratePragma()
	 * @generated
	 * @ordered
	 */
	protected PreelaboratePragma preelaboratePragma;

	/**
	 * The cached value of the '{@link #getPriorityPragma() <em>Priority Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPriorityPragma()
	 * @generated
	 * @ordered
	 */
	protected PriorityPragma priorityPragma;

	/**
	 * The cached value of the '{@link #getPurePragma() <em>Pure Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPurePragma()
	 * @generated
	 * @ordered
	 */
	protected PurePragma purePragma;

	/**
	 * The cached value of the '{@link #getQueuingPolicyPragma() <em>Queuing Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQueuingPolicyPragma()
	 * @generated
	 * @ordered
	 */
	protected QueuingPolicyPragma queuingPolicyPragma;

	/**
	 * The cached value of the '{@link #getRemoteCallInterfacePragma() <em>Remote Call Interface Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRemoteCallInterfacePragma()
	 * @generated
	 * @ordered
	 */
	protected RemoteCallInterfacePragma remoteCallInterfacePragma;

	/**
	 * The cached value of the '{@link #getRemoteTypesPragma() <em>Remote Types Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRemoteTypesPragma()
	 * @generated
	 * @ordered
	 */
	protected RemoteTypesPragma remoteTypesPragma;

	/**
	 * The cached value of the '{@link #getRestrictionsPragma() <em>Restrictions Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRestrictionsPragma()
	 * @generated
	 * @ordered
	 */
	protected RestrictionsPragma restrictionsPragma;

	/**
	 * The cached value of the '{@link #getReviewablePragma() <em>Reviewable Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReviewablePragma()
	 * @generated
	 * @ordered
	 */
	protected ReviewablePragma reviewablePragma;

	/**
	 * The cached value of the '{@link #getSharedPassivePragma() <em>Shared Passive Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSharedPassivePragma()
	 * @generated
	 * @ordered
	 */
	protected SharedPassivePragma sharedPassivePragma;

	/**
	 * The cached value of the '{@link #getStorageSizePragma() <em>Storage Size Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStorageSizePragma()
	 * @generated
	 * @ordered
	 */
	protected StorageSizePragma storageSizePragma;

	/**
	 * The cached value of the '{@link #getSuppressPragma() <em>Suppress Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuppressPragma()
	 * @generated
	 * @ordered
	 */
	protected SuppressPragma suppressPragma;

	/**
	 * The cached value of the '{@link #getTaskDispatchingPolicyPragma() <em>Task Dispatching Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskDispatchingPolicyPragma()
	 * @generated
	 * @ordered
	 */
	protected TaskDispatchingPolicyPragma taskDispatchingPolicyPragma;

	/**
	 * The cached value of the '{@link #getVolatilePragma() <em>Volatile Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVolatilePragma()
	 * @generated
	 * @ordered
	 */
	protected VolatilePragma volatilePragma;

	/**
	 * The cached value of the '{@link #getVolatileComponentsPragma() <em>Volatile Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVolatileComponentsPragma()
	 * @generated
	 * @ordered
	 */
	protected VolatileComponentsPragma volatileComponentsPragma;

	/**
	 * The cached value of the '{@link #getAssertPragma() <em>Assert Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssertPragma()
	 * @generated
	 * @ordered
	 */
	protected AssertPragma assertPragma;

	/**
	 * The cached value of the '{@link #getAssertionPolicyPragma() <em>Assertion Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssertionPolicyPragma()
	 * @generated
	 * @ordered
	 */
	protected AssertionPolicyPragma assertionPolicyPragma;

	/**
	 * The cached value of the '{@link #getDetectBlockingPragma() <em>Detect Blocking Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDetectBlockingPragma()
	 * @generated
	 * @ordered
	 */
	protected DetectBlockingPragma detectBlockingPragma;

	/**
	 * The cached value of the '{@link #getNoReturnPragma() <em>No Return Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNoReturnPragma()
	 * @generated
	 * @ordered
	 */
	protected NoReturnPragma noReturnPragma;

	/**
	 * The cached value of the '{@link #getPartitionElaborationPolicyPragma() <em>Partition Elaboration Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPartitionElaborationPolicyPragma()
	 * @generated
	 * @ordered
	 */
	protected PartitionElaborationPolicyPragma partitionElaborationPolicyPragma;

	/**
	 * The cached value of the '{@link #getPreelaborableInitializationPragma() <em>Preelaborable Initialization Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreelaborableInitializationPragma()
	 * @generated
	 * @ordered
	 */
	protected PreelaborableInitializationPragma preelaborableInitializationPragma;

	/**
	 * The cached value of the '{@link #getPrioritySpecificDispatchingPragma() <em>Priority Specific Dispatching Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrioritySpecificDispatchingPragma()
	 * @generated
	 * @ordered
	 */
	protected PrioritySpecificDispatchingPragma prioritySpecificDispatchingPragma;

	/**
	 * The cached value of the '{@link #getProfilePragma() <em>Profile Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProfilePragma()
	 * @generated
	 * @ordered
	 */
	protected ProfilePragma profilePragma;

	/**
	 * The cached value of the '{@link #getRelativeDeadlinePragma() <em>Relative Deadline Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelativeDeadlinePragma()
	 * @generated
	 * @ordered
	 */
	protected RelativeDeadlinePragma relativeDeadlinePragma;

	/**
	 * The cached value of the '{@link #getUncheckedUnionPragma() <em>Unchecked Union Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUncheckedUnionPragma()
	 * @generated
	 * @ordered
	 */
	protected UncheckedUnionPragma uncheckedUnionPragma;

	/**
	 * The cached value of the '{@link #getUnsuppressPragma() <em>Unsuppress Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnsuppressPragma()
	 * @generated
	 * @ordered
	 */
	protected UnsuppressPragma unsuppressPragma;

	/**
	 * The cached value of the '{@link #getDefaultStoragePoolPragma() <em>Default Storage Pool Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultStoragePoolPragma()
	 * @generated
	 * @ordered
	 */
	protected DefaultStoragePoolPragma defaultStoragePoolPragma;

	/**
	 * The cached value of the '{@link #getDispatchingDomainPragma() <em>Dispatching Domain Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDispatchingDomainPragma()
	 * @generated
	 * @ordered
	 */
	protected DispatchingDomainPragma dispatchingDomainPragma;

	/**
	 * The cached value of the '{@link #getCpuPragma() <em>Cpu Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCpuPragma()
	 * @generated
	 * @ordered
	 */
	protected CpuPragma cpuPragma;

	/**
	 * The cached value of the '{@link #getIndependentPragma() <em>Independent Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndependentPragma()
	 * @generated
	 * @ordered
	 */
	protected IndependentPragma independentPragma;

	/**
	 * The cached value of the '{@link #getIndependentComponentsPragma() <em>Independent Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndependentComponentsPragma()
	 * @generated
	 * @ordered
	 */
	protected IndependentComponentsPragma independentComponentsPragma;

	/**
	 * The cached value of the '{@link #getImplementationDefinedPragma() <em>Implementation Defined Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImplementationDefinedPragma()
	 * @generated
	 * @ordered
	 */
	protected ImplementationDefinedPragma implementationDefinedPragma;

	/**
	 * The cached value of the '{@link #getUnknownPragma() <em>Unknown Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnknownPragma()
	 * @generated
	 * @ordered
	 */
	protected UnknownPragma unknownPragma;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeclarationClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getDeclarationClass();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotAnElement getNotAnElement() {
		return notAnElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNotAnElement(NotAnElement newNotAnElement, NotificationChain msgs) {
		NotAnElement oldNotAnElement = notAnElement;
		notAnElement = newNotAnElement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__NOT_AN_ELEMENT, oldNotAnElement, newNotAnElement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotAnElement(NotAnElement newNotAnElement) {
		if (newNotAnElement != notAnElement) {
			NotificationChain msgs = null;
			if (notAnElement != null)
				msgs = ((InternalEObject)notAnElement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__NOT_AN_ELEMENT, null, msgs);
			if (newNotAnElement != null)
				msgs = ((InternalEObject)newNotAnElement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__NOT_AN_ELEMENT, null, msgs);
			msgs = basicSetNotAnElement(newNotAnElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__NOT_AN_ELEMENT, newNotAnElement, newNotAnElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrdinaryTypeDeclaration getOrdinaryTypeDeclaration() {
		return ordinaryTypeDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOrdinaryTypeDeclaration(OrdinaryTypeDeclaration newOrdinaryTypeDeclaration, NotificationChain msgs) {
		OrdinaryTypeDeclaration oldOrdinaryTypeDeclaration = ordinaryTypeDeclaration;
		ordinaryTypeDeclaration = newOrdinaryTypeDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ORDINARY_TYPE_DECLARATION, oldOrdinaryTypeDeclaration, newOrdinaryTypeDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrdinaryTypeDeclaration(OrdinaryTypeDeclaration newOrdinaryTypeDeclaration) {
		if (newOrdinaryTypeDeclaration != ordinaryTypeDeclaration) {
			NotificationChain msgs = null;
			if (ordinaryTypeDeclaration != null)
				msgs = ((InternalEObject)ordinaryTypeDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ORDINARY_TYPE_DECLARATION, null, msgs);
			if (newOrdinaryTypeDeclaration != null)
				msgs = ((InternalEObject)newOrdinaryTypeDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ORDINARY_TYPE_DECLARATION, null, msgs);
			msgs = basicSetOrdinaryTypeDeclaration(newOrdinaryTypeDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ORDINARY_TYPE_DECLARATION, newOrdinaryTypeDeclaration, newOrdinaryTypeDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskTypeDeclaration getTaskTypeDeclaration() {
		return taskTypeDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTaskTypeDeclaration(TaskTypeDeclaration newTaskTypeDeclaration, NotificationChain msgs) {
		TaskTypeDeclaration oldTaskTypeDeclaration = taskTypeDeclaration;
		taskTypeDeclaration = newTaskTypeDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__TASK_TYPE_DECLARATION, oldTaskTypeDeclaration, newTaskTypeDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaskTypeDeclaration(TaskTypeDeclaration newTaskTypeDeclaration) {
		if (newTaskTypeDeclaration != taskTypeDeclaration) {
			NotificationChain msgs = null;
			if (taskTypeDeclaration != null)
				msgs = ((InternalEObject)taskTypeDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__TASK_TYPE_DECLARATION, null, msgs);
			if (newTaskTypeDeclaration != null)
				msgs = ((InternalEObject)newTaskTypeDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__TASK_TYPE_DECLARATION, null, msgs);
			msgs = basicSetTaskTypeDeclaration(newTaskTypeDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__TASK_TYPE_DECLARATION, newTaskTypeDeclaration, newTaskTypeDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProtectedTypeDeclaration getProtectedTypeDeclaration() {
		return protectedTypeDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProtectedTypeDeclaration(ProtectedTypeDeclaration newProtectedTypeDeclaration, NotificationChain msgs) {
		ProtectedTypeDeclaration oldProtectedTypeDeclaration = protectedTypeDeclaration;
		protectedTypeDeclaration = newProtectedTypeDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PROTECTED_TYPE_DECLARATION, oldProtectedTypeDeclaration, newProtectedTypeDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProtectedTypeDeclaration(ProtectedTypeDeclaration newProtectedTypeDeclaration) {
		if (newProtectedTypeDeclaration != protectedTypeDeclaration) {
			NotificationChain msgs = null;
			if (protectedTypeDeclaration != null)
				msgs = ((InternalEObject)protectedTypeDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PROTECTED_TYPE_DECLARATION, null, msgs);
			if (newProtectedTypeDeclaration != null)
				msgs = ((InternalEObject)newProtectedTypeDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PROTECTED_TYPE_DECLARATION, null, msgs);
			msgs = basicSetProtectedTypeDeclaration(newProtectedTypeDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PROTECTED_TYPE_DECLARATION, newProtectedTypeDeclaration, newProtectedTypeDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IncompleteTypeDeclaration getIncompleteTypeDeclaration() {
		return incompleteTypeDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIncompleteTypeDeclaration(IncompleteTypeDeclaration newIncompleteTypeDeclaration, NotificationChain msgs) {
		IncompleteTypeDeclaration oldIncompleteTypeDeclaration = incompleteTypeDeclaration;
		incompleteTypeDeclaration = newIncompleteTypeDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__INCOMPLETE_TYPE_DECLARATION, oldIncompleteTypeDeclaration, newIncompleteTypeDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIncompleteTypeDeclaration(IncompleteTypeDeclaration newIncompleteTypeDeclaration) {
		if (newIncompleteTypeDeclaration != incompleteTypeDeclaration) {
			NotificationChain msgs = null;
			if (incompleteTypeDeclaration != null)
				msgs = ((InternalEObject)incompleteTypeDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__INCOMPLETE_TYPE_DECLARATION, null, msgs);
			if (newIncompleteTypeDeclaration != null)
				msgs = ((InternalEObject)newIncompleteTypeDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__INCOMPLETE_TYPE_DECLARATION, null, msgs);
			msgs = basicSetIncompleteTypeDeclaration(newIncompleteTypeDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__INCOMPLETE_TYPE_DECLARATION, newIncompleteTypeDeclaration, newIncompleteTypeDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaggedIncompleteTypeDeclaration getTaggedIncompleteTypeDeclaration() {
		return taggedIncompleteTypeDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTaggedIncompleteTypeDeclaration(TaggedIncompleteTypeDeclaration newTaggedIncompleteTypeDeclaration, NotificationChain msgs) {
		TaggedIncompleteTypeDeclaration oldTaggedIncompleteTypeDeclaration = taggedIncompleteTypeDeclaration;
		taggedIncompleteTypeDeclaration = newTaggedIncompleteTypeDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__TAGGED_INCOMPLETE_TYPE_DECLARATION, oldTaggedIncompleteTypeDeclaration, newTaggedIncompleteTypeDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaggedIncompleteTypeDeclaration(TaggedIncompleteTypeDeclaration newTaggedIncompleteTypeDeclaration) {
		if (newTaggedIncompleteTypeDeclaration != taggedIncompleteTypeDeclaration) {
			NotificationChain msgs = null;
			if (taggedIncompleteTypeDeclaration != null)
				msgs = ((InternalEObject)taggedIncompleteTypeDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__TAGGED_INCOMPLETE_TYPE_DECLARATION, null, msgs);
			if (newTaggedIncompleteTypeDeclaration != null)
				msgs = ((InternalEObject)newTaggedIncompleteTypeDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__TAGGED_INCOMPLETE_TYPE_DECLARATION, null, msgs);
			msgs = basicSetTaggedIncompleteTypeDeclaration(newTaggedIncompleteTypeDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__TAGGED_INCOMPLETE_TYPE_DECLARATION, newTaggedIncompleteTypeDeclaration, newTaggedIncompleteTypeDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrivateTypeDeclaration getPrivateTypeDeclaration() {
		return privateTypeDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrivateTypeDeclaration(PrivateTypeDeclaration newPrivateTypeDeclaration, NotificationChain msgs) {
		PrivateTypeDeclaration oldPrivateTypeDeclaration = privateTypeDeclaration;
		privateTypeDeclaration = newPrivateTypeDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PRIVATE_TYPE_DECLARATION, oldPrivateTypeDeclaration, newPrivateTypeDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrivateTypeDeclaration(PrivateTypeDeclaration newPrivateTypeDeclaration) {
		if (newPrivateTypeDeclaration != privateTypeDeclaration) {
			NotificationChain msgs = null;
			if (privateTypeDeclaration != null)
				msgs = ((InternalEObject)privateTypeDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PRIVATE_TYPE_DECLARATION, null, msgs);
			if (newPrivateTypeDeclaration != null)
				msgs = ((InternalEObject)newPrivateTypeDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PRIVATE_TYPE_DECLARATION, null, msgs);
			msgs = basicSetPrivateTypeDeclaration(newPrivateTypeDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PRIVATE_TYPE_DECLARATION, newPrivateTypeDeclaration, newPrivateTypeDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrivateExtensionDeclaration getPrivateExtensionDeclaration() {
		return privateExtensionDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrivateExtensionDeclaration(PrivateExtensionDeclaration newPrivateExtensionDeclaration, NotificationChain msgs) {
		PrivateExtensionDeclaration oldPrivateExtensionDeclaration = privateExtensionDeclaration;
		privateExtensionDeclaration = newPrivateExtensionDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PRIVATE_EXTENSION_DECLARATION, oldPrivateExtensionDeclaration, newPrivateExtensionDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrivateExtensionDeclaration(PrivateExtensionDeclaration newPrivateExtensionDeclaration) {
		if (newPrivateExtensionDeclaration != privateExtensionDeclaration) {
			NotificationChain msgs = null;
			if (privateExtensionDeclaration != null)
				msgs = ((InternalEObject)privateExtensionDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PRIVATE_EXTENSION_DECLARATION, null, msgs);
			if (newPrivateExtensionDeclaration != null)
				msgs = ((InternalEObject)newPrivateExtensionDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PRIVATE_EXTENSION_DECLARATION, null, msgs);
			msgs = basicSetPrivateExtensionDeclaration(newPrivateExtensionDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PRIVATE_EXTENSION_DECLARATION, newPrivateExtensionDeclaration, newPrivateExtensionDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubtypeDeclaration getSubtypeDeclaration() {
		return subtypeDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSubtypeDeclaration(SubtypeDeclaration newSubtypeDeclaration, NotificationChain msgs) {
		SubtypeDeclaration oldSubtypeDeclaration = subtypeDeclaration;
		subtypeDeclaration = newSubtypeDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__SUBTYPE_DECLARATION, oldSubtypeDeclaration, newSubtypeDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubtypeDeclaration(SubtypeDeclaration newSubtypeDeclaration) {
		if (newSubtypeDeclaration != subtypeDeclaration) {
			NotificationChain msgs = null;
			if (subtypeDeclaration != null)
				msgs = ((InternalEObject)subtypeDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__SUBTYPE_DECLARATION, null, msgs);
			if (newSubtypeDeclaration != null)
				msgs = ((InternalEObject)newSubtypeDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__SUBTYPE_DECLARATION, null, msgs);
			msgs = basicSetSubtypeDeclaration(newSubtypeDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__SUBTYPE_DECLARATION, newSubtypeDeclaration, newSubtypeDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariableDeclaration getVariableDeclaration() {
		return variableDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVariableDeclaration(VariableDeclaration newVariableDeclaration, NotificationChain msgs) {
		VariableDeclaration oldVariableDeclaration = variableDeclaration;
		variableDeclaration = newVariableDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__VARIABLE_DECLARATION, oldVariableDeclaration, newVariableDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVariableDeclaration(VariableDeclaration newVariableDeclaration) {
		if (newVariableDeclaration != variableDeclaration) {
			NotificationChain msgs = null;
			if (variableDeclaration != null)
				msgs = ((InternalEObject)variableDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__VARIABLE_DECLARATION, null, msgs);
			if (newVariableDeclaration != null)
				msgs = ((InternalEObject)newVariableDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__VARIABLE_DECLARATION, null, msgs);
			msgs = basicSetVariableDeclaration(newVariableDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__VARIABLE_DECLARATION, newVariableDeclaration, newVariableDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstantDeclaration getConstantDeclaration() {
		return constantDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConstantDeclaration(ConstantDeclaration newConstantDeclaration, NotificationChain msgs) {
		ConstantDeclaration oldConstantDeclaration = constantDeclaration;
		constantDeclaration = newConstantDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__CONSTANT_DECLARATION, oldConstantDeclaration, newConstantDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstantDeclaration(ConstantDeclaration newConstantDeclaration) {
		if (newConstantDeclaration != constantDeclaration) {
			NotificationChain msgs = null;
			if (constantDeclaration != null)
				msgs = ((InternalEObject)constantDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__CONSTANT_DECLARATION, null, msgs);
			if (newConstantDeclaration != null)
				msgs = ((InternalEObject)newConstantDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__CONSTANT_DECLARATION, null, msgs);
			msgs = basicSetConstantDeclaration(newConstantDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__CONSTANT_DECLARATION, newConstantDeclaration, newConstantDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeferredConstantDeclaration getDeferredConstantDeclaration() {
		return deferredConstantDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDeferredConstantDeclaration(DeferredConstantDeclaration newDeferredConstantDeclaration, NotificationChain msgs) {
		DeferredConstantDeclaration oldDeferredConstantDeclaration = deferredConstantDeclaration;
		deferredConstantDeclaration = newDeferredConstantDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__DEFERRED_CONSTANT_DECLARATION, oldDeferredConstantDeclaration, newDeferredConstantDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeferredConstantDeclaration(DeferredConstantDeclaration newDeferredConstantDeclaration) {
		if (newDeferredConstantDeclaration != deferredConstantDeclaration) {
			NotificationChain msgs = null;
			if (deferredConstantDeclaration != null)
				msgs = ((InternalEObject)deferredConstantDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__DEFERRED_CONSTANT_DECLARATION, null, msgs);
			if (newDeferredConstantDeclaration != null)
				msgs = ((InternalEObject)newDeferredConstantDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__DEFERRED_CONSTANT_DECLARATION, null, msgs);
			msgs = basicSetDeferredConstantDeclaration(newDeferredConstantDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__DEFERRED_CONSTANT_DECLARATION, newDeferredConstantDeclaration, newDeferredConstantDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SingleTaskDeclaration getSingleTaskDeclaration() {
		return singleTaskDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSingleTaskDeclaration(SingleTaskDeclaration newSingleTaskDeclaration, NotificationChain msgs) {
		SingleTaskDeclaration oldSingleTaskDeclaration = singleTaskDeclaration;
		singleTaskDeclaration = newSingleTaskDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__SINGLE_TASK_DECLARATION, oldSingleTaskDeclaration, newSingleTaskDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSingleTaskDeclaration(SingleTaskDeclaration newSingleTaskDeclaration) {
		if (newSingleTaskDeclaration != singleTaskDeclaration) {
			NotificationChain msgs = null;
			if (singleTaskDeclaration != null)
				msgs = ((InternalEObject)singleTaskDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__SINGLE_TASK_DECLARATION, null, msgs);
			if (newSingleTaskDeclaration != null)
				msgs = ((InternalEObject)newSingleTaskDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__SINGLE_TASK_DECLARATION, null, msgs);
			msgs = basicSetSingleTaskDeclaration(newSingleTaskDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__SINGLE_TASK_DECLARATION, newSingleTaskDeclaration, newSingleTaskDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SingleProtectedDeclaration getSingleProtectedDeclaration() {
		return singleProtectedDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSingleProtectedDeclaration(SingleProtectedDeclaration newSingleProtectedDeclaration, NotificationChain msgs) {
		SingleProtectedDeclaration oldSingleProtectedDeclaration = singleProtectedDeclaration;
		singleProtectedDeclaration = newSingleProtectedDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__SINGLE_PROTECTED_DECLARATION, oldSingleProtectedDeclaration, newSingleProtectedDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSingleProtectedDeclaration(SingleProtectedDeclaration newSingleProtectedDeclaration) {
		if (newSingleProtectedDeclaration != singleProtectedDeclaration) {
			NotificationChain msgs = null;
			if (singleProtectedDeclaration != null)
				msgs = ((InternalEObject)singleProtectedDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__SINGLE_PROTECTED_DECLARATION, null, msgs);
			if (newSingleProtectedDeclaration != null)
				msgs = ((InternalEObject)newSingleProtectedDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__SINGLE_PROTECTED_DECLARATION, null, msgs);
			msgs = basicSetSingleProtectedDeclaration(newSingleProtectedDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__SINGLE_PROTECTED_DECLARATION, newSingleProtectedDeclaration, newSingleProtectedDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntegerNumberDeclaration getIntegerNumberDeclaration() {
		return integerNumberDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIntegerNumberDeclaration(IntegerNumberDeclaration newIntegerNumberDeclaration, NotificationChain msgs) {
		IntegerNumberDeclaration oldIntegerNumberDeclaration = integerNumberDeclaration;
		integerNumberDeclaration = newIntegerNumberDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__INTEGER_NUMBER_DECLARATION, oldIntegerNumberDeclaration, newIntegerNumberDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntegerNumberDeclaration(IntegerNumberDeclaration newIntegerNumberDeclaration) {
		if (newIntegerNumberDeclaration != integerNumberDeclaration) {
			NotificationChain msgs = null;
			if (integerNumberDeclaration != null)
				msgs = ((InternalEObject)integerNumberDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__INTEGER_NUMBER_DECLARATION, null, msgs);
			if (newIntegerNumberDeclaration != null)
				msgs = ((InternalEObject)newIntegerNumberDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__INTEGER_NUMBER_DECLARATION, null, msgs);
			msgs = basicSetIntegerNumberDeclaration(newIntegerNumberDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__INTEGER_NUMBER_DECLARATION, newIntegerNumberDeclaration, newIntegerNumberDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RealNumberDeclaration getRealNumberDeclaration() {
		return realNumberDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRealNumberDeclaration(RealNumberDeclaration newRealNumberDeclaration, NotificationChain msgs) {
		RealNumberDeclaration oldRealNumberDeclaration = realNumberDeclaration;
		realNumberDeclaration = newRealNumberDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__REAL_NUMBER_DECLARATION, oldRealNumberDeclaration, newRealNumberDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRealNumberDeclaration(RealNumberDeclaration newRealNumberDeclaration) {
		if (newRealNumberDeclaration != realNumberDeclaration) {
			NotificationChain msgs = null;
			if (realNumberDeclaration != null)
				msgs = ((InternalEObject)realNumberDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__REAL_NUMBER_DECLARATION, null, msgs);
			if (newRealNumberDeclaration != null)
				msgs = ((InternalEObject)newRealNumberDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__REAL_NUMBER_DECLARATION, null, msgs);
			msgs = basicSetRealNumberDeclaration(newRealNumberDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__REAL_NUMBER_DECLARATION, newRealNumberDeclaration, newRealNumberDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationLiteralSpecification getEnumerationLiteralSpecification() {
		return enumerationLiteralSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEnumerationLiteralSpecification(EnumerationLiteralSpecification newEnumerationLiteralSpecification, NotificationChain msgs) {
		EnumerationLiteralSpecification oldEnumerationLiteralSpecification = enumerationLiteralSpecification;
		enumerationLiteralSpecification = newEnumerationLiteralSpecification;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ENUMERATION_LITERAL_SPECIFICATION, oldEnumerationLiteralSpecification, newEnumerationLiteralSpecification);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnumerationLiteralSpecification(EnumerationLiteralSpecification newEnumerationLiteralSpecification) {
		if (newEnumerationLiteralSpecification != enumerationLiteralSpecification) {
			NotificationChain msgs = null;
			if (enumerationLiteralSpecification != null)
				msgs = ((InternalEObject)enumerationLiteralSpecification).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ENUMERATION_LITERAL_SPECIFICATION, null, msgs);
			if (newEnumerationLiteralSpecification != null)
				msgs = ((InternalEObject)newEnumerationLiteralSpecification).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ENUMERATION_LITERAL_SPECIFICATION, null, msgs);
			msgs = basicSetEnumerationLiteralSpecification(newEnumerationLiteralSpecification, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ENUMERATION_LITERAL_SPECIFICATION, newEnumerationLiteralSpecification, newEnumerationLiteralSpecification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscriminantSpecification getDiscriminantSpecification() {
		return discriminantSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscriminantSpecification(DiscriminantSpecification newDiscriminantSpecification, NotificationChain msgs) {
		DiscriminantSpecification oldDiscriminantSpecification = discriminantSpecification;
		discriminantSpecification = newDiscriminantSpecification;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__DISCRIMINANT_SPECIFICATION, oldDiscriminantSpecification, newDiscriminantSpecification);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscriminantSpecification(DiscriminantSpecification newDiscriminantSpecification) {
		if (newDiscriminantSpecification != discriminantSpecification) {
			NotificationChain msgs = null;
			if (discriminantSpecification != null)
				msgs = ((InternalEObject)discriminantSpecification).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__DISCRIMINANT_SPECIFICATION, null, msgs);
			if (newDiscriminantSpecification != null)
				msgs = ((InternalEObject)newDiscriminantSpecification).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__DISCRIMINANT_SPECIFICATION, null, msgs);
			msgs = basicSetDiscriminantSpecification(newDiscriminantSpecification, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__DISCRIMINANT_SPECIFICATION, newDiscriminantSpecification, newDiscriminantSpecification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentDeclaration getComponentDeclaration() {
		return componentDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComponentDeclaration(ComponentDeclaration newComponentDeclaration, NotificationChain msgs) {
		ComponentDeclaration oldComponentDeclaration = componentDeclaration;
		componentDeclaration = newComponentDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__COMPONENT_DECLARATION, oldComponentDeclaration, newComponentDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponentDeclaration(ComponentDeclaration newComponentDeclaration) {
		if (newComponentDeclaration != componentDeclaration) {
			NotificationChain msgs = null;
			if (componentDeclaration != null)
				msgs = ((InternalEObject)componentDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__COMPONENT_DECLARATION, null, msgs);
			if (newComponentDeclaration != null)
				msgs = ((InternalEObject)newComponentDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__COMPONENT_DECLARATION, null, msgs);
			msgs = basicSetComponentDeclaration(newComponentDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__COMPONENT_DECLARATION, newComponentDeclaration, newComponentDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LoopParameterSpecification getLoopParameterSpecification() {
		return loopParameterSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLoopParameterSpecification(LoopParameterSpecification newLoopParameterSpecification, NotificationChain msgs) {
		LoopParameterSpecification oldLoopParameterSpecification = loopParameterSpecification;
		loopParameterSpecification = newLoopParameterSpecification;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__LOOP_PARAMETER_SPECIFICATION, oldLoopParameterSpecification, newLoopParameterSpecification);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLoopParameterSpecification(LoopParameterSpecification newLoopParameterSpecification) {
		if (newLoopParameterSpecification != loopParameterSpecification) {
			NotificationChain msgs = null;
			if (loopParameterSpecification != null)
				msgs = ((InternalEObject)loopParameterSpecification).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__LOOP_PARAMETER_SPECIFICATION, null, msgs);
			if (newLoopParameterSpecification != null)
				msgs = ((InternalEObject)newLoopParameterSpecification).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__LOOP_PARAMETER_SPECIFICATION, null, msgs);
			msgs = basicSetLoopParameterSpecification(newLoopParameterSpecification, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__LOOP_PARAMETER_SPECIFICATION, newLoopParameterSpecification, newLoopParameterSpecification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GeneralizedIteratorSpecification getGeneralizedIteratorSpecification() {
		return generalizedIteratorSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGeneralizedIteratorSpecification(GeneralizedIteratorSpecification newGeneralizedIteratorSpecification, NotificationChain msgs) {
		GeneralizedIteratorSpecification oldGeneralizedIteratorSpecification = generalizedIteratorSpecification;
		generalizedIteratorSpecification = newGeneralizedIteratorSpecification;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__GENERALIZED_ITERATOR_SPECIFICATION, oldGeneralizedIteratorSpecification, newGeneralizedIteratorSpecification);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGeneralizedIteratorSpecification(GeneralizedIteratorSpecification newGeneralizedIteratorSpecification) {
		if (newGeneralizedIteratorSpecification != generalizedIteratorSpecification) {
			NotificationChain msgs = null;
			if (generalizedIteratorSpecification != null)
				msgs = ((InternalEObject)generalizedIteratorSpecification).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__GENERALIZED_ITERATOR_SPECIFICATION, null, msgs);
			if (newGeneralizedIteratorSpecification != null)
				msgs = ((InternalEObject)newGeneralizedIteratorSpecification).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__GENERALIZED_ITERATOR_SPECIFICATION, null, msgs);
			msgs = basicSetGeneralizedIteratorSpecification(newGeneralizedIteratorSpecification, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__GENERALIZED_ITERATOR_SPECIFICATION, newGeneralizedIteratorSpecification, newGeneralizedIteratorSpecification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementIteratorSpecification getElementIteratorSpecification() {
		return elementIteratorSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElementIteratorSpecification(ElementIteratorSpecification newElementIteratorSpecification, NotificationChain msgs) {
		ElementIteratorSpecification oldElementIteratorSpecification = elementIteratorSpecification;
		elementIteratorSpecification = newElementIteratorSpecification;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ELEMENT_ITERATOR_SPECIFICATION, oldElementIteratorSpecification, newElementIteratorSpecification);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElementIteratorSpecification(ElementIteratorSpecification newElementIteratorSpecification) {
		if (newElementIteratorSpecification != elementIteratorSpecification) {
			NotificationChain msgs = null;
			if (elementIteratorSpecification != null)
				msgs = ((InternalEObject)elementIteratorSpecification).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ELEMENT_ITERATOR_SPECIFICATION, null, msgs);
			if (newElementIteratorSpecification != null)
				msgs = ((InternalEObject)newElementIteratorSpecification).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ELEMENT_ITERATOR_SPECIFICATION, null, msgs);
			msgs = basicSetElementIteratorSpecification(newElementIteratorSpecification, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ELEMENT_ITERATOR_SPECIFICATION, newElementIteratorSpecification, newElementIteratorSpecification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureDeclaration getProcedureDeclaration() {
		return procedureDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcedureDeclaration(ProcedureDeclaration newProcedureDeclaration, NotificationChain msgs) {
		ProcedureDeclaration oldProcedureDeclaration = procedureDeclaration;
		procedureDeclaration = newProcedureDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PROCEDURE_DECLARATION, oldProcedureDeclaration, newProcedureDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcedureDeclaration(ProcedureDeclaration newProcedureDeclaration) {
		if (newProcedureDeclaration != procedureDeclaration) {
			NotificationChain msgs = null;
			if (procedureDeclaration != null)
				msgs = ((InternalEObject)procedureDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PROCEDURE_DECLARATION, null, msgs);
			if (newProcedureDeclaration != null)
				msgs = ((InternalEObject)newProcedureDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PROCEDURE_DECLARATION, null, msgs);
			msgs = basicSetProcedureDeclaration(newProcedureDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PROCEDURE_DECLARATION, newProcedureDeclaration, newProcedureDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionDeclaration getFunctionDeclaration() {
		return functionDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFunctionDeclaration(FunctionDeclaration newFunctionDeclaration, NotificationChain msgs) {
		FunctionDeclaration oldFunctionDeclaration = functionDeclaration;
		functionDeclaration = newFunctionDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__FUNCTION_DECLARATION, oldFunctionDeclaration, newFunctionDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFunctionDeclaration(FunctionDeclaration newFunctionDeclaration) {
		if (newFunctionDeclaration != functionDeclaration) {
			NotificationChain msgs = null;
			if (functionDeclaration != null)
				msgs = ((InternalEObject)functionDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__FUNCTION_DECLARATION, null, msgs);
			if (newFunctionDeclaration != null)
				msgs = ((InternalEObject)newFunctionDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__FUNCTION_DECLARATION, null, msgs);
			msgs = basicSetFunctionDeclaration(newFunctionDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__FUNCTION_DECLARATION, newFunctionDeclaration, newFunctionDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterSpecification getParameterSpecification() {
		return parameterSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParameterSpecification(ParameterSpecification newParameterSpecification, NotificationChain msgs) {
		ParameterSpecification oldParameterSpecification = parameterSpecification;
		parameterSpecification = newParameterSpecification;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PARAMETER_SPECIFICATION, oldParameterSpecification, newParameterSpecification);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterSpecification(ParameterSpecification newParameterSpecification) {
		if (newParameterSpecification != parameterSpecification) {
			NotificationChain msgs = null;
			if (parameterSpecification != null)
				msgs = ((InternalEObject)parameterSpecification).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PARAMETER_SPECIFICATION, null, msgs);
			if (newParameterSpecification != null)
				msgs = ((InternalEObject)newParameterSpecification).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PARAMETER_SPECIFICATION, null, msgs);
			msgs = basicSetParameterSpecification(newParameterSpecification, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PARAMETER_SPECIFICATION, newParameterSpecification, newParameterSpecification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureBodyDeclaration getProcedureBodyDeclaration() {
		return procedureBodyDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcedureBodyDeclaration(ProcedureBodyDeclaration newProcedureBodyDeclaration, NotificationChain msgs) {
		ProcedureBodyDeclaration oldProcedureBodyDeclaration = procedureBodyDeclaration;
		procedureBodyDeclaration = newProcedureBodyDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PROCEDURE_BODY_DECLARATION, oldProcedureBodyDeclaration, newProcedureBodyDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcedureBodyDeclaration(ProcedureBodyDeclaration newProcedureBodyDeclaration) {
		if (newProcedureBodyDeclaration != procedureBodyDeclaration) {
			NotificationChain msgs = null;
			if (procedureBodyDeclaration != null)
				msgs = ((InternalEObject)procedureBodyDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PROCEDURE_BODY_DECLARATION, null, msgs);
			if (newProcedureBodyDeclaration != null)
				msgs = ((InternalEObject)newProcedureBodyDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PROCEDURE_BODY_DECLARATION, null, msgs);
			msgs = basicSetProcedureBodyDeclaration(newProcedureBodyDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PROCEDURE_BODY_DECLARATION, newProcedureBodyDeclaration, newProcedureBodyDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionBodyDeclaration getFunctionBodyDeclaration() {
		return functionBodyDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFunctionBodyDeclaration(FunctionBodyDeclaration newFunctionBodyDeclaration, NotificationChain msgs) {
		FunctionBodyDeclaration oldFunctionBodyDeclaration = functionBodyDeclaration;
		functionBodyDeclaration = newFunctionBodyDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__FUNCTION_BODY_DECLARATION, oldFunctionBodyDeclaration, newFunctionBodyDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFunctionBodyDeclaration(FunctionBodyDeclaration newFunctionBodyDeclaration) {
		if (newFunctionBodyDeclaration != functionBodyDeclaration) {
			NotificationChain msgs = null;
			if (functionBodyDeclaration != null)
				msgs = ((InternalEObject)functionBodyDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__FUNCTION_BODY_DECLARATION, null, msgs);
			if (newFunctionBodyDeclaration != null)
				msgs = ((InternalEObject)newFunctionBodyDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__FUNCTION_BODY_DECLARATION, null, msgs);
			msgs = basicSetFunctionBodyDeclaration(newFunctionBodyDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__FUNCTION_BODY_DECLARATION, newFunctionBodyDeclaration, newFunctionBodyDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReturnVariableSpecification getReturnVariableSpecification() {
		return returnVariableSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReturnVariableSpecification(ReturnVariableSpecification newReturnVariableSpecification, NotificationChain msgs) {
		ReturnVariableSpecification oldReturnVariableSpecification = returnVariableSpecification;
		returnVariableSpecification = newReturnVariableSpecification;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__RETURN_VARIABLE_SPECIFICATION, oldReturnVariableSpecification, newReturnVariableSpecification);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReturnVariableSpecification(ReturnVariableSpecification newReturnVariableSpecification) {
		if (newReturnVariableSpecification != returnVariableSpecification) {
			NotificationChain msgs = null;
			if (returnVariableSpecification != null)
				msgs = ((InternalEObject)returnVariableSpecification).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__RETURN_VARIABLE_SPECIFICATION, null, msgs);
			if (newReturnVariableSpecification != null)
				msgs = ((InternalEObject)newReturnVariableSpecification).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__RETURN_VARIABLE_SPECIFICATION, null, msgs);
			msgs = basicSetReturnVariableSpecification(newReturnVariableSpecification, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__RETURN_VARIABLE_SPECIFICATION, newReturnVariableSpecification, newReturnVariableSpecification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReturnConstantSpecification getReturnConstantSpecification() {
		return returnConstantSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReturnConstantSpecification(ReturnConstantSpecification newReturnConstantSpecification, NotificationChain msgs) {
		ReturnConstantSpecification oldReturnConstantSpecification = returnConstantSpecification;
		returnConstantSpecification = newReturnConstantSpecification;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__RETURN_CONSTANT_SPECIFICATION, oldReturnConstantSpecification, newReturnConstantSpecification);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReturnConstantSpecification(ReturnConstantSpecification newReturnConstantSpecification) {
		if (newReturnConstantSpecification != returnConstantSpecification) {
			NotificationChain msgs = null;
			if (returnConstantSpecification != null)
				msgs = ((InternalEObject)returnConstantSpecification).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__RETURN_CONSTANT_SPECIFICATION, null, msgs);
			if (newReturnConstantSpecification != null)
				msgs = ((InternalEObject)newReturnConstantSpecification).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__RETURN_CONSTANT_SPECIFICATION, null, msgs);
			msgs = basicSetReturnConstantSpecification(newReturnConstantSpecification, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__RETURN_CONSTANT_SPECIFICATION, newReturnConstantSpecification, newReturnConstantSpecification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NullProcedureDeclaration getNullProcedureDeclaration() {
		return nullProcedureDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNullProcedureDeclaration(NullProcedureDeclaration newNullProcedureDeclaration, NotificationChain msgs) {
		NullProcedureDeclaration oldNullProcedureDeclaration = nullProcedureDeclaration;
		nullProcedureDeclaration = newNullProcedureDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__NULL_PROCEDURE_DECLARATION, oldNullProcedureDeclaration, newNullProcedureDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNullProcedureDeclaration(NullProcedureDeclaration newNullProcedureDeclaration) {
		if (newNullProcedureDeclaration != nullProcedureDeclaration) {
			NotificationChain msgs = null;
			if (nullProcedureDeclaration != null)
				msgs = ((InternalEObject)nullProcedureDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__NULL_PROCEDURE_DECLARATION, null, msgs);
			if (newNullProcedureDeclaration != null)
				msgs = ((InternalEObject)newNullProcedureDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__NULL_PROCEDURE_DECLARATION, null, msgs);
			msgs = basicSetNullProcedureDeclaration(newNullProcedureDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__NULL_PROCEDURE_DECLARATION, newNullProcedureDeclaration, newNullProcedureDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionFunctionDeclaration getExpressionFunctionDeclaration() {
		return expressionFunctionDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExpressionFunctionDeclaration(ExpressionFunctionDeclaration newExpressionFunctionDeclaration, NotificationChain msgs) {
		ExpressionFunctionDeclaration oldExpressionFunctionDeclaration = expressionFunctionDeclaration;
		expressionFunctionDeclaration = newExpressionFunctionDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__EXPRESSION_FUNCTION_DECLARATION, oldExpressionFunctionDeclaration, newExpressionFunctionDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpressionFunctionDeclaration(ExpressionFunctionDeclaration newExpressionFunctionDeclaration) {
		if (newExpressionFunctionDeclaration != expressionFunctionDeclaration) {
			NotificationChain msgs = null;
			if (expressionFunctionDeclaration != null)
				msgs = ((InternalEObject)expressionFunctionDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__EXPRESSION_FUNCTION_DECLARATION, null, msgs);
			if (newExpressionFunctionDeclaration != null)
				msgs = ((InternalEObject)newExpressionFunctionDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__EXPRESSION_FUNCTION_DECLARATION, null, msgs);
			msgs = basicSetExpressionFunctionDeclaration(newExpressionFunctionDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__EXPRESSION_FUNCTION_DECLARATION, newExpressionFunctionDeclaration, newExpressionFunctionDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackageDeclaration getPackageDeclaration() {
		return packageDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPackageDeclaration(PackageDeclaration newPackageDeclaration, NotificationChain msgs) {
		PackageDeclaration oldPackageDeclaration = packageDeclaration;
		packageDeclaration = newPackageDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PACKAGE_DECLARATION, oldPackageDeclaration, newPackageDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPackageDeclaration(PackageDeclaration newPackageDeclaration) {
		if (newPackageDeclaration != packageDeclaration) {
			NotificationChain msgs = null;
			if (packageDeclaration != null)
				msgs = ((InternalEObject)packageDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PACKAGE_DECLARATION, null, msgs);
			if (newPackageDeclaration != null)
				msgs = ((InternalEObject)newPackageDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PACKAGE_DECLARATION, null, msgs);
			msgs = basicSetPackageDeclaration(newPackageDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PACKAGE_DECLARATION, newPackageDeclaration, newPackageDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackageBodyDeclaration getPackageBodyDeclaration() {
		return packageBodyDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPackageBodyDeclaration(PackageBodyDeclaration newPackageBodyDeclaration, NotificationChain msgs) {
		PackageBodyDeclaration oldPackageBodyDeclaration = packageBodyDeclaration;
		packageBodyDeclaration = newPackageBodyDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PACKAGE_BODY_DECLARATION, oldPackageBodyDeclaration, newPackageBodyDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPackageBodyDeclaration(PackageBodyDeclaration newPackageBodyDeclaration) {
		if (newPackageBodyDeclaration != packageBodyDeclaration) {
			NotificationChain msgs = null;
			if (packageBodyDeclaration != null)
				msgs = ((InternalEObject)packageBodyDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PACKAGE_BODY_DECLARATION, null, msgs);
			if (newPackageBodyDeclaration != null)
				msgs = ((InternalEObject)newPackageBodyDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PACKAGE_BODY_DECLARATION, null, msgs);
			msgs = basicSetPackageBodyDeclaration(newPackageBodyDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PACKAGE_BODY_DECLARATION, newPackageBodyDeclaration, newPackageBodyDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectRenamingDeclaration getObjectRenamingDeclaration() {
		return objectRenamingDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetObjectRenamingDeclaration(ObjectRenamingDeclaration newObjectRenamingDeclaration, NotificationChain msgs) {
		ObjectRenamingDeclaration oldObjectRenamingDeclaration = objectRenamingDeclaration;
		objectRenamingDeclaration = newObjectRenamingDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__OBJECT_RENAMING_DECLARATION, oldObjectRenamingDeclaration, newObjectRenamingDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectRenamingDeclaration(ObjectRenamingDeclaration newObjectRenamingDeclaration) {
		if (newObjectRenamingDeclaration != objectRenamingDeclaration) {
			NotificationChain msgs = null;
			if (objectRenamingDeclaration != null)
				msgs = ((InternalEObject)objectRenamingDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__OBJECT_RENAMING_DECLARATION, null, msgs);
			if (newObjectRenamingDeclaration != null)
				msgs = ((InternalEObject)newObjectRenamingDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__OBJECT_RENAMING_DECLARATION, null, msgs);
			msgs = basicSetObjectRenamingDeclaration(newObjectRenamingDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__OBJECT_RENAMING_DECLARATION, newObjectRenamingDeclaration, newObjectRenamingDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExceptionRenamingDeclaration getExceptionRenamingDeclaration() {
		return exceptionRenamingDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExceptionRenamingDeclaration(ExceptionRenamingDeclaration newExceptionRenamingDeclaration, NotificationChain msgs) {
		ExceptionRenamingDeclaration oldExceptionRenamingDeclaration = exceptionRenamingDeclaration;
		exceptionRenamingDeclaration = newExceptionRenamingDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__EXCEPTION_RENAMING_DECLARATION, oldExceptionRenamingDeclaration, newExceptionRenamingDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExceptionRenamingDeclaration(ExceptionRenamingDeclaration newExceptionRenamingDeclaration) {
		if (newExceptionRenamingDeclaration != exceptionRenamingDeclaration) {
			NotificationChain msgs = null;
			if (exceptionRenamingDeclaration != null)
				msgs = ((InternalEObject)exceptionRenamingDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__EXCEPTION_RENAMING_DECLARATION, null, msgs);
			if (newExceptionRenamingDeclaration != null)
				msgs = ((InternalEObject)newExceptionRenamingDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__EXCEPTION_RENAMING_DECLARATION, null, msgs);
			msgs = basicSetExceptionRenamingDeclaration(newExceptionRenamingDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__EXCEPTION_RENAMING_DECLARATION, newExceptionRenamingDeclaration, newExceptionRenamingDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackageRenamingDeclaration getPackageRenamingDeclaration() {
		return packageRenamingDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPackageRenamingDeclaration(PackageRenamingDeclaration newPackageRenamingDeclaration, NotificationChain msgs) {
		PackageRenamingDeclaration oldPackageRenamingDeclaration = packageRenamingDeclaration;
		packageRenamingDeclaration = newPackageRenamingDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PACKAGE_RENAMING_DECLARATION, oldPackageRenamingDeclaration, newPackageRenamingDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPackageRenamingDeclaration(PackageRenamingDeclaration newPackageRenamingDeclaration) {
		if (newPackageRenamingDeclaration != packageRenamingDeclaration) {
			NotificationChain msgs = null;
			if (packageRenamingDeclaration != null)
				msgs = ((InternalEObject)packageRenamingDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PACKAGE_RENAMING_DECLARATION, null, msgs);
			if (newPackageRenamingDeclaration != null)
				msgs = ((InternalEObject)newPackageRenamingDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PACKAGE_RENAMING_DECLARATION, null, msgs);
			msgs = basicSetPackageRenamingDeclaration(newPackageRenamingDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PACKAGE_RENAMING_DECLARATION, newPackageRenamingDeclaration, newPackageRenamingDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureRenamingDeclaration getProcedureRenamingDeclaration() {
		return procedureRenamingDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcedureRenamingDeclaration(ProcedureRenamingDeclaration newProcedureRenamingDeclaration, NotificationChain msgs) {
		ProcedureRenamingDeclaration oldProcedureRenamingDeclaration = procedureRenamingDeclaration;
		procedureRenamingDeclaration = newProcedureRenamingDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PROCEDURE_RENAMING_DECLARATION, oldProcedureRenamingDeclaration, newProcedureRenamingDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcedureRenamingDeclaration(ProcedureRenamingDeclaration newProcedureRenamingDeclaration) {
		if (newProcedureRenamingDeclaration != procedureRenamingDeclaration) {
			NotificationChain msgs = null;
			if (procedureRenamingDeclaration != null)
				msgs = ((InternalEObject)procedureRenamingDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PROCEDURE_RENAMING_DECLARATION, null, msgs);
			if (newProcedureRenamingDeclaration != null)
				msgs = ((InternalEObject)newProcedureRenamingDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PROCEDURE_RENAMING_DECLARATION, null, msgs);
			msgs = basicSetProcedureRenamingDeclaration(newProcedureRenamingDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PROCEDURE_RENAMING_DECLARATION, newProcedureRenamingDeclaration, newProcedureRenamingDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionRenamingDeclaration getFunctionRenamingDeclaration() {
		return functionRenamingDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFunctionRenamingDeclaration(FunctionRenamingDeclaration newFunctionRenamingDeclaration, NotificationChain msgs) {
		FunctionRenamingDeclaration oldFunctionRenamingDeclaration = functionRenamingDeclaration;
		functionRenamingDeclaration = newFunctionRenamingDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__FUNCTION_RENAMING_DECLARATION, oldFunctionRenamingDeclaration, newFunctionRenamingDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFunctionRenamingDeclaration(FunctionRenamingDeclaration newFunctionRenamingDeclaration) {
		if (newFunctionRenamingDeclaration != functionRenamingDeclaration) {
			NotificationChain msgs = null;
			if (functionRenamingDeclaration != null)
				msgs = ((InternalEObject)functionRenamingDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__FUNCTION_RENAMING_DECLARATION, null, msgs);
			if (newFunctionRenamingDeclaration != null)
				msgs = ((InternalEObject)newFunctionRenamingDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__FUNCTION_RENAMING_DECLARATION, null, msgs);
			msgs = basicSetFunctionRenamingDeclaration(newFunctionRenamingDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__FUNCTION_RENAMING_DECLARATION, newFunctionRenamingDeclaration, newFunctionRenamingDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericPackageRenamingDeclaration getGenericPackageRenamingDeclaration() {
		return genericPackageRenamingDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGenericPackageRenamingDeclaration(GenericPackageRenamingDeclaration newGenericPackageRenamingDeclaration, NotificationChain msgs) {
		GenericPackageRenamingDeclaration oldGenericPackageRenamingDeclaration = genericPackageRenamingDeclaration;
		genericPackageRenamingDeclaration = newGenericPackageRenamingDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__GENERIC_PACKAGE_RENAMING_DECLARATION, oldGenericPackageRenamingDeclaration, newGenericPackageRenamingDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGenericPackageRenamingDeclaration(GenericPackageRenamingDeclaration newGenericPackageRenamingDeclaration) {
		if (newGenericPackageRenamingDeclaration != genericPackageRenamingDeclaration) {
			NotificationChain msgs = null;
			if (genericPackageRenamingDeclaration != null)
				msgs = ((InternalEObject)genericPackageRenamingDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__GENERIC_PACKAGE_RENAMING_DECLARATION, null, msgs);
			if (newGenericPackageRenamingDeclaration != null)
				msgs = ((InternalEObject)newGenericPackageRenamingDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__GENERIC_PACKAGE_RENAMING_DECLARATION, null, msgs);
			msgs = basicSetGenericPackageRenamingDeclaration(newGenericPackageRenamingDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__GENERIC_PACKAGE_RENAMING_DECLARATION, newGenericPackageRenamingDeclaration, newGenericPackageRenamingDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericProcedureRenamingDeclaration getGenericProcedureRenamingDeclaration() {
		return genericProcedureRenamingDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGenericProcedureRenamingDeclaration(GenericProcedureRenamingDeclaration newGenericProcedureRenamingDeclaration, NotificationChain msgs) {
		GenericProcedureRenamingDeclaration oldGenericProcedureRenamingDeclaration = genericProcedureRenamingDeclaration;
		genericProcedureRenamingDeclaration = newGenericProcedureRenamingDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__GENERIC_PROCEDURE_RENAMING_DECLARATION, oldGenericProcedureRenamingDeclaration, newGenericProcedureRenamingDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGenericProcedureRenamingDeclaration(GenericProcedureRenamingDeclaration newGenericProcedureRenamingDeclaration) {
		if (newGenericProcedureRenamingDeclaration != genericProcedureRenamingDeclaration) {
			NotificationChain msgs = null;
			if (genericProcedureRenamingDeclaration != null)
				msgs = ((InternalEObject)genericProcedureRenamingDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__GENERIC_PROCEDURE_RENAMING_DECLARATION, null, msgs);
			if (newGenericProcedureRenamingDeclaration != null)
				msgs = ((InternalEObject)newGenericProcedureRenamingDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__GENERIC_PROCEDURE_RENAMING_DECLARATION, null, msgs);
			msgs = basicSetGenericProcedureRenamingDeclaration(newGenericProcedureRenamingDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__GENERIC_PROCEDURE_RENAMING_DECLARATION, newGenericProcedureRenamingDeclaration, newGenericProcedureRenamingDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericFunctionRenamingDeclaration getGenericFunctionRenamingDeclaration() {
		return genericFunctionRenamingDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGenericFunctionRenamingDeclaration(GenericFunctionRenamingDeclaration newGenericFunctionRenamingDeclaration, NotificationChain msgs) {
		GenericFunctionRenamingDeclaration oldGenericFunctionRenamingDeclaration = genericFunctionRenamingDeclaration;
		genericFunctionRenamingDeclaration = newGenericFunctionRenamingDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__GENERIC_FUNCTION_RENAMING_DECLARATION, oldGenericFunctionRenamingDeclaration, newGenericFunctionRenamingDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGenericFunctionRenamingDeclaration(GenericFunctionRenamingDeclaration newGenericFunctionRenamingDeclaration) {
		if (newGenericFunctionRenamingDeclaration != genericFunctionRenamingDeclaration) {
			NotificationChain msgs = null;
			if (genericFunctionRenamingDeclaration != null)
				msgs = ((InternalEObject)genericFunctionRenamingDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__GENERIC_FUNCTION_RENAMING_DECLARATION, null, msgs);
			if (newGenericFunctionRenamingDeclaration != null)
				msgs = ((InternalEObject)newGenericFunctionRenamingDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__GENERIC_FUNCTION_RENAMING_DECLARATION, null, msgs);
			msgs = basicSetGenericFunctionRenamingDeclaration(newGenericFunctionRenamingDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__GENERIC_FUNCTION_RENAMING_DECLARATION, newGenericFunctionRenamingDeclaration, newGenericFunctionRenamingDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskBodyDeclaration getTaskBodyDeclaration() {
		return taskBodyDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTaskBodyDeclaration(TaskBodyDeclaration newTaskBodyDeclaration, NotificationChain msgs) {
		TaskBodyDeclaration oldTaskBodyDeclaration = taskBodyDeclaration;
		taskBodyDeclaration = newTaskBodyDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__TASK_BODY_DECLARATION, oldTaskBodyDeclaration, newTaskBodyDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaskBodyDeclaration(TaskBodyDeclaration newTaskBodyDeclaration) {
		if (newTaskBodyDeclaration != taskBodyDeclaration) {
			NotificationChain msgs = null;
			if (taskBodyDeclaration != null)
				msgs = ((InternalEObject)taskBodyDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__TASK_BODY_DECLARATION, null, msgs);
			if (newTaskBodyDeclaration != null)
				msgs = ((InternalEObject)newTaskBodyDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__TASK_BODY_DECLARATION, null, msgs);
			msgs = basicSetTaskBodyDeclaration(newTaskBodyDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__TASK_BODY_DECLARATION, newTaskBodyDeclaration, newTaskBodyDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProtectedBodyDeclaration getProtectedBodyDeclaration() {
		return protectedBodyDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProtectedBodyDeclaration(ProtectedBodyDeclaration newProtectedBodyDeclaration, NotificationChain msgs) {
		ProtectedBodyDeclaration oldProtectedBodyDeclaration = protectedBodyDeclaration;
		protectedBodyDeclaration = newProtectedBodyDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PROTECTED_BODY_DECLARATION, oldProtectedBodyDeclaration, newProtectedBodyDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProtectedBodyDeclaration(ProtectedBodyDeclaration newProtectedBodyDeclaration) {
		if (newProtectedBodyDeclaration != protectedBodyDeclaration) {
			NotificationChain msgs = null;
			if (protectedBodyDeclaration != null)
				msgs = ((InternalEObject)protectedBodyDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PROTECTED_BODY_DECLARATION, null, msgs);
			if (newProtectedBodyDeclaration != null)
				msgs = ((InternalEObject)newProtectedBodyDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PROTECTED_BODY_DECLARATION, null, msgs);
			msgs = basicSetProtectedBodyDeclaration(newProtectedBodyDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PROTECTED_BODY_DECLARATION, newProtectedBodyDeclaration, newProtectedBodyDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntryDeclaration getEntryDeclaration() {
		return entryDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEntryDeclaration(EntryDeclaration newEntryDeclaration, NotificationChain msgs) {
		EntryDeclaration oldEntryDeclaration = entryDeclaration;
		entryDeclaration = newEntryDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ENTRY_DECLARATION, oldEntryDeclaration, newEntryDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntryDeclaration(EntryDeclaration newEntryDeclaration) {
		if (newEntryDeclaration != entryDeclaration) {
			NotificationChain msgs = null;
			if (entryDeclaration != null)
				msgs = ((InternalEObject)entryDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ENTRY_DECLARATION, null, msgs);
			if (newEntryDeclaration != null)
				msgs = ((InternalEObject)newEntryDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ENTRY_DECLARATION, null, msgs);
			msgs = basicSetEntryDeclaration(newEntryDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ENTRY_DECLARATION, newEntryDeclaration, newEntryDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntryBodyDeclaration getEntryBodyDeclaration() {
		return entryBodyDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEntryBodyDeclaration(EntryBodyDeclaration newEntryBodyDeclaration, NotificationChain msgs) {
		EntryBodyDeclaration oldEntryBodyDeclaration = entryBodyDeclaration;
		entryBodyDeclaration = newEntryBodyDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ENTRY_BODY_DECLARATION, oldEntryBodyDeclaration, newEntryBodyDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntryBodyDeclaration(EntryBodyDeclaration newEntryBodyDeclaration) {
		if (newEntryBodyDeclaration != entryBodyDeclaration) {
			NotificationChain msgs = null;
			if (entryBodyDeclaration != null)
				msgs = ((InternalEObject)entryBodyDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ENTRY_BODY_DECLARATION, null, msgs);
			if (newEntryBodyDeclaration != null)
				msgs = ((InternalEObject)newEntryBodyDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ENTRY_BODY_DECLARATION, null, msgs);
			msgs = basicSetEntryBodyDeclaration(newEntryBodyDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ENTRY_BODY_DECLARATION, newEntryBodyDeclaration, newEntryBodyDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EntryIndexSpecification getEntryIndexSpecification() {
		return entryIndexSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEntryIndexSpecification(EntryIndexSpecification newEntryIndexSpecification, NotificationChain msgs) {
		EntryIndexSpecification oldEntryIndexSpecification = entryIndexSpecification;
		entryIndexSpecification = newEntryIndexSpecification;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ENTRY_INDEX_SPECIFICATION, oldEntryIndexSpecification, newEntryIndexSpecification);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntryIndexSpecification(EntryIndexSpecification newEntryIndexSpecification) {
		if (newEntryIndexSpecification != entryIndexSpecification) {
			NotificationChain msgs = null;
			if (entryIndexSpecification != null)
				msgs = ((InternalEObject)entryIndexSpecification).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ENTRY_INDEX_SPECIFICATION, null, msgs);
			if (newEntryIndexSpecification != null)
				msgs = ((InternalEObject)newEntryIndexSpecification).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ENTRY_INDEX_SPECIFICATION, null, msgs);
			msgs = basicSetEntryIndexSpecification(newEntryIndexSpecification, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ENTRY_INDEX_SPECIFICATION, newEntryIndexSpecification, newEntryIndexSpecification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureBodyStub getProcedureBodyStub() {
		return procedureBodyStub;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcedureBodyStub(ProcedureBodyStub newProcedureBodyStub, NotificationChain msgs) {
		ProcedureBodyStub oldProcedureBodyStub = procedureBodyStub;
		procedureBodyStub = newProcedureBodyStub;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PROCEDURE_BODY_STUB, oldProcedureBodyStub, newProcedureBodyStub);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcedureBodyStub(ProcedureBodyStub newProcedureBodyStub) {
		if (newProcedureBodyStub != procedureBodyStub) {
			NotificationChain msgs = null;
			if (procedureBodyStub != null)
				msgs = ((InternalEObject)procedureBodyStub).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PROCEDURE_BODY_STUB, null, msgs);
			if (newProcedureBodyStub != null)
				msgs = ((InternalEObject)newProcedureBodyStub).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PROCEDURE_BODY_STUB, null, msgs);
			msgs = basicSetProcedureBodyStub(newProcedureBodyStub, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PROCEDURE_BODY_STUB, newProcedureBodyStub, newProcedureBodyStub));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionBodyStub getFunctionBodyStub() {
		return functionBodyStub;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFunctionBodyStub(FunctionBodyStub newFunctionBodyStub, NotificationChain msgs) {
		FunctionBodyStub oldFunctionBodyStub = functionBodyStub;
		functionBodyStub = newFunctionBodyStub;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__FUNCTION_BODY_STUB, oldFunctionBodyStub, newFunctionBodyStub);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFunctionBodyStub(FunctionBodyStub newFunctionBodyStub) {
		if (newFunctionBodyStub != functionBodyStub) {
			NotificationChain msgs = null;
			if (functionBodyStub != null)
				msgs = ((InternalEObject)functionBodyStub).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__FUNCTION_BODY_STUB, null, msgs);
			if (newFunctionBodyStub != null)
				msgs = ((InternalEObject)newFunctionBodyStub).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__FUNCTION_BODY_STUB, null, msgs);
			msgs = basicSetFunctionBodyStub(newFunctionBodyStub, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__FUNCTION_BODY_STUB, newFunctionBodyStub, newFunctionBodyStub));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackageBodyStub getPackageBodyStub() {
		return packageBodyStub;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPackageBodyStub(PackageBodyStub newPackageBodyStub, NotificationChain msgs) {
		PackageBodyStub oldPackageBodyStub = packageBodyStub;
		packageBodyStub = newPackageBodyStub;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PACKAGE_BODY_STUB, oldPackageBodyStub, newPackageBodyStub);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPackageBodyStub(PackageBodyStub newPackageBodyStub) {
		if (newPackageBodyStub != packageBodyStub) {
			NotificationChain msgs = null;
			if (packageBodyStub != null)
				msgs = ((InternalEObject)packageBodyStub).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PACKAGE_BODY_STUB, null, msgs);
			if (newPackageBodyStub != null)
				msgs = ((InternalEObject)newPackageBodyStub).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PACKAGE_BODY_STUB, null, msgs);
			msgs = basicSetPackageBodyStub(newPackageBodyStub, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PACKAGE_BODY_STUB, newPackageBodyStub, newPackageBodyStub));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskBodyStub getTaskBodyStub() {
		return taskBodyStub;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTaskBodyStub(TaskBodyStub newTaskBodyStub, NotificationChain msgs) {
		TaskBodyStub oldTaskBodyStub = taskBodyStub;
		taskBodyStub = newTaskBodyStub;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__TASK_BODY_STUB, oldTaskBodyStub, newTaskBodyStub);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaskBodyStub(TaskBodyStub newTaskBodyStub) {
		if (newTaskBodyStub != taskBodyStub) {
			NotificationChain msgs = null;
			if (taskBodyStub != null)
				msgs = ((InternalEObject)taskBodyStub).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__TASK_BODY_STUB, null, msgs);
			if (newTaskBodyStub != null)
				msgs = ((InternalEObject)newTaskBodyStub).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__TASK_BODY_STUB, null, msgs);
			msgs = basicSetTaskBodyStub(newTaskBodyStub, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__TASK_BODY_STUB, newTaskBodyStub, newTaskBodyStub));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProtectedBodyStub getProtectedBodyStub() {
		return protectedBodyStub;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProtectedBodyStub(ProtectedBodyStub newProtectedBodyStub, NotificationChain msgs) {
		ProtectedBodyStub oldProtectedBodyStub = protectedBodyStub;
		protectedBodyStub = newProtectedBodyStub;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PROTECTED_BODY_STUB, oldProtectedBodyStub, newProtectedBodyStub);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProtectedBodyStub(ProtectedBodyStub newProtectedBodyStub) {
		if (newProtectedBodyStub != protectedBodyStub) {
			NotificationChain msgs = null;
			if (protectedBodyStub != null)
				msgs = ((InternalEObject)protectedBodyStub).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PROTECTED_BODY_STUB, null, msgs);
			if (newProtectedBodyStub != null)
				msgs = ((InternalEObject)newProtectedBodyStub).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PROTECTED_BODY_STUB, null, msgs);
			msgs = basicSetProtectedBodyStub(newProtectedBodyStub, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PROTECTED_BODY_STUB, newProtectedBodyStub, newProtectedBodyStub));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExceptionDeclaration getExceptionDeclaration() {
		return exceptionDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExceptionDeclaration(ExceptionDeclaration newExceptionDeclaration, NotificationChain msgs) {
		ExceptionDeclaration oldExceptionDeclaration = exceptionDeclaration;
		exceptionDeclaration = newExceptionDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__EXCEPTION_DECLARATION, oldExceptionDeclaration, newExceptionDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExceptionDeclaration(ExceptionDeclaration newExceptionDeclaration) {
		if (newExceptionDeclaration != exceptionDeclaration) {
			NotificationChain msgs = null;
			if (exceptionDeclaration != null)
				msgs = ((InternalEObject)exceptionDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__EXCEPTION_DECLARATION, null, msgs);
			if (newExceptionDeclaration != null)
				msgs = ((InternalEObject)newExceptionDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__EXCEPTION_DECLARATION, null, msgs);
			msgs = basicSetExceptionDeclaration(newExceptionDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__EXCEPTION_DECLARATION, newExceptionDeclaration, newExceptionDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChoiceParameterSpecification getChoiceParameterSpecification() {
		return choiceParameterSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChoiceParameterSpecification(ChoiceParameterSpecification newChoiceParameterSpecification, NotificationChain msgs) {
		ChoiceParameterSpecification oldChoiceParameterSpecification = choiceParameterSpecification;
		choiceParameterSpecification = newChoiceParameterSpecification;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__CHOICE_PARAMETER_SPECIFICATION, oldChoiceParameterSpecification, newChoiceParameterSpecification);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChoiceParameterSpecification(ChoiceParameterSpecification newChoiceParameterSpecification) {
		if (newChoiceParameterSpecification != choiceParameterSpecification) {
			NotificationChain msgs = null;
			if (choiceParameterSpecification != null)
				msgs = ((InternalEObject)choiceParameterSpecification).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__CHOICE_PARAMETER_SPECIFICATION, null, msgs);
			if (newChoiceParameterSpecification != null)
				msgs = ((InternalEObject)newChoiceParameterSpecification).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__CHOICE_PARAMETER_SPECIFICATION, null, msgs);
			msgs = basicSetChoiceParameterSpecification(newChoiceParameterSpecification, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__CHOICE_PARAMETER_SPECIFICATION, newChoiceParameterSpecification, newChoiceParameterSpecification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericProcedureDeclaration getGenericProcedureDeclaration() {
		return genericProcedureDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGenericProcedureDeclaration(GenericProcedureDeclaration newGenericProcedureDeclaration, NotificationChain msgs) {
		GenericProcedureDeclaration oldGenericProcedureDeclaration = genericProcedureDeclaration;
		genericProcedureDeclaration = newGenericProcedureDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__GENERIC_PROCEDURE_DECLARATION, oldGenericProcedureDeclaration, newGenericProcedureDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGenericProcedureDeclaration(GenericProcedureDeclaration newGenericProcedureDeclaration) {
		if (newGenericProcedureDeclaration != genericProcedureDeclaration) {
			NotificationChain msgs = null;
			if (genericProcedureDeclaration != null)
				msgs = ((InternalEObject)genericProcedureDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__GENERIC_PROCEDURE_DECLARATION, null, msgs);
			if (newGenericProcedureDeclaration != null)
				msgs = ((InternalEObject)newGenericProcedureDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__GENERIC_PROCEDURE_DECLARATION, null, msgs);
			msgs = basicSetGenericProcedureDeclaration(newGenericProcedureDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__GENERIC_PROCEDURE_DECLARATION, newGenericProcedureDeclaration, newGenericProcedureDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericFunctionDeclaration getGenericFunctionDeclaration() {
		return genericFunctionDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGenericFunctionDeclaration(GenericFunctionDeclaration newGenericFunctionDeclaration, NotificationChain msgs) {
		GenericFunctionDeclaration oldGenericFunctionDeclaration = genericFunctionDeclaration;
		genericFunctionDeclaration = newGenericFunctionDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__GENERIC_FUNCTION_DECLARATION, oldGenericFunctionDeclaration, newGenericFunctionDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGenericFunctionDeclaration(GenericFunctionDeclaration newGenericFunctionDeclaration) {
		if (newGenericFunctionDeclaration != genericFunctionDeclaration) {
			NotificationChain msgs = null;
			if (genericFunctionDeclaration != null)
				msgs = ((InternalEObject)genericFunctionDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__GENERIC_FUNCTION_DECLARATION, null, msgs);
			if (newGenericFunctionDeclaration != null)
				msgs = ((InternalEObject)newGenericFunctionDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__GENERIC_FUNCTION_DECLARATION, null, msgs);
			msgs = basicSetGenericFunctionDeclaration(newGenericFunctionDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__GENERIC_FUNCTION_DECLARATION, newGenericFunctionDeclaration, newGenericFunctionDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GenericPackageDeclaration getGenericPackageDeclaration() {
		return genericPackageDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGenericPackageDeclaration(GenericPackageDeclaration newGenericPackageDeclaration, NotificationChain msgs) {
		GenericPackageDeclaration oldGenericPackageDeclaration = genericPackageDeclaration;
		genericPackageDeclaration = newGenericPackageDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__GENERIC_PACKAGE_DECLARATION, oldGenericPackageDeclaration, newGenericPackageDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGenericPackageDeclaration(GenericPackageDeclaration newGenericPackageDeclaration) {
		if (newGenericPackageDeclaration != genericPackageDeclaration) {
			NotificationChain msgs = null;
			if (genericPackageDeclaration != null)
				msgs = ((InternalEObject)genericPackageDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__GENERIC_PACKAGE_DECLARATION, null, msgs);
			if (newGenericPackageDeclaration != null)
				msgs = ((InternalEObject)newGenericPackageDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__GENERIC_PACKAGE_DECLARATION, null, msgs);
			msgs = basicSetGenericPackageDeclaration(newGenericPackageDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__GENERIC_PACKAGE_DECLARATION, newGenericPackageDeclaration, newGenericPackageDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackageInstantiation getPackageInstantiation() {
		return packageInstantiation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPackageInstantiation(PackageInstantiation newPackageInstantiation, NotificationChain msgs) {
		PackageInstantiation oldPackageInstantiation = packageInstantiation;
		packageInstantiation = newPackageInstantiation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PACKAGE_INSTANTIATION, oldPackageInstantiation, newPackageInstantiation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPackageInstantiation(PackageInstantiation newPackageInstantiation) {
		if (newPackageInstantiation != packageInstantiation) {
			NotificationChain msgs = null;
			if (packageInstantiation != null)
				msgs = ((InternalEObject)packageInstantiation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PACKAGE_INSTANTIATION, null, msgs);
			if (newPackageInstantiation != null)
				msgs = ((InternalEObject)newPackageInstantiation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PACKAGE_INSTANTIATION, null, msgs);
			msgs = basicSetPackageInstantiation(newPackageInstantiation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PACKAGE_INSTANTIATION, newPackageInstantiation, newPackageInstantiation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureInstantiation getProcedureInstantiation() {
		return procedureInstantiation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcedureInstantiation(ProcedureInstantiation newProcedureInstantiation, NotificationChain msgs) {
		ProcedureInstantiation oldProcedureInstantiation = procedureInstantiation;
		procedureInstantiation = newProcedureInstantiation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PROCEDURE_INSTANTIATION, oldProcedureInstantiation, newProcedureInstantiation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcedureInstantiation(ProcedureInstantiation newProcedureInstantiation) {
		if (newProcedureInstantiation != procedureInstantiation) {
			NotificationChain msgs = null;
			if (procedureInstantiation != null)
				msgs = ((InternalEObject)procedureInstantiation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PROCEDURE_INSTANTIATION, null, msgs);
			if (newProcedureInstantiation != null)
				msgs = ((InternalEObject)newProcedureInstantiation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PROCEDURE_INSTANTIATION, null, msgs);
			msgs = basicSetProcedureInstantiation(newProcedureInstantiation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PROCEDURE_INSTANTIATION, newProcedureInstantiation, newProcedureInstantiation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionInstantiation getFunctionInstantiation() {
		return functionInstantiation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFunctionInstantiation(FunctionInstantiation newFunctionInstantiation, NotificationChain msgs) {
		FunctionInstantiation oldFunctionInstantiation = functionInstantiation;
		functionInstantiation = newFunctionInstantiation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__FUNCTION_INSTANTIATION, oldFunctionInstantiation, newFunctionInstantiation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFunctionInstantiation(FunctionInstantiation newFunctionInstantiation) {
		if (newFunctionInstantiation != functionInstantiation) {
			NotificationChain msgs = null;
			if (functionInstantiation != null)
				msgs = ((InternalEObject)functionInstantiation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__FUNCTION_INSTANTIATION, null, msgs);
			if (newFunctionInstantiation != null)
				msgs = ((InternalEObject)newFunctionInstantiation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__FUNCTION_INSTANTIATION, null, msgs);
			msgs = basicSetFunctionInstantiation(newFunctionInstantiation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__FUNCTION_INSTANTIATION, newFunctionInstantiation, newFunctionInstantiation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalObjectDeclaration getFormalObjectDeclaration() {
		return formalObjectDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalObjectDeclaration(FormalObjectDeclaration newFormalObjectDeclaration, NotificationChain msgs) {
		FormalObjectDeclaration oldFormalObjectDeclaration = formalObjectDeclaration;
		formalObjectDeclaration = newFormalObjectDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__FORMAL_OBJECT_DECLARATION, oldFormalObjectDeclaration, newFormalObjectDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalObjectDeclaration(FormalObjectDeclaration newFormalObjectDeclaration) {
		if (newFormalObjectDeclaration != formalObjectDeclaration) {
			NotificationChain msgs = null;
			if (formalObjectDeclaration != null)
				msgs = ((InternalEObject)formalObjectDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__FORMAL_OBJECT_DECLARATION, null, msgs);
			if (newFormalObjectDeclaration != null)
				msgs = ((InternalEObject)newFormalObjectDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__FORMAL_OBJECT_DECLARATION, null, msgs);
			msgs = basicSetFormalObjectDeclaration(newFormalObjectDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__FORMAL_OBJECT_DECLARATION, newFormalObjectDeclaration, newFormalObjectDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalTypeDeclaration getFormalTypeDeclaration() {
		return formalTypeDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalTypeDeclaration(FormalTypeDeclaration newFormalTypeDeclaration, NotificationChain msgs) {
		FormalTypeDeclaration oldFormalTypeDeclaration = formalTypeDeclaration;
		formalTypeDeclaration = newFormalTypeDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__FORMAL_TYPE_DECLARATION, oldFormalTypeDeclaration, newFormalTypeDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalTypeDeclaration(FormalTypeDeclaration newFormalTypeDeclaration) {
		if (newFormalTypeDeclaration != formalTypeDeclaration) {
			NotificationChain msgs = null;
			if (formalTypeDeclaration != null)
				msgs = ((InternalEObject)formalTypeDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__FORMAL_TYPE_DECLARATION, null, msgs);
			if (newFormalTypeDeclaration != null)
				msgs = ((InternalEObject)newFormalTypeDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__FORMAL_TYPE_DECLARATION, null, msgs);
			msgs = basicSetFormalTypeDeclaration(newFormalTypeDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__FORMAL_TYPE_DECLARATION, newFormalTypeDeclaration, newFormalTypeDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalIncompleteTypeDeclaration getFormalIncompleteTypeDeclaration() {
		return formalIncompleteTypeDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalIncompleteTypeDeclaration(FormalIncompleteTypeDeclaration newFormalIncompleteTypeDeclaration, NotificationChain msgs) {
		FormalIncompleteTypeDeclaration oldFormalIncompleteTypeDeclaration = formalIncompleteTypeDeclaration;
		formalIncompleteTypeDeclaration = newFormalIncompleteTypeDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__FORMAL_INCOMPLETE_TYPE_DECLARATION, oldFormalIncompleteTypeDeclaration, newFormalIncompleteTypeDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalIncompleteTypeDeclaration(FormalIncompleteTypeDeclaration newFormalIncompleteTypeDeclaration) {
		if (newFormalIncompleteTypeDeclaration != formalIncompleteTypeDeclaration) {
			NotificationChain msgs = null;
			if (formalIncompleteTypeDeclaration != null)
				msgs = ((InternalEObject)formalIncompleteTypeDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__FORMAL_INCOMPLETE_TYPE_DECLARATION, null, msgs);
			if (newFormalIncompleteTypeDeclaration != null)
				msgs = ((InternalEObject)newFormalIncompleteTypeDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__FORMAL_INCOMPLETE_TYPE_DECLARATION, null, msgs);
			msgs = basicSetFormalIncompleteTypeDeclaration(newFormalIncompleteTypeDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__FORMAL_INCOMPLETE_TYPE_DECLARATION, newFormalIncompleteTypeDeclaration, newFormalIncompleteTypeDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalProcedureDeclaration getFormalProcedureDeclaration() {
		return formalProcedureDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalProcedureDeclaration(FormalProcedureDeclaration newFormalProcedureDeclaration, NotificationChain msgs) {
		FormalProcedureDeclaration oldFormalProcedureDeclaration = formalProcedureDeclaration;
		formalProcedureDeclaration = newFormalProcedureDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__FORMAL_PROCEDURE_DECLARATION, oldFormalProcedureDeclaration, newFormalProcedureDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalProcedureDeclaration(FormalProcedureDeclaration newFormalProcedureDeclaration) {
		if (newFormalProcedureDeclaration != formalProcedureDeclaration) {
			NotificationChain msgs = null;
			if (formalProcedureDeclaration != null)
				msgs = ((InternalEObject)formalProcedureDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__FORMAL_PROCEDURE_DECLARATION, null, msgs);
			if (newFormalProcedureDeclaration != null)
				msgs = ((InternalEObject)newFormalProcedureDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__FORMAL_PROCEDURE_DECLARATION, null, msgs);
			msgs = basicSetFormalProcedureDeclaration(newFormalProcedureDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__FORMAL_PROCEDURE_DECLARATION, newFormalProcedureDeclaration, newFormalProcedureDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalFunctionDeclaration getFormalFunctionDeclaration() {
		return formalFunctionDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalFunctionDeclaration(FormalFunctionDeclaration newFormalFunctionDeclaration, NotificationChain msgs) {
		FormalFunctionDeclaration oldFormalFunctionDeclaration = formalFunctionDeclaration;
		formalFunctionDeclaration = newFormalFunctionDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__FORMAL_FUNCTION_DECLARATION, oldFormalFunctionDeclaration, newFormalFunctionDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalFunctionDeclaration(FormalFunctionDeclaration newFormalFunctionDeclaration) {
		if (newFormalFunctionDeclaration != formalFunctionDeclaration) {
			NotificationChain msgs = null;
			if (formalFunctionDeclaration != null)
				msgs = ((InternalEObject)formalFunctionDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__FORMAL_FUNCTION_DECLARATION, null, msgs);
			if (newFormalFunctionDeclaration != null)
				msgs = ((InternalEObject)newFormalFunctionDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__FORMAL_FUNCTION_DECLARATION, null, msgs);
			msgs = basicSetFormalFunctionDeclaration(newFormalFunctionDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__FORMAL_FUNCTION_DECLARATION, newFormalFunctionDeclaration, newFormalFunctionDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalPackageDeclaration getFormalPackageDeclaration() {
		return formalPackageDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalPackageDeclaration(FormalPackageDeclaration newFormalPackageDeclaration, NotificationChain msgs) {
		FormalPackageDeclaration oldFormalPackageDeclaration = formalPackageDeclaration;
		formalPackageDeclaration = newFormalPackageDeclaration;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__FORMAL_PACKAGE_DECLARATION, oldFormalPackageDeclaration, newFormalPackageDeclaration);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalPackageDeclaration(FormalPackageDeclaration newFormalPackageDeclaration) {
		if (newFormalPackageDeclaration != formalPackageDeclaration) {
			NotificationChain msgs = null;
			if (formalPackageDeclaration != null)
				msgs = ((InternalEObject)formalPackageDeclaration).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__FORMAL_PACKAGE_DECLARATION, null, msgs);
			if (newFormalPackageDeclaration != null)
				msgs = ((InternalEObject)newFormalPackageDeclaration).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__FORMAL_PACKAGE_DECLARATION, null, msgs);
			msgs = basicSetFormalPackageDeclaration(newFormalPackageDeclaration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__FORMAL_PACKAGE_DECLARATION, newFormalPackageDeclaration, newFormalPackageDeclaration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormalPackageDeclarationWithBox getFormalPackageDeclarationWithBox() {
		return formalPackageDeclarationWithBox;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalPackageDeclarationWithBox(FormalPackageDeclarationWithBox newFormalPackageDeclarationWithBox, NotificationChain msgs) {
		FormalPackageDeclarationWithBox oldFormalPackageDeclarationWithBox = formalPackageDeclarationWithBox;
		formalPackageDeclarationWithBox = newFormalPackageDeclarationWithBox;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__FORMAL_PACKAGE_DECLARATION_WITH_BOX, oldFormalPackageDeclarationWithBox, newFormalPackageDeclarationWithBox);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalPackageDeclarationWithBox(FormalPackageDeclarationWithBox newFormalPackageDeclarationWithBox) {
		if (newFormalPackageDeclarationWithBox != formalPackageDeclarationWithBox) {
			NotificationChain msgs = null;
			if (formalPackageDeclarationWithBox != null)
				msgs = ((InternalEObject)formalPackageDeclarationWithBox).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__FORMAL_PACKAGE_DECLARATION_WITH_BOX, null, msgs);
			if (newFormalPackageDeclarationWithBox != null)
				msgs = ((InternalEObject)newFormalPackageDeclarationWithBox).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__FORMAL_PACKAGE_DECLARATION_WITH_BOX, null, msgs);
			msgs = basicSetFormalPackageDeclarationWithBox(newFormalPackageDeclarationWithBox, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__FORMAL_PACKAGE_DECLARATION_WITH_BOX, newFormalPackageDeclarationWithBox, newFormalPackageDeclarationWithBox));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Comment getComment() {
		return comment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetComment(Comment newComment, NotificationChain msgs) {
		Comment oldComment = comment;
		comment = newComment;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__COMMENT, oldComment, newComment);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComment(Comment newComment) {
		if (newComment != comment) {
			NotificationChain msgs = null;
			if (comment != null)
				msgs = ((InternalEObject)comment).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__COMMENT, null, msgs);
			if (newComment != null)
				msgs = ((InternalEObject)newComment).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__COMMENT, null, msgs);
			msgs = basicSetComment(newComment, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__COMMENT, newComment, newComment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AllCallsRemotePragma getAllCallsRemotePragma() {
		return allCallsRemotePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAllCallsRemotePragma(AllCallsRemotePragma newAllCallsRemotePragma, NotificationChain msgs) {
		AllCallsRemotePragma oldAllCallsRemotePragma = allCallsRemotePragma;
		allCallsRemotePragma = newAllCallsRemotePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ALL_CALLS_REMOTE_PRAGMA, oldAllCallsRemotePragma, newAllCallsRemotePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAllCallsRemotePragma(AllCallsRemotePragma newAllCallsRemotePragma) {
		if (newAllCallsRemotePragma != allCallsRemotePragma) {
			NotificationChain msgs = null;
			if (allCallsRemotePragma != null)
				msgs = ((InternalEObject)allCallsRemotePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ALL_CALLS_REMOTE_PRAGMA, null, msgs);
			if (newAllCallsRemotePragma != null)
				msgs = ((InternalEObject)newAllCallsRemotePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ALL_CALLS_REMOTE_PRAGMA, null, msgs);
			msgs = basicSetAllCallsRemotePragma(newAllCallsRemotePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ALL_CALLS_REMOTE_PRAGMA, newAllCallsRemotePragma, newAllCallsRemotePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AsynchronousPragma getAsynchronousPragma() {
		return asynchronousPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAsynchronousPragma(AsynchronousPragma newAsynchronousPragma, NotificationChain msgs) {
		AsynchronousPragma oldAsynchronousPragma = asynchronousPragma;
		asynchronousPragma = newAsynchronousPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ASYNCHRONOUS_PRAGMA, oldAsynchronousPragma, newAsynchronousPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAsynchronousPragma(AsynchronousPragma newAsynchronousPragma) {
		if (newAsynchronousPragma != asynchronousPragma) {
			NotificationChain msgs = null;
			if (asynchronousPragma != null)
				msgs = ((InternalEObject)asynchronousPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ASYNCHRONOUS_PRAGMA, null, msgs);
			if (newAsynchronousPragma != null)
				msgs = ((InternalEObject)newAsynchronousPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ASYNCHRONOUS_PRAGMA, null, msgs);
			msgs = basicSetAsynchronousPragma(newAsynchronousPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ASYNCHRONOUS_PRAGMA, newAsynchronousPragma, newAsynchronousPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtomicPragma getAtomicPragma() {
		return atomicPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAtomicPragma(AtomicPragma newAtomicPragma, NotificationChain msgs) {
		AtomicPragma oldAtomicPragma = atomicPragma;
		atomicPragma = newAtomicPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ATOMIC_PRAGMA, oldAtomicPragma, newAtomicPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAtomicPragma(AtomicPragma newAtomicPragma) {
		if (newAtomicPragma != atomicPragma) {
			NotificationChain msgs = null;
			if (atomicPragma != null)
				msgs = ((InternalEObject)atomicPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ATOMIC_PRAGMA, null, msgs);
			if (newAtomicPragma != null)
				msgs = ((InternalEObject)newAtomicPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ATOMIC_PRAGMA, null, msgs);
			msgs = basicSetAtomicPragma(newAtomicPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ATOMIC_PRAGMA, newAtomicPragma, newAtomicPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtomicComponentsPragma getAtomicComponentsPragma() {
		return atomicComponentsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAtomicComponentsPragma(AtomicComponentsPragma newAtomicComponentsPragma, NotificationChain msgs) {
		AtomicComponentsPragma oldAtomicComponentsPragma = atomicComponentsPragma;
		atomicComponentsPragma = newAtomicComponentsPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ATOMIC_COMPONENTS_PRAGMA, oldAtomicComponentsPragma, newAtomicComponentsPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAtomicComponentsPragma(AtomicComponentsPragma newAtomicComponentsPragma) {
		if (newAtomicComponentsPragma != atomicComponentsPragma) {
			NotificationChain msgs = null;
			if (atomicComponentsPragma != null)
				msgs = ((InternalEObject)atomicComponentsPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ATOMIC_COMPONENTS_PRAGMA, null, msgs);
			if (newAtomicComponentsPragma != null)
				msgs = ((InternalEObject)newAtomicComponentsPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ATOMIC_COMPONENTS_PRAGMA, null, msgs);
			msgs = basicSetAtomicComponentsPragma(newAtomicComponentsPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ATOMIC_COMPONENTS_PRAGMA, newAtomicComponentsPragma, newAtomicComponentsPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttachHandlerPragma getAttachHandlerPragma() {
		return attachHandlerPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAttachHandlerPragma(AttachHandlerPragma newAttachHandlerPragma, NotificationChain msgs) {
		AttachHandlerPragma oldAttachHandlerPragma = attachHandlerPragma;
		attachHandlerPragma = newAttachHandlerPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ATTACH_HANDLER_PRAGMA, oldAttachHandlerPragma, newAttachHandlerPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttachHandlerPragma(AttachHandlerPragma newAttachHandlerPragma) {
		if (newAttachHandlerPragma != attachHandlerPragma) {
			NotificationChain msgs = null;
			if (attachHandlerPragma != null)
				msgs = ((InternalEObject)attachHandlerPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ATTACH_HANDLER_PRAGMA, null, msgs);
			if (newAttachHandlerPragma != null)
				msgs = ((InternalEObject)newAttachHandlerPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ATTACH_HANDLER_PRAGMA, null, msgs);
			msgs = basicSetAttachHandlerPragma(newAttachHandlerPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ATTACH_HANDLER_PRAGMA, newAttachHandlerPragma, newAttachHandlerPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlledPragma getControlledPragma() {
		return controlledPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetControlledPragma(ControlledPragma newControlledPragma, NotificationChain msgs) {
		ControlledPragma oldControlledPragma = controlledPragma;
		controlledPragma = newControlledPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__CONTROLLED_PRAGMA, oldControlledPragma, newControlledPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setControlledPragma(ControlledPragma newControlledPragma) {
		if (newControlledPragma != controlledPragma) {
			NotificationChain msgs = null;
			if (controlledPragma != null)
				msgs = ((InternalEObject)controlledPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__CONTROLLED_PRAGMA, null, msgs);
			if (newControlledPragma != null)
				msgs = ((InternalEObject)newControlledPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__CONTROLLED_PRAGMA, null, msgs);
			msgs = basicSetControlledPragma(newControlledPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__CONTROLLED_PRAGMA, newControlledPragma, newControlledPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConventionPragma getConventionPragma() {
		return conventionPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConventionPragma(ConventionPragma newConventionPragma, NotificationChain msgs) {
		ConventionPragma oldConventionPragma = conventionPragma;
		conventionPragma = newConventionPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__CONVENTION_PRAGMA, oldConventionPragma, newConventionPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConventionPragma(ConventionPragma newConventionPragma) {
		if (newConventionPragma != conventionPragma) {
			NotificationChain msgs = null;
			if (conventionPragma != null)
				msgs = ((InternalEObject)conventionPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__CONVENTION_PRAGMA, null, msgs);
			if (newConventionPragma != null)
				msgs = ((InternalEObject)newConventionPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__CONVENTION_PRAGMA, null, msgs);
			msgs = basicSetConventionPragma(newConventionPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__CONVENTION_PRAGMA, newConventionPragma, newConventionPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiscardNamesPragma getDiscardNamesPragma() {
		return discardNamesPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiscardNamesPragma(DiscardNamesPragma newDiscardNamesPragma, NotificationChain msgs) {
		DiscardNamesPragma oldDiscardNamesPragma = discardNamesPragma;
		discardNamesPragma = newDiscardNamesPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__DISCARD_NAMES_PRAGMA, oldDiscardNamesPragma, newDiscardNamesPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiscardNamesPragma(DiscardNamesPragma newDiscardNamesPragma) {
		if (newDiscardNamesPragma != discardNamesPragma) {
			NotificationChain msgs = null;
			if (discardNamesPragma != null)
				msgs = ((InternalEObject)discardNamesPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__DISCARD_NAMES_PRAGMA, null, msgs);
			if (newDiscardNamesPragma != null)
				msgs = ((InternalEObject)newDiscardNamesPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__DISCARD_NAMES_PRAGMA, null, msgs);
			msgs = basicSetDiscardNamesPragma(newDiscardNamesPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__DISCARD_NAMES_PRAGMA, newDiscardNamesPragma, newDiscardNamesPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElaboratePragma getElaboratePragma() {
		return elaboratePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElaboratePragma(ElaboratePragma newElaboratePragma, NotificationChain msgs) {
		ElaboratePragma oldElaboratePragma = elaboratePragma;
		elaboratePragma = newElaboratePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ELABORATE_PRAGMA, oldElaboratePragma, newElaboratePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElaboratePragma(ElaboratePragma newElaboratePragma) {
		if (newElaboratePragma != elaboratePragma) {
			NotificationChain msgs = null;
			if (elaboratePragma != null)
				msgs = ((InternalEObject)elaboratePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ELABORATE_PRAGMA, null, msgs);
			if (newElaboratePragma != null)
				msgs = ((InternalEObject)newElaboratePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ELABORATE_PRAGMA, null, msgs);
			msgs = basicSetElaboratePragma(newElaboratePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ELABORATE_PRAGMA, newElaboratePragma, newElaboratePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElaborateAllPragma getElaborateAllPragma() {
		return elaborateAllPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElaborateAllPragma(ElaborateAllPragma newElaborateAllPragma, NotificationChain msgs) {
		ElaborateAllPragma oldElaborateAllPragma = elaborateAllPragma;
		elaborateAllPragma = newElaborateAllPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ELABORATE_ALL_PRAGMA, oldElaborateAllPragma, newElaborateAllPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElaborateAllPragma(ElaborateAllPragma newElaborateAllPragma) {
		if (newElaborateAllPragma != elaborateAllPragma) {
			NotificationChain msgs = null;
			if (elaborateAllPragma != null)
				msgs = ((InternalEObject)elaborateAllPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ELABORATE_ALL_PRAGMA, null, msgs);
			if (newElaborateAllPragma != null)
				msgs = ((InternalEObject)newElaborateAllPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ELABORATE_ALL_PRAGMA, null, msgs);
			msgs = basicSetElaborateAllPragma(newElaborateAllPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ELABORATE_ALL_PRAGMA, newElaborateAllPragma, newElaborateAllPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElaborateBodyPragma getElaborateBodyPragma() {
		return elaborateBodyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElaborateBodyPragma(ElaborateBodyPragma newElaborateBodyPragma, NotificationChain msgs) {
		ElaborateBodyPragma oldElaborateBodyPragma = elaborateBodyPragma;
		elaborateBodyPragma = newElaborateBodyPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ELABORATE_BODY_PRAGMA, oldElaborateBodyPragma, newElaborateBodyPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElaborateBodyPragma(ElaborateBodyPragma newElaborateBodyPragma) {
		if (newElaborateBodyPragma != elaborateBodyPragma) {
			NotificationChain msgs = null;
			if (elaborateBodyPragma != null)
				msgs = ((InternalEObject)elaborateBodyPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ELABORATE_BODY_PRAGMA, null, msgs);
			if (newElaborateBodyPragma != null)
				msgs = ((InternalEObject)newElaborateBodyPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ELABORATE_BODY_PRAGMA, null, msgs);
			msgs = basicSetElaborateBodyPragma(newElaborateBodyPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ELABORATE_BODY_PRAGMA, newElaborateBodyPragma, newElaborateBodyPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExportPragma getExportPragma() {
		return exportPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExportPragma(ExportPragma newExportPragma, NotificationChain msgs) {
		ExportPragma oldExportPragma = exportPragma;
		exportPragma = newExportPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__EXPORT_PRAGMA, oldExportPragma, newExportPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExportPragma(ExportPragma newExportPragma) {
		if (newExportPragma != exportPragma) {
			NotificationChain msgs = null;
			if (exportPragma != null)
				msgs = ((InternalEObject)exportPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__EXPORT_PRAGMA, null, msgs);
			if (newExportPragma != null)
				msgs = ((InternalEObject)newExportPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__EXPORT_PRAGMA, null, msgs);
			msgs = basicSetExportPragma(newExportPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__EXPORT_PRAGMA, newExportPragma, newExportPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImportPragma getImportPragma() {
		return importPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImportPragma(ImportPragma newImportPragma, NotificationChain msgs) {
		ImportPragma oldImportPragma = importPragma;
		importPragma = newImportPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__IMPORT_PRAGMA, oldImportPragma, newImportPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImportPragma(ImportPragma newImportPragma) {
		if (newImportPragma != importPragma) {
			NotificationChain msgs = null;
			if (importPragma != null)
				msgs = ((InternalEObject)importPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__IMPORT_PRAGMA, null, msgs);
			if (newImportPragma != null)
				msgs = ((InternalEObject)newImportPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__IMPORT_PRAGMA, null, msgs);
			msgs = basicSetImportPragma(newImportPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__IMPORT_PRAGMA, newImportPragma, newImportPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InlinePragma getInlinePragma() {
		return inlinePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInlinePragma(InlinePragma newInlinePragma, NotificationChain msgs) {
		InlinePragma oldInlinePragma = inlinePragma;
		inlinePragma = newInlinePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__INLINE_PRAGMA, oldInlinePragma, newInlinePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInlinePragma(InlinePragma newInlinePragma) {
		if (newInlinePragma != inlinePragma) {
			NotificationChain msgs = null;
			if (inlinePragma != null)
				msgs = ((InternalEObject)inlinePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__INLINE_PRAGMA, null, msgs);
			if (newInlinePragma != null)
				msgs = ((InternalEObject)newInlinePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__INLINE_PRAGMA, null, msgs);
			msgs = basicSetInlinePragma(newInlinePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__INLINE_PRAGMA, newInlinePragma, newInlinePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InspectionPointPragma getInspectionPointPragma() {
		return inspectionPointPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInspectionPointPragma(InspectionPointPragma newInspectionPointPragma, NotificationChain msgs) {
		InspectionPointPragma oldInspectionPointPragma = inspectionPointPragma;
		inspectionPointPragma = newInspectionPointPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__INSPECTION_POINT_PRAGMA, oldInspectionPointPragma, newInspectionPointPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInspectionPointPragma(InspectionPointPragma newInspectionPointPragma) {
		if (newInspectionPointPragma != inspectionPointPragma) {
			NotificationChain msgs = null;
			if (inspectionPointPragma != null)
				msgs = ((InternalEObject)inspectionPointPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__INSPECTION_POINT_PRAGMA, null, msgs);
			if (newInspectionPointPragma != null)
				msgs = ((InternalEObject)newInspectionPointPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__INSPECTION_POINT_PRAGMA, null, msgs);
			msgs = basicSetInspectionPointPragma(newInspectionPointPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__INSPECTION_POINT_PRAGMA, newInspectionPointPragma, newInspectionPointPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterruptHandlerPragma getInterruptHandlerPragma() {
		return interruptHandlerPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInterruptHandlerPragma(InterruptHandlerPragma newInterruptHandlerPragma, NotificationChain msgs) {
		InterruptHandlerPragma oldInterruptHandlerPragma = interruptHandlerPragma;
		interruptHandlerPragma = newInterruptHandlerPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__INTERRUPT_HANDLER_PRAGMA, oldInterruptHandlerPragma, newInterruptHandlerPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterruptHandlerPragma(InterruptHandlerPragma newInterruptHandlerPragma) {
		if (newInterruptHandlerPragma != interruptHandlerPragma) {
			NotificationChain msgs = null;
			if (interruptHandlerPragma != null)
				msgs = ((InternalEObject)interruptHandlerPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__INTERRUPT_HANDLER_PRAGMA, null, msgs);
			if (newInterruptHandlerPragma != null)
				msgs = ((InternalEObject)newInterruptHandlerPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__INTERRUPT_HANDLER_PRAGMA, null, msgs);
			msgs = basicSetInterruptHandlerPragma(newInterruptHandlerPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__INTERRUPT_HANDLER_PRAGMA, newInterruptHandlerPragma, newInterruptHandlerPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterruptPriorityPragma getInterruptPriorityPragma() {
		return interruptPriorityPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInterruptPriorityPragma(InterruptPriorityPragma newInterruptPriorityPragma, NotificationChain msgs) {
		InterruptPriorityPragma oldInterruptPriorityPragma = interruptPriorityPragma;
		interruptPriorityPragma = newInterruptPriorityPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__INTERRUPT_PRIORITY_PRAGMA, oldInterruptPriorityPragma, newInterruptPriorityPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterruptPriorityPragma(InterruptPriorityPragma newInterruptPriorityPragma) {
		if (newInterruptPriorityPragma != interruptPriorityPragma) {
			NotificationChain msgs = null;
			if (interruptPriorityPragma != null)
				msgs = ((InternalEObject)interruptPriorityPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__INTERRUPT_PRIORITY_PRAGMA, null, msgs);
			if (newInterruptPriorityPragma != null)
				msgs = ((InternalEObject)newInterruptPriorityPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__INTERRUPT_PRIORITY_PRAGMA, null, msgs);
			msgs = basicSetInterruptPriorityPragma(newInterruptPriorityPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__INTERRUPT_PRIORITY_PRAGMA, newInterruptPriorityPragma, newInterruptPriorityPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkerOptionsPragma getLinkerOptionsPragma() {
		return linkerOptionsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLinkerOptionsPragma(LinkerOptionsPragma newLinkerOptionsPragma, NotificationChain msgs) {
		LinkerOptionsPragma oldLinkerOptionsPragma = linkerOptionsPragma;
		linkerOptionsPragma = newLinkerOptionsPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__LINKER_OPTIONS_PRAGMA, oldLinkerOptionsPragma, newLinkerOptionsPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLinkerOptionsPragma(LinkerOptionsPragma newLinkerOptionsPragma) {
		if (newLinkerOptionsPragma != linkerOptionsPragma) {
			NotificationChain msgs = null;
			if (linkerOptionsPragma != null)
				msgs = ((InternalEObject)linkerOptionsPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__LINKER_OPTIONS_PRAGMA, null, msgs);
			if (newLinkerOptionsPragma != null)
				msgs = ((InternalEObject)newLinkerOptionsPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__LINKER_OPTIONS_PRAGMA, null, msgs);
			msgs = basicSetLinkerOptionsPragma(newLinkerOptionsPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__LINKER_OPTIONS_PRAGMA, newLinkerOptionsPragma, newLinkerOptionsPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ListPragma getListPragma() {
		return listPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetListPragma(ListPragma newListPragma, NotificationChain msgs) {
		ListPragma oldListPragma = listPragma;
		listPragma = newListPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__LIST_PRAGMA, oldListPragma, newListPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setListPragma(ListPragma newListPragma) {
		if (newListPragma != listPragma) {
			NotificationChain msgs = null;
			if (listPragma != null)
				msgs = ((InternalEObject)listPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__LIST_PRAGMA, null, msgs);
			if (newListPragma != null)
				msgs = ((InternalEObject)newListPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__LIST_PRAGMA, null, msgs);
			msgs = basicSetListPragma(newListPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__LIST_PRAGMA, newListPragma, newListPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LockingPolicyPragma getLockingPolicyPragma() {
		return lockingPolicyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLockingPolicyPragma(LockingPolicyPragma newLockingPolicyPragma, NotificationChain msgs) {
		LockingPolicyPragma oldLockingPolicyPragma = lockingPolicyPragma;
		lockingPolicyPragma = newLockingPolicyPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__LOCKING_POLICY_PRAGMA, oldLockingPolicyPragma, newLockingPolicyPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLockingPolicyPragma(LockingPolicyPragma newLockingPolicyPragma) {
		if (newLockingPolicyPragma != lockingPolicyPragma) {
			NotificationChain msgs = null;
			if (lockingPolicyPragma != null)
				msgs = ((InternalEObject)lockingPolicyPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__LOCKING_POLICY_PRAGMA, null, msgs);
			if (newLockingPolicyPragma != null)
				msgs = ((InternalEObject)newLockingPolicyPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__LOCKING_POLICY_PRAGMA, null, msgs);
			msgs = basicSetLockingPolicyPragma(newLockingPolicyPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__LOCKING_POLICY_PRAGMA, newLockingPolicyPragma, newLockingPolicyPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NormalizeScalarsPragma getNormalizeScalarsPragma() {
		return normalizeScalarsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNormalizeScalarsPragma(NormalizeScalarsPragma newNormalizeScalarsPragma, NotificationChain msgs) {
		NormalizeScalarsPragma oldNormalizeScalarsPragma = normalizeScalarsPragma;
		normalizeScalarsPragma = newNormalizeScalarsPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__NORMALIZE_SCALARS_PRAGMA, oldNormalizeScalarsPragma, newNormalizeScalarsPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNormalizeScalarsPragma(NormalizeScalarsPragma newNormalizeScalarsPragma) {
		if (newNormalizeScalarsPragma != normalizeScalarsPragma) {
			NotificationChain msgs = null;
			if (normalizeScalarsPragma != null)
				msgs = ((InternalEObject)normalizeScalarsPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__NORMALIZE_SCALARS_PRAGMA, null, msgs);
			if (newNormalizeScalarsPragma != null)
				msgs = ((InternalEObject)newNormalizeScalarsPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__NORMALIZE_SCALARS_PRAGMA, null, msgs);
			msgs = basicSetNormalizeScalarsPragma(newNormalizeScalarsPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__NORMALIZE_SCALARS_PRAGMA, newNormalizeScalarsPragma, newNormalizeScalarsPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OptimizePragma getOptimizePragma() {
		return optimizePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOptimizePragma(OptimizePragma newOptimizePragma, NotificationChain msgs) {
		OptimizePragma oldOptimizePragma = optimizePragma;
		optimizePragma = newOptimizePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__OPTIMIZE_PRAGMA, oldOptimizePragma, newOptimizePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOptimizePragma(OptimizePragma newOptimizePragma) {
		if (newOptimizePragma != optimizePragma) {
			NotificationChain msgs = null;
			if (optimizePragma != null)
				msgs = ((InternalEObject)optimizePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__OPTIMIZE_PRAGMA, null, msgs);
			if (newOptimizePragma != null)
				msgs = ((InternalEObject)newOptimizePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__OPTIMIZE_PRAGMA, null, msgs);
			msgs = basicSetOptimizePragma(newOptimizePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__OPTIMIZE_PRAGMA, newOptimizePragma, newOptimizePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackPragma getPackPragma() {
		return packPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPackPragma(PackPragma newPackPragma, NotificationChain msgs) {
		PackPragma oldPackPragma = packPragma;
		packPragma = newPackPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PACK_PRAGMA, oldPackPragma, newPackPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPackPragma(PackPragma newPackPragma) {
		if (newPackPragma != packPragma) {
			NotificationChain msgs = null;
			if (packPragma != null)
				msgs = ((InternalEObject)packPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PACK_PRAGMA, null, msgs);
			if (newPackPragma != null)
				msgs = ((InternalEObject)newPackPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PACK_PRAGMA, null, msgs);
			msgs = basicSetPackPragma(newPackPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PACK_PRAGMA, newPackPragma, newPackPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PagePragma getPagePragma() {
		return pagePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPagePragma(PagePragma newPagePragma, NotificationChain msgs) {
		PagePragma oldPagePragma = pagePragma;
		pagePragma = newPagePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PAGE_PRAGMA, oldPagePragma, newPagePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPagePragma(PagePragma newPagePragma) {
		if (newPagePragma != pagePragma) {
			NotificationChain msgs = null;
			if (pagePragma != null)
				msgs = ((InternalEObject)pagePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PAGE_PRAGMA, null, msgs);
			if (newPagePragma != null)
				msgs = ((InternalEObject)newPagePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PAGE_PRAGMA, null, msgs);
			msgs = basicSetPagePragma(newPagePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PAGE_PRAGMA, newPagePragma, newPagePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PreelaboratePragma getPreelaboratePragma() {
		return preelaboratePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPreelaboratePragma(PreelaboratePragma newPreelaboratePragma, NotificationChain msgs) {
		PreelaboratePragma oldPreelaboratePragma = preelaboratePragma;
		preelaboratePragma = newPreelaboratePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PREELABORATE_PRAGMA, oldPreelaboratePragma, newPreelaboratePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreelaboratePragma(PreelaboratePragma newPreelaboratePragma) {
		if (newPreelaboratePragma != preelaboratePragma) {
			NotificationChain msgs = null;
			if (preelaboratePragma != null)
				msgs = ((InternalEObject)preelaboratePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PREELABORATE_PRAGMA, null, msgs);
			if (newPreelaboratePragma != null)
				msgs = ((InternalEObject)newPreelaboratePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PREELABORATE_PRAGMA, null, msgs);
			msgs = basicSetPreelaboratePragma(newPreelaboratePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PREELABORATE_PRAGMA, newPreelaboratePragma, newPreelaboratePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PriorityPragma getPriorityPragma() {
		return priorityPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPriorityPragma(PriorityPragma newPriorityPragma, NotificationChain msgs) {
		PriorityPragma oldPriorityPragma = priorityPragma;
		priorityPragma = newPriorityPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PRIORITY_PRAGMA, oldPriorityPragma, newPriorityPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPriorityPragma(PriorityPragma newPriorityPragma) {
		if (newPriorityPragma != priorityPragma) {
			NotificationChain msgs = null;
			if (priorityPragma != null)
				msgs = ((InternalEObject)priorityPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PRIORITY_PRAGMA, null, msgs);
			if (newPriorityPragma != null)
				msgs = ((InternalEObject)newPriorityPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PRIORITY_PRAGMA, null, msgs);
			msgs = basicSetPriorityPragma(newPriorityPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PRIORITY_PRAGMA, newPriorityPragma, newPriorityPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PurePragma getPurePragma() {
		return purePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPurePragma(PurePragma newPurePragma, NotificationChain msgs) {
		PurePragma oldPurePragma = purePragma;
		purePragma = newPurePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PURE_PRAGMA, oldPurePragma, newPurePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPurePragma(PurePragma newPurePragma) {
		if (newPurePragma != purePragma) {
			NotificationChain msgs = null;
			if (purePragma != null)
				msgs = ((InternalEObject)purePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PURE_PRAGMA, null, msgs);
			if (newPurePragma != null)
				msgs = ((InternalEObject)newPurePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PURE_PRAGMA, null, msgs);
			msgs = basicSetPurePragma(newPurePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PURE_PRAGMA, newPurePragma, newPurePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QueuingPolicyPragma getQueuingPolicyPragma() {
		return queuingPolicyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetQueuingPolicyPragma(QueuingPolicyPragma newQueuingPolicyPragma, NotificationChain msgs) {
		QueuingPolicyPragma oldQueuingPolicyPragma = queuingPolicyPragma;
		queuingPolicyPragma = newQueuingPolicyPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__QUEUING_POLICY_PRAGMA, oldQueuingPolicyPragma, newQueuingPolicyPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQueuingPolicyPragma(QueuingPolicyPragma newQueuingPolicyPragma) {
		if (newQueuingPolicyPragma != queuingPolicyPragma) {
			NotificationChain msgs = null;
			if (queuingPolicyPragma != null)
				msgs = ((InternalEObject)queuingPolicyPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__QUEUING_POLICY_PRAGMA, null, msgs);
			if (newQueuingPolicyPragma != null)
				msgs = ((InternalEObject)newQueuingPolicyPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__QUEUING_POLICY_PRAGMA, null, msgs);
			msgs = basicSetQueuingPolicyPragma(newQueuingPolicyPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__QUEUING_POLICY_PRAGMA, newQueuingPolicyPragma, newQueuingPolicyPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RemoteCallInterfacePragma getRemoteCallInterfacePragma() {
		return remoteCallInterfacePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRemoteCallInterfacePragma(RemoteCallInterfacePragma newRemoteCallInterfacePragma, NotificationChain msgs) {
		RemoteCallInterfacePragma oldRemoteCallInterfacePragma = remoteCallInterfacePragma;
		remoteCallInterfacePragma = newRemoteCallInterfacePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA, oldRemoteCallInterfacePragma, newRemoteCallInterfacePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRemoteCallInterfacePragma(RemoteCallInterfacePragma newRemoteCallInterfacePragma) {
		if (newRemoteCallInterfacePragma != remoteCallInterfacePragma) {
			NotificationChain msgs = null;
			if (remoteCallInterfacePragma != null)
				msgs = ((InternalEObject)remoteCallInterfacePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA, null, msgs);
			if (newRemoteCallInterfacePragma != null)
				msgs = ((InternalEObject)newRemoteCallInterfacePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA, null, msgs);
			msgs = basicSetRemoteCallInterfacePragma(newRemoteCallInterfacePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA, newRemoteCallInterfacePragma, newRemoteCallInterfacePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RemoteTypesPragma getRemoteTypesPragma() {
		return remoteTypesPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRemoteTypesPragma(RemoteTypesPragma newRemoteTypesPragma, NotificationChain msgs) {
		RemoteTypesPragma oldRemoteTypesPragma = remoteTypesPragma;
		remoteTypesPragma = newRemoteTypesPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__REMOTE_TYPES_PRAGMA, oldRemoteTypesPragma, newRemoteTypesPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRemoteTypesPragma(RemoteTypesPragma newRemoteTypesPragma) {
		if (newRemoteTypesPragma != remoteTypesPragma) {
			NotificationChain msgs = null;
			if (remoteTypesPragma != null)
				msgs = ((InternalEObject)remoteTypesPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__REMOTE_TYPES_PRAGMA, null, msgs);
			if (newRemoteTypesPragma != null)
				msgs = ((InternalEObject)newRemoteTypesPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__REMOTE_TYPES_PRAGMA, null, msgs);
			msgs = basicSetRemoteTypesPragma(newRemoteTypesPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__REMOTE_TYPES_PRAGMA, newRemoteTypesPragma, newRemoteTypesPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RestrictionsPragma getRestrictionsPragma() {
		return restrictionsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRestrictionsPragma(RestrictionsPragma newRestrictionsPragma, NotificationChain msgs) {
		RestrictionsPragma oldRestrictionsPragma = restrictionsPragma;
		restrictionsPragma = newRestrictionsPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__RESTRICTIONS_PRAGMA, oldRestrictionsPragma, newRestrictionsPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRestrictionsPragma(RestrictionsPragma newRestrictionsPragma) {
		if (newRestrictionsPragma != restrictionsPragma) {
			NotificationChain msgs = null;
			if (restrictionsPragma != null)
				msgs = ((InternalEObject)restrictionsPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__RESTRICTIONS_PRAGMA, null, msgs);
			if (newRestrictionsPragma != null)
				msgs = ((InternalEObject)newRestrictionsPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__RESTRICTIONS_PRAGMA, null, msgs);
			msgs = basicSetRestrictionsPragma(newRestrictionsPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__RESTRICTIONS_PRAGMA, newRestrictionsPragma, newRestrictionsPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReviewablePragma getReviewablePragma() {
		return reviewablePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReviewablePragma(ReviewablePragma newReviewablePragma, NotificationChain msgs) {
		ReviewablePragma oldReviewablePragma = reviewablePragma;
		reviewablePragma = newReviewablePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__REVIEWABLE_PRAGMA, oldReviewablePragma, newReviewablePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReviewablePragma(ReviewablePragma newReviewablePragma) {
		if (newReviewablePragma != reviewablePragma) {
			NotificationChain msgs = null;
			if (reviewablePragma != null)
				msgs = ((InternalEObject)reviewablePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__REVIEWABLE_PRAGMA, null, msgs);
			if (newReviewablePragma != null)
				msgs = ((InternalEObject)newReviewablePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__REVIEWABLE_PRAGMA, null, msgs);
			msgs = basicSetReviewablePragma(newReviewablePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__REVIEWABLE_PRAGMA, newReviewablePragma, newReviewablePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SharedPassivePragma getSharedPassivePragma() {
		return sharedPassivePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSharedPassivePragma(SharedPassivePragma newSharedPassivePragma, NotificationChain msgs) {
		SharedPassivePragma oldSharedPassivePragma = sharedPassivePragma;
		sharedPassivePragma = newSharedPassivePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__SHARED_PASSIVE_PRAGMA, oldSharedPassivePragma, newSharedPassivePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSharedPassivePragma(SharedPassivePragma newSharedPassivePragma) {
		if (newSharedPassivePragma != sharedPassivePragma) {
			NotificationChain msgs = null;
			if (sharedPassivePragma != null)
				msgs = ((InternalEObject)sharedPassivePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__SHARED_PASSIVE_PRAGMA, null, msgs);
			if (newSharedPassivePragma != null)
				msgs = ((InternalEObject)newSharedPassivePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__SHARED_PASSIVE_PRAGMA, null, msgs);
			msgs = basicSetSharedPassivePragma(newSharedPassivePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__SHARED_PASSIVE_PRAGMA, newSharedPassivePragma, newSharedPassivePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StorageSizePragma getStorageSizePragma() {
		return storageSizePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStorageSizePragma(StorageSizePragma newStorageSizePragma, NotificationChain msgs) {
		StorageSizePragma oldStorageSizePragma = storageSizePragma;
		storageSizePragma = newStorageSizePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__STORAGE_SIZE_PRAGMA, oldStorageSizePragma, newStorageSizePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStorageSizePragma(StorageSizePragma newStorageSizePragma) {
		if (newStorageSizePragma != storageSizePragma) {
			NotificationChain msgs = null;
			if (storageSizePragma != null)
				msgs = ((InternalEObject)storageSizePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__STORAGE_SIZE_PRAGMA, null, msgs);
			if (newStorageSizePragma != null)
				msgs = ((InternalEObject)newStorageSizePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__STORAGE_SIZE_PRAGMA, null, msgs);
			msgs = basicSetStorageSizePragma(newStorageSizePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__STORAGE_SIZE_PRAGMA, newStorageSizePragma, newStorageSizePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SuppressPragma getSuppressPragma() {
		return suppressPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSuppressPragma(SuppressPragma newSuppressPragma, NotificationChain msgs) {
		SuppressPragma oldSuppressPragma = suppressPragma;
		suppressPragma = newSuppressPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__SUPPRESS_PRAGMA, oldSuppressPragma, newSuppressPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuppressPragma(SuppressPragma newSuppressPragma) {
		if (newSuppressPragma != suppressPragma) {
			NotificationChain msgs = null;
			if (suppressPragma != null)
				msgs = ((InternalEObject)suppressPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__SUPPRESS_PRAGMA, null, msgs);
			if (newSuppressPragma != null)
				msgs = ((InternalEObject)newSuppressPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__SUPPRESS_PRAGMA, null, msgs);
			msgs = basicSetSuppressPragma(newSuppressPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__SUPPRESS_PRAGMA, newSuppressPragma, newSuppressPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskDispatchingPolicyPragma getTaskDispatchingPolicyPragma() {
		return taskDispatchingPolicyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTaskDispatchingPolicyPragma(TaskDispatchingPolicyPragma newTaskDispatchingPolicyPragma, NotificationChain msgs) {
		TaskDispatchingPolicyPragma oldTaskDispatchingPolicyPragma = taskDispatchingPolicyPragma;
		taskDispatchingPolicyPragma = newTaskDispatchingPolicyPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA, oldTaskDispatchingPolicyPragma, newTaskDispatchingPolicyPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaskDispatchingPolicyPragma(TaskDispatchingPolicyPragma newTaskDispatchingPolicyPragma) {
		if (newTaskDispatchingPolicyPragma != taskDispatchingPolicyPragma) {
			NotificationChain msgs = null;
			if (taskDispatchingPolicyPragma != null)
				msgs = ((InternalEObject)taskDispatchingPolicyPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA, null, msgs);
			if (newTaskDispatchingPolicyPragma != null)
				msgs = ((InternalEObject)newTaskDispatchingPolicyPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA, null, msgs);
			msgs = basicSetTaskDispatchingPolicyPragma(newTaskDispatchingPolicyPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA, newTaskDispatchingPolicyPragma, newTaskDispatchingPolicyPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VolatilePragma getVolatilePragma() {
		return volatilePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVolatilePragma(VolatilePragma newVolatilePragma, NotificationChain msgs) {
		VolatilePragma oldVolatilePragma = volatilePragma;
		volatilePragma = newVolatilePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__VOLATILE_PRAGMA, oldVolatilePragma, newVolatilePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVolatilePragma(VolatilePragma newVolatilePragma) {
		if (newVolatilePragma != volatilePragma) {
			NotificationChain msgs = null;
			if (volatilePragma != null)
				msgs = ((InternalEObject)volatilePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__VOLATILE_PRAGMA, null, msgs);
			if (newVolatilePragma != null)
				msgs = ((InternalEObject)newVolatilePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__VOLATILE_PRAGMA, null, msgs);
			msgs = basicSetVolatilePragma(newVolatilePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__VOLATILE_PRAGMA, newVolatilePragma, newVolatilePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VolatileComponentsPragma getVolatileComponentsPragma() {
		return volatileComponentsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVolatileComponentsPragma(VolatileComponentsPragma newVolatileComponentsPragma, NotificationChain msgs) {
		VolatileComponentsPragma oldVolatileComponentsPragma = volatileComponentsPragma;
		volatileComponentsPragma = newVolatileComponentsPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__VOLATILE_COMPONENTS_PRAGMA, oldVolatileComponentsPragma, newVolatileComponentsPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVolatileComponentsPragma(VolatileComponentsPragma newVolatileComponentsPragma) {
		if (newVolatileComponentsPragma != volatileComponentsPragma) {
			NotificationChain msgs = null;
			if (volatileComponentsPragma != null)
				msgs = ((InternalEObject)volatileComponentsPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__VOLATILE_COMPONENTS_PRAGMA, null, msgs);
			if (newVolatileComponentsPragma != null)
				msgs = ((InternalEObject)newVolatileComponentsPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__VOLATILE_COMPONENTS_PRAGMA, null, msgs);
			msgs = basicSetVolatileComponentsPragma(newVolatileComponentsPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__VOLATILE_COMPONENTS_PRAGMA, newVolatileComponentsPragma, newVolatileComponentsPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertPragma getAssertPragma() {
		return assertPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssertPragma(AssertPragma newAssertPragma, NotificationChain msgs) {
		AssertPragma oldAssertPragma = assertPragma;
		assertPragma = newAssertPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ASSERT_PRAGMA, oldAssertPragma, newAssertPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssertPragma(AssertPragma newAssertPragma) {
		if (newAssertPragma != assertPragma) {
			NotificationChain msgs = null;
			if (assertPragma != null)
				msgs = ((InternalEObject)assertPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ASSERT_PRAGMA, null, msgs);
			if (newAssertPragma != null)
				msgs = ((InternalEObject)newAssertPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ASSERT_PRAGMA, null, msgs);
			msgs = basicSetAssertPragma(newAssertPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ASSERT_PRAGMA, newAssertPragma, newAssertPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertionPolicyPragma getAssertionPolicyPragma() {
		return assertionPolicyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssertionPolicyPragma(AssertionPolicyPragma newAssertionPolicyPragma, NotificationChain msgs) {
		AssertionPolicyPragma oldAssertionPolicyPragma = assertionPolicyPragma;
		assertionPolicyPragma = newAssertionPolicyPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ASSERTION_POLICY_PRAGMA, oldAssertionPolicyPragma, newAssertionPolicyPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssertionPolicyPragma(AssertionPolicyPragma newAssertionPolicyPragma) {
		if (newAssertionPolicyPragma != assertionPolicyPragma) {
			NotificationChain msgs = null;
			if (assertionPolicyPragma != null)
				msgs = ((InternalEObject)assertionPolicyPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ASSERTION_POLICY_PRAGMA, null, msgs);
			if (newAssertionPolicyPragma != null)
				msgs = ((InternalEObject)newAssertionPolicyPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__ASSERTION_POLICY_PRAGMA, null, msgs);
			msgs = basicSetAssertionPolicyPragma(newAssertionPolicyPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__ASSERTION_POLICY_PRAGMA, newAssertionPolicyPragma, newAssertionPolicyPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DetectBlockingPragma getDetectBlockingPragma() {
		return detectBlockingPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDetectBlockingPragma(DetectBlockingPragma newDetectBlockingPragma, NotificationChain msgs) {
		DetectBlockingPragma oldDetectBlockingPragma = detectBlockingPragma;
		detectBlockingPragma = newDetectBlockingPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__DETECT_BLOCKING_PRAGMA, oldDetectBlockingPragma, newDetectBlockingPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDetectBlockingPragma(DetectBlockingPragma newDetectBlockingPragma) {
		if (newDetectBlockingPragma != detectBlockingPragma) {
			NotificationChain msgs = null;
			if (detectBlockingPragma != null)
				msgs = ((InternalEObject)detectBlockingPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__DETECT_BLOCKING_PRAGMA, null, msgs);
			if (newDetectBlockingPragma != null)
				msgs = ((InternalEObject)newDetectBlockingPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__DETECT_BLOCKING_PRAGMA, null, msgs);
			msgs = basicSetDetectBlockingPragma(newDetectBlockingPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__DETECT_BLOCKING_PRAGMA, newDetectBlockingPragma, newDetectBlockingPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NoReturnPragma getNoReturnPragma() {
		return noReturnPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNoReturnPragma(NoReturnPragma newNoReturnPragma, NotificationChain msgs) {
		NoReturnPragma oldNoReturnPragma = noReturnPragma;
		noReturnPragma = newNoReturnPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__NO_RETURN_PRAGMA, oldNoReturnPragma, newNoReturnPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNoReturnPragma(NoReturnPragma newNoReturnPragma) {
		if (newNoReturnPragma != noReturnPragma) {
			NotificationChain msgs = null;
			if (noReturnPragma != null)
				msgs = ((InternalEObject)noReturnPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__NO_RETURN_PRAGMA, null, msgs);
			if (newNoReturnPragma != null)
				msgs = ((InternalEObject)newNoReturnPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__NO_RETURN_PRAGMA, null, msgs);
			msgs = basicSetNoReturnPragma(newNoReturnPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__NO_RETURN_PRAGMA, newNoReturnPragma, newNoReturnPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PartitionElaborationPolicyPragma getPartitionElaborationPolicyPragma() {
		return partitionElaborationPolicyPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPartitionElaborationPolicyPragma(PartitionElaborationPolicyPragma newPartitionElaborationPolicyPragma, NotificationChain msgs) {
		PartitionElaborationPolicyPragma oldPartitionElaborationPolicyPragma = partitionElaborationPolicyPragma;
		partitionElaborationPolicyPragma = newPartitionElaborationPolicyPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA, oldPartitionElaborationPolicyPragma, newPartitionElaborationPolicyPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPartitionElaborationPolicyPragma(PartitionElaborationPolicyPragma newPartitionElaborationPolicyPragma) {
		if (newPartitionElaborationPolicyPragma != partitionElaborationPolicyPragma) {
			NotificationChain msgs = null;
			if (partitionElaborationPolicyPragma != null)
				msgs = ((InternalEObject)partitionElaborationPolicyPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA, null, msgs);
			if (newPartitionElaborationPolicyPragma != null)
				msgs = ((InternalEObject)newPartitionElaborationPolicyPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA, null, msgs);
			msgs = basicSetPartitionElaborationPolicyPragma(newPartitionElaborationPolicyPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA, newPartitionElaborationPolicyPragma, newPartitionElaborationPolicyPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PreelaborableInitializationPragma getPreelaborableInitializationPragma() {
		return preelaborableInitializationPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPreelaborableInitializationPragma(PreelaborableInitializationPragma newPreelaborableInitializationPragma, NotificationChain msgs) {
		PreelaborableInitializationPragma oldPreelaborableInitializationPragma = preelaborableInitializationPragma;
		preelaborableInitializationPragma = newPreelaborableInitializationPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA, oldPreelaborableInitializationPragma, newPreelaborableInitializationPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreelaborableInitializationPragma(PreelaborableInitializationPragma newPreelaborableInitializationPragma) {
		if (newPreelaborableInitializationPragma != preelaborableInitializationPragma) {
			NotificationChain msgs = null;
			if (preelaborableInitializationPragma != null)
				msgs = ((InternalEObject)preelaborableInitializationPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA, null, msgs);
			if (newPreelaborableInitializationPragma != null)
				msgs = ((InternalEObject)newPreelaborableInitializationPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA, null, msgs);
			msgs = basicSetPreelaborableInitializationPragma(newPreelaborableInitializationPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA, newPreelaborableInitializationPragma, newPreelaborableInitializationPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrioritySpecificDispatchingPragma getPrioritySpecificDispatchingPragma() {
		return prioritySpecificDispatchingPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPrioritySpecificDispatchingPragma(PrioritySpecificDispatchingPragma newPrioritySpecificDispatchingPragma, NotificationChain msgs) {
		PrioritySpecificDispatchingPragma oldPrioritySpecificDispatchingPragma = prioritySpecificDispatchingPragma;
		prioritySpecificDispatchingPragma = newPrioritySpecificDispatchingPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA, oldPrioritySpecificDispatchingPragma, newPrioritySpecificDispatchingPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrioritySpecificDispatchingPragma(PrioritySpecificDispatchingPragma newPrioritySpecificDispatchingPragma) {
		if (newPrioritySpecificDispatchingPragma != prioritySpecificDispatchingPragma) {
			NotificationChain msgs = null;
			if (prioritySpecificDispatchingPragma != null)
				msgs = ((InternalEObject)prioritySpecificDispatchingPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA, null, msgs);
			if (newPrioritySpecificDispatchingPragma != null)
				msgs = ((InternalEObject)newPrioritySpecificDispatchingPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA, null, msgs);
			msgs = basicSetPrioritySpecificDispatchingPragma(newPrioritySpecificDispatchingPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA, newPrioritySpecificDispatchingPragma, newPrioritySpecificDispatchingPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProfilePragma getProfilePragma() {
		return profilePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProfilePragma(ProfilePragma newProfilePragma, NotificationChain msgs) {
		ProfilePragma oldProfilePragma = profilePragma;
		profilePragma = newProfilePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PROFILE_PRAGMA, oldProfilePragma, newProfilePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProfilePragma(ProfilePragma newProfilePragma) {
		if (newProfilePragma != profilePragma) {
			NotificationChain msgs = null;
			if (profilePragma != null)
				msgs = ((InternalEObject)profilePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PROFILE_PRAGMA, null, msgs);
			if (newProfilePragma != null)
				msgs = ((InternalEObject)newProfilePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__PROFILE_PRAGMA, null, msgs);
			msgs = basicSetProfilePragma(newProfilePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__PROFILE_PRAGMA, newProfilePragma, newProfilePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RelativeDeadlinePragma getRelativeDeadlinePragma() {
		return relativeDeadlinePragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRelativeDeadlinePragma(RelativeDeadlinePragma newRelativeDeadlinePragma, NotificationChain msgs) {
		RelativeDeadlinePragma oldRelativeDeadlinePragma = relativeDeadlinePragma;
		relativeDeadlinePragma = newRelativeDeadlinePragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__RELATIVE_DEADLINE_PRAGMA, oldRelativeDeadlinePragma, newRelativeDeadlinePragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRelativeDeadlinePragma(RelativeDeadlinePragma newRelativeDeadlinePragma) {
		if (newRelativeDeadlinePragma != relativeDeadlinePragma) {
			NotificationChain msgs = null;
			if (relativeDeadlinePragma != null)
				msgs = ((InternalEObject)relativeDeadlinePragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__RELATIVE_DEADLINE_PRAGMA, null, msgs);
			if (newRelativeDeadlinePragma != null)
				msgs = ((InternalEObject)newRelativeDeadlinePragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__RELATIVE_DEADLINE_PRAGMA, null, msgs);
			msgs = basicSetRelativeDeadlinePragma(newRelativeDeadlinePragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__RELATIVE_DEADLINE_PRAGMA, newRelativeDeadlinePragma, newRelativeDeadlinePragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UncheckedUnionPragma getUncheckedUnionPragma() {
		return uncheckedUnionPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUncheckedUnionPragma(UncheckedUnionPragma newUncheckedUnionPragma, NotificationChain msgs) {
		UncheckedUnionPragma oldUncheckedUnionPragma = uncheckedUnionPragma;
		uncheckedUnionPragma = newUncheckedUnionPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__UNCHECKED_UNION_PRAGMA, oldUncheckedUnionPragma, newUncheckedUnionPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUncheckedUnionPragma(UncheckedUnionPragma newUncheckedUnionPragma) {
		if (newUncheckedUnionPragma != uncheckedUnionPragma) {
			NotificationChain msgs = null;
			if (uncheckedUnionPragma != null)
				msgs = ((InternalEObject)uncheckedUnionPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__UNCHECKED_UNION_PRAGMA, null, msgs);
			if (newUncheckedUnionPragma != null)
				msgs = ((InternalEObject)newUncheckedUnionPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__UNCHECKED_UNION_PRAGMA, null, msgs);
			msgs = basicSetUncheckedUnionPragma(newUncheckedUnionPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__UNCHECKED_UNION_PRAGMA, newUncheckedUnionPragma, newUncheckedUnionPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnsuppressPragma getUnsuppressPragma() {
		return unsuppressPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnsuppressPragma(UnsuppressPragma newUnsuppressPragma, NotificationChain msgs) {
		UnsuppressPragma oldUnsuppressPragma = unsuppressPragma;
		unsuppressPragma = newUnsuppressPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__UNSUPPRESS_PRAGMA, oldUnsuppressPragma, newUnsuppressPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnsuppressPragma(UnsuppressPragma newUnsuppressPragma) {
		if (newUnsuppressPragma != unsuppressPragma) {
			NotificationChain msgs = null;
			if (unsuppressPragma != null)
				msgs = ((InternalEObject)unsuppressPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__UNSUPPRESS_PRAGMA, null, msgs);
			if (newUnsuppressPragma != null)
				msgs = ((InternalEObject)newUnsuppressPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__UNSUPPRESS_PRAGMA, null, msgs);
			msgs = basicSetUnsuppressPragma(newUnsuppressPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__UNSUPPRESS_PRAGMA, newUnsuppressPragma, newUnsuppressPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefaultStoragePoolPragma getDefaultStoragePoolPragma() {
		return defaultStoragePoolPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDefaultStoragePoolPragma(DefaultStoragePoolPragma newDefaultStoragePoolPragma, NotificationChain msgs) {
		DefaultStoragePoolPragma oldDefaultStoragePoolPragma = defaultStoragePoolPragma;
		defaultStoragePoolPragma = newDefaultStoragePoolPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA, oldDefaultStoragePoolPragma, newDefaultStoragePoolPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultStoragePoolPragma(DefaultStoragePoolPragma newDefaultStoragePoolPragma) {
		if (newDefaultStoragePoolPragma != defaultStoragePoolPragma) {
			NotificationChain msgs = null;
			if (defaultStoragePoolPragma != null)
				msgs = ((InternalEObject)defaultStoragePoolPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA, null, msgs);
			if (newDefaultStoragePoolPragma != null)
				msgs = ((InternalEObject)newDefaultStoragePoolPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA, null, msgs);
			msgs = basicSetDefaultStoragePoolPragma(newDefaultStoragePoolPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA, newDefaultStoragePoolPragma, newDefaultStoragePoolPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DispatchingDomainPragma getDispatchingDomainPragma() {
		return dispatchingDomainPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDispatchingDomainPragma(DispatchingDomainPragma newDispatchingDomainPragma, NotificationChain msgs) {
		DispatchingDomainPragma oldDispatchingDomainPragma = dispatchingDomainPragma;
		dispatchingDomainPragma = newDispatchingDomainPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__DISPATCHING_DOMAIN_PRAGMA, oldDispatchingDomainPragma, newDispatchingDomainPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDispatchingDomainPragma(DispatchingDomainPragma newDispatchingDomainPragma) {
		if (newDispatchingDomainPragma != dispatchingDomainPragma) {
			NotificationChain msgs = null;
			if (dispatchingDomainPragma != null)
				msgs = ((InternalEObject)dispatchingDomainPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__DISPATCHING_DOMAIN_PRAGMA, null, msgs);
			if (newDispatchingDomainPragma != null)
				msgs = ((InternalEObject)newDispatchingDomainPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__DISPATCHING_DOMAIN_PRAGMA, null, msgs);
			msgs = basicSetDispatchingDomainPragma(newDispatchingDomainPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__DISPATCHING_DOMAIN_PRAGMA, newDispatchingDomainPragma, newDispatchingDomainPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CpuPragma getCpuPragma() {
		return cpuPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCpuPragma(CpuPragma newCpuPragma, NotificationChain msgs) {
		CpuPragma oldCpuPragma = cpuPragma;
		cpuPragma = newCpuPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__CPU_PRAGMA, oldCpuPragma, newCpuPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCpuPragma(CpuPragma newCpuPragma) {
		if (newCpuPragma != cpuPragma) {
			NotificationChain msgs = null;
			if (cpuPragma != null)
				msgs = ((InternalEObject)cpuPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__CPU_PRAGMA, null, msgs);
			if (newCpuPragma != null)
				msgs = ((InternalEObject)newCpuPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__CPU_PRAGMA, null, msgs);
			msgs = basicSetCpuPragma(newCpuPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__CPU_PRAGMA, newCpuPragma, newCpuPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IndependentPragma getIndependentPragma() {
		return independentPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIndependentPragma(IndependentPragma newIndependentPragma, NotificationChain msgs) {
		IndependentPragma oldIndependentPragma = independentPragma;
		independentPragma = newIndependentPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__INDEPENDENT_PRAGMA, oldIndependentPragma, newIndependentPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndependentPragma(IndependentPragma newIndependentPragma) {
		if (newIndependentPragma != independentPragma) {
			NotificationChain msgs = null;
			if (independentPragma != null)
				msgs = ((InternalEObject)independentPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__INDEPENDENT_PRAGMA, null, msgs);
			if (newIndependentPragma != null)
				msgs = ((InternalEObject)newIndependentPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__INDEPENDENT_PRAGMA, null, msgs);
			msgs = basicSetIndependentPragma(newIndependentPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__INDEPENDENT_PRAGMA, newIndependentPragma, newIndependentPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IndependentComponentsPragma getIndependentComponentsPragma() {
		return independentComponentsPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIndependentComponentsPragma(IndependentComponentsPragma newIndependentComponentsPragma, NotificationChain msgs) {
		IndependentComponentsPragma oldIndependentComponentsPragma = independentComponentsPragma;
		independentComponentsPragma = newIndependentComponentsPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA, oldIndependentComponentsPragma, newIndependentComponentsPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndependentComponentsPragma(IndependentComponentsPragma newIndependentComponentsPragma) {
		if (newIndependentComponentsPragma != independentComponentsPragma) {
			NotificationChain msgs = null;
			if (independentComponentsPragma != null)
				msgs = ((InternalEObject)independentComponentsPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA, null, msgs);
			if (newIndependentComponentsPragma != null)
				msgs = ((InternalEObject)newIndependentComponentsPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA, null, msgs);
			msgs = basicSetIndependentComponentsPragma(newIndependentComponentsPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA, newIndependentComponentsPragma, newIndependentComponentsPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImplementationDefinedPragma getImplementationDefinedPragma() {
		return implementationDefinedPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImplementationDefinedPragma(ImplementationDefinedPragma newImplementationDefinedPragma, NotificationChain msgs) {
		ImplementationDefinedPragma oldImplementationDefinedPragma = implementationDefinedPragma;
		implementationDefinedPragma = newImplementationDefinedPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA, oldImplementationDefinedPragma, newImplementationDefinedPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImplementationDefinedPragma(ImplementationDefinedPragma newImplementationDefinedPragma) {
		if (newImplementationDefinedPragma != implementationDefinedPragma) {
			NotificationChain msgs = null;
			if (implementationDefinedPragma != null)
				msgs = ((InternalEObject)implementationDefinedPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA, null, msgs);
			if (newImplementationDefinedPragma != null)
				msgs = ((InternalEObject)newImplementationDefinedPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA, null, msgs);
			msgs = basicSetImplementationDefinedPragma(newImplementationDefinedPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA, newImplementationDefinedPragma, newImplementationDefinedPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnknownPragma getUnknownPragma() {
		return unknownPragma;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUnknownPragma(UnknownPragma newUnknownPragma, NotificationChain msgs) {
		UnknownPragma oldUnknownPragma = unknownPragma;
		unknownPragma = newUnknownPragma;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__UNKNOWN_PRAGMA, oldUnknownPragma, newUnknownPragma);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnknownPragma(UnknownPragma newUnknownPragma) {
		if (newUnknownPragma != unknownPragma) {
			NotificationChain msgs = null;
			if (unknownPragma != null)
				msgs = ((InternalEObject)unknownPragma).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__UNKNOWN_PRAGMA, null, msgs);
			if (newUnknownPragma != null)
				msgs = ((InternalEObject)newUnknownPragma).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.DECLARATION_CLASS__UNKNOWN_PRAGMA, null, msgs);
			msgs = basicSetUnknownPragma(newUnknownPragma, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.DECLARATION_CLASS__UNKNOWN_PRAGMA, newUnknownPragma, newUnknownPragma));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.DECLARATION_CLASS__NOT_AN_ELEMENT:
				return basicSetNotAnElement(null, msgs);
			case AdaPackage.DECLARATION_CLASS__ORDINARY_TYPE_DECLARATION:
				return basicSetOrdinaryTypeDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__TASK_TYPE_DECLARATION:
				return basicSetTaskTypeDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__PROTECTED_TYPE_DECLARATION:
				return basicSetProtectedTypeDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__INCOMPLETE_TYPE_DECLARATION:
				return basicSetIncompleteTypeDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__TAGGED_INCOMPLETE_TYPE_DECLARATION:
				return basicSetTaggedIncompleteTypeDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__PRIVATE_TYPE_DECLARATION:
				return basicSetPrivateTypeDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__PRIVATE_EXTENSION_DECLARATION:
				return basicSetPrivateExtensionDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__SUBTYPE_DECLARATION:
				return basicSetSubtypeDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__VARIABLE_DECLARATION:
				return basicSetVariableDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__CONSTANT_DECLARATION:
				return basicSetConstantDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__DEFERRED_CONSTANT_DECLARATION:
				return basicSetDeferredConstantDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__SINGLE_TASK_DECLARATION:
				return basicSetSingleTaskDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__SINGLE_PROTECTED_DECLARATION:
				return basicSetSingleProtectedDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__INTEGER_NUMBER_DECLARATION:
				return basicSetIntegerNumberDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__REAL_NUMBER_DECLARATION:
				return basicSetRealNumberDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__ENUMERATION_LITERAL_SPECIFICATION:
				return basicSetEnumerationLiteralSpecification(null, msgs);
			case AdaPackage.DECLARATION_CLASS__DISCRIMINANT_SPECIFICATION:
				return basicSetDiscriminantSpecification(null, msgs);
			case AdaPackage.DECLARATION_CLASS__COMPONENT_DECLARATION:
				return basicSetComponentDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__LOOP_PARAMETER_SPECIFICATION:
				return basicSetLoopParameterSpecification(null, msgs);
			case AdaPackage.DECLARATION_CLASS__GENERALIZED_ITERATOR_SPECIFICATION:
				return basicSetGeneralizedIteratorSpecification(null, msgs);
			case AdaPackage.DECLARATION_CLASS__ELEMENT_ITERATOR_SPECIFICATION:
				return basicSetElementIteratorSpecification(null, msgs);
			case AdaPackage.DECLARATION_CLASS__PROCEDURE_DECLARATION:
				return basicSetProcedureDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__FUNCTION_DECLARATION:
				return basicSetFunctionDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__PARAMETER_SPECIFICATION:
				return basicSetParameterSpecification(null, msgs);
			case AdaPackage.DECLARATION_CLASS__PROCEDURE_BODY_DECLARATION:
				return basicSetProcedureBodyDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__FUNCTION_BODY_DECLARATION:
				return basicSetFunctionBodyDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__RETURN_VARIABLE_SPECIFICATION:
				return basicSetReturnVariableSpecification(null, msgs);
			case AdaPackage.DECLARATION_CLASS__RETURN_CONSTANT_SPECIFICATION:
				return basicSetReturnConstantSpecification(null, msgs);
			case AdaPackage.DECLARATION_CLASS__NULL_PROCEDURE_DECLARATION:
				return basicSetNullProcedureDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__EXPRESSION_FUNCTION_DECLARATION:
				return basicSetExpressionFunctionDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__PACKAGE_DECLARATION:
				return basicSetPackageDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__PACKAGE_BODY_DECLARATION:
				return basicSetPackageBodyDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__OBJECT_RENAMING_DECLARATION:
				return basicSetObjectRenamingDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__EXCEPTION_RENAMING_DECLARATION:
				return basicSetExceptionRenamingDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__PACKAGE_RENAMING_DECLARATION:
				return basicSetPackageRenamingDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__PROCEDURE_RENAMING_DECLARATION:
				return basicSetProcedureRenamingDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__FUNCTION_RENAMING_DECLARATION:
				return basicSetFunctionRenamingDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__GENERIC_PACKAGE_RENAMING_DECLARATION:
				return basicSetGenericPackageRenamingDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__GENERIC_PROCEDURE_RENAMING_DECLARATION:
				return basicSetGenericProcedureRenamingDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__GENERIC_FUNCTION_RENAMING_DECLARATION:
				return basicSetGenericFunctionRenamingDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__TASK_BODY_DECLARATION:
				return basicSetTaskBodyDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__PROTECTED_BODY_DECLARATION:
				return basicSetProtectedBodyDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__ENTRY_DECLARATION:
				return basicSetEntryDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__ENTRY_BODY_DECLARATION:
				return basicSetEntryBodyDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__ENTRY_INDEX_SPECIFICATION:
				return basicSetEntryIndexSpecification(null, msgs);
			case AdaPackage.DECLARATION_CLASS__PROCEDURE_BODY_STUB:
				return basicSetProcedureBodyStub(null, msgs);
			case AdaPackage.DECLARATION_CLASS__FUNCTION_BODY_STUB:
				return basicSetFunctionBodyStub(null, msgs);
			case AdaPackage.DECLARATION_CLASS__PACKAGE_BODY_STUB:
				return basicSetPackageBodyStub(null, msgs);
			case AdaPackage.DECLARATION_CLASS__TASK_BODY_STUB:
				return basicSetTaskBodyStub(null, msgs);
			case AdaPackage.DECLARATION_CLASS__PROTECTED_BODY_STUB:
				return basicSetProtectedBodyStub(null, msgs);
			case AdaPackage.DECLARATION_CLASS__EXCEPTION_DECLARATION:
				return basicSetExceptionDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__CHOICE_PARAMETER_SPECIFICATION:
				return basicSetChoiceParameterSpecification(null, msgs);
			case AdaPackage.DECLARATION_CLASS__GENERIC_PROCEDURE_DECLARATION:
				return basicSetGenericProcedureDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__GENERIC_FUNCTION_DECLARATION:
				return basicSetGenericFunctionDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__GENERIC_PACKAGE_DECLARATION:
				return basicSetGenericPackageDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__PACKAGE_INSTANTIATION:
				return basicSetPackageInstantiation(null, msgs);
			case AdaPackage.DECLARATION_CLASS__PROCEDURE_INSTANTIATION:
				return basicSetProcedureInstantiation(null, msgs);
			case AdaPackage.DECLARATION_CLASS__FUNCTION_INSTANTIATION:
				return basicSetFunctionInstantiation(null, msgs);
			case AdaPackage.DECLARATION_CLASS__FORMAL_OBJECT_DECLARATION:
				return basicSetFormalObjectDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__FORMAL_TYPE_DECLARATION:
				return basicSetFormalTypeDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__FORMAL_INCOMPLETE_TYPE_DECLARATION:
				return basicSetFormalIncompleteTypeDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__FORMAL_PROCEDURE_DECLARATION:
				return basicSetFormalProcedureDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__FORMAL_FUNCTION_DECLARATION:
				return basicSetFormalFunctionDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__FORMAL_PACKAGE_DECLARATION:
				return basicSetFormalPackageDeclaration(null, msgs);
			case AdaPackage.DECLARATION_CLASS__FORMAL_PACKAGE_DECLARATION_WITH_BOX:
				return basicSetFormalPackageDeclarationWithBox(null, msgs);
			case AdaPackage.DECLARATION_CLASS__COMMENT:
				return basicSetComment(null, msgs);
			case AdaPackage.DECLARATION_CLASS__ALL_CALLS_REMOTE_PRAGMA:
				return basicSetAllCallsRemotePragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__ASYNCHRONOUS_PRAGMA:
				return basicSetAsynchronousPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__ATOMIC_PRAGMA:
				return basicSetAtomicPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__ATOMIC_COMPONENTS_PRAGMA:
				return basicSetAtomicComponentsPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__ATTACH_HANDLER_PRAGMA:
				return basicSetAttachHandlerPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__CONTROLLED_PRAGMA:
				return basicSetControlledPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__CONVENTION_PRAGMA:
				return basicSetConventionPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__DISCARD_NAMES_PRAGMA:
				return basicSetDiscardNamesPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__ELABORATE_PRAGMA:
				return basicSetElaboratePragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__ELABORATE_ALL_PRAGMA:
				return basicSetElaborateAllPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__ELABORATE_BODY_PRAGMA:
				return basicSetElaborateBodyPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__EXPORT_PRAGMA:
				return basicSetExportPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__IMPORT_PRAGMA:
				return basicSetImportPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__INLINE_PRAGMA:
				return basicSetInlinePragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__INSPECTION_POINT_PRAGMA:
				return basicSetInspectionPointPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__INTERRUPT_HANDLER_PRAGMA:
				return basicSetInterruptHandlerPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__INTERRUPT_PRIORITY_PRAGMA:
				return basicSetInterruptPriorityPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__LINKER_OPTIONS_PRAGMA:
				return basicSetLinkerOptionsPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__LIST_PRAGMA:
				return basicSetListPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__LOCKING_POLICY_PRAGMA:
				return basicSetLockingPolicyPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__NORMALIZE_SCALARS_PRAGMA:
				return basicSetNormalizeScalarsPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__OPTIMIZE_PRAGMA:
				return basicSetOptimizePragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__PACK_PRAGMA:
				return basicSetPackPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__PAGE_PRAGMA:
				return basicSetPagePragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__PREELABORATE_PRAGMA:
				return basicSetPreelaboratePragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__PRIORITY_PRAGMA:
				return basicSetPriorityPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__PURE_PRAGMA:
				return basicSetPurePragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__QUEUING_POLICY_PRAGMA:
				return basicSetQueuingPolicyPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA:
				return basicSetRemoteCallInterfacePragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__REMOTE_TYPES_PRAGMA:
				return basicSetRemoteTypesPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__RESTRICTIONS_PRAGMA:
				return basicSetRestrictionsPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__REVIEWABLE_PRAGMA:
				return basicSetReviewablePragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__SHARED_PASSIVE_PRAGMA:
				return basicSetSharedPassivePragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__STORAGE_SIZE_PRAGMA:
				return basicSetStorageSizePragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__SUPPRESS_PRAGMA:
				return basicSetSuppressPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA:
				return basicSetTaskDispatchingPolicyPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__VOLATILE_PRAGMA:
				return basicSetVolatilePragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__VOLATILE_COMPONENTS_PRAGMA:
				return basicSetVolatileComponentsPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__ASSERT_PRAGMA:
				return basicSetAssertPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__ASSERTION_POLICY_PRAGMA:
				return basicSetAssertionPolicyPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__DETECT_BLOCKING_PRAGMA:
				return basicSetDetectBlockingPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__NO_RETURN_PRAGMA:
				return basicSetNoReturnPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA:
				return basicSetPartitionElaborationPolicyPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA:
				return basicSetPreelaborableInitializationPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return basicSetPrioritySpecificDispatchingPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__PROFILE_PRAGMA:
				return basicSetProfilePragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__RELATIVE_DEADLINE_PRAGMA:
				return basicSetRelativeDeadlinePragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__UNCHECKED_UNION_PRAGMA:
				return basicSetUncheckedUnionPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__UNSUPPRESS_PRAGMA:
				return basicSetUnsuppressPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA:
				return basicSetDefaultStoragePoolPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__DISPATCHING_DOMAIN_PRAGMA:
				return basicSetDispatchingDomainPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__CPU_PRAGMA:
				return basicSetCpuPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__INDEPENDENT_PRAGMA:
				return basicSetIndependentPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA:
				return basicSetIndependentComponentsPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA:
				return basicSetImplementationDefinedPragma(null, msgs);
			case AdaPackage.DECLARATION_CLASS__UNKNOWN_PRAGMA:
				return basicSetUnknownPragma(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.DECLARATION_CLASS__NOT_AN_ELEMENT:
				return getNotAnElement();
			case AdaPackage.DECLARATION_CLASS__ORDINARY_TYPE_DECLARATION:
				return getOrdinaryTypeDeclaration();
			case AdaPackage.DECLARATION_CLASS__TASK_TYPE_DECLARATION:
				return getTaskTypeDeclaration();
			case AdaPackage.DECLARATION_CLASS__PROTECTED_TYPE_DECLARATION:
				return getProtectedTypeDeclaration();
			case AdaPackage.DECLARATION_CLASS__INCOMPLETE_TYPE_DECLARATION:
				return getIncompleteTypeDeclaration();
			case AdaPackage.DECLARATION_CLASS__TAGGED_INCOMPLETE_TYPE_DECLARATION:
				return getTaggedIncompleteTypeDeclaration();
			case AdaPackage.DECLARATION_CLASS__PRIVATE_TYPE_DECLARATION:
				return getPrivateTypeDeclaration();
			case AdaPackage.DECLARATION_CLASS__PRIVATE_EXTENSION_DECLARATION:
				return getPrivateExtensionDeclaration();
			case AdaPackage.DECLARATION_CLASS__SUBTYPE_DECLARATION:
				return getSubtypeDeclaration();
			case AdaPackage.DECLARATION_CLASS__VARIABLE_DECLARATION:
				return getVariableDeclaration();
			case AdaPackage.DECLARATION_CLASS__CONSTANT_DECLARATION:
				return getConstantDeclaration();
			case AdaPackage.DECLARATION_CLASS__DEFERRED_CONSTANT_DECLARATION:
				return getDeferredConstantDeclaration();
			case AdaPackage.DECLARATION_CLASS__SINGLE_TASK_DECLARATION:
				return getSingleTaskDeclaration();
			case AdaPackage.DECLARATION_CLASS__SINGLE_PROTECTED_DECLARATION:
				return getSingleProtectedDeclaration();
			case AdaPackage.DECLARATION_CLASS__INTEGER_NUMBER_DECLARATION:
				return getIntegerNumberDeclaration();
			case AdaPackage.DECLARATION_CLASS__REAL_NUMBER_DECLARATION:
				return getRealNumberDeclaration();
			case AdaPackage.DECLARATION_CLASS__ENUMERATION_LITERAL_SPECIFICATION:
				return getEnumerationLiteralSpecification();
			case AdaPackage.DECLARATION_CLASS__DISCRIMINANT_SPECIFICATION:
				return getDiscriminantSpecification();
			case AdaPackage.DECLARATION_CLASS__COMPONENT_DECLARATION:
				return getComponentDeclaration();
			case AdaPackage.DECLARATION_CLASS__LOOP_PARAMETER_SPECIFICATION:
				return getLoopParameterSpecification();
			case AdaPackage.DECLARATION_CLASS__GENERALIZED_ITERATOR_SPECIFICATION:
				return getGeneralizedIteratorSpecification();
			case AdaPackage.DECLARATION_CLASS__ELEMENT_ITERATOR_SPECIFICATION:
				return getElementIteratorSpecification();
			case AdaPackage.DECLARATION_CLASS__PROCEDURE_DECLARATION:
				return getProcedureDeclaration();
			case AdaPackage.DECLARATION_CLASS__FUNCTION_DECLARATION:
				return getFunctionDeclaration();
			case AdaPackage.DECLARATION_CLASS__PARAMETER_SPECIFICATION:
				return getParameterSpecification();
			case AdaPackage.DECLARATION_CLASS__PROCEDURE_BODY_DECLARATION:
				return getProcedureBodyDeclaration();
			case AdaPackage.DECLARATION_CLASS__FUNCTION_BODY_DECLARATION:
				return getFunctionBodyDeclaration();
			case AdaPackage.DECLARATION_CLASS__RETURN_VARIABLE_SPECIFICATION:
				return getReturnVariableSpecification();
			case AdaPackage.DECLARATION_CLASS__RETURN_CONSTANT_SPECIFICATION:
				return getReturnConstantSpecification();
			case AdaPackage.DECLARATION_CLASS__NULL_PROCEDURE_DECLARATION:
				return getNullProcedureDeclaration();
			case AdaPackage.DECLARATION_CLASS__EXPRESSION_FUNCTION_DECLARATION:
				return getExpressionFunctionDeclaration();
			case AdaPackage.DECLARATION_CLASS__PACKAGE_DECLARATION:
				return getPackageDeclaration();
			case AdaPackage.DECLARATION_CLASS__PACKAGE_BODY_DECLARATION:
				return getPackageBodyDeclaration();
			case AdaPackage.DECLARATION_CLASS__OBJECT_RENAMING_DECLARATION:
				return getObjectRenamingDeclaration();
			case AdaPackage.DECLARATION_CLASS__EXCEPTION_RENAMING_DECLARATION:
				return getExceptionRenamingDeclaration();
			case AdaPackage.DECLARATION_CLASS__PACKAGE_RENAMING_DECLARATION:
				return getPackageRenamingDeclaration();
			case AdaPackage.DECLARATION_CLASS__PROCEDURE_RENAMING_DECLARATION:
				return getProcedureRenamingDeclaration();
			case AdaPackage.DECLARATION_CLASS__FUNCTION_RENAMING_DECLARATION:
				return getFunctionRenamingDeclaration();
			case AdaPackage.DECLARATION_CLASS__GENERIC_PACKAGE_RENAMING_DECLARATION:
				return getGenericPackageRenamingDeclaration();
			case AdaPackage.DECLARATION_CLASS__GENERIC_PROCEDURE_RENAMING_DECLARATION:
				return getGenericProcedureRenamingDeclaration();
			case AdaPackage.DECLARATION_CLASS__GENERIC_FUNCTION_RENAMING_DECLARATION:
				return getGenericFunctionRenamingDeclaration();
			case AdaPackage.DECLARATION_CLASS__TASK_BODY_DECLARATION:
				return getTaskBodyDeclaration();
			case AdaPackage.DECLARATION_CLASS__PROTECTED_BODY_DECLARATION:
				return getProtectedBodyDeclaration();
			case AdaPackage.DECLARATION_CLASS__ENTRY_DECLARATION:
				return getEntryDeclaration();
			case AdaPackage.DECLARATION_CLASS__ENTRY_BODY_DECLARATION:
				return getEntryBodyDeclaration();
			case AdaPackage.DECLARATION_CLASS__ENTRY_INDEX_SPECIFICATION:
				return getEntryIndexSpecification();
			case AdaPackage.DECLARATION_CLASS__PROCEDURE_BODY_STUB:
				return getProcedureBodyStub();
			case AdaPackage.DECLARATION_CLASS__FUNCTION_BODY_STUB:
				return getFunctionBodyStub();
			case AdaPackage.DECLARATION_CLASS__PACKAGE_BODY_STUB:
				return getPackageBodyStub();
			case AdaPackage.DECLARATION_CLASS__TASK_BODY_STUB:
				return getTaskBodyStub();
			case AdaPackage.DECLARATION_CLASS__PROTECTED_BODY_STUB:
				return getProtectedBodyStub();
			case AdaPackage.DECLARATION_CLASS__EXCEPTION_DECLARATION:
				return getExceptionDeclaration();
			case AdaPackage.DECLARATION_CLASS__CHOICE_PARAMETER_SPECIFICATION:
				return getChoiceParameterSpecification();
			case AdaPackage.DECLARATION_CLASS__GENERIC_PROCEDURE_DECLARATION:
				return getGenericProcedureDeclaration();
			case AdaPackage.DECLARATION_CLASS__GENERIC_FUNCTION_DECLARATION:
				return getGenericFunctionDeclaration();
			case AdaPackage.DECLARATION_CLASS__GENERIC_PACKAGE_DECLARATION:
				return getGenericPackageDeclaration();
			case AdaPackage.DECLARATION_CLASS__PACKAGE_INSTANTIATION:
				return getPackageInstantiation();
			case AdaPackage.DECLARATION_CLASS__PROCEDURE_INSTANTIATION:
				return getProcedureInstantiation();
			case AdaPackage.DECLARATION_CLASS__FUNCTION_INSTANTIATION:
				return getFunctionInstantiation();
			case AdaPackage.DECLARATION_CLASS__FORMAL_OBJECT_DECLARATION:
				return getFormalObjectDeclaration();
			case AdaPackage.DECLARATION_CLASS__FORMAL_TYPE_DECLARATION:
				return getFormalTypeDeclaration();
			case AdaPackage.DECLARATION_CLASS__FORMAL_INCOMPLETE_TYPE_DECLARATION:
				return getFormalIncompleteTypeDeclaration();
			case AdaPackage.DECLARATION_CLASS__FORMAL_PROCEDURE_DECLARATION:
				return getFormalProcedureDeclaration();
			case AdaPackage.DECLARATION_CLASS__FORMAL_FUNCTION_DECLARATION:
				return getFormalFunctionDeclaration();
			case AdaPackage.DECLARATION_CLASS__FORMAL_PACKAGE_DECLARATION:
				return getFormalPackageDeclaration();
			case AdaPackage.DECLARATION_CLASS__FORMAL_PACKAGE_DECLARATION_WITH_BOX:
				return getFormalPackageDeclarationWithBox();
			case AdaPackage.DECLARATION_CLASS__COMMENT:
				return getComment();
			case AdaPackage.DECLARATION_CLASS__ALL_CALLS_REMOTE_PRAGMA:
				return getAllCallsRemotePragma();
			case AdaPackage.DECLARATION_CLASS__ASYNCHRONOUS_PRAGMA:
				return getAsynchronousPragma();
			case AdaPackage.DECLARATION_CLASS__ATOMIC_PRAGMA:
				return getAtomicPragma();
			case AdaPackage.DECLARATION_CLASS__ATOMIC_COMPONENTS_PRAGMA:
				return getAtomicComponentsPragma();
			case AdaPackage.DECLARATION_CLASS__ATTACH_HANDLER_PRAGMA:
				return getAttachHandlerPragma();
			case AdaPackage.DECLARATION_CLASS__CONTROLLED_PRAGMA:
				return getControlledPragma();
			case AdaPackage.DECLARATION_CLASS__CONVENTION_PRAGMA:
				return getConventionPragma();
			case AdaPackage.DECLARATION_CLASS__DISCARD_NAMES_PRAGMA:
				return getDiscardNamesPragma();
			case AdaPackage.DECLARATION_CLASS__ELABORATE_PRAGMA:
				return getElaboratePragma();
			case AdaPackage.DECLARATION_CLASS__ELABORATE_ALL_PRAGMA:
				return getElaborateAllPragma();
			case AdaPackage.DECLARATION_CLASS__ELABORATE_BODY_PRAGMA:
				return getElaborateBodyPragma();
			case AdaPackage.DECLARATION_CLASS__EXPORT_PRAGMA:
				return getExportPragma();
			case AdaPackage.DECLARATION_CLASS__IMPORT_PRAGMA:
				return getImportPragma();
			case AdaPackage.DECLARATION_CLASS__INLINE_PRAGMA:
				return getInlinePragma();
			case AdaPackage.DECLARATION_CLASS__INSPECTION_POINT_PRAGMA:
				return getInspectionPointPragma();
			case AdaPackage.DECLARATION_CLASS__INTERRUPT_HANDLER_PRAGMA:
				return getInterruptHandlerPragma();
			case AdaPackage.DECLARATION_CLASS__INTERRUPT_PRIORITY_PRAGMA:
				return getInterruptPriorityPragma();
			case AdaPackage.DECLARATION_CLASS__LINKER_OPTIONS_PRAGMA:
				return getLinkerOptionsPragma();
			case AdaPackage.DECLARATION_CLASS__LIST_PRAGMA:
				return getListPragma();
			case AdaPackage.DECLARATION_CLASS__LOCKING_POLICY_PRAGMA:
				return getLockingPolicyPragma();
			case AdaPackage.DECLARATION_CLASS__NORMALIZE_SCALARS_PRAGMA:
				return getNormalizeScalarsPragma();
			case AdaPackage.DECLARATION_CLASS__OPTIMIZE_PRAGMA:
				return getOptimizePragma();
			case AdaPackage.DECLARATION_CLASS__PACK_PRAGMA:
				return getPackPragma();
			case AdaPackage.DECLARATION_CLASS__PAGE_PRAGMA:
				return getPagePragma();
			case AdaPackage.DECLARATION_CLASS__PREELABORATE_PRAGMA:
				return getPreelaboratePragma();
			case AdaPackage.DECLARATION_CLASS__PRIORITY_PRAGMA:
				return getPriorityPragma();
			case AdaPackage.DECLARATION_CLASS__PURE_PRAGMA:
				return getPurePragma();
			case AdaPackage.DECLARATION_CLASS__QUEUING_POLICY_PRAGMA:
				return getQueuingPolicyPragma();
			case AdaPackage.DECLARATION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA:
				return getRemoteCallInterfacePragma();
			case AdaPackage.DECLARATION_CLASS__REMOTE_TYPES_PRAGMA:
				return getRemoteTypesPragma();
			case AdaPackage.DECLARATION_CLASS__RESTRICTIONS_PRAGMA:
				return getRestrictionsPragma();
			case AdaPackage.DECLARATION_CLASS__REVIEWABLE_PRAGMA:
				return getReviewablePragma();
			case AdaPackage.DECLARATION_CLASS__SHARED_PASSIVE_PRAGMA:
				return getSharedPassivePragma();
			case AdaPackage.DECLARATION_CLASS__STORAGE_SIZE_PRAGMA:
				return getStorageSizePragma();
			case AdaPackage.DECLARATION_CLASS__SUPPRESS_PRAGMA:
				return getSuppressPragma();
			case AdaPackage.DECLARATION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA:
				return getTaskDispatchingPolicyPragma();
			case AdaPackage.DECLARATION_CLASS__VOLATILE_PRAGMA:
				return getVolatilePragma();
			case AdaPackage.DECLARATION_CLASS__VOLATILE_COMPONENTS_PRAGMA:
				return getVolatileComponentsPragma();
			case AdaPackage.DECLARATION_CLASS__ASSERT_PRAGMA:
				return getAssertPragma();
			case AdaPackage.DECLARATION_CLASS__ASSERTION_POLICY_PRAGMA:
				return getAssertionPolicyPragma();
			case AdaPackage.DECLARATION_CLASS__DETECT_BLOCKING_PRAGMA:
				return getDetectBlockingPragma();
			case AdaPackage.DECLARATION_CLASS__NO_RETURN_PRAGMA:
				return getNoReturnPragma();
			case AdaPackage.DECLARATION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA:
				return getPartitionElaborationPolicyPragma();
			case AdaPackage.DECLARATION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA:
				return getPreelaborableInitializationPragma();
			case AdaPackage.DECLARATION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return getPrioritySpecificDispatchingPragma();
			case AdaPackage.DECLARATION_CLASS__PROFILE_PRAGMA:
				return getProfilePragma();
			case AdaPackage.DECLARATION_CLASS__RELATIVE_DEADLINE_PRAGMA:
				return getRelativeDeadlinePragma();
			case AdaPackage.DECLARATION_CLASS__UNCHECKED_UNION_PRAGMA:
				return getUncheckedUnionPragma();
			case AdaPackage.DECLARATION_CLASS__UNSUPPRESS_PRAGMA:
				return getUnsuppressPragma();
			case AdaPackage.DECLARATION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA:
				return getDefaultStoragePoolPragma();
			case AdaPackage.DECLARATION_CLASS__DISPATCHING_DOMAIN_PRAGMA:
				return getDispatchingDomainPragma();
			case AdaPackage.DECLARATION_CLASS__CPU_PRAGMA:
				return getCpuPragma();
			case AdaPackage.DECLARATION_CLASS__INDEPENDENT_PRAGMA:
				return getIndependentPragma();
			case AdaPackage.DECLARATION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA:
				return getIndependentComponentsPragma();
			case AdaPackage.DECLARATION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA:
				return getImplementationDefinedPragma();
			case AdaPackage.DECLARATION_CLASS__UNKNOWN_PRAGMA:
				return getUnknownPragma();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.DECLARATION_CLASS__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__ORDINARY_TYPE_DECLARATION:
				setOrdinaryTypeDeclaration((OrdinaryTypeDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__TASK_TYPE_DECLARATION:
				setTaskTypeDeclaration((TaskTypeDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__PROTECTED_TYPE_DECLARATION:
				setProtectedTypeDeclaration((ProtectedTypeDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__INCOMPLETE_TYPE_DECLARATION:
				setIncompleteTypeDeclaration((IncompleteTypeDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__TAGGED_INCOMPLETE_TYPE_DECLARATION:
				setTaggedIncompleteTypeDeclaration((TaggedIncompleteTypeDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__PRIVATE_TYPE_DECLARATION:
				setPrivateTypeDeclaration((PrivateTypeDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__PRIVATE_EXTENSION_DECLARATION:
				setPrivateExtensionDeclaration((PrivateExtensionDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__SUBTYPE_DECLARATION:
				setSubtypeDeclaration((SubtypeDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__VARIABLE_DECLARATION:
				setVariableDeclaration((VariableDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__CONSTANT_DECLARATION:
				setConstantDeclaration((ConstantDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__DEFERRED_CONSTANT_DECLARATION:
				setDeferredConstantDeclaration((DeferredConstantDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__SINGLE_TASK_DECLARATION:
				setSingleTaskDeclaration((SingleTaskDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__SINGLE_PROTECTED_DECLARATION:
				setSingleProtectedDeclaration((SingleProtectedDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__INTEGER_NUMBER_DECLARATION:
				setIntegerNumberDeclaration((IntegerNumberDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__REAL_NUMBER_DECLARATION:
				setRealNumberDeclaration((RealNumberDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__ENUMERATION_LITERAL_SPECIFICATION:
				setEnumerationLiteralSpecification((EnumerationLiteralSpecification)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__DISCRIMINANT_SPECIFICATION:
				setDiscriminantSpecification((DiscriminantSpecification)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__COMPONENT_DECLARATION:
				setComponentDeclaration((ComponentDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__LOOP_PARAMETER_SPECIFICATION:
				setLoopParameterSpecification((LoopParameterSpecification)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__GENERALIZED_ITERATOR_SPECIFICATION:
				setGeneralizedIteratorSpecification((GeneralizedIteratorSpecification)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__ELEMENT_ITERATOR_SPECIFICATION:
				setElementIteratorSpecification((ElementIteratorSpecification)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__PROCEDURE_DECLARATION:
				setProcedureDeclaration((ProcedureDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__FUNCTION_DECLARATION:
				setFunctionDeclaration((FunctionDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__PARAMETER_SPECIFICATION:
				setParameterSpecification((ParameterSpecification)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__PROCEDURE_BODY_DECLARATION:
				setProcedureBodyDeclaration((ProcedureBodyDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__FUNCTION_BODY_DECLARATION:
				setFunctionBodyDeclaration((FunctionBodyDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__RETURN_VARIABLE_SPECIFICATION:
				setReturnVariableSpecification((ReturnVariableSpecification)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__RETURN_CONSTANT_SPECIFICATION:
				setReturnConstantSpecification((ReturnConstantSpecification)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__NULL_PROCEDURE_DECLARATION:
				setNullProcedureDeclaration((NullProcedureDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__EXPRESSION_FUNCTION_DECLARATION:
				setExpressionFunctionDeclaration((ExpressionFunctionDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__PACKAGE_DECLARATION:
				setPackageDeclaration((PackageDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__PACKAGE_BODY_DECLARATION:
				setPackageBodyDeclaration((PackageBodyDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__OBJECT_RENAMING_DECLARATION:
				setObjectRenamingDeclaration((ObjectRenamingDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__EXCEPTION_RENAMING_DECLARATION:
				setExceptionRenamingDeclaration((ExceptionRenamingDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__PACKAGE_RENAMING_DECLARATION:
				setPackageRenamingDeclaration((PackageRenamingDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__PROCEDURE_RENAMING_DECLARATION:
				setProcedureRenamingDeclaration((ProcedureRenamingDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__FUNCTION_RENAMING_DECLARATION:
				setFunctionRenamingDeclaration((FunctionRenamingDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__GENERIC_PACKAGE_RENAMING_DECLARATION:
				setGenericPackageRenamingDeclaration((GenericPackageRenamingDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__GENERIC_PROCEDURE_RENAMING_DECLARATION:
				setGenericProcedureRenamingDeclaration((GenericProcedureRenamingDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__GENERIC_FUNCTION_RENAMING_DECLARATION:
				setGenericFunctionRenamingDeclaration((GenericFunctionRenamingDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__TASK_BODY_DECLARATION:
				setTaskBodyDeclaration((TaskBodyDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__PROTECTED_BODY_DECLARATION:
				setProtectedBodyDeclaration((ProtectedBodyDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__ENTRY_DECLARATION:
				setEntryDeclaration((EntryDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__ENTRY_BODY_DECLARATION:
				setEntryBodyDeclaration((EntryBodyDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__ENTRY_INDEX_SPECIFICATION:
				setEntryIndexSpecification((EntryIndexSpecification)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__PROCEDURE_BODY_STUB:
				setProcedureBodyStub((ProcedureBodyStub)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__FUNCTION_BODY_STUB:
				setFunctionBodyStub((FunctionBodyStub)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__PACKAGE_BODY_STUB:
				setPackageBodyStub((PackageBodyStub)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__TASK_BODY_STUB:
				setTaskBodyStub((TaskBodyStub)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__PROTECTED_BODY_STUB:
				setProtectedBodyStub((ProtectedBodyStub)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__EXCEPTION_DECLARATION:
				setExceptionDeclaration((ExceptionDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__CHOICE_PARAMETER_SPECIFICATION:
				setChoiceParameterSpecification((ChoiceParameterSpecification)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__GENERIC_PROCEDURE_DECLARATION:
				setGenericProcedureDeclaration((GenericProcedureDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__GENERIC_FUNCTION_DECLARATION:
				setGenericFunctionDeclaration((GenericFunctionDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__GENERIC_PACKAGE_DECLARATION:
				setGenericPackageDeclaration((GenericPackageDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__PACKAGE_INSTANTIATION:
				setPackageInstantiation((PackageInstantiation)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__PROCEDURE_INSTANTIATION:
				setProcedureInstantiation((ProcedureInstantiation)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__FUNCTION_INSTANTIATION:
				setFunctionInstantiation((FunctionInstantiation)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__FORMAL_OBJECT_DECLARATION:
				setFormalObjectDeclaration((FormalObjectDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__FORMAL_TYPE_DECLARATION:
				setFormalTypeDeclaration((FormalTypeDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__FORMAL_INCOMPLETE_TYPE_DECLARATION:
				setFormalIncompleteTypeDeclaration((FormalIncompleteTypeDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__FORMAL_PROCEDURE_DECLARATION:
				setFormalProcedureDeclaration((FormalProcedureDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__FORMAL_FUNCTION_DECLARATION:
				setFormalFunctionDeclaration((FormalFunctionDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__FORMAL_PACKAGE_DECLARATION:
				setFormalPackageDeclaration((FormalPackageDeclaration)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__FORMAL_PACKAGE_DECLARATION_WITH_BOX:
				setFormalPackageDeclarationWithBox((FormalPackageDeclarationWithBox)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__COMMENT:
				setComment((Comment)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__ALL_CALLS_REMOTE_PRAGMA:
				setAllCallsRemotePragma((AllCallsRemotePragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__ASYNCHRONOUS_PRAGMA:
				setAsynchronousPragma((AsynchronousPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__ATOMIC_PRAGMA:
				setAtomicPragma((AtomicPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__ATOMIC_COMPONENTS_PRAGMA:
				setAtomicComponentsPragma((AtomicComponentsPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__ATTACH_HANDLER_PRAGMA:
				setAttachHandlerPragma((AttachHandlerPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__CONTROLLED_PRAGMA:
				setControlledPragma((ControlledPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__CONVENTION_PRAGMA:
				setConventionPragma((ConventionPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__DISCARD_NAMES_PRAGMA:
				setDiscardNamesPragma((DiscardNamesPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__ELABORATE_PRAGMA:
				setElaboratePragma((ElaboratePragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__ELABORATE_ALL_PRAGMA:
				setElaborateAllPragma((ElaborateAllPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__ELABORATE_BODY_PRAGMA:
				setElaborateBodyPragma((ElaborateBodyPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__EXPORT_PRAGMA:
				setExportPragma((ExportPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__IMPORT_PRAGMA:
				setImportPragma((ImportPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__INLINE_PRAGMA:
				setInlinePragma((InlinePragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__INSPECTION_POINT_PRAGMA:
				setInspectionPointPragma((InspectionPointPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__INTERRUPT_HANDLER_PRAGMA:
				setInterruptHandlerPragma((InterruptHandlerPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__INTERRUPT_PRIORITY_PRAGMA:
				setInterruptPriorityPragma((InterruptPriorityPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__LINKER_OPTIONS_PRAGMA:
				setLinkerOptionsPragma((LinkerOptionsPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__LIST_PRAGMA:
				setListPragma((ListPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__LOCKING_POLICY_PRAGMA:
				setLockingPolicyPragma((LockingPolicyPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__NORMALIZE_SCALARS_PRAGMA:
				setNormalizeScalarsPragma((NormalizeScalarsPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__OPTIMIZE_PRAGMA:
				setOptimizePragma((OptimizePragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__PACK_PRAGMA:
				setPackPragma((PackPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__PAGE_PRAGMA:
				setPagePragma((PagePragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__PREELABORATE_PRAGMA:
				setPreelaboratePragma((PreelaboratePragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__PRIORITY_PRAGMA:
				setPriorityPragma((PriorityPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__PURE_PRAGMA:
				setPurePragma((PurePragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__QUEUING_POLICY_PRAGMA:
				setQueuingPolicyPragma((QueuingPolicyPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA:
				setRemoteCallInterfacePragma((RemoteCallInterfacePragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__REMOTE_TYPES_PRAGMA:
				setRemoteTypesPragma((RemoteTypesPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__RESTRICTIONS_PRAGMA:
				setRestrictionsPragma((RestrictionsPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__REVIEWABLE_PRAGMA:
				setReviewablePragma((ReviewablePragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__SHARED_PASSIVE_PRAGMA:
				setSharedPassivePragma((SharedPassivePragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__STORAGE_SIZE_PRAGMA:
				setStorageSizePragma((StorageSizePragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__SUPPRESS_PRAGMA:
				setSuppressPragma((SuppressPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA:
				setTaskDispatchingPolicyPragma((TaskDispatchingPolicyPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__VOLATILE_PRAGMA:
				setVolatilePragma((VolatilePragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__VOLATILE_COMPONENTS_PRAGMA:
				setVolatileComponentsPragma((VolatileComponentsPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__ASSERT_PRAGMA:
				setAssertPragma((AssertPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__ASSERTION_POLICY_PRAGMA:
				setAssertionPolicyPragma((AssertionPolicyPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__DETECT_BLOCKING_PRAGMA:
				setDetectBlockingPragma((DetectBlockingPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__NO_RETURN_PRAGMA:
				setNoReturnPragma((NoReturnPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA:
				setPartitionElaborationPolicyPragma((PartitionElaborationPolicyPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA:
				setPreelaborableInitializationPragma((PreelaborableInitializationPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				setPrioritySpecificDispatchingPragma((PrioritySpecificDispatchingPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__PROFILE_PRAGMA:
				setProfilePragma((ProfilePragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__RELATIVE_DEADLINE_PRAGMA:
				setRelativeDeadlinePragma((RelativeDeadlinePragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__UNCHECKED_UNION_PRAGMA:
				setUncheckedUnionPragma((UncheckedUnionPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__UNSUPPRESS_PRAGMA:
				setUnsuppressPragma((UnsuppressPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA:
				setDefaultStoragePoolPragma((DefaultStoragePoolPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__DISPATCHING_DOMAIN_PRAGMA:
				setDispatchingDomainPragma((DispatchingDomainPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__CPU_PRAGMA:
				setCpuPragma((CpuPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__INDEPENDENT_PRAGMA:
				setIndependentPragma((IndependentPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA:
				setIndependentComponentsPragma((IndependentComponentsPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA:
				setImplementationDefinedPragma((ImplementationDefinedPragma)newValue);
				return;
			case AdaPackage.DECLARATION_CLASS__UNKNOWN_PRAGMA:
				setUnknownPragma((UnknownPragma)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.DECLARATION_CLASS__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)null);
				return;
			case AdaPackage.DECLARATION_CLASS__ORDINARY_TYPE_DECLARATION:
				setOrdinaryTypeDeclaration((OrdinaryTypeDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__TASK_TYPE_DECLARATION:
				setTaskTypeDeclaration((TaskTypeDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__PROTECTED_TYPE_DECLARATION:
				setProtectedTypeDeclaration((ProtectedTypeDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__INCOMPLETE_TYPE_DECLARATION:
				setIncompleteTypeDeclaration((IncompleteTypeDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__TAGGED_INCOMPLETE_TYPE_DECLARATION:
				setTaggedIncompleteTypeDeclaration((TaggedIncompleteTypeDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__PRIVATE_TYPE_DECLARATION:
				setPrivateTypeDeclaration((PrivateTypeDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__PRIVATE_EXTENSION_DECLARATION:
				setPrivateExtensionDeclaration((PrivateExtensionDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__SUBTYPE_DECLARATION:
				setSubtypeDeclaration((SubtypeDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__VARIABLE_DECLARATION:
				setVariableDeclaration((VariableDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__CONSTANT_DECLARATION:
				setConstantDeclaration((ConstantDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__DEFERRED_CONSTANT_DECLARATION:
				setDeferredConstantDeclaration((DeferredConstantDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__SINGLE_TASK_DECLARATION:
				setSingleTaskDeclaration((SingleTaskDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__SINGLE_PROTECTED_DECLARATION:
				setSingleProtectedDeclaration((SingleProtectedDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__INTEGER_NUMBER_DECLARATION:
				setIntegerNumberDeclaration((IntegerNumberDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__REAL_NUMBER_DECLARATION:
				setRealNumberDeclaration((RealNumberDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__ENUMERATION_LITERAL_SPECIFICATION:
				setEnumerationLiteralSpecification((EnumerationLiteralSpecification)null);
				return;
			case AdaPackage.DECLARATION_CLASS__DISCRIMINANT_SPECIFICATION:
				setDiscriminantSpecification((DiscriminantSpecification)null);
				return;
			case AdaPackage.DECLARATION_CLASS__COMPONENT_DECLARATION:
				setComponentDeclaration((ComponentDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__LOOP_PARAMETER_SPECIFICATION:
				setLoopParameterSpecification((LoopParameterSpecification)null);
				return;
			case AdaPackage.DECLARATION_CLASS__GENERALIZED_ITERATOR_SPECIFICATION:
				setGeneralizedIteratorSpecification((GeneralizedIteratorSpecification)null);
				return;
			case AdaPackage.DECLARATION_CLASS__ELEMENT_ITERATOR_SPECIFICATION:
				setElementIteratorSpecification((ElementIteratorSpecification)null);
				return;
			case AdaPackage.DECLARATION_CLASS__PROCEDURE_DECLARATION:
				setProcedureDeclaration((ProcedureDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__FUNCTION_DECLARATION:
				setFunctionDeclaration((FunctionDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__PARAMETER_SPECIFICATION:
				setParameterSpecification((ParameterSpecification)null);
				return;
			case AdaPackage.DECLARATION_CLASS__PROCEDURE_BODY_DECLARATION:
				setProcedureBodyDeclaration((ProcedureBodyDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__FUNCTION_BODY_DECLARATION:
				setFunctionBodyDeclaration((FunctionBodyDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__RETURN_VARIABLE_SPECIFICATION:
				setReturnVariableSpecification((ReturnVariableSpecification)null);
				return;
			case AdaPackage.DECLARATION_CLASS__RETURN_CONSTANT_SPECIFICATION:
				setReturnConstantSpecification((ReturnConstantSpecification)null);
				return;
			case AdaPackage.DECLARATION_CLASS__NULL_PROCEDURE_DECLARATION:
				setNullProcedureDeclaration((NullProcedureDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__EXPRESSION_FUNCTION_DECLARATION:
				setExpressionFunctionDeclaration((ExpressionFunctionDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__PACKAGE_DECLARATION:
				setPackageDeclaration((PackageDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__PACKAGE_BODY_DECLARATION:
				setPackageBodyDeclaration((PackageBodyDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__OBJECT_RENAMING_DECLARATION:
				setObjectRenamingDeclaration((ObjectRenamingDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__EXCEPTION_RENAMING_DECLARATION:
				setExceptionRenamingDeclaration((ExceptionRenamingDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__PACKAGE_RENAMING_DECLARATION:
				setPackageRenamingDeclaration((PackageRenamingDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__PROCEDURE_RENAMING_DECLARATION:
				setProcedureRenamingDeclaration((ProcedureRenamingDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__FUNCTION_RENAMING_DECLARATION:
				setFunctionRenamingDeclaration((FunctionRenamingDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__GENERIC_PACKAGE_RENAMING_DECLARATION:
				setGenericPackageRenamingDeclaration((GenericPackageRenamingDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__GENERIC_PROCEDURE_RENAMING_DECLARATION:
				setGenericProcedureRenamingDeclaration((GenericProcedureRenamingDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__GENERIC_FUNCTION_RENAMING_DECLARATION:
				setGenericFunctionRenamingDeclaration((GenericFunctionRenamingDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__TASK_BODY_DECLARATION:
				setTaskBodyDeclaration((TaskBodyDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__PROTECTED_BODY_DECLARATION:
				setProtectedBodyDeclaration((ProtectedBodyDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__ENTRY_DECLARATION:
				setEntryDeclaration((EntryDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__ENTRY_BODY_DECLARATION:
				setEntryBodyDeclaration((EntryBodyDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__ENTRY_INDEX_SPECIFICATION:
				setEntryIndexSpecification((EntryIndexSpecification)null);
				return;
			case AdaPackage.DECLARATION_CLASS__PROCEDURE_BODY_STUB:
				setProcedureBodyStub((ProcedureBodyStub)null);
				return;
			case AdaPackage.DECLARATION_CLASS__FUNCTION_BODY_STUB:
				setFunctionBodyStub((FunctionBodyStub)null);
				return;
			case AdaPackage.DECLARATION_CLASS__PACKAGE_BODY_STUB:
				setPackageBodyStub((PackageBodyStub)null);
				return;
			case AdaPackage.DECLARATION_CLASS__TASK_BODY_STUB:
				setTaskBodyStub((TaskBodyStub)null);
				return;
			case AdaPackage.DECLARATION_CLASS__PROTECTED_BODY_STUB:
				setProtectedBodyStub((ProtectedBodyStub)null);
				return;
			case AdaPackage.DECLARATION_CLASS__EXCEPTION_DECLARATION:
				setExceptionDeclaration((ExceptionDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__CHOICE_PARAMETER_SPECIFICATION:
				setChoiceParameterSpecification((ChoiceParameterSpecification)null);
				return;
			case AdaPackage.DECLARATION_CLASS__GENERIC_PROCEDURE_DECLARATION:
				setGenericProcedureDeclaration((GenericProcedureDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__GENERIC_FUNCTION_DECLARATION:
				setGenericFunctionDeclaration((GenericFunctionDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__GENERIC_PACKAGE_DECLARATION:
				setGenericPackageDeclaration((GenericPackageDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__PACKAGE_INSTANTIATION:
				setPackageInstantiation((PackageInstantiation)null);
				return;
			case AdaPackage.DECLARATION_CLASS__PROCEDURE_INSTANTIATION:
				setProcedureInstantiation((ProcedureInstantiation)null);
				return;
			case AdaPackage.DECLARATION_CLASS__FUNCTION_INSTANTIATION:
				setFunctionInstantiation((FunctionInstantiation)null);
				return;
			case AdaPackage.DECLARATION_CLASS__FORMAL_OBJECT_DECLARATION:
				setFormalObjectDeclaration((FormalObjectDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__FORMAL_TYPE_DECLARATION:
				setFormalTypeDeclaration((FormalTypeDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__FORMAL_INCOMPLETE_TYPE_DECLARATION:
				setFormalIncompleteTypeDeclaration((FormalIncompleteTypeDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__FORMAL_PROCEDURE_DECLARATION:
				setFormalProcedureDeclaration((FormalProcedureDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__FORMAL_FUNCTION_DECLARATION:
				setFormalFunctionDeclaration((FormalFunctionDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__FORMAL_PACKAGE_DECLARATION:
				setFormalPackageDeclaration((FormalPackageDeclaration)null);
				return;
			case AdaPackage.DECLARATION_CLASS__FORMAL_PACKAGE_DECLARATION_WITH_BOX:
				setFormalPackageDeclarationWithBox((FormalPackageDeclarationWithBox)null);
				return;
			case AdaPackage.DECLARATION_CLASS__COMMENT:
				setComment((Comment)null);
				return;
			case AdaPackage.DECLARATION_CLASS__ALL_CALLS_REMOTE_PRAGMA:
				setAllCallsRemotePragma((AllCallsRemotePragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__ASYNCHRONOUS_PRAGMA:
				setAsynchronousPragma((AsynchronousPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__ATOMIC_PRAGMA:
				setAtomicPragma((AtomicPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__ATOMIC_COMPONENTS_PRAGMA:
				setAtomicComponentsPragma((AtomicComponentsPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__ATTACH_HANDLER_PRAGMA:
				setAttachHandlerPragma((AttachHandlerPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__CONTROLLED_PRAGMA:
				setControlledPragma((ControlledPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__CONVENTION_PRAGMA:
				setConventionPragma((ConventionPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__DISCARD_NAMES_PRAGMA:
				setDiscardNamesPragma((DiscardNamesPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__ELABORATE_PRAGMA:
				setElaboratePragma((ElaboratePragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__ELABORATE_ALL_PRAGMA:
				setElaborateAllPragma((ElaborateAllPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__ELABORATE_BODY_PRAGMA:
				setElaborateBodyPragma((ElaborateBodyPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__EXPORT_PRAGMA:
				setExportPragma((ExportPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__IMPORT_PRAGMA:
				setImportPragma((ImportPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__INLINE_PRAGMA:
				setInlinePragma((InlinePragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__INSPECTION_POINT_PRAGMA:
				setInspectionPointPragma((InspectionPointPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__INTERRUPT_HANDLER_PRAGMA:
				setInterruptHandlerPragma((InterruptHandlerPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__INTERRUPT_PRIORITY_PRAGMA:
				setInterruptPriorityPragma((InterruptPriorityPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__LINKER_OPTIONS_PRAGMA:
				setLinkerOptionsPragma((LinkerOptionsPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__LIST_PRAGMA:
				setListPragma((ListPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__LOCKING_POLICY_PRAGMA:
				setLockingPolicyPragma((LockingPolicyPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__NORMALIZE_SCALARS_PRAGMA:
				setNormalizeScalarsPragma((NormalizeScalarsPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__OPTIMIZE_PRAGMA:
				setOptimizePragma((OptimizePragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__PACK_PRAGMA:
				setPackPragma((PackPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__PAGE_PRAGMA:
				setPagePragma((PagePragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__PREELABORATE_PRAGMA:
				setPreelaboratePragma((PreelaboratePragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__PRIORITY_PRAGMA:
				setPriorityPragma((PriorityPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__PURE_PRAGMA:
				setPurePragma((PurePragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__QUEUING_POLICY_PRAGMA:
				setQueuingPolicyPragma((QueuingPolicyPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA:
				setRemoteCallInterfacePragma((RemoteCallInterfacePragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__REMOTE_TYPES_PRAGMA:
				setRemoteTypesPragma((RemoteTypesPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__RESTRICTIONS_PRAGMA:
				setRestrictionsPragma((RestrictionsPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__REVIEWABLE_PRAGMA:
				setReviewablePragma((ReviewablePragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__SHARED_PASSIVE_PRAGMA:
				setSharedPassivePragma((SharedPassivePragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__STORAGE_SIZE_PRAGMA:
				setStorageSizePragma((StorageSizePragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__SUPPRESS_PRAGMA:
				setSuppressPragma((SuppressPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA:
				setTaskDispatchingPolicyPragma((TaskDispatchingPolicyPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__VOLATILE_PRAGMA:
				setVolatilePragma((VolatilePragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__VOLATILE_COMPONENTS_PRAGMA:
				setVolatileComponentsPragma((VolatileComponentsPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__ASSERT_PRAGMA:
				setAssertPragma((AssertPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__ASSERTION_POLICY_PRAGMA:
				setAssertionPolicyPragma((AssertionPolicyPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__DETECT_BLOCKING_PRAGMA:
				setDetectBlockingPragma((DetectBlockingPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__NO_RETURN_PRAGMA:
				setNoReturnPragma((NoReturnPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA:
				setPartitionElaborationPolicyPragma((PartitionElaborationPolicyPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA:
				setPreelaborableInitializationPragma((PreelaborableInitializationPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				setPrioritySpecificDispatchingPragma((PrioritySpecificDispatchingPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__PROFILE_PRAGMA:
				setProfilePragma((ProfilePragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__RELATIVE_DEADLINE_PRAGMA:
				setRelativeDeadlinePragma((RelativeDeadlinePragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__UNCHECKED_UNION_PRAGMA:
				setUncheckedUnionPragma((UncheckedUnionPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__UNSUPPRESS_PRAGMA:
				setUnsuppressPragma((UnsuppressPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA:
				setDefaultStoragePoolPragma((DefaultStoragePoolPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__DISPATCHING_DOMAIN_PRAGMA:
				setDispatchingDomainPragma((DispatchingDomainPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__CPU_PRAGMA:
				setCpuPragma((CpuPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__INDEPENDENT_PRAGMA:
				setIndependentPragma((IndependentPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA:
				setIndependentComponentsPragma((IndependentComponentsPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA:
				setImplementationDefinedPragma((ImplementationDefinedPragma)null);
				return;
			case AdaPackage.DECLARATION_CLASS__UNKNOWN_PRAGMA:
				setUnknownPragma((UnknownPragma)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.DECLARATION_CLASS__NOT_AN_ELEMENT:
				return notAnElement != null;
			case AdaPackage.DECLARATION_CLASS__ORDINARY_TYPE_DECLARATION:
				return ordinaryTypeDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__TASK_TYPE_DECLARATION:
				return taskTypeDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__PROTECTED_TYPE_DECLARATION:
				return protectedTypeDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__INCOMPLETE_TYPE_DECLARATION:
				return incompleteTypeDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__TAGGED_INCOMPLETE_TYPE_DECLARATION:
				return taggedIncompleteTypeDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__PRIVATE_TYPE_DECLARATION:
				return privateTypeDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__PRIVATE_EXTENSION_DECLARATION:
				return privateExtensionDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__SUBTYPE_DECLARATION:
				return subtypeDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__VARIABLE_DECLARATION:
				return variableDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__CONSTANT_DECLARATION:
				return constantDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__DEFERRED_CONSTANT_DECLARATION:
				return deferredConstantDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__SINGLE_TASK_DECLARATION:
				return singleTaskDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__SINGLE_PROTECTED_DECLARATION:
				return singleProtectedDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__INTEGER_NUMBER_DECLARATION:
				return integerNumberDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__REAL_NUMBER_DECLARATION:
				return realNumberDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__ENUMERATION_LITERAL_SPECIFICATION:
				return enumerationLiteralSpecification != null;
			case AdaPackage.DECLARATION_CLASS__DISCRIMINANT_SPECIFICATION:
				return discriminantSpecification != null;
			case AdaPackage.DECLARATION_CLASS__COMPONENT_DECLARATION:
				return componentDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__LOOP_PARAMETER_SPECIFICATION:
				return loopParameterSpecification != null;
			case AdaPackage.DECLARATION_CLASS__GENERALIZED_ITERATOR_SPECIFICATION:
				return generalizedIteratorSpecification != null;
			case AdaPackage.DECLARATION_CLASS__ELEMENT_ITERATOR_SPECIFICATION:
				return elementIteratorSpecification != null;
			case AdaPackage.DECLARATION_CLASS__PROCEDURE_DECLARATION:
				return procedureDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__FUNCTION_DECLARATION:
				return functionDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__PARAMETER_SPECIFICATION:
				return parameterSpecification != null;
			case AdaPackage.DECLARATION_CLASS__PROCEDURE_BODY_DECLARATION:
				return procedureBodyDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__FUNCTION_BODY_DECLARATION:
				return functionBodyDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__RETURN_VARIABLE_SPECIFICATION:
				return returnVariableSpecification != null;
			case AdaPackage.DECLARATION_CLASS__RETURN_CONSTANT_SPECIFICATION:
				return returnConstantSpecification != null;
			case AdaPackage.DECLARATION_CLASS__NULL_PROCEDURE_DECLARATION:
				return nullProcedureDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__EXPRESSION_FUNCTION_DECLARATION:
				return expressionFunctionDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__PACKAGE_DECLARATION:
				return packageDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__PACKAGE_BODY_DECLARATION:
				return packageBodyDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__OBJECT_RENAMING_DECLARATION:
				return objectRenamingDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__EXCEPTION_RENAMING_DECLARATION:
				return exceptionRenamingDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__PACKAGE_RENAMING_DECLARATION:
				return packageRenamingDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__PROCEDURE_RENAMING_DECLARATION:
				return procedureRenamingDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__FUNCTION_RENAMING_DECLARATION:
				return functionRenamingDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__GENERIC_PACKAGE_RENAMING_DECLARATION:
				return genericPackageRenamingDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__GENERIC_PROCEDURE_RENAMING_DECLARATION:
				return genericProcedureRenamingDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__GENERIC_FUNCTION_RENAMING_DECLARATION:
				return genericFunctionRenamingDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__TASK_BODY_DECLARATION:
				return taskBodyDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__PROTECTED_BODY_DECLARATION:
				return protectedBodyDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__ENTRY_DECLARATION:
				return entryDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__ENTRY_BODY_DECLARATION:
				return entryBodyDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__ENTRY_INDEX_SPECIFICATION:
				return entryIndexSpecification != null;
			case AdaPackage.DECLARATION_CLASS__PROCEDURE_BODY_STUB:
				return procedureBodyStub != null;
			case AdaPackage.DECLARATION_CLASS__FUNCTION_BODY_STUB:
				return functionBodyStub != null;
			case AdaPackage.DECLARATION_CLASS__PACKAGE_BODY_STUB:
				return packageBodyStub != null;
			case AdaPackage.DECLARATION_CLASS__TASK_BODY_STUB:
				return taskBodyStub != null;
			case AdaPackage.DECLARATION_CLASS__PROTECTED_BODY_STUB:
				return protectedBodyStub != null;
			case AdaPackage.DECLARATION_CLASS__EXCEPTION_DECLARATION:
				return exceptionDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__CHOICE_PARAMETER_SPECIFICATION:
				return choiceParameterSpecification != null;
			case AdaPackage.DECLARATION_CLASS__GENERIC_PROCEDURE_DECLARATION:
				return genericProcedureDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__GENERIC_FUNCTION_DECLARATION:
				return genericFunctionDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__GENERIC_PACKAGE_DECLARATION:
				return genericPackageDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__PACKAGE_INSTANTIATION:
				return packageInstantiation != null;
			case AdaPackage.DECLARATION_CLASS__PROCEDURE_INSTANTIATION:
				return procedureInstantiation != null;
			case AdaPackage.DECLARATION_CLASS__FUNCTION_INSTANTIATION:
				return functionInstantiation != null;
			case AdaPackage.DECLARATION_CLASS__FORMAL_OBJECT_DECLARATION:
				return formalObjectDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__FORMAL_TYPE_DECLARATION:
				return formalTypeDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__FORMAL_INCOMPLETE_TYPE_DECLARATION:
				return formalIncompleteTypeDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__FORMAL_PROCEDURE_DECLARATION:
				return formalProcedureDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__FORMAL_FUNCTION_DECLARATION:
				return formalFunctionDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__FORMAL_PACKAGE_DECLARATION:
				return formalPackageDeclaration != null;
			case AdaPackage.DECLARATION_CLASS__FORMAL_PACKAGE_DECLARATION_WITH_BOX:
				return formalPackageDeclarationWithBox != null;
			case AdaPackage.DECLARATION_CLASS__COMMENT:
				return comment != null;
			case AdaPackage.DECLARATION_CLASS__ALL_CALLS_REMOTE_PRAGMA:
				return allCallsRemotePragma != null;
			case AdaPackage.DECLARATION_CLASS__ASYNCHRONOUS_PRAGMA:
				return asynchronousPragma != null;
			case AdaPackage.DECLARATION_CLASS__ATOMIC_PRAGMA:
				return atomicPragma != null;
			case AdaPackage.DECLARATION_CLASS__ATOMIC_COMPONENTS_PRAGMA:
				return atomicComponentsPragma != null;
			case AdaPackage.DECLARATION_CLASS__ATTACH_HANDLER_PRAGMA:
				return attachHandlerPragma != null;
			case AdaPackage.DECLARATION_CLASS__CONTROLLED_PRAGMA:
				return controlledPragma != null;
			case AdaPackage.DECLARATION_CLASS__CONVENTION_PRAGMA:
				return conventionPragma != null;
			case AdaPackage.DECLARATION_CLASS__DISCARD_NAMES_PRAGMA:
				return discardNamesPragma != null;
			case AdaPackage.DECLARATION_CLASS__ELABORATE_PRAGMA:
				return elaboratePragma != null;
			case AdaPackage.DECLARATION_CLASS__ELABORATE_ALL_PRAGMA:
				return elaborateAllPragma != null;
			case AdaPackage.DECLARATION_CLASS__ELABORATE_BODY_PRAGMA:
				return elaborateBodyPragma != null;
			case AdaPackage.DECLARATION_CLASS__EXPORT_PRAGMA:
				return exportPragma != null;
			case AdaPackage.DECLARATION_CLASS__IMPORT_PRAGMA:
				return importPragma != null;
			case AdaPackage.DECLARATION_CLASS__INLINE_PRAGMA:
				return inlinePragma != null;
			case AdaPackage.DECLARATION_CLASS__INSPECTION_POINT_PRAGMA:
				return inspectionPointPragma != null;
			case AdaPackage.DECLARATION_CLASS__INTERRUPT_HANDLER_PRAGMA:
				return interruptHandlerPragma != null;
			case AdaPackage.DECLARATION_CLASS__INTERRUPT_PRIORITY_PRAGMA:
				return interruptPriorityPragma != null;
			case AdaPackage.DECLARATION_CLASS__LINKER_OPTIONS_PRAGMA:
				return linkerOptionsPragma != null;
			case AdaPackage.DECLARATION_CLASS__LIST_PRAGMA:
				return listPragma != null;
			case AdaPackage.DECLARATION_CLASS__LOCKING_POLICY_PRAGMA:
				return lockingPolicyPragma != null;
			case AdaPackage.DECLARATION_CLASS__NORMALIZE_SCALARS_PRAGMA:
				return normalizeScalarsPragma != null;
			case AdaPackage.DECLARATION_CLASS__OPTIMIZE_PRAGMA:
				return optimizePragma != null;
			case AdaPackage.DECLARATION_CLASS__PACK_PRAGMA:
				return packPragma != null;
			case AdaPackage.DECLARATION_CLASS__PAGE_PRAGMA:
				return pagePragma != null;
			case AdaPackage.DECLARATION_CLASS__PREELABORATE_PRAGMA:
				return preelaboratePragma != null;
			case AdaPackage.DECLARATION_CLASS__PRIORITY_PRAGMA:
				return priorityPragma != null;
			case AdaPackage.DECLARATION_CLASS__PURE_PRAGMA:
				return purePragma != null;
			case AdaPackage.DECLARATION_CLASS__QUEUING_POLICY_PRAGMA:
				return queuingPolicyPragma != null;
			case AdaPackage.DECLARATION_CLASS__REMOTE_CALL_INTERFACE_PRAGMA:
				return remoteCallInterfacePragma != null;
			case AdaPackage.DECLARATION_CLASS__REMOTE_TYPES_PRAGMA:
				return remoteTypesPragma != null;
			case AdaPackage.DECLARATION_CLASS__RESTRICTIONS_PRAGMA:
				return restrictionsPragma != null;
			case AdaPackage.DECLARATION_CLASS__REVIEWABLE_PRAGMA:
				return reviewablePragma != null;
			case AdaPackage.DECLARATION_CLASS__SHARED_PASSIVE_PRAGMA:
				return sharedPassivePragma != null;
			case AdaPackage.DECLARATION_CLASS__STORAGE_SIZE_PRAGMA:
				return storageSizePragma != null;
			case AdaPackage.DECLARATION_CLASS__SUPPRESS_PRAGMA:
				return suppressPragma != null;
			case AdaPackage.DECLARATION_CLASS__TASK_DISPATCHING_POLICY_PRAGMA:
				return taskDispatchingPolicyPragma != null;
			case AdaPackage.DECLARATION_CLASS__VOLATILE_PRAGMA:
				return volatilePragma != null;
			case AdaPackage.DECLARATION_CLASS__VOLATILE_COMPONENTS_PRAGMA:
				return volatileComponentsPragma != null;
			case AdaPackage.DECLARATION_CLASS__ASSERT_PRAGMA:
				return assertPragma != null;
			case AdaPackage.DECLARATION_CLASS__ASSERTION_POLICY_PRAGMA:
				return assertionPolicyPragma != null;
			case AdaPackage.DECLARATION_CLASS__DETECT_BLOCKING_PRAGMA:
				return detectBlockingPragma != null;
			case AdaPackage.DECLARATION_CLASS__NO_RETURN_PRAGMA:
				return noReturnPragma != null;
			case AdaPackage.DECLARATION_CLASS__PARTITION_ELABORATION_POLICY_PRAGMA:
				return partitionElaborationPolicyPragma != null;
			case AdaPackage.DECLARATION_CLASS__PREELABORABLE_INITIALIZATION_PRAGMA:
				return preelaborableInitializationPragma != null;
			case AdaPackage.DECLARATION_CLASS__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return prioritySpecificDispatchingPragma != null;
			case AdaPackage.DECLARATION_CLASS__PROFILE_PRAGMA:
				return profilePragma != null;
			case AdaPackage.DECLARATION_CLASS__RELATIVE_DEADLINE_PRAGMA:
				return relativeDeadlinePragma != null;
			case AdaPackage.DECLARATION_CLASS__UNCHECKED_UNION_PRAGMA:
				return uncheckedUnionPragma != null;
			case AdaPackage.DECLARATION_CLASS__UNSUPPRESS_PRAGMA:
				return unsuppressPragma != null;
			case AdaPackage.DECLARATION_CLASS__DEFAULT_STORAGE_POOL_PRAGMA:
				return defaultStoragePoolPragma != null;
			case AdaPackage.DECLARATION_CLASS__DISPATCHING_DOMAIN_PRAGMA:
				return dispatchingDomainPragma != null;
			case AdaPackage.DECLARATION_CLASS__CPU_PRAGMA:
				return cpuPragma != null;
			case AdaPackage.DECLARATION_CLASS__INDEPENDENT_PRAGMA:
				return independentPragma != null;
			case AdaPackage.DECLARATION_CLASS__INDEPENDENT_COMPONENTS_PRAGMA:
				return independentComponentsPragma != null;
			case AdaPackage.DECLARATION_CLASS__IMPLEMENTATION_DEFINED_PRAGMA:
				return implementationDefinedPragma != null;
			case AdaPackage.DECLARATION_CLASS__UNKNOWN_PRAGMA:
				return unknownPragma != null;
		}
		return super.eIsSet(featureID);
	}

} //DeclarationClassImpl
