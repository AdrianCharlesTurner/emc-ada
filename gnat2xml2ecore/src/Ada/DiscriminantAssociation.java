/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Discriminant Association</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.DiscriminantAssociation#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.DiscriminantAssociation#getDiscriminantSelectorNamesQl <em>Discriminant Selector Names Ql</em>}</li>
 *   <li>{@link Ada.DiscriminantAssociation#getDiscriminantExpressionQ <em>Discriminant Expression Q</em>}</li>
 *   <li>{@link Ada.DiscriminantAssociation#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getDiscriminantAssociation()
 * @model extendedMetaData="name='Discriminant_Association' kind='elementOnly'"
 * @generated
 */
public interface DiscriminantAssociation extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getDiscriminantAssociation_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.DiscriminantAssociation#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Discriminant Selector Names Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discriminant Selector Names Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discriminant Selector Names Ql</em>' containment reference.
	 * @see #setDiscriminantSelectorNamesQl(ExpressionList)
	 * @see Ada.AdaPackage#getDiscriminantAssociation_DiscriminantSelectorNamesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='discriminant_selector_names_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionList getDiscriminantSelectorNamesQl();

	/**
	 * Sets the value of the '{@link Ada.DiscriminantAssociation#getDiscriminantSelectorNamesQl <em>Discriminant Selector Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discriminant Selector Names Ql</em>' containment reference.
	 * @see #getDiscriminantSelectorNamesQl()
	 * @generated
	 */
	void setDiscriminantSelectorNamesQl(ExpressionList value);

	/**
	 * Returns the value of the '<em><b>Discriminant Expression Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discriminant Expression Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discriminant Expression Q</em>' containment reference.
	 * @see #setDiscriminantExpressionQ(ExpressionClass)
	 * @see Ada.AdaPackage#getDiscriminantAssociation_DiscriminantExpressionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='discriminant_expression_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getDiscriminantExpressionQ();

	/**
	 * Sets the value of the '{@link Ada.DiscriminantAssociation#getDiscriminantExpressionQ <em>Discriminant Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discriminant Expression Q</em>' containment reference.
	 * @see #getDiscriminantExpressionQ()
	 * @generated
	 */
	void setDiscriminantExpressionQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getDiscriminantAssociation_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.DiscriminantAssociation#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // DiscriminantAssociation
