/**
 */
package Ada;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Name List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.NameList#getGroup <em>Group</em>}</li>
 *   <li>{@link Ada.NameList#getNotAnElement <em>Not An Element</em>}</li>
 *   <li>{@link Ada.NameList#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link Ada.NameList#getSelectedComponent <em>Selected Component</em>}</li>
 *   <li>{@link Ada.NameList#getAccessAttribute <em>Access Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getAddressAttribute <em>Address Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getAdjacentAttribute <em>Adjacent Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getAftAttribute <em>Aft Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getAlignmentAttribute <em>Alignment Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getBaseAttribute <em>Base Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getBitOrderAttribute <em>Bit Order Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getBodyVersionAttribute <em>Body Version Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getCallableAttribute <em>Callable Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getCallerAttribute <em>Caller Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getCeilingAttribute <em>Ceiling Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getClassAttribute <em>Class Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getComponentSizeAttribute <em>Component Size Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getComposeAttribute <em>Compose Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getConstrainedAttribute <em>Constrained Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getCopySignAttribute <em>Copy Sign Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getCountAttribute <em>Count Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getDefiniteAttribute <em>Definite Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getDeltaAttribute <em>Delta Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getDenormAttribute <em>Denorm Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getDigitsAttribute <em>Digits Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getExponentAttribute <em>Exponent Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getExternalTagAttribute <em>External Tag Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getFirstAttribute <em>First Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getFirstBitAttribute <em>First Bit Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getFloorAttribute <em>Floor Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getForeAttribute <em>Fore Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getFractionAttribute <em>Fraction Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getIdentityAttribute <em>Identity Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getImageAttribute <em>Image Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getInputAttribute <em>Input Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getLastAttribute <em>Last Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getLastBitAttribute <em>Last Bit Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getLeadingPartAttribute <em>Leading Part Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getLengthAttribute <em>Length Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getMachineAttribute <em>Machine Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getMachineEmaxAttribute <em>Machine Emax Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getMachineEminAttribute <em>Machine Emin Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getMachineMantissaAttribute <em>Machine Mantissa Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getMachineOverflowsAttribute <em>Machine Overflows Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getMachineRadixAttribute <em>Machine Radix Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getMachineRoundsAttribute <em>Machine Rounds Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getMaxAttribute <em>Max Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getMaxSizeInStorageElementsAttribute <em>Max Size In Storage Elements Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getMinAttribute <em>Min Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getModelAttribute <em>Model Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getModelEminAttribute <em>Model Emin Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getModelEpsilonAttribute <em>Model Epsilon Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getModelMantissaAttribute <em>Model Mantissa Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getModelSmallAttribute <em>Model Small Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getModulusAttribute <em>Modulus Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getOutputAttribute <em>Output Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getPartitionIdAttribute <em>Partition Id Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getPosAttribute <em>Pos Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getPositionAttribute <em>Position Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getPredAttribute <em>Pred Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getRangeAttribute <em>Range Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getReadAttribute <em>Read Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getRemainderAttribute <em>Remainder Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getRoundAttribute <em>Round Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getRoundingAttribute <em>Rounding Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getSafeFirstAttribute <em>Safe First Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getSafeLastAttribute <em>Safe Last Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getScaleAttribute <em>Scale Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getScalingAttribute <em>Scaling Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getSignedZerosAttribute <em>Signed Zeros Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getSizeAttribute <em>Size Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getSmallAttribute <em>Small Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getStoragePoolAttribute <em>Storage Pool Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getStorageSizeAttribute <em>Storage Size Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getSuccAttribute <em>Succ Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getTagAttribute <em>Tag Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getTerminatedAttribute <em>Terminated Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getTruncationAttribute <em>Truncation Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getUnbiasedRoundingAttribute <em>Unbiased Rounding Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getUncheckedAccessAttribute <em>Unchecked Access Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getValAttribute <em>Val Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getValidAttribute <em>Valid Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getValueAttribute <em>Value Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getVersionAttribute <em>Version Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getWideImageAttribute <em>Wide Image Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getWideValueAttribute <em>Wide Value Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getWideWidthAttribute <em>Wide Width Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getWidthAttribute <em>Width Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getWriteAttribute <em>Write Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getMachineRoundingAttribute <em>Machine Rounding Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getModAttribute <em>Mod Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getPriorityAttribute <em>Priority Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getStreamSizeAttribute <em>Stream Size Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getWideWideImageAttribute <em>Wide Wide Image Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getWideWideValueAttribute <em>Wide Wide Value Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getWideWideWidthAttribute <em>Wide Wide Width Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getMaxAlignmentForAllocationAttribute <em>Max Alignment For Allocation Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getOverlapsStorageAttribute <em>Overlaps Storage Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getImplementationDefinedAttribute <em>Implementation Defined Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getUnknownAttribute <em>Unknown Attribute</em>}</li>
 *   <li>{@link Ada.NameList#getComment <em>Comment</em>}</li>
 *   <li>{@link Ada.NameList#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getAsynchronousPragma <em>Asynchronous Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getAtomicPragma <em>Atomic Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getAttachHandlerPragma <em>Attach Handler Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getControlledPragma <em>Controlled Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getConventionPragma <em>Convention Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getDiscardNamesPragma <em>Discard Names Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getElaboratePragma <em>Elaborate Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getElaborateAllPragma <em>Elaborate All Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getExportPragma <em>Export Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getImportPragma <em>Import Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getInlinePragma <em>Inline Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getInspectionPointPragma <em>Inspection Point Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getLinkerOptionsPragma <em>Linker Options Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getListPragma <em>List Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getLockingPolicyPragma <em>Locking Policy Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getOptimizePragma <em>Optimize Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getPackPragma <em>Pack Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getPagePragma <em>Page Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getPreelaboratePragma <em>Preelaborate Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getPriorityPragma <em>Priority Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getPurePragma <em>Pure Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getRemoteTypesPragma <em>Remote Types Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getRestrictionsPragma <em>Restrictions Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getReviewablePragma <em>Reviewable Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getSharedPassivePragma <em>Shared Passive Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getStorageSizePragma <em>Storage Size Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getSuppressPragma <em>Suppress Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getVolatilePragma <em>Volatile Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getAssertPragma <em>Assert Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getNoReturnPragma <em>No Return Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getProfilePragma <em>Profile Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getUnsuppressPragma <em>Unsuppress Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getCpuPragma <em>Cpu Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getIndependentPragma <em>Independent Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getIndependentComponentsPragma <em>Independent Components Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}</li>
 *   <li>{@link Ada.NameList#getUnknownPragma <em>Unknown Pragma</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getNameList()
 * @model extendedMetaData="name='Name_List' kind='elementOnly'"
 * @generated
 */
public interface NameList extends EObject {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' attribute list.
	 * @see Ada.AdaPackage#getNameList_Group()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:0'"
	 * @generated
	 */
	FeatureMap getGroup();

	/**
	 * Returns the value of the '<em><b>Not An Element</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NotAnElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not An Element</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not An Element</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_NotAnElement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='not_an_element' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NotAnElement> getNotAnElement();

	/**
	 * Returns the value of the '<em><b>Identifier</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.Identifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Identifier</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identifier</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_Identifier()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='identifier' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<Identifier> getIdentifier();

	/**
	 * Returns the value of the '<em><b>Selected Component</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SelectedComponent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selected Component</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selected Component</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_SelectedComponent()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='selected_component' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SelectedComponent> getSelectedComponent();

	/**
	 * Returns the value of the '<em><b>Access Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AccessAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Access Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Access Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_AccessAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='access_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AccessAttribute> getAccessAttribute();

	/**
	 * Returns the value of the '<em><b>Address Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AddressAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Address Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Address Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_AddressAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='address_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AddressAttribute> getAddressAttribute();

	/**
	 * Returns the value of the '<em><b>Adjacent Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AdjacentAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Adjacent Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Adjacent Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_AdjacentAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='adjacent_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AdjacentAttribute> getAdjacentAttribute();

	/**
	 * Returns the value of the '<em><b>Aft Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AftAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aft Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aft Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_AftAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='aft_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AftAttribute> getAftAttribute();

	/**
	 * Returns the value of the '<em><b>Alignment Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AlignmentAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alignment Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alignment Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_AlignmentAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='alignment_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AlignmentAttribute> getAlignmentAttribute();

	/**
	 * Returns the value of the '<em><b>Base Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.BaseAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_BaseAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='base_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<BaseAttribute> getBaseAttribute();

	/**
	 * Returns the value of the '<em><b>Bit Order Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.BitOrderAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bit Order Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bit Order Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_BitOrderAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='bit_order_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<BitOrderAttribute> getBitOrderAttribute();

	/**
	 * Returns the value of the '<em><b>Body Version Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.BodyVersionAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body Version Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body Version Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_BodyVersionAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='body_version_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<BodyVersionAttribute> getBodyVersionAttribute();

	/**
	 * Returns the value of the '<em><b>Callable Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CallableAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Callable Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Callable Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_CallableAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='callable_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CallableAttribute> getCallableAttribute();

	/**
	 * Returns the value of the '<em><b>Caller Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CallerAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Caller Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Caller Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_CallerAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='caller_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CallerAttribute> getCallerAttribute();

	/**
	 * Returns the value of the '<em><b>Ceiling Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CeilingAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ceiling Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ceiling Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_CeilingAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ceiling_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CeilingAttribute> getCeilingAttribute();

	/**
	 * Returns the value of the '<em><b>Class Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ClassAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ClassAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='class_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ClassAttribute> getClassAttribute();

	/**
	 * Returns the value of the '<em><b>Component Size Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ComponentSizeAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Size Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Size Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ComponentSizeAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='component_size_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ComponentSizeAttribute> getComponentSizeAttribute();

	/**
	 * Returns the value of the '<em><b>Compose Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ComposeAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Compose Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Compose Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ComposeAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='compose_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ComposeAttribute> getComposeAttribute();

	/**
	 * Returns the value of the '<em><b>Constrained Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ConstrainedAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constrained Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constrained Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ConstrainedAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='constrained_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ConstrainedAttribute> getConstrainedAttribute();

	/**
	 * Returns the value of the '<em><b>Copy Sign Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CopySignAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Copy Sign Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Copy Sign Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_CopySignAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='copy_sign_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CopySignAttribute> getCopySignAttribute();

	/**
	 * Returns the value of the '<em><b>Count Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CountAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Count Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Count Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_CountAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='count_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CountAttribute> getCountAttribute();

	/**
	 * Returns the value of the '<em><b>Definite Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefiniteAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Definite Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definite Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_DefiniteAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='definite_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefiniteAttribute> getDefiniteAttribute();

	/**
	 * Returns the value of the '<em><b>Delta Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DeltaAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delta Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delta Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_DeltaAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='delta_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DeltaAttribute> getDeltaAttribute();

	/**
	 * Returns the value of the '<em><b>Denorm Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DenormAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Denorm Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Denorm Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_DenormAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='denorm_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DenormAttribute> getDenormAttribute();

	/**
	 * Returns the value of the '<em><b>Digits Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DigitsAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Digits Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Digits Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_DigitsAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='digits_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DigitsAttribute> getDigitsAttribute();

	/**
	 * Returns the value of the '<em><b>Exponent Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExponentAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exponent Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exponent Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ExponentAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='exponent_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExponentAttribute> getExponentAttribute();

	/**
	 * Returns the value of the '<em><b>External Tag Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExternalTagAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External Tag Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Tag Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ExternalTagAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='external_tag_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExternalTagAttribute> getExternalTagAttribute();

	/**
	 * Returns the value of the '<em><b>First Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FirstAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_FirstAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='first_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FirstAttribute> getFirstAttribute();

	/**
	 * Returns the value of the '<em><b>First Bit Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FirstBitAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Bit Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Bit Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_FirstBitAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='first_bit_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FirstBitAttribute> getFirstBitAttribute();

	/**
	 * Returns the value of the '<em><b>Floor Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FloorAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Floor Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Floor Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_FloorAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='floor_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FloorAttribute> getFloorAttribute();

	/**
	 * Returns the value of the '<em><b>Fore Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ForeAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fore Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fore Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ForeAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='fore_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ForeAttribute> getForeAttribute();

	/**
	 * Returns the value of the '<em><b>Fraction Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FractionAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fraction Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fraction Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_FractionAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='fraction_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FractionAttribute> getFractionAttribute();

	/**
	 * Returns the value of the '<em><b>Identity Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IdentityAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Identity Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identity Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_IdentityAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='identity_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IdentityAttribute> getIdentityAttribute();

	/**
	 * Returns the value of the '<em><b>Image Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ImageAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Image Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Image Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ImageAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='image_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ImageAttribute> getImageAttribute();

	/**
	 * Returns the value of the '<em><b>Input Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InputAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_InputAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='input_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InputAttribute> getInputAttribute();

	/**
	 * Returns the value of the '<em><b>Last Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LastAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_LastAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='last_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LastAttribute> getLastAttribute();

	/**
	 * Returns the value of the '<em><b>Last Bit Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LastBitAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Bit Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Bit Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_LastBitAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='last_bit_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LastBitAttribute> getLastBitAttribute();

	/**
	 * Returns the value of the '<em><b>Leading Part Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LeadingPartAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Leading Part Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Leading Part Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_LeadingPartAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='leading_part_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LeadingPartAttribute> getLeadingPartAttribute();

	/**
	 * Returns the value of the '<em><b>Length Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LengthAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Length Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Length Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_LengthAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='length_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LengthAttribute> getLengthAttribute();

	/**
	 * Returns the value of the '<em><b>Machine Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MachineAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_MachineAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='machine_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MachineAttribute> getMachineAttribute();

	/**
	 * Returns the value of the '<em><b>Machine Emax Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MachineEmaxAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Emax Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Emax Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_MachineEmaxAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='machine_emax_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MachineEmaxAttribute> getMachineEmaxAttribute();

	/**
	 * Returns the value of the '<em><b>Machine Emin Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MachineEminAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Emin Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Emin Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_MachineEminAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='machine_emin_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MachineEminAttribute> getMachineEminAttribute();

	/**
	 * Returns the value of the '<em><b>Machine Mantissa Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MachineMantissaAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Mantissa Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Mantissa Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_MachineMantissaAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='machine_mantissa_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MachineMantissaAttribute> getMachineMantissaAttribute();

	/**
	 * Returns the value of the '<em><b>Machine Overflows Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MachineOverflowsAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Overflows Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Overflows Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_MachineOverflowsAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='machine_overflows_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MachineOverflowsAttribute> getMachineOverflowsAttribute();

	/**
	 * Returns the value of the '<em><b>Machine Radix Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MachineRadixAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Radix Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Radix Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_MachineRadixAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='machine_radix_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MachineRadixAttribute> getMachineRadixAttribute();

	/**
	 * Returns the value of the '<em><b>Machine Rounds Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MachineRoundsAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Rounds Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Rounds Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_MachineRoundsAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='machine_rounds_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MachineRoundsAttribute> getMachineRoundsAttribute();

	/**
	 * Returns the value of the '<em><b>Max Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MaxAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_MaxAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='max_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MaxAttribute> getMaxAttribute();

	/**
	 * Returns the value of the '<em><b>Max Size In Storage Elements Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MaxSizeInStorageElementsAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Size In Storage Elements Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Size In Storage Elements Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_MaxSizeInStorageElementsAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='max_size_in_storage_elements_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MaxSizeInStorageElementsAttribute> getMaxSizeInStorageElementsAttribute();

	/**
	 * Returns the value of the '<em><b>Min Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MinAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_MinAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='min_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MinAttribute> getMinAttribute();

	/**
	 * Returns the value of the '<em><b>Model Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ModelAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ModelAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='model_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ModelAttribute> getModelAttribute();

	/**
	 * Returns the value of the '<em><b>Model Emin Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ModelEminAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Emin Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Emin Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ModelEminAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='model_emin_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ModelEminAttribute> getModelEminAttribute();

	/**
	 * Returns the value of the '<em><b>Model Epsilon Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ModelEpsilonAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Epsilon Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Epsilon Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ModelEpsilonAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='model_epsilon_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ModelEpsilonAttribute> getModelEpsilonAttribute();

	/**
	 * Returns the value of the '<em><b>Model Mantissa Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ModelMantissaAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Mantissa Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Mantissa Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ModelMantissaAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='model_mantissa_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ModelMantissaAttribute> getModelMantissaAttribute();

	/**
	 * Returns the value of the '<em><b>Model Small Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ModelSmallAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Small Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Small Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ModelSmallAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='model_small_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ModelSmallAttribute> getModelSmallAttribute();

	/**
	 * Returns the value of the '<em><b>Modulus Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ModulusAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Modulus Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Modulus Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ModulusAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='modulus_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ModulusAttribute> getModulusAttribute();

	/**
	 * Returns the value of the '<em><b>Output Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OutputAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_OutputAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='output_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OutputAttribute> getOutputAttribute();

	/**
	 * Returns the value of the '<em><b>Partition Id Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PartitionIdAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partition Id Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partition Id Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_PartitionIdAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='partition_id_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PartitionIdAttribute> getPartitionIdAttribute();

	/**
	 * Returns the value of the '<em><b>Pos Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PosAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pos Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pos Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_PosAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='pos_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PosAttribute> getPosAttribute();

	/**
	 * Returns the value of the '<em><b>Position Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PositionAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Position Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_PositionAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='position_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PositionAttribute> getPositionAttribute();

	/**
	 * Returns the value of the '<em><b>Pred Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PredAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pred Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pred Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_PredAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='pred_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PredAttribute> getPredAttribute();

	/**
	 * Returns the value of the '<em><b>Range Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RangeAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Range Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_RangeAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='range_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RangeAttribute> getRangeAttribute();

	/**
	 * Returns the value of the '<em><b>Read Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ReadAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Read Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Read Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ReadAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='read_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ReadAttribute> getReadAttribute();

	/**
	 * Returns the value of the '<em><b>Remainder Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RemainderAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remainder Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remainder Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_RemainderAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='remainder_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RemainderAttribute> getRemainderAttribute();

	/**
	 * Returns the value of the '<em><b>Round Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RoundAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Round Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Round Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_RoundAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='round_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RoundAttribute> getRoundAttribute();

	/**
	 * Returns the value of the '<em><b>Rounding Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RoundingAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rounding Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rounding Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_RoundingAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='rounding_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RoundingAttribute> getRoundingAttribute();

	/**
	 * Returns the value of the '<em><b>Safe First Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SafeFirstAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Safe First Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Safe First Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_SafeFirstAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='safe_first_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SafeFirstAttribute> getSafeFirstAttribute();

	/**
	 * Returns the value of the '<em><b>Safe Last Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SafeLastAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Safe Last Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Safe Last Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_SafeLastAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='safe_last_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SafeLastAttribute> getSafeLastAttribute();

	/**
	 * Returns the value of the '<em><b>Scale Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ScaleAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scale Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scale Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ScaleAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='scale_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ScaleAttribute> getScaleAttribute();

	/**
	 * Returns the value of the '<em><b>Scaling Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ScalingAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scaling Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scaling Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ScalingAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='scaling_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ScalingAttribute> getScalingAttribute();

	/**
	 * Returns the value of the '<em><b>Signed Zeros Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SignedZerosAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signed Zeros Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signed Zeros Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_SignedZerosAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='signed_zeros_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SignedZerosAttribute> getSignedZerosAttribute();

	/**
	 * Returns the value of the '<em><b>Size Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SizeAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Size Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Size Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_SizeAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='size_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SizeAttribute> getSizeAttribute();

	/**
	 * Returns the value of the '<em><b>Small Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SmallAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Small Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Small Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_SmallAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='small_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SmallAttribute> getSmallAttribute();

	/**
	 * Returns the value of the '<em><b>Storage Pool Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.StoragePoolAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Pool Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Pool Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_StoragePoolAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='storage_pool_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<StoragePoolAttribute> getStoragePoolAttribute();

	/**
	 * Returns the value of the '<em><b>Storage Size Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.StorageSizeAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Size Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Size Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_StorageSizeAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='storage_size_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<StorageSizeAttribute> getStorageSizeAttribute();

	/**
	 * Returns the value of the '<em><b>Succ Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SuccAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Succ Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Succ Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_SuccAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='succ_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SuccAttribute> getSuccAttribute();

	/**
	 * Returns the value of the '<em><b>Tag Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TagAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tag Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tag Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_TagAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='tag_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TagAttribute> getTagAttribute();

	/**
	 * Returns the value of the '<em><b>Terminated Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TerminatedAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Terminated Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Terminated Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_TerminatedAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='terminated_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TerminatedAttribute> getTerminatedAttribute();

	/**
	 * Returns the value of the '<em><b>Truncation Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TruncationAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Truncation Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Truncation Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_TruncationAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='truncation_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TruncationAttribute> getTruncationAttribute();

	/**
	 * Returns the value of the '<em><b>Unbiased Rounding Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnbiasedRoundingAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unbiased Rounding Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unbiased Rounding Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_UnbiasedRoundingAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unbiased_rounding_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnbiasedRoundingAttribute> getUnbiasedRoundingAttribute();

	/**
	 * Returns the value of the '<em><b>Unchecked Access Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UncheckedAccessAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unchecked Access Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unchecked Access Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_UncheckedAccessAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unchecked_access_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UncheckedAccessAttribute> getUncheckedAccessAttribute();

	/**
	 * Returns the value of the '<em><b>Val Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ValAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Val Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Val Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ValAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='val_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ValAttribute> getValAttribute();

	/**
	 * Returns the value of the '<em><b>Valid Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ValidAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Valid Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Valid Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ValidAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='valid_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ValidAttribute> getValidAttribute();

	/**
	 * Returns the value of the '<em><b>Value Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ValueAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ValueAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='value_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ValueAttribute> getValueAttribute();

	/**
	 * Returns the value of the '<em><b>Version Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.VersionAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_VersionAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='version_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<VersionAttribute> getVersionAttribute();

	/**
	 * Returns the value of the '<em><b>Wide Image Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WideImageAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Image Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Image Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_WideImageAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='wide_image_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WideImageAttribute> getWideImageAttribute();

	/**
	 * Returns the value of the '<em><b>Wide Value Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WideValueAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Value Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Value Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_WideValueAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='wide_value_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WideValueAttribute> getWideValueAttribute();

	/**
	 * Returns the value of the '<em><b>Wide Width Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WideWidthAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Width Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Width Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_WideWidthAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='wide_width_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WideWidthAttribute> getWideWidthAttribute();

	/**
	 * Returns the value of the '<em><b>Width Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WidthAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Width Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Width Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_WidthAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='width_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WidthAttribute> getWidthAttribute();

	/**
	 * Returns the value of the '<em><b>Write Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WriteAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Write Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Write Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_WriteAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='write_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WriteAttribute> getWriteAttribute();

	/**
	 * Returns the value of the '<em><b>Machine Rounding Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MachineRoundingAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Machine Rounding Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Machine Rounding Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_MachineRoundingAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='machine_rounding_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MachineRoundingAttribute> getMachineRoundingAttribute();

	/**
	 * Returns the value of the '<em><b>Mod Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ModAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mod Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mod Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ModAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='mod_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ModAttribute> getModAttribute();

	/**
	 * Returns the value of the '<em><b>Priority Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PriorityAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_PriorityAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='priority_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PriorityAttribute> getPriorityAttribute();

	/**
	 * Returns the value of the '<em><b>Stream Size Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.StreamSizeAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stream Size Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stream Size Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_StreamSizeAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='stream_size_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<StreamSizeAttribute> getStreamSizeAttribute();

	/**
	 * Returns the value of the '<em><b>Wide Wide Image Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WideWideImageAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Wide Image Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Wide Image Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_WideWideImageAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='wide_wide_image_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WideWideImageAttribute> getWideWideImageAttribute();

	/**
	 * Returns the value of the '<em><b>Wide Wide Value Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WideWideValueAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Wide Value Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Wide Value Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_WideWideValueAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='wide_wide_value_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WideWideValueAttribute> getWideWideValueAttribute();

	/**
	 * Returns the value of the '<em><b>Wide Wide Width Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WideWideWidthAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wide Wide Width Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wide Wide Width Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_WideWideWidthAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='wide_wide_width_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WideWideWidthAttribute> getWideWideWidthAttribute();

	/**
	 * Returns the value of the '<em><b>Max Alignment For Allocation Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.MaxAlignmentForAllocationAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Alignment For Allocation Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Alignment For Allocation Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_MaxAlignmentForAllocationAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='max_alignment_for_allocation_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<MaxAlignmentForAllocationAttribute> getMaxAlignmentForAllocationAttribute();

	/**
	 * Returns the value of the '<em><b>Overlaps Storage Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OverlapsStorageAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overlaps Storage Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overlaps Storage Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_OverlapsStorageAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='overlaps_storage_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OverlapsStorageAttribute> getOverlapsStorageAttribute();

	/**
	 * Returns the value of the '<em><b>Implementation Defined Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ImplementationDefinedAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implementation Defined Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implementation Defined Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ImplementationDefinedAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='implementation_defined_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ImplementationDefinedAttribute> getImplementationDefinedAttribute();

	/**
	 * Returns the value of the '<em><b>Unknown Attribute</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnknownAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unknown Attribute</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unknown Attribute</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_UnknownAttribute()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unknown_attribute' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnknownAttribute> getUnknownAttribute();

	/**
	 * Returns the value of the '<em><b>Comment</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.Comment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comment</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comment</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_Comment()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='comment' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<Comment> getComment();

	/**
	 * Returns the value of the '<em><b>All Calls Remote Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AllCallsRemotePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Calls Remote Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Calls Remote Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_AllCallsRemotePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='all_calls_remote_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AllCallsRemotePragma> getAllCallsRemotePragma();

	/**
	 * Returns the value of the '<em><b>Asynchronous Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AsynchronousPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asynchronous Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asynchronous Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_AsynchronousPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='asynchronous_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AsynchronousPragma> getAsynchronousPragma();

	/**
	 * Returns the value of the '<em><b>Atomic Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AtomicPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_AtomicPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='atomic_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AtomicPragma> getAtomicPragma();

	/**
	 * Returns the value of the '<em><b>Atomic Components Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AtomicComponentsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Components Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Components Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_AtomicComponentsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='atomic_components_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AtomicComponentsPragma> getAtomicComponentsPragma();

	/**
	 * Returns the value of the '<em><b>Attach Handler Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AttachHandlerPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attach Handler Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attach Handler Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_AttachHandlerPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='attach_handler_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AttachHandlerPragma> getAttachHandlerPragma();

	/**
	 * Returns the value of the '<em><b>Controlled Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ControlledPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controlled Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controlled Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ControlledPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='controlled_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ControlledPragma> getControlledPragma();

	/**
	 * Returns the value of the '<em><b>Convention Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ConventionPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Convention Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Convention Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ConventionPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='convention_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ConventionPragma> getConventionPragma();

	/**
	 * Returns the value of the '<em><b>Discard Names Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscardNamesPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discard Names Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discard Names Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_DiscardNamesPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discard_names_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscardNamesPragma> getDiscardNamesPragma();

	/**
	 * Returns the value of the '<em><b>Elaborate Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElaboratePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ElaboratePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='elaborate_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElaboratePragma> getElaboratePragma();

	/**
	 * Returns the value of the '<em><b>Elaborate All Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElaborateAllPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate All Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate All Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ElaborateAllPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='elaborate_all_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElaborateAllPragma> getElaborateAllPragma();

	/**
	 * Returns the value of the '<em><b>Elaborate Body Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElaborateBodyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Body Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Body Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ElaborateBodyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='elaborate_body_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElaborateBodyPragma> getElaborateBodyPragma();

	/**
	 * Returns the value of the '<em><b>Export Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExportPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Export Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Export Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ExportPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='export_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExportPragma> getExportPragma();

	/**
	 * Returns the value of the '<em><b>Import Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ImportPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Import Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Import Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ImportPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='import_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ImportPragma> getImportPragma();

	/**
	 * Returns the value of the '<em><b>Inline Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InlinePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inline Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inline Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_InlinePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='inline_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InlinePragma> getInlinePragma();

	/**
	 * Returns the value of the '<em><b>Inspection Point Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InspectionPointPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inspection Point Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inspection Point Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_InspectionPointPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='inspection_point_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InspectionPointPragma> getInspectionPointPragma();

	/**
	 * Returns the value of the '<em><b>Interrupt Handler Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InterruptHandlerPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Handler Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Handler Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_InterruptHandlerPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='interrupt_handler_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InterruptHandlerPragma> getInterruptHandlerPragma();

	/**
	 * Returns the value of the '<em><b>Interrupt Priority Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InterruptPriorityPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Priority Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Priority Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_InterruptPriorityPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='interrupt_priority_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InterruptPriorityPragma> getInterruptPriorityPragma();

	/**
	 * Returns the value of the '<em><b>Linker Options Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LinkerOptionsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Linker Options Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Linker Options Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_LinkerOptionsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='linker_options_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LinkerOptionsPragma> getLinkerOptionsPragma();

	/**
	 * Returns the value of the '<em><b>List Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ListPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ListPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='list_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ListPragma> getListPragma();

	/**
	 * Returns the value of the '<em><b>Locking Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LockingPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Locking Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Locking Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_LockingPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='locking_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LockingPolicyPragma> getLockingPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Normalize Scalars Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NormalizeScalarsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Normalize Scalars Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Normalize Scalars Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_NormalizeScalarsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='normalize_scalars_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NormalizeScalarsPragma> getNormalizeScalarsPragma();

	/**
	 * Returns the value of the '<em><b>Optimize Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OptimizePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Optimize Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Optimize Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_OptimizePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='optimize_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OptimizePragma> getOptimizePragma();

	/**
	 * Returns the value of the '<em><b>Pack Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PackPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pack Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pack Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_PackPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='pack_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PackPragma> getPackPragma();

	/**
	 * Returns the value of the '<em><b>Page Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PagePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Page Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Page Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_PagePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='page_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PagePragma> getPagePragma();

	/**
	 * Returns the value of the '<em><b>Preelaborate Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PreelaboratePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborate Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborate Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_PreelaboratePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='preelaborate_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PreelaboratePragma> getPreelaboratePragma();

	/**
	 * Returns the value of the '<em><b>Priority Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PriorityPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_PriorityPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='priority_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PriorityPragma> getPriorityPragma();

	/**
	 * Returns the value of the '<em><b>Pure Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PurePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pure Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pure Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_PurePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='pure_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PurePragma> getPurePragma();

	/**
	 * Returns the value of the '<em><b>Queuing Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.QueuingPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Queuing Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Queuing Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_QueuingPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='queuing_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<QueuingPolicyPragma> getQueuingPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Remote Call Interface Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RemoteCallInterfacePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Call Interface Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Call Interface Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_RemoteCallInterfacePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='remote_call_interface_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RemoteCallInterfacePragma> getRemoteCallInterfacePragma();

	/**
	 * Returns the value of the '<em><b>Remote Types Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RemoteTypesPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Types Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Types Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_RemoteTypesPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='remote_types_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RemoteTypesPragma> getRemoteTypesPragma();

	/**
	 * Returns the value of the '<em><b>Restrictions Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RestrictionsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Restrictions Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Restrictions Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_RestrictionsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='restrictions_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RestrictionsPragma> getRestrictionsPragma();

	/**
	 * Returns the value of the '<em><b>Reviewable Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ReviewablePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reviewable Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reviewable Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ReviewablePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='reviewable_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ReviewablePragma> getReviewablePragma();

	/**
	 * Returns the value of the '<em><b>Shared Passive Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SharedPassivePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shared Passive Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shared Passive Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_SharedPassivePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='shared_passive_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SharedPassivePragma> getSharedPassivePragma();

	/**
	 * Returns the value of the '<em><b>Storage Size Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.StorageSizePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Size Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Size Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_StorageSizePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='storage_size_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<StorageSizePragma> getStorageSizePragma();

	/**
	 * Returns the value of the '<em><b>Suppress Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SuppressPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Suppress Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Suppress Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_SuppressPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='suppress_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SuppressPragma> getSuppressPragma();

	/**
	 * Returns the value of the '<em><b>Task Dispatching Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TaskDispatchingPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Dispatching Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Dispatching Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_TaskDispatchingPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='task_dispatching_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TaskDispatchingPolicyPragma> getTaskDispatchingPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Volatile Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.VolatilePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_VolatilePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='volatile_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<VolatilePragma> getVolatilePragma();

	/**
	 * Returns the value of the '<em><b>Volatile Components Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.VolatileComponentsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Components Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Components Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_VolatileComponentsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='volatile_components_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<VolatileComponentsPragma> getVolatileComponentsPragma();

	/**
	 * Returns the value of the '<em><b>Assert Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AssertPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assert Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assert Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_AssertPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='assert_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AssertPragma> getAssertPragma();

	/**
	 * Returns the value of the '<em><b>Assertion Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AssertionPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assertion Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assertion Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_AssertionPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='assertion_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AssertionPolicyPragma> getAssertionPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Detect Blocking Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DetectBlockingPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Detect Blocking Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Detect Blocking Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_DetectBlockingPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='detect_blocking_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DetectBlockingPragma> getDetectBlockingPragma();

	/**
	 * Returns the value of the '<em><b>No Return Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NoReturnPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>No Return Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>No Return Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_NoReturnPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='no_return_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NoReturnPragma> getNoReturnPragma();

	/**
	 * Returns the value of the '<em><b>Partition Elaboration Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PartitionElaborationPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partition Elaboration Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partition Elaboration Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_PartitionElaborationPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='partition_elaboration_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PartitionElaborationPolicyPragma> getPartitionElaborationPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Preelaborable Initialization Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PreelaborableInitializationPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborable Initialization Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborable Initialization Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_PreelaborableInitializationPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='preelaborable_initialization_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PreelaborableInitializationPragma> getPreelaborableInitializationPragma();

	/**
	 * Returns the value of the '<em><b>Priority Specific Dispatching Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PrioritySpecificDispatchingPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Specific Dispatching Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Specific Dispatching Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_PrioritySpecificDispatchingPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='priority_specific_dispatching_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PrioritySpecificDispatchingPragma> getPrioritySpecificDispatchingPragma();

	/**
	 * Returns the value of the '<em><b>Profile Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProfilePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Profile Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Profile Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ProfilePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='profile_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProfilePragma> getProfilePragma();

	/**
	 * Returns the value of the '<em><b>Relative Deadline Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RelativeDeadlinePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relative Deadline Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relative Deadline Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_RelativeDeadlinePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='relative_deadline_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RelativeDeadlinePragma> getRelativeDeadlinePragma();

	/**
	 * Returns the value of the '<em><b>Unchecked Union Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UncheckedUnionPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unchecked Union Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unchecked Union Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_UncheckedUnionPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unchecked_union_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UncheckedUnionPragma> getUncheckedUnionPragma();

	/**
	 * Returns the value of the '<em><b>Unsuppress Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnsuppressPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unsuppress Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unsuppress Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_UnsuppressPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unsuppress_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnsuppressPragma> getUnsuppressPragma();

	/**
	 * Returns the value of the '<em><b>Default Storage Pool Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefaultStoragePoolPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Storage Pool Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Storage Pool Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_DefaultStoragePoolPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='default_storage_pool_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefaultStoragePoolPragma> getDefaultStoragePoolPragma();

	/**
	 * Returns the value of the '<em><b>Dispatching Domain Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DispatchingDomainPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dispatching Domain Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dispatching Domain Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_DispatchingDomainPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='dispatching_domain_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DispatchingDomainPragma> getDispatchingDomainPragma();

	/**
	 * Returns the value of the '<em><b>Cpu Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CpuPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cpu Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cpu Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_CpuPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='cpu_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CpuPragma> getCpuPragma();

	/**
	 * Returns the value of the '<em><b>Independent Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IndependentPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_IndependentPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='independent_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IndependentPragma> getIndependentPragma();

	/**
	 * Returns the value of the '<em><b>Independent Components Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IndependentComponentsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Components Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Components Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_IndependentComponentsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='independent_components_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IndependentComponentsPragma> getIndependentComponentsPragma();

	/**
	 * Returns the value of the '<em><b>Implementation Defined Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ImplementationDefinedPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implementation Defined Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implementation Defined Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_ImplementationDefinedPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='implementation_defined_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ImplementationDefinedPragma> getImplementationDefinedPragma();

	/**
	 * Returns the value of the '<em><b>Unknown Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnknownPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unknown Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unknown Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getNameList_UnknownPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unknown_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnknownPragma> getUnknownPragma();

} // NameList
