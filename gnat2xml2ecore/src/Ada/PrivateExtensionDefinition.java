/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Private Extension Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.PrivateExtensionDefinition#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.PrivateExtensionDefinition#getHasAbstractQ <em>Has Abstract Q</em>}</li>
 *   <li>{@link Ada.PrivateExtensionDefinition#getHasLimitedQ <em>Has Limited Q</em>}</li>
 *   <li>{@link Ada.PrivateExtensionDefinition#getHasSynchronizedQ <em>Has Synchronized Q</em>}</li>
 *   <li>{@link Ada.PrivateExtensionDefinition#getAncestorSubtypeIndicationQ <em>Ancestor Subtype Indication Q</em>}</li>
 *   <li>{@link Ada.PrivateExtensionDefinition#getDefinitionInterfaceListQl <em>Definition Interface List Ql</em>}</li>
 *   <li>{@link Ada.PrivateExtensionDefinition#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getPrivateExtensionDefinition()
 * @model extendedMetaData="name='Private_Extension_Definition' kind='elementOnly'"
 * @generated
 */
public interface PrivateExtensionDefinition extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getPrivateExtensionDefinition_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.PrivateExtensionDefinition#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Has Abstract Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Abstract Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Abstract Q</em>' containment reference.
	 * @see #setHasAbstractQ(HasAbstractQType5)
	 * @see Ada.AdaPackage#getPrivateExtensionDefinition_HasAbstractQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='has_abstract_q' namespace='##targetNamespace'"
	 * @generated
	 */
	HasAbstractQType5 getHasAbstractQ();

	/**
	 * Sets the value of the '{@link Ada.PrivateExtensionDefinition#getHasAbstractQ <em>Has Abstract Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Abstract Q</em>' containment reference.
	 * @see #getHasAbstractQ()
	 * @generated
	 */
	void setHasAbstractQ(HasAbstractQType5 value);

	/**
	 * Returns the value of the '<em><b>Has Limited Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Limited Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Limited Q</em>' containment reference.
	 * @see #setHasLimitedQ(HasLimitedQType2)
	 * @see Ada.AdaPackage#getPrivateExtensionDefinition_HasLimitedQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='has_limited_q' namespace='##targetNamespace'"
	 * @generated
	 */
	HasLimitedQType2 getHasLimitedQ();

	/**
	 * Sets the value of the '{@link Ada.PrivateExtensionDefinition#getHasLimitedQ <em>Has Limited Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Limited Q</em>' containment reference.
	 * @see #getHasLimitedQ()
	 * @generated
	 */
	void setHasLimitedQ(HasLimitedQType2 value);

	/**
	 * Returns the value of the '<em><b>Has Synchronized Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Synchronized Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Synchronized Q</em>' containment reference.
	 * @see #setHasSynchronizedQ(HasSynchronizedQType1)
	 * @see Ada.AdaPackage#getPrivateExtensionDefinition_HasSynchronizedQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='has_synchronized_q' namespace='##targetNamespace'"
	 * @generated
	 */
	HasSynchronizedQType1 getHasSynchronizedQ();

	/**
	 * Sets the value of the '{@link Ada.PrivateExtensionDefinition#getHasSynchronizedQ <em>Has Synchronized Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Synchronized Q</em>' containment reference.
	 * @see #getHasSynchronizedQ()
	 * @generated
	 */
	void setHasSynchronizedQ(HasSynchronizedQType1 value);

	/**
	 * Returns the value of the '<em><b>Ancestor Subtype Indication Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ancestor Subtype Indication Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ancestor Subtype Indication Q</em>' containment reference.
	 * @see #setAncestorSubtypeIndicationQ(ElementClass)
	 * @see Ada.AdaPackage#getPrivateExtensionDefinition_AncestorSubtypeIndicationQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='ancestor_subtype_indication_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementClass getAncestorSubtypeIndicationQ();

	/**
	 * Sets the value of the '{@link Ada.PrivateExtensionDefinition#getAncestorSubtypeIndicationQ <em>Ancestor Subtype Indication Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ancestor Subtype Indication Q</em>' containment reference.
	 * @see #getAncestorSubtypeIndicationQ()
	 * @generated
	 */
	void setAncestorSubtypeIndicationQ(ElementClass value);

	/**
	 * Returns the value of the '<em><b>Definition Interface List Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Definition Interface List Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Definition Interface List Ql</em>' containment reference.
	 * @see #setDefinitionInterfaceListQl(ExpressionList)
	 * @see Ada.AdaPackage#getPrivateExtensionDefinition_DefinitionInterfaceListQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='definition_interface_list_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionList getDefinitionInterfaceListQl();

	/**
	 * Sets the value of the '{@link Ada.PrivateExtensionDefinition#getDefinitionInterfaceListQl <em>Definition Interface List Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Definition Interface List Ql</em>' containment reference.
	 * @see #getDefinitionInterfaceListQl()
	 * @generated
	 */
	void setDefinitionInterfaceListQl(ExpressionList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getPrivateExtensionDefinition_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.PrivateExtensionDefinition#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // PrivateExtensionDefinition
