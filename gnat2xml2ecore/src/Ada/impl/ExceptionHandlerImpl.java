/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.DeclarationClass;
import Ada.ElementList;
import Ada.ExceptionHandler;
import Ada.SourceLocation;
import Ada.StatementList;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Exception Handler</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.ExceptionHandlerImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.ExceptionHandlerImpl#getChoiceParameterSpecificationQ <em>Choice Parameter Specification Q</em>}</li>
 *   <li>{@link Ada.impl.ExceptionHandlerImpl#getExceptionChoicesQl <em>Exception Choices Ql</em>}</li>
 *   <li>{@link Ada.impl.ExceptionHandlerImpl#getHandlerStatementsQl <em>Handler Statements Ql</em>}</li>
 *   <li>{@link Ada.impl.ExceptionHandlerImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExceptionHandlerImpl extends MinimalEObjectImpl.Container implements ExceptionHandler {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getChoiceParameterSpecificationQ() <em>Choice Parameter Specification Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChoiceParameterSpecificationQ()
	 * @generated
	 * @ordered
	 */
	protected DeclarationClass choiceParameterSpecificationQ;

	/**
	 * The cached value of the '{@link #getExceptionChoicesQl() <em>Exception Choices Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExceptionChoicesQl()
	 * @generated
	 * @ordered
	 */
	protected ElementList exceptionChoicesQl;

	/**
	 * The cached value of the '{@link #getHandlerStatementsQl() <em>Handler Statements Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHandlerStatementsQl()
	 * @generated
	 * @ordered
	 */
	protected StatementList handlerStatementsQl;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExceptionHandlerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getExceptionHandler();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.EXCEPTION_HANDLER__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXCEPTION_HANDLER__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXCEPTION_HANDLER__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.EXCEPTION_HANDLER__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeclarationClass getChoiceParameterSpecificationQ() {
		return choiceParameterSpecificationQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChoiceParameterSpecificationQ(DeclarationClass newChoiceParameterSpecificationQ, NotificationChain msgs) {
		DeclarationClass oldChoiceParameterSpecificationQ = choiceParameterSpecificationQ;
		choiceParameterSpecificationQ = newChoiceParameterSpecificationQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.EXCEPTION_HANDLER__CHOICE_PARAMETER_SPECIFICATION_Q, oldChoiceParameterSpecificationQ, newChoiceParameterSpecificationQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChoiceParameterSpecificationQ(DeclarationClass newChoiceParameterSpecificationQ) {
		if (newChoiceParameterSpecificationQ != choiceParameterSpecificationQ) {
			NotificationChain msgs = null;
			if (choiceParameterSpecificationQ != null)
				msgs = ((InternalEObject)choiceParameterSpecificationQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXCEPTION_HANDLER__CHOICE_PARAMETER_SPECIFICATION_Q, null, msgs);
			if (newChoiceParameterSpecificationQ != null)
				msgs = ((InternalEObject)newChoiceParameterSpecificationQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXCEPTION_HANDLER__CHOICE_PARAMETER_SPECIFICATION_Q, null, msgs);
			msgs = basicSetChoiceParameterSpecificationQ(newChoiceParameterSpecificationQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.EXCEPTION_HANDLER__CHOICE_PARAMETER_SPECIFICATION_Q, newChoiceParameterSpecificationQ, newChoiceParameterSpecificationQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementList getExceptionChoicesQl() {
		return exceptionChoicesQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExceptionChoicesQl(ElementList newExceptionChoicesQl, NotificationChain msgs) {
		ElementList oldExceptionChoicesQl = exceptionChoicesQl;
		exceptionChoicesQl = newExceptionChoicesQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.EXCEPTION_HANDLER__EXCEPTION_CHOICES_QL, oldExceptionChoicesQl, newExceptionChoicesQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExceptionChoicesQl(ElementList newExceptionChoicesQl) {
		if (newExceptionChoicesQl != exceptionChoicesQl) {
			NotificationChain msgs = null;
			if (exceptionChoicesQl != null)
				msgs = ((InternalEObject)exceptionChoicesQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXCEPTION_HANDLER__EXCEPTION_CHOICES_QL, null, msgs);
			if (newExceptionChoicesQl != null)
				msgs = ((InternalEObject)newExceptionChoicesQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXCEPTION_HANDLER__EXCEPTION_CHOICES_QL, null, msgs);
			msgs = basicSetExceptionChoicesQl(newExceptionChoicesQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.EXCEPTION_HANDLER__EXCEPTION_CHOICES_QL, newExceptionChoicesQl, newExceptionChoicesQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatementList getHandlerStatementsQl() {
		return handlerStatementsQl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHandlerStatementsQl(StatementList newHandlerStatementsQl, NotificationChain msgs) {
		StatementList oldHandlerStatementsQl = handlerStatementsQl;
		handlerStatementsQl = newHandlerStatementsQl;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.EXCEPTION_HANDLER__HANDLER_STATEMENTS_QL, oldHandlerStatementsQl, newHandlerStatementsQl);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHandlerStatementsQl(StatementList newHandlerStatementsQl) {
		if (newHandlerStatementsQl != handlerStatementsQl) {
			NotificationChain msgs = null;
			if (handlerStatementsQl != null)
				msgs = ((InternalEObject)handlerStatementsQl).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXCEPTION_HANDLER__HANDLER_STATEMENTS_QL, null, msgs);
			if (newHandlerStatementsQl != null)
				msgs = ((InternalEObject)newHandlerStatementsQl).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.EXCEPTION_HANDLER__HANDLER_STATEMENTS_QL, null, msgs);
			msgs = basicSetHandlerStatementsQl(newHandlerStatementsQl, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.EXCEPTION_HANDLER__HANDLER_STATEMENTS_QL, newHandlerStatementsQl, newHandlerStatementsQl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.EXCEPTION_HANDLER__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.EXCEPTION_HANDLER__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.EXCEPTION_HANDLER__CHOICE_PARAMETER_SPECIFICATION_Q:
				return basicSetChoiceParameterSpecificationQ(null, msgs);
			case AdaPackage.EXCEPTION_HANDLER__EXCEPTION_CHOICES_QL:
				return basicSetExceptionChoicesQl(null, msgs);
			case AdaPackage.EXCEPTION_HANDLER__HANDLER_STATEMENTS_QL:
				return basicSetHandlerStatementsQl(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.EXCEPTION_HANDLER__SLOC:
				return getSloc();
			case AdaPackage.EXCEPTION_HANDLER__CHOICE_PARAMETER_SPECIFICATION_Q:
				return getChoiceParameterSpecificationQ();
			case AdaPackage.EXCEPTION_HANDLER__EXCEPTION_CHOICES_QL:
				return getExceptionChoicesQl();
			case AdaPackage.EXCEPTION_HANDLER__HANDLER_STATEMENTS_QL:
				return getHandlerStatementsQl();
			case AdaPackage.EXCEPTION_HANDLER__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.EXCEPTION_HANDLER__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.EXCEPTION_HANDLER__CHOICE_PARAMETER_SPECIFICATION_Q:
				setChoiceParameterSpecificationQ((DeclarationClass)newValue);
				return;
			case AdaPackage.EXCEPTION_HANDLER__EXCEPTION_CHOICES_QL:
				setExceptionChoicesQl((ElementList)newValue);
				return;
			case AdaPackage.EXCEPTION_HANDLER__HANDLER_STATEMENTS_QL:
				setHandlerStatementsQl((StatementList)newValue);
				return;
			case AdaPackage.EXCEPTION_HANDLER__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.EXCEPTION_HANDLER__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.EXCEPTION_HANDLER__CHOICE_PARAMETER_SPECIFICATION_Q:
				setChoiceParameterSpecificationQ((DeclarationClass)null);
				return;
			case AdaPackage.EXCEPTION_HANDLER__EXCEPTION_CHOICES_QL:
				setExceptionChoicesQl((ElementList)null);
				return;
			case AdaPackage.EXCEPTION_HANDLER__HANDLER_STATEMENTS_QL:
				setHandlerStatementsQl((StatementList)null);
				return;
			case AdaPackage.EXCEPTION_HANDLER__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.EXCEPTION_HANDLER__SLOC:
				return sloc != null;
			case AdaPackage.EXCEPTION_HANDLER__CHOICE_PARAMETER_SPECIFICATION_Q:
				return choiceParameterSpecificationQ != null;
			case AdaPackage.EXCEPTION_HANDLER__EXCEPTION_CHOICES_QL:
				return exceptionChoicesQl != null;
			case AdaPackage.EXCEPTION_HANDLER__HANDLER_STATEMENTS_QL:
				return handlerStatementsQl != null;
			case AdaPackage.EXCEPTION_HANDLER__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //ExceptionHandlerImpl
