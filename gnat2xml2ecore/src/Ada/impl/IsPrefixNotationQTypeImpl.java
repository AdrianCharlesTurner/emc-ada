/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.IsPrefixNotation;
import Ada.IsPrefixNotationQType;
import Ada.NotAnElement;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Is Prefix Notation QType</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.IsPrefixNotationQTypeImpl#getIsPrefixNotation <em>Is Prefix Notation</em>}</li>
 *   <li>{@link Ada.impl.IsPrefixNotationQTypeImpl#getNotAnElement <em>Not An Element</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IsPrefixNotationQTypeImpl extends MinimalEObjectImpl.Container implements IsPrefixNotationQType {
	/**
	 * The cached value of the '{@link #getIsPrefixNotation() <em>Is Prefix Notation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsPrefixNotation()
	 * @generated
	 * @ordered
	 */
	protected IsPrefixNotation isPrefixNotation;

	/**
	 * The cached value of the '{@link #getNotAnElement() <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotAnElement()
	 * @generated
	 * @ordered
	 */
	protected NotAnElement notAnElement;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IsPrefixNotationQTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getIsPrefixNotationQType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsPrefixNotation getIsPrefixNotation() {
		return isPrefixNotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIsPrefixNotation(IsPrefixNotation newIsPrefixNotation, NotificationChain msgs) {
		IsPrefixNotation oldIsPrefixNotation = isPrefixNotation;
		isPrefixNotation = newIsPrefixNotation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.IS_PREFIX_NOTATION_QTYPE__IS_PREFIX_NOTATION, oldIsPrefixNotation, newIsPrefixNotation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsPrefixNotation(IsPrefixNotation newIsPrefixNotation) {
		if (newIsPrefixNotation != isPrefixNotation) {
			NotificationChain msgs = null;
			if (isPrefixNotation != null)
				msgs = ((InternalEObject)isPrefixNotation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.IS_PREFIX_NOTATION_QTYPE__IS_PREFIX_NOTATION, null, msgs);
			if (newIsPrefixNotation != null)
				msgs = ((InternalEObject)newIsPrefixNotation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.IS_PREFIX_NOTATION_QTYPE__IS_PREFIX_NOTATION, null, msgs);
			msgs = basicSetIsPrefixNotation(newIsPrefixNotation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.IS_PREFIX_NOTATION_QTYPE__IS_PREFIX_NOTATION, newIsPrefixNotation, newIsPrefixNotation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotAnElement getNotAnElement() {
		return notAnElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNotAnElement(NotAnElement newNotAnElement, NotificationChain msgs) {
		NotAnElement oldNotAnElement = notAnElement;
		notAnElement = newNotAnElement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.IS_PREFIX_NOTATION_QTYPE__NOT_AN_ELEMENT, oldNotAnElement, newNotAnElement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotAnElement(NotAnElement newNotAnElement) {
		if (newNotAnElement != notAnElement) {
			NotificationChain msgs = null;
			if (notAnElement != null)
				msgs = ((InternalEObject)notAnElement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.IS_PREFIX_NOTATION_QTYPE__NOT_AN_ELEMENT, null, msgs);
			if (newNotAnElement != null)
				msgs = ((InternalEObject)newNotAnElement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.IS_PREFIX_NOTATION_QTYPE__NOT_AN_ELEMENT, null, msgs);
			msgs = basicSetNotAnElement(newNotAnElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.IS_PREFIX_NOTATION_QTYPE__NOT_AN_ELEMENT, newNotAnElement, newNotAnElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.IS_PREFIX_NOTATION_QTYPE__IS_PREFIX_NOTATION:
				return basicSetIsPrefixNotation(null, msgs);
			case AdaPackage.IS_PREFIX_NOTATION_QTYPE__NOT_AN_ELEMENT:
				return basicSetNotAnElement(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.IS_PREFIX_NOTATION_QTYPE__IS_PREFIX_NOTATION:
				return getIsPrefixNotation();
			case AdaPackage.IS_PREFIX_NOTATION_QTYPE__NOT_AN_ELEMENT:
				return getNotAnElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.IS_PREFIX_NOTATION_QTYPE__IS_PREFIX_NOTATION:
				setIsPrefixNotation((IsPrefixNotation)newValue);
				return;
			case AdaPackage.IS_PREFIX_NOTATION_QTYPE__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.IS_PREFIX_NOTATION_QTYPE__IS_PREFIX_NOTATION:
				setIsPrefixNotation((IsPrefixNotation)null);
				return;
			case AdaPackage.IS_PREFIX_NOTATION_QTYPE__NOT_AN_ELEMENT:
				setNotAnElement((NotAnElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.IS_PREFIX_NOTATION_QTYPE__IS_PREFIX_NOTATION:
				return isPrefixNotation != null;
			case AdaPackage.IS_PREFIX_NOTATION_QTYPE__NOT_AN_ELEMENT:
				return notAnElement != null;
		}
		return super.eIsSet(featureID);
	}

} //IsPrefixNotationQTypeImpl
