/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Loop Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.LoopStatement#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.LoopStatement#getLabelNamesQl <em>Label Names Ql</em>}</li>
 *   <li>{@link Ada.LoopStatement#getStatementIdentifierQ <em>Statement Identifier Q</em>}</li>
 *   <li>{@link Ada.LoopStatement#getLoopStatementsQl <em>Loop Statements Ql</em>}</li>
 *   <li>{@link Ada.LoopStatement#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getLoopStatement()
 * @model extendedMetaData="name='Loop_Statement' kind='elementOnly'"
 * @generated
 */
public interface LoopStatement extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getLoopStatement_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.LoopStatement#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Label Names Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label Names Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label Names Ql</em>' containment reference.
	 * @see #setLabelNamesQl(DefiningNameList)
	 * @see Ada.AdaPackage#getLoopStatement_LabelNamesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='label_names_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameList getLabelNamesQl();

	/**
	 * Sets the value of the '{@link Ada.LoopStatement#getLabelNamesQl <em>Label Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label Names Ql</em>' containment reference.
	 * @see #getLabelNamesQl()
	 * @generated
	 */
	void setLabelNamesQl(DefiningNameList value);

	/**
	 * Returns the value of the '<em><b>Statement Identifier Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Statement Identifier Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Statement Identifier Q</em>' containment reference.
	 * @see #setStatementIdentifierQ(DefiningNameClass)
	 * @see Ada.AdaPackage#getLoopStatement_StatementIdentifierQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='statement_identifier_q' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameClass getStatementIdentifierQ();

	/**
	 * Sets the value of the '{@link Ada.LoopStatement#getStatementIdentifierQ <em>Statement Identifier Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Statement Identifier Q</em>' containment reference.
	 * @see #getStatementIdentifierQ()
	 * @generated
	 */
	void setStatementIdentifierQ(DefiningNameClass value);

	/**
	 * Returns the value of the '<em><b>Loop Statements Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Loop Statements Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Loop Statements Ql</em>' containment reference.
	 * @see #setLoopStatementsQl(StatementList)
	 * @see Ada.AdaPackage#getLoopStatement_LoopStatementsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='loop_statements_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	StatementList getLoopStatementsQl();

	/**
	 * Sets the value of the '{@link Ada.LoopStatement#getLoopStatementsQl <em>Loop Statements Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Loop Statements Ql</em>' containment reference.
	 * @see #getLoopStatementsQl()
	 * @generated
	 */
	void setLoopStatementsQl(StatementList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getLoopStatement_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.LoopStatement#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // LoopStatement
