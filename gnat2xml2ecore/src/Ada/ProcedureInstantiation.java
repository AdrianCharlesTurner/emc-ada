/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Procedure Instantiation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.ProcedureInstantiation#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.ProcedureInstantiation#getIsOverridingDeclarationQ <em>Is Overriding Declaration Q</em>}</li>
 *   <li>{@link Ada.ProcedureInstantiation#getIsNotOverridingDeclarationQ <em>Is Not Overriding Declaration Q</em>}</li>
 *   <li>{@link Ada.ProcedureInstantiation#getNamesQl <em>Names Ql</em>}</li>
 *   <li>{@link Ada.ProcedureInstantiation#getGenericUnitNameQ <em>Generic Unit Name Q</em>}</li>
 *   <li>{@link Ada.ProcedureInstantiation#getGenericActualPartQl <em>Generic Actual Part Ql</em>}</li>
 *   <li>{@link Ada.ProcedureInstantiation#getAspectSpecificationsQl <em>Aspect Specifications Ql</em>}</li>
 *   <li>{@link Ada.ProcedureInstantiation#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getProcedureInstantiation()
 * @model extendedMetaData="name='Procedure_Instantiation' kind='elementOnly'"
 * @generated
 */
public interface ProcedureInstantiation extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getProcedureInstantiation_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.ProcedureInstantiation#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Is Overriding Declaration Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Overriding Declaration Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Overriding Declaration Q</em>' containment reference.
	 * @see #setIsOverridingDeclarationQ(IsOverridingDeclarationQType9)
	 * @see Ada.AdaPackage#getProcedureInstantiation_IsOverridingDeclarationQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='is_overriding_declaration_q' namespace='##targetNamespace'"
	 * @generated
	 */
	IsOverridingDeclarationQType9 getIsOverridingDeclarationQ();

	/**
	 * Sets the value of the '{@link Ada.ProcedureInstantiation#getIsOverridingDeclarationQ <em>Is Overriding Declaration Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Overriding Declaration Q</em>' containment reference.
	 * @see #getIsOverridingDeclarationQ()
	 * @generated
	 */
	void setIsOverridingDeclarationQ(IsOverridingDeclarationQType9 value);

	/**
	 * Returns the value of the '<em><b>Is Not Overriding Declaration Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Not Overriding Declaration Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Not Overriding Declaration Q</em>' containment reference.
	 * @see #setIsNotOverridingDeclarationQ(IsNotOverridingDeclarationQType6)
	 * @see Ada.AdaPackage#getProcedureInstantiation_IsNotOverridingDeclarationQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='is_not_overriding_declaration_q' namespace='##targetNamespace'"
	 * @generated
	 */
	IsNotOverridingDeclarationQType6 getIsNotOverridingDeclarationQ();

	/**
	 * Sets the value of the '{@link Ada.ProcedureInstantiation#getIsNotOverridingDeclarationQ <em>Is Not Overriding Declaration Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Not Overriding Declaration Q</em>' containment reference.
	 * @see #getIsNotOverridingDeclarationQ()
	 * @generated
	 */
	void setIsNotOverridingDeclarationQ(IsNotOverridingDeclarationQType6 value);

	/**
	 * Returns the value of the '<em><b>Names Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Names Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Names Ql</em>' containment reference.
	 * @see #setNamesQl(DefiningNameList)
	 * @see Ada.AdaPackage#getProcedureInstantiation_NamesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='names_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	DefiningNameList getNamesQl();

	/**
	 * Sets the value of the '{@link Ada.ProcedureInstantiation#getNamesQl <em>Names Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Names Ql</em>' containment reference.
	 * @see #getNamesQl()
	 * @generated
	 */
	void setNamesQl(DefiningNameList value);

	/**
	 * Returns the value of the '<em><b>Generic Unit Name Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Unit Name Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Unit Name Q</em>' containment reference.
	 * @see #setGenericUnitNameQ(ExpressionClass)
	 * @see Ada.AdaPackage#getProcedureInstantiation_GenericUnitNameQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='generic_unit_name_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getGenericUnitNameQ();

	/**
	 * Sets the value of the '{@link Ada.ProcedureInstantiation#getGenericUnitNameQ <em>Generic Unit Name Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generic Unit Name Q</em>' containment reference.
	 * @see #getGenericUnitNameQ()
	 * @generated
	 */
	void setGenericUnitNameQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Generic Actual Part Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Actual Part Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Actual Part Ql</em>' containment reference.
	 * @see #setGenericActualPartQl(AssociationList)
	 * @see Ada.AdaPackage#getProcedureInstantiation_GenericActualPartQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='generic_actual_part_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	AssociationList getGenericActualPartQl();

	/**
	 * Sets the value of the '{@link Ada.ProcedureInstantiation#getGenericActualPartQl <em>Generic Actual Part Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generic Actual Part Ql</em>' containment reference.
	 * @see #getGenericActualPartQl()
	 * @generated
	 */
	void setGenericActualPartQl(AssociationList value);

	/**
	 * Returns the value of the '<em><b>Aspect Specifications Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Aspect Specifications Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Aspect Specifications Ql</em>' containment reference.
	 * @see #setAspectSpecificationsQl(ElementList)
	 * @see Ada.AdaPackage#getProcedureInstantiation_AspectSpecificationsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='aspect_specifications_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementList getAspectSpecificationsQl();

	/**
	 * Sets the value of the '{@link Ada.ProcedureInstantiation#getAspectSpecificationsQl <em>Aspect Specifications Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Aspect Specifications Ql</em>' containment reference.
	 * @see #getAspectSpecificationsQl()
	 * @generated
	 */
	void setAspectSpecificationsQl(ElementList value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getProcedureInstantiation_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.ProcedureInstantiation#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // ProcedureInstantiation
