/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>And Then Short Circuit</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.AndThenShortCircuit#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.AndThenShortCircuit#getShortCircuitOperationLeftExpressionQ <em>Short Circuit Operation Left Expression Q</em>}</li>
 *   <li>{@link Ada.AndThenShortCircuit#getShortCircuitOperationRightExpressionQ <em>Short Circuit Operation Right Expression Q</em>}</li>
 *   <li>{@link Ada.AndThenShortCircuit#getChecks <em>Checks</em>}</li>
 *   <li>{@link Ada.AndThenShortCircuit#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getAndThenShortCircuit()
 * @model extendedMetaData="name='And_Then_Short_Circuit' kind='elementOnly'"
 * @generated
 */
public interface AndThenShortCircuit extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getAndThenShortCircuit_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.AndThenShortCircuit#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Short Circuit Operation Left Expression Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Short Circuit Operation Left Expression Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Short Circuit Operation Left Expression Q</em>' containment reference.
	 * @see #setShortCircuitOperationLeftExpressionQ(ExpressionClass)
	 * @see Ada.AdaPackage#getAndThenShortCircuit_ShortCircuitOperationLeftExpressionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='short_circuit_operation_left_expression_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getShortCircuitOperationLeftExpressionQ();

	/**
	 * Sets the value of the '{@link Ada.AndThenShortCircuit#getShortCircuitOperationLeftExpressionQ <em>Short Circuit Operation Left Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Short Circuit Operation Left Expression Q</em>' containment reference.
	 * @see #getShortCircuitOperationLeftExpressionQ()
	 * @generated
	 */
	void setShortCircuitOperationLeftExpressionQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Short Circuit Operation Right Expression Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Short Circuit Operation Right Expression Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Short Circuit Operation Right Expression Q</em>' containment reference.
	 * @see #setShortCircuitOperationRightExpressionQ(ExpressionClass)
	 * @see Ada.AdaPackage#getAndThenShortCircuit_ShortCircuitOperationRightExpressionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='short_circuit_operation_right_expression_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getShortCircuitOperationRightExpressionQ();

	/**
	 * Sets the value of the '{@link Ada.AndThenShortCircuit#getShortCircuitOperationRightExpressionQ <em>Short Circuit Operation Right Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Short Circuit Operation Right Expression Q</em>' containment reference.
	 * @see #getShortCircuitOperationRightExpressionQ()
	 * @generated
	 */
	void setShortCircuitOperationRightExpressionQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getAndThenShortCircuit_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.AndThenShortCircuit#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see Ada.AdaPackage#getAndThenShortCircuit_Type()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='type' namespace='##targetNamespace'"
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link Ada.AndThenShortCircuit#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

} // AndThenShortCircuit
