/**
 */
package Ada;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Declarative Item List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.DeclarativeItemList#getGroup <em>Group</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getNotAnElement <em>Not An Element</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getOrdinaryTypeDeclaration <em>Ordinary Type Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getTaskTypeDeclaration <em>Task Type Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getProtectedTypeDeclaration <em>Protected Type Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getIncompleteTypeDeclaration <em>Incomplete Type Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getTaggedIncompleteTypeDeclaration <em>Tagged Incomplete Type Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getPrivateTypeDeclaration <em>Private Type Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getPrivateExtensionDeclaration <em>Private Extension Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getSubtypeDeclaration <em>Subtype Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getVariableDeclaration <em>Variable Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getConstantDeclaration <em>Constant Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getDeferredConstantDeclaration <em>Deferred Constant Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getSingleTaskDeclaration <em>Single Task Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getSingleProtectedDeclaration <em>Single Protected Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getIntegerNumberDeclaration <em>Integer Number Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getRealNumberDeclaration <em>Real Number Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getEnumerationLiteralSpecification <em>Enumeration Literal Specification</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getDiscriminantSpecification <em>Discriminant Specification</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getComponentDeclaration <em>Component Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getLoopParameterSpecification <em>Loop Parameter Specification</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getGeneralizedIteratorSpecification <em>Generalized Iterator Specification</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getElementIteratorSpecification <em>Element Iterator Specification</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getProcedureDeclaration <em>Procedure Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getFunctionDeclaration <em>Function Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getParameterSpecification <em>Parameter Specification</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getProcedureBodyDeclaration <em>Procedure Body Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getFunctionBodyDeclaration <em>Function Body Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getReturnVariableSpecification <em>Return Variable Specification</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getReturnConstantSpecification <em>Return Constant Specification</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getNullProcedureDeclaration <em>Null Procedure Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getExpressionFunctionDeclaration <em>Expression Function Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getPackageDeclaration <em>Package Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getPackageBodyDeclaration <em>Package Body Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getObjectRenamingDeclaration <em>Object Renaming Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getExceptionRenamingDeclaration <em>Exception Renaming Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getPackageRenamingDeclaration <em>Package Renaming Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getProcedureRenamingDeclaration <em>Procedure Renaming Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getFunctionRenamingDeclaration <em>Function Renaming Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getGenericPackageRenamingDeclaration <em>Generic Package Renaming Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getGenericProcedureRenamingDeclaration <em>Generic Procedure Renaming Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getGenericFunctionRenamingDeclaration <em>Generic Function Renaming Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getTaskBodyDeclaration <em>Task Body Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getProtectedBodyDeclaration <em>Protected Body Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getEntryDeclaration <em>Entry Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getEntryBodyDeclaration <em>Entry Body Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getEntryIndexSpecification <em>Entry Index Specification</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getProcedureBodyStub <em>Procedure Body Stub</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getFunctionBodyStub <em>Function Body Stub</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getPackageBodyStub <em>Package Body Stub</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getTaskBodyStub <em>Task Body Stub</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getProtectedBodyStub <em>Protected Body Stub</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getExceptionDeclaration <em>Exception Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getChoiceParameterSpecification <em>Choice Parameter Specification</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getGenericProcedureDeclaration <em>Generic Procedure Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getGenericFunctionDeclaration <em>Generic Function Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getGenericPackageDeclaration <em>Generic Package Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getPackageInstantiation <em>Package Instantiation</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getProcedureInstantiation <em>Procedure Instantiation</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getFunctionInstantiation <em>Function Instantiation</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getFormalObjectDeclaration <em>Formal Object Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getFormalTypeDeclaration <em>Formal Type Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getFormalIncompleteTypeDeclaration <em>Formal Incomplete Type Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getFormalProcedureDeclaration <em>Formal Procedure Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getFormalFunctionDeclaration <em>Formal Function Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getFormalPackageDeclaration <em>Formal Package Declaration</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getFormalPackageDeclarationWithBox <em>Formal Package Declaration With Box</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getUsePackageClause <em>Use Package Clause</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getUseTypeClause <em>Use Type Clause</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getUseAllTypeClause <em>Use All Type Clause</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getWithClause <em>With Clause</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getAttributeDefinitionClause <em>Attribute Definition Clause</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getEnumerationRepresentationClause <em>Enumeration Representation Clause</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getRecordRepresentationClause <em>Record Representation Clause</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getAtClause <em>At Clause</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getComponentClause <em>Component Clause</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getComment <em>Comment</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getAsynchronousPragma <em>Asynchronous Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getAtomicPragma <em>Atomic Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getAttachHandlerPragma <em>Attach Handler Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getControlledPragma <em>Controlled Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getConventionPragma <em>Convention Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getDiscardNamesPragma <em>Discard Names Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getElaboratePragma <em>Elaborate Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getElaborateAllPragma <em>Elaborate All Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getExportPragma <em>Export Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getImportPragma <em>Import Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getInlinePragma <em>Inline Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getInspectionPointPragma <em>Inspection Point Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getLinkerOptionsPragma <em>Linker Options Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getListPragma <em>List Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getLockingPolicyPragma <em>Locking Policy Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getOptimizePragma <em>Optimize Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getPackPragma <em>Pack Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getPagePragma <em>Page Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getPreelaboratePragma <em>Preelaborate Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getPriorityPragma <em>Priority Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getPurePragma <em>Pure Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getRemoteTypesPragma <em>Remote Types Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getRestrictionsPragma <em>Restrictions Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getReviewablePragma <em>Reviewable Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getSharedPassivePragma <em>Shared Passive Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getStorageSizePragma <em>Storage Size Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getSuppressPragma <em>Suppress Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getVolatilePragma <em>Volatile Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getAssertPragma <em>Assert Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getNoReturnPragma <em>No Return Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getProfilePragma <em>Profile Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getUnsuppressPragma <em>Unsuppress Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getCpuPragma <em>Cpu Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getIndependentPragma <em>Independent Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getIndependentComponentsPragma <em>Independent Components Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}</li>
 *   <li>{@link Ada.DeclarativeItemList#getUnknownPragma <em>Unknown Pragma</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getDeclarativeItemList()
 * @model extendedMetaData="name='Declarative_Item_List' kind='elementOnly'"
 * @generated
 */
public interface DeclarativeItemList extends EObject {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' attribute list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_Group()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:0'"
	 * @generated
	 */
	FeatureMap getGroup();

	/**
	 * Returns the value of the '<em><b>Not An Element</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NotAnElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not An Element</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not An Element</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_NotAnElement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='not_an_element' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NotAnElement> getNotAnElement();

	/**
	 * Returns the value of the '<em><b>Ordinary Type Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OrdinaryTypeDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ordinary Type Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ordinary Type Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_OrdinaryTypeDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='ordinary_type_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OrdinaryTypeDeclaration> getOrdinaryTypeDeclaration();

	/**
	 * Returns the value of the '<em><b>Task Type Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TaskTypeDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Type Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Type Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_TaskTypeDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='task_type_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TaskTypeDeclaration> getTaskTypeDeclaration();

	/**
	 * Returns the value of the '<em><b>Protected Type Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProtectedTypeDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protected Type Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protected Type Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ProtectedTypeDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='protected_type_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProtectedTypeDeclaration> getProtectedTypeDeclaration();

	/**
	 * Returns the value of the '<em><b>Incomplete Type Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IncompleteTypeDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incomplete Type Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incomplete Type Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_IncompleteTypeDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='incomplete_type_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IncompleteTypeDeclaration> getIncompleteTypeDeclaration();

	/**
	 * Returns the value of the '<em><b>Tagged Incomplete Type Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TaggedIncompleteTypeDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tagged Incomplete Type Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tagged Incomplete Type Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_TaggedIncompleteTypeDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='tagged_incomplete_type_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TaggedIncompleteTypeDeclaration> getTaggedIncompleteTypeDeclaration();

	/**
	 * Returns the value of the '<em><b>Private Type Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PrivateTypeDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Private Type Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Private Type Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_PrivateTypeDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='private_type_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PrivateTypeDeclaration> getPrivateTypeDeclaration();

	/**
	 * Returns the value of the '<em><b>Private Extension Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PrivateExtensionDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Private Extension Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Private Extension Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_PrivateExtensionDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='private_extension_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PrivateExtensionDeclaration> getPrivateExtensionDeclaration();

	/**
	 * Returns the value of the '<em><b>Subtype Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SubtypeDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subtype Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subtype Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_SubtypeDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='subtype_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SubtypeDeclaration> getSubtypeDeclaration();

	/**
	 * Returns the value of the '<em><b>Variable Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.VariableDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variable Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variable Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_VariableDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='variable_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<VariableDeclaration> getVariableDeclaration();

	/**
	 * Returns the value of the '<em><b>Constant Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ConstantDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ConstantDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='constant_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ConstantDeclaration> getConstantDeclaration();

	/**
	 * Returns the value of the '<em><b>Deferred Constant Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DeferredConstantDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deferred Constant Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deferred Constant Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_DeferredConstantDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='deferred_constant_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DeferredConstantDeclaration> getDeferredConstantDeclaration();

	/**
	 * Returns the value of the '<em><b>Single Task Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SingleTaskDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Single Task Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Single Task Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_SingleTaskDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='single_task_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SingleTaskDeclaration> getSingleTaskDeclaration();

	/**
	 * Returns the value of the '<em><b>Single Protected Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SingleProtectedDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Single Protected Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Single Protected Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_SingleProtectedDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='single_protected_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SingleProtectedDeclaration> getSingleProtectedDeclaration();

	/**
	 * Returns the value of the '<em><b>Integer Number Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IntegerNumberDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integer Number Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integer Number Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_IntegerNumberDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='integer_number_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IntegerNumberDeclaration> getIntegerNumberDeclaration();

	/**
	 * Returns the value of the '<em><b>Real Number Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RealNumberDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Real Number Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Real Number Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_RealNumberDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='real_number_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RealNumberDeclaration> getRealNumberDeclaration();

	/**
	 * Returns the value of the '<em><b>Enumeration Literal Specification</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.EnumerationLiteralSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumeration Literal Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumeration Literal Specification</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_EnumerationLiteralSpecification()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='enumeration_literal_specification' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<EnumerationLiteralSpecification> getEnumerationLiteralSpecification();

	/**
	 * Returns the value of the '<em><b>Discriminant Specification</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscriminantSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discriminant Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discriminant Specification</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_DiscriminantSpecification()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discriminant_specification' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscriminantSpecification> getDiscriminantSpecification();

	/**
	 * Returns the value of the '<em><b>Component Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ComponentDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ComponentDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='component_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ComponentDeclaration> getComponentDeclaration();

	/**
	 * Returns the value of the '<em><b>Loop Parameter Specification</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LoopParameterSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Loop Parameter Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Loop Parameter Specification</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_LoopParameterSpecification()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='loop_parameter_specification' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LoopParameterSpecification> getLoopParameterSpecification();

	/**
	 * Returns the value of the '<em><b>Generalized Iterator Specification</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.GeneralizedIteratorSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generalized Iterator Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generalized Iterator Specification</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_GeneralizedIteratorSpecification()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='generalized_iterator_specification' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<GeneralizedIteratorSpecification> getGeneralizedIteratorSpecification();

	/**
	 * Returns the value of the '<em><b>Element Iterator Specification</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElementIteratorSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element Iterator Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Iterator Specification</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ElementIteratorSpecification()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='element_iterator_specification' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElementIteratorSpecification> getElementIteratorSpecification();

	/**
	 * Returns the value of the '<em><b>Procedure Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProcedureDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ProcedureDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='procedure_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProcedureDeclaration> getProcedureDeclaration();

	/**
	 * Returns the value of the '<em><b>Function Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FunctionDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_FunctionDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='function_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FunctionDeclaration> getFunctionDeclaration();

	/**
	 * Returns the value of the '<em><b>Parameter Specification</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ParameterSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Specification</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ParameterSpecification()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='parameter_specification' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ParameterSpecification> getParameterSpecification();

	/**
	 * Returns the value of the '<em><b>Procedure Body Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProcedureBodyDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure Body Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure Body Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ProcedureBodyDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='procedure_body_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProcedureBodyDeclaration> getProcedureBodyDeclaration();

	/**
	 * Returns the value of the '<em><b>Function Body Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FunctionBodyDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Body Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Body Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_FunctionBodyDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='function_body_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FunctionBodyDeclaration> getFunctionBodyDeclaration();

	/**
	 * Returns the value of the '<em><b>Return Variable Specification</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ReturnVariableSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Return Variable Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Return Variable Specification</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ReturnVariableSpecification()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='return_variable_specification' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ReturnVariableSpecification> getReturnVariableSpecification();

	/**
	 * Returns the value of the '<em><b>Return Constant Specification</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ReturnConstantSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Return Constant Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Return Constant Specification</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ReturnConstantSpecification()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='return_constant_specification' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ReturnConstantSpecification> getReturnConstantSpecification();

	/**
	 * Returns the value of the '<em><b>Null Procedure Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NullProcedureDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Null Procedure Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Null Procedure Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_NullProcedureDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='null_procedure_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NullProcedureDeclaration> getNullProcedureDeclaration();

	/**
	 * Returns the value of the '<em><b>Expression Function Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExpressionFunctionDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression Function Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression Function Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ExpressionFunctionDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='expression_function_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExpressionFunctionDeclaration> getExpressionFunctionDeclaration();

	/**
	 * Returns the value of the '<em><b>Package Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PackageDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_PackageDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='package_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PackageDeclaration> getPackageDeclaration();

	/**
	 * Returns the value of the '<em><b>Package Body Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PackageBodyDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Body Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Body Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_PackageBodyDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='package_body_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PackageBodyDeclaration> getPackageBodyDeclaration();

	/**
	 * Returns the value of the '<em><b>Object Renaming Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ObjectRenamingDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Renaming Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Renaming Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ObjectRenamingDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='object_renaming_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ObjectRenamingDeclaration> getObjectRenamingDeclaration();

	/**
	 * Returns the value of the '<em><b>Exception Renaming Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExceptionRenamingDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exception Renaming Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exception Renaming Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ExceptionRenamingDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='exception_renaming_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExceptionRenamingDeclaration> getExceptionRenamingDeclaration();

	/**
	 * Returns the value of the '<em><b>Package Renaming Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PackageRenamingDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Renaming Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Renaming Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_PackageRenamingDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='package_renaming_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PackageRenamingDeclaration> getPackageRenamingDeclaration();

	/**
	 * Returns the value of the '<em><b>Procedure Renaming Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProcedureRenamingDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure Renaming Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure Renaming Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ProcedureRenamingDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='procedure_renaming_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProcedureRenamingDeclaration> getProcedureRenamingDeclaration();

	/**
	 * Returns the value of the '<em><b>Function Renaming Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FunctionRenamingDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Renaming Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Renaming Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_FunctionRenamingDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='function_renaming_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FunctionRenamingDeclaration> getFunctionRenamingDeclaration();

	/**
	 * Returns the value of the '<em><b>Generic Package Renaming Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.GenericPackageRenamingDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Package Renaming Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Package Renaming Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_GenericPackageRenamingDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='generic_package_renaming_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<GenericPackageRenamingDeclaration> getGenericPackageRenamingDeclaration();

	/**
	 * Returns the value of the '<em><b>Generic Procedure Renaming Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.GenericProcedureRenamingDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Procedure Renaming Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Procedure Renaming Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_GenericProcedureRenamingDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='generic_procedure_renaming_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<GenericProcedureRenamingDeclaration> getGenericProcedureRenamingDeclaration();

	/**
	 * Returns the value of the '<em><b>Generic Function Renaming Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.GenericFunctionRenamingDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Function Renaming Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Function Renaming Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_GenericFunctionRenamingDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='generic_function_renaming_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<GenericFunctionRenamingDeclaration> getGenericFunctionRenamingDeclaration();

	/**
	 * Returns the value of the '<em><b>Task Body Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TaskBodyDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Body Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Body Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_TaskBodyDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='task_body_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TaskBodyDeclaration> getTaskBodyDeclaration();

	/**
	 * Returns the value of the '<em><b>Protected Body Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProtectedBodyDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protected Body Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protected Body Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ProtectedBodyDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='protected_body_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProtectedBodyDeclaration> getProtectedBodyDeclaration();

	/**
	 * Returns the value of the '<em><b>Entry Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.EntryDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_EntryDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='entry_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<EntryDeclaration> getEntryDeclaration();

	/**
	 * Returns the value of the '<em><b>Entry Body Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.EntryBodyDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry Body Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry Body Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_EntryBodyDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='entry_body_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<EntryBodyDeclaration> getEntryBodyDeclaration();

	/**
	 * Returns the value of the '<em><b>Entry Index Specification</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.EntryIndexSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entry Index Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entry Index Specification</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_EntryIndexSpecification()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='entry_index_specification' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<EntryIndexSpecification> getEntryIndexSpecification();

	/**
	 * Returns the value of the '<em><b>Procedure Body Stub</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProcedureBodyStub}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure Body Stub</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure Body Stub</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ProcedureBodyStub()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='procedure_body_stub' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProcedureBodyStub> getProcedureBodyStub();

	/**
	 * Returns the value of the '<em><b>Function Body Stub</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FunctionBodyStub}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Body Stub</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Body Stub</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_FunctionBodyStub()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='function_body_stub' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FunctionBodyStub> getFunctionBodyStub();

	/**
	 * Returns the value of the '<em><b>Package Body Stub</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PackageBodyStub}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Body Stub</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Body Stub</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_PackageBodyStub()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='package_body_stub' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PackageBodyStub> getPackageBodyStub();

	/**
	 * Returns the value of the '<em><b>Task Body Stub</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TaskBodyStub}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Body Stub</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Body Stub</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_TaskBodyStub()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='task_body_stub' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TaskBodyStub> getTaskBodyStub();

	/**
	 * Returns the value of the '<em><b>Protected Body Stub</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProtectedBodyStub}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Protected Body Stub</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protected Body Stub</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ProtectedBodyStub()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='protected_body_stub' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProtectedBodyStub> getProtectedBodyStub();

	/**
	 * Returns the value of the '<em><b>Exception Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExceptionDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exception Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exception Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ExceptionDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='exception_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExceptionDeclaration> getExceptionDeclaration();

	/**
	 * Returns the value of the '<em><b>Choice Parameter Specification</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ChoiceParameterSpecification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Choice Parameter Specification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Choice Parameter Specification</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ChoiceParameterSpecification()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='choice_parameter_specification' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ChoiceParameterSpecification> getChoiceParameterSpecification();

	/**
	 * Returns the value of the '<em><b>Generic Procedure Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.GenericProcedureDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Procedure Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Procedure Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_GenericProcedureDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='generic_procedure_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<GenericProcedureDeclaration> getGenericProcedureDeclaration();

	/**
	 * Returns the value of the '<em><b>Generic Function Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.GenericFunctionDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Function Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Function Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_GenericFunctionDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='generic_function_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<GenericFunctionDeclaration> getGenericFunctionDeclaration();

	/**
	 * Returns the value of the '<em><b>Generic Package Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.GenericPackageDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generic Package Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generic Package Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_GenericPackageDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='generic_package_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<GenericPackageDeclaration> getGenericPackageDeclaration();

	/**
	 * Returns the value of the '<em><b>Package Instantiation</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PackageInstantiation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Instantiation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Instantiation</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_PackageInstantiation()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='package_instantiation' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PackageInstantiation> getPackageInstantiation();

	/**
	 * Returns the value of the '<em><b>Procedure Instantiation</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProcedureInstantiation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Procedure Instantiation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Procedure Instantiation</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ProcedureInstantiation()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='procedure_instantiation' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProcedureInstantiation> getProcedureInstantiation();

	/**
	 * Returns the value of the '<em><b>Function Instantiation</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FunctionInstantiation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Instantiation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Instantiation</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_FunctionInstantiation()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='function_instantiation' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FunctionInstantiation> getFunctionInstantiation();

	/**
	 * Returns the value of the '<em><b>Formal Object Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalObjectDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Object Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Object Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_FormalObjectDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_object_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalObjectDeclaration> getFormalObjectDeclaration();

	/**
	 * Returns the value of the '<em><b>Formal Type Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalTypeDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Type Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Type Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_FormalTypeDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_type_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalTypeDeclaration> getFormalTypeDeclaration();

	/**
	 * Returns the value of the '<em><b>Formal Incomplete Type Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalIncompleteTypeDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Incomplete Type Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Incomplete Type Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_FormalIncompleteTypeDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_incomplete_type_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalIncompleteTypeDeclaration> getFormalIncompleteTypeDeclaration();

	/**
	 * Returns the value of the '<em><b>Formal Procedure Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalProcedureDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Procedure Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Procedure Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_FormalProcedureDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_procedure_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalProcedureDeclaration> getFormalProcedureDeclaration();

	/**
	 * Returns the value of the '<em><b>Formal Function Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalFunctionDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Function Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Function Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_FormalFunctionDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_function_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalFunctionDeclaration> getFormalFunctionDeclaration();

	/**
	 * Returns the value of the '<em><b>Formal Package Declaration</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalPackageDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Package Declaration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Package Declaration</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_FormalPackageDeclaration()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_package_declaration' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalPackageDeclaration> getFormalPackageDeclaration();

	/**
	 * Returns the value of the '<em><b>Formal Package Declaration With Box</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.FormalPackageDeclarationWithBox}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Package Declaration With Box</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Package Declaration With Box</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_FormalPackageDeclarationWithBox()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='formal_package_declaration_with_box' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<FormalPackageDeclarationWithBox> getFormalPackageDeclarationWithBox();

	/**
	 * Returns the value of the '<em><b>Use Package Clause</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UsePackageClause}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use Package Clause</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Package Clause</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_UsePackageClause()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='use_package_clause' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UsePackageClause> getUsePackageClause();

	/**
	 * Returns the value of the '<em><b>Use Type Clause</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UseTypeClause}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use Type Clause</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Type Clause</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_UseTypeClause()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='use_type_clause' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UseTypeClause> getUseTypeClause();

	/**
	 * Returns the value of the '<em><b>Use All Type Clause</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UseAllTypeClause}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use All Type Clause</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use All Type Clause</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_UseAllTypeClause()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='use_all_type_clause' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UseAllTypeClause> getUseAllTypeClause();

	/**
	 * Returns the value of the '<em><b>With Clause</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.WithClause}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>With Clause</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>With Clause</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_WithClause()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='with_clause' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<WithClause> getWithClause();

	/**
	 * Returns the value of the '<em><b>Attribute Definition Clause</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AttributeDefinitionClause}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute Definition Clause</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Definition Clause</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_AttributeDefinitionClause()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='attribute_definition_clause' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AttributeDefinitionClause> getAttributeDefinitionClause();

	/**
	 * Returns the value of the '<em><b>Enumeration Representation Clause</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.EnumerationRepresentationClause}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumeration Representation Clause</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumeration Representation Clause</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_EnumerationRepresentationClause()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='enumeration_representation_clause' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<EnumerationRepresentationClause> getEnumerationRepresentationClause();

	/**
	 * Returns the value of the '<em><b>Record Representation Clause</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RecordRepresentationClause}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Record Representation Clause</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Record Representation Clause</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_RecordRepresentationClause()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='record_representation_clause' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RecordRepresentationClause> getRecordRepresentationClause();

	/**
	 * Returns the value of the '<em><b>At Clause</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AtClause}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>At Clause</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>At Clause</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_AtClause()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='at_clause' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AtClause> getAtClause();

	/**
	 * Returns the value of the '<em><b>Component Clause</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ComponentClause}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Clause</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Clause</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ComponentClause()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='component_clause' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ComponentClause> getComponentClause();

	/**
	 * Returns the value of the '<em><b>Comment</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.Comment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comment</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comment</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_Comment()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='comment' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<Comment> getComment();

	/**
	 * Returns the value of the '<em><b>All Calls Remote Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AllCallsRemotePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Calls Remote Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Calls Remote Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_AllCallsRemotePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='all_calls_remote_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AllCallsRemotePragma> getAllCallsRemotePragma();

	/**
	 * Returns the value of the '<em><b>Asynchronous Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AsynchronousPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asynchronous Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asynchronous Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_AsynchronousPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='asynchronous_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AsynchronousPragma> getAsynchronousPragma();

	/**
	 * Returns the value of the '<em><b>Atomic Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AtomicPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_AtomicPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='atomic_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AtomicPragma> getAtomicPragma();

	/**
	 * Returns the value of the '<em><b>Atomic Components Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AtomicComponentsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Components Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Components Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_AtomicComponentsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='atomic_components_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AtomicComponentsPragma> getAtomicComponentsPragma();

	/**
	 * Returns the value of the '<em><b>Attach Handler Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AttachHandlerPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attach Handler Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attach Handler Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_AttachHandlerPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='attach_handler_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AttachHandlerPragma> getAttachHandlerPragma();

	/**
	 * Returns the value of the '<em><b>Controlled Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ControlledPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controlled Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controlled Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ControlledPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='controlled_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ControlledPragma> getControlledPragma();

	/**
	 * Returns the value of the '<em><b>Convention Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ConventionPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Convention Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Convention Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ConventionPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='convention_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ConventionPragma> getConventionPragma();

	/**
	 * Returns the value of the '<em><b>Discard Names Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DiscardNamesPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discard Names Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discard Names Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_DiscardNamesPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='discard_names_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DiscardNamesPragma> getDiscardNamesPragma();

	/**
	 * Returns the value of the '<em><b>Elaborate Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElaboratePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ElaboratePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='elaborate_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElaboratePragma> getElaboratePragma();

	/**
	 * Returns the value of the '<em><b>Elaborate All Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElaborateAllPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate All Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate All Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ElaborateAllPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='elaborate_all_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElaborateAllPragma> getElaborateAllPragma();

	/**
	 * Returns the value of the '<em><b>Elaborate Body Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ElaborateBodyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Body Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Body Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ElaborateBodyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='elaborate_body_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ElaborateBodyPragma> getElaborateBodyPragma();

	/**
	 * Returns the value of the '<em><b>Export Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ExportPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Export Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Export Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ExportPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='export_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ExportPragma> getExportPragma();

	/**
	 * Returns the value of the '<em><b>Import Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ImportPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Import Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Import Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ImportPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='import_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ImportPragma> getImportPragma();

	/**
	 * Returns the value of the '<em><b>Inline Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InlinePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inline Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inline Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_InlinePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='inline_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InlinePragma> getInlinePragma();

	/**
	 * Returns the value of the '<em><b>Inspection Point Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InspectionPointPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inspection Point Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inspection Point Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_InspectionPointPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='inspection_point_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InspectionPointPragma> getInspectionPointPragma();

	/**
	 * Returns the value of the '<em><b>Interrupt Handler Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InterruptHandlerPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Handler Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Handler Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_InterruptHandlerPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='interrupt_handler_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InterruptHandlerPragma> getInterruptHandlerPragma();

	/**
	 * Returns the value of the '<em><b>Interrupt Priority Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.InterruptPriorityPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Priority Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Priority Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_InterruptPriorityPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='interrupt_priority_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<InterruptPriorityPragma> getInterruptPriorityPragma();

	/**
	 * Returns the value of the '<em><b>Linker Options Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LinkerOptionsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Linker Options Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Linker Options Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_LinkerOptionsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='linker_options_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LinkerOptionsPragma> getLinkerOptionsPragma();

	/**
	 * Returns the value of the '<em><b>List Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ListPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ListPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='list_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ListPragma> getListPragma();

	/**
	 * Returns the value of the '<em><b>Locking Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.LockingPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Locking Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Locking Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_LockingPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='locking_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<LockingPolicyPragma> getLockingPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Normalize Scalars Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NormalizeScalarsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Normalize Scalars Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Normalize Scalars Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_NormalizeScalarsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='normalize_scalars_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NormalizeScalarsPragma> getNormalizeScalarsPragma();

	/**
	 * Returns the value of the '<em><b>Optimize Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.OptimizePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Optimize Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Optimize Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_OptimizePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='optimize_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<OptimizePragma> getOptimizePragma();

	/**
	 * Returns the value of the '<em><b>Pack Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PackPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pack Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pack Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_PackPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='pack_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PackPragma> getPackPragma();

	/**
	 * Returns the value of the '<em><b>Page Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PagePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Page Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Page Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_PagePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='page_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PagePragma> getPagePragma();

	/**
	 * Returns the value of the '<em><b>Preelaborate Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PreelaboratePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborate Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborate Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_PreelaboratePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='preelaborate_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PreelaboratePragma> getPreelaboratePragma();

	/**
	 * Returns the value of the '<em><b>Priority Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PriorityPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_PriorityPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='priority_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PriorityPragma> getPriorityPragma();

	/**
	 * Returns the value of the '<em><b>Pure Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PurePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pure Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pure Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_PurePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='pure_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PurePragma> getPurePragma();

	/**
	 * Returns the value of the '<em><b>Queuing Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.QueuingPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Queuing Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Queuing Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_QueuingPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='queuing_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<QueuingPolicyPragma> getQueuingPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Remote Call Interface Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RemoteCallInterfacePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Call Interface Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Call Interface Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_RemoteCallInterfacePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='remote_call_interface_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RemoteCallInterfacePragma> getRemoteCallInterfacePragma();

	/**
	 * Returns the value of the '<em><b>Remote Types Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RemoteTypesPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Types Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Types Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_RemoteTypesPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='remote_types_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RemoteTypesPragma> getRemoteTypesPragma();

	/**
	 * Returns the value of the '<em><b>Restrictions Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RestrictionsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Restrictions Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Restrictions Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_RestrictionsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='restrictions_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RestrictionsPragma> getRestrictionsPragma();

	/**
	 * Returns the value of the '<em><b>Reviewable Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ReviewablePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reviewable Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reviewable Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ReviewablePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='reviewable_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ReviewablePragma> getReviewablePragma();

	/**
	 * Returns the value of the '<em><b>Shared Passive Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SharedPassivePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shared Passive Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shared Passive Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_SharedPassivePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='shared_passive_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SharedPassivePragma> getSharedPassivePragma();

	/**
	 * Returns the value of the '<em><b>Storage Size Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.StorageSizePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Size Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Size Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_StorageSizePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='storage_size_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<StorageSizePragma> getStorageSizePragma();

	/**
	 * Returns the value of the '<em><b>Suppress Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.SuppressPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Suppress Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Suppress Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_SuppressPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='suppress_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<SuppressPragma> getSuppressPragma();

	/**
	 * Returns the value of the '<em><b>Task Dispatching Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.TaskDispatchingPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Dispatching Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Dispatching Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_TaskDispatchingPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='task_dispatching_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<TaskDispatchingPolicyPragma> getTaskDispatchingPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Volatile Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.VolatilePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_VolatilePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='volatile_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<VolatilePragma> getVolatilePragma();

	/**
	 * Returns the value of the '<em><b>Volatile Components Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.VolatileComponentsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Components Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Components Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_VolatileComponentsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='volatile_components_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<VolatileComponentsPragma> getVolatileComponentsPragma();

	/**
	 * Returns the value of the '<em><b>Assert Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AssertPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assert Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assert Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_AssertPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='assert_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AssertPragma> getAssertPragma();

	/**
	 * Returns the value of the '<em><b>Assertion Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.AssertionPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assertion Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assertion Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_AssertionPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='assertion_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<AssertionPolicyPragma> getAssertionPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Detect Blocking Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DetectBlockingPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Detect Blocking Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Detect Blocking Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_DetectBlockingPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='detect_blocking_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DetectBlockingPragma> getDetectBlockingPragma();

	/**
	 * Returns the value of the '<em><b>No Return Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.NoReturnPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>No Return Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>No Return Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_NoReturnPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='no_return_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<NoReturnPragma> getNoReturnPragma();

	/**
	 * Returns the value of the '<em><b>Partition Elaboration Policy Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PartitionElaborationPolicyPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partition Elaboration Policy Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partition Elaboration Policy Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_PartitionElaborationPolicyPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='partition_elaboration_policy_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PartitionElaborationPolicyPragma> getPartitionElaborationPolicyPragma();

	/**
	 * Returns the value of the '<em><b>Preelaborable Initialization Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PreelaborableInitializationPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborable Initialization Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborable Initialization Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_PreelaborableInitializationPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='preelaborable_initialization_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PreelaborableInitializationPragma> getPreelaborableInitializationPragma();

	/**
	 * Returns the value of the '<em><b>Priority Specific Dispatching Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.PrioritySpecificDispatchingPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Specific Dispatching Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Specific Dispatching Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_PrioritySpecificDispatchingPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='priority_specific_dispatching_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<PrioritySpecificDispatchingPragma> getPrioritySpecificDispatchingPragma();

	/**
	 * Returns the value of the '<em><b>Profile Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ProfilePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Profile Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Profile Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ProfilePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='profile_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ProfilePragma> getProfilePragma();

	/**
	 * Returns the value of the '<em><b>Relative Deadline Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.RelativeDeadlinePragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relative Deadline Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relative Deadline Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_RelativeDeadlinePragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='relative_deadline_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<RelativeDeadlinePragma> getRelativeDeadlinePragma();

	/**
	 * Returns the value of the '<em><b>Unchecked Union Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UncheckedUnionPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unchecked Union Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unchecked Union Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_UncheckedUnionPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unchecked_union_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UncheckedUnionPragma> getUncheckedUnionPragma();

	/**
	 * Returns the value of the '<em><b>Unsuppress Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnsuppressPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unsuppress Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unsuppress Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_UnsuppressPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unsuppress_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnsuppressPragma> getUnsuppressPragma();

	/**
	 * Returns the value of the '<em><b>Default Storage Pool Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DefaultStoragePoolPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Storage Pool Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Storage Pool Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_DefaultStoragePoolPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='default_storage_pool_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DefaultStoragePoolPragma> getDefaultStoragePoolPragma();

	/**
	 * Returns the value of the '<em><b>Dispatching Domain Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.DispatchingDomainPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dispatching Domain Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dispatching Domain Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_DispatchingDomainPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='dispatching_domain_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<DispatchingDomainPragma> getDispatchingDomainPragma();

	/**
	 * Returns the value of the '<em><b>Cpu Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.CpuPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cpu Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cpu Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_CpuPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='cpu_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<CpuPragma> getCpuPragma();

	/**
	 * Returns the value of the '<em><b>Independent Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IndependentPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_IndependentPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='independent_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IndependentPragma> getIndependentPragma();

	/**
	 * Returns the value of the '<em><b>Independent Components Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.IndependentComponentsPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Components Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Components Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_IndependentComponentsPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='independent_components_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<IndependentComponentsPragma> getIndependentComponentsPragma();

	/**
	 * Returns the value of the '<em><b>Implementation Defined Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.ImplementationDefinedPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implementation Defined Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implementation Defined Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_ImplementationDefinedPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='implementation_defined_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<ImplementationDefinedPragma> getImplementationDefinedPragma();

	/**
	 * Returns the value of the '<em><b>Unknown Pragma</b></em>' containment reference list.
	 * The list contents are of type {@link Ada.UnknownPragma}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unknown Pragma</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unknown Pragma</em>' containment reference list.
	 * @see Ada.AdaPackage#getDeclarativeItemList_UnknownPragma()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='unknown_pragma' namespace='##targetNamespace' group='group:0'"
	 * @generated
	 */
	EList<UnknownPragma> getUnknownPragma();

} // DeclarativeItemList
