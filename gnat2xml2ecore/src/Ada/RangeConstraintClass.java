/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Range Constraint Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.RangeConstraintClass#getNotAnElement <em>Not An Element</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getRangeAttributeReference <em>Range Attribute Reference</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getSimpleExpressionRange <em>Simple Expression Range</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getComment <em>Comment</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getAsynchronousPragma <em>Asynchronous Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getAtomicPragma <em>Atomic Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getAttachHandlerPragma <em>Attach Handler Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getControlledPragma <em>Controlled Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getConventionPragma <em>Convention Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getDiscardNamesPragma <em>Discard Names Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getElaboratePragma <em>Elaborate Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getElaborateAllPragma <em>Elaborate All Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getExportPragma <em>Export Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getImportPragma <em>Import Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getInlinePragma <em>Inline Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getInspectionPointPragma <em>Inspection Point Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getLinkerOptionsPragma <em>Linker Options Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getListPragma <em>List Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getLockingPolicyPragma <em>Locking Policy Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getOptimizePragma <em>Optimize Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getPackPragma <em>Pack Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getPagePragma <em>Page Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getPreelaboratePragma <em>Preelaborate Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getPriorityPragma <em>Priority Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getPurePragma <em>Pure Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getRemoteTypesPragma <em>Remote Types Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getRestrictionsPragma <em>Restrictions Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getReviewablePragma <em>Reviewable Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getSharedPassivePragma <em>Shared Passive Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getStorageSizePragma <em>Storage Size Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getSuppressPragma <em>Suppress Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getVolatilePragma <em>Volatile Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getAssertPragma <em>Assert Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getNoReturnPragma <em>No Return Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getProfilePragma <em>Profile Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getUnsuppressPragma <em>Unsuppress Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getCpuPragma <em>Cpu Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getIndependentPragma <em>Independent Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getIndependentComponentsPragma <em>Independent Components Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}</li>
 *   <li>{@link Ada.RangeConstraintClass#getUnknownPragma <em>Unknown Pragma</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getRangeConstraintClass()
 * @model extendedMetaData="name='Range_Constraint_Class' kind='elementOnly'"
 * @generated
 */
public interface RangeConstraintClass extends EObject {
	/**
	 * Returns the value of the '<em><b>Not An Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Not An Element</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Not An Element</em>' containment reference.
	 * @see #setNotAnElement(NotAnElement)
	 * @see Ada.AdaPackage#getRangeConstraintClass_NotAnElement()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='not_an_element' namespace='##targetNamespace'"
	 * @generated
	 */
	NotAnElement getNotAnElement();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getNotAnElement <em>Not An Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Not An Element</em>' containment reference.
	 * @see #getNotAnElement()
	 * @generated
	 */
	void setNotAnElement(NotAnElement value);

	/**
	 * Returns the value of the '<em><b>Range Attribute Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Range Attribute Reference</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range Attribute Reference</em>' containment reference.
	 * @see #setRangeAttributeReference(RangeAttributeReference)
	 * @see Ada.AdaPackage#getRangeConstraintClass_RangeAttributeReference()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='range_attribute_reference' namespace='##targetNamespace'"
	 * @generated
	 */
	RangeAttributeReference getRangeAttributeReference();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getRangeAttributeReference <em>Range Attribute Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Range Attribute Reference</em>' containment reference.
	 * @see #getRangeAttributeReference()
	 * @generated
	 */
	void setRangeAttributeReference(RangeAttributeReference value);

	/**
	 * Returns the value of the '<em><b>Simple Expression Range</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simple Expression Range</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simple Expression Range</em>' containment reference.
	 * @see #setSimpleExpressionRange(SimpleExpressionRange)
	 * @see Ada.AdaPackage#getRangeConstraintClass_SimpleExpressionRange()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='simple_expression_range' namespace='##targetNamespace'"
	 * @generated
	 */
	SimpleExpressionRange getSimpleExpressionRange();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getSimpleExpressionRange <em>Simple Expression Range</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Simple Expression Range</em>' containment reference.
	 * @see #getSimpleExpressionRange()
	 * @generated
	 */
	void setSimpleExpressionRange(SimpleExpressionRange value);

	/**
	 * Returns the value of the '<em><b>Comment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comment</em>' containment reference.
	 * @see #setComment(Comment)
	 * @see Ada.AdaPackage#getRangeConstraintClass_Comment()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='comment' namespace='##targetNamespace'"
	 * @generated
	 */
	Comment getComment();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getComment <em>Comment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comment</em>' containment reference.
	 * @see #getComment()
	 * @generated
	 */
	void setComment(Comment value);

	/**
	 * Returns the value of the '<em><b>All Calls Remote Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Calls Remote Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Calls Remote Pragma</em>' containment reference.
	 * @see #setAllCallsRemotePragma(AllCallsRemotePragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_AllCallsRemotePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='all_calls_remote_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AllCallsRemotePragma getAllCallsRemotePragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>All Calls Remote Pragma</em>' containment reference.
	 * @see #getAllCallsRemotePragma()
	 * @generated
	 */
	void setAllCallsRemotePragma(AllCallsRemotePragma value);

	/**
	 * Returns the value of the '<em><b>Asynchronous Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asynchronous Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asynchronous Pragma</em>' containment reference.
	 * @see #setAsynchronousPragma(AsynchronousPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_AsynchronousPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='asynchronous_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AsynchronousPragma getAsynchronousPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getAsynchronousPragma <em>Asynchronous Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Asynchronous Pragma</em>' containment reference.
	 * @see #getAsynchronousPragma()
	 * @generated
	 */
	void setAsynchronousPragma(AsynchronousPragma value);

	/**
	 * Returns the value of the '<em><b>Atomic Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Pragma</em>' containment reference.
	 * @see #setAtomicPragma(AtomicPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_AtomicPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='atomic_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AtomicPragma getAtomicPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getAtomicPragma <em>Atomic Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Atomic Pragma</em>' containment reference.
	 * @see #getAtomicPragma()
	 * @generated
	 */
	void setAtomicPragma(AtomicPragma value);

	/**
	 * Returns the value of the '<em><b>Atomic Components Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Components Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Components Pragma</em>' containment reference.
	 * @see #setAtomicComponentsPragma(AtomicComponentsPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_AtomicComponentsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='atomic_components_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AtomicComponentsPragma getAtomicComponentsPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Atomic Components Pragma</em>' containment reference.
	 * @see #getAtomicComponentsPragma()
	 * @generated
	 */
	void setAtomicComponentsPragma(AtomicComponentsPragma value);

	/**
	 * Returns the value of the '<em><b>Attach Handler Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attach Handler Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attach Handler Pragma</em>' containment reference.
	 * @see #setAttachHandlerPragma(AttachHandlerPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_AttachHandlerPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='attach_handler_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AttachHandlerPragma getAttachHandlerPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getAttachHandlerPragma <em>Attach Handler Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attach Handler Pragma</em>' containment reference.
	 * @see #getAttachHandlerPragma()
	 * @generated
	 */
	void setAttachHandlerPragma(AttachHandlerPragma value);

	/**
	 * Returns the value of the '<em><b>Controlled Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controlled Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controlled Pragma</em>' containment reference.
	 * @see #setControlledPragma(ControlledPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_ControlledPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='controlled_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ControlledPragma getControlledPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getControlledPragma <em>Controlled Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Controlled Pragma</em>' containment reference.
	 * @see #getControlledPragma()
	 * @generated
	 */
	void setControlledPragma(ControlledPragma value);

	/**
	 * Returns the value of the '<em><b>Convention Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Convention Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Convention Pragma</em>' containment reference.
	 * @see #setConventionPragma(ConventionPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_ConventionPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='convention_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ConventionPragma getConventionPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getConventionPragma <em>Convention Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Convention Pragma</em>' containment reference.
	 * @see #getConventionPragma()
	 * @generated
	 */
	void setConventionPragma(ConventionPragma value);

	/**
	 * Returns the value of the '<em><b>Discard Names Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Discard Names Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Discard Names Pragma</em>' containment reference.
	 * @see #setDiscardNamesPragma(DiscardNamesPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_DiscardNamesPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='discard_names_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	DiscardNamesPragma getDiscardNamesPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getDiscardNamesPragma <em>Discard Names Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Discard Names Pragma</em>' containment reference.
	 * @see #getDiscardNamesPragma()
	 * @generated
	 */
	void setDiscardNamesPragma(DiscardNamesPragma value);

	/**
	 * Returns the value of the '<em><b>Elaborate Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Pragma</em>' containment reference.
	 * @see #setElaboratePragma(ElaboratePragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_ElaboratePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='elaborate_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ElaboratePragma getElaboratePragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getElaboratePragma <em>Elaborate Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Elaborate Pragma</em>' containment reference.
	 * @see #getElaboratePragma()
	 * @generated
	 */
	void setElaboratePragma(ElaboratePragma value);

	/**
	 * Returns the value of the '<em><b>Elaborate All Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate All Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate All Pragma</em>' containment reference.
	 * @see #setElaborateAllPragma(ElaborateAllPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_ElaborateAllPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='elaborate_all_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ElaborateAllPragma getElaborateAllPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getElaborateAllPragma <em>Elaborate All Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Elaborate All Pragma</em>' containment reference.
	 * @see #getElaborateAllPragma()
	 * @generated
	 */
	void setElaborateAllPragma(ElaborateAllPragma value);

	/**
	 * Returns the value of the '<em><b>Elaborate Body Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elaborate Body Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elaborate Body Pragma</em>' containment reference.
	 * @see #setElaborateBodyPragma(ElaborateBodyPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_ElaborateBodyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='elaborate_body_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ElaborateBodyPragma getElaborateBodyPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Elaborate Body Pragma</em>' containment reference.
	 * @see #getElaborateBodyPragma()
	 * @generated
	 */
	void setElaborateBodyPragma(ElaborateBodyPragma value);

	/**
	 * Returns the value of the '<em><b>Export Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Export Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Export Pragma</em>' containment reference.
	 * @see #setExportPragma(ExportPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_ExportPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='export_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ExportPragma getExportPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getExportPragma <em>Export Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Export Pragma</em>' containment reference.
	 * @see #getExportPragma()
	 * @generated
	 */
	void setExportPragma(ExportPragma value);

	/**
	 * Returns the value of the '<em><b>Import Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Import Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Import Pragma</em>' containment reference.
	 * @see #setImportPragma(ImportPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_ImportPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='import_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ImportPragma getImportPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getImportPragma <em>Import Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Import Pragma</em>' containment reference.
	 * @see #getImportPragma()
	 * @generated
	 */
	void setImportPragma(ImportPragma value);

	/**
	 * Returns the value of the '<em><b>Inline Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inline Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inline Pragma</em>' containment reference.
	 * @see #setInlinePragma(InlinePragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_InlinePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='inline_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	InlinePragma getInlinePragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getInlinePragma <em>Inline Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inline Pragma</em>' containment reference.
	 * @see #getInlinePragma()
	 * @generated
	 */
	void setInlinePragma(InlinePragma value);

	/**
	 * Returns the value of the '<em><b>Inspection Point Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inspection Point Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inspection Point Pragma</em>' containment reference.
	 * @see #setInspectionPointPragma(InspectionPointPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_InspectionPointPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='inspection_point_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	InspectionPointPragma getInspectionPointPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getInspectionPointPragma <em>Inspection Point Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inspection Point Pragma</em>' containment reference.
	 * @see #getInspectionPointPragma()
	 * @generated
	 */
	void setInspectionPointPragma(InspectionPointPragma value);

	/**
	 * Returns the value of the '<em><b>Interrupt Handler Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Handler Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Handler Pragma</em>' containment reference.
	 * @see #setInterruptHandlerPragma(InterruptHandlerPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_InterruptHandlerPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='interrupt_handler_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	InterruptHandlerPragma getInterruptHandlerPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interrupt Handler Pragma</em>' containment reference.
	 * @see #getInterruptHandlerPragma()
	 * @generated
	 */
	void setInterruptHandlerPragma(InterruptHandlerPragma value);

	/**
	 * Returns the value of the '<em><b>Interrupt Priority Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt Priority Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt Priority Pragma</em>' containment reference.
	 * @see #setInterruptPriorityPragma(InterruptPriorityPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_InterruptPriorityPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='interrupt_priority_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	InterruptPriorityPragma getInterruptPriorityPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interrupt Priority Pragma</em>' containment reference.
	 * @see #getInterruptPriorityPragma()
	 * @generated
	 */
	void setInterruptPriorityPragma(InterruptPriorityPragma value);

	/**
	 * Returns the value of the '<em><b>Linker Options Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Linker Options Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Linker Options Pragma</em>' containment reference.
	 * @see #setLinkerOptionsPragma(LinkerOptionsPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_LinkerOptionsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='linker_options_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	LinkerOptionsPragma getLinkerOptionsPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getLinkerOptionsPragma <em>Linker Options Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Linker Options Pragma</em>' containment reference.
	 * @see #getLinkerOptionsPragma()
	 * @generated
	 */
	void setLinkerOptionsPragma(LinkerOptionsPragma value);

	/**
	 * Returns the value of the '<em><b>List Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List Pragma</em>' containment reference.
	 * @see #setListPragma(ListPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_ListPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='list_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ListPragma getListPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getListPragma <em>List Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>List Pragma</em>' containment reference.
	 * @see #getListPragma()
	 * @generated
	 */
	void setListPragma(ListPragma value);

	/**
	 * Returns the value of the '<em><b>Locking Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Locking Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Locking Policy Pragma</em>' containment reference.
	 * @see #setLockingPolicyPragma(LockingPolicyPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_LockingPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='locking_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	LockingPolicyPragma getLockingPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getLockingPolicyPragma <em>Locking Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Locking Policy Pragma</em>' containment reference.
	 * @see #getLockingPolicyPragma()
	 * @generated
	 */
	void setLockingPolicyPragma(LockingPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Normalize Scalars Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Normalize Scalars Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Normalize Scalars Pragma</em>' containment reference.
	 * @see #setNormalizeScalarsPragma(NormalizeScalarsPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_NormalizeScalarsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='normalize_scalars_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	NormalizeScalarsPragma getNormalizeScalarsPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Normalize Scalars Pragma</em>' containment reference.
	 * @see #getNormalizeScalarsPragma()
	 * @generated
	 */
	void setNormalizeScalarsPragma(NormalizeScalarsPragma value);

	/**
	 * Returns the value of the '<em><b>Optimize Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Optimize Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Optimize Pragma</em>' containment reference.
	 * @see #setOptimizePragma(OptimizePragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_OptimizePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='optimize_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	OptimizePragma getOptimizePragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getOptimizePragma <em>Optimize Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Optimize Pragma</em>' containment reference.
	 * @see #getOptimizePragma()
	 * @generated
	 */
	void setOptimizePragma(OptimizePragma value);

	/**
	 * Returns the value of the '<em><b>Pack Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pack Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pack Pragma</em>' containment reference.
	 * @see #setPackPragma(PackPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_PackPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='pack_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PackPragma getPackPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getPackPragma <em>Pack Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pack Pragma</em>' containment reference.
	 * @see #getPackPragma()
	 * @generated
	 */
	void setPackPragma(PackPragma value);

	/**
	 * Returns the value of the '<em><b>Page Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Page Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Page Pragma</em>' containment reference.
	 * @see #setPagePragma(PagePragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_PagePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='page_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PagePragma getPagePragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getPagePragma <em>Page Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Page Pragma</em>' containment reference.
	 * @see #getPagePragma()
	 * @generated
	 */
	void setPagePragma(PagePragma value);

	/**
	 * Returns the value of the '<em><b>Preelaborate Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborate Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborate Pragma</em>' containment reference.
	 * @see #setPreelaboratePragma(PreelaboratePragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_PreelaboratePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='preelaborate_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PreelaboratePragma getPreelaboratePragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getPreelaboratePragma <em>Preelaborate Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preelaborate Pragma</em>' containment reference.
	 * @see #getPreelaboratePragma()
	 * @generated
	 */
	void setPreelaboratePragma(PreelaboratePragma value);

	/**
	 * Returns the value of the '<em><b>Priority Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Pragma</em>' containment reference.
	 * @see #setPriorityPragma(PriorityPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_PriorityPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='priority_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PriorityPragma getPriorityPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getPriorityPragma <em>Priority Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Priority Pragma</em>' containment reference.
	 * @see #getPriorityPragma()
	 * @generated
	 */
	void setPriorityPragma(PriorityPragma value);

	/**
	 * Returns the value of the '<em><b>Pure Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pure Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pure Pragma</em>' containment reference.
	 * @see #setPurePragma(PurePragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_PurePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='pure_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PurePragma getPurePragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getPurePragma <em>Pure Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pure Pragma</em>' containment reference.
	 * @see #getPurePragma()
	 * @generated
	 */
	void setPurePragma(PurePragma value);

	/**
	 * Returns the value of the '<em><b>Queuing Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Queuing Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Queuing Policy Pragma</em>' containment reference.
	 * @see #setQueuingPolicyPragma(QueuingPolicyPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_QueuingPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='queuing_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	QueuingPolicyPragma getQueuingPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Queuing Policy Pragma</em>' containment reference.
	 * @see #getQueuingPolicyPragma()
	 * @generated
	 */
	void setQueuingPolicyPragma(QueuingPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Remote Call Interface Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Call Interface Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Call Interface Pragma</em>' containment reference.
	 * @see #setRemoteCallInterfacePragma(RemoteCallInterfacePragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_RemoteCallInterfacePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='remote_call_interface_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	RemoteCallInterfacePragma getRemoteCallInterfacePragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Remote Call Interface Pragma</em>' containment reference.
	 * @see #getRemoteCallInterfacePragma()
	 * @generated
	 */
	void setRemoteCallInterfacePragma(RemoteCallInterfacePragma value);

	/**
	 * Returns the value of the '<em><b>Remote Types Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Remote Types Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Remote Types Pragma</em>' containment reference.
	 * @see #setRemoteTypesPragma(RemoteTypesPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_RemoteTypesPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='remote_types_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	RemoteTypesPragma getRemoteTypesPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getRemoteTypesPragma <em>Remote Types Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Remote Types Pragma</em>' containment reference.
	 * @see #getRemoteTypesPragma()
	 * @generated
	 */
	void setRemoteTypesPragma(RemoteTypesPragma value);

	/**
	 * Returns the value of the '<em><b>Restrictions Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Restrictions Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Restrictions Pragma</em>' containment reference.
	 * @see #setRestrictionsPragma(RestrictionsPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_RestrictionsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='restrictions_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	RestrictionsPragma getRestrictionsPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getRestrictionsPragma <em>Restrictions Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Restrictions Pragma</em>' containment reference.
	 * @see #getRestrictionsPragma()
	 * @generated
	 */
	void setRestrictionsPragma(RestrictionsPragma value);

	/**
	 * Returns the value of the '<em><b>Reviewable Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reviewable Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reviewable Pragma</em>' containment reference.
	 * @see #setReviewablePragma(ReviewablePragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_ReviewablePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='reviewable_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ReviewablePragma getReviewablePragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getReviewablePragma <em>Reviewable Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reviewable Pragma</em>' containment reference.
	 * @see #getReviewablePragma()
	 * @generated
	 */
	void setReviewablePragma(ReviewablePragma value);

	/**
	 * Returns the value of the '<em><b>Shared Passive Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shared Passive Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shared Passive Pragma</em>' containment reference.
	 * @see #setSharedPassivePragma(SharedPassivePragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_SharedPassivePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='shared_passive_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	SharedPassivePragma getSharedPassivePragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getSharedPassivePragma <em>Shared Passive Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Shared Passive Pragma</em>' containment reference.
	 * @see #getSharedPassivePragma()
	 * @generated
	 */
	void setSharedPassivePragma(SharedPassivePragma value);

	/**
	 * Returns the value of the '<em><b>Storage Size Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Storage Size Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Storage Size Pragma</em>' containment reference.
	 * @see #setStorageSizePragma(StorageSizePragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_StorageSizePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='storage_size_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	StorageSizePragma getStorageSizePragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getStorageSizePragma <em>Storage Size Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Storage Size Pragma</em>' containment reference.
	 * @see #getStorageSizePragma()
	 * @generated
	 */
	void setStorageSizePragma(StorageSizePragma value);

	/**
	 * Returns the value of the '<em><b>Suppress Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Suppress Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Suppress Pragma</em>' containment reference.
	 * @see #setSuppressPragma(SuppressPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_SuppressPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='suppress_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	SuppressPragma getSuppressPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getSuppressPragma <em>Suppress Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Suppress Pragma</em>' containment reference.
	 * @see #getSuppressPragma()
	 * @generated
	 */
	void setSuppressPragma(SuppressPragma value);

	/**
	 * Returns the value of the '<em><b>Task Dispatching Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Dispatching Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Dispatching Policy Pragma</em>' containment reference.
	 * @see #setTaskDispatchingPolicyPragma(TaskDispatchingPolicyPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_TaskDispatchingPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='task_dispatching_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	TaskDispatchingPolicyPragma getTaskDispatchingPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Dispatching Policy Pragma</em>' containment reference.
	 * @see #getTaskDispatchingPolicyPragma()
	 * @generated
	 */
	void setTaskDispatchingPolicyPragma(TaskDispatchingPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Volatile Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Pragma</em>' containment reference.
	 * @see #setVolatilePragma(VolatilePragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_VolatilePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='volatile_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	VolatilePragma getVolatilePragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getVolatilePragma <em>Volatile Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Volatile Pragma</em>' containment reference.
	 * @see #getVolatilePragma()
	 * @generated
	 */
	void setVolatilePragma(VolatilePragma value);

	/**
	 * Returns the value of the '<em><b>Volatile Components Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Volatile Components Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Volatile Components Pragma</em>' containment reference.
	 * @see #setVolatileComponentsPragma(VolatileComponentsPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_VolatileComponentsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='volatile_components_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	VolatileComponentsPragma getVolatileComponentsPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Volatile Components Pragma</em>' containment reference.
	 * @see #getVolatileComponentsPragma()
	 * @generated
	 */
	void setVolatileComponentsPragma(VolatileComponentsPragma value);

	/**
	 * Returns the value of the '<em><b>Assert Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assert Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assert Pragma</em>' containment reference.
	 * @see #setAssertPragma(AssertPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_AssertPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='assert_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AssertPragma getAssertPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getAssertPragma <em>Assert Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assert Pragma</em>' containment reference.
	 * @see #getAssertPragma()
	 * @generated
	 */
	void setAssertPragma(AssertPragma value);

	/**
	 * Returns the value of the '<em><b>Assertion Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assertion Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assertion Policy Pragma</em>' containment reference.
	 * @see #setAssertionPolicyPragma(AssertionPolicyPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_AssertionPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='assertion_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	AssertionPolicyPragma getAssertionPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assertion Policy Pragma</em>' containment reference.
	 * @see #getAssertionPolicyPragma()
	 * @generated
	 */
	void setAssertionPolicyPragma(AssertionPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Detect Blocking Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Detect Blocking Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Detect Blocking Pragma</em>' containment reference.
	 * @see #setDetectBlockingPragma(DetectBlockingPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_DetectBlockingPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='detect_blocking_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	DetectBlockingPragma getDetectBlockingPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Detect Blocking Pragma</em>' containment reference.
	 * @see #getDetectBlockingPragma()
	 * @generated
	 */
	void setDetectBlockingPragma(DetectBlockingPragma value);

	/**
	 * Returns the value of the '<em><b>No Return Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>No Return Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>No Return Pragma</em>' containment reference.
	 * @see #setNoReturnPragma(NoReturnPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_NoReturnPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='no_return_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	NoReturnPragma getNoReturnPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getNoReturnPragma <em>No Return Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>No Return Pragma</em>' containment reference.
	 * @see #getNoReturnPragma()
	 * @generated
	 */
	void setNoReturnPragma(NoReturnPragma value);

	/**
	 * Returns the value of the '<em><b>Partition Elaboration Policy Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partition Elaboration Policy Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partition Elaboration Policy Pragma</em>' containment reference.
	 * @see #setPartitionElaborationPolicyPragma(PartitionElaborationPolicyPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_PartitionElaborationPolicyPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='partition_elaboration_policy_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PartitionElaborationPolicyPragma getPartitionElaborationPolicyPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Partition Elaboration Policy Pragma</em>' containment reference.
	 * @see #getPartitionElaborationPolicyPragma()
	 * @generated
	 */
	void setPartitionElaborationPolicyPragma(PartitionElaborationPolicyPragma value);

	/**
	 * Returns the value of the '<em><b>Preelaborable Initialization Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preelaborable Initialization Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preelaborable Initialization Pragma</em>' containment reference.
	 * @see #setPreelaborableInitializationPragma(PreelaborableInitializationPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_PreelaborableInitializationPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='preelaborable_initialization_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PreelaborableInitializationPragma getPreelaborableInitializationPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preelaborable Initialization Pragma</em>' containment reference.
	 * @see #getPreelaborableInitializationPragma()
	 * @generated
	 */
	void setPreelaborableInitializationPragma(PreelaborableInitializationPragma value);

	/**
	 * Returns the value of the '<em><b>Priority Specific Dispatching Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Specific Dispatching Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Specific Dispatching Pragma</em>' containment reference.
	 * @see #setPrioritySpecificDispatchingPragma(PrioritySpecificDispatchingPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_PrioritySpecificDispatchingPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='priority_specific_dispatching_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	PrioritySpecificDispatchingPragma getPrioritySpecificDispatchingPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Priority Specific Dispatching Pragma</em>' containment reference.
	 * @see #getPrioritySpecificDispatchingPragma()
	 * @generated
	 */
	void setPrioritySpecificDispatchingPragma(PrioritySpecificDispatchingPragma value);

	/**
	 * Returns the value of the '<em><b>Profile Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Profile Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Profile Pragma</em>' containment reference.
	 * @see #setProfilePragma(ProfilePragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_ProfilePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='profile_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ProfilePragma getProfilePragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getProfilePragma <em>Profile Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Profile Pragma</em>' containment reference.
	 * @see #getProfilePragma()
	 * @generated
	 */
	void setProfilePragma(ProfilePragma value);

	/**
	 * Returns the value of the '<em><b>Relative Deadline Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relative Deadline Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relative Deadline Pragma</em>' containment reference.
	 * @see #setRelativeDeadlinePragma(RelativeDeadlinePragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_RelativeDeadlinePragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='relative_deadline_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	RelativeDeadlinePragma getRelativeDeadlinePragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relative Deadline Pragma</em>' containment reference.
	 * @see #getRelativeDeadlinePragma()
	 * @generated
	 */
	void setRelativeDeadlinePragma(RelativeDeadlinePragma value);

	/**
	 * Returns the value of the '<em><b>Unchecked Union Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unchecked Union Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unchecked Union Pragma</em>' containment reference.
	 * @see #setUncheckedUnionPragma(UncheckedUnionPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_UncheckedUnionPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unchecked_union_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	UncheckedUnionPragma getUncheckedUnionPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unchecked Union Pragma</em>' containment reference.
	 * @see #getUncheckedUnionPragma()
	 * @generated
	 */
	void setUncheckedUnionPragma(UncheckedUnionPragma value);

	/**
	 * Returns the value of the '<em><b>Unsuppress Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unsuppress Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unsuppress Pragma</em>' containment reference.
	 * @see #setUnsuppressPragma(UnsuppressPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_UnsuppressPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unsuppress_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	UnsuppressPragma getUnsuppressPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getUnsuppressPragma <em>Unsuppress Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unsuppress Pragma</em>' containment reference.
	 * @see #getUnsuppressPragma()
	 * @generated
	 */
	void setUnsuppressPragma(UnsuppressPragma value);

	/**
	 * Returns the value of the '<em><b>Default Storage Pool Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Storage Pool Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Storage Pool Pragma</em>' containment reference.
	 * @see #setDefaultStoragePoolPragma(DefaultStoragePoolPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_DefaultStoragePoolPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='default_storage_pool_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	DefaultStoragePoolPragma getDefaultStoragePoolPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Storage Pool Pragma</em>' containment reference.
	 * @see #getDefaultStoragePoolPragma()
	 * @generated
	 */
	void setDefaultStoragePoolPragma(DefaultStoragePoolPragma value);

	/**
	 * Returns the value of the '<em><b>Dispatching Domain Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dispatching Domain Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dispatching Domain Pragma</em>' containment reference.
	 * @see #setDispatchingDomainPragma(DispatchingDomainPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_DispatchingDomainPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='dispatching_domain_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	DispatchingDomainPragma getDispatchingDomainPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dispatching Domain Pragma</em>' containment reference.
	 * @see #getDispatchingDomainPragma()
	 * @generated
	 */
	void setDispatchingDomainPragma(DispatchingDomainPragma value);

	/**
	 * Returns the value of the '<em><b>Cpu Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cpu Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cpu Pragma</em>' containment reference.
	 * @see #setCpuPragma(CpuPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_CpuPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='cpu_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	CpuPragma getCpuPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getCpuPragma <em>Cpu Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cpu Pragma</em>' containment reference.
	 * @see #getCpuPragma()
	 * @generated
	 */
	void setCpuPragma(CpuPragma value);

	/**
	 * Returns the value of the '<em><b>Independent Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Pragma</em>' containment reference.
	 * @see #setIndependentPragma(IndependentPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_IndependentPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='independent_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	IndependentPragma getIndependentPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getIndependentPragma <em>Independent Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Independent Pragma</em>' containment reference.
	 * @see #getIndependentPragma()
	 * @generated
	 */
	void setIndependentPragma(IndependentPragma value);

	/**
	 * Returns the value of the '<em><b>Independent Components Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Independent Components Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Independent Components Pragma</em>' containment reference.
	 * @see #setIndependentComponentsPragma(IndependentComponentsPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_IndependentComponentsPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='independent_components_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	IndependentComponentsPragma getIndependentComponentsPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getIndependentComponentsPragma <em>Independent Components Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Independent Components Pragma</em>' containment reference.
	 * @see #getIndependentComponentsPragma()
	 * @generated
	 */
	void setIndependentComponentsPragma(IndependentComponentsPragma value);

	/**
	 * Returns the value of the '<em><b>Implementation Defined Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implementation Defined Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implementation Defined Pragma</em>' containment reference.
	 * @see #setImplementationDefinedPragma(ImplementationDefinedPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_ImplementationDefinedPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='implementation_defined_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	ImplementationDefinedPragma getImplementationDefinedPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Implementation Defined Pragma</em>' containment reference.
	 * @see #getImplementationDefinedPragma()
	 * @generated
	 */
	void setImplementationDefinedPragma(ImplementationDefinedPragma value);

	/**
	 * Returns the value of the '<em><b>Unknown Pragma</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unknown Pragma</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unknown Pragma</em>' containment reference.
	 * @see #setUnknownPragma(UnknownPragma)
	 * @see Ada.AdaPackage#getRangeConstraintClass_UnknownPragma()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='unknown_pragma' namespace='##targetNamespace'"
	 * @generated
	 */
	UnknownPragma getUnknownPragma();

	/**
	 * Sets the value of the '{@link Ada.RangeConstraintClass#getUnknownPragma <em>Unknown Pragma</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unknown Pragma</em>' containment reference.
	 * @see #getUnknownPragma()
	 * @generated
	 */
	void setUnknownPragma(UnknownPragma value);

} // RangeConstraintClass
