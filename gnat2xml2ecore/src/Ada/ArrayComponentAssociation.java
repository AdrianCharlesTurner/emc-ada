/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Array Component Association</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.ArrayComponentAssociation#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.ArrayComponentAssociation#getArrayComponentChoicesQl <em>Array Component Choices Ql</em>}</li>
 *   <li>{@link Ada.ArrayComponentAssociation#getComponentExpressionQ <em>Component Expression Q</em>}</li>
 *   <li>{@link Ada.ArrayComponentAssociation#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getArrayComponentAssociation()
 * @model extendedMetaData="name='Array_Component_Association' kind='elementOnly'"
 * @generated
 */
public interface ArrayComponentAssociation extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getArrayComponentAssociation_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.ArrayComponentAssociation#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Array Component Choices Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Array Component Choices Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Array Component Choices Ql</em>' containment reference.
	 * @see #setArrayComponentChoicesQl(ElementList)
	 * @see Ada.AdaPackage#getArrayComponentAssociation_ArrayComponentChoicesQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='array_component_choices_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementList getArrayComponentChoicesQl();

	/**
	 * Sets the value of the '{@link Ada.ArrayComponentAssociation#getArrayComponentChoicesQl <em>Array Component Choices Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Array Component Choices Ql</em>' containment reference.
	 * @see #getArrayComponentChoicesQl()
	 * @generated
	 */
	void setArrayComponentChoicesQl(ElementList value);

	/**
	 * Returns the value of the '<em><b>Component Expression Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Expression Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Expression Q</em>' containment reference.
	 * @see #setComponentExpressionQ(ExpressionClass)
	 * @see Ada.AdaPackage#getArrayComponentAssociation_ComponentExpressionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='component_expression_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionClass getComponentExpressionQ();

	/**
	 * Sets the value of the '{@link Ada.ArrayComponentAssociation#getComponentExpressionQ <em>Component Expression Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component Expression Q</em>' containment reference.
	 * @see #getComponentExpressionQ()
	 * @generated
	 */
	void setComponentExpressionQ(ExpressionClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getArrayComponentAssociation_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.ArrayComponentAssociation#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // ArrayComponentAssociation
