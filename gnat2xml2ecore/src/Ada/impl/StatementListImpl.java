/**
 */
package Ada.impl;

import Ada.AbortStatement;
import Ada.AcceptStatement;
import Ada.AdaPackage;
import Ada.AllCallsRemotePragma;
import Ada.AssertPragma;
import Ada.AssertionPolicyPragma;
import Ada.AssignmentStatement;
import Ada.AsynchronousPragma;
import Ada.AsynchronousSelectStatement;
import Ada.AtomicComponentsPragma;
import Ada.AtomicPragma;
import Ada.AttachHandlerPragma;
import Ada.BlockStatement;
import Ada.CaseStatement;
import Ada.CodeStatement;
import Ada.Comment;
import Ada.ConditionalEntryCallStatement;
import Ada.ControlledPragma;
import Ada.ConventionPragma;
import Ada.CpuPragma;
import Ada.DefaultStoragePoolPragma;
import Ada.DelayRelativeStatement;
import Ada.DelayUntilStatement;
import Ada.DetectBlockingPragma;
import Ada.DiscardNamesPragma;
import Ada.DispatchingDomainPragma;
import Ada.ElaborateAllPragma;
import Ada.ElaborateBodyPragma;
import Ada.ElaboratePragma;
import Ada.EntryCallStatement;
import Ada.ExitStatement;
import Ada.ExportPragma;
import Ada.ExtendedReturnStatement;
import Ada.ForLoopStatement;
import Ada.GotoStatement;
import Ada.IfStatement;
import Ada.ImplementationDefinedPragma;
import Ada.ImportPragma;
import Ada.IndependentComponentsPragma;
import Ada.IndependentPragma;
import Ada.InlinePragma;
import Ada.InspectionPointPragma;
import Ada.InterruptHandlerPragma;
import Ada.InterruptPriorityPragma;
import Ada.LinkerOptionsPragma;
import Ada.ListPragma;
import Ada.LockingPolicyPragma;
import Ada.LoopStatement;
import Ada.NoReturnPragma;
import Ada.NormalizeScalarsPragma;
import Ada.NotAnElement;
import Ada.NullStatement;
import Ada.OptimizePragma;
import Ada.PackPragma;
import Ada.PagePragma;
import Ada.PartitionElaborationPolicyPragma;
import Ada.PreelaborableInitializationPragma;
import Ada.PreelaboratePragma;
import Ada.PriorityPragma;
import Ada.PrioritySpecificDispatchingPragma;
import Ada.ProcedureCallStatement;
import Ada.ProfilePragma;
import Ada.PurePragma;
import Ada.QueuingPolicyPragma;
import Ada.RaiseStatement;
import Ada.RelativeDeadlinePragma;
import Ada.RemoteCallInterfacePragma;
import Ada.RemoteTypesPragma;
import Ada.RequeueStatement;
import Ada.RequeueStatementWithAbort;
import Ada.RestrictionsPragma;
import Ada.ReturnStatement;
import Ada.ReviewablePragma;
import Ada.SelectiveAcceptStatement;
import Ada.SharedPassivePragma;
import Ada.StatementList;
import Ada.StorageSizePragma;
import Ada.SuppressPragma;
import Ada.TaskDispatchingPolicyPragma;
import Ada.TerminateAlternativeStatement;
import Ada.TimedEntryCallStatement;
import Ada.UncheckedUnionPragma;
import Ada.UnknownPragma;
import Ada.UnsuppressPragma;
import Ada.VolatileComponentsPragma;
import Ada.VolatilePragma;
import Ada.WhileLoopStatement;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Statement List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.StatementListImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getNotAnElement <em>Not An Element</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getNullStatement <em>Null Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getAssignmentStatement <em>Assignment Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getIfStatement <em>If Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getCaseStatement <em>Case Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getLoopStatement <em>Loop Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getWhileLoopStatement <em>While Loop Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getForLoopStatement <em>For Loop Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getBlockStatement <em>Block Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getExitStatement <em>Exit Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getGotoStatement <em>Goto Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getProcedureCallStatement <em>Procedure Call Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getReturnStatement <em>Return Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getExtendedReturnStatement <em>Extended Return Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getAcceptStatement <em>Accept Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getEntryCallStatement <em>Entry Call Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getRequeueStatement <em>Requeue Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getRequeueStatementWithAbort <em>Requeue Statement With Abort</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getDelayUntilStatement <em>Delay Until Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getDelayRelativeStatement <em>Delay Relative Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getTerminateAlternativeStatement <em>Terminate Alternative Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getSelectiveAcceptStatement <em>Selective Accept Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getTimedEntryCallStatement <em>Timed Entry Call Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getConditionalEntryCallStatement <em>Conditional Entry Call Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getAsynchronousSelectStatement <em>Asynchronous Select Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getAbortStatement <em>Abort Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getRaiseStatement <em>Raise Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getCodeStatement <em>Code Statement</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getAllCallsRemotePragma <em>All Calls Remote Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getAsynchronousPragma <em>Asynchronous Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getAtomicPragma <em>Atomic Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getAtomicComponentsPragma <em>Atomic Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getAttachHandlerPragma <em>Attach Handler Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getControlledPragma <em>Controlled Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getConventionPragma <em>Convention Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getDiscardNamesPragma <em>Discard Names Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getElaboratePragma <em>Elaborate Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getElaborateAllPragma <em>Elaborate All Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getElaborateBodyPragma <em>Elaborate Body Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getExportPragma <em>Export Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getImportPragma <em>Import Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getInlinePragma <em>Inline Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getInspectionPointPragma <em>Inspection Point Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getInterruptHandlerPragma <em>Interrupt Handler Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getInterruptPriorityPragma <em>Interrupt Priority Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getLinkerOptionsPragma <em>Linker Options Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getListPragma <em>List Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getLockingPolicyPragma <em>Locking Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getNormalizeScalarsPragma <em>Normalize Scalars Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getOptimizePragma <em>Optimize Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getPackPragma <em>Pack Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getPagePragma <em>Page Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getPreelaboratePragma <em>Preelaborate Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getPriorityPragma <em>Priority Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getPurePragma <em>Pure Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getQueuingPolicyPragma <em>Queuing Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getRemoteCallInterfacePragma <em>Remote Call Interface Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getRemoteTypesPragma <em>Remote Types Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getRestrictionsPragma <em>Restrictions Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getReviewablePragma <em>Reviewable Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getSharedPassivePragma <em>Shared Passive Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getStorageSizePragma <em>Storage Size Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getSuppressPragma <em>Suppress Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getTaskDispatchingPolicyPragma <em>Task Dispatching Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getVolatilePragma <em>Volatile Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getVolatileComponentsPragma <em>Volatile Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getAssertPragma <em>Assert Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getAssertionPolicyPragma <em>Assertion Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getDetectBlockingPragma <em>Detect Blocking Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getNoReturnPragma <em>No Return Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getPartitionElaborationPolicyPragma <em>Partition Elaboration Policy Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getPreelaborableInitializationPragma <em>Preelaborable Initialization Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getPrioritySpecificDispatchingPragma <em>Priority Specific Dispatching Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getProfilePragma <em>Profile Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getRelativeDeadlinePragma <em>Relative Deadline Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getUncheckedUnionPragma <em>Unchecked Union Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getUnsuppressPragma <em>Unsuppress Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getDefaultStoragePoolPragma <em>Default Storage Pool Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getDispatchingDomainPragma <em>Dispatching Domain Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getCpuPragma <em>Cpu Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getIndependentPragma <em>Independent Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getIndependentComponentsPragma <em>Independent Components Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getImplementationDefinedPragma <em>Implementation Defined Pragma</em>}</li>
 *   <li>{@link Ada.impl.StatementListImpl#getUnknownPragma <em>Unknown Pragma</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StatementListImpl extends MinimalEObjectImpl.Container implements StatementList {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StatementListImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getStatementList();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, AdaPackage.STATEMENT_LIST__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NotAnElement> getNotAnElement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_NotAnElement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NullStatement> getNullStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_NullStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssignmentStatement> getAssignmentStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_AssignmentStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IfStatement> getIfStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_IfStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CaseStatement> getCaseStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_CaseStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LoopStatement> getLoopStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_LoopStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<WhileLoopStatement> getWhileLoopStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_WhileLoopStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ForLoopStatement> getForLoopStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_ForLoopStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BlockStatement> getBlockStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_BlockStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExitStatement> getExitStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_ExitStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GotoStatement> getGotoStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_GotoStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProcedureCallStatement> getProcedureCallStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_ProcedureCallStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReturnStatement> getReturnStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_ReturnStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExtendedReturnStatement> getExtendedReturnStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_ExtendedReturnStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AcceptStatement> getAcceptStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_AcceptStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EntryCallStatement> getEntryCallStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_EntryCallStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RequeueStatement> getRequeueStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_RequeueStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RequeueStatementWithAbort> getRequeueStatementWithAbort() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_RequeueStatementWithAbort());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DelayUntilStatement> getDelayUntilStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_DelayUntilStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DelayRelativeStatement> getDelayRelativeStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_DelayRelativeStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TerminateAlternativeStatement> getTerminateAlternativeStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_TerminateAlternativeStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SelectiveAcceptStatement> getSelectiveAcceptStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_SelectiveAcceptStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TimedEntryCallStatement> getTimedEntryCallStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_TimedEntryCallStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConditionalEntryCallStatement> getConditionalEntryCallStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_ConditionalEntryCallStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AsynchronousSelectStatement> getAsynchronousSelectStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_AsynchronousSelectStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AbortStatement> getAbortStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_AbortStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RaiseStatement> getRaiseStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_RaiseStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CodeStatement> getCodeStatement() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_CodeStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Comment> getComment() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_Comment());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AllCallsRemotePragma> getAllCallsRemotePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_AllCallsRemotePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AsynchronousPragma> getAsynchronousPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_AsynchronousPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AtomicPragma> getAtomicPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_AtomicPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AtomicComponentsPragma> getAtomicComponentsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_AtomicComponentsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AttachHandlerPragma> getAttachHandlerPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_AttachHandlerPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlledPragma> getControlledPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_ControlledPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConventionPragma> getConventionPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_ConventionPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DiscardNamesPragma> getDiscardNamesPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_DiscardNamesPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElaboratePragma> getElaboratePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_ElaboratePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElaborateAllPragma> getElaborateAllPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_ElaborateAllPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ElaborateBodyPragma> getElaborateBodyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_ElaborateBodyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExportPragma> getExportPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_ExportPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ImportPragma> getImportPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_ImportPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InlinePragma> getInlinePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_InlinePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InspectionPointPragma> getInspectionPointPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_InspectionPointPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InterruptHandlerPragma> getInterruptHandlerPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_InterruptHandlerPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InterruptPriorityPragma> getInterruptPriorityPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_InterruptPriorityPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LinkerOptionsPragma> getLinkerOptionsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_LinkerOptionsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ListPragma> getListPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_ListPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LockingPolicyPragma> getLockingPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_LockingPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NormalizeScalarsPragma> getNormalizeScalarsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_NormalizeScalarsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OptimizePragma> getOptimizePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_OptimizePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PackPragma> getPackPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_PackPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PagePragma> getPagePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_PagePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PreelaboratePragma> getPreelaboratePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_PreelaboratePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PriorityPragma> getPriorityPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_PriorityPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PurePragma> getPurePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_PurePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QueuingPolicyPragma> getQueuingPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_QueuingPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RemoteCallInterfacePragma> getRemoteCallInterfacePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_RemoteCallInterfacePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RemoteTypesPragma> getRemoteTypesPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_RemoteTypesPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RestrictionsPragma> getRestrictionsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_RestrictionsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ReviewablePragma> getReviewablePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_ReviewablePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SharedPassivePragma> getSharedPassivePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_SharedPassivePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StorageSizePragma> getStorageSizePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_StorageSizePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SuppressPragma> getSuppressPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_SuppressPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskDispatchingPolicyPragma> getTaskDispatchingPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_TaskDispatchingPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VolatilePragma> getVolatilePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_VolatilePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VolatileComponentsPragma> getVolatileComponentsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_VolatileComponentsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssertPragma> getAssertPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_AssertPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssertionPolicyPragma> getAssertionPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_AssertionPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DetectBlockingPragma> getDetectBlockingPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_DetectBlockingPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NoReturnPragma> getNoReturnPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_NoReturnPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PartitionElaborationPolicyPragma> getPartitionElaborationPolicyPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_PartitionElaborationPolicyPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PreelaborableInitializationPragma> getPreelaborableInitializationPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_PreelaborableInitializationPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PrioritySpecificDispatchingPragma> getPrioritySpecificDispatchingPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_PrioritySpecificDispatchingPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProfilePragma> getProfilePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_ProfilePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RelativeDeadlinePragma> getRelativeDeadlinePragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_RelativeDeadlinePragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UncheckedUnionPragma> getUncheckedUnionPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_UncheckedUnionPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnsuppressPragma> getUnsuppressPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_UnsuppressPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DefaultStoragePoolPragma> getDefaultStoragePoolPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_DefaultStoragePoolPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DispatchingDomainPragma> getDispatchingDomainPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_DispatchingDomainPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CpuPragma> getCpuPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_CpuPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IndependentPragma> getIndependentPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_IndependentPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IndependentComponentsPragma> getIndependentComponentsPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_IndependentComponentsPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ImplementationDefinedPragma> getImplementationDefinedPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_ImplementationDefinedPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnknownPragma> getUnknownPragma() {
		return getGroup().list(AdaPackage.eINSTANCE.getStatementList_UnknownPragma());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.STATEMENT_LIST__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__NOT_AN_ELEMENT:
				return ((InternalEList<?>)getNotAnElement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__NULL_STATEMENT:
				return ((InternalEList<?>)getNullStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__ASSIGNMENT_STATEMENT:
				return ((InternalEList<?>)getAssignmentStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__IF_STATEMENT:
				return ((InternalEList<?>)getIfStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__CASE_STATEMENT:
				return ((InternalEList<?>)getCaseStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__LOOP_STATEMENT:
				return ((InternalEList<?>)getLoopStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__WHILE_LOOP_STATEMENT:
				return ((InternalEList<?>)getWhileLoopStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__FOR_LOOP_STATEMENT:
				return ((InternalEList<?>)getForLoopStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__BLOCK_STATEMENT:
				return ((InternalEList<?>)getBlockStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__EXIT_STATEMENT:
				return ((InternalEList<?>)getExitStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__GOTO_STATEMENT:
				return ((InternalEList<?>)getGotoStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__PROCEDURE_CALL_STATEMENT:
				return ((InternalEList<?>)getProcedureCallStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__RETURN_STATEMENT:
				return ((InternalEList<?>)getReturnStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__EXTENDED_RETURN_STATEMENT:
				return ((InternalEList<?>)getExtendedReturnStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__ACCEPT_STATEMENT:
				return ((InternalEList<?>)getAcceptStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__ENTRY_CALL_STATEMENT:
				return ((InternalEList<?>)getEntryCallStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__REQUEUE_STATEMENT:
				return ((InternalEList<?>)getRequeueStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__REQUEUE_STATEMENT_WITH_ABORT:
				return ((InternalEList<?>)getRequeueStatementWithAbort()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__DELAY_UNTIL_STATEMENT:
				return ((InternalEList<?>)getDelayUntilStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__DELAY_RELATIVE_STATEMENT:
				return ((InternalEList<?>)getDelayRelativeStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__TERMINATE_ALTERNATIVE_STATEMENT:
				return ((InternalEList<?>)getTerminateAlternativeStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__SELECTIVE_ACCEPT_STATEMENT:
				return ((InternalEList<?>)getSelectiveAcceptStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__TIMED_ENTRY_CALL_STATEMENT:
				return ((InternalEList<?>)getTimedEntryCallStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__CONDITIONAL_ENTRY_CALL_STATEMENT:
				return ((InternalEList<?>)getConditionalEntryCallStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__ASYNCHRONOUS_SELECT_STATEMENT:
				return ((InternalEList<?>)getAsynchronousSelectStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__ABORT_STATEMENT:
				return ((InternalEList<?>)getAbortStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__RAISE_STATEMENT:
				return ((InternalEList<?>)getRaiseStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__CODE_STATEMENT:
				return ((InternalEList<?>)getCodeStatement()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__COMMENT:
				return ((InternalEList<?>)getComment()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__ALL_CALLS_REMOTE_PRAGMA:
				return ((InternalEList<?>)getAllCallsRemotePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__ASYNCHRONOUS_PRAGMA:
				return ((InternalEList<?>)getAsynchronousPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__ATOMIC_PRAGMA:
				return ((InternalEList<?>)getAtomicPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__ATOMIC_COMPONENTS_PRAGMA:
				return ((InternalEList<?>)getAtomicComponentsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__ATTACH_HANDLER_PRAGMA:
				return ((InternalEList<?>)getAttachHandlerPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__CONTROLLED_PRAGMA:
				return ((InternalEList<?>)getControlledPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__CONVENTION_PRAGMA:
				return ((InternalEList<?>)getConventionPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__DISCARD_NAMES_PRAGMA:
				return ((InternalEList<?>)getDiscardNamesPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__ELABORATE_PRAGMA:
				return ((InternalEList<?>)getElaboratePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__ELABORATE_ALL_PRAGMA:
				return ((InternalEList<?>)getElaborateAllPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__ELABORATE_BODY_PRAGMA:
				return ((InternalEList<?>)getElaborateBodyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__EXPORT_PRAGMA:
				return ((InternalEList<?>)getExportPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__IMPORT_PRAGMA:
				return ((InternalEList<?>)getImportPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__INLINE_PRAGMA:
				return ((InternalEList<?>)getInlinePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__INSPECTION_POINT_PRAGMA:
				return ((InternalEList<?>)getInspectionPointPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__INTERRUPT_HANDLER_PRAGMA:
				return ((InternalEList<?>)getInterruptHandlerPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__INTERRUPT_PRIORITY_PRAGMA:
				return ((InternalEList<?>)getInterruptPriorityPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__LINKER_OPTIONS_PRAGMA:
				return ((InternalEList<?>)getLinkerOptionsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__LIST_PRAGMA:
				return ((InternalEList<?>)getListPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__LOCKING_POLICY_PRAGMA:
				return ((InternalEList<?>)getLockingPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__NORMALIZE_SCALARS_PRAGMA:
				return ((InternalEList<?>)getNormalizeScalarsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__OPTIMIZE_PRAGMA:
				return ((InternalEList<?>)getOptimizePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__PACK_PRAGMA:
				return ((InternalEList<?>)getPackPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__PAGE_PRAGMA:
				return ((InternalEList<?>)getPagePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__PREELABORATE_PRAGMA:
				return ((InternalEList<?>)getPreelaboratePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__PRIORITY_PRAGMA:
				return ((InternalEList<?>)getPriorityPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__PURE_PRAGMA:
				return ((InternalEList<?>)getPurePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__QUEUING_POLICY_PRAGMA:
				return ((InternalEList<?>)getQueuingPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				return ((InternalEList<?>)getRemoteCallInterfacePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__REMOTE_TYPES_PRAGMA:
				return ((InternalEList<?>)getRemoteTypesPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__RESTRICTIONS_PRAGMA:
				return ((InternalEList<?>)getRestrictionsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__REVIEWABLE_PRAGMA:
				return ((InternalEList<?>)getReviewablePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__SHARED_PASSIVE_PRAGMA:
				return ((InternalEList<?>)getSharedPassivePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__STORAGE_SIZE_PRAGMA:
				return ((InternalEList<?>)getStorageSizePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__SUPPRESS_PRAGMA:
				return ((InternalEList<?>)getSuppressPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				return ((InternalEList<?>)getTaskDispatchingPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__VOLATILE_PRAGMA:
				return ((InternalEList<?>)getVolatilePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__VOLATILE_COMPONENTS_PRAGMA:
				return ((InternalEList<?>)getVolatileComponentsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__ASSERT_PRAGMA:
				return ((InternalEList<?>)getAssertPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__ASSERTION_POLICY_PRAGMA:
				return ((InternalEList<?>)getAssertionPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__DETECT_BLOCKING_PRAGMA:
				return ((InternalEList<?>)getDetectBlockingPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__NO_RETURN_PRAGMA:
				return ((InternalEList<?>)getNoReturnPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				return ((InternalEList<?>)getPartitionElaborationPolicyPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				return ((InternalEList<?>)getPreelaborableInitializationPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return ((InternalEList<?>)getPrioritySpecificDispatchingPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__PROFILE_PRAGMA:
				return ((InternalEList<?>)getProfilePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__RELATIVE_DEADLINE_PRAGMA:
				return ((InternalEList<?>)getRelativeDeadlinePragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__UNCHECKED_UNION_PRAGMA:
				return ((InternalEList<?>)getUncheckedUnionPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__UNSUPPRESS_PRAGMA:
				return ((InternalEList<?>)getUnsuppressPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				return ((InternalEList<?>)getDefaultStoragePoolPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__DISPATCHING_DOMAIN_PRAGMA:
				return ((InternalEList<?>)getDispatchingDomainPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__CPU_PRAGMA:
				return ((InternalEList<?>)getCpuPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__INDEPENDENT_PRAGMA:
				return ((InternalEList<?>)getIndependentPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				return ((InternalEList<?>)getIndependentComponentsPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				return ((InternalEList<?>)getImplementationDefinedPragma()).basicRemove(otherEnd, msgs);
			case AdaPackage.STATEMENT_LIST__UNKNOWN_PRAGMA:
				return ((InternalEList<?>)getUnknownPragma()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.STATEMENT_LIST__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case AdaPackage.STATEMENT_LIST__NOT_AN_ELEMENT:
				return getNotAnElement();
			case AdaPackage.STATEMENT_LIST__NULL_STATEMENT:
				return getNullStatement();
			case AdaPackage.STATEMENT_LIST__ASSIGNMENT_STATEMENT:
				return getAssignmentStatement();
			case AdaPackage.STATEMENT_LIST__IF_STATEMENT:
				return getIfStatement();
			case AdaPackage.STATEMENT_LIST__CASE_STATEMENT:
				return getCaseStatement();
			case AdaPackage.STATEMENT_LIST__LOOP_STATEMENT:
				return getLoopStatement();
			case AdaPackage.STATEMENT_LIST__WHILE_LOOP_STATEMENT:
				return getWhileLoopStatement();
			case AdaPackage.STATEMENT_LIST__FOR_LOOP_STATEMENT:
				return getForLoopStatement();
			case AdaPackage.STATEMENT_LIST__BLOCK_STATEMENT:
				return getBlockStatement();
			case AdaPackage.STATEMENT_LIST__EXIT_STATEMENT:
				return getExitStatement();
			case AdaPackage.STATEMENT_LIST__GOTO_STATEMENT:
				return getGotoStatement();
			case AdaPackage.STATEMENT_LIST__PROCEDURE_CALL_STATEMENT:
				return getProcedureCallStatement();
			case AdaPackage.STATEMENT_LIST__RETURN_STATEMENT:
				return getReturnStatement();
			case AdaPackage.STATEMENT_LIST__EXTENDED_RETURN_STATEMENT:
				return getExtendedReturnStatement();
			case AdaPackage.STATEMENT_LIST__ACCEPT_STATEMENT:
				return getAcceptStatement();
			case AdaPackage.STATEMENT_LIST__ENTRY_CALL_STATEMENT:
				return getEntryCallStatement();
			case AdaPackage.STATEMENT_LIST__REQUEUE_STATEMENT:
				return getRequeueStatement();
			case AdaPackage.STATEMENT_LIST__REQUEUE_STATEMENT_WITH_ABORT:
				return getRequeueStatementWithAbort();
			case AdaPackage.STATEMENT_LIST__DELAY_UNTIL_STATEMENT:
				return getDelayUntilStatement();
			case AdaPackage.STATEMENT_LIST__DELAY_RELATIVE_STATEMENT:
				return getDelayRelativeStatement();
			case AdaPackage.STATEMENT_LIST__TERMINATE_ALTERNATIVE_STATEMENT:
				return getTerminateAlternativeStatement();
			case AdaPackage.STATEMENT_LIST__SELECTIVE_ACCEPT_STATEMENT:
				return getSelectiveAcceptStatement();
			case AdaPackage.STATEMENT_LIST__TIMED_ENTRY_CALL_STATEMENT:
				return getTimedEntryCallStatement();
			case AdaPackage.STATEMENT_LIST__CONDITIONAL_ENTRY_CALL_STATEMENT:
				return getConditionalEntryCallStatement();
			case AdaPackage.STATEMENT_LIST__ASYNCHRONOUS_SELECT_STATEMENT:
				return getAsynchronousSelectStatement();
			case AdaPackage.STATEMENT_LIST__ABORT_STATEMENT:
				return getAbortStatement();
			case AdaPackage.STATEMENT_LIST__RAISE_STATEMENT:
				return getRaiseStatement();
			case AdaPackage.STATEMENT_LIST__CODE_STATEMENT:
				return getCodeStatement();
			case AdaPackage.STATEMENT_LIST__COMMENT:
				return getComment();
			case AdaPackage.STATEMENT_LIST__ALL_CALLS_REMOTE_PRAGMA:
				return getAllCallsRemotePragma();
			case AdaPackage.STATEMENT_LIST__ASYNCHRONOUS_PRAGMA:
				return getAsynchronousPragma();
			case AdaPackage.STATEMENT_LIST__ATOMIC_PRAGMA:
				return getAtomicPragma();
			case AdaPackage.STATEMENT_LIST__ATOMIC_COMPONENTS_PRAGMA:
				return getAtomicComponentsPragma();
			case AdaPackage.STATEMENT_LIST__ATTACH_HANDLER_PRAGMA:
				return getAttachHandlerPragma();
			case AdaPackage.STATEMENT_LIST__CONTROLLED_PRAGMA:
				return getControlledPragma();
			case AdaPackage.STATEMENT_LIST__CONVENTION_PRAGMA:
				return getConventionPragma();
			case AdaPackage.STATEMENT_LIST__DISCARD_NAMES_PRAGMA:
				return getDiscardNamesPragma();
			case AdaPackage.STATEMENT_LIST__ELABORATE_PRAGMA:
				return getElaboratePragma();
			case AdaPackage.STATEMENT_LIST__ELABORATE_ALL_PRAGMA:
				return getElaborateAllPragma();
			case AdaPackage.STATEMENT_LIST__ELABORATE_BODY_PRAGMA:
				return getElaborateBodyPragma();
			case AdaPackage.STATEMENT_LIST__EXPORT_PRAGMA:
				return getExportPragma();
			case AdaPackage.STATEMENT_LIST__IMPORT_PRAGMA:
				return getImportPragma();
			case AdaPackage.STATEMENT_LIST__INLINE_PRAGMA:
				return getInlinePragma();
			case AdaPackage.STATEMENT_LIST__INSPECTION_POINT_PRAGMA:
				return getInspectionPointPragma();
			case AdaPackage.STATEMENT_LIST__INTERRUPT_HANDLER_PRAGMA:
				return getInterruptHandlerPragma();
			case AdaPackage.STATEMENT_LIST__INTERRUPT_PRIORITY_PRAGMA:
				return getInterruptPriorityPragma();
			case AdaPackage.STATEMENT_LIST__LINKER_OPTIONS_PRAGMA:
				return getLinkerOptionsPragma();
			case AdaPackage.STATEMENT_LIST__LIST_PRAGMA:
				return getListPragma();
			case AdaPackage.STATEMENT_LIST__LOCKING_POLICY_PRAGMA:
				return getLockingPolicyPragma();
			case AdaPackage.STATEMENT_LIST__NORMALIZE_SCALARS_PRAGMA:
				return getNormalizeScalarsPragma();
			case AdaPackage.STATEMENT_LIST__OPTIMIZE_PRAGMA:
				return getOptimizePragma();
			case AdaPackage.STATEMENT_LIST__PACK_PRAGMA:
				return getPackPragma();
			case AdaPackage.STATEMENT_LIST__PAGE_PRAGMA:
				return getPagePragma();
			case AdaPackage.STATEMENT_LIST__PREELABORATE_PRAGMA:
				return getPreelaboratePragma();
			case AdaPackage.STATEMENT_LIST__PRIORITY_PRAGMA:
				return getPriorityPragma();
			case AdaPackage.STATEMENT_LIST__PURE_PRAGMA:
				return getPurePragma();
			case AdaPackage.STATEMENT_LIST__QUEUING_POLICY_PRAGMA:
				return getQueuingPolicyPragma();
			case AdaPackage.STATEMENT_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				return getRemoteCallInterfacePragma();
			case AdaPackage.STATEMENT_LIST__REMOTE_TYPES_PRAGMA:
				return getRemoteTypesPragma();
			case AdaPackage.STATEMENT_LIST__RESTRICTIONS_PRAGMA:
				return getRestrictionsPragma();
			case AdaPackage.STATEMENT_LIST__REVIEWABLE_PRAGMA:
				return getReviewablePragma();
			case AdaPackage.STATEMENT_LIST__SHARED_PASSIVE_PRAGMA:
				return getSharedPassivePragma();
			case AdaPackage.STATEMENT_LIST__STORAGE_SIZE_PRAGMA:
				return getStorageSizePragma();
			case AdaPackage.STATEMENT_LIST__SUPPRESS_PRAGMA:
				return getSuppressPragma();
			case AdaPackage.STATEMENT_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				return getTaskDispatchingPolicyPragma();
			case AdaPackage.STATEMENT_LIST__VOLATILE_PRAGMA:
				return getVolatilePragma();
			case AdaPackage.STATEMENT_LIST__VOLATILE_COMPONENTS_PRAGMA:
				return getVolatileComponentsPragma();
			case AdaPackage.STATEMENT_LIST__ASSERT_PRAGMA:
				return getAssertPragma();
			case AdaPackage.STATEMENT_LIST__ASSERTION_POLICY_PRAGMA:
				return getAssertionPolicyPragma();
			case AdaPackage.STATEMENT_LIST__DETECT_BLOCKING_PRAGMA:
				return getDetectBlockingPragma();
			case AdaPackage.STATEMENT_LIST__NO_RETURN_PRAGMA:
				return getNoReturnPragma();
			case AdaPackage.STATEMENT_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				return getPartitionElaborationPolicyPragma();
			case AdaPackage.STATEMENT_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				return getPreelaborableInitializationPragma();
			case AdaPackage.STATEMENT_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return getPrioritySpecificDispatchingPragma();
			case AdaPackage.STATEMENT_LIST__PROFILE_PRAGMA:
				return getProfilePragma();
			case AdaPackage.STATEMENT_LIST__RELATIVE_DEADLINE_PRAGMA:
				return getRelativeDeadlinePragma();
			case AdaPackage.STATEMENT_LIST__UNCHECKED_UNION_PRAGMA:
				return getUncheckedUnionPragma();
			case AdaPackage.STATEMENT_LIST__UNSUPPRESS_PRAGMA:
				return getUnsuppressPragma();
			case AdaPackage.STATEMENT_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				return getDefaultStoragePoolPragma();
			case AdaPackage.STATEMENT_LIST__DISPATCHING_DOMAIN_PRAGMA:
				return getDispatchingDomainPragma();
			case AdaPackage.STATEMENT_LIST__CPU_PRAGMA:
				return getCpuPragma();
			case AdaPackage.STATEMENT_LIST__INDEPENDENT_PRAGMA:
				return getIndependentPragma();
			case AdaPackage.STATEMENT_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				return getIndependentComponentsPragma();
			case AdaPackage.STATEMENT_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				return getImplementationDefinedPragma();
			case AdaPackage.STATEMENT_LIST__UNKNOWN_PRAGMA:
				return getUnknownPragma();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.STATEMENT_LIST__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case AdaPackage.STATEMENT_LIST__NOT_AN_ELEMENT:
				getNotAnElement().clear();
				getNotAnElement().addAll((Collection<? extends NotAnElement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__NULL_STATEMENT:
				getNullStatement().clear();
				getNullStatement().addAll((Collection<? extends NullStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__ASSIGNMENT_STATEMENT:
				getAssignmentStatement().clear();
				getAssignmentStatement().addAll((Collection<? extends AssignmentStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__IF_STATEMENT:
				getIfStatement().clear();
				getIfStatement().addAll((Collection<? extends IfStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__CASE_STATEMENT:
				getCaseStatement().clear();
				getCaseStatement().addAll((Collection<? extends CaseStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__LOOP_STATEMENT:
				getLoopStatement().clear();
				getLoopStatement().addAll((Collection<? extends LoopStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__WHILE_LOOP_STATEMENT:
				getWhileLoopStatement().clear();
				getWhileLoopStatement().addAll((Collection<? extends WhileLoopStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__FOR_LOOP_STATEMENT:
				getForLoopStatement().clear();
				getForLoopStatement().addAll((Collection<? extends ForLoopStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__BLOCK_STATEMENT:
				getBlockStatement().clear();
				getBlockStatement().addAll((Collection<? extends BlockStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__EXIT_STATEMENT:
				getExitStatement().clear();
				getExitStatement().addAll((Collection<? extends ExitStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__GOTO_STATEMENT:
				getGotoStatement().clear();
				getGotoStatement().addAll((Collection<? extends GotoStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__PROCEDURE_CALL_STATEMENT:
				getProcedureCallStatement().clear();
				getProcedureCallStatement().addAll((Collection<? extends ProcedureCallStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__RETURN_STATEMENT:
				getReturnStatement().clear();
				getReturnStatement().addAll((Collection<? extends ReturnStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__EXTENDED_RETURN_STATEMENT:
				getExtendedReturnStatement().clear();
				getExtendedReturnStatement().addAll((Collection<? extends ExtendedReturnStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__ACCEPT_STATEMENT:
				getAcceptStatement().clear();
				getAcceptStatement().addAll((Collection<? extends AcceptStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__ENTRY_CALL_STATEMENT:
				getEntryCallStatement().clear();
				getEntryCallStatement().addAll((Collection<? extends EntryCallStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__REQUEUE_STATEMENT:
				getRequeueStatement().clear();
				getRequeueStatement().addAll((Collection<? extends RequeueStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__REQUEUE_STATEMENT_WITH_ABORT:
				getRequeueStatementWithAbort().clear();
				getRequeueStatementWithAbort().addAll((Collection<? extends RequeueStatementWithAbort>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__DELAY_UNTIL_STATEMENT:
				getDelayUntilStatement().clear();
				getDelayUntilStatement().addAll((Collection<? extends DelayUntilStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__DELAY_RELATIVE_STATEMENT:
				getDelayRelativeStatement().clear();
				getDelayRelativeStatement().addAll((Collection<? extends DelayRelativeStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__TERMINATE_ALTERNATIVE_STATEMENT:
				getTerminateAlternativeStatement().clear();
				getTerminateAlternativeStatement().addAll((Collection<? extends TerminateAlternativeStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__SELECTIVE_ACCEPT_STATEMENT:
				getSelectiveAcceptStatement().clear();
				getSelectiveAcceptStatement().addAll((Collection<? extends SelectiveAcceptStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__TIMED_ENTRY_CALL_STATEMENT:
				getTimedEntryCallStatement().clear();
				getTimedEntryCallStatement().addAll((Collection<? extends TimedEntryCallStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__CONDITIONAL_ENTRY_CALL_STATEMENT:
				getConditionalEntryCallStatement().clear();
				getConditionalEntryCallStatement().addAll((Collection<? extends ConditionalEntryCallStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__ASYNCHRONOUS_SELECT_STATEMENT:
				getAsynchronousSelectStatement().clear();
				getAsynchronousSelectStatement().addAll((Collection<? extends AsynchronousSelectStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__ABORT_STATEMENT:
				getAbortStatement().clear();
				getAbortStatement().addAll((Collection<? extends AbortStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__RAISE_STATEMENT:
				getRaiseStatement().clear();
				getRaiseStatement().addAll((Collection<? extends RaiseStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__CODE_STATEMENT:
				getCodeStatement().clear();
				getCodeStatement().addAll((Collection<? extends CodeStatement>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__COMMENT:
				getComment().clear();
				getComment().addAll((Collection<? extends Comment>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__ALL_CALLS_REMOTE_PRAGMA:
				getAllCallsRemotePragma().clear();
				getAllCallsRemotePragma().addAll((Collection<? extends AllCallsRemotePragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__ASYNCHRONOUS_PRAGMA:
				getAsynchronousPragma().clear();
				getAsynchronousPragma().addAll((Collection<? extends AsynchronousPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__ATOMIC_PRAGMA:
				getAtomicPragma().clear();
				getAtomicPragma().addAll((Collection<? extends AtomicPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__ATOMIC_COMPONENTS_PRAGMA:
				getAtomicComponentsPragma().clear();
				getAtomicComponentsPragma().addAll((Collection<? extends AtomicComponentsPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__ATTACH_HANDLER_PRAGMA:
				getAttachHandlerPragma().clear();
				getAttachHandlerPragma().addAll((Collection<? extends AttachHandlerPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__CONTROLLED_PRAGMA:
				getControlledPragma().clear();
				getControlledPragma().addAll((Collection<? extends ControlledPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__CONVENTION_PRAGMA:
				getConventionPragma().clear();
				getConventionPragma().addAll((Collection<? extends ConventionPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__DISCARD_NAMES_PRAGMA:
				getDiscardNamesPragma().clear();
				getDiscardNamesPragma().addAll((Collection<? extends DiscardNamesPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__ELABORATE_PRAGMA:
				getElaboratePragma().clear();
				getElaboratePragma().addAll((Collection<? extends ElaboratePragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__ELABORATE_ALL_PRAGMA:
				getElaborateAllPragma().clear();
				getElaborateAllPragma().addAll((Collection<? extends ElaborateAllPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__ELABORATE_BODY_PRAGMA:
				getElaborateBodyPragma().clear();
				getElaborateBodyPragma().addAll((Collection<? extends ElaborateBodyPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__EXPORT_PRAGMA:
				getExportPragma().clear();
				getExportPragma().addAll((Collection<? extends ExportPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__IMPORT_PRAGMA:
				getImportPragma().clear();
				getImportPragma().addAll((Collection<? extends ImportPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__INLINE_PRAGMA:
				getInlinePragma().clear();
				getInlinePragma().addAll((Collection<? extends InlinePragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__INSPECTION_POINT_PRAGMA:
				getInspectionPointPragma().clear();
				getInspectionPointPragma().addAll((Collection<? extends InspectionPointPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__INTERRUPT_HANDLER_PRAGMA:
				getInterruptHandlerPragma().clear();
				getInterruptHandlerPragma().addAll((Collection<? extends InterruptHandlerPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__INTERRUPT_PRIORITY_PRAGMA:
				getInterruptPriorityPragma().clear();
				getInterruptPriorityPragma().addAll((Collection<? extends InterruptPriorityPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__LINKER_OPTIONS_PRAGMA:
				getLinkerOptionsPragma().clear();
				getLinkerOptionsPragma().addAll((Collection<? extends LinkerOptionsPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__LIST_PRAGMA:
				getListPragma().clear();
				getListPragma().addAll((Collection<? extends ListPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__LOCKING_POLICY_PRAGMA:
				getLockingPolicyPragma().clear();
				getLockingPolicyPragma().addAll((Collection<? extends LockingPolicyPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__NORMALIZE_SCALARS_PRAGMA:
				getNormalizeScalarsPragma().clear();
				getNormalizeScalarsPragma().addAll((Collection<? extends NormalizeScalarsPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__OPTIMIZE_PRAGMA:
				getOptimizePragma().clear();
				getOptimizePragma().addAll((Collection<? extends OptimizePragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__PACK_PRAGMA:
				getPackPragma().clear();
				getPackPragma().addAll((Collection<? extends PackPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__PAGE_PRAGMA:
				getPagePragma().clear();
				getPagePragma().addAll((Collection<? extends PagePragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__PREELABORATE_PRAGMA:
				getPreelaboratePragma().clear();
				getPreelaboratePragma().addAll((Collection<? extends PreelaboratePragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__PRIORITY_PRAGMA:
				getPriorityPragma().clear();
				getPriorityPragma().addAll((Collection<? extends PriorityPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__PURE_PRAGMA:
				getPurePragma().clear();
				getPurePragma().addAll((Collection<? extends PurePragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__QUEUING_POLICY_PRAGMA:
				getQueuingPolicyPragma().clear();
				getQueuingPolicyPragma().addAll((Collection<? extends QueuingPolicyPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				getRemoteCallInterfacePragma().clear();
				getRemoteCallInterfacePragma().addAll((Collection<? extends RemoteCallInterfacePragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__REMOTE_TYPES_PRAGMA:
				getRemoteTypesPragma().clear();
				getRemoteTypesPragma().addAll((Collection<? extends RemoteTypesPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__RESTRICTIONS_PRAGMA:
				getRestrictionsPragma().clear();
				getRestrictionsPragma().addAll((Collection<? extends RestrictionsPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__REVIEWABLE_PRAGMA:
				getReviewablePragma().clear();
				getReviewablePragma().addAll((Collection<? extends ReviewablePragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__SHARED_PASSIVE_PRAGMA:
				getSharedPassivePragma().clear();
				getSharedPassivePragma().addAll((Collection<? extends SharedPassivePragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__STORAGE_SIZE_PRAGMA:
				getStorageSizePragma().clear();
				getStorageSizePragma().addAll((Collection<? extends StorageSizePragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__SUPPRESS_PRAGMA:
				getSuppressPragma().clear();
				getSuppressPragma().addAll((Collection<? extends SuppressPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				getTaskDispatchingPolicyPragma().clear();
				getTaskDispatchingPolicyPragma().addAll((Collection<? extends TaskDispatchingPolicyPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__VOLATILE_PRAGMA:
				getVolatilePragma().clear();
				getVolatilePragma().addAll((Collection<? extends VolatilePragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__VOLATILE_COMPONENTS_PRAGMA:
				getVolatileComponentsPragma().clear();
				getVolatileComponentsPragma().addAll((Collection<? extends VolatileComponentsPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__ASSERT_PRAGMA:
				getAssertPragma().clear();
				getAssertPragma().addAll((Collection<? extends AssertPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__ASSERTION_POLICY_PRAGMA:
				getAssertionPolicyPragma().clear();
				getAssertionPolicyPragma().addAll((Collection<? extends AssertionPolicyPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__DETECT_BLOCKING_PRAGMA:
				getDetectBlockingPragma().clear();
				getDetectBlockingPragma().addAll((Collection<? extends DetectBlockingPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__NO_RETURN_PRAGMA:
				getNoReturnPragma().clear();
				getNoReturnPragma().addAll((Collection<? extends NoReturnPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				getPartitionElaborationPolicyPragma().clear();
				getPartitionElaborationPolicyPragma().addAll((Collection<? extends PartitionElaborationPolicyPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				getPreelaborableInitializationPragma().clear();
				getPreelaborableInitializationPragma().addAll((Collection<? extends PreelaborableInitializationPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				getPrioritySpecificDispatchingPragma().clear();
				getPrioritySpecificDispatchingPragma().addAll((Collection<? extends PrioritySpecificDispatchingPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__PROFILE_PRAGMA:
				getProfilePragma().clear();
				getProfilePragma().addAll((Collection<? extends ProfilePragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__RELATIVE_DEADLINE_PRAGMA:
				getRelativeDeadlinePragma().clear();
				getRelativeDeadlinePragma().addAll((Collection<? extends RelativeDeadlinePragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__UNCHECKED_UNION_PRAGMA:
				getUncheckedUnionPragma().clear();
				getUncheckedUnionPragma().addAll((Collection<? extends UncheckedUnionPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__UNSUPPRESS_PRAGMA:
				getUnsuppressPragma().clear();
				getUnsuppressPragma().addAll((Collection<? extends UnsuppressPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				getDefaultStoragePoolPragma().clear();
				getDefaultStoragePoolPragma().addAll((Collection<? extends DefaultStoragePoolPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__DISPATCHING_DOMAIN_PRAGMA:
				getDispatchingDomainPragma().clear();
				getDispatchingDomainPragma().addAll((Collection<? extends DispatchingDomainPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__CPU_PRAGMA:
				getCpuPragma().clear();
				getCpuPragma().addAll((Collection<? extends CpuPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__INDEPENDENT_PRAGMA:
				getIndependentPragma().clear();
				getIndependentPragma().addAll((Collection<? extends IndependentPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				getIndependentComponentsPragma().clear();
				getIndependentComponentsPragma().addAll((Collection<? extends IndependentComponentsPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				getImplementationDefinedPragma().clear();
				getImplementationDefinedPragma().addAll((Collection<? extends ImplementationDefinedPragma>)newValue);
				return;
			case AdaPackage.STATEMENT_LIST__UNKNOWN_PRAGMA:
				getUnknownPragma().clear();
				getUnknownPragma().addAll((Collection<? extends UnknownPragma>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.STATEMENT_LIST__GROUP:
				getGroup().clear();
				return;
			case AdaPackage.STATEMENT_LIST__NOT_AN_ELEMENT:
				getNotAnElement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__NULL_STATEMENT:
				getNullStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__ASSIGNMENT_STATEMENT:
				getAssignmentStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__IF_STATEMENT:
				getIfStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__CASE_STATEMENT:
				getCaseStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__LOOP_STATEMENT:
				getLoopStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__WHILE_LOOP_STATEMENT:
				getWhileLoopStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__FOR_LOOP_STATEMENT:
				getForLoopStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__BLOCK_STATEMENT:
				getBlockStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__EXIT_STATEMENT:
				getExitStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__GOTO_STATEMENT:
				getGotoStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__PROCEDURE_CALL_STATEMENT:
				getProcedureCallStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__RETURN_STATEMENT:
				getReturnStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__EXTENDED_RETURN_STATEMENT:
				getExtendedReturnStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__ACCEPT_STATEMENT:
				getAcceptStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__ENTRY_CALL_STATEMENT:
				getEntryCallStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__REQUEUE_STATEMENT:
				getRequeueStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__REQUEUE_STATEMENT_WITH_ABORT:
				getRequeueStatementWithAbort().clear();
				return;
			case AdaPackage.STATEMENT_LIST__DELAY_UNTIL_STATEMENT:
				getDelayUntilStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__DELAY_RELATIVE_STATEMENT:
				getDelayRelativeStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__TERMINATE_ALTERNATIVE_STATEMENT:
				getTerminateAlternativeStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__SELECTIVE_ACCEPT_STATEMENT:
				getSelectiveAcceptStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__TIMED_ENTRY_CALL_STATEMENT:
				getTimedEntryCallStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__CONDITIONAL_ENTRY_CALL_STATEMENT:
				getConditionalEntryCallStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__ASYNCHRONOUS_SELECT_STATEMENT:
				getAsynchronousSelectStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__ABORT_STATEMENT:
				getAbortStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__RAISE_STATEMENT:
				getRaiseStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__CODE_STATEMENT:
				getCodeStatement().clear();
				return;
			case AdaPackage.STATEMENT_LIST__COMMENT:
				getComment().clear();
				return;
			case AdaPackage.STATEMENT_LIST__ALL_CALLS_REMOTE_PRAGMA:
				getAllCallsRemotePragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__ASYNCHRONOUS_PRAGMA:
				getAsynchronousPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__ATOMIC_PRAGMA:
				getAtomicPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__ATOMIC_COMPONENTS_PRAGMA:
				getAtomicComponentsPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__ATTACH_HANDLER_PRAGMA:
				getAttachHandlerPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__CONTROLLED_PRAGMA:
				getControlledPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__CONVENTION_PRAGMA:
				getConventionPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__DISCARD_NAMES_PRAGMA:
				getDiscardNamesPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__ELABORATE_PRAGMA:
				getElaboratePragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__ELABORATE_ALL_PRAGMA:
				getElaborateAllPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__ELABORATE_BODY_PRAGMA:
				getElaborateBodyPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__EXPORT_PRAGMA:
				getExportPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__IMPORT_PRAGMA:
				getImportPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__INLINE_PRAGMA:
				getInlinePragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__INSPECTION_POINT_PRAGMA:
				getInspectionPointPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__INTERRUPT_HANDLER_PRAGMA:
				getInterruptHandlerPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__INTERRUPT_PRIORITY_PRAGMA:
				getInterruptPriorityPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__LINKER_OPTIONS_PRAGMA:
				getLinkerOptionsPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__LIST_PRAGMA:
				getListPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__LOCKING_POLICY_PRAGMA:
				getLockingPolicyPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__NORMALIZE_SCALARS_PRAGMA:
				getNormalizeScalarsPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__OPTIMIZE_PRAGMA:
				getOptimizePragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__PACK_PRAGMA:
				getPackPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__PAGE_PRAGMA:
				getPagePragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__PREELABORATE_PRAGMA:
				getPreelaboratePragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__PRIORITY_PRAGMA:
				getPriorityPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__PURE_PRAGMA:
				getPurePragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__QUEUING_POLICY_PRAGMA:
				getQueuingPolicyPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				getRemoteCallInterfacePragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__REMOTE_TYPES_PRAGMA:
				getRemoteTypesPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__RESTRICTIONS_PRAGMA:
				getRestrictionsPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__REVIEWABLE_PRAGMA:
				getReviewablePragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__SHARED_PASSIVE_PRAGMA:
				getSharedPassivePragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__STORAGE_SIZE_PRAGMA:
				getStorageSizePragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__SUPPRESS_PRAGMA:
				getSuppressPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				getTaskDispatchingPolicyPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__VOLATILE_PRAGMA:
				getVolatilePragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__VOLATILE_COMPONENTS_PRAGMA:
				getVolatileComponentsPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__ASSERT_PRAGMA:
				getAssertPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__ASSERTION_POLICY_PRAGMA:
				getAssertionPolicyPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__DETECT_BLOCKING_PRAGMA:
				getDetectBlockingPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__NO_RETURN_PRAGMA:
				getNoReturnPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				getPartitionElaborationPolicyPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				getPreelaborableInitializationPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				getPrioritySpecificDispatchingPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__PROFILE_PRAGMA:
				getProfilePragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__RELATIVE_DEADLINE_PRAGMA:
				getRelativeDeadlinePragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__UNCHECKED_UNION_PRAGMA:
				getUncheckedUnionPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__UNSUPPRESS_PRAGMA:
				getUnsuppressPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				getDefaultStoragePoolPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__DISPATCHING_DOMAIN_PRAGMA:
				getDispatchingDomainPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__CPU_PRAGMA:
				getCpuPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__INDEPENDENT_PRAGMA:
				getIndependentPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				getIndependentComponentsPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				getImplementationDefinedPragma().clear();
				return;
			case AdaPackage.STATEMENT_LIST__UNKNOWN_PRAGMA:
				getUnknownPragma().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.STATEMENT_LIST__GROUP:
				return group != null && !group.isEmpty();
			case AdaPackage.STATEMENT_LIST__NOT_AN_ELEMENT:
				return !getNotAnElement().isEmpty();
			case AdaPackage.STATEMENT_LIST__NULL_STATEMENT:
				return !getNullStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__ASSIGNMENT_STATEMENT:
				return !getAssignmentStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__IF_STATEMENT:
				return !getIfStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__CASE_STATEMENT:
				return !getCaseStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__LOOP_STATEMENT:
				return !getLoopStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__WHILE_LOOP_STATEMENT:
				return !getWhileLoopStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__FOR_LOOP_STATEMENT:
				return !getForLoopStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__BLOCK_STATEMENT:
				return !getBlockStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__EXIT_STATEMENT:
				return !getExitStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__GOTO_STATEMENT:
				return !getGotoStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__PROCEDURE_CALL_STATEMENT:
				return !getProcedureCallStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__RETURN_STATEMENT:
				return !getReturnStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__EXTENDED_RETURN_STATEMENT:
				return !getExtendedReturnStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__ACCEPT_STATEMENT:
				return !getAcceptStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__ENTRY_CALL_STATEMENT:
				return !getEntryCallStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__REQUEUE_STATEMENT:
				return !getRequeueStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__REQUEUE_STATEMENT_WITH_ABORT:
				return !getRequeueStatementWithAbort().isEmpty();
			case AdaPackage.STATEMENT_LIST__DELAY_UNTIL_STATEMENT:
				return !getDelayUntilStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__DELAY_RELATIVE_STATEMENT:
				return !getDelayRelativeStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__TERMINATE_ALTERNATIVE_STATEMENT:
				return !getTerminateAlternativeStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__SELECTIVE_ACCEPT_STATEMENT:
				return !getSelectiveAcceptStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__TIMED_ENTRY_CALL_STATEMENT:
				return !getTimedEntryCallStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__CONDITIONAL_ENTRY_CALL_STATEMENT:
				return !getConditionalEntryCallStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__ASYNCHRONOUS_SELECT_STATEMENT:
				return !getAsynchronousSelectStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__ABORT_STATEMENT:
				return !getAbortStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__RAISE_STATEMENT:
				return !getRaiseStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__CODE_STATEMENT:
				return !getCodeStatement().isEmpty();
			case AdaPackage.STATEMENT_LIST__COMMENT:
				return !getComment().isEmpty();
			case AdaPackage.STATEMENT_LIST__ALL_CALLS_REMOTE_PRAGMA:
				return !getAllCallsRemotePragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__ASYNCHRONOUS_PRAGMA:
				return !getAsynchronousPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__ATOMIC_PRAGMA:
				return !getAtomicPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__ATOMIC_COMPONENTS_PRAGMA:
				return !getAtomicComponentsPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__ATTACH_HANDLER_PRAGMA:
				return !getAttachHandlerPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__CONTROLLED_PRAGMA:
				return !getControlledPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__CONVENTION_PRAGMA:
				return !getConventionPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__DISCARD_NAMES_PRAGMA:
				return !getDiscardNamesPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__ELABORATE_PRAGMA:
				return !getElaboratePragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__ELABORATE_ALL_PRAGMA:
				return !getElaborateAllPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__ELABORATE_BODY_PRAGMA:
				return !getElaborateBodyPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__EXPORT_PRAGMA:
				return !getExportPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__IMPORT_PRAGMA:
				return !getImportPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__INLINE_PRAGMA:
				return !getInlinePragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__INSPECTION_POINT_PRAGMA:
				return !getInspectionPointPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__INTERRUPT_HANDLER_PRAGMA:
				return !getInterruptHandlerPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__INTERRUPT_PRIORITY_PRAGMA:
				return !getInterruptPriorityPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__LINKER_OPTIONS_PRAGMA:
				return !getLinkerOptionsPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__LIST_PRAGMA:
				return !getListPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__LOCKING_POLICY_PRAGMA:
				return !getLockingPolicyPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__NORMALIZE_SCALARS_PRAGMA:
				return !getNormalizeScalarsPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__OPTIMIZE_PRAGMA:
				return !getOptimizePragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__PACK_PRAGMA:
				return !getPackPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__PAGE_PRAGMA:
				return !getPagePragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__PREELABORATE_PRAGMA:
				return !getPreelaboratePragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__PRIORITY_PRAGMA:
				return !getPriorityPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__PURE_PRAGMA:
				return !getPurePragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__QUEUING_POLICY_PRAGMA:
				return !getQueuingPolicyPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__REMOTE_CALL_INTERFACE_PRAGMA:
				return !getRemoteCallInterfacePragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__REMOTE_TYPES_PRAGMA:
				return !getRemoteTypesPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__RESTRICTIONS_PRAGMA:
				return !getRestrictionsPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__REVIEWABLE_PRAGMA:
				return !getReviewablePragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__SHARED_PASSIVE_PRAGMA:
				return !getSharedPassivePragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__STORAGE_SIZE_PRAGMA:
				return !getStorageSizePragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__SUPPRESS_PRAGMA:
				return !getSuppressPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__TASK_DISPATCHING_POLICY_PRAGMA:
				return !getTaskDispatchingPolicyPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__VOLATILE_PRAGMA:
				return !getVolatilePragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__VOLATILE_COMPONENTS_PRAGMA:
				return !getVolatileComponentsPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__ASSERT_PRAGMA:
				return !getAssertPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__ASSERTION_POLICY_PRAGMA:
				return !getAssertionPolicyPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__DETECT_BLOCKING_PRAGMA:
				return !getDetectBlockingPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__NO_RETURN_PRAGMA:
				return !getNoReturnPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__PARTITION_ELABORATION_POLICY_PRAGMA:
				return !getPartitionElaborationPolicyPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__PREELABORABLE_INITIALIZATION_PRAGMA:
				return !getPreelaborableInitializationPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__PRIORITY_SPECIFIC_DISPATCHING_PRAGMA:
				return !getPrioritySpecificDispatchingPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__PROFILE_PRAGMA:
				return !getProfilePragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__RELATIVE_DEADLINE_PRAGMA:
				return !getRelativeDeadlinePragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__UNCHECKED_UNION_PRAGMA:
				return !getUncheckedUnionPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__UNSUPPRESS_PRAGMA:
				return !getUnsuppressPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__DEFAULT_STORAGE_POOL_PRAGMA:
				return !getDefaultStoragePoolPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__DISPATCHING_DOMAIN_PRAGMA:
				return !getDispatchingDomainPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__CPU_PRAGMA:
				return !getCpuPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__INDEPENDENT_PRAGMA:
				return !getIndependentPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__INDEPENDENT_COMPONENTS_PRAGMA:
				return !getIndependentComponentsPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__IMPLEMENTATION_DEFINED_PRAGMA:
				return !getImplementationDefinedPragma().isEmpty();
			case AdaPackage.STATEMENT_LIST__UNKNOWN_PRAGMA:
				return !getUnknownPragma().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(')');
		return result.toString();
	}

} //StatementListImpl
