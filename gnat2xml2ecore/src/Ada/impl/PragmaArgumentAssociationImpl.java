/**
 */
package Ada.impl;

import Ada.AdaPackage;
import Ada.ElementClass;
import Ada.ExpressionClass;
import Ada.PragmaArgumentAssociation;
import Ada.SourceLocation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Pragma Argument Association</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link Ada.impl.PragmaArgumentAssociationImpl#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.impl.PragmaArgumentAssociationImpl#getFormalParameterQ <em>Formal Parameter Q</em>}</li>
 *   <li>{@link Ada.impl.PragmaArgumentAssociationImpl#getActualParameterQ <em>Actual Parameter Q</em>}</li>
 *   <li>{@link Ada.impl.PragmaArgumentAssociationImpl#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PragmaArgumentAssociationImpl extends MinimalEObjectImpl.Container implements PragmaArgumentAssociation {
	/**
	 * The cached value of the '{@link #getSloc() <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSloc()
	 * @generated
	 * @ordered
	 */
	protected SourceLocation sloc;

	/**
	 * The cached value of the '{@link #getFormalParameterQ() <em>Formal Parameter Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalParameterQ()
	 * @generated
	 * @ordered
	 */
	protected ElementClass formalParameterQ;

	/**
	 * The cached value of the '{@link #getActualParameterQ() <em>Actual Parameter Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActualParameterQ()
	 * @generated
	 * @ordered
	 */
	protected ExpressionClass actualParameterQ;

	/**
	 * The default value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECKS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChecks() <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChecks()
	 * @generated
	 * @ordered
	 */
	protected String checks = CHECKS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PragmaArgumentAssociationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AdaPackage.eINSTANCE.getPragmaArgumentAssociation();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceLocation getSloc() {
		return sloc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSloc(SourceLocation newSloc, NotificationChain msgs) {
		SourceLocation oldSloc = sloc;
		sloc = newSloc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__SLOC, oldSloc, newSloc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSloc(SourceLocation newSloc) {
		if (newSloc != sloc) {
			NotificationChain msgs = null;
			if (sloc != null)
				msgs = ((InternalEObject)sloc).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__SLOC, null, msgs);
			if (newSloc != null)
				msgs = ((InternalEObject)newSloc).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__SLOC, null, msgs);
			msgs = basicSetSloc(newSloc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__SLOC, newSloc, newSloc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementClass getFormalParameterQ() {
		return formalParameterQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFormalParameterQ(ElementClass newFormalParameterQ, NotificationChain msgs) {
		ElementClass oldFormalParameterQ = formalParameterQ;
		formalParameterQ = newFormalParameterQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__FORMAL_PARAMETER_Q, oldFormalParameterQ, newFormalParameterQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalParameterQ(ElementClass newFormalParameterQ) {
		if (newFormalParameterQ != formalParameterQ) {
			NotificationChain msgs = null;
			if (formalParameterQ != null)
				msgs = ((InternalEObject)formalParameterQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__FORMAL_PARAMETER_Q, null, msgs);
			if (newFormalParameterQ != null)
				msgs = ((InternalEObject)newFormalParameterQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__FORMAL_PARAMETER_Q, null, msgs);
			msgs = basicSetFormalParameterQ(newFormalParameterQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__FORMAL_PARAMETER_Q, newFormalParameterQ, newFormalParameterQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionClass getActualParameterQ() {
		return actualParameterQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetActualParameterQ(ExpressionClass newActualParameterQ, NotificationChain msgs) {
		ExpressionClass oldActualParameterQ = actualParameterQ;
		actualParameterQ = newActualParameterQ;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__ACTUAL_PARAMETER_Q, oldActualParameterQ, newActualParameterQ);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActualParameterQ(ExpressionClass newActualParameterQ) {
		if (newActualParameterQ != actualParameterQ) {
			NotificationChain msgs = null;
			if (actualParameterQ != null)
				msgs = ((InternalEObject)actualParameterQ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__ACTUAL_PARAMETER_Q, null, msgs);
			if (newActualParameterQ != null)
				msgs = ((InternalEObject)newActualParameterQ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__ACTUAL_PARAMETER_Q, null, msgs);
			msgs = basicSetActualParameterQ(newActualParameterQ, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__ACTUAL_PARAMETER_Q, newActualParameterQ, newActualParameterQ));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChecks() {
		return checks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChecks(String newChecks) {
		String oldChecks = checks;
		checks = newChecks;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__CHECKS, oldChecks, checks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__SLOC:
				return basicSetSloc(null, msgs);
			case AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__FORMAL_PARAMETER_Q:
				return basicSetFormalParameterQ(null, msgs);
			case AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__ACTUAL_PARAMETER_Q:
				return basicSetActualParameterQ(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__SLOC:
				return getSloc();
			case AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__FORMAL_PARAMETER_Q:
				return getFormalParameterQ();
			case AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__ACTUAL_PARAMETER_Q:
				return getActualParameterQ();
			case AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__CHECKS:
				return getChecks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__SLOC:
				setSloc((SourceLocation)newValue);
				return;
			case AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__FORMAL_PARAMETER_Q:
				setFormalParameterQ((ElementClass)newValue);
				return;
			case AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__ACTUAL_PARAMETER_Q:
				setActualParameterQ((ExpressionClass)newValue);
				return;
			case AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__CHECKS:
				setChecks((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__SLOC:
				setSloc((SourceLocation)null);
				return;
			case AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__FORMAL_PARAMETER_Q:
				setFormalParameterQ((ElementClass)null);
				return;
			case AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__ACTUAL_PARAMETER_Q:
				setActualParameterQ((ExpressionClass)null);
				return;
			case AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__CHECKS:
				setChecks(CHECKS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__SLOC:
				return sloc != null;
			case AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__FORMAL_PARAMETER_Q:
				return formalParameterQ != null;
			case AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__ACTUAL_PARAMETER_Q:
				return actualParameterQ != null;
			case AdaPackage.PRAGMA_ARGUMENT_ASSOCIATION__CHECKS:
				return CHECKS_EDEFAULT == null ? checks != null : !CHECKS_EDEFAULT.equals(checks);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (checks: ");
		result.append(checks);
		result.append(')');
		return result.toString();
	}

} //PragmaArgumentAssociationImpl
