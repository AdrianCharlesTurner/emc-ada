/**
 */
package Ada;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unconstrained Array Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link Ada.UnconstrainedArrayDefinition#getSloc <em>Sloc</em>}</li>
 *   <li>{@link Ada.UnconstrainedArrayDefinition#getIndexSubtypeDefinitionsQl <em>Index Subtype Definitions Ql</em>}</li>
 *   <li>{@link Ada.UnconstrainedArrayDefinition#getArrayComponentDefinitionQ <em>Array Component Definition Q</em>}</li>
 *   <li>{@link Ada.UnconstrainedArrayDefinition#getChecks <em>Checks</em>}</li>
 * </ul>
 *
 * @see Ada.AdaPackage#getUnconstrainedArrayDefinition()
 * @model extendedMetaData="name='Unconstrained_Array_Definition' kind='elementOnly'"
 * @generated
 */
public interface UnconstrainedArrayDefinition extends EObject {
	/**
	 * Returns the value of the '<em><b>Sloc</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sloc</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sloc</em>' containment reference.
	 * @see #setSloc(SourceLocation)
	 * @see Ada.AdaPackage#getUnconstrainedArrayDefinition_Sloc()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='sloc' namespace='##targetNamespace'"
	 * @generated
	 */
	SourceLocation getSloc();

	/**
	 * Sets the value of the '{@link Ada.UnconstrainedArrayDefinition#getSloc <em>Sloc</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sloc</em>' containment reference.
	 * @see #getSloc()
	 * @generated
	 */
	void setSloc(SourceLocation value);

	/**
	 * Returns the value of the '<em><b>Index Subtype Definitions Ql</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Index Subtype Definitions Ql</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Index Subtype Definitions Ql</em>' containment reference.
	 * @see #setIndexSubtypeDefinitionsQl(ExpressionList)
	 * @see Ada.AdaPackage#getUnconstrainedArrayDefinition_IndexSubtypeDefinitionsQl()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='index_subtype_definitions_ql' namespace='##targetNamespace'"
	 * @generated
	 */
	ExpressionList getIndexSubtypeDefinitionsQl();

	/**
	 * Sets the value of the '{@link Ada.UnconstrainedArrayDefinition#getIndexSubtypeDefinitionsQl <em>Index Subtype Definitions Ql</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Index Subtype Definitions Ql</em>' containment reference.
	 * @see #getIndexSubtypeDefinitionsQl()
	 * @generated
	 */
	void setIndexSubtypeDefinitionsQl(ExpressionList value);

	/**
	 * Returns the value of the '<em><b>Array Component Definition Q</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Array Component Definition Q</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Array Component Definition Q</em>' containment reference.
	 * @see #setArrayComponentDefinitionQ(ElementClass)
	 * @see Ada.AdaPackage#getUnconstrainedArrayDefinition_ArrayComponentDefinitionQ()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='array_component_definition_q' namespace='##targetNamespace'"
	 * @generated
	 */
	ElementClass getArrayComponentDefinitionQ();

	/**
	 * Sets the value of the '{@link Ada.UnconstrainedArrayDefinition#getArrayComponentDefinitionQ <em>Array Component Definition Q</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Array Component Definition Q</em>' containment reference.
	 * @see #getArrayComponentDefinitionQ()
	 * @generated
	 */
	void setArrayComponentDefinitionQ(ElementClass value);

	/**
	 * Returns the value of the '<em><b>Checks</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Checks</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checks</em>' attribute.
	 * @see #setChecks(String)
	 * @see Ada.AdaPackage#getUnconstrainedArrayDefinition_Checks()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='checks' namespace='##targetNamespace'"
	 * @generated
	 */
	String getChecks();

	/**
	 * Sets the value of the '{@link Ada.UnconstrainedArrayDefinition#getChecks <em>Checks</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checks</em>' attribute.
	 * @see #getChecks()
	 * @generated
	 */
	void setChecks(String value);

} // UnconstrainedArrayDefinition
